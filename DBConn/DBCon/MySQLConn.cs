﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using DBConn.Common;
using MySql.Data.MySqlClient;
//AddMySQL.Data from C:\Program Files (x86)\MySQL\MySQL Connector Net 6.10.6\Assemblies\v4.5.2
namespace DBConn.DbCon
{
    public class MySQLConn
    {
        #region Variables
        public static String strLog = String.Empty;
        #endregion

        private void OpenConnection(MySqlConnection myConnection)
        {
            myConnection.Open();
        }

        public MySqlDataReader GetData(String mySQL, String sConnStr)
        {
            MySqlDataReader myDataReader;

            try
            {
                MySqlCommand sqlCommand = new MySqlCommand();
                sqlCommand.CommandType = CommandType.Text;
                sqlCommand.CommandText = mySQL;
                sqlCommand.Connection = GetConnection(sConnStr);
                sqlCommand.Connection.Open();

                myDataReader = sqlCommand.ExecuteReader();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                throw new Exception(" " + ex.Message);
            }

            return myDataReader;
        }

        public DataSet GetDS(String mySQL, String sConnStr)
        {
            DataSet myDataSet = new DataSet();
            try
            {
                MySqlDataAdapter myDataAdapter = new MySqlDataAdapter(mySQL, GetConnection(sConnStr));
                myDataAdapter.Fill(myDataSet);
            }
            catch
            { }
            return myDataSet;
        }

        public DataSet GetDataSet(String sql, String sConnStr)
        {
            MySqlConnection CN = GetConnection(sConnStr);
            MySqlCommand sqlCmd = new MySqlCommand(sql, CN);
            MySqlDataAdapter sqldata = new MySqlDataAdapter();
            DataSet locdataset = new DataSet();
            try
            {
                CN.Open();
                sqldata.SelectCommand = sqlCmd;
                sqldata.Fill(locdataset);
            }
            catch { }
            finally
            {
                CN.Close();
                CN.Dispose();
            }
            return locdataset;
        }

        #region WS specifics
        public DataSet GetLatestGPSData(List<int> lAssetID, String strDB)
        {
            DataTable dtsql = dtSql();
            DataRow drSql = dtsql.NewRow();

            String strAssetID = String.Empty;
            foreach (int i in lAssetID)
                strAssetID += i.ToString() + ",";
            strAssetID = strAssetID.Substring(0, strAssetID.Length - 1);

            String sql = String.Empty;
            foreach (int i in lAssetID)
            {
                sql = @"SELECT top 2 UID, 
                            AssetID, 
                            DriverID,
                            DateTimeGPS_UTC, 
                            DateTimeServer, 
                            Longitude, 
                            Latitude, 
                            LongLatValidFlag, 
                            Speed, 
                            EgineOn, 
                            StopFlag, 
                            TripDistance, 
                            TripTime, 
                            WorkHour from GFI_GPS_GPSData
                            WHERE AssetID = " + i.ToString() + @" 
                                and DateTimeGPS_UTC < getdate() + 1
                                and LongLatValidFlag = 1
                            order by DateTimeGPS_UTC desc";

                drSql = dtsql.NewRow();
                drSql["sSQL"] = sql.Replace("                            ", " ");
                drSql["sTableName"] = "GPSData";
                dtsql.Rows.Add(drSql);
            }

            DataSet ds = GetDataDS(dtsql, strDB);
            int iChk = lAssetID.Count * 2;
            if (iChk != ds.Tables["GPSData"].Rows.Count)
            {
                foreach (int i in lAssetID)
                {
                    DataRow[] drchk = ds.Tables["GPSData"].Select("AssetID = " + i.ToString());
                    if (drchk.Length == 1)
                    {
                        DataRow drSame = ds.Tables["GPSData"].NewRow();
                        drSame.ItemArray = drchk[0].ItemArray;
                        ds.Tables["GPSData"].Rows.Add(drSame);
                    }
                }
            }
            String GPSDataUID = String.Empty;
            foreach (DataRow dr in ds.Tables["GPSData"].Rows)
                GPSDataUID += dr["UID"] + ",";

            if (GPSDataUID == String.Empty)
                GPSDataUID = "0";
            else
                GPSDataUID = GPSDataUID.Substring(0, GPSDataUID.Length - 1);

            dtsql.Clear();
            sql = "SELECT d.* from GFI_GPS_GPSData h, GFI_GPS_GPSDataDetail d ";
            sql += "where h.uid = d.uid and h.AssetID in (" + strAssetID + @") and h.UID in (" + GPSDataUID + @")";
            drSql = dtsql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "GPSDataDetail";
            dtsql.Rows.Add(drSql);

            sql = "select * from dbo.GFI_FLT_Asset where AssetID in (" + strAssetID + @")";
            drSql = dtsql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "GPSAsset";
            dtsql.Rows.Add(drSql);

            ds.Merge(GetDataDS(dtsql, strDB));
            return ds;
        }
        public DataTable GetExceptions(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            String strAssetID = String.Empty;
            foreach (int i in lAssetID)
                strAssetID += i.ToString() + ",";
            strAssetID = strAssetID.Substring(0, strAssetID.Length - 1);

            String sql = @"select v.sDescription Vehicle, er.sDescription ExRule, g.fLongitude, g.fLatitude, g.dtDateTime DateTime, g.fSpeed Speed, e.iExceptionEventID --, er.sExpression, '*******',* 
                            from CoreCM.dbo.Exceptions e, CoreCM.dbo.ExceptionEvent ev, CoreCM.dbo.ExceptionRules er, CoreCM.dbo.Vehicle v, CoreCM.dbo.GPSData g
                            where e.iExceptionEventID = ev.iID
	                            and ev.iExceptionRulesID = er.iID and er.sExpression is not null
	                            and ev.iVehicleID = v.iID
                            	and e.iGPSDataID = g.iID
	                            and v.sVIN COLLATE DATABASE_DEFAULT in 
		                            (select DeviceID COLLATE DATABASE_DEFAULT from CoreDB.dbo.GFI_FLT_AssetDeviceMap where AssetID in (" + strAssetID + @"))
                                and g.dtDateTime >= ? and g.dtDateTime <= ?";
            String strCMDB = GetDBName("Geotab");
            String strCoreDB = GetDBName("CoreConnStr");

            sql = sql.Replace("CoreDB.dbo", strCoreDB + ".dbo");
            sql = sql.Replace("CoreCM.dbo", strCMDB + ".dbo");

            //String strActualGbl = GblDB;
            //GblDB = "DB1";
            DataTable dt = GetDataDT(sql, dtFrom.ToString() + "¬" + dtTo.ToString(), null, sConnStr);
            //GblDB = strActualGbl;

            return dt;
        }
        #endregion
        public DataTable dtSql()
        {
            DataTable dtSql = new DataTable();
            dtSql.TableName = "dtSQL";
            dtSql.Columns.Add("sSQL");
            dtSql.Columns.Add("sParam");
            dtSql.Columns.Add("sTableName");

            return dtSql;
        }
        public DataSet GetDataDS(DataTable dtSQL, String strDB)
        {
            return GetDataDS(dtSQL, strDB, null, -1);
        }
        public DataSet GetDataDS(DataTable dtSQL, String sConnStr, DbTransaction tran, int timeout)
        {
            //dtSQL has 3 columns, sSQL, sParam which can be empty, and sTableName

            MySqlTransaction transaction = (MySqlTransaction)tran;
            //GetConn(strDB);
            Boolean bAutoCommit = false;
            if (transaction == null)
                bAutoCommit = true;

            MySqlConnection conn;
            if (bAutoCommit)
                conn = GetConnection(sConnStr);
            else
                conn = transaction.Connection;

            DataSet ds = new DataSet();
            String mySQL = String.Empty;

            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = conn;
            cmd.Transaction = transaction;
            if (timeout > 0)
                cmd.CommandTimeout = timeout;

            try
            {
                foreach (DataRow dr in dtSQL.Rows)
                {
                    cmd.Parameters.Clear();
                    DataTable myDataTable = new DataTable();

                    mySQL = dr["sSQL"].ToString();
                    mySQL = mySQL.Replace("GetDate()", "NOW()");
                    mySQL = mySQL.Replace("Getdate()", "NOW()");
                    mySQL = mySQL.Replace("getdate()", "NOW()");
                    String strParam = dr["sParam"].ToString();

                    Boolean bFound = mySQL.Contains("?");
                    int iCount = 1;
                    while (bFound)
                    {
                        int i = mySQL.IndexOf("?");
                        mySQL = ReplaceFirst(mySQL, "?", "@param" + iCount.ToString());
                        bFound = mySQL.Contains("?");
                        iCount++;
                    }

                    cmd.CommandText = mySQL;
                    iCount = 1;
                    foreach (String s in strParam.Split('¬'))
                        if (s.Trim().Length > 0)
                        {
                            cmd.Parameters.Add(new MySqlParameter("@param" + iCount.ToString(), (object)s));
                            cmd.Parameters["@param" + iCount.ToString()].Value = s;
                            iCount++;
                        }

                    MySqlDataAdapter myDataAdapter = new MySqlDataAdapter(cmd);

                    if (dr["sTableName"].ToString() == "MultipleDTfromQuery")
                    {
                        DataSet dsMultiple = new DataSet();
                        myDataAdapter.Fill(dsMultiple);

                        foreach (DataTable dt in dsMultiple.Tables)
                        {
                            try
                            {
                                dsMultiple.Tables[dsMultiple.Tables.IndexOf(dt) + 1].TableName = dt.Rows[0]["TableName"].ToString();
                                ds.Merge(dsMultiple.Tables[dsMultiple.Tables.IndexOf(dt) + 1]);
                            }
                            catch { }
                        }
                    }
                    else
                    {
                        myDataAdapter.Fill(myDataTable);
                        myDataTable.TableName = dr["sTableName"].ToString();
                        ds.Merge(myDataTable);
                    }
                }
            }
            catch (Exception ex)
            {
                GenAudit("EXRD", "GetDT", "", "", "", mySQL + " exception " + ex.ToString(), sConnStr);
                DataTable dt = new DataTable("dtErr");
                dt.Columns.Add("sSQL");
                dt.Columns.Add("sError");
                DataRow dr = dt.NewRow();
                dr[0] = mySQL;
                dr[1] = ex.ToString();
                dt.Rows.Add(dr);
                ds.Merge(dt);
                //throw ex;
            }
            finally
            {
                if (bAutoCommit)
                    conn.Close();
            }

            return ds;
        }

        public DataTable GetDataDT(String mySQL, String sConnStr)
        {
            return GetDataDT(mySQL, String.Empty, null, sConnStr, -1);
        }
        public DataTable GetDataDT(String mySQL, DbTransaction transaction, String sConnStr)
        {
            return GetDataDT(mySQL, String.Empty, (MySqlTransaction)transaction, sConnStr, -1);
        }
        public DataTable GetDataDT(String mySQL, String strParams, DbTransaction transaction, String sConnStr)
        {
            return GetDataDT(mySQL, strParams, (MySqlTransaction)transaction, sConnStr, -1);
        }
        DataTable GetDataDT(String mySQL, String strParam, MySqlTransaction transaction, String sConnStr, int timeout)
        {
            //GetConn(strDB);
            Boolean bAutoCommit = false;
            if (transaction == null)
                bAutoCommit = true;

            MySqlConnection conn;
            if (bAutoCommit)
                conn = GetConnection(sConnStr);
            else
                conn = transaction.Connection;

            DataTable myDataTable = new DataTable();
            try
            {
                Boolean bFound = mySQL.Contains("?");
                int iCount = 1;
                while (bFound)
                {
                    int i = mySQL.IndexOf("?");
                    mySQL = ReplaceFirst(mySQL, "?", "@param" + iCount.ToString());
                    bFound = mySQL.Contains("?");
                    iCount++;
                }

                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandText = mySQL;
                iCount = 1;
                foreach (String s in strParam.Split('¬'))
                    if (s.Trim().Length > 0)
                    {
                        cmd.Parameters.Add(new MySqlParameter("@param" + iCount.ToString(), (object)s));
                        cmd.Parameters["@param" + iCount.ToString()].Value = s;
                        iCount++;
                    }
                cmd.Connection = conn;
                cmd.Transaction = transaction;
                if (timeout > 0)
                    cmd.CommandTimeout = timeout;
                MySqlDataAdapter myDataAdapter = new MySqlDataAdapter(cmd);
                myDataAdapter.Fill(myDataTable);
            }
            catch (Exception ex)
            {
                GenAudit("EXRD", "GetDT", "", "", "", mySQL + " exception " + ex.ToString(), sConnStr);
                throw ex;
            }
            finally
            {
                if (bAutoCommit)
                    conn.Close();
            }
            return myDataTable;
        }

        public void RunStoreProc(System.Xml.XmlDocument xmldoc, String sp, String sConnStr)
        {
            MySqlConnection sqlConn = GetConnection(sConnStr);
            sqlConn.Open();

            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = sqlConn;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = sp;
            cmd.Parameters.AddWithValue("@doc", xmldoc.OuterXml);
            cmd.ExecuteNonQuery();
            sqlConn.Close();
        }

        public DataTable GetDataTableBy(String mySQL, String id, String sConnStr)
        {
            //GetConn(strDB);
            mySQL = mySQL.Replace("?", "@param0");
            DataTable dt = new DataTable();
            using (MySqlConnection conn = MySQLConn.GetConnection(sConnStr))
            {
                try
                {
                    using (MySqlCommand orcCmd = new MySqlCommand(mySQL, conn))
                    {
                        orcCmd.Parameters.Add(new MySqlParameter("@param0", (object)id));
                        orcCmd.Parameters["@param0"].Value = id;
                        using (MySqlDataAdapter OrcAdapter = new MySqlDataAdapter(orcCmd))
                        {
                            OrcAdapter.Fill(dt);
                        }
                    }
                }
                catch (Exception ex)
                {
                    //MessageBox.Show(ex.ToString());
                    //throw new Exception(" " + ex.Message);
                    GenAudit("EXRD", "GetDTBy", "", "", "", mySQL + " exception " + ex.ToString(), sConnStr);
                }
                finally
                {
                    conn.Close();
                }
            }
            return dt;
        }

        public int GetNumberOfRecords(String pTable, String pColumn, String pWhere, String sConnStr)
        {
            int count = -1;
            String strSql;

            MySqlCommand _sqlTrans = new MySqlCommand();
            MySqlConnection conn = GetConnection(sConnStr);
            try
            {
                // Open the connection
                conn.Open();

                strSql = "select count(" + pColumn + ") from " + pTable;
                if (pWhere != null || pWhere != "")
                    strSql += " Where " + pWhere;

                _sqlTrans.CommandText = strSql;
                _sqlTrans.Connection = conn;
                count = Convert.ToInt32(_sqlTrans.ExecuteScalar());
            }
            catch { }
            finally
            {
                // Close the connection
                conn.Close();
            }
            return count;
        }
        public int GetNextID(String strTableName, DbTransaction transaction, String sConnStr)
        {
            int iResult = 0;
            Boolean bAutoCommit = false;
            if (transaction == null)
                bAutoCommit = true;

            MySqlConnection conn;
            MySqlTransaction transs = (MySqlTransaction)transaction;
            if (bAutoCommit)
                conn = MySQLConn.GetConnection(sConnStr);
            else
                conn = transs.Connection;

            String sqlString = @"SELECT AUTO_INCREMENT as CN FROM information_schema.tables WHERE table_name = '" + strTableName + "' AND table_schema = DATABASE( ) ;";

            strLog = String.Empty;
            MySqlCommand SqlCmd = new MySqlCommand(sqlString, conn);
            try
            {
                if (bAutoCommit)
                {
                    conn.Open();
                    transaction = conn.BeginTransaction();
                }

                DataTable lDt = GetDataDT(sqlString, sConnStr);
                if (lDt.Rows[0]["CN"].ToString() == "2")
                {
                    String sql = "select * from " + strTableName;
                    DataTable dt = GetDataDT(sql, sConnStr);
                    iResult = dt.Rows.Count + 1;
                }
                else
                    iResult = Convert.ToInt32(lDt.Rows[0]["CN"].ToString());
            }
            catch (Exception ex)
            {
            }
            finally
            {
                if (bAutoCommit)
                    conn.Close();
            }
            return iResult;
        }

        public int dbExecute(String strSQl, String sConnStr)
        {
            return dbExecute(strSQl, null, 0, sConnStr);
        }
        public int dbExecute(String strSQl, DbTransaction transaction, String sConnStr)
        {
            return dbExecute(strSQl, (MySqlTransaction)transaction, 0, sConnStr);
        }
        public int dbExecute(String strSQl, MySqlTransaction transaction, int timeout, String sConnStr)
        {
            //The time in seconds to wait for the command to execute. The default is 30 seconds.
            int iReturn = 0;

            Boolean bAutoCommit = false;
            if (transaction == null)
                bAutoCommit = true;

            MySqlConnection conn;
            if (bAutoCommit)
                conn = MySQLConn.GetConnection(sConnStr);
            else
                conn = transaction.Connection;

            MySqlCommand _sqlTrans = new MySqlCommand();
            if (timeout > 0)
                _sqlTrans.CommandTimeout = timeout;

            if (bAutoCommit)
            {
                conn.Open();
                transaction = conn.BeginTransaction();
            }

            try
            {
                _sqlTrans.CommandText = strSQl;
                _sqlTrans.Connection = conn;
                _sqlTrans.Transaction = transaction;
                iReturn = _sqlTrans.ExecuteNonQuery();

                if (bAutoCommit)
                    transaction.Commit();
            }
            catch (Exception ex)
            {
                if (bAutoCommit)
                    transaction.Rollback();

                GenAudit("EXRD", "DBEXEC", "", "", "", strSQl + " exception " + ex.ToString(), sConnStr);
                throw new Exception(ex.Message);
            }
            finally
            {
                if (bAutoCommit)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            return iReturn;
        }
        public DataSet dbExecuteNSelect(String strSQL, MySqlTransaction transaction, int timeout, String sConnStr)
        {
            DataSet dsResult = new DataSet();

            Boolean bAutoCommit = false;
            if (transaction == null)
                bAutoCommit = true;

            MySqlConnection conn;
            if (bAutoCommit)
                conn = MySQLConn.GetConnection(sConnStr);
            else
                conn = transaction.Connection;

            MySqlCommand dbCommand = new MySqlCommand();
            if (timeout > 0)
                dbCommand.CommandTimeout = timeout;

            if (bAutoCommit)
            {
                try
                {
                    conn.Open();
                    transaction = conn.BeginTransaction();
                }
                catch (Exception ex)
                {

                }
            }

            dbCommand.Transaction = transaction;
            try
            {
                MySqlDataAdapter dbAdapter = new MySqlDataAdapter();
                dbCommand.CommandText = strSQL;
                dbCommand.Connection = conn;
                dbAdapter.SelectCommand = dbCommand;
                dbAdapter.Fill(dsResult);

                if (bAutoCommit)
                    transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();

                GenAudit("EXRD", "DBEXEC", "", "", "", strSQL + " exception " + ex.ToString(), sConnStr);
                throw new Exception(ex.Message);
            }
            finally
            {
                if (bAutoCommit)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            return dsResult;
        }

        public Boolean ScriptExecute(String strSQl, String sConnStr, int iTimeOut = -1)
        {
            MySqlConnection conn = MySQLConn.GetConnection(sConnStr);
            using (var connection = conn)
            {
                var script = new MySqlScript(connection, strSQl);
                connection.Open();
                script.Execute();
                return true;
            }
            return false;
        }
        public int dbExecuteWithoutTransaction(String strSQl, int iTimeOut, String sConnStr)
        {
            int iReturn = 0;

            MySqlConnection conn = MySQLConn.GetConnection(sConnStr);
            conn.Open();
            MySqlCommand sqlCmd = new MySqlCommand();
            if (iTimeOut > 0)
                sqlCmd.CommandTimeout = iTimeOut;

            try
            {
                sqlCmd.CommandText = strSQl;
                sqlCmd.Connection = conn;
                iReturn = sqlCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                GenAudit("EXRD", "DBEXEC", "", "", "", strSQl + " exception " + ex.ToString(), sConnStr);
                throw new Exception(ex.Message);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
            return iReturn;
        }

        public int GetNumberOfRows(String strSQl, MySqlTransaction transaction, int timeout, String sConnStr)
        {
            int iReturn = 0;

            Boolean bAutoCommit = false;
            if (transaction == null)
                bAutoCommit = true;

            MySqlConnection conn;
            if (bAutoCommit)
                conn = MySQLConn.GetConnection(sConnStr);
            else
                conn = transaction.Connection;

            MySqlCommand _sqlTrans = new MySqlCommand();
            if (timeout > 0)
                _sqlTrans.CommandTimeout = timeout;

            if (bAutoCommit)
            {
                conn.Open();
                //transaction = conn.BeginTransaction();
            }

            DataTable myDataTable = new DataTable();
            try
            {
                _sqlTrans.CommandText = strSQl;
                _sqlTrans.Connection = conn;
                _sqlTrans.Transaction = transaction;
                if (timeout > 0)
                    _sqlTrans.CommandTimeout = timeout;
                MySqlDataAdapter myDataAdapter = new MySqlDataAdapter(_sqlTrans);
                myDataAdapter.Fill(myDataTable);

                iReturn = myDataTable.Rows.Count;

                //if (bAutoCommit)
                //    transaction.Commit();
            }
            catch (Exception ex)
            {
                GenAudit("EXRD", "DBEXEC", "", "", "", strSQl + " exception " + ex.ToString(), sConnStr);
                transaction.Rollback();

                throw new Exception(ex.Message);
            }
            finally
            {
                if (bAutoCommit)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            return iReturn;
        }

        public static String ReplaceFirst(String s, String find, String replace)
        {
            var first = s.IndexOf(find);
            return s.Substring(0, first) + replace + s.Substring(first + find.Length);
        }
        public Boolean GenInsert(DataTable DTInsert, String sConnStr)
        {
            return GenInsert(DTInsert, (MySqlTransaction)null, sConnStr);
        }
        public Boolean GenInsert(DataTable DTInsert, DbTransaction dbTran, String sConnStr)
        {
            if (dbTran != null)
                return GenInsert(DTInsert, (MySqlTransaction)dbTran, sConnStr);
            else
                return GenInsert(DTInsert, (MySqlTransaction)null, sConnStr);
        }
        Boolean GenInsert(DataTable DTInsert, MySqlTransaction transaction, String sConnStr)
        {
            strLog = String.Empty;
            bool insert = false;
            Boolean bAutoCommit = false;

            if (transaction == null)
                bAutoCommit = true;

            MySqlConnection conn;
            if (bAutoCommit)
                conn = MySQLConn.GetConnection(sConnStr);
            else
                conn = transaction.Connection;

            String sqlString = "INSERT INTO " + DTInsert.TableName;
            sqlString += "(";
            foreach (DataColumn dc in DTInsert.Columns)
                sqlString += dc.ColumnName + ", ";
            sqlString = sqlString.Substring(0, sqlString.Length - 2) + " ) values (";
            foreach (DataColumn dc in DTInsert.Columns)
                sqlString += "?, ";
            sqlString = sqlString.Substring(0, sqlString.Length - 2) + " )";

            Boolean bFound = sqlString.Contains("?");
            int iCount = 1;
            while (bFound)
            {
                int i = sqlString.IndexOf("?");
                sqlString = ReplaceFirst(sqlString, "?", "@param" + iCount.ToString());
                bFound = sqlString.Contains("?");
                iCount++;
            }

            MySqlCommand SqlCmd = new MySqlCommand(sqlString, conn);
            try
            {
                if (bAutoCommit)
                {
                    conn.Open();
                    transaction = conn.BeginTransaction();
                }
                try
                {
                    iCount = 1;
                    foreach (DataRow dr in DTInsert.Rows)
                        foreach (DataColumn dc in DTInsert.Columns)
                        {
                            SqlCmd.Parameters.Add(new MySqlParameter("@param" + iCount.ToString(), (object)dr[dc.ColumnName]));
                            SqlCmd.Parameters["@param" + iCount.ToString()].Value = dr[dc.ColumnName];
                            iCount++;

                            strLog = strLog + dr[dc.ColumnName].ToString() + ",";
                        }

                    SqlCmd.Transaction = transaction;
                    SqlCmd.ExecuteNonQuery();

                    if (bAutoCommit)
                        transaction.Commit();

                    //GenAudit("IN", "INSERT", "", DTInsert.TableName, "", SqlCmd.CommandText.ToString() + " Values [" + strLog + "]");
                    insert = true;
                }
                catch (Exception ex)
                {
                    if (bAutoCommit)
                        transaction.Rollback();

                    //GenAudit("EXIN", "INSERT", "", "", "", SqlCmd.CommandText.ToString() + " Values [" + strLog + "] + exception " + ex.ToString());
                    insert = false;
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                //GenAudit("EXIN", "INSERT", "", "", "", SqlCmd.CommandText.ToString() + " Values [" + strLog + "] + exception " + ex.ToString());
                insert = false;
                throw ex;
            }
            finally
            {
                if (bAutoCommit)
                    conn.Close();
            }
            return insert;
        }

        public Boolean InsertGPSData(DataTable DTInsert, Int32 UID, int AssetID, DbTransaction dbTran, String sConnStr)
        {
            MySqlTransaction transaction = (MySqlTransaction)dbTran;
            strLog = String.Empty;
            bool insert = false;
            Boolean bAutoCommit = false;

            if (transaction == null)
                bAutoCommit = true;

            MySqlConnection conn;
            if (bAutoCommit)
                conn = MySQLConn.GetConnection(sConnStr);
            else
                conn = transaction.Connection;

            String sqlString = "INSERT INTO " + DTInsert.TableName;
            sqlString += "(";
            foreach (DataColumn dc in DTInsert.Columns)
                sqlString += dc.ColumnName + ", ";
            sqlString = sqlString.Substring(0, sqlString.Length - 2) + " ) values (";
            foreach (DataColumn dc in DTInsert.Columns)
                sqlString += "?, ";
            sqlString = sqlString.Substring(0, sqlString.Length - 2) + " )";

            Boolean bFound = sqlString.Contains("?");
            int iCount = 1;
            while (bFound)
            {
                int i = sqlString.IndexOf("?");
                sqlString = ReplaceFirst(sqlString, "?", "@param" + iCount.ToString());
                bFound = sqlString.Contains("?");
                iCount++;
            }

            MySqlCommand SqlCmd = new MySqlCommand(sqlString, conn);
            try
            {
                if (bAutoCommit)
                {
                    conn.Open();
                    transaction = conn.BeginTransaction();
                }
                try
                {
                    iCount = 1;
                    foreach (DataRow dr in DTInsert.Rows)
                        foreach (DataColumn dc in DTInsert.Columns)
                        {
                            SqlCmd.Parameters.Add(new MySqlParameter("@param" + iCount.ToString(), (object)dr[dc.ColumnName]));
                            SqlCmd.Parameters["@param" + iCount.ToString()].Value = dr[dc.ColumnName];
                            iCount++;

                            strLog = strLog + dr[dc.ColumnName].ToString() + ",";
                        }

                    SqlCmd.Transaction = transaction;
                    SqlCmd.ExecuteNonQuery();

                    //Live
                    sqlString = sqlString.Replace("GFI_GPS_GPSData(", "GFI_GPS_LiveData(UID,");
                    sqlString = sqlString.Replace("values (@param1", "values (" + UID.ToString() + ",@param1");
                    SqlCmd.CommandText = sqlString;
                    SqlCmd.ExecuteNonQuery();

                    SqlCmd.CommandText = "delete from GFI_GPS_LiveData where AssetID = " + AssetID.ToString() + @" and LongLatValidFlag = 0";
                    SqlCmd.ExecuteNonQuery();

                    sqlString = @";with cte as (
                                select uid, AssetID, 
                                    DriverID,
                                    DateTimeGPS_UTC, 
                                    LongLatValidFlag, 
                                    row_number() over(partition by AssetID order by DateTimeGPS_UTC desc) as RowNum
	                            from GFI_GPS_LiveData
                                WHERE AssetID = " + AssetID.ToString() + @"
                                    and DateTimeGPS_UTC < getdate() + 1
                                    --and LongLatValidFlag = 1 
	                               )
                            delete from GFI_GPS_LiveData 
	                            where uid in
		                            (select uid from cte where RowNum > 6)";
                    SqlCmd.CommandText = sqlString;
                    SqlCmd.ExecuteNonQuery();

                    if (bAutoCommit)
                        transaction.Commit();

                    insert = true;
                }
                catch (Exception ex)
                {
                    if (bAutoCommit)
                        transaction.Rollback();

                    insert = false;
                }
            }
            catch (Exception ex)
            {
                insert = false;
            }
            finally
            {
                if (bAutoCommit)
                    conn.Close();
            }
            return insert;
        }
        public Boolean InsertGPSDataDetail(DataTable DTInsert, Int32 UID, DbTransaction dbTran, String sConnStr)
        {
            MySqlTransaction transaction = (MySqlTransaction)dbTran;
            strLog = String.Empty;
            bool insert = false;
            Boolean bAutoCommit = false;

            if (transaction == null)
                bAutoCommit = true;

            MySqlConnection conn;
            if (bAutoCommit)
                conn = MySQLConn.GetConnection(sConnStr);
            else
                conn = transaction.Connection;

            String sqlString = "INSERT INTO " + DTInsert.TableName;
            sqlString += "(";
            foreach (DataColumn dc in DTInsert.Columns)
                sqlString += dc.ColumnName + ", ";
            sqlString = sqlString.Substring(0, sqlString.Length - 2) + " ) values (";
            foreach (DataColumn dc in DTInsert.Columns)
                sqlString += "?, ";
            sqlString = sqlString.Substring(0, sqlString.Length - 2) + " )";

            Boolean bFound = sqlString.Contains("?");
            int iCount = 1;
            while (bFound)
            {
                int i = sqlString.IndexOf("?");
                sqlString = ReplaceFirst(sqlString, "?", "@param" + iCount.ToString());
                bFound = sqlString.Contains("?");
                iCount++;
            }

            MySqlCommand SqlCmd = new MySqlCommand(sqlString, conn);
            try
            {
                if (bAutoCommit)
                {
                    conn.Open();
                    transaction = conn.BeginTransaction();
                }
                try
                {
                    iCount = 1;
                    foreach (DataRow dr in DTInsert.Rows)
                        foreach (DataColumn dc in DTInsert.Columns)
                        {
                            SqlCmd.Parameters.Add(new MySqlParameter("@param" + iCount.ToString(), (object)dr[dc.ColumnName]));
                            SqlCmd.Parameters["@param" + iCount.ToString()].Value = dr[dc.ColumnName];
                            iCount++;

                            strLog = strLog + dr[dc.ColumnName].ToString() + ",";
                        }

                    SqlCmd.Transaction = transaction;
                    SqlCmd.ExecuteNonQuery();

                    //Live
                    try
                    {
                        sqlString = sqlString.Replace("GFI_GPS_GPSDataDetail(", "GFI_GPS_LiveDataDetail(");
                        SqlCmd.CommandText = sqlString;
                        SqlCmd.ExecuteNonQuery();
                    }
                    catch
                    {
                        //This is a normal situation, assuming it is an old data.
                        //Old data. Header already deleted, deleted just after insert.
                        //Bypassing DeleteOldLive to gain time
                    }

                    if (bAutoCommit)
                        transaction.Commit();

                    insert = true;
                }
                catch (Exception ex)
                {
                    if (bAutoCommit)
                        transaction.Rollback();

                    insert = false;
                }
            }
            catch (Exception ex)
            {
                insert = false;
            }
            finally
            {
                if (bAutoCommit)
                    conn.Close();
            }
            return insert;
        }
        public Boolean DeleteOldLive(int AssetID, DbTransaction dbTran)
        {
            Boolean bResult = false;
            String sql = @";with cte as (
                                select uid, AssetID, 
                                    DriverID,
                                    DateTimeGPS_UTC, 
                                    LongLatValidFlag, 
                                    row_number() over(partition by AssetID order by DateTimeGPS_UTC desc) as RowNum
	                            from GFI_GPS_LiveData
                                WHERE AssetID = " + AssetID.ToString() + @"
                                    and DateTimeGPS_UTC < getdate() + 1
                                    and LongLatValidFlag = 1 
	                               )
                            delete from GFI_GPS_LiveData 
	                            where uid in
		                            (select uid from cte where RowNum > 6)";

            MySqlTransaction sqlTran = (MySqlTransaction)dbTran;
            MySqlConnection conn = sqlTran.Connection;
            MySqlCommand SqlCmd = new MySqlCommand(sql, conn);
            try
            {
                SqlCmd.Transaction = sqlTran;
                SqlCmd.CommandText = sql;
                SqlCmd.ExecuteNonQuery();

                bResult = true;
            }
            catch
            {
                bResult = false;
            }
            return bResult;
        }

        public DataTable CTRLTBL()
        {
            // dtSQL above return DS needed after process data.

            DataTable CTRLTBL = new DataTable();
            //To be filled by Developper
            CTRLTBL.Columns.Add("SeqNo", typeof(int));          //Order of tables to be updated
            CTRLTBL.Columns.Add("TblName");                     //TblName
            CTRLTBL.Columns.Add("CmdType");                     //CmdType : Table, StoredProc, Chk, spToGetID. Chk checks on db and returns rows. 0 set bOPRStatus true else false.
            CTRLTBL.Columns.Add("Command");                     //Unused for now
            CTRLTBL.Columns.Add("KeyFieldName");                //Primary key of table
            CTRLTBL.Columns.Add("KeyIsIdentity");               //Primary key is an Identity field
            CTRLTBL.Columns.Add("NextIdAction");                //Get\Set Primary\Foreign keys; GetFromSP, GetFromField updates CtrlTable with right values then loops in data Tables and updares fields accordingly
            CTRLTBL.Columns.Add("NextIdFieldName");             //Primary key of table that need Get\Set - Parent Primary key or Child foreign key
            CTRLTBL.Columns.Add("ParentTblName");               //Child Parent table
            CTRLTBL.Columns.Add("TimeOut", typeof(int));        //SQL TimeOut
            //To be used inside Process Data
            CTRLTBL.Columns.Add("NextIdValue");                 //Used to store Next Id values
            CTRLTBL.Columns.Add("RecAffected");                 //Returns number of rows affrected
            CTRLTBL.Columns.Add("ExceptionInfo");               //Exception Info
            CTRLTBL.Columns.Add("UpdateStatus");                //True if all updates successfull/False if not all updates successful

            CTRLTBL.TableName = "CTRLTBL";
            return CTRLTBL;

            //The following column needs to be added in the affected Data tables
            //  oprType     INSERT UPDATE DELETE
            //  OPRSeqNo    int for tables where there are many 'one to many' for same header\details. this field is therefore optional

            //Return values
            //if (objCTRLTBL.Rows[0]["UpdateStatus"].ToString() == "TRUE")
            //    return true;
            //else
            //    return false;
        }
        public DataSet ProcessData(DataSet DSProc, String sConnStr, Boolean bUseTransaction = true)
        {
            //Need To add timeout. it should be in control table and applied in switch (strCmdType)

            //Explanation in CTRLTBL above.
            Boolean bOPRStatus = true;

            MySqlConnection sqlConn = new MySqlConnection();
            DbTransaction DBTran = null;

            DSProc.Tables["CTRLTBL"].DefaultView.Sort = "SeqNo ASC";
            DataTable CTRLTBL = DSProc.Tables["CTRLTBL"].DefaultView.ToTable();
            try
            {
                sqlConn = MySQLConn.GetConnection(sConnStr);
                sqlConn.Open();

                if (bUseTransaction)
                    DBTran = sqlConn.BeginTransaction();

                MySQLConn myConn = new MySQLConn();

                #region GET
                //Gets Next Id for related table
                DataTable dtNextID = new DataTable(); //making tblname unique
                dtNextID.Columns.Add("TblName");
                dtNextID.Columns.Add("iPlus", typeof(int));
                foreach (DataRow dr in CTRLTBL.Rows)
                {
                    if (dr["NextIdAction"].ToString().ToUpper() == "GET")
                    {
                        int iPlus = 0;
                        DataRow[] drSearch = dtNextID.Select("TblName = '" + dr["TblName"].ToString().Trim() + "'");
                        if (drSearch.Length == 0)
                            dtNextID.Rows.Add(dr["TblName"].ToString().Trim(), 0);
                        else
                        {
                            foreach (DataRow drs in dtNextID.Rows)
                            {
                                if (drs["TblName"].ToString() == dr["TblName"].ToString())
                                {
                                    iPlus = (int)drs["iPlus"] + 1;
                                    drs["iPlus"] = iPlus;
                                }
                            }
                        }

                        int iNextId = myConn.GetNextID(dr["TblName"].ToString(), DBTran, sConnStr);
                        dr["NextIdValue"] = iNextId + iPlus;
                    }
                    else if (dr["NextIdAction"].ToString().ToUpper() == "GETFROMSP")
                    {
                        //SavePlanning
                        String sql = @"declare @t table (name varchar(100))
                                        insert @t (name)
                                      " + dr["Command"].ToString() + @"
                                        select * from @t";

                        DataTable dt = GetDataDT(sql, sConnStr);
                        dr["NextIdValue"] = dt.Rows[0][0].ToString();
                    }
                }
                #endregion

                #region SET
                //Sets Next Id for related table
                foreach (DataRow drCTRL in CTRLTBL.Rows)
                {
                    String strTblName = drCTRL["TblName"].ToString();
                    String strNextIdAction = drCTRL["NextIdAction"].ToString().ToUpper();
                    String strParentTblName = drCTRL["ParentTblName"].ToString();
                    String strNextIdFieldName = drCTRL["NextIdFieldName"].ToString();

                    if ((strNextIdAction == "SET") && (!String.IsNullOrEmpty(strParentTblName)))
                    {
                        DataRow[] drParentRow = CTRLTBL.Select("TblName = '" + drCTRL["ParentTblName"].ToString() + "' and NextIdAction = 'GET'");

                        foreach (DataRow drParent in drParentRow)
                        {
                            int iNextId = int.Parse(drParent["NextIdValue"].ToString());

                            //used to ensure that we get a value, in cases where there are multiple set for same child table, but from multiple get.
                            //assumption, 1 get\set using seqno, the other using only from 1 main get
                            //example where wrking is in save rules, where we save many 'one to many' config and configdetails, and config from rules parentid 
                            if (DSProc.Tables[strTblName].Columns.Contains("OPRSeqNo"))
                            {
                                Boolean bFound = false;
                                foreach (DataRow dr in DSProc.Tables[strTblName].Rows)
                                    if (dr["OPRSeqNo"].ToString() == drParent["SeqNo"].ToString())
                                    {
                                        dr[strNextIdFieldName] = iNextId;
                                        bFound = true;
                                    }
                                if (!bFound)
                                    foreach (DataRow dr in DSProc.Tables[strTblName].Rows)
                                        dr[strNextIdFieldName] = iNextId;
                            }
                            else
                                foreach (DataRow dr in DSProc.Tables[strTblName].Rows)
                                    dr[strNextIdFieldName] = iNextId;
                        }
                    }
                    else if ((strNextIdAction.ToUpper() == "GETFROMSP") && (!String.IsNullOrEmpty(drCTRL["NextIdValue"].ToString())))
                    {
                        String fieldName = drCTRL["KeyFieldName"].ToString();
                        if (DSProc.Tables[strParentTblName].Columns.Contains("OPRSeqNo"))
                        {
                            Boolean bFound = false;
                            foreach (DataRow dr in DSProc.Tables[strParentTblName].Rows)
                                if (dr["OPRSeqNo"].ToString() == drCTRL["SeqNo"].ToString())
                                {
                                    dr[fieldName] = drCTRL["NextIdValue"].ToString();
                                    bFound = true;
                                }
                            if (!bFound)
                                foreach (DataRow dr in DSProc.Tables[strParentTblName].Rows)
                                    dr[fieldName] = drCTRL["NextIdValue"].ToString();
                        }
                        else
                        {
                            String sNextId = drCTRL["NextIdValue"].ToString();
                            foreach (DataRow dr in DSProc.Tables[strParentTblName].Rows)
                                dr[fieldName] = sNextId;
                        }
                    }
                    else if ((strNextIdAction.ToUpper() == "GETFROMFIELD"))
                    {
                        String nextFieldName = drCTRL["NextIdFieldName"].ToString();
                        String fieldName = drCTRL["KeyFieldName"].ToString();
                        DataRow[] drParentRow = CTRLTBL.Select("ParentTblName = '" + drCTRL["ParentTblName"].ToString() + "' and KeyFieldName = '" + fieldName + "' and NextIdValue is not null");
                        if (drParentRow.Length > 0)
                            foreach (DataRow dr in DSProc.Tables[strParentTblName].Rows)
                                dr[nextFieldName] = drParentRow[0]["NextIdValue"].ToString();
                    }
                }
                #endregion

                #region SUBMIT
                //Submits Inserts/Updates/Deletes
                List<String> sTblName = new List<String>(); //Checking if TblName has already been processed
                foreach (DataRow drCTRL in CTRLTBL.Rows)
                {
                    String strTableName = drCTRL["TblName"].ToString();
                    int rowIndex = CTRLTBL.Rows.IndexOf(drCTRL);

                    if (sTblName.Contains(strTableName))
                    {
                        CTRLTBL.Rows[rowIndex]["UpdateStatus"] = true;
                        continue;   // TblName already processed
                    }
                    sTblName.Add(strTableName);

                    int iRecAffected = 0;

                    String strCmdType = drCTRL["CmdType"].ToString().ToUpper();
                    String strCommand = drCTRL["Command"].ToString();//.ToUpper();
                    String strKeyFieldName = drCTRL["KeyFieldName"].ToString();
                    String strKeyIsIdentity = drCTRL["KeyIsIdentity"].ToString();

                    int iTimeOut = -1;
                    String sTimeOut = drCTRL["TimeOut"].ToString();
                    if (sTimeOut.Length > 0)
                        iTimeOut = Convert.ToInt32(sTimeOut);

                    CTRLTBL.Rows[rowIndex]["RecAffected"] = 0;

                    switch (strCmdType)
                    {
                        case "CHK":
                            // Still testing. need to test with ADM where we need to ensure that only 1 device id is saved on db
                            int iResult = myConn.GetNumberOfRows(strCommand, (MySqlTransaction)DBTran, -1, sConnStr);
                            if (iResult > 0)
                                bOPRStatus = false;
                            else
                                bOPRStatus = bOPRStatus && true;
                            break;
                        case "STOREDPROC":
                            int iRAffected = 0;
                            if (bUseTransaction)
                                iRAffected = myConn.dbExecute(strCommand, (MySqlTransaction)DBTran, iTimeOut, sConnStr);
                            else
                                iRAffected = myConn.dbExecuteWithoutTransaction(strCommand, iTimeOut, sConnStr);
                            bOPRStatus = bOPRStatus && true;
                            iRecAffected += iRAffected;
                            CTRLTBL.Rows[rowIndex]["RecAffected"] = iRecAffected;
                            break;

                        case "TABLE":
                            for (int ir = 0; ir < DSProc.Tables[strTableName].Rows.Count; ir++)
                            {
                                String sWhereClause = String.Empty;

                                DataTable objDTTemp = DSProc.Tables[strTableName].Clone();
                                objDTTemp.TableName = drCTRL["TblName"].ToString();
                                objDTTemp.ImportRow(DSProc.Tables[drCTRL["TblName"].ToString()].Rows[ir]);

                                if (objDTTemp.Columns.Contains("lMatrix"))
                                    objDTTemp.Columns.Remove("lMatrix");
                                if (objDTTemp.Columns.Contains("OPRSeqNo"))
                                    objDTTemp.Columns.Remove("OPRSeqNo");
                                objDTTemp.Columns.Remove("oprType");

                                if ((strKeyIsIdentity == "TRUE") && objDTTemp.Columns.Contains(strKeyFieldName))
                                    objDTTemp.Columns.Remove(strKeyFieldName);

                                switch ((DataRowState)DSProc.Tables[strTableName].Rows[ir]["oprType"])
                                {
                                    case DataRowState.Added:        //4
                                        bOPRStatus = bOPRStatus & myConn.GenInsert(objDTTemp, DBTran, sConnStr);
                                        break;

                                    case DataRowState.Modified:     //16
                                        sWhereClause = strKeyFieldName + " = '" + DSProc.Tables[strTableName].Rows[ir][strKeyFieldName].ToString() + "'";
                                        bOPRStatus = bOPRStatus & myConn.GenUpdate(objDTTemp, sWhereClause, DBTran, sConnStr);
                                        break;

                                    case DataRowState.Deleted:      //8
                                        sWhereClause = strKeyFieldName + " = '" + DSProc.Tables[strTableName].Rows[ir][strKeyFieldName].ToString() + "'";
                                        bOPRStatus = bOPRStatus & myConn.GenDelete(strTableName, sWhereClause, DBTran, sConnStr);
                                        break;

                                    case DataRowState.Unchanged:    //2
                                        break;

                                    default:
                                        String k = String.Empty;
                                        break;
                                }

                                if (bOPRStatus == true)
                                {
                                    iRecAffected++;
                                    CTRLTBL.Rows[rowIndex]["RecAffected"] = iRecAffected;
                                }
                                else
                                {
                                    bOPRStatus = false;
                                    CTRLTBL.Rows[rowIndex]["UpdateStatus"] = bOPRStatus;
                                }
                            }
                            break;
                    }
                    CTRLTBL.Rows[rowIndex]["UpdateStatus"] = bOPRStatus;
                }
                #endregion

                DataSet dsResult = new DataSet();
                if (bOPRStatus == true)
                {
                    if (bUseTransaction)
                        DBTran.Commit();
                }
                else
                {
                    if (bUseTransaction)
                        DBTran.Rollback();
                }

                if (DSProc.Tables.Contains("dtSQL"))
                    dsResult = GetDataDS(DSProc.Tables["dtSQL"], sConnStr, null, -1);

                dsResult.Merge(CTRLTBL);
                return dsResult;
            }
            catch (Exception ex)
            {
                CTRLTBL.Rows[0]["ExceptionInfo"] = ex.Message;

                if (bUseTransaction)
                    DBTran.Rollback();
                DataSet dsResult = new DataSet();
                dsResult.Merge(CTRLTBL);
                return dsResult;
            }
            finally
            {
                sqlConn.Close();
                sqlConn.Dispose();
            }
        }

        public Boolean BulkIns(DataTable DTInsert, MySqlTransaction sqltran, String sConnStr)
        {
            Boolean insert = false;
            Boolean bAutoCommit = false;

            if (sqltran == null)
                bAutoCommit = true;

            MySqlConnection conn;
            if (bAutoCommit)
                conn = MySQLConn.GetConnection(sConnStr);
            else
                conn = sqltran.Connection;

            try
            {
                if (bAutoCommit)
                {
                    conn.Open();
                    //sqltran = conn.BeginTransaction();
                }

                //SqlBulkCopy bulk = new SqlBulkCopy(conn);
                //bulk.DestinationTableName = DTInsert.TableName;
                //bulk.BulkCopyTimeout = 4000;
                //bulk.WriteToServer(DTInsert);

                //if (bAutoCommit)
                //   sqltran.Commit();

                insert = true;
            }
            catch (Exception ex)
            {
                //if (bAutoCommit)
                //    sqltran.Rollback();

                insert = false;
            }
            finally
            {
                conn.Close();
            }
            return insert;
        }

        public Boolean GenUpdate(DataTable DT, String strWhereClause, String sConnStr)
        {
            return GenUpdate(DT, strWhereClause, (MySqlTransaction)null, sConnStr);
        }
        public Boolean GenUpdate(DataTable DT, String strWhereClause, DbTransaction dbTran, String sConnStr)
        {
            if (dbTran != null)
                return GenUpdate(DT, strWhereClause, (MySqlTransaction)dbTran, sConnStr);
            else
                return GenUpdate(DT, strWhereClause, (MySqlTransaction)null, sConnStr);
        }
        public Boolean GenUpdate(DataTable DT, String strWhereClause, MySqlTransaction transaction, String sConnStr)
        {
            bool updatef = false;
            Boolean bAutoCommit = false;
            if (transaction == null)
                bAutoCommit = true;

            MySqlConnection conn;
            if (bAutoCommit)
                conn = MySQLConn.GetConnection(sConnStr);
            else
                conn = transaction.Connection;

            String sqlString = "UPDATE " + DT.TableName + " set ";
            int iCount = 1;
            foreach (DataColumn dc in DT.Columns)
            {
                sqlString += dc.ColumnName + " = @param" + iCount.ToString() + ", ";
                iCount++;
            }
            sqlString = sqlString.Substring(0, sqlString.Length - 2) + " where " + strWhereClause;

            MySqlCommand SqlCmd = new MySqlCommand(sqlString, conn);
            try
            {
                if (bAutoCommit)
                {
                    conn.Open();
                    transaction = conn.BeginTransaction();
                }
                try
                {
                    iCount = 1;
                    foreach (DataRow dr in DT.Rows)
                        foreach (DataColumn dc in DT.Columns)
                        {
                            SqlCmd.Parameters.Add(new MySqlParameter("@param" + iCount.ToString(), (object)dr[dc.ColumnName]));
                            SqlCmd.Parameters["@param" + iCount.ToString()].Value = dr[dc.ColumnName];
                            iCount++;

                            strLog = strLog + dr[dc.ColumnName].ToString() + ",";
                        }

                    SqlCmd.Transaction = transaction;
                    SqlCmd.ExecuteNonQuery();
                    if (bAutoCommit)
                        transaction.Commit();

                    //GenAudit("UP", strWhereClause.Substring(strWhereClause.IndexOf("=") + 1), strWhereClause.Substring(strWhereClause.IndexOf("=") + 1), DT.TableName, "", SqlCmd.CommandText.ToString() + " Values [" + strLog + "]");
                    updatef = true;
                }
                catch (Exception ex)
                {
                    if (bAutoCommit)
                        transaction.Rollback();
                    updatef = false;
                    throw ex;
                    //GenAudit("EXUP", strWhereClause.Substring(strWhereClause.IndexOf("=") + 1), strWhereClause.Substring(strWhereClause.IndexOf("=") + 1), DT.TableName, "", SqlCmd.CommandText.ToString() + " Values [" + strLog + "] + exception " + ex.ToString());
                }
            }
            catch (Exception ex)
            {
                updatef = false;
                throw ex;
                //GenAudit("EXUP", strWhereClause.Substring(strWhereClause.IndexOf("=") + 1), strWhereClause.Substring(strWhereClause.IndexOf("=") + 1), DT.TableName, "", SqlCmd.CommandText.ToString() + " Values [" + strLog + "] + exception " + ex.ToString());
            }
            finally
            {
                if (bAutoCommit)
                    conn.Close();
            }
            return updatef;
        }

        public Boolean GenDelete(String strDelete, String strWhereClause, String sConnStr)
        {
            return GenDelete(strDelete, strWhereClause, (MySqlTransaction)null, sConnStr);
        }
        public Boolean GenDelete(String strDelete, String strWhereClause, DbTransaction dbTran, String sConnStr)
        {
            if (dbTran != null)
                return GenDelete(strDelete, strWhereClause, (MySqlTransaction)dbTran, sConnStr);
            else
                return GenDelete(strDelete, strWhereClause, (MySqlTransaction)null, sConnStr);
        }
        Boolean GenDelete(String strDelete, String strWhereClause, MySqlTransaction transaction, String sConnStr)
        {
            Boolean deletef = false;
            Boolean bAutoCommit = false;
            if (transaction == null)
                bAutoCommit = true;

            MySqlConnection conn;
            if (bAutoCommit)
                conn = MySQLConn.GetConnection(sConnStr);
            else
                conn = transaction.Connection;

            String sqlString = "DELETE FROM " + strDelete + "  ";
            sqlString += " where " + strWhereClause;

            MySqlCommand SqlCmd = new MySqlCommand(sqlString, conn);
            try
            {
                if (bAutoCommit)
                {
                    conn.Open();
                    transaction = conn.BeginTransaction();
                }
                try
                {
                    SqlCmd.Transaction = transaction;
                    SqlCmd.ExecuteNonQuery();
                    if (bAutoCommit)
                        transaction.Commit();

                    //GenAudit("DEL", strWhereClause.Substring(strWhereClause.IndexOf("=") + 1), strWhereClause.Substring(strWhereClause.IndexOf("=") + 1), strDelete, "", SqlCmd.CommandText.ToString() + " Values [" + strLog + "]");
                    deletef = true;
                }
                catch (Exception ex)
                {
                    if (bAutoCommit)
                        transaction.Rollback();
                    //GenAudit("EXDL", strWhereClause.Substring(strWhereClause.IndexOf("=") + 1), strWhereClause.Substring(strWhereClause.IndexOf("=") + 1), strDelete, "", SqlCmd.CommandText.ToString() + " Values [" + strLog + "] + exception " + ex.ToString());
                    deletef = false;
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                //GenAudit("EXDL", strWhereClause.Substring(strWhereClause.IndexOf("=") + 1), strWhereClause.Substring(strWhereClause.IndexOf("=") + 1), strDelete, "", SqlCmd.CommandText.ToString() + " Values [" + strLog + "] + exception " + ex.ToString());
                deletef = false;
                throw ex;
            }
            finally
            {
                if (bAutoCommit)
                    conn.Close();
            }

            return deletef;
        }

        public bool GenAudit(String strType,
                     String strRefCode,
                     String strRefDesc,
                     String strTable,
                     String strCallerFunc,
                     String strSQLremarks, String sConnStr)
        {
            if (strType == String.Empty)
                return false;
            if (strRefCode == String.Empty)
                return false;

            switch (strTable.ToUpper())
            {
                case "GFI_GPS_GPSDATA":
                    {
                        return true;
                        // break;
                    }
                case "GFI_GPS_GPSDATADETAIL":
                    {
                        return true;
                        // break;
                    }
                case "GFI_GPS_TRIPHEADER":
                    {
                        return true;
                        // break;
                    }
                case "GFI_GPS_TRIPDETAIL":
                    {
                        return true;
                        // break;
                    }
                case "CM_TH":
                    {
                        return true;
                        // break;
                    }
                case "CM_THEXISTING":
                    {
                        return true;
                        // break;
                    }
            }

            MySQLConn Conn = new MySQLConn();
            string sqlString = @"INSERT INTO SYS_Audit
                                        ( AuditTypes, ReferenceCode,ReferenceDesc,CreatedDate,AuditUser,ReferenceTable,CallerFunction,SQLRemarks
                                        )
                                 VALUES (?,?,?,?,?,?,?,?
                                        )";
            try
            {
                using (MySqlConnection conn = MySQLConn.GetConnection(sConnStr))
                {
                    conn.Open();
                    using (MySqlTransaction transaction = conn.BeginTransaction())
                    {
                        try
                        {
                            Boolean bFound = sqlString.Contains("?");
                            int iCount = 1;
                            while (bFound)
                            {
                                int i = sqlString.IndexOf("?");
                                sqlString = ReplaceFirst(sqlString, "?", "@param" + iCount.ToString());
                                bFound = sqlString.Contains("?");
                                iCount++;
                            }
                            if (strSQLremarks.Length > 1000)
                                strSQLremarks = strSQLremarks.Substring(0, 999);

                            using (MySqlCommand SqlCmd = new MySqlCommand(sqlString, conn))
                            {
                                SqlCmd.Parameters.Add(new MySqlParameter("@param1", (object)strType));
                                SqlCmd.Parameters["@param1"].Value = strType;
                                SqlCmd.Parameters.Add(new MySqlParameter("@param2", (object)strRefCode));
                                SqlCmd.Parameters["@param2"].Value = strRefCode;
                                SqlCmd.Parameters.Add(new MySqlParameter("@param3", (object)strRefDesc));
                                SqlCmd.Parameters["@param3"].Value = strRefDesc;
                                SqlCmd.Parameters.Add(new MySqlParameter("@param4", SqlDbType.DateTime));
                                SqlCmd.Parameters["@param4"].Value = System.DateTime.Now;
                                //SqlCmd.Parameters.Add(new SqlParameter("@param5", SqlDbType.VarChar));
                                //SqlCmd.Parameters["@param5"].Value = Globals.uLogin.Username.ToString();
                                SqlCmd.Parameters.Add(new MySqlParameter("@param6", (object)strTable));
                                SqlCmd.Parameters["@param6"].Value = strTable;
                                SqlCmd.Parameters.Add(new MySqlParameter("@param7", (object)strCallerFunc));
                                SqlCmd.Parameters["@param7"].Value = strCallerFunc;
                                SqlCmd.Parameters.Add(new MySqlParameter("@param8", (object)strSQLremarks));
                                SqlCmd.Parameters["@param8"].Value = strSQLremarks;


                                SqlCmd.Transaction = transaction;
                                SqlCmd.ExecuteNonQuery();
                            }
                            transaction.Commit();
                            return true;
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            //er.ShowExceptions(ex);
                            return false;
                        }
                        finally
                        {
                            conn.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //  er.ShowExceptions(ex);
                return false;
            }
        }

        public String BackupDB(String sConnStr)
        {
            try
            {
                String SqlBakPath = DBGlobals.BackupPath();
                String DatabaseName = GetDBName(sConnStr);
                DatabaseName = GetsConnStr(sConnStr);
                String strPath = SqlBakPath + DatabaseName + DateTime.Today.DayOfWeek.ToString() + ".bak";
                String sql = "Backup database " + DatabaseName
                    + " to disk = '" + strPath + "' with init";

                dbExecute(sql, sConnStr);
                return strPath;
            }
            catch //(Exception ex)
            {
                return String.Empty;
            }
        }
        public static String GetDBName(String sConnStr)
        {
            String str, ServerName, UserID, Password, strDBName = String.Empty;
            str = GetsConnStr(sConnStr);

            char[] chSep = { ';' };
            String[] strIndivParam = str.Split(chSep);

            for (int i = 0; i < strIndivParam.Length; i++)
            {
                int intFound = -1;
                if ((intFound = strIndivParam[i].IndexOf("SERVER=")) != -1)
                    ServerName = strIndivParam[i].Substring(intFound + "SERVER=".Length, strIndivParam[i].Length - "SERVER=".Length);

                if ((intFound = strIndivParam[i].IndexOf("UID=")) != -1)
                    UserID = strIndivParam[i].Substring(intFound + "UID=".Length, strIndivParam[i].Length - "UID=".Length);

                if ((intFound = strIndivParam[i].IndexOf("PASSWORD=")) != -1)
                    Password = strIndivParam[i].Substring(intFound + "PASSWORD=".Length, strIndivParam[i].Length - "PASSWORD=".Length);

                if ((intFound = strIndivParam[i].IndexOf("DATABASE=")) != -1)
                    strDBName = strIndivParam[i].Substring(intFound + "DATABASE=".Length, strIndivParam[i].Length - "DATABASE=".Length);
            }

            return strDBName;
        }

        public static String GetsConnStr(String sConnStr)
        {
            String str = String.Empty;
            switch (sConnStr)
            {
                case "ConnStr":
                    str = ConfigurationManager.AppSettings[sConnStr].ToString();
                    break;

                case "ConnStrTransferred":
                    str = ConfigurationManager.AppSettings["ConnStr"].ToString();
                    String ServerName, UserID, Password, strDBName = String.Empty;

                    char[] chSep = { ';' };
                    String[] strIndivParam = str.Split(chSep);

                    for (int i = 0; i < strIndivParam.Length; i++)
                    {
                        int intFound = -1;
                        if ((intFound = strIndivParam[i].IndexOf("SERVER=")) != -1)
                            ServerName = strIndivParam[i].Substring(intFound + "SERVER=".Length, strIndivParam[i].Length - "SERVER=".Length);

                        if ((intFound = strIndivParam[i].IndexOf("UID=")) != -1)
                            UserID = strIndivParam[i].Substring(intFound + "UID=".Length, strIndivParam[i].Length - "UID=".Length);

                        if ((intFound = strIndivParam[i].IndexOf("PASSWORD=")) != -1)
                            Password = strIndivParam[i].Substring(intFound + "PASSWORD=".Length, strIndivParam[i].Length - "PASSWORD=".Length);

                        if ((intFound = strIndivParam[i].IndexOf("DATABASE=")) != -1)
                            strDBName = strIndivParam[i].Substring(intFound + "DATABASE=".Length, strIndivParam[i].Length - "DATABASE=".Length);
                    }

                    str = str.Replace(strDBName, strDBName + "Transferred");
                    break;

                case "DB1":
                    //Demo
                    str = ConfigurationManager.AppSettings["CoreConnStr"].ToString().Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB2":
                    //Mercury
                    str = ConfigurationManager.AppSettings["CoreConnStrDB2"].ToString().Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB3":
                    //Venus
                    str = ConfigurationManager.AppSettings["CoreConnStrDB3"].ToString().Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB4":
                    //Earth
                    str = ConfigurationManager.AppSettings["CoreConnStrDB4"].ToString().Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB5":
                    //Mars
                    str = ConfigurationManager.AppSettings["CoreConnStrDB5"].ToString().Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB6":
                    //Jupiter
                    str = ConfigurationManager.AppSettings["CoreConnStrDB6"].ToString().Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB7":
                    //Saturn
                    str = ConfigurationManager.AppSettings["CoreConnStrDB7"].ToString().Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB8":
                    //Uranus
                    str = ConfigurationManager.AppSettings["CoreConnStrDB8"].ToString().Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB9":
                    //Neptune
                    str = ConfigurationManager.AppSettings["CoreConnStrDB9"].ToString().Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB10":
                    //Pluto
                    str = ConfigurationManager.AppSettings["CoreConnStrDB10"].ToString().Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB11":
                    //Baby
                    str = ConfigurationManager.AppSettings["CoreConnStrDB11"].ToString().Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB12":
                    //Kids
                    str = ConfigurationManager.AppSettings["CoreConnStrDB12"].ToString().Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB13":
                    //
                    str = ConfigurationManager.AppSettings["CoreConnStrDB13"].ToString().Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB14":
                    //
                    str = ConfigurationManager.AppSettings["CoreConnStrDB14"].ToString().Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB15":
                    //
                    str = ConfigurationManager.AppSettings["CoreConnStrDB15"].ToString().Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB99":
                    //RezaHP , LocalHost
                    str = ConfigurationManager.AppSettings["CoreConnStrDB99"].ToString().Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                default:
                    str = ConfigurationManager.AppSettings[sConnStr].ToString().Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;
            }
            return str;
        }

        public static MySqlConnection GetConnection(String sConnStr)
        {
            MySqlConnection myConnection = null;

            if (!DBGlobals.ValidateLicence())
                return myConnection;

            try
            {
                myConnection = new MySqlConnection(GetsConnStr(sConnStr));
            }
            catch (Exception ex)
            {
                //MessageBox.Show(" Network/Communication Error ","NaveoOne");
                throw new Exception(" " + ex.Message);
            }

            return myConnection;
        }

        #region Migration
        public static Boolean ChkSQLConn(String sConnStr)
        {
            MySqlConnection myConnection = null;
            try
            {
                myConnection = new MySqlConnection(sConnStr);
                myConnection.Open();
                myConnection.Close();
                return true;
            }
            catch
            {
                //MessageBox.Show(" Network/Communication Error ", "NaveoOne");
                return false;
            }
        }
        #endregion

        void NoNull(String tableName, String sConnStr)
        {
            // Locate Primary Keys
            String sql = "sp_pkeys " + tableName;
            MySqlDataAdapter Primary_da = new MySqlDataAdapter();
            DataSet Primary_ds = new DataSet();

            MySqlDataAdapter myDataAdapter = new MySqlDataAdapter(sql, GetConnection("Core"));
            myDataAdapter.Fill(Primary_ds);

            DataRow[] Primary_drx = Primary_ds.Tables[0].Select("1 = 1");
            string[] PrimaryKey = new string[Primary_drx.Length];
            {
                int i = 0;
                foreach (DataRow dr in Primary_drx)
                {
                    PrimaryKey[i] = dr["COLUMN_NAME"].ToString();
                    i++;
                }
            }
            // Locate Primary Keys ends

            // Locate none nullable fields
            sql = "sp_columns " + tableName;
            MySqlDataAdapter nonull_da = new MySqlDataAdapter();
            DataSet nonull_ds = new DataSet();

            myDataAdapter = new MySqlDataAdapter(sql, GetConnection(sConnStr));
            myDataAdapter.Fill(nonull_ds);

            DataRow[] nonull_drx = nonull_ds.Tables[0].Select("nullable = false");
            string[] nonull = new string[nonull_drx.Length];
            {
                int i = 0;
                foreach (DataRow dr in nonull_drx)
                {
                    nonull[i] = dr["COLUMN_NAME"].ToString();
                    i++;
                }
            }
            // Locate none nullable fields end

            // Locate identity table
            DataRow[] Identity_drx = nonull_ds.Tables[0].Select("1 = 1");
            int identity = 0;
            foreach (DataRow dr in Identity_drx)
                if (dr["TYPE_NAME"].ToString().Contains("identity") == true)
                    identity = 1;
            // Locate identity table ends

            // Locate Date Types
            DataRow[] date_drx = nonull_ds.Tables[0].Select("TYPE_NAME = 'datetime'");
            string[] DateF = new string[date_drx.Length];
            {
                int i = 0;
                foreach (DataRow dr in date_drx)
                {
                    DateF[i] = dr["COLUMN_NAME"].ToString();
                    i++;
                }
            }
            // Locate Date Types ends
        }

        #region DB create
        public static Boolean CreateDatabaseIfNotExist(String sConnStr)
        {
            try
            {
                //USE master
                //IF(SELECT count(1) FROM sys.databases WHERE name = 'NewDatabase') = 0
                //  BEGIN
                //    CREATE DATABASE NewDatabase;
                //  END

                MySqlConnection connection = GetConnection(sConnStr);
                String strDB = GetDBName(sConnStr);
                //String appPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                //GrantAccess(appPath); //Need to assign the permission for current application to allow create database on server (if you are in domain).
                Boolean IsExits = CheckDatabaseExists(connection, strDB); //Check database exists in sql server.
                if (!IsExits)
                {
                    String sql = "CREATE DATABASE IF NOT EXISTS " + strDB;
                    String s = connection.ConnectionString.Replace("database=" + strDB, "database=mysql");
                    connection.ConnectionString = s;
                    MySqlCommand command = new MySqlCommand(sql, connection);
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
                    finally
                    {
                        if (connection.State == ConnectionState.Open)
                        {
                            connection.Close();
                        }
                    }

                    System.Threading.Thread.Sleep(15000);
                    return true;
                }
                return true;
            }
            catch { return false; }
        }

        static Boolean GrantAccess(String fullPath)
        {
            System.IO.DirectoryInfo info = new System.IO.DirectoryInfo(fullPath);
            System.Security.Principal.WindowsIdentity self = System.Security.Principal.WindowsIdentity.GetCurrent();
            System.Security.AccessControl.DirectorySecurity ds = info.GetAccessControl();
            ds.AddAccessRule(new System.Security.AccessControl.FileSystemAccessRule(
                self.Name
                , System.Security.AccessControl.FileSystemRights.FullControl
                , System.Security.AccessControl.InheritanceFlags.ObjectInherit | System.Security.AccessControl.InheritanceFlags.ContainerInherit
                , System.Security.AccessControl.PropagationFlags.None
                , System.Security.AccessControl.AccessControlType.Allow));
            info.SetAccessControl(ds);
            return true;
        }

        static Boolean CheckDatabaseExists(MySqlConnection tmpConn, String databaseName)
        {
            String sqlCreateDBQuery;
            Boolean result = false;

            try
            {
                sqlCreateDBQuery = string.Format("SHOW DATABASES LIKE '{0}'", databaseName);
                using (MySqlCommand sqlCmd = new MySqlCommand(sqlCreateDBQuery, tmpConn))
                {
                    tmpConn.Open();
                    object resultObj = sqlCmd.ExecuteScalar();
                    tmpConn.Close();
                    result = (resultObj.ToString().Length > 0);
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }
        #endregion
    }
}