﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Windows.Forms;
using System.Data;
using System.Data.Common;

using CORE.Common;



namespace CORE.DbCon
{
    public class OleDBConn
    {

        #region Variables

        public static string GblConnection = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

        Errors er = new Errors();
        // Boolean Vallic = Globals.ValidateLicence();

        public static String strLog = "";

        #endregion

        public static OleDbConnection GetConnection()
        {
            OleDbConnection myConnection = null;

            if (!Globals.ValidateLicence())
            {
                return myConnection;
            }

            try
            {
                myConnection = new OleDbConnection(GblConnection);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw new Exception(" " + ex.Message);
            }

            return myConnection;
        }

        private void OpenConnection(OleDbConnection myConnection)
        {
            myConnection.Open();
        }

        public OleDbDataReader GetData(String mySQL)
        {
            OleDbDataReader myDataReader;

            try
            {

                OleDbCommand sqlCommand = new OleDbCommand();

                sqlCommand.CommandType = CommandType.Text;
                sqlCommand.CommandText = mySQL;
                sqlCommand.Connection = GetConnection();
                sqlCommand.Connection.Open();

                myDataReader = sqlCommand.ExecuteReader();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw new Exception(" " + ex.Message);
            }

            return myDataReader;

        }

        public int GetNextID(String strTableName, DbTransaction transaction)
        {

            Boolean bAutoCommit = false;
            if (transaction == null)
                bAutoCommit = true;

            OleDbConnection conn;
            OleDbTransaction transs = (OleDbTransaction)transaction;
            if (bAutoCommit)
                conn = Connection.GetConnection();
            else
                conn = transs.Connection;



            String sqlString = "SELECT IDENT_CURRENT('" + strTableName + "') + 1  AS CN";


            strLog = String.Empty;

            OleDbCommand SqlCmd = new OleDbCommand(sqlString, conn);
            try
            {
                if (bAutoCommit)
                {
                    conn.Open();
                    transaction = conn.BeginTransaction();
                }

                DataTable lDt = GetDataDT(sqlString);

                //Dr.Read();
                if (lDt.Rows[0]["CN"].ToString() == "1")
                    return 1;
                else
                    return Convert.ToInt32(lDt.Rows[0]["CN"].ToString());


            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (bAutoCommit)
                    conn.Close();
            }



            return 0;
        }

        public DataSet GetDataDS(String mySQL)
        {
            DataSet myDataSet = new DataSet();
            try
            {
                OleDbDataAdapter myDataAdapter = new OleDbDataAdapter(mySQL, GetConnection());
                myDataAdapter.Fill(myDataSet);
            }
            catch
            { }
            return myDataSet;
        }
        public DataTable GetDataDT(String mySQL, String strParams, DbTransaction transaction)
        {
            return GetDataDT(mySQL, strParams, (OleDbTransaction)transaction);
        }
        public DataTable GetDataDT(String mySQL)
        {
            return GetDataDT(mySQL, String.Empty, null);
        }
        public DataTable GetDataDT(String mySQL, DbTransaction transaction)
        {
            return GetDataDT(mySQL, String.Empty, (OleDbTransaction)transaction);
        }
        DataTable GetDataDT(String mySQL, String strParam, OleDbTransaction transaction)
        {
            Boolean bAutoCommit = false;
            if (transaction == null)
                bAutoCommit = true;

            OleDbConnection conn;
            if (bAutoCommit)
                conn = GetConnection();
            else
                conn = transaction.Connection;

            DataTable myDataTable = new DataTable();
            try
            {
                OleDbCommand cmd = new OleDbCommand();
                cmd.CommandText = mySQL;
                foreach (String s in strParam.Split('¬'))
                    if (s.Trim().Length > 0)
                        cmd.Parameters.Add(new OleDbParameter { Value = s });
                cmd.Connection = conn;
                cmd.Transaction = transaction;
                OleDbDataAdapter myDataAdapter = new OleDbDataAdapter(cmd);
                myDataAdapter.Fill(myDataTable);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                if (bAutoCommit)
                    conn.Close();
            }
            return myDataTable;
        }

        public int dbExecute(String strSQl)
        {
            int iReturn = 0;

            OleDbCommand _sqlTrans = new OleDbCommand();
            OleDbConnection conn = GetConnection();
            try
            {
                conn.Open();
                _sqlTrans.CommandText = strSQl;
                _sqlTrans.Connection = conn;
                iReturn = _sqlTrans.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                conn.Close();
            }
            return iReturn;
        }

        public int GetNumberOfRecords(String pTable, String pColumn, String pWhere)
        {
            int count = -1;
            String strSql;

            OleDbCommand _sqlTrans = new OleDbCommand();
            OleDbConnection conn = GetConnection();
            try
            {
                // Open the connection
                conn.Open();

                strSql = "select count(" + pColumn + ") from " + pTable;
                if (pWhere != null || pWhere != "")
                    strSql += " Where " + pWhere;

                _sqlTrans.CommandText = strSql;
                _sqlTrans.Connection = conn;
                count = (int)_sqlTrans.ExecuteScalar();
            }
            catch { }
            finally
            {
                // Close the connection
                conn.Close();
            }
            return count;
        }
        public DataTable GetDataTableBy(String mySQL, String id)
        {
            DataTable dt = new DataTable();
            using (OleDbConnection conn = Connection.GetConnection())
            {
                try
                {
                    using (OleDbCommand orcCmd = new OleDbCommand(mySQL, conn))
                    {
                        orcCmd.Parameters.Add(new OleDbParameter { Value = id });
                        using (OleDbDataAdapter OrcAdapter = new OleDbDataAdapter(orcCmd))
                        {
                            OrcAdapter.Fill(dt);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    throw new Exception(" " + ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
            return dt;
        }

        public bool GenInsertObsolete(DataTable DTInsert, OleDbTransaction transaction)
        {
            bool insert = false;
            Boolean bAutoCommit = false;
            if (transaction == null)
                bAutoCommit = true;

            OleDbConnection conn;
            if (bAutoCommit)
                conn = Connection.GetConnection();
            else
                conn = transaction.Connection;

            String sqlString = "INSERT INTO " + DTInsert.TableName;
            sqlString += "(";
            foreach (DataColumn dc in DTInsert.Columns)
                sqlString += dc.ColumnName + ", ";
            sqlString = sqlString.Substring(0, sqlString.Length - 2) + " ) values (";
            foreach (DataColumn dc in DTInsert.Columns)
                sqlString += "?, ";
            sqlString = sqlString.Substring(0, sqlString.Length - 2) + " )";
            strLog = String.Empty;

            OleDbCommand SqlCmd = new OleDbCommand(sqlString, conn);
            try
            {
                if (bAutoCommit)
                {
                    conn.Open();
                    transaction = conn.BeginTransaction();
                }
                foreach (DataRow dr in DTInsert.Rows)
                    foreach (DataColumn dc in DTInsert.Columns)
                    {
                        SqlCmd.Parameters.Add(new OleDbParameter { Value = dr[dc.ColumnName] });
                        strLog = strLog + dr[dc.ColumnName].ToString() + ",";
                    }

                SqlCmd.Transaction = transaction;
                SqlCmd.ExecuteNonQuery();

                if (bAutoCommit)
                    transaction.Commit();

                GenAudit("IN", "INS_OK", "", "", "", SqlCmd.CommandText.ToString() + " Values [" + strLog + "]");
                insert = true;
            }
            catch (Exception ex)
            {
                //er.ShowExceptions(ex);
                GenAudit("EXIN", "INS_ERR", "", "", "", SqlCmd.CommandText.ToString() + " Values [" + strLog + "]");
                insert = false;
            }
            finally
            {
                if (bAutoCommit)
                    conn.Close();
            }
            return insert;
        }
        public bool GenInsertObsolete(DataTable DTInsert)
        {
            return GenInsert(DTInsert, null);
        }

        public Boolean GenInsert(DataTable DTInsert)
        {
            return GenInsert(DTInsert, (OleDbTransaction)null);
        }
        public Boolean GenInsert(DataTable DTInsert, DbTransaction dbTran)
        {
            if (dbTran != null)
                return GenInsert(DTInsert, (OleDbTransaction)dbTran);
            else
                return GenInsert(DTInsert, (OleDbTransaction)null);
        }
        Boolean GenInsert(DataTable DTInsert, OleDbTransaction transaction)
        {
            strLog = String.Empty;
            bool insert = false;
            Boolean bAutoCommit = false;

            if (transaction == null)
                bAutoCommit = true;

            OleDbConnection conn;
            if (bAutoCommit)
                conn = Connection.GetConnection();
            else
                conn = transaction.Connection;

            String sqlString = "INSERT INTO " + DTInsert.TableName;
            sqlString += "(";
            foreach (DataColumn dc in DTInsert.Columns)
                sqlString += dc.ColumnName + ", ";
            sqlString = sqlString.Substring(0, sqlString.Length - 2) + " ) values (";
            foreach (DataColumn dc in DTInsert.Columns)
                sqlString += "?, ";
            sqlString = sqlString.Substring(0, sqlString.Length - 2) + " )";

            OleDbCommand SqlCmd = new OleDbCommand(sqlString, conn);
            try
            {
                if (bAutoCommit)
                {
                    conn.Open();
                    transaction = conn.BeginTransaction();
                }
                try
                {
                    foreach (DataRow dr in DTInsert.Rows)
                        foreach (DataColumn dc in DTInsert.Columns)
                        {
                            SqlCmd.Parameters.Add(new OleDbParameter { Value = dr[dc.ColumnName] });
                            strLog = strLog + dr[dc.ColumnName].ToString() + ",";
                        }

                    SqlCmd.Transaction = transaction;
                    SqlCmd.ExecuteNonQuery();

                    if (bAutoCommit)
                        transaction.Commit();

                    GenAudit("IN", "INSERT", "", "", "", SqlCmd.CommandText.ToString() + " Values [" + strLog + "]");
                    insert = true;
                }
                catch (Exception ex)
                {
                    if (bAutoCommit)
                        transaction.Rollback();

                    GenAudit("EXIN", "INSERT", "", "", "", SqlCmd.CommandText.ToString() + " Values [" + strLog + "] + exception " + ex.ToString());
                    insert = false;
                }
            }
            catch (Exception ex)
            {
                GenAudit("EXIN", "INSERT", "", "", "", SqlCmd.CommandText.ToString() + " Values [" + strLog + "] + exception " + ex.ToString());
                insert = false;
            }
            finally
            {
                if (bAutoCommit)
                    conn.Close();
            }
            return insert;
        }

        public Boolean BulkIns(DataTable DTInsert)
        {
            bool insert = false;

            DbConnection conn = Connection.GetConnection();

            String ServerName, UserID=String.Empty, Password=String.Empty, strDBName = String.Empty;

            char[] chSep = { ';' };
            String[] strIndivParam = conn.ConnectionString.Split(chSep);

            for (int i = 0; i < strIndivParam.Length; i++)
            {
                int intFound = -1;
                if ((intFound = strIndivParam[i].IndexOf("Data Source=")) != -1)
                    ServerName = strIndivParam[i].Substring(intFound + "Data Source=".Length, strIndivParam[i].Length - "Data Source=".Length);

                if ((intFound = strIndivParam[i].IndexOf("User ID=")) != -1)
                    UserID = strIndivParam[i].Substring(intFound + "User ID=".Length, strIndivParam[i].Length - "User ID=".Length);

                if ((intFound = strIndivParam[i].IndexOf("Password=")) != -1)
                    Password = strIndivParam[i].Substring(intFound + "Pwd=".Length, strIndivParam[i].Length - "Pwd=".Length);

                if ((intFound = strIndivParam[i].IndexOf("Initial Catalog=")) != -1)
                    strDBName = strIndivParam[i].Substring(intFound + "Initial Catalog=".Length, strIndivParam[i].Length - "Initial Catalog=".Length);
            }

            String strConn = "Data Source=" + conn.DataSource + ";Initial Catalog=" + conn.Database + ";User ID=" + UserID + ";Pwd=" + Password + ";Connect Timeout=200;";
            SqlConnection sqlconn = new SqlConnection(strConn);
            sqlconn.Open();

            
            try
            {
                SqlBulkCopy bulk = new SqlBulkCopy(sqlconn);
                bulk.DestinationTableName = DTInsert.TableName;
                bulk.BulkCopyTimeout = 4000;
                bulk.WriteToServer(DTInsert);
                insert = true;
            }
            catch (Exception ex)
            {
            }
            finally
            {
                    conn.Close();
            }
            return insert;
        }

        public Boolean GenUpdate(DataTable DT, String strWhereClause)
        {
            return GenUpdate(DT, strWhereClause, (OleDbTransaction)null);
        }
        public Boolean GenUpdate(DataTable DT, String strWhereClause, DbTransaction dbTran)
        {
            if (dbTran != null)
                return GenUpdate(DT, strWhereClause, (OleDbTransaction)dbTran);
            else
                return GenUpdate(DT, strWhereClause, (OleDbTransaction)null);
        }
        Boolean GenUpdate(DataTable DT, String strWhereClause, OleDbTransaction transaction)
        {
            bool updatef = false;
            Boolean bAutoCommit = false;
            if (transaction == null)
                bAutoCommit = true;

            OleDbConnection conn;
            if (bAutoCommit)
                conn = Connection.GetConnection();
            else
                conn = transaction.Connection;

            String sqlString = "UPDATE " + DT.TableName + " set ";
            foreach (DataColumn dc in DT.Columns)
                sqlString += dc.ColumnName + " = ?, ";
            sqlString = sqlString.Substring(0, sqlString.Length - 2) + " where " + strWhereClause;

            OleDbCommand SqlCmd = new OleDbCommand(sqlString, conn);
            try
            {
                if (bAutoCommit)
                {
                    conn.Open();
                    transaction = conn.BeginTransaction();
                }
                try
                {
                    foreach (DataRow dr in DT.Rows)
                        foreach (DataColumn dc in DT.Columns)
                        {
                            SqlCmd.Parameters.Add(new OleDbParameter { Value = dr[dc.ColumnName] });
                            strLog = strLog + dr[dc.ColumnName].ToString() + ",";
                        }

                    SqlCmd.Transaction = transaction;
                    SqlCmd.ExecuteNonQuery();
                    if (bAutoCommit)
                        transaction.Commit();

                    GenAudit("UP", strWhereClause.Substring(strWhereClause.IndexOf("=") + 1), strWhereClause.Substring(strWhereClause.IndexOf("=") + 1), DT.TableName, "", SqlCmd.CommandText.ToString() + " Values [" + strLog + "]");
                    updatef = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    GenAudit("EXUP", strWhereClause.Substring(strWhereClause.IndexOf("=") + 1), strWhereClause.Substring(strWhereClause.IndexOf("=") + 1), DT.TableName, "", SqlCmd.CommandText.ToString() + " Values [" + strLog + "] + exception " + ex.ToString());
                    updatef = false;
                    //er.ShowExceptions(ex);
                }
            }
            catch (Exception ex)
            {
                //er.ShowExceptions(ex);

                GenAudit("EXUP", strWhereClause.Substring(strWhereClause.IndexOf("=") + 1), strWhereClause.Substring(strWhereClause.IndexOf("=") + 1), DT.TableName, "", SqlCmd.CommandText.ToString() + " Values [" + strLog + "] + exception " + ex.ToString());
                updatef = false;
            }
            finally
            {
                if (bAutoCommit)
                    conn.Close();
            }
            return updatef;
        }
        public Boolean GenUpdate(DataSet dsUpdate)
        {
            //dsUpdate should contain dtWhereClause
            Boolean bResult = false;
            OleDbConnection conn = null;
            OleDbTransaction transaction = null;
            try
            {
                conn = Connection.GetConnection();
                conn.Open();
                transaction = conn.BeginTransaction();

                Boolean bBatchCompleted = false;
                foreach (DataTable dt in dsUpdate.Tables)
                {
                    if (dt.TableName == "dtWhereClause")
                        continue;
                    switch (dt.Namespace)   //Each Name Space will insert according to its logic
                    {

                    }
                    if (!bResult)
                        break;

                    if (bBatchCompleted)
                        break;
                }

                if (bResult)
                    transaction.Commit();
                else
                    transaction.Rollback();
            }
            catch (OleDbException ex)
            {
                transaction.Rollback();
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
            return bResult;
        }

        Boolean GenDelete(String strDelete, String strWhereClause, OleDbTransaction transaction)
        {
            Boolean deletef = false;
            Boolean bAutoCommit = false;
            if (transaction == null)
                bAutoCommit = true;

            OleDbConnection conn;
            if (bAutoCommit)
                conn = Connection.GetConnection();
            else
                conn = transaction.Connection;

            String sqlString = "DELETE FROM " + strDelete + "  ";
            sqlString += " where " + strWhereClause;

            OleDbCommand SqlCmd = new OleDbCommand(sqlString, conn);
            try
            {
                if (bAutoCommit)
                {
                    conn.Open();
                    transaction = conn.BeginTransaction();
                }
                try
                {
                    SqlCmd.Transaction = transaction;
                    SqlCmd.ExecuteNonQuery();
                    if (bAutoCommit)
                        transaction.Commit();

                    GenAudit("DEL", strWhereClause.Substring(strWhereClause.IndexOf("=") + 1), strWhereClause.Substring(strWhereClause.IndexOf("=") + 1), strDelete, "", SqlCmd.CommandText.ToString() + " Values [" + strLog + "]");
                    deletef = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    GenAudit("EXDL", strWhereClause.Substring(strWhereClause.IndexOf("=") + 1), strWhereClause.Substring(strWhereClause.IndexOf("=") + 1), strDelete, "", SqlCmd.CommandText.ToString() + " Values [" + strLog + "] + exception " + ex.ToString());
                    deletef = false;
                    //er.ShowExceptions(ex);
                }
            }
            catch (Exception ex)
            {
                //er.ShowExceptions(ex);

                GenAudit("EXDL", strWhereClause.Substring(strWhereClause.IndexOf("=") + 1), strWhereClause.Substring(strWhereClause.IndexOf("=") + 1), strDelete, "", SqlCmd.CommandText.ToString() + " Values [" + strLog + "] + exception " + ex.ToString());
                deletef = false;
            }
            finally
            {
                if (bAutoCommit)
                    conn.Close();
            }

            return deletef;
        }
        public Boolean GenDelete(String strDelete, String strWhereClause)
        {
            return GenDelete(strDelete, strWhereClause, (OleDbTransaction)null);
        }
        public Boolean GenDelete(String strDelete, String strWhereClause, DbTransaction dbTran)
        {
            if (dbTran != null)
                return GenDelete(strDelete, strWhereClause, (OleDbTransaction)dbTran);
            else
                return GenDelete(strDelete, strWhereClause, (OleDbTransaction)null);
        }

        public bool GenAudit(String strType,
                     String strRefCode,
                     String strRefDesc,
                     String strTable,
                     String strCallerFunc,
                     String strSQLremarks)
        {
            if (strType == String.Empty)
                return false;
            if (strRefCode == String.Empty)
                return false;
            
            switch (strTable.ToUpper())
            {
                case "GFI_GPS_GPSDATA":
                    return true;
                    break;
                case "GFI_GPS_GPSDATADETAIL":
                    return true;
                    break;
                case "GFI_GPS_TRIPHEADER":
                    return true;
                    break;
                case "GFI_GPS_TRIPDETAIL":
                    return true;
                    break;
                case "CM_TH":
                    return true;
                    break;
                case "CM_THEXISTING":
                    return true;
                    break;
            }

            Connection Conn = new Connection();
            string sqlString = @"INSERT INTO SYS_Audit
                                        ( AuditTypes, ReferenceCode,ReferenceDesc,CreatedDate,AuditUser,ReferenceTable,CallerFunction,SQLRemarks
                                        )
                                 VALUES (?,?,?,?,?,?,?,?
                                        )";
            try
            {
                using (OleDbConnection conn = Connection.GetConnection())
                {
                    conn.Open();
                    using (OleDbTransaction transaction = conn.BeginTransaction())
                    {
                        try
                        {
                            using (OleDbCommand SqlCmd = new OleDbCommand(sqlString, conn))
                            {
                                SqlCmd.Parameters.Add(new OleDbParameter { Value = strType });
                                SqlCmd.Parameters.Add(new OleDbParameter { Value = strRefCode });
                                SqlCmd.Parameters.Add(new OleDbParameter { Value = strRefDesc });
                                SqlCmd.Parameters.Add(new OleDbParameter { Value = System.DateTime.Now });
                                SqlCmd.Parameters.Add(new OleDbParameter { Value = Globals.uLogin.Username.ToString() });
                                SqlCmd.Parameters.Add(new OleDbParameter { Value = strTable });
                                SqlCmd.Parameters.Add(new OleDbParameter { Value = strCallerFunc });
                                SqlCmd.Parameters.Add(new OleDbParameter { Value = strSQLremarks });



                                SqlCmd.Transaction = transaction;
                                SqlCmd.ExecuteNonQuery();
                            }
                            transaction.Commit();
                            return true;
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            //er.ShowExceptions(ex);
                            return false;
                        }
                        finally
                        {
                            conn.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //  er.ShowExceptions(ex);
                return false;
            }
        }

        public String BackupDB()
        {
            try
            {
                String SqlBakPath = Globals.BackupPath();
                String DatabaseName = GetDBName();
                String strPath = SqlBakPath + DatabaseName + DateTime.Today.DayOfWeek.ToString() + ".bak";
                String sql = "Backup database " + DatabaseName
                    + " to disk = '" + strPath + "' with init";

                dbExecute(sql);
                return strPath;
            }
            catch //(Exception ex)
            {
                return String.Empty;
            }
        }
        public static String GetDBName()
        {
            String str, ServerName, UserID, Password, strDBName = String.Empty;
            str = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

            char[] chSep = { ';' };
            String[] strIndivParam = str.Split(chSep);

            for (int i = 0; i < strIndivParam.Length; i++)
            {
                int intFound = -1;
                if ((intFound = strIndivParam[i].IndexOf("Data Source=")) != -1)
                    ServerName = strIndivParam[i].Substring(intFound + "Data Source=".Length, strIndivParam[i].Length - "Data Source=".Length);

                if ((intFound = strIndivParam[i].IndexOf("User ID=")) != -1)
                    UserID = strIndivParam[i].Substring(intFound + "User ID=".Length, strIndivParam[i].Length - "User ID=".Length);

                if ((intFound = strIndivParam[i].IndexOf("Pwd=")) != -1)
                    Password = strIndivParam[i].Substring(intFound + "Pwd=".Length, strIndivParam[i].Length - "Pwd=".Length);

                if ((intFound = strIndivParam[i].IndexOf("Initial Catalog=")) != -1)
                    strDBName = strIndivParam[i].Substring(intFound + "Initial Catalog=".Length, strIndivParam[i].Length - "Initial Catalog=".Length);
            }

            return strDBName;
        }
    }
}