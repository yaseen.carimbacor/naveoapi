﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DBConn.Common
{
    class SqlToOracle
    {
        public static String FormatOracleSQL(String sql)
        {
            if (sql.EndsWith("--AlreadyOracleStatement"))
                return sql;

            if (sql.StartsWith("\r\n--Trips Processor"))
                return sqlStartsWith(sql, "\r\n--Trips Processor");

            if (sql.StartsWith("\r\n--GetAssetsStatus\r\n"))
                return GetAssetsStatus(sql);

            if (sql.StartsWith("\r\n--RetrieveLiveData"))
            {
                sql = sql.Replace("select cte.*, '' AssetStatus, '' Comment into #GFI_GPS_LiveData ", "insert into #GFI_GPS_LiveData (UID , AssetID, Asset, DriverID, DateTimeGPS_UTC, dtLocalTime, DateTimeServer, Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, StopFlag, TripDistance, TripTime, Row_Num)");
            }
            sql = sql.ReplaceIgnoreCase("GetDate()", "systimestamp");
            sql = sql.ReplaceIgnoreCase("GetUTCDate()", "systimestamp");
            sql = sql.ReplaceIgnoreCase("ON UPDATE NO ACTION", "");
            sql = sql.ReplaceIgnoreCase("ON DELETE NO ACTION", "");
            sql = sql.ReplaceIgnoreCase("insert into ", "insert ");
            sql = sql.ReplaceIgnoreCase("insert ", "insert into ");
            sql = sql.ReplaceIgnoreCase("BEFORE insert into ON ", "BEFORE INSERT ON ");
            sql = sql.ReplaceIgnoreCase("NONCLUSTERED ", " ");
            sql = sql.Replace(" DATEADD(dd, -5, DATEDIFF(dd, 0, systimestamp))", " DATEADD(dd, -5, systimestamp)");

            sql = sql.ReplaceIgnoreCase("RowNum", "Row_Num");
            sql = sql.ReplaceIgnoreCase("RoowNum", "RowNum");

            sql = sql.ReplaceIgnoreCase("SET ANSI_WARNINGS OFF", " ");
            sql = sql.ReplaceIgnoreCase("SET ANSI_WARNINGS ON", " ");

            sql = sql.Replace("= ?", " COLLATE BINARY_CI = ?");
            if (!sql.StartsWith("--DoNotReplace[]"))
            {
                sql = sql.Replace("[", String.Empty);
                sql = sql.Replace("]", String.Empty);
            }

            sql = sql.Replace(@"""UID""", "UID");
            sql = Regex.Replace(sql, @"\bUID\b", @"""UID""", RegexOptions.IgnoreCase);
            sql = Regex.Replace(sql, @"\bCOMMENT\b", @"""COMMENT""", RegexOptions.IgnoreCase);
            sql = Regex.Replace(sql, @"COLLATE BINARY_CI", String.Empty, RegexOptions.IgnoreCase);
            sql = Regex.Replace(sql, @"COLLATE DATABASE_DEFAULT", String.Empty, RegexOptions.IgnoreCase);
            sql = Regex.Replace(sql, @"\bdbo.\b", String.Empty, RegexOptions.IgnoreCase);
            sql = Regex.Replace(sql, @"GetDate()", "systimestamp", RegexOptions.IgnoreCase);
            sql = Regex.Replace(sql, @"GetUTCDate()", "SYS_EXTRACT_UTC(SYSTIMESTAMP)", RegexOptions.IgnoreCase);

            //LibData Tree
            if (sql.Contains("'' myStatus, -9 mID, -20 matrixiID FROM tree"))
            {
                sql = sql.Replace("a.Status_b", "null");
                sql = sql.Replace("order by GMID", String.Empty);
            }

            sql = Regex.Replace(sql, @"\bCLUSTERED\b", String.Empty, RegexOptions.IgnoreCase);
            sql = Regex.Replace(sql, @"\bdbo.\b", String.Empty, RegexOptions.IgnoreCase);
            sql = Regex.Replace(sql, @"\bASC\b", String.Empty, RegexOptions.IgnoreCase);
            sql = Regex.Replace(sql, @"\bNOT NULL Default\b", "Default", RegexOptions.IgnoreCase);
            sql = Regex.Replace(sql, @"\bnvarchar2\b", "nvarchar", RegexOptions.IgnoreCase);
            sql = Regex.Replace(sql, @"\bnvarchar\b", "nvarchar2", RegexOptions.IgnoreCase);
            sql = Regex.Replace(sql, @"\bDatetime\b", "timestamp(3)", RegexOptions.IgnoreCase);

            sql = sql.Replace("a.Names + '(' + a.Email + ')'", "a.Names || '(' || a.Email || ')'");

            sql = sql.ReplaceIgnoreCase("DATEADD(dd, -1, DATEDIFF(dd, 0, systimestamp))", "to_timestamp(trunc(systimestamp))");

            String SQL = sql.ToUpper();
            //wip. this works only if its on 1 line, and has where clause. need to loop to find exact lines, as DATEADD. should test if works when sql is 1 line only
            if (SQL.Contains("SELECTs TOP "))
            {
                String s = "select top ";
                s = sql.Substring(sql.IndexOf("select top ") + s.Length);
                s = s.Split(' ')[0];

                int i = 0;
                if (int.TryParse(s, out i))
                {
                    sql = sql.ReplaceIgnoreCase(" top " + i.ToString(), " ");
                    if (sql.ToLower().Contains(" where "))
                        sql = sql.ReplaceIgnoreCase(" where ", " where RowNum <= " + i.ToString() + " and ");
                }
            }
            //above enhanced. on test
            if (SQL.Contains("SELECT TOP "))
            {
                string[] lines = sql.Split('\n');
                String newSQL = String.Empty;
                for (int x = 0; x < lines.Length; x++)
                {
                    String line = lines[x].Trim();
                    String LINE = line.ToUpper();

                    if (LINE.Contains("SELECT TOP "))
                    {
                        String s = "select top ";
                        int c = LINE.IndexOf("SELECT TOP ") + s.Length;
                        s = line.Substring(c);
                        s = s.Split(' ')[0];

                        int i = 0;
                        if (int.TryParse(s, out i))
                        {
                            line = line.ReplaceIgnoreCase(" top " + i.ToString(), " ");
                            if (line.ToLower().Contains(" where "))
                            {
                                line = line.ReplaceIgnoreCase(" where ", " where RowNum <= " + i.ToString() + " and ");
                                lines[x] = line;
                            }
                        }
                    }
                    newSQL += lines[x] + "\n";
                }

                sql = newSQL;
            }

            Boolean bSingleReplacement = false;
            if (SQL.Contains("DATEADD"))
                bSingleReplacement = true;
            if (bSingleReplacement)
            {
                string[] lines = sql.Split('\n');
                String s = String.Empty;
                for (int i = 0; i < lines.Length; i++)
                {
                    String line = lines[i].Trim();
                    String LINE = line.ToUpper();

                    //l.DateTimeGPS_UTC + interval '-10' MINUTE dtLocalTime, --DATEADD(MI, 10, l.DateTimeGPS_UTC) dtLocalTime,
                    //f1.DateTimeGPS_UTC + (interval '1' MINUTE * f2.TimeZone)
                    if (LINE.Contains("DATEADD"))
                        lines[i] = OracleDateAddDiff(line, "DATEADD");
                    if (LINE.Contains("DATEDIFF"))
                        lines[i] = OracleDateAddDiff(line, "DATEDIFF");

                    s += lines[i] + "\n";
                }

                sql = s;
            }

            Boolean bSP = false;
            if (sql.StartsWith("\r\n--Live"))
                bSP = true;
            else if (sql.StartsWith("\r\n--Update Road Speed"))
                bSP = true;
            else if (sql.Split('\n').Length > 5)
                bSP = true;
            else
            {
                String tmp = String.Empty;
                string[] lines = sql.Split('\n');
                for (int i = 0; i < lines.Length; i++)
                {
                    String line = lines[i].Trim();
                    if (line.EndsWith("--Remove from Oracle"))  //ignore full line
                        continue;
                    tmp += line + "\n";
                }
                sql = tmp;
            }

            if (bSP)
            {
                sql = sql.Replace("--Oracle Add SemiColumn", ";");
                sql = sql.ReplaceIgnoreCase("' AS TableName", "' AS TableName from dual;");

                sql = sql.ReplaceIgnoreCase("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ", "-- SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ");
                sql = sql.Replace("  ", " ");
                sql = sql.ReplaceIgnoreCase("Declare @ProcessTable Table (AssetID int, dt timestamp(3))", " ");
                sql = sql.Replace("@ProcessTable", "GlobalTmpProcessTable");

                sql = sql.ReplaceIgnoreCase("declare @ZonesIDs table (i int)", " ");
                sql = sql.Replace("@ZonesIDs", "GlobalTmpZonesIDs");

                sql = sql.ReplaceIgnoreCase("declare @fTmpZone table (i int)", " ");
                sql = sql.Replace("@fTmpZone", "GlobalTmpfTmpZone");

                sql = sql.ReplaceIgnoreCase("declare @tTmpDistinctZone table (i int)", " ");
                sql = sql.Replace("@tTmpDistinctZone", "GlobalTmptTmpDistinctZone");

                sql = sql.ReplaceIgnoreCase("declare @Zones table (ZoneID int, minLat float, minLon float, maxLat float, maxLon float)", " ");
                sql = sql.Replace("@Zones", "GlobalTmpZones");

                sql = sql.ReplaceIgnoreCase("#ZoneExceptions", "GlobalTmp_ZoneExceptions");
                sql = sql.Replace("#TripIDs", "GlobalTmpTripDetails_TripIDs");
                sql = sql.Replace("#td", "GlobalTmpTripDetails_td");

                sql = sql.Replace("#Trips", "GlobalTmpAddressTrips");
                sql = sql.Replace("#TripWizNoAddress", "GlobalTmpAddressTripWizNoAddress");

                sql = sql.ReplaceIgnoreCase("begin tran", "");
                sql = sql.ReplaceIgnoreCase("begin try", "BEGIN --try"); ;
                sql = sql.ReplaceIgnoreCase("end try", "");
                sql = sql.ReplaceIgnoreCase("begin catch", "EXCEPTION --[Oracle Exception");
                sql = sql.ReplaceIgnoreCase("end catch", "END; --Oracle Exception]");

                sql = sql.Replace("--Cursor Starts", "--[Cursor Starts");
                sql = sql.Replace("--Cursor ends", "--Cursor ends]");

                sql = sql.Replace("--SelectIntoStarts", "--[SelectIntoStarts");
                sql = sql.Replace("--SelectIntoEnds", "--SelectIntoEnds]");

                sql = sql.Replace("--OracleTHReplaceStarts", "--[OracleTHReplaceStarts");
                sql = sql.Replace("--OracleTHReplaceEnds", "--OracleTHReplaceEnds]");

                //sql = sql.Replace("EXCEPTION --Oracle Exception", "EXCEPTION --[Oracle Exception");
                //sql = sql.Replace("END; --Oracle Exception", "END; --Oracle Exception]");
                sql = sql.Replace("--min from multiple columns", "--[min from multiple columns");
                sql = sql.Replace("-- end of minFromMultipleColumns", "-- end of minFromMultipleColumns]");

                String sResult = string.Empty;
                string[] lines = sql.Split('\n');
                for (int i = 0; i < lines.Length; i++)
                {
                    try
                    {
                        string tab = String.Empty;
                        if (lines[i].StartsWith("\t"))
                        {
                            for (int x = 0; x <= lines[i].Length; x++)
                            {
                                if (lines[i][x].ToString() == "\t")
                                    tab += lines[i][x];
                                else
                                    break;
                            }
                        }
                        String line = lines[i].Trim();
                        String LINE = line.ToUpper();
                        String deleteCmd = String.Empty;
                        String updateCmd = String.Empty;
                        String withCmd = String.Empty;
                        if (line.EndsWith("--Remove from Oracle"))  //ignore full line
                            continue;

                        if (LINE.Contains(" AS TIME)"))
                            line = CastSecondsToTime(line);

                        if (line.Contains("GFI_GPS_GPSData values ("))
                        {
                            String[] date = line.Split('\'');
                            DateTime dt = DateTime.Now;
                            foreach (String d in date)
                                if (DateTime.TryParse(d, out dt))
                                {
                                    sResult += line.Replace("'" + d + "'", "TO_TIMESTAMP('" + d + "','DD-MON-YYYY HH24:MI:SS.FF3')");
                                }
                            sResult += "\n";
                            continue;
                        }
                        else if (line.StartsWith("--[OracleTHReplaceStarts"))
                        {
                            if (line.StartsWith("--[OracleTHReplaceStarts0"))
                            {
                                String sTH = GetNestedString(i, sql, '[', ']');
                                sResult += OracleTHReplace(0);
                                int numLines = sTH.Split('\n').Length;
                                i += numLines;
                                continue;
                            }
                            else if (line.StartsWith("--[OracleTHReplaceStarts1"))
                            {
                                String sTH = GetNestedString(i, sql, '[', ']');
                                sResult += OracleTHReplace(1);
                                int numLines = sTH.Split('\n').Length;
                                i += numLines;
                                continue;
                            }
                        }
                        else if (line.StartsWith("update ", true, System.Globalization.CultureInfo.CurrentCulture))
                        {
                            for (int j = i; j < lines.Length; j++)
                            {
                                i = j;
                                line = lines[i].Trim();
                                updateCmd += line + "\n";

                                if (String.IsNullOrEmpty(line))
                                    break;
                            }
                            sResult += OracleMerge(updateCmd, tab); //sResult += OracleUpdate(updateCmd, tab);
                            continue;
                        }
                        else if (line.StartsWith("delete ", true, System.Globalization.CultureInfo.CurrentCulture))
                        {
                            for (int j = i; j < lines.Length; j++)
                            {
                                i = j;
                                line = lines[i].Trim();
                                deleteCmd += line + "\n";

                                if (String.IsNullOrEmpty(line))
                                    break;
                            }
                            sResult += OracleDelete(deleteCmd, tab);
                            continue;
                        }
                        else if (line.StartsWith("--[SelectIntoStarts"))
                        {
                            String sInto = GetNestedString(i, sql, '[', ']');
                            sResult += OracleSelectInto(sInto, tab);
                            int numLines = sInto.Split('\n').Length;
                            i += numLines;
                            continue;
                        }
                        else if (line.ToLower().Contains(";with"))
                        {
                            for (int j = i; j < lines.Length; j++)
                            {
                                i = j;
                                line = lines[i].Trim();
                                withCmd += line + "\n";

                                if (String.IsNullOrEmpty(line))
                                    break;
                            }
                            sResult += OracleWith(SQLIndentation(withCmd, tab), tab);
                            continue;
                        }
                        else if (line.StartsWith("--[Cursor Starts >>> Outer"))
                        {
                            String sCursor = GetNestedString(i, sql, '[', ']');
                            sResult += OracleCursor(sCursor, tab);
                            i++;
                            continue;
                        }
                        else if (line.StartsWith("--[Cursor Starts"))
                        {
                            String sCursor = GetNestedString(i, sql, '[', ']');
                            sResult += OracleCursor(sCursor, tab);
                            int numLines = sCursor.Split('\n').Length;
                            i += numLines;
                            continue;
                        }
                        else if (LINE.StartsWith("SET @"))
                        {
                            String s = line;
                            s = s.ReplaceIgnoreCase("set @", "v_");
                            s = s.Replace("=", ":=");
                            s += ";" + "\n"; ;
                            sResult += s;
                            continue;
                        }
                        else if (line.StartsWith("--sOracleGeomData"))
                        {
                            String sGeomData = GetNestedStringFromStartPos(line.IndexOf('(') - 1, line, '(', ')');
                            String id = lines[lines.Length - 1].Split('=')[1].Trim();
                            i = lines.Length - 1;
                            sResult += GeomData(sGeomData, id);
                            continue;
                        }

                        //All Above continue
                        else if (line.StartsWith("EXCEPTION --[Oracle Exception"))
                        {
                            String sException = GetNestedString(i, sql, '[', ']');
                            if (sException.Contains("--SQLLiveExceptions"))
                            {
                                sResult += LiveException(sException, tab);
                                int numLines = sException.Split('\n').Length;
                                i += numLines;
                            }
                        }
                        else if (line.StartsWith("--[min from multiple columns"))
                        {
                            String s = GetNestedString(i, sql, '[', ']');
                            sResult += ProcessRoadSpeed(s);
                            int numLines = s.Split('\n').Length;
                            i += numLines;
                        }
                        else if (line.StartsWith("--Declaring variables"))
                        {
                            sResult += line + "\n";
                            for (int j = i + 1; j < lines.Length; j++)
                            {
                                line = lines[j].Trim().ReplaceIgnoreCase("declare @", "v_");
                                line = line.ReplaceIgnoreCase(" int", " number");
                                if (line == "end")
                                {
                                    sResult += "--" + line + "\n";
                                    i = j;
                                    break;
                                }
                                else if (line.ToLower() == "begin")
                                {
                                    //sResult += line + "\n";
                                    sResult += "Declare \r\n";
                                }
                                else if (line.Length > 0)
                                    sResult += "\t" + line + ";" + "\n";
                            }
                        }

                        sResult += tab + line + "\n";
                    }
                    catch (Exception ex) { }
                }

                if (sql.StartsWith("\r\n--Live"))
                    sResult = sqlStartsWith(sResult, "\r\n--Live");
                else if (sql.StartsWith("\r\n--Update Road Speed"))
                    sResult = sqlStartsWith(sResult, "\r\n----Update Road Speed");
                else if (sql.StartsWith("\r\n--RetrieveLiveData"))
                    sResult = sqlStartsWith(sResult, "\r\n--RetrieveLiveData");
                else if (sql.StartsWith("\r\n--Trips Processor"))
                    sResult = sqlStartsWith(sResult, "\r\n--Trips Processor");
                else if (sql.StartsWith("\r\n--GetAssetsStatus"))
                    sResult = sqlStartsWith(sResult, "\r\n--GetAssetsStatus");
                else if (sql.StartsWith("\r\n--TripsRetrieval"))
                    sResult = sqlStartsWith(sResult, "\r\n--TripsRetrieval");
                else if (sql.StartsWith("\r\n--TripDetails"))
                    sResult = sqlStartsWith(sResult, "\r\n--TripDetails");

                sql = sResult;
            }

            sql += "\r\n--AlreadyOracleStatement";
            return sql;
        }
        static String SQLIndentation(String sql, String tab)
        {
            String sResult = String.Empty;
            String[] lines = sql.Split('\n');
            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].Contains(";with"))
                    sResult += lines[i] + "\n";
                else
                    sResult += tab + lines[i] + "\n";
            }
            return sResult;
        }
        static String OracleDelete(String sql, String tab)
        {
            /*
delete from TMP_d2e5f723_2aee_4966_8e41_e946c8fec6cdGFI_GPS_GPSDataDetail
from TMP_d2e5f723_2aee_4966_8e41_e946c8fec6cdGFI_GPS_GPSDataDetail t1
inner join GFI_FLT_Driver t2 on t1.TypeValue  = t2.iButton  where t2.EmployeeType = 'Driver' and t2.iButton != ''
           
delete from TMP_d2e5f723_2aee_4966_8e41_e946c8fec6cdGFI_GPS_GPSDataDetail where rowid in
(select t1.rowid from TMP_d2e5f723_2aee_4966_8e41_e946c8fec6cdGFI_GPS_GPSDataDetail t1
inner join GFI_FLT_Driver t2 on t1.TypeValue  = t2.iButton  where t2.EmployeeType = 'Driver' and t2.iButton != ''
)
*/
            if (sql.ToUpper().Contains("WHERE NOT EXISTS"))
                return sql.TrimEnd() + ";" + "\r\n";

            String sDelete = String.Empty;
            string[] lines = sql.Split('\r', '\n');
            String T2 = String.Empty;
            for (int i = 0; i < lines.Length; i++)
            {
                String line = lines[i].Trim();

                if (String.IsNullOrEmpty(line))
                    continue;
                else
                {
                    if (line.ToUpper().Trim().StartsWith("DELETE"))
                    {
                        sDelete = lines[i];
                        if ((i + 1) <= lines.Length && !String.IsNullOrEmpty(lines[i + 1].Trim()))
                            sDelete += " where rowid in (";
                        else sDelete += ";";
                    }
                    else if (line.Trim().StartsWith("from", StringComparison.CurrentCultureIgnoreCase))
                    {
                        String table = line.Trim().Split(' ')[1];
                        String alias = line.Trim().Split(' ')[2];
                        sDelete += "select " + alias + ".rowid from " + table + " " + alias + " ";
                    }
                    else if (line.Trim().StartsWith("inner join", StringComparison.CurrentCultureIgnoreCase))
                    {
                        sDelete += line;
                        sDelete += ");";
                        sDelete += "\r\n";
                    }
                }
            }
            return SQLIndentation(sDelete, tab);
        }
        static String OracleUpdate(String sql, String tab)
        {
            /*
    update TMP_fd0cca29_a832_4c0d_82cb_b4e5345b5784GFI_GPS_GPSData a
    set a.AssetID = (
    select b.AssetID
    from GFI_FLT_AssetDeviceMap b
    where a.DeviceId = b.DeviceId and Status_b = 'AC'
    )

    UPDATE TMP_12570440_fb24_4c3a_b75e_5b8111a3dd67GFI_GPS_GPSData T1
    SET AssetID = T2.AssetID
    FROM TMP_12570440_fb24_4c3a_b75e_5b8111a3dd67GFI_GPS_GPSData T1
    INNER JOIN GFI_FLT_AssetDeviceMap T2 ON T2.DeviceId COLLATE DATABASE_DEFAULT = T1.DeviceID COLLATE DATABASE_DEFAULT and Status_b = 'AC'
    */
            String sUpdate = String.Empty;
            String exists = String.Empty;
            string[] lines = sql.Split('\r', '\n');
            String T2 = String.Empty;
            for (int i = 0; i < lines.Length; i++)
            {
                String line = lines[i].Trim();

                if (String.IsNullOrEmpty(line))
                    continue;
                else
                {
                    String[] split = line.Split(' ');
                    if (line.ToUpper().StartsWith("UPDATE"))
                        sUpdate = lines[i] + " T1" + "\n";
                    if (line.ToUpper().StartsWith("SET"))
                    {
                        sUpdate += "\t" + split[0] + " T1." + split[1] + " = (";
                        T2 = split[3].Split('.')[1];
                        exists = "where exists(";
                    }
                    else if (line.ToUpper().StartsWith("FROM"))
                    {
                        sUpdate += "Select distinct T2." + T2 + " FROM ";
                        exists += "Select T2." + T2 + " FROM ";
                    }
                    else if (line.ToUpper().StartsWith("INNER JOIN"))
                    {
                        sUpdate += split[2] + " T2 where ";
                        sUpdate += line.Substring(line.IndexOf(" on ", StringComparison.CurrentCultureIgnoreCase) + 4);
                        sUpdate += ")";
                        sUpdate += "\r\n";

                        exists += split[2] + " T2 where ";
                        exists += line.Substring(line.IndexOf(" on ", StringComparison.CurrentCultureIgnoreCase) + 4);
                        exists += ");";
                        exists += "\r\n";

                        sUpdate += exists;
                    }
                }
            }
            sUpdate = sUpdate.Replace("--UseUpdateCmd", String.Empty);
            return SQLIndentation(sUpdate, tab);
        }
        static String OracleMerge(String sql, String tab)
        {
            /*
update GlobalTmpProcessRoadSpeed
    set TypeRoadSpeed = r.MotorWay
from GlobalTmpProcessRoadSpeed d
    inner join GFI_GPS_GPSData g on d.UID = g.UID
	inner join GFI_FLT_Asset a on a.AssetID = g.AssetID
	inner join GFI_FLT_RoadSpeedType r on r.iID = a.RoadSpeedType
where d.RoadType = 'Motorway'


MERGE INTO GlobalTmpProcessRoadSpeed D
USING (SELECT D.ROWID row_id, r.MotorWay
FROM GlobalTmpProcessRoadSpeed D
       JOIN GFI_GPS_GPSData g ON D."UID" = g."UID"
       JOIN GFI_FLT_Asset a ON a.AssetID = g.AssetID
       JOIN GFI_FLT_RoadSpeedType r ON r.iID = a.RoadSpeedType 
 WHERE D.RoadType = 'MotorWay') src
ON ( D.ROWID = src.row_id )
WHEN MATCHED THEN UPDATE SET TypeRoadSpeed = src.MotorWay;
*/
            if (!sql.ToLower().Contains(" join "))
                return sql.Trim() + ";\r\n";

            if (sql.Contains("--UseUpdateCmd"))
                return OracleUpdate(sql, tab);

            String sMerge = String.Empty;
            String alias = String.Empty;
            String sSet = String.Empty;
            String table = String.Empty;

            string[] lines = sql.Split('\n');
            for (int i = 0; i < lines.Length; i++)
            {
                String line = lines[i].Trim();

                if (String.IsNullOrEmpty(line))
                    continue;
                else
                {
                    String[] split = line.Split(' ');
                    if (line.ToUpper().StartsWith("UPDATE"))
                    {
                        table = line.Trim().Split(' ')[1];

                        for (int x = i + 1; x < lines.Length; x++)
                        {
                            if (lines[x].Trim().ToLower().StartsWith("from"))
                            {
                                alias = lines[x].ReplaceIgnoreCase("from " + table, String.Empty).Trim();
                                break;
                            }
                        }

                        for (int x = i + 1; x < lines.Length; x++)
                        {
                            if (lines[x].Trim().ToLower().StartsWith("set"))
                            {
                                sSet = lines[x];
                                String[] s = sSet.Trim().Split(' ');
                                break;
                            }
                        }

                        sMerge = lines[i].ReplaceIgnoreCase("update ", "MERGE INTO ") + " " + alias + "\n";
                        String[] sFields = sSet.Split(',');
                        String sField = String.Empty;
                        foreach (String s in sFields)
                            sField += s.Substring(s.LastIndexOf(" ")).ToString().Trim() + ", ";
                        sField = sField.Substring(0, sField.Length - 2);
                        sMerge += "USING (SELECT " + alias + ".ROWID row_id, " + sField + "\r\n";
                    }
                    else if (line.ToUpper().StartsWith("SET"))
                    { }
                    else
                        sMerge += lines[i] + "\n";
                }
            }
            sMerge += ") src" + "\r\n";
            sMerge += "ON (" + alias + ".ROWID = src.row_id)" + "\r\n";

            String[] sMatches = sSet.Split(',');
            String match = String.Empty;
            foreach (String s in sMatches)
            {
                String myAlias = s.Substring(s.LastIndexOf(" ")).ToString().Trim().Split('.')[0];
                String g = s.Replace(myAlias + ".", "src.");
                match += g + ", ";
            }
            match = match.Substring(0, match.Length - 2);

            sMerge += "WHEN MATCHED THEN UPDATE " + match + ";\r\n";
            sMerge += "\r\n";

            return sMerge;
        }
        static String OracleWith(String sql, String tab)
        {
            /*
    delete from GFI_GPS_HeartBeat where rowid in(
    with cte as
    (
    SELECT T1.*, T2.*, ROW_NUMBER() OVER (PARTITION BY T2.AssetID ORDER BY T2.DateTimeGPS_UTC DESC) as Row_Num
    FROM TMP_8121f06d_7d06_4c55_8f87_dc21d17ba858GFI_GPS_GPSDataDetail T1
    INNER JOIN TMP_8121f06d_7d06_4c55_8f87_dc21d17ba858GFI_GPS_GPSData T2 ON T2.iID = T1."UID"
    where TypeID = 'Heartbeat'
    and GPSDataID = 0
    )
    select h.rowid
    from GFI_GPS_HeartBeat h
    inner join cte on cte.AssetID = h.AssetID
    )

    insert into GFI_GPS_HeartBeat (AssetID, DateTimeGPS_UTC, DateTimeServer, Longitude, Latitude)
    with cte as
    (
    SELECT T1.*, T2.*, ROW_NUMBER() OVER (PARTITION BY T2.AssetID ORDER BY T2.DateTimeGPS_UTC DESC) as Row_Num
    FROM TMP_8121f06d_7d06_4c55_8f87_dc21d17ba858GFI_GPS_GPSDataDetail T1
    INNER JOIN TMP_8121f06d_7d06_4c55_8f87_dc21d17ba858GFI_GPS_GPSData T2 ON T2.iID = T1."UID"
    where TypeID = 'Heartbeat'
    and GPSDataID = 0
    )
    select AssetID, DateTimeGPS_UTC, systimestamp, Longitude, Latitude
    from CTE where Row_Num = 1
             */
            String sResult = String.Empty;
            if (sql.ToLower().Contains(";with"))
            {
                if (sql.Contains("ZoneMatrixRetrieval"))
                {
                    sResult = @"
insert into GlobalTmpfTmpZone(i)
WITH 
	parent AS 
	(
		SELECT distinct ParentGMID
		FROM GFI_SYS_GroupMatrix g
            inner join GlobalTmpZonesIDs a on g.ParentGMID = a.i
	), 
	tree (GMID, GMDescription, ParentGMID) as
	(
		SELECT 
			x.GMID
			, x.GMDescription
			, x.ParentGMID
		FROM GFI_SYS_GroupMatrix x
		INNER JOIN parent ON x.ParentGMID = parent.ParentGMID
		UNION ALL
		SELECT 
			y.GMID
			, y.GMDescription
			, y.ParentGMID
		FROM GFI_SYS_GroupMatrix y
		INNER JOIN tree t ON y.ParentGMID = t.GMID
	)
	select distinct a.ZoneID   
		from GFI_FLT_ZoneHeader a
            inner join GFI_SYS_GroupMatrixZone m on a.ZoneID = m.iID
            inner join tree t on t.GMID = m.GMID;
";
                    return sResult;
                }
                String insideWith = GetNestedString(0, sql, '(', ')');
                String[] iw = insideWith.Split('\n');
                String lastLine = String.Empty;
                Boolean bDelete = false;
                Boolean isSelect = false;
                int iFirstRun = 0;

                foreach (String l in iw)
                    if (!String.IsNullOrEmpty(l.Trim()))
                        lastLine = l;

                string[] lines = sql.Split('\n');
                for (int i = 0; i < lines.Length; i++)
                {
                    Boolean bFound = false;
                    String line = lines[i];
                    if (line == lastLine)
                    {
                        bFound = true;
                        i++;
                        i++;
                    }
                    if (bFound)
                    {
                        line = lines[i];

                        iFirstRun++;
                        if (iFirstRun == 1)
                            if (line.Trim().ToLower().StartsWith("select "))
                                isSelect = true;
                        if (line.ToLower().Contains("insert "))
                            sResult = line + "\n";
                        else if (line.ToLower().Contains("delete "))
                        {
                            String[] s = line.Trim().Split(' ');
                            sResult = "delete from " + s[1] + " where rowid in(" + "\n";
                            bDelete = true;
                        }

                        foreach (String l in sql.Split('\n'))
                            if (l.ToLower().Contains(";with"))
                            {
                                sResult += l.Replace(";", String.Empty) + "\n";
                                break;
                            }
                        sResult += "(" + SQLIndentation(insideWith, "\t") + ")";
                        if (!bDelete && !isSelect) //&& condition still under test. need to chk live data fuel to confirm. !line.Trim().StartsWith("select ", StringComparison.CurrentCultureIgnoreCase)
                            i++;
                    }

                    if (!String.IsNullOrEmpty(sResult))
                    {
                        line = lines[i];
                        if (line.ToLower().Contains("delete "))
                        {
                            String alias = "h";
                            String table = line.Trim().Split(' ')[1].ToLower();
                            for (int x = i; x < lines.Length; x++)
                                if (lines[x].ToLower().Contains("from " + table))
                                    alias = lines[x].ToLower().Replace("from " + table, String.Empty).Trim();

                            line = "select " + alias + ".rowid";
                        }
                        sResult += line + "\n";
                    }
                }

                //select statement
                if (sResult.ToLower().StartsWith("with"))
                    sResult = sql;

                //need to automate following
                sResult = sResult.ReplaceIgnoreCase("SELECT *, Row_Num = ROW_NUMBER() OVER (PARTITION BY T2.AssetID ORDER BY T2.DateTimeGPS_UTC DESC)", "SELECT T1.*, T2.*, ROW_NUMBER() OVER (PARTITION BY T2.AssetID ORDER BY T2.DateTimeGPS_UTC DESC) as Row_Num");
                sResult = sResult.ReplaceIgnoreCase("SELECT T1.*, Row_Num = ROW_NUMBER() OVER (PARTITION BY AssetID, DateTimeGPS_UTC ORDER BY DateTimeGPS_UTC DESC)", "SELECT T1.*, ROW_NUMBER() OVER (PARTITION BY AssetID, DateTimeGPS_UTC ORDER BY DateTimeGPS_UTC DESC) as Row_Num");
                sResult = sResult.ReplaceIgnoreCase("SELECT T1.*, Row_Num = ROW_NUMBER() OVER (PARTITION BY AssetID ORDER BY DateTimeGPS_UTC DESC)", "SELECT T1.*, ROW_NUMBER() OVER (PARTITION BY AssetID ORDER BY DateTimeGPS_UTC DESC) as Row_Num");
                sResult = sResult.ReplaceIgnoreCase("SELECT T1.*, Row_Num = ROW_NUMBER() OVER (PARTITION BY AssetID ORDER BY DateTimeGPS_UTC ASC)", "SELECT T1.*, ROW_NUMBER() OVER (PARTITION BY AssetID ORDER BY DateTimeGPS_UTC ASC) as Row_Num");
                sResult = sResult.ReplaceIgnoreCase("SELECT T1.*, Row_Num = ROW_NUMBER() OVER(PARTITION BY AssetID ORDER BY DateTimeGPS_UTC)", "SELECT T1.*, ROW_NUMBER() OVER (PARTITION BY AssetID ORDER BY DateTimeGPS_UTC) as Row_Num");
                sResult = sResult.ReplaceIgnoreCase("SELECT T1.*, RowNum = ROW_NUMBER() OVER (PARTITION BY AssetID ORDER BY DateTimeGPS_UTC ASC)", "SELECT T1.*, ROW_NUMBER() OVER (PARTITION BY AssetID ORDER BY DateTimeGPS_UTC ASC) as Row_Num");
                sResult = sResult.ReplaceIgnoreCase("SELECT T1.*, Row_Num = ROW_NUMBER() OVER (PARTITION BY AssetID ORDER BY DateTimeGPS_UTC )", "SELECT T1.*, ROW_NUMBER() OVER (PARTITION BY AssetID ORDER BY DateTimeGPS_UTC) as Row_Num");
                sResult = sResult.ReplaceIgnoreCase(";with", "with");
                if (bDelete)
                    sResult = sResult.Trim() + ")";

                if (!isSelect)
                    sResult = sResult.Trim() + ";\n";
                //String g = doBracesWork(sql);
            }
            return SQLIndentation(sResult, tab);
        }
        static String OracleCursor(String sql, String tab)
        {
            sql = sql.Replace("Cursor Starts", "--Cursor Starts");
            string[] lines = sql.Split('\n');
            String sResult = String.Empty;
            Boolean bDeclareStarted = false;
            String sCursorName = String.Empty;

            for (int i = 1; i < lines.Length; i++)
            {
                String line = lines[i];
                if (line == "--declare @AllClients number := (select GMID from GFI_SYS_GroupMatrix where GMDescription = '### All Clients ###'); --for oracle\r")
                    line = "declare @AllClients number := (select GMID from GFI_SYS_GroupMatrix where GMDescription = '### All Clients ###');  --for oracle\r\n";
                if (line.Trim().ToLower().StartsWith("Declare", StringComparison.CurrentCultureIgnoreCase))
                {
                    if (!bDeclareStarted)
                    {
                        sResult = line.Replace("@", "v_").TrimEnd().Replace("\r", " ") + ";\r\n";
                        bDeclareStarted = true;
                    }
                    else
                    {
                        if (line.ToLower().Contains("cursor for") || line.ToUpper().Contains("CURSOR LOCAL SCROLL STATIC FOR"))
                        {
                            //Declare c Cursor for
                            //CURSOR v_customers is 

                            sCursorName = line.ReplaceIgnoreCase("Declare ", " ");
                            sCursorName = sCursorName.ReplaceIgnoreCase(" Cursor for", " ").Trim();
                            sCursorName = sCursorName.ReplaceIgnoreCase(" CURSOR LOCAL SCROLL STATIC FOR", " ").Trim();

                            String s = line.ReplaceIgnoreCase(" Cursor for", " is");
                            s = s.ReplaceIgnoreCase(" CURSOR LOCAL SCROLL STATIC FOR", " is");
                            s = s.ReplaceIgnoreCase("Declare ", "Cursor ");
                            for (int j = i + 1; j < lines.Length; j++)
                            {
                                i = j;
                                s += lines[i];
                                line = lines[i].Trim();

                                if (String.IsNullOrEmpty(line))
                                {
                                    s = s.Substring(0, s.LastIndexOf('\r')) + ";" + "\r";
                                    break;
                                }
                            }
                            sResult += s + "\r\n";
                        }
                        else
                        {
                            String s = line.Replace("@", "v_").TrimEnd().Replace("\r", " ") + ";\r\n";
                            s = s.ReplaceIgnoreCase("Declare", " ");
                            sResult += s;
                        }
                    }
                }
                else //open cursor and loop
                {
                    if (line.ToLower().Contains("open "))
                    {
                        sResult += "BEGIN \r\n";
                        sResult += tab + "OPEN " + sCursorName + ";\r\n";
                        sResult += "LOOP \r\n";
                    }
                    else if (line.ToLower().Contains("fetch "))
                    {
                        String s = line.ReplaceIgnoreCase(" NEXT FROM ", " ");
                        s = s.Substring(0, s.LastIndexOf('\r')) + ";" + "\r";
                        s += tab + "\tEXIT WHEN " + sCursorName + "%notfound;\r\n";

                        i++;    //skipping WHILE @@FETCH_STATUS = 0
                        i++;    //skipping BEGIN

                        //locating next fetch
                        for (int j = i + 1; j < lines.Length; j++)
                        {
                            i = j;
                            line = lines[i];
                            if (line.ToUpper().Contains("FETCH NEXT FROM " + sCursorName.ToUpper() + " INTO"))
                                continue;
                            if (line.ToLower().Trim() == "end")
                            {
                                s += tab + "END LOOP;\r\n";
                                break;
                            }
                            else if (line.Trim().ToLower().StartsWith("exec "))
                            {
                                String g = line.ReplaceIgnoreCase("EXEC ", "BEGIN ");
                                g = g.ReplaceIgnoreCase(" RegisterDevicesForProcessing ", " RegisterDevicesForProcessing (");
                                g = g.Substring(0, g.LastIndexOf('\r')) + "); END;\r\n";
                                s += g;
                                continue;
                            }

                            s += lines[i];
                        }
                        sResult += s;
                    }
                    else if (line.ToUpper().Contains("DEALLOCATE "))
                    {
                        sResult += tab + "END;\r\n";
                    }
                }
            }
            sResult = sResult.ReplaceIgnoreCase("@", "v_");
            return SQLIndentation(sResult, tab);

        }
        static String OracleSelectInto(String sql, String tab)
        {
            String sTable = String.Empty;
            sql = sql.Replace("SelectIntoStarts", "--SelectIntoStarts");
            sql = sql.Replace("--SelectIntoEnds", String.Empty);
            string[] lines = sql.Split('\n');
            foreach (String line in lines)
            {
                if (line.ToLower().Trim().Contains("into "))
                {
                    String sInto = line.Substring(line.IndexOf("into "));
                    sTable = sInto.Split(' ')[1];
                    break;
                }
            }
            String sResult = String.Empty;

            String s = sql.Replace("into " + sTable, String.Empty);
            sResult = "\t" + "insert into " + sTable + "\r\n";
            sResult += s + ";";
            sResult = sResult.ReplaceIgnoreCase(";with", "with");

            sResult = sResult.Replace(", CONVERT(varchar, DATEADD(s, DATEDIFF('SECOND', min(DateTimeGPS_UTC), max(DateTimeGPS_UTC)), 0), 108) AS duration", ", DATEDIFF('SECOND', min(DateTimeGPS_UTC), max(DateTimeGPS_UTC)) AS duration");
            sResult = sResult.Replace(", convert(nvarchar2(2500), '') sZones", ", '' sZones");
            sResult = sResult.Replace(", convert(nvarchar2(250), '') sDriverName", ", '' sDriverName");

            sResult += "\r\n";
            return sResult;
        }
        static String OracleDateAddDiffCalc(String search, String pattern)
        {
            //f1.DateTimeGPS_UTC + (interval '1' MINUTE * f2.TimeZone)
            List<int> iFound = FindingAllPositionsInString(search, pattern);
            String result = search;
            foreach (int i in iFound)
            {
                String inDateAdd = GetNestedStringFromStartPos(i, search, '(', ')');
                String myFunc = pattern + "(" + inDateAdd + ")";
                String[] inDate = inDateAdd.Split(' ');

                String partResult = inDate[2].Trim().Replace(",", "") + " + (interval '";
                if (pattern.ToUpper() == "DATEDIFF")
                    partResult += "-";

                String intervalValue = inDate[1].Trim().Replace(",", "");
                int iValue = 0;
                if (!int.TryParse(intervalValue, out iValue))
                    intervalValue = "1";

                partResult += intervalValue + "' ";
                switch (inDate[0].Replace(",", "").ToUpper())
                {
                    case "MI":
                        partResult += "MINUTE";
                        break;

                    default:
                        partResult += "MINUTE";
                        break;
                }

                if (!int.TryParse(inDate[1].Trim().Replace(",", ""), out iValue))
                    partResult += " * " + inDate[1].Trim().Replace(",", "");

                partResult += ")";
                result = result.Replace(myFunc, partResult);
            }
            return result;
        }
        static String OracleDateAddDiff(String search, String pattern)
        {
            List<int> iFound = FindingAllPositionsInString(search, pattern);
            foreach (int i in iFound)
            {
                String inDateAdd = GetNestedStringFromStartPos(i, search, '(', ')');
                String intervalold = inDateAdd.Split(',')[0] + ", ";
                String intervalnew = "'" + inDateAdd.Split(',')[0] + "', ";
                search = search.Replace(intervalold, intervalnew);

                if (search.Contains("AS TIME)"))
                    search = CastSecondsToTime(search);

            }
            return search;
        }
        static String CastSecondsToTime(String sql)
        {
            int icast = sql.ToUpper().LastIndexOf("CAST");
            String cast = GetNestedStringFromStartPos(icast, sql, '(', ')');

            String[] tmp = cast.Split(',');
            String field = String.Empty;

            Boolean bFound = false;
            foreach(String t in tmp)
            {
                String x = t.Trim();
                if(x.Contains("'")) //Detecting interval type > minutes, seconds...
                {
                    bFound = true;
                    continue;
                }

                if(bFound)
                {
                    field = x;
                    break;
                }
            }

            String oracleCast = "to_char (trunc(sysdate) + numtodsinterval(125, 'second'),'hh24:mi:ss') ";
            String sResult = sql.Replace(cast, oracleCast);
            sResult = sResult.ReplaceIgnoreCase("cast", String.Empty);
            sResult = sResult.Replace("125", field);

            return sResult;
        }
        static String LiveException(String sql, String tab)
        {
            String sException = @"
EXCEPTION -- Oracle Exception
        WHEN OTHERS THEN
            declare errMsg NVARCHAR2 (200);
                    err_code NVARCHAR2 (50);
            BEGIN
                errMsg := SUBSTR(SQLERRM, 1, 200);
                err_code := SQLCODE; --SUBSTR(SQLCODE, 1, 50);
                ROLLBACK;              
                BEGIN                 
                    BEGIN
                        INSERT INTO GFI_GPS_ProcessErrors (sError, sOrigine, sFieldValue)
                            VALUES ( errMsg, 'LiveDwn', err_code );             
                    END;
                EXCEPTION
                    WHEN OTHERS THEN dbms_output.put_line(errMsg);
                END;   
            END;

    BEGIN
        --EXECUTE IMMEDIATE 'drop table table_name';
        EXECUTE IMMEDIATE 'truncate table GlobalTmp_GFI_GPS_GPSData';
        EXECUTE IMMEDIATE 'truncate table GlobalTmp_GFI_GPS_GPSDataDetail';
        EXECUTE IMMEDIATE 'truncate table GlobalTmpProcessTable';
    EXCEPTION
        WHEN OTHERS THEN
            DBMS_OUTPUT.put_line(SQLERRM);
    END;           
END;
";
            return sException;
        }
        static String ProcessRoadSpeed(String sql)
        {
            String s = @"
MERGE INTO GlobalTmpProcessRoadSpeed d
USING 
    (SELECT d.ROWID row_id, least(RoadSpeed, TypeRoadSpeed) minVal from GlobalTmpProcessRoadSpeed d) src
ON (d.ROWID = src.row_id)
WHEN MATCHED THEN UPDATE SET FinalRoadSpeed = src.MinVal;
";
            return s;
        }
        static string doBracesWork(string input)
        {
            Stack<int> openingBraces = new Stack<int>();
            string output = "";

            for (int i = 0; i < input.Length; i++)
            {
                if (input[i] == '(')
                {
                    openingBraces.Push(i);
                }
                else if (input[i] == ')')
                {
                    output += string.Format("{0} - {1}\n", openingBraces.Pop(), i);
                }
            }

            return output;
        }
        static string GetNestedString(int StartLine, string str, char start, char end)
        {
            String[] lines = str.Split('\n');
            String tmp = String.Empty;
            for (int x = StartLine; x < lines.Length; x++)
            {
                tmp += lines[x] + "\n";
            }
            str = tmp;

            int s = -1;
            int i = -1;
            while (++i < str.Length)
                if (str[i] == start)
                {
                    s = i;
                    break;
                }
            int e = -1;
            int depth = 0;
            while (++i < str.Length)
                if (str[i] == end)
                {
                    e = i;
                    if (depth == 0)
                        break;
                    else
                        --depth;
                }
                else if (str[i] == start)
                    ++depth;
            if (e > s)
                return str.Substring(s + 1, e - s - 1);
            return null;
        }
        static string GetNestedStringFromStartPos(int StartPos, string str, char start, char end)
        {
            int s = -1;
            int i = StartPos;
            while (++i < str.Length)
                if (str[i] == start)
                {
                    s = i;
                    break;
                }
            int e = -1;
            int depth = 0;
            while (++i < str.Length)
                if (str[i] == end)
                {
                    e = i;
                    if (depth == 0)
                        break;
                    else
                        --depth;
                }
                else if (str[i] == start)
                    ++depth;
            if (e > s)
                return str.Substring(s + 1, e - s - 1);
            return null;
        }
        static List<int> FindingAllPositionsInString(String search, String pattern)
        {
            /*
            //For multiple patterns:
            string search = "123aa456AA789bb9991AACAA";
            string[] patterns = new string[] { "aa", "99" };
            patterns.SelectMany(pattern => Enumerable.Range(0, search.Length)
               .Select(index => { return new { Index = index, Length = (index + pattern.Length) > search.Length ? search.Length - index : pattern.Length }; })
               .Where(searchbit => searchbit.Length == pattern.Length && pattern.Equals(search.Substring(searchbit.Index, searchbit.Length), StringComparison.OrdinalIgnoreCase))
               .Select(searchbit => searchbit.Index))
            */
            List<int> iFound = Enumerable.Range(0, search.Length)
                                         .Select(index => { return new { Index = index, Length = (index + pattern.Length) > search.Length ? search.Length - index : pattern.Length }; })
                                         .Where(searchbit => searchbit.Length == pattern.Length && pattern.Equals(search.Substring(searchbit.Index, searchbit.Length), StringComparison.OrdinalIgnoreCase))
                                         .Select(searchbit => searchbit.Index).ToList();
            return iFound;
        }
        static String sqlStartsWith(String sResult, String StartString)
        {
            int start = 0;
            int end = 0;
            int length = 0;

            sResult = sResult.Substring(sResult.IndexOf("--Oracle Starts Here"));
            sResult = sResult.Replace("STUFF(", String.Empty);
            sResult = sResult.Replace(", 1, 1, '')", String.Empty);

            switch (StartString)
            {
                case "\r\n--Live":
                    sResult = sResult.Trim() + ";\r\n";
                    sResult = sResult.Replace("#GFI_GPS_GPSDataDetail", "GlobalTmp_GFI_GPS_GPSDataDetail");
                    sResult = sResult.Replace("#GFI_GPS_GPSData", "GlobalTmp_GFI_GPS_GPSData");
                    sResult = @"
                                BEGIN
                                    EXECUTE IMMEDIATE 'truncate table GlobalTmp_GFI_GPS_GPSData';
                                    EXECUTE IMMEDIATE 'truncate table GlobalTmp_GFI_GPS_GPSDataDetail';
                                    EXECUTE IMMEDIATE 'truncate table GlobalTmpProcessTable';
                                EXCEPTION
                                    WHEN OTHERS THEN
                                        DBMS_OUTPUT.put_line(SQLERRM);
                                END;           
                            " + sResult;
                    sResult = "BEGIN\r\n" + sResult;
                    sResult = sResult.Replace("BEGIN --try", "--BEGIN --try");

                    sResult = sResult.Substring(0, sResult.LastIndexOf("EXCEPTION --[Oracle Exception"));
                    break;

                case "\r\n--Update Road Speed":
                    sResult = sResult.Trim() + ";\r\n";
                    sResult = sResult.Replace("#Debug", "GlobalTmpProcessRoadSpeed");
                    sResult = @"
                                BEGIN
                                    EXECUTE IMMEDIATE 'truncate table GlobalTmp_GFI_GPS_GPSData';
                                EXCEPTION
                                    WHEN OTHERS THEN
                                        DBMS_OUTPUT.put_line(SQLERRM);
                                END;           
                            " + sResult;
                    sResult = string.Format("BEGIN {0} END;", sResult);
                    break;

                case "\r\n--RetrieveLiveData":
                    sResult = sResult.Trim() + "\r\n";
                    sResult = sResult.Replace(")from cte where Row_Num <= 2;", @")select ""UID"", AssetID, Asset, DriverID, DateTimeGPS_UTC, dtLocalTime, DateTimeServer, Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, StopFlag, TripDistance, TripTime, Row_Num from cte where Row_Num <= 2;");
                    sResult = sResult.Replace("#Assets", "GlobalTmpLiveAssets");
                    sResult = sResult.Replace("#GFI_GPS_LiveData", "GlobalTmpLiveData");
                    sResult = @"
                                truncate table GlobalTmpLiveAssets;
                                truncate table GlobalTmpLiveData;
                                truncate table GlobalTmpZonesIDs;
                                truncate table GlobalTmpfTmpZone;
                                truncate table GlobalTmptTmpDistinctZone;
                                truncate table GlobalTmpZones;
                                " + sResult;
                    sResult = sResult.Substring(0, sResult.LastIndexOf(";"));
                    //sResult = string.Format("BEGIN {0} END;", sResult);
                    break;

                case "\r\n--GetAssetsStatus":
                    sResult = sResult.Replace("@Assets", "GlobalTmpAssetStatusAssets");
                    sResult = sResult.Replace("@Table", "GlobalTmpAssetStatusTable");
                    sResult = @"
                                truncate table GlobalTmpAssetStatusAssets;
                                truncate table GlobalTmpAssetStatusTable;
                                " + sResult;
                    sResult = sResult.Substring(0, sResult.LastIndexOf(";"));
                    break;

                case "\r\n--Trips Processor":
                    string[] lines = sResult.Split('\n');
                    int iAssetID = -1;
                    for (int i = 0; i < lines.Length; i++)
                    {
                        String line = lines[i].Trim();
                        if (line.StartsWith("set @ActualAssetID =", StringComparison.CurrentCultureIgnoreCase))
                        {
                            String sAssetID = line.Split('=')[1];
                            if (int.TryParse(sAssetID, out iAssetID))
                                sResult = NaveoIntel.ProcessIntel.OracleProcessTrips(iAssetID);
                            break;
                        }
                    }
                    break;

                case "\r\n--TripsRetrieval":
                    sResult = sResult.Replace("STUFF(", String.Empty);
                    sResult = sResult.Replace(", 1, 1, '')", String.Empty);
                    sResult = sResult.Replace("(SELECT ', ' + e.Description FROM #ArrivalZoneTrips e Where e.iID = h.iID FOR XML PATH(''))", "(select listagg (e.Description, ',' ) WITHIN GROUP (ORDER BY Row_Number) x FROM #ArrivalZoneTrips e Where e.iID = h.iID)");
                    sResult = sResult.Replace("(SELECT ', ' + CONVERT(varchar(10), e.ZoneID) FROM #ArrivalZoneTrips e Where e.iID = h.iID FOR XML PATH(''))", "(select listagg (e.ZoneID, ',' ) WITHIN GROUP (ORDER BY Row_Number) x FROM #ArrivalZoneTrips e Where e.iID = h.iID)");
                    sResult = sResult.Replace("(SELECT ', ' + CONVERT(varchar(10), e.Color) FROM #ArrivalZoneTrips e Where e.iID = h.iID FOR XML PATH(''))", "(select listagg (e.Color, ',' ) WITHIN GROUP (ORDER BY Row_Number) x FROM #ArrivalZoneTrips e Where e.iID = h.iID)");

                    sResult = sResult.Replace("(SELECT ', ' + Driver from (select distinct Driver from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.GroupDate = h.GroupDate) Driver FOR XML PATH(''))", "(select distinct listagg (t.Driver, ',' ) WITHIN GROUP (ORDER BY GroupDate) x FROM (select distinct AssetID, Driver, GroupDate from GlobalTmpTrips_GFI_GPS_TripHeader) t Where t.AssetID = h.AssetID and t.GroupDate = h.GroupDate)");
                    sResult = sResult.Replace("(SELECT ', ' + Driver from (select distinct Driver from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID) Driver FOR XML PATH('')) driver", "(select distinct listagg (t.Driver, ',' ) WITHIN GROUP (ORDER BY AssetID) x FROM (select distinct AssetID, Driver from #GFI_GPS_TripHeader) t Where t.AssetID = h.AssetID) driver");

                    sResult = sResult.Replace("iArrivalZone = (SELECT top 1 e.ZoneID FROM #ArrivalZoneTrips e Where e.iID = h.iID)", "iArrivalZone = (SELECT e.ZoneID FROM #ArrivalZoneTrips e Where e.iID = h.iID and rownum = 1)");
                    sResult = sResult.Replace("sArrivalZone = (SELECT top 1 e.Description FROM #ArrivalZoneTrips e Where e.iID = h.iID)", "sArrivalZone = (SELECT e.Description FROM #ArrivalZoneTrips e Where e.iID = h.iID and rownum = 1)");
                    sResult = sResult.Replace("iArrivalZoneColor = (SELECT top 1 e.Color FROM #ArrivalZoneTrips e Where e.iID = h.iID)", "iArrivalZoneColor = (SELECT e.Color FROM #ArrivalZoneTrips e Where e.iID = h.iID and rownum = 1)");

                    sResult = sResult.Replace("iDepartureZone = (SELECT top 1 e.ZoneID FROM #DepartureZoneTrips e Where e.iID = h.iID)", "iDepartureZone = (SELECT e.ZoneID FROM #DepartureZoneTrips e Where e.iID = h.iID and rownum = 1)");
                    sResult = sResult.Replace("sDepartureZone = (SELECT top 1 e.Description FROM #DepartureZoneTrips e Where e.iID = h.iID)", "sDepartureZone = (SELECT e.Description FROM #DepartureZoneTrips e Where e.iID = h.iID and rownum = 1)");
                    sResult = sResult.Replace("iDepartureZoneColor = (SELECT top 1 e.Color FROM #DepartureZoneTrips e Where e.iID = h.iID)", "iDepartureZoneColor = (SELECT e.Color FROM #DepartureZoneTrips e Where e.iID = h.iID and rownum = 1)");

                    sResult = sResult.Replace("(SELECT distinct ', ' + e.Description FROM GlobalTmp_ZoneExceptions e Where e.GPSDataID = h.GPSDataID FOR XML PATH(''))", "(select distinct listagg (e.Description, ',' ) WITHIN GROUP (ORDER BY dateTimeToLocal) x FROM GlobalTmp_ZoneExceptions e Where e.GPSDataID = h.GPSDataID)");
                    sResult = sResult.Replace("SELECT 1 totalTrips, 1 maxSpeed, 1 totalTripDistance, 1 totalTripTime, 1 totalStopTime, 1 totalIdlingTime", "SELECT 1 totalTrips, 1 maxSpeed, 1 totalTripDistance, 1 totalTripTime, 1 totalStopTime, 1 totalIdlingTime from dual");

                    sResult = sResult.Replace("select GroupDate date\r\n", "select GroupDate \r\n");
                    sResult = sResult.Replace("select * from #DailySummary order by registrationNo, date", "select * from #DailySummary order by registrationNo, GroupDate");
                    sResult = sResult.Replace("select * from #DailySummary", "select 'DailySummary' as TableName from dual; \r\n select * from #DailySummary");

                    //sResult = sResult.Replace(", CAST(DATEADD('second', sum(TripTime), '1900-01-01') AS TIME) totalTripTime", ", DATEADD('second', sum(TripTime), systimestamp)  totalTripTime");
                    //sResult = sResult.Replace(", CAST(DATEADD('second', sum(StopTime), '1900-01-01') AS TIME) totalStopTime", ", DATEADD('second', sum(StopTime), systimestamp)  totalStopTime");
                    //sResult = sResult.Replace(", CAST(DATEADD('second', sum(IdlingTime), '1900-01-01') AS TIME) totalIdlingTime", ", DATEADD('second', sum(IdlingTime), systimestamp)  totalIdlingTime");

                    sResult = sResult.Replace("#GFI_GPS_TripHeader) union SELECT * from GFI_FLT_Driver", "#GFI_GPS_TripHeader) union all SELECT * from GFI_FLT_Driver");
                    sResult = sResult.Replace("#GFI_GPS_TripHeader", "GlobalTmpTrips_GFI_GPS_TripHeader");
                    sResult = sResult.Replace("#GFI_GPS_GPSData", "GlobalTmpTrips_GFI_GPS_GPSData");
                    sResult = sResult.Replace("#Assets", "GlobalTmpTrips_Assets");
                    sResult = sResult.Replace("#FltAssets", "GlobalTmpTrips_FltAssets");
                    sResult = sResult.Replace("#ArrivalZoneTrips", "GlobalTmpTrips_ArrivalZoneTrips");
                    sResult = sResult.Replace("#DepartureZoneTrips", "GlobalTmpTrips_DepartureZoneTrips");
                    sResult = sResult.Replace("#FltDrivers", "GlobalTmpTrips_FltDrivers");
                    sResult = sResult.Replace("#Exceptions", "GlobalTmp_Exceptions");
                    sResult = sResult.Replace("#ExceptionDetails", "GlobalTmp_ExceptionDetails");
                    sResult = sResult.Replace("#DailySummary", "GlobalTmpTrips_DailySummary");

                    sResult = sResult.Replace("begin --then", "then");
                    sResult = sResult.Replace("else --if", "else if");
                    sResult = sResult.Replace("else --if", "else if");
                    sResult = sResult.Replace("end --end if;", "end if;");

                    start = sResult.IndexOf("--Oracle Starts Here");
                    end = sResult.IndexOf("--Oracle ends Here");
                    length = end - start;
                    sResult = sResult.Substring(start, length);

                    sResult = @"
                                truncate table GlobalTmpTrips_GFI_GPS_TripHeader;
                                truncate table GlobalTmpTrips_GFI_GPS_GPSData;
                                truncate table GlobalTmpTrips_Assets;
                                truncate table GlobalTmpTrips_FltAssets;

                                truncate table GlobalTmpZonesIDs;
                                truncate table GlobalTmpfTmpZone;
                                truncate table GlobalTmptTmpDistinctZone;
                                truncate table GlobalTmpZones;
                                truncate table GlobalTmpTrips_ArrivalZoneTrips;
                                truncate table GlobalTmpTrips_DepartureZoneTrips;
                                truncate table GlobalTmp_Exceptions;
                                truncate table GlobalTmp_ExceptionDetails;
                                truncate table GlobalTmpZonesIDs;
                                truncate table GlobalTmpfTmpZone;
                                truncate table GlobalTmptTmpDistinctZone;
                                truncate table GlobalTmpZones;
                                truncate table GlobalTmpTrips_DailySummary;
                                " + sResult;
                    break;

                case "\r\n--TripDetails":
                    start = sResult.IndexOf("--Oracle Starts Here");
                    end = sResult.IndexOf("--Oracle ends Here");
                    length = end - start;
                    sResult = sResult.Substring(start, length);

                    sResult = sResult.Replace("(SELECT ', ' + d.TypeID + ' : ' + d.TypeValue + ' : ' + d.UOM FROM GFI_GPS_GPSDataDetail d Where d.\"UID\" = g.\"UID\" FOR XML PATH(''))", " (select listagg (TypeID || ' ' || TypeValue || ' ' || UOM, '|' ) WITHIN GROUP (ORDER BY TypeID) TypeIDs FROM GFI_GPS_GPSDataDetail d Where d.\"UID\" = g.\"UID\") ");
                    sResult = sResult.Replace(", convert(varchar(300), NULL) as ruleName", ", '' as ruleName");
                    sResult = @"
                                truncate table GlobalTmpTripDetails_TripIDs;
                                truncate table GlobalTmpTripDetails_td;
                                " + sResult;

                    break;
            }
            return sResult;
        }
        static String GeomData(String sGeomData, String id)
        {
            String OracleGeomData = @"
MERGE INTO GFI_FLT_ZoneHeader z
USING (
        SELECT z.ROWID row_id, T1.geom
            from GFI_FLT_ZoneHeader z
            inner join
            (
                select z.*,
            
                            SDO_GEOMETRY
                            (
                                2003,  -- two-dimensional polygon
                                4326,
                                NULL,
                                SDO_ELEM_INFO_ARRAY(1,1003,1), -- one rectangle (1003 = exterior)
                                SDO_ORDINATE_ARRAY(" + sGeomData + @")
                            ) as geom
                from GFI_FLT_ZoneHeader z
            ) T1 on z.ZoneID = T1.ZoneID
            inner join
            (
                SELECT d1.*
                    FROM GFI_FLT_ZoneDetail d1
                LEFT JOIN GFI_FLT_ZoneDetail d2 ON (d1.ZoneID = d2.ZoneID AND d1.CordOrder > d2.CordOrder)
                    WHERE d2.ZoneID IS NULL
            )T2 on z.ZoneID = T2.ZoneID
            inner join
            (
                SELECT d1.*
                    FROM GFI_FLT_ZoneDetail d1
                LEFT JOIN GFI_FLT_ZoneDetail d2 ON (d1.ZoneID = d2.ZoneID AND d1.CordOrder < d2.CordOrder)
                    WHERE d2.ZoneID IS NULL
            )T3 on z.ZoneID = T3.ZoneID and T2.Longitude = T3.Longitude and T2.Latitude = T3.Latitude
            where z.ZoneID  = " + id + @"
        ) src ON (z.ROWID = src.row_id)
    WHEN MATCHED THEN UPDATE set GeomData = SDO_UTIL.RECTIFY_GEOMETRY(src.geom, 0.005)";
            return OracleGeomData;
        }
        static String OracleTHReplace(int i)
        {
            String s = String.Empty;
            switch (i)
            {
                case 0:
                    s = @"
MERGE INTO GlobalTmpTrips_GFI_GPS_TripHeader T1
USING 
(
    SELECT T1.ROWID row_id, T2.Asset
        , T2.TimeZone
        , T1.IdlingTime
        , T1.StopTime
        , T1.TripTime
        , T1.OverSpeed1Time
        , T1.OverSpeed2Time
    FROM GlobalTmpTrips_GFI_GPS_TripHeader T1
    INNER JOIN GlobalTmpTrips_Assets T2 ON T2.iAssetID = T1.AssetID
) src
ON (T1.ROWID = src.row_id)
WHEN MATCHED THEN UPDATE SET 
    Asset = src.Asset
    , TimeZone = src.TimeZone
    , dtStart = dtStart + (interval '1' MINUTE * src.TimeZone)
    , dtEnd = dtEnd + (interval '1' MINUTE * src.TimeZone)
    , GroupDate = to_char(DATEADD('mi', src.TimeZone, dtStart),'DD/MM/YYYY') --dtStart + (interval '1' MINUTE * src.TimeZone)
    , tsIdlingTime = to_char (trunc(sysdate) + numtodsinterval(T1.IdlingTime, 'second'),'hh24:mi:ss')
    , tsStopTime   = to_char (trunc(sysdate) + numtodsinterval(T1.StopTime, 'second'),'hh24:mi:ss') 
    , tsTripTime   = to_char (trunc(sysdate) + numtodsinterval(T1.TripTime, 'second'),'hh24:mi:ss') 
    , tsOverSpeed1Time   = to_char (trunc(sysdate) + numtodsinterval(T1.OverSpeed1Time, 'second'),'hh24:mi:ss') 
    , tsOverSpeed2Time   = to_char (trunc(sysdate) + numtodsinterval(T1.OverSpeed2Time, 'second'),'hh24:mi:ss');

";
                    break;

                case 1:
                    s = @"
MERGE INTO GlobalTmpTrips_GFI_GPS_TripHeader t
USING 
(
    with cte as
    (
        select h.iID, RuleID, GroupID
            from GlobalTmp_ExceptionDetails e
        inner join GFI_GPS_TripDetail d on d.GPSDataUID = e.""UID""
        inner join GlobalTmpTrips_GFI_GPS_TripHeader h on h.iID = d.HeaderiID
            group by h.iID, RuleID, GroupID
    ),
    ctf as
    (
        select iID, count(iID)cnt from cte group by iID
    )
    select t.RowID row_id, ctf.cnt
    from GlobalTmpTrips_GFI_GPS_TripHeader t
        inner join ctf on t.iID = ctf.iID

) src
ON(t.ROWID = src.row_id)
WHEN MATCHED THEN UPDATE SET exceptionsCnt = src.cnt;
 ";
                    break;
            }

            return s;
        }

        static String GetAssetsStatus(String sql)
        {
            int start = sql.IndexOf("--Oracle Starts Here");
            int end = sql.IndexOf("--Oracle ends Here\r\n");
            int length = end - start;
            String sAssets = sql.Substring(start, length);
            sAssets = sAssets.Replace("@Assets", "into GlobalTmpAssetStatusAssets");
            sAssets = sAssets.Replace(")\r\n", ");\r\n");
            sAssets = sAssets.Replace("--Oracle Starts Here", String.Empty);
            sAssets = sAssets.Trim();
            String s = @"
--GetAssetsStatus
begin
    BEGIN
        EXECUTE IMMEDIATE 'truncate table GlobalTmpAssetStatusAssets';
        EXECUTE IMMEDIATE 'truncate table GlobalTmpAssetStatusTable';
    EXCEPTION
        WHEN OTHERS THEN
            DBMS_OUTPUT.put_line(SQLERRM);
    END; 
    --Oracle Starts Here
    --insert into GlobalTmpAssetStatusAssets (iAssetID) values (1);
    
    insert into GlobalTmpAssetStatusTable
        select a.AssetID, d.DeviceID, g.GMDescription, g.GMID, null, null, null, null, null, null
        from GFI_FLT_Asset a
            inner join GlobalTmpAssetStatusAssets t on a.AssetID = t.iAssetID
            inner join GFI_FLT_AssetDeviceMap d on a.AssetID = d.AssetID
            inner join GFI_SYS_GroupMatrixAsset gma on a.AssetID = gma.iID
            inner join GFI_SYS_GroupMatrix g on gma.GMID = g.GMID;
    
    insert into GlobalTmpAssetStatusTable(AssetID, DeviceID, GMDescription)
        select a.AssetID, d.DeviceID, '*** No Matrix ***'
            from GFI_FLT_Asset a
                left outer join GFI_FLT_AssetDeviceMap d on a.AssetID = d.AssetID
            where a.AssetID not in (select iID from GFI_SYS_GroupMatrixAsset);
    
    declare v_AssetID INT;
      v_GMID INT;
      v_AllClients number;
    Cursor ProcessCursor is 
        select AssetID, GMID from GlobalTmpAssetStatusTable;
    
    BEGIN
    select GMID into v_AllClients from GFI_SYS_GroupMatrix where GMDescription = '### All Clients ###';
    OPEN ProcessCursor;
        LOOP 
            --DBMS_OUTPUT.put_line(v_AllClients);
            fetch ProcessCursor into v_AssetID, v_GMID;
            EXIT WHEN ProcessCursor%notfound;
            MERGE INTO GlobalTmpAssetStatusTable T1
            USING
                (
                    with cte1(GMID, ParentGMID, GMDescription, lvl)  as
                    (
                        select T.GMID, T.ParentGMID, T.GMDescription, 1 as lvl
                            from GFI_SYS_GroupMatrix T
                        where T.GMID = v_GMID
                        union all
                            select T.GMID, T.ParentGMID, T.GMDescription, C.lvl+1
                                from GFI_SYS_GroupMatrix T
                            inner join cte1 C on T.GMID = C.ParentGMID
                    ) select cte1.* from cte1 where ParentGMID = v_AllClients
                ) src ON (ParentGMID = v_AllClients)
            WHEN MATCHED THEN UPDATE set GMDescription = src.GMDescription
                where AssetID = v_AssetID; 
                            ---and (select GMDescription from cte1 where ParentGMID = v_AllClients) is not null;    
        END LOOP;
    END;
        
    MERGE INTO GlobalTmpAssetStatusTable T1
    USING
        (
            SELECT T1.ROWID row_id, T2.DateTimeGPS_UTC, T2.Speed, T2.EngineOn
                , (select listagg (d.TypeID || ' ' || d.TypeValue || ' ' || d.UOM, '|' ) WITHIN GROUP (ORDER BY d.TypeID) TypeIDs FROM GFI_GPS_LiveDataDetail d Where d.""UID"" = T2.""UID"" ) as details
            from GlobalTmpAssetStatusTable T1
                left outer join GFI_GPS_LiveData T2 on T1.AssetID = T2.AssetID
            where T2.DateTimeGPS_UTC = (SELECT MAX(DateTimeGPS_UTC) FROM GFI_GPS_LiveData a WHERE a.AssetID = T2.AssetID)
        ) src ON (T1.ROWID = src.row_id)
    WHEN MATCHED THEN UPDATE set dtLastRptd = src.DateTimeGPS_UTC,  speed = src.Speed,  EngineOn = src.EngineOn, details = src.details;
    
    update GlobalTmpAssetStatusTable set color = '#F63B5D', details = '*** Never Reported ***' where dtLastRptd is null;
    
    --update GlobalTmpAssetStatusTable set color = '#D8941D', details = '*** Not Recent *** ' || details where dtLastRptd <= DATEADD(day, systimestamp + (interval '-0' MINUTE), -1);
    
    update GlobalTmpAssetStatusTable set color = '#B7F737' where color is null;
    
    update GlobalTmpAssetStatusTable set color = '#1D33D8' where GMDescription = '*** No Matrix ***';
    
    update GlobalTmpAssetStatusTable set daysNoReport = (TRUNC(systimestamp)) - TRUNC(dtLastRptd);
    
 EXCEPTION
    WHEN OTHERS THEN
        Declare v_errMsg NVARCHAR2 (200) := SQLERRM; 
                v_err_code NVARCHAR2 (50);
        BEGIN
            v_errMsg := SUBSTR(SQLERRM, 1, 200);
            v_err_code := SQLCODE; --SUBSTR(SQLCODE, 1, 50);
            ROLLBACK;              
            BEGIN                 
                BEGIN
                    INSERT INTO GFI_GPS_ProcessErrors (sError, sOrigine, sFieldValue)
                        VALUES (v_errMsg, 'AssetStatus', v_err_code );             
                END;
            EXCEPTION
                WHEN OTHERS THEN dbms_output.put_line(v_errMsg);
            END;   
        END;
END;

--begin
--    select * from GlobalTmpAssetStatusTable order by AssetID;
--end;
--AlreadyOracleStatement";

            s = s.Replace("--insert into GlobalTmpAssetStatusAssets (iAssetID) values (1);", sAssets);

            return s;
        }
    }
}
