﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBConn.Common
{
    public class ConnName
    {
        public static String GetsConnStr(String sConnStr)
        {
            //connection = ConfigurationManager.ConnectionStrings["Test"].ConnectionString;
            //connection = ConfigurationManager.ConnectionStrings["Test"].ProviderName;
            //connection = ConfigurationManager.AppSettings[sConnStr].ToString();

            String str = String.Empty;
            switch (sConnStr)
            {
                case "ConnStr":
                    str = ConfigurationManager.ConnectionStrings[sConnStr].ConnectionString;
                    break;

                case "ConnStrTransferred":
                    str = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
                    String ServerName, UserID, Password, strDBName = String.Empty;

                    char[] chSep = { ';' };
                    String[] strIndivParam = str.Split(chSep);

                    for (int i = 0; i < strIndivParam.Length; i++)
                    {
                        int intFound = -1;
                        if ((intFound = strIndivParam[i].IndexOf("Data Source=")) != -1)
                            ServerName = strIndivParam[i].Substring(intFound + "Data Source=".Length, strIndivParam[i].Length - "Data Source=".Length);

                        if ((intFound = strIndivParam[i].IndexOf("User ID=")) != -1)
                            UserID = strIndivParam[i].Substring(intFound + "User ID=".Length, strIndivParam[i].Length - "User ID=".Length);

                        if ((intFound = strIndivParam[i].IndexOf("Pwd=")) != -1)
                            Password = strIndivParam[i].Substring(intFound + "Pwd=".Length, strIndivParam[i].Length - "Pwd=".Length);

                        if ((intFound = strIndivParam[i].IndexOf("Initial Catalog=")) != -1)
                            strDBName = strIndivParam[i].Substring(intFound + "Initial Catalog=".Length, strIndivParam[i].Length - "Initial Catalog=".Length);
                    }

                    str = str.Replace(strDBName, strDBName + "Transferred");
                    break;

                case "DB1":
                    //Demo
                    str = ConfigurationManager.ConnectionStrings["DB1"].ConnectionString.Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB2":
                    //Mercury
                    str = ConfigurationManager.ConnectionStrings["DB2"].ConnectionString.Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB3":
                    //Venus
                    str = ConfigurationManager.ConnectionStrings["DB3"].ConnectionString.Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB4":
                    //Earth
                    str = ConfigurationManager.ConnectionStrings["DB4"].ConnectionString.Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB5":
                    //Mars
                    str = ConfigurationManager.ConnectionStrings["DB5"].ConnectionString.Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB6":
                    //Jupiter
                    str = ConfigurationManager.ConnectionStrings["DB6"].ConnectionString.Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB7":
                    //Saturn
                    str = ConfigurationManager.ConnectionStrings["DB7"].ConnectionString.Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB8":
                    //Uranus
                    str = ConfigurationManager.ConnectionStrings["DB8"].ConnectionString.Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB9":
                    //Neptune
                    str = ConfigurationManager.ConnectionStrings["DB9"].ConnectionString.Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB10":
                    //Pluto
                    str = ConfigurationManager.ConnectionStrings["DB10"].ConnectionString.Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB11":
                    //Baby
                    str = ConfigurationManager.ConnectionStrings["DB11"].ConnectionString.Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB12":
                    //Kids
                    str = ConfigurationManager.ConnectionStrings["DB12"].ConnectionString.Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB13":
                    //Sun
                    str = ConfigurationManager.ConnectionStrings["DB13"].ConnectionString.Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB14":
                    //Moon
                    str = ConfigurationManager.ConnectionStrings["DB14"].ConnectionString.Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB15":
                    //BlackWidow
                    str = ConfigurationManager.ConnectionStrings["DB15"].ConnectionString.Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

             
                case "DB16":
                    //Sirius
                    str = ConfigurationManager.ConnectionStrings["DB16"].ConnectionString.Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;


                case "DB17":
                    //Vega
                    str = ConfigurationManager.ConnectionStrings["DB17"].ConnectionString.Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB18":
                    //Pega
                    str = ConfigurationManager.ConnectionStrings["DB18"].ConnectionString.Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB19":
                    //Andromeda
                    str = ConfigurationManager.ConnectionStrings["DB19"].ConnectionString.Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB20":
                    //Centaurus
                    str = ConfigurationManager.ConnectionStrings["DB20"].ConnectionString.Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB21":
                    //Centaurus
                    str = ConfigurationManager.ConnectionStrings["DB21"].ConnectionString.Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB22":
                    //Cosmos
                    str = ConfigurationManager.ConnectionStrings["DB22"].ConnectionString.Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB23":
                    //Epsilon
                    str = ConfigurationManager.ConnectionStrings["DB23"].ConnectionString.Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                case "DB99":
                    //RezaHP , LocalHost
                    str = ConfigurationManager.ConnectionStrings["DB99"].ConnectionString.Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;

                default:
                    str = ConfigurationManager.ConnectionStrings[sConnStr].ConnectionString.Replace("User ID=?;Pwd=?", "User ID=CoreUser;Pwd=ITManager08");
                    break;
            }
            return str;
        }
        public static String GetDBName(String sConnStr)
        {
            String str, ServerName, UserID=String.Empty, Password, strDBName = String.Empty;
            str = GetsConnStr(sConnStr);

            char[] chSep = { ';' };
            String[] strIndivParam = str.Split(chSep);

            for (int i = 0; i < strIndivParam.Length; i++)
            {
                int intFound = -1;
                if ((intFound = strIndivParam[i].IndexOf("Data Source=")) != -1)
                    ServerName = strIndivParam[i].Substring(intFound + "Data Source=".Length, strIndivParam[i].Length - "Data Source=".Length);

                if ((intFound = strIndivParam[i].IndexOf("User ID=")) != -1)
                    UserID = strIndivParam[i].Substring(intFound + "User ID=".Length, strIndivParam[i].Length - "User ID=".Length);

                if ((intFound = strIndivParam[i].IndexOf("Pwd=")) != -1)
                    Password = strIndivParam[i].Substring(intFound + "Pwd=".Length, strIndivParam[i].Length - "Pwd=".Length);

                if ((intFound = strIndivParam[i].IndexOf("Initial Catalog=")) != -1)
                    strDBName = strIndivParam[i].Substring(intFound + "Initial Catalog=".Length, strIndivParam[i].Length - "Initial Catalog=".Length);
            }

            if(String.IsNullOrEmpty(strDBName)) //PostgreSQL
                for (int i = 0; i < strIndivParam.Length; i++)
                {
                    int intFound = -1;
                    if ((intFound = strIndivParam[i].ToLower().IndexOf("server=")) != -1)
                        ServerName = strIndivParam[i].ToLower().Substring(intFound + "server=".Length, strIndivParam[i].ToLower().Length - "server=".Length);

                    if ((intFound = strIndivParam[i].IndexOf("User ID=")) != -1)
                        UserID = strIndivParam[i].Substring(intFound + "User ID=".Length, strIndivParam[i].Length - "User ID=".Length);

                    if ((intFound = strIndivParam[i].IndexOf("Pwd=")) != -1)
                        Password = strIndivParam[i].Substring(intFound + "Pwd=".Length, strIndivParam[i].Length - "Pwd=".Length);

                    if ((intFound = strIndivParam[i].ToLower().IndexOf("database=")) != -1)
                        strDBName = strIndivParam[i].Substring(intFound + "database=".Length, strIndivParam[i].Length - "database=".Length);
                }

            return strDBName;
        }
        public static String GetServerName(String sConnStr)
        {
            String str, ServerName = String.Empty, UserID, Password, strDBName = String.Empty;
            if (sConnStr == "Geotab")
            {
                str = "Data Source=";
                str += ConfigurationManager.AppSettings["GeotabServer"].ToString();
                str += ";Initial Catalog=";
                str += ConfigurationManager.AppSettings["Database"].ToString();
                str += ";User ID=geotabuser;Pwd=vircom43;Connect Timeout=200;";
            }
            else if (sConnStr == "ConnStrTransferred")
                str = ConfigurationManager.AppSettings["ConnStr"].ToString();
            else
                str = ConfigurationManager.AppSettings[sConnStr].ToString();

            char[] chSep = { ';' };
            String[] strIndivParam = str.Split(chSep);

            for (int i = 0; i < strIndivParam.Length; i++)
            {
                int intFound = -1;
                if ((intFound = strIndivParam[i].IndexOf("Data Source=")) != -1)
                    ServerName = strIndivParam[i].Substring(intFound + "Data Source=".Length, strIndivParam[i].Length - "Data Source=".Length);

                if ((intFound = strIndivParam[i].IndexOf("User ID=")) != -1)
                    UserID = strIndivParam[i].Substring(intFound + "User ID=".Length, strIndivParam[i].Length - "User ID=".Length);

                if ((intFound = strIndivParam[i].IndexOf("Pwd=")) != -1)
                    Password = strIndivParam[i].Substring(intFound + "Pwd=".Length, strIndivParam[i].Length - "Pwd=".Length);

                if ((intFound = strIndivParam[i].IndexOf("Initial Catalog=")) != -1)
                    strDBName = strIndivParam[i].Substring(intFound + "Initial Catalog=".Length, strIndivParam[i].Length - "Initial Catalog=".Length);
            }

            return ServerName;
        }
        public static String GetOracleDBName(String sConnStr)
        {
            String str, ServerName, UserID = String.Empty, Password, strDBName = String.Empty;
            str = GetsConnStr(sConnStr);

            char[] chSep = { ';' };
            String[] strIndivParam = str.Split(chSep);

            for (int i = 0; i < strIndivParam.Length; i++)
            {
                int intFound = -1;
                if ((intFound = strIndivParam[i].IndexOf("Data Source=")) != -1)
                    ServerName = strIndivParam[i].Substring(intFound + "Data Source=".Length, strIndivParam[i].Length - "Data Source=".Length);

                if ((intFound = strIndivParam[i].IndexOf("User ID=")) != -1)
                    UserID = strIndivParam[i].Substring(intFound + "User ID=".Length, strIndivParam[i].Length - "User ID=".Length);

                if ((intFound = strIndivParam[i].IndexOf("Pwd=")) != -1)
                    Password = strIndivParam[i].Substring(intFound + "Pwd=".Length, strIndivParam[i].Length - "Pwd=".Length);

                if ((intFound = strIndivParam[i].IndexOf("Initial Catalog=")) != -1)
                    strDBName = strIndivParam[i].Substring(intFound + "Initial Catalog=".Length, strIndivParam[i].Length - "Initial Catalog=".Length);
            }

            return UserID;
        }
        public static String GetPostGreDBName(String sConnStr)
        {
            String str, ServerName, UserID = String.Empty, Password, strDBName = String.Empty;
            str = GetsConnStr(sConnStr);

            char[] chSep = { ';' };
            String[] strIndivParam = str.Split(chSep);

            for (int i = 0; i < strIndivParam.Length; i++)
            {
                int intFound = -1;
                if ((intFound = strIndivParam[i].IndexOf("Server=")) != -1)
                    ServerName = strIndivParam[i].Substring(intFound + "Server=".Length, strIndivParam[i].Length - "Server=".Length);

                if ((intFound = strIndivParam[i].IndexOf("User ID=")) != -1)
                    UserID = strIndivParam[i].Substring(intFound + "User ID=".Length, strIndivParam[i].Length - "User ID=".Length);

                if ((intFound = strIndivParam[i].IndexOf("Pwd=")) != -1)
                    Password = strIndivParam[i].Substring(intFound + "Pwd=".Length, strIndivParam[i].Length - "Pwd=".Length);

                if ((intFound = strIndivParam[i].IndexOf("Database=")) != -1)
                    strDBName = strIndivParam[i].Substring(intFound + "Database=".Length, strIndivParam[i].Length - "Database=".Length);
            }

            return strDBName;
        }
        public static String GetPostGreSQLpgpass(String sConnStr)
        {
            //localhost:5432:mhldb:postgres:Manager08
            //Server=localhost;Port=5432;Database=tripsdb;User Id=postgres;Password=Manager08
            String str, ServerName = String.Empty, UserID = String.Empty, Password = String.Empty, strDBName = String.Empty, Port = String.Empty;
            str = GetsConnStr(sConnStr);

            char[] chSep = { ';' };
            String[] strIndivParam = str.Split(chSep);

            for (int i = 0; i < strIndivParam.Length; i++)
            {
                int intFound = -1;
                if ((intFound = strIndivParam[i].ToLower().IndexOf("server=")) != -1)
                    ServerName = strIndivParam[i].Substring(intFound + "Server=".Length, strIndivParam[i].Length - "Server=".Length);

                if ((intFound = strIndivParam[i].ToLower().IndexOf("user id=")) != -1)
                    UserID = strIndivParam[i].Substring(intFound + "User ID=".Length, strIndivParam[i].Length - "User ID=".Length);

                if ((intFound = strIndivParam[i].ToLower().IndexOf("password=")) != -1)
                    Password = strIndivParam[i].Substring(intFound + "Password=".Length, strIndivParam[i].Length - "Password=".Length);

                if ((intFound = strIndivParam[i].ToLower().IndexOf("database=")) != -1)
                    strDBName = strIndivParam[i].Substring(intFound + "Database=".Length, strIndivParam[i].Length - "Database=".Length);

                if ((intFound = strIndivParam[i].ToLower().IndexOf("port=")) != -1)
                    Port = strIndivParam[i].Substring(intFound + "Port=".Length, strIndivParam[i].Length - "Port=".Length);
            }

            if (String.IsNullOrEmpty(ServerName) || String.IsNullOrEmpty(Port) || String.IsNullOrEmpty(strDBName) || String.IsNullOrEmpty(UserID) || String.IsNullOrEmpty(Password))
                return String.Empty;

            return ServerName + ":" + Port + ":" + strDBName + ":" + UserID + ":" + Password;
        }
    }
}
