﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.IO.Compression;

namespace DBConn.Common
{
    public class Crypt
    {
       
        private static String key = "NaveoEng";
        public static String Encrypt(String PlainText)//, String key)
        {
            TripleDES des = CreateDES(key);
            ICryptoTransform ct = des.CreateEncryptor();

            Byte[] input = Encoding.Unicode.GetBytes(PlainText.Trim());
            Byte[] buffer = ct.TransformFinalBlock(input, 0, input.Length);

            return Convert.ToBase64String(buffer);

        }
        public static String Decrypt(String CypherText)//, String key)
        {
            Byte[] b = Convert.FromBase64String(CypherText.Trim());

            TripleDES des = CreateDES(key);
            ICryptoTransform ct = des.CreateDecryptor();
            Byte[] output = ct.TransformFinalBlock(b, 0, b.Length);

            String strCheck = Encoding.Unicode.GetString(output);
            return Encoding.Unicode.GetString(output);

        }
        static TripleDES CreateDES(String key)
        {
            key += "Manager08";
            MD5 md5 = new MD5CryptoServiceProvider();
            TripleDES des = new TripleDESCryptoServiceProvider();
            des.Key = md5.ComputeHash(Encoding.Unicode.GetBytes(key));
            des.IV = new Byte[des.BlockSize / 8];
            return des;
        }
    }
}
