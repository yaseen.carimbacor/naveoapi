﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

using System.Data.OleDb;
using System.Data;
using System.IO;
using System.Data.Odbc;
using Microsoft.VisualBasic;
using System.Windows.Forms;

using System.Xml;
using System.IO.Compression;
using System.Configuration;

namespace DBConn.Common
{
    public class DataTransport
    {
        public static byte[] Compress(byte[] data)
        {
            //create a new MemoryStream for holding and
            //returning the compressed ViewState
            MemoryStream output = new MemoryStream();
            //create a new GZipStream object for compressing
            //the ViewState
            GZipStream gzip = new GZipStream(output, CompressionMode.Compress, true);
            //write the compressed bytes to the underlying stream
            gzip.Write(data, 0, data.Length);
            //close the object
            gzip.Close();
            //convert the MemoryStream to an array and return
            //it to the calling method
            return output.ToArray();
        }
        public static byte[] Decompress(byte[] data)
        {
            //create a MemoryStream for holding the incoming data
            MemoryStream input = new MemoryStream();
            //write the incoming bytes to the MemoryStream
            input.Write(data, 0, data.Length);
            //set our position to the start of the Stream
            input.Position = 0;
            //create an instance of the GZipStream to decompress
            //the incoming byte array (the compressed ViewState)
            GZipStream gzip = new GZipStream(input, CompressionMode.Decompress, true);
            //create a new MemoryStream for holding
            //the output
            MemoryStream output = new MemoryStream();
            //create a byte array
            byte[] buff = new byte[64];
            int read = -1;
            //read the decompressed ViewState into
            //our byte array, set that value to our
            //read variable (int data type)
            read = gzip.Read(buff, 0, buff.Length);
            //make sure we have something to read
            while (read > 0)
            {
                //write the decompressed bytes to our
                //out going MemoryStream
                output.Write(buff, 0, read);
                //get the rest of the buffer
                read = gzip.Read(buff, 0, buff.Length);
            }
            gzip.Close();
            //return our out going MemoryStream
            //in an array
            return output.ToArray();
        }
        public static String DecompressToDisk(byte[] data)
        {
            String sPath = System.IO.Path.GetTempPath() + "Naveo\\";
            if (!(System.IO.Directory.Exists(sPath)))
                System.IO.Directory.CreateDirectory(sPath);
            sPath += Path.GetRandomFileName();

            //create a MemoryStream for holding the incoming data
            MemoryStream input = new MemoryStream();
            //write the incoming bytes to the MemoryStream
            input.Write(data, 0, data.Length);
            //set our position to the start of the Stream
            input.Position = 0;
            //create an instance of the GZipStream to decompress
            //the incoming byte array (the compressed ViewState)
            GZipStream gzip = new GZipStream(input, CompressionMode.Decompress, true);
            //create a new MemoryStream for holding
            //the output
            FileStream output = new FileStream(sPath, FileMode.Create);
            //create a byte array
            byte[] buff = new byte[64];
            int read = -1;
            //read the decompressed ViewState into
            //our byte array, set that value to our
            //read variable (int data type)
            read = gzip.Read(buff, 0, buff.Length);
            //make sure we have something to read
            while (read > 0)
            {
                //write the decompressed bytes to our
                //out going MemoryStream
                output.Write(buff, 0, read);
                //get the rest of the buffer
                read = gzip.Read(buff, 0, buff.Length);
            }
            gzip.Close();
            output.Close();
            //return our out going MemoryStream
            //in an array

            return sPath;
        }

        public static string ConvertDataSetToXML(DataSet xmlDS)
        {
            MemoryStream stream = null;
            XmlTextWriter writer = null;
            try
            {
                stream = new MemoryStream();
                // Load the XmlTextReader from the stream
                writer = new XmlTextWriter(stream, Encoding.Unicode);
                // Write to the file with the WriteXml method.
                xmlDS.WriteXml(writer, XmlWriteMode.WriteSchema);
                int count = (int)stream.Length;
                byte[] arr = new byte[count];
                stream.Seek(0, SeekOrigin.Begin);
                stream.Read(arr, 0, count);
                UnicodeEncoding utf = new UnicodeEncoding();
                return utf.GetString(arr).Trim();
            }
            catch (Exception ex)
            {
                ex.ToString();
                return String.Empty;
            }
            finally
            {
                if (writer != null) writer.Close();
            }
        }
        public static byte[] CompressDS(DataSet ds)
        {
            RemoveTimezoneForDataSet(ds);
            return Compress(Encoding.Unicode.GetBytes(ConvertDataSetToXML(ds)));
        }
        public static DataSet DeCompressByteToDataSet(byte[] dsbytes)
        {
            try
            {
                DataSet ds = new DataSet();

                if (dsbytes.LongLength > 800000)
                {
                    String pathtofile = DecompressToDisk(dsbytes);
                    FileStream filestream = File.OpenRead(pathtofile);
                    BufferedStream buffered = new BufferedStream(filestream);
                    ds.ReadXml(buffered);
                    return ds;
                }

                byte[] decompressed_dsBytes = Decompress(dsbytes);
                //ds.ReadXml(new StringReader(Encoding.Unicode.GetString(decompressed_dsBytes).Substring(1)));
                UnicodeEncoding utf = new UnicodeEncoding();
                String g = utf.GetString(decompressed_dsBytes).Trim();
                //g = g.Replace("+04:00", "");
                if (decompressed_dsBytes[0] == 255)
                    ds.ReadXml(new StringReader(g.Substring(1)));
                else
                    ds.ReadXml(new StringReader(g));

                return ds;
            }
            catch 
            {
                return null;
            }
        }
        public static void RemoveTimezoneForDataSet(DataSet ds)
        {
            foreach (DataTable dt in ds.Tables)
            {
                foreach (DataColumn dc in dt.Columns)
                {
                    if (dc.DataType == typeof(DateTime))
                    {
                        dc.DateTimeMode = DataSetDateTime.Unspecified;
                    }
                }
            }
        }

        public static string ConvertDTToXML(DataTable xmlDT)
        {
            MemoryStream stream = null;
            XmlTextWriter writer = null;
            try
            {
                if (xmlDT.TableName == String.Empty)
                    xmlDT.TableName = "Table1";
                stream = new MemoryStream();
                // Load the XmlTextReader from the stream
                writer = new XmlTextWriter(stream, Encoding.Unicode);
                // Write to the file with the WriteXml method.
                xmlDT.WriteXml(writer, XmlWriteMode.WriteSchema);
                int count = (int)stream.Length;
                byte[] arr = new byte[count];
                stream.Seek(0, SeekOrigin.Begin);
                stream.Read(arr, 0, count);
                UnicodeEncoding utf = new UnicodeEncoding();
                return utf.GetString(arr).Trim();
            }
            catch (Exception ex)
            {
                ex.ToString();
                return String.Empty;
            }
            finally
            {
                if (writer != null) writer.Close();
            }
        }
        public static byte[] CompressDT(DataTable dt)
        {
            return Compress(Encoding.Unicode.GetBytes(ConvertDTToXML(dt)));
        }
        public static DataTable DeCompressByteToDT(byte[] dtbytes)
        {
            try
            {
                DataTable dt = new DataTable();
                byte[] decompressed_dtBytes = Decompress(dtbytes);
                //ds.ReadXml(new StringReader(Encoding.Unicode.GetString(decompressed_dsBytes).Substring(1)));
                UnicodeEncoding utf = new UnicodeEncoding();
                String g = utf.GetString(decompressed_dtBytes).Trim();
                if (decompressed_dtBytes[0] == 255)
                    dt.ReadXml(new StringReader(g.Substring(1)));
                else
                    dt.ReadXml(new StringReader(g));

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
                //return null;
            }
        }

        public static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }
        public static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public static Boolean IsAddressAvailable(String address)
        {
            try
            {
                System.Net.WebClient client = new System.Net.WebClient();
                client.DownloadData(address);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
