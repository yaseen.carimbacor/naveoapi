﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBConn.Common
{
    public static class GenUtils
    {
        public static string ReplaceIgnoreCase(this string source, string oldVale, string newVale)
        {
            if (String.IsNullOrEmpty(source) || String.IsNullOrEmpty(oldVale))
                return source;

            StringBuilder stringBuilder = new StringBuilder();
            string result = source;

            int index = result.IndexOf(oldVale, StringComparison.InvariantCultureIgnoreCase);

            while (index >= 0)
            {
                string substr = result.Substring(0, index);
                substr = substr + newVale;
                result = result.Remove(0, index);
                result = result.Remove(0, oldVale.Length);

                stringBuilder.Append(substr);

                index = result.IndexOf(oldVale, StringComparison.InvariantCultureIgnoreCase);
            }

            if (result.Length > 0)
            {
                stringBuilder.Append(result);
            }

            return stringBuilder.ToString();
        }
    }
}
