﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DBConn.Common
{
    class SqlToPostgreSQL
    {
        static String sAfterDoCmd = String.Empty;
        public static String FormatPostgreSQL(String sql)
        {
            if (sql.Contains("--Skipping in PostgreSQL"))
                return "select 1";
            sql = sql.Replace("--postGreSQL RECURSIVE", "RECURSIVE");
            sql = sql.Replace("[", String.Empty);
            sql = sql.Replace("]", String.Empty);
            sql = sql.Replace("varchar(max)", "varchar(1024)");
            sql = sql.ReplaceIgnoreCase("varchar(max)", "varchar(1024)");
            sql = sql.ReplaceIgnoreCase("NONCLUSTERED", String.Empty);
            sql = sql.ReplaceIgnoreCase("getdate()", "now()");
            sql = sql.ReplaceIgnoreCase("GetUTCDate()", "now()");
            sql = sql.ReplaceIgnoreCase("SET ANSI_WARNINGS ON", String.Empty);
            sql = sql.ReplaceIgnoreCase("SET ANSI_WARNINGS OFF", String.Empty);

            sql = sql.ReplaceIgnoreCase("insert ", "insert into ");
            sql = sql.ReplaceIgnoreCase("insert into into ", "insert into ");
            sql = sql.ReplaceIgnoreCase("COLLATE DATABASE_DEFAULT", String.Empty);

            sql = sql.ReplaceIgnoreCase("Geometry::STPolyFromText", "ST_GeomFromText");

            if (!sql.EndsWith("\r\n --PostgreSQLGMS"))
                sql = Regex.Replace(sql, @"\bdbo.\b", String.Empty, RegexOptions.IgnoreCase);
            sql = Regex.Replace(sql, @"\bnvarchar\b", "varchar", RegexOptions.IgnoreCase);
            sql = Regex.Replace(sql, @"\bASC\b", String.Empty, RegexOptions.IgnoreCase);
            sql = Regex.Replace(sql, @"\bCLUSTERED\b", String.Empty, RegexOptions.IgnoreCase);
            sql = Regex.Replace(sql, @"\btinyint\b", "int", RegexOptions.IgnoreCase);

            sql = Regex.Replace(sql, @"\bDatetime\b", "TIMESTAMP", RegexOptions.IgnoreCase);

            sql = sql.ReplaceIgnoreCase("GeomData.STSrid = 4326", "GeomData = ST_SetSRID(GeomData, 4326)");
            sql = sql.ReplaceIgnoreCase("GeomData = GeomData.MakeValid()", "GeomData = ST_MakeValid(GeomData)");
            sql = sql.ReplaceIgnoreCase("GeomData.STIsValid() = 0", "ST_IsValid(GeomData)");
            sql = sql.ReplaceIgnoreCase("Geometry::STPointFromText('POINT(' + CONVERT(varchar, fEndLon) + ' ' + CONVERT(varchar, fEndLat) + ')', 4326) ", "ST_GeomFromText('POINT(' || fEndLon || ' ' || fEndLat || ')', 4326) ");
            sql = sql.ReplaceIgnoreCase("z.GeomData.STIntersects(pointFr) = 1", "ST_Intersects(z.GeomData, pointFr)");
            sql = sql.ReplaceIgnoreCase("Geometry::STPointFromText('POINT(' + CONVERT(varchar, fStartLon) + ' ' + CONVERT(varchar, fStartLat) + ')', 4326) ", "ST_GeomFromText('POINT(' || fStartLon || ' ' || fStartLat || ')', 4326) ");
            sql = sql.ReplaceIgnoreCase("Geometry::STPointFromText('POINT(' + CONVERT(varchar, lon) + ' ' + CONVERT(varchar, lat) + ')', 4326)", "ST_GeomFromText('POINT(' || Lon || ' ' || Lat || ')', 4326) ");
            sql = sql.ReplaceIgnoreCase("Geometry::STPointFromText('POINT(' + CONVERT(varchar, Longitude) + ' ' + CONVERT(varchar, Latitude) + ')', 4326)", "ST_GeomFromText('POINT(' || Longitude || ' ' || Latitude || ')', 4326) ");
            sql = sql.ReplaceIgnoreCase("WITH (INDEX(SIndx_GFI_FLT_ZoneHeader))", " ");
            sql = sql.ReplaceIgnoreCase("geometry::UnionAggregate", "ST_Union");

            sql = sql.ReplaceIgnoreCase("DECLARE @Assets TABLE", "CREATE TEMP TABLE _Assets");
            sql = sql.ReplaceIgnoreCase("DECLARE @Table TABLE", "CREATE TEMP TABLE _Table");

            sql = sql.ReplaceIgnoreCase("GeomData.ToString()", "ST_AsText(GeomData)");

            sql = sql.Replace("DATEDIFF(dd, 0, now())", "now() - interval '1 day'");
            if (sql.StartsWith("select top 1000 "))
            {
                sql = sql.Replace(" top 1000 ", " ");
                sql += " limit 1000";
            }
            else if (sql.StartsWith("select top 3000 "))
            {
                sql = sql.Replace(" top 3000 ", " ");
                sql += " limit 3000";
            }

            sql = sql.Replace("(select top 1000 iID from GFI_GPS_TripWizNoAddress where 1 = 1 order by iID)", "(select iID from GFI_GPS_TripWizNoAddress where 1 = 1 order by iID limit 1000)");

            sql = sql.Replace("declare @ZonesIDs table (i int)", "CREATE TEMPORARY TABLE _ZonesIDs (i int);");
            sql = sql.Replace("declare @fTmpZone table (i int)", "CREATE TEMPORARY TABLE _fTmpZone (i int);");
            sql = sql.Replace("declare @tTmpDistinctZone table (i int)", "CREATE TEMPORARY TABLE _tTmpDistinctZone (i int);");
            sql = sql.Replace("@ZonesIDs", "_ZonesIDs");
            sql = sql.Replace("@fTmpZone", "_fTmpZone");
            sql = sql.Replace("@tTmpDistinctZone", "_tTmpDistinctZone");
            sql = sql.Replace("--SelectIntoStarts ZoneExceptionsTmp", "drop table if exists _ZoneExceptionsTmp; CREATE TEMP TABLE _ZoneExceptionsTmp AS ");
            sql = sql.Replace("into #ZoneExceptionsTmp", String.Empty);

            sql = sql.Replace("begin try", "--begin try");
            sql = sql.Replace("end try", "--end try");
            sql = sql.Replace("begin catch", "--begin catch");
            sql = sql.Replace("end catch", "--end catch");
            sql = sql.Replace("(SELECT rownum = ROW_NUMBER() OVER (ORDER BY DateTimeGPS_UTC)", "(SELECT  ROW_NUMBER() OVER (ORDER BY DateTimeGPS_UTC) as rownum");
            sql = sql.Replace(", DATEADD(MINUTE, t.TotalMinutes, f.DateTimeGPS_UTC) localTime", ",f.DateTimeGPS_UTC + t.TotalMinutes * INTERVAL '1 minute' as localTime");
            sql = sql.Replace("(SELECT rownum = ROW_NUMBER() OVER (ORDER BY DateTimeGPS_UTC desc)", "(SELECT  ROW_NUMBER() OVER (ORDER BY DateTimeGPS_UTC desc) as rownum");
            sql = sql.ReplaceIgnoreCase(", a.Names + '(' + a.Email + ')',", ", a.Names || '(' || a.Email || ')',");

            sql = sql.Replace("as TableName", "as TableName;");
            sql = sql.Replace("and dateadd(dd, datediff(dd, 0, n.SendAt), 0) = dateadd(dd, datediff(dd, 0, now()), 0)", "and (n.SendAt - interval '1 day') + interval '1 day'  = (Now() - interval '1 day') + interval '1 day'");
            sql = sql.Replace("select top 1 DateTimeGPS_UTC from GFI_ARC_GPSData order by UID desc", "select DateTimeGPS_UTC from GFI_ARC_GPSData order by UID desc limit 1;");
            //select top 1 DateTimeGPS_UTC from GFI_ARC_GPSData order by UID desc

            String sqlType = String.Empty;
            Boolean bSP = false;
            Boolean bDoStatement = false;

            if (sql.StartsWith("\r\n--Live"))
            {
                sql = sqlStartsWith(sql, "\r\n--Live");
                bSP = true;
                bDoStatement = true;
            }
            else if (sql.StartsWith("\r\n--RetrieveLiveData"))
            {
                sql = sqlStartsWith(sql, "\r\n--RetrieveLiveData");
                bSP = true;
            }
            else if (sql.StartsWith("\r\n--Update Road Speed"))
            {
                sql = sqlStartsWith(sql, "\r\n--Update Road Speed");
                bSP = true;
                sqlType = "Update Road Speed";
            }
            else if (sql.StartsWith("\r\n--GetNotificationData"))
            {
                sql = sqlStartsWith(sql, "\r\n--GetNotificationData");
                bSP = true;
            }
            else if (sql.StartsWith("\r\n--GetAssetsStatus"))
            {
                sql = sqlStartsWith(sql, "\r\n--GetAssetsStatus");
                bSP = true;
                bDoStatement = true;
                sqlType = "GetAssetsStatus";
            }
            else if (sql.StartsWith("\r\n--GetSensorData"))
            {
                sql = sqlStartsWith(sql, "\r\n--GetSensorData");
                bSP = true;
            }
            else if (sql.StartsWith("\r\n--GetDailyTripTime"))
            {
                sql = sqlStartsWith(sql, "\r\n--GetDailyTripTime");
                bDoStatement = true;
                bSP = true;
                sqlType = "GetDailyTripTime";
            }
            else if (sql.StartsWith("\r\n--GetExcetpionssql"))
            {
                sql = sqlStartsWith(sql, "\r\n--GetExcetpionssql");
                bSP = true;
                sqlType = "GetExcetpionssql";
            }
            else if (sql.StartsWith("\r\n--Trips Processor"))
                sql = sqlStartsWith(sql, "\r\n--Trips Processor");
            else if (sql.StartsWith("\r\n--Exceptions Processor"))
                sql = sqlStartsWith(sql, "\r\n--Exceptions Processor");
            else if (sql.StartsWith("--TripAddresses"))
            {
                sql = sqlStartsWith(sql, "--TripAddresses");
                bSP = true;
            }
            else if (sql.StartsWith("\r\n--TripsRetrieval"))
            {
                sql = sqlStartsWith(sql, "\r\n--TripsRetrieval");
                bSP = true;
                bDoStatement = true;
                sqlType = "TripsRetrieval";
            }
            else if (sql.StartsWith("--GetMohRequestCode"))
            {
                sql = sqlStartsWith(sql, "--GetMohRequestCode");
                bSP = true;
            }
            else if (sql.StartsWith("\r\n--GetPlanningRequest"))
            {
                sql = sqlStartsWith(sql, "\r\n--GetPlanningRequest");
                bSP = true;
            }
            else if (sql.StartsWith("\r\n--GetScheduleRequest"))
            {
                sql = sqlStartsWith(sql, "\r\n--GetScheduleRequest");
                bSP = true;
            }
            else if (sql.StartsWith("\r\n--GetFuelConsumptionReport"))
            {
                sql = sqlStartsWith(sql, "\r\n--GetFuelConsumptionReport");
                bSP = true;
            }
            else if (sql.StartsWith("\r\n--ZoneVisitedReport"))
            {
                sql = sqlStartsWith(sql, "\r\n--ZoneVisitedReport");
                bSP = true;
                sqlType = "ZoneVisitedReport";
            }
            else if (sql.StartsWith("\r\n--GetPlanningActual"))
            {
                sql = sqlStartsWith(sql, "\r\n--GetPlanningActual");
                bSP = true;
            }
            else if (sql.StartsWith("\r\n--GetDriverScoreCard"))
            {
                sql = sqlStartsWith(sql, "\r\n--GetDriverScoreCard");
                bSP = true;
            }
            else if (sql.StartsWith("\r\n--GetLastProcessedFuel"))
            {
                sql = sqlStartsWith(sql, "\r\n--GetLastProcessedFuel");
                bSP = true;
            }
            else if (sql.StartsWith("\r\n--FuelLocation"))
            {
                sql = sqlStartsWith(sql, "\r\n--FuelLocation");
                bSP = true;
            }
            else if (sql.StartsWith("\r\n--SaveFuelToProcess"))
            {
                sql = sqlStartsWith(sql, "\r\n--SaveFuelToProcess");
                bSP = true;
            }
            else if (sql.StartsWith("\r\n--GetFuelDataToProcess"))
            {
                sql = sqlStartsWith(sql, "\r\n--GetFuelDataToProcess");
            }
            else if (sql.StartsWith("\r\n--GetDriverExpiry"))
            {
                sql = sqlStartsWith(sql, "\r\n--GetDriverExpiry");
            }
            else if (sql.StartsWith("\r\n--GetOdometer"))
            {
                sql = sqlStartsWith(sql, "\r\n--GetOdometer");
                bSP = true;
            }
            else if (sql.StartsWith("\r\n--Archive"))
            {
                sqlType = "Archive";
            }
            else if (sql.StartsWith("\r\n--GetInactiveUsers"))
            {
                sql = sqlStartsWith(sql, "\r\n--GetInactiveUsers");
                bSP = true;
            }
            else if (sql.StartsWith("\r\n--GetAssetMntDetails"))
            {
                sql = sqlStartsWith(sql, "\r\n--GetAssetMntDetails");
                //bSP = true;
            }
            else if (sql.StartsWith("\r\n--Trips Processor"))
                sql = sqlStartsWith(sql, "\r\n--Trips Processor");
            else if (sql.StartsWith("\r\n--Exceptions Processor"))
                sql = sqlStartsWith(sql, "\r\n--Exceptions Processor");
            else if (sql.StartsWith("\r\n--ListRoster"))
                sql = sqlStartsWith(sql, "\r\n--ListRoster");
            else if (sql.StartsWith("\r\n --PostgreSQLROND"))
                sql = sqlStartsWith(sql, "\r\n --PostgreSQLROND");
            else if (sql.EndsWith("--PostgreSQLGMS"))
            {
                bSP = true;
                sql = sql.ReplaceIgnoreCase("select * from dbo.FleetServer where Name", "select * from dbo.\"FleetServer\" where \"Name\" ");
                sql = sql.ReplaceIgnoreCase("select * from dbo.fleetuser where GMSUsername", "select * from dbo.\"FleetUser\" where \"GMSUsername\" ");
            }
            else if (sql.StartsWith("\r\n--TripDetails"))
            {
                sql = sqlStartsWith(sql, "\r\n--TripDetails");
                bSP = true;
                bDoStatement = true;
                sqlType = "TripDetails";
            }
            sql = sql.Replace("--Oracle Add SemiColumn", ";");

            if (bSP)
            {
                if (sql.ToUpper().Contains("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED"))
                {
                    sql = sql.Replace("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED", String.Empty);
                    sql = "SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;\r\n" + sql;
                }
                String sBlock = AddSemiColumn(sql);

                if (bDoStatement)
                {
                    sBlock = @" 		
do $$
begin
" + sBlock + @"
END;

$$ LANGUAGE plpgsql;
";
                }
                sql = sBlock;
            }

            switch (sqlType)
            {
                case "GetAssetsStatus":
                    sql += @"
select * from _Table order by AssetID; 
--drop table _Table;
--drop table _Assets;";
                    break;

                case ("TripsRetrieval"):
                    //sql = sql.Replace("and h.dtStart >= ?", "--and h.dtStart >= ?");
                    //sql = sql.Replace("and h.dtStart <= ?", "--and h.dtStart <= ?");
                    //sql = sql.Replace("and e.DateTimeGPS_UTC >= ? and e.DateTimeGPS_UTC <= ?", "--and e.DateTimeGPS_UTC >= ? and e.DateTimeGPS_UTC <= ?");
                    sql = sql.Replace("select * from _ExceptionDetails", "--select * from _ExceptionDetails");
                    sql = sql.Replace("AS; ", "AS ");

                    sql += "\r\n" + AddSemiColumn(sAfterDoCmd) + "\r\n";
                    sql = sql.Replace("--Oracle Add SemiColumn", ";");
                    sql = sql.Replace("Round(sum(TripDistance), 0) totalTripDistance", "Round(sum(TripDistance::numeric), 0) totalTripDistance");
                    sql = sql.Replace("convert(varchar(2500), '')", "''");
                    sql = sql.Replace("convert(varchar(250), '')", "''");
                    sql = sql.Replace("CONVERT(varchar, DATEADD(s, DATEDIFF(SECOND, min(DateTimeGPS_UTC), max(DateTimeGPS_UTC)), 0), 108)", "DATEDIFF('SECOND', min(DateTimeGPS_UTC), max(DateTimeGPS_UTC)) * interval '1 second'");
                    sql = sql.Replace("DATEADD(MINUTE, t.TotalMinutes, cte.dtFrom)", "DATEADD('mi', t.TotalMinutes::integer, cte.dtFrom::timestamptz)");
                    sql = sql.Replace("DATEADD(MINUTE, t.TotalMinutes, g.DateTimeGPS_UTC)", "DATEADD('mi', t.TotalMinutes::integer, g.DateTimeGPS_UTC::timestamptz)");
                    sql = sql.Replace("create table _GFI_GPS_GPSData", "create temp table _GFI_GPS_GPSData");

                    sql = sql.Replace("SELECT top 1 Asset from _GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.GroupDate = h.GroupDate", "SELECT Asset from _GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.GroupDate = h.GroupDate limit 1");
                    sql = sql.Replace("STUFF((SELECT ', ' + Driver from (select distinct Driver from _GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.GroupDate = h.GroupDate) Driver FOR XML PATH('')), 1, 1, '')",
                        "(select array_to_string(ARRAY(SELECT distinct Driver FROM _GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.GroupDate = h.GroupDate), ', '))");
                    sql = sql.Replace(@"group by GroupDate, AssetID
--SelectIntoEnds", "group by GroupDate, AssetID; ");
                    sql = sql.Replace("Round((select Sum(TripDistance) from _GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.GroupDate = h.GroupDate), 0) kmTravelled", "Round((select Sum(TripDistance) from _GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.GroupDate = h.GroupDate)::numeric, 0) kmTravelled");

                    sql = sql.Replace("CAST(DATEADD(second, (select Sum(IdlingTime) from _GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.GroupDate = h.GroupDate), '1900-01-01') AS TIME) ", "((select Sum(IdlingTime) from _GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.GroupDate = h.GroupDate) * interval '1 second') as ");
                    sql = sql.Replace("CAST(DATEADD(second, (select Sum(TripTime) from _GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.GroupDate = h.GroupDate), '1900-01-01') AS TIME) ", " ((select Sum(TripTime) from _GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.GroupDate = h.GroupDate) * interval '1 second') as ");
                    sql = sql.Replace("CAST(DATEADD(second, (select Sum(StopTime) from _GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.GroupDate = h.GroupDate and t.iArrivalZone is not null), '1900-01-01') AS TIME) ", "((select Sum(StopTime) from _GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.GroupDate = h.GroupDate and t.iArrivalZone is not null) * interval '1 second') as ");
                    sql = sql.Replace("CAST(DATEADD(second, (select Sum(StopTime) from _GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.GroupDate = h.GroupDate and t.iArrivalZone is null), '1900-01-01') AS TIME) ", "((select Sum(StopTime) from _GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.GroupDate = h.GroupDate and t.iArrivalZone is null) * interval '1 second') as ");
                    sql = sql.Replace("(select top 1 Asset from _GFI_GPS_TripHeader t where t.AssetID = h.AssetID) registrationNo", "(select Asset from _GFI_GPS_TripHeader t where t.AssetID = h.AssetID limit 1) registrationNo");

                    sql = sql.Replace("CAST(DATEADD(second, x.totalTripTime + y.totalTripTime, '1900-01-01') AS TIME)  as totalTripTime", "(x.totalTripTime + y.totalTripTime) * interval '1 second' as totalTripTime");
                    sql = sql.Replace("CAST(DATEADD(second, x.totalStopTime + y.totalStopTime, '1900-01-01') AS TIME) as totalStopTime", "(x.totalTripTime + y.totalTripTime) * interval '1 second' as totalStopTime");
                    sql = sql.Replace("CAST(DATEADD(second, x.totalIdlingTime + y.totalIdlingTime, '1900-01-01') AS TIME) as totalIdlingTime", "(x.totalIdlingTime + y.totalIdlingTime) * interval '1 second' as totalIdlingTime");

                    sql = sql.Replace(@"STUFF((SELECT ', ' + Driver from (select distinct Driver from _GFI_GPS_TripHeader t where t.AssetID = h.AssetID) Driver FOR XML PATH('')), 1, 1, '') driver", "(select array_to_string(ARRAY(SELECT Driver from (select distinct Driver from _GFI_GPS_TripHeader t where t.AssetID = h.AssetID) x),', ') as Driver) driver");
                    sql = sql.Replace("Round((select Sum(TripDistance) from _GFI_GPS_TripHeader t where t.AssetID = h.AssetID), 0) kmTravelled", "Round((select Sum(TripDistance) :: numeric from _GFI_GPS_TripHeader t where t.AssetID = h.AssetID), 0) kmTravelled");
                    sql = sql.Replace(" CAST(DATEADD(second, (select Sum(IdlingTime) from _GFI_GPS_TripHeader t where t.AssetID = h.AssetID), '1900-01-01') AS TIME) idlingTime", "(select Sum(IdlingTime) * interval '1 sec' from _GFI_GPS_TripHeader t where t.AssetID = h.AssetID) idlingTime");
                    sql = sql.Replace(" CAST(DATEADD(second, (select Sum(TripTime) from _GFI_GPS_TripHeader t where t.AssetID = h.AssetID), '1900-01-01') AS TIME) drivingTime", "(select Sum(TripTime) * interval '1 sec' from _GFI_GPS_TripHeader t where t.AssetID = h.AssetID) drivingTime ");
                    break;

                case ("TripDetails"):
                    sql += "\r\nselect 'TripDetails' as TableName;\r\nselect * from _td order by \"localtime\";\r\n";
                    sql += @"
select 'TripExceptions' as TableName;
select t.HeaderID, t.gpsdatauid, r.RuleID, r.RuleName, eh.groupID, eh.dateTo - eh.dateFrom as duration from _td t
	inner join GFI_GPS_Exceptions e on t.UID = e.GPSDataID
	inner join GFI_GPS_Rules r on e.RuleID = r.ParentRuleID and r.RuleID = r.ParentRuleID
    inner join gfi_gps_exceptionsheader eh on eh.RuleID = e.RuleID and  eh.AssetID = e.AssetID and  eh.GroupID = e.GroupID
order by t.localTime;";
                    break;

                case "ZoneVisitedReport":
                    sql += @"
do $$
begin
	perform pivotcode_sql('_PivotData', 'dtLocal', 'Asset', 'sZones', 'Count(iZone)', 'varchar');
END;
$$ LANGUAGE plpgsql;
select 'PivotData' as TableName;
execute crosstab_stmt;
--Execute the Dynamic Pivot Query
--EXEC sp_executesql @DynamicPivotQuery
--select sp_executesql _DynamicPivotQuery;";
                    break;

                case ("GetExcetpionssql"):
                    sql = sql.Replace("CREATE TEMP TABLE _ZoneExceptionsTmp AS;", "CREATE TEMP TABLE _ZoneExceptionsTmp AS");
                    sql = sql.Replace("into _Exceptions", "into temp _Exceptions");
                    sql = sql.Replace("into _ValidatedData", "into temp _ValidatedData");
                    sql = sql.Replace("DATE_PART('second', dateTo::time - dateFrom::time) AS duration", " dateTo - dateFrom AS duration");
                    sql = sql.Replace("--OracleSpecialExceptions", String.Empty);
                    break;

                case "GetDailyTripTime":
                    sql += @"
select 'TripHeader' as TableName;
select * from _TH order by dtStart, AssetName;

select 'Pivot' as TableName;
execute crosstab_stmt;";
                    break;

                case "Archive":
                    sql = ProcessArchivingL(sql);
                    break;

                case "Update Road Speed":
                    sql = sql.ReplaceIgnoreCase("#", "_");
                    break;
            }

            sql = sql.Replace("DATEADD(dd,", "DATEADD('day',");
            sql = sql.Replace("DATEADD(mi,", "DATEADD('mi',");
            sql = sql.Replace(",;", ",");
            sql = sql.Replace(";drop", "drop");
            sql += "\r\n--AlreadyPostgreSQLStatement";
            sql = sql.Replace("group by iid;", "group by iid");
            if (sql.ToUpper().Contains("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED"))
            {
                sql = sql.Replace("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;", String.Empty);
                sql = "SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;\r\n" + sql;
            }
            for (int i = 0; i < 10; i++)
                sql = sql.Replace(";;", ";");

            sql = sql.ReplaceIgnoreCase("RECURSIVE    RECURSIVE", "RECURSIVE");
            sql = sql.Replace("now() - 1", "now() - interval '1 day'");
            sql = sql.ReplaceIgnoreCase("create table #", "create temp table _");
            sql = sql.ReplaceIgnoreCase("create table _", "create temp table _");

            return sql;
        }

        #region StartsWith
        static String sqlStartsWith(String sResult, String StartString)
        {
            switch (StartString)
            {
                case "\r\n--Live":
                    sResult = LiveData(sResult);
                    break;

                case "\r\n--RetrieveLiveData":
                    sResult = LiveRetrieval(sResult);
                    break;

                case "\r\n--GetAssetsStatus":
                    sResult = AssetStatus(sResult);
                    break;

                case "\r\n--GetSensorData":
                    sResult = SensorData(sResult);
                    break;

                case "\r\n--GetDailyTripTime":
                    sResult = DailyTripTime(sResult);
                    break;

                case "\r\n--GetExcetpionssql":
                    sResult = ExcetpionsSql(sResult);
                    break;

                case "--GetMohRequestCode":
                    sResult = MohRequestCode(sResult);
                    break;

                case "\r\n--GetPlanningRequest":
                    sResult = PlanningRequest(sResult);
                    break;

                case "\r\n--GetScheduleRequest":
                    sResult = ScheduleRequest(sResult);
                    break;

                case "\r\n--GetPlanningActual":
                    sResult = PlanningActual(sResult);
                    break;

                case "\r\n--ZoneVisitedReport":
                    sResult = ZoneVisitedReport(sResult);
                    break;

                case "\r\n--GetFuelConsumptionReport":
                    sResult = FuelConsumption(sResult);
                    break;

                case "\r\n--Trips Processor":
                    sResult = TripsProcessor(sResult);
                    break;

                case "\r\n--Exceptions Processor":
                    sResult = ExceptionsProcessor(sResult);
                    break;

                case "--TripAddresses":
                    sResult = TripAddresses(sResult);
                    break;

                case "\r\n--TripsRetrieval":
                    sResult = TripsRetrieval(sResult);
                    break;

                case "\r\n--TripDetails":
                    sResult = TripDetails(sResult);
                    break;

                case "\r\n--GetDriverScoreCard":
                    sResult = ScoreCards(sResult);
                    break;

                case "\r\n--ListRoster":
                    sResult = RosterSql(sResult);
                    break;

                case "\r\n --PostgreSQLROND":
                    sResult = RondSql(sResult);
                    break;

                case "\r\n--GetLastProcessedFuel":
                    sResult = GetLastProcessedFuel(sResult);
                    break;

                case "\r\n--GetFuelDataToProcess":
                    sResult = GetFuelDataToProcess(sResult);
                    break;

                case "\r\n--FuelLocation":
                    sResult = FuelLocation(sResult);
                    break;

                case "\r\n--SaveFuelToProcess":
                    sResult = SaveFuelToProcess(sResult);
                    break;

                case "\r\n--GetDriverExpiry":
                    sResult = GetDriverExpiry(sResult);
                    break;

                case "\r\n--GetInactiveUsers":
                    sResult = GetInactiveUsers(sResult);
                    break;

                case "\r\n--GetNotificationData":
                    sResult = GetNotificationData(sResult);
                    break;

                case "\r\n--GetOdometer":
                    sResult = SensorData(sResult);
                    break;
                case "\r\n--Update Road Speed":
                    sResult = UpdateRoadSpeed(sResult);
                    break;


                case "\r\n--GetAssetMntDetails":
                    sResult = GetAssetMntDetails(sResult);
                    break;
            }
            return sResult;
        }


        static String GetAssetMntDetails(String sql)
        {
            String sResult = sql;
            sResult = sResult.Trim() + ";\r\n";
            sResult = sResult.Replace("ROUND(COALESCE(SUM(h.TripDistance),0), 0)", "ROUND(COALESCE(SUM(h.TripDistance) :: numeric,0), 0)");
            return sResult;
        }

        static String UpdateRoadSpeed(String sql)
        {
            String sResult = sql;
            sResult = sResult.Trim() + ";\r\n";
            sResult = sResult.Replace("-- end of minFromMultipleColumns", String.Empty);
            return sResult;
        }

        static String RosterSql(String sql)
        {
            String sResult = sql;
            sResult = sResult.Trim() + ";\r\n";
            sResult = sResult.Replace("concat(convert(varchar, rh.dtFrom, 103), '-', convert(varchar, rh.dtTo, 103)) as RosterPeriod,", "cast(cast(rh.dtFrom as Date) as text) || ' TO ' ||  cast(cast(rh.dtTo  as Date) as text) as RosterPeriod,");
            sResult = sResult.Replace("convert(varchar, rh.dtFrom, 103) as dtFrom,", "cast(cast(rh.dtFrom as Date) as text),");
            sResult = sResult.Replace("convert(varchar, rh.dtTo, 103) as dtTo,", "cast(cast(rh.dtTo  as Date) as text) dtTo,");

            return sResult;
        }

        static String RondSql(String sql)
        {
            String sResult = sql;
            sResult = sResult.Trim() + ";\r\n";
            sResult = sResult.Replace("ROUND(COALESCE(SUM(h.TripDistance),0), 0)", "ROUND(COALESCE(SUM(h.TripDistance)::numeric,0), 0)");

            return sResult;
        }

        static String GetLastProcessedFuel(String sql)
        {
            String sResult = sql;
            string sResult1 = string.Empty;
            sResult = sResult.Trim() + ";\r\n";
            sResult = sResult.Replace(@"--GetLastProcessedFuel
select", @"--GetLastProcessedFuel
perform");
            sResult = sResult.Replace("if(@@ROWCOUNT = 0)", "GET DIAGNOSTICS total_rows := ROW_COUNT;\r\nif(total_rows = 0) then");
            sResult = sResult.Replace("begin", "--begin");
            sResult = sResult.Replace("\r\nend", "\r\nend if;\r\nEND;\r\nEND;$$ LANGUAGE plpgsql; \r\n");
            sResult = "do $$ BEGIN \r\n declare total_rows int;  begin\r\n" + sResult.ToString();

            return sResult;
        }

        static String GetFuelDataToProcess(String sql)
        {
            String sResult = sql;
            sResult = sResult.Trim() + ";\r\n";
            //drop table if exists _data;
            sResult = sResult.Replace("into #data", "into temp _data");
            sResult = sResult.Replace("#data", "_data");

            sResult = ("drop table if exists _data;" + sResult.ToString());
            sResult = sResult.Replace("if(@@RowCount > 0)", "--------------");
            sResult = sResult.Replace(@"--aaa

select UID from _data where RowNum", "_#");
            sResult = sResult.Replace("--FinalResult", "do $$ BEGIN");
            sResult = sResult.Replace("_#", "IF EXISTS(select UID from _data where RowNum");
            sResult = sResult.Replace("--@@", ")THEN");
            sResult = sResult.Replace(" begin", "");
            sResult = sResult.Replace(" end--p", "END IF; ---test"); //--FinalResult
            sResult = sResult.Replace("---test", "END;$$ LANGUAGE plpgsql; ");
            sResult = sResult.Replace("---test", "END;$$ LANGUAGE plpgsql; ");
            sResult = sResult.Replace("minValue", "as minValue ");
            sResult = sResult.Replace("maxValue", "as maxValue ");
            return sResult;
        }

        static String FuelLocation(String sql)
        {
            String sResult = sql;

            sResult = sResult.Trim() + ";\r\n";
            sResult = ("drop table if exists _FuelLocation;" + sResult.ToString());
            sResult = sResult.Replace("CREATE TABLE", "CREATE TEMP TABLE");
            sResult = sResult.Replace("#FuelLocation", "_FuelLocation");
            sResult = sResult.Replace("top 1", "");


            return sResult;
        }

        static String SaveFuelToProcess(String sql)
        {
            String sResult = sql;

            sResult = sResult.Trim() + ";\r\n";
            sResult = "drop table if exists _GFI_GPS_FuelData; drop table if exists _fuelconsumption; drop table if exists _fuelexceptions; \r\n" + sResult.ToString();
            sResult = sResult.Replace("CREATE TABLE", "CREATE TEMP TABLE");
            sResult = sResult.Replace("#GFI_GPS_FuelData", "_GFI_GPS_FuelData");
            sResult = sResult.Replace("#FuelConsumption", "_FuelConsumption");
            sResult = sResult.Replace("#FuelExceptions", "_FuelExceptions");
            sResult = sResult.Replace("datetime", "timestamp");
            sResult = sResult.Replace("nvarchar(20)", "varchar(20)");
            sResult = sResult.Replace("nvarchar(15)", "varchar(15)");
            sResult = sResult.Replace("SELECT rownum = ROW_NUMBER() OVER (ORDER BY AssetID, DateTimeGPS_UTC)", "SELECT  ROW_NUMBER() OVER (ORDER BY AssetID, DateTimeGPS_UTC) as rownum");
            sResult = sResult.Replace("SELECT rownum = ROW_NUMBER() OVER (ORDER BY AssetID, Origine, DateTimeGPS_UTC)", "SELECT ROW_NUMBER() OVER (ORDER BY AssetID, Origine, DateTimeGPS_UTC) as rownum");
            sResult = sResult.Replace("SELECT rownum = ROW_NUMBER() OVER (ORDER BY AssetID, DateTimeGPS_UTC)", "SELECT ROW_NUMBER() OVER (ORDER BY AssetID, DateTimeGPS_UTC) as rownum");
            sResult = sResult.Replace("COLLATE DATABASE_DEFAULT", "");
            sResult = sResult.Replace(@"from _FuelConsumption f
	where Origine = 2
		and dtPreviousFill < DateTimeGPS_UTC;", @"from _FuelConsumption f
	where f.Origine = 2
		and f.dtPreviousFill < f.DateTimeGPS_UTC;");
            sResult = sResult.Replace("where Origine = 2", "where f.Origine = 2");
            sResult = sResult.Replace(" CHECKSUM(NEWID()) GroupID", "floor(random()*1000000000000) as GroupID");
            sResult = sResult.Replace("into _FuelExceptions", "into temp _FuelExceptions");
            sResult += @"
DELETE FROM GFI_GPS_FuelConsumption
	WHERE iID IN
		(SELECT iID FROM 
			(SELECT iID, ROW_NUMBER() OVER(PARTITION BY fueldatauid	ORDER BY iID) AS row_num
				FROM GFI_GPS_FuelConsumption 
			) t
		WHERE t.row_num > 1 
		);";
            return sResult;
        }

        static String GetDriverExpiry(String sql)
        {
            String sResult = sql;

            sResult = sResult.Trim() + ";\r\n";

            sResult = sResult.Replace("DATEDIFF(month ,expirydate, now())", "rs.expirydate::date - now()::date");



            return sResult;
        }

        static String GetNotificationData(String sql)
        {
            String sResult = sql;

            sResult = sResult.Replace("WITH (READPAST)", String.Empty);
            sResult = sResult.Replace(" - 1", "- interval '1 day'");
            return sResult;
        }

        static String GetInactiveUsers(String sql)
        {
            String sResult = sql;

            sResult = sResult.Trim() + ";\r\n";

            sResult = sResult.Replace("DATEDIFF(day, b.a, CURRENT_TIMESTAMP)", "DATE_PART('day', CURRENT_TIMESTAMP - b.a)");

            return sResult;
        }

        static String LiveData(String sql)
        {
            String sResult = sql;
            sResult = sResult.Trim() + ";\r\n";
            sResult = sResult.Replace("CREATE TABLE #GFI_GPS_GPSData", "CREATE TEMP TABLE #GFI_GPS_GPSData");
            sResult = sResult.Replace("CREATE TABLE #GFI_GPS_GPSDataDetail", "CREATE TEMP TABLE #GFI_GPS_GPSDataDetail");
            sResult = sResult.Replace("#GFI_GPS_GPSDataDetail", "_GFI_GPS_GPSDataDetail");
            sResult = sResult.Replace("#GFI_GPS_GPSData", "_GFI_GPS_GPSData");
            sResult = sResult.Replace("--PostGreSQL order by DateTimeGPS_UTC", " order by DateTimeGPS_UTC");

            sResult = sResult.ReplaceIgnoreCase("begin tran", "--begin tran");
            sResult = sResult.ReplaceIgnoreCase("begin try", "--BEGIN try");
            sResult = sResult.ReplaceIgnoreCase("end try", "--end try");
            sResult = sResult.ReplaceIgnoreCase("begin catch", "EXCEPTION --[PostgreSQL Exception");
            sResult = sResult.ReplaceIgnoreCase("end catch", "END; --PostgreSQL Exception]");

            sResult = sResult.Replace("delete GFI_GPS_LiveData", "delete --GFI_GPS_LiveData");
            sResult = sResult.Replace("inner join cte on cte.UID = l.UID", "using cte where cte.UID = l.UID");

            sResult = sResult.Replace("delete GFI_GPS_HeartBeat", "delete --GFI_GPS_HeartBeat");
            sResult = sResult.Replace("inner join cte on cte.AssetID = h.AssetID", "using cte where cte.AssetID = h.AssetID");

            sResult = sResult.Replace("delete _GFI_GPS_GPSData", "delete --_GFI_GPS_GPSData");
            sResult = sResult.Replace("inner join cte on cte.iid = g.iid", "using cte where cte.iid = g.iid");

            sResult = sResult.Replace("where cte.RowNum > 6", "and cte.RowNum > 6");
            sResult = sResult.Replace("WHERE DateTimeGPS_UTC < now() + 1", "WHERE DateTimeGPS_UTC < now() + INTERVAL '1day'");

            sResult = sResult.Replace("Declare @ProcessTable Table (AssetID int, dt TIMESTAMP)", "CREATE TEMP TABLE _ProcessTable (AssetID int, dt TIMESTAMP);");
            sResult = sResult.Replace("@ProcessTable", "_ProcessTable");

            sResult = sResult.Substring(0, sResult.LastIndexOf("--Cursor Starts for RegisterDevicesForProcessing"));
            sResult += LiveException();

            return sResult;
        }
        static String LiveException()
        {
            String sException = @"
        --Cursor Starts for RegisterDevicesForProcessing
		Declare	rec RECORD;
		declare xyCursor cursor FOR  
				select * from _ProcessTable;
		begin
			open xyCursor;
				LOOP
				fetch xyCursor into rec;
				EXIT WHEN NOT FOUND;
                    Call RegisterDevicesForProcessing (rec.AssetID, rec.dt, 'Trips');
                    Call RegisterDevicesForProcessing (rec.AssetID, rec.dt, 'Exceptions');
				END LOOP;
			close xyCursor;
        end;

EXCEPTION --[PostgreSQL Exception
        WHEN OTHERS THEN
            declare errMsg VARCHAR (200);
                    err_code VARCHAR (50);
            BEGIN
                errMsg := SUBSTR(SQLERRM, 1, 200);
                err_code := SUBSTR(SQLSTATE, 1, 200);
                ROLLBACK;              
                BEGIN                 
                    BEGIN
                        INSERT INTO GFI_GPS_ProcessErrors (sError, sOrigine, sFieldValue)
                            VALUES ( errMsg, 'LiveDwn', err_code );             
                    END;
                EXCEPTION
                    WHEN OTHERS THEN raise notice 'errMsg';
                END;   
            END;

    BEGIN
        drop table if exists _GFI_GPS_GPSData;
        drop table if exists _GFI_GPS_GPSDataDetail;
        drop table if exists _ProcessTable;
    EXCEPTION
        WHEN OTHERS THEN
           raise notice 'errMsg';
    END; 

    commit;
    drop table if exists _GFI_GPS_GPSData;
    drop table if exists _GFI_GPS_GPSDataDetail;
    drop table if exists _ProcessTable;

";
            return sException;
        }

        static String LiveRetrieval(String sql)
        {
            String sResult = sql;
            sResult = "CREATE TEMP SEQUENCE _Assets_seq START WITH 1 INCREMENT BY 1 \r\n" + sResult;
            sResult = sResult.Replace("CREATE TABLE #Assets", "CREATE TEMP TABLE #Assets");
            sResult = sResult.Replace("declare @ZonesIDs table (i int)", "CREATE TEMP TABLE _ZonesIDs (i int);");
            sResult = sResult.Replace("UPDATE #Assets", "UPDATE _Assets T1");
            sResult = sResult.Replace("FROM #Assets T1", "from GFI_FLT_Asset T2");
            sResult = sResult.Replace("    INNER JOIN GFI_FLT_Asset T2 ON T2.AssetID = T1.iAssetID", "INNER JOIN GFI_SYS_TimeZones T3 on T2.TimeZoneID = T3.StandardName where T2.AssetID = T1.iAssetID");
            sResult = sResult.Replace("    INNER JOIN GFI_SYS_TimeZones T3 on T2.TimeZoneID = T3.StandardName", "");

            sResult = sResult.Replace("#Assets", "_Assets");
            sResult = sResult.Replace("@ZonesIDs", "_ZonesIDs");
            sResult = sResult.Replace("'' Comment", "'' as Comment");
            sResult = sResult.Replace("now() + 1", "now() + INTERVAL '1day'");
            sResult = sResult.Replace("--CREATE TEMP TABLE PostgreSQLGFI_GPS_LiveData AS", "CREATE TEMP TABLE _GFI_GPS_LiveData AS");
            sResult = sResult.ReplaceIgnoreCase("IDENTITY(1,1) NOT NULL", "DEFAULT NEXTVAL ('_Assets_seq" + "')");
            sResult = sResult.Replace("into #GFI_GPS_LiveData", "--into #GFI_GPS_LiveData");

            sResult = sResult.Replace("declare @fTmpZone table (i int)", "create temp table _fTmpZone (i int);");
            sResult = sResult.Replace("@fTmpZone", "_fTmpZone");

            sResult = sResult.Replace("declare @tTmpDistinctZone table (i int)", "create temp table _tTmpDistinctZone (i int);");
            sResult = sResult.Replace("@tTmpDistinctZone", "_tTmpDistinctZone");

            sResult = sResult.Replace("--CREATE TEMP TABLE PostgreSQLLiveZoneTmp AS", "CREATE TEMP TABLE _LiveZoneTmp AS");
            sResult = sResult.Replace("into #LiveZoneTmp", String.Empty);
            sResult = sResult.Replace("#LiveZoneTmp", "_LiveZoneTmp");

            sResult = sResult.Replace("--CREATE TEMP TABLE PostgreSQLLiveZone AS", "CREATE TEMP TABLE _LiveZone AS");
            sResult = sResult.Replace("into #LiveZone", String.Empty);
            sResult = sResult.Replace("#LiveZone", "_LiveZone");

            sResult = sResult.Replace("--Oracle Starts Here", ";--Oracle Starts Here");
            sResult = sResult.Replace("convert(varchar(250), '')", "''");

            sResult = sResult.Replace("#GFI_GPS_LiveData", "_GFI_GPS_LiveData");

            sResult = sResult.Replace("SELECT top 1 e.ZoneID FROM _LiveZone e Where e.UID = h.UID", "SELECT e.ZoneID FROM _LiveZone e Where e.UID = h.UID limit 1");
            sResult = sResult.Replace("SELECT top 1 Coalesce(e.Description, '') FROM _LiveZone e Where e.UID = h.UID", "SELECT Coalesce(e.Description, '') FROM _LiveZone e Where e.UID = h.UID limit 1");

            sResult = sResult.Replace("--ZoneMatrixRetrieval", "RECURSIVE");
            sResult = sResult.Replace(" TableName", " TableName;");

            sResult = sResult.Replace("Geometry::STPointFromText('POINT(' + CONVERT(varchar, Longitude) + ' ' + CONVERT(varchar, Latitude) + ')', 4326) pointFr", "ST_GeomFromText('POINT(' || Longitude || ' ' ||  Latitude || ')', 4326) pointFr");
            sResult = sResult.Replace("z.GeomData.STIntersects(pointFr) = 1", "ST_Intersects(z.GeomData, pointFr)");

            sResult = sResult.Replace("from _GFI_GPS_LiveData h", String.Empty);
            sResult = sResult.Replace("update _GFI_GPS_LiveData", "update _GFI_GPS_LiveData h");
            sResult += @"
drop table _GFI_GPS_LiveData; 
drop table _Assets;
drop SEQUENCE _Assets_seq;
drop table _zonesids;
drop table _ftmpzone;
drop table _ttmpdistinctzone;
drop table _livezonetmp;
drop table _livezone;

";
            return sResult;
        }

        static String AssetStatus(String sql)
        {
            String sResult = sql;
            sResult = sResult.Trim() + ";\r\n";
            sResult = sResult.Replace("@Assets", "_Assets");
            sResult = sResult.Replace("@Table", "_Table");
            sResult = sResult.Replace("@GMID", "_GMID");
            sResult = sResult.Replace("@AssetID", "_AssetID");
            sResult = sResult.Replace("@AllClients", "_AllClients");
            sResult = sResult.Replace("declare _AllClients int", "");
            sResult = sResult.Replace("declare _AssetID INT", "DECLARE _AllClients integer;");
            sResult = sResult.Replace("declare _GMID INT", "DECLARE Rec record;");
            sResult = sResult.Replace("set _AllClients", "--set _AllClients");
            sResult = sResult.Replace("open ProcessCursor", @"
	Begin
		_AllClients := (select GMID from GFI_SYS_GroupMatrix where GMDescription = '### All Clients ###'); 
		open ProcessCursor;
			LOOP
				FETCH ProcessCursor INTO Rec;
				EXIT WHEN NOT FOUND; ");

            sResult = sResult.Replace(@"fetch next from ProcessCursor into _AssetID, _GMID
	WHILE @@FETCH_STATUS = 0   
	BEGIN ", String.Empty);

            sResult = sResult.Replace(";with", "with RECURSIVE");
            sResult = sResult.Replace(@"fetch next from ProcessCursor into _AssetID, _GMID
	END
close ProcessCursor
deallocate ProcessCursor", @"			END LOOP;
		close ProcessCursor;
	end;
");
            sResult = sResult.Replace(", details = STUFF((SELECT '| ' + d.TypeID + ' : ' + d.TypeValue + ' : ' + d.UOM FROM GFI_GPS_LiveDataDetail d Where d.UID = T2.UID FOR XML PATH('')), 1, 1, '')  ", ", details = (select array_to_string(ARRAY(SELECT distinct d.TypeID || ' : ' || d.TypeValue || ' : ' || d.UOM FROM GFI_GPS_GPSDataDetail d Where d.UID = T2.UID), ', ')) ");
            sResult = sResult.Replace("dtLastRptd <= DATEADD(day, DATEDIFF(day, 0, now()), -1)", "dtLastRptd <= now() - interval '1 day'");
            sResult = sResult.Replace("DATEDIFF(day, dtLastRptd, now())", "DATEDIFF('day', dtLastRptd::timestamp, now()::timestamp);");
            sResult = sResult.Replace("select * from _Table order by AssetID", "--select * from _Table order by AssetID; ");
            sResult = sResult.Replace("_GMID", "Rec.GMID");
            sResult = sResult.Replace("_AssetID", "Rec.AssetID");
            sResult = sResult.Replace("'*** Not Recent *** ' +", "'*** Not Recent *** ' ||");

            return sResult;
        }

        static String SensorData(String sql)
        {
            String sResult = sql;
            sResult = sResult.Trim() + ";\r\n";
            sResult = sResult.Replace("CREATE TABLE", "CREATE Temp TABLE");
            sResult = sResult.Replace("#Assets", "_Assets");
            sResult = sResult.Replace("#TripHeader", "_TripHeader");
            sResult = sResult.Replace("#GPSData", "_GPSData");
            sResult = sResult.Replace("RowNumber int IDENTITY(1,1) NOT NULL,", "RowNumber int GENERATED ALWAYS AS IDENTITY,");
            sResult = sResult.Replace("[iAssetID] [int] PRIMARY KEY CLUSTERED", "iAssetID int PRIMARY KEY");
            sResult = sResult.Replace("CAST(DATEADD(second, h.TripTime, '1900-01-01') AS TIME)", "h.TripTime * interval '1 second'");

            sResult = sResult.Replace("DATEADD(MINUTE, t.TotalMinutes, h.DateTimeGPS_UTC) localTime", "t.TotalMinutes * INTERVAL '1 MINUTE' + h.DateTimeGPS_UTC as localTime");
            //sResult = sResult.Replace(", + STUFF((SELECT '| ' + d.TypeID + ' : ' + d.TypeValue + ' : ' + d.UOM FROM GFI_GPS_GPSDataDetail d Where d.UID = h.UID and d.TypeID in ('Harsh Accelleration', 'Harsh Breaking', 'Harsh Cornering', 'Heading', 'Idling', 'Ignition') FOR XML PATH('')), 1, 1, '') As details ", "--, STRING_AGG(--(SELECT '| ' || d.TypeID || ' : ' || d.TypeValue || ' : ' || d.UOM FROM GFI_GPS_GPSDataDetail d Where d.UID = h.UID and d.TypeID in ('Harsh Accelleration', 'Harsh Breaking', 'Harsh Cornering', 'Heading', 'Idling', 'Ignition')) --, ', ') As details, '' details");
            sResult = sResult.Replace(", + STUFF(", "--");
            sResult = sResult.Replace("(SELECT '| ' + d.TypeID + ' : ' + d.TypeValue + ' : ' + d.UOM FROM", "--");
            sResult = sResult.Replace("GFI_GPS_GPSDataDetail d Where d.UID = h.UID", "--");
            sResult = sResult.Replace("FOR XML PATH", "--");
            sResult = sResult.Replace(", 1, 1, '') As details ", ",'' details");
            //sResult = sResult.Replace(",'' details", ",(select array_to_string(ARRAY(SELECT '| ' || d.TypeID || ' : ' || d.TypeValue || ' : ' || d.UOM FROM GFI_GPS_GPSDataDetail d Where d.UID = h.UID and d.TypeID in ('Temperature', 'Temperature 2', 'Temperature 3', 'Temperature 4')), ', '))  as details");

            return sResult;
        }

        static String ProcessArchivingL(String sql)
        {
            String strDate = String.Empty;
            String AssetID = String.Empty;
            string[] lines = sql.Split('\n');
            for (int i = 0; i < lines.Length; i++)
            {
                String line = lines[i];
                if (line.Trim().Contains("insert into #Assets (iAssetID) values"))
                {
                    String[] tmp = line.Split(new string[] { "values" }, StringSplitOptions.RemoveEmptyEntries);
                    if (String.IsNullOrEmpty(AssetID))
                        AssetID = tmp[1].Replace("(", String.Empty).Replace(")", String.Empty).Replace("\r", String.Empty);
                }

                if (line.Trim().Contains("where h.dtStart < '"))
                {
                    String[] tmp = line.Split(new string[] { "where h.dtStart < '" }, StringSplitOptions.RemoveEmptyEntries);
                    if (String.IsNullOrEmpty(strDate))
                        strDate = tmp[1].Replace("'", String.Empty).Replace("\r", String.Empty);
                }
            }

            String ProcessArchivingLsql = @"
                do $$
begin
	drop table if exists _TripHeader;
	drop table if exists _GPSData;
	drop table if exists _Assets;

	--Archive
	CREATE Temp TABLE _TripHeader
	(
		iID int PRIMARY KEY,
		dtStart TIMESTAMP
	); 
 
	CREATE Temp TABLE _GPSData
	(
		UID int PRIMARY KEY ,
		AssetID int,
		DateTimeGPS_UTC TIMESTAMP
	); 
 
	CREATE Temp TABLE _Assets
	(
		RowNumber int GENERATED ALWAYS AS IDENTITY,
		iAssetID int PRIMARY KEY 
	); 
 
	insert into _Assets (iAssetID) values (40); 
 
	declare _dtStart TIMESTAMP;
	declare _MaxRows int;
	declare _inTrans int;
	declare _dtTripEnd TIMESTAMP;
	declare _GPSIDTripEnd int; 
	declare total_rows int;
	declare _MyAsset int;
	begin 
 
		_dtStart := now();  
		_MaxRows := 5;  
		_inTrans := 0; 
		select into _MyAsset iAssetID from _Assets limit 1;
 
		--prepare (_MaxRows) line migrations
		perform 1;
		GET DIAGNOSTICS total_rows := ROW_COUNT; 
		while(total_rows > 0) loop
			begin
				if(DATEDIFF('mi', _dtStart::timestamp, now()::timestamp) > 12) then
					exit;
				end if;
 
				--Begin Try
					--set _Run = _Run + 1
					select into _dtTripEnd dtEnd
						from (
								select h.* from GFI_GPS_TripHeader h
									inner join _Assets a on h.AssetID = a.iAssetID
								where h.dtStart < '2020-03-28 00:00:01'
									order by h.dtStart
								limit _MaxRows
								) x
					order by dtStart desc limit 1;

					select into _GPSIDTripEnd GPSDataEndUID
						from (
								select h.* from GFI_GPS_TripHeader h
									inner join _Assets a on h.AssetID = a.iAssetID
								where h.dtStart < '2020-03-28 00:00:01'
									order by h.dtStart
								limit _MaxRows
								) x
					order by dtStart desc limit 1;
					
					raise notice '% % %', _MyAsset, _dtTripEnd, _GPSIDTripEnd;
					--select _dtTripEnd, _GPSIDTripEnd; 
 
					--Trips
					--begin tran
							 _inTrans := 1; 
 
							delete from _TripHeader;
							insert into _TripHeader (iID, dtStart)
								select h.iID, h.dtStart from GFI_GPS_TripHeader h
									inner join _Assets a on h.AssetID = a.iAssetID
								where h.dtStart <= _dtTripEnd and GPSDataEndUID <= _GPSIDTripEnd
								order by h.dtStart limit (_MaxRows); 
 
							insert into GFI_ARC_TripHeader (iID, AssetID, DriverID, ExceptionFlag, MaxSpeed, IdlingTime, StopTime, TripDistance, TripTime, GPSDataStartUID, GPSDataEndUID, dtStart, fStartLon, fStartLat, dtEnd, fEndLon, fEndLat, OverSpeed1Time, OverSpeed2Time, Accel, Brake, Corner, DepartureAddress, ArrivalAddress)
								select h.* from GFI_GPS_TripHeader h
									inner join _Assets a on h.AssetID = a.iAssetID
								where h.dtStart <= _dtTripEnd and GPSDataEndUID <= _GPSIDTripEnd
								order by h.dtStart limit (_MaxRows); 
 
							insert into GFI_ARC_TripDetail (iID, HeaderiID, GPSDataUID)
								select d.* from _TripHeader h
									--inner join _Assets a on h.AssetID = a.iAssetID
									inner join GFI_GPS_TripDetail d on h.iID = d.HeaderiID
								--where h.dtStart <= _dtTripEnd and GPSDataEndUID <= _GPSIDTripEnd
									order by h.dtStart; 
 
							perform 1;
							GET DIAGNOSTICS total_rows := ROW_COUNT; 
							while(total_rows > 0) loop
								begin
									delete from GFI_GPS_TripHeader
										where ctid = any (array(SELECT h.ctid from GFI_GPS_TripHeader h
																	inner join _Assets a on h.AssetID = a.iAssetID
																where h.dtStart <= _dtTripEnd and GPSDataEndUID <= _GPSIDTripEnd
																	limit _MaxRows
																)
														);

									perform h.ctid from GFI_GPS_TripHeader h
										inner join _Assets a on h.AssetID = a.iAssetID
									where h.dtStart <= _dtTripEnd and GPSDataEndUID <= _GPSIDTripEnd
										limit _MaxRows;
									GET DIAGNOSTICS total_rows := ROW_COUNT;
								end;
							end loop;
					--commit tran
					 _inTrans := 0; 
 
					--Exceptions
					--begin tran
						_inTrans := 1;
						insert into GFI_ARC_Exceptions (iID, RuleID, GPSDataID, AssetID, DriverID, Severity, GroupID, Posted, InsertedAt, DateTimeGPS_UTC)
							select h.* from GFI_GPS_Exceptions h
								inner join _Assets a on h.AssetID = a.iAssetID
							where h.InsertedAt <= _dtTripEnd and GPSDataID <= _GPSIDTripEnd; 
 
						--notification; 
 
						--GPSData
						delete from _GPSData;
						insert into _GPSData
							select g.UID, g.AssetID, g.DateTimeGPS_UTC from GFI_GPS_GPSData g 
									inner join _Assets a ON g.AssetID = a.iAssetID
								where g.DateTimeGPS_UTC <= _dtTripEnd and g.UID <= _GPSIDTripEnd; 
 
						insert into GFI_ARC_GPSData (UID, AssetID, DateTimeGPS_UTC, DateTimeServer, Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, StopFlag, TripDistance, TripTime, WorkHour, DriverID, RoadSpeed)
							select g.* from GFI_GPS_GPSData g 
									inner join _Assets a ON g.AssetID = a.iAssetID
								where g.DateTimeGPS_UTC <= _dtTripEnd and g.UID <= _GPSIDTripEnd; 
 
						--GPSDataDetail
						insert into GFI_ARC_GPSDataDetail (GPSDetailID, UID, TypeID, TypeValue, UOM, Remarks)
							select d.* from _GPSData g
									inner join _Assets a ON g.AssetID = a.iAssetID
									inner join GFI_GPS_GPSDataDetail d on g.UID = d.UID
								where g.DateTimeGPS_UTC <= _dtTripEnd and g.UID <= _GPSIDTripEnd; 
 
						perform 1;
						GET DIAGNOSTICS total_rows := ROW_COUNT; 
						while(total_rows > 0) loop
							begin
								delete from GFI_GPS_GPSData
									where uid = any (array(SELECT uid from GFI_GPS_GPSData g 
																inner join _Assets a ON g.AssetID = a.iAssetID
															where g.DateTimeGPS_UTC <= _dtTripEnd 
																and g.UID <= _GPSIDTripEnd 
															limit _MaxRows
															)
													);

								perform g.ctid from GFI_GPS_GPSData g 
										inner join _Assets a ON g.AssetID = a.iAssetID
									where g.DateTimeGPS_UTC <= _dtTripEnd and g.UID <= _GPSIDTripEnd 
										limit _MaxRows;
								GET DIAGNOSTICS total_rows := ROW_COUNT;
							end; 
 						end loop;
			
						--Deleting from GFI_SYS_Notification
						perform 1;
						GET DIAGNOSTICS total_rows := ROW_COUNT; 
						while(total_rows > 0) loop
							begin
								delete from GFI_SYS_Notification
									where ctid = any (array(SELECT h.ctid from GFI_SYS_Notification h
																inner join _Assets a on h.AssetID = a.iAssetID
															where h.InsertedAt < _dtTripEnd 
																	limit _MaxRows
															)
														);

								perform h.ctid from GFI_SYS_Notification h
									inner join _Assets a on h.AssetID = a.iAssetID
								where h.InsertedAt < _dtTripEnd 
										limit _MaxRows;
								GET DIAGNOSTICS total_rows := ROW_COUNT;
							end; 
						end loop;
 
						perform 1;
						GET DIAGNOSTICS total_rows := ROW_COUNT; 
						while(total_rows > 0) loop
							begin
								delete from GFI_GPS_Exceptions
									WHERE ctid = any (array(SELECT h.ctid from GFI_GPS_Exceptions h
																inner join _Assets a on h.AssetID = a.iAssetID
															where h.InsertedAt < _dtTripEnd
																	limit _MaxRows
															)
													);

								perform h.ctid from GFI_GPS_Exceptions h
									inner join _Assets a on h.AssetID = a.iAssetID
								where h.InsertedAt < _dtTripEnd
										limit _MaxRows;
								GET DIAGNOSTICS total_rows := ROW_COUNT;
							end; 
						end loop;
 
					--commit tran
					 _inTrans := 0; 
 
				   --Audit
					--begin tran
						 _inTrans := 1; 
 
						insert into GFI_ARC_Audit (auditId, auditUser, auditDate, auditType, ReferenceCode, ReferenceDesc, ReferenceTable, CreatedDate, CallerFunction, SQLRemarks, NewValue, OldValue, PostedDate, PostedFlag, UserID)
							select a.* from GFI_SYS_Audit a 
								where a.auditDate <= _dtTripEnd; 
 
						perform 1;
						GET DIAGNOSTICS total_rows := ROW_COUNT; 
						while(total_rows > 0) loop
							begin
								delete from GFI_SYS_Audit
									where ctid = any (array(SELECT ctid from GFI_SYS_Audit
																where auditDate <= _dtTripEnd
															limit _MaxRows
															)
														);

									perform ctid from GFI_SYS_Audit
										where auditDate <= _dtTripEnd
									limit _MaxRows;
									GET DIAGNOSTICS total_rows := ROW_COUNT;
							end;
						end loop;
					--commit tran
					 _inTrans := 0;
				--end try
				--begin catch
					if(_inTrans = 1) then
						begin
							--rollback tran
						end; 
					end if;
 
					--begin try
						--Declare _MyAssetID int;
						--select _MyAssetID = max(iAssetID) from _Assets
						--insert into GFI_GPS_ProcessErrors (sError, sOrigine, AssetID, dtUTC) values (ERROR_MESSAGE(), 'Archiving' , _MyAssetID, _dtStart)
					--end try
					--begin catch
					--end catch; 
 
				--end catch; 
 
				--restart loop if needed
				perform h.* from GFI_GPS_TripHeader h
					inner join _Assets a on h.AssetID = a.iAssetID
				where h.dtStart < '2020-03-28 00:00:01' limit 1;
				GET DIAGNOSTICS total_rows := ROW_COUNT; 
			end; 
		end loop;
		--select _Run Run; 



	EXCEPTION --[PostgreSQL Exception
			WHEN OTHERS THEN
				declare errMsg VARCHAR (200);
						err_code VARCHAR (50);
				BEGIN
					errMsg := SUBSTR(SQLERRM, 1, 200);
					err_code := SUBSTR(SQLSTATE, 1, 200);
					ROLLBACK;              
					BEGIN                 
						BEGIN
							INSERT INTO GFI_GPS_ProcessErrors (sError, sOrigine, sFieldValue, AssetID)
								VALUES ( errMsg, 'Archiving', err_code, _MyAsset);             
						END;
					EXCEPTION
						WHEN OTHERS THEN raise notice 'Archiving';
					END;   
				END;
	end; 
END;

$$ LANGUAGE plpgsql;

--AlreadyPostgreSQLStatement
";
            ProcessArchivingLsql = ProcessArchivingLsql.Replace("2020-03-28 00:00:01", strDate);
            ProcessArchivingLsql = ProcessArchivingLsql.Replace("40", AssetID);
            return ProcessArchivingLsql;
        }

        static String DailyTripTime(String sql)
        {
            String sResult = sql;
            sResult = sResult.Trim() + ";\r\n";
            sResult = sResult.Replace("CREATE TABLE", "CREATE Temp TABLE");
            sResult = sResult.Replace("#Assets", "_Assets");
            sResult = sResult.Replace("RowNumber int IDENTITY(1,1) NOT NULL,", "RowNumber int GENERATED ALWAYS AS IDENTITY,");
            sResult = sResult.Replace("[iAssetID] [int] PRIMARY KEY CLUSTERED", "iAssetID int PRIMARY KEY");
            //sResult = sResult.Replace("insert", "insert into");
            sResult = sResult.Replace("#TripHeader", "_TripHeader");
            sResult = sResult.Replace("#TripHeader", "_TripHeader");
            sResult = sResult.Replace("#TH", "_TH");

            sResult = sResult.Replace("select a.AssetName, t.*", @"
drop table if exists _TH;
CREATE TEMP TABLE _TH AS
    select a.AssetName, t.*");
            sResult = sResult.Replace("into _TH", "--into _TH");

            sResult = sResult.Replace("--statement", @"
drop table if exists _TripHeader;
CREATE TEMP TABLE _TripHeader AS ");

            sResult = sResult.Replace("into _TripHeader", " -- into _TripHeader");
            sResult = sResult.Replace("(CONVERT(varchar, DATEADD(s, totalIdlingTime, 0), 108)) ts_totalIdlingTime", " cast(totalIdlingTime as int) * interval '1 second' ts_totalIdlingTime");
            sResult = sResult.Replace("(CONVERT(varchar, DATEADD(s, totalTripTime, 0), 108)) ts_totalTripTime ", " cast(totalTripTime as int) * interval '1 second' ts_totalTripTime ");
            sResult = sResult.Replace("Convert(VarChar, DateAdd(S, sum(totalTripTime), 0), 108) AS Total", "cast(sum(totalTripTime) as int) * interval '1 second')  Total");
            sResult = sResult.Replace("--, (CONVERT(varchar, DATEADD(s, totalStopTime, 0), 108)) ts_totalStopTime ", ", cast(totalStopTime as int) * interval '1 second' ts_totalStopTime");
            sResult = sResult.Replace("select AssetID, cast(sum(totalTripTime)/86400 as varchar(50))+':'+ cast(sum(totalTripTime) as int) * interval '1 second')  Total from _TH group by AssetID", "select AssetID, cast(sum(totalTripTime) as int) * interval '1 second' Total from _TH group by AssetID");

            String PivotCol = sql.Split('\r')[1].Split(' ')[2].Trim();
            sResult = sResult.Substring(0, sResult.IndexOf("select 'TripHeader' as TableName"));
            sResult += "\r\nperform pivotcode_sql('_TH', 'AssetName', null, 'dtStart', 'max(" + PivotCol + ")', 'varchar');";
            return sResult;
        }

        static String ExcetpionsSql(String sql)
        {
            String sResult = sql;
            sResult = sResult.Trim() + ";\r\n";
            sResult = sResult.Replace("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED --drop table script", "drop table if exists _assets;\r\n drop table if exists _validateddata;\r\n drop table if exists _exceptions; \r\ndrop table if exists _exceptionsdetails;\r\n drop table if exists _zoneexceptions;\r\n drop table if exists _zonesids;\r\n drop table if exists _ftmpzone;\r\n drop table if exists _ttmpdistinctzone;");
            //sResult = sResult.Replace("--drop table script", "drop table if exists _assets;\r\n drop table if exists _validateddata;\r\n drop table if exists _exceptions; \r\ndrop table if exists _exceptionsdetails;\r\n drop table if exists _zoneexceptions;\r\n drop table if exists _zonesids;\r\n drop table if exists _ftmpzone;\r\n drop table if exists _ttmpdistinctzone;");
            sResult = sResult.Replace("CREATE TABLE", "CREATE Temp TABLE");
            sResult = sResult.Replace("#Assets", "_Assets");
            sResult = sResult.Replace("RowNumber int IDENTITY(1,1) NOT NULL PRIMARY KEY ,", "RowNumber int GENERATED ALWAYS AS IDENTITY,");
            sResult = sResult.Replace("NOT NULL PRIMARY KEY CLUSTERED", "PRIMARY KEY");
            sResult = sResult.Replace("[iAssetID] [int]", "iAssetID int");
            sResult = sResult.Replace("#ValidatedData", "_ValidatedData");
            sResult = sResult.Replace("#Exceptions", "_Exceptions");
            sResult = sResult.Replace("#ExceptionsDetails", "_ExceptionsDetails");
            sResult = sResult.Replace(", convert(varchar(max), '')", ",''");
            sResult = sResult.Replace(", convert(varchar(250), '')", ",''");
            sResult = sResult.Replace("WITH (INDEX(SIndx_GFI_FLT_ZoneHeader))", "");
            sResult = sResult.Replace("#ZoneExceptions", "_ZoneExceptions");
            sResult = sResult.Replace("CONVERT(VARCHAR(10),dtLocalFrom,103)", "dtLocalFrom::date");
            sResult = sResult.Replace("CONVERT(VARCHAR(10),dtLocalFrom,108)", "dtLocalFrom::time");
            sResult = sResult.Replace(" DATEADD(MINUTE, t.TotalMinutes, e.DateTimeGPS_UTC)", "(e.DateTimeGPS_UTC + t.TotalMinutes * interval '1 minute')");
            sResult = sResult.Replace(" CONVERT(varchar, DATEADD(s, DATEDIFF(SECOND, dateFrom, dateTo), 0), 108)", "DATE_PART('second', dateTo::time - dateFrom::time)");
            sResult = sResult.Replace(" DATEADD(MINUTE, t.TotalMinutes, cte.dateFrom)", "(cte.dateFrom + t.TotalMinutes* interval '1 minute')");
            sResult = sResult.Replace(" DATEADD(MINUTE, t.TotalMinutes, cte.dateTo)", "(cte.dateTo + t.TotalMinutes* interval '1 minute')");
            sResult = sResult.Replace(" DATEADD(MINUTE, t.TotalMinutes, cte.dateTo)", "(cte.dateTo + t.TotalMinutes* interval '1 minute')");
            sResult = sResult.Replace(@"STUFF(
		                                        (SELECT distinct ', ' + e.Description FROM _ZoneExceptions e Where e.RuleID = h.RuleID and e.GroupID = h.GroupID and e.AssetID = h.AssetID and e.DateTimeGPS_UTC >= h.dateFrom and e.DateTimeGPS_UTC <= h.dateTo FOR XML PATH(''))
	                                        , 1, 1, '')", "(select array_to_string(ARRAY(SELECT distinct e.Description FROM _ZoneExceptions e Where e.RuleID = h.RuleID and e.GroupID = h.GroupID and e.AssetID = h.AssetID and e.DateTimeGPS_UTC >= h.dateFrom and e.DateTimeGPS_UTC <= h.dateTo), ', '))");

            sResult = sResult.Replace(@" update _ExceptionsDetails
	set Speed = g.Speed, lon = g.Longitude, lat = g.Latitude, roadSpeed = g.RoadSpeed, DriverID = g.DriverID
from _ExceptionsDetails e
	inner join GFI_GPS_GPSData g on e.GPSDataID = g.UID", "update _ExceptionsDetails e set Speed = g.Speed, lon = g.Longitude, lat = g.Latitude, roadSpeed = g.RoadSpeed, DriverID = g.DriverID from GFI_GPS_GPSData g where e.GPSDataID = g.UID; ");

            sResult = sResult.Replace(@"update _Exceptions
	set DriverID = d.DriverID, sDriverName = d.sDriverName, roadSpeed = t2.RoadSpeed, Lon = t2.lon, lat = t2.lat
from _Exceptions t1
	inner join _ExceptionsDetails t2 on t1.dateFrom = t2.DateTimeGPS_UTC and t1.AssetID = t2.AssetID and t1.GroupID = t2.GroupID and t1.RuleID = t2.RuleID
    inner join GFI_FLT_Driver d on d.DriverID = t2.DriverID", "update _Exceptions t1 set DriverID = d.DriverID, sDriverName = d.sDriverName, roadSpeed = t2.RoadSpeed, Lon = t2.lon, lat = t2.lat from _ExceptionsDetails t2, GFI_FLT_Driver d where t1.dateFrom = t2.DateTimeGPS_UTC and t1.AssetID = t2.AssetID and t1.GroupID = t2.GroupID and t1.RuleID = t2.RuleID and d.DriverID = t2.DriverID; ");

            sResult = sResult.Replace(@"select distinct RuleID, AssetID, GroupID, dateFrom, DateTo 
    	,DATE_PART('second', dateTo::time - dateFrom::time) AS duration
    from GFI_GPS_ExceptionsHeader h
        inner join _Assets a on h.AssetID = a.iAssetID", @"select distinct h.RuleID, h.AssetID, h.GroupID, h.dateFrom, h.DateTo 
    	,DATE_PART('second', dateTo::time - dateFrom::time) AS duration
    from GFI_GPS_ExceptionsHeader h
		inner join GFI_GPS_Exceptions e on h.groupID = e.GroupID and h.RuleID = e.RuleID and h.AssetID = e.AssetID
        inner join _Assets a on h.AssetID = a.iAssetID");
            return sResult;
        }

        static String MohRequestCode(String sql)
        {
            String sResult = sql;
            sResult = sResult.Trim() + ";\r\n";
            sResult = sResult.Replace("declare @t table", "CREATE Temp TABLE  @t");
            sResult = sResult.Replace("@t", "_t");
            sResult = sResult.Replace("exec sp_MOHRequestCode 'RET'", "select sp_MOHRequestCode ('RET');");
            //sResult = sResult.Replace("select * from _t;", "");

            return sResult;
        }

        static String PlanningRequest(String sql)
        {
            String sResult = sql;
            sResult = sResult.Trim() + ";\r\n";
            sResult = sResult.Replace("#Planning", "_Planning");
            sResult = sResult.Replace(" Convert(varchar(500), null)", "''");
            sResult = sResult.Replace(" Convert(nvarchar(max), null)", "''");
            sResult = sResult.Replace(" Convert(varchar(1024), null)", "''");

            sResult = sResult.Replace(@"update _Planning 
	set dtFrom = h.dtUTC
from _Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
where h.SeqNo = 1", @"UPDATE _Planning T1
    SET dtFrom = T2.dtUTC
FROM GFI_PLN_PlanningViaPointH T2
where T2.PID = T1.PID
   and T2.SeqNo =1");


            sResult = sResult.Replace(@"update _Planning 
	set dtTo = h.dtUTC
from _Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
where h.SeqNo = 999", @"UPDATE _Planning T1
    SET dtTo = T2.dtUTC
FROM GFI_PLN_PlanningViaPointH T2
where T2.PID = T1.PID
   and T2.SeqNo = 999");

            sResult = sResult.Replace(@"update _Planning 
	set ZoneFr = z.Description ,DepartureID =z.ZoneID
from _Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_FLT_ZoneHeader z on h.ZoneID = z.ZoneID 
where h.SeqNo = 1", @"UPDATE _Planning T1
    SET ZoneFr = T3.Description ,DepartureID =T3.ZoneID
FROM GFI_PLN_PlanningViaPointH T2, GFI_FLT_ZoneHeader T3 
where T2.PID = T1.PID
   and T2.ZoneID = T3.ZoneID and T2.SeqNo = 1");



            sResult = sResult.Replace(@"update _Planning 
	set ZoneTo = z.Description ,ArrivalID =z.ZoneID
from _Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_FLT_ZoneHeader z on h.ZoneID = z.ZoneID 
where h.SeqNo = 999", @"UPDATE _Planning T1
    SET ZoneTo = T3.Description ,ArrivalID =T3.ZoneID
FROM GFI_PLN_PlanningViaPointH T2, GFI_FLT_ZoneHeader T3 
where T2.PID = T1.PID
   and T2.ZoneID = T3.ZoneID and T2.SeqNo = 999");


            sResult = sResult.Replace(@"update _Planning 
	set CoordinatesFrom = concat(zh.Latitude ,',',zh.Longitude)
from _Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_FLT_ZoneHeader z on h.ZoneID = z.ZoneID 
	inner join GFI_FLT_ZoneDetail zh on z.ZoneID = zh.ZoneID 
where h.SeqNo = 1", @"UPDATE _Planning T1
    SET CoordinatesFrom = concat(T4.Latitude ,',',T4.Longitude)
FROM GFI_PLN_PlanningViaPointH T2, GFI_FLT_ZoneHeader T3 ,GFI_FLT_ZoneDetail T4
where T2.PID = T1.PID
    and T2.ZoneID = T3.ZoneID and  T4.ZoneID = T3.ZoneID and T2.SeqNo = 1");

            sResult = sResult.Replace(@"update _Planning 
	set CoordinatesTo = concat(zh.Latitude ,',',zh.Longitude)
from _Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_FLT_ZoneHeader z on h.ZoneID = z.ZoneID
	inner join GFI_FLT_ZoneDetail zh on z.ZoneID = zh.ZoneID  
where h.SeqNo = 999", @"UPDATE _Planning T1
    SET CoordinatesTo = concat(T4.Latitude ,',',T4.Longitude)
FROM GFI_PLN_PlanningViaPointH T2, GFI_FLT_ZoneHeader T3 ,GFI_FLT_ZoneDetail T4
where T2.PID = T1.PID
   and T2.ZoneID = T3.ZoneID and  T4.ZoneID = T3.ZoneID and T2.SeqNo = 999");

            sResult = sResult.Replace(@"update _Planning 
	set VCAFr = v.VcaName
from _Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_GIS_VCA v on h.LocalityVCA = v.VCAID 
where h.SeqNo = 1", @"UPDATE _Planning T1
    SET VCAFr = T3.VcaName
FROM GFI_PLN_PlanningViaPointH T2 ,GFI_GIS_VCA T3
where T2.PID = T1.PID
   and T3.VCAID = T2.LocalityVCA and T2.SeqNo = 1");

            sResult = sResult.Replace(@"update _Planning 
	set VCATo = v.VcaName
from _Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_GIS_VCA v on h.LocalityVCA = v.VCAID 
where h.SeqNo = 999", @"UPDATE _Planning T1
    SET VCATo = T3.VcaName
FROM GFI_PLN_PlanningViaPointH T2 ,GFI_GIS_VCA T3
where T2.PID = T1.PID
   and T3.VCAID = T2.LocalityVCA and T2.SeqNo = 999");

            sResult = sResult.Replace(@"update _Planning 
	set CoordinatesFrom = concat(vD.Latitude ,',',vD.Longitude)
from _Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_FLT_VCAHeader v on h.LocalityVCA = v.VCAID 
	inner join GFI_FLT_VCADetail vD on v.VCAID = vD.VCAID 
where h.SeqNo = 1", @"UPDATE _Planning T1
    SET CoordinatesFrom = concat(T4.Latitude ,',',T4.Longitude)
FROM GFI_PLN_PlanningViaPointH T2, GFI_FLT_VCAHeader T3 ,GFI_FLT_VCADetail T4
where T2.PID = T1.PID
   and T2.LocalityVCA = T3.VCAID and  T4.VCAID = T3.VCAID and T2.SeqNo = 1");

            sResult = sResult.Replace(@"update _Planning 
	set CoordinatesTo =concat(vD.Latitude ,',',vD.Longitude)
from _Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_FLT_VCAHeader v on h.LocalityVCA = v.VCAID 
	inner join GFI_FLT_VCADetail vD on v.VCAID = vD.VCAID 
where h.SeqNo = 999", @"UPDATE _Planning T1
    SET CoordinatesTo = concat(T4.Latitude ,',',T4.Longitude)
FROM GFI_PLN_PlanningViaPointH T2, GFI_FLT_VCAHeader T3 ,GFI_FLT_VCADetail T4
where T2.PID = T1.PID
   and T2.LocalityVCA = T3.VCAID and  T4.VCAID = T3.VCAID and T2.SeqNo = 999");



            sResult = sResult.Replace(@"update _Planning 
	set ClientFr = c.Name
from _Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_SYS_CustomerMaster c on h.ClientID = c.ClientID 
where h.SeqNo = 1", @"UPDATE _Planning T1
    SET ClientFr = T3.Name
FROM GFI_PLN_PlanningViaPointH T2 ,GFI_SYS_CustomerMaster T3
where T2.PID = T1.PID
   and T3.ClientID = T2.ClientID and T2.SeqNo = 1");


            sResult = sResult.Replace(@"update _Planning 
	set ClientTo = c.Name
from _Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_SYS_CustomerMaster c on h.ClientID = c.ClientID 
where h.SeqNo = 999", @" UPDATE _Planning T1
    SET ClientTo = T3.Name
FROM GFI_PLN_PlanningViaPointH T2 ,GFI_SYS_CustomerMaster T3
where T2.PID = T1.PID
   and T3.ClientID = T2.ClientID and T2.SeqNo = 999");





            sResult = sResult.Replace(@"update _Planning 
	set Departure = CONVERT(varchar(15), h.Lat) + ', ' + CONVERT(varchar(15), h.Lon)
from _Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
where h.SeqNo = 1 and h.Lat is not null", @"UPDATE _Planning T1
    SET Departure = CAST (T2.Lat as varchar(15)) || ', ' || CAST (T2.Lon as varchar(15))
FROM GFI_PLN_PlanningViaPointH T2
where T2.PID = T1.PID
   and T2.SeqNo = 1");


            sResult = sResult.Replace(@"update _Planning 
	set Arrival = CONVERT(varchar(15), h.Lat) + ', ' + CONVERT(varchar(15), h.Lon)
from _Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
where h.SeqNo = 999 and h.Lat is not null", @"UPDATE _Planning T1
    SET Arrival = CAST (T2.Lat as varchar(15)) || ', ' || CAST (T2.Lon as varchar(15))
FROM GFI_PLN_PlanningViaPointH T2
where T2.PID = T1.PID
   and T2.SeqNo = 999");


            sResult = sResult.Replace(@"update _Planning 
     set ProccessedDate = h.dtUTC
from _Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
where h.SeqNo = 1", @"UPDATE _Planning T1
    SET ProccessedDate = T2.dtUTC
FROM GFI_PLN_PlanningViaPointH T2
where T2.PID = T1.PID and T2.SeqNo = 1");



            sResult = sResult.Replace(@"update _Planning
     set ProcessedBy =  u.Names
	 from  _Planning p 
	 inner join GFI_SYS_USER u on u.UID = p.CreatedBy", @"UPDATE _Planning T1
    SET ProcessedBy = T2.Names
FROM GFI_SYS_USER T2
where T2.UID = T1.CreatedBy");



            //sResult = sResult.Replace("set Departure = CONVERT(varchar(15), h.Lat) + ', ' + CONVERT(varchar(15), h.Lon)", "set Departure = CAST (h.Lat as varchar(15)) || ', ' || CAST (h.Lon as varchar(15))");
            //sResult = sResult.Replace("set Arrival = CONVERT(varchar(15), h.Lat) + ', ' + CONVERT(varchar(15), h.Lon)", "set Arrival = CAST (h.Lat as varchar(15)) || ', ' || CAST (h.Lon as varchar(15))");


            return sResult;
        }

        static String ScheduleRequest(String sql)
        {
            String sResult = sql;
            sResult = sResult.Trim() + ";\r\n";
            sResult = sResult.Replace("#Planning", "_Planning");
            sResult = sResult.Replace(" Convert(varchar(500), null)", "''");
            sResult = sResult.Replace(" Convert(nvarchar(max), null)", "''");
            sResult = sResult.Replace(" Convert(varchar(1024), null)", "''");
            sResult = sResult.Replace(" Convert(int, null)", "''");
            sResult = sResult.Replace("set Departure = CONVERT(varchar(15), h.Lat) + ', ' + CONVERT(varchar(15), h.Lon)", "set Departure = CAST (h.Lat as varchar(15)) || ', ' || CAST (h.Lon as varchar(15))");
            sResult = sResult.Replace("set Arrival = CONVERT(varchar(15), h.Lat) + ', ' + CONVERT(varchar(15), h.Lon)", "set Arrival = CAST (h.Lat as varchar(15)) || ', ' || CAST (h.Lon as varchar(15))");
            sResult = sResult.Replace("(select convert(varchar, h.dtUTC, 108))", "(select h.dtUTC) ::time");

            sResult = sResult.Replace(@"update _Planning 
	set TripType = lkv.name
from _Planning p 
	inner join gfi_pln_planninglkup lkp on p.PID = lkp.PID
	inner join gfi_sys_lookupvalues lkv on lkp.lookupvalueid = lkv.vid
	inner join gfi_sys_lookuptypes lkt on lkv.tid = lkt.tid
where lkt.code = 'TRIP_TYPE'", @"UPDATE _Planning T1
   set TripType = T3.name
FROM gfi_pln_planninglkup T2,gfi_sys_lookupvalues T3 , gfi_sys_lookuptypes T4
where T2.PID = T1.PID and T3.vid = T2.lookupvalueid and T4.tid = T3.tid  and T4.code = 'TRIP_TYPE'");

            sResult = sResult.Replace(@"update _Planning 
	set dtFrom = h.dtUTC
from _Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
where h.SeqNo = 1", @"UPDATE _Planning T1
    SET dtFrom = T2.dtUTC
FROM GFI_PLN_PlanningViaPointH T2
where T2.PID = T1.PID
   and T2.SeqNo =1");

            sResult = sResult.Replace(@"update _Planning 
	set dtTo = h.dtUTC ,SoilType = h.Remarks
from _Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
where h.SeqNo = 999", @"UPDATE _Planning T1
    SET dtTo = T2.dtUTC ,SoilType = T2.Remarks
FROM GFI_PLN_PlanningViaPointH T2
where T2.PID = T1.PID
   and T2.SeqNo = 999");

            sResult = sResult.Replace(@"update _Planning 
	set StartTime = (select h.dtUTC) ::time
from _Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
where h.SeqNo = 1", @"UPDATE _Planning T1
    SET StartTime = (select T2.dtUTC :: time)
FROM GFI_PLN_PlanningViaPointH T2
where T2.PID = T1.PID
   and T2.SeqNo = 1");


            sResult = sResult.Replace(@"update _Planning 
	set EndTime = (select h.dtUTC) ::time
from _Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
where h.SeqNo = 999", @"UPDATE _Planning T1
    SET EndTime = (select T2.dtUTC :: time)
FROM GFI_PLN_PlanningViaPointH T2
where T2.PID = T1.PID
   and T2.SeqNo = 999");



            sResult = sResult.Replace(@"update _Planning 
	set ZoneFr = z.Description ,ZoneArea = z.ExternalRef ,DepartureID =z.ZoneID
from _Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_FLT_ZoneHeader z on h.ZoneID = z.ZoneID 
where h.SeqNo = 1", @"UPDATE _Planning T1
    SET ZoneFr = T3.Description , ZoneArea = T3.ExternalRef ,DepartureID =T3.ZoneID 
FROM GFI_PLN_PlanningViaPointH T2, GFI_FLT_ZoneHeader T3 
where T2.PID = T1.PID
   and T2.ZoneID = T3.ZoneID and T2.SeqNo = 1");



            sResult = sResult.Replace(@"update _Planning 
	set ZoneTo = z.Description ,ZoneArea = z.ExternalRef,ArrivalID =z.ZoneID
from _Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_FLT_ZoneHeader z on h.ZoneID = z.ZoneID 
where h.SeqNo = 999", @"UPDATE _Planning T1
    SET ZoneTo = T3.Description , ZoneArea = T3.ExternalRef ,ArrivalID =T3.ZoneID 
FROM GFI_PLN_PlanningViaPointH T2, GFI_FLT_ZoneHeader T3 
where T2.PID = T1.PID
   and T2.ZoneID = T3.ZoneID and T2.SeqNo = 999");



            sResult = sResult.Replace(@"update _Planning 
	set VCAFr = v.VCAName
from _Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_GIS_VCA v on h.LocalityVCA = v.VCAID 
where h.SeqNo = 1", @"UPDATE _Planning T1
    SET VCAFr = T3.VcaName
FROM GFI_PLN_PlanningViaPointH T2 ,GFI_GIS_VCA T3
where T2.PID = T1.PID
   and T3.VCAID = T2.LocalityVCA and T2.SeqNo = 1");


            sResult = sResult.Replace(@"update _Planning 
	set VCATo = v.VCAName
from _Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_GIS_VCA v on h.LocalityVCA = v.VCAID 
where h.SeqNo = 999", @"UPDATE _Planning T1
    SET VCATo = T3.VcaName
FROM GFI_PLN_PlanningViaPointH T2 ,GFI_GIS_VCA T3
where T2.PID = T1.PID
   and T3.VCAID = T2.LocalityVCA and T2.SeqNo = 999");




            sResult = sResult.Replace(@"update _Planning 
	set ClientFr = c.Name
from _Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_SYS_CustomerMaster c on h.ClientID = c.ClientID 
where h.SeqNo = 1", @"UPDATE _Planning T1
    SET ClientFr = T3.Name
FROM GFI_PLN_PlanningViaPointH T2 ,GFI_SYS_CustomerMaster T3
where T2.PID = T1.PID
   and T3.ClientID = T2.ClientID and T2.SeqNo = 1");


            sResult = sResult.Replace(@"update _Planning 
	set ClientTo = c.Name
from _Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_SYS_CustomerMaster c on h.ClientID = c.ClientID 
where h.SeqNo = 999", @" UPDATE _Planning T1
    SET ClientTo = T3.Name
FROM GFI_PLN_PlanningViaPointH T2 ,GFI_SYS_CustomerMaster T3
where T2.PID = T1.PID
   and T3.ClientID = T2.ClientID and T2.SeqNo = 999");





            sResult = sResult.Replace(@"update _Planning 
	set Departure = CAST (h.Lat as varchar(15)) || ', ' || CAST (h.Lon as varchar(15))
from _Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
where h.SeqNo = 1 and h.Lat is not null", @"UPDATE _Planning T1
    SET Departure = CAST (T2.Lat as varchar(15)) || ', ' || CAST (T2.Lon as varchar(15))
FROM GFI_PLN_PlanningViaPointH T2
where T2.PID = T1.PID
   and T2.SeqNo = 1");


            sResult = sResult.Replace(@"update _Planning 
	set Arrival = CAST (h.Lat as varchar(15)) || ', ' || CAST (h.Lon as varchar(15))
from _Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
where h.SeqNo = 999 and h.Lat is not null", @"UPDATE _Planning T1
    SET Arrival = CAST (T2.Lat as varchar(15)) || ', ' || CAST (T2.Lon as varchar(15))
FROM GFI_PLN_PlanningViaPointH T2
where T2.PID = T1.PID
   and T2.SeqNo = 999");


            sResult = sResult.Replace(@"update _Planning 
	set AssetID = a.AssetID
from 
 _Planning p 
	inner join GFI_PLN_ScheduledPlan sP on p.PID = sP.PID
	inner join GFI_PLN_Scheduled s on s.HID = sP.HID
	inner join GFI_PLN_ScheduledAsset sA on s.HID =sA.HID
	inner join GFI_FLT_Asset  a on a.AssetID =sA.AssetID", @"UPDATE _Planning T1
    set AssetID = T5.AssetID
FROM GFI_PLN_ScheduledPlan T2 ,GFI_PLN_Scheduled T3 ,GFI_PLN_ScheduledAsset T4 ,GFI_FLT_Asset T5 
where T2.PID = T1.PID 
 and T3.HID = T2.HID and T4.HID = T3.HID and T5.AssetID = T4.AssetID ");



            sResult = sResult.Replace(@"update _Planning 
	set Asset = a.AssetName
from 
 _Planning p 
	inner join GFI_PLN_ScheduledPlan sP on p.PID = sP.PID
	inner join GFI_PLN_Scheduled s on s.HID = sP.HID
	inner join GFI_PLN_ScheduledAsset sA on s.HID =sA.HID
	inner join GFI_FLT_Asset  a on a.AssetID =sA.AssetID", @"UPDATE _Planning T1
    set Asset = T5.AssetName
FROM GFI_PLN_ScheduledPlan T2 ,GFI_PLN_Scheduled T3 ,GFI_PLN_ScheduledAsset T4 ,GFI_FLT_Asset T5 
where T2.PID = T1.PID 
 and T3.HID = T2.HID and T4.HID = T3.HID and T5.AssetID = T4.AssetID ");

            sResult = sResult.Replace(@"update _Planning 
	set Driver = d.sDriverName
from 
 _Planning p 
	inner join GFI_PLN_ScheduledPlan sP on p.PID = sP.PID
	inner join GFI_PLN_Scheduled s on s.HID = sP.HID
	inner join GFI_PLN_ScheduledAsset sA on s.HID =sA.HID
	inner join GFI_FLT_Driver  d on d.DriverID =sA.DriverID", @"UPDATE _Planning T1
    set Driver = T5.sDriverName
FROM GFI_PLN_ScheduledPlan T2 ,GFI_PLN_Scheduled T3 ,GFI_PLN_ScheduledAsset T4 ,GFI_FLT_Driver T5 
where T2.PID = T1.PID 
 and T3.HID = T2.HID and T4.HID = T3.HID and T5.DriverID = T4.DriverID  ");


            sResult = sResult.Replace(@"update _Planning 
	set DriverID = d.DriverID
from 
 _Planning p 
	inner join GFI_PLN_ScheduledPlan sP on p.PID = sP.PID
	inner join GFI_PLN_Scheduled s on s.HID = sP.HID
	inner join GFI_PLN_ScheduledAsset sA on s.HID =sA.HID
	inner join GFI_FLT_Driver  d on d.DriverID =sA.DriverID", @"UPDATE _Planning T1
    set DriverID = T5.DriverID
FROM GFI_PLN_ScheduledPlan T2 ,GFI_PLN_Scheduled T3 ,GFI_PLN_ScheduledAsset T4 ,GFI_FLT_Driver T5 
where T2.PID = T1.PID 
 and T3.HID = T2.HID and T4.HID = T3.HID and T5.DriverID = T4.DriverID ");



            sResult = sResult.Replace(@"update _Planning 
	set ScheduleId = s.HID
from 
 _Planning p 
	inner join GFI_PLN_ScheduledPlan sP on p.PID = sP.PID
	inner join GFI_PLN_Scheduled s on s.HID = sP.HID", @"UPDATE _Planning T1
    set ScheduleId = T3.HID
FROM GFI_PLN_ScheduledPlan T2,GFI_PLN_Scheduled T3
where T2.PID = T1.PID and T3.HID = T2.HID");


            sResult = sResult.Replace(@"update _Planning 
	set RequestDate = p.dtFrom
from _Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID", @"UPDATE _Planning T1
    set RequestDate = T1.dtFrom
FROM GFI_PLN_PlanningViaPointH T2
where T2.PID = T1.PID");


            sResult = sResult.Replace(@"update _Planning 
	set ScheduledDate = s.CreatedDate
from 
 _Planning p 
	inner join GFI_PLN_ScheduledPlan sP on p.PID = sP.PID
	inner join GFI_PLN_Scheduled s on s.HID = sP.HID", @"UPDATE _Planning T1
    set ScheduledDate = T3.CreatedDate
FROM GFI_PLN_ScheduledPlan T2,GFI_PLN_Scheduled T3
where T2.PID = T1.PID and T3.HID = T2.HID");



            sResult = sResult.Replace(@"update _Planning 
	set CoordinatesFrom = concat(zh.Latitude ,',',zh.Longitude)
from _Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_FLT_ZoneHeader z on h.ZoneID = z.ZoneID 
	inner join GFI_FLT_ZoneDetail zh on z.ZoneID = zh.ZoneID 
where h.SeqNo = 1", @"update _Planning T1
     set CoordinatesFrom = concat(T4.Latitude ,',',T4.Longitude)
from GFI_PLN_PlanningViaPointH T2 , GFI_FLT_ZoneHeader T3 , GFI_FLT_ZoneDetail T4
where T1.PID = T2.PID and T2.ZoneID = T3.ZoneID and T4.ZoneID = T3.ZoneID
and T2.SeqNo  = 1");

            sResult = sResult.Replace(@"update _Planning 
	set CoordinatesTo = concat(zh.Latitude ,',',zh.Longitude)
from _Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_FLT_ZoneHeader z on h.ZoneID = z.ZoneID
	inner join GFI_FLT_ZoneDetail zh on z.ZoneID = zh.ZoneID  
where h.SeqNo = 999", @"update _Planning T1
     set CoordinatesTo = concat(t4.Latitude ,',',t4.Longitude)
from GFI_PLN_PlanningViaPointH T2 , GFI_FLT_ZoneHeader T3 , GFI_FLT_ZoneDetail T4
where T1.PID = T2.PID and T2.ZoneID = T3.ZoneID and T4.ZoneID = T3.ZoneID
and  T2.SeqNo  = 999");

            return sResult;
        }

        static String PlanningActual(String sql)
        {
            String sResult = sql;
            sResult = sResult.Trim() + ";\r\n";
            sResult = sResult.Replace(", DATEADD(mi, tz.TotalMinutes, srp.dtUTC)", ",viaPointH.dtUTC + tz.TotalMinutes * INTERVAL '1 minute'");
            sResult = sResult.Replace("	, DATEADD(mi, -viaPointH.Buffer,  DATEADD(mi, tz.TotalMinutes, srp.dtUTC))", ",(srp.dtUTC + tz.TotalMinutes * INTERVAL '1 minute') - viaPointH.Buffer * INTERVAL '1 minute'");
            sResult = sResult.Replace("	, DATEADD(mi, viaPointH.Buffer,  DATEADD(mi, tz.TotalMinutes, srp.dtUTC))", ",(srp.dtUTC + tz.TotalMinutes * INTERVAL '1 minute') + viaPointH.Buffer * INTERVAL '1 minute'");

            //sResult = sResult.Replace(", DATEADD(mi, tz.TotalMinutes, srp.dtUTC) as schdtUTC", ",srp.dtUTC + tz.TotalMinutes * INTERVAL '1 minute'  as schdtUTC");
            //sResult = sResult.Replace("	, DATEADD(mi, -viaPointH.Buffer,  DATEADD(mi, tz.TotalMinutes, srp.dtUTC))) as schminTime", ",(srp.dtUTC + tz.TotalMinutes * INTERVAL '1 minute') - viaPointH.Buffer * INTERVAL '1 minute'  as schminTime");
            //sResult = sResult.Replace("	, DATEADD(mi, viaPointH.Buffer,  DATEADD(mi, tz.TotalMinutes, srp.dtUTC)) as schmaxTime", ",(srp.dtUTC + tz.TotalMinutes * INTERVAL '1 minute') + viaPointH.Buffer * INTERVAL '1 minute'  as schmaxTime");

            return sResult;
        }

        static String ZoneVisitedReport(String sql)
        {
            string[] lines = sql.Split('\n');
            String _IdlingSpeed = "50";
            String _IdleDuration = "(60 * 5)";

            for (int i = 0; i < lines.Length; i++)
            {
                String line = lines[i].Trim();
                if (line.Contains("set @IdlingSpeed ="))
                {
                    String[] s = line.Trim().Split(' ');
                    _IdlingSpeed = s[s.Length - 1];
                }

                if (line.Contains("set @IdleDuration ="))
                {
                    String[] s = line.Trim().Split(' ');
                    _IdleDuration = "(60 * " + s[s.Length - 1];
                    break;
                }
            }

            String sResult = sql;
            sResult = sResult.Replace("--ZoneVisitedReport", @"--ZoneVisitedReport
drop table if exists _assets;
drop table if exists _trip;
drop table if exists _idles;
drop table if exists _zonesids;
drop table if exists _ftmpzone;
drop table if exists _ttmpdistinctzone;
drop table if exists _zonefilter;
drop table if exists _pivotdata;
drop table if exists _arrivalzonetrips;
drop table if exists _tmpidles;
drop table if exists _zonesvisited;");
            sResult = sResult.Trim() + ";\r\n";
            sResult = sResult.Replace("@", "_");
            sResult = sResult.Replace("#", "_");
            sResult = sResult.Replace(" convert(nvarchar(20), null)", "''");

            sResult = sResult.Replace("CREATE TABLE", "CREATE TEMP TABLE");
            sResult = sResult.Replace("into _trip", "into temp _trip");
            sResult = sResult.Replace("into _idles", "into temp _idles");
            sResult = sResult.Replace("into _ArrivalZoneTrips", "into temp _ArrivalZoneTrips");
            sResult = sResult.Replace("into _tmpIdles", "into temp _tmpIdles");
            sResult = sResult.Replace("into _ZonesVisited", "into temp _ZonesVisited");
            sResult = sResult.Replace("into _PivotData", "into temp _PivotData");

            sResult = sResult.Replace("Declare _", "--Declare _");
            sResult = sResult.Replace("set _", "--set _");

            sResult = sResult.Replace("RowNumber int IDENTITY(1,1) NOT NULL,", "RowNumber int GENERATED ALWAYS AS IDENTITY,");
            sResult = sResult.Replace(" CLUSTERED", String.Empty);
            sResult = sResult.Replace(" rownum = ROW_NUMBER() OVER (PARTITION by g.AssetID ORDER BY g.DateTimeGPS_UTC)", "ROW_NUMBER() OVER (PARTITION by g.AssetID ORDER BY g.DateTimeGPS_UTC) as rownum");
            sResult = sResult.Replace(" rownum = ROW_NUMBER() OVER (PARTITION by g.AssetID ORDER BY g.DateTimeGPS_UTC)", "ROW_NUMBER() OVER (PARTITION by g.AssetID ORDER BY g.DateTimeGPS_UTC) as rownum");
            sResult = sResult.Replace(" CONVERT(VARCHAR(11), DATEADD(mi, a.TimeZone, g.DateTimeGPS_UTC), 106)", " to_char((g.DateTimeGPS_UTC + a.TimeZone *  INTERVAL '1 minute') :: date, 'DD Mon YYYY')");

            sResult = sResult.Replace("convert(varchar(20), null)", "''");
            sResult = sResult.Replace("convert(varchar(2500), null)", "''");

            sResult = sResult.Replace(@"STUFF(
		                (SELECT ', ' + e.Description FROM _ArrivalZoneTrips e Where e.UID = h.UID FOR XML PATH(''))
	                , 1, 1, '')", "(select array_to_string(ARRAY(SELECT distinct e.Description FROM _ArrivalZoneTrips e Where e.UID = h.UID), ', '))");
            sResult = sResult.Replace(@"STUFF(
		                (SELECT ', ' + CONVERT(varchar(25), e.ZoneID) FROM _ArrivalZoneTrips e Where e.UID = h.UID FOR XML PATH(''))
	                , 1, 1, '')", "(select array_to_string(ARRAY(SELECT distinct e.ZoneID FROM _ArrivalZoneTrips e Where e.UID = h.UID), ', '))");
            sResult = sResult.Replace("SELECT top 1 e.ZoneID FROM _ArrivalZoneTrips e Where e.UID = h.UID", "SELECT e.ZoneID FROM _ArrivalZoneTrips e Where e.UID = h.UID limit 1");
            sResult = sResult.Replace("SELECT top 1 e.Description FROM _ArrivalZoneTrips e Where e.UID = h.UID", "SELECT e.Description FROM _ArrivalZoneTrips e Where e.UID = h.UID limit 1");
            sResult = sResult.Replace("SELECT top 1 e.Color FROM _ArrivalZoneTrips e Where e.UID = h.UID", "SELECT e.Color FROM _ArrivalZoneTrips e Where e.UID = h.UID limit 1");

            sResult = sResult.Replace("--and (RowNumByGrp % 10) = 1", String.Empty);
            sResult = sResult.Replace("alter table _idles add RN int", "alter table _idles add RN int;");
            sResult = sResult.Replace("update _idles set FG = null where iZone is null", "update _idles set FG = null where iZone is null;");
            sResult = sResult.Replace("update _idles set FG = null where iZone is null", "update _idles set FG = null where iZone is null;");
            sResult = sResult.Replace("alter table _idles Drop column RN ", "alter table _idles Drop column RN;");
            sResult = sResult.Replace("alter table _idles drop column FG", "alter table _idles drop column FG;");

            sResult = sResult.Replace("as TableName", "as TableName;");
            sResult = sResult.Replace("CONVERT(VARCHAR, DATEADD(mi, a.TimeZone, g.DateTimeGPS_UTC), 108) dtLocalTime", "DATEADD('mi', a.TimeZone, g.DateTimeGPS_UTC)::timestamptz dtLocalTime");

            sResult = sResult.Replace("CONVERT(varchar, DATEADD(s, Duration, 0), 108)", "cast(Duration as int) * interval '1 second'");
            sResult = sResult.Replace("FinalDuration < _IdleDuration", "FinalDuration < _IdleDuration;");
            sResult = sResult.Replace("_IdlingSpeed", _IdlingSpeed);
            sResult = sResult.Replace("_IdleDuration", _IdleDuration);
            sResult = sResult.Replace("DATEDIFF(ss, min(DateTimeGPS_UTC)", "DATEDIFF('ss', min(DateTimeGPS_UTC)");

            sResult = sResult.Replace("zh.GeomData.STIntersects(h.pointFr) = 1", "ST_Intersects(zh.GeomData, h.pointFr)");
            sResult = sResult.Replace("CONVERT(date, DateTimeGPS_UTC)", "DateTimeGPS_UTC::date");
            sResult = sResult.Replace("null Final", "0 Final");
            sResult = sResult.Replace("null RowNumByGrp", "0 RowNumByGrp");
            sResult = sResult.Replace("DATEADD(second", "DATEADD('ss'");

            sResult = sResult.Replace(@"update _idles 
    set sDuration = CAST(FLOOR(FinalDuration / 86400) AS VARCHAR(10)) + ' Day(s), ' +
						CONVERT(VARCHAR, DATEADD(SECOND, FinalDuration, '19000101'), 8)", @"update _idles 
    set sDuration = CAST(FLOOR(FinalDuration / 86400) AS VARCHAR(10)) || ' Day(s), '
					|| cast(DATEADD('ss', FinalDuration, '19000101'::timestamptz) as time)");

            sResult = sResult.Replace("DATEADD('ss', FinalDuration * -1, dtLocalTime) + dtLocal", "DATEADD('ss', FinalDuration * -1, dtLocalTime)");

            sResult = sResult.Substring(0, sResult.LastIndexOf("--Pivot Table"));
            return sResult;
        }

        static String FuelConsumption(String sql)
        {
            String sResult = sql;
            sResult = sResult.Trim() + ";\r\n";
            sResult = sResult.Replace("into #Fill", "into temp _Fill");
            sResult = sResult.Replace("#Fill", "_Fill");
            sResult = sResult.Replace("Top 10", " ");
            sResult = sResult.Replace(" DATEADD(mi, T.TotalMinutes, f.DateTimeGPS_UTC)", "f.DateTimeGPS_UTC + t.TotalMinutes * INTERVAL '1 minute'");
            sResult = sResult.Replace("order by f.dateTimeGPS_UTC desc", "order by f.dateTimeGPS_UTC desc limit 10");
            sResult = sResult.Replace("	order by f.DateTimeGPS_UTC desc --secondOrder", "order by f.dateTimeGPS_UTC desc limit 1");
            sResult = sResult.Replace(", ROUND(c.Consumption, 1)", ",ROUND(c.Consumption::numeric, 1)");
            sResult = sResult.Replace(", ROUND(c.Distance, 1)", ",ROUND(c.Distance::numeric, 1)");
            sResult = sResult.Replace("ROUND(sum(Consumption) / count(1), 1)", "ROUND(sum(Consumption::numeric) / count(1), 1)");
            sResult = sResult.Replace("top 1", " ");
            sResult = sResult.Replace("	DATEADD(mi, T.TotalMinutes, f.DateTimeGPS_UTC)", "f.DateTimeGPS_UTC + t.TotalMinutes * INTERVAL '1 minute' ");

            return sResult;
        }

        static String TripsProcessor(String sql)
        {
            String sResult = sql;
            string[] lines = sResult.Split('\n');
            int iAssetID = -1;
            for (int i = 0; i < lines.Length; i++)
            {
                String line = lines[i].Trim();
                if (line.StartsWith("set @ActualAssetID =", StringComparison.CurrentCultureIgnoreCase))
                {
                    String sAssetID = line.Split('=')[1];
                    if (int.TryParse(sAssetID, out iAssetID))
                        sResult = NaveoIntel.ProcessIntel.sPostgreSQLTrips(iAssetID);
                    break;
                }
            }

            return sResult;
        }
        static String ExceptionsProcessor(String sql)
        {
            String sResult = sql;
            string[] lines = sResult.Split('\n');
            int iAssetID = -1;
            for (int i = 0; i < lines.Length; i++)
            {
                String line = lines[i].Trim();
                if (line.StartsWith("set @ActualAssetID =", StringComparison.CurrentCultureIgnoreCase))
                {
                    String sAssetID = line.Split('=')[1];
                    if (int.TryParse(sAssetID, out iAssetID))
                        sResult = NaveoIntel.ProcessIntel.sProcessExceptionsPostgreSQL(iAssetID);
                    break;
                }
            }

            return sResult;
        }

        static String TripAddresses(String sql)
        {
            String sResult = sql;
            sResult = sResult.Replace("create table #", "create temp table #");
            sResult = sResult.Replace("#", "_");

            return sResult;
        }

        static String getAfterDoCmd(String sql)
        {
            String sResult = String.Empty;
            sql = sql.Replace("--PosgreSQLMoveDownStart", "--[PosgreSQLMoveDownStart");
            sql = sql.Replace("--PosgreSQLMoveDownEnd", "--PosgreSQLMoveDownEnd]");
            sql = sql.Replace("#", "_");

            string[] lines = sql.Split('\n');
            for (int i = 0; i < lines.Length; i++)
            {
                String line = lines[i];
                if (line.StartsWith("--[PosgreSQLMoveDownStart"))
                {
                    String sTH = GetNestedString(i, sql, '[', ']');
                    sAfterDoCmd = sTH;

                    int numLines = sTH.Split('\n').Length;
                    i += numLines;
                    continue;
                }
                else
                {
                    sResult += line;
                    sResult += "\n";
                }
            }
            return sResult;
        }

        static String TripsRetrieval(String sql)
        {
            String sResult = String.Empty;
            String[] tmp1 = sql.Split(new string[] { "h.TripDistance >= " }, StringSplitOptions.RemoveEmptyEntries);
            Double.TryParse(tmp1[1].Split(' ')[0], out double dTripDistance);

            sResult = getAfterDoCmd(sql);
            sResult = sResult.Replace("CREATE TABLE", "CREATE TEMP TABLE");
            sResult = sResult.Replace(@"GFI_GPS_TripHeader
(
	RowNumber int IDENTITY(1,1) NOT NULL,", @"GFI_GPS_TripHeader
(
	RowNumber int DEFAULT NEXTVAL ('_GFI_GPS_TripHeader_seq') NOT NULL,");

            sResult = sResult.Replace(@"GFI_GPS_GPSData 
(
	--RowNumber int IDENTITY(1,1) NOT NULL,
	--UID int PRIMARY KEY ,
	RowNumber int IDENTITY(1,1) PRIMARY KEY  NOT NULL", @"GFI_GPS_GPSData 
(
	--RowNumber int IDENTITY(1,1) NOT NULL,
	--UID int PRIMARY KEY ,
	RowNumber int DEFAULT NEXTVAL ('_GFI_GPS_GPSData_seq') PRIMARY KEY  NOT NULL");

            sResult = sResult.Replace(@"Assets
(
	RowNumber int IDENTITY(1,1) NOT NULL PRIMARY KEY", @"Assets
(
	RowNumber int DEFAULT NEXTVAL ('_Assets_seq') NOT NULL PRIMARY KEY");

            sResult = sResult.Replace("--Creating temp tables", @"
--Creating temp tables
CREATE TEMPORARY SEQUENCE _GFI_GPS_TripHeader_seq START WITH 1 INCREMENT BY 1;
CREATE TEMPORARY SEQUENCE _GFI_GPS_GPSData_seq START WITH 1 INCREMENT BY 1;
CREATE TEMPORARY SEQUENCE _Assets_seq START WITH 1 INCREMENT BY 1;
");

            sResult = sResult.Replace(@"UPDATE _Assets
    SET Asset = T2.AssetNumber, TimeZone = T3.TotalMinutes
FROM _Assets T1
    INNER JOIN _FltAssets T2 ON T2.AssetID = T1.iAssetID
    INNER JOIN GFI_SYS_TimeZones T3 on T2.TimeZoneID = T3.StandardName
", @"UPDATE _Assets T1
    SET Asset = T2.AssetNumber, TimeZone = T3.TotalMinutes
FROM _FltAssets T2, GFI_SYS_TimeZones T3 
where T2.AssetID = T1.iAssetID
   and T2.TimeZoneID = T3.StandardName 
");
            sResult = sResult.Replace(@"                        update _GFI_GPS_TripHeader T1 
                            set sArrivalZones = (select array_to_string(ARRAY(SELECT e.Description FROM _ArrivalZoneTrips e Where e.iID = h.iID), ', ')),
                                iArrivalZones = (select array_to_string(ARRAY(SELECT cast(e.ZoneID as varchar(10)) FROM _ArrivalZoneTrips e Where e.iID = h.iID), ', ')),
                                sArrivalZonesColor = (select array_to_string(ARRAY(SELECT cast(e.Color as varchar(10)) FROM _ArrivalZoneTrips e Where e.iID = h.iID), ', ')),
                                iArrivalZone = (SELECT e.ZoneID FROM _ArrivalZoneTrips e Where e.iID = h.iID limit 1 ),
                                sArrivalZone = (SELECT e.Description FROM _ArrivalZoneTrips e Where e.iID = h.iID limit 1 ),
                                iArrivalZoneColor = (SELECT e.Color FROM _ArrivalZoneTrips e Where e.iID = h.iID limit 1 )
                        from _GFI_GPS_TripHeader h", @"                        update _GFI_GPS_TripHeader h 
                            set sArrivalZones = (select array_to_string(ARRAY(SELECT e.Description FROM _ArrivalZoneTrips e Where e.iID = h.iID), ', ')),
                                iArrivalZones = (select array_to_string(ARRAY(SELECT cast(e.ZoneID as varchar(10)) FROM _ArrivalZoneTrips e Where e.iID = h.iID), ', ')),
                                sArrivalZonesColor = (select array_to_string(ARRAY(SELECT cast(e.Color as varchar(10)) FROM _ArrivalZoneTrips e Where e.iID = h.iID), ', ')),
                                iArrivalZone = (SELECT e.ZoneID FROM _ArrivalZoneTrips e Where e.iID = h.iID limit 1 ),
                                sArrivalZone = (SELECT e.Description FROM _ArrivalZoneTrips e Where e.iID = h.iID limit 1 ),
                                iArrivalZoneColor = (SELECT e.Color FROM _ArrivalZoneTrips e Where e.iID = h.iID limit 1 )
                        --from _GFI_GPS_TripHeader h");
            sResult = sResult.Replace(@"update _GFI_GPS_TripHeader T1 
                            set iDepartureZone = (SELECT e.ZoneID FROM _DepartureZoneTrips e Where e.iID = h.iID limit 1 ),
                                sDepartureZone = (SELECT e.Description FROM _DepartureZoneTrips e Where e.iID = h.iID limit 1 ),
                                iDepartureZoneColor = (SELECT e.Color FROM _DepartureZoneTrips e Where e.iID = h.iID limit 1 )
                        from _GFI_GPS_TripHeader h", @"update _GFI_GPS_TripHeader h 
                            set iDepartureZone = (SELECT e.ZoneID FROM _DepartureZoneTrips e Where e.iID = h.iID limit 1 ),
                                sDepartureZone = (SELECT e.Description FROM _DepartureZoneTrips e Where e.iID = h.iID limit 1 ),
                                iDepartureZoneColor = (SELECT e.Color FROM _DepartureZoneTrips e Where e.iID = h.iID limit 1 )
                        --from _GFI_GPS_TripHeader h");
            sResult = sResult.Replace(@"update _GFI_GPS_TripHeader 
    set exceptionsCnt = ctf.cnt
from _GFI_GPS_TripHeader th
	inner join ctf on th.iID = ctf.iID
", @"update _GFI_GPS_TripHeader th 
    set exceptionsCnt = ctf.cnt
from ctf where th.iID = ctf.iID

");

            sResult = sResult.Replace("select 1 x into _myAssets   --Remove from Oracle", "CREATE TEMP TABLE _myAssets as select 1 x;");
            sResult = sResult.Replace("delete _GFI_GPS_TripHeader", "delete from _GFI_GPS_TripHeader");

            sResult = sResult.Replace("SELECT Distinct AssetID", @"
drop table if exists _FltAssets;
CREATE TEMP TABLE _FltAssets AS
    SELECT Distinct AssetID");
            sResult = sResult.Replace("into _FltAssets", String.Empty);
            sResult = sResult.Replace(@"
 

			    order by g.DateTimeGPS_UTC  --OracleAddSemiColumn", "order by g.DateTimeGPS_UTC;");
            sResult = sResult.Replace("begin --then", "then");
            sResult = sResult.Replace("end --end if", "end if;");
            sResult = sResult.Replace("end --Remove from Oracle", "--end --Remove from Oracle");
            sResult = sResult.Replace("begin --Remove from Oracle", "--begin --Remove from Oracle");
            sResult = sResult.Replace("--OracleAddSemiColumn", ";");

            sResult = sResult.Replace(@"with cte as
                        (
	                        select h.*, z.*
", @"drop table if exists _ArrivalZoneTripsTmp;
CREATE TEMP TABLE _ArrivalZoneTripsTmp AS
with cte as
                        (
	                        select h.*, z.* --ArrivalZoneTripsTmp
");
            sResult = sResult.Replace("into _ArrivalZoneTripsTmp", String.Empty);

            sResult = sResult.Replace("select t1.* into _ArrivalZoneTrips", "CREATE TEMP TABLE _ArrivalZoneTrips AS select t1.*");

            sResult = sResult.Replace(@"STUFF(
		                                        (SELECT ', ' + e.Description FROM _ArrivalZoneTrips e Where e.iID = h.iID FOR XML PATH(''))
	                                        , 1, 1, '')", "(select array_to_string(ARRAY(SELECT e.Description FROM _ArrivalZoneTrips e Where e.iID = T1.iID), ', '))");

            sResult = sResult.Replace(@"STUFF(
		                                        (SELECT ', ' + CONVERT(varchar(10), e.ZoneID) FROM _ArrivalZoneTrips e Where e.iID = h.iID FOR XML PATH(''))
	                                        , 1, 1, '')", "(select array_to_string(ARRAY(SELECT cast(e.ZoneID as varchar(10)) FROM _ArrivalZoneTrips e Where e.iID = T1.iID), ', '))");

            sResult = sResult.Replace(@"STUFF(
		                                        (SELECT ', ' + CONVERT(varchar(10), e.Color) FROM _ArrivalZoneTrips e Where e.iID = h.iID FOR XML PATH(''))
	                                        , 1, 1, '')", "(select array_to_string(ARRAY(SELECT cast(e.Color as varchar(10)) FROM _ArrivalZoneTrips e Where e.iID = T1.iID), ', '))");

            sResult = sResult.Replace("SELECT top 1 e.ZoneID FROM _ArrivalZoneTrips e Where e.iID = h.iID", "SELECT e.ZoneID FROM _ArrivalZoneTrips e Where e.iID = T1.iID limit 1");
            sResult = sResult.Replace("SELECT top 1 e.Description FROM _ArrivalZoneTrips e Where e.iID = h.iID", "SELECT e.Description FROM _ArrivalZoneTrips e Where e.iID = T1.iID limit 1");
            sResult = sResult.Replace("SELECT top 1 e.Color FROM _ArrivalZoneTrips e Where e.iID = h.iID", "SELECT e.Color FROM _ArrivalZoneTrips e Where e.iID = T1.iID limit 1");

            sResult = sResult.Replace(@"with cte as
                        (
	                        select h.*, z.* --DepartureZoneTripsTmp
", @"drop table if exists _DepartureZoneTripsTmp;
CREATE TEMP TABLE _DepartureZoneTripsTmp AS
with cte as
                        (
	                        select h.*, z.* --DepartureZoneTripsTmp
");
            sResult = sResult.Replace("into _DepartureZoneTripsTmp", String.Empty);
            sResult = sResult.Replace("SELECT top 1 e.ZoneID FROM _DepartureZoneTrips e Where e.iID = h.iID", "SELECT e.ZoneID FROM _DepartureZoneTrips e Where e.iID = T1.iID limit 1");
            sResult = sResult.Replace("SELECT top 1 e.Description FROM _DepartureZoneTrips e Where e.iID = h.iID", "SELECT e.Description FROM _DepartureZoneTrips e Where e.iID = T1.iID limit 1");
            sResult = sResult.Replace("SELECT top 1 e.Color FROM _DepartureZoneTrips e Where e.iID = h.iID", "SELECT e.Color FROM _DepartureZoneTrips e Where e.iID = T1.iID limit 1");
            sResult = sResult.Replace(@"
                        from _GFI_GPS_TripHeader h", ";");

            sResult = sResult.Replace("select t1.* into _DepartureZoneTrips", @"drop table if exists _DepartureZoneTrips;
CREATE TEMP TABLE _DepartureZoneTrips AS
    select t1.* ");

            sResult = sResult.Replace("SELECT u.* into _FltDrivers", @"drop table if exists _FltDrivers;
CREATE TEMP TABLE _FltDrivers AS
    select u.* ");

            sResult = sResult.Replace("--OracleTHReplaceEnds0", String.Empty);

            sResult = sResult.Replace("--SelectIntoStarts. Exceptions Codes Starting", @"drop table if exists _Exceptions;
CREATE TEMP TABLE _Exceptions AS");
            sResult = sResult.Replace("into _Exceptions", String.Empty);

            sResult = sResult.Replace("select t1.* into _ZoneExceptions", @"drop table if exists _ZoneExceptions;
CREATE TEMP TABLE _ZoneExceptions AS
    select t1.* ");

            sResult = sResult.Replace(@"STUFF(
		                                        (SELECT distinct ', ' + e.Description FROM _ZoneExceptions e Where e.GPSDataID = h.GPSDataID FOR XML PATH(''))
	                                        , 1, 1, '')", "(select array_to_string(ARRAY(SELECT e.Description FROM _ZoneExceptions e Where e.GPSDataID = h.GPSDataID), ', '))");

            sResult = sResult.Replace("SELECT 'dtExceptionHeader' AS TableName", "--SELECT 'dtExceptionHeader' AS TableName");
            sResult = sResult.Replace("select r.ruleID, r.ruleName, count(1) cnt from _Exceptions r group by r.ruleID, r.ruleName", "--select r.ruleID, r.ruleName, count(1) cnt from _Exceptions r group by r.ruleID, r.ruleName");

            sResult = sResult.Replace("--SelectIntoStarts ExceptionDetails", @"drop table if exists _ExceptionDetails;
CREATE TEMP TABLE _ExceptionDetails AS");
            sResult = sResult.Replace("into _ExceptionDetails", String.Empty);
            sResult = sResult.Replace("-- into _PostGre.ExceptionDetails", " into _ExceptionDetails");

            sResult = sResult.Replace("SELECT 'dtExceptionDetails' AS TableName", "--SELECT 'dtExceptionDetails' AS TableName");
            sResult = sResult.Replace("select * from #ExceptionDetails --Oracle Add SemiColumn", "--select * from #ExceptionDetails --Oracle Add SemiColumn");

            sResult = sResult.Replace("SELECT 'TripHeader' AS TableName", "--SELECT 'TripHeader' AS TableName");

            sResult = sResult.Replace(@"with cte as
                        (
	                        select h.*, z.* --ArrivalZoneTripsTmp", @"drop table if exists _ArrivalZoneTripsTmp;
CREATE TEMP TABLE _ArrivalZoneTripsTmp AS
with cte as
                        (
	                        select h.*, z.* --ArrivalZoneTripsTmp
");
            sResult = sResult.Replace("into _ArrivalZoneTripsTmp", String.Empty);
            sResult = sResult.Replace("--OracleTHReplaceStarts1", ";");
            sResult = sResult.Replace("--OracleTHReplaceEnds1", String.Empty);

            sResult = sResult.Replace("ROUND(h.TripDistance,1) TripDistance", "ROUND(h.TripDistance::numeric, 1) TripDistance");

            sResult = sResult.Replace("WITH --ZoneMatrixRetrieval", "with recursive --ZoneMatrixRetrieval");

            sResult = sResult.Replace("FROM _GFI_GPS_TripHeader T1", "--FROM _GFI_GPS_TripHeader T1");
            sResult = sResult.Replace("INNER JOIN _Assets T2 ON T2.iAssetID = T1.AssetID", "from _Assets T2 where T2.iAssetID = T1.AssetID");
            sResult = sResult.ReplaceIgnoreCase("update _GFI_GPS_TripHeader", "update _GFI_GPS_TripHeader T1");
            sResult = sResult.Replace("update _GFI_GPS_TripHeader T1 th", "update _GFI_GPS_TripHeader th");
            sResult = sResult.Replace("CAST(DATEADD(second, T1.IdlingTime, '1900-01-01') AS TIME)", "T1.IdlingTime * interval '1 second'");
            sResult = sResult.Replace("CAST(DATEADD(second, T1.StopTime, '1900-01-01') AS TIME)", "T1.StopTime * interval '1 second'");
            sResult = sResult.Replace("CAST(DATEADD(second, T1.TripTime, '1900-01-01') AS TIME)", "T1.TripTime * interval '1 second'");
            sResult = sResult.Replace("CAST(DATEADD(second, T1.OverSpeed1Time, '1900-01-01') AS TIME)", "T1.OverSpeed1Time * interval '1 second'");
            sResult = sResult.Replace("CAST(DATEADD(second, T1.OverSpeed2Time, '1900-01-01') AS TIME)", "T1.OverSpeed2Time * interval '1 second'");
            sResult = sResult.Replace("CONVERT(date, DATEADD(mi, T2.TimeZone, dtStart))", "cast(DATEADD('mi', T2.TimeZone, dtStart) as date)");
            sResult = sResult.Replace("INNER JOIN _FltDrivers T2 ON T2.DriverID = T1.DriverID", "from _FltDrivers T2 where T2.DriverID = T1.DriverID");
            return sResult;
        }

        static String TripDetails(String sql)
        {
            String sResult = sql;
            sResult = sResult.Replace("CREATE TABLE #TripIDs", "CREATE TEMPORARY SEQUENCE _TripIDs_seq START WITH 1 INCREMENT BY 1; CREATE TEMP TABLE _TripIDs");
            sResult = sResult.Replace("IDENTITY(1,1) NOT NULL,", "GENERATED ALWAYS AS IDENTITY,");

            sResult = sResult.Replace(@"update #td 
	set RuleName = r.RuleName, RuleID = r.ParentRuleID, GrpID = e.GroupID , Duration = CONVERT(varchar, DATEADD(s, DATEDIFF(SECOND, eh1.dateFrom, eh1.dateTo), 0), 108)
from #td f
	inner join GFI_GPS_Exceptions e on f.UID = e.GPSDataID
	inner join GFI_GPS_Rules r on e.RuleID = r.RuleID
    inner join gfi_gps_exceptionsheader eh1 on eh1.RuleID = e.RuleID and  eh1.AssetID = e.AssetID and  eh1.GroupID = e.GroupID
", @"update #td f
	set RuleName = r.RuleName, RuleID = r.ParentRuleID, GrpID = e.GroupID ,Duration = eh.dateTo - eh.dateFrom
from GFI_GPS_Exceptions e, GFI_GPS_Rules r , gfi_gps_exceptionsheader eh
where f.UID = e.GPSDataID and e.RuleID = r.RuleID and eh.ruleid  = e.ruleid and eh.groupid  = e.groupid and eh.assetid  = e.assetid
");

            sResult = sResult.Replace(@"with cte as
	(select *, row_number() over(partition by AssetID, GrpID, RuleID order by localTime) as ExpRowNum from #td where RuleID is not null)
update #td 
	set ExcpStart = 1
from #td 
	inner join cte on  #td.RowNum = cte.RowNum
where cte.ExpRowNum = 1", @"with cte as
	(select *, row_number() over(partition by AssetID, GrpID, RuleID order by localTime) as ExpRowNum from #td where RuleID is not null)
update #td 
	set ExcpStart = 1
from cte 
where #td.RowNum = cte.RowNum and cte.ExpRowNum = 1");

            sResult = sResult.Replace("#", "_");

            sResult = sResult.Replace("\r\ninto _td", "\r\n --into _td");
            sResult = sResult.Replace("--SelectIntoStarts", @"--SelectIntoStarts
drop table if exists _td;
CREATE TEMP TABLE _td AS");
            sResult = sResult.Replace(@"KEY ,
)", @"KEY 
);");
            sResult = sResult.Replace(@"order by td.HeaderiID, g.DateTimeGPS_UTC", "order by td.HeaderiID, g.DateTimeGPS_UTC;");

            sResult = sResult.Replace("DATEADD(MINUTE, t.TotalMinutes, g.DateTimeGPS_UTC) localTime", "t.TotalMinutes * INTERVAL '1 MINUTE' +  g.DateTimeGPS_UTC as localTime");
            sResult = sResult.Replace(@"STUFF(
				(SELECT ', ' + d.TypeID + ' : ' + d.TypeValue + ' : ' + d.UOM FROM GFI_GPS_GPSDataDetail d Where d.UID = g.UID FOR XML PATH(''))
			, 1, 1, '')", "(select array_to_string(ARRAY(SELECT d.TypeID || ' : ' || d.TypeValue || ' : ' || d.UOM FROM GFI_GPS_GPSDataDetail d Where d.UID = g.UID), ', '))");
            sResult = sResult.Replace(@"STUFF(
				(SELECT ', ' + d.TypeID + ' : ' + d.TypeValue + ' : ' + d.UOM FROM GFI_ARC_GPSDataDetail d Where d.UID = g.UID FOR XML PATH(''))
			, 1, 1, '')", "(select array_to_string(ARRAY(SELECT d.TypeID || ' : ' || d.TypeValue || ' : ' || d.UOM FROM GFI_ARC_GPSDataDetail d Where d.UID = g.UID), ', '))");
            sResult = sResult.Replace("convert(varchar(300), NULL)", "''");
            sResult = sResult.Replace("ROUND(g.Speed, 0)", "ROUND(g.Speed::numeric, 0)");

            sResult = sResult.Replace(@"from _td 
	inner join cte on  _td.RowNum = cte.RowNum
where cte.ExpRowNum = 1", "from cte where _td.RowNum = cte.RowNum and cte.ExpRowNum = 1");

            sResult = sResult.Replace("select * from _td order by localTime --where RuleID is not null order by localTime", String.Empty);
            sResult = sResult.Replace("As RowNum", "As \"RowNum\"");
            sResult = sResult.Replace("_td.RowNum = cte.RowNum", "_td.\"RowNum\" = cte.\"RowNum\"");
            sResult = sResult.Replace("_td.RowNum = cte.RowNum", "_td.\"RowNum\" = cte.\"RowNum\"");

            sResult = sResult.Substring(0, sResult.LastIndexOf("--PostgreSQL ends here"));
            return sResult;
        }

        static String ScoreCards(String sql)
        {
            String sResult = sql;
            sResult = sResult.Replace("#Drivers", "_Drivers");
            sResult = sResult.Replace("CREATE", "CREATE temp");
            sResult = sResult.Replace("RowNumber int IDENTITY(1,1) NOT NULL,", "RowNumber int GENERATED ALWAYS AS IDENTITY,");
            sResult = sResult.Replace("CAST(DATEADD(second,SUM(h.OverSpeed1Time) , '1900-01-01') AS TIME)", "TO_CHAR((SUM(h.OverSpeed1Time) || ' second')::interval, 'HH24:MI:SS')");
            sResult = sResult.Replace("CAST(DATEADD(second,SUM(h.OverSpeed2Time) , '1900-01-01') AS TIME)", "TO_CHAR((SUM(h.OverSpeed2Time) || ' second')::interval, 'HH24:MI:SS')");
            sResult = sResult.Replace("ROUND(sum(h.TripDistance), 2)", "ROUND(sum(h.TripDistance) ::numeric, 2 )");
            sResult = sResult.Replace("CAST(DATEADD(second,sum(h.TripTime) , '1900-01-01') AS TIME)", "TO_CHAR((SUM(h.TripTime) || ' second')::interval, 'HH24:MI:SS')");
            sResult = sResult.Replace("CAST(DATEADD(second,sum(h.idlingTime) , '1900-01-01') AS TIME)", "TO_CHAR((SUM(h.idlingtime) || ' second')::interval, 'HH24:MI:SS')");

            return sResult;
        }
        static String s(String sql)
        {
            String sResult = sql;

            return sResult;
        }
        #endregion
        static String AddSemiColumn(String sql)
        {
            String sBlock = string.Empty;
            Boolean inUpdate = false;
            Boolean bPerformUpdateCmd = false;
            String updateCmd = String.Empty;

            if (sql.StartsWith("--Live\r\n"))
                bPerformUpdateCmd = true;
            else if (sql.StartsWith("--Update Road Speed\r\n"))
                bPerformUpdateCmd = true;
            else if (sql.Contains("--SaveFuelToProcess"))
                bPerformUpdateCmd = true;
            else if (sql.Contains("--ZoneVisitedReport"))
                bPerformUpdateCmd = true;
            else if (sql.Contains("--GetNotificationData"))
                bPerformUpdateCmd = true;
            else if (sql.StartsWith("--GetExcetpionssql"))
                bPerformUpdateCmd = true;
            else if (sql.Contains("--GetDailyTripTime"))
                bPerformUpdateCmd = true;
            else if (sql.Contains("--TripAddresses"))
                bPerformUpdateCmd = true;

            string[] lines = sql.Split('\n');
            for (int i = 0; i < lines.Length; i++)
            {
                String line = lines[i];
                for (int j = i; j < lines.Length; j++)
                {
                    i = j;
                    line = lines[i];

                    int x = line.IndexOf("RowNum = ROW_NUMBER()");
                    if (x > -1)
                    {
                        line = line.Replace("\r", String.Empty);
                        line += " as RowNum \r";
                        line = line.ReplaceIgnoreCase("RowNum = ", String.Empty);
                    }

                    if (line.Trim().ToLower().StartsWith("update"))
                        inUpdate = true && bPerformUpdateCmd;

                    if (inUpdate)
                        updateCmd += line + "\n";
                    else
                        sBlock += line + "\n";

                    if (String.IsNullOrEmpty(line.Trim()))
                    {
                        if (inUpdate)
                        {
                            String s = PostgreSQLUpdate(updateCmd);
                            sBlock += s;
                            inUpdate = false;
                            updateCmd = String.Empty;
                        }
                        sBlock = sBlock.TrimEnd() + "; \r\n \r\n";
                    }
                }
            }

            sBlock = sBlock.Replace("AS TableName", "AS TableName;");
            sBlock = sBlock.Replace(";;", ";");
            sBlock = sBlock.Replace(";;", ";");
            sBlock = sBlock.Replace(";;", ";");
            sBlock = sBlock.Replace(";;", ";");
            sBlock = sBlock.Replace(";;", ";");
            sBlock = sBlock.ReplaceIgnoreCase(";with", "with");
            return sBlock;
        }
        static String PostgreSQLUpdate(String sql)
        {
            String sResult = String.Empty;
            List<String> sAlias = new List<String>();
            List<String> sTable = new List<String>();
            Boolean bWhere = false;

            string[] lines = sql.Split('\n');

            for (int i = 0; i < lines.Length; i++)
            {
                String line = lines[i].Trim();
                if (line.Trim().ToLower().StartsWith("update"))
                {
                    String[] s = line.Trim().Split(' ');
                    sTable.Add(s[s.Length - 1].Trim());
                }
                else if (line.Trim().ToLower().StartsWith("from"))
                {
                    String[] s = line.Trim().Split(' ');
                    sAlias.Add(s[s.Length - 1].Trim());
                }
                else if (line.Trim().ToLower().Contains(" join "))
                {
                    line = line.Replace("  ", " ");
                    String[] sjoins = line.Trim().Split(' ');
                    int x = 0;
                    int z = 0;
                    foreach (String s in sjoins)
                    {
                        if (s.Trim().ToLower() == "on")
                        {
                            z = x;
                            break;
                        }
                        x++;
                    }

                    String sTN = String.Empty;
                    for (int y = 2; y < z - 1; y++)
                        sTN += sjoins[y].Trim() + " ";
                    if (String.IsNullOrEmpty(sTN))  // no alias used
                        sTN = sjoins[z - 1].Trim();

                    sTable.Add(sTN);
                    sAlias.Add(sjoins[z - 1].Trim());
                }
            }

            for (int i = 0; i < lines.Length; i++)
            {
                String line = lines[i].TrimEnd();
                if (line.Trim().ToLower().StartsWith("update"))
                {
                    sResult += line + " ";
                    if (sAlias.Count > 0)
                        sResult += sAlias[0];
                    sResult += "\n";
                }
                else if (line.Trim().ToLower().StartsWith("from"))
                {
                    String s = line.Split(' ')[0] + " ";
                    for (int j = 1; j < sTable.Count; j++)
                        s += sTable[j] + ' ' + sAlias[j] + ", ";
                    if (sTable.Count > 1)
                    {
                        s = s.Substring(0, s.Length - 2);
                        sResult += s + "\n";
                    }
                }
                else if (line.Trim().ToLower().Contains(" join "))
                {
                    String s = line;
                    if (!bWhere)
                    {
                        bWhere = true;
                        for (int j = 0; j < sTable.Count; j++)
                        {
                            String x = sTable[j] + ' ' + sAlias[j];
                            s = s.Replace(x, String.Empty);
                        }

                        int t = s.ToLower().LastIndexOf(" on ");
                        s = s.Substring(t, s.Length - t);
                        s = s.ReplaceIgnoreCase(" on ", " where ");
                        sResult += s + "\n";
                    }
                    else
                    {
                        for (int j = 0; j < sTable.Count; j++)
                        {
                            String x = sTable[j] + ' ' + sAlias[j];
                            s = s.Replace(x, String.Empty);
                        }

                        int t = s.ToLower().LastIndexOf(" on ");
                        s = s.ToLower().Substring(t, s.Length - t);
                        s = s.Replace(" on ", " and ");
                        sResult += s + "\n";
                    }
                }
                else if (line.Trim().ToLower().StartsWith("where "))
                {
                    String s = line;
                    if (bWhere)
                        s = s.ReplaceIgnoreCase("where", " and ");
                    bWhere = true;
                    sResult += s + "\n";
                }
                else
                    sResult += line + "\n";
            }

            if (sAlias.Count > 0)
                sResult = sResult.Replace(sTable[0] + ' ' + sAlias[0] + ",", String.Empty);
            return sResult;
        }

        static string GetNestedString(int StartLine, string str, char start, char end)
        {
            String[] lines = str.Split('\n');
            String tmp = String.Empty;
            for (int x = StartLine; x < lines.Length; x++)
            {
                tmp += lines[x] + "\n";
            }
            str = tmp;

            int s = -1;
            int i = -1;
            while (++i < str.Length)
                if (str[i] == start)
                {
                    s = i;
                    break;
                }
            int e = -1;
            int depth = 0;
            while (++i < str.Length)
                if (str[i] == end)
                {
                    e = i;
                    if (depth == 0)
                        break;
                    else
                        --depth;
                }
                else if (str[i] == start)
                    ++depth;
            if (e > s)
                return str;//.Substring(s + 1, e - s - 1);
            return null;
        }
        static string GetNestedStringFromStartPos(int StartPos, string str, char start, char end)
        {
            int s = -1;
            int i = StartPos;
            while (++i < str.Length)
                if (str[i] == start)
                {
                    s = i;
                    break;
                }
            int e = -1;
            int depth = 0;
            while (++i < str.Length)
                if (str[i] == end)
                {
                    e = i;
                    if (depth == 0)
                        break;
                    else
                        --depth;
                }
                else if (str[i] == start)
                    ++depth;
            if (e > s)
                return str.Substring(s + 1, e - s - 1);
            return null;
        }
    }
}
/*
 * 
[ <<label>> ]
[ DECLARE
    declarations ]
BEGIN
    statements
EXCEPTION
    WHEN condition [ OR condition ... ] THEN
        handler_statements
    [ WHEN condition [ OR condition ... ] THEN
          handler_statements
      ... ]
END;

    */

/*
	Declare	v_ID RECORD;
	Declare myCursor cursor(p_GMID int) for
		select StrucID from tGetDriverIDFromGMID where StrucID > 0 order by StrucID;
	Begin
		open myCursor(p_GMID);
			LOOP
			FETCH myCursor INTO v_ID;
			EXIT WHEN NOT FOUND;  
				v_Result := v_Result || v_ID.StrucID || ',';
			END LOOP;
		close myCursor;

		if(length(v_Result) > 0) then
			v_Result := SUBSTRING(v_Result, 0, length(v_Result));
		end if;
	end;

*/

/*
UPDATE link_tmp
SET rel = link.rel,
 description = link.description,
 last_update = link.last_update
FROM
   link
WHERE
   link_tmp.id = link.id;
*/

/*select '120' * interval '1 second'*/
/*do command > returns no rows*/

/*
 select *, (
                select array_to_string(ARRAY(SELECT '| ' || d.TypeID || ' : ' || d.TypeValue || ' : ' || d.UOM FROM GFI_GPS_GPSDataDetail d Where d.UID = T1.UID), ', ')
           ) as details
from GFI_GPS_GPSData T1
where uid < 5
*/

/*
--anonymous function
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
do $$
begin
    create temp tables;
    declare variables;
    begin
        codes
    end;
END;

$$ LANGUAGE plpgsql;
select statements
*/

/*
DELETE FROM logtable 
    WHERE id = any (array(SELECT id FROM logtable ORDER BY timestamp LIMIT 10));
*/

/*
C:\Program Files\PostgreSQL\12\bin\pg_dump.exe --file "C:\\Reza\\Tmp\\MHLLIVE.sql" --host "localhost" --port "5432" --username "postgres" --no-password --verbose --format=c --blobs "tripsdb"
*/
