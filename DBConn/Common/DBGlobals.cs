﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data;
using System.Windows.Forms;

using System.IO;

namespace DBConn.Common
{
    public class DBGlobals
    {
        public static String BackupPath()
        {
            String strP = Application.StartupPath.ToString() + "\\SQLBak\\";
            if (!(System.IO.Directory.Exists(strP)))
                System.IO.Directory.CreateDirectory(strP);

            return strP;
        }

        public static Boolean ValidateLicence()
        {
            return true;
        }
    }
}