﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GlobalFunctions;
using NaveoOneLib.DBCon;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using System.IO;
using NaveoOneLib.Constant;
using NaveoService.Models;
using NaveoOneLib.Models.Permissions;
using NaveoService.Helpers;
using NaveoService.Constants;
using NaveoOneLib.Common;

namespace NaveoMonitoring
{
    public class NaveoMonitoring
    {   
        public static DataTable GetSIMCardData(Guid sUserToken, string sConnStr, int? Page, int? LimitPerPage)
        {
            String sqlMonitoring = @"select distinct unitID, max(dtdatetime) as MaxDateTime, min(dtdatetime) as MinDateTime, Count(1) cnt, min(Model) Allmodel
                                into #RawData from RawData 
                                group by unitID
--table 0
select *, '' IMSI, '' ServerName, '' DBName, '' Client from #RawData

--table 1
select * from RawData r
inner join #RawData c on c.MinDateTime = r.dtDateTime and c.unitID = r.unitID
where model in ('AT100', 'AT200', 'AT240', 'ATRACK', 'ENFORN', 'MVT100', 'MVT380', 'ORION', 'ATRACK')

--table 2
select distinct model, min(RawData) RawData, min(cast(RawData as varchar(500))) RData, unitID from RawData where model in ('AT100IMSI', 'AtrackImsi', 'EnfornIMSI', 'MeitrackImsi', 'OrionImsi')
	group by model, unitID";

            DataSet ds = new GlobalFunc().QryNaveoLnkSvr(sqlMonitoring, false);
            DataTable dtResult = ds.Tables[0];

            foreach (DataRow dr in dtResult.Rows)
            {
                String sUnitID = dr["UnitID"].ToString();
                String strImsiTmp = String.Empty;
                String strModel = String.Empty;
                try
                {
                    DataRow dr1 = ds.Tables[1].Select("UnitID = '" + sUnitID + "'").FirstOrDefault();
                    DataRow dr2 = ds.Tables[2].Select("UnitID = '" + sUnitID + "'").FirstOrDefault();

                    if (dr2 != null)
                    {
                        strModel = dr2["Model"].ToString();

                        try
                        {
                            switch (strModel)
                            {
                                case "AtrackImsi":
                                    strImsiTmp = new NavLib.Atrack.DecodeAtrack((Byte[])dr2["RawData"]).GetImsi().Trim();
                                    break;

                                case "OrionImsi":
                                    strImsiTmp = new NavLib.Orion.DecodeOrion((Byte[])dr2["RawData"]).oReports[0].GetIMSI().Trim();
                                    break;

                                case "MeitrackImsi":
                                    strImsiTmp = new NavLib.Meitrack.DecodeMeiTrack((Byte[])dr2["RawData"]).GetIMSI().Trim();
                                    break;

                                case "AT100IMSI":
                                    strImsiTmp = new NavLib.AT100.DecodeAT100IMSI((Byte[])dr2["RawData"]).GetIMSI().Trim();
                                    break;

                                case "EnforaImsi":
                                    strImsiTmp = new NavLib.Enfora.clsGSM2218((Byte[])dr2["RawData"]).GetIMSI().Trim();
                                    break;

                                case "EnfornIMSI":
                                    strImsiTmp = new NavLib.Enfora.DecodeEnfora((Byte[])dr2["RawData"]).GetIMSI().Trim();
                                    break;

                                case "TELTONIKAIMSI":
                                    break;
                            }
                        }
                        catch (Exception)
                        {
                            strImsiTmp = "error";
                        }
                    }

                    dr["Imsi"] = strImsiTmp;
                    if (dr1 != null)
                    {
                        dr["ServerName"] += dr1["ServerName"].ToString();
                        dr["DBName"] += dr1["DBName"].ToString();
                    }
                    else
                    {
                        dr["ServerName"] += "*********";
                        dr["DBName"] += "****";
                    }
                }
                catch (Exception ex) { }
            }

            return dtResult;
        }

        public class clsMonitoring
        {
            public DateTime? DateTimeStart { get; set; }

            public DateTime? DateTimeEnd { get; set; }

        }

        public static DataTable GetExceptions(Guid sUserToken, string sConnStr, DateTime dtStart, DateTime dtEnd, int? Page, int? LimitPerPage)
        {
            //GetExceptions
            DataTable dt = new DataTable();
            try
            {
                String sqlString = @"SELECT count(1) as CountNo, GFI_GPS_Exceptions.AssetID, GFI_GPS_Exceptions.RuleID, AssetName, RuleName
  FROM GFI_GPS_Exceptions, GFI_FLT_Asset, GFI_GPS_Rules
  where DateTimeGPS_UTC >= '" + dtStart.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "' and DateTimeGPS_UTC <= '" + dtEnd.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "' AND GFI_GPS_Exceptions.AssetID = GFI_FLT_Asset.AssetID  AND GFI_GPS_Rules.RuleID = GFI_GPS_Exceptions.RuleID  group by GFI_GPS_Exceptions.AssetID, GFI_GPS_Exceptions.RuleID, AssetName, RuleName";

                dt = new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
            }
            catch (Exception)
            {

                throw;
            }

            return dt;
        }

        public static ExcelPackage XcelDynamic(DataTable dt, DateTime? DateFrom, DateTime? DateTimeTo, int row)
        {
            ExcelPackage package = new ExcelPackage();

            string reportName = "";

            // Grab the 1st sheet with the template.
            ExcelWorksheet sheet = package.Workbook.Worksheets.Add("Naveo");
            sheet = package.Workbook.Worksheets.First();
            row = 3;

            if (DateFrom == null | DateTimeTo == null)
            {
                reportName = "Report Extracted for the current period";
            }
            else
            {
                reportName = "Report Extracted for the Period of " + DateFrom.ToString() + " To " + DateTimeTo.ToString();
            }


            sheet.Cells[1, 5].Value = reportName.ToUpper();
            //sheet.Column(5).Width = 100;
            //sheet.GetMergeCellId(1, 50);
            sheet.Column(5).AutoFit();

            Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#00BFFF");
            sheet.Cells[1, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[1, 1].Style.Fill.BackgroundColor.SetColor(colFromHex);
            sheet.Cells[1, 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[1, 2].Style.Fill.BackgroundColor.SetColor(colFromHex);

            sheet.Cells[1, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[1, 3].Style.Fill.BackgroundColor.SetColor(colFromHex);
            sheet.Cells[1, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[1, 4].Style.Fill.BackgroundColor.SetColor(colFromHex);
            sheet.Cells[1, 5].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[1, 5].Style.Fill.BackgroundColor.SetColor(colFromHex);
            sheet.Cells[1, 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[1, 6].Style.Fill.BackgroundColor.SetColor(colFromHex);
            sheet.Cells[1, 7].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[1, 7].Style.Fill.BackgroundColor.SetColor(colFromHex);
            sheet.Cells[1, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[1, 8].Style.Fill.BackgroundColor.SetColor(colFromHex);
            sheet.Cells[1, 9].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[1, 9].Style.Fill.BackgroundColor.SetColor(colFromHex);
            sheet.Cells[1, 10].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[1, 10].Style.Fill.BackgroundColor.SetColor(colFromHex);
            sheet.Cells[1, 11].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[1, 11].Style.Fill.BackgroundColor.SetColor(colFromHex);
            sheet.Cells[1, 12].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[1, 12].Style.Fill.BackgroundColor.SetColor(colFromHex);

            sheet.Cells[1, 5].Style.Font.Size = 15;
            sheet.Cells[1, 5].Style.Font.Color.SetColor(Color.White);
            sheet.Cells[1, 5].Style.Font.Bold = true;


            // Insert data
            if (sheet != null)
            {
                var rows = dt.Rows;
                foreach (DataRow dr in rows)
                {
                    int countColumn = 0;
                    foreach (DataColumn dc in dt.Columns)
                    {
                        countColumn++;
                        if (countColumn > 0)
                        {
                            string colData = dc.ColumnName.ToString();
                            sheet.Cells[2, countColumn].Value = colData;

                            if (colData == "EngineOn")
                            {
                                //colData = "EngineOn";
                                sheet.Cells[2, countColumn].Value = "EngineOn";
                                sheet.Cells[row, countColumn].Value = dr[colData].ToString();
                            }



                            if (colData == "TripDistance")
                            {
                                decimal tD = Convert.ToDecimal(dr[colData]);
                                dr[colData] = Math.Round(tD, 2);
                            }


                            sheet.Cells[row, countColumn].Value = dr[colData].ToString();
                        }
                    }
                    row++;
                }

            }
            return package;
        }

        public static MemoryStream GenerateExcelStreamToExport(Entity entity, SecurityToken securityToken, string path)  //MemoryStream FileStream
        {
            exclPak ePak = excelPak(entity, securityToken);

            // Results Output
            MemoryStream output = new MemoryStream();
            if (String.IsNullOrEmpty("DDD"))
            {
                string exportFilesPath = Path.Combine(path, securityToken.UserToken.ToString());

                // Determine whether the directory exists.
                if (!Directory.Exists(exportFilesPath))
                    Directory.CreateDirectory(exportFilesPath);

                ePak.excelPackage.SaveAs(new FileInfo(Path.Combine(exportFilesPath, FileHelper.NameFormat(entity.entityName, FileSystem.Extension.XLSX))));
            }
            else
            {
                if (ePak.ex != null)
                {
                    System.IO.File.AppendAllText(Globals.ErrorsPath(), "\r\n Export \r\n" + ePak.ex.Message + "\r\n" + ePak.ex.InnerException);
                    return null;
                }
                else
                    ePak.excelPackage.SaveAs(output);
            }
            return output;
        }

        public static bool SaveExcelExport(Entity entity, SecurityToken securityToken, string path)  //MemoryStream FileStream
        {
            exclPak ePak = excelPak(entity, securityToken);

            if (ePak.ex != null)
            {
                // Create a text file in the location where the excel files are to be generated if error in excel saving
                string exportFilesPath = Path.Combine(path, securityToken.UserToken.ToString());
                if (!Directory.Exists(exportFilesPath))
                    Directory.CreateDirectory(exportFilesPath);

                string errorTxtFile = Path.Combine(exportFilesPath, FileHelper.NameFormat(entity.entityName, FileSystem.Extension.TXT));
                File.WriteAllText(errorTxtFile, ePak.ex.Message + Environment.NewLine + ePak.ex.InnerException);
                return false;
            }
            else if (ePak.excelPackage != null)
            {
                string exportFilesPath = Path.Combine(path, securityToken.UserToken.ToString());
                // Determine whether the directory exists.
                if (!Directory.Exists(exportFilesPath))
                    Directory.CreateDirectory(exportFilesPath);

                ePak.excelPackage.SaveAs(new FileInfo(Path.Combine(exportFilesPath, FileHelper.NameFormat(entity.entityName, FileSystem.Extension.XLSX))));
                return true;
            }

            //This should never hit
            return false;
        }

        static exclPak excelPak(Entity entity, SecurityToken securityToken)
        {
            exclPak ePak = new exclPak();

            DataTable dt = new DataTable();

            ExcelPackage package = null;
            int row = 2; // First excel row

            try
            {
                switch (entity.entityName)
                {
                    #region MonitoringData
                    case NaveoEntity.Monitoring_Data:
                        dt = GetSIMCardData(securityToken.UserToken, securityToken.sConnStr, null, null);
                        package = XcelDynamic(dt, null, null, row);
                        break;
                    #endregion
                    #region MonitoringDevice
                    case NaveoEntity.Monitoring_Device:
                        dt = GetExceptions(securityToken.UserToken, securityToken.sConnStr, (DateTime)entity.monitoring.DateTimeStart, (DateTime)entity.monitoring.DateTimeEnd, null, null);
                        package = XcelDynamic(dt, (DateTime)entity.monitoring.DateTimeStart, (DateTime)entity.monitoring.DateTimeEnd, row);
                        break;
                    #endregion
                    default:
                        break;
                }

                ePak.excelPackage = package;
            }
            catch (Exception ex)
            {
                ePak.excelPackage = null;
                ePak.ex = ex;
            }

            return ePak;
        }

        class exclPak
        {
            public ExcelPackage excelPackage { get; set; }
            public Exception ex { get; set; }
        }
    }
}
