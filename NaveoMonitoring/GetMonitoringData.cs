﻿using System.Data;

namespace NaveoMonitoring
{
    internal class GetMonitoringData : DataTable
    {
        private object userToken;
        private object sConnStr;
        private object page;
        private object limitPerPage;

        public GetMonitoringData(object userToken, object sConnStr, object page, object limitPerPage)
        {
            this.userToken = userToken;
            this.sConnStr = sConnStr;
            this.page = page;
            this.limitPerPage = limitPerPage;
        }
    }
}