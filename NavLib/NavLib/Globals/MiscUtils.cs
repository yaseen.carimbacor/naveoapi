﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Net;
using System.Net.Mail;
using System.Configuration;
using System.IO;

namespace NavLib.Globals
{
    public class MiscUtils
    {
        public MiscUtils()
        {
        }

        public void SendEmails(String strTo, String strSubject, String strBody)
        {
            SendEmails(strTo, strSubject, strBody, true, 587, "smtp.gmail.com", "NoReply@Naveo.mu");
        }
        public void SendEmails(String strTo, String strSubject, String strBody, Boolean bSmtpSSL, int iSmtpPort, String sSmtpHost, String sSmtpSender)
        {
            if (strTo.Trim().Length == 0)
                return;

            SmtpClient smtpclient = new SmtpClient();
            smtpclient.Port = iSmtpPort;
            smtpclient.EnableSsl = bSmtpSSL;
            smtpclient.Host = sSmtpHost;
            if (sSmtpSender == "NoReply@Naveo.mu")
                smtpclient.Credentials = new NetworkCredential("NaveoOne@gmail.com", "ebs4dmin");

            MailMessage email = new MailMessage();
            email.Sender = email.From = email.ReplyTo = new MailAddress(sSmtpSender, sSmtpSender);
            //email.Sender = email.From = email.ReplyToList[0] = new MailAddress(sSmtpSender, sSmtpSender);
            email.To.Add(strTo);
            email.Subject = strSubject; ;
            email.Body = strBody; 
            //email.Attachments.Add(new Attachment("C:\\NaveoCache.txt"));

            //GOC
            //smtpclient.EnableSsl = false;
            //smtpclient.Port = 25;
            //smtpclient.Host = "202.123.27.104";
            //email.Sender = email.From = email.ReplyTo = new MailAddress("NaveoNoReply@govmu.org", "NaveoNoReply@govmu.org");

            smtpclient.Send(email);
        }

        public void UpdateConfigFile()
        {
            Configuration c = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);//System.Windows.Forms.Application.ExecutablePath);
            Boolean bFound = false;
            Boolean bSave = false;
            foreach (KeyValueConfigurationElement s in c.AppSettings.Settings)
                if (s.Key == "UseProxy")
                {
                    bFound = true;
                    break;
                }
            if (!bFound)
            {
                c.AppSettings.Settings.Add("UseProxy", "no");
                c.AppSettings.Settings.Add("ProxyDomain", "DomainName");
                c.AppSettings.Settings.Add("ProxyUserName", "Naveo");
                c.AppSettings.Settings.Add("ProxyPassword", "EncryptedPassword");
                c.AppSettings.Settings.Add("ProxyAddress", "192.168.161.240");
                c.AppSettings.Settings.Add("ProxyPort", "8080");
                bSave = true;
                bFound = false;
            }
            //String OldValue = c.AppSettings.Settings["Fuel"].Value;
            //c.AppSettings.Settings["Fuel"].Value = "wawa";
            c.Save(ConfigurationSaveMode.Minimal, false);
            if (bSave)
                ConfigurationManager.RefreshSection("appSettings");

            //UseWebService
            c = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            bFound = false;
            bSave = false;
            foreach (KeyValueConfigurationElement s in c.AppSettings.Settings)
                if (s.Key == "UseWebService")
                {
                    bFound = true;
                    break;
                }
            if (!bFound)
            {
                c.AppSettings.Settings.Add("UseWebService", "NotUsed");
                bSave = true;
                bFound = false;
            }
            c.Save(ConfigurationSaveMode.Minimal, false);
            if (bSave)
                ConfigurationManager.RefreshSection("appSettings");

            //WebURL
            c = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            bFound = false;
            bSave = false;
            foreach (KeyValueConfigurationElement s in c.AppSettings.Settings)
                if (s.Key == "WebURL")
                {
                    bFound = true;
                    break;
                }
            if (!bFound)
            {
                c.AppSettings.Settings.Add("WebURL", "http://localhost:59831/EngService.asmx");
                bSave = true;
                bFound = false;
            }
            c.Save(ConfigurationSaveMode.Minimal, false);
            if (bSave)
                ConfigurationManager.RefreshSection("appSettings");

            //ServiceToRun
            c = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            bFound = false;
            bSave = false;
            foreach (KeyValueConfigurationElement s in c.AppSettings.Settings)
                if (s.Key == "ServiceToRun")
                {
                    bFound = true;
                    break;
                }
            if (!bFound)
            {
                c.AppSettings.Settings.Add("ServiceToRun", "none");
                bSave = true;
                bFound = false;
            }
            c.Save(ConfigurationSaveMode.Minimal, false);
            if (bSave)
                ConfigurationManager.RefreshSection("appSettings");
        }
        public void UpdateConfigKey(string strKey, string newValue)
        {
            Configuration c = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            //chk if exist
            Boolean bFound = false;
            foreach (KeyValueConfigurationElement s in c.AppSettings.Settings)
                if (s.Key == strKey)
                {
                    bFound = true;
                    break;
                }
            if (!bFound)
            {
                c.AppSettings.Settings.Add(strKey, newValue);
                c.Save(ConfigurationSaveMode.Minimal, false);
                c = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            }

            foreach (KeyValueConfigurationElement s in c.AppSettings.Settings)
                if (s.Key == strKey)
                {
                    s.Value = newValue;
                    c.Save(ConfigurationSaveMode.Minimal, false);
                    ConfigurationManager.RefreshSection("appSettings");
                }
        }
        public void InsertConfigKey(string strKey, string newValue)
        {
            Configuration c = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            //chk if exist
            Boolean bFound = false;
            foreach (KeyValueConfigurationElement s in c.AppSettings.Settings)
                if (s.Key == strKey)
                {
                    bFound = true;
                    break;
                }
            if (!bFound)
            {
                c.AppSettings.Settings.Add(strKey, newValue);
                c.Save(ConfigurationSaveMode.Minimal, false);
                ConfigurationManager.RefreshSection("appSettings");
            }
        }

        public void UpdateConnectionString(string strKey, string newValue, string dbProvider)
        {
            Configuration c = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            String sDBProvider = "System.Data.SqlClient";
            if (dbProvider == "Oracle")
                sDBProvider = "Oracle";
            if (dbProvider == "PostgreSQL")
                sDBProvider = "PostgreSQL";

            //chk if exist
            Boolean bFound = false;
            foreach (ConnectionStringSettings s in c.ConnectionStrings.ConnectionStrings)
                if (s.Name == strKey)
                {
                    bFound = true;
                    break;
                }
            if (!bFound)
            {
                c.ConnectionStrings.ConnectionStrings.Add(new ConnectionStringSettings(strKey, newValue, sDBProvider));
                c.Save(ConfigurationSaveMode.Minimal, false);
                c = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            }

            c.ConnectionStrings.ConnectionStrings[strKey].ConnectionString = newValue;
            c.ConnectionStrings.ConnectionStrings[strKey].ProviderName = sDBProvider;
            c.Save(ConfigurationSaveMode.Modified);
            //foreach (ConnectionStringSettings s in c.ConnectionStrings.ConnectionStrings)
            //    if (s.Name == strKey)
            //    {
            //        s.ConnectionString = newValue;
            //        c.Save(ConfigurationSaveMode.Modified, false);
            //        ConfigurationManager.RefreshSection("ConnectionStrings");
            //    }
        }

        public Boolean isDirectoryEmpty(String path)
        {
            int fileCount = Directory.GetFiles(path).Length;
            if (fileCount > 0)
            {
                return false;
            }

            string[] dirs = Directory.GetDirectories(path);
            if (dirs.Length > 0)
                return false;

            //foreach (string dir in dirs)
            //{
            //    if (!isDirectoryEmpty(dir))
            //    {
            //        return false;
            //    }
            //}

            return true;
        }

    }
}