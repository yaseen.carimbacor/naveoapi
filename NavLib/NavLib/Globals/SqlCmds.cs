using System;
using System.Collections.Generic;
using System.Text;

namespace NavLib.Globals
{
    public class SqlCmds
    {
        public static String GetDailyTran()
        {
            String sql = "select * from RawData where DATEPART(dayofyear , dtdatetime) =  DATEPART(dayofyear , GETDATE()) - 1";
            return sql;
        }

        public static String GenerateUnprocessedFuelData(String strUnitIDs, Boolean InProgress)
        {
            String statusTo = "2";
            String statusFrom = "1";
            if (InProgress)
            {
                statusTo = "1";
                statusFrom = "0";
            }

            String sql = "update RawData set FuelProcessed = " + statusTo;
            sql += "\r\nwhere unitID in ";
            sql += "(" + strUnitIDs + ")";
            sql += "\r\n    and FuelProcessed = " + statusFrom;
            return sql;
        }
        public static String GetUnprocessedFuelData(String strUnitIDs)
        {
            String sql = "select * from RawData ";
            sql += "\r\nwhere unitID in ";
            sql += "(" + strUnitIDs + ")";
            sql += "\r\n    and FuelProcessed = 1";
            sql += "\r\norder by dtDatetime";
            return sql;
        }
        public static String GetFuelData(String myUnits, Boolean ExcludeMessageDesc)
        {
            String sql = "select fm.Description, fd.* ";
            if (ExcludeMessageDesc)
                sql = "select fd.* ";
            sql += "\r\nfrom FuelData fd, FuelMessageTypes fm";
            sql += "\r\nwhere fd.messagetype = fm.messagetype";
            sql += "\r\n	and fm.IsNotif = 0";
            sql += "\r\n	--and fm.messagetype = 6	--Real";
            sql += "\r\n	--and fm.messagetype = 1	--SensorDataMessage";
            sql += "\r\n	and ( fm.messagetype = 5	--FillDataMessage";
            sql += "\r\n			or fm.messagetype = 6	--Real";
            sql += "\r\n		)";
            if (myUnits != String.Empty)
            {
                String statusInProgress = "1";
                sql += "\r\n    and fd.status = " + statusInProgress;
                sql += "\r\nand fd.unitID in (" + myUnits + ")";
            }
            return sql;
        }
        public static String GetFuelDataManually(String myUnits, Boolean ExcludeMessageDesc, String iRows)
        {
            String sql = "select top "+ iRows + " fm.Description, fd.* ";
            if (ExcludeMessageDesc)
                sql = "select fd.* ";
            sql += "\r\nfrom FuelData fd, FuelMessageTypes fm";
            sql += "\r\nwhere fd.messagetype = fm.messagetype";
            sql += "\r\n	and fm.IsNotif = 0";
            sql += "\r\n	--and fm.messagetype = 6	--Real";
            sql += "\r\n	--and fm.messagetype = 1	--SensorDataMessage";
            sql += "\r\n	and ( fm.messagetype = 5	--FillDataMessage";
            sql += "\r\n			or fm.messagetype = 6	--Real";
            sql += "\r\n		)";
            if (myUnits != String.Empty)
            {
                sql += "\r\nand fd.unitID in (" + myUnits + ")";
            }
            sql += "\r\n order by dtDateTime desc";
            return sql;
        }
        public static String UpdateFuelData(String myUnits, Boolean InProgress)
        {
            String statusInProgress = "2";
            String statusNew = "1";
            if (InProgress)
            {
                statusInProgress = "1";
                statusNew = "0";
            }

            //String sql = "update FuelData set status = " + statusInProgress;
            //sql += "\r\nfrom (select top 500 * from FuelData";
            //sql += "\r\nwhere status = " + statusNew;
            //sql += "\r\nand unitID in (" + myUnits + ")";
            //sql += "\r\norder by dtdatetime ";
            //sql += "\r\n) as t1 where FuelData.iID = t1.iID";

            String sql = "update FuelData set status = " + statusInProgress;
            sql += "\r\nwhere status = " + statusNew;
            sql += "\r\nand unitID in (" + myUnits + ")";
            return sql;
        }
        public static String GetLastFuelSensorDataID()
        {
            String sql = "select max(fd.DataID) from FuelData fd, fuelmessagetypes mt";
            sql += "\r\nwhere fd.MessageType = mt.MessageType";
            sql += "\r\n	and fd.messagetype = 1";
            sql += "\r\n	and mt.IsNotif = 0";
            return sql;
        }
        public static String GetFuelID(String unitID)
        {
            String sql = "select FuelID, Reverse from Fuel where unitID = '";
            sql += unitID + "'";
            return sql;
        }
        public static String GetUnitID(int FuelID)
        {
            String sql = "select unitID from Fuel where FuelID = " + FuelID;
            if (FuelID == -1)
                sql = "select unitID from Fuel";
            return sql;
        }
        public static String InsertFuelID(String unitID, String Reverse)
        {
            String sql = "insert Fuel (unitID, Reverse) values (";
            sql += "'" + unitID + "'";
            sql += ", '" + Reverse + "')";
            return sql;
        }
        public static String ChangeFuelIDReverse(String unitID, String Reverse)
        {
            String sql = "update Fuel set reverse = ";
            sql += "'" + Reverse + "'";
            sql += "\r\n where unitID = '" + unitID + "'";
            return sql;
        }

        //Processing FuelData using Naveo Logic 
        public static String GenerateUnprocessedFuelDataFromFuelDataTable(String strUnitID, Boolean InProgress, int Mylimit)
        {
            String statusTo = "2";
            String statusFrom = "1";
            if (InProgress)
            {
                statusTo = "1";
                statusFrom = "0";
            }

            String sql = "update FuelData set DataID = " + statusTo;
            sql += "\r\nfrom (select top " + Mylimit + " * from FuelData";
            sql += "\r\nwhere unitID = ";
            sql += "(" + strUnitID + ")";
            sql += "\r\n    and DataID = " + statusFrom;
            sql += "\r\n    order by dtDateTime";
            sql += "\r\n) as t1 where FuelData.iID = t1.iID";
            return sql;
        }
        public static String GetUnprocessedFuelDataFromFuelDataTable(String strUnitID, int Mylimit)
        {
            String sql = "select top " + Mylimit.ToString() + " * from FuelData ";
            sql += "\r\nwhere unitID = ";
            sql += "(" + strUnitID + ")";
            sql += "\r\n    and DataID = 1";
            sql += "\r\norder by dtDatetime";
            return sql;
        }

        public static String GetAllFuelGeotabUnitID()
        {
            String sql = "select distinct unitID from NaveoFuelData";
            return sql;
        }

        public static String GetOnlyLastRec()
        {
            String sql = @"select max(dtDateTime) from RawData with(noLock)";
            return sql;
        }

        public static String GetUnitIDsStatus()
        {
            String sql = @"select distinct(UnitID), Model, ServerName, DBName 
	                            , (select min(dtdatetime) from RawData where unitID = rd.unitID) StartDate
	                            , (select max(dtdatetime) from RawData where unitID = rd.unitID) EndDate
                            from RawData rd
                            order by Model, UnitID";
            return sql;
        }
        public static String GetUnitLastRec()
        {
            String sql = @"select rd.* 
                            from RawData rd
	                            , (select distinct(UnitID) 
                                        , (select max(dtdatetime) from RawData where unitID = rd.unitID
                                                and model not in "+ GlobalVariables.GetImsis() + @") EndDate
                            --            , (select max(iID) from RawData where unitID = rd.unitID) EndID
                                    from RawData rd) v1
                            where rd.unitID = v1.unitID
	                            --and rd.iID = v1.EndID
	                            and rd.dtDateTime = v1.EndDate
                            order by rd.Model, rd.UnitID";
            return sql;
        }
        public static String GetUnitFirstRec()
        {
            String sql = @"select rd.* 
                            from RawData rd
	                            , (select distinct(UnitID) 
                                        , (select min(dtdatetime) from RawData where unitID = rd.unitID) EndDate
                            --            , (select max(iID) from RawData where unitID = rd.unitID) EndID
                                    from RawData rd) v1
                            where rd.unitID = v1.unitID
	                            --and rd.iID = v1.EndID
	                            and rd.dtDateTime = v1.EndDate
                            order by rd.Model, rd.UnitID";
            return sql;
        }

        public static String GetNoOfUnitsPerModel()
        {
            String sql = @"select model, count(model) 
	                            from
		                            (select distinct(UnitID), Model from RawData) rd
	                            group by model
                            order by model
                        ";
            return sql;
        }

        public static String GetSQLConnections()
        {
            String sql = "		SELECT SPID,";
            sql += "\r\n STATUS,";
            sql += "\r\nPROGRAM_NAME,";
            sql += "\r\nLOGINAME=RTRIM(LOGINAME),";
            sql += "\r\nHOSTNAME,";
            sql += "\r\nCMD, Login_Time, Last_Batch";
            sql += "\r\nFROM  MASTER.DBO.SYSPROCESSES";
            sql += "\r\nWHERE DB_NAME(DBID) = 'GeoLink' AND DBID != 0 ";
            return sql;
        }
        public static String ExistTableSql(String pStrTable)
        {
            String sql = "select count(*) from dbo.sysobjects where id = object_id(N'" 
                + pStrTable 
                + "') and OBJECTPROPERTY(id, N'IsUserTable') = 1";
            return sql;
        }
        public static String CreateNaveoDbUpdate()
        {
            String sql = "create table NaveoDbUpdate( upd_id varchar(10) not null, upd_desc varchar(300) not null, upd_date datetime not null )";
            return sql;
        }
        public static String CheckNaveoDbUpdate(String UpdateId)
        {
            String sql = "select * from NaveoDbUpdate where upd_id = '";
            sql += UpdateId;
            sql += "'";
            return sql;
        }
        public static String InsertNaveoDb(String pUpd_Id, String pUpd_Desc)
        {
            String sql = "Insert into NaveoDbUpdate values( ";
            sql += "'" + pUpd_Id + "'";
            sql += ", '" + pUpd_Desc + "'";
            sql += ", getdate())";
            return sql;
        }

        public static String SQLFromDescToAsc(String sqlcmd)
        {
            String sql = "SELECT *\r\n";
            sql += "FROM (" + sqlcmd + ") x\r\n";
            sql += "ORDER BY dtDateTime";
            return sql;
        }

        public static String DeleteGeotabFutureRecords()
        {
            return "delete from GPSData where dtdatetime > getdate()";
        }
    }
}
