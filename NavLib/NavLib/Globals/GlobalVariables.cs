using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Configuration;

namespace NavLib.Globals
{
    public class GlobalVariables
    {
        //public static String UtilPath = Application.StartupPath + "\\Utilities\\";
        //public static String exePath = Application.ExecutablePath.ToString() + "\\ByteFile\\";
        public static String execDir = Application.StartupPath.ToString();
        public static String exeFile = System.IO.Path.GetFileName(Application.ExecutablePath);
        public static String bytePath()
        {
            String strP = Application.StartupPath.ToString() + "\\ByteFile\\";
            if (!(System.IO.Directory.Exists(strP)))
                System.IO.Directory.CreateDirectory(strP);

            return strP;
        }
        public static String zipPath()
        {
            String strP = Application.StartupPath.ToString() + "\\Zipped\\";
            if (!(System.IO.Directory.Exists(strP)))
                System.IO.Directory.CreateDirectory(strP);

            return strP;
        }
        public static String UtilPath()
        {
            String strP = Application.StartupPath.ToString() + "\\Utilities\\";
            if (!(System.IO.Directory.Exists(strP)))
                System.IO.Directory.CreateDirectory(strP);

            return strP;
        }
        public static String DailyTranPath()
        {
            String strP = Application.StartupPath.ToString() + "\\DailyTran\\";
            if (!(System.IO.Directory.Exists(strP)))
                System.IO.Directory.CreateDirectory(strP);

            return strP;
        }
        public static String logPath()
        {
            String strP = Application.StartupPath.ToString() + "\\Log\\";
            if (!(System.IO.Directory.Exists(strP)))
                System.IO.Directory.CreateDirectory(strP);

            return strP;
        }
        public static String FuelVehiclePath()
        {
            String strP = Application.StartupPath.ToString() + "\\FuelVehicle\\";
            if (!(System.IO.Directory.Exists(strP)))
                System.IO.Directory.CreateDirectory(strP);

            return strP;
        }
        public static String TelDlls()
        {
            String strP = Application.StartupPath.ToString() + "\\TelDlls\\";
            if (!(System.IO.Directory.Exists(strP)))
                System.IO.Directory.CreateDirectory(strP);

            return strP;
        }
        public static String ExportPath()
        {
            String strP = Application.StartupPath.ToString() + "\\Resources\\Template\\Export\\Excel";

            return strP;
        }

        public static String GetAppFolder()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
        }
        public static String GetNaveoAppFolder(String sAdditionalDir)
        {
            if (sAdditionalDir != String.Empty)
                sAdditionalDir = sAdditionalDir + "\\";
            String strP = GetAppFolder() + "\\Naveo\\" + sAdditionalDir;
            if (!(System.IO.Directory.Exists(strP)))
                System.IO.Directory.CreateDirectory(strP);

            return strP;
        }
        public static String GetAppDataFolder()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        }

        public static String GetsConnStr(String sConnStr)
        {
            String str = String.Empty;
            if (sConnStr == "ConnStr")
                str = ConfigurationManager.AppSettings[sConnStr].ToString();
            else if (sConnStr == "ConnStrTransferred")
            {
                str = ConfigurationManager.AppSettings["ConnStr"].ToString();
                String ServerName, UserID, Password, strDBName = String.Empty;

                char[] chSep = { ';' };
                String[] strIndivParam = str.Split(chSep);

                for (int i = 0; i < strIndivParam.Length; i++)
                {
                    int intFound = -1;
                    if ((intFound = strIndivParam[i].IndexOf("Data Source=")) != -1)
                        ServerName = strIndivParam[i].Substring(intFound + "Data Source=".Length, strIndivParam[i].Length - "Data Source=".Length);

                    if ((intFound = strIndivParam[i].IndexOf("User ID=")) != -1)
                        UserID = strIndivParam[i].Substring(intFound + "User ID=".Length, strIndivParam[i].Length - "User ID=".Length);

                    if ((intFound = strIndivParam[i].IndexOf("Pwd=")) != -1)
                        Password = strIndivParam[i].Substring(intFound + "Pwd=".Length, strIndivParam[i].Length - "Pwd=".Length);

                    if ((intFound = strIndivParam[i].IndexOf("Initial Catalog=")) != -1)
                        strDBName = strIndivParam[i].Substring(intFound + "Initial Catalog=".Length, strIndivParam[i].Length - "Initial Catalog=".Length);
                }

                str = str.Replace(strDBName, strDBName + "Transferred");
            }
            else if (sConnStr == "Geotab")
            {
                str = "Data Source=";
                str += ConfigurationManager.AppSettings["GeotabServer"].ToString();
                str += ";Initial Catalog=";
                str += ConfigurationManager.AppSettings["Database"].ToString();
                str += ";User ID=geotabuser;Pwd=vircom43;Connect Timeout=200;";
            }
            else if (sConnStr == String.Empty)
            {
                str = ConfigurationManager.ConnectionStrings["DB1"].ToString();
            }
            else if (sConnStr == "Core")
            {
                str = ConfigurationManager.ConnectionStrings["DB1"].ToString();
            }
            else if (sConnStr == "DB1")
            {
                str = ConfigurationManager.ConnectionStrings["DB1"].ToString();
            }
            else if (sConnStr == "DB2")
            {
                str = ConfigurationManager.ConnectionStrings["DB2"].ToString();
            }
            else if (sConnStr == "DB3")
            {
                str = ConfigurationManager.ConnectionStrings["DB3"].ToString();
            }
            else if (sConnStr == "DB4")
            {
                str = ConfigurationManager.ConnectionStrings["DB4"].ToString();
            }
            else if (sConnStr == "DB5")
            {
                str = ConfigurationManager.ConnectionStrings["DB5"].ToString();
            }
            else
                str = ConfigurationManager.ConnectionStrings[sConnStr].ToString();

            return str;
        }
        public static String GetDBName(String sConnStr)
        {
            String str, ServerName, UserID, Password, strDBName = String.Empty;
            str = GetsConnStr(sConnStr);

            char[] chSep = { ';' };
            String[] strIndivParam = str.Split(chSep);

            for (int i = 0; i < strIndivParam.Length; i++)
            {
                int intFound = -1;
                if ((intFound = strIndivParam[i].IndexOf("Data Source=")) != -1)
                    ServerName = strIndivParam[i].Substring(intFound + "Data Source=".Length, strIndivParam[i].Length - "Data Source=".Length);

                if ((intFound = strIndivParam[i].IndexOf("User ID=")) != -1)
                    UserID = strIndivParam[i].Substring(intFound + "User ID=".Length, strIndivParam[i].Length - "User ID=".Length);

                if ((intFound = strIndivParam[i].IndexOf("Pwd=")) != -1)
                    Password = strIndivParam[i].Substring(intFound + "Pwd=".Length, strIndivParam[i].Length - "Pwd=".Length);

                if ((intFound = strIndivParam[i].IndexOf("Initial Catalog=")) != -1)
                    strDBName = strIndivParam[i].Substring(intFound + "Initial Catalog=".Length, strIndivParam[i].Length - "Initial Catalog=".Length);
            }

            return strDBName;
        }
        public static String GetServerName(String sConnStr)
        {
            String str, ServerName = String.Empty, UserID, Password, strDBName = String.Empty;
            if (sConnStr == "Geotab")
            {
                str = "Data Source=";
                str += ConfigurationManager.AppSettings["GeotabServer"].ToString();
                str += ";Initial Catalog=";
                str += ConfigurationManager.AppSettings["Database"].ToString();
                str += ";User ID=geotabuser;Pwd=vircom43;Connect Timeout=200;";
            }
            else if (sConnStr == "ConnStrTransferred")
                str = ConfigurationManager.AppSettings["ConnStr"].ToString();
            else if (sConnStr == "ConnStr")
                str = ConfigurationManager.AppSettings["ConnStr"].ToString();
            else
                str = ConfigurationManager.ConnectionStrings[sConnStr].ToString();

            char[] chSep = { ';' };
            String[] strIndivParam = str.Split(chSep);

            for (int i = 0; i < strIndivParam.Length; i++)
            {
                int intFound = -1;
                if ((intFound = strIndivParam[i].IndexOf("Data Source=")) != -1)
                    ServerName = strIndivParam[i].Substring(intFound + "Data Source=".Length, strIndivParam[i].Length - "Data Source=".Length);

                if ((intFound = strIndivParam[i].IndexOf("User ID=")) != -1)
                    UserID = strIndivParam[i].Substring(intFound + "User ID=".Length, strIndivParam[i].Length - "User ID=".Length);

                if ((intFound = strIndivParam[i].IndexOf("Pwd=")) != -1)
                    Password = strIndivParam[i].Substring(intFound + "Pwd=".Length, strIndivParam[i].Length - "Pwd=".Length);

                if ((intFound = strIndivParam[i].IndexOf("Initial Catalog=")) != -1)
                    strDBName = strIndivParam[i].Substring(intFound + "Initial Catalog=".Length, strIndivParam[i].Length - "Initial Catalog=".Length);
            }

            return ServerName;
        }

        public static String GetFuelXmlTemplate()
        {
            String strResult = @"
<!-- 
<?xml version=""1.0"" encoding=""UTF-8""?>
 
     Document   : vehicle_101.xml
     Created on : October 14, 2009, 20:00
     Author     : Naveo RD
     Description: Configuration file template 
-->

<tns:vehicle xmlns:tns=""http://Naveo.mu/vehicle"" xmlns:xsi=""http://Naveo.mu/2014/XMLSchema-instance"" xsi:schemaLocation=""http://Naveo.mu/vehicle http://schemes.Naveo.mu/vehicle.xsd"">
<id>0</id>
<!-- Calibration date. Field format: YYYY-MM-DD -->
<calibrationDate>2009-09-01</calibrationDate>
<!-- Smoothing filter buffer length, points -->
<approximationBufferLength>50</approximationBufferLength>
<!-- Refuel threshold, decilitres, dl (1 dl = 0,1 L) -->
<fillThreshold>50</fillThreshold>
<!-- Drain threshold, dl -->
<drainThreshold>30</drainThreshold>
<!-- Refuel/drain search rough filter length, points -->
<roughFilterLength>25</roughFilterLength>
<!-- Refuel/drain search fine filter length, points -->
<fineFilterLength>20</fineFilterLength>
SensorsRezaDeveloping

<!-- Vehicle/machine registration number or license plate number -->
<registrationNumber>5427 ZP 02</registrationNumber>
<!-- The following 18 fields are reserved for future use -->
<phone></phone>
<rpmCoef>100000</rpmCoef>
<speedCoef>100000</speedCoef>
<levToConnLocal>0</levToConnLocal>
<levToConnRoaming>0</levToConnRoaming>
<timeOutConn>0</timeOutConn>
<tmrPeriod>0</tmrPeriod>
<alwaysActive>0</alwaysActive>
<turnInputPullUp>0</turnInputPullUp>
<sirenTurnOn>0</sirenTurnOn>
<speedInputType>0</speedInputType>
<speedInputRange>0</speedInputRange>
<speedInputPullUp>0</speedInputPullUp>
<serverIp>0.0.0.0</serverIp>
<serverPort>0</serverPort>
<apn></apn>
<sensorsCount>0</sensorsCount>
<manufactureId></manufactureId>
</tns:vehicle>
";
            return strResult;
        }
        public static String GetFuelXmlTemplateObsolete()
        {
            String strResult = @"<?xml version=""1.0"" encoding=""UTF-8""?>
<!-- 
     Document   : vehicle_101.xml
     Created on : October 14, 2009, 20:00
     Author     : Vepamon OU
     Description: Configuration file template 
-->

<tns:vehicle xmlns:tns=""http://schemes.omnicomm.ru/vehicle"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://schemes.omnicomm.ru/vehicle http://schemes.omnicomm.ru/vehicle.xsd"">
<id>1</id>
<!-- Calibration date. Field format: YYYY-MM-DD -->
<calibrationDate>2009-09-01</calibrationDate>
<!-- Smoothing filter buffer length, points -->
<approximationBufferLength>50</approximationBufferLength>
<!-- Refuel threshold, decilitres, dl (1 dl = 0,1 L) -->
<fillThreshold>50</fillThreshold>
<!-- Drain threshold, dl -->
<drainThreshold>30</drainThreshold>
<!-- Refuel/drain search rough filter length, points -->
<roughFilterLength>25</roughFilterLength>
<!-- Refuel/drain search fine filter length, points -->
<fineFilterLength>20</fineFilterLength>
<!-- Calibration tables for level sensors. There are can be up to 4 sensors on a vehicle. 
  Sensor ID is a unique number in range [0..3]. -->
	<sensor number=""0"">
	   <!-- List of code-value pairs for sensor 1 (internal number 0). Quantity of pairs - [2..4096].
       Code means raw level information from the sensor, value means real volume in decilitres. 
	   Each code value should be unique, in range - [0..4095], and increase/decrease monotonically -->
    <value code=""0"">0</value>
    <value code=""20"">30</value>
    <value code=""40"">65</value>
    <value code=""60"">100</value>
    <value code=""80"">135</value>
    <value code=""100"">170</value>
    <value code=""120"">205</value>
    <value code=""140"">240</value>
    <value code=""160"">275</value>
    <value code=""180"">310</value>
    <value code=""200"">345</value>
    <value code=""220"">380</value>
    <value code=""240"">415</value>
    <value code=""255"">450</value>
  </sensor>

<!-- Vehicle/machine registration number or license plate number -->
<registrationNumber>5427 ZP 02</registrationNumber>
<!-- The following 18 fields are reserved for future use -->
<phone></phone>
<rpmCoef>100000</rpmCoef>
<speedCoef>100000</speedCoef>
<levToConnLocal>0</levToConnLocal>
<levToConnRoaming>0</levToConnRoaming>
<timeOutConn>0</timeOutConn>
<tmrPeriod>0</tmrPeriod>
<alwaysActive>0</alwaysActive>
<turnInputPullUp>0</turnInputPullUp>
<sirenTurnOn>0</sirenTurnOn>
<speedInputType>0</speedInputType>
<speedInputRange>0</speedInputRange>
<speedInputPullUp>0</speedInputPullUp>
<serverIp>0.0.0.0</serverIp>
<serverPort>0</serverPort>
<apn></apn>
<sensorsCount>0</sensorsCount>
<manufactureId></manufactureId>
</tns:vehicle>
";
            return strResult;
        }

        public static String GetNaveoKey()
        {
            return "Naveo GPS Encryption 2812";
        }

        //primary, secondary, tertiary, quaternary, quinary, senary, septenary, octonary, nonary, and denary.
        public static String GetPrimarySyncService()
        {
            return "https://gpsDevices.naveo.mu/Service.asmx";
            return "http://52.143.151.8:88/Service.asmx";
            return "https://Naveo.is-into-cars.com/Service.asmx";
        }
        public static String GetSecondarySyncService()
        {
            return "https://gpsDevices.naveo.mu/Service.asmx";
            return "http://52.143.151.8:88/Service.asmx";
            return "https://NaveoSvr.is-into-cars.com/Service.asmx";
        }
        public static String GetTertiarySyncService()
        {
            //Government Online Center GOC
            //mys.govmu.org
            //public ip is 196.13.125.25
            return "http://192.168.33.49:88/Service.asmx";
        }
        public static String GetQuaternarySyncService()
        {
            //Government Online Center GOC
            //gender.govmu.org
            //public ip is 196.13.125.26
            return "http://192.168.33.50:88/Service.asmx";
        }
        public static String GetQuinarySyncService()
        {
            //Government Online Center GOC
            //moh.govmu.org
            //public ip is 196.13.125.53
            return "http://192.168.33.63:88/Service.asmx";
        }

        public static String GesenarySyncService()
        {
            //Government Online Center GOC
            //mhl.govmu.org
            //public ip is 196.13.125.147
            return "http://192.168.33.185:88/Service.asmx";
        }

        public static String GetImsis()
        { 
            return "('OrionImsi', 'AT100IMSI', 'EnforaImsi', 'MeitrackImsi', 'AtrackImsi', 'EnfornIMSI')"; 
        }
    }
}