﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Generic;

/// <summary>
/// Summary description for SSTCryptographer
/// </summary>
namespace NavLib.Globals
{
    public class SSTCryptographer
    {
        private static String _key;

        public SSTCryptographer()
        {
        }

        public static String Key
        {
            set
            {
                _key = value;
            }
        }

        /// <summary>
        /// Encrypt the given String using the default key.
        /// </summary>
        /// <param name="strToEncrypt">The String to be encrypted.</param>
        /// <returns>The encrypted String.</returns>
        public static String Encrypt(String strToEncrypt)
        {
            try
            {
                return Encrypt(strToEncrypt, _key);
            }
            catch (Exception ex)
            {
                return "Wrong Input. " + ex.Message;
            }

        }

        /// <summary>
        /// Decrypt the given String using the default key.
        /// </summary>
        /// <param name="strEncrypted">The String to be decrypted.</param>
        /// <returns>The decrypted String.</returns>
        public static String Decrypt(String strEncrypted)
        {
            try
            {
                return Decrypt(strEncrypted, _key);
            }
            catch (Exception ex)
            {
                return "Wrong Input. " + ex.Message;
            }
        }

        /// <summary>
        /// Encrypt the given String using the specified key.
        /// </summary>
        /// <param name="strToEncrypt">The String to be encrypted.</param>
        /// <param name="strKey">The encryption key.</param>
        /// <returns>The encrypted String.</returns>
        public static String Encrypt(String strToEncrypt, String strKey)
        {
            try
            {
                TripleDESCryptoServiceProvider objDESCrypto = new TripleDESCryptoServiceProvider();
                MD5CryptoServiceProvider objHashMD5 = new MD5CryptoServiceProvider();

                byte[] byteHash, byteBuff;
                String strTempKey = strKey;

                byteHash = objHashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strTempKey));
                objHashMD5 = null;
                objDESCrypto.Key = byteHash;
                objDESCrypto.Mode = CipherMode.ECB; //CBC, CFB

                byteBuff = ASCIIEncoding.ASCII.GetBytes(strToEncrypt);
                return Convert.ToBase64String(objDESCrypto.CreateEncryptor().TransformFinalBlock(byteBuff, 0, byteBuff.Length));
            }
            catch (Exception ex)
            {
                return "Wrong Input. " + ex.Message;
            }
        }

        /// <summary>
        /// Decrypt the given String using the specified key.
        /// </summary>
        /// <param name="strEncrypted">The String to be decrypted.</param>
        /// <param name="strKey">The decryption key.</param>
        /// <returns>The decrypted String.</returns>
        public static String Decrypt(String strEncrypted, String strKey)
        {
            try
            {
                TripleDESCryptoServiceProvider objDESCrypto = new TripleDESCryptoServiceProvider();
                MD5CryptoServiceProvider objHashMD5 = new MD5CryptoServiceProvider();

                byte[] byteHash, byteBuff;
                String strTempKey = strKey;

                byteHash = objHashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strTempKey));
                objHashMD5 = null;
                objDESCrypto.Key = byteHash;
                objDESCrypto.Mode = CipherMode.ECB; //CBC, CFB

                byteBuff = Convert.FromBase64String(strEncrypted);
                String strDecrypted = ASCIIEncoding.ASCII.GetString(objDESCrypto.CreateDecryptor().TransformFinalBlock(byteBuff, 0, byteBuff.Length));
                objDESCrypto = null;

                return strDecrypted;
            }
            catch (Exception ex)
            {
                return "Wrong Input. " + ex.Message;
            }
        }
    }
}