using System;
using System.Collections.Generic;
using System.Text;

namespace NavLib.Globals
{
    public class Utils
    {
        public static String BytesToHex(Byte[] bytPacketData)
        {
            String g = String.Empty;
            foreach (byte x in bytPacketData)
                g += x.ToString("X");
            return g;
        }
        public static String BytesToHex2(Byte[] bytPacketData)
        {
            String g = String.Empty;
            foreach (byte x in bytPacketData)
                g += x.ToString("X2");
            return g;
        }
        public static String BytesToDoubleHex(Byte[] bytPacketData)
        {
            String bResult = String.Empty;
            foreach (byte x in bytPacketData)
            {
                String strTemp = x.ToString("X");
                if (strTemp.Length == 1)
                    strTemp = "0" + strTemp;
                bResult += strTemp;// +"\r\n";
            }
            return bResult;
        }
        public static String BytesToDoubleHex(Byte[] bytPacketData, int iFrom, int iTo)
        {
            String bResult = String.Empty;
            for (int i = iFrom; i <= iTo; i++)
            {
                if (i < bytPacketData.Length)   //Sometimes last bit is not in message
                {
                    byte x = bytPacketData[i];
                    String strTemp = x.ToString("X");
                    if (strTemp.Length == 1)
                        strTemp = "0" + strTemp;
                    bResult += strTemp;
                }
            }
            return bResult;
        }
        public static String BytesToDoubleHexSeperate(Byte[] bytPacketData)
        {
            String bResult = String.Empty;
            int i = 0;
            foreach (byte x in bytPacketData)
            {
                String strTemp = x.ToString("X");
                if (strTemp.Length == 1)
                    strTemp = "0" + strTemp;
                bResult += i.ToString() + "\t" + strTemp + "\r\n";
                i++;
            }
            return bResult;
        }
        public static String ConvertToHex(String asciiString)
        {
            String hex = "";
            foreach (char c in asciiString)
            {
                int tmp = c;
                hex += String.Format("{0:x2}", (uint)System.Convert.ToUInt32(tmp.ToString()));
            }
            return hex;
        }
        public static String strToHex(String str)
        {
            String hex = "";
            char[] values = str.ToCharArray();
            foreach (char letter in values)
            {
                // Get the integral value of the character.
                int value = Convert.ToInt32(letter);
                // Convert the decimal value to a hexadecimal value in string form.
                string hexOutput = String.Format("{0:X}", value);
                hex += hexOutput + ";";
            }

            return hex;
        }

        public static byte[] StringToBytes(string data)
        {
            var array = new byte[data.Length / 2];

            var substring = 0;
            for (var i = 0; i < array.Length; i++)
            {
                array[i] = Byte.Parse(data.Substring(substring, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
                substring += 2;
            }

            return array;
        }

        public static int HexToDecimal(String hexValue)
        {
            int dec = int.Parse(hexValue, System.Globalization.NumberStyles.HexNumber);
            //dec = Convert.ToInt64(hexValue, 16);
            return dec;
        }
        public static String DecimalToHex(int decValue)
        {
            String hexValue = decValue.ToString("X");
            hexValue = String.Format("{0:X}", decValue);
            return hexValue;
        }

        public static Int64 HexToBigInt(String hexValue)
        {
            Int64 dec = Int64.Parse(hexValue, System.Globalization.NumberStyles.HexNumber);
            //dec = Convert.ToInt64(hexValue, 16);
            return dec;
        }
        public static String BigIntToHex(Int64 iValue)
        {
            String hexValue = iValue.ToString("X");
            hexValue = String.Format("{0:X}", iValue);
            return hexValue;
        }

        public static Byte[] HexString2Bytes(String hexString)
        {
            //check for null
            if (hexString == null) return null;
            //get length
            int len = hexString.Length;
            if (len % 2 == 1) return null;
            int len_half = len / 2;
            //create a byte array
            byte[] bs = new byte[len_half];
            try
            {
                //convert the hexstring to bytes
                for (int i = 0; i != len_half; i++)
                {
                    bs[i] = (byte)Int32.Parse(hexString.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
                }
            }
            catch { }

            return bs;
        }
        public static byte[] HexStringToByteArray(string Hex)
        {
            byte[] Bytes = new byte[Hex.Length / 2];
            int[] HexValue = new int[] { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09,
                                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0A, 0x0B, 0x0C, 0x0D,
                                 0x0E, 0x0F };

            for (int x = 0, i = 0; i < Hex.Length; i += 2, x += 1)
            {
                Bytes[x] = (byte)(HexValue[Char.ToUpper(Hex[i + 0]) - '0'] << 4 |
                                  HexValue[Char.ToUpper(Hex[i + 1]) - '0']);
            }

            return Bytes;
        }
        public static byte[] HexStringToBytes(string HexString)
        {
            //Downloaded codes. not yet tested. use HexStringToByteArray instead
            if (HexString.Length % 2 == 1)
                HexString = "0" + HexString;
            byte[] buf = new byte[HexString.Length / 2];
            for (int i = 0; i < HexString.Length / 2; i++)
            {
                buf[i] = Convert.ToByte(HexString.Substring(i * 2, 2), 16);
            }
            return buf;
        }
        public static string ByteArrayToHexString(byte[] Bytes)
        {
            StringBuilder Result = new StringBuilder();
            string HexAlphabet = "0123456789ABCDEF";

            foreach (byte B in Bytes)
            {
                Result.Append(HexAlphabet[(int)(B >> 4)]);
                Result.Append(HexAlphabet[(int)(B & 0xF)]);
            }

            return Result.ToString();
        }

        public static String AsciiToHex(String AsciiString)
        {
            byte[] b = System.Text.ASCIIEncoding.ASCII.GetBytes(AsciiString);
            return BitConverter.ToString(b).Replace("-", " ");
            //toasc.Text = System.Text.ASCIIEncoding.ASCII.GetString(HexStringToBytes(hextxt.Text.Replace(" ", "").Replace("-", "")));
        }
        public static String HexString2Ascii(String hexString)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i <= hexString.Length - 2; i += 2)
            {
                sb.Append(Convert.ToString(Convert.ToChar(Int32.Parse(hexString.Substring(i, 2), System.Globalization.NumberStyles.HexNumber))));
            }
            return sb.ToString();
        }

        public static String Hex2ASCII(String hex)
        {
            String res = String.Empty;

            for (int a = 0; a < hex.Length; a = a + 2)
            {
                String Char2Convert = hex.Substring(a, 2);
                int n = Convert.ToInt32(Char2Convert, 16);
                char c = (char)n;
                res += c.ToString();
            }
            return res;
        }
        public static String ASCII2Hex(String ascii)
        {
            StringBuilder sb = new StringBuilder();
            Byte[] inputBytes = Encoding.UTF8.GetBytes(ascii);

            foreach (Byte b in inputBytes)
                sb.Append(String.Format("{0:x2}", b));

            return sb.ToString();
        }

        public static String Hex2Binary(String hexValue)
        {
            string binaryVal = "";
            binaryVal = Convert.ToString(Convert.ToInt32(hexValue, 16), 2);
            return binaryVal;
        }
        public static String Binary2Hex(String BinaryValue)
        {
            return Convert.ToInt32(BinaryValue, 2).ToString("X");
        }

        public static Byte[] ReverseBytes(Byte[] OriginalB)
        {
            int iLen = OriginalB.Length;
            Byte[] bResult = new Byte[iLen];
            for (int i = 0; i < iLen; i++)
                bResult[i] = OriginalB[iLen - i - 1];
            return bResult;
        }
        public static String RemoveSpecialCharacters(String str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (Char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') | c == '.' || c == '_' || c == ' ')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        public static Byte[] bytWorking1024(Byte[] bytWorkingReduced)
        {
            // To save space on SQL db, null bytes were removed from bytWorking. Add then up now
            Byte[] b = new Byte[1024];
            int iLen = bytWorkingReduced.Length;
            Array.Copy(bytWorkingReduced, 0, b, 0, iLen);
            return b;
        }
        public static Byte[] CompressBytWorking(Byte[] bytWorking)
        {
            int iLen = bytWorking.Length;
            Byte[] b = new Byte[1];
            for (int i = bytWorking.Length - 1; i >= 0; --i)
                if (bytWorking[i] != b[0])
                {
                    iLen = i;
                    break;
                }
            Byte[] bytWorkingReduced = new Byte[iLen + 1];
            Array.Copy(bytWorking, 0, bytWorkingReduced, 0, iLen + 1);
            return bytWorkingReduced;
        }

        //Add leading 0s
        public static String Decimal2Binary(int n)
        {
            char[] b = new char[32];
            int pos = 31;
            int i = 0;

            while (i < 32)
            {
                if ((n & (1 << i)) != 0)
                    b[pos] = '1';
                else
                    b[pos] = '0';
                pos--;
                i++;
            }
            return new String(b);
        }
        public static int Binary2Decimal(String BinaryValue)
        {
            int dec = 0;
            for (int i = 0; i < BinaryValue.Length; i++)
            {
                // we start with the least significant digit, and work our way to the left
                if (BinaryValue[BinaryValue.Length - i - 1] == '0')
                    continue;

                dec += (int)Math.Pow(2, i);
            }
            return dec;
        }

        //Return only needed bits
        public static String DecimalToBinary(int n)
        {
            return Convert.ToString(n, 2).PadLeft(8, '0');
        }
        public static Int64 BinaryToDecimal(String BinaryValue)
        {
            return Convert.ToInt64(BinaryValue, 2);
        }

        //Return in bits of InBitsOf (e.g 4 > 0101)
        public static String DecimalToBinary(int i, int InBitsOf)
        {
            String result = DecimalToBinary(i);
            int remainder = result.Length % InBitsOf;

            while (remainder != 0)
            {
                result = "0" + result;
                remainder = result.Length % InBitsOf;
            }
            return result;
        }

        public static String BitwiseComplement(String BinaryValue)
        {
            //Bitwise complement changes all bits. It turns 0 into 1 and 1 into 0
            //The character "~" denotes the complement operator

            Int64 i = BinaryToDecimal(BinaryValue);
            i = ~i;
            String strResult = DecimalToBinary((int)i);
            return strResult.Substring(strResult.Length - BinaryValue.Length);
        }
        public static int TwosComplement(int i)
        {
            //Convert to binary 28  00011100
            //invert                11100011
            //Add 1
            String g = Utils.DecimalToBinary(i);
            g = Utils.BitwiseComplement(g);
            i = Utils.Binary2Decimal(g);
            i += 1;
            return i;
        }
        public static int hexToTwosComplement(String hex)
        {
            int i = HexToDecimal(hex);
            return TwosComplement(i);
        }

        public static String ToBinary(Int64 Decimal)
        {
            //Int64 DecimalValue = Convert.ToInt64(MyPacketData[30].ToString());
            //Int64 BinToDec = Convert.ToInt64(Convert.ToInt64("1010", 2));

            // Declare a few variables we're going to need
            Int64 BinaryHolder;
            char[] BinaryArray;
            String BinaryResult = "";

            while (Decimal > 0)
            {
                BinaryHolder = Decimal % 2;
                BinaryResult += BinaryHolder;
                Decimal = Decimal / 2;
            }

            // The algoritm gives us the binary number in reverse order (mirrored)
            // We store it in an array so that we can reverse it back to normal
            BinaryArray = BinaryResult.ToCharArray();
            Array.Reverse(BinaryArray);
            BinaryResult = new string(BinaryArray);

            while (BinaryResult.Length < 8)
                BinaryResult = "0" + BinaryResult;

            return BinaryResult;
        }
        public static Boolean GetBitValue(Byte[] MyPacketData, int Offset_Code, int pos)
        {
            Boolean bResult = false;
            String ReasonCode = ToBinary((Int64)MyPacketData[Offset_Code]);
            if (ReasonCode.Substring(pos, 1) == "1")
                bResult = true;
            return bResult;
        }
        public static Boolean GetBitValue(String BinaryString, int pos)
        {
            if (pos >= BinaryString.Length)
                return false;

            Boolean bResult = false;
            //Following conditions recently added to ensure pos is valid
            if (pos < BinaryString.Length)
                if (BinaryString.Substring(pos, 1) == "1")
                    bResult = true;
            return bResult;
        }

        public static ulong BuildLong(Byte[] MyPacketData, int intStartByte, int intEndByte)
        {
            ulong lngOut = 0;
            // Check if this is sane and within all the bounds!?
            if ((intEndByte > intStartByte) && (intEndByte <= MyPacketData.GetUpperBound(0)))
            {
                // For each needed byte.
                while (intStartByte < intEndByte)
                {
                    // Add the byte
                    lngOut += MyPacketData[intStartByte++];
                    // Shift it into place.
                    //Reza... arithmetic shift
                    //If only 1 byte is needed, return directly
                    //However, if more that 1 byte is needed, Shift is needed
                    // i = 5 --> 0000 0101
                    // i <<= 3 
                    //   ans 1 = 40 > 0000 0101 000 > 0010 1000
                    lngOut <<= 8;
                }
                // And add the last byte ( we don't have to shift after this
                lngOut += MyPacketData[intStartByte];
            }
            return lngOut;
        }

        // This routine takes a byte array and calcualtes the CRC16 of it.
        // All data minus the last 2 bytes are taken into account.
        public static uint GenerateCheckSum(Byte[] bytPacketDatam)
        {
            uint[] laCrc16LookUpTable = 
            {
                0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301, 0x03C0, 0x0280, 0xC241, 0xC601, 0x06C0, 0x0780, 0xC741, 0x0500,
                0xC5C1, 0xC481, 0x0440, 0xCC01, 0x0CC0, 0x0D80, 0xCD41, 0x0F00, 0xCFC1, 0xCE81, 0x0E40, 0x0A00, 0xCAC1,
                0xCB81, 0x0B40, 0xC901, 0x09C0, 0x0880, 0xC841, 0xD801, 0x18C0, 0x1980, 0xD941, 0x1B00, 0xDBC1, 0xDA81,
                0x1A40, 0x1E00, 0xDEC1, 0xDF81, 0x1F40, 0xDD01, 0x1DC0, 0x1C80, 0xDC41, 0x1400, 0xD4C1, 0xD581, 0x1540, 
                0xD701, 0x17C0, 0x1680, 0xD641, 0xD201, 0x12C0, 0x1380, 0xD341, 0x1100, 0xD1C1, 0xD081, 0x1040, 0xF001,
                0x30C0, 0x3180, 0xF141, 0x3300, 0xF3C1, 0xF281, 0x3240, 0x3600, 0xF6C1, 0xF781, 0x3740, 0xF501, 0x35C0,
                0x3480, 0xF441, 0x3C00, 0xFCC1, 0xFD81, 0x3D40, 0xFF01, 0x3FC0, 0x3E80, 0xFE41, 0xFA01, 0x3AC0, 0x3B80, 
                0xFB41, 0x3900, 0xF9C1, 0xF881, 0x3840, 0x2800, 0xE8C1, 0xE981, 0x2940, 0xEB01, 0x2BC0, 0x2A80, 0xEA41, 
                0xEE01, 0x2EC0, 0x2F80, 0xEF41, 0x2D00, 0xEDC1, 0xEC81, 0x2C40, 0xE401, 0x24C0, 0x2580, 0xE541, 0x2700, 
                0xE7C1, 0xE681, 0x2640, 0x2200, 0xE2C1, 0xE381, 0x2340, 0xE101, 0x21C0, 0x2080, 0xE041, 0xA001, 0x60C0, 
                0x6180, 0xA141, 0x6300, 0xA3C1, 0xA281, 0x6240, 0x6600, 0xA6C1, 0xA781, 0x6740, 0xA501, 0x65C0, 0x6480, 
                0xA441, 0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01, 0x6FC0, 0x6E80, 0xAE41, 0xAA01, 0x6AC0, 0x6B80, 0xAB41, 
                0x6900, 0xA9C1, 0xA881, 0x6840, 0x7800, 0xB8C1, 0xB981, 0x7940, 0xBB01, 0x7BC0, 0x7A80, 0xBA41, 0xBE01, 
                0x7EC0, 0x7F80, 0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40, 0xB401, 0x74C0, 0x7580, 0xB541, 0x7700, 0xB7C1, 
                0xB681, 0x7640, 0x7200, 0xB2C1, 0xB381, 0x7340, 0xB101, 0x71C0, 0x7080, 0xB041, 0x5000, 0x90C1, 0x9181, 
                0x5140, 0x9301, 0x53C0, 0x5280, 0x9241, 0x9601, 0x56C0, 0x5780, 0x9741, 0x5500, 0x95C1, 0x9481, 0x5440, 
                0x9C01, 0x5CC0, 0x5D80, 0x9D41, 0x5F00, 0x9FC1, 0x9E81, 0x5E40, 0x5A00, 0x9AC1, 0x9B81, 0x5B40, 0x9901, 
                0x59C0, 0x5880, 0x9841, 0x8801, 0x48C0, 0x4980, 0x8941, 0x4B00, 0x8BC1, 0x8A81, 0x4A40, 0x4E00, 0x8EC1, 
                0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80, 0x8C41, 0x4400, 0x84C1, 0x8581, 0x4540, 0x8701, 0x47C0, 0x4680, 
                0x8641, 0x8201, 0x42C0, 0x4380, 0x8341, 0x4100, 0x81C1, 0x8081, 0x4040
            };

            uint llcrc = 0xFFFF; 	/* high byte of CRC initialized */
            uint llChar;
            int lnPos;

            for (lnPos = 0; lnPos < bytPacketDatam.GetUpperBound(0) - 1; lnPos++)
            {
                llChar = (llcrc ^ bytPacketDatam[lnPos]) & 0x00ff;
                llChar = laCrc16LookUpTable[llChar];
                llcrc = (llcrc >> 8) ^ llChar;
            }
            return (llcrc);
        }
        private uint ReadCheckSum(Byte[] bytPacketDatam, int PacketLength)
        {
            uint intCheckSum = bytPacketDatam[PacketLength - 1];
            intCheckSum <<= 8;
            intCheckSum += bytPacketDatam[PacketLength];
            return intCheckSum;
        }

        static byte[] table8 = 
          {
             0, 94, 188, 226, 97, 63, 221, 131, 194, 156, 126,
             32, 163, 253, 31, 65, 157, 195, 33, 127, 252, 162,
             64, 30, 95, 1, 227, 189, 62, 96, 130, 220, 35, 
             125, 159, 193, 66, 28, 254, 160, 225, 191, 93, 3,
             128, 222, 60, 98, 190, 224, 2, 92, 223, 129, 99, 
             61, 124, 34, 192, 158, 29, 67, 161, 255, 70, 24, 
             250, 164, 39, 121, 155, 197, 132, 218, 56, 102, 
             229, 187, 89, 7, 219, 133, 103, 57, 186, 228, 6,
             88, 25, 71, 165, 251, 120, 38, 196, 154, 101, 59,
             217, 135, 4, 90, 184, 230, 167, 249, 27, 69, 198,
             152, 122, 36, 248, 166, 68, 26, 153, 199, 37, 123,
             58, 100, 134, 216, 91, 5, 231, 185, 140, 210, 48,
             110, 237, 179, 81, 15, 78, 16, 242, 172, 47, 113,
             147, 205, 17, 79, 173, 243, 112, 46, 204, 146,
             211, 141, 111, 49, 178, 236, 14, 80, 175, 241, 19,
             77, 206, 144, 114, 44, 109, 51, 209, 143, 12, 82,
             176, 238, 50, 108, 142, 208, 83, 13, 239, 177, 
             240, 174, 76, 18, 145, 207, 45, 115, 202, 148, 118,
             40, 171, 245, 23, 73, 8, 86, 180, 234, 105, 55, 
             213, 139, 87, 9, 235, 181, 54, 104, 138, 212, 149,
             203, 41, 119, 244, 170, 72, 22, 233, 183, 85, 11,
             136, 214, 52, 106, 43, 117, 151, 201, 74, 20, 246,
             168, 116, 42, 200, 150, 21, 75, 169, 247, 182, 232,
             10, 84, 215, 137, 107, 53
          };

        public static byte ComputeChecksum8(params byte[] bytes)
        {
            byte crc = 0;
            if (bytes != null && bytes.Length > 0)
            {
                foreach (byte b in bytes)
                {
                    crc = table8[crc ^ b];
                }
            }
            return crc;
        }

        public static bool isNumeric(string val, System.Globalization.NumberStyles NumberStyle)
        {
            Double result;
            return Double.TryParse(val, NumberStyle,
                System.Globalization.CultureInfo.CurrentCulture, out result);
            //Utils.isNumeric("310500123", System.Globalization.NumberStyles.Number)
        }

        public static Double degreesToRadians(Double degrees)
        { return degrees * Math.PI / 180; }
        public static Double radiansToDegrees(Double radians)
        { return radians * 180 / Math.PI; }
        public static Double calculateBearingTo(Double lat1, Double lng1, Double lat2, Double lng2)
        {
            //calc bearing from prev point to this one
            Double rLat1 = degreesToRadians(lat1);
            Double rLat2 = degreesToRadians(lat2);
            Double rDeltaLng = degreesToRadians(lng2 - lng1);

            Double y = Math.Sin(rDeltaLng) * Math.Cos(rLat2);
            Double x = Math.Cos(rLat1) * Math.Sin(rLat2) -
                    Math.Sin(rLat1) * Math.Cos(rLat2) * Math.Cos(rDeltaLng);
            Double brng = Math.Atan2(y, x);

            return (radiansToDegrees(brng) + 360) % 360;
        }

        static String getPointImage(Double bearing, Boolean isStop)
        {
            Double imagesCount = 16;
            Double degreesPerImage = 360 / imagesCount;
            String s = String.Empty;
            for (int i = 0; i < imagesCount; i++)
                s += ("Arrow" + i.ToString() + ".png;");
            String[] arrImage = s.Split(';');

            if (isStop)
            {
                return "Stop.png";
            }
            else
            {
                Double imageIndex = Math.Floor(bearing / degreesPerImage),
                    midlineDistance = bearing % degreesPerImage;
                if (midlineDistance > degreesPerImage / 2)
                {
                    imageIndex++;
                }
                imageIndex = imageIndex % arrImage.Length;

                if (imageIndex >= 16)
                    imageIndex = 0;

                return arrImage[Convert.ToInt16(imageIndex)];
            }
        }
        public static String DisplayZonesInGoogle(String strZoneDelimiters)
        {
            String strResult = String.Empty;
            //Zonename1:x1,y1;x2,y2;x3,y3|ZoneName2:x1,y1;x2,y2;x3,y3
            String[] Zones = strZoneDelimiters.Split('|');  //Zonename1:x1,y1;x2,y2;x3,y3
            String strZoneScriptBuilder = String.Empty;
            foreach (String zone in Zones)
            {
                String gMarker = String.Empty;
                String Locations = Environment.NewLine + "var zCoords = [ ";

                if (zone == String.Empty)
                    continue;
                String[] Zonepoints = zone.Split(';');      //Zonename1:x1,y1
                String zoneName = String.Empty;
                Double dx = 0.0;
                Double dy = 0.0;
                foreach (String zp in Zonepoints)
                {
                    if (zp == String.Empty)
                        continue;
                    String[] xy = zp.Split(',');            //Zonename1:x1
                    if (xy[0].Contains(":"))
                    {
                        int ifind = xy[0].IndexOf(":");
                        zoneName = xy[0].Substring(0, ifind);
                        dx = Convert.ToDouble(xy[0].Substring(ifind + 1, xy[0].Length - ifind - 1));
                        dy = Convert.ToDouble(xy[1]);
                        gMarker = "var myLatlng = new google.maps.LatLng(" + dy.ToString() + "," + dx.ToString() + ");\r\n";
                    }
                    else
                    {
                        dx = Convert.ToDouble(xy[0]);
                        dy = Convert.ToDouble(xy[1]);
                    }

                    Double dLatitude = dy;
                    Double dLongitude = dx;

                    Locations += Environment.NewLine + " new google.maps.LatLng(" + dLatitude.ToString() + "," + dLongitude.ToString() + "),\r\n";
                }
                Locations = Locations.Substring(0, Locations.Length - 1);
                Locations += "];\r\n";
                strZoneScriptBuilder += Locations + @"
                                      var gZone = new google.maps.Polygon({
                                        paths: zCoords,
                                        strokeColor: ""#FFBBFF"",
                                        strokeOpacity: 0.8,
                                        strokeWeight: 2,
                                        fillColor: ""#FFE1FF"",
                                        fillOpacity: 0.35
                                      });

                                      gZone.setMap(map);

                                      // Add a listener for the click event
                                      //google.maps.event.addListener(gZone, 'click', showArrays);
                                      //infowindow = new google.maps.InfoWindow();
                            ";
                strZoneScriptBuilder += gMarker;
                strZoneScriptBuilder += @"var image ='icons/transparent10x10.png'
                                        var marker = new google.maps.Marker({
                                              position: myLatlng,
                                              map: map,
                                              icon: image,
                                              title:'" + zoneName.Replace("'", "") + @" '
                                          });";
            }
            strResult = @"<script type='text/javascript'>
                            function DisplayGeneratedZones() { ";
            strResult += "\r\n     var gZone;";
            strResult += "\r\n" + strZoneScriptBuilder;
            strResult += "}\r\n</script> ";

            return strResult;
        }
        public static String DisplayTripsInGoogle(System.Data.DataTable dt)
        {
            int iActiveTripSelected = 0;    //Trip ID
            int iTripsSelected = 0;         //Total number of trips. When TripID is last trip, we enclose result in SCRIPT
            int iStopCount = 1;             //Stop Counter
            int iTmpMyDeviceID = 0;         //Check if MyDeviceID changes
            String strPointBuilder = String.Empty;
            String strCordsBuilder = String.Empty;

            Double dPrevLat = 0.0;
            Double dPrevLon = 0.0;
            int iGP = 0; int i = 0;
            //Double dDistance = 0; 
            //Double dtmpDist = 0;

            System.Data.DataView _dv = new System.Data.DataView(dt);
            _dv.Sort = "DeviceName ASC, DateTime ASC";

            for (int dvCounter = 0; dvCounter < _dv.Count; dvCounter++)
            {
                iGP++; i++;
                // bypass empty rows	 	
                if (_dv[dvCounter]["Latitude"].ToString().Trim().Length == 0)
                    continue;
                if (_dv[dvCounter]["Latitude"].ToString() == "0")
                    continue;
                if (_dv[dvCounter]["Longitude"].ToString() == "0")
                    continue;

                iActiveTripSelected = Convert.ToInt16(_dv[dvCounter]["TripID"].ToString());
                iTripsSelected = Convert.ToInt16(_dv[dvCounter]["TotalTrips"].ToString());

                if (i > 1)
                    if (_dv[i - 2]["Latitude"].ToString().Trim().Length > 0 || _dv[dvCounter]["Latitude"].ToString() != "0")
                    {
                        dPrevLat = Convert.ToDouble(_dv[dvCounter - 1]["Latitude"].ToString());
                        dPrevLon = Convert.ToDouble(_dv[dvCounter - 1]["Longitude"].ToString());
                        //dDistance = Convert.ToDouble(_dv[dvCounter]["Distance"].ToString()) - Convert.ToDouble(_dv[dvCounter - 1]["Distance"].ToString());
                    }

                Double dLatitude = Convert.ToDouble(_dv[dvCounter]["Latitude"].ToString());
                Double dLongitude = Convert.ToDouble(_dv[dvCounter]["Longitude"].ToString());
                Double bBearing = Utils.calculateBearingTo(dPrevLat, dPrevLon, dLatitude, dLongitude);

                int iImagePic = Convert.ToInt16(_dv[dvCounter]["MyDeviceID"].ToString());
                if (iTmpMyDeviceID != iImagePic)
                {
                    iStopCount = 1;
                    iTmpMyDeviceID = iImagePic;
                }
                //Google icon
                //if (dDistance > 0.1)
                {
                    String strTitle = _dv[dvCounter]["DeviceName"].ToString() + " ";
                    strTitle += "@ " + _dv[dvCounter]["DateTime"].ToString() + " ";
                    if (Convert.ToBoolean(_dv[dvCounter]["IsStop"].ToString()))
                    {
                        strTitle += "Stopped " + iStopCount.ToString();
                        iStopCount++;
                    }
                    else
                        strTitle += "Driving @ " + _dv[dvCounter]["Speed"].ToString() + " km"; //_dv[dvCounter]["LogReason"].ToString() + 

                    strPointBuilder += "\r\n";
                    strPointBuilder += "var image = ";
                    strPointBuilder += "'deviceDirections/" + getPointImage(bBearing, Convert.ToBoolean(_dv[dvCounter]["IsStop"].ToString())) + "'";
                    strPointBuilder += "\r\n";
                    strPointBuilder += "var myLatLng = new google.maps.LatLng(" + dLatitude.ToString() + ", " + dLongitude.ToString() + ");";
                    strPointBuilder += @"var beachMarker = new google.maps.Marker({
                                                  position: myLatLng,
                                                  map: map,
                                                  icon: image,
                                                  title : '" + strTitle + @"'
                                                  });
                                                ";
                }
                strCordsBuilder += "new google.maps.LatLng(" + dLatitude.ToString() + ", " + dLongitude.ToString() + "),";
            }

            String strResult = "            function DisplayGeneratedPoints() {";
            strResult += strPointBuilder + "\r\n";
            strResult += "                var flightPlanCoordinates = [";
            strResult += "\r\n";
            strResult += strCordsBuilder.Substring(0, strCordsBuilder.Length - 1);

            String strStrokeColor = "\"#6699FF\"";
            strResult += @"                  ];
                var flightPath = new google.maps.Polyline({
                                        path: flightPlanCoordinates,
                                        strokeColor: " + strStrokeColor + @",
                                        strokeOpacity: 1.0,
                                        strokeWeight: 2
                                    });

                                    flightPath.setMap(map);
                                //} //Close Function
                ";

            if (iActiveTripSelected == iTripsSelected)
            {
                //Centre Map
                strResult += @"var bounds = new google.maps.LatLngBounds();
                                    for (var i = 0; i < flightPlanCoordinates.length; i++) {
                                        bounds.extend(flightPlanCoordinates[i]);
                                    }

                                    bounds.getCenter();
                                    map.fitBounds(bounds);
                                }   //Close Function
                                    ";
                strResult = @"<script type='text/javascript'>" + "\r\n" + strResult;
                strResult += "\r\n</script> ";
            }
            return strResult;
        }
        public static String DisplayPointsInGoogle(System.Data.DataTable dt)
        {
            String strBuilder = String.Empty;
            String strCordsBuilder = String.Empty;  //Centre Map
            String strResult = "            function DisplayGeneratedPoints() { clearOverlays();";
            int iGP = 0; int i = 0;

            //Remove all
            foreach (System.Data.DataRow r in dt.Rows)
            {
                iGP++; i++;
                // bypass empty rows	 	
                if (r["Latitude"].ToString().Trim().Length == 0)
                    continue;
                if (r["Latitude"].ToString() == "0")
                    continue;
                if (r["Longitude"].ToString() == "0")
                    continue;
                if (r["GPSValid"].ToString().ToLower() == "false")
                    continue;

                String strVehicleImage = "'icons/MyMegane.png'; ";
                int iImagePic = Convert.ToInt16(r["MyDeviceID"].ToString());
                if (iImagePic <= 42)
                    strVehicleImage = "'icons/Megane" + iImagePic.ToString() + ".png'; ";
                strBuilder = "var image = " + strVehicleImage;

                String strTitle = "var mytitle = '";
                strTitle += r["DeviceName"].ToString() + " ";
                strTitle += "@" + r["DateTime"].ToString() + " ";
                if (!Convert.ToBoolean(r["Ignition"].ToString()))
                    strTitle += "stopped";
                else
                    strTitle += "Driving @ " + r["Speed"].ToString() + " km"; //r["LogReason"].ToString() + 
                strTitle += "';";
                strBuilder += strTitle;

                String strLatitude = r["Latitude"].ToString();
                String strLongitude = r["Longitude"].ToString();
                Double dLatitude = Convert.ToDouble(strLatitude);
                Double dLongitude = Convert.ToDouble(strLongitude);

                strBuilder += "\r\n";
                strBuilder += "AddTrackPoint(" + dLatitude.ToString() + ", " + dLongitude.ToString() + ", \"0000FF\", \"mob2\", image, mytitle);";
                strResult += strBuilder;

                strCordsBuilder += "new google.maps.LatLng(" + dLatitude.ToString() + ", " + dLongitude.ToString() + "),";
            }

            //Centre Map
            if (strCordsBuilder.Length > 1)
            {
                strResult += "                var flightPlanCoordinates = [";
                strResult += "\r\n";
                strResult += strCordsBuilder.Substring(0, strCordsBuilder.Length - 1);
                strResult += "                  ];";
                strResult += @"var bounds = new google.maps.LatLngBounds();
                                    for (var i = 0; i < flightPlanCoordinates.length; i++) {
                                        bounds.extend(flightPlanCoordinates[i]);
                                    }
            
                                    bounds.getCenter();
                                    map.fitBounds(bounds);
                                    ";
            }

            strResult += "\r\n            } DisplayGeneratedPoints();";
            strBuilder = "<script type=\"text/javascript\">";
            strBuilder += "\r\n" + strResult;
            strBuilder += "\r\n</script> ";
            return strBuilder;
        }

        public static Double getTimeZone(String TimeZoneId)
        {
            Double dTimeZone = 0;
            switch (TimeZoneId)
            {
                case "Arabian Standard Time":
                    dTimeZone = +4.00;
                    break;

                //list.Items.Add(new ListItem("(UTC-12:00) International Date Line West", "Dateline Standard Time"));
                //list.Items.Add(new ListItem("(UTC-11:00) Coordinated Universal Time-11", "UTC-11"));
                //list.Items.Add(new ListItem("(UTC-11:00) Samoa", "Samoa Standard Time"));
                //list.Items.Add(new ListItem("(UTC-10:00) Hawaii", "Hawaiian Standard Time"));
                //list.Items.Add(new ListItem("(UTC-09:00) Alaska", "Alaskan Standard Time"));
                //list.Items.Add(new ListItem("(UTC-08:00) Baja California", "Pacific Standard Time (Mexico)"));
                //list.Items.Add(new ListItem("(UTC-08:00) Pacific Time (US & Canada)", "Pacific Standard Time"));
                //list.Items.Add(new ListItem("(UTC-07:00) Arizona", "US Mountain Standard Time"));
                //list.Items.Add(new ListItem("(UTC-07:00) Chihuahua, La Paz, Mazatlan", "Mountain Standard Time (Mexico)"));
                //list.Items.Add(new ListItem("(UTC-07:00) Mountain Time (US & Canada)", "Mountain Standard Time"));
                //list.Items.Add(new ListItem("(UTC-06:00) Central America", "Central America Standard Time"));
                //list.Items.Add(new ListItem("(UTC-06:00) Central Time (US & Canada)", "Central Standard Time"));
                //list.Items.Add(new ListItem("(UTC-06:00) Guadalajara, Mexico City, Monterrey", "Central Standard Time (Mexico)"));
                //list.Items.Add(new ListItem("(UTC-06:00) Saskatchewan", "Canada Central Standard Time"));
                //list.Items.Add(new ListItem("(UTC-05:00) Bogota, Lima, Quito", "SA Pacific Standard Time"));
                //list.Items.Add(new ListItem("(UTC-05:00) Eastern Time (US & Canada)", "Eastern Standard Time"));
                //list.Items.Add(new ListItem("(UTC-05:00) Indiana (East)", "US Eastern Standard Time"));
                //list.Items.Add(new ListItem("(UTC-04:30) Caracas", "Venezuela Standard Time"));
                //list.Items.Add(new ListItem("(UTC-04:00) Asuncion", "Paraguay Standard Time"));
                //list.Items.Add(new ListItem("(UTC-04:00) Atlantic Time (Canada)", "Atlantic Standard Time"));
                //list.Items.Add(new ListItem("(UTC-04:00) Cuiaba", "Central Brazilian Standard Time"));
                //list.Items.Add(new ListItem("(UTC-04:00) Georgetown, La Paz, Manaus, San Juan", "SA Western Standard Time"));
                //list.Items.Add(new ListItem("(UTC-04:00) Santiago", "Pacific SA Standard Time"));
                //list.Items.Add(new ListItem("(UTC-03:30) Newfoundland", "Newfoundland Standard Time"));
                //list.Items.Add(new ListItem("(UTC-03:00) Brasilia", "E. South America Standard Time"));
                //list.Items.Add(new ListItem("(UTC-03:00) Buenos Aires", "Argentina Standard Time"));
                //list.Items.Add(new ListItem("(UTC-03:00) Cayenne, Fortaleza", "SA Eastern Standard Time"));
                //list.Items.Add(new ListItem("(UTC-03:00) Greenland", "Greenland Standard Time"));
                //list.Items.Add(new ListItem("(UTC-03:00) Montevideo", "Montevideo Standard Time"));
                //list.Items.Add(new ListItem("(UTC-02:00) Coordinated Universal Time-02", "UTC-02"));
                //list.Items.Add(new ListItem("(UTC-02:00) Mid-Atlantic", "Mid-Atlantic Standard Time"));
                //list.Items.Add(new ListItem("(UTC-01:00) Azores", "Azores Standard Time"));
                //list.Items.Add(new ListItem("(UTC-01:00) Cape Verde Is.", "Cape Verde Standard Time"));
                //list.Items.Add(new ListItem("(UTC) Casablanca", "Morocco Standard Time"));
                //list.Items.Add(new ListItem("(UTC) Coordinated Universal Time", "Coordinated Universal Time"));
                //list.Items.Add(new ListItem("(UTC) Dublin, Edinburgh, Lisbon, London", "GMT Standard Time"));
                //list.Items.Add(new ListItem("(UTC) Monrovia, Reykjavik", "Greenwich Standard Time"));
                //list.Items.Add(new ListItem("(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna", "W. Europe Standard Time"));
                //list.Items.Add(new ListItem("(UTC+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague", "Central Europe Standard Time"));
                //list.Items.Add(new ListItem("(UTC+01:00) Brussels, Copenhagen, Madrid, Paris", "Romance Standard Time"));
                //list.Items.Add(new ListItem("(UTC+01:00) Sarajevo, Skopje, Warsaw, Zagreb", "Central European Standard Time"));
                //list.Items.Add(new ListItem("(UTC+01:00) West Central Africa", "W. Central Africa Standard Time"));
                //list.Items.Add(new ListItem("(UTC+01:00) Windhoek", "Namibia Standard Time"));
                //list.Items.Add(new ListItem("(UTC+02:00) Amman", "Jordan Standard Time"));
                //list.Items.Add(new ListItem("(UTC+02:00) Athens, Bucharest, Istanbul", "GTB Standard Time"));
                //list.Items.Add(new ListItem("(UTC+02:00) Beirut", "Middle East Standard Time"));
                //list.Items.Add(new ListItem("(UTC+02:00) Cairo", "Egypt Standard Time"));
                //list.Items.Add(new ListItem("(UTC+02:00) Damascus", "Syria Standard Time"));
                //list.Items.Add(new ListItem("(UTC+02:00) Harare, Pretoria", "South Africa Standard Time"));
                //list.Items.Add(new ListItem("(UTC+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius", "FLE Standard Time"));
                //list.Items.Add(new ListItem("(UTC+02:00) Jerusalem", "Jerusalem Standard Time"));
                //list.Items.Add(new ListItem("(UTC+02:00) Minsk", "E. Europe Standard Time"));
                //list.Items.Add(new ListItem("(UTC+03:00) Baghdad", "Arabic Standard Time"));
                //list.Items.Add(new ListItem("(UTC+03:00) Kuwait, Riyadh", "Arab Standard Time"));
                //list.Items.Add(new ListItem("(UTC+03:00) Moscow, St. Petersburg, Volgograd", "Russian Standard Time"));
                //list.Items.Add(new ListItem("(UTC+03:00) Nairobi", "E. Africa Standard Time"));
                //list.Items.Add(new ListItem("(UTC+03:30) Tehran", "Iran Standard Time"));
                //list.Items.Add(new ListItem("(UTC+04:00) Abu Dhabi, Muscat", "Arabian Standard Time"));
                //list.Items.Add(new ListItem("(UTC+04:00) Baku", "Azerbaijan Standard Time"));
                //list.Items.Add(new ListItem("(UTC+04:00) Port Louis", "Mauritius Standard Time"));
                //list.Items.Add(new ListItem("(UTC+04:00) Tbilisi", "Georgian Standard Time"));
                //list.Items.Add(new ListItem("(UTC+04:00) Yerevan", "Caucasus Standard Time"));
                //list.Items.Add(new ListItem("(UTC+04:30) Kabul", "Afghanistan Standard Time"));
                //list.Items.Add(new ListItem("(UTC+05:00) Ekaterinburg", "Ekaterinburg Standard Time"));
                //list.Items.Add(new ListItem("(UTC+05:00) Islamabad, Karachi", "Pakistan Standard Time"));
                //list.Items.Add(new ListItem("(UTC+05:00) Tashkent", "West Asia Standard Time"));
                //list.Items.Add(new ListItem("(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi", "India Standard Time"));
                //list.Items.Add(new ListItem("(UTC+05:30) Sri Jayawardenepura", "Sri Lanka Standard Time"));
                //list.Items.Add(new ListItem("(UTC+05:45) Kathmandu", "Nepal Standard Time"));
                //list.Items.Add(new ListItem("(UTC+06:00) Astana", "Central Asia Standard Time"));
                //list.Items.Add(new ListItem("(UTC+06:00) Dhaka", "Bangladesh Standard Time"));
                //list.Items.Add(new ListItem("(UTC+06:00) Novosibirsk", "N. Central Asia Standard Time"));
                //list.Items.Add(new ListItem("(UTC+06:30) Yangon (Rangoon)", "Myanmar Standard Time"));
                //list.Items.Add(new ListItem("(UTC+07:00) Bangkok, Hanoi, Jakarta", "SE Asia Standard Time"));
                //list.Items.Add(new ListItem("(UTC+07:00) Krasnoyarsk", "North Asia Standard Time"));
                //list.Items.Add(new ListItem("(UTC+08:00) Beijing, Chongqing, Hong Kong, Urumqi", "China Standard Time"));
                //list.Items.Add(new ListItem("(UTC+08:00) Irkutsk", "North Asia East Standard Time"));
                //list.Items.Add(new ListItem("(UTC+08:00) Kuala Lumpur, Singapore", "Malay Peninsula Standard Time"));
                //list.Items.Add(new ListItem("(UTC+08:00) Perth", "W. Australia Standard Time"));
                //list.Items.Add(new ListItem("(UTC+08:00) Taipei", "Taipei Standard Time"));
                //list.Items.Add(new ListItem("(UTC+08:00) Ulaanbaatar", "Ulaanbaatar Standard Time"));
                //list.Items.Add(new ListItem("(UTC+09:00) Osaka, Sapporo, Tokyo", "Tokyo Standard Time"));
                //list.Items.Add(new ListItem("(UTC+09:00) Seoul", "Korea Standard Time"));
                //list.Items.Add(new ListItem("(UTC+09:00) Yakutsk", "Yakutsk Standard Time"));
                //list.Items.Add(new ListItem("(UTC+09:30) Adelaide", "Cen. Australia Standard Time"));
                //list.Items.Add(new ListItem("(UTC+09:30) Darwin", "AUS Central Standard Time"));
                //list.Items.Add(new ListItem("(UTC+10:00) Brisbane", "E. Australia Standard Time"));
                //list.Items.Add(new ListItem("(UTC+10:00) Canberra, Melbourne, Sydney", "AUS Eastern Standard Time"));
                //list.Items.Add(new ListItem("(UTC+10:00) Guam, Port Moresby", "West Pacific Standard Time"));
                //list.Items.Add(new ListItem("(UTC+10:00) Hobart", "Tasmania Standard Time"));
                //list.Items.Add(new ListItem("(UTC+10:00) Vladivostok", "Vladivostok Standard Time"));
                //list.Items.Add(new ListItem("(UTC+11:00) Magadan", "Magadan Standard Time"));
                //list.Items.Add(new ListItem("(UTC+11:00) Solomon Is., New Caledonia", "Central Pacific Standard Time"));
                //list.Items.Add(new ListItem("(UTC+12:00) Auckland, Wellington", "New Zealand Standard Time"));
                //list.Items.Add(new ListItem("(UTC+12:00) Coordinated Universal Time+12", "UTC+12"));
                //list.Items.Add(new ListItem("(UTC+12:00) Fiji", "Fiji Standard Time"));
                //list.Items.Add(new ListItem("(UTC+12:00) Petropavlovsk-Kamchatsky - Old", "Kamchatka Standard Time"));
                //list.Items.Add(new ListItem("(UTC+13:00) Nuku'alofa", "Tonga Standard Time"));
            }
            return dTimeZone;
        }

        public static Boolean IsAddressAvailable(String address)
        {
            try
            {
                System.Net.WebClient client = new System.Net.WebClient();
                client.DownloadData(address);
                return true;
            }
            catch
            {
                return false;
            }
        }

        #region Weeks
        public static DateTime GetStartOfLastWeek()
        {
            int DaysToSubtract = (int)DateTime.Now.DayOfWeek + 7;
            DateTime dt = DateTime.Now.Subtract(TimeSpan.FromDays(DaysToSubtract));
            return new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0, 0);
        }
        public static DateTime GetEndOfLastWeek()
        {
            DateTime dt = GetStartOfLastWeek().AddDays(6);
            return new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 59, 999);
        }
        public static DateTime GetStartOfCurrentWeek()
        {
            int DaysToSubtract = (int)DateTime.Now.DayOfWeek;
            DateTime dt = DateTime.Now.Subtract(TimeSpan.FromDays(DaysToSubtract));
            return new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0, 0);
        }
        public static DateTime GetEndOfCurrentWeek()
        {
            DateTime dt = GetStartOfCurrentWeek().AddDays(6);
            return new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 59, 999);
        }

        public static DateTime FirstDateInWeek(DateTime dt, DayOfWeek weekStartDay)
        {
            while (dt.DayOfWeek != weekStartDay)
                dt = dt.AddDays(-1);
            return new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0, 0);
        }
        public static DateTime LastDateInWeek(DateTime dt, DayOfWeek weekStartDay)
        {
            DateTime dtr = FirstDateInWeek(dt, weekStartDay).AddDays(6);
            return new DateTime(dtr.Year, dtr.Month, dtr.Day, 23, 59, 59, 999);
        }
        public static DateTime FirstDateInLastWeek(DateTime dt, DayOfWeek weekStartDay)
        {
            return FirstDateInWeek(DateTime.Now, DayOfWeek.Monday).AddDays(-7);
        }
        public static DateTime LastDateInLastWeek(DateTime dt, DayOfWeek weekStartDay)
        {
            DateTime dtr = FirstDateInLastWeek(dt, weekStartDay).AddDays(6);
            return new DateTime(dtr.Year, dtr.Month, dtr.Day, 23, 59, 59, 999);
        }
        #endregion

        #region HDD spaces
        public static long GetTotalFreeSpace(string driveName)
        {
            foreach (System.IO.DriveInfo drive in System.IO.DriveInfo.GetDrives())
            {
                if (drive.IsReady && drive.Name == driveName)
                {
                    return drive.TotalFreeSpace;
                }
            }
            return -1;
        }
        #endregion

    }
}