﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NavLib.Globals
{
    public static class MyMath
    {
        /*            
         * int[] M = { 41,48,42,41,43,40,53,41,46,41,40,52,40,47,47,46,47,45,46,42,40,45,46,40,46,54,-999 };
            int x;
            int f = MyMath.MostFreq(M, out x);

            List<int> ints = new List<int>() { 41, 48, 42, 41, 43, 40, 53, 41, 46, 41, 40, 52, 40, 47, 47, 46, 47, 45, 46, 42, 40, 45, 46, 40, 46, 54, -999 };
            int mode = MyMath.Mode<int>(ints);
            int mode = MyMath.Mode<int>(M);
        */

        public static int MostFreq(int[] _M, out int x)
        {
            //x = MostFreq
            //Max_Freq = Number of occurrence of x

            //First I need to sort the array in ascending order
            int Max_Freq, No_Freq, i, k;
            Array.Sort(_M);
            k = _M[0];
            Max_Freq = 0; i = 0; x = 0;
            while (i < _M.Length)
            {
                //No_Freq= the frequency of the current number
                No_Freq = 0;
                //X here is the number which is appear in the array Frequently 
                while (k == _M[i])
                {
                    No_Freq++;
                    i++;
                    if (i == _M.Length)
                        break;
                }
                if (No_Freq > Max_Freq)
                {
                    //so it will be printed the same
                    Max_Freq = No_Freq;
                    x = k;
                }
                if (i < _M.Length) k = _M[i];
            }
            return (Max_Freq);
        }
        public static int sumAverageElements<T>(IEnumerable<T> list, out int average)
        {
            // Initialize the return value
            int sum = 0;
            average = 0;

            // Test for a null reference and an empty list
            if (list != null)//&& list.Count() > 0)
            {
                int i = 0;
                foreach (T element in list)
                {
                    sum += int.Parse(element.ToString());
                    i++;
                }

                average = sum / i;
            }
            return sum;
        }

        /// <summary>
        /// Gets the element that occurs most frequently in the collection.
        /// </summary>
        /// <param name="list"></param>
        /// <returns>Returns the element that occurs most frequently in the collection.
        /// If all elements occur an equal number of times, a random element in
        /// the collection will be returned.</returns>
        public static T Mode<T>( IEnumerable<T> list)
        {
            // Initialize the return value
            T mode = default(T);

            // Test for a null reference and an empty list
            if (list != null )//&& list.Count() > 0)
            {
                // Store the number of occurences for each element
                Dictionary<T, int> counts = new Dictionary<T, int>();

                // Add one to the count for the occurence of a character
                foreach (T element in list)
                {
                    if (counts.ContainsKey(element))
                        counts[element]++;
                    else
                        counts.Add(element, 1);
                }

                // Loop through the counts of each element and find the 
                // element that occurred most often
                int max = 0;

                foreach (KeyValuePair<T, int> count in counts)
                {
                    if (count.Value > max)
                    {
                        // Update the mode
                        mode = count.Key;
                        max = count.Value;
                    }
                }
            }

            return mode;
        }
    }
}
