﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NavLib.Globals;
using NavLib.CRC;

namespace NavLib.Metron
{
    public class DecodeMetron
    {
        /*
POWE,METRON2AP,AP1000000M2110192193,20060101045502,3133
S,0,0,23.50,0,0,EMTEL-MRU,22,7FBD
L,20060101045502,1,7369,43FC
E
>>>POWE,METRON2AP,AP1000000M2110192193,20060101045502,3133\r\nS,0,0,23.50,0,0,EMTEL-MRU,22,7FBD\r\nL,20060101045502,1,7369,43FC\r\nE\r\n
         */
        int OffsetHeader = 0;
        int OffsetType = 1;
        int OffsetRTUID = 2;
        int OffsetDT = 3;
        int OffsetCRC = 4;

        String[] MsgArrayHeader;
        String[] MsgArrayStatus;
        String[] MsgArrayValue;
        String[] MsgArrayEnd;

        Byte[] bFullbytPacketData;
        System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();

        public DecodeMetron(Byte[] bytPacketData)
        {
            Byte[] MyPacketData = Utils.CompressBytWorking(bytPacketData);
            String Msg = Encoding.ASCII.GetString(MyPacketData);

            nfi.NumberDecimalSeparator = ".";
            String[] MsgArray = Msg.Split('\r');
            MsgArrayHeader = MsgArray[0].Split(',');
            MsgArrayStatus = MsgArray[1].Split(',');
            MsgArrayValue = MsgArray[2].Split('%');
            MsgArrayEnd = MsgArray[3].Split('\n');

            bFullbytPacketData = bytPacketData;
        }
        
        public String GetHeader()
        {
            //“POWE” will be a fixed string and used to identify PowElectrics RTUs when they callout to the host system
            return MsgArrayHeader[OffsetHeader];
        }
        public String GetPowElectricsType()
        {
            //A string further qualifying what type of PowElectrics RTU is calling in. 
            return MsgArrayHeader[OffsetType];
        }
        public String GetUnitID()
        {
            //The RTUID is factory assigned. 
            return MsgArrayHeader[OffsetRTUID];
        }
        public DateTime GetUTCdt()
        {
            //24h format, time is always UTC
            //YYYYMMDDhhmmss
            String g = MsgArrayHeader[OffsetDT];
            int yy = Convert.ToInt16(g.Substring(0, 4));
            int mm = Convert.ToInt16(g.Substring(4, 2));
            int dd = Convert.ToInt16(g.Substring(6, 2));
            int HH = Convert.ToInt16(g.Substring(8, 2));
            int MM = Convert.ToInt16(g.Substring(10, 2));
            int SS = Convert.ToInt16(g.Substring(12, 2));
            DateTime dt = new DateTime(yy, mm, dd, HH, MM, SS);
            return dt;
        }
        public String GetCRC()
        {
            //CRC based on rtuid, timestamp, and predefined secret password. 
            return MsgArrayHeader[OffsetCRC];
        }

        public String GetStatus()
        {
            //S denotes that this is a status message. 
            return MsgArrayStatus[0];
        }
        public Boolean isMaintenanceMode()
        {
            //Denotes whether or not the unit is in offline maintenance mode 
            //(i.e “maintenance in progress”. 
            //0 = not MIP 
            //1 = MIP 
            //A “1” value will allow the host system to recognise that the unit is inactive and prevent further communications
            //, until the unit notifies the host system with a “0” value (i.e unit is active). 
            String s = MsgArrayStatus[1];
            if (s == "1")
                return true;
            return false;
        }
        public Boolean isFailedCallout()
        {
            //Set to 1 if last callout attempt and all re-attempts failed 
            String s = MsgArrayStatus[2];
            if (s == "1")
                return true;
            return false;
        }
        public float GetTemperature()
        {
            //characters including decimal point and sign. 22.56 
            //Temperature of the unit in degrees C. 
            //Positive temperature is assumed when no sign given. 
            //Negative temperature explicitly stated by a “-” prefixed to the value. If [tempenabled] parameter in section 2.5 is set to “0” then no value is provided (empty string). 
            float f = 0;
            try 
            {
                f = (float)Convert.ToDouble(MsgArrayStatus[3]);
            }
            catch { }
            return f;
        }
        public Boolean isBatteryLow()
        {
            //Denotes whether the battery level is below the configured threshold. 0 = not below threshold 1 = below threshold 
            String s = MsgArrayStatus[4];
            if (s == "1")
                return true;
            return false;
        }
        public Boolean isAutoConfig()
        {
            //Denotes if the host system is to send configuration strings to the unit. 
            //0 = do not send 
            //1 = do send 
            //This functionality will allow the unit to receive a complete configuration from the host system.  
            //This allows the unit to be set up with a minimum configuration, and then contact the host system for complete configuration settings.  
            //The unit will turn this bit off when it is has received and successfully processed host-to-unit messages Command 2, Command 3, and Command 7. 
            String s = MsgArrayStatus[5];
            if (s == "1")
                return true;
            return false;
        }
        public String GetCarrier()
        {
            return MsgArrayStatus[6];
        }
        public int GetSignal()
        {
            int i = 0;
            try 
            {
                i = Convert.ToInt16(MsgArrayStatus[7]);
            }
            catch { }

            return i;
        }
        public String GetStatusCRC()
        {
            //CRC check based on all characters from start of message to end excluding commas, and predefined secret password 
            return MsgArrayStatus[8];
        }

        public String GetValueLog()
        {
            //L denotes this is a log values message. 
            String[] s = MsgArrayValue[0].Split(',');
            return s[0];
        }
        String sGetChannelData(int iChannel)
        {
            //iChannel > 0
            String sChannel = String.Empty;
            int i = MsgArrayValue.Length;

            if (iChannel <= i)
                sChannel = MsgArrayValue[iChannel - 1];

            return sChannel;
        }
        public DateTime GetValueUTCdt(int iChannel)
        {
            String sChannel = sGetChannelData(iChannel);
            if (sChannel == String.Empty)
                return new DateTime(2000, 01, 01);

            int i = 0;
            if (iChannel == 1)
                i = 1;

            //24h format, time is always UTC
            //YYYYMMDDhhmmss
            String g = sChannel.Split(',')[i];
            int yy = Convert.ToInt16(g.Substring(0, 4));
            int mm = Convert.ToInt16(g.Substring(4, 2));
            int dd = Convert.ToInt16(g.Substring(6, 2));
            int HH = Convert.ToInt16(g.Substring(8, 2));
            int MM = Convert.ToInt16(g.Substring(10, 2));
            int SS = Convert.ToInt16(g.Substring(12, 2));
            DateTime dt = new DateTime(yy, mm, dd, HH, MM, SS);
            return dt;
        }
        public int GetValueChannel(int iChannel)
        {
            //Understood that there will only be 8 channels (4 analogue and 4 digital if APMED board used) so in effect will be 1-8, but 99 is specified for future expansion. 
            String sChannel = sGetChannelData(iChannel);
            if (sChannel == String.Empty)
                return -1;

            int i = 1;
            if (iChannel == 1)
                i = 2;

            //24h format, time is always UTC
            //YYYYMMDDhhmmss
            String g = sChannel.Split(',')[i];
            return Convert.ToInt16(g);
        }
        public float GetValueChannelValue(int iChannel)
        {
            //Scaled value - max. 8 characters including decimal point 
            //e.g 12345.67(a); OFF/ON(d) 
            //If the unit has no reading for the channel, the scaled value that corresponds to the unscaled (engineering) zero reading will be sent. 
            //For digital channels, a low-to-high state transition will be indicated by the text “ON”, and a high-to-low state transition will be indicated by the text “OFF”. 
            String sChannel = sGetChannelData(iChannel);
            if (sChannel == String.Empty)
                return 0;

            int i = 2;
            if (iChannel == 1)
                i = 3;

            String g = sChannel.Split(',')[i];
            float f = 0;
            if (g == "ON")
                f = 1;
            else if (g == "OFF")
                f = 0;
            else
            {
                try
                {
                    f = (float)Convert.ToDouble(g);
                }
                catch { }
            }

            return f;
        }
        public String GetValueCRC()
        {
            //4 characters 0-9,A-F 
            //CRC check is based on all characters from start of message to end, excluding commas, percentage sign delimiters, and predefined secret password 
            String[] s = MsgArrayValue[MsgArrayValue.Length - 1].Split(',');
            int i = 3;
            if (MsgArrayValue.Length == 1) //need to chk. 1 or 2
                i = 4;
            return s[i].Substring(0, 4);
        }

        public Boolean hasEndBlock()
        {
            if(MsgArrayEnd.Length >= 2)
                if (MsgArrayEnd[1] == "E")
                return true;

            return false;
        }

        public static String PreparedMsg(String msg)
        {
            //2477,3,9,x,74AB   The above example uses  the secret password ‘secret’ and so the CRC for this command will be based on ‘247739xsecret’ 
            //String s = msg.Replace(",", "") + "secret";
            //byte[] b = System.Text.Encoding.ASCII.GetBytes(s);
            //byte[] z = new Crc16Ccitt(InitialCrcValue.Reza).ComputeChecksumBytes(b);
            //String CRC = Utils.BytesToDoubleHex(z);
            //s = msg + "," + CRC + "\r\n";
            //s = msg + CRC + "\r\nE\r\n";
            //return s;

            String s = String.Empty;
            String[] g = msg.Split('\n');
            foreach (String m in g)
            {
                if (m.Trim() == String.Empty)
                    continue;

                String sMyCmd = m.Replace(",", "") + "secret";
                byte[] b = System.Text.Encoding.ASCII.GetBytes(sMyCmd);
                byte[] z = new Crc16Ccitt(InitialCrcValue.Reza).ComputeChecksumBytes(b);
                String CRC = Utils.BytesToDoubleHex(z);
                s += m + CRC + "\r\n";
            }
            s += "\r\nE\r\n";
            return s;
        }
        public static String SetRealClock()
        {
            DateTime dt = DateTime.UtcNow;
            String s = "1234,0," + dt.ToString("yyyy") + dt.ToString("MM") + dt.ToString("dd");
            s += "," + dt.ToString("HH");
            s += "," + dt.ToString("mm");
            s += ",";

            return s;
        }
        public static String PollConnectedDevice()
        {
            return "1234,6,";
        }
    }
}
