﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NavLib.Globals;
using NavLib.CRC;

namespace NavLib.Metron
{
    public class DecodeMetronAtex
    {
        /*
H,MetronATEX,M09130502,060101000437,2088,,,
L,,level_1,268;temp,27.75;ss,21;net,EMTEL-MRU
E
         */
        int OffsetHeader = 0;
        int OffsetType = 1;
        int OffsetRTUID = 2;
        int OffsetDT = 3;

        String[] MsgArrayHeader;
        String[] MsgArrayValue;
        String[] MsgArrayEnd;

        Byte[] bFullbytPacketData;
        System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();

        public DecodeMetronAtex(Byte[] bytPacketData)
        {
            Byte[] MyPacketData = Utils.CompressBytWorking(bytPacketData);
            String Msg = Encoding.ASCII.GetString(MyPacketData);

            nfi.NumberDecimalSeparator = ".";
            String[] MsgArray = Msg.Split('\r');
            MsgArrayHeader = MsgArray[0].Split(',');
            MsgArrayValue = MsgArray[1].Split(';');
            MsgArrayEnd = MsgArray[2].Split('\n');

            bFullbytPacketData = bytPacketData;
        }

        public String GetHeader()
        {
            //“POWE” will be a fixed string and used to identify PowElectrics RTUs when they callout to the host system
            return MsgArrayHeader[OffsetHeader];
        }
        public String GetPowElectricsType()
        {
            //A string further qualifying what type of PowElectrics RTU is calling in. 
            return MsgArrayHeader[OffsetType];
        }
        public String GetUnitID()
        {
            //The RTUID is factory assigned. 
            return MsgArrayHeader[OffsetRTUID];
        }
        public DateTime GetUTCdt()
        {
            //24h format, time is always UTC
            //YYYYMMDDhhmmss
            String g = MsgArrayHeader[OffsetDT];
            int yy = Convert.ToInt16(g.Substring(0, 2)) + 2000;
            int mm = Convert.ToInt16(g.Substring(2, 2));
            int dd = Convert.ToInt16(g.Substring(4, 2));
            int HH = Convert.ToInt16(g.Substring(6, 2));
            int MM = Convert.ToInt16(g.Substring(8, 2));
            int SS = Convert.ToInt16(g.Substring(10, 2));
            DateTime dt = new DateTime(yy, mm, dd, HH, MM, SS);
            //if (dt.Year < 2015)
            //    dt = DateTime.UtcNow;

            return dt;
        }

        public String GetValueLog()
        {
            //L denotes this is a log values message. 
            String[] s = MsgArrayValue[0].Split(',');
            return s[0];
        }
        String sGetChannelData(int iChannel)
        {
            //iChannel > 0
            String sChannel = String.Empty;
            int i = MsgArrayValue.Length;

            if (iChannel <= i)
                sChannel = MsgArrayValue[iChannel - 1];

            return sChannel;
        }
        public int GetChannelName(int iChannel)
        {
            String sChannel = sGetChannelData(iChannel);
            if (sChannel == String.Empty)
                return -1;

            //int i = 0;
            //if (iChannel == 1)
            //    i = 2;
            //String g = sChannel.Split(',')[i];
            return iChannel;
        }
        public float GetChannelValue(int iChannel)
        {
            String sChannel = sGetChannelData(iChannel);
            if (sChannel == String.Empty)
                return 0;

            int i = 1;
            if (iChannel == 1)
                i = 3;

            String g = sChannel.Split(',')[i];
            float f = 0;
            if (g == "ON")
                f = 1;
            else if (g == "OFF")
                f = 0;
            else
            {
                try
                {
                    f = (float)Convert.ToDouble(g);
                }
                catch { }
            }

            return f;
        }

        public float GetTemperature()
        {
            float f = 0;
            try
            {
                for (int i = 1; i < 10; i++)
                {
                    String[] s = sGetChannelData(i).Split(',');
                    if (s[0] == "temp")
                        return GetChannelValue(i);
                }
            }
            catch { }
            return f;
        }


        public String GetCarrier()
        {
            String s = MsgArrayValue[MsgArrayValue.Length - 1];
            return s.Split(',')[1];
        }
        public int GetSignal()
        {
            int i = 0;
            try
            {
                String s = MsgArrayValue[MsgArrayValue.Length - 2];
                String g = s.Split(',')[1];
                i = Convert.ToInt16(g);
            }
            catch { }

            return i;
        }

        public Boolean hasEndBlock()
        {
            if (MsgArrayEnd.Length >= 2)
                if (MsgArrayEnd[1] == "E")
                    return true;

            return false;
        }

        public static String PreparedMsg(String msg)
        {
            msg += "\r\n";
            return msg;
        }
        public static String SetRealClock()
        {
            DateTime dt = DateTime.UtcNow;
            String s = "SP,1,20" + dt.ToString("yy") + dt.ToString("MM") + dt.ToString("dd") + dt.ToString("HH") + dt.ToString("mm") + dt.ToString("ss") + "\r\nA\r\n";
            return s;
        }
        public static String PollConnectedDevice()
        {
            return "1234,6";
        }
    }
}
