﻿using System;
using System.Collections.Generic;
using System.Text;
using NavLib.Globals;

namespace NavLib.Meitrack
{
    public class DecodeMeiTrack
    {
        //$$P134,359231038240915,AAA,35,-20.225775,57.506450,111205084637,A,5,27,0,193,2.8,375,6176,61351,617|10|0068|56B0,0000,001E|||02D6|00F5,*0F
        //$$B133,359231038903306,AFF,1,1,-20.225955,57.506203,120113080608,A,9,31,0,94,0.8,369,0,430,617|10|0068|56AF,0100,0008|0007||02D9|00F8,*BC
        int OffsetHeader = 0;
        int OffsetSerialNo = 1;
        int OffsetProtocol = 2;
        int OffsetLogReason = 3;
        int OffsetLat = 4;
        int OffsetLon = 5;
        int OffsetDate = 6;
        int OffsetGPSValid = 7;
        int OffsetSatellites = 8;
        //int OffsetGSMSignal = 9;
        int OffsetSpeed = 10;
        //int OffsetAngle = 11;
        //int OffsetDHop = 12;
        //int OffsetAltitude = 13;
        //int OffsetMileage = 14;
        //int OffsetRunTime = 15;
        //int OffsetBaseID = 16;
        //int OffsetState = 17;
        int OffsetAD = 18;

        String[] MsgArray;
        String[] MsgArrayInFull;

        System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();

        public DecodeMeiTrack(Byte[] bytPacketData)
        {
            Byte[] MyPacketData = Utils.CompressBytWorking(bytPacketData);
            String Msg = Encoding.ASCII.GetString(MyPacketData);

            nfi.NumberDecimalSeparator = ".";
            MsgArray = Msg.Split(',');
            MsgArrayInFull = MsgArray;

            if (GetProtocol().Trim() == "AFF")
            {
                OffsetLogReason = 3 + 1;
                OffsetLat = 4 + 1;
                OffsetLon = 5 + 1;
                OffsetDate = 6 + 1;
                OffsetGPSValid = 7 + 1;
                OffsetSatellites = 8 + 1;
                //OffsetGSMSignal = 9 + 1;
                OffsetSpeed = 10 + 1;
                //OffsetAngle = 11 + 1;
                //OffsetDHop = 12 + 1;
                //OffsetAltitude = 13 + 1;
                //OffsetMileage = 14 + 1;
                //OffsetRunTime = 15 + 1;
                //OffsetBaseID = 16 + 1;
                //OffsetState = 17 + 1;
                OffsetAD = 18 + 1;
            }
        }

        public int GetReportLength()
        { 
            return MsgArray.Length;
        }
        public String GetPackageFlag()
        {
            //$$P134 >> P
            String strResult = MsgArray[OffsetHeader];
            return strResult.Substring(2, 1);
        }
        public String GetIMEI()
        {
            String strResult = MsgArray[OffsetSerialNo];
            return strResult;
        }
        public String GetProtocol()
        {
            //AAA - No reply needed
            //AFF - Need Server reply. ACK
            String strResult = MsgArray[OffsetProtocol];
            return strResult;
        }
        public Double GetLatitude()
        {
            String g = MsgArray[OffsetLat].Trim();
            Double LatX = Double.Parse(g, nfi);
            return LatX;
        }
        public Double GetLongitude()
        {
            String g = MsgArray[OffsetLon].Trim();
            Double LatX = Double.Parse(g, nfi);
            return LatX;
        }
        public DateTime GetUTCDate()
        {
            String g = MsgArray[OffsetDate];
            int yy = 2000 + Convert.ToInt16(g.Substring(0, 2));
            int mm = Convert.ToInt16(g.Substring(2, 2));
            int dd = Convert.ToInt16(g.Substring(4, 2));
            int HH = Convert.ToInt16(g.Substring(6, 2));
            int MM = Convert.ToInt16(g.Substring(8, 2));
            int SS = Convert.ToInt16(g.Substring(10, 2));
            DateTime dt = new DateTime(yy, mm, dd, HH, MM, SS);
            return dt;
        }
        public Boolean IsGPSValid()
        {
            Boolean bResult = false;
            String g = MsgArray[OffsetGPSValid];
            if (g == "A")
                bResult = true;
            return bResult;
        }
        public Double GetSpeed()
        {
            //Km/h
            String str = MsgArray[OffsetSpeed];
            return Convert.ToDouble(str, nfi);
        }
        public Boolean GetAux1(String StrModel)
        {
            //Still in progress
            Boolean bResult = false;

            if (GetLogReason(StrModel).Contains("Aux1"))
                bResult = true;

            //if (GetLogReason(String.Empty).Contains("9 Aux1 Off"))
            //    bResult = true;

            return bResult;
        }
        public Boolean GetAux2(String StrModel)
        {
            Boolean bResult = false;

            if (GetLogReason(StrModel).Contains("Aux2"))
                bResult = true;

            return bResult;
        }
        public Boolean GetAux3(String StrModel)
        {
            Boolean bResult = false;

            if (GetLogReason(StrModel).Contains("Aux3"))
                bResult = true;

            return bResult;
        }
        public Boolean GetAux4(String StrModel)
        {
            Boolean bResult = false;

            if (GetLogReason(StrModel).Contains("Aux4"))
                bResult = true;

            return bResult;
        }
        public String GetAD()
        {
            String strResult = String.Empty;
            if (MsgArray.Length >= OffsetAD)
                strResult = MsgArray[OffsetAD];
            return strResult;
        }
        public String GetsatelliteNos()
        {
            String strResult = MsgArray[OffsetSatellites];
            return strResult;
        }

        public String GetCheckSumInASCII(String ss)
        {
            // 2 bytes
            // Start of message till *
            String checksum = String.Empty;
            if (ss.Length > 0)
            {
                if (ss.IndexOf("$$") != 0 && ss.IndexOf("@@") != 0)
                    return checksum;

                int pos1 = ss.IndexOf("*");
                if (pos1 == -1)
                    return checksum;

                checksum = CheckSumInASCII(ss.Substring(0, pos1 + 1));
            }
            return checksum;
            //byte[] b = System.Text.ASCIIEncoding.ASCII.GetBytes(checksum);
            //this.textBox2.Text = BitConverter.ToString(b).Replace("-", " ");
            //this.textBox1.Text = checksum;
        }
        public String GetCheckSumInHex(String ss)
        {
            String g = GetCheckSumInASCII(ss);
            byte[] b = System.Text.ASCIIEncoding.ASCII.GetBytes(g);
            return BitConverter.ToString(b).Replace("-", " ");
        }
        String CheckSumInASCII(String str)
        {
            byte[] b = System.Text.ASCIIEncoding.ASCII.GetBytes(str);
            byte bsum = 0;
            for (int i = 0; i < b.Length; i++)
            {
                bsum = (byte)(bsum + b[i]);
            }
            return bsum.ToString("X").PadLeft(2, '0');
        }
        public int GetMessageLength(String str)
        {
            //Length of message excluding header, package flag and L (length)
            //String str = ",353358017784062,AFF,1*0B\r\n";
            return str.Length;
        }
        public String GetAFFReplyMessage(String strPackageFlag, String strIMEI, int NoOfMessagesToAck)
        {
            //message from tracker $$J134,359231038240915,AFF,3,35,-20.219218,57.468311,111207110911,V,0,31,0,0,0,233,59665,145929,617|10|0068|56AF,0200,0014|||02D5|0102,*F8
            //Reply from server @@J27,359231038240915,AFF,1*BA
            //String strResult = "@@J27," + strIMEI + ",AFF," + NoOfMessagesToAck.ToString() + "*";
            //strResult += GetCheckSumInASCII(strResult);
            //strResult += "\r\n";
            //return strResult;

            //Prepare AFF message                                                               ,353358017784062,AFF,1*
            //insert Header from server to tracker                                              @@
            //insert package flag A - z. it is flag from original message                       J
            //insert message length, excluding header, flag, length 
            //    but including checksum and \r\n                                               27
            //insert checksum from @ to * ("@@h27,353358017784062,AFF,1*)                       BA
            //insert "\r\n"                                                                     "\r\n"
            String strResult = "," + strIMEI + ",AFF," + NoOfMessagesToAck.ToString() + "*";
            strResult = "@@" + strPackageFlag + (strResult.Length + 4).ToString() + strResult;  //4 > \r\n + 2 bytes for checksum
            strResult += CheckSumInASCII(strResult);
            strResult += "\r\n";
            return strResult;
        }
        public String GetIMSIMessage(String strIMEI)
        {
            //@@T25,359231037552112,B58*6D\r\n
            //$$T45,359231038728596,B58,8923002401090357446*4C
            //insert package flag A - z. it is flag from original message                       T
            //insert message length, excluding header, flag, length 
            //    but including checksum and \r\n                                               25
            //insert checksum from @ to * (@@T25,359231037552112,B58*)                          6D
            //insert "\r\n"                                                                     "\r\n"
            String strResult = "," + strIMEI + ",B58*";
            strResult = "@@T" + (strResult.Length + 4).ToString() + strResult;  //4 > \r\n + 2 bytes for checksum
            strResult += CheckSumInASCII(strResult);
            strResult += "\r\n";
            return strResult;
        }
        public String GetMietrackMessage(String strIMEI, String strMessage)
        {
            //Still testing...
            //@@T25,359231037552112,B58*6D\r\n
            //$$T45,359231038728596,B58,8923002401090357446*4C
            //insert package flag A - z. it is flag from original message                       T
            //insert message length, excluding header, flag, length 
            //    but including checksum and \r\n                                               25
            //insert checksum from @ to * (@@T25,359231037552112,B58*)                          6D
            //insert "\r\n"                                                                     "\r\n"
            String strResult = "," + strIMEI + "," + strMessage + "*";
            strResult = "@@T" + (strResult.Length + 4).ToString() + strResult;  //4 > \r\n + 2 bytes for checksum
            strResult += CheckSumInASCII(strResult);
            strResult += "\r\n";
            return strResult;
        }
        public String GetIMSI()
        {
            String strResult = String.Empty;
            if (GetProtocol() == "B58")
                strResult = MsgArray[3].Substring(0, 19).Trim();

            return strResult;
        }

        public String GetLogReason(String StrModel)
        {
            String strResult = String.Empty;
            String str = MsgArray[OffsetLogReason];
            switch (str.Trim()) //Default to MVT380
            {
                case "1":
                    strResult = "1 Panic -ve (Aux1On) (Aux)";
                    break;
                case "2":
                    strResult = "2 -ve (Aux2On) (Aux)";
                    break;
                case "3":
                    strResult = "3 -ve (Aux3On) (Aux)";
                    break;
                case "4":
                    strResult = "4 +ve (Aux4On) (Aux)";
                    break;
                case "5":
                    strResult = "5 IgnOn (Ignition)";
                    break;
                case "9":
                    strResult = "9 Panic -ve (Aux1Off) (Aux)";
                    break;
                case "10":
                    strResult = "10 -ve (Aux2Off) (Aux)";
                    break;
                case "11":
                    strResult = "11 -ve (Aux3Off) (Aux)";
                    break;
                case "12":
                    strResult = "12 +ve (Aux4Off) (Aux)";
                    break;
                case "13":
                    strResult = "13 (IgnOff) (Ignition)";
                    break;
                case "19":
                    strResult = "19 Speed (Heading)";
                    break;
                case "22":
                    strResult = "22 External Power on (Reset)(main power up)";
                    break;
                case "23":
                    strResult = "23 External Power off (Reset)";
                    break;
                case "31":
                    strResult = "31 Heartbeat Report (Heartbeat)";
                    break;
                case "32":
                    strResult = "32 Angle (Heading)";
                    break;
                case "33":
                    strResult = "33 Time (Heading)";
                    break;
                default:
                    strResult = str + "(???)";
                    break;
            }

            switch (StrModel.Trim())
            {
                case "MVT100":
                    switch (str.Trim())
                    {
                        case "2":
                            strResult = "2 IgnOn (Ignition)";
                            break;
                        case "10":
                            strResult = "10 (IgnOff) (Ignition)";
                            break;
                    }
                    break;

                case "MVT340":
                    switch (str.Trim())
                    {
                        case "2":
                            strResult = "2 IgnOn (Ignition)";
                            break;
                        case "10":
                            strResult = "10 (IgnOff) (Ignition)";
                            break;
                    }
                    break;
            }

            return str + " > " + strResult;
        }
    }
}