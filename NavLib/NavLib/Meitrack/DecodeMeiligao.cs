﻿using System;
using System.Collections.Generic;
using System.Text;
using NavLib.Globals;

namespace NavLib.Meitrack
{
    public class DecodeMeiligao
    {
        //From server to tracker
        //  @@<L (2 Bytes)><ID (7 Bytes)><command (2 Bytes)><parameter><checksum (2 bytes)>\r\n
        //From tracker to server
        //  $$<L (2 Bytes)><ID (7 Bytes)><command (2 Bytes)><data><checksum (2 bytes)>\r\n
        //$$\0EB /ÿ™™195922.000,A,2013.2090,S,05728.0962,E,5.08,35,140612,,*2B|5.3|241|0000|0146,0122|0269000A006859B1|0E|000070C2:ê\r\n
        // 0, 1 Header
        // 2, 3 Length of whole packet
        // 4, 5 ,6 ,7 ,8 ,9 ,10 ID
        // 11, 12 Log reason

        String[] MsgArray;
        String[] MsgArrayInFull;
        Byte[] MyPacketData;
        String Msg = String.Empty;

        System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();

        public DecodeMeiligao(Byte[] bytPacketData)
        {
            MyPacketData = Utils.CompressBytWorking(bytPacketData);
            Msg = Encoding.ASCII.GetString(MyPacketData);

            nfi.NumberDecimalSeparator = ".";
            MsgArray = Msg.Split(',');
            MsgArrayInFull = MsgArray;
        }

        public String GetPackageFlag()
        {
            //$$
            String strResult = String.Empty;
            strResult = MyPacketData[0].ToString("X");
            strResult += MyPacketData[1].ToString("X");
            return strResult;
        }
        public String GetSerial()
        {
            //bytes 4,5,6,7,8,9,10
            String strResult = String.Empty;
            Byte[] Serial = new Byte[7];
            Array.Copy(MyPacketData, 4, Serial, 0, 7);
            strResult = Utils.BytesToDoubleHex(Serial);
            strResult = strResult.Replace("F", String.Empty);
            return strResult;
        }
        public String GetLogReason()
        {
            String strResult = String.Empty;
            String g = GetCommand();
            if (g == "9999")
            {
                switch (MyPacketData[13].ToString("X"))
                { 
                    case "1":
                        strResult = "SOS Pressed (???)";
                        break;

                    case "2":
                        strResult = "Call buttonB pressed (???)";
                        break;

                    case "3":
                        strResult = "Call buttonC pressed (???)";
                        break;

                    case "10":
                        strResult = "Low Battery(???)";
                        break;

                    case "11":
                        strResult = "Speed (Heading)";
                        break;

                    case "14":
                        strResult = "tracker being turned on (Reset)(main power up)";
                        break;

                    case "15":
                        strResult = "No GPS (GpsShutdown)";
                        break;

                    default:
                        strResult = MyPacketData[13].ToString("X") + " (Heading)";
                        break;
                }
                strResult = "Alarm " + strResult;
            }
            else
                strResult = g + " Command other than alarm(???)";

            return strResult;
        }
        String GetCommand()
        { 
            //9955 : time interval
            //9999 : alarm event
            String strResult = String.Empty;
            strResult = MyPacketData[11].ToString("X");
            strResult += MyPacketData[12].ToString("X");
            return strResult;
        }
        public DateTime GetUTCDate()
        {
            int iTimePos = 13;
            String g = GetCommand();
            if (g == "9999")
                iTimePos = 14;

            String strTime = Msg.Substring(iTimePos, 10);
            String strDate = MsgArray[8];
            int yy = 2000 + Convert.ToInt16(strDate.Substring(4, 2));
            int mm = Convert.ToInt16(strDate.Substring(2, 2));
            int dd = Convert.ToInt16(strDate.Substring(0, 2));
            int HH = Convert.ToInt16(strTime.Substring(0, 2));
            int MM = Convert.ToInt16(strTime.Substring(2, 2));
            int SS = Convert.ToInt16(strTime.Substring(4, 2));
            DateTime dt = new DateTime(yy, mm, dd, HH, MM, SS);
            return dt;
        }
        public Boolean IsGPSValid()
        {
            Boolean bResult = false;
            String g = MsgArray[1];
            if (g == "A")
                bResult = true;
            return bResult;
        }
        public Double GetLatitude()
        {
            String g = MsgArray[2];
            Double LatX = Double.Parse(g.Substring(0, 2), nfi);
            Double dblTempDegrees = Double.Parse(g.Substring(2), nfi) / 60;
            LatX += dblTempDegrees;
            LatX *= -1;
            return LatX;
        }
        public Double GetLongitude()
        {
            String g = MsgArray[4].Trim();

            int Lat = 3;
            if (g.Substring(4, 1) == ".")
                Lat = 2;

            Double LatX = Double.Parse(g.Substring(0, Lat), nfi);
            Double dblTempDegrees = Double.Parse(g.Substring(Lat), nfi) / 60;
            LatX += dblTempDegrees;
            return LatX;
        }
        public Double GetSpeed()
        {
            //Km/h
            //Speed is in knot. 1 knot = 1.852km
            String str = MsgArray[6];
            return Convert.ToDouble(str, nfi) * 1.852;
        }
    }
}
