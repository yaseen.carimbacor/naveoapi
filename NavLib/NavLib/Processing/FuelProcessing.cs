using System;
using System.Collections.Generic;
using System.Text;
using NavLib.Data;
using System.Data;
using NavLib.Globals;

namespace NavLib.Processing
{
    public class FuelProcessing
    {
        Runme oRunme;
        public FuelProcessing()
        {
            oRunme = new Runme();
        }

        public void GetFuelADC(String unitID, int RawADC, out int oFuelID, out int oADC)
        {
            DataSet ds = oRunme.GetDataSet(SqlCmds.GetFuelID(unitID), "ConnStr");
            Boolean bReverse = false;
            if (ds != null)
                if ((int)ds.Tables[0].Rows[0]["Reverse"] == 1)
                    bReverse = true;

            oFuelID = (int)ds.Tables[0].Rows[0]["FuelID"];
            oADC = RawADC;
            if (bReverse)
                oADC = 255 - RawADC;
        }
        public String GetUnitID(int FuelID)
        {
            String bResult = String.Empty;
            try
            {
                if (FuelID != -1)
                {
                    DataSet ds = oRunme.GetDataSet(SqlCmds.GetUnitID(FuelID), "ConnStr");
                    bResult = (String)ds.Tables[0].Rows[0]["unitID"];
                }
            }
            finally
            {
            }
            return bResult;
        }
        public String GetAllUnitIDs()
        {
            DataSet dsUnitIDs = oRunme.GetDataSet(SqlCmds.GetUnitID(-1), "ConnStr");
            String strUnitIDs = String.Empty;
            foreach (DataRow dr in dsUnitIDs.Tables[0].Rows)
                strUnitIDs += "'" + dr["unitID"] + "',";
            if (strUnitIDs != String.Empty)
                strUnitIDs = strUnitIDs.Remove(strUnitIDs.Length - 1);

            return strUnitIDs;
        }
    }
}
