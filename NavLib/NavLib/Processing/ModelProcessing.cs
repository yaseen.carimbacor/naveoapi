using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.IO;
using NavLib.Data;
using NavLib.AT100;
using NavLib.STEPP;
using NavLib.Enfora;
using NavLib.Orion;
using System.Data;
using NavLib.Globals;
using NavLib.AT220;
using NavLib.Meitrack;
using NavLib.Atrack;
using NavLib.Squarell;
using NavLib.Xexun;
using NavLib.CoreData;
using NaveoOneLib.Models.GPS;
using NavLib.AstraTelematics.ProtokolKL;
using NavLib.Metron;
using NavLib.AstraTelematics.ProtokolMNV;
using NavLib.Inventia;
//using NavLib.TelTonika;
using NaveoOneLib.Models.Telemetry;

namespace NavLib.Processing
{
    public class ModelProcessing
    {
        public ModelProcessing()
        { }

        #region Enfora
        void WriteEnforaLogInAFile(Byte[] bytWorking, String Filename)
        {
            File.AppendAllText(GlobalVariables.logPath() + Filename + "_Raw.txt", Encoding.ASCII.GetString(Utils.CompressBytWorking(bytWorking)));
            File.AppendAllText(GlobalVariables.logPath() + Filename + ".txt", GetEnforaLog(bytWorking));
        }
        String GetEnforaLog(Byte[] bytWorking)
        {
            bytWorking = Utils.bytWorking1024(bytWorking);
            //Unit Name = Unit Sim Number
            clsGSM2218 gsm2218 = new clsGSM2218(bytWorking);
            Boolean bValid = gsm2218.IsGPSValid();
            String strUnitModel = "Enfora";
            String UnitName = gsm2218.GetUnitName();
            Double Lat = gsm2218.GetLatitude();
            Double Lon = gsm2218.GetLongitude();
            DateTime dt = gsm2218.GetDate();
            float speed = gsm2218.GetSpeed();
            String lr = gsm2218.GetLogReason();
            String striButton = String.Empty;

            String strResult = String.Empty;
            strResult += "strUnitModel : " + strUnitModel + "; ";
            strResult += "Date         : " + dt.ToString() + "; ";
            strResult += "UnitName     : " + UnitName + "; ";
            strResult += "Lat          : " + Lat.ToString() + "; ";
            strResult += "Lon          : " + Lon.ToString() + "; ";
            strResult += "Speed        : " + speed.ToString() + "; ";
            strResult += "LR           : " + lr + "\r\n";
            strResult += "IsValid      : " + bValid.ToString() + "\r\n";
            return strResult;
        }

        void CoreProcessingEnforaGSM2218(Byte[] bytWorking, NaveoCore naveo)
        {
            bytWorking = Utils.bytWorking1024(bytWorking);
            clsGSM2218 gsm2218 = new clsGSM2218(bytWorking);
            if (!gsm2218.IsGPSValid())
                return;
            String strUnitModel = "Enfora";
            String UnitName = gsm2218.GetUnitName();
            Double Lat = gsm2218.GetLatitude();
            Double Lon = gsm2218.GetLongitude();
            DateTime dt = gsm2218.GetDate();
            float speed = gsm2218.GetSpeed();
            float dist = 0;
            String lr = gsm2218.GetLogReason();
            String striButton = String.Empty;

            naveo.UpdateAddLastRecord(strUnitModel, striButton, UnitName, Lat, Lon, dt, speed, dist, false, lr, true);
        }
        GPSData GPSDataEnforaGSM2218(Byte[] bytWorking, NaveoCore naveo)
        {
            GPSData g = new GPSData();
            bytWorking = Utils.bytWorking1024(bytWorking);
            clsGSM2218 gsm2218 = new clsGSM2218(bytWorking);
            if (!gsm2218.IsGPSValid())
                return g;
            String strUnitModel = "Enfora";
            String UnitName = gsm2218.GetUnitName();
            Double Lat = gsm2218.GetLatitude();
            Double Lon = gsm2218.GetLongitude();
            DateTime dt = gsm2218.GetDate();
            float speed = gsm2218.GetSpeed();
            float dist = 0;
            String lr = gsm2218.GetLogReason();
            String striButton = String.Empty;

            g = naveo.initGPSData(strUnitModel, striButton, UnitName, Lat, Lon, dt, speed, dist, false, lr, true);
            return g;
        }

        //Updated Enfora > MT4000
        void WriteEnforaGSM2448LogInAFile(Byte[] bytWorking, String Filename)
        {
            File.AppendAllText(GlobalVariables.logPath() + Filename + "_Raw.txt", Encoding.ASCII.GetString(Utils.CompressBytWorking(bytWorking)) + "\r\n");
            File.AppendAllText(GlobalVariables.logPath() + Filename + "_Hex.txt", Utils.BytesToDoubleHex(Utils.CompressBytWorking(bytWorking)) + "\r\n");
            File.AppendAllText(GlobalVariables.logPath() + Filename + ".txt", GetEnforaGSM2448Log(bytWorking));
        }
        String GetEnforaGSM2448Log(Byte[] bytWorking)
        {
            String strToFile = String.Empty;
            try
            {
                DecodeEnfora de = new DecodeEnfora(bytWorking);

                String UnitName = de.GetMdMid();
                Double Lat = de.gsm2448Bitmask3244031.GetLatitude();
                Double Lon = de.gsm2448Bitmask3244031.GetLongitude();
                DateTime dt = de.gsm2448Bitmask3244031.GetRTC();
                float speed = (float)de.gsm2448Bitmask3244031.GetSpeed();
                String strLogReason = de.GetParam1();
                String striButton = string.Empty;

                strToFile = UnitName + " " + dt.AddHours(4).ToString();
                strToFile += "; Lon: " + Lon.ToString();
                strToFile += "; Lat: " + Lat.ToString();
                strToFile += "; Speed: " + speed.ToString();
                strToFile += "; LogReason: " + strLogReason;
                strToFile += "; GPSValid: " + de.gsm2448Bitmask3244031.isGPSOn().ToString();
                strToFile += "; NoOfSat: " + de.gsm2448Bitmask3244031.GetNoOfSat().ToString();
                strToFile += "; Ign+Aux123: " + de.gsm2448Bitmask3244031.isIgnitionOn().ToString()
                    + de.gsm2448Bitmask3244031.isGP1On().ToString()
                    + de.gsm2448Bitmask3244031.isGP9On().ToString()
                    + de.gsm2448Bitmask3244031.isGP10On().ToString()
                    + de.gsm2448Bitmask3244031.GPIO();
                strToFile += "AD1 + 2: " + de.gsm2448Bitmask3244031.GetAD1().ToString() + " & " + de.gsm2448Bitmask3244031.GetAD2().ToString();
                strToFile += "; iButton: " + de.OptHdr.GetDriverID();
                strToFile += "; OptHdrMdmId: " + de.OptHdr.GetModemID();
                strToFile += "; Bitmask: " + de.OptHdr.GetBitmask();
                strToFile += "; SeqNo: " + de.OptHdr.GetSeqNo();
                strToFile += "; OptHdrParam3: " + de.OptHdr.GetParam3();

                switch (de.OptHdr.GetParam3().Trim())
                {
                    case "4":
                        strToFile += "; Temp1: " + de.gsm2448Bitmask3244031.GetTemperature1();
                        break;
                    case "8":
                        strToFile += "; Temp2: " + de.gsm2448Bitmask3244031.GetTemperature2();
                        break;
                    case "12":
                        strToFile += "; Temp1: " + de.gsm2448Bitmask3244031.GetTemperature1();
                        strToFile += "; Temp2: " + de.gsm2448Bitmask3244031.GetTemperature2();
                        break;
                }
                strToFile += "\r\n";
            }
            catch (System.Exception ex)
            {
                Logger.WriteToErrorLog(ex);
            }

            return strToFile;
        }

        void CoreProcessingEnforaGSM2448(Byte[] bytWorking, NaveoCore naveo)
        {
            bytWorking = Utils.bytWorking1024(bytWorking);
            DecodeEnfora de = new DecodeEnfora(bytWorking);

            String strUnitModel = "EnforN";
            String UnitName = de.GetMdMid();
            Double Lat = de.gsm2448Bitmask3244031.GetLatitude();
            Double Lon = de.gsm2448Bitmask3244031.GetLongitude();
            DateTime dt = de.gsm2448Bitmask3244031.GetRTC();
            float speed = (float)de.gsm2448Bitmask3244031.GetSpeed();
            float dist = 0;
            String lr = de.GetParam1();
            switch (de.OptHdr.GetParam3().Trim())
            {
                case "4":
                    lr += "\n" + de.gsm2448Bitmask3244031.GetTemperature1().ToString();
                    break;
                case "8":
                    lr += "\n" + de.gsm2448Bitmask3244031.GetTemperature2().ToString();
                    break;
                case "12":
                    lr += "\n" + de.gsm2448Bitmask3244031.GetTemperature1().ToString();
                    lr += "\n" + de.gsm2448Bitmask3244031.GetTemperature2().ToString();
                    break;
            }

            String striButton = de.OptHdr.GetDriverID().Trim();
            Boolean bIgn = de.gsm2448Bitmask3244031.isIgnitionOn();
            Boolean bValid = de.gsm2448Bitmask3244031.isGPSOn();

            naveo.UpdateAddLastRecord(strUnitModel, striButton, UnitName, Lat, Lon, dt, speed, dist, bIgn, lr, bValid);
        }
        GPSData GPSDataGSM2448(Byte[] bytWorking, NaveoCore naveo)
        {
            GPSData g = new GPSData();
            bytWorking = Utils.bytWorking1024(bytWorking);
            DecodeEnfora de = new DecodeEnfora(bytWorking);

            String strUnitModel = "EnforN";
            String UnitName = de.GetMdMid();
            Double Lat = de.gsm2448Bitmask3244031.GetLatitude();
            Double Lon = de.gsm2448Bitmask3244031.GetLongitude();
            DateTime dt = de.gsm2448Bitmask3244031.GetRTC();
            float speed = (float)de.gsm2448Bitmask3244031.GetSpeed();
            float dist = 0;
            String lr = de.GetParam1();
            switch (de.OptHdr.GetParam3().Trim())
            {
                case "4":
                    lr += "\n" + de.gsm2448Bitmask3244031.GetTemperature1().ToString();
                    break;
                case "8":
                    lr += "\n" + de.gsm2448Bitmask3244031.GetTemperature2().ToString();
                    break;
                case "12":
                    lr += "\n" + de.gsm2448Bitmask3244031.GetTemperature1().ToString();
                    lr += "\n" + de.gsm2448Bitmask3244031.GetTemperature2().ToString();
                    break;
            }

            String striButton = de.OptHdr.GetDriverID().Trim();
            Boolean bIgn = de.gsm2448Bitmask3244031.isIgnitionOn();
            Boolean bValid = de.gsm2448Bitmask3244031.isGPSOn();

            g = naveo.initGPSData(strUnitModel, striButton, UnitName, Lat, Lon, dt, speed, dist, bIgn, lr, bValid);
            return g;
        }
        #endregion

        #region AT100
        void WriteAT100LogInAFile(Byte[] bytWorking, String Filename)
        {
            File.AppendAllText(GlobalVariables.logPath() + Filename + ".txt", GetAT100Log(bytWorking));
            //File.AppendAllText(GlobalVariables.logPath() + Filename + "_Raw.txt", Encoding.ASCII.GetString(Utils.CompressBytWorking(bytWorking)));
        }
        String GetAT100Log(Byte[] bytWorking)
        {
            bytWorking = Utils.bytWorking1024(bytWorking);

            // Initialise DecodeAT100ProtocolC with the array of bytes
            DecodeAT100ProtocolC dp = new DecodeAT100ProtocolC(bytWorking);

            //Testing Phil codes...
            clsAT100Parser objPacketParser = new clsAT100Parser(bytWorking);

            String strToFile = String.Empty;
            try
            {
                if (dp.IsPacketDataValid)
                {
                    Boolean bReportsToFollow = true;
                    int iReportsToFollow = 0;  //To ensure we do not enter an infinite loop

                    String UniqueID = dp.GetUnitID();
                    while (bReportsToFollow)
                    {
                        bReportsToFollow = dp.AT100Reports[iReportsToFollow].GetStatusReportsToFollow();
                        DateTime dt = dp.AT100Reports[iReportsToFollow].GetDate();
                        float Dist = dp.AT100Reports[iReportsToFollow].GetJourneyDistanceTravelled();// "km"
                        Double Lon = dp.AT100Reports[iReportsToFollow].GetLongitude();
                        Double Lat = dp.AT100Reports[iReportsToFollow].GetLatitude();
                        String strLogReason = dp.AT100Reports[iReportsToFollow].GetReasons();
                        Boolean IgnitionOn = dp.AT100Reports[iReportsToFollow].GetStatusIgnitionOn();
                        int Speed = dp.AT100Reports[iReportsToFollow].GetSpeedInKmh();
                        int JourneyIdleSeconds = dp.AT100Reports[iReportsToFollow].GetJourneyIdleSeconds();
                        Boolean Aux1 = dp.AT100Reports[iReportsToFollow].GetDigitalInput1();//Ignition
                        Boolean Aux2 = dp.AT100Reports[iReportsToFollow].GetDigitalInput2();//Panic
                        Boolean Aux3 = dp.AT100Reports[iReportsToFollow].GetDigitalInput3();
                        Boolean Aux4 = dp.AT100Reports[iReportsToFollow].GetDigitalInput4();
                        Boolean Aux5 = dp.AT100Reports[iReportsToFollow].GetDigitalInput5();//AUX1 in Checkmate
                        int Aux = dp.AT100Reports[iReportsToFollow].GetDigitals();
                        int y = dp.AT100Reports[iReportsToFollow].GetPacketSeqNo();

                        #region Testing Phil codes...
                        String strLogreasonPhil = string.Empty;
                        {
                            DateTime dtPhil = Convert.ToDateTime(objPacketParser.objReports[iReportsToFollow].GetTime().Replace("@", ""));
                            float DistPhil = objPacketParser.objReports[iReportsToFollow].GetJourneyDistanceTravelled();// "km"
                            Double LonPhil = objPacketParser.objReports[iReportsToFollow].GetLongitude();
                            Double LatPhil = objPacketParser.objReports[iReportsToFollow].GetLatitude();
                            Boolean HeadingChangePhil = objPacketParser.objReports[iReportsToFollow].Reason_HeadingChange();
                            Boolean IgnitionOnPhil = objPacketParser.objReports[iReportsToFollow].Status_IgnitionOn();
                            int SpeedPhil = objPacketParser.objReports[iReportsToFollow].GetSpeedInKmh();
                            int JourneyIdleSecondsPhil = objPacketParser.objReports[iReportsToFollow].GetJourneyIdleSeconds();
                            byte ADC1Phil = objPacketParser.objReports[iReportsToFollow].GetADC1();
                            byte ADC2Phil = objPacketParser.objReports[iReportsToFollow].GetADC2();
                            int seqnoPhil = (int)objPacketParser.objReports[iReportsToFollow].GetPacketSequenceNumber();
                            //Heading
                            Boolean Reason_TimedIntervalElapsed = objPacketParser.objReports[iReportsToFollow].Reason_TimedIntervalElapsed();
                            Boolean Reason_HeadingChange = objPacketParser.objReports[iReportsToFollow].Reason_HeadingChange();
                            //Igintion
                            Boolean Reason_JourneyStart = objPacketParser.objReports[iReportsToFollow].Reason_JourneyStart();
                            Boolean Reason_JourneyStop = objPacketParser.objReports[iReportsToFollow].Reason_JourneyStop();
                            //Idle
                            Boolean Reason_IdlingStarting = objPacketParser.objReports[iReportsToFollow].Reason_IdlingStarting();
                            Boolean Reason_IdlingEnd = objPacketParser.objReports[iReportsToFollow].Reason_IdlingEnd();
                            Boolean Reason_IdlingOnGoing = objPacketParser.objReports[iReportsToFollow].Reason_IdlingOnGoing();
                            //Other in Geotab
                            Boolean Reason_PanicSwitchActivated = objPacketParser.objReports[iReportsToFollow].Reason_PanicSwitchActivated();
                            Boolean Reason_LowBattery = objPacketParser.objReports[iReportsToFollow].Reason_LowBattery();
                            Boolean Reason_ExtPowerEvent = objPacketParser.objReports[iReportsToFollow].Reason_ExtPowerEvent();
                            //Other
                            Boolean Reason_DistanceTravelledExceeded = objPacketParser.objReports[iReportsToFollow].Reason_DistanceTravelledExceeded();
                            Boolean Reason_PosOnDemand = objPacketParser.objReports[iReportsToFollow].Reason_PosOnDemand();
                            Boolean Reason_GeoFence = objPacketParser.objReports[iReportsToFollow].Reason_GeoFence();
                            Boolean Reason_ExtInputEvent = objPacketParser.objReports[iReportsToFollow].Reason_ExtInputEvent();
                            Boolean Reason_Alarm = objPacketParser.objReports[iReportsToFollow].Reason_Alarm();
                            Boolean Reason_SpeedOverThreshold = objPacketParser.objReports[iReportsToFollow].Reason_SpeedOverThreshold();

                            //Status codes
                            Boolean Status_IgnitionOn = objPacketParser.objReports[iReportsToFollow].Status_IgnitionOn();
                            Boolean Status_PowerOnOrRestart = objPacketParser.objReports[iReportsToFollow].Status_PowerOnOrRestart();
                            Boolean Status_GPSTimeOut = objPacketParser.objReports[iReportsToFollow].Status_GPSTimeOut();
                            Boolean Status_ReportsToFollow = objPacketParser.objReports[iReportsToFollow].Status_ReportsToFollow();

                            strLogreasonPhil += LonPhil.ToString() + " " + LatPhil.ToString();
                            if (Reason_TimedIntervalElapsed || Reason_HeadingChange || Reason_DistanceTravelledExceeded)
                                strLogreasonPhil += "TimeOrHeadOrDist.";
                            if (Reason_JourneyStart || Reason_JourneyStop)
                                strLogreasonPhil += "IgnStartOrEnd.";
                            if (Reason_IdlingStarting || Reason_IdlingEnd || Reason_IdlingOnGoing)
                                strLogreasonPhil += "AnyIdle.";
                            if (Reason_LowBattery)
                                strLogreasonPhil += "LowBattery.";
                            if (Reason_ExtPowerEvent)
                                strLogreasonPhil += "ExpPower.";
                            if (Status_IgnitionOn)
                                strLogreasonPhil += "Status_IgnitionOn.";
                            if (Status_PowerOnOrRestart)
                                strLogreasonPhil += "Status_PowerOnOrRestart.";
                            if (Status_GPSTimeOut)
                                strLogreasonPhil += "Status_GPSTimeOut.";
                            if (Status_ReportsToFollow)
                                strLogreasonPhil += "Status_ReportsToFollow.";
                            //Digital Inputs > AUX
                            int AuxPhil = (int)objPacketParser.objReports[iReportsToFollow].DigitalStatus();
                        }
                        #endregion

                        strToFile += dt.AddHours(4).ToString();
                        strToFile += "; ID: " + UniqueID;
                        strToFile += "; SeqNo: " + dp.AT100Reports[iReportsToFollow].GetPacketSeqNo().ToString();
                        strToFile += "; ADC1: " + dp.AT100Reports[iReportsToFollow].GetADC1().ToString();
                        strToFile += "; ADC2: " + dp.AT100Reports[iReportsToFollow].GetADC2().ToString();
                        strToFile += "; ADC: " + dp.AT100Reports[iReportsToFollow].GetADC().ToString();
                        strToFile += "; BatteryLvl: " + dp.AT100Reports[iReportsToFollow].GetBatteryLevelPercentage().ToString();
                        strToFile += "; Lon: " + Lon.ToString();
                        strToFile += "; Lat: " + Lat.ToString();
                        strToFile += "; LogReason: " + strLogReason;
                        strToFile += "; IsValid: " + dp.AT100Reports[iReportsToFollow].IsReportValid().ToString();//strToFile += " PhilLogReason: " + strLogreasonPhil;
                        strToFile += "; Aux1: " + Aux1.ToString();
                        strToFile += "; Aux2: " + Aux2.ToString();
                        strToFile += "; Aux3: " + Aux3.ToString();
                        strToFile += "; Aux4: " + Aux4.ToString();
                        strToFile += "\r\n";

                        ++iReportsToFollow;
                        if (iReportsToFollow > 8)
                            bReportsToFollow = false;
                    }
                }
            }
            catch (System.Exception ex)
            {
                Logger.WriteToErrorLog(ex);
            }
            return strToFile;
        }

        void CoreProcessingAT100(Byte[] bytWorking, NaveoCore naveo)
        {
            bytWorking = Utils.bytWorking1024(bytWorking);
            // Initialise DecodeAT100ProtocolC with the array of bytes
            DecodeAT100ProtocolC dp = new DecodeAT100ProtocolC(bytWorking);
            try
            {
                if (dp.IsPacketDataValid)
                {
                    String strUnitModel = "AT100";
                    Boolean bReportsToFollow = true;
                    int iReportsToFollow = 0;  //To ensure we do not enter an infinite loop

                    String UniqueID = dp.GetUnitID();
                    while (bReportsToFollow)
                    {
                        bReportsToFollow = dp.AT100Reports[iReportsToFollow].GetStatusReportsToFollow();
                        DateTime dt = dp.AT100Reports[iReportsToFollow].GetDate();
                        float Dist = dp.AT100Reports[iReportsToFollow].GetJourneyDistanceTravelled();// "km"
                        Double Lon = dp.AT100Reports[iReportsToFollow].GetLongitude();
                        Double Lat = dp.AT100Reports[iReportsToFollow].GetLatitude();
                        String strLogReason = dp.AT100Reports[iReportsToFollow].GetReasons();
                        Boolean IgnitionOn = dp.AT100Reports[iReportsToFollow].GetStatusIgnitionOn();
                        int Speed = dp.AT100Reports[iReportsToFollow].GetSpeedInKmh();
                        int JourneyIdleSeconds = dp.AT100Reports[iReportsToFollow].GetJourneyIdleSeconds();
                        int PacketSeqNo = dp.AT100Reports[iReportsToFollow].GetPacketSeqNo();
                        int Aux = dp.AT100Reports[iReportsToFollow].GetDigitals();
                        String striButton = string.Empty;

                        if (dt.Year > 2014) //1980
                            if (dt <= DateTime.UtcNow.AddHours(1))
                                naveo.UpdateAddLastRecord(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, Dist, IgnitionOn, strLogReason, true);

                        iReportsToFollow += 1;
                        if (iReportsToFollow > 8)
                            bReportsToFollow = false;
                    }
                }
            }
            catch (System.Exception ex)
            {
                Logger.WriteToErrorLog(ex);
            }
        }
        List<GPSData> lGPSDataAT100(Byte[] bytWorking, NaveoCore naveo)
        {
            List<GPSData> l = new List<GPSData>();

            bytWorking = Utils.bytWorking1024(bytWorking);
            // Initialise DecodeAT100ProtocolC with the array of bytes
            DecodeAT100ProtocolC dp = new DecodeAT100ProtocolC(bytWorking);
            try
            {
                if (dp.IsPacketDataValid)
                {
                    String strUnitModel = "AT100";
                    Boolean bReportsToFollow = true;
                    int iReportsToFollow = 0;  //To ensure we do not enter an infinite loop

                    String UniqueID = dp.GetUnitID();
                    while (bReportsToFollow)
                    {
                        GPSData g = new GPSData();

                        bReportsToFollow = dp.AT100Reports[iReportsToFollow].GetStatusReportsToFollow();
                        DateTime dt = dp.AT100Reports[iReportsToFollow].GetDate();
                        float Dist = dp.AT100Reports[iReportsToFollow].GetJourneyDistanceTravelled();// "km"
                        Double Lon = dp.AT100Reports[iReportsToFollow].GetLongitude();
                        Double Lat = dp.AT100Reports[iReportsToFollow].GetLatitude();
                        String strLogReason = dp.AT100Reports[iReportsToFollow].GetReasons();
                        Boolean IgnitionOn = dp.AT100Reports[iReportsToFollow].GetStatusIgnitionOn();
                        int Speed = dp.AT100Reports[iReportsToFollow].GetSpeedInKmh();
                        int JourneyIdleSeconds = dp.AT100Reports[iReportsToFollow].GetJourneyIdleSeconds();
                        int PacketSeqNo = dp.AT100Reports[iReportsToFollow].GetPacketSeqNo();
                        int Aux = dp.AT100Reports[iReportsToFollow].GetDigitals();
                        String striButton = string.Empty;

                        if (dt.Year > 2014) //1980
                            if (dt <= DateTime.UtcNow.AddHours(1))
                                g = naveo.initGPSData(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, Dist, IgnitionOn, strLogReason, true);

                        l.Add(g);

                        iReportsToFollow += 1;
                        if (iReportsToFollow > 8)
                            bReportsToFollow = false;
                    }
                }
            }
            catch (System.Exception ex)
            {
                Logger.WriteToErrorLog(ex);
            }
            return l;
        }
        #endregion

        #region Astra ProtocolKL 
        void WriteAstraProtocolKLLogInAFile(Byte[] bytWorking, String Filename)
        {
            File.AppendAllText(GlobalVariables.logPath() + Filename + "_Raw.txt", Encoding.ASCII.GetString(Utils.CompressBytWorking(bytWorking)));
            File.AppendAllText(GlobalVariables.logPath() + Filename + "_Hex.txt", Utils.BytesToDoubleHex(Utils.CompressBytWorking(bytWorking)) + "\r\n");
            File.AppendAllText(GlobalVariables.logPath() + Filename + ".txt", GetAstraProtocolKLLog(bytWorking));
        }
        String GetAstraProtocolKLLog(Byte[] bytWorking)
        {
            bytWorking = Utils.bytWorking1024(bytWorking);

            String ProtocolID = Encoding.ASCII.GetString(bytWorking, 0, 1);
            //AT220Parser dp = new AT220Parser(bytWorking, (byte)'K');
            AT200Parser dp = new AT200Parser(bytWorking, bytWorking[0]);

            String strToFile = String.Empty;
            try
            {
                if (dp.objHeader.Valid)
                {
                    Boolean bReportsToFollow = true;
                    int iReportsToFollow = 0;  //To ensure we do not enter an infinite loop

                    String UniqueID = dp.objHeader.GetUnitID();
                    while (bReportsToFollow)
                    {
                        bReportsToFollow = dp.objReports[iReportsToFollow].Status_ReportsToFollow();

                        #region Testing Phil codes...
                        String strLogreasonPhil = string.Empty;
                        {
                            DateTime dtPhil = Convert.ToDateTime(dp.objReports[iReportsToFollow].GetTime().Replace("@", ""));
                            float DistPhil = dp.objReports[iReportsToFollow].GetJourneyDistanceTravelled();// "km"
                            Double LonPhil = dp.objReports[iReportsToFollow].GetLongitude();
                            Double LatPhil = dp.objReports[iReportsToFollow].GetLatitude();
                            Boolean HeadingChangePhil = dp.objReports[iReportsToFollow].Reason_HeadingChange();
                            Boolean IgnitionOnPhil = dp.objReports[iReportsToFollow].Status_IgnitionOn();
                            int SpeedPhil = dp.objReports[iReportsToFollow].GetSpeedInKmh();
                            int JourneyIdleSecondsPhil = dp.objReports[iReportsToFollow].GetJourneyIdleSeconds();
                            int seqnoPhil = (int)dp.objReports[iReportsToFollow].GetPacketSequenceNumber();
                            //Heading
                            Boolean Reason_TimedIntervalElapsed = dp.objReports[iReportsToFollow].Reason_TimedIntervalElapsed();
                            Boolean Reason_HeadingChange = dp.objReports[iReportsToFollow].Reason_HeadingChange();
                            //Igintion
                            Boolean Reason_JourneyStart = dp.objReports[iReportsToFollow].Reason_JourneyStart();
                            Boolean Reason_JourneyStop = dp.objReports[iReportsToFollow].Reason_JourneyStop();
                            //Idle
                            Boolean Reason_IdlingStarting = dp.objReports[iReportsToFollow].Reason_IdlingStarting();
                            Boolean Reason_IdlingEnd = dp.objReports[iReportsToFollow].Reason_IdlingEnd();
                            Boolean Reason_IdlingOnGoing = dp.objReports[iReportsToFollow].Reason_IdlingOnGoing();
                            //Other in Geotab
                            Boolean Reason_PanicSwitchActivated = dp.objReports[iReportsToFollow].Reason_PanicSwitchActivated();
                            Boolean Reason_LowBattery = dp.objReports[iReportsToFollow].Reason_LowBattery();
                            Boolean Reason_ExtPowerEvent = dp.objReports[iReportsToFollow].Reason_ExtPowerEvent();
                            //Other
                            Boolean Reason_DistanceTravelledExceeded = dp.objReports[iReportsToFollow].Reason_DistanceTravelledExceeded();
                            Boolean Reason_PosOnDemand = dp.objReports[iReportsToFollow].Reason_PosOnDemand();
                            Boolean Reason_GeoFence = dp.objReports[iReportsToFollow].Reason_GeoFence();
                            Boolean Reason_Alarm = dp.objReports[iReportsToFollow].Reason_Alarm();
                            Boolean Reason_SpeedOverThreshold = dp.objReports[iReportsToFollow].Reason_SpeedOverThreshold();

                            //Status codes
                            Boolean Status_IgnitionOn = dp.objReports[iReportsToFollow].Status_IgnitionOn();
                            Boolean Status_PowerOnOrRestart = dp.objReports[iReportsToFollow].Status_PowerOnOrRestart();
                            Boolean Status_GPSTimeOut = dp.objReports[iReportsToFollow].Status_GPSTimeOut();
                            Boolean Status_ReportsToFollow = dp.objReports[iReportsToFollow].Status_ReportsToFollow();

                            strLogreasonPhil += LonPhil.ToString() + " " + LatPhil.ToString();
                            if (Reason_TimedIntervalElapsed || Reason_HeadingChange || Reason_DistanceTravelledExceeded)
                                strLogreasonPhil += "TimeOrHeadOrDist.";
                            if (Reason_JourneyStart || Reason_JourneyStop)
                                strLogreasonPhil += "IgnStartOrEnd.";
                            if (Reason_IdlingStarting || Reason_IdlingEnd || Reason_IdlingOnGoing)
                                strLogreasonPhil += "AnyIdle.";
                            if (Reason_LowBattery)
                                strLogreasonPhil += "LowBattery.";
                            if (Reason_ExtPowerEvent)
                                strLogreasonPhil += "ExpPower.";
                            if (Status_IgnitionOn)
                                strLogreasonPhil += "Status_IgnitionOn.";
                            if (Status_PowerOnOrRestart)
                                strLogreasonPhil += "Status_PowerOnOrRestart.";
                            if (Status_GPSTimeOut)
                                strLogreasonPhil += "Status_GPSTimeOut.";
                            if (Status_ReportsToFollow)
                                strLogreasonPhil += "Status_ReportsToFollow.";
                            //Digital Inputs > AUX
                            int AuxPhil = (int)dp.objReports[iReportsToFollow].DigitalStatus();
                        }
                        #endregion

                        strToFile += strLogreasonPhil;
                        strToFile += "*** Naveo LR *** ";
                        strToFile += dp.objReports[iReportsToFollow].GetDate().AddHours(4).ToString();
                        strToFile += "\tID: " + UniqueID + " " + ProtocolID;
                        strToFile += "\tSeqNo: " + dp.objReports[iReportsToFollow].GetPacketSequenceNumber().ToString();
                        strToFile += "\tADC1: " + dp.objReports[iReportsToFollow].GetADC1().ToString();
                        strToFile += "\tBatteryLvl: " + dp.objReports[iReportsToFollow].GetBatteryPercentage().ToString();
                        strToFile += "\tLon: " + dp.objReports[iReportsToFollow].GetLongitude().ToString();
                        strToFile += "\tLat: " + dp.objReports[iReportsToFollow].GetLatitude().ToString();
                        strToFile += "\tIsValid: " + dp.objHeader.Valid.ToString();
                        strToFile += "\tAux1: " + dp.objReports[iReportsToFollow].GetDigitalInput1().ToString();
                        strToFile += "\tAux2: " + dp.objReports[iReportsToFollow].GetDigitalInput2().ToString();
                        strToFile += "\tAux3: " + dp.objReports[iReportsToFollow].GetDigitalInput3().ToString();
                        strToFile += "\tAux4: " + dp.objReports[iReportsToFollow].GetDigitalInput4().ToString();
                        strToFile += "\tTot Odo: " + dp.objReports[iReportsToFollow].GetExtraOdom().ToString();
                        strToFile += "\tTot Hrs: " + dp.objReports[iReportsToFollow].GetExtraHrs().ToString();
                        strToFile += "\tDriverID: " + dp.objReports[iReportsToFollow].GetDriverID();
                        if (dp.objReports[iReportsToFollow].GetDriverID() == "None")
                            strToFile += "\t";

                        strToFile += "\tGetXAxisMaxDeceleration: " + dp.objReports[iReportsToFollow].GetXAxisMaxDeceleration().ToString();
                        strToFile += "\tGetXAxisMaxAcceleration: " + dp.objReports[iReportsToFollow].GetXAxisMaxAcceleration().ToString();
                        strToFile += "\tGetYAxisMaxDeceleration: " + dp.objReports[iReportsToFollow].GetYAxisMaxDeceleration().ToString();
                        strToFile += "\tGetYAxisMaxAcceleration: " + dp.objReports[iReportsToFollow].GetYAxisMaxAcceleration().ToString();

                        strToFile += "\tGetSatellitesInUse: " + dp.objReports[iReportsToFollow].GetSatellitesInUse().ToString();
                        strToFile += "\tSpeed: " + dp.objReports[iReportsToFollow].GetSpeedInKmh();
                        strToFile += "\tMaxSpeed: " + dp.objReports[iReportsToFollow].GetMaxJourneySpeed();

                        strToFile += "\tLogReason: " + dp.objReports[iReportsToFollow].GetLogReasons();
                        strToFile += "\r\n";

                        ++iReportsToFollow;
                        if (iReportsToFollow >= 8)
                            bReportsToFollow = false;
                    }
                }

                //strToFile += "\r\nAstraTest\r\n";
                //strToFile += new NavLib.AstraTelematics.ProtokolKL.Decoder(bytWorking[0]).ReportToStrings(bytWorking);
            }
            catch (System.Exception ex)
            {
                Logger.WriteToErrorLog(ex);
            }
            return strToFile;
        }

        void CoreProcessingAstraProtocolKL(Byte[] bytWorking, NaveoCore naveo)
        {
            bytWorking = Utils.bytWorking1024(bytWorking);
            String ProtocolID = Encoding.ASCII.GetString(bytWorking, 0, 1);
            //AT220Parser dp = new AT220Parser(bytWorking, (byte)'K');
            AT200Parser dp = new AT200Parser(bytWorking, bytWorking[0]);
            try
            {
                if (dp.objHeader.Valid)
                {
                    String strUnitModel = "AT200";
                    switch (ProtocolID)
                    {
                        case "C":
                            strUnitModel = "AT100";
                            break;

                        case "G":
                            break;

                        case "I":
                            strUnitModel = "ASTRAIMSI";
                            break;

                        case "K":
                            strUnitModel = "AT200";
                            break;
                    }

                    Boolean bReportsToFollow = true;
                    int iReportsToFollow = 0;  //To ensure we do not enter an infinite loop

                    String UniqueID = dp.objHeader.GetUnitID();
                    while (bReportsToFollow)
                    {
                        bReportsToFollow = dp.objReports[iReportsToFollow].Status_ReportsToFollow();
                        DateTime dt = dp.objReports[iReportsToFollow].GetDate();
                        float Dist = dp.objReports[iReportsToFollow].GetJourneyDistanceTravelled();// "km"
                        Double Lon = dp.objReports[iReportsToFollow].GetLongitude();
                        Double Lat = dp.objReports[iReportsToFollow].GetLatitude();
                        String strLogReason = dp.objReports[iReportsToFollow].GetLogReasons();
                        Boolean IgnitionOn = dp.objReports[iReportsToFollow].Status_IgnitionOn();
                        int Speed = dp.objReports[iReportsToFollow].GetSpeedInKmh();
                        int JourneyIdleSeconds = dp.objReports[iReportsToFollow].GetJourneyIdleSeconds();
                        String striButton = string.Empty;

                        if (dt.Year > 1980)
                            if (dt <= DateTime.UtcNow.AddHours(1))
                                naveo.UpdateAddLastRecord(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, Dist, IgnitionOn, strLogReason, true);

                        iReportsToFollow += 1;
                        if (iReportsToFollow >= 8)
                            bReportsToFollow = false;
                    }
                }
            }
            catch (System.Exception ex)
            {
                Logger.WriteToErrorLog(ex);
            }
        }
        List<GPSData> lGPSDataAstraProtocolKL(Byte[] bytWorking, NaveoCore naveo)
        {
            List<GPSData> l = new List<GPSData>();

            bytWorking = Utils.bytWorking1024(bytWorking);
            String ProtocolID = Encoding.ASCII.GetString(bytWorking, 0, 1);
            AT200Parser dp = new AT200Parser(bytWorking, bytWorking[0]);
            try
            {
                if (dp.objHeader.Valid)
                {
                    String strUnitModel = "ASTRA";
                    switch (ProtocolID)
                    {
                        case "C":
                            strUnitModel = "AT100";
                            break;

                        case "G":
                            break;

                        case "I":
                            strUnitModel = "ASTRAIMSI";
                            break;

                        case "K":
                            strUnitModel = "AT200";
                            break;
                    }

                    Boolean bReportsToFollow = true;
                    int iReportsToFollow = 0;  //To ensure we do not enter an infinite loop

                    String UniqueID = dp.objHeader.GetUnitID();
                    while (bReportsToFollow)
                    {
                        GPSData g = new GPSData();

                        bReportsToFollow = dp.objReports[iReportsToFollow].Status_ReportsToFollow();
                        DateTime dt = dp.objReports[iReportsToFollow].GetDate();
                        float Dist = dp.objReports[iReportsToFollow].GetJourneyDistanceTravelled();// "km"
                        Double Lon = dp.objReports[iReportsToFollow].GetLongitude();
                        Double Lat = dp.objReports[iReportsToFollow].GetLatitude();
                        String strLogReason = dp.objReports[iReportsToFollow].GetLogReasons() + "\n" + dp.objReports[iReportsToFollow].GetADC1().ToString();
                        Boolean IgnitionOn = dp.objReports[iReportsToFollow].Status_IgnitionOn();
                        int Speed = dp.objReports[iReportsToFollow].GetSpeedInKmh();
                        int JourneyIdleSeconds = dp.objReports[iReportsToFollow].GetJourneyIdleSeconds();
                        String striButton = dp.objReports[iReportsToFollow].GetDriverID();

                        if (dt.Year > 1980)
                            if (dt <= DateTime.UtcNow.AddHours(1))
                                g = naveo.initGPSData(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, Dist, IgnitionOn, strLogReason, true);

                        l.Add(g);

                        iReportsToFollow += 1;
                        if (iReportsToFollow >= 8)
                            bReportsToFollow = false;
                    }
                }
            }
            catch (System.Exception ex)
            {
                Logger.WriteToErrorLog(ex);
            }
            return l;
        }
        #endregion

        #region Astra ProtocolMNV
        void WriteAstraProtocolMNVLogInAFile(Byte[] bytWorking, String Filename)
        {
            File.AppendAllText(GlobalVariables.logPath() + Filename + "_Raw.txt", Encoding.ASCII.GetString(Utils.CompressBytWorking(bytWorking)));
            File.AppendAllText(GlobalVariables.logPath() + Filename + "_Hex.txt", Utils.BytesToDoubleHex(Utils.CompressBytWorking(bytWorking)) + "\r\n");
            File.AppendAllText(GlobalVariables.logPath() + Filename + ".txt", GetAstraProtocolMNVLog(bytWorking));
            //File.AppendAllText(GlobalVariables.logPath() + Filename + "_Str.txt", Encoding.ASCII.GetString(bytWorking, 0, 50));
            File.AppendAllText(GlobalVariables.logPath() + Filename + "_1.txt", new NavLib.AstraTelematics.ProtokolMNV.Decoder(bytWorking[0]).ReportToStrings(bytWorking));
        }
        String GetAstraProtocolMNVLog(Byte[] bytWorking)
        {
            bytWorking = Utils.bytWorking1024(bytWorking);

            String ProtocolID = Encoding.ASCII.GetString(bytWorking, 0, 1);
            AstraParser dp = new AstraParser(bytWorking, bytWorking[0]);

            String strToFile = String.Empty;
            try
            {
                if (dp.objHeader.Valid)
                {
                    Boolean bReportsToFollow = true;
                    int iReportsToFollow = 0;  //To ensure we do not enter an infinite loop

                    String UniqueID = dp.objHeader.GetUnitID();
                    while (bReportsToFollow)
                    {
                        bReportsToFollow = dp.objReports[iReportsToFollow].Status_ReportsToFollow();

                        strToFile += dp.objReports[iReportsToFollow].GetDate().AddHours(4).ToString();
                        strToFile += "\tID: " + UniqueID + " " + ProtocolID;
                        strToFile += "\tSeqNo: " + dp.objReports[iReportsToFollow].GetPacketSequenceNumber().ToString();
                        strToFile += "\tADC1: " + dp.objReports[iReportsToFollow].Get12BitADC1().ToString();
                        strToFile += "\tADC2: " + dp.objReports[iReportsToFollow].Get12BitADC2().ToString();
                        strToFile += "\tBatteryLvl: " + dp.objReports[iReportsToFollow].GetBatteryPercentage().ToString();
                        strToFile += "\tLon: " + dp.objReports[iReportsToFollow].GetLongitude().ToString();
                        strToFile += "\tLat: " + dp.objReports[iReportsToFollow].GetLatitude().ToString();
                        strToFile += "\tIsValid: " + dp.objHeader.Valid.ToString();
                        strToFile += "\tAux1: " + dp.objReports[iReportsToFollow].GetDigitalInput1().ToString();
                        strToFile += "\tAux2: " + dp.objReports[iReportsToFollow].GetDigitalInput2().ToString();
                        strToFile += "\tAux3: " + dp.objReports[iReportsToFollow].GetDigitalInput3().ToString();
                        strToFile += "\tAux4: " + dp.objReports[iReportsToFollow].GetDigitalInput4().ToString();
                        strToFile += "\tTot Odo: " + dp.objReports[iReportsToFollow].GetExtraOdom().ToString();
                        strToFile += "\tTot Hrs: " + dp.objReports[iReportsToFollow].GetExtraHrs().ToString();
                        strToFile += "\tDriverID: " + dp.objReports[iReportsToFollow].GetDriverID();
                        if (dp.objReports[iReportsToFollow].GetDriverID() == "None")
                            strToFile += "\t";

                        strToFile += "\tGetXAxisMaxDeceleration: " + dp.objReports[iReportsToFollow].GetXAxisMaxDeceleration().ToString();
                        strToFile += "\tGetXAxisMaxAcceleration: " + dp.objReports[iReportsToFollow].GetXAxisMaxAcceleration().ToString();
                        strToFile += "\tGetYAxisMaxDeceleration: " + dp.objReports[iReportsToFollow].GetYAxisMaxDeceleration().ToString();
                        strToFile += "\tGetYAxisMaxAcceleration: " + dp.objReports[iReportsToFollow].GetYAxisMaxAcceleration().ToString();

                        strToFile += "\tGetSatellitesInUse: " + dp.objReports[iReportsToFollow].GetSatellitesInUse().ToString();
                        strToFile += "\tSpeed: " + dp.objReports[iReportsToFollow].GetSpeedInKmh();
                        strToFile += "\tMaxSpeed: " + dp.objReports[iReportsToFollow].GetMaxJourneySpeed();

                        strToFile += "\tLogReason: " + dp.objReports[iReportsToFollow].GetLogReasons();
                        strToFile += "\r\n";

                        ++iReportsToFollow;
                        if (iReportsToFollow >= 8)
                            bReportsToFollow = false;
                    }
                }
            }
            catch (System.Exception ex)
            {
                Logger.WriteToErrorLog(ex);
            }
            return strToFile;
        }

        void CoreProcessingAstraProtocolMNV(Byte[] bytWorking, NaveoCore naveo)
        {
            bytWorking = Utils.bytWorking1024(bytWorking);
            String ProtocolID = Encoding.ASCII.GetString(bytWorking, 0, 1);
            AstraParser dp = new AstraParser(bytWorking, bytWorking[0]);
            try
            {
                if (dp.objHeader.Valid)
                {
                    String strUnitModel = "AT200";
                    switch (ProtocolID)
                    {
                        case "C":
                            strUnitModel = "AT100";
                            break;

                        case "G":
                            break;

                        case "I":
                            strUnitModel = "ASTRAIMSI";
                            break;

                        case "K":
                            strUnitModel = "AT200";
                            break;

                        case "V":
                            strUnitModel = "AT240";
                            break;
                    }

                    Boolean bReportsToFollow = true;
                    int iReportsToFollow = 0;  //To ensure we do not enter an infinite loop

                    String UniqueID = dp.objHeader.GetUnitID();
                    while (bReportsToFollow)
                    {
                        bReportsToFollow = dp.objReports[iReportsToFollow].Status_ReportsToFollow();
                        DateTime dt = dp.objReports[iReportsToFollow].GetDate();
                        float Dist = dp.objReports[iReportsToFollow].GetJourneyDistanceTravelled();// "km"
                        Double Lon = dp.objReports[iReportsToFollow].GetLongitude();
                        Double Lat = dp.objReports[iReportsToFollow].GetLatitude();
                        String strLogReason = dp.objReports[iReportsToFollow].GetLogReasons();
                        Boolean IgnitionOn = dp.objReports[iReportsToFollow].Status_IgnitionOn();
                        int Speed = dp.objReports[iReportsToFollow].GetSpeedInKmh();
                        int JourneyIdleSeconds = dp.objReports[iReportsToFollow].GetJourneyIdleSeconds();
                        String striButton = string.Empty;

                        if (dt.Year > 1980)
                            if (dt <= DateTime.UtcNow.AddHours(1))
                                naveo.UpdateAddLastRecord(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, Dist, IgnitionOn, strLogReason, true);

                        iReportsToFollow += 1;
                        if (iReportsToFollow >= 8)
                            bReportsToFollow = false;
                    }
                }
            }
            catch (System.Exception ex)
            {
                Logger.WriteToErrorLog(ex);
            }
        }
        List<GPSData> lGPSDataAstraProtocolMNV(Byte[] bytWorking, NaveoCore naveo)
        {
            List<GPSData> l = new List<GPSData>();

            bytWorking = Utils.bytWorking1024(bytWorking);
            String ProtocolID = Encoding.ASCII.GetString(bytWorking, 0, 1);
            AstraParser dp = new AstraParser(bytWorking, bytWorking[0]);
            try
            {
                if (dp.objHeader.Valid)
                {
                    String strUnitModel = "ASTRA";
                    switch (ProtocolID)
                    {
                        case "C":
                            strUnitModel = "AT100";
                            break;

                        case "G":
                            break;

                        case "I":
                            strUnitModel = "ASTRAIMSI";
                            break;

                        case "K":
                            strUnitModel = "AT200";
                            break;

                        case "V":
                            strUnitModel = "AT240";
                            break;
                    }

                    Boolean bReportsToFollow = true;
                    int iReportsToFollow = 0;  //To ensure we do not enter an infinite loop

                    String UniqueID = dp.objHeader.GetUnitID();
                    while (bReportsToFollow)
                    {
                        GPSData g = new GPSData();

                        bReportsToFollow = dp.objReports[iReportsToFollow].Status_ReportsToFollow();
                        DateTime dt = dp.objReports[iReportsToFollow].GetDate();
                        float Dist = dp.objReports[iReportsToFollow].GetJourneyDistanceTravelled();// "km"
                        Double Lon = dp.objReports[iReportsToFollow].GetLongitude();
                        Double Lat = dp.objReports[iReportsToFollow].GetLatitude();
                        Boolean IgnitionOn = dp.objReports[iReportsToFollow].Status_IgnitionOn();
                        int Speed = dp.objReports[iReportsToFollow].GetSpeedInKmh();
                        int JourneyIdleSeconds = dp.objReports[iReportsToFollow].GetJourneyIdleSeconds();
                        String striButton = dp.objReports[iReportsToFollow].GetDriverID();

                        String sADC1 = dp.objReports[iReportsToFollow].Get12BitADC1().ToString();
                        String sADC2 = dp.objReports[iReportsToFollow].Get12BitADC2().ToString();
                        String strLogReason = dp.objReports[iReportsToFollow].GetLogReasons() + "\n" + sADC1 + "\n" + sADC2;

                        if (dt.Year > 1980)
                            if (dt <= DateTime.UtcNow.AddHours(1))
                                g = naveo.initGPSData(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, Dist, IgnitionOn, strLogReason, true);

                        l.Add(g);

                        iReportsToFollow += 1;
                        if (iReportsToFollow >= 8)
                            bReportsToFollow = false;
                    }
                }
            }
            catch (System.Exception ex)
            {
                Logger.WriteToErrorLog(ex);
            }
            return l;
        }
        #endregion

        #region Stepp
        void WriteSteppLogInAFile(Byte[] bytWorking, String Filename)
        {
            File.AppendAllText(GlobalVariables.logPath() + Filename + "_Raw.txt", Encoding.ASCII.GetString(Utils.CompressBytWorking(bytWorking)));
            File.AppendAllText(GlobalVariables.logPath() + Filename + ".txt", GetSteppLog(bytWorking));
        }
        String GetSteppLog(Byte[] bytWorking)
        {
            String strToFile = String.Empty;
            bytWorking = Utils.bytWorking1024(bytWorking);

            // Initialise DecodeStepp with the array of bytes
            DecodeStepp ds = new DecodeStepp(bytWorking);
            int intNumReports = ds.SteppReports.Count;
            for (int i = 0; i < intNumReports; i++)
            {
                try
                {
                    String UniqueID = ds.SteppReports[i].GetUnitIMEI();
                    DateTime dt = ds.SteppReports[i].GetDateTime();
                    Double Lat = ds.SteppReports[i].GetLat();
                    Double Lon = ds.SteppReports[i].GetLon();
                    float Speed = (float)ds.SteppReports[i].GetSpeed() * 2;
                    Boolean bIgnitionOn = ds.SteppReports[i].IgnOn();
                    String heading = ds.SteppReports[i].GetHeading();
                    String strLogReason = ds.SteppReports[i].GetReason();
                    String striButton = string.Empty;

                    strToFile += "STEPP " + UniqueID + " " + dt.AddHours(4).ToString();
                    strToFile += "; Lon: " + Lon.ToString();
                    strToFile += "; Lat: " + Lat.ToString();
                    strToFile += "; Speed: " + Speed.ToString();
                    strToFile += "; LogReason: " + strLogReason;
                    strToFile += "; Date: " + dt.ToString() + ",";
                    strToFile += "; Speed: " + Speed.ToString() + ",";
                    strToFile += "; Ign: " + bIgnitionOn.ToString() + ",";
                    strToFile += "\r\n";
                }
                catch (System.Exception ex)
                {
                    Logger.WriteToErrorLog(ex);
                }
            }
            return strToFile;
        }

        void CoreProcessingStepp(Byte[] bytWorking, NaveoCore naveo)
        {
            String strUnitModel = "STEPP";
            bytWorking = Utils.bytWorking1024(bytWorking);

            // Initialise DecodeStepp with the array of bytes
            DecodeStepp ds = new DecodeStepp(bytWorking);
            int intNumReports = ds.SteppReports.Count;
            for (int i = 0; i < intNumReports; i++)
            {
                try
                {
                    String UniqueID = ds.SteppReports[i].GetUnitIMEI();
                    DateTime dt = ds.SteppReports[i].GetDateTime();
                    Double Lat = ds.SteppReports[i].GetLat();
                    Double Lon = ds.SteppReports[i].GetLon();
                    float Speed = (float)ds.SteppReports[i].GetSpeed() * 2;
                    Boolean bIgnitionOn = ds.SteppReports[i].IgnOn();
                    String heading = ds.SteppReports[i].GetHeading();
                    String strLogReason = ds.SteppReports[i].GetReason();
                    String striButton = string.Empty;

                    naveo.UpdateAddLastRecord(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, 0, bIgnitionOn, strLogReason, ds.SteppReports[i].IsValid());
                }
                catch (System.Exception ex)
                {
                    Logger.WriteToErrorLog(ex);
                }
            }
        }
        List<GPSData> lGPSDataStepp(Byte[] bytWorking, NaveoCore naveo)
        {
            List<GPSData> l = new List<GPSData>();
            String strUnitModel = "STEPP";
            bytWorking = Utils.bytWorking1024(bytWorking);

            // Initialise DecodeStepp with the array of bytes
            DecodeStepp ds = new DecodeStepp(bytWorking);
            int intNumReports = ds.SteppReports.Count;
            for (int i = 0; i < intNumReports; i++)
            {
                try
                {
                    GPSData g = new GPSData();

                    String UniqueID = ds.SteppReports[i].GetUnitIMEI();
                    DateTime dt = ds.SteppReports[i].GetDateTime();
                    Double Lat = ds.SteppReports[i].GetLat();
                    Double Lon = ds.SteppReports[i].GetLon();
                    float Speed = (float)ds.SteppReports[i].GetSpeed() * 2;
                    Boolean bIgnitionOn = ds.SteppReports[i].IgnOn();
                    String heading = ds.SteppReports[i].GetHeading();
                    String strLogReason = ds.SteppReports[i].GetReason();
                    String striButton = string.Empty;

                    g = naveo.initGPSData(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, 0, bIgnitionOn, strLogReason, ds.SteppReports[i].IsValid());
                    l.Add(g);
                }
                catch (System.Exception ex)
                {
                    Logger.WriteToErrorLog(ex);
                }
            }
            return l;
        }
        #endregion

        #region Orion
        void WriteOrionLogInAFile(Byte[] bytWorking, String Filename)
        {
            File.AppendAllText(GlobalVariables.logPath() + Filename + "_Raw.txt", Encoding.ASCII.GetString(Utils.CompressBytWorking(bytWorking)));
            File.AppendAllText(GlobalVariables.logPath() + Filename + ".txt", GetOrionLog(bytWorking));
        }
        String GetOrionLog(Byte[] bytWorking)
        {
            String strToFile = String.Empty;
            DecodeOrion dOrion = new DecodeOrion(bytWorking);

            try
            {
                foreach (OrionReports oReport in dOrion.oReports)
                {
                    String UniqueID = oReport.GetSerialNo();
                    DateTime dt = oReport.GetUTCDateTime();
                    Double Lat = oReport.GetLatitude();
                    Double Lon = oReport.GetLongitude();
                    float Speed = (float)oReport.GetSpeed();
                    String heading = oReport.GetAngle();
                    String strLogReason = oReport.GetLogReason();
                    String striButton = string.Empty;
                    int iFuel = oReport.GetVepamonLvl(1);

                    strToFile += dt.AddHours(4).ToString();
                    strToFile += "; Lon: " + Lon.ToString();
                    strToFile += "; Lat: " + Lat.ToString();
                    strToFile += "; Speed: " + Speed.ToString();
                    strToFile += "; Angle: " + heading;
                    strToFile += "; Mileage: " + oReport.GetMileage().ToString();
                    strToFile += "; AI1: " + oReport.GetAI1().ToString();
                    strToFile += "; AI2: " + oReport.GetAI2().ToString();
                    strToFile += "; DI : " + oReport.GetDI().ToString();
                    strToFile += "; DO : " + oReport.GetDO().ToString();
                    strToFile += "; Signel : " + oReport.GetSignel().ToString();
                    strToFile += "; Sat Nos : " + oReport.GetSatelliteNumbers().ToString();
                    strToFile += "; LogReason: " + strLogReason;
                    strToFile += "; Fuel: " + iFuel.ToString();
                    strToFile += "; Aux: " + oReport.GetAux2().ToString();
                    //strToFile += "; Panic II: " + oReport.GetAux2().ToString();
                    strToFile += "\r\n";
                }
            }
            catch (System.Exception ex)
            {
                Logger.WriteToErrorLog(ex);
            }
            return strToFile;
        }

        void CoreProcessingOrion(Byte[] bytWorking, NaveoCore naveo)
        {
            String strUnitModel = "ORION";
            DecodeOrion dOrion = new DecodeOrion(bytWorking);
            try
            {
                foreach (OrionReports oReport in dOrion.oReports)
                {
                    String UniqueID = oReport.GetSerialNo();
                    DateTime dt = oReport.GetUTCDateTime();
                    Double Lat = oReport.GetLatitude();
                    Double Lon = oReport.GetLongitude();
                    float Speed = (float)oReport.GetSpeed();
                    String heading = oReport.GetAngle();
                    String strLogReason = oReport.GetLogReason();
                    Boolean bIgnitionOn = true;
                    if (strLogReason.Contains("(IgnOff)"))
                        bIgnitionOn = false;
                    String striButton = oReport.GetDriverID();
                    Boolean Aux2 = oReport.GetAux2();
                    Boolean bValid = oReport.IsValid();

                    if (bValid)
                        naveo.UpdateAddLastRecord(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, 0, bIgnitionOn, strLogReason, bValid);
                    else if (Aux2)//Need to get valid lon, lat datenow to utc
                    {
                        strLogReason = "(V with Panic) " + strLogReason;
                        naveo.UpdateAddLastRecord(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, 0, bIgnitionOn, strLogReason, bValid);
                    }
                    else if (strLogReason.Contains("(Ignition)"))//Need to get valid lon, lat datenow to utc
                    {
                        strLogReason = "(V with Ignition) " + strLogReason;
                        naveo.UpdateAddLastRecord(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, 0, bIgnitionOn, strLogReason, bValid);
                    }
                    else if (Lon == 0.0)
                    {
                        strLogReason = "(GpsShutdown) " + strLogReason;
                        naveo.UpdateAddLastRecord(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, 0, bIgnitionOn, strLogReason, bValid);
                    }
                }
            }
            catch (System.Exception ex)
            {
                Logger.WriteToErrorLog(ex);
            }

        }
        List<GPSData> lGPSDataOrion(Byte[] bytWorking, NaveoCore naveo)
        {
            List<GPSData> l = new List<GPSData>();

            String strUnitModel = "ORION";
            DecodeOrion dOrion = new DecodeOrion(bytWorking);
            try
            {
                foreach (OrionReports oReport in dOrion.oReports)
                {
                    GPSData g = new GPSData();

                    String UniqueID = oReport.GetSerialNo();
                    DateTime dt = oReport.GetUTCDateTime();
                    Double Lat = oReport.GetLatitude();
                    Double Lon = oReport.GetLongitude();
                    float Speed = (float)oReport.GetSpeed();
                    String heading = oReport.GetAngle();
                    String strLogReason = oReport.GetLogReason();
                    Boolean bIgnitionOn = true;
                    if (strLogReason.Contains("(IgnOff)"))
                        bIgnitionOn = false;
                    String striButton = oReport.GetDriverID();
                    Boolean Aux2 = oReport.GetAux2();
                    Boolean bValid = oReport.IsValid();

                    if (bValid)
                        g = naveo.initGPSData(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, 0, bIgnitionOn, strLogReason, bValid);
                    else if (Aux2)//Need to get valid lon, lat datenow to utc
                    {
                        strLogReason = "(V with Panic) " + strLogReason;
                        g = naveo.initGPSData(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, 0, bIgnitionOn, strLogReason, bValid);
                    }
                    else if (strLogReason.Contains("(Ignition)"))//Need to get valid lon, lat datenow to utc
                    {
                        strLogReason = "(V with Ignition) " + strLogReason;
                        g = naveo.initGPSData(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, 0, bIgnitionOn, strLogReason, bValid);
                    }
                    else if (Lon == 0.0)
                    {
                        strLogReason = "(GpsShutdown) " + strLogReason;
                        g = naveo.initGPSData(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, 0, bIgnitionOn, strLogReason, bValid);
                    }

                    l.Add(g);
                }
            }
            catch (System.Exception ex)
            {
                Logger.WriteToErrorLog(ex);
            }
            return l;
        }
        #endregion

        #region MeiTrack
        void WriteMeiTrackLogInAFile(Byte[] bytWorking, String Filename, String strSubModel)
        {
            File.AppendAllText(GlobalVariables.logPath() + Filename + "_Raw.txt", Encoding.ASCII.GetString(Utils.CompressBytWorking(bytWorking)));
            File.AppendAllText(GlobalVariables.logPath() + Filename + "_Hex.txt", Utils.BytesToHex(Utils.CompressBytWorking(bytWorking)));
            File.AppendAllText(GlobalVariables.logPath() + Filename + ".txt", GetMeiTrackLog(bytWorking, strSubModel));
        }
        String GetMeiTrackLog(Byte[] bytWorking, String strSubModel)
        {
            String strToFile = String.Empty;
            DecodeMeiTrack dMeiTrack = new DecodeMeiTrack(bytWorking);
            try
            {
                if (dMeiTrack.GetReportLength() <= 4)
                    return strToFile;

                String UniqueID = dMeiTrack.GetIMEI();
                DateTime dt = dMeiTrack.GetUTCDate();
                Double Lat = dMeiTrack.GetLatitude();
                Double Lon = dMeiTrack.GetLongitude();
                float Speed = (float)dMeiTrack.GetSpeed();
                String striButton = string.Empty;
                Boolean Aux1 = dMeiTrack.GetAux1(strSubModel);
                Boolean Aux2 = dMeiTrack.GetAux2(strSubModel);
                Boolean Aux3 = dMeiTrack.GetAux3(strSubModel);
                Boolean Aux4 = dMeiTrack.GetAux4(strSubModel);
                Boolean bValid = dMeiTrack.IsGPSValid();
                String strLogReason = dMeiTrack.GetLogReason(strSubModel);
                String strAD = dMeiTrack.GetAD();

                strToFile += UniqueID + " " + dt.AddHours(4).ToString();
                strToFile += "; Lon: " + Lon.ToString();
                strToFile += "; Lat: " + Lat.ToString();
                strToFile += "; Speed: " + Speed.ToString();
                strToFile += "; LogReason: " + strLogReason;
                strToFile += "; GPSValid: " + bValid.ToString();
                strToFile += "; Aux1-4: " + Aux1.ToString() + Aux2.ToString() + Aux3.ToString() + Aux4.ToString();
                strToFile += "; AD: " + strAD;
                strToFile += "; Sat Nos: " + dMeiTrack.GetsatelliteNos();
                strToFile += "\r\n";
            }
            catch (System.Exception ex)
            {
                Logger.WriteToErrorLog(ex);
            }

            return strToFile;
        }

        void CoreProcessingMeiTrack(Byte[] bytWorking, NaveoCore naveo, String strSubModel)
        {
            String strUnitModel = "MEITRACK";
            DecodeMeiTrack dMeiTrack = new DecodeMeiTrack(bytWorking);

            try
            {
                if (dMeiTrack.GetReportLength() <= 4)
                    return;
                String UniqueID = dMeiTrack.GetIMEI();
                DateTime dt = dMeiTrack.GetUTCDate();
                Double Lat = dMeiTrack.GetLatitude();
                Double Lon = dMeiTrack.GetLongitude();
                float Speed = (float)dMeiTrack.GetSpeed();
                Boolean bIgnitionOn = true;
                String strLogReason = dMeiTrack.GetLogReason(strSubModel);
                if (strLogReason.Contains("(IgnOff)"))
                    bIgnitionOn = false;
                String striButton = string.Empty;
                Boolean bValid = dMeiTrack.IsGPSValid();

                if (bValid)
                    naveo.UpdateAddLastRecord(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, 0, bIgnitionOn, strLogReason, bValid);
            }
            catch (System.Exception ex)
            {
                Logger.WriteToErrorLog(ex);
            }
        }
        GPSData GPSDataMeiTrack(Byte[] bytWorking, NaveoCore naveo, String strSubModel)
        {
            GPSData g = new GPSData();
            String strUnitModel = "MEITRACK";
            DecodeMeiTrack dMeiTrack = new DecodeMeiTrack(bytWorking);

            try
            {
                if (dMeiTrack.GetReportLength() <= 4)
                    return g;
                String UniqueID = dMeiTrack.GetIMEI();
                DateTime dt = dMeiTrack.GetUTCDate();
                Double Lat = dMeiTrack.GetLatitude();
                Double Lon = dMeiTrack.GetLongitude();
                float Speed = (float)dMeiTrack.GetSpeed();
                String strLogReason = dMeiTrack.GetLogReason(strSubModel);
                Boolean bIgnitionOn = true;
                String striButton = "1";
                if (strLogReason.Contains("(IgnOff)"))
                    bIgnitionOn = false;
                Boolean bValid = dMeiTrack.IsGPSValid();

                if (bValid)
                    g = naveo.initGPSData(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, 0, bIgnitionOn, strLogReason, bValid);
            }
            catch (System.Exception ex)
            {
                Logger.WriteToErrorLog(ex);
            }
            return g;
        }

        void WriteMeiligaoLogInAFile(Byte[] bytWorking, String Filename, String strSubModel)
        {
            DecodeMeiligao dMeiligao = new DecodeMeiligao(bytWorking);
            File.AppendAllText(GlobalVariables.logPath() + Filename + "_Raw.txt", Encoding.ASCII.GetString(Utils.CompressBytWorking(bytWorking)));
            File.AppendAllText(GlobalVariables.logPath() + Filename + "_Hex.txt", Utils.BytesToHex(Utils.CompressBytWorking(bytWorking)));

            try
            {
                String UniqueID = dMeiligao.GetSerial();
                DateTime dt = dMeiligao.GetUTCDate();
                Double Lat = dMeiligao.GetLatitude();
                Double Lon = dMeiligao.GetLongitude();
                float Speed = (float)dMeiligao.GetSpeed();
                String striButton = string.Empty;
                Boolean bValid = dMeiligao.IsGPSValid();
                String strLogReason = dMeiligao.GetLogReason();

                String strToFile = UniqueID + " " + dt.AddHours(4).ToString();
                strToFile += "; Lon: " + Lon.ToString();
                strToFile += "; Lat: " + Lat.ToString();
                strToFile += "; Speed: " + Speed.ToString();
                strToFile += "; LogReason: " + strLogReason;
                strToFile += "; GPSValid: " + bValid.ToString();
                strToFile += "\r\n";

                if (Filename == string.Empty)
                    Filename = "LogReason";
                File.AppendAllText(GlobalVariables.logPath() + Filename + ".txt", strToFile);
            }
            catch (System.Exception ex)
            {
                Logger.WriteToErrorLog(ex);
            }

        }
        void CoreProcessingMeiligao(Byte[] bytWorking, NaveoCore naveo, String strSubModel)
        {
            String strUnitModel = "MEITRACK";
            DecodeMeiligao dMeiligao = new DecodeMeiligao(bytWorking);

            try
            {
                String UniqueID = dMeiligao.GetSerial();
                DateTime dt = dMeiligao.GetUTCDate();
                Double Lat = dMeiligao.GetLatitude();
                Double Lon = dMeiligao.GetLongitude();
                float Speed = (float)dMeiligao.GetSpeed();
                Boolean bIgnitionOn = true;
                String striButton = string.Empty;
                Boolean bValid = dMeiligao.IsGPSValid();
                String strLogReason = dMeiligao.GetLogReason();

                //if (bValid)
                naveo.UpdateAddLastRecord(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, 0, bIgnitionOn, strLogReason, bValid);
            }
            catch (System.Exception ex)
            {
                Logger.WriteToErrorLog(ex);
            }
        }
        #endregion

        #region Atrack
        void WriteATrackLogInAFile(Byte[] bytWorking, String Filename)
        {
            File.AppendAllText(GlobalVariables.logPath() + Filename + "_Raw.txt", Encoding.ASCII.GetString(Utils.CompressBytWorking(bytWorking)));
            File.AppendAllText(GlobalVariables.logPath() + Filename + "_Hex.txt", Utils.BytesToHex(Utils.CompressBytWorking(bytWorking)));
            File.AppendAllText(GlobalVariables.logPath() + Filename + ".txt", GetATrackLog(bytWorking));
        }
        String GetATrackLog(Byte[] bytWorking)
        {
            String strToFile = String.Empty;
            DecodeAtrack dAtrack = new DecodeAtrack(bytWorking);

            try
            {
                if (dAtrack.aReports != null)
                    foreach (AtrackReport aReport in dAtrack.aReports)
                    {
                        String UniqueID = dAtrack.GetUnitID();
                        DateTime dt = aReport.GetUTCDate();
                        Double Lat = aReport.GetLatitude();
                        Double Lon = aReport.GetLongitude();
                        float Speed = (float)aReport.GetSpeed();
                        String striButton = aReport.GetDriverID();
                        Boolean bIgnitionOn = aReport.GetAux0();
                        Boolean Aux1 = aReport.GetAux1();
                        Boolean Aux2 = aReport.GetAux2();
                        Boolean Aux3 = aReport.GetAux3();
                        Boolean Aux4 = false;
                        Boolean bValid = aReport.IsGPSValid();
                        String strLogReason = aReport.GetLogReason();
                        String strOBDData = aReport.GetOBDDataInMsg();

                        strToFile += UniqueID + " " + dt.AddHours(4).ToString();
                        strToFile += "; Lon: " + Lon.ToString();
                        strToFile += "; Lat: " + Lat.ToString();
                        strToFile += "; Speed: " + Speed.ToString();
                        strToFile += "; LogReason: " + strLogReason;
                        strToFile += "; GPSValid: " + bValid.ToString();
                        strToFile += "; Aux0-4: " + bIgnitionOn.ToString() + Aux1.ToString() + Aux2.ToString() + Aux3.ToString() + Aux4.ToString();
                        strToFile += "; iButton: " + striButton;
                        strToFile += "; Temp1: " + aReport.GetTemperature(1).ToString();
                        strToFile += "; Temp2: " + aReport.GetTemperature(2).ToString();
                        strToFile += "; Temp3: " + aReport.GetTemperature(3).ToString();
                        strToFile += "; Temp4: " + aReport.GetTemperature(4).ToString();
                        strToFile += "; Analogue: " + aReport.GetAnalogInputValue().ToString();
                        if (aReport.bHasSquarellData)
                            //if (aReport.dSquarell.AllData().Contains("$FMS"))
                            strToFile += "; Squarell: " + aReport.dSquarell.AllData();
                        strToFile += "; MechatronicsData: " + aReport.GetMechatronicsData().allData;
                        strToFile += "; ODBData: " + strOBDData
                            + " OBDFuel: " + aReport.GetOBDFuel();

                        strToFile += "\r\n";
                    }
            }
            catch (System.Exception ex)
            {
                Logger.WriteToErrorLog(ex);
            }
            return strToFile;
        }

        void CoreProcessingAtrack(Byte[] bytWorking, NaveoCore naveo)
        {
            String strUnitModel = "ATRACK";
            DecodeAtrack dAtrack = new DecodeAtrack(bytWorking);

            try
            {
                foreach (AtrackReport aReport in dAtrack.aReports)
                {
                    String UniqueID = dAtrack.GetUnitID();
                    DateTime dt = aReport.GetUTCDate();
                    Double Lat = aReport.GetLatitude();
                    Double Lon = aReport.GetLongitude();
                    float Speed = (float)aReport.GetSpeed();
                    Boolean bIgnitionOn = aReport.GetAux0();
                    String striButton = aReport.GetDriverID();
                    Boolean bValid = aReport.IsGPSValid();
                    String strTemp = aReport.GetTemperature(1).ToString();
                    String strAnalogue = aReport.GetAnalogInputValue().ToString();
                    String strLogReason = aReport.GetLogReason() + "\n" + strTemp + "\n" + strAnalogue;
                    if (aReport.bHasSquarellData)
                        if (aReport.dSquarell.AllData().Contains("$FMS"))
                            strLogReason += "\n" + aReport.dSquarell.AllData();

                    if (bValid)
                        naveo.UpdateAddLastRecord(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, 0, bIgnitionOn, strLogReason, bValid);
                    else if (strLogReason.Contains("(DriverID)"))    //Force save DriverID in NaveoGrnlList
                        naveo.UpdateAddLastRecord(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, 0, bIgnitionOn, strLogReason, bValid);
                    else if (strLogReason.Contains("(IgnOff)"))    //Force delete DriverID in NaveoGrnlList
                        naveo.UpdateAddLastRecord(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, 0, bIgnitionOn, "(InvalidGpsSignals) " + strLogReason, bValid);
                    else if (strTemp != "200")    //Force save temperature if GPS invalid
                        naveo.UpdateAddLastRecord(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, 0, bIgnitionOn, "(InvalidGpsSignals) " + strLogReason, bValid);
                    else if (strLogReason.Contains("GPS antenna Disconnected"))    //Force save if GPS antenna Disconnected
                        naveo.UpdateAddLastRecord(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, 0, bIgnitionOn, strLogReason, bValid);
                    else
                        naveo.UpdateAddLastRecord(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, 0, bIgnitionOn, "(GpsShutdown) " + strLogReason, bValid);
                }
            }
            catch (System.Exception ex)
            {
                Logger.WriteToErrorLog(ex);
            }

        }
        List<GPSData> lGPSDataAtrack(Byte[] bytWorking, NaveoCore naveo)
        {
            List<GPSData> l = new List<GPSData>();
            String strUnitModel = "ATRACK";
            DecodeAtrack dAtrack = new DecodeAtrack(bytWorking);

            try
            {
                if (dAtrack.aReports != null)
                    foreach (AtrackReport aReport in dAtrack.aReports)
                    {
                        GPSData g = new GPSData();

                        String UniqueID = dAtrack.GetUnitID();
                        DateTime dt = aReport.GetUTCDate();
                        Double Lat = aReport.GetLatitude();
                        Double Lon = aReport.GetLongitude();
                        float Speed = (float)aReport.GetSpeed();
                        Boolean bIgnitionOn = aReport.GetAux0();
                        String striButton = aReport.GetDriverID();
                        Boolean bValid = aReport.IsGPSValid();
                        String strTemp = aReport.GetTemperature(1).ToString();
                        String strTemp2 = aReport.GetTemperature(2).ToString();
                        String strTemp3 = aReport.GetTemperature(3).ToString();
                        String strTemp4 = aReport.GetTemperature(4).ToString();
                        String strAnalogue = aReport.GetAnalogInputValue().ToString();
                        String sMechatronicsData = aReport.GetMechatronicsData().allData;
                        String strLogReason = aReport.GetLogReason() + "\n" + strTemp + "\n" + strTemp2 + "\n" + strTemp3 + "\n" + strTemp4 + "\n" + strAnalogue + "\n" + sMechatronicsData;
                        if (aReport.bHasSquarellData)
                            if (aReport.dSquarell.AllData().Contains("$FMS"))
                                strLogReason += "\n" + aReport.dSquarell.AllData();

                        if (bValid)
                            g = naveo.initGPSData(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, 0, bIgnitionOn, strLogReason, bValid);
                        else if (strLogReason.Contains("(DriverID)"))    //Force save DriverID in NaveoGrnlList
                            g = naveo.initGPSData(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, 0, bIgnitionOn, strLogReason, bValid);
                        else if (strLogReason.Contains("(IgnOff)"))    //Force delete DriverID in NaveoGrnlList
                            g = naveo.initGPSData(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, 0, bIgnitionOn, "(InvalidGpsSignals) " + strLogReason, bValid);
                        else if (strTemp != "200")    //Force save temperature if GPS invalid
                            g = naveo.initGPSData(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, 0, bIgnitionOn, "(InvalidGpsSignals) " + strLogReason, bValid);
                        else if (strLogReason.Contains("GPS antenna Disconnected"))    //Force save if GPS antenna Disconnected
                            g = naveo.initGPSData(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, 0, bIgnitionOn, strLogReason, bValid);
                        else
                            g = naveo.initGPSData(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, 0, bIgnitionOn, "(GpsShutdown) " + strLogReason, bValid);

                        l.Add(g);
                    }
            }
            catch (System.Exception ex)
            {
                Logger.WriteToErrorLog(ex);
            }
            return l;
        }
        #endregion

        #region Metron
        void WriteMetronLogInAFile(Byte[] bytWorking, String Filename)
        {
            File.AppendAllText(GlobalVariables.logPath() + Filename + "_Raw.txt", Encoding.ASCII.GetString(Utils.CompressBytWorking(bytWorking)));
            File.AppendAllText(GlobalVariables.logPath() + Filename + ".txt", GetMetronLog(bytWorking));
        }
        String GetMetronLog(Byte[] bytWorking)
        {
            String strToFile = String.Empty;
            DecodeMetron dMetron = new DecodeMetron(bytWorking);

            try
            {
                String sHeader = dMetron.GetHeader();
                String PowElectricsType = dMetron.GetPowElectricsType();
                String UniqueID = dMetron.GetUnitID();
                DateTime dt = dMetron.GetUTCdt();
                String hCRC = dMetron.GetCRC();

                String Status = dMetron.GetStatus();
                Boolean isMaintenanceMode = dMetron.isMaintenanceMode();
                Boolean isFailedCallout = dMetron.isFailedCallout();
                String GetTemperature = dMetron.GetTemperature().ToString();
                Boolean isBatteryLow = dMetron.isBatteryLow();
                Boolean isAutoConfig = dMetron.isAutoConfig();
                String GetCarrier = dMetron.GetCarrier();
                String GetSignal = dMetron.GetSignal().ToString();
                String GetStatusCRC = dMetron.GetStatusCRC();

                String GetValueLog = dMetron.GetValueLog();
                String GetValueCRC = dMetron.GetValueCRC();

                strToFile += "***\r\n";
                strToFile += "Header: " + sHeader;
                strToFile += " Type: " + PowElectricsType;
                strToFile += " UniqueID: " + UniqueID;
                strToFile += " dt: " + dt.AddHours(4).ToString();
                strToFile += " HCRC: " + hCRC;
                strToFile += "\r\n";
                strToFile += "Status: " + Status;
                strToFile += " isMaintenanceMode: " + isMaintenanceMode.ToString();
                strToFile += " isFailedCallout: " + isFailedCallout.ToString();
                strToFile += " Temperature: " + GetTemperature;
                strToFile += " isBatteryLow: " + isBatteryLow.ToString();
                strToFile += " isAutoConfig: " + isAutoConfig.ToString();
                strToFile += " Carrier: " + GetCarrier;
                strToFile += " Signal :" + GetSignal;
                strToFile += " GetStatusCRC: " + GetStatusCRC;
                strToFile += "\r\n";

                strToFile += "ValueLog: " + GetValueLog;
                for (int i = 1; i <= 8; i++)
                {
                    strToFile += " ValueUTCdt: " + dMetron.GetValueUTCdt(i);
                    strToFile += " ValueChannel: " + dMetron.GetValueChannel(i);
                    strToFile += " ValueChannelValue: " + dMetron.GetValueChannelValue(i);
                }
                strToFile += " ValueCRC: " + GetValueCRC;

                strToFile += "\r\nHasEndBlock: " + dMetron.hasEndBlock().ToString();
                strToFile += "\r\n";
            }
            catch (System.Exception ex)
            {
                strToFile += "\r\n";
                strToFile += ex.ToString();
                strToFile += "\r\n";
            }
            return strToFile;
        }
        void CoreProcessingMetron(Byte[] bytWorking, NaveoCore naveo)
        {
            DecodeMetron dMetron = new DecodeMetron(bytWorking);
            try
            {
                TelDataHeader TelData = new TelDataHeader();
                TelData.MsgHeader = dMetron.GetHeader();
                TelData.Type = dMetron.GetPowElectricsType();
                TelData.DeviceID = dMetron.GetUnitID();
                TelData.UTCdt = dMetron.GetUTCdt();

                TelData.MsgStatus = dMetron.GetStatus();
                TelData.isMaintenanceMode = dMetron.isMaintenanceMode() == true ? 1 : 0;
                TelData.isFailedCallout = dMetron.isFailedCallout() == true ? 1 : 0;
                TelData.Temperature = dMetron.GetTemperature();
                TelData.isBatteryLow = dMetron.isBatteryLow() == true ? 1 : 0;
                TelData.isAutoConfig = dMetron.isAutoConfig() == true ? 1 : 0;
                TelData.Carrier = dMetron.GetCarrier();
                TelData.SignalStrength = dMetron.GetSignal();

                //Max of 8 sensors
                for (int i = 1; i <= 8; i++)
                {
                    int iChannel = dMetron.GetValueChannel(i);
                    if (iChannel > 0)
                    {
                        TelDataDetail d = new TelDataDetail();
                        d.UTCdt = dMetron.GetValueUTCdt(i);
                        d.Sensor = dMetron.GetValueChannel(i);
                        d.SensorReading = dMetron.GetValueChannelValue(i);
                        TelData.lTelDataDetail.Add(d);
                    }
                }

                naveo.AddTelemetricRecord(TelData);
            }
            catch (System.Exception ex)
            {
                Logger.WriteToErrorLog(ex);
            }
        }

        void WriteMetronAtexLogInAFile(Byte[] bytWorking, String Filename)
        {
            File.AppendAllText(GlobalVariables.logPath() + Filename + "_Raw.txt", Encoding.ASCII.GetString(Utils.CompressBytWorking(bytWorking)));
            File.AppendAllText(GlobalVariables.logPath() + Filename + ".txt", GetMetronAtexLog(bytWorking));
        }
        String GetMetronAtexLog(Byte[] bytWorking)
        {
            String strToFile = String.Empty;
            DecodeMetronAtex dMetron = new DecodeMetronAtex(bytWorking);

            try
            {
                String sHeader = dMetron.GetHeader();
                String PowElectricsType = dMetron.GetPowElectricsType();
                String UniqueID = dMetron.GetUnitID();
                DateTime dt = dMetron.GetUTCdt();

                String GetValueLog = dMetron.GetValueLog();

                strToFile += "***\r\n";
                strToFile += "Header: " + sHeader;
                strToFile += " Type: " + PowElectricsType;
                strToFile += " UniqueID: " + UniqueID;
                strToFile += " dt: " + dt.AddHours(4).ToString();
                strToFile += " Temperature: " + dMetron.GetTemperature().ToString();
                strToFile += "\r\n";
                strToFile += "\r\n";

                strToFile += "ValueLog: " + GetValueLog;
                for (int i = 1; i <= 6; i++)
                {
                    strToFile += " ValueChannel: " + dMetron.GetChannelName(i);
                    strToFile += " ValueChannelValue: " + dMetron.GetChannelValue(i);
                }

                strToFile += " Carrier " + dMetron.GetCarrier();
                strToFile += " CarrierStrength " + dMetron.GetSignal();

                strToFile += "\r\nHasEndBlock: " + dMetron.hasEndBlock().ToString();
                strToFile += "\r\n";
            }
            catch (System.Exception ex)
            {
                strToFile += "\r\n";
                strToFile += ex.ToString();
                strToFile += "\r\n";
            }
            return strToFile;
        }
        void CoreProcessingMetronAtex(Byte[] bytWorking, NaveoCore naveo)
        {
            DecodeMetronAtex dMetron = new DecodeMetronAtex(bytWorking);
            try
            {
                TelDataHeader TelData = new TelDataHeader();
                TelData.MsgHeader = dMetron.GetHeader();
                TelData.Type = dMetron.GetPowElectricsType();
                TelData.DeviceID = dMetron.GetUnitID();
                TelData.UTCdt = dMetron.GetUTCdt();
                TelData.Carrier = dMetron.GetCarrier();
                TelData.SignalStrength = dMetron.GetSignal();
                TelData.Temperature = dMetron.GetTemperature();

                //Max of 6 sensors
                for (int i = 1; i <= 6; i++)
                {
                    int iChannel = dMetron.GetChannelName(i);
                    if (iChannel > 0)
                    {
                        TelDataDetail d = new TelDataDetail();
                        d.Sensor = dMetron.GetChannelName(i);
                        d.SensorReading = dMetron.GetChannelValue(i);
                        TelData.lTelDataDetail.Add(d);
                    }
                }

                naveo.AddTelemetricRecord(TelData);
            }
            catch (System.Exception ex)
            {
                Logger.WriteToErrorLog(ex);
            }
        }
        #endregion

        #region Inventia
        void WriteInventiaLogInAFile(Byte[] bytWorking, String Filename)
        {
            File.AppendAllText(GlobalVariables.logPath() + Filename + "_Raw.txt", Encoding.ASCII.GetString(Utils.CompressBytWorking(bytWorking)));
            File.AppendAllText(GlobalVariables.logPath() + Filename + ".txt", GetInventiaLog(bytWorking));
        }
        String GetInventiaLog(Byte[] bytWorking)
        {
            String strToFile = String.Empty;
            DecodeInventia dInventia = new DecodeInventia(bytWorking);

            try
            {
                String UniqueID = dInventia.GetUnitID();
                DateTime dt = dInventia.GetUTCDate();

                strToFile += "***\r\n";
                strToFile += " UniqueID: " + UniqueID;
                strToFile += " dt: " + dt.AddHours(4).ToString();
                strToFile += "\r\n";
            }
            catch (System.Exception ex)
            {
                strToFile += "\r\n";
                strToFile += ex.ToString();
                strToFile += "\r\n";
            }
            return strToFile;
        }
        void CoreProcessingInventia(Byte[] bytWorking, NaveoCore naveo)
        {
            DecodeInventia dInventia = new DecodeInventia(bytWorking);
            try
            {
                TelDataHeader TelData = new TelDataHeader();
                TelData.DeviceID = dInventia.GetUnitID();
                TelData.UTCdt = dInventia.GetUTCDate();

                DataSet ds = dInventia.GetAllData();
                foreach (DataRow dr in ds.Tables["dtData"].Rows)
                {
                    TelDataDetail d = new TelDataDetail();
                    d.UTCdt = TelData.UTCdt;
                    d.Sensor = Convert.ToInt32(dr["Type"].ToString());
                    d.SensorReading = (float)Convert.ToDouble(dr["value"].ToString());
                    d.SensorAddress = Convert.ToInt32(dr["Address"].ToString());
                    TelData.lTelDataDetail.Add(d);
                }

                naveo.AddTelemetricRecord(TelData);
            }
            catch (System.Exception ex)
            {
                Logger.WriteToErrorLog(ex);
            }
        }

        #endregion

        #region Xexun
        void WriteXexunLogInAFile(Byte[] bytWorking, String Filename)
        {
            DecodeXexun dXexun = new DecodeXexun(bytWorking);
            File.AppendAllText(GlobalVariables.logPath() + Filename + "_Raw.txt", Encoding.ASCII.GetString(Utils.CompressBytWorking(bytWorking)));

            try
            {
                String UniqueID = dXexun.getImei();
                DateTime dt = dXexun.decodeGPRMC[0].GetDateTime();
                Double Lat = dXexun.decodeGPRMC[0].GetLat();
                Double Lon = dXexun.decodeGPRMC[0].GetLon();
                float Speed = (float)dXexun.decodeGPRMC[0].GetSpeed();
                String heading = dXexun.decodeGPRMC[0].GetAngle();
                String striButton = string.Empty;
                Boolean bValid = dXexun.decodeGPRMC[0].IsValid();
                String strToFile = dt.AddHours(4).ToString();

                strToFile += "; Lon: " + Lon.ToString();
                strToFile += "; Lat: " + Lat.ToString();
                strToFile += "; Speed: " + Speed.ToString();
                strToFile += "; Angle: " + heading;

                strToFile += "\r\n";

                if (Filename == string.Empty)
                    Filename = "LogReason";
                File.AppendAllText(GlobalVariables.logPath() + Filename + ".txt", strToFile);
            }
            catch (System.Exception ex)
            {
                Logger.WriteToErrorLog(ex);
            }
        }
        #endregion

        #region Tramigo
        public void TramigoProcessing(Boolean UpdateCheckmate, out String UnitsUpdated)
        {
            Runme r = new Runme();
            UnitsUpdated = String.Empty;
            //DataSet dset = r.GetAllTramigo();
            DataSet dset = new DataSet(); dset.Tables.Add();
            foreach (DataRow dr in dset.Tables[0].Rows)
            {
                String UniqueID = dr["Number"].ToString();
                DataSet ds = r.GetTramigo(UniqueID);
                if (ds.Tables[0].Rows.Count == 0)
                    continue;

                String[] sCoord = ds.Tables[0].Rows[0]["Coordinates"].ToString().Split(',');
                String Lon = sCoord[0];
                String Lat = sCoord[1];

                DateTimePicker dtp = new DateTimePicker();
                dtp.Value = Convert.ToDateTime(ds.Tables[0].Rows[0]["DateCreated"].ToString()).ToUniversalTime();
                dtp.CustomFormat =
                    Application.CurrentCulture.DateTimeFormat.ShortDatePattern + " " +
                    Application.CurrentCulture.DateTimeFormat.ShortTimePattern;
                DateTime dt = Convert.ToDateTime(dtp.Value.ToString("dd-MMM-yyyy HH:mm:ss tt"));

                UnitsUpdated += UniqueID;
            }
        }
        #endregion

        #region Teltonika
        ////should remove public
        //void WriteTeltonikaLogInAFile(Byte[] bytWorking, String Filename, int i)
        //{
        //    File.AppendAllText(GlobalVariables.logPath() + Filename + "_Raw.txt", Encoding.ASCII.GetString(bytWorking));
        //    File.AppendAllText(GlobalVariables.logPath() + Filename + "_Hex.txt", i.ToString() + "\r\n" + Utils.BytesToHex2(bytWorking) + "\r\n");
        //    File.AppendAllText(GlobalVariables.logPath() + Filename + ".txt", i.ToString() + GetTeltonikaLog(bytWorking, Filename));
        //}
        //String GetTeltonikaLog(Byte[] bytWorking, String strDeviceID)
        //{
        //    String strToFile = "\r\n";
        //    DecodeTeltonika dTeltonika = new DecodeTeltonika(bytWorking);

        //    if (dTeltonika.tReports != null)
        //        foreach (TeltonikaReport tReport in dTeltonika.tReports)
        //        {
        //            strToFile += strDeviceID + " " + tReport.GetTimeStamp().ToString();

        //            strToFile += " Ign : " + tReport.GetIgnition().ToString();
        //            strToFile += " Lat : " + tReport.GetLatitude().ToString();
        //            strToFile += " Lon : " + tReport.GetLongitude().ToString();
        //            strToFile += " Alt : " + tReport.GetAltitude().ToString();
        //            strToFile += " Ang : " + tReport.GetAngle().ToString();
        //            strToFile += " Sat : " + tReport.GetNoOfSalettiles().ToString();
        //            strToFile += " Speed : " + tReport.GetSpeed().ToString();
        //            strToFile += " Priority : " + tReport.GetPriority().ToString();
        //            strToFile += " LR : " + tReport.GetLogReason().ToString();
        //            strToFile += " IMSI : " + tReport.GetSimIMSI().ToString();
        //            strToFile += " GetAnalogInput1 : " + tReport.GetAnalogInput1();
        //            //strToFile += " GetDigitalInput1 : " + tReport.GetDigitalInput1();
        //            strToFile += " GetExternalVoltage : " + tReport.GetExternalVoltage();    //BEasy only
        //            strToFile += " GetDigitalOutput1 : " + tReport.GetDigitalOutput1();     //BEasy only
        //            strToFile += "\r\n";
        //        }
        //    return strToFile;
        //}

        //List<GPSData> lGPSDataTeltonika(Byte[] bytWorking, NaveoCore naveo, String strDeviceID)
        //{
        //    List<GPSData> l = new List<GPSData>();
        //    String strUnitModel = "TELTONIKA";
        //    DecodeTeltonika dTeltonika = new DecodeTeltonika(bytWorking);

        //    try
        //    {
        //        if (dTeltonika.tReports != null)
        //            foreach (TeltonikaReport tReport in dTeltonika.tReports)
        //            {
        //                GPSData g = new GPSData();

        //                String UniqueID = strDeviceID;
        //                DateTime dt = tReport.GetTimeStamp();
        //                Double Lat = tReport.GetLatitude();
        //                Double Lon = tReport.GetLongitude();
        //                float Speed = (float)tReport.GetSpeed();
        //                Boolean bIgnitionOn = tReport.GetIgnition();
        //                String striButton = "000000";
        //                Boolean bValid = tReport.GetSatelliteValid();
        //                String strLogReason = tReport.GetLogReason();
        //                g = naveo.initGPSData(strUnitModel, striButton, UniqueID, Lat, Lon, dt, Speed, 0, bIgnitionOn, strLogReason, bValid);

        //                l.Add(g);
        //            }
        //    }
        //    catch (System.Exception ex)
        //    {
        //        Logger.WriteToErrorLog(ex);
        //    }
        //    return l;
        //}
        #endregion

        #region AllInOne
        public void WriteUnitLogsInAFile(String strModel, Byte[] bytWorking, String unitID, int iRowCnt)
        {
            switch (strModel.ToUpper())
            {
                case "AT100":
                    WriteAT100LogInAFile(bytWorking, unitID);
                    break;

                case "AT200":
                    WriteAstraProtocolKLLogInAFile(bytWorking, unitID);
                    break;

                case "AT240":
                    WriteAstraProtocolMNVLogInAFile(bytWorking, unitID);
                    break;

                case "ORION":
                    WriteOrionLogInAFile(bytWorking, unitID);
                    break;

                case "MEILIGAO":
                    WriteMeiligaoLogInAFile(bytWorking, unitID, strModel);
                    break;

                case "MVT100":
                    WriteMeiTrackLogInAFile(bytWorking, unitID, strModel);
                    break;

                case "MVT340":
                    WriteMeiTrackLogInAFile(bytWorking, unitID, strModel);
                    break;

                case "MVT380":
                    WriteMeiTrackLogInAFile(bytWorking, unitID, strModel);
                    break;

                case "ORIONIMSI":
                    WriteOrionLogInAFile(bytWorking, unitID);
                    break;

                case "ENFORA":
                    WriteEnforaLogInAFile(bytWorking, unitID);
                    break;

                case "ENFORN":
                    WriteEnforaGSM2448LogInAFile(bytWorking, unitID);
                    break;

                case "STEPP":
                    WriteSteppLogInAFile(bytWorking, unitID);
                    break;

                case "ATRACK":
                    WriteATrackLogInAFile(bytWorking, unitID);
                    break;

                case "ATRACKIMSI":
                    WriteATrackLogInAFile(bytWorking, unitID);
                    break;

                case "XEXUN":
                    WriteXexunLogInAFile(bytWorking, unitID);
                    break;

                case "METRON":
                    WriteMetronLogInAFile(bytWorking, unitID);
                    break;

                case "INVENTIA":
                    WriteInventiaLogInAFile(bytWorking, unitID);
                    break;

                case "METRONATEX":
                    WriteMetronAtexLogInAFile(bytWorking, unitID);
                    break;

                case "TELTONIKA":
                    //WriteTeltonikaLogInAFile(bytWorking, unitID, iRowCnt);
                    break;
            }
        }
        public String GetUnitLogs(String strModel, Byte[] bytWorking, String unitID)
        {
            String strResult = String.Empty;
            switch (strModel.ToUpper())
            {
                case "ENFORN":
                    strResult = GetEnforaGSM2448Log(bytWorking);
                    break;

                case "ENFORA":
                    strResult = GetEnforaLog(bytWorking);
                    break;

                case "ATRACK":
                    strResult = GetATrackLog(bytWorking);
                    break;

                case "MVT100":
                    strResult = GetMeiTrackLog(bytWorking, strModel);
                    break;

                case "MVT340":
                    strResult = GetMeiTrackLog(bytWorking, strModel);
                    break;

                case "MVT380":
                    strResult = GetMeiTrackLog(bytWorking, strModel);
                    break;

                case "ORION":
                    strResult = GetOrionLog(bytWorking);
                    break;

                case "ORIONIMSI":
                    strResult = GetOrionLog(bytWorking);
                    break;

                case "AT100":
                    strResult = GetAT100Log(bytWorking);
                    break;

                case "AT200":
                    strResult = GetAstraProtocolKLLog(bytWorking);
                    break;

                case "AT240":
                    strResult = GetAstraProtocolMNVLog(bytWorking);
                    break;

                case "STEPP":
                    strResult = GetSteppLog(bytWorking);
                    break;

                case "METRON":
                    strResult = GetMetronLog(bytWorking);
                    break;

                case "METRONATEX":
                    strResult = GetMetronAtexLog(bytWorking);
                    break;

                case "TELTONIKA":
                    //strResult = GetTeltonikaLog(bytWorking, unitID);
                    break;
            }
            return strResult;
        }

        List<GPSData> mylGPSData(String strModel, Byte[] bytWorking, NaveoCore naveo, String strDeviceID)
        {
            List<GPSData> l = new List<GPSData>();
            try
            {
                switch (strModel.ToUpper())
                {
                    case "ENFORN":
                        l.Add(GPSDataGSM2448(bytWorking, naveo));
                        break;

                    case "ENFORA":
                        l.Add(GPSDataEnforaGSM2218(bytWorking, naveo));
                        break;

                    case "ATRACK":
                        l = lGPSDataAtrack(bytWorking, naveo);
                        break;

                    case "MEILIGAO":
                        //CoreProcessingMeiligao(bytWorking, naveo, strModel);
                        break;

                    case "MVT100":
                        l.Add(GPSDataMeiTrack(bytWorking, naveo, strModel));
                        break;

                    case "MVT340":
                        l.Add(GPSDataMeiTrack(bytWorking, naveo, strModel));
                        break;

                    case "MVT380":
                        l.Add(GPSDataMeiTrack(bytWorking, naveo, strModel));
                        break;

                    case "ORION":
                        l = lGPSDataOrion(bytWorking, naveo);
                        break;

                    case "AT100":
                        l = lGPSDataAT100(bytWorking, naveo);
                        break;

                    case "AT200":
                        l = lGPSDataAstraProtocolKL(bytWorking, naveo);
                        break;

                    case "AT240":
                        l = lGPSDataAstraProtocolMNV(bytWorking, naveo);
                        break;

                    case "STEPP":
                        l = lGPSDataStepp(bytWorking, naveo);
                        break;

                    case "METRON":
                        CoreProcessingMetron(bytWorking, naveo);
                        break;

                    case "INVENTIA":
                        CoreProcessingInventia(bytWorking, naveo);
                        break;

                    case "METRONATEX":
                        CoreProcessingMetronAtex(bytWorking, naveo);
                        break;

                    case "TELTONIKA":
                        //l = lGPSDataTeltonika(bytWorking, naveo, strDeviceID);
                        break;
                }
            }
            catch (System.Exception ex)
            {
                Logger.WriteToErrorLog(ex);
            }

            return l;
        }
        public int BulkCoreProcessing(DataTable dt, NaveoCore naveo)
        {
            List<GPSData> lGPSData = new List<GPSData>();
            foreach (DataRow dr in dt.Rows)
            {
                List<GPSData> l = mylGPSData((String)dr["Model"], (Byte[])dr["RawData"], naveo, (String)dr["unitID"]);
                lGPSData.AddRange(l);
            }

            Boolean b = true;
            int i = lGPSData.Count;
            if (i > 0)
                b = naveo.BulkCoreProcessing(lGPSData);

            if (b)
                return i;
            else
                return -999;
        }

        public int BulkSecuMonitorProcessing(DataTable dt, NaveoCore naveo)
        {
            List<GPSData> lGPSData = new List<GPSData>();
            foreach (DataRow dr in dt.Rows)
            {
                List<GPSData> l = mylGPSData((String)dr["Model"], (Byte[])dr["RawData"], naveo, (String)dr["unitID"]);
                lGPSData.AddRange(l);
            }

            Boolean b = true;
            int i = lGPSData.Count;
            if (i > 0)
                b = naveo.BulkSecuMonitorProcessing(lGPSData);

            if (b)
                return i;
            else
                return -999;
        }
        #endregion
    }
}