using System;
using System.Collections.Generic;
using System.Text;

namespace NavLib.STEPP
{
    public class DecodeStepp
    {
        const int Offset_ReportLength = 20;

        public List<SteppReport> SteppReports;
        public String GetUnitIMEI;

        public DecodeStepp(byte[] bytPacketData)
        {
            SteppReports = new List<SteppReport>();
            GetUnitIMEI = String.Empty;

            String myStrData = Encoding.ASCII.GetString(bytPacketData);
            myStrData = Encoding.ASCII.GetString(bytPacketData);
            myStrData = myStrData.Replace("<end>", "|");

            String[] myData = myStrData.Split('|');
            foreach (String strData in myData)
            {
                String[] MySingleReport = strData.Split(',');
                if (MySingleReport[0].Contains("Navigation") && MySingleReport.Length == Offset_ReportLength)
                {
                    SteppReports.Add(new SteppReport(strData));
                    if (GetUnitIMEI == String.Empty)
                        GetUnitIMEI = MySingleReport[3].Trim();
                }
            }
        }
    }
}
