using System;
using System.Collections.Generic;
using System.Text;

namespace NavLib.STEPP
{
    public class SteppReport
    {
        const int Offset_F1 = 0 + 8;
        const int Offset_F2 = 1 + 8;
        const int Offset_F3 = 2 + 8;
        const int Offset_F4 = 3 + 8;
        const int Offset_F5 = 4 + 8;
        const int Offset_F6 = 5 + 8;
        const int Offset_F7 = 6 + 8;
        const int Offset_F8 = 7 + 8;
        const int Offset_F9 = 8 + 8;
        const int Offset_F10 = 9 + 8;
        const int Offset_F11 = 10 + 8;
        //const int Offset_F12 = 11 + 8;
        const int Offset_F13 = 11 + 8;
        //const int Offset_F14 = 13 + 8;

        String myStrData = String.Empty;
        String[] myData;

        public SteppReport(String StrPacketData)
        {
            myData = StrPacketData.Split(',');
        }

        public String GetHeader()
        {
            return myData[0];
        }
        public String GetReason()
        {
            String lr = myData[2];
            if (lr.Contains("Distance"))
                lr += "(Heading)";

            if (lr.Contains("HeadChange"))
                lr += "(Heading)";

            //Check this condition last, as only 1 lr is saved
            if (lr.Contains("IgnitionChange"))
                lr += "(Ignition)";

            Boolean bIgn = IgnOn();
            if (!bIgn && lr.Contains("Distance"))
                lr += "(GpsShutdown)";

            if (!bIgn && lr.Contains("HeadChange"))
                lr += "(GpsShutdown)";

            //if (GetSpeed() * 2 < 5)
            //    lr += "(GpsShutdown)";

            lr += " Ign " + bIgn;

            return lr;
        }
        public String GetUnitIMEI()
        {
            return myData[3].Trim();
        }
        public Boolean IgnOn()
        {
            Boolean bResult = true;
            String strResult = myData[4];
            if (strResult.Trim() == "0")
                bResult = false;

            return bResult;
        }

        public String GetFormat()
        {
            return myData[Offset_F1];
        }
        public String GetTime()
        {
            return myData[Offset_F2];
        }
        public Boolean IsValid()
        {
            Boolean bResult = false;
            if (myData[Offset_F3] == "A")
                bResult = true;
            return bResult;
        }
        public Double GetLat()
        {
            String g = myData[Offset_F4].Trim();
            Double LatX = Double.Parse(g.Substring(0, 2));
            Double dblTempDegrees = Double.Parse(g.Substring(2)) / 60;
            LatX += dblTempDegrees;
            LatX *= -1;
            return LatX;
        }
        public String GetNS()
        {
            return myData[Offset_F5];
        }
        public Double GetLon()
        {
            String g = myData[Offset_F6].Trim();
            Double LatX = Double.Parse(g.Substring(0, 3));
            Double dblTempDegrees = Double.Parse(g.Substring(3)) / 60;
            LatX += dblTempDegrees;
            return LatX;
        }
        public String GetEW()
        {
            return myData[Offset_F7];
        }
        public Double GetSpeed()
        {
            if (myData[Offset_F8].Length == 0)
                return 0;
            else
                return Convert.ToDouble(myData[Offset_F8].ToString());
        }
        public String GetHeading()
        {
            return myData[Offset_F9];
        }
        public String GetDate()
        {
            return myData[Offset_F10];
        }
        public String GetMagneticVariation()
        {
            return myData[Offset_F11];
        }
        //public String GetMagneticVariationDirection()
        //{
        //    return myData[Offset_F12];
        //}
        public String GetFormatChecksum()
        {
            return myData[Offset_F13];
        }
        //public String GetEOT()
        //{
        //    return myData[Offset_F14];
        //}
        public DateTime GetDateTime()
        {
            String dt = GetDate() + GetTime();
            int dd = Convert.ToInt16(dt.Substring(0, 2));
            int mm = Convert.ToInt16(dt.Substring(2, 2));
            int yy = 2000 + Convert.ToInt16(dt.Substring(4, 2));
            int hh = Convert.ToInt16(dt.Substring(6, 2));
            int min = Convert.ToInt16(dt.Substring(8, 2));
            int ss = Convert.ToInt16(dt.Substring(10, 2));

            DateTime dtResult = new DateTime(yy, mm, dd, hh, min, ss);
            return dtResult;
        }
    }
}
