﻿using System;

namespace NavLib.AstraTelematics.ProtokolKL
{
	/// <summary>
	/// Description of Decoder.
	/// </summary>
	public class Decoder
	{
		private String decode = String.Empty;
		private byte protocol;

		public Decoder(byte protocolType)
		{
			protocol = protocolType;
		}
		
		public String ReportToStrings(byte[] packet)
		{
			byte index;
			int status;
			int bit;
			String tacFac;
			String msn;
			String geofenceEvent;

			AT200Parser parser = new AT200Parser(packet, protocol);

			if (!parser.CheckSumValid)
			{
				decode = "Checksum fail: recv=" + parser.CheckSumData + " calculated=" + parser.CheckSumCalculated + Environment.NewLine;
			}
			else if (parser.objHeader.Valid)
            {
				decode = "== PROTOCOL " + Convert.ToChar(protocol) + " ==" + Environment.NewLine;

				if ((protocol == AT200Constants.PROTOCOL_K) || (protocol == AT200Constants.PROTOCOL_L))
				{
					decode += "Packet len:" + parser.objHeader.PacketLen().ToString() + Environment.NewLine;
	
					if (protocol == AT200Constants.PROTOCOL_K)
					{
						tacFac = String.Format("{0:00000000}", parser.objHeader.GetTACFAC());
						msn = String.Format("{0:0000000}", parser.objHeader.GetMSN());
						decode += "IMEI: " + tacFac + msn + Environment.NewLine;
						decode += Environment.NewLine;
					}
	
					foreach (AT200Report report in parser.objReports)
					{			
						decode += Environment.NewLine;
						decode += "REPORT (Seq num:" + report.GetPacketSequenceNumber().ToString() + ")" + Environment.NewLine;
						decode += "Latitude:" + report.GetLatitude().ToString() + " Longitude:" + report.GetLongitude().ToString() + Environment.NewLine;
						decode += "Time:" + report.GetTime().ToString() + Environment.NewLine;
						decode += "Speed (km/h):" + report.GetSpeedInKmh().ToString() + " Heading (deg):" + report.GetHeadingInDegrees().ToString() + Environment.NewLine;
	
						// Reason code
						decode += "Reason codes:" + Environment.NewLine;
						if (report.Reason_TimedIntervalElapsed())
							decode += "> Timed interval" + Environment.NewLine;
						if (report.Reason_DistanceTravelledExceeded())
							decode += "> Distance travelled exceeded" + Environment.NewLine;
						if (report.Reason_PosOnDemand())
							decode += "> Position on demand" + Environment.NewLine;
						if (report.Reason_GeoFence())
							decode += "> Geofence event" + Environment.NewLine;
						if (report.Reason_PanicSwitchActivated())
							decode += "> Panic switch activated" + Environment.NewLine;
						if (report.Reason_ExtInputEvent())
							decode += "> External input event" + Environment.NewLine;
						if (report.Reason_JourneyStart())
							decode += "> Journey start" + Environment.NewLine;
						if (report.Reason_JourneyStop())
							decode += "> Journey Stop" + Environment.NewLine;
						if (report.Reason_HeadingChange())
							decode += "> Heading change" + Environment.NewLine;
						if (report.Reason_LowBattery())
							decode += "> Low battery" + Environment.NewLine;
						if (report.Reason_ExtPowerEvent())
							decode += "> External power event" + Environment.NewLine;
						if (report.Reason_IdlingStarting())
							decode += "> Idle start" + Environment.NewLine;
						if (report.Reason_IdlingEnd())
							decode += "> Idle end" + Environment.NewLine;
						if (report.Reason_IdlingOnGoing())
							decode += "> Idle ongoing" + Environment.NewLine;
						if (report.Reason_Alarm())
							decode += "> Restart" + Environment.NewLine;
						if (report.Reason_SpeedOverThreshold())
							decode += "> Speed over threshold" + Environment.NewLine;
						if (report.Reason_TowingAlarm())
							decode += "> Towing alarm" + Environment.NewLine;
						if (report.Reason_UnauthorisedDriverAlarm())
							decode += "> Unauthorised driver" + Environment.NewLine;
						if (report.Reason_CollisionAlarm())
							decode += "> Collision alarm" + Environment.NewLine;
						if (report.Reason_AccelThresholdEvent())
							decode += "> Accel max event" + Environment.NewLine;
						if (report.Reason_CorneringThresholdEvent())
							decode += "> Cornering max event" + Environment.NewLine;
						if (report.Reason_DecelThresholdEvent())
							decode += "> Decel max event" + Environment.NewLine;
						if (report.Reason_GPSReacquired())
							decode += "> GPS re-acquired" + Environment.NewLine;
						if (report.Reason_CanbusEvent())
							decode += "> CANBus event" + Environment.NewLine;
	
						// Status
						decode += "Status:" + Environment.NewLine;
						decode += "> IGN ";
						if (report.Status_IgnitionOn())
							decode += "ON" + Environment.NewLine;
						else
							decode += "OFF" + Environment.NewLine;
						decode += "> Mode:";
						if (report.Status_PowerOnOrRestart())
							decode += "Private" + Environment.NewLine;
						else
							decode += "Business" + Environment.NewLine;
						decode += "> GPS:";
						if (report.Status_GPSTimeOut())
							decode += "Invalid" + Environment.NewLine;
						else
							decode += "Valid" + Environment.NewLine;
						decode += "> Network:";
						if (report.Status_NetworkRoaming())
							decode += "Roaming" + Environment.NewLine;
						else
							decode += "Home" + Environment.NewLine;
						if (report.Status_ReportsToFollow())
							decode += "> Reports to follow" + Environment.NewLine;
						decode += "> Report:";
						if (report.Status_StoredReport())
							decode += "Stored" + Environment.NewLine;
						else
							decode += "Live" + Environment.NewLine;
						if (report.Status_Extension())
							decode += "> Optional extension data present" + Environment.NewLine;
						if (report.Status_SerialDeviceReadError())
							decode += "> Serial device read error" + Environment.NewLine;
						if (report.Status_ExtraData())
							decode += "> Extra data present" + Environment.NewLine;
	
						decode += "Digital change status:" + report.DigitalChangeStatus().ToString() + " Digital status:" + report.DigitalStatus().ToString() + Environment.NewLine;
						decode += "Ext ADC1:" + report.GetADC1().ToString() + Environment.NewLine;
						decode += "Battery level (%):" + report.GetBatteryPercentage().ToString() + Environment.NewLine;
						decode += "Ext input voltage (V):" + report.GetExternalInputVoltage().ToString() + Environment.NewLine;
						decode += "Max journey speed (km/h):" + report.GetMaxJourneySpeed().ToString() + Environment.NewLine;
						decode += "Max X accel:" + report.GetXAxisMaxAcceleration().ToString() + " Max X decel:" + report.GetXAxisMaxDeceleration().ToString() + Environment.NewLine;
						decode += "Max Y accel:" + report.GetYAxisMaxAcceleration().ToString() + " Max Y decel:" + report.GetYAxisMaxDeceleration().ToString() + Environment.NewLine;
						decode += "Max Z accel:" + report.GetYAxisMaxAcceleration().ToString() + " Max Z decel:" + report.GetZAxisMaxDeceleration().ToString() + Environment.NewLine;
						decode += "Journey distance (km):" + report.GetJourneyDistanceTravelled().ToString() + Environment.NewLine;
						decode += "Journey idle time (s):" + report.GetJourneyIdleSeconds().ToString() + Environment.NewLine;
						decode += "Altitude (m):" + report.GetAltitudeInMeters().ToString() + Environment.NewLine;
						decode += "GSM signal strength:" + report.GetGSMSignalStrength().ToString() + " GPS satellites in use:" + report.GetSatellitesInUse().ToString() + Environment.NewLine;
						decode += "Geofence events:";
						index = report.GetGeoFenceIndex();
						
						if (index > 0)
						{
							if (report.GeoFenceCongestionZone())
								geofenceEvent = "Congestion zone";
							else
								geofenceEvent = "Index " + Convert.ToString(index);
							
							if (report.GeoFenceEntred())
								geofenceEvent += " entered";
							else
								geofenceEvent += " left";
							
							decode += geofenceEvent + Environment.NewLine;
						}
						else
						{
							decode += "None" + Environment.NewLine;
						}
						
						// CAN Bus data
						if (protocol == AT200Constants.PROTOCOL_L)
						{
							decode += "CANBus basic:" + Environment.NewLine;
							decode += "Wheel speed (km/h):" + report.GetCanWheelSpeed().ToString() + Environment.NewLine;
							decode += "Engine speed (RPM):" + report.GetCanEngineSpeed().ToString() + Environment.NewLine;
							decode += "Accel pos (%):" + report.GetCanAccelPos().ToString() + Environment.NewLine;
							decode += "Engine load (%):" + report.GetCanEngineLoad().ToString() + Environment.NewLine;
							decode += "Journey distance (km):" + report.GetCanJourneyDistance().ToString() + Environment.NewLine;
							decode += "Engine coolant temp (deg C):" + report.GetCanEngineTemp().ToString() + Environment.NewLine;
							decode += "CANBus status:" + Environment.NewLine;
							status = report.GetCanStatus();
							for (bit = 0; bit < report.GetStringSizeCanStatus(); bit++)
							{
								if ((status & (1 << bit)) != 0)
								{
									decode += "> " + report.GetStringCanStatus(bit) + Environment.NewLine;
								}
							}
	
							decode += "CANBus monitoring status:" + Environment.NewLine;
							status = report.GetCanMonitoringStatus();
							for (bit = 0; bit < report.GetStringSizeCanMonitoringStatus(); bit++)
							{
								if ((status & (1 << bit)) != 0)
								{
									decode += "> " + report.GetStringCanMonitoringStatus(bit) + Environment.NewLine;
								}
							}
						}
						
						if (report.Status_ExtraData())
						{
							if (protocol == AT200Constants.PROTOCOL_K)
							{
								if (report.Reason_JourneyStart() || report.Reason_JourneyStop())
								{
									decode += "Extra data:" + Environment.NewLine;
	
									// Get Driver ID
								}
							}
	
							if (protocol == AT200Constants.PROTOCOL_L)
							{
								if (report.Reason_JourneyStart())
								{
									decode += "Extra data:" + Environment.NewLine;
	
									// Get Driver ID
									
									// Get CANBus data
									decode += "CANBus start:" + Environment.NewLine;
									decode += "Fuel level (%):" + report.GetCanFuelLevel().ToString() + Environment.NewLine;
									decode += "Axle weight (total kg):" + report.GetCanAxleWeight().ToString() + Environment.NewLine;
								}
	
								if (report.Reason_JourneyStop())
								{
									// Get Driver ID
									
									// Get CANBus data
									decode += "CANBus stop:" + Environment.NewLine;
									decode += "Fuel level (%):" + report.GetCanFuelLevel().ToString() + Environment.NewLine;
									decode += "Axle weight (total kg):" + report.GetCanAxleWeight().ToString() + Environment.NewLine;
									decode += "Engine total operation (hours):" + report.GetCanEngineHours().ToString() + Environment.NewLine;
									decode += "Fuel used in journey (l):" + report.GetCanFuelUsedInJourney().ToString() + Environment.NewLine;
									decode += "Fuel used (l per 100km):" + report.GetCanJourneyFuelPer100Km().ToString() + Environment.NewLine;
									decode += "Cruise control active (secs):" + report.GetCanJourneyCruiseControl().ToString() + Environment.NewLine;
									decode += "Service distance (km):" + report.GetCanServiceDistance().ToString() + Environment.NewLine;
								}
							}
						}
					}
				}
			}

			return decode;
		}
	}
}
