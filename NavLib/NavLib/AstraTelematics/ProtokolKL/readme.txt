Assuming you have received a packet in buffer:

private byte[] ack = {0x06};
byte protocol;
int packetLen;
.
.
.
protocol = buffer[0];
packetLen = buffer[1] * 256 + buffer[2];

if (packetLen > bytesInBuffer)
{
	// Not enough data - skip
}
else if (((protocol == AT200Constants.PROTOCOL_K) && bytesInBuffer >= AT200Constants.K_MINSIZE)) ||
         ((protocol == AT200Constants.PROTOCOL_L) && bytesInBuffer >= AT200Constants.L_MINSIZE)))
{
	Decoder decoder = new Decoder(protocol);
	output += decoder.ReportToStrings(buffer);
	.
	.
	.
	sendRemote(ack, ack.Length);
}
