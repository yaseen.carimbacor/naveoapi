Assuming you have received a packet in buffer:

private byte[] ack = {0x06};
byte protocol;
int packetLen;
.
.
.
protocol = buffer[0];
packetLen = buffer[1] * 256 + buffer[2];

if (packetLen > bytesInBuffer)
{
	// Not enough data - skip
}
else if (((protocol == AstraConstants.PROTOCOL_M) && (inIdx >= AstraConstants.M_MINSIZE)) ||
	     ((protocol == AstraConstants.PROTOCOL_N) && (inIdx >= AstraConstants.N_MINSIZE)) ||
	     ((protocol == AstraConstants.PROTOCOL_V) && (inIdx >= AstraConstants.V_MINSIZE)))
{
	Decoder decoder = new Decoder(protocol);
	output += decoder.ReportToStrings(buffer);
	.
	.
	.
	sendRemote(ack, ack.Length);
}

