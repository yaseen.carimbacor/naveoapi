﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NavLib.Globals;

namespace NavLib.AstraTelematics.ProtokolMNV
{
	class AstraReport
	{
	    // These constants are offsets into a report packet and are consistent with Protocol K, M and N
	    int M_OFFSET_PACKET_SQN = 0; 
	    int M_OFFSET_LAT_START = 1; 
	    int M_OFFSET_LAT_END = 4; 
	    int M_OFFSET_LONG_START = 5; 
	    int M_OFFSET_LONG_END = 8;
	    int M_OFFSET_TIME_START = 9;
	    int M_OFFSET_TIME_END = 12; 
	    int M_OFFSET_SPEED = 13;
	    int M_OFFSET_HEADING = 14;
	    int M_OFFSET_REASON_CODE_MSB = 15;
	    int M_OFFSET_REASON_CODE_2ND_BYTE = 16;
	    int M_OFFSET_REASON_CODE_LSB = 17;
	    int M_OFFSET_STATUS_MSB = 18;
	    int M_OFFSET_STATUS_LSB = 19;
	    int M_OFFSET_DIGITALS_1 = 20;
	    int M_OFFSET_DIGITALS_2 = 21;
	    int M_OFFSET_DIGITALS_3 = 22;
	    int M_OFFSET_ADC_1 = 23; 
	    int M_OFFSET_ADC_2 = 24; 
	    int M_OFFSET_BATTERY = 25;
	    int M_OFFSET_EXTERNAL_INPUT_VOLTAGE = 26;
	    int M_OFFSET_MAX_JOURNEY_SPEED = 27;
	    int M_OFFSET_MAX_X_ACCELERATION = 28;
	    int M_OFFSET_MAX_X_DECELERATION = 29; 
	    int M_OFFSET_MAX_Y_ACCELERATION = 30;
	    int M_OFFSET_MAX_Y_DECELERATION = 31; 
	    int M_OFFSET_MAX_Z_ACCELERATION = 32;
	    int M_OFFSET_MAX_Z_DECELERATION = 33; 
	    int M_OFFSET_JOURNEY_DIST_TRAVELLED_START = 34;
	    int M_OFFSET_JOURNEY_DIST_TRAVELLED_END = 35;
	    int M_OFFSET_JOURNEY_IDLE_SECONDS_START = 36; 
	    int M_OFFSET_JOURNEY_IDLE_SECONDS_END = 37;
	    int M_OFFSET_ALTITUDE = 38;
	    int M_OFFSET_SIGNAL_QUALITY = 39;
	    int M_OFFSET_GEO_FENCE_EVENT = 40;
	    int M_OFFSET_CAN_WHEEL_SPEED = 41;
	    int M_OFFSET_CAN_ENGINE_SPEED = 42;
	    int M_OFFSET_CAN_ACCEL_POS = 43;
	    int M_OFFSET_CAN_ENGINE_LOAD = 44;
	    int M_OFFSET_CAN_JOURNEY_DIST = 45;
	    int M_OFFSET_CAN_ENGINE_TEMP = 47;
	    int M_OFFSET_CAN_STATUS = 48;
	    int M_OFFSET_CAN_MONITORING_STATUS = 50;
	    int M_OFFSET_CAN_FUEL_LEVEL = 59;
	    int M_OFFSET_CAN_AXLE_WEIGHT = 60;
	    int M_OFFSET_CAN_ENGINE_HOURS = 62;
	    int M_OFFSET_CAN_JOURNEY_TOTAL_FUEL = 66;
	    int M_OFFSET_CAN_JOURNEY_LITRES_PER_100KM = 68;
	    int M_OFFSET_CAN_JOURNEY_CRUISE_CTRL = 70;
	    int M_OFFSET_CAN_SERVICE_DISTANCE = 72;

	    // These constants are offsets into a report packet for Protocol V
	    int M_OFFSET_EVENT_TIME_START = 1;
	    int M_OFFSET_EVENT_TIME_END = 4;
	    int M_OFFSET_ADC_3 = 29;
	    int M_OFFSET_CAN_FMS_STATUS = 53;
	    int M_OFFSET_CAN_OBD_STATUS = 55;
	    int M_OFFSET_CAN_TOTAL_FUEL_USED = 60;
	    int M_OFFSET_CAN_TOTAL_VEHICLE_DIST = 64;
	    int M_OFFSET_CAN_AVG_WHEEL_SPEED = 68;
	    int M_OFFSET_CAN_AVG_ENGINE_SPEED = 69;
	    int M_OFFSET_CAN_AVG_ACCEL_POS = 70;
	    int M_OFFSET_CAN_AVG_ENGINE_LOAD = 71;
	    int M_OFFSET_CAN_TIME_SINCE_ENGINE_START = 94;
	    int M_OFFSET_CAN_DISTANCE_MIL_ON = 96;
	    
	    // Individual Bit Field Descriptors 
	    /* Constants for BYTE M_OFFSET_REASON_CODE_LSB ( Reason Code Least Significant Byte ) */
	    const byte M_REASON_CODE_TIME_INT_ELAPSED = 1;
	    const byte M_REASON_CODE_DIST_TRAVELLED_EXCEEDED = 2;
	    const byte M_REASON_CODE_POS_ON_DEMAND = 4;
	    const byte M_REASON_CODE_GEO_FENCE = 8;
	    const byte M_REASON_CODE_PANIC_SWITCH_ACTIVATED = 16;
	    const byte M_REASON_CODE_EXT_INPUT_EVENT = 32;
	    const byte M_REASON_CODE_JOURNEY_START = 64;
	    const byte M_REASON_CODE_JOURNEY_STOP = 128;
	    /* Constants for BYTE M_OFFSET_REASON_CODE_MSB ( Reason Code 2nd Byte ) */
	    const byte M_REASON_CODE_HEADING_CHANGE = 1;
	    const byte M_REASON_CODE_LOW_BATTERY = 2;
	    const byte M_REASON_CODE_EXT_POWER_EVENT = 4;
	    const byte M_REASON_CODE_IDLING_START = 8;
	    const byte M_REASON_CODE_IDLING_END = 16;
	    const byte M_REASON_CODE_IDLING_ONGOING = 32;
	    const byte M_REASON_CODE_ALARM = 64;
	    const byte M_REASON_CODE_SPEED_OVER_THRESHOLD = 128;
	    /* Constants for BYTE M_OFFSET_REASON_CODE_MSB ( Reason Code Most Significant Byte ) */
	    const byte M_REASON_CODE_TOWING_ALARM = 1;
	    const byte M_REASON_CODE_UNAUTHORISED_DRIVER = 2;
	    const byte M_REASON_CODE_COLLISION = 4;
	    const byte M_REASON_CODE_ACCEL_MAX = 8;
	    const byte M_REASON_CODE_CORNERING_MAX = 16;
	    const byte M_REASON_CODE_DECEL_MAX = 32;
	    const byte M_REASON_CODE_GPS_REACQUIRED = 64;
	    const byte M_REASON_CODE_CANBUS_EVENT = 128;
	    /* Constants for Status byte ( M_OFFSET_STATUS Least Significant Byte ) */
	    const byte M_STATUS_IGNITION_ON = 1;
	    const byte M_STATUS_POWER_ON_OR_RESTART = 2;
	    const byte M_STATUS_GPS_TIMEOUT = 4;
	    const byte M_STATUS_NETWORK_ROAMING = 8;
	    const byte M_STATUS_REPORTS_TO_FOLLOW = 16;
	    const byte M_STATUS_STORED_REPORT = 32;
	    const byte M_STATUS_EXTENSION = 64;
	    const byte M_STATUS_SERIAL_DEVICE_READ_ERROR = 128;
	    /* Constants for Status byte ( M_OFFSET_STATUS Most Significant Byte ) */
	    const byte M_STATUS_EXTRA_DATA = 1;
	    const byte M_STATUS_NETWORK_LOCATION = 2;
	    const byte M_STATUS_CANBUS_INTERFACE_FMS = 4;
	    const byte M_STATUS_CANBUS_INTERFACE_OBD = 8;

	    String[] CanbusStatus = {
	    	"Brake released",
	    	"Brake depressed",
	    	"Cruise control ON",
	    	"Cruise control OFF",
	    	"PTO off/disabled",
	    	"PTO set",
	    	"PTO not available",
	    	"Vehicle direction forward",
	    	"Vehicle direction reverse",
	    	"Vehicle speed overspeed",
	    	"Vehicle speed no overspeed"
	    };

	    String[] CanbusMonitoringStatus = {
	    	"RPM threshold reached",
	    	"RPM threshold ongoing",
	    	"RPM threshold cleared",
	    	"Throttle limit reached",
	    	"Throttle limit ongoing",
	    	"Throttle limit cleared",
	    	"Engine load threshold reached",
	    	"Engine load threshold ongoing",
	    	"Engine load threshold cleared"
	    };
	
	    // Class Level Data.
	    Byte[] m_bytReportData;
	    bool m_blnDataLengthCorrect;
	
	    // Constructor. Only one. We need the packet data.
	    public AstraReport(ArraySegment<byte> bytPacketData, byte protocol)
	    {
	        int intCounter = 0;
	        bool lenCorrect = false;

	        if (protocol == AstraConstants.PROTOCOL_M)
			{
		        if ((bytPacketData.Count == AstraConstants.M_BASIC_REPORT_LEN) ||
	        	    (bytPacketData.Count == AstraConstants.M_START_REPORT_LEN) ||
	        	    (bytPacketData.Count == AstraConstants.M_STOP_REPORT_LEN))
		        {
		        	lenCorrect = true;
		        }
	        }

	        if (protocol == AstraConstants.PROTOCOL_N)
	        {
		        if ((bytPacketData.Count == AstraConstants.N_BASIC_REPORT_LEN) ||
	        	    (bytPacketData.Count == AstraConstants.N_START_REPORT_LEN) ||
	        	    (bytPacketData.Count == AstraConstants.N_STOP_REPORT_LEN))
		        {
		        	lenCorrect = true;
		        }
	        }

	        if (protocol == AstraConstants.PROTOCOL_V)
	        {
	        	SetProtocolVOffsets();

		        if ((bytPacketData.Count == AstraConstants.V_BASIC_REPORT_LEN) ||
	        	    (bytPacketData.Count == AstraConstants.V_START_REPORT_LEN) ||
	        	    (bytPacketData.Count == AstraConstants.V_STOP_REPORT_LEN))
		        {
		        	lenCorrect = true;
		        }
	        }

	        if (lenCorrect)
	        {
	            for (intCounter = bytPacketData.Offset; intCounter <= bytPacketData.Count + bytPacketData.Offset; intCounter++)
	            {
	                Array.Resize<Byte>(ref m_bytReportData, (null == m_bytReportData) ? 1 : m_bytReportData.Length + 1);
	                m_bytReportData[m_bytReportData.Length - 1] = bytPacketData.Array[intCounter];
	            }
	
	            m_blnDataLengthCorrect = true;
	        }
	    }
	
	    private bool IsValidReport()
	    {
	        return m_blnDataLengthCorrect; 
	    }
	
	    public byte GetPacketSequenceNumber()
	    {
	        // The Packet Sequence number rolls over at 255 and is stored in byte M_OFFSET_PACKET_SQN of the packet;
	        byte uintRet = 0;
	        if (this.IsValidReport())
	        {
	            // Just read and return the byte
	            uintRet = m_bytReportData[M_OFFSET_PACKET_SQN];
	        }
	        return uintRet;
	    }
	
	    public double GetLatitude()
	    {
	        // The Lattitude is stored in bytes M_OFFSET_LAT_START to M_OFFSET_LAT_END and is the degrees * 1,000,000
	        double dblRet = 0;
			Int32 iLatitude = BuildInt32(M_OFFSET_LAT_START, M_OFFSET_LAT_END);
	        double dblLatitude = Convert.ToDouble(iLatitude);
	        dblRet = dblLatitude / (double)1000000.0;
	        return dblRet;
	    }
	    public double GetLongitude()
	    {
	        // The Longitude is stored in bytes M_OFFSET_LONG_START to M_OFFSET_LONG_END and is the degrees * 1,000,000
	        double dblRet = 0;
			Int32 iLongitude = BuildInt32(M_OFFSET_LONG_START, M_OFFSET_LONG_END);
	        double dblLongitude = Convert.ToDouble(iLongitude);
	        dblRet = dblLongitude / (double)1000000.0;
	        return dblRet;
	    }
	
	    public String GetTime()
	    {
	        // The date / time is recorded in seconds since the 6th of January 1980 at 00:00:00 
	        // That number of second is stored in the bytes M_OFFSET_TIME_START to M_OFFSET_TIME_END
	
	        // Initilise the DateTime to Jan 6th 1980 at midnight
	        DateTime datBaseDate = new DateTime(1980, 1, 6, 0, 0, 0);
	        ulong ulongSecondsSince = 0;
	        if (this.IsValidReport())
	        {
	            ulongSecondsSince = BuildLong(M_OFFSET_TIME_START, M_OFFSET_TIME_END);
	            datBaseDate = datBaseDate.AddSeconds(ulongSecondsSince);
	            // I have chosen to reutrn the formatted string here
	            // You can of course return the DateTime and format it when you use it.
	        }
	        return datBaseDate.ToString("dd MMM yyyy @ HH:mm:ss");
	    }
        public DateTime GetDate()
        {
            // The date / time is recorded in seconds since the 6th of January 1980 at 00:00:00 
            // That number of second is stored in the bytes M_OFFSET_TIME_START to M_OFFSET_TIME_END

            // Initilise the DateTime to Jan 6th 1980 at midnight
            DateTime datBaseDate = new DateTime(1980, 1, 6, 0, 0, 0);
            ulong ulongSecondsSince = 0;
            if (this.IsValidReport())
            {
                ulongSecondsSince = BuildLong(M_OFFSET_TIME_START, M_OFFSET_TIME_END);
                datBaseDate = datBaseDate.AddSeconds(ulongSecondsSince);
                // I have chosen to reutrn the formatted string here
                // You can of course return the DateTime and format it when you use it.
            }
            return datBaseDate;
        }
	    public String GetRTCTime()
	    {
	        // The date / time is recorded in seconds since the 6th of January 1980 at 00:00:00 
	        // That number of second is stored in the bytes M_OFFSET_EVENT_TIME_START to M_OFFSET_EVENT_TIME_END
	
	        // Initilise the DateTime to Jan 6th 1980 at midnight
	        DateTime datBaseDate = new DateTime(1980, 1, 6, 0, 0, 0);
	        ulong ulongSecondsSince = 0;
	        if (this.IsValidReport())
	        {
	            ulongSecondsSince = BuildLong(M_OFFSET_EVENT_TIME_START, M_OFFSET_EVENT_TIME_END);
	            datBaseDate = datBaseDate.AddSeconds(ulongSecondsSince);
	            // I have chosen to reutrn the formatted string here
	            // You can of course return the DateTime and format it when you use it.
	        }
	        return datBaseDate.ToString("dd MMM yyyy @ HH:mm:ss");
	    }
	
	    public int GetSpeedInKmh()
	    {
	        // The speed is stored in byte M_OFFSET_SPEED and is in Km/h divided by 2
	        int intRet = 0;
	
	        if (this.IsValidReport())
	        {
	            intRet = m_bytReportData[M_OFFSET_SPEED];
	            intRet *= 2;
	        }
	
	        return intRet;
	    }
	    public int GetHeadingInDegrees()
	    {
	        // The heading is stored in byte M_OFFSET_HEADING and is in degrees divided by 2
	        int intRet = 0;
	
	        if (this.IsValidReport())
	        {
	            intRet = m_bytReportData[M_OFFSET_HEADING];
	            intRet *= 2;
	        }
	
	        return intRet;
	    }
	    public int GetAltitudeInMeters()
	    {
	        // The altitude is stored in byte M_OFFSET_ALTITUDE and is in meters divided by 20
	        int intRet = 0;
	
	        if (this.IsValidReport())
	        {
	            intRet = m_bytReportData[M_OFFSET_ALTITUDE];
	            intRet *= 20;
	        }
	
	        return intRet;
	    }
	
	    public bool Reason_TimedIntervalElapsed()
	    {
	        // This bit is bit 0 of Reason Code LSB
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_REASON_CODE_LSB] & M_REASON_CODE_TIME_INT_ELAPSED) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Reason_DistanceTravelledExceeded()
	    {
	        // This bit is bit 1 of Reason Code LSB 
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_REASON_CODE_LSB] & M_REASON_CODE_DIST_TRAVELLED_EXCEEDED) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Reason_PosOnDemand()
	    {
	        // This bit is bit 2 of Reason Code LSB 
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_REASON_CODE_LSB] & M_REASON_CODE_POS_ON_DEMAND) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Reason_GeoFence()
	    {
	        // This bit is bit 3 of Reason Code LSB 
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_REASON_CODE_LSB] & M_REASON_CODE_GEO_FENCE) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Reason_PanicSwitchActivated()
	    {
	        // This bit is bit 4 of Reason Code LSB 
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_REASON_CODE_LSB] & M_REASON_CODE_PANIC_SWITCH_ACTIVATED) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Reason_ExtInputEvent()
	    {
	        // This bit is bit 5 of Reason Code LSB 
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_REASON_CODE_LSB] & M_REASON_CODE_EXT_INPUT_EVENT) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Reason_JourneyStart()
	    {
	        // This bit is bit 6 of Reason Code LSB 
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_REASON_CODE_LSB] & M_REASON_CODE_JOURNEY_START) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Reason_JourneyStop()
	    {
	        // This bit is bit 7 of Reason Code LSB 
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_REASON_CODE_LSB] & M_REASON_CODE_JOURNEY_STOP) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Reason_HeadingChange()
	    {
	        // This bit is bit 0 of Reason Code 2nd byte
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_REASON_CODE_2ND_BYTE] & M_REASON_CODE_HEADING_CHANGE) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Reason_LowBattery()
	    {
	        // This bit is bit 1 of Reason Code 2nd byte
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_REASON_CODE_2ND_BYTE] & M_REASON_CODE_LOW_BATTERY) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Reason_ExtPowerEvent()
	    {
	        // This bit is bit 2 of Reason Code 2nd byte
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_REASON_CODE_2ND_BYTE] & M_REASON_CODE_EXT_POWER_EVENT) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Reason_IdlingStarting()
	    {
	        // This bit is bit 3 of Reason Code 2nd byte
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_REASON_CODE_2ND_BYTE] & M_REASON_CODE_IDLING_START) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Reason_IdlingEnd()
	    {
	        // This bit is bit 4 of Reason Code 2nd byte
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_REASON_CODE_2ND_BYTE] & M_REASON_CODE_IDLING_END) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Reason_IdlingOnGoing()
	    {
	        // This bit is bit 5 of Reason Code 2nd byte
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_REASON_CODE_2ND_BYTE] & M_REASON_CODE_IDLING_ONGOING) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Reason_Alarm()
	    {
	        // This bit is bit 6 of Reason Code 2nd byte
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_REASON_CODE_2ND_BYTE] & M_REASON_CODE_ALARM) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Reason_SpeedOverThreshold()
	    {
	        // This bit is bit 7 of Reason Code 2nd byte
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_REASON_CODE_2ND_BYTE] & M_REASON_CODE_SPEED_OVER_THRESHOLD) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Reason_TowingAlarm()
	    {
	        // This bit is bit 0 of Reason Code MSB
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_REASON_CODE_MSB] & M_REASON_CODE_TOWING_ALARM) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Reason_UnauthorisedDriverAlarm()
	    {
	        // This bit is bit 1 of Reason Code MSB
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_REASON_CODE_MSB] & M_REASON_CODE_UNAUTHORISED_DRIVER) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Reason_CollisionAlarm()
	    {
	        // This bit is bit 2 of Reason Code MSB
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_REASON_CODE_MSB] & M_REASON_CODE_COLLISION) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Reason_AccelThresholdEvent()
	    {
	        // This bit is bit 3 of Reason Code MSB
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_REASON_CODE_MSB] & M_REASON_CODE_ACCEL_MAX) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Reason_CorneringThresholdEvent()
	    {
	        // This bit is bit 4 of Reason Code MSB
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_REASON_CODE_MSB] & M_REASON_CODE_CORNERING_MAX) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Reason_DecelThresholdEvent()
	    {
	        // This bit is bit 5 of Reason Code MSB
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_REASON_CODE_MSB] & M_REASON_CODE_DECEL_MAX) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Reason_GPSReacquired()
	    {
	        // This bit is bit 6 of Reason Code MSB
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_REASON_CODE_MSB] & M_REASON_CODE_GPS_REACQUIRED) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Reason_CanbusEvent()
	    {
	        // This bit is bit 7 of Reason Code MSB
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_REASON_CODE_MSB] & M_REASON_CODE_CANBUS_EVENT) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	
	    /* Status Byte Decoding */
	    public bool Status_IgnitionOn()
	    {
	        // This bit is bit 0 of Status LSB
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_STATUS_LSB] & M_STATUS_IGNITION_ON) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Status_PowerOnOrRestart()
	    {
	        // This bit is bit 1 of Status LSB
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_STATUS_LSB] & M_STATUS_POWER_ON_OR_RESTART) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Status_GPSTimeOut()
	    {
	        // This bit is bit 2 of Status LSB
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_STATUS_LSB] & M_STATUS_GPS_TIMEOUT) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Status_NetworkRoaming()
	    {
	        // This bit is bit 3 of Status LSB
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_STATUS_LSB] & M_STATUS_NETWORK_ROAMING) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Status_ReportsToFollow()
	    {
	        // This bit is bit 4 of Status LSB
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_STATUS_LSB] & M_STATUS_REPORTS_TO_FOLLOW) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Status_StoredReport()
	    {
	        // This bit is bit 5 of Status LSB
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_STATUS_LSB] & M_STATUS_STORED_REPORT) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Status_Extension()
	    {
	        // This bit is bit 6 of Status LSB
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_STATUS_LSB] & M_STATUS_EXTENSION) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Status_SerialDeviceReadError()
	    {
	        // This bit is bit 7 of Status LSB
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_STATUS_LSB] & M_STATUS_SERIAL_DEVICE_READ_ERROR) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Status_ExtraData()
	    {
	        // This bit is bit 0 of Status MSB
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_STATUS_MSB] & M_STATUS_EXTRA_DATA) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Status_NetworkLocationData()
	    {
	        // This bit is bit 1 of Status MSB
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_STATUS_MSB] & M_STATUS_NETWORK_LOCATION) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Status_CanbusInterfaceFMS()
	    {
	        // This is bit 2 of Status MSB
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_STATUS_MSB] & M_STATUS_CANBUS_INTERFACE_FMS) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public bool Status_CanbusInterfaceOBD()
	    {
	        // This is bit 3 of Status MSB
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_STATUS_MSB] & M_STATUS_CANBUS_INTERFACE_OBD) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }

        //Reza
        public String GetDriverID()
        {
            String sDriverID = "None";
            if (Status_ExtraData())
            {
                byte M_STATUS_EXTRA_DriverID = 5;
                if ((m_bytReportData[M_OFFSET_STATUS_MSB] & M_STATUS_EXTRA_DATA & M_STATUS_EXTRA_DriverID) > 0)
                {
                    //Byte[] bDriver = new Byte[1];
                    //Array.Copy(m_bytReportData, 72, bDriver, 0, 1);
                    //String sFamily = Utils.BytesToDoubleHex(bDriver);

                    Byte[] bDriver = new Byte[6];
                    Array.Copy(m_bytReportData, 73, bDriver, 0, 6);
                    sDriverID = Utils.BytesToDoubleHex(bDriver);

                    //String g = String.Empty;
                    //try
                    //{
                    //    for (int i = 0; 1 < m_bytReportData.Length - 7; i++)
                    //    {
                    //        bDriver = new Byte[6];
                    //        Array.Copy(m_bytReportData, i, bDriver, 0, 6);
                    //        g += i.ToString() + "\t" + Utils.BytesToDoubleHex(bDriver) + "\r\n";
                    //    }
                    //}
                    //catch { }
                    //finally
                    //{
                    //    System.IO.File.AppendAllText(GlobalVariables.logPath() + "Driv.txt", g);
                    //}
                }
            }
            return sDriverID;
        }
        public int GetExtraOdom()
        {
            int i = 0;
            if (Status_ExtraData())
                i = BuildInt32(45, 47);

            return i;
        }
        public int GetExtraHrs()
        {
            int i = 0;
            if (Status_ExtraData())
                i = BuildInt32(48, 49);

            return i;
        }

	    public bool GeoFenceEntred()
	    {
	        /* The GeoFence Byte is byte M_OFFSET_GEO_FENCE_EVENT and has the format 
	         * Bit 7 = Geo Fence Entered if 1 or 0 if "Left Geo Fence" 
	         * Bit 0..6 Geo Fence Index that was entered or left. Values > 0 are valid indexes
	         */
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	            if ((m_bytReportData[M_OFFSET_GEO_FENCE_EVENT] & 0x80) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }
	    public byte GetGeoFenceIndex()
	    {
	        // The Geofence Byte is stored at location M_OFFSET_GEO_FENCE_EVENT. See bool GeoFenceEntred for full description.
	        // The low order 7 bits store the GeoFence index that was affected. Value 127 = congestion zone
	        Byte bytGeoFenceIndex = 0;
	        if (this.IsValidReport())
	        {
	            // GeoFence Index 0 is represented as value 1.
	            // Except for Fence 127 where the Congestion Zone is indicated.
	            bytGeoFenceIndex = (byte)(m_bytReportData[M_OFFSET_GEO_FENCE_EVENT] & 0x7f);
	        }
	
	        return bytGeoFenceIndex;
	    }
	    public bool GeoFenceCongestionZone()
	    {
	        // Just test GeoFenceIndex for == 127 to determine if this is a congestion zone event. 
	        // Note we need not test the status of the data here as the function that is called does that.
	        bool blnReturn = false;
	        if ((this.GetGeoFenceIndex() & 0x7f) == 0x7f)
	        {
	            blnReturn = true;
	        }
	        return blnReturn;
	    }
	
	    public int DigitalStatus()
	    {
	        // The digital input and output statuses are stored at byte M_OFFSET_DIGITALS_1 bits 0-7 and
	        // at byte M_OFFSET_DIGITALS_3 bits 0-2.
	        int bytDigitalStatus = 0;
	        if (this.IsValidReport())
	        {
	            bytDigitalStatus = m_bytReportData[M_OFFSET_DIGITALS_1];
	            bytDigitalStatus |= (m_bytReportData[M_OFFSET_DIGITALS_3] & 0x07) << 16;
	        }
	        return bytDigitalStatus;
	    }
	    public int DigitalChangeStatus()
	    {
	        // The change in digital inputs and outputs are stored in byte M_OFFSET_DIGITALS_2 bits 0-7 and
	        // at byte M_OFFSET_DIGITALS_3 bits 3-5.
	        int bytDigitalChange = 0;
	        if (this.IsValidReport())
	        {
	            bytDigitalChange = m_bytReportData[M_OFFSET_DIGITALS_2] << 8;
	            bytDigitalChange |= (m_bytReportData[M_OFFSET_DIGITALS_3] & 0x38) << 16;
	        }
	        return bytDigitalChange;
	    }
	
	    public byte GetADC1()
	    {
	        // ADC 1 is byte M_OFFSET_ADC_1 of the packet
	        byte bytOut = 0;
	        if (this.IsValidReport())
	        {
	        	bytOut = m_bytReportData[M_OFFSET_ADC_1];
	        }
	        return bytOut;
	    }
	    public byte GetADC2()
	    {
	        // ADC 2 is byte M_OFFSET_ADC_2 of the packet
	        byte bytOut = 0;
	        if (this.IsValidReport())
	        {
	        	bytOut = m_bytReportData[M_OFFSET_ADC_2];
	        }
	        return bytOut;
	    }
	
	    public int Get12BitADC1()
	    {
	        // ADC 1 bits 7-0 are byte M_OFFSET_ADC_1 of the packet
	        // ADC 1 bits 11-8 are low nibble of M_OFFSET_ADC_2
	        int adcOut = 0;
	        if (this.IsValidReport())
	        {
	        	adcOut = m_bytReportData[M_OFFSET_ADC_1];
	        	adcOut += ((m_bytReportData[M_OFFSET_ADC_2] & 0x0F) << 8);
	        }
	        return adcOut;
	    }
	    public int Get12BitADC2()
	    {
	        // ADC 2 bits 3-0 are high nibble of M_OFFSET_ADC_2 of the packet
	        // ADC 2 bits 11-4 are byte M_OFFSET_ADC_3
	        int adcOut = 0;
	        if (this.IsValidReport())
	        {
	        	adcOut = ((m_bytReportData[M_OFFSET_ADC_2] & 0xF0) >> 4);
	        	adcOut += (m_bytReportData[M_OFFSET_ADC_3] << 4);
	        }
	        return adcOut;
	    }
	
	    public byte GetBatteryPercentage()
	    {
	        // Battery Percentage is stored in byte M_OFFSET_BATTERY.
	        byte bytOut = 0;
	        if (this.IsValidReport())
	        {
	            bytOut = m_bytReportData[M_OFFSET_BATTERY];
	        }
	        return bytOut;
	    }
	    public float GetExternalInputVoltage()
	    {
	        // The exteral voltage stored in byte M_OFFSET_EXTERNAL_INPUT_VOLTAGE needs to be * 0.2 to get the correct value.
	        float sngOut = 0;
	        if (this.IsValidReport())
	        {
	            sngOut = ((float)m_bytReportData[M_OFFSET_EXTERNAL_INPUT_VOLTAGE]) * (float)0.2;
	        }
	
	        return sngOut;
	    }
	
	    public int GetMaxJourneySpeed()
	    {
	        // The Maximum Journey Speed is in km/h and is divided by 2. Stored in byte M_OFFSET_MAX_JOURNEY_SPEED
	        int intOut = 0;
	        if (this.IsValidReport())
	        {
	            intOut = m_bytReportData[M_OFFSET_MAX_JOURNEY_SPEED] * 2;
	        }
	        return intOut;
	    }
	    public int GetXAxisMaxDeceleration()
	    {
	        // The Maximum X Axis deceleration is in ms2 and is stored in byte M_OFFSET_MAX_X_DECELERATION
	        int intOut = 0;
	        if (this.IsValidReport())
	        {
	            intOut = m_bytReportData[M_OFFSET_MAX_X_DECELERATION];
	        }
	        return intOut;
	    }
	    public int GetXAxisMaxAcceleration()
	    {
	        // The Maximum X Axis acceleration is in ms2 and is stored in byte M_OFFSET_MAX_X_ACCELERATION
	        int intOut = 0;
	        if (this.IsValidReport())
	        {
	            intOut = m_bytReportData[M_OFFSET_MAX_X_ACCELERATION];
	        }
	        return intOut;
	    }
	    public int GetYAxisMaxDeceleration()
	    {
	        // The Maximum Y Axis deceleration is in ms2 and is stored in byte M_OFFSET_MAX_Y_DECELERATION
	        int intOut = 0;
	        if (this.IsValidReport())
	        {
	            intOut = m_bytReportData[M_OFFSET_MAX_Y_DECELERATION];
	        }
	        return intOut;
	    }
	    public int GetYAxisMaxAcceleration()
	    {
	        // The Maximum Y Axis acceleration is in ms2 and is stored in byte M_OFFSET_MAX_Y_ACCELERATION
	        int intOut = 0;
	        if (this.IsValidReport())
	        {
	            intOut = m_bytReportData[M_OFFSET_MAX_Y_ACCELERATION];
	        }
	        return intOut;
	    }
	    public int GetZAxisMaxDeceleration()
	    {
	        // The Maximum Z Axis deceleration is in ms2 and is stored in byte M_OFFSET_MAX_Z_DECELERATION
	        int intOut = 0;
	        if (this.IsValidReport())
	        {
	            intOut = m_bytReportData[M_OFFSET_MAX_Z_DECELERATION];
	        }
	        return intOut;
	    }
	    public int GetZAxisMaxAcceleration()
	    {
	        // The Maximum Z Axis acceleration is in ms2 and is stored in byte M_OFFSET_MAX_Z_ACCELERATION
	        int intOut = 0;
	        if (this.IsValidReport())
	        {
	            intOut = m_bytReportData[M_OFFSET_MAX_Z_ACCELERATION];
	        }
	        return intOut;
	    }
	
        public float GetJourneyDistanceTravelled()
	    {
	        // The total distance travelled in this journey is stored in km and is multiplied by 10. Mul by 0.1 to get it back. 
	        // Stored in bytes  M_OFFSET_JOURNEY_DIST_TRAVELLED_START and M_OFFSET_JOURNEY_DIST_TRAVELLED_END
	        float sngOut = 0;
	        if (this.IsValidReport())
	        {
	            sngOut = ((float)BuildLong(M_OFFSET_JOURNEY_DIST_TRAVELLED_START, M_OFFSET_JOURNEY_DIST_TRAVELLED_END)) * (float)0.1;
	        }
	
	        return sngOut;
	    }
	    public int GetJourneyIdleSeconds()
	    {
	        // How many seconds were Idle Seconds during the journey? The information is stored between bytes
	        // M_OFFSET_JOURNEY_IDLE_SECONDS_START and M_OFFSET_JOURNEY_IDLE_SECONDS_END
	        int intOut = 0;
	        if (this.IsValidReport())
	        {
	            intOut = (int)BuildLong(M_OFFSET_JOURNEY_IDLE_SECONDS_START, M_OFFSET_JOURNEY_IDLE_SECONDS_END);
	
	        }
	        return intOut;
	    }
	    public int GetSatellitesInUse()
	    {
	        // The number of GPS satellites in use stored at byte M_OFFSET_SIGNAL_QUALITY bits 0-3.
	        int intSatellites = 0;
	        if (this.IsValidReport())
	        {
	            intSatellites = m_bytReportData[M_OFFSET_SIGNAL_QUALITY] & 0x0F;
	        }
	        return intSatellites;
	    }
	    public int GetGSMSignalStrength()
	    {
	    	// Get the GSM signal strength in the range 0-15
	        // The GSM signal strength is stored at byte M_OFFSET_SIGNAL_QUALITY bits 4-7.
	        int intGSMStrength = 0;
	        if (this.IsValidReport())
	        {
	            intGSMStrength = m_bytReportData[M_OFFSET_SIGNAL_QUALITY] & 0xF0;
	            intGSMStrength >>= 4;
	        }
	        return intGSMStrength;
	    }
	
	    public int GetCanWheelSpeed()
	    {
	        // Get the wheel based speed in the range 0-250 km/h
	        int wheelSpeed = 0;
	        if (this.IsValidReport())
	        {
	            wheelSpeed = m_bytReportData[M_OFFSET_CAN_WHEEL_SPEED];
	        }
	        return wheelSpeed;
	    }
	    public int GetCanEngineSpeed()
	    {
	        // Get the engine speed in the range 0-8000 RPM (it is stored divided by 32)
	        int engineSpeed = 0;
	        if (this.IsValidReport())
	        {
	            engineSpeed = m_bytReportData[M_OFFSET_CAN_ENGINE_SPEED];
	        }
	        return (engineSpeed * 32);
	    }
	    public int GetCanAccelPos()
	    {
	        // Get the accelerator position as a percentage (it is stored in the range 0-250)
	        int accelPos = 0;
	        float accelPercent;
	        if (this.IsValidReport())
	        {
	        	accelPercent = ((float)m_bytReportData[M_OFFSET_CAN_ACCEL_POS]) / (float)2.5;
	        	accelPos = Convert.ToInt32(accelPercent);
	        }
	        return accelPos;
	    }
	    public int GetCanEngineLoad()
	    {
	        // Get the engine load as 0-125% of operating range
	        int engineLoad = 0;
	        if (this.IsValidReport())
	        {
	            engineLoad = m_bytReportData[M_OFFSET_CAN_ENGINE_LOAD];
	        }
	        return engineLoad;
	    }
	    public int GetCanAvgWheelSpeed()
	    {
	        // Get the average wheel based speed in the range 0-250 km/h
	        int wheelSpeed = 0;
	        if (this.IsValidReport())
	        {
	            wheelSpeed = m_bytReportData[M_OFFSET_CAN_AVG_WHEEL_SPEED];
	        }
	        return wheelSpeed;
	    }
	    public int GetCanAvgEngineSpeed()
	    {
	        // Get the average engine speed in the range 0-8000 RPM (it is stored divided by 32)
	        int engineSpeed = 0;
	        if (this.IsValidReport())
	        {
	            engineSpeed = m_bytReportData[M_OFFSET_CAN_AVG_ENGINE_SPEED];
	        }
	        return (engineSpeed * 32);
	    }
	    public int GetCanAvgAccelPos()
	    {
	        // Get the average accelerator position as a percentage (it is stored in the range 0-250)
	        int accelPos = 0;
	        float accelPercent;
	        if (this.IsValidReport())
	        {
	        	accelPercent = ((float)m_bytReportData[M_OFFSET_CAN_AVG_ACCEL_POS]) / (float)2.5;
	        	accelPos = Convert.ToInt32(accelPercent);
	        }
	        return accelPos;
	    }
	    public int GetCanAvgEngineLoad()
	    {
	        // Get the average engine load as 0-125% of operating range
	        int engineLoad = 0;
	        if (this.IsValidReport())
	        {
	            engineLoad = m_bytReportData[M_OFFSET_CAN_AVG_ENGINE_LOAD];
	        }
	        return engineLoad;
	    }
	    public float GetCanJourneyDistance()
	    {
	        // The distance travelled in this journey stored in km and multiplied by 10. Mul by 0.1 to get it back. 
	        float sngOut = 0;
	        if (this.IsValidReport())
	        {
	        	sngOut = ((float)BuildLong(M_OFFSET_CAN_JOURNEY_DIST, (M_OFFSET_CAN_JOURNEY_DIST + 1))) * (float)0.1;
	        }
	
	        return sngOut;
	    }
	    public int GetCanEngineTemp()
	    {
	        // Get the engine coolant temperature in the range -40 to +210 deg C (it is stored in range 0-250)
	        int engineTemp = 0;
	        if (this.IsValidReport())
	        {
	            engineTemp = m_bytReportData[M_OFFSET_CAN_ENGINE_TEMP];
	        }
	        return (engineTemp - 40);
	    }
	    public int GetCanStatus()
	    {
	        int intOut = 0;
	        if (this.IsValidReport())
	        {
	        	intOut = (int)BuildLong(M_OFFSET_CAN_STATUS, (M_OFFSET_CAN_STATUS + 1));
	        }
	        return intOut;
	    }

	    public int GetFMSCanStatus()
	    {
	        int intOut = 0;
	        if (this.IsValidReport())
	        {
	        	intOut = (int)BuildLong(M_OFFSET_CAN_FMS_STATUS, (M_OFFSET_CAN_FMS_STATUS + 1));
	        }
	        return intOut;
	    }

	    public int GetOBDCanStatus_CodeCount()
	    {
	        int intOut = 0;
	        if (this.IsValidReport())
	        {
	        	intOut = (int)BuildLong(M_OFFSET_CAN_OBD_STATUS, (M_OFFSET_CAN_OBD_STATUS + 1));
	        }
	        return (intOut & 0x7F);
	    }
	    public bool GetOBDCanStatus_MilOn()
	    {
	        bool blnRet = false;
	        if (this.IsValidReport())
	        {
	        	if ((m_bytReportData[M_OFFSET_CAN_OBD_STATUS + 1] & 0x80) > 0)
	            {
	                blnRet = true;
	            }
	        }
	        return blnRet;
	    }

	    public int GetCanMonitoringStatus()
	    {
	        int intOut = 0;
	        if (this.IsValidReport())
	        {
	        	intOut = (int)BuildLong(M_OFFSET_CAN_MONITORING_STATUS, (M_OFFSET_CAN_MONITORING_STATUS + 1));
	        }
	        return intOut;
	    }
	    public int GetCanFuelLevel()
	    {
	        // Get the fuel level as a percentage (it is stored in the range 0-250)
	        int fuelLevel = 0;
	        float fuelPercent;
	        if (this.IsValidReport())
	        {
	        	fuelPercent = ((float)m_bytReportData[M_OFFSET_CAN_FUEL_LEVEL]) / (float)2.5;
	        	fuelLevel = Convert.ToInt32(fuelPercent);
	        }
	        return fuelLevel;
	    }
	    public int GetCanAxleWeight()
	    {
	        // Get the sum of all individual axle weights stored in kg and multiplied by 2. Divide by 2 to get it back.
	        int intOut = 0;
	        if (this.IsValidReport())
	        {
	        	intOut = (int)BuildLong(M_OFFSET_CAN_AXLE_WEIGHT, (M_OFFSET_CAN_AXLE_WEIGHT + 1));
	        }
	        return (intOut / 2);
	    }
	    public int GetCanEngineHours()
	    {
	        // Get the engine total hours of operation
	        int intOut = 0;
	        if (this.IsValidReport())
	        {
	        	intOut = (int)BuildLong(M_OFFSET_CAN_ENGINE_HOURS, (M_OFFSET_CAN_ENGINE_HOURS + 3));
	        }
	        return intOut;
	    }
	    public float GetCanFuelUsedInJourney()
	    {
	        // Get the total fuel used during the latest journey
	        // The fuel used is stored in litres multiplied by 2. Mul by 0.5 to get it back to litres.
	        float sngOut = 0;
	        if (this.IsValidReport())
	        {
	        	sngOut = ((float)BuildLong(M_OFFSET_CAN_JOURNEY_TOTAL_FUEL, (M_OFFSET_CAN_JOURNEY_TOTAL_FUEL + 1))) * (float)0.5;
	        }
	
	        return sngOut;
	    }
	    public int GetCanJourneyFuelPer100Km()
	    {
	        // Get the fuel economy during the latest journey
	        int intOut = 0;
	        if (this.IsValidReport())
	        {
	        	intOut = (int)BuildLong(M_OFFSET_CAN_JOURNEY_LITRES_PER_100KM, (M_OFFSET_CAN_JOURNEY_LITRES_PER_100KM + 1));
	        }
	        return intOut;
	    }
	    public int GetCanJourneyCruiseControl()
	    {
	        // Get the time cruise control was active during the latest journey
	        int intOut = 0;
	        if (this.IsValidReport())
	        {
	        	intOut = (int)BuildLong(M_OFFSET_CAN_JOURNEY_CRUISE_CTRL, (M_OFFSET_CAN_JOURNEY_CRUISE_CTRL + 1));
	        }
	        return intOut;
		}
	    public int GetCanServiceDistance()
	    {
	        // Get the distance before the next scheduled service is due. Stored in km divided by 5 so multiply by 5.
	        int intOut = 0;
	        Int16 serviceDist;
	        if (this.IsValidReport())
	        {
	        	serviceDist = Convert.ToInt16(m_bytReportData[M_OFFSET_CAN_SERVICE_DISTANCE]);
	        	serviceDist <<= 8;
	        	serviceDist += Convert.ToInt16(m_bytReportData[M_OFFSET_CAN_SERVICE_DISTANCE + 1]);
	        	intOut = (int)serviceDist;
	        }
	        return (intOut * 5);
		}
	    public int GetCanRunTimeEngineStart()
	    {
	        // Get the time the engine has been running since the ignition was switched on
	        int intOut = 0;
	        if (this.IsValidReport())
	        {
	        	intOut = (int)BuildLong(M_OFFSET_CAN_TIME_SINCE_ENGINE_START, (M_OFFSET_CAN_TIME_SINCE_ENGINE_START + 1));
	        }
	        return intOut;
		}
	    public int GetCanDistanceMilOn()
	    {
	        // Get the distance travelled since the engine Malfunction Indicator Light has been on
	        int intOut = 0;
	        if (this.IsValidReport())
	        {
	        	intOut = (int)BuildLong(M_OFFSET_CAN_DISTANCE_MIL_ON, (M_OFFSET_CAN_DISTANCE_MIL_ON + 1));
	        }
	        return intOut;
		}
	    public int GetCanTotalFuelUsed()
	    {
	        int intOut = 0;
	        if (this.IsValidReport())
	        {
	        	intOut = (int)BuildLong(M_OFFSET_CAN_TOTAL_FUEL_USED, (M_OFFSET_CAN_TOTAL_FUEL_USED + 3));
	        }
	        return intOut;
	    }
	    public int GetCanTotalVehicleDistance()
	    {
	        int intOut = 0;
	        if (this.IsValidReport())
	        {
	        	intOut = (int)BuildLong(M_OFFSET_CAN_TOTAL_VEHICLE_DIST, (M_OFFSET_CAN_TOTAL_VEHICLE_DIST + 3));
	        }
	        return intOut;
	    }

	    public String GetStringCanStatus(int index)
	    {
	    	return (CanbusStatus[index]);
	    }
	    public int GetStringSizeCanStatus()
	    {
	    	return (CanbusStatus.Length);
	    }
	    public String GetStringCanMonitoringStatus(int index)
	    {
	    	return (CanbusMonitoringStatus[index]);
	    }
	    public int GetStringSizeCanMonitoringStatus()
	    {
	    	return (CanbusMonitoringStatus.Length);
	    }

	    private ulong BuildLong(int intStartByte, int intEndByte)
	    {
	        ulong lngOut = 0;
	        // Check if this is sane and within all the bounds!?
	        if ((intEndByte > intStartByte) && (intEndByte <= m_bytReportData.GetUpperBound(0))) 
	        {
	            // For each needed byte.
	            while (intStartByte < intEndByte)
	            {
	                // Add the byte
	                lngOut += m_bytReportData[intStartByte++];
	                // Shift it into place.
	                lngOut <<= 8;
	            }
	            // And add the last byte ( we don't have to shift after this
	            lngOut += m_bytReportData[intStartByte];
	        }
	        return lngOut;
	    }
	    private Int32 BuildInt32(int intStartByte, int intEndByte)
	    {
	        Int32 intOut = 0;
	        // Check if this is sane and within all the bounds!?
	        if ((intEndByte > intStartByte) && (intEndByte <= m_bytReportData.GetUpperBound(0))) 
	        {
	            // For each needed byte.
	            while (intStartByte < intEndByte)
	            {
	                // Add the byte
	                intOut += m_bytReportData[intStartByte++];
	                // Shift it into place.
	                intOut <<= 8;
	            }
	            // And add the last byte (we don't have to shift after this)
	            intOut += m_bytReportData[intStartByte];
	        }
	        return intOut;
	    }

        public String GetLogReasons()
        {
            String strResult = String.Empty;
            if (Reason_TimedIntervalElapsed() || Reason_HeadingChange() || Reason_DistanceTravelledExceeded())
                strResult += "TimeOrHeadOrDist(Heading)";
            if (Reason_JourneyStart())
                strResult += "JourneyStart(Ignition)";
            if (Reason_JourneyStop())
                strResult += "JourneyEnd (Ignition)";
            if (Reason_IdlingStarting() || Reason_IdlingEnd() || Reason_IdlingOnGoing())
                strResult += "Idling(Time)";
            if (Reason_AccelThresholdEvent())
                strResult += "Reason_AccelThresholdEvent(HarshAS)";
            if (Reason_DecelThresholdEvent())
                strResult += "Reason_DecelThresholdEvent(HarshBS)";
            if (Reason_CorneringThresholdEvent())
                strResult += "Reason_CorneringThresholdEvent(HarshCN)";

            if (Reason_PosOnDemand())
                strResult += "Reason_PosOnDemand";
            if (Reason_GeoFence())
                strResult += "Reason_GeoFence";
            if (Reason_PanicSwitchActivated())
            {
                strResult += "Reason_PanicSwitchActivated Aux2";
                //Aux 2
                String bAux = "Off";
                if (GetDigitalInput2())
                    bAux = "On";
                String sAux = " (Aux) (Aux2" + bAux + ")";
                strResult += sAux;
            }
            if (Reason_ExtInputEvent())
            {
                strResult += "Reason_ExtInputEvent";

                //Aux 1
                String bAux = "Off";
                if (GetDigitalInput1())
                    bAux = "On";
                String sAux = " (Aux) (Aux1" + bAux + ")";
                strResult += sAux;
            }
            if (Reason_LowBattery())
                strResult += "Reason_LowBattery";
            if (Reason_ExtPowerEvent())
                strResult += "Reason_ExtPowerEvent";
            if (Reason_Alarm())
                strResult += "Reason_Alarm";
            if (Reason_SpeedOverThreshold())
                strResult += "Reason_SpeedOverThreshold";
            if (Reason_TowingAlarm())
                strResult += "Reason_TowingAlarm";
            if (Reason_UnauthorisedDriverAlarm())
                strResult += "Reason_UnauthorisedDriverAlarm";
            if (Reason_CollisionAlarm())
                strResult += "Reason_CollisionAlarm";
            if (Reason_GPSReacquired())
                strResult += "Reason_GPSReacquired";
            if (Reason_CanbusEvent())
                strResult += "Reason_CanbusEvent";

            strResult += " >>> StatusCode >>>";
            if (Status_IgnitionOn())
                strResult += "(IgnOn)";
            else
                strResult += "(IgnOff)";
            if (Status_PowerOnOrRestart())
                strResult += "StatusPowerOn(Reset)";
            if (Status_GPSTimeOut())
                strResult += "StatusGPSTimeOut(GpsShutdown)";
            if (Status_NetworkRoaming())
                strResult += "StatusNetworkRoaming";
            if (Status_ReportsToFollow())
                strResult += "StatusReportsToFollow";
            if (Status_StoredReport())
                strResult += "StoredReport";
            if (Status_Extension())
                strResult += "StatusExtensionDataFieldPresent";
            if (Status_SerialDeviceReadError())
                strResult += "StatusSerialDeviceReadError";

            return strResult;
        }
        private String ToBinary(Int64 Decimal)
        {
            //Int64 DecimalValue = Convert.ToInt64(MyPacketData[30].ToString());
            //Int64 BinToDec = Convert.ToInt64(Convert.ToInt64("1010", 2));

            // Declare a few variables we're going to need
            Int64 BinaryHolder;
            char[] BinaryArray;
            String BinaryResult = "";

            while (Decimal > 0)
            {
                BinaryHolder = Decimal % 2;
                BinaryResult += BinaryHolder;
                Decimal = Decimal / 2;
            }

            // The algoritm gives us the binary number in reverse order (mirrored)
            // We store it in an array so that we can reverse it back to normal
            BinaryArray = BinaryResult.ToCharArray();
            Array.Reverse(BinaryArray);
            BinaryResult = new string(BinaryArray);

            while (BinaryResult.Length < 8)
                BinaryResult = "0" + BinaryResult;

            return BinaryResult;
        }
        private Boolean GetBitValue(int Offset_Code, int pos)
        {
            Boolean bResult = false;
            String ReasonCode = ToBinary((Int64)m_bytReportData[Offset_Code]);
            if (ReasonCode.Substring(pos, 1) == "1")
                bResult = true;
            return bResult;
        }
        public Boolean GetDigitalInput1()
        {
            return GetBitValue(M_OFFSET_DIGITALS_1, 7);
        }
        public Boolean GetDigitalInput2()
        {
            return GetBitValue(M_OFFSET_DIGITALS_1, 6);
        }
        public Boolean GetDigitalInput3()
        {
            return GetBitValue(M_OFFSET_DIGITALS_1, 5);
        }
        public Boolean GetDigitalInput4()
        {
            return GetBitValue(M_OFFSET_DIGITALS_1, 4);
        }
        public Boolean GetDigitalInput5()
        {
            return GetBitValue(M_OFFSET_DIGITALS_1, 3);
        }
        public Boolean GetDigitalInput6()
        {
            return GetBitValue(M_OFFSET_DIGITALS_1, 2);
        }
	    
	    private void SetProtocolVOffsets()
	    {
		    M_OFFSET_PACKET_SQN = 0;
		    M_OFFSET_EVENT_TIME_START = 1;
		    M_OFFSET_EVENT_TIME_END = 4;
		    M_OFFSET_LAT_START = 5; 
		    M_OFFSET_LAT_END = 8; 
		    M_OFFSET_LONG_START = 9; 
		    M_OFFSET_LONG_END = 12;
		    M_OFFSET_TIME_START = 13;
		    M_OFFSET_TIME_END = 16; 
		    M_OFFSET_SPEED = 17;
		    M_OFFSET_HEADING = 18;
		    M_OFFSET_REASON_CODE_MSB = 19;
		    M_OFFSET_REASON_CODE_2ND_BYTE = 20;
		    M_OFFSET_REASON_CODE_LSB = 21;
		    M_OFFSET_STATUS_MSB = 22;
		    M_OFFSET_STATUS_LSB = 23;
		    M_OFFSET_DIGITALS_1 = 24;
		    M_OFFSET_DIGITALS_2 = 25;
		    M_OFFSET_DIGITALS_3 = 26;
		    M_OFFSET_ADC_1 = 27; 
		    M_OFFSET_ADC_2 = 28; 
		    M_OFFSET_ADC_3 = 29; 
		    M_OFFSET_BATTERY = 30;
		    M_OFFSET_EXTERNAL_INPUT_VOLTAGE = 31;
		    M_OFFSET_MAX_JOURNEY_SPEED = 32;
		    M_OFFSET_MAX_X_ACCELERATION = 33;
		    M_OFFSET_MAX_X_DECELERATION = 34; 
		    M_OFFSET_MAX_Y_ACCELERATION = 35;
		    M_OFFSET_MAX_Y_DECELERATION = 36; 
		    M_OFFSET_MAX_Z_ACCELERATION = 37;
		    M_OFFSET_MAX_Z_DECELERATION = 38; 
		    M_OFFSET_JOURNEY_DIST_TRAVELLED_START = 39;
		    M_OFFSET_JOURNEY_DIST_TRAVELLED_END = 40;
		    M_OFFSET_JOURNEY_IDLE_SECONDS_START = 41; 
		    M_OFFSET_JOURNEY_IDLE_SECONDS_END = 42;
		    M_OFFSET_ALTITUDE = 43;
		    M_OFFSET_SIGNAL_QUALITY = 44;
		    M_OFFSET_GEO_FENCE_EVENT = 45;
		    M_OFFSET_CAN_WHEEL_SPEED = 46;
		    M_OFFSET_CAN_ENGINE_SPEED = 47;
		    M_OFFSET_CAN_ACCEL_POS = 48;
		    M_OFFSET_CAN_ENGINE_LOAD = 49;
		    M_OFFSET_CAN_JOURNEY_DIST = 50;
		    M_OFFSET_CAN_ENGINE_TEMP = 52;
		    M_OFFSET_CAN_FMS_STATUS = 53;
		    M_OFFSET_CAN_OBD_STATUS = 55;
		    M_OFFSET_CAN_MONITORING_STATUS = 57;
		    M_OFFSET_CAN_FUEL_LEVEL = 59;
		    M_OFFSET_CAN_TOTAL_FUEL_USED = 60;
		    M_OFFSET_CAN_TOTAL_VEHICLE_DIST = 64;
		    M_OFFSET_CAN_AVG_WHEEL_SPEED = 68;
		    M_OFFSET_CAN_AVG_ENGINE_SPEED = 69;
		    M_OFFSET_CAN_AVG_ACCEL_POS = 70;
		    M_OFFSET_CAN_AVG_ENGINE_LOAD = 71;
		    M_OFFSET_CAN_AXLE_WEIGHT = 84;
		    M_OFFSET_CAN_ENGINE_HOURS = 86;
		    M_OFFSET_CAN_JOURNEY_LITRES_PER_100KM = 88;
		    M_OFFSET_CAN_JOURNEY_CRUISE_CTRL = 90;
		    M_OFFSET_CAN_SERVICE_DISTANCE = 92;
		    M_OFFSET_CAN_TIME_SINCE_ENGINE_START = 94;
		    M_OFFSET_CAN_DISTANCE_MIL_ON = 96;
	    }
	}
}
