﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace NavLib.AstraTelematics.ProtokolMNV
{
	public class AstraHeader
	{
	    // Constants for the header.
	    // The Expected Value
	    const int M_ASTRA_LEN_HEADER = 9; 
	
	    // The offsets into the data structure.
	    const int M_OFFSET_PROTOCOL = 0; 
	    const int M_OFFSET_LEN_START = 1;
	    const int M_OFFSET_LEN_END = 2;
	    const int M_OFFSET_TACFAC_START = 3;
	    const int M_OFFSET_TACFAC_END = 6;
	    const int M_OFFSET_MSN_START = 7;
	    const int M_OFFSET_MSN_END = 9;
	
	    Byte[] m_bytHeaderData;
	    bool m_blnHeaderValid = false; 
	
	    public AstraHeader(ArraySegment<byte> bytHeaderData)
	    {
	        int intCounter = 0; 
	
            for (intCounter = bytHeaderData.Offset; intCounter <= bytHeaderData.Count; intCounter++)
            {
                Array.Resize<Byte>(ref m_bytHeaderData, (null == m_bytHeaderData) ? 1 : m_bytHeaderData.Length + 1);
                m_bytHeaderData[m_bytHeaderData.Length-1] = bytHeaderData.Array[intCounter];
            }

            m_blnHeaderValid = true;
	    }
	
	    // Private Function to determine if we're vaild
	    private bool IsHeaderValid()
	    {
	        return m_blnHeaderValid; 
	    }
	    
	    // Are we valid. Tell the outside world. As a property
	    public bool Valid
	    {
	        get { return IsHeaderValid(); }
	    }
	
	
	    public ushort PacketLen()
	    {
	        ushort uintLen = 0;
	
	        if (this.IsHeaderValid())
	        {
	            // Get the high byte;
	            uintLen += m_bytHeaderData[M_OFFSET_LEN_START];
	
	            // Make it a real high byte;
	            uintLen *= 256;
	            uintLen += m_bytHeaderData[M_OFFSET_LEN_END];
	        }
	
	        return uintLen;
	    }
	
	    public long GetTACFAC()
	    {
	        // The TAC/FAC is stored in bytes 3,4,5 and 6; 
	        long lngTAC = 0;
	
	        if (this.IsHeaderValid())
	        {
	            // This converstion can be written in a loop, of course, it is expanded here for clarity.
	            // 1) Read the most significant byte;
	            lngTAC = m_bytHeaderData[3];
	
	            // Shift it into postiion
	            lngTAC <<= 8;
	
	            // 2) Read next most significant byte... etc.etc
	            lngTAC += m_bytHeaderData[4];
	            lngTAC <<= 8;
	            lngTAC += m_bytHeaderData[5];
	            lngTAC <<= 8;
	            lngTAC += m_bytHeaderData[6];
	
	            /* The alternative way of doing it is to call 
	             * lngTAC = BuildLong(M_OFFSET_TACFAC_START, M_OFFSET_TACFAC_END); 
	             */
	        }
	
	        return lngTAC;
	
	    }
	
	    public ulong GetMSN()
	    {
	        // The MSN is the serial number that will change per device. This is stored in bytes 7,8 and 9
	        ulong lngMSN = 0;
	
	        if (this.IsHeaderValid())
	        {
	            lngMSN = BuildLong(M_OFFSET_MSN_START, M_OFFSET_MSN_END);
	        }
	
	        return lngMSN;
	    }

        public String GetUnitID()
        {
            String sTACFAC = String.Format("{0:00000000}", GetTACFAC());
            String sMSN = String.Format("{0:0000000}", GetMSN());

            return sTACFAC + sMSN;
        }
	
	    private ulong BuildLong(int intStartByte, int intEndByte)
	    {
	        ulong lngOut = 0;
	
	        // Check if this is sane and within all the bounds!?
	        if ((intEndByte > intStartByte) && (intEndByte <= m_bytHeaderData.GetUpperBound(0))) 
	        {
	            // For each needed byte.
	            while (intStartByte < intEndByte)
	            {
	                // Add the byte
	                lngOut += m_bytHeaderData[intStartByte++];
	
	                // Shift it into place.
	                lngOut <<= 8;
	            }
	
	            // And add the last byte ( we don't have to shift after this
	            lngOut += m_bytHeaderData[intStartByte];
	        }
	
	        return lngOut;
	    }
	}
}
