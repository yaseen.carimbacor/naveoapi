using System;
using System.Collections.Generic;
using System.Text;
using NavLib.Globals;

namespace NavLib.Orion
{
    public class DecodeOrion
    {
        public List<OrionReports> oReports;

        public DecodeOrion(Byte[] bytPacketData)
        {
            Byte[] MyPacketData = Utils.CompressBytWorking(bytPacketData);
            String Msg = Encoding.ASCII.GetString(MyPacketData);
            String[] Reports = Msg.Split('$');

            oReports = new List<OrionReports>();
            foreach (String report in Reports)
                if (report.Trim().Length > 0)
                    oReports.Add(new OrionReports(report));

            //System.IO.File.WriteAllBytes("c:\\Orion.byt", MyPacketData);
        }
    }
}
