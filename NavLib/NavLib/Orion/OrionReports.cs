using System;
using System.Collections.Generic;
using System.Text;
using NavLib.Globals;

namespace NavLib.Orion
{
    public class OrionReports
    { 
        //Normally 18 fields
        //$RGTK,[UnitID],[UTCTime],[A|V],[Longitude],[E|W],[Latitude],[N|S],[Satellite Numbers],[GSM Signal],[Angle],[Speed],[Mileage],[AI1],[AI2],[DI],[DO],[RTCTime]&
        //$LACF,010351708,311210065721,A,5730.3443,E,2013.5668,S,4,31,0,0.0,0.0,-48C,19C,0,0,101231065721&6B
        //Last field has ACK value, after the &
        //19 Fields when having Vepamon Fuel
        //$RGTK,010351726,180111100622,A,5729.8606,E,2008.9521,S,6,17,256,10.2,343.6,-48C,-50C,2,0,110118100621,FFF0FFF0&56

        const int OffsetPacketLog = 0;
        const int OffsetSerialNo = 1;
        const int OffsetUTCDateTime = 2;
        const int OffsetAV = 3;
        const int OffsetLon = 4;
        const int OffsetEW = 5;
        const int OffsetLat = 6;
        const int OffsetSN = 7;
        const int OffsetSatellites = 8;
        const int OffsetSignel = 9;
        const int OffsetAngle = 10;
        const int OffsetSpeed = 11;
        const int OffsetMileage = 12;
        const int OffsetAI1 = 13;
        const int OffsetAI2 = 14;
        const int OffsetDI = 15;
        const int OffsetDO = 16;
        const int OffsetRTCDateTime = 17;
        const int OffsetVepamon = 18;

        const int OffsetIMSI = 4;

        String[] MsgArray;
        String[] MsgArrayInFull;

        System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();

        public OrionReports(String strReport)
        {
            nfi.NumberDecimalSeparator = ".";

            MsgArray = strReport.Split(',');
            MsgArrayInFull = MsgArray;

            if (MsgArray.GetUpperBound(0) < 13)//Previously 10
                MsgArray = new String[19];

            //$GetUnitID,1234,310500129,356614021139819,617100109252950,V4,970817T029,110801092124&69
            //ID, IMEI, IMSI, HW Version, FW Version
            if (strReport.Contains("GetUnitID"))
            {
                strReport = strReport + ",,,,,,,,,,";   //Add up to 18 fields
                MsgArray = strReport.Split(',');
                MsgArray[1] = MsgArray[2];
            }
        }
        
        public String GetIMSI()
        {
            String strResult = String.Empty;
            if (MsgArray[0].Contains("GetUnitID"))
                strResult=MsgArray[OffsetIMSI];

            return strResult;
        }
        
        public String GetLogReason()
        {
            String strLog = MsgArray[OffsetPacketLog];
            strLog = "$" + strLog;
            String strResult = String.Empty;

            if (strLog.Contains("$GetUnitID"))
                strResult = strLog + " GetUnitID and IMSI (???)";
            else if (strLog.Contains("$RGTK"))
                strResult = strLog + " Real Time (Heading)";
            else if (strLog.Contains("$LGTK"))
                strResult = strLog + " Stored Time (Heading)";
            else if (strLog.Contains("$RMTK"))
                strResult = strLog + " SMS Real Time (???)";
            else if (strLog.Contains("$LMTK"))
                strResult = strLog + " SMS Stored Time (???)";
            else if (strLog.Contains("$LGPS"))
                strResult = strLog + " ActGetPos (Heading)";
            else if (strLog.Contains("$LACN"))
                strResult = strLog + " IgnOn [DI] (Ignition)";
            else if (strLog.Contains("$LACF"))
                strResult = strLog + " (IgnOff) [DI] (Ignition)";
            else if (strLog.Contains("$LMPN"))
                strResult = strLog + " (main power up) [AI1],[AI2],[DI] - DI even(Reset)";
            else if (strLog.Contains("$LMPF"))
                strResult = strLog + " Main Power Off [AI1],[AI2],[DI] - DI odd(Reset)";
            else if (strLog.Contains("$LBAT"))
                strResult = strLog + " Battery Report Interval [AI1],[AI2] (???)";
            else if (strLog.Contains("$LVML"))
                strResult = strLog + " Total Mileage reaches Mileage Limitation [Mileage] (???)";
            else if (strLog.Contains("$LDIS"))
                strResult = strLog + " Distance Track Interval > 0 [Mileage] (Heading)";
            else if (strLog.Contains("$LANG"))
                strResult = strLog + " Deviation Angle > �Dev Angle� of $ActSetDevAngle [Mileage] (Heading)";
            else if (strLog.Contains("$LIDL"))
                strResult = strLog + " GPS speed < 5mph for more than �Idle Duration� [Speed] (Time)";
            else if (strLog.Contains("$LMOV"))
                strResult = strLog + " The tracker sends this report when it moves again from its idling. GPS speed >5mph and moving > 0.02 miles (30 meters). [Speed] (EndOfStop)";
            else if (strLog.Contains("$LSIN"))
                strResult = strLog + " GPS speed > �Speed Limitation� [Speed] (Heading)";
            else if (strLog.Contains("$LSUT"))
                strResult = strLog + " The tracker sends this report when its speed is lower than �Speed Limitation� again [Speed] (Heading)";
            else if (strLog.Contains("$LGIN"))
                strResult = strLog + " When the tracker enters the GeoFence boundary [GeoFence Group No. AI1],[Moved Distance AI2],[Empty DI],[Empty DO] (???)";
            else if (strLog.Contains("$LGUT"))
                strResult = strLog + " When the tracker exits the GeoFence boundary [GeoFence Group No. AI1],[Moved Distance AI2],[Empty DI],[Empty DO] (???)";
            else if (strLog.Contains("$LD1N"))
                strResult = strLog + " Digital 1 Changed to On [DI] - IgnOn (???)";
            else if (strLog.Contains("$LD2N"))
                strResult = strLog + " Digital 2 Changed to On [DI] (Aux) (Aux1On)";
            else if (strLog.Contains("$LD3N"))
                strResult = strLog + " Digital 3 Changed to On [DI] (Aux) (Aux2On)";
            else if (strLog.Contains("$LD4N"))
                strResult = strLog + " Digital 4 Changed to On [DI] (Aux) (Aux3On)";
            else if (strLog.Contains("$LD5N"))
                strResult = strLog + " Digital 5 Changed to On [DI] (Aux) (Aux4On)";
            else if (strLog.Contains("$LD6N"))
                strResult = strLog + " Digital 6 Changed to On [DI] (Aux)";
            else if (strLog.Contains("$LD1F"))
                strResult = strLog + " Digital 1 Changed to Off [DI] - IgnOff (???)";
            else if (strLog.Contains("$LD2F"))
                strResult = strLog + " Digital 2 Changed to Off [DI] (Aux) (Aux1Off)";
            else if (strLog.Contains("$LD3F"))
                strResult = strLog + " Digital 3 Changed to Off [DI] (Aux) (Aux2Off)";
            else if (strLog.Contains("$LD4F"))
                strResult = strLog + " Digital 4 Changed to Off [DI] (Aux) (Aux3Off)";
            else if (strLog.Contains("$LD5F"))
                strResult = strLog + " Digital 5 Changed to Off [DI] (Aux) (Aux4Off)";
            else if (strLog.Contains("$LD6F"))
                strResult = strLog + " Digital 6 Changed to Off [DI] (Aux)";
            else if (strLog.Contains("$LKIN"))
                strResult = strLog + " When the user enters some number by the Keypad that connectes to the tracker and press �Enter�, the tracker sends this report to the server. The available numbers are 0~9, A (F1 key of keypad), B(F2 key), C (F3 key) and D (F4 key). The maximum data entry is 27 digits. [Keyin Data AI1], [Empty AI2],[Empty DI],[Empty DO](???)";
            else if (strLog.Contains("$LUIN"))
                strResult = strLog + " RFID card or iButton [RFID No. AI1], [Empty AI2],[Empty DI],[Empty DO](???)";
            else if (strLog.Contains("$LUOT"))
                strResult = strLog + " When the user removes his iButton from the iButton [RFID No. AI1], [Empty AI2],[Empty DI],[Empty DO](???)";
            else if (strLog.Contains("$LGSK"))
                strResult = strLog + " When a Choco or a Speedtrac PRO is waked up by a schedule timer or a vibration detection Start journey (Heading)";
            else if (strLog.Contains("$LGEK"))
                strResult = strLog + " When a Speedtrac or Choco stops without vibration detected for more than certain time period (ex. 60 sec), it sends this report to the server by GPRS then shutting down. End journey (???)";
            else if (strLog.Contains("$LMSK"))
                strResult = strLog + " When a Choco or a Speedtrac PRO is waked up by a schedule timer or a vibration detection Start journey SMS (???)";
            else if (strLog.Contains("$LMEK"))
                strResult = strLog + " When a Speedtrac or Choco stops without vibration detected for more than certain time period (ex. 60 sec), it sends this report to the server by SMS then shutting down. End journey SMS (???)";
            else if (strLog.Contains("$RGRK"))
                strResult = strLog + " When a Speedtrac or Choco is waked up by the schedule timer (???)";
            else if (strLog.Contains("$LGRK"))
                strResult = strLog + " When a Speedtrac or Choco is waked up by the schedule timer, and the tracker can not send the report to the server by GPRS immediately. The tracker will send it as LGRK next time it get GPRS connected. (???)";
            else if (strLog.Contains("$RMRK"))
                strResult = strLog + " When a Speedtrac or Choco is waked up by the schedule timer SMS (???)";
            else if (strLog.Contains("$LMRK"))
                strResult = strLog + " When a Speedtrac or Choco is waked up by the schedule timer, and the tracker can not send the report to the server by SMS immediately. The tracker will send it as LMRK next time it gets GSM connected. (???)";
            else if (strLog.Contains("$LIBT"))
                strResult = strLog + " When an ORION iButton reader is connected to Easytrac or Speedtrac, all iButton reader events will be send by the tracker as $LBAT event reports. In $LIBT reports, it uses AI-1 column for the iButton reader �Event� and AI-2 column for the �iButton ID�. [iButton Event AI1],[iButton ID AI2],[DI],[DO] (Orion)";
            /*
            iButton Events  Examples                            Description
            DN              DN,C50008014DEA8E10                 Driver Log-in : ID: C50008014DEA8E10
            DF              DF,C50008014DEA8E10                 Driver Log-out : ID: C50008014DEA8E10
            1N              1N,C50008014DEA8E11                 1st Passenger Log-in: ID: C50008014DEA8E11
            1F              1F,C50008014DEA8E11                 1st Passenger Log-out: ID: C50008014DEA8E11
            2N              2N,C50008014DEA8E12                 2nd Passenger Log-in: ID: C50008014DEA8E12
            2F              2F,C50008014DEA8E12                 2nd Passenger Log-out: ID: C50008014DEA8E12
            3N              3N,C50008014DEA8E13                 3rd Passenger Log-in: ID: C50008014DEA8E13
            3F              3F,C50008014DEA8E13                 3rd Passenger Log-out: ID: C50008014DEA8E13
            4N              4N,C50008014DEA8E14                 4th Passenger Log-in: ID: C50008014DEA8E14
            4F              4F,C50008014DEA8E14                 4th Passenger Log-out: ID: C50008014DEA8E14
            5N              5N,C50008014DEA8E15                 5th Passenger Log-in: ID: C50008014DEA8E15
            5F              5F,C50008014DEA8E15                 5th Passenger Log-out: ID: C50008014DEA8E15
            BT              BT,                                 Button pushed: Business Trip
            PT              PT,                                 Button pushed: Private Trip
            OT              OT,                                 Button pushed: Other Trip
            PB              PB,                                 Button pushed for more than 3 sec as a Panic Button: SOS
            */
            return strResult;
        }

        public String GetSerialNo()
        {
            //10 Chars
            //Cannot use IsValid() here, as we need to save data in all cases
            String strResult = String.Empty;
            try
            {
                strResult = MsgArray[OffsetSerialNo];
            }
            catch
            {
                strResult = String.Empty;
            }
            return strResult;
        }
        public DateTime GetUTCDateTimeWithoutValidCheck()
        {
            //ddMMyyHHmmss
            String strDate = ((int)(DateTime.Now.Year / 100)).ToString()
                + MsgArray[OffsetUTCDateTime].Substring(4, 2)
                + "-" + MsgArray[OffsetUTCDateTime].Substring(2, 2)
                + "-" + MsgArray[OffsetUTCDateTime].Substring(0, 2)
                + " " + MsgArray[OffsetUTCDateTime].Substring(6, 2)
                + ":" + MsgArray[OffsetUTCDateTime].Substring(8, 2)
                + ":" + MsgArray[OffsetUTCDateTime].Substring(10, 2);
            return Convert.ToDateTime(strDate);
        }
        public DateTime GetUTCDateTime()
        {
            //if (!IsValid())
            //    return new DateTime(1975, 10, 8);

            return GetUTCDateTimeWithoutValidCheck();
        }
        public Boolean IsValid()
        {
            //GPS Valid
            //A Valid V not
            String g = MsgArray[OffsetAV];
            Boolean bResult = false;
            if (g == "A")
                bResult = true;

            return bResult;
        }
        public Double GetLongitude()
        {
            //if (!IsValid())
            //    return 1.1;

            //xxxxx.xxxx                
            String g = MsgArray[OffsetLon].Trim();

            int Lat = 3;
            if (g.Substring(4, 1) == ".")
                Lat = 2;

            Double LatX = Double.Parse(g.Substring(0, Lat), nfi);
            Double dblTempDegrees = Double.Parse(g.Substring(Lat), nfi) / 60;
            LatX += dblTempDegrees;
            return LatX;
        }
        public String GetEW()
        {
            //East West
            return MsgArray[OffsetEW];
        }
        public Double GetLatitude()
        {
            //if (!IsValid())
            //    return 1.1;

            //xxxx.xxxx
            String g = MsgArray[OffsetLat];
            Double LatX = Double.Parse(g.Substring(0, 2), nfi);
            Double dblTempDegrees = Double.Parse(g.Substring(2), nfi) / 60;
            LatX += dblTempDegrees;
            LatX *= -1;
            return LatX;
        }
        public String GetSN()
        {
            //South North
            return MsgArray[OffsetSN];
        }
        public String GetSatelliteNumbers()
        {
            return MsgArray[OffsetSatellites];
        }
        public String GetSignel()
        {
            return MsgArray[OffsetSignel];
        }
        public String GetAngle()
        {
            return MsgArray[OffsetAngle];
        }
        public Double GetSpeed()
        {
            //Speed is in miles
            // 1 mile = 1.609344000000865 KM
            String str = MsgArray[OffsetSpeed];
            return Convert.ToDouble(str, nfi) * 1.909344000000865;
        }
        public String GetMileage()
        {
            //if (!IsValid())
            //    return "0";
            return MsgArray[OffsetMileage];
        }
        public String GetAI1()
        {
            //Main Power voltage
            return MsgArray[OffsetAI1];
        }
        public String GetAI2()
        {
            //Backup Battery voltage
            return MsgArray[OffsetAI2];
        }
        public String GetDI()
        {
            return MsgArray[OffsetDI];
        }
        public String GetDO()
        {
            return MsgArray[OffsetDO];
        }
        public DateTime GetRTCDateTime()
        {
            String strDate = ((int)(DateTime.Now.Year / 100)).ToString()
                + MsgArray[OffsetRTCDateTime].Substring(0, 2)
                + "-" + MsgArray[OffsetRTCDateTime].Substring(2, 2)
                + "-" + MsgArray[OffsetRTCDateTime].Substring(4, 2)
                + " " + MsgArray[OffsetRTCDateTime].Substring(6, 2)
                + ":" + MsgArray[OffsetRTCDateTime].Substring(8, 2)
                + ":" + MsgArray[OffsetRTCDateTime].Substring(10, 2);
            return Convert.ToDateTime(strDate);
        }
        public int GetVepamonLvl(int iSensorNumber)
        { 
            //Max value is 1023
            //Sample FFFF0211&41\r\n    >>> Values FFFF and 0211
            //iSensorNumber is either 1 or 2
            int iResult = 9999;
            int iStart = 0;
            if (iSensorNumber == 2)
                iStart = 4;

            if (MsgArray.Length == 19)
            {
                String strHex = MsgArray[OffsetVepamon].Substring(iStart, 4);
                iResult = Utils.HexToDecimal(strHex);
            }
            return iResult;
        }
        public String GetCheckSum()
        {
            //Checksum is always in the last field
            String str = MsgArrayInFull[MsgArrayInFull.Length - 1];

            if (str == String.Empty || str == null || str.IndexOf('&') == -1)
                return String.Empty;

            int iFrom = str.IndexOf('&') + 1;
            int iTo = str.IndexOf(Convert.ToChar(13)) - 1;
            String strResult = str.Substring(iFrom, iTo - iFrom + 1);
            
            return strResult;
        }

        public String GetDriverID()
        {
            String StrResult = String.Empty;
            //Driver LogIn
            if (GetAI1() == "DN")
                StrResult = GetAI2();

            //Driver /LogOut
            if (GetAI1() == "DF")
                StrResult = "Driver Log Out";

            return StrResult;
        }
        public Boolean IsPanic()
        {
            Boolean bResult = false;
            //Button pushed for more than 3 sec as a Panic
            if (GetAI1() == "PB")
                bResult = true;
            return bResult;
        }

        public Boolean GetAux2()
        {
            Boolean bResult = false;
            String strLog = MsgArray[OffsetPacketLog];
            strLog = "$" + strLog;

            if (strLog.Contains("$LD2N"))
                bResult = true;

            if (strLog.Contains("$LD2F"))
                bResult = true;

            return bResult;
        }
        public Boolean GetAux3()
        {
            Boolean bResult = false;
            String strLog = MsgArray[OffsetPacketLog];
            strLog = "$" + strLog;

            if (strLog.Contains("$LD3N"))
                bResult = true;

            return bResult;
        }
        public Boolean GetAux4()
        {
            Boolean bResult = false;
            String strLog = MsgArray[OffsetPacketLog];
            strLog = "$" + strLog;

            if (strLog.Contains("$LD4N"))
                bResult = true;

            return bResult;
        }
        public Boolean GetAux5()
        {
            Boolean bResult = false;
            String strLog = MsgArray[OffsetPacketLog];
            strLog = "$" + strLog;

            if (strLog.Contains("$LD5N"))
                bResult = true;

            return bResult;
        }
        public Boolean GetAux6()
        {
            Boolean bResult = false;
            String strLog = MsgArray[OffsetPacketLog];
            strLog = "$" + strLog;

            if (strLog.Contains("$LD6N"))
                bResult = true;

            return bResult;
        }
    }
}
