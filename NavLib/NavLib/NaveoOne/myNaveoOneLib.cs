﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NaveoOneLib;

namespace NavLib.NaveoOne
{
    public static class myNaveoOneLib
    {
        public static String ProcessArchiving(List<int> lAssetID, String strDate)
        {
            return NaveoOneLib.Services.DataManagement.ProcessArchiving(lAssetID, strDate, "DB1", 500, 10, true);
        }

        public static String ProcessArchivingNow(List<int> lAssetID, String strDate)
        {
            return NaveoOneLib.Services.DataManagement.ProcessArchiving(lAssetID, strDate, "DB1", -1, 60, false);
        }

        public static int MaxAssetID()
        {
            return NaveoOneLib.Services.DataManagement.MaxAssetID("DB1");
        }
        public static List<int> lAssetIDs()
        {
            return NaveoOneLib.Services.DataManagement.lAssetIDs("DB1");
        }
    }
}
