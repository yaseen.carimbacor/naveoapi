using System;
using System.Collections.Generic;
using System.Text;
using NavLib.Globals;

namespace NavLib.Fuel
{
    //Obsolete now, because we are no longer using the Vepamon server
    public class SendMessages
    {
        public static Byte[] SensorDataMessage(int dataID, int FuelID, int SensorNumber, int SensorValue)
        {
            Byte[] bResult = new Byte[15];
            bResult[0] = 0xC0;

            Byte[] b = new Byte[14];
            b[0] = 0x01;
            Byte[] bDataID = BitConverter.GetBytes(dataID);
            bDataID = Utils.ReverseBytes(bDataID);
            if (bDataID.Length == 4)
                Array.Copy(bDataID, 0, b, 5, bDataID.Length);
            else if (bDataID.Length == 8)
                Array.Copy(bDataID, 0, b, 1, bDataID.Length);

            Byte[] bFuelID = BitConverter.GetBytes(FuelID);
            b[9] = bFuelID[1];
            b[10] = bFuelID[0];

            Byte[] bSensorNumber = BitConverter.GetBytes(SensorNumber);
            b[11] = bSensorNumber[0];

            Byte[] bSensorValue = BitConverter.GetBytes(SensorValue);
            b[12] = bSensorValue[1];
            b[13] = bSensorValue[0];

            Array.Copy(b, 0, bResult, 1, b.Length);
            return bResult;
        }
        public static Byte[] RestartStreamMessage()
        {
            Byte[] bResult = new Byte[2];
            bResult[0] = 0xC0;

            Byte[] b = new Byte[1];
            b[0] = 0x09;

            Array.Copy(b, 0, bResult, 1, b.Length);
            return bResult;
        }
    }
}
