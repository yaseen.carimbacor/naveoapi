using System;
using System.Collections.Generic;
using System.Text;
using NavLib.Globals;
using NavLib.Data;
using NavLib.Processing;

namespace NavLib.Fuel
{
    public class FuelDecode
    {
        Byte[] BytFuelOnly;
        public FuelDecode(Byte[] BytFuel)
        {
            BytFuelOnly = new Byte[BytFuel.Length - 1];
            Array.Copy(BytFuel, 1, BytFuelOnly, 0, BytFuelOnly.Length);
        }

        public String GetData()
        {
            String strResult = GetMessageType();
            switch (strResult)
            {
                case "SensorDataMessage":
                    break;

                case "RawDataMessage":
                    strResult += " " + GetRawDataMessage();
                    break;

                case "RawSumDataMessage":
                    strResult += " " + GetSumRawDataMessage();
                    break;

                case "FillDataMessage":
                    strResult += " " + GetFillDataMessage();
                    break;

                case "RealDataMessage":
                    strResult += " " + GetRealDataMessage();
                    break;

                case "NotificationMessage":
                    strResult += " " + GetNotification();
                    break;

                case "RestartStreamMessage":
                    break;
            }
            return "\r\n" + strResult;
        }
        public void SaveData()
        {
            int MessageType = (int)BytFuelOnly[0];
            int DataID = -1;
            int FuelID = -1;
            int SensorNo = -1;
            int Volume = -1;
            int StartDataID = -1;
            int EndDataID = -1;
            int notifCode = -1;
            DateTime dt = DateTime.Now.ToUniversalTime();

            String strResult = GetMessageType();
            switch (strResult)
            {
                case "SensorDataMessage":
                    break;

                case "RawDataMessage":
                    DataID = (int)Utils.BuildLong(BytFuelOnly, 1, 8);
                    FuelID = (int)Utils.BuildLong(BytFuelOnly, 9, 10);
                    SensorNo = (int)Utils.BuildLong(BytFuelOnly, 11, 11);
                    Volume = (int)Utils.BuildLong(BytFuelOnly, 12, 15);
                    break;

                case "RawSumDataMessage":
                    DataID = (int)Utils.BuildLong(BytFuelOnly, 1, 8);
                    FuelID = (int)Utils.BuildLong(BytFuelOnly, 9, 10);
                    Volume = (int)Utils.BuildLong(BytFuelOnly, 11, 14);
                    break;

                case "FillDataMessage":
                    StartDataID = (int)Utils.BuildLong(BytFuelOnly, 1, 8);
                    EndDataID = (int)Utils.BuildLong(BytFuelOnly, 9, 16);
                    FuelID = (int)Utils.BuildLong(BytFuelOnly, 17, 18);
                    Volume = (int)Utils.BuildLong(BytFuelOnly, 19, 22);
                    break;

                case "RealDataMessage":
                    DataID = (int)Utils.BuildLong(BytFuelOnly, 1, 8);
                    FuelID = (int)Utils.BuildLong(BytFuelOnly, 9, 10);
                    Volume = (int)Utils.BuildLong(BytFuelOnly, 11, 14);
                    break;

                case "NotificationMessage":
                    notifCode = (int)Utils.BuildLong(BytFuelOnly, 1, 2);
                    break;

                case "RestartStreamMessage":
                    break;
            }
            String unitID = new FuelProcessing().GetUnitID(FuelID);
            new Runme().InsertFuelData(MessageType, dt, DataID, StartDataID, EndDataID, FuelID, unitID, SensorNo, -1, -1, Volume, notifCode);
        }
        String GetMessageType()
        {
            int Header = (int)BytFuelOnly[0];
            String MessageType = String.Empty;
            switch (Header)
            {
                case 1: //ToServer
                    MessageType = "SensorDataMessage";
                    break;

                case 3: //FromServer
                    MessageType = "RawDataMessage";
                    break;

                case 4: //FromServer
                    MessageType = "RawSumDataMessage";
                    break;

                case 5: //FromServer
                    MessageType = "FillDataMessage";
                    break;

                case 6: //FromServer
                    MessageType = "RealDataMessage";
                    break;

                case 7: //FromServer
                    MessageType = "NotificationMessage";
                    break;

                case 9: //ToServer
                    MessageType = "RestartStreamMessage";
                    break;
            }
            return MessageType;
        }
        String GetNotification()
        {
            int notifCode = (int)Utils.BuildLong(BytFuelOnly, 1, 2);
            String strResult = String.Empty;
            switch (notifCode)
            {
                case 5:
                    strResult = "DataID does not increase monotically";
                    break;

                case 6:
                    strResult = "Session Time out";
                    break;

                case 7:
                    strResult = "Wrong vehicle id or wrong sensor number";
                    break;

                case 12:
                    strResult = "Buffers cleared";
                    break;

                default:
                    strResult = notifCode.ToString();
                    break;
            }
            return strResult;
        }
        String GetRawDataMessage()
        {
            int DataID = (int)Utils.BuildLong(BytFuelOnly, 1, 8);
            int VehicleID = (int)Utils.BuildLong(BytFuelOnly, 9, 10);
            int SensorNo = (int)Utils.BuildLong(BytFuelOnly, 11, 11);
            int Volume = (int)Utils.BuildLong(BytFuelOnly, 12, 15);

            String strResult = String.Empty;
            strResult = "DataID:" + DataID.ToString() + ";";
            strResult += " VehicleID:" + VehicleID.ToString() + ";";
            strResult += " SensorNo:" + SensorNo.ToString() + ";";
            strResult += " Volume:" + Volume.ToString();
            return strResult;
        }
        String GetSumRawDataMessage()
        {
            int DataID = (int)Utils.BuildLong(BytFuelOnly, 1, 8);
            int VehicleID = (int)Utils.BuildLong(BytFuelOnly, 9, 10);
            int Capacity = (int)Utils.BuildLong(BytFuelOnly, 11, 14);

            String strResult = String.Empty;
            strResult = "DataID:" + DataID.ToString() + ";";
            strResult += " VehicleID:" + VehicleID.ToString() + ";";
            strResult += " Capacity:" + Capacity.ToString();
            return strResult;
        }
        String GetFillDataMessage()
        {
            int StartDataID = (int)Utils.BuildLong(BytFuelOnly, 1, 8);
            int EndDataID = (int)Utils.BuildLong(BytFuelOnly, 9, 16);
            int VehicleID = (int)Utils.BuildLong(BytFuelOnly, 17, 18);
            int Capacity = (int)Utils.BuildLong(BytFuelOnly, 19, 22);

            String strResult = String.Empty;
            strResult = "StartDataID:" + StartDataID.ToString() + ";";
            strResult += " EndDataID:" + EndDataID.ToString() + ";";
            strResult += " VehicleID:" + VehicleID.ToString() + ";";
            strResult += " Capacity:" + Capacity.ToString();
            return strResult;
        }
        String GetRealDataMessage()
        {
            int DataID = (int)Utils.BuildLong(BytFuelOnly, 1, 8);
            int VehicleID = (int)Utils.BuildLong(BytFuelOnly, 9, 10);
            int Capacity = (int)Utils.BuildLong(BytFuelOnly, 11, 14);

            String strResult = String.Empty;
            strResult = " DataID:" + DataID.ToString() + ";";
            strResult += " VehicleID:" + VehicleID.ToString() + ";";
            strResult += " Capacity:" + Capacity.ToString();
            return strResult;
        }
    }
}
