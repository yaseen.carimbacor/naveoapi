﻿using System;
using System.Collections.Generic;
using System.Text;
using NaveoOneLib.Models;
using System.Data;
using System.Threading;
using NavLib.Globals;
using NavLib.Data;
using System.Windows.Forms;
using NaveoOneLib.Common;
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.Telemetry;
using NaveoOneLib.Models.GPS;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Zones;
using NaveoOneLib.Models.Users;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.Drivers;
using NaveoOneLib.Models.Reportings;
using NaveoOneLib.Services.Rules;
using NaveoOneLib.Services.Fuels;
using NaveoOneLib.DBCon;
using NaveoOneLib.Services.Assets;
using NaveoOneLib.Services.Zones;
using NaveoOneLib.Services.Drivers;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Services.GPS;
using NaveoOneLib.Services.Telemetry;
using NaveoOneLib.Services.Users;
using NaveoOneLib.Services.Reportings;
using Newtonsoft.Json;

namespace NavLib.CoreData
{
    public class NaveoCore
    {
        public NaveoCore()
        {
        }

        public GPSData initGPSData(
            String strUnitModel
            , String striButtonID
            , String strUnitID
            , Double Lat
            , Double Lon
            , DateTime dt
            , float speed
            , float distance
            , Boolean IgnitionOn
            , String strLogReason
            , Boolean GPSValid)
        {
            GPSData gpsData = new GPSData();
            try
            {
                if (dt.Year < 2018)
                    return gpsData;

                gpsData.DateTimeGPS_UTC = dt;
                gpsData.EngineOn = IgnitionOn == true ? 1 : 0;
                gpsData.StopFlag = IgnitionOn == true ? 0 : 1;
                gpsData.Latitude = Lat;
                gpsData.Longitude = Lon;
                gpsData.LongLatValidFlag = GPSValid == true ? 1 : 0;
                gpsData.Speed = speed;
                gpsData.iButton = striButtonID;
                gpsData.DeviceID = strUnitID;
                List<GPSDataDetail> lr = new GPSDataDetailService().GetLogReason(strLogReason);

                switch (strUnitModel)
                {
                    case "Enfora":
                        Boolean bIgn = GetEnforaIgnition(strLogReason);
                        gpsData.EngineOn = bIgn == true ? 1 : 0;
                        gpsData.StopFlag = bIgn == true ? 0 : 1;
                        break;

                    case "EnforN":
                        String[] etemp = strLogReason.Split('\n');
                        if (etemp.Length >= 2)
                            if (etemp[1] != "999")   //Auxiliary Temperature 1 Degrees C
                            {
                                GPSDataDetail gpsDetail = new GPSDataDetail();
                                gpsDetail.TypeID = " Temperature 1";
                                gpsDetail.TypeValue = etemp[1];
                                gpsDetail.UOM = "Degrees C";
                                lr.Add(gpsDetail);
                            }
                        if (etemp.Length >= 3)
                            if (etemp[2] != "999")     //Auxiliary Temperature 2 Degrees C
                            {
                                GPSDataDetail gpsDetail = new GPSDataDetail();
                                gpsDetail.TypeID = " Temperature 2";
                                gpsDetail.TypeValue = etemp[2];
                                gpsDetail.UOM = "Degrees C";
                                lr.Add(gpsDetail);
                            }
                        break;

                    case "ATRACK":
                        String[] temp = strLogReason.Split('\n');
                        if (temp[1] != "200")   //Analogue. Temperature
                        {
                            GPSDataDetail gpsDetail = new GPSDataDetail();
                            gpsDetail.TypeID = "Temperature";
                            gpsDetail.TypeValue = temp[1];
                            gpsDetail.UOM = "Degrees C";
                            lr.Add(gpsDetail);
                        }
                        if (temp[2] != "200")   //Analogue. Temperature 2
                        {
                            GPSDataDetail gpsDetail = new GPSDataDetail();
                            gpsDetail.TypeID = "Temperature 2";
                            gpsDetail.TypeValue = temp[2];
                            gpsDetail.UOM = "Degrees C";
                            lr.Add(gpsDetail);
                        }
                        if (temp[3] != "200")   //Analogue. Temperature 3
                        {
                            GPSDataDetail gpsDetail = new GPSDataDetail();
                            gpsDetail.TypeID = "Temperature 3";
                            gpsDetail.TypeValue = temp[3];
                            gpsDetail.UOM = "Degrees C";
                            lr.Add(gpsDetail);
                        }
                        if (temp[4] != "200")   //Analogue. Temperature 4
                        {
                            GPSDataDetail gpsDetail = new GPSDataDetail();
                            gpsDetail.TypeID = "Temperature 4";
                            gpsDetail.TypeValue = temp[4];
                            gpsDetail.UOM = "Degrees C";
                            lr.Add(gpsDetail);
                        }
                        if (temp[5] != "0")     //Analogue Value. Fuel
                        {
                            GPSDataDetail gpsDetail = new GPSDataDetail();
                            gpsDetail.TypeID = "Fuel";
                            gpsDetail.TypeValue = temp[5];
                            gpsDetail.UOM = "%";
                            lr.Add(gpsDetail);
                        }
                        if (temp[6] != "No Data")     //Analogue Value. Fuel
                        {
                            String[] allData = temp[6].Split(';');
                            foreach (String s in allData)
                            {
                                String[] g = s.Split(':');
                                if (g[0].Contains("["))
                                {
                                    String x = g[0].Replace("[", String.Empty);
                                    x = x.Replace("]", String.Empty);
                                    GPSDataDetail gpsDetail = new GPSDataDetail();
                                    gpsDetail.TypeID = x;
                                    gpsDetail.TypeValue = g[1];
                                    gpsDetail.UOM = g[2];
                                    lr.Add(gpsDetail);
                                }
                            }
                        }

                        //17 June 2020. To stop using below codes as DriverID will b on all lines.
                        //String strDriverID = GetAtrackDriver(striButtonID, strLogReason, dt, strUnitID);
                        //if (strDriverID != string.Empty)
                        //    gpsData.iButton = strDriverID;
                        break;

                    case "AT240":
                        String[] sAT240 = strLogReason.Split('\n');
                        if (sAT240[1] != "0")   //ADC1
                        {
                            GPSDataDetail gpsDetail = new GPSDataDetail();
                            gpsDetail.TypeID = "Fuel";
                            gpsDetail.TypeValue = sAT240[1];
                            gpsDetail.UOM = "ADC1";
                            lr.Add(gpsDetail);
                        }
                        if (sAT240[2] != "0")   //ADC2
                        {
                            GPSDataDetail gpsDetail = new GPSDataDetail();
                            gpsDetail.TypeID = "Fuel";
                            gpsDetail.TypeValue = sAT240[2];
                            gpsDetail.UOM = "ADC2";
                            lr.Add(gpsDetail);
                        }
                        break;

                    case "AT200":
                        String[] sAT200 = strLogReason.Split('\n');
                        if (sAT200[1] != "0")   //ADC1
                        {
                            GPSDataDetail gpsDetail = new GPSDataDetail();
                            gpsDetail.TypeID = "Fuel";
                            gpsDetail.TypeValue = sAT200[1];
                            gpsDetail.UOM = "ADC1";
                            lr.Add(gpsDetail);
                        }
                        break;
                }

                foreach (GPSDataDetail d in lr)
                    if (d.TypeValue == "No GPS")
                        gpsData.LongLatValidFlag = 0;

                gpsData.GPSDataDetails = lr;
            }
            catch (System.Exception ex)
            {
                Logger.WriteToErrorLog(ex, strUnitID);
            }

            return gpsData;
        }

        public Boolean BulkCoreProcessing(List<GPSData> lGPSData)
        {
            return new GPSDataService().BulkSaveGPSData(lGPSData, "DB1");
        }
        public Boolean BulkSecuMonitorProcessing(List<GPSData> lGPSData)
        {
            return new GPSDataService().BulkSaveGPSDataForSecuMonitor(lGPSData, "DB1");
        }

        public void UpdateAddLastRecord(
            String strUnitModel
            , String striButtonID
            , String strUnitID
            , Double Lat
            , Double Lon
            , DateTime dt
            , float speed
            , float distance
            , Boolean IgnitionOn
            , String strLogReason
            , Boolean GPSValid)
        {
            GPSData gpsData = initGPSData(strUnitModel, striButtonID, strUnitID, Lat, Lon, dt, speed, distance, IgnitionOn, strLogReason, GPSValid);
            Boolean b = new GPSDataService().SaveGPSData(gpsData, gpsData.GPSDataDetails, "DB1");
        }

        public void AddTelemetricRecord(TelDataHeader th)
        {
            try
            {
                new TelDataHeaderService().SaveTelDataHeader(th, "DB1");
            }
            catch (System.Exception ex)
            {
                Logger.WriteToErrorLog(ex, th.DeviceID);
            }
        }

        #region Atrack
        String GetAtrackDriver(String striButtonID, String strLogReason, DateTime dt, String strDevice)
        {
            String strResult = String.Empty;
            Runme runme = new Runme();

            if (strLogReason.Contains("(DriverID)"))    //save in NaveoGrnlList. Need to add iID as primary key, because if IgnOff is not detected, delete will not occur.
            {
                runme.InsertDriverID(strDevice, striButtonID, dt, "DB1");
                strResult = striButtonID;
            }
            else                                        //Read from NaveoGrnlList 
            {
                DataSet ds = runme.GetDriverID(strDevice, "DB1");
                if (ds.Tables[0].Rows.Count > 0)
                    strResult = ds.Tables[0].Rows[0]["DriverID"].ToString();
            }
            if (strLogReason.Contains("(IgnOff)"))      //delete from NaveoGrnlList
                runme.DeleteDriverID(strDevice, "DB1");

            return strResult;
        }
        #endregion
        #region Enfora
        Boolean GetEnforaIgnition(String strLogReason)
        {
            Boolean b = true;
            switch (strLogReason)
            {
                case "64"://ON
                    b = true;
                    break;

                case "Z"://ON
                    b = true;
                    break;

                case "65"://OFF
                    b = false;
                    break;

                case "IgnOff"://OFF
                    b = false;
                    break;
            }
            if (strLogReason.Contains("IgnOff"))
                b = false;

            return b;
        }

        #endregion

        public void CheckDB()
        {
            new SystemInit().UpdateDB("DB1");
        }

        public String GetAllActiveDevices()
        {
            return new AssetDeviceMapService().GetActiveDevices("DB1");
        }
        public String GetAllSecuMonitorDevices()
        {
            return new SecuMonitorAsset().GetAllSecuMonitorDevices("DB1");
        }

        public void insertGroup(String Desc, String ParentDesc)
        {
            GroupMatrix gm = new GroupMatrix();
            GroupMatrixService groupMatrixService = new GroupMatrixService();
            gm = groupMatrixService.GetGroupMatrixByName(Desc, "DB1");
            if (gm == null)
            {
                gm = new GroupMatrix();
                gm.gmDescription = Desc;
                gm.parentGMID = groupMatrixService.GetGroupMatrixByName(ParentDesc, "DB1").gmID;
                groupMatrixService.SaveGroupMatrix(gm, true, "DB1");
            }
        }
        public String insertVehicles(DataTable dt)
        {
            String s = String.Empty;
            int iSaved = 0;
            foreach (DataRow dr in dt.Rows)
            {
                AssetService assetService = new AssetService();
                Asset a = assetService.GetAssetByName(dr["Desc"].ToString(), "DB1");
                if (a == null)
                {
                    a = new Asset();
                    a.AssetNumber = dr["Desc"].ToString();
                    Matrix m = new Matrix();
                    m.GMID = new GroupMatrixService().GetGroupMatrixByName(dr["Group"].ToString(), "DB1").gmID;
                    m.oprType = DataRowState.Added;
                    a.lMatrix.Add(m);

                    a.Odometer = (int)Convert.ToDouble(dr["Odo"].ToString());
                    String strDevice = dr["UnitID"].ToString();
                    AssetDeviceMap adm = new AssetDeviceMap();
                    adm.DeviceID = strDevice;
                    adm.Status_b = NaveoOneLib.Common.Constants.strActive;
                    adm.oprType = DataRowState.Added;
                    a.assetDeviceMap = adm;

                    Boolean b = assetService.Save(a, true, 1, "DB1").data;
                    if (!b)
                        s += a.AssetNumber + "\r\n";
                    else
                        iSaved++;
                }
            }
            if (s.Trim().Length > 0)
                s += "not saved\r\n";

            s += iSaved.ToString() + " new assets saved\r\n";
            return s;
        }
        public String insertZoneTypes(DataTable dt)
        {
            String s = String.Empty;
            ZoneTypeService zoneType = new ZoneTypeService();
            foreach (DataRow dr in dt.Rows)
            {
                ZoneType zt = zoneType.GetZoneTypeByDescription(dr["Desc"].ToString(), "DB1");
                if (zt == null)
                {
                    zt = new ZoneType();
                    zt.sDescription = dr["Desc"].ToString();
                    zt.sComments = dr["id"].ToString();

                    Matrix m = new Matrix();
                    m.GMID = new GroupMatrixService().GetGroupMatrixByName(dr["Group"].ToString(), "DB1").gmID;
                    m.oprType = DataRowState.Added;
                    zt.lMatrix.Add(m);

                    Boolean b = zoneType.SaveZoneType(zt, true, "DB1");
                    if (!b)
                        s += zt.sDescription + "\r\n";
                }
            }

            if (s.Trim().Length > 0)
                s += "not saved\r\n";
            else
                s = "new ZoneTypes saved\r\n";
            return s;
        }
        public String insertZones(DataSet ds)
        {
            String s = String.Empty;
            foreach (DataRow dr in ds.Tables["Header"].Rows)
            {
                Zone z = new ZoneService().GetZoneByName(dr["Desc"].ToString(), dr["Group"].ToString(), "DB1");
                if (z == null)
                {
                    z = new Zone();
                    z.description = dr["Desc"].ToString();
                    z.displayed = 1;
                    z.color = (int)dr["iColor"];

                    Matrix m = new Matrix();
                    m.GMID = new GroupMatrixService().GetGroupMatrixByName(dr["Group"].ToString(), "DB1").gmID;
                    m.oprType = DataRowState.Added;
                    z.lMatrix.Add(m);

                    List<ZoneDetail> l = new List<ZoneDetail>();
                    DataRow[] drDetail = ds.Tables["Detail"].Select("Desc = '" + dr["Desc"].ToString().Replace("'", "''") + "'");
                    foreach (DataRow drd in drDetail)
                    {
                        ZoneDetail zd = new ZoneDetail();
                        zd.latitude = (float)drd["Lat"];
                        zd.longitude = (float)drd["Lon"];
                        l.Add(zd);
                    }
                    z.zoneDetails = l;

                    List<ZoneType> lzt = new List<ZoneType>();
                    DataRow[] drzt = ds.Tables["ZoneType"].Select("Desc = '" + dr["Desc"].ToString().Replace("'", "''") + "'");
                    foreach (DataRow drd in drzt)
                    {
                        ZoneType zt = new ZoneType();
                        zt.ZoneTypeID = new ZoneTypeService().GetZoneTypeByComments(drd["TypeID"].ToString(), "DB1").ZoneTypeID;
                        lzt.Add(zt);
                    }
                    z.lZoneType = lzt;

                    DataSet dsr = new DataSet();
                    Boolean b = new ZoneService().SaveZone(z, true, 1, out dsr, "DB1");
                    if (!b)
                        s += z.description + "\r\n";
                }
            }

            if (s.Trim().Length > 0)
                s += "not saved\r\n";
            else
                s = "new zones saved\r\n";
            return s;
        }
        public String insertUsers(DataTable dt)
        {
            String s = String.Empty;
            UserService userService = new UserService();
            foreach (DataRow dr in dt.Rows)
            {
                User u = new User();
                u = userService.GetUserByeMail(dr["User"].ToString(), String.Empty, "DB1", false, false);
                if (u.UID == -999)
                {
                    u = new User();
                    u.Names = dr["User"].ToString();
                    u.Email = dr["User"].ToString();
                    u.Password = dr["Password"].ToString();

                    Matrix m = new Matrix();
                    m.GMID = new GroupMatrixService().GetGroupMatrixByName(dr["Group"].ToString(), "DB1").gmID;
                    m.oprType = DataRowState.Added;
                    u.lMatrix.Add(m);

                    BaseModel b = new UserService().SaveUser(u, true, "DB1");
                    if (!b.data)
                        s += u.Names + "\r\n";
                }
            }
            if (s.Trim().Length > 0)
                s += "not saved\r\n";
            else
                s = "new Users saved\r\n";
            return s;
        }
        public String insertDrivers(DataTable dt)
        {
            String s = String.Empty;
            foreach (DataRow dr in dt.Rows)
            {
                DriverService driverService = new DriverService();
                Driver d = new Driver();
                d = driverService.GetDriverByIbutton(dr["iButton"].ToString(), "DB1");
                if (d.iButton == driverService.GetDefaultDriver("DB1").iButton)
                {
                    d = new Driver();
                    d.sDriverName = dr["DriverN"].ToString();
                    d.iButton = dr["iButton"].ToString();

                    Matrix m = new Matrix();
                    m.GMID = new GroupMatrixService().GetGroupMatrixByName(dr["Group"].ToString(), "DB1").gmID;
                    m.oprType = DataRowState.Added;
                    d.lMatrix.Add(m);

                    Boolean b = new DriverService().SaveDriver(d, true, "DB1").data;
                    if (!b)
                        s += d.sDriverName + "\r\n";
                }
            }
            if (s.Trim().Length > 0)
                s += "not saved\r\n";
            else
                s = "new Drivers saved\r\n";
            return s;
        }

        public void CreateAssets(String UnitID, String Desc)
        {
            Asset a = new Asset();

            AssetDeviceMap adm = new AssetDeviceMap();
            adm.DeviceID = UnitID;
            adm.Status_b = NaveoOneLib.Common.Constants.strActive;
            adm.oprType = DataRowState.Added;
            a.assetDeviceMap = adm;

            Matrix m = new Matrix();
            m.GMID = new GroupMatrixService().GetGroupMatrixByName("### All Clients ###", "DB1").gmID;
            m.oprType = DataRowState.Added;
            a.lMatrix.Add(m);

            a.AssetNumber = Desc;
            a.AssetType = 1;
            a.TimeZoneID = "Arabian Standard Time";
            new AssetService().Save(a, true, 1, "DB1");
        }
        public void UpdateCAComment(String strUnitID, String sComment)
        { }
        public void GetStruct(TreeView tvNode, List<int> iParentIDs, NaveoModels strStruc)
        {
            new GroupMatrixService().PopulateTreeCategories(tvNode, iParentIDs, strStruc, "DB1");
            tvNode.ExpandAll();
        }

        public Double CalcDistance(Double Lat1, Double Lon1, Double Lat2, Double Lon2)
        {
            return GeoCalc.CDistanceBetweenLocations.Calc(Lat1, Lon1, Lat2, Lon2);
        }

        public String ProcessDevices()
        {
            return new NaveoOneLib.Services.ProcessPendingService().ProcessDevices("DB1");
        }
        public String ProcessExceptions()
        {
            return new NaveoOneLib.Services.ProcessPendingService().ProcessExceptions("DB1");
        }
        public String ProcessRoadSpeed()
        {
            return new NaveoOneLib.Services.ProcessPendingService().updateRoadSpeed("DB1").ToString();
        }
        public String UpdateTripAddress()
        {
            BaseModel bm = new NaveoOneLib.Services.ProcessPendingService().updateTripAddress("DB1");
            if (bm.data)
                return bm.data.ToString();

            String sError = "dtErr : ";
            try
            {
                if (bm.dbResult != null)
                    if (bm.dbResult.Tables.Contains("dtErr"))
                    {
                        DataRow[] dRow = bm.dbResult.Tables["dtErr"].Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");
                        if (dRow.Length > 0)
                            sError += JsonConvert.SerializeObject(bm.dbResult.Tables["dtErr"], Formatting.Indented);
                    }
            }
            catch { }
            return sError;
        }

        public String ProcessFuel()
        {
            String s = String.Empty;
            try
            {
                BaseModel bm = new FuelService().ProcessFuelData("DB1");
                if (bm.data)
                    s = "Fuel processed.";
                else
                {
                    s = "Fuel not processed. " + bm.errorMessage;
                    if (bm.dbResult != null)
                        if (bm.dbResult.Tables.Contains("ResultCtrlTbl"))
                            foreach (DataRow dr in bm.dbResult.Tables["ResultCtrlTbl"].Rows)
                                foreach (DataColumn dc in bm.dbResult.Tables["ResultCtrlTbl"].Columns)
                                    s += "\r\n" + dc.ToString() + " > " + dr[dc].ToString();
                }
            }
            catch (Exception ex)
            {
                s = ex.ToString();
            }
            return s;
        }

        public String NotificationSender(Boolean bIsDevEnvironment)
        {
            Constants.bIsDevEnvironment = bIsDevEnvironment;
            return NotificationService.getNotificationData("DB1");
        }
        public String AutoReportSender()
        {
            String s = String.Empty;
            try
            {
                s += NotificationService.ProcessDriverLicenseExpiryNotif("DB1");
                s += NotificationService.SendMailsFromNotifTable("DB1");
            }
            catch { }

            s += new NaveoService.Services.AutomaticReportingService().ProcessAutomaticReports("DB1");
            return s;
        }

        public void InsertDriverID(String unitID, String striButtonID, DateTime dateTime, String sConnStr)
        {
            new DriverService().InsertDriverID(unitID, striButtonID, dateTime, sConnStr);
        }
        public DataSet GetDriverID(String unitID, String sConnStr)
        {
            return new DriverService().GetDriverID(unitID, sConnStr);
        }
        public void DeleteDriverID(String unitID, String sConnStr)
        {
            new DriverService().DeleteDriverID(unitID, sConnStr);
        }
    }
}