﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NavLib.Std
{
    /*
     GLL - Geographic Latitude and Longitude is a holdover from Loran data and some old units may not send the time and data active information if they are emulating Loran data. If a gps is emulating Loran data they may use the LC Loran prefix instead of GP.

     $GPGLL,4916.45,N,12311.12,W,225444,A,*1D
        Where:
            GLL          Geographic position, Latitude and Longitude
            4916.46,N    Latitude 49 deg. 16.45 min. North
            12311.12,W   Longitude 123 deg. 11.12 min. West
            225444       Fix taken at 22:54:44 UTC
            A            Data Active or V (void)
            *iD          checksum data
     Note that, as of the 2.3 release of NMEA, there is a new field in the GLL sentence at the end just prior to the checksum. For more information on this field see here.
    */
    public class DecodeGPGLL
    {
        const int oMessageID = 0;

        String myStrData = String.Empty;
        String[] myData;

        public DecodeGPGLL(String[] StrPacketData)
        {
            myData = StrPacketData;
        }
    }
}
