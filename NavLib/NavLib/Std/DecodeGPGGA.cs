﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NavLib.Std
{
    /*
        The most important NMEA sentences include the GGA which provides the current Fix data, the RMC which provides the minimum gps sentences information, and the GSA which provides the Satellite status data.

        GGA - essential fix data which provide 3D location and accuracy data.

            $GPGGA,123519,4807.038,N,01131.000,E,1,08,0.9,545.4,M,46.9,M,,*47
            Where:
                GGA          Global Positioning System Fix Data
                123519       Fix taken at 12:35:19 UTC
                4807.038,N   Latitude 48 deg 07.038' N
                01131.000,E  Longitude 11 deg 31.000' E
                1            Fix quality: 0 = invalid
                                        1 = GPS fix (SPS)
                                        2 = DGPS fix
                                        3 = PPS fix
			                            4 = Real Time Kinematic
			                            5 = Float RTK
                                        6 = estimated (dead reckoning) (2.3 feature)
			                            7 = Manual input mode
			                            8 = Simulation mode
                08           Number of satellites being tracked
                0.9          Horizontal dilution of position
                545.4,M      Altitude, Meters, above mean sea level
                46.9,M       Height of geoid (mean sea level) above WGS84
                                ellipsoid
                (empty field) time in seconds since last DGPS update
                (empty field) DGPS station ID number
                *47          the checksum data, always begins with *
        If the height of geoid is missing then the altitude should be suspect. Some non-standard implementations report altitude with respect to the ellipsoid rather than geoid altitude. Some units do not report negative altitudes at all. This is the only sentence that reports altitude.
     */

    public class DecodeGPGGA
    {        
        const int oMessageID = 0;
        const int oUtcTime = 1;
        const int oLat = 2;
        const int oNS = 3;
        const int oLon = 4;
        const int oEW = 5;
        const int oFixQ = 6;
        const int oNoOfSat = 7;
        const int oHorizontalDilution = 8;
        const int oAltitude = 9;
        const int oMeters = 10;
        const int oSeaLevelHeight = 11;
        const int oChecksum = 14;

        String myStrData = String.Empty;
        String[] myData;

        public DecodeGPGGA(String[] StrPacketData)
        {
            myData = StrPacketData;
        }

        public String GetFormat()
        {
            return myData[oMessageID];
        }
        public String GetTime()
        {
            return myData[oUtcTime];
        }
        public Double GetLat()
        {
            String g = myData[oLat].Trim();
            Double LatX = Double.Parse(g.Substring(0, 2));
            Double dblTempDegrees = Double.Parse(g.Substring(2)) / 60;
            LatX += dblTempDegrees;
            LatX *= -1;
            return LatX;
        }
        public String GetNS()
        {
            return myData[oNS];
        }
        public Double GetLon()
        {
            String g = myData[oLon].Trim();
            Double LatX = Double.Parse(g.Substring(0, 3));
            Double dblTempDegrees = Double.Parse(g.Substring(3)) / 60;
            LatX += dblTempDegrees;
            return LatX;
        }
        public String GetEW()
        {
            return myData[oEW];
        }
    }
}
