﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NavLib.Std
{
    /*
    RMC - NMEA has its own version of essential gps pvt (position, velocity, time) data. It is called RMC, The Recommended Minimum, which will look similar to:

    $GPRMC,123519,A,4807.038,N,01131.000,E,022.4,084.4,230394,003.1,W*6A

    Where:
         RMC          Recommended Minimum sentence C
         123519       Fix taken at 12:35:19 UTC
         A            Status A=active or V=Void.
         4807.038,N   Latitude 48 deg 07.038' N
         01131.000,E  Longitude 11 deg 31.000' E
         022.4        Speed over the ground in knots
         084.4        Track angle in degrees True
         230394       Date - 23rd of March 1994
         003.1,W      Magnetic Variation
         *6A          The checksum data, always begins with *
    */
    public class DecodeGPRMC
    {        
        const int oMessageID = 0;
        const int oUtcTime = 1;
        const int oStatus = 2;
        const int oLat = 3;
        const int oNS = 4;
        const int oLon = 5;
        const int oEW = 6;
        const int oSpeed = 7;
        const int oAngle = 8;
        const int oDate = 9;
        const int oMagneticVariation = 10;
        const int oVariationSense = 11;
        const int oMode = 12;
        const int oChecksum = 13;
        const int oEOT = 14;

        String myStrData = String.Empty;
        String[] myData;

        public DecodeGPRMC(String[] StrPacketData)
        {
            myData = StrPacketData;
        }

        public String GetFormat()
        {
            return myData[oMessageID];
        }
        public String GetTime()
        {
            return myData[oUtcTime];
        }
        public Boolean IsValid()
        {
            Boolean bResult = false;
            if (myData[oStatus] == "A")
                bResult = true;
            return bResult;
        }
        public Double GetLat()
        {
            String g = myData[oLat].Trim();
            Double LatX = Double.Parse(g.Substring(0, 2));
            Double dblTempDegrees = Double.Parse(g.Substring(2)) / 60;
            LatX += dblTempDegrees;
            LatX *= -1;
            return LatX;
        }
        public String GetNS()
        {
            return myData[oNS];
        }
        public Double GetLon()
        {
            String g = myData[oLon].Trim();
            Double LatX = Double.Parse(g.Substring(0, 3));
            Double dblTempDegrees = Double.Parse(g.Substring(3)) / 60;
            LatX += dblTempDegrees;
            return LatX;
        }
        public String GetEW()
        {
            return myData[oEW];
        }
        public Double GetSpeed()
        {
            if (myData[oSpeed].Length == 0)
                return 0;
            else
                return Convert.ToDouble(myData[oSpeed].ToString());
        }
        public String GetAngle()
        {
            return myData[oAngle];
        }
        public String GetDate()
        {
            return myData[oDate];
        }
        public String GetMagneticVariation()
        {
            return myData[oMagneticVariation];
        }
        public String GetMagneticVariationDirection()
        {
            return myData[oVariationSense];
        }
        public String GetFormatChecksum()
        {
            return myData[oVariationSense];
        }
        public String GetEOT()
        {
            return myData[oEOT];
        }
        public DateTime GetDateTime()
        {
            String dt = GetDate() + GetTime();
            int dd = Convert.ToInt16(dt.Substring(0, 2));
            int mm = Convert.ToInt16(dt.Substring(2, 2));
            int yy = 2000 + Convert.ToInt16(dt.Substring(4, 2));
            int hh = Convert.ToInt16(dt.Substring(6, 2));
            int min = Convert.ToInt16(dt.Substring(8, 2));
            int ss = Convert.ToInt16(dt.Substring(10, 2));

            DateTime dtResult = new DateTime(yy, mm, dd, hh, min, ss);
            return dtResult;
        }
    }
}
