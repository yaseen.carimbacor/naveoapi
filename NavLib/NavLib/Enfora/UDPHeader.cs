﻿using System;
using System.Collections.Generic;
using System.Text;
using NavLib.Globals;

namespace NavLib.Enfora
{
    public class UDPHeader
    {
        Byte[] MyPacketData = new Byte[20];


        public UDPHeader(Byte[] bytPacketData)
        {
            MyPacketData = Utils.CompressBytWorking(bytPacketData);
            String Msg = Encoding.ASCII.GetString(MyPacketData);
        }

        public int GetSourcePort()
        {
            ulong u = Utils.BuildLong(MyPacketData, 0, 1);
            return (int)u;
        }
        public int GetDestinationPort()
        {
            ulong u = Utils.BuildLong(MyPacketData, 2, 3);
            return (int)u;
        }
        public int GetUDPPacketLength()
        {
            ulong u = Utils.BuildLong(MyPacketData, 4, 5);
            return (int)u;
        }
        public int GetUDPChecksum()
        {
            ulong u = Utils.BuildLong(MyPacketData, 6, 7);
            return (int)u;
        }
    }
}