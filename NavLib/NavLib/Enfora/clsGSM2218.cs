using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace NavLib.Enfora
{
    public class clsGSM2218
    {
        // These constants are offsets into a report packet
        const int M_LogReasonIgnOnStart = 7;
        const int M_LogReasonIgnOnEnd = 7;
        const int M_LogReasonStart = 12;
        const int M_LogReasonEnd = 13;
        const int M_NameStart = 14;
        const int M_NameEnd = 35;
        const int M_TimeStart = 63;
        const int M_TimeEnd = 71;
        const int M_GPS_Valid = 73;
        const int M_LatStart = 75;
        const int M_LatEnd = 83;
        const int M_NorthSouth = 85;
        const int M_LongStart = 87;
        const int M_LongEnd = 96;
        const int M_EastWest = 98;
        const int M_SpeedStart = 100;
        const int M_SpeedEnd = 104;
        const int M_HeadingStart = 106;
        const int M_HeadingEnd = 110;
        const int M_DateStart = 112;
        const int M_DateEnd = 117;
        const int M_Speed = 999;
        const int M_Degrees = 999;


        // Class Level Data.
        Byte[] m_bytReportData;
        System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();

        public clsGSM2218(byte[] bytPacketData)
        {
            nfi.NumberDecimalSeparator = ".";
            m_bytReportData = bytPacketData;
        }

        public String GetLogReason()
        {
            // Z  ON
            // 64 ON
            // 65 OFF

            //New
            // 90 On
            // 95 Off
            // 85 Idle
            // 12 Driving
            String g = String.Empty;

            for (int i = M_LogReasonIgnOnStart; i <= M_LogReasonIgnOnEnd; i++)
                g += Convert.ToChar(m_bytReportData[i]);

            if (g.Trim() != "Z")
                for (int i = M_LogReasonStart; i <= M_LogReasonEnd; i++)
                    g += Convert.ToChar(m_bytReportData[i]);
            
            if (g.Trim() == "12")
                g += " IgnOn (Heading)";
            else if (g.Trim() == "85")
                g += " IgnOn (Time)";
            else if (g.Trim() == "90")
                g += " (IgnOn) (Ignition)";
            else if (g.Trim() == "95")
                g += " (IgnOff) (Ignition)";

            return g.Trim();
        }
        public String GetUnitName()
        {
            String g = String.Empty;
            for (int i = M_NameStart; i <= M_NameEnd; i++)
                g += Convert.ToChar(m_bytReportData[i]);

            //return g.Substring(0, 10).Trim();
            return g.Trim();
        }
        public Boolean IsGPSValid()
        {
            String g = (Convert.ToChar(m_bytReportData[M_GPS_Valid])).ToString();
            Boolean b = false;
            if (g.Trim() == "A")
                b = true;
            return b;
        }
        public Double GetLatitude()
        {
            String g = String.Empty;
            for (int i = M_LatStart; i <= M_LatEnd; i++)
                g += Convert.ToChar(m_bytReportData[i]);

            Double LatX = Double.Parse(g.Substring(0, 2), nfi);
            Double dblTempDegrees = Double.Parse(g.Substring(2), nfi) / 60;
            LatX += dblTempDegrees;
            LatX *= -1;
            return LatX;
        }
        public Double GetLongitude()
        {
            String g = String.Empty;
            for (int i = M_LongStart; i <= M_LongEnd; i++)
                g += Convert.ToChar(m_bytReportData[i]);

            Double LatX = Double.Parse(g.Substring(0, 3), nfi);
            Double dblTempDegrees = Double.Parse(g.Substring(3), nfi) / 60;
            LatX += dblTempDegrees;
            return LatX;
        }
        public DateTime GetDate()
        {
            String StrDate = String.Empty;
            for (int i = M_DateStart; i <= M_DateEnd; i++)
                StrDate += Convert.ToChar(m_bytReportData[i]);

            String StrTime = String.Empty;
            for (int i = M_TimeStart; i <= M_TimeEnd; i++)
                StrTime += Convert.ToChar(m_bytReportData[i]);

            DateTime dt = new DateTime(
                2000 + int.Parse(StrDate.Substring(4))
                , int.Parse(StrDate.Substring(2, 2))
                , int.Parse(StrDate.Substring(0, 2))
                , int.Parse(StrTime.Substring(0, 2))
                , int.Parse(StrTime.Substring(2, 2))
                , int.Parse(StrTime.Substring(4, 2))
                , int.Parse(StrTime.Substring(7, 2)));
            return dt;
        }
        public float GetSpeed()
        {
            String g = String.Empty;
            for (int i = M_SpeedStart; i <= M_SpeedEnd; i++)
                g += Convert.ToChar(m_bytReportData[i]);

            //Assumptions > speed is in miles. converting to km
            return float.Parse(g) * 1.609344F;
        }
        public float GetHeading()
        {
            String g = String.Empty;
            for (int i = M_HeadingStart; i <= M_HeadingEnd; i++)
                g += Convert.ToChar(m_bytReportData[i]);

            return float.Parse(g);
        }
        public Boolean IsIMSI()
        {
            Boolean bResult = false;
            String g = String.Empty;
            for (int i = 7; i <= 10; i++)
                g += Convert.ToChar(m_bytReportData[i]);

            if (g == "MID:")
                bResult = true;

            return bResult;
        }
        public String GetUnitNameFromIMSI()
        {
            String strUnitName = String.Empty; ;
            if (IsIMSI())
                for (int i = 13; i <= 27; i++)
                    strUnitName += Convert.ToChar(m_bytReportData[i]);

            return strUnitName.Trim();
        }
        public String GetIMSI()
        {
            String g = String.Empty;
            for (int i = 32; i <= 46; i++)
                g += Convert.ToChar(m_bytReportData[i]);

            return g.Trim();
        }
        public String GetUnitNameFromIMSIObsolete()
        {
            String strUnitName = String.Empty; ;
            //String g = String.Empty;
            //for (int i = 7; i <= 10; i++)
            //    g += Convert.ToChar(m_bytReportData[i]);

            //if (g == "MID:")
            if (IsIMSI())
                for (int i = 13; i <= 27; i++)
                    strUnitName += Convert.ToChar(m_bytReportData[i]);
            //else
            //    strUnitName = "000";

            return strUnitName.Trim();
        }
    }
}
