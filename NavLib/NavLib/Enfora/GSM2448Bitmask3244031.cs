﻿using System;
using System.Collections.Generic;
using System.Text;
using NavLib.Globals;

namespace NavLib.Enfora
{
    public class GSM2448Bitmask3244031
    {
        Byte[] MyPacketData;
        System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();

        public GSM2448Bitmask3244031(Byte[] bytPacketData)
        {
            //First tests showing 36 bytes
            MyPacketData = Utils.CompressBytWorking(bytPacketData);
        }

        public String GPIO()
        {
            String hexValue = Utils.BytesToDoubleHex(MyPacketData, 0, 1);
            int i = Utils.HexToDecimal(hexValue);
            String binaryValue = Utils.DecimalToBinary(i, 16);
            return binaryValue;
        }

        public Boolean isIgnitionOn()
        {
            return Utils.GetBitValue(GPIO(), 15);
        }
        public Boolean isGPSOn()
        {
            return Utils.GetBitValue(GPIO(), 14);
        }
        public Boolean isGSMOn()
        {
            return Utils.GetBitValue(GPIO(), 13);
        }
        public Boolean isGP5On()
        {
            return Utils.GetBitValue(GPIO(), 12);
        }
        public Boolean isPwrOn()
        {
            return Utils.GetBitValue(GPIO(), 11);
        }
        public Boolean isGP3On()
        {
            return Utils.GetBitValue(GPIO(), 10);
        }
        public Boolean isGP2On()
        {
            return Utils.GetBitValue(GPIO(), 9);
        }
        public Boolean isGP1On()//Aux1
        {
            return !Utils.GetBitValue(GPIO(), 8);
        }
        public Boolean isGP9On()//Aux2
        {
            return !Utils.GetBitValue(GPIO(), 0);
        }
        public Boolean isGP10On()//Aux3
        {
            return !Utils.GetBitValue(GPIO(), 1);
        }

        public int GetInputEventNo()
        {
            String hexValue = Utils.BytesToDoubleHex(MyPacketData, 6, 6);
            return Utils.HexToDecimal(hexValue);
        }

        String GetGPSDate()
        {
            String hexValue = Utils.BytesToDoubleHex(MyPacketData, 7, 9);
            return Utils.HexToDecimal(hexValue).ToString();
        }
        String GetGPSTime()
        {
            String hexValue = Utils.BytesToDoubleHex(MyPacketData, 22, 24);
            return Utils.HexToDecimal(hexValue).ToString();
        }
        public DateTime GetRTC()
        {
            String hexValue = Utils.BytesToDoubleHex(MyPacketData, 30, 35);
            int yy = Utils.HexToDecimal(hexValue.Substring(0, 2));
            yy = 2000 + yy;
            int mm = Utils.HexToDecimal(hexValue.Substring(2, 2));
            int dd = Utils.HexToDecimal(hexValue.Substring(4, 2));
            int HH = Utils.HexToDecimal(hexValue.Substring(6, 2));
            int MM = 0;
            if (hexValue.Length > 8)
                MM = Utils.HexToDecimal(hexValue.Substring(8, 2));
            //Sometimes last bit is not in message
            int SS = 0;
            if (hexValue.Length > 10)
                SS = Utils.HexToDecimal(hexValue.Substring(10, 2));

            DateTime dt = new DateTime(yy, mm, dd, HH, MM, SS);
            if (dt < DateTime.Now.AddDays(1023 * -7))
                dt = dt.AddDays(1024 * 7);

            return dt;
        }
        public int GetGPSStatus()
        {
            //1 - Valid Fix
            String hexValue = Utils.BytesToDoubleHex(MyPacketData, 10, 10);
            return Utils.HexToDecimal(hexValue);
        }
        public int GetNoOfSat()
        {
            String hexValue = Utils.BytesToDoubleHex(MyPacketData, 25, 25);
            return Utils.HexToDecimal(hexValue);

        }
        public Double GetLatitude()
        {
            Double LatX = 0.0;
            if (GetGPSStatus() >= 1)
            {
                String hexValue = Utils.BytesToDoubleHex(MyPacketData, 11, 13);
                int i = Utils.HexToDecimal(hexValue);
                String strBinary = Utils.DecimalToBinary(i, 4);

                Boolean isNegative = false;
                char c = strBinary[0];
                if (c.ToString() == "0")
                    isNegative = false;
                else if (c.ToString() == "1")
                    isNegative = true;

                strBinary = Utils.BitwiseComplement(strBinary);
                i = (int)Utils.BinaryToDecimal(strBinary);

                String g = i.ToString();
                String degrees = g.Substring(0, 2);
                String min = g.Substring(2, 2) + "." + g.Substring(4);
                LatX = Double.Parse(min, nfi);
                LatX = LatX / 60;
                LatX += Convert.ToDouble(degrees);
                if (isNegative)
                    LatX = LatX * -1;
                LatX = Math.Round(LatX, 6);
            }
            return LatX;
        }
        public Double GetLongitude()
        {
            Double LatX = 0.0;
            if (GetGPSStatus() >= 1)
            {
                String hexValue = Utils.BytesToDoubleHex(MyPacketData, 14, 17);
                int i = Utils.HexToDecimal(hexValue);
                String strBinary = Utils.DecimalToBinary(i, 4);

                Boolean isNegative = false;
                char c = strBinary[0];
                if (c.ToString() == "0")
                    isNegative = false;
                else if (c.ToString() == "1")
                    isNegative = true;

                String g = i.ToString();
                String degrees = g.Substring(0, 2);
                String min = g.Substring(2, 2) + "." + g.Substring(4);
                LatX = Double.Parse(min, nfi);
                LatX = LatX / 60;
                LatX += Convert.ToDouble(degrees);
                if (isNegative)
                    LatX = LatX * -1;
                LatX = Math.Round(LatX, 6);
            }
            return LatX;
        }
        public Double GetSpeed()
        {
            //Speed is in knot. 1 knot = 1.852km
            String hexValue = Utils.BytesToDoubleHex(MyPacketData, 18, 19);
            float knot = (float)(Utils.HexToDecimal(hexValue) / 10);
            return knot * 1.852;
        }
        public float GetHeading()
        {
            String hexValue = Utils.BytesToDoubleHex(MyPacketData, 20, 21);
            return (float)Utils.HexToDecimal(hexValue) / 10;
        }
        public float GetAD1()
        {
            String hexValue = Utils.BytesToDoubleHex(MyPacketData, 2, 3);
            return (float)Utils.HexToDecimal(hexValue);
        }
        public float GetAD2()
        {
            String hexValue = Utils.BytesToDoubleHex(MyPacketData, 4, 5);
            return (float)Utils.HexToDecimal(hexValue);
        }
        public float GetGPSOdo()
        {
            String hexValue = Utils.BytesToDoubleHex(MyPacketData, 26, 29);
            return (float)Utils.HexToDecimal(hexValue);
        }

        public Double GetTemperature1()
        {
            //03 00 1F or just 01
            //03 = 3 Bytes, including 03
            //00 1F = Data in decimal
            Double dResult = 999.0;     //An invalid value
            int iStartByte = 36;

            String hexValue = Utils.BytesToDoubleHex(MyPacketData, iStartByte, iStartByte);
            int i = Utils.HexToDecimal(hexValue);

            hexValue = Utils.BytesToDoubleHex(MyPacketData, iStartByte, iStartByte + i - 1);
            int icount = 0;
            String strDecode = String.Empty;
            foreach (char c in hexValue)
            {
                icount++;
                if (icount <= 2) //1st 2 is No. of bytes
                    strDecode += c;
            }

            icount = Utils.HexToDecimal(strDecode);
            hexValue = Utils.BytesToDoubleHex(MyPacketData, iStartByte + 1, iStartByte + icount - 1);
            strDecode = String.Empty;
            foreach (char c in hexValue)
                strDecode += c;

            if (strDecode != String.Empty)
            {
                if (strDecode.Substring(0, 1) == "F")
                    dResult = Utils.hexToTwosComplement(strDecode) * -1;
                else
                    dResult = Utils.HexToDecimal(strDecode);
                dResult = dResult / 10;
            }
            return dResult;
        }
        public Double GetTemperature2()
        {
            //03 00 1F or just 01
            //03 = 3 Bytes, including 03
            //00 1F = Data in decimal
            Double dResult = 999.0;
            int iStartByte = 36;

            //Temperature 1
            String hexValue = Utils.BytesToDoubleHex(MyPacketData, iStartByte, iStartByte);
            int i = Utils.HexToDecimal(hexValue);

            hexValue = Utils.BytesToDoubleHex(MyPacketData, iStartByte, iStartByte + i - 1);
            int icount = 0;
            String strDecode = String.Empty;
            foreach (char c in hexValue)
            {
                icount++;
                if (icount <= 2) //1st 2 is No. of bytes
                    strDecode += c;
            }
            icount = Utils.HexToDecimal(strDecode);

            //Temperature2
            iStartByte = iStartByte + icount;
            hexValue = Utils.BytesToDoubleHex(MyPacketData, iStartByte, iStartByte);
            i = Utils.HexToDecimal(hexValue);

            hexValue = Utils.BytesToDoubleHex(MyPacketData, iStartByte, iStartByte + i - 1);
            icount = 0;
            strDecode = String.Empty;
            foreach (char c in hexValue)
            {
                icount++;
                if (icount <= 2) //1st 2 is No. of bytes
                    strDecode += c;
            }

            icount = Utils.HexToDecimal(strDecode);
            hexValue = Utils.BytesToDoubleHex(MyPacketData, iStartByte + 1, iStartByte + icount - 1);
            strDecode = String.Empty;
            foreach (char c in hexValue)
                strDecode += c;

            if (strDecode != String.Empty)
            {
                if (strDecode.Substring(0, 1) == "F")
                    dResult = Utils.hexToTwosComplement(strDecode) * -1;
                else
                    dResult = Utils.HexToDecimal(strDecode);
                dResult = dResult / 10;
            }
            return dResult;
        }
    }
}