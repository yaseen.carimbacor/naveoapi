﻿using System;
using System.Collections.Generic;
using System.Text;
using NavLib.Globals;

namespace NavLib.Enfora
{
    public class IPHeader
    {
        Byte[] MyPacketData = new Byte[20];

        public IPHeader(Byte[] bytPacketData)
        {
            MyPacketData = Utils.CompressBytWorking(bytPacketData);
            String Msg = Encoding.ASCII.GetString(MyPacketData);
        }

        public int GetVersionLength()
        {
            return (int)MyPacketData[0];
        }
        public int GetTypeOfService()
        {
            return (int)MyPacketData[1];
        }
        public int GetLengthOfIPPacket()
        {
            ulong u = Utils.BuildLong(MyPacketData, 2, 3);
            return (int)u;
        }
        public int GetPacketNo()
        {
            ulong u = Utils.BuildLong(MyPacketData, 4, 5);
            return (int)u;
        }
        public int GetFragmentationOffset()
        {
            ulong u = Utils.BuildLong(MyPacketData, 6, 7);
            return (int)u;
        }
        public int GetProtocol()
        {
            return (int)MyPacketData[9];
        }
        public int GetChecksum()
        {
            ulong u = Utils.BuildLong(MyPacketData, 10, 11);
            return (int)u;
        }
        public String GetSourceIP()
        {
            String strResult = String.Empty;
            for (int i = 12; i <= 15; i++)
                strResult += ((int)MyPacketData[i]).ToString() + ".";
            return strResult.Substring(0, strResult.Length - 1);
        }
        public String GetDestinationIP()
        {
            String strResult = String.Empty;
            for (int i = 16; i <= 19; i++)
                strResult += ((int)MyPacketData[i]).ToString() + ".";
            return strResult.Substring(0, strResult.Length - 1);
        }
    }
}
