﻿using System;
using System.Collections.Generic;
using System.Text;
using NavLib.Globals;

namespace NavLib.Enfora
{
    public class DecodeEnfora
    {
        /*
        //Bitmask 3244031 Report No API Header (bit 3 = 0)
        0	00
        1	05	API Parameter	5
        2	02
        3	00  00 API Header Length
        4	00  
        5	00
        6	08     
        7	FC      BIT 01 0008FC
        8	20	MDMID S
        9	20
        10	20
        11	20
        12	20
        13	20
        14	30
        15	31
        16	32
        17	39
        18	38
        19	31
        20	30
        21	30
        22	30
        23	34
        24	39
        25	30
        26	31
        27	30
        28	39
        29	20	BIT 02 MDMID end ASCII -012981000490109(Hex to string)
        30	C0  Data Starts Here. Above was header
        31	97	BIT 03 C097-GPIO status-1100000010010111
        32	00 
        33	00      BIT 04 0000 AD1:0
        34	00      
        35	00      BIT 05 0000 AD2:0
        36	10      BIT 07 10 INPUT EVENT:16
        37	03
        38	5F
        39	55      BIT 08 035F55 GPS date:221013
        40	01      BIT 09 01 GPS status - 1-Valid fix
        41	E1 
        42	4A
        43	9A      BIT 10 E14A9A - GPS Latitude -??
        44	00
        45	57
        46	69
        47	C6      BIT 11 005769C6 - GPS Longitude - ??
        48	01   
        49	06      BIT 12 0106 -GPS Speed-262
        50	08     
        51	A4      BIT 13 084A -GPS heading- 2122
        52	01
        53	FF
        54	6E	BIT 14 01FF6E -GPS Time: 130926
        55	0A      BIT 16 0A-Bit 16- GPS No of satellites: 10
        56	00     
        57	00
        58	1E
        59	C3	BIT 20 00001EC3 - Bit 20-GPS Odometer: 7875
        60	0D	
        61	0A
        62	16	
        63	0D
        64	09
        65	1A      BIT 21 0D0A160D091A - Bit 21 (0D:13 0A:10 16:22 0D:13 09:9 1A:26)
        */
        /*
        //IMSI > 0x00010400AT$MDMID?;+cimi
        00
        01   0001	Informational
        05   05	AT command Response
        00
        244D444D49443A202230313239383130303034393031303922200D0A3631373130303136323130303832340D0A0D0A4F4B0D0A00000000000D0A1F100613    Data
        */
        /*
        //Bitmask 3244031 Report with API Header (bit 3 = 23) all bits then shift with 23 bits
        0	00
        1	05	API Parameter	5
        2	02	HDR	API_COMMAND_TYPE	2 - General Status Information	02
        3	17	HDR	API_OPT_HDR_LENGTH	23				17
        4	11	OPT	OPT_HDR_MODEM		012981000490109			1101303132393831303030343930313039
        5	01
        6	30
        7	31
        8	32
        9	39
        10	38
        11	31
        12	30
        13	30
        14	30
        15	34
        16	39
        17	30
        18	31
        19	30
        20	39
        21	06	OPT	OPT_HDR_DRIVER_ID	00000000			060800000000
        22	08
        23	00
        24	00
        25	00
        26	00
        27	00	1	USER_NUMBER_PARAM1	40				00000028
        28	00
        29	00
        30	28
        31	20	2	MODEM			012981000490109			20202020202030313239383130303034393031303920
        32	20
        33	20
        34	20
        35	20
        36	20
        37	30
        38	31
        39	32
        40	39
        41	38
        42	31
        43	30
        44	30
        45	30
        46	34
        47	39
        48	30
        49	31
        50	30
        51	39
        52	20	ModemEnds
        53	C0	3	GPIO			1100 0000 1001 1111		C09F
        54	9F
        55	00	BIT 04 0000 AD1:0
        56	00
        57	00	others
        58	00
        59	1E
        60	01
        61	15
        62	C9
        63	01
        64	E1
        65	48
        66	30
        67	00
        68	57
        69	67
        70	7B
        71	00
        72	00
        73	00
        74	00
        75	02
        76	27
        77	9B
        78	08
        79	00
        80	00
        81	09
        82	FF
        83	0D
        84	0B
        85	07
        86	0E
        87	0C
        88	0C
        */

        //Header
        const int oApiNoStart = 0;
        const int oApiNoEnd = 1;
        const int oApiCmd = 2;
        const int oApiHrdLen = 3;
        const int oApiOptHrd = 4;
        //Data
        int oParam1Start = 4;
        int oParam1End = 7;
        int oMdMidStart = 8;
        int oMdMidEnd = 29;
        //Report
        int oReportStart = 30;

        //InformationalData
        int idMdMidStart = 13;
        int idoMdMidEnd = 27;
        int idIMSIStart = 31;
        int idIMSISEnd = 46;

        Byte[] MyPacketData;

        public GSM2448Bitmask3244031 gsm2448Bitmask3244031;
        public OptionalHeader OptHdr;

        public DecodeEnfora(byte[] bytPacketData)
        {
            MyPacketData = Utils.CompressBytWorking(bytPacketData);

            int iApiHdrLen = GetAPIHeaderLen();
            oParam1Start += iApiHdrLen;
            oParam1End += iApiHdrLen;
            oMdMidStart += iApiHdrLen;
            oMdMidEnd += iApiHdrLen;
            oReportStart += iApiHdrLen;

            idMdMidStart += iApiHdrLen;
            idoMdMidEnd += iApiHdrLen;
            idIMSIStart += iApiHdrLen;
            idIMSISEnd += iApiHdrLen;

            if (!isInformationalData())
            {
                int iLen = MyPacketData.Length - oReportStart;  //iLen should be 36
                Byte[] bReport = new Byte[iLen];
                Array.Copy(MyPacketData, oReportStart, bReport, 0, iLen);
                gsm2448Bitmask3244031 = new GSM2448Bitmask3244031(bReport);
                //String fg = Utils.BytesToDoubleHexSeperate(bReport);

                //Optional Header
                int iOptHdrLen = GetAPIHeaderLen();
                if (iOptHdrLen > 0)
                {
                    Byte[] bOptHdr = new Byte[iOptHdrLen];
                    Array.Copy(MyPacketData, oApiOptHrd, bOptHdr, 0, iOptHdrLen);
                    String g = Utils.BytesToDoubleHex(bOptHdr);

                    OptHdr = new OptionalHeader(bOptHdr);
                }
                else
                    OptHdr = new OptionalHeader(new Byte[1]);
            }
        }

        public String GetParam1()
        {
            ulong u = Utils.BuildLong(MyPacketData, oParam1Start, oParam1End);

            String strResult = String.Empty;
            String str = u.ToString();
            switch (str.Trim())
            {
                case "10":
                    strResult = "10 (ModemReset) Modem power up";
                    break;
                case "11":
                    strResult = "11 (Reset) (main power lost)";
                    break;
                case "12":
                    strResult = "12 (Reset) (main power up)";
                    break;
                case "20":
                    strResult = "20 (IgnOff) (Ignition)";
                    break;
                case "30":
                    strResult = "30 IgnOn (Ignition)";
                    break;
                case "32":
                    strResult = "32 (Aux) (Aux1On) -VeOff";
                    break;
                case "31":
                    strResult = "31 (Aux) (Aux1Off) -VeOn";
                    break;
                case "34":
                    strResult = "34 (Aux) (Aux2On) -VeOff";
                    break;
                case "33":
                    strResult = "33 (Aux) (Aux2Off) -VeOn";
                    break;
                case "36":
                    strResult = "36 (Aux) (Aux3On) -VeOff";
                    break;
                case "35":
                    strResult = "35 (Aux) (Aux3Off) -VeOn";
                    break;
                case "40":
                    strResult = "40 (Time) Idle";
                    break;
                case "50":
                    strResult = "50 TimeDistance (Heading)";
                    break;
                case "51":
                    strResult = "51 Distance (Heading)";
                    break;
                case "52":
                    strResult = "52 Heartbeat (Heartbeat)";
                    break;
                case "70":
                    strResult = "70 (HarshAS) Harsh Acc (HarshAS_Start)";
                    break;
                case "71":
                    strResult = "71 (HarshAS) Harsh Acc (HarshAS_End)";
                    break;
                case "72":
                    strResult = "72 (HarshBS) Harsh brake (HarshBS_Start)";
                    break;
                case "73":
                    strResult = "73 (HarshBS) Harsh Brake (HarshBS_End)";
                    break;
                case "80":
                    strResult = "80 (PassengerID) iButton PassengerID ";
                    try
                    {
                        strResult += "[" + OptHdr.GetDriverID().Trim() + "]";
                    }
                    catch { }
                    break;
                case "2601":
                    strResult = "2601 HEADING CHANGE -VE (Heading)";
                    break;
                case "2701":
                    strResult = "2701 HEADING CHANGE +VE (Heading)";
                    break;
                #region unused
                case "60i":
                    strResult = "60 Motion (???)";
                    break;
                case "65i":
                    strResult = "65 Motion (???)";
                    break;
                case "70i":
                    strResult = "70 Motion (???)";
                    break;
                case "71i":
                    strResult = "71 Motion (???)";
                    break;
                case "72i":
                    strResult = "72 Motion (???)";
                    break;
                case "73i":
                    strResult = "73 Motion (???)";
                    break;
                case "10i":
                    strResult = "10 DriverID (DriverID)";
                    break;
                case "106i":
                    strResult = "106 (Reset) main power reconnect";
                    break;
                case "107i":
                    strResult = "107 (HarshAS) Harsh Acc (HarshAS_Start)";
                    break;
                case "108i":
                    strResult = "108 (HarshAS) Harsh Acc (HarshAS_End)";
                    break;
                case "109i":
                    strResult = "109 (HarshBS) Harsh brake (HarshBS_Start)";
                    break;
                case "110i":
                    strResult = "110 (HarshBS) Harsh Brake (HarshBS_End)";
                    break;
                #endregion
                default:
                    strResult = str + "(???)";
                    break;
            }

            return str + " > " + strResult;
        }
        String GetAPINumber()
        {
            //0005	Binary Event Data
            //0001	Informational
            return Utils.BytesToDoubleHex(MyPacketData, oApiNoStart, oApiNoEnd);
        }
        String GetAPICommandType()
        {
            //02	General information Status  //0005	Binary Event Data
            //05	AT command Response         //0001	Informational
            //04	AT command Request          //0001	Informational
            return Utils.BytesToDoubleHex(MyPacketData, oApiCmd, oApiCmd);
        }
        int GetAPIHeaderLen()
        {
            String hexValue = Utils.BytesToDoubleHex(MyPacketData, oApiHrdLen, oApiHrdLen);
            return Utils.HexToDecimal(hexValue);
        }
        public Boolean isInformationalData()
        {
            //IMSI data for example
            //Binary Event Data                 000502
            //Informational AT command Request  000104
            //Informational AT command Response 000105
            
            Boolean bResult = false;
            if (GetAPINumber() + GetAPICommandType() == "000105")
                bResult = true;
            return bResult;
        }
        public String GetMdMid()
        {
            String g = String.Empty;
            if (isInformationalData())
                g = Utils.BytesToDoubleHex(MyPacketData, idMdMidStart, idoMdMidEnd);
            else
                g = Utils.BytesToDoubleHex(MyPacketData, oMdMidStart, oMdMidEnd);

            return Utils.Hex2ASCII(g).Trim();
        }
        public String GetIMSI()
        {
            String g = String.Empty;
            if (isInformationalData())
                g = Utils.BytesToDoubleHex(MyPacketData, idIMSIStart, idIMSISEnd);

            return Utils.Hex2ASCII(g).Trim();
        }
    }
}
