﻿using System;
using System.Collections.Generic;
using System.Text;
using NavLib.Globals;

namespace NavLib.Enfora
{
    public class OptionalHeader
    {
        Byte[] MyPacketData;
        String strDecodeData = String.Empty;
        System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();

        public OptionalHeader(Byte[] bytPacketData)
        {
            if (bytPacketData.Length == 1)
                return;

            MyPacketData = bytPacketData;
            String g = Utils.BytesToDoubleHex(MyPacketData);
            int iLen = bytPacketData.Length;
            int iStart = 0;

            for (int i = iStart; i < iLen; )
            {
                String hexValue = Utils.BytesToDoubleHex(MyPacketData, i, i);
                int iMyLen = Utils.HexToDecimal(hexValue);

                Byte[] bOptHdr = new Byte[iMyLen];
                Array.Copy(MyPacketData, i, bOptHdr, 0, iMyLen);
                g = Utils.BytesToDoubleHex(bOptHdr);
                strDecodeData += DecodeOptHdr(bOptHdr);

                i += iMyLen;
            }
        }

        String DecodeOptHdr(Byte[] bOphHdr)
        {
            String strResult = String.Empty;

            //1st byte = Length
            String hexValue = Utils.BytesToDoubleHex(bOphHdr, 0, 0);
            int iMyLen = Utils.HexToDecimal(hexValue);

            //2nd byte = Definition
            hexValue = Utils.BytesToDoubleHex(bOphHdr, 1, 1);
            int iMyDef = Utils.HexToDecimal(hexValue);
            strResult = GetOptHdrType(iMyDef);

            //Data
            hexValue = Utils.BytesToDoubleHex(bOphHdr, 2, iMyLen - 1);
            if (strResult == "OPT_HDR_iButton")
                strResult = strResult + " : " + hexValue.Trim() + ";";
            else if (strResult == "OPT_HDR_MODEM")
                strResult = strResult + " : " + Utils.HexString2Ascii(hexValue).Trim() + ";";
            else
                strResult = strResult + " : " + Utils.HexToDecimal(hexValue).ToString() + ";";

            return strResult;
        }
        String GetOptHdrType(int iType)
        {
            String strResult = String.Empty;
            switch (iType)
            {
                case 0:
                    strResult = "End of Options Sequence";
                    break;

                case 1:
                    strResult = "OPT_HDR_MODEM";
                    break;

                case 2:
                    strResult = "OPT_HDR_FORMAT_CODE_PARAM2 BitMask";
                    break;

                case 3:
                    strResult = "OPT_HDR_SEQ_NBR";
                    break;

                case 8:
                    strResult = "OPT_HDR_iButton";
                    break;

                case 9:
                    strResult = "OPT_HDR_FORMAT_CODE_PARAM3";
                    break;

                default:
                    strResult = iType.ToString();
                    break;
            }

            return strResult;
        }

        String GetData(String strType)
        {
            String strResult = String.Empty;
            String[] strData = strDecodeData.Split(';');
            foreach (String g in strData)
            {
                String[] pDesc = g.Split(':');
                if (pDesc[0].Trim() == strType.Trim())
                {
                    strResult = pDesc[1];
                    break;
                }
            }
            return strResult;
        }
        public String GetDriverID()
        {
            String s = GetData("OPT_HDR_iButton");
            try
            {
                s = s.Trim().Substring(2, 12);
            }
            catch { }
            return s;
        }
        public String GetModemID()
        {
            return GetData("OPT_HDR_MODEM");
        }
        public String GetBitmask()
        {
            return GetData("OPT_HDR_FORMAT_CODE_PARAM2 BitMask");
        }
        public String GetSeqNo()
        {
            return GetData("OPT_HDR_SEQ_NBR");
        }
        public String GetParam3()
        {
            return GetData("OPT_HDR_FORMAT_CODE_PARAM3");
        }
    }
}
