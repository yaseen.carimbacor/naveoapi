﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace NavLib.myXML
{
    public class FuelXml
    {
        public static int[,] GetXMLValue(String XMLPath, String SensorNo)
        {
            int[,] iResult = new int[1,1];
            XmlDocument doc = new XmlDocument();
            doc.Load(XMLPath);
            XmlNodeList nodes = doc.DocumentElement.SelectNodes("sensor");

            foreach (XmlNode node in nodes)
            {
                XmlAttributeCollection nodeAtt = node.Attributes;
                if (nodeAtt["number"].Value.ToString() == SensorNo)
                {
                    XmlDocument childNode = new XmlDocument();
                    childNode.LoadXml(node.OuterXml);

                    XmlDocument inner = new XmlDocument();
                    inner.LoadXml(childNode.InnerXml);
                    XmlNodeList innernodes = inner.DocumentElement.SelectNodes("value");
                    int i = 0;
                    iResult = new int[innernodes.Count, 2];    //ADC, Liters
                    foreach (XmlNode innernode in innernodes)
                    {
                        XmlAttributeCollection innernodeAtt = innernode.Attributes;
                        iResult[i, 0] = Convert.ToInt32(innernodeAtt["code"].Value.ToString());
                        iResult[i, 1] = Convert.ToInt32(innernode.InnerText);
                        i++;
                    }
                }
            }

            return iResult;
        }

        public static int GetVolume(int[,] MyCallitrationTable, int searchValue)
        {
            int currentADC = 0, previousADC = 0;
            int volume = 0, previousVolume = 0;
            Boolean bExactlyEquals = false;

            for (int i = 0; i < MyCallitrationTable.GetLength(0); i++)
            {
                if (MyCallitrationTable[i, 0] == searchValue)
                {
                    bExactlyEquals = true;
                    volume = MyCallitrationTable[i, 1];
                }
                else if (MyCallitrationTable[i, 0] < searchValue)
                    currentADC = MyCallitrationTable[i, 0];
                else if (MyCallitrationTable[i, 0] > searchValue && i > 0 && MyCallitrationTable[i - 1, 0] < searchValue)
                {
                    previousADC = MyCallitrationTable[i - 1, 0];
                    currentADC = MyCallitrationTable[i, 0];

                    previousVolume = MyCallitrationTable[i - 1, 1];
                    volume = MyCallitrationTable[i, 1];
                }
            }

            if (bExactlyEquals)
                return volume;

            Double itmp = currentADC - searchValue;
            itmp = itmp / (currentADC - previousADC);
            itmp = itmp * (volume - previousVolume);
            itmp = volume - itmp;

            return (int)itmp;
        }
        public static int GetDiff(int[,] MyCallitrationTable, int searchValue)
        {
            int currentNearest = MyCallitrationTable[0, 0];
            int volume = 0;
            Double currentDifference = Math.Abs(currentNearest - searchValue);

            for (int i = 1; i < MyCallitrationTable.GetLength(0); i++)
            {
                Double diff = Math.Abs(MyCallitrationTable[i, 0] - searchValue);
                if (diff < currentDifference)
                {
                    currentDifference = diff;
                    currentNearest = MyCallitrationTable[i, 0];
                    volume = MyCallitrationTable[i, 1];
                }
            }
            return volume;
        }
    }
}