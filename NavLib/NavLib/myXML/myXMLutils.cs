﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;

namespace NavLib.myXML
{
    public class myXMLutils
    {
        public static String FormatXml(String XMLPath)
        {
            XmlDocument document = new XmlDocument();
            document.Load(XMLPath);
            //document.Load(new StringReader(XMLPath));

            StringBuilder builder = new StringBuilder();
            using (XmlTextWriter writer = new XmlTextWriter(new StringWriter(builder)))
            {
                writer.Formatting = Formatting.Indented;
                document.Save(writer);
            }

            return builder.ToString();
        }

        public static void WriteXml(String XMLPath, String xmlData)
        {
            XmlWriterSettings settings = new XmlWriterSettings { Indent = true, NewLineOnAttributes = true };
            
            using (var writer = XmlWriter.Create(XMLPath, settings))
            {
                writer.WriteRaw(xmlData);
            }
        }
    }
}
