using System;
using System.Collections.Generic;
using System.Text;
using NavLib.Globals;

namespace NavLib.Data
{
    public class GeotabSystemHelper
    {
        Runme _Runme = new Runme();

        public void UpdateDB(String sConnStr)
        {
            String sql = String.Empty;
            if ((int)_Runme.GetDataSet(SqlCmds.ExistTableSql("NaveoDbUpdate"), sConnStr).Tables[0].Rows[0][0] == 0)
                _Runme.RunSQLCMD(SqlCmds.CreateNaveoDbUpdate(), sConnStr, -1);

            #region Update 1
            if (!CheckUpdateExist("DBUPD00001", sConnStr))
            {
                sql = "CREATE TABLE [dbo].[NaveoFuelData](";
                sql += "\r\n    [iID] [int] IDENTITY(1,1) NOT NULL,";
                sql += "\r\n    [MessageType] [int] NOT NULL,";
                sql += "\r\n    [dtDateTime] [datetime] NULL,";
                sql += "\r\n    [DataID] [int] NOT NULL,";
                sql += "\r\n    [DataIDFrom] [int] NOT NULL CONSTRAINT [DF_FuelData_DateIDFrom]  DEFAULT ((-1)),";
                sql += "\r\n    [DataIDTo] [int] NOT NULL CONSTRAINT [DF_FuelData_DataIDTo]  DEFAULT ((-1)),";
                sql += "\r\n    [FuelID] [int] NOT NULL,";
                sql += "\r\n    [unitID] [varchar](20) NOT NULL,";
                sql += "\r\n    [sensorNo] [int] NOT NULL,";
                sql += "\r\n    [RawADC] [int] NOT NULL CONSTRAINT [DF_FuelData_RawADC]  DEFAULT ((-1)),";
                sql += "\r\n    [UsedADC] [int] NOT NULL CONSTRAINT [DF_FuelData_UsedADC]  DEFAULT ((-1)),";
                sql += "\r\n    [FuelLvl] [int] NOT NULL,";
                sql += "\r\n    [NotifCode] [int] NOT NULL CONSTRAINT [DF_FuelData_NotifCode]  DEFAULT ((-1)),";
                sql += "\r\n	[Status] [int] NOT NULL DEFAULT (0)";
                sql += "\r\n CONSTRAINT [PK_NaveoFuelData] PRIMARY KEY CLUSTERED ";
                sql += "\r\n(";
                sql += "\r\n    [iID] ASC";
                sql += "\r\n)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]";
                sql += "\r\n) ON [PRIMARY]";
                _Runme.RunSQLCMD(sql, sConnStr, -1);

                sql = "CREATE TABLE [dbo].[NaveoFuelMessageTypes](";
                sql += "\r\n    [MessageType] [int] NOT NULL,";
                sql += "\r\n    [Description] [varchar](100) NOT NULL,";
                sql += "\r\n    [IsNotif] [tinyint] NOT NULL,";
                sql += "\r\nCONSTRAINT [PK_NaveoFuelMessageTypes] PRIMARY KEY CLUSTERED ";
                sql += "\r\n (";
                sql += "\r\n    [MessageType] ASC,";
                sql += "\r\n    [IsNotif] ASC";
                sql += "\r\n    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]";
                sql += "\r\n    ) ON [PRIMARY]";
                _Runme.RunSQLCMD(sql, sConnStr, 4000);

                sql = "INSERT INTO [dbo].[NaveoFuelMessageTypes]";
                sql += "\r\n    ([MessageType]";
                sql += "\r\n    ,[Description]";
                sql += "\r\n    ,[IsNotif])";
                sql += "\r\nVALUES";
                sql += "\r\n    (1, 'SensorDataMessage', 0)";
                _Runme.RunSQLCMD(sql, sConnStr, 4000);

                sql = "INSERT INTO [dbo].[NaveoFuelMessageTypes]";
                sql += "\r\n    ([MessageType]";
                sql += "\r\n    ,[Description]";
                sql += "\r\n    ,[IsNotif])";
                sql += "\r\nVALUES";
                sql += "\r\n    (3, 'RawDataMessage', 0)";
                _Runme.RunSQLCMD(sql, sConnStr, 4000);

                sql = "INSERT INTO [dbo].[NaveoFuelMessageTypes]";
                sql += "\r\n    ([MessageType]";
                sql += "\r\n    ,[Description]";
                sql += "\r\n    ,[IsNotif])";
                sql += "\r\nVALUES";
                sql += "\r\n    (4, 'RawSumDataMessage', 0)";
                _Runme.RunSQLCMD(sql, sConnStr, 4000);

                sql = "INSERT INTO [dbo].[NaveoFuelMessageTypes]";
                sql += "\r\n    ([MessageType]";
                sql += "\r\n    ,[Description]";
                sql += "\r\n    ,[IsNotif])";
                sql += "\r\nVALUES";
                sql += "\r\n    (5, 'FillDataMessage', 0)";
                _Runme.RunSQLCMD(sql, sConnStr, 4000);

                sql = "INSERT INTO [dbo].[NaveoFuelMessageTypes]";
                sql += "\r\n    ([MessageType]";
                sql += "\r\n    ,[Description]";
                sql += "\r\n    ,[IsNotif])";
                sql += "\r\nVALUES";
                sql += "\r\n    (6, 'RealDataMessage', 0)";
                _Runme.RunSQLCMD(sql, sConnStr, 4000);

                sql = "INSERT INTO [dbo].[NaveoFuelMessageTypes]";
                sql += "\r\n    ([MessageType]";
                sql += "\r\n    ,[Description]";
                sql += "\r\n    ,[IsNotif])";
                sql += "\r\nVALUES";
                sql += "\r\n    (7, 'NotificationMessage', 0)";
                _Runme.RunSQLCMD(sql, sConnStr, 4000);

                sql = "INSERT INTO [dbo].[NaveoFuelMessageTypes]";
                sql += "\r\n    ([MessageType]";
                sql += "\r\n    ,[Description]";
                sql += "\r\n    ,[IsNotif])";
                sql += "\r\nVALUES";
                sql += "\r\n    (9, 'RestartStreamMessage', 0)";
                _Runme.RunSQLCMD(sql, sConnStr, 4000);

                sql = "INSERT INTO [dbo].[NaveoFuelMessageTypes]";
                sql += "\r\n    ([MessageType]";
                sql += "\r\n    ,[Description]";
                sql += "\r\n    ,[IsNotif])";
                sql += "\r\nVALUES";
                sql += "\r\n    (5, 'Wrong vehicle id or wrong sensor number', 1)";
                _Runme.RunSQLCMD(sql, sConnStr, 4000);

                sql = "INSERT INTO [dbo].[NaveoFuelMessageTypes]";
                sql += "\r\n    ([MessageType]";
                sql += "\r\n    ,[Description]";
                sql += "\r\n    ,[IsNotif])";
                sql += "\r\nVALUES";
                sql += "\r\n    (6, 'Session Time out', 1)";
                _Runme.RunSQLCMD(sql, sConnStr, 4000);

                sql = "INSERT INTO [dbo].[NaveoFuelMessageTypes]";
                sql += "\r\n    ([MessageType]";
                sql += "\r\n    ,[Description]";
                sql += "\r\n    ,[IsNotif])";
                sql += "\r\nVALUES";
                sql += "\r\n    (7, 'Wrong vehicle id or wrong sensor number', 1)";
                _Runme.RunSQLCMD(sql, sConnStr, 4000);

                sql = "INSERT INTO [dbo].[NaveoFuelMessageTypes]";
                sql += "\r\n    ([MessageType]";
                sql += "\r\n    ,[Description]";
                sql += "\r\n    ,[IsNotif])";
                sql += "\r\nVALUES";
                sql += "\r\n    (12, 'Buffers cleared', 1)";
                _Runme.RunSQLCMD(sql, sConnStr, 4000);

                InsertDBUpdate("DBUPD00001", "NaveoFuelData Table", sConnStr);
            }
            #endregion

            #region Update 2
            if (!CheckUpdateExist("DBUPD00002", sConnStr))
            {
                sql = "CREATE TABLE [dbo].[NaveoGrnlList](";
                sql += "\r\n    [unitID] [varchar](20) NOT NULL,";
                sql += "\r\n    [DriverID] [varchar](20) NULL,";
                sql += "\r\n    [dtDateTime] [datetime] NULL,";
                sql += "\r\n CONSTRAINT [PK_NaveoGrnlList] PRIMARY KEY CLUSTERED ";
                sql += "\r\n(";
                sql += "\r\n    [unitID] ASC";
                sql += "\r\n)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]";
                sql += "\r\n) ON [PRIMARY]";
                _Runme.RunSQLCMD(sql, sConnStr, -1);

                InsertDBUpdate("DBUPD00002", "NaveoGeneralList Table", sConnStr);
            }
            #endregion

            #region Update 3 NaveoAverageDataMessage
            if (!CheckUpdateExist("DBUPD00003", sConnStr))
            {
                sql = "INSERT INTO [dbo].[NaveoFuelMessageTypes]";
                sql += "\r\n    ([MessageType]";
                sql += "\r\n    ,[Description]";
                sql += "\r\n    ,[IsNotif])";
                sql += "\r\nVALUES";
                sql += "\r\n    (2, 'NaveoAverageDataMessage', 0)";
                new Runme().RunSQLCMD(sql, sConnStr, 4000);
                InsertDBUpdate("DBUPD00003", "NaveoAverageDataMessage row created in FuelMessageTypes", sConnStr);
            }
            #endregion

            #region Update 4
            if (!CheckUpdateExist("DBUPD00004", sConnStr))
            {
                sql = "CREATE TABLE [dbo].[NaveoCMTH](";
                sql += "\r\n    [TripID] int NOT NULL,";
                sql += "\r\n    [VehicleID] int NOT NULL,";
                sql += "\r\n    [IDAction] [nvarchar](1) NOT NULL,";
                sql += "\r\n    [Processed] [nvarchar](1) NOT NULL DEFAULT 'N',";
                sql += "\r\n    [dtDateTime] [datetime] NOT NULL DEFAULT GETDATE(),";
                sql += "\r\n CONSTRAINT [PK_CMTH] PRIMARY KEY CLUSTERED ";
                sql += "\r\n(";
                sql += "\r\n    [TripID] ASC";
                sql += "\r\n)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]";
                sql += "\r\n) ON [PRIMARY]";
                _Runme.RunSQLCMD(sql, sConnStr, -1);

                sql = @"CREATE TRIGGER trgAfterDelete ON  dbo.Trips
                            FOR DELETE
                            AS  
                                insert into NaveoCMTH
                                       (TripID, VehicleID, IDAction) 
                                SELECT iiD, iVehicleID,'D'
                                FROM deleted;";
                _Runme.RunSQLCMD(sql, sConnStr, -1);

                InsertDBUpdate("DBUPD00004", "NaveoCMTH", sConnStr);
            }
            #endregion

        }

        Boolean CheckUpdateExist(String UpdateId, String sConnStr)
        {
            Boolean bResult = false;
            if ((int)_Runme.GetDataSet(SqlCmds.CheckNaveoDbUpdate(UpdateId), sConnStr).Tables[0].Rows.Count > 0)
                bResult = true;
            return bResult;
        }
        void InsertDBUpdate(String pUpd_Id, String pUpd_Desc, String sConnStr)
        {
            _Runme.RunSQLCMD(SqlCmds.InsertNaveoDb(pUpd_Id, pUpd_Desc), sConnStr, -1);
        }
    }
}
