using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using NavLib.Globals;

namespace NavLib.Data
{
    public class SystemHelper
    {
        #region BO Objects
        private DBHelper dbHelper = new DBHelper();
        #endregion

        public void UpdateDB(String sConnStr)
        {
            String sql = String.Empty;
            SqlConnection _SqlConn = dbHelper.GetDbConnection(sConnStr);
            if (dbHelper.ExecuteScalar(ExistTableSql("db_update"), _SqlConn, null, sConnStr) == 0)
                dbHelper.Execute("create table db_update( upd_id varchar(10) not null, upd_desc varchar(300) not null, upd_date datetime not null )");

            #region Update 2
            if (!CheckUpdateExist("DBUPD00002", sConnStr))
            {
                sql = "CREATE TABLE [dbo].[MessageData](";
                sql += "\r\n    [iID] [int] IDENTITY(1,1) NOT NULL,";
                sql += "\r\n    [dtDateTime] [datetime] NULL DEFAULT (getdate()),";
                sql += "\r\n    [Model] [varchar] (15),";
                sql += "\r\n	[UnitData] [varchar](1024),";
                sql += "\r\n	[Status] [int] NOT NULL DEFAULT (0),";
                sql += "\r\n    [unitID] [varchar](20) NOT NULL, CONSTRAINT [PK_MessageData] PRIMARY KEY CLUSTERED ([iID] ASC)";
                sql += "\r\n) ";

                if (dbHelper.ExecuteScalar(ExistTableSql("MessageData"), _SqlConn, null, sConnStr) == 0)
                    dbHelper.Execute(sql, null, null, true, null, sConnStr);
                InsertDBUpdate("DBUPD00002", "MessageData Table", sConnStr);
            }
            #endregion

            #region Update 3
            if (!CheckUpdateExist("DBUPD00003", sConnStr))
            {
                sql = "CREATE TABLE [dbo].[RawData](";
                sql += "\r\n    [iID] [int] IDENTITY(1,1) NOT NULL,";
                sql += "\r\n    [dtDateTime] [datetime] NULL DEFAULT (getdate()),";
                sql += "\r\n    [Model] [varchar] (15),";
                sql += "\r\n	[RawData] [Binary](1024), ";
                sql += "\r\n	[Data] [varchar](1024),";
                sql += "\r\n	[Status] [int] NOT NULL DEFAULT (0)";
                sql += "\r\n) ";

                if (dbHelper.ExecuteScalar(ExistTableSql("RawData"), _SqlConn, null, sConnStr) == 0)
                    dbHelper.Execute(sql, null, null, true, null, sConnStr);
                InsertDBUpdate("DBUPD00003", "RawData Table", sConnStr);
            }
            #endregion

            #region Update 4
            if (!CheckUpdateExist("DBUPD00004", sConnStr))
            {
                sql = "Alter TABLE [dbo].[RawData]";
                sql += "\r\n    Add [unitID] [varchar](20)NULL";
                dbHelper.Execute(sql, null, null, true, null, sConnStr);

                InsertDBUpdate("DBUPD00004", "unitID column in RawData Table", sConnStr);
            }
            #endregion

            #region Update 5
            if (!CheckUpdateExist("DBUPD00005", sConnStr))
            {
                sql = "Alter TABLE [dbo].[RawData]";
                sql += "\r\n    Add [ServerName] [varchar](30)NULL";
                dbHelper.Execute(sql, null, null, true, null, sConnStr);

                sql = "Alter TABLE [dbo].[RawData]";
                sql += "\r\n    Add [DBName] [varchar](20)NULL";
                dbHelper.Execute(sql, null, null, true, null, sConnStr);
                InsertDBUpdate("DBUPD00005", "More columns in RawData Table", sConnStr);
            }
            #endregion

            #region Update 6
            if (!CheckUpdateExist("DBUPD00006", sConnStr))
            {
                sql = "/****** Object:  Index [PK_RawData]    Script Date: 07/07/2010 13:28:06 ******/";
                sql += "\r\nALTER TABLE [dbo].[RawData] ADD  CONSTRAINT [PK_RawData] PRIMARY KEY CLUSTERED ";
                sql += "\r\n(";
                sql += "\r\n	[iID] ASC";
                sql += "\r\n)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]";
                dbHelper.Execute(sql, null, null, true, null, sConnStr);

                sql = "/****** Object:  Index [IX_RawData]    Script Date: 07/07/2010 13:27:44 ******/";
                sql += "\r\nCREATE NONCLUSTERED INDEX [IX_RawData] ON [dbo].[RawData] ";
                sql += "\r\n(";
                sql += "\r\n	[dtDateTime] DESC";
                sql += "\r\n)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]";
                dbHelper.Execute(sql, null, null, true, null, sConnStr);
                InsertDBUpdate("DBUPD00006", "RawData Primary Key and index", sConnStr);
            }
            #endregion

            #region Update 7
            if (!CheckUpdateExist("DBUPD00007", sConnStr))
            {
                if (sConnStr == "ConnStr")
                {
                    sql = "Create Database " + GlobalVariables.GetDBName(sConnStr) + "Transferred";
                    dbHelper.Execute(sql, null, null, true, null, sConnStr);

                    InsertDBUpdate("DBUPD00007", "Transferred DB created ", sConnStr);
                }
                else
                    InsertDBUpdate("DBUPD00007", "Update not required in this DB ", sConnStr);
            }
            #endregion

            #region Update 8
            if (!CheckUpdateExist("DBUPD00008", sConnStr))
            {
                sql = "Alter TABLE [dbo].[RawData]";
                sql += "\r\n    Alter Column [RawData] [varBinary](1024)NULL";
                new Runme().RunSQLCMD(sql, sConnStr, 4000);

                sql = "DBCC SHRINKDATABASE (" + GlobalVariables.GetDBName(sConnStr) + ", 10)";
                new Runme().RunSQLCMD(sql, sConnStr, 4000);

                InsertDBUpdate("DBUPD00008", "RawData Col now varBinary", sConnStr);
            }
            #endregion

            #region Update 9
            if (!CheckUpdateExist("DBUPD00009", sConnStr))
            {
                sql = "/****** Object:  Index [IX_RawData]    Script Date: 07/07/2010 13:27:44 ******/";
                sql += "\r\nCREATE NONCLUSTERED INDEX [IX_RawData_Status] ON [dbo].[RawData] ";
                sql += "\r\n(";
                sql += "\r\n	[Status] DESC";
                sql += "\r\n)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]";
                new Runme().RunSQLCMD(sql, sConnStr, 4000);
                InsertDBUpdate("DBUPD00009", "Status index in RawData Table", sConnStr);
            }
            #endregion

            #region Update 10
            if (!CheckUpdateExist("DBUPD00010", sConnStr))
            {
                sql = "/****** Object:  Index [IX_RawData]    Script Date: 07/07/2010 13:27:44 ******/";
                sql += "\r\nCREATE NONCLUSTERED INDEX [IX_RawData_Model] ON [dbo].[RawData] ";
                sql += "\r\n(";
                sql += "\r\n	[Model] DESC";
                sql += "\r\n)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]";
                new Runme().RunSQLCMD(sql, sConnStr, 4000);

                sql = "/****** Object:  Index [IX_RawData]    Script Date: 07/07/2010 13:27:44 ******/";
                sql += "\r\nCREATE NONCLUSTERED INDEX [IX_RawData_unitID] ON [dbo].[RawData] ";
                sql += "\r\n(";
                sql += "\r\n	[unitID] DESC";
                sql += "\r\n)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]";
                new Runme().RunSQLCMD(sql, sConnStr, 4000);

                InsertDBUpdate("DBUPD00010", "Model and UnitID index in RawData Table", sConnStr);
            }
            #endregion

            #region Update 11
            if (!CheckUpdateExist("DBUPD00011", sConnStr))
            {
                sql = "CREATE TABLE [dbo].[FuelConv](";
                sql += "\r\n    [UnitID] [varchar](20) NOT NULL,";
                sql += "\r\n    [Conv] [int] NOT NULL,";
                sql += "\r\n    [ADC] [int] NOT NULL,";
                sql += "\r\n    [DL] [int] NOT NULL,";
                sql += "\r\n    CONSTRAINT [PK_FuelConv] PRIMARY KEY CLUSTERED ";
                sql += "\r\n    (";
                sql += "\r\n    [Conv] ASC,";
                sql += "\r\n    [UnitID] ASC";
                sql += "\r\n    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]";
                sql += "\r\n    ) ON [PRIMARY]";

                if (dbHelper.ExecuteScalar(ExistTableSql("FuelConv"), _SqlConn, null, sConnStr) == 0)
                    dbHelper.Execute(sql, null, null, true, null, sConnStr);
                InsertDBUpdate("DBUPD00011", "FuelConv Table Created", sConnStr);
            }
            #endregion

            #region Update 12
            if (!CheckUpdateExist("DBUPD00012", sConnStr))
            {
                sql = "CREATE TABLE [dbo].[Fuel](";
                sql += "\r\n    [FuelID] [int] IDENTITY(1,1) NOT NULL,";
                sql += "\r\n    [unitID] [varchar](20) NOT NULL,";
                sql += "\r\n    [Reverse] [int] NOT NULL CONSTRAINT [DF_Table_1_Reverse]  DEFAULT ((0)),";
                sql += "\r\n    CONSTRAINT [PK_Fuel] PRIMARY KEY CLUSTERED ";
                sql += "\r\n    (";
                sql += "\r\n    [unitID] ASC";
                sql += "\r\n    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]";
                sql += "\r\n    ) ON [PRIMARY]";

                if (dbHelper.ExecuteScalar(ExistTableSql("Fuel"), _SqlConn, null, sConnStr) == 0)
                    dbHelper.Execute(sql, null, null, true, null, sConnStr);
                InsertDBUpdate("DBUPD00012", "Fuel Table Created", sConnStr);
            }
            #endregion

            #region Update 13
            if (!CheckUpdateExist("DBUPD00013", sConnStr))
            {
                Runme runMe = new Runme();
                sql = "delete from FuelConv";
                runMe.RunSQLCMD(sql, sConnStr, 4000);
                runMe.InsertNewFuelConv("101", sConnStr);

                sql = "CREATE TABLE [dbo].[FuelData](";
                sql += "\r\n    [iID] [int] IDENTITY(1,1) NOT NULL,";
                sql += "\r\n    [MessageType] [int] NOT NULL,";
                sql += "\r\n    [dtDateTime] [datetime] NULL,";
                sql += "\r\n    [DataID] [int] NOT NULL,";
                sql += "\r\n    [DataIDFrom] [int] NOT NULL CONSTRAINT [DF_FuelData_DateIDFrom]  DEFAULT ((-1)),";
                sql += "\r\n    [DataIDTo] [int] NOT NULL CONSTRAINT [DF_FuelData_DataIDTo]  DEFAULT ((-1)),";
                sql += "\r\n    [FuelID] [int] NOT NULL,";
                sql += "\r\n    [unitID] [varchar](20) NOT NULL,";
                sql += "\r\n    [sensorNo] [int] NOT NULL,";
                sql += "\r\n    [RawADC] [int] NOT NULL CONSTRAINT [DF_FuelData_RawADC]  DEFAULT ((-1)),";
                sql += "\r\n    [UsedADC] [int] NOT NULL CONSTRAINT [DF_FuelData_UsedADC]  DEFAULT ((-1)),";
                sql += "\r\n    [FuelLvl] [int] NOT NULL,";
                sql += "\r\n    [NotifCode] [int] NOT NULL CONSTRAINT [DF_FuelData_NotifCode]  DEFAULT ((-1)),";
                sql += "\r\n	[Status] [int] NOT NULL DEFAULT (0)";
                sql += "\r\n CONSTRAINT [PK_FuelData] PRIMARY KEY CLUSTERED ";
                sql += "\r\n(";
                sql += "\r\n    [iID] ASC";
                sql += "\r\n)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]";
                sql += "\r\n) ON [PRIMARY]";
                if (dbHelper.ExecuteScalar(ExistTableSql("FuelData"), _SqlConn, null, sConnStr) == 0)
                    runMe.RunSQLCMD(sql, sConnStr, 4000);

                sql = "CREATE TABLE [dbo].[FuelMessageTypes](";
                sql += "\r\n    [MessageType] [int] NOT NULL,";
                sql += "\r\n    [Description] [varchar](100) NOT NULL,";
                sql += "\r\n    [IsNotif] [tinyint] NOT NULL,";
                sql += "\r\nCONSTRAINT [PK_FuelMessageTypes] PRIMARY KEY CLUSTERED ";
                sql += "\r\n (";
                sql += "\r\n    [MessageType] ASC,";
                sql += "\r\n    [IsNotif] ASC";
                sql += "\r\n    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]";
                sql += "\r\n    ) ON [PRIMARY]";
                if (dbHelper.ExecuteScalar(ExistTableSql("FuelMessageTypes"), _SqlConn, null, sConnStr) == 0)
                    runMe.RunSQLCMD(sql, sConnStr, 4000);

                sql = "INSERT INTO [dbo].[FuelMessageTypes]";
                sql += "\r\n    ([MessageType]";
                sql += "\r\n    ,[Description]";
                sql += "\r\n    ,[IsNotif])";
                sql += "\r\nVALUES";
                sql += "\r\n    (1, 'SensorDataMessage', 0)";
                runMe.RunSQLCMD(sql, sConnStr, 4000);

                sql = "INSERT INTO [dbo].[FuelMessageTypes]";
                sql += "\r\n    ([MessageType]";
                sql += "\r\n    ,[Description]";
                sql += "\r\n    ,[IsNotif])";
                sql += "\r\nVALUES";
                sql += "\r\n    (3, 'RawDataMessage', 0)";
                runMe.RunSQLCMD(sql, sConnStr, 4000);

                sql = "INSERT INTO [dbo].[FuelMessageTypes]";
                sql += "\r\n    ([MessageType]";
                sql += "\r\n    ,[Description]";
                sql += "\r\n    ,[IsNotif])";
                sql += "\r\nVALUES";
                sql += "\r\n    (4, 'RawSumDataMessage', 0)";
                runMe.RunSQLCMD(sql, sConnStr, 4000);

                sql = "INSERT INTO [dbo].[FuelMessageTypes]";
                sql += "\r\n    ([MessageType]";
                sql += "\r\n    ,[Description]";
                sql += "\r\n    ,[IsNotif])";
                sql += "\r\nVALUES";
                sql += "\r\n    (5, 'FillDataMessage', 0)";
                runMe.RunSQLCMD(sql, sConnStr, 4000);

                sql = "INSERT INTO [dbo].[FuelMessageTypes]";
                sql += "\r\n    ([MessageType]";
                sql += "\r\n    ,[Description]";
                sql += "\r\n    ,[IsNotif])";
                sql += "\r\nVALUES";
                sql += "\r\n    (6, 'RealDataMessage', 0)";
                runMe.RunSQLCMD(sql, sConnStr, 4000);

                sql = "INSERT INTO [dbo].[FuelMessageTypes]";
                sql += "\r\n    ([MessageType]";
                sql += "\r\n    ,[Description]";
                sql += "\r\n    ,[IsNotif])";
                sql += "\r\nVALUES";
                sql += "\r\n    (7, 'NotificationMessage', 0)";
                runMe.RunSQLCMD(sql, sConnStr, 4000);

                sql = "INSERT INTO [dbo].[FuelMessageTypes]";
                sql += "\r\n    ([MessageType]";
                sql += "\r\n    ,[Description]";
                sql += "\r\n    ,[IsNotif])";
                sql += "\r\nVALUES";
                sql += "\r\n    (9, 'RestartStreamMessage', 0)";
                runMe.RunSQLCMD(sql, sConnStr, 4000);

                sql = "INSERT INTO [dbo].[FuelMessageTypes]";
                sql += "\r\n    ([MessageType]";
                sql += "\r\n    ,[Description]";
                sql += "\r\n    ,[IsNotif])";
                sql += "\r\nVALUES";
                sql += "\r\n    (5, 'Wrong vehicle id or wrong sensor number', 1)";
                runMe.RunSQLCMD(sql, sConnStr, 4000);

                sql = "INSERT INTO [dbo].[FuelMessageTypes]";
                sql += "\r\n    ([MessageType]";
                sql += "\r\n    ,[Description]";
                sql += "\r\n    ,[IsNotif])";
                sql += "\r\nVALUES";
                sql += "\r\n    (6, 'Session Time out', 1)";
                runMe.RunSQLCMD(sql, sConnStr, 4000);

                sql = "INSERT INTO [dbo].[FuelMessageTypes]";
                sql += "\r\n    ([MessageType]";
                sql += "\r\n    ,[Description]";
                sql += "\r\n    ,[IsNotif])";
                sql += "\r\nVALUES";
                sql += "\r\n    (7, 'Wrong vehicle id or wrong sensor number', 1)";
                runMe.RunSQLCMD(sql, sConnStr, 4000);

                sql = "INSERT INTO [dbo].[FuelMessageTypes]";
                sql += "\r\n    ([MessageType]";
                sql += "\r\n    ,[Description]";
                sql += "\r\n    ,[IsNotif])";
                sql += "\r\nVALUES";
                sql += "\r\n    (12, 'Buffers cleared', 1)";
                runMe.RunSQLCMD(sql, sConnStr, 4000);

                sql = "INSERT INTO [dbo].[Fuel]";
                sql += "\r\n    ([unitID]";
                sql += "\r\n    ,[Reverse])";
                sql += "\r\nVALUES";
                sql += "\r\n    ('31106', 1)";
                runMe.RunSQLCMD(sql, sConnStr, 4000);

                InsertDBUpdate("DBUPD00013", "Fuel Tables Created/Updated", sConnStr);
            }
            #endregion

            #region Update 14
            if (!CheckUpdateExist("DBUPD00014", sConnStr))
            {
                sql = "ALTER TABLE [dbo].[RawData] ";
                sql += "\r\n    Add [FuelProcessed] [int] NOT NULL DEFAULT (0)";
                new Runme().RunSQLCMD(sql, sConnStr, 4000);

                InsertDBUpdate("DBUPD00014", "FuelProcessed field in RawData Table", sConnStr);
            }
            #endregion

            #region Update 15
            if (!CheckUpdateExist("DBUPD00015", sConnStr))
            {
                sql = "/****** Object:  Index [Idx_UnitStatus]    Script Date: 02/11/2011 14:51:27 ******/";
                sql += "\r\nCREATE NONCLUSTERED INDEX [Idx_UnitStatus] ON [dbo].[RawData] ";
                sql += "\r\n(";
                sql += "\r\n	[unitID] ASC,";
                sql += "\r\n	[Model] ASC,";
                sql += "\r\n	[dtDateTime] ASC,";
                sql += "\r\n	[ServerName] ASC,";
                sql += "\r\n	[DBName] ASC";
                sql += "\r\n)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]";
                new Runme().RunSQLCMD(sql, sConnStr, 4000);
                InsertDBUpdate("DBUPD00015", "Idx_UnitStatus index in RawData Table", sConnStr);
            }
            #endregion

            #region Update 16
            if (!CheckUpdateExist("DBUPD00016", sConnStr))
            {
                sql = "/****** Object:  Index [Idx_Upd]    Script Date: 12/01/2012 00:59:06 ******/";
                sql += "\r\nCREATE NONCLUSTERED INDEX [Idx_Upd] ON [dbo].[RawData] ";
                sql += "\r\n(";
                sql += "\r\n	[iID] ASC,";
                sql += "\r\n	[dtDateTime] ASC,";
                sql += "\r\n	[Status] ASC,";
                sql += "\r\n	[unitID] ASC";
                sql += "\r\n)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]";
                new Runme().RunSQLCMD(sql, sConnStr, 4000);

                sql = "/****** Object:  Index [Idx_SelectUpd]    Script Date: 12/01/2012 01:01:16 ******/";
                sql += "\r\nCREATE NONCLUSTERED INDEX [Idx_SelectUpd] ON [dbo].[RawData] ";
                sql += "\r\n(";
                sql += "\r\n	[Status] ASC,";
                sql += "\r\n	[unitID] ASC";
                sql += "\r\n)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]";
                new Runme().RunSQLCMD(sql, sConnStr, 4000);

                InsertDBUpdate("DBUPD00016", "Idx_Upd &  index in RawData Table", sConnStr);
            }
            #endregion

            #region Update 17
            if (!CheckUpdateExist("DBUPD00017", sConnStr))
            {
                sql = "CREATE TABLE [dbo].[AudQryReceived](";
                sql += "\r\n	[ServerName] [varchar](50) NULL,";
                sql += "\r\n	[DBName] [varchar](50) NULL,";
                sql += "\r\n	[iID] [int] IDENTITY(1,1) NOT NULL,";
                sql += "\r\n	[Retrieval] [varchar](5) NULL,";
                sql += "\r\n	[qFrom] [varchar](50) NULL,";
                sql += "\r\n CONSTRAINT [PK_AudQryReceived] PRIMARY KEY CLUSTERED ";
                sql += "\r\n(";
                sql += "\r\n	[iID] ASC";
                sql += "\r\n)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]";
                sql += "\r\n) ON [PRIMARY]";
                if (dbHelper.ExecuteScalar(ExistTableSql("AudQryReceived"), _SqlConn, null, sConnStr) == 0)
                    dbHelper.Execute(sql, null, null, true, null, sConnStr);

                sql="/****** Object:  Index [idxAll]    Script Date: 12/04/2012 01:27:52 ******/";
                sql += "\r\nCREATE NONCLUSTERED INDEX [idxAll] ON [dbo].[AudQryReceived] ";
                sql += "\r\n(";
                sql += "\r\n	[ServerName] ASC,";
                sql += "\r\n	[DBName] ASC,";
                sql += "\r\n	[Retrieval] ASC,";
                sql += "\r\n	[qFrom] ASC";
                sql += "\r\n)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]";
                new Runme().RunSQLCMD(sql, sConnStr, 4000);

                sql="CREATE TABLE [dbo].[AudQryRcvCount](";
                sql += "\r\n	[iID] [int] NULL,";
                sql += "\r\n	[ip] [varchar](50) NULL,";
                sql += "\r\n	[iCount] [int] NULL,";
                sql += "\r\n	[myUnits] [varchar](5000) NULL";
                sql += "\r\n) ON [PRIMARY]";
                if (dbHelper.ExecuteScalar(ExistTableSql("AudQryRcvCount"), _SqlConn, null, sConnStr) == 0)
                    dbHelper.Execute(sql, null, null, true, null, sConnStr);

                sql="/****** Object:  Index [idx_AQRC]    Script Date: 12/04/2012 01:38:11 ******/";
                sql += "\r\nCREATE NONCLUSTERED INDEX [idx_AQRC] ON [dbo].[AudQryRcvCount] ";
                sql += "\r\n(";
                sql += "\r\n	[iID] ASC,";
                sql += "\r\n	[ip] ASC";
                sql += "\r\n)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]";
                new Runme().RunSQLCMD(sql, sConnStr, 4000);

                InsertDBUpdate("DBUPD00017", "AudQryReceived and AudQryRcvCount Tables For Web", sConnStr);
            }
            #endregion

            #region Update 18 NaveoAverageDataMessage
            if (!CheckUpdateExist("DBUPD00018", sConnStr))
            {
                sql = "INSERT INTO [dbo].[FuelMessageTypes]";
                sql += "\r\n    ([MessageType]";
                sql += "\r\n    ,[Description]";
                sql += "\r\n    ,[IsNotif])";
                sql += "\r\nVALUES";
                sql += "\r\n    (2, 'NaveoAverageDataMessage', 0)";
                new Runme().RunSQLCMD(sql, sConnStr, 4000);
                InsertDBUpdate("DBUPD00018", "NaveoAverageDataMessage row created in FuelMessageTypes", sConnStr);
            }
            #endregion

            #region Update 19
            if (!CheckUpdateExist("DBUPD00019", sConnStr))
            {
                sql = "/****** Object:  Index [IX_PollUpdates]    Script Date: 07/Dec/2014 10:50:10 PM ******/";
                sql += "\r\nCREATE NONCLUSTERED INDEX [IX_PollUpdates] ON [dbo].[RawData] ";
                sql += "\r\n(";
                sql += "\r\n	[Status] ASC,";
                sql += "\r\n	[unitID] ASC,";
                sql += "\r\n	[dtDateTime] ASC";
                sql += "\r\n)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]";
                dbHelper.Execute(sql, null, null, true, null, sConnStr);
                InsertDBUpdate("DBUPD00019", "RawData Poll index", sConnStr);
            }
            #endregion

            #region Update 20
            if (!CheckUpdateExist("DBUPD00020", sConnStr))
            {
                sql = @"
CREATE TABLE [dbo].[Devices](
	[unitID] [nvarchar](20) NOT NULL,
    [dtInsert] [DateTime] NOT NULL Default GetUTCDate(),
 CONSTRAINT [PK_Devices] PRIMARY KEY CLUSTERED 
(
	[unitID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]";

                if (dbHelper.ExecuteScalar(ExistTableSql("Devices"), _SqlConn, null, sConnStr) == 0)
                    dbHelper.Execute(sql, null, null, true, null, sConnStr);
                InsertDBUpdate("DBUPD00020", "Devices Table", sConnStr);
            }
            #endregion

            #region Update 21
            if (!CheckUpdateExist("DBUPD00021", sConnStr))
            {
                sql = "Alter TABLE [dbo].[RawData]";
                sql += "\r\n    Add [SMStatus] int not null Default(0)";
                dbHelper.Execute(sql, null, null, true, null, sConnStr);

                InsertDBUpdate("DBUPD00021", "SMStatus column in RawData Table", sConnStr);
            }
            #endregion

            #region Update 22
            if (!CheckUpdateExist("DBUPD00022", sConnStr))
            {
                sql = "\r\nCREATE NONCLUSTERED INDEX [IX_RawData_SMStatus] ON [dbo].[RawData] ";
                sql += "\r\n(";
                sql += "\r\n	[SMStatus] DESC";
                sql += "\r\n)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]";
                new Runme().RunSQLCMD(sql, sConnStr, 4000);
                InsertDBUpdate("DBUPD00022", "SMStatus index in RawData Table", sConnStr);
            }
            #endregion

            #region Update 23
            if (!CheckUpdateExist("DBUPD00023", sConnStr))
            {
                sql = "CREATE INDEX [IX_RawData_unitID_SMStatus] ON [dbo].[RawData] ([unitID], [SMStatus]) INCLUDE ([iID], [dtDateTime]) ";
                new Runme().RunSQLCMD(sql, sConnStr, 4000);

                sql = "CREATE NONCLUSTERED INDEX [nci_wi_RawData_Sync] ON [dbo].[RawData] ([Status], [unitID]) INCLUDE ([Data], [DBName], [dtDateTime], [Model], [RawData], [ServerName]) ";
                new Runme().RunSQLCMD(sql, sConnStr, 4000);

                sql = "ALTER DATABASE [NaveoListenerDB] SET PARAMETERIZATION FORCED ";
                new Runme().RunSQLCMD(sql, sConnStr, 4000);

                InsertDBUpdate("DBUPD00023", "unitID SMStatus index in RawData Table", sConnStr);
            }
            #endregion

            #region Update 24
            if (!CheckUpdateExist("DBUPD00024", sConnStr))
            {
                sql = "Alter TABLE [dbo].[RawData]";
                sql += "\r\n    Alter Column [ServerName] [varchar](50) NULL";
                dbHelper.Execute(sql, null, null, true, null, sConnStr);

                InsertDBUpdate("DBUPD00004", "ServerName column in RawData Table increased", sConnStr);
            }
            #endregion


            #region Update 24
            //if (!CheckUpdateExist("DBUPD00024", sConnStr))
            //{
            //    sql = "Drop index TmpIX_RawData ON [dbo].[RawData]";
            //    if (dbHelper.GetDataSet(ExistsIndex("RawData", "TmpIX_RawData")).Tables[0].Rows.Count > 0)
            //        new Runme().RunSQLCMD(sql, sConnStr, 4000);

            //    sql = "CREATE NONCLUSTERED INDEX IX_Billing_RawData ON [dbo].[RawData] ([ServerName]) INCLUDE ([unitID],[DBName]) ";
            //    if (dbHelper.GetDataSet(ExistsIndex("RawData", "IX_Billing_RawData")).Tables[0].Rows.Count == 0)
            //        new Runme().RunSQLCMD(sql, sConnStr, 4000);

            //    sql = "CREATE NONCLUSTERED INDEX IX_Billing_Date_RawData ON [dbo].[RawData] ([dtDateTime]) INCLUDE ([Model],[unitID])";
            //    if (dbHelper.GetDataSet(ExistsIndex("RawData", "IX_Billing_Date_RawData")).Tables[0].Rows.Count == 0)
            //        new Runme().RunSQLCMD(sql, sConnStr, 4000);

            //    new Runme().RunSQLCMD(sql, sConnStr, 4000);
            //    InsertDBUpdate("DBUPD00024", "IX_Billing_RawData index in RawData Table", sConnStr);
            //}
            #endregion

            _SqlConn.Close();
            dbHelper.CloseDbConnection();
        }
        public DataSet GetTablesFromColumn(String pStrColumn)
        {
            return (dbHelper.GetDataSet("select object_name(id) as Table_Name from dbo.syscolumns where name  = '" + pStrColumn + "' and OBJECTPROPERTY(id, N'IsUserTable') = 1"));
        }

        private String ExistTableSql(String pStrTable)
        {
            return ("select count(*) from dbo.sysobjects where id = object_id(N'" + pStrTable + "') and OBJECTPROPERTY(id, N'IsUserTable') = 1");
        }

        private String ExistViewSql(String pStrView)
        {
            return ("select count(*) from dbo.sysobjects where id = object_id(N'" + pStrView + "') and OBJECTPROPERTY(id, N'IsView') = 1");
        }

        private String ExistColumnSql(String pStrTable, String pStrColumn)
        {
            return ("select count(*) from syscolumns where name = '" + pStrColumn + "' and object_name(id) = '" + pStrTable + "'");
        }

        private Boolean CheckUpdateExist(String UpdateId, String sConnStr)
        {
            Boolean bOk = false;
            if (dbHelper.GetNumberOfRecords("db_update", "*", " upd_id = '" + UpdateId + "'", sConnStr) > 0)
                bOk = true;
            return bOk;
        }

        private Boolean CheckParamExist(String pCode, String sConnStr)
        {
            Boolean bOk = false;
            if (dbHelper.GetNumberOfRecords("db_param", "*", " code = '" + pCode + "'", sConnStr) > 0)
                bOk = true;
            return bOk;
        }

        private String ExistsIndex(String pStrTable, String pStrIndex)
        {
            return "SELECT * FROM sys.indexes WHERE name='" + pStrIndex + "' AND object_id = OBJECT_ID('" + pStrTable + "')";
        }

        private void InsertDBUpdate(String pUpd_Id, String pUpd_Desc, String sConnStr)
        {
            dbHelper.Execute("Insert into db_update values( '" + pUpd_Id + "', '" + pUpd_Desc + "', getdate())", null, null, true, null, sConnStr);
        }
    }
}