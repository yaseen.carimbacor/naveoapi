using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Xml;
using System.Diagnostics;
using System.Drawing;
using NavLib.Globals;
using System.Threading;


namespace NavLib.Data
{
    public class Functions
    {
        public static String ProperCase(String stringInput)
        {
            StringBuilder sb = new StringBuilder();
            bool fEmptyBefore = true;
            foreach (char ch in stringInput)
            {
                char chThis = ch;
                if (Char.IsWhiteSpace(chThis))
                    fEmptyBefore = true;
                else
                {
                    if (Char.IsLetter(chThis) && fEmptyBefore)
                        chThis = Char.ToUpper(chThis);
                    else
                        chThis = Char.ToLower(chThis);
                    fEmptyBefore = false;
                }
                sb.Append(chThis);
            }
            return sb.ToString();
        }
        public static Boolean IsNumeric(String theValue)
        {
            try
            {
                Convert.ToInt32(theValue);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static Single ConvertToSingle(String strInput, Single sngDefault)
        {
            Single sngValue = 0;
            try
            {
                sngValue = Convert.ToSingle(strInput);
                if (Single.IsNaN(sngValue)) sngValue = sngDefault;
                if (Single.IsInfinity(sngValue)) sngValue = sngDefault;
                if (Single.IsNegativeInfinity(sngValue)) sngValue = sngDefault;
                if (Single.IsPositiveInfinity(sngValue)) sngValue = sngDefault;

            }
            catch
            {
                sngValue = sngDefault;
            }
            return sngValue;
        }
        public static Int32 ConvertToInt(String strInput, Int32 lngDefault)
        {
            Int32 lngValue = 0;
            try
            {
                lngValue = Convert.ToInt32(strInput);
            }
            catch
            {
                lngValue = lngDefault;
            }
            return lngValue;
        }
        public static Decimal ConvertToDecimal(String strInput, Decimal dcmDefault)
        {
            Decimal dcmValue = 0;
            try
            {
                dcmValue = Convert.ToDecimal(strInput);
            }
            catch
            {
                dcmValue = dcmDefault;
            }
            return dcmValue;
        }
        public static int CalculateAge(DateTime birthdate)
        {
            int years = 0;
            try
            {
                // get the difference in years
                years = DateTime.Now.Year - birthdate.Year;
                // subtract another year if we're before the
                // birth day in the current year
                if (DateTime.Now.Month < birthdate.Month || (DateTime.Now.Month == birthdate.Month && DateTime.Now.Day < birthdate.Day))
                    years--;
            }
            catch { }
            return years;
        }
        public static DataTable GetDistinctRows(DataTable dtTable, String colName)
        {
            return dtTable.DefaultView.ToTable(true, colName);
            //myTable = new DataView(myTable).ToTable(true, new string[]{"ColumnYouWantDistinctValuesFor"});

        }
        public static DataTable GetDistinctRows(DataTable dtTable, String[] colName)
        {
            return dtTable.DefaultView.ToTable(true, colName);
            //myTable = new DataView(myTable).ToTable(true, new string[]{"ColumnYouWantDistinctValuesFor"});
        }
        public static String BuildSelectWhere(String strSql, String strWhere)
        {
            if (strSql.ToUpper().Contains("WHERE"))
                strSql += " AND " + strWhere;
            else
                strSql += " WHERE " + strWhere;

            return strSql.ToString();
        }

        public static byte[] CompressDS(DataSet ds)
        {
            return Compress(Encoding.Unicode.GetBytes(ConvertDataSetToXML(ds)));
        }
        public static DataSet DeCompressByteToDataSet(byte[] dsbytes)
        {
            try
            {
                DataSet ds = new DataSet();
                byte[] decompressed_dsBytes = Decompress(dsbytes);
                UnicodeEncoding utf = new UnicodeEncoding();
                String g = utf.GetString(decompressed_dsBytes).Trim();
                if (decompressed_dsBytes[0] == 255)
                    ds.ReadXml(new StringReader(g.Substring(1)));
                else
                    ds.ReadXml(new StringReader(g));

                //ds.ReadXml(new StringReader(Encoding.Unicode.GetString(decompressed_dsBytes)));
                return ds;
            }
            catch
            {
                return null;
            }
        }

        public static byte[] Compress(byte[] data)
        {
            //create a new MemoryStream for holding and
            //returning the compressed ViewState
            MemoryStream output = new MemoryStream();
            //create a new GZipStream object for compressing
            //the ViewState
            GZipStream gzip = new GZipStream(output, CompressionMode.Compress, true);
            //write the compressed bytes to the underlying stream
            gzip.Write(data, 0, data.Length);
            //close the object
            gzip.Close();
            //convert the MemoryStream to an array and return
            //it to the calling method
            return output.ToArray();
        }
        public static byte[] Decompress(byte[] data)
        {
            //create a MemoryStream for holding the incoming data
            MemoryStream input = new MemoryStream();
            //write the incoming bytes to the MemoryStream
            input.Write(data, 0, data.Length);
            //set our position to the start of the Stream
            input.Position = 0;
            //create an instance of the GZipStream to decompress
            //the incoming byte array (the compressed ViewState)
            GZipStream gzip = new GZipStream(input, CompressionMode.Decompress, true);
            //create a new MemoryStream for holding
            //the output
            MemoryStream output = new MemoryStream();
            //create a byte array
            byte[] buff = new byte[64];
            int read = -1;
            //read the decompressed ViewState into
            //our byte array, set that value to our
            //read variable (int data type)
            read = gzip.Read(buff, 0, buff.Length);
            //make sure we have something to read
            while (read > 0)
            {
                //write the decompressed bytes to our
                //out going MemoryStream
                output.Write(buff, 0, read);
                //get the rest of the buffer
                read = gzip.Read(buff, 0, buff.Length);
            }
            gzip.Close();
            //return our out going MemoryStream
            //in an array
            return output.ToArray();
        }
        public static string ConvertDataSetToXML(DataSet xmlDS)
        {
            MemoryStream stream = null;
            XmlTextWriter writer = null;
            try
            {
                stream = new MemoryStream();
                // Load the XmlTextReader from the stream
                writer = new XmlTextWriter(stream, Encoding.Unicode);
                // Write to the file with the WriteXml method.
                xmlDS.WriteXml(writer, XmlWriteMode.WriteSchema);
                int count = (int)stream.Length;
                byte[] arr = new byte[count];
                stream.Seek(0, SeekOrigin.Begin);
                stream.Read(arr, 0, count);
                UnicodeEncoding utf = new UnicodeEncoding();
                return utf.GetString(arr).Trim();
            }
            catch (Exception ex)
            {
                ex.ToString();
                return String.Empty;
            }
            finally
            {
                if (writer != null) writer.Close();
            }
        }

        public static String ConvertBigDataSetToXML(DataSet xmlDS, String strPath)
        {
            FileStream stream = null;
            XmlTextWriter writer = null;
            try
            {
                stream = File.Create(strPath);
                // Load the XmlTextReader from the stream
                writer = new XmlTextWriter(stream, Encoding.Unicode);
                // Write to the file with the WriteXml method.
                xmlDS.WriteXml(writer, XmlWriteMode.WriteSchema);
                int count = (int)stream.Length;
                byte[] arr = new byte[count];
                stream.Seek(0, SeekOrigin.Begin);
                stream.Read(arr, 0, count);
                UnicodeEncoding utf = new UnicodeEncoding();
                return String.Empty;// utf.GetString(arr).Trim();
            }
            catch (Exception ex)
            {
                ex.ToString();
                return String.Empty;
            }
            finally
            {
                if (writer != null)
                    writer.Close();
                stream.Close();
                stream.Dispose();
            }
        }
        public static String CompressBigFile(FileInfo strPath, FileInfo zipPath)
        {
            FileStream output = File.Create(zipPath + strPath.Name + ".zip");
            //create a new GZipStream object for compressing
            //the ViewState
            GZipStream gzip = new GZipStream(output, CompressionMode.Compress, true);
            //write the compressed bytes to the underlying stream

            //byte[] x = File.ReadAllBytes(strPath.FullName);
            //gzip.Write(x, 0, x.Length);

            FileStream fs = File.OpenRead(strPath.FullName);
            int i = 0;
            while (i != fs.Length)
            {
                Byte[] buffer = new Byte[1024];
                i += fs.Read(buffer, 0, buffer.Length);
                gzip.Write(buffer, 0, buffer.Length);
            }

            //close the object
            gzip.Close();
            gzip.Dispose();
            output.Close();
            output.Dispose();

            return zipPath + strPath.Name + ".zip";
        }

        public static String CompressFile(FileInfo strPath)
        {
            //create a new MemoryStream for holding and
            //returning the compressed ViewState
             
            String zipPath = GlobalVariables.zipPath();
            FileStream output = File.Create(zipPath + strPath.Name + ".zip");
            //create a new GZipStream object for compressing
            //the ViewState
            GZipStream gzip = new GZipStream(output, CompressionMode.Compress, true);
            //write the compressed bytes to the underlying stream

            //byte[] x = File.ReadAllBytes(strPath.FullName);
            //gzip.Write(x, 0, x.Length);
                
            //FileStream fs = File.OpenRead(strPath.FullName);
            //Byte[] buffer = new Byte[fs.Length];
            //fs.Read(buffer, 0, buffer.Length);
            //gzip.Write(buffer, 0, buffer.Length);

            FileStream fs = File.OpenRead(strPath.FullName);
            int i = 0;
            //for (int i = 0; i < fs.Length; i = i + 1024)
            while (i != fs.Length)
            {
                Byte[] buffer = new Byte[1024];
                i += fs.Read(buffer, 0, buffer.Length);
                gzip.Write(buffer, 0, buffer.Length);
            }
            
            //close the object
            gzip.Close();
            gzip.Dispose();
            output.Close();
            output.Dispose();

            return zipPath + strPath.Name + ".zip";
        }
        public static String ZipFile(FileInfo strPath, FileInfo DestPath)
        {
            String zipPath = DestPath.FullName;
            String exe = GlobalVariables.UtilPath() + "zip";
            exe += " -D -9 ";
            exe += zipPath + strPath.Name + ".zip ";
            exe += strPath.FullName;
            File.WriteAllText(zipPath + "yo.bat", exe);
            Process proc = new Process();
            proc.StartInfo.FileName = zipPath + "yo.bat";
            proc.Start();
            proc.WaitForExit();
            proc.Close();
            proc.Dispose();
            return zipPath + strPath.Name + ".zip";
        }

        //New Version. This one seems more compressed than above.
        public static Byte[] CompressDsToBytes(DataSet ds)
        {
            try
            {
                Byte[] data;
                MemoryStream memStream = new MemoryStream();
                GZipStream zipStream = new GZipStream(memStream, CompressionMode.Compress);
                ds.WriteXml(zipStream, XmlWriteMode.WriteSchema);
                zipStream.Close();
                data = memStream.ToArray();
                memStream.Close();
                return data;
            }
            catch
            {
                return new Byte[1];
            }
        }
        public static DataSet DecompressBytesToDs(Byte[] data)
        {
            try
            {
                MemoryStream memStream = new MemoryStream(data);
                GZipStream unzipStream = new GZipStream(memStream, CompressionMode.Decompress);
                DataSet objDataSet = new DataSet();
                objDataSet.ReadXml(unzipStream, XmlReadMode.ReadSchema);
                unzipStream.Close();
                memStream.Close();
                return objDataSet;
            }
            catch
            {
                return new DataSet();
            }
        }
    }
}