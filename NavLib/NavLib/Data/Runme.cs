using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.IO;
using System.Windows.Forms;
using NavLib.AT100;
using NavLib.STEPP;
using NavLib.Globals;
using NavLib.Processing;
using NavLib.AT220;
using NavLib.Orion;
using NavLib.Enfora;
using NavLib.Atrack;
using NavLib.AstraTelematics.ProtokolKL;
using NavLib.AstraTelematics.ProtokolMNV;

namespace NavLib.Data
{
    public class Runme
    {
        public Runme()
        {
        }

        public void CheckDB()
        {
            SystemHelper sh = new SystemHelper();
            sh.UpdateDB("ConnStr");
            sh = new SystemHelper();
            sh.UpdateDB("ConnStrTransferred");
        }

        public class RawDataInsertResult
        {
            public Boolean bInsertOK { get; set; }
            public String UnitID { get; set; }
            public String strUnitModel { get; set; }
        }
        public RawDataInsertResult InsertRawData(Byte[] bytWorking, String strUnitModel, String sSerialNo = "No")
        {
            Boolean bInsertOK = true;
            int iResult = -999;
            String UnitID = String.Empty;
            try
            {
                switch (strUnitModel)
                {
                    case "C":
                        UnitID = new DecodeAT100ProtocolC(bytWorking).GetUnitID();
                        strUnitModel = "AT100";
                        break;

                    case "G":
                        UnitID = new DecodeAT220ProtocolG(bytWorking).GetUnitID();
                        break;

                    case "I":
                        UnitID = new AT200Parser(bytWorking, (byte)'K').objHeader.GetUnitID();
                        strUnitModel = "ASTRAIMSI";
                        break;

                    case "K":
                        UnitID = new AT200Parser(bytWorking, (byte)'K').objHeader.GetUnitID();
                        strUnitModel = "AT200";
                        break;

                    case "V":
                        UnitID = new AstraParser(bytWorking, (byte)'V').objHeader.GetUnitID();
                        strUnitModel = "AT240";
                        break;

                    case "R":
                        UnitID = "Reza";
                        strUnitModel = "Reza";
                        break;

                    case "AT200":
                        UnitID = new AT200Parser(bytWorking, (byte)'K').objHeader.GetUnitID();
                        strUnitModel = "AT200";
                        break;

                    case "AT100":
                        UnitID = new DecodeAT100ProtocolC(bytWorking).GetUnitID();
                        break;

                    case "AT100IMSI":
                        UnitID = new DecodeAT100IMSI(bytWorking).GetUnitID();
                        break;

                    case "STEPP":
                        UnitID = new DecodeStepp(bytWorking).GetUnitIMEI;
                        break;

                    case "ORION":
                        UnitID = new DecodeOrion(bytWorking).oReports[0].GetSerialNo();
                        if (UnitID == null)
                            UnitID = String.Empty;

                        //Do not save LKIN reports
                        if (new DecodeOrion(bytWorking).oReports[0].GetLogReason().Contains("LKIN"))
                            UnitID = String.Empty;

                        //IMSI
                        if (UnitID != String.Empty)
                            if (new DecodeOrion(bytWorking).oReports[0].GetIMSI().Trim().Length > 3)
                                strUnitModel = "OrionImsi";
                        break;

                    case "MEILIGAO":
                        UnitID = new NavLib.Meitrack.DecodeMeiligao(bytWorking).GetSerial();
                        break;

                    case "MVT100":
                        //UnitID = new NavLib.Meitrack.DecodeMeiTrack(bytWorking).GetIMEI();
                        NavLib.Meitrack.DecodeMeiTrack dMeiTrackMVT100 = new NavLib.Meitrack.DecodeMeiTrack(bytWorking);
                        UnitID = dMeiTrackMVT100.GetIMEI();
                        if (dMeiTrackMVT100.GetProtocol() == "B58")
                            strUnitModel = "MeitrackImsi";
                        break;

                    case "MVT340":
                        //UnitID = new NavLib.Meitrack.DecodeMeiTrack(bytWorking).GetIMEI();
                        NavLib.Meitrack.DecodeMeiTrack dMeiTrackMVT340 = new NavLib.Meitrack.DecodeMeiTrack(bytWorking);
                        UnitID = dMeiTrackMVT340.GetIMEI();
                        if (dMeiTrackMVT340.GetProtocol() == "B58")
                            strUnitModel = "MeitrackImsi";
                        break;

                    case "MVT380":
                        NavLib.Meitrack.DecodeMeiTrack dMeiTrack = new NavLib.Meitrack.DecodeMeiTrack(bytWorking);
                        UnitID = dMeiTrack.GetIMEI();
                        if (dMeiTrack.GetProtocol() == "B58")
                            strUnitModel = "MeitrackImsi";
                        break;

                    case "ATRACK":
                        if (sSerialNo != "No")
                        {
                            try
                            {
                                DecodeAtrack dAtrack = new DecodeAtrack(bytWorking);
                                UnitID = dAtrack.GetUnitID();
                            }
                            catch
                            {
                                UnitID = String.Empty;
                            }

                            if (UnitID.Length != 15)
                                UnitID = sSerialNo;
                        }
                        else
                        {
                            //default
                            DecodeAtrack dAtrack = new DecodeAtrack(bytWorking);
                            UnitID = dAtrack.GetUnitID();
                            if (dAtrack.isIMSI())
                                strUnitModel = "AtrackImsi";
                        }
                        break;

                    case "XEXUN":
                        UnitID = new NavLib.Xexun.DecodeXexun(bytWorking).getImei();
                        break;

                    case "METRON":
                        UnitID = new NavLib.Metron.DecodeMetron(bytWorking).GetUnitID();
                        break;

                    case "METRONATEX":
                        UnitID = new NavLib.Metron.DecodeMetronAtex(bytWorking).GetUnitID();
                        break;

                    case "ENFORN":
                        try
                        {
                            DecodeEnfora de = new DecodeEnfora(bytWorking);
                            UnitID = de.GetMdMid();
                            if (de.isInformationalData())
                                strUnitModel = "EnfornIMSI";
                        }
                        catch
                        {
                            UnitID = "1234";
                        }
                        break;

                    case "Enfora":
                        if (new clsGSM2218(bytWorking).IsIMSI())
                        {
                            UnitID = new clsGSM2218(bytWorking).GetUnitNameFromIMSI();
                            strUnitModel = "EnforaImsi";
                        }
                        else if (new clsGSM2218(bytWorking).IsGPSValid())
                            UnitID = new clsGSM2218(bytWorking).GetUnitName();
                        break;

                    case "Inventia":
                        UnitID = sSerialNo;
                        break;

                    case "TELTONIKA":
                        UnitID = sSerialNo;
                        break;

                    case "TELTONIKAIMSI":
                        UnitID = sSerialNo;
                        break;
                }
            }
            catch (Exception ex)
            {
                bInsertOK = false;
                String Msg = Encoding.ASCII.GetString(bytWorking);
                Logger.WriteToErrorLog(ex, Msg);
            }
            if (UnitID.Trim().Length > 0)
            {
                String sql = @"
insert into RawData (RawData, Model, unitID)
    select  @bData, @Model, @unitID
        from Devices d WITH (NOLOCK)
    where d.unitID = @unitID";

                //13 June 2020 commented for now after ransomware attack. will revert back codes by the 18 June 2020
                if (GlobalVariables.GetsConnStr("ConnStr").Contains("DellReza"))
                    sql = "insert into RawData (RawData, Model, unitID) values (@bData, @Model, @unitID)";

                SqlConnection CN = new SqlConnection(GlobalVariables.GetsConnStr("ConnStr"));
                SqlCommand sqlCmd = new SqlCommand(sql, CN);
                CN.Open();
                try
                {
                    Byte[] bytWorkingReduced = Utils.CompressBytWorking(bytWorking);
                    sqlCmd.Parameters.Add(new SqlParameter("@bData", SqlDbType.VarBinary));//(object)bytWorkingReduced));
                    sqlCmd.Parameters.Add(new SqlParameter("@Model", SqlDbType.VarChar));//(object)strUnitModel));
                    sqlCmd.Parameters.Add(new SqlParameter("@unitID", SqlDbType.VarChar));//(object)UnitID));

                    sqlCmd.Parameters["@bData"].Value = bytWorkingReduced;
                    sqlCmd.Parameters["@Model"].Value = strUnitModel;
                    sqlCmd.Parameters["@unitID"].Value = UnitID;
                    iResult = sqlCmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    bInsertOK = false;
                    Logger.WriteToErrorLog(ex, sql + "; UnitID = " + UnitID + "; Model = " + strUnitModel + "; bytWorking " + Encoding.UTF8.GetString(bytWorking));
                }
                finally
                {
                    CN.Close();
                    CN.Dispose();
                }
            }

            if (iResult == 0)
            {
                //Logger.WriteToErrorLog(" UnitID = " + UnitID + " not inserted", "RawDataInsert", "RawData", Application.StartupPath.ToString());

                String app_path = Application.StartupPath.ToString();
                if (!(System.IO.Directory.Exists(app_path + "\\Errors\\")))
                    System.IO.Directory.CreateDirectory(app_path + "\\Errors\\");
                File.AppendAllText(app_path + "\\Errors\\UnitsNotRegistered.txt", UnitID + "\r\n");

                try
                {
                    FileInfo f = new FileInfo(app_path + "\\Errors\\UnitsNotRegistered.txt");
                    if (f.Length > 100000)
                        File.Delete(app_path + "\\Errors\\UnitsNotRegistered.txt");
                }
                catch { }

                bInsertOK = false;
            }

            RawDataInsertResult rawDataInsertResult = new RawDataInsertResult();
            rawDataInsertResult.bInsertOK = bInsertOK;
            rawDataInsertResult.UnitID = UnitID;
            rawDataInsertResult.strUnitModel = strUnitModel;
            return rawDataInsertResult;
        }

        public int InsertDevices(String strDeviceID)
        {
            int iResult = -999;
            if (strDeviceID.Trim().Length > 0)
            {
                String sql = @"
insert Devices (UnitID) 
	select @UnitID from Devices where unitID = @UnitID having count(1) = 0
";

                SqlConnection CN = new SqlConnection(GlobalVariables.GetsConnStr("ConnStr"));
                SqlCommand sqlCmd = new SqlCommand(sql, CN);
                CN.Open();
                try
                {
                    sqlCmd.Parameters.Add(new SqlParameter("@unitID", SqlDbType.VarChar));

                    sqlCmd.Parameters["@unitID"].Value = strDeviceID;
                    iResult = sqlCmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    Logger.WriteToErrorLog(ex, sql + "; UnitID = " + strDeviceID);
                }
                finally
                {
                    CN.Close();
                    CN.Dispose();
                }
            }
            return iResult;
        }
        public int RemoveDevices(String strDeviceID)
        {
            int iResult = -999;
            if (strDeviceID.Trim().Length > 0)
            {
                String sql = "delete from Devices where UnitID =  @UnitID";

                SqlConnection CN = new SqlConnection(GlobalVariables.GetsConnStr("ConnStr"));
                SqlCommand sqlCmd = new SqlCommand(sql, CN);
                CN.Open();
                try
                {
                    sqlCmd.Parameters.Add(new SqlParameter("@unitID", SqlDbType.VarChar));

                    sqlCmd.Parameters["@unitID"].Value = strDeviceID;
                    iResult = sqlCmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    Logger.WriteToErrorLog(ex, sql + "; UnitID = " + strDeviceID);
                }
                finally
                {
                    CN.Close();
                    CN.Dispose();
                }
            }
            return iResult;
        }

        void MoveNDeleteTransferredDataObsolete()
        {
            //A data should be in ONLY 1 db at a time...
            // Only Transferred data is transferred to TransferredDB
            // Fuel data not transferred to TransferredDB for now

            //Transfer Data to TransferredDB
            String sql = "update RawData set status = status * 100";
            sql += "\r\nwhere (status = 2 or status = 4) ";
            //sql += "\r\n    and dtdatetime < GETDATE() - 1";
            new Runme().RunSQLCMD(sql, "ConnStr", 4000);

            sql = "select * from RawData ";
            sql += "\r\nwhere (status >= 200) ";// or status = 400) ";
            DataSet ds = new Runme().GetDataSet(sql, "ConnStr");
            SqlConnection CNDest = new SqlConnection(GlobalVariables.GetsConnStr("ConnStrTransferred"));
            CNDest.Open();
            SqlBulkCopy bulk = new SqlBulkCopy(CNDest);
            bulk.DestinationTableName = "RawData";
            try
            {
                bulk.BulkCopyTimeout = 4000;
                bulk.WriteToServer(ds.Tables[0]);
            }
            catch { }
            finally
            {
                CNDest.Close();
                CNDest.Dispose();
            }

            //Delete Downloaded Data only from Live DB
            sql = "delete from RawData ";
            sql += "\r\nwhere (status >= 200) ";// or status = 400) ";
            new Runme().RunSQLCMD(sql, "ConnStr", 4000);

            //All undownloaded data from Live DB
            sql = "delete from RawData ";
            sql += "\r\nwhere dtdatetime < GETDATE() - 15";
            new Runme().RunSQLCMD(sql, "ConnStr", 4000);

            //Delete Fuel data from Live DB. Note : Fuel Data only found in Live DB
            sql = "delete from FuelData ";
            sql += "\r\nwhere status = 2 ";
            //sql += "\r\n	and dtdatetime < GETDATE() - 1";
            new Runme().RunSQLCMD(sql, "ConnStr", 4000);
            sql = "delete from FuelData ";
            sql += "\r\nwhere dtdatetime < GETDATE() - 15";
            new Runme().RunSQLCMD(sql, "ConnStr", 4000);

            //Delete Downloaded Data only - TransferredDB
            sql = "delete from RawData ";
            sql += "\r\n    where dtdatetime < GETDATE() - 7";
            new Runme().RunSQLCMD(sql, "ConnStrTransferred", 4000);
        }
        public String MoveNDeleteTransferredDataToBeTested()
        {
            //A data should be in ONLY 1 db at a time...
            // Only Transferred data is transferred to TransferredDB
            // Fuel data not transferred to TransferredDB for now
            String strResult = String.Empty;
            String sql = String.Empty;

            //Transfer Data to TransferredDB
            sql = "update RawData set RawData.status = RawData.status * 100 where (status = 2 or status = 4)";
            new Runme().RunSQLCMD(sql, "ConnStr", 4000);

            for (int i = 0; i < 1200; i++)
            {
                sql = "select * from RawData, ";
                sql += "\r\n(select top 10000 * from RawData";
                sql += "\r\nwhere (status >= 200)";
                sql += "\r\norder by dtdatetime ";
                sql += "\r\n) as t1 where RawData.iID = t1.iID";

                DataSet ds = new Runme().GetDataSet(sql, "ConnStr");
                int iCount = ds.Tables[0].Rows.Count;
                strResult = "No of Rows " + iCount.ToString() + " No of loops " + i.ToString() + " ";
                if (iCount == 0)
                    i = 999999;
                SqlConnection CNDest = new SqlConnection(GlobalVariables.GetsConnStr("ConnStrTransferred"));
                CNDest.Open();
                SqlBulkCopy bulk = new SqlBulkCopy(CNDest);
                bulk.DestinationTableName = "RawData";
                try
                {
                    bulk.BulkCopyTimeout = 4000;
                    bulk.WriteToServer(ds.Tables[0]);
                }
                catch { }
                finally
                {
                    CNDest.Close();
                    CNDest.Dispose();
                }
            }

            //Delete Downloaded Data only from Live DB
            sql = "delete from RawData ";
            sql += "\r\nwhere (status >= 200) ";// or status = 400) ";
            new Runme().RunSQLCMD(sql, "ConnStr", 4000);

            //All undownloaded data from Live DB
            sql = "delete from RawData ";
            sql += "\r\nwhere dtdatetime < GETDATE() - 15";
            new Runme().RunSQLCMD(sql, "ConnStr", 4000);

            //Delete Fuel data from Live DB. Note : Fuel Data only found in Live DB
            sql = "delete from FuelData ";
            sql += "\r\nwhere status = 2 ";
            //sql += "\r\n	and dtdatetime < GETDATE() - 1";
            new Runme().RunSQLCMD(sql, "ConnStr", 4000);
            sql = "delete from FuelData ";
            sql += "\r\nwhere dtdatetime < GETDATE() - 15";
            new Runme().RunSQLCMD(sql, "ConnStr", 4000);

            //Delete Downloaded Data only - TransferredDB
            sql = "delete from RawData ";
            sql += "\r\n    where dtdatetime < GETDATE() - 7";
            new Runme().RunSQLCMD(sql, "ConnStrTransferred", 4000);

            return strResult;
        }

        public String MoveNDeleteTransferredData(String strMyServer)
        {
            File.Delete(GlobalVariables.logPath() + "ArchiveRunning.txt");
            File.AppendAllText(GlobalVariables.logPath() + "ArchiveRunning.txt", DateTime.Now + " Deleted Started " + "\r\n");

            //A data should be in ONLY 1 db at a time...
            // Only Transferred data is transferred to TransferredDB
            // Fuel data not transferred to TransferredDB for now
            String strResult = String.Empty;
            String sql = String.Empty;

            //All undownloaded data from Live DB
            strResult += DateTime.Now.ToString() + " Delete RawData Started. ";
            sql = @"
--declare @cnt int
--set @cnt = (select count(1) from RawData)
--select count(8) from RawData where dtdatetime < GETDATE() - 21
Declare @dtStart datetime
set @dtStart = getdate()
Declare @i int
set @i = 0

;WITH T AS
(
	select top 100 * from RawData where dtdatetime < GETDATE() - 21
)
DELETE FROM T

while @@rowcount > 0
begin
	set @i = @i + 1
	;WITH T AS
	(
		select top 100 * from RawData where dtdatetime < @dtStart - 21
	)
	DELETE FROM T
	if(@@rowcount > 0)
		if(DATEDIFF(mi, @dtStart, GETDATE()) < 240)
			set @i = @i * 1;
end
--print @i

--set @cnt = @cnt - (select count(1) from RawData)
--select DATEDIFF(mi, @dtStart, GETDATE()), @dtStart dtStart, GETDATE() dtEnd --, @cnt
--select count(8) from RawData where dtdatetime < GETDATE() - 31
";
            new Runme().RunSQLCMD(sql, "ConnStr", 15000); //240 mins * 60 seconds
            strResult += "ended " + DateTime.Now.ToString() + "\r\n";
            File.AppendAllText(GlobalVariables.logPath() + "ArchiveRunning.txt", DateTime.Now + " deleted ended " + "\r\n");

            //Delete Fuel data from Live DB. Note : Fuel Data only found in Live DB
            sql = "delete from FuelData where status = 2 ";
            strResult += DateTime.Now.ToString() + " ConnStr Started. " + sql + ". ";
            new Runme().RunSQLCMD(sql, "ConnStr", 400);
            strResult += "ended " + DateTime.Now.ToString() + "\r\n";

            sql = "delete from FuelData where dtdatetime < GETDATE() - 15";
            strResult += DateTime.Now.ToString() + " ConnStr Started. " + sql + ". ";
            new Runme().RunSQLCMD(sql, "ConnStr", 400);
            strResult += "ended " + DateTime.Now.ToString() + "\r\n";
            File.AppendAllText(GlobalVariables.logPath() + "ArchiveRunning.txt", DateTime.Now + " deleted fuel ended " + "\r\n");

            //Delete Downloaded Data only - TransferredDB
            sql = @"
Declare @dtStart datetime
set @dtStart = getdate()
Declare @i int
set @i = 0

;WITH T AS
(
	select top 100 * from RawData where dtdatetime < GETDATE() - 7
)
DELETE FROM T
while @@rowcount > 0
begin
	set @i = @i + 1
    ;WITH T AS
    (
	    select top 100 * from RawData where dtdatetime < GETDATE() - 7
    )
    DELETE FROM T

	if(DATEDIFF(mi, @dtStart, GETDATE()) < 61) --Stop after 60 mins
		select 1
end
print @i
select DATEDIFF(mi, @dtStart, GETDATE()), @dtStart, GETDATE()
";
            strResult += DateTime.Now.ToString() + "delete 7 days from ConnStrTransferred Started. ";
            new Runme().RunSQLCMD(sql, "ConnStrTransferred", 3600);
            strResult += "ended " + DateTime.Now.ToString() + "\r\n";
            File.AppendAllText(GlobalVariables.logPath() + "ArchiveRunning.txt", DateTime.Now + " Trfd deleted ended " + "\r\n");

            if (!(System.IO.Directory.Exists(Application.StartupPath.ToString() + "\\Log\\")))
                System.IO.Directory.CreateDirectory(Application.StartupPath.ToString() + "\\Log\\");
            File.WriteAllText(Application.StartupPath.ToString() + "\\Log\\Trfd.txt", strResult);

            int iCount = 0;
            int iLoop = 0;
            DateTime dtStart = DateTime.Now;
            for (int i = 0; i < 25000; i++)
            {
                iLoop++;

                if (Math.Abs(DateTime.Now.Hour - dtStart.Hour) >= 5)
                    break;

                if (DateTime.Now.Hour <= 18)
                    if (DateTime.Now.Hour >= 6)
                        break;

                SqlConnection CNDest = new SqlConnection(GlobalVariables.GetsConnStr("ConnStrTransferred"));
                try
                {
                    //Transfer Data to TransferredDB
                    sql = "update RawData set RawData.status = RawData.status * 100";
                    sql += "\r\nfrom (select top 100 * from RawData";
                    sql += "\r\nwhere dtdatetime < GETDATE() - 3 and (status = 2 or status = 4)";
                    sql += "\r\norder by dtdatetime ";
                    sql += "\r\n) as t1 where RawData.iID = t1.iID";
                    new Runme().RunSQLCMD(sql, "ConnStr", 200);

                    sql = "select * from RawData where status >= 200 ";
                    DataSet ds = new Runme().GetDataSet(sql, "ConnStr", 200);
                    Boolean bCopied = false;
                    if (ds != null)
                        if (ds.Tables.Count > 0)
                        {
                            iCount += ds.Tables[0].Rows.Count;

                            CNDest.Open();
                            SqlBulkCopy bulk = new SqlBulkCopy(CNDest);
                            bulk.DestinationTableName = "RawData";
                            bulk.BulkCopyTimeout = 200;
                            bulk.WriteToServer(ds.Tables[0]);
                            bCopied = true;
                        }

                    //Delete Downloaded Data only from Live DB
                    sql = "delete from RawData where (status >= 200) ";
                    sql = @"delete top (100) RawData where (status >= 200)
                            while @@rowcount > 0
                            begin
                                delete top (100) RawData where (status >= 200)
                            end";
                    if (bCopied)
                        new Runme().RunSQLCMD(sql, "ConnStr", 400);
                }
                catch (Exception ex)
                {
                    File.AppendAllText(Application.StartupPath.ToString() + "\\Log\\Trfd.txt", DateTime.Now.ToString() + " Failed... " + sql + i + "\r\n");
                    Logger.WriteToErrorLog(ex);
                }
                finally
                {
                    CNDest.Close();
                    CNDest.Dispose();
                }

                File.AppendAllText(GlobalVariables.logPath() + "ArchiveRunning.txt", DateTime.Now + " Data Migration deleted ended " + "\r\n");
                File.AppendAllText(Application.StartupPath.ToString() + "\\Log\\Trfd.txt", DateTime.Now.ToString() + " ConnStr " + sql + "\r\n");
            }

            strResult += DateTime.Now.ToString() + "No. of Rows " + iCount.ToString() + " No of loops " + iLoop.ToString() + " ";
            return strResult;
        }
        public String DatabaseMaintenance(String strMyServer)
        {
            String strResult = MoveNDeleteTransferredData(strMyServer);
            String sql = String.Empty;

            if (DateTime.Now.Hour > 6)
                if (DateTime.Now.Hour < 18)
                    return strResult;

            //Now Shrink using SQL Agent as below
            //use CoreData
            //ALTER DATABASE CoreData SET RECOVERY SIMPLE;
            //DBCC SHRINKFILE (CoreData_log, 1);
            //ALTER DATABASE CoreData SET RECOVERY FULL;
            //GO

            //strResult += "\r\n" + DateTime.Now + "SHRINKDATABASE Started";
            //sql = "DBCC SHRINKDATABASE (" + GlobalVariables.GetDBName("ConnStr") + ", 10)";
            //new Runme().RunSQLCMD(sql, "ConnStr", 400);
            //strResult += " Shrink completed @ " + DateTime.Now; ;

            int iResult = Convert.ToInt32(new Runme().GetDataSet("select IDENT_CURRENT('RawData')", "ConnStr").Tables[0].Rows[0][0]);
            if (iResult >= 2000000000)
                new Runme().RunSQLCMD("DBCC CHECKIDENT (RawData, RESEED, 1)", "ConnStr", 400);

            if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday)
            {
                sql = "ALTER INDEX ALL ON [dbo].[RawData] REBUILD WITH ( PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, SORT_IN_TEMPDB = OFF, ONLINE = OFF )";
                RunSQLCMD(sql, "ConnStr", 400);
                sql = "ALTER INDEX ALL ON [dbo].[RawData] REORGANIZE WITH ( LOB_COMPACTION = ON )";
                RunSQLCMD(sql, "ConnStr", 400);
                sql = "ALTER INDEX ALL ON RawData REBUILD WITH (FILLFACTOR = 90)";
                RunSQLCMD(sql, "ConnStr", 400);
                strResult += "Alter index GeoLink done ";
            }

            if (DateTime.Now.Hour > 6)
                if (DateTime.Now.Hour < 18)
                    return strResult;

            //strResult += "\r\n" + DateTime.Now + "SHRINKDATABASE ConnStrTransferred Started";
            //sql = "DBCC SHRINKDATABASE (" + GlobalVariables.GetDBName("ConnStrTransferred") + ", 10)";
            //new Runme().RunSQLCMD(sql, "ConnStrTransferred", 400);
            //strResult += " Shrink completed @ " + DateTime.Now; ;

            iResult = Convert.ToInt32(new Runme().GetDataSet("select IDENT_CURRENT('RawData')", "ConnStrTransferred").Tables[0].Rows[0][0]);
            if (iResult >= 2000000000)
                new Runme().RunSQLCMD("DBCC CHECKIDENT (RawData, RESEED, 1)", "ConnStrTransferred", 400);

            if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday)
            {
                if (DateTime.Now.Hour > 6)
                    if (DateTime.Now.Hour < 18)
                        return strResult;

                sql = "ALTER INDEX ALL ON [dbo].[RawData] REBUILD WITH ( PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, SORT_IN_TEMPDB = OFF, ONLINE = OFF )";
                RunSQLCMD(sql, "ConnStrTransferred", 400);
                sql = "ALTER INDEX ALL ON [dbo].[RawData] REORGANIZE WITH ( LOB_COMPACTION = ON )";
                RunSQLCMD(sql, "ConnStrTransferred", 400);
                strResult += "Alter index GeoLinkT done ";
            }

            return strResult;
            //Reset Identity
            //select IDENT_CURRENT('RawData')
            //DBCC CHECKIDENT (RawData)
            //DBCC CHECKIDENT (RawData, RESEED, 1)
            //DBCC CHECKIDENT (RawData, NORESEED)
        }

        #region Azure
        public String AzureLinkDatabaseMaintenance()
        {
            String strResult = MoveToTransferredData();

            if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday)
            {
                int iResult = Convert.ToInt32(new Runme().GetDataSet("select IDENT_CURRENT('RawData')", "ConnStr").Tables[0].Rows[0][0]);
                if (iResult >= 2000000000)
                    new Runme().RunSQLCMD("DBCC CHECKIDENT (RawData, RESEED, 1)", "ConnStr", 400);

                String sql = "ALTER INDEX ALL ON [dbo].[RawData] REBUILD WITH ( PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, SORT_IN_TEMPDB = OFF, ONLINE = OFF )";
                RunSQLCMD(sql, "ConnStr", 400);
                sql = "ALTER INDEX ALL ON [dbo].[RawData] REORGANIZE WITH ( LOB_COMPACTION = ON )";
                RunSQLCMD(sql, "ConnStr", 400);
                sql = "ALTER INDEX ALL ON RawData REBUILD WITH (FILLFACTOR = 90)";
                RunSQLCMD(sql, "ConnStr", 400);
                strResult += "Alter index GeoLink done ";
            }

            return strResult;
        }
        public String MoveToTransferredData()
        {
            File.Delete(GlobalVariables.logPath() + "ArchiveRunning.txt");

            //A data should be in ONLY 1 db at a time...
            String strResult = "Started @" + DateTime.Now + "\r\n";
            File.AppendAllText(GlobalVariables.logPath() + "ArchiveRunning.txt", strResult);

            //Migrating undownloaded data
            String sql = "update RawData set status = 5 where dtdatetime < GETDATE() - 21 and status = 0";
            new Runme().RunSQLCMD(sql, "ConnStr", 200);

            int iCount = 0;
            int iLoop = 0;
            int iConsecutiveCnt = 0;
            for (int i = 0; i < 25000; i++)
            {
                SqlConnection CNDest = new SqlConnection(GlobalVariables.GetsConnStr("ConnStrTransferred"));
                try
                {
                    //Transfer Data to TransferredDB                    
                    sql = @"
update RawData 
    set RawData.status = RawData.status * 100
from 
    (
        select 
            top 100 * 
        from RawData
            where dtdatetime < GETDATE() - 3 and (status = 2 or status = 4 or status = 5)
        order by dtdatetime 
    ) as t1 
where RawData.iID = t1.iID";
                    new Runme().RunSQLCMD(sql, "ConnStr", 200);

                    sql = "select top 100 * from RawData where status >= 200 order by iid";
                    DataSet ds = new Runme().GetDataSet(sql, "ConnStr", 200);
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count == 0)
                            iConsecutiveCnt++;
                        else
                            iConsecutiveCnt = 0;

                        if (iConsecutiveCnt == 5)
                            i = 100000;

                        iCount += ds.Tables[0].Rows.Count;

                        CNDest.Open();
                        SqlBulkCopy bulk = new SqlBulkCopy(CNDest);
                        bulk.DestinationTableName = "RawData";
                        bulk.BulkCopyTimeout = 200;
                        bulk.WriteToServer(ds.Tables[0]);
                        CNDest.Close();

                        //Delete Downloaded Data only from Live DB
                        sql = "select 1";
                        foreach (DataRow dr in ds.Tables[0].Rows)
                            sql += "\r\nDelete from RawData where iid = " + dr["iID"];

                        new Runme().RunSQLCMD(sql, "ConnStr", 400);
                    }
                }
                catch (Exception ex)
                {
                    File.AppendAllText(GlobalVariables.logPath() + "ArchiveRunning.txt", DateTime.Now.ToString() + " Failed... " + ex.ToString() + i + "\r\n" + sql + "\r\n");
                }
                finally
                {
                    if (i < 100000)
                        iLoop = i;
                    else
                        iLoop = 100000 + i;

                    try
                    {
                        if (CNDest.State == ConnectionState.Open)
                            CNDest.Close();
                        CNDest.Dispose();
                    }
                    catch (Exception ex)
                    {
                        File.AppendAllText(GlobalVariables.logPath() + "ArchiveRunning.txt", DateTime.Now.ToString() + " Failed... " + ex.ToString() + i + "\r\n" + sql + "\r\n");
                    }
                }

                File.AppendAllText(GlobalVariables.logPath() + "ArchiveRunning.txt", DateTime.Now + " Data Migration deleted ended " + "\r\n");
            }

            strResult += DateTime.Now.ToString() + "No. of Rows " + iCount.ToString() + " No of loops " + iLoop.ToString() + " ";
            File.AppendAllText(GlobalVariables.logPath() + "ArchiveRunning.txt", strResult);
            return strResult;
        }

        #endregion

        public String DatabaseMaintenanceOfNaveoOne()
        {
            String strResult = String.Empty;
            String sql = String.Empty; ;

            //Now Shrink using SQL Agent as below
            //use CoreData
            //ALTER DATABASE CoreData SET RECOVERY SIMPLE;
            //DBCC SHRINKFILE (CoreData_log, 1);
            //ALTER DATABASE CoreData SET RECOVERY FULL;
            //GO

            //String strResult = "\r\nSHRINKDATABASE started @ " + DateTime.Now.ToString();
            //String sql = "DBCC SHRINKDATABASE (" + GlobalVariables.GetDBName("ConnStr") + ", 10)";
            //new Runme().RunSQLCMD(sql, "ConnStr", 8000);
            //strResult += "\r\nSHRINKDATABASE completed @ " + DateTime.Now.ToString();

            sql = @"DECLARE @Table VARCHAR(255)  
                    DECLARE @cmd NVARCHAR(500)  
                    DECLARE @fillfactor INT 
                    SET @fillfactor = 90 

                    Declare @dtStart DateTime
                    set @dtStart = GETDATE()

                    SET @cmd = 'DECLARE TableCursor CURSOR FOR SELECT ''['' + table_catalog + ''].['' + table_schema + ''].['' + 
                        table_name + '']'' as tableName FROM ' + " + "'" + GlobalVariables.GetDBName("ConnStr") + "'" + @" + '.INFORMATION_SCHEMA.TABLES 
                        WHERE table_type = ''BASE TABLE'''   

                        -- create table cursor  
                        EXEC (@cmd)  
                        OPEN TableCursor   

                        FETCH NEXT FROM TableCursor INTO @Table   
                        WHILE @@FETCH_STATUS = 0   
                        BEGIN   
                            IF (@@MICROSOFTVERSION / POWER(2, 24) >= 9)
                            BEGIN
                                -- SQL 2005 or higher command 
                                SET @cmd = 'ALTER INDEX ALL ON ' + @Table + ' REBUILD WITH (FILLFACTOR = ' + CONVERT(VARCHAR(3),@fillfactor) + ')' 
                                EXEC (@cmd) 
                            END
                            ELSE
                            BEGIN
                                -- SQL 2000 command 
                                DBCC DBREINDEX(@Table,' ',@fillfactor)  
                            END

		                    if(ABS(DATEPART(Hour, GETDATE() - @dtStart)) >= 2)
			                    break

		                    if(ABS(DATEPART(Hour, GETDATE())) > 6)
		                     if(ABS(DATEPART(Hour, GETDATE())) < 18)
			                        break

                            FETCH NEXT FROM TableCursor INTO @Table   
                        END   

                        CLOSE TableCursor   
                        DEALLOCATE TableCursor";
            RunSQLCMD(sql, "ConnStr", 8000);
            strResult += "\r\nRebuild indexes completed @ " + DateTime.Now.ToString();

            return strResult + "\r\n";
        }

        public void RunSQLCMDWithParam(String sql, String sConnStr, int timeout, String param)
        {
            SqlConnection CN = new SqlConnection(GlobalVariables.GetsConnStr(sConnStr));
            SqlCommand sqlCmd = new SqlCommand(sql, CN);
            if (timeout > 0)
                sqlCmd.CommandTimeout = timeout;
            CN.Open();
            String[] strParams = param.Split(',');
            foreach (String s in strParams)
                if (s != String.Empty)
                {
                    String[] sp = s.Split('-');
                    sqlCmd.Parameters.Add(new SqlParameter(sp[0], (object)sp[1]));
                }
            sqlCmd.ExecuteNonQuery();
            CN.Close();
            CN.Dispose();
        }
        public Boolean RunSQLCMD(String sql, String sConnStr, int timeout)
        {
            Boolean bHasError = false;
            SqlConnection CN = new SqlConnection(GlobalVariables.GetsConnStr(sConnStr));
            SqlCommand sqlCmd = new SqlCommand(sql, CN);
            if (timeout > 0)
                sqlCmd.CommandTimeout = timeout;
            try
            {
                CN.Open();
                sqlCmd.ExecuteNonQuery();
            }
            catch
            {
                bHasError = true;
            }
            //catch (Exception ex) { MessageBox.Show(ex.ToString()); }
            finally
            {
                CN.Close();
                CN.Dispose();
            }
            return bHasError;
        }
        public void ObsoleteRunSQLCMD(String sql, String sConnStr)
        {
            String str = String.Empty;
            if (sConnStr == "ConnStr")
                str = ConfigurationManager.AppSettings[sConnStr].ToString();
            else
            {
                str = "Data Source=";
                str += ConfigurationManager.AppSettings["GeotabServer"].ToString();
                str += ";Initial Catalog=";
                str += ConfigurationManager.AppSettings["Database"].ToString();
                str += ";User ID=geotabuser;Pwd=vircom43;Connect Timeout=200;";
            }

            SqlConnection CN = new SqlConnection(str);
            SqlCommand sqlCmd = new SqlCommand(sql, CN);
            CN.Open();
            sqlCmd.ExecuteNonQuery();
            CN.Close();
            CN.Dispose();
        }

        public DataTable GetSQLEdition()
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("ProductVersion");
            dt.Columns.Add("Edition");
            dt.Rows.Add("xxx", "xxx");

            String sql = "SELECT SERVERPROPERTY('ProductVersion') AS ProductVersion, SERVERPROPERTY('Edition') AS Edition";
            DataSet ds = GetDataSet(sql, "ConnStr");
            if (ds.Tables.Count > 0)
                dt = ds.Tables[0];

            return dt;
        }

        public DataSet GetDataSet(String sql, String sConnStr)
        {
            return GetDataSet(sql, sConnStr, -1);
        }
        public DataSet GetDataSet(String sql, String sConnStr, int timeout)
        {
            SqlConnection CN = new SqlConnection(GlobalVariables.GetsConnStr(sConnStr));
            SqlCommand sqlCmd = new SqlCommand(sql, CN);
            if (timeout > 0)
                sqlCmd.CommandTimeout = timeout;
            SqlDataAdapter sqldata = new SqlDataAdapter();
            DataSet locdataset = new DataSet();
            try
            {
                CN.Open();
                sqldata.SelectCommand = sqlCmd;
                sqldata.Fill(locdataset);
            }
            catch { }
            finally
            {
                CN.Close();
                CN.Dispose();
            }
            return locdataset;
        }
        public Boolean ChkDBAvailability(String myString)
        {
            Boolean bAvailable = false;

            SqlConnection CN = new SqlConnection(myString);
            try
            {
                CN.Open();
                bAvailable = true;
            }
            catch
            {
                bAvailable = false;
            }
            finally
            {
                if (CN != null)
                    CN.Close();
            }

            CN.Dispose();
            return bAvailable;
        }

        DataSet GetDataSetFromCmd(String sql, String sConnStr, int timeout)
        {
            SqlConnection CN = new SqlConnection(GlobalVariables.GetsConnStr(sConnStr));
            SqlCommand sqlCmd = new SqlCommand(sql, CN);
            if (timeout > 0)
                sqlCmd.CommandTimeout = timeout;
            SqlDataAdapter sqldata = new SqlDataAdapter(sqlCmd);
            DataSet locdataset = new DataSet();
            try
            {
                CN.Open();
                //sqldata.SelectCommand = sqlCmd;
                sqldata.Fill(locdataset);
            }
            catch { }
            finally
            {
                CN.Close();
                CN.Dispose();
            }
            return locdataset;
        }

        public void TransformEnforaRawData()
        {
            String bytePath = GlobalVariables.bytePath();
            String path = bytePath + "ALL.txt";
            File.Delete(path);
            DataSet ds = new Runme().GetDataSet("select * from RawData where status = 0 order by iID", "ConnStr");
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                //String sql = "update EnforaRawData set data = '";
                //sql += Encoding.ASCII.GetString((Byte[])ds.Tables[0].Rows[0]["RawData"]);
                //sql += "'";
                //sql += " where iID = " + dr["iID"];
                //db.Execute(sql);

                DBHelper newDB = new DBHelper();
                SqlCommand sqlCmd = new SqlCommand();
                sqlCmd.Connection = newDB.GetDbConnection();
                sqlCmd.CommandText = "update RawData set data = @bData where iID = @iID";
                sqlCmd.Parameters.Add(new SqlParameter("@bData", (object)(dr["RawData"])));
                sqlCmd.Parameters.Add(new SqlParameter("@iID", (object)dr["iID"]));
                sqlCmd.ExecuteNonQuery();
                sqlCmd.Connection.Close();
                newDB.CloseDbConnection();

                File.AppendAllText(path
                    , dr["iID"] + Encoding.UTF8.GetString((Byte[])dr["RawData"]) + "\r\n");
            }
        }
        public DataSet ReadData(String sql)
        {
            DataSet ds = new Runme().GetDataSet(sql, "ConnStr");
            return ds;
        }

        public void tstStepp()
        {
            //insert RawData (RawData) values (convert([binary](1024),'Reza 123'))
            DataSet ds = ReadData("select top 1* from RawData where UnitID = '357023003036101' order by dtdatetime desc");
            String g = Encoding.ASCII.GetString((byte[])ds.Tables[0].Rows[0]["RawData"]);
        }

        public void InsertDriverID(String unitID, String striButtonID, DateTime dt, String strConn)
        {
            new NavLib.CoreData.NaveoCore().InsertDriverID(unitID, striButtonID, dt, strConn);
        }
        public DataSet GetDriverID(String unitID, String strConn)
        {
            return new NavLib.CoreData.NaveoCore().GetDriverID(unitID, strConn);
            String sql = "select * from GFI_GPS_DriversDriving where unitID = '" + unitID + "'";
            DataSet ds = GetDataSet(sql, strConn);
            return ds;
        }
        public void DeleteDriverID(String unitID, String strConn)
        {
            new NavLib.CoreData.NaveoCore().DeleteDriverID(unitID, strConn);
        }

        #region Fuel
        public void InsertNewFuelConv(String VehicleID, String sConnStr)
        {
            String sql = String.Empty;
            int ADC = 0;
            for (int i = 0; i <= 255; i++)
            {
                sql = "insert FuelConv ([UnitID], [Conv], [ADC], [DL]) values (";
                sql += "'" + VehicleID + "'";
                sql += ", " + i.ToString();
                ADC = 255 - i;
                sql += ", " + ADC.ToString();
                sql += ", 0";
                sql += " )";

                RunSQLCMD(sql, sConnStr, -1);
            }
        }
        public void InsertFuelData(int MessageType, DateTime dtDateTime
            , int DataID, int DataIDFrom, int DataIDTo
            , int FuelID, String unitID, int sensorNo
            , int RawADC, int UsedADC, int FuelLvl, int NotifCode)
        {
            String sql = "insert into FuelData ";
            sql += "(MessageType, dtDateTime, DataID, DataIDFrom, DataIDTo, FuelID, unitID, sensorNo, RawADC, UsedADC, FuelLvl, NotifCode)";
            sql += "values ";
            sql += "(@MessageType, @dtDateTime, @DataID, @DataIDFrom, @DataIDTo, @FuelID, @unitID, @sensorNo, @RawADC, @UsedADC, @FuelLvl, @NotifCode)";
            SqlConnection CN = new SqlConnection(GlobalVariables.GetsConnStr("ConnStr"));
            SqlCommand sqlCmd = new SqlCommand(sql, CN);
            CN.Open();
            try
            {
                sqlCmd.Parameters.Add(new SqlParameter("@MessageType", SqlDbType.Int));//(object)bytWorkingReduced));
                sqlCmd.Parameters.Add(new SqlParameter("@dtDateTime", SqlDbType.DateTime));//(object)strUnitModel));
                sqlCmd.Parameters.Add(new SqlParameter("@DataID", SqlDbType.Int));//(object)UnitID));
                sqlCmd.Parameters.Add(new SqlParameter("@DataIDFrom", SqlDbType.Int));//(object)UnitID));
                sqlCmd.Parameters.Add(new SqlParameter("@DataIDTo", SqlDbType.Int));//(object)UnitID));
                sqlCmd.Parameters.Add(new SqlParameter("@FuelID", SqlDbType.Int));//(object)UnitID));
                sqlCmd.Parameters.Add(new SqlParameter("@unitID", SqlDbType.VarChar));//(object)UnitID));
                sqlCmd.Parameters.Add(new SqlParameter("@sensorNo", SqlDbType.Int));//(object)UnitID));
                sqlCmd.Parameters.Add(new SqlParameter("@RawADC", SqlDbType.Int));//(object)UnitID));
                sqlCmd.Parameters.Add(new SqlParameter("@UsedADC", SqlDbType.Int));//(object)UnitID));
                sqlCmd.Parameters.Add(new SqlParameter("@FuelLvl", SqlDbType.Int));//(object)UnitID));
                sqlCmd.Parameters.Add(new SqlParameter("@NotifCode", SqlDbType.Int));//(object)UnitID));

                sqlCmd.Parameters["@MessageType"].Value = MessageType;
                sqlCmd.Parameters["@dtDateTime"].Value = dtDateTime;
                sqlCmd.Parameters["@DataID"].Value = DataID;
                sqlCmd.Parameters["@DataIDFrom"].Value = DataIDFrom;
                sqlCmd.Parameters["@DataIDTo"].Value = DataIDTo;
                sqlCmd.Parameters["@FuelID"].Value = FuelID;
                sqlCmd.Parameters["@unitID"].Value = unitID;
                sqlCmd.Parameters["@sensorNo"].Value = sensorNo;
                sqlCmd.Parameters["@RawADC"].Value = RawADC;
                sqlCmd.Parameters["@UsedADC"].Value = UsedADC;
                sqlCmd.Parameters["@FuelLvl"].Value = FuelLvl;
                sqlCmd.Parameters["@NotifCode"].Value = NotifCode;
                sqlCmd.ExecuteNonQuery();
            }
            //catch (Exception ex)
            //{
            //    Logger.WriteToErrorLog(ex);
            //}
            finally
            {
                CN.Close();
                CN.Dispose();
            }
        }

        public void InsertFuelInGeotabTable(DataSet dsFuel)
        {
            SqlConnection CNDest = new SqlConnection(GlobalVariables.GetsConnStr("Geotab"));
            CNDest.Open();
            SqlBulkCopy bulk = new SqlBulkCopy(CNDest);
            bulk.DestinationTableName = "NaveoFuelData";
            try
            {
                bulk.BulkCopyTimeout = 4000;
                bulk.WriteToServer(dsFuel.Tables[0]);
            }
            catch { }
            finally
            {
                CNDest.Close();
                CNDest.Dispose();
            }
        }

        public DataSet GetFuelDataFromGeotab(String myUnits, DateTime dtFrom, DateTime dtTo, Boolean ExcludeMessageDesc)
        {
            String sql = "select fm.Description, fd.* ";
            if (ExcludeMessageDesc)
                sql = "select fd.* ";
            sql += "\r\nfrom NaveoFuelData fd, NaveoFuelMessageTypes fm";
            sql += "\r\nwhere fd.messagetype = fm.messagetype";
            sql += "\r\n	and fm.IsNotif = 0";
            sql += "\r\n	--and fm.messagetype = 6	--Real";
            sql += "\r\n	--and fm.messagetype = 1	--SensorDataMessage";
            sql += "\r\n	and ( fm.messagetype = 5	--FillDataMessage";
            sql += "\r\n			or fm.messagetype = 6	--Real";
            sql += "\r\n		)";
            sql += "\r\nand fd.unitID in (" + myUnits + ")";
            sql += "\r\nand fd.dtDatetime between @dtFrom and @dtTo";
            sql += "\r\norder by fd.dtDatetime";

            SqlConnection CN = new SqlConnection(GlobalVariables.GetsConnStr("Geotab"));
            SqlCommand sqlCmd = new SqlCommand(sql, CN);
            SqlDataAdapter sqldata = new SqlDataAdapter();
            DataSet ds = new DataSet();
            CN.Open();
            try
            {
                sqlCmd.Parameters.Add(new SqlParameter("@dtFrom", SqlDbType.DateTime));
                sqlCmd.Parameters.Add(new SqlParameter("@dtTo", SqlDbType.DateTime));

                sqlCmd.Parameters["@dtFrom"].Value = dtFrom;
                sqlCmd.Parameters["@dtTo"].Value = dtTo;

                sqldata.SelectCommand = sqlCmd;
                sqldata.Fill(ds);
            }
            finally
            {
                CN.Close();
                CN.Dispose();
            }
            return ds;
        }
        public DataSet GetFuelDataRealFromGeotab(String myUnits, DateTime dtFrom, DateTime dtTo)
        {
            String sql = "select * ";
            sql += "\r\nfrom NaveoFuelData ";
            sql += "\r\nwhere messagetype = 6	--Real";
            sql += "\r\nand unitID in (" + myUnits + ")";
            sql += "\r\nand dtDatetime between @dtFrom and @dtTo";
            sql += "\r\norder by dtDatetime";

            SqlConnection CN = new SqlConnection(GlobalVariables.GetsConnStr("Geotab"));
            SqlCommand sqlCmd = new SqlCommand(sql, CN);
            SqlDataAdapter sqldata = new SqlDataAdapter();
            DataSet ds = new DataSet();
            CN.Open();
            try
            {
                sqlCmd.Parameters.Add(new SqlParameter("@dtFrom", SqlDbType.DateTime));
                sqlCmd.Parameters.Add(new SqlParameter("@dtTo", SqlDbType.DateTime));

                sqlCmd.Parameters["@dtFrom"].Value = dtFrom;
                sqlCmd.Parameters["@dtTo"].Value = dtTo;

                sqldata.SelectCommand = sqlCmd;
                sqldata.Fill(ds);
            }
            finally
            {
                CN.Close();
                CN.Dispose();
            }
            return ds;
        }
        public DataSet GetFuelDataFillFromGeotab(String myUnits, DateTime dtFrom, DateTime dtTo)
        {
            String sql = "select * ";
            sql += "\r\nfrom NaveoFuelData ";
            sql += "\r\nwhere messagetype = 5	--Fill";
            sql += "\r\nand unitID in (" + myUnits + ")";
            sql += "\r\nand dtDatetime between @dtFrom and @dtTo";
            sql += "\r\norder by dtDatetime";

            SqlConnection CN = new SqlConnection(GlobalVariables.GetsConnStr("Geotab"));
            SqlCommand sqlCmd = new SqlCommand(sql, CN);
            SqlDataAdapter sqldata = new SqlDataAdapter();
            DataSet ds = new DataSet();
            CN.Open();
            try
            {
                sqlCmd.Parameters.Add(new SqlParameter("@dtFrom", SqlDbType.DateTime));
                sqlCmd.Parameters.Add(new SqlParameter("@dtTo", SqlDbType.DateTime));

                sqlCmd.Parameters["@dtFrom"].Value = dtFrom;
                sqlCmd.Parameters["@dtTo"].Value = dtTo;

                sqldata.SelectCommand = sqlCmd;
                sqldata.Fill(ds);
            }
            finally
            {
                CN.Close();
                CN.Dispose();
            }
            return ds;
        }
        #endregion

        #region WebService
        void InsertReceivedQry(String myUnits, String ServerName, String DBName, String Retrieval, String qFrom, String ip)
        {
            String sql = "select * from AudQryReceived where ServerName = @ServerName and DBName = @DBName and Retrieval = @Retrieval and qFrom = @qFrom";
            SqlConnection CN = new SqlConnection(GlobalVariables.GetsConnStr("ConnStr"));
            SqlCommand sqlCmd = new SqlCommand(sql, CN);
            SqlDataAdapter sqldata = new SqlDataAdapter(sqlCmd);
            try
            {
                CN.Open();
                sqlCmd.Parameters.Add(new SqlParameter("@myUnits", SqlDbType.VarChar));
                sqlCmd.Parameters.Add(new SqlParameter("@ServerName", SqlDbType.VarChar));
                sqlCmd.Parameters.Add(new SqlParameter("@DBName", SqlDbType.VarChar));
                sqlCmd.Parameters.Add(new SqlParameter("@Retrieval", SqlDbType.VarChar));
                sqlCmd.Parameters.Add(new SqlParameter("@qFrom", SqlDbType.VarChar));
                sqlCmd.Parameters.Add(new SqlParameter("@ip", SqlDbType.VarChar));
                sqlCmd.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int));
                sqlCmd.Parameters.Add(new SqlParameter("@iCount", SqlDbType.Int));

                sqlCmd.Parameters["@myUnits"].Value = myUnits;
                sqlCmd.Parameters["@ServerName"].Value = ServerName;
                sqlCmd.Parameters["@DBName"].Value = DBName;
                sqlCmd.Parameters["@Retrieval"].Value = Retrieval;
                sqlCmd.Parameters["@qFrom"].Value = qFrom;
                sqlCmd.Parameters["@ip"].Value = ip;
                sqlCmd.Parameters["@iID"].Value = 0;
                sqlCmd.Parameters["@iCount"].Value = 0;
                sqlCmd.CommandText = sql;

                sqlCmd.CommandText = sql;
                DataSet ds = new DataSet();
                sqldata.Fill(ds);

                //insert
                if (ds.Tables[0].Rows.Count == 0)
                {
                    sql = "insert into AudQryReceived (ServerName, DBName, Retrieval, qFrom) values (@ServerName, @DBName, @Retrieval, @qFrom)";
                    sqlCmd.CommandText = sql;
                    sqlCmd.ExecuteNonQuery();

                    sql = "select * from AudQryReceived where ServerName = @ServerName and DBName = @DBName and Retrieval = @Retrieval and qFrom = @qFrom";
                    sqlCmd.CommandText = sql;
                    ds = new DataSet();
                    sqldata.Fill(ds);
                    int iID = Convert.ToInt16(ds.Tables[0].Rows[0]["iID"]);
                    sqlCmd.Parameters["@iID"].Value = iID;

                    sql = "insert into AudQryRcvCount (iID, ip, iCount, myUnits) values (@iID, @ip, 1, @myUnits)";
                    sqlCmd.CommandText = sql;
                    sqlCmd.ExecuteNonQuery();
                }
                else //Update
                {
                    int iID = Convert.ToInt16(ds.Tables[0].Rows[0]["iID"]);
                    sqlCmd.Parameters["@iID"].Value = iID;

                    sql = "select * from AudQryRcvCount where iID = @iID and ip = @ip";
                    sqlCmd.CommandText = sql;
                    ds = new DataSet();
                    sqldata.Fill(ds);

                    // insert if new ip
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        sql = "insert into AudQryRcvCount (iID, ip, iCount, myUnits) values (@iID, @ip, 0, @myUnits)";
                        sqlCmd.CommandText = sql;
                        sqlCmd.ExecuteNonQuery();
                    }
                    else //Update iCount if existing ip
                    {
                        int iCount = Convert.ToInt16(ds.Tables[0].Rows[0]["iCount"]) + 1;
                        sql = "update AudQryRcvCount set iCount = @iCount where iID = @iID and ip = @ip";
                        sqlCmd.Parameters["@iCount"].Value = iCount;
                        sqlCmd.CommandText = sql;
                        sqlCmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteToErrorLog(ex, sql);
            }
            finally
            {
                CN.Close();
                CN.Dispose();
            }
        }
        void InsertReceivedQryOld(String myUnits, String ServerName, String DBName, String Misc)
        {
            String sql = "insert into AudQryReceived (myUnits, ServerName, DBName, Misc) values (@myUnits, @ServerName, @DBName, @Misc)";
            SqlConnection CN = new SqlConnection(GlobalVariables.GetsConnStr("ConnStr"));
            SqlCommand sqlCmd = new SqlCommand(sql, CN);
            CN.Open();
            try
            {
                sqlCmd.Parameters.Add(new SqlParameter("@myUnits", SqlDbType.VarChar));
                sqlCmd.Parameters.Add(new SqlParameter("@ServerName", SqlDbType.VarChar));
                sqlCmd.Parameters.Add(new SqlParameter("@DBName", SqlDbType.VarChar));
                sqlCmd.Parameters.Add(new SqlParameter("@Misc", SqlDbType.VarChar));

                sqlCmd.Parameters["@myUnits"].Value = myUnits;
                sqlCmd.Parameters["@ServerName"].Value = ServerName;
                sqlCmd.Parameters["@DBName"].Value = DBName;
                sqlCmd.Parameters["@Misc"].Value = Misc;
                sqlCmd.CommandText = sql;
                sqlCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Logger.WriteToErrorLog(ex, sql);
            }
            finally
            {
                CN.Close();
                CN.Dispose();
            }
        }

        public DataSet PollUpdates(String myUnits, String ServerName, String DBName, int ClientRetrieval, String ClientIP)
        {
            try
            {
                // InsertReceivedQry(myUnits, ServerName, DBName, ClientRetrieval.ToString(), "PollUpdates", ClientIP);
            }
            catch { }

            String statusInProgress = "1";
            String statusNew = "0";
            if (ClientRetrieval == 2)
            {
                statusInProgress = "3";
                statusNew = "2";
            }

            String sDevices = @"
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 

CREATE TABLE #Devices
(
	[RowNumber] [int] IDENTITY(1,1) PRIMARY KEY CLUSTERED NOT NULL,
	[sUnitID] [varchar](20),
)
";
            String[] sDev = myUnits.Split(',');
            foreach (String s in sDev)
                sDevices += "insert #Devices (sUnitID) values (" + s + ")\r\n";

            int iTimeOut = 600;
            String sql = sDevices + "\r\n";
            sql += @"select r.* from #Devices d 
                            inner join RawData r on r.unitID = d.sUnitID where status = " + statusInProgress;
            DataSet ds = GetDataSetFromCmd(sql, "ConnStr", iTimeOut);
            if (ds.Tables[0].Rows.Count == 0)
            {
                ds = new DataSet();
                sql = sDevices + @"
;with 
cte as 
(
    select *, 
        row_number() over(partition by UnitID order by dtDateTime asc) as RowNum
	from #Devices d
	    inner join RawData r on r.unitID = d.sUnitID
    WHERE status = " + statusNew + @"
)
update RawData
    set status = " + statusInProgress + @"
	    , ServerName = @ServerName
	    , DBName = @DBName
from RawData r
	inner join cte c on c.iID = r.iID
    where RowNum <= 50  --50 rows per UnitID

select r.* from RawData r
	inner join #Devices d on d.sUnitID = r.unitID
	where status = " + statusInProgress + @"

drop table #Devices";
                SqlConnection CN = new SqlConnection(GlobalVariables.GetsConnStr("ConnStr"));
                SqlCommand sqlCmd = new SqlCommand(sql, CN);
                try
                {
                    CN.Open();
                    sqlCmd.Parameters.Add(new SqlParameter("@ServerName", (object)ServerName));
                    sqlCmd.Parameters.Add(new SqlParameter("@DBName", (object)DBName));
                    sqlCmd.CommandTimeout = iTimeOut;
                    SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                    da.Fill(ds);
                }
                finally
                {
                    CN.Close();
                    CN.Dispose();
                }
            }
            return ds;
        }
        public Boolean ConfirmTheUpdates(String myUnits, int ClientRetrieval)
        {
            String statusInProgress = "1";
            String statusComplete = "2";
            if (ClientRetrieval == 2)
            {
                statusInProgress = "3";
                statusComplete = "4";
            }

            //String[] sDev = myUnits.Split(',');
            //foreach (String s in sDev)
            //    sql += "update RawData WITH (READPAST) set Status = " + statusComplete + " where status = " + statusInProgress + " and unitID = " + s + " \r\n";


            String sql = @"
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 

CREATE TABLE #Devices
(
	[RowNumber] [int] IDENTITY(1,1) PRIMARY KEY CLUSTERED NOT NULL,
	[sUnitID] [varchar](20),
)
";

            String[] sDev = myUnits.Split(',');
            foreach (String s in sDev)
                sql += "insert #Devices (sUnitID) values (" + s + ")\r\n";
            sql += @"
;with 
cte as 
(
    select *
	from #Devices d
	    inner join RawData r on r.unitID = d.sUnitID
    WHERE status = " + statusInProgress + @"
)
update RawData 
    set status = " + statusComplete + @"
from RawData r
	inner join cte c on c.iID = r.iID

drop table #Devices";

            Boolean bOK = true;
            try
            {
                RunSQLCMD(sql, "ConnStr", -1);
            }
            catch
            {
                bOK = false;
            }
            return bOK;
        }

        public DataSet PollFuelUpdates(String myUnits)
        {
            try
            {
                InsertReceivedQry(myUnits, String.Empty, String.Empty, String.Empty, "PollFuelUpdates", String.Empty);
            }
            catch { }

            String cnn = "ConnStr";
            DataSet ds = GetDataSet(SqlCmds.GetFuelData(myUnits, true), cnn);
            if (ds.Tables[0].Rows.Count == 0)
            {
                RunSQLCMD(SqlCmds.UpdateFuelData(myUnits, true), cnn, -1);
                ds = GetDataSet(SqlCmds.GetFuelData(myUnits, true), cnn);
            }
            return ds;
        }
        public Boolean ConfirmTheFuelUpdates(String myUnits)
        {
            String cnn = "ConnStr";
            Boolean bOK = true;
            try
            {
                RunSQLCMD(SqlCmds.UpdateFuelData(myUnits, false), cnn, -1);
            }
            catch
            {
                bOK = false;
            }
            return bOK;
        }

        public DataSet PollSecuMonitorUpdates(String myUnits, String ServerName, String DBName, int ClientRetrieval, String ClientIP)
        {
            String statusInProgress = "1";
            String statusNew = "0";
            if (ClientRetrieval == 2)
            {
                statusInProgress = "3";
                statusNew = "2";
            }

            String sDevices = @"
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 

CREATE TABLE #Devices
(
	[RowNumber] [int] IDENTITY(1,1) PRIMARY KEY CLUSTERED NOT NULL,
	[sUnitID] [varchar](20),
)
";
            String[] sDev = myUnits.Split(',');
            foreach (String s in sDev)
                sDevices += "insert #Devices (sUnitID) values (" + s + ")\r\n";

            int iTimeOut = 600;
            String sql = sDevices + "\r\n";
            sql += @"select r.* from #Devices d 
                            inner join RawData r on r.unitID = d.sUnitID where SMStatus = " + statusInProgress;
            DataSet ds = GetDataSetFromCmd(sql, "ConnStr", iTimeOut);
            if (ds.Tables[0].Rows.Count == 0)
            {
                ds = new DataSet();
                sql = sDevices + @"
;with 
cte as 
(
    select *, 
        row_number() over(partition by UnitID order by dtDateTime asc) as RowNum
	from #Devices d
	    inner join RawData r on r.unitID = d.sUnitID
    WHERE SMStatus = " + statusNew + @"
)
update RawData 
set SMStatus = " + statusInProgress + @"
	, ServerName = @ServerName
	, DBName = @DBName
from RawData r
	inner join cte c on c.iID = r.iID
    where RowNum <= 50  --50 rows per UnitID

select r.* from RawData r
	inner join #Devices d on d.sUnitID = r.unitID
	where SMStatus = " + statusInProgress + @"

drop table #Devices";
                SqlConnection CN = new SqlConnection(GlobalVariables.GetsConnStr("ConnStr"));
                SqlCommand sqlCmd = new SqlCommand(sql, CN);
                CN.Open();
                try
                {
                    sqlCmd.Parameters.Add(new SqlParameter("@ServerName", (object)ServerName));
                    sqlCmd.Parameters.Add(new SqlParameter("@DBName", (object)DBName));
                    sqlCmd.CommandTimeout = iTimeOut;
                    SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                    da.Fill(ds);
                }
                finally
                {
                    CN.Close();
                    CN.Dispose();
                }
            }
            return ds;
        }
        public Boolean ConfirmTheSecuMonitorUpdates(String myUnits, int ClientRetrieval)
        {
            String statusInProgress = "1";
            String statusComplete = "2";
            if (ClientRetrieval == 2)
            {
                statusInProgress = "3";
                statusComplete = "4";
            }

            //            String sDevices = @"CREATE TABLE #Devices
            //(
            //	[RowNumber] [int] IDENTITY(1,1) PRIMARY KEY CLUSTERED NOT NULL,
            //	[sUnitID] [varchar](20),
            //)
            //";
            //            String[] sDev = myUnits.Split(',');
            //            foreach (String s in sDev)
            //                sDevices += "insert #Devices (sUnitID) values (" + s + ")\r\n";

            //            String sql = sDevices;
            //            //sql += "\r\nupdate RawData set SMStatus = " + statusComplete;
            //            //sql += "\r\n from RawData r";
            //            //sql += "\r\n inner join #Devices d on r.unitID = d.sUnitID";
            //            //sql += "\r\n where r.SMStatus = " + statusInProgress;
            //            //sql += "\r\n drop table #Devices";

            //            sql += @"
            //;with cte as
            //(
            //	select * from RawData r
            //		inner join #Devices d on r.unitID = d.sUnitID
            //	where r.SMStatus = " + statusInProgress + @"
            //)
            //update RawData 
            //	set SMStatus = " + statusComplete + @"
            //from RawData r
            //	inner join cte c on r.iID = c.iID
            //";

            String sql = String.Empty;
            String[] sDev = myUnits.Split(',');
            foreach (String s in sDev)
                sql += "update RawData set SMStatus = " + statusComplete + " where SMStatus = " + statusInProgress + " and unitID = " + s + " \r\n";

            Boolean bOK = true;
            try
            {
                RunSQLCMD(sql, "ConnStr", -1);
            }
            catch
            {
                bOK = false;
            }
            return bOK;
        }


        public Boolean RawBatchInsert(DataTable dt)
        {
            Boolean b = false;
            int i = 0;
            foreach (DataRow dr in dt.Rows)
            {
                if (i == 0)
                    b = true & InsertRawData((Byte[])dr["RawData"], dr["Model"].ToString()).bInsertOK;
                else
                    b = b & InsertRawData((Byte[])dr["RawData"], dr["Model"].ToString()).bInsertOK;
                i++;
            }
            return b;
        }
        #endregion

        #region Tramigo
        public DataSet GetAllTramigo()
        {
            AccessDBHelper adbHelper = new AccessDBHelper();
            String sql = "select * from Devices ";
            DataSet ds = adbHelper.GetDataSet(sql);
            adbHelper.ReleaseConnectionCache();
            return ds;
        }
        public DataSet GetTramigo(String VehicleIdentificationNumber)
        {
            AccessDBHelper adbHelper = new AccessDBHelper();
            String sql = "select top 1 * from Messages ";
            sql += "\r\n where coordinates <> '' ";
            sql += "\r\n and Number = '" + VehicleIdentificationNumber + "'";
            sql += "\r\n order by DateCreated desc";
            DataSet ds = adbHelper.GetDataSet(sql);
            adbHelper.ReleaseConnectionCache();
            return ds;
        }
        #endregion

        #region Old Codes
        public void InsertRawDataOLDToDel(Byte[] bytWorking, String strUnitModel)
        {
            //DBHelper newDB = new DBHelper();
            //SqlConnection CN = new SqlConnection(ConfigurationManager.AppSettings["ConnStr"].ToString());
            //String sql = "insert into EnforaRawData (RawData) values (@bData)";
            //SqlCommand sqlCmd = new SqlCommand(sql, CN);
            //sqlCmd.Parameters.Add(new SqlParameter ("@bData", (object)bytWorking));
            //CN.Open();
            //sqlCmd.ExecuteNonQuery();
            //CN.Close();

            String UnitID = String.Empty;
            try
            {
                switch (strUnitModel)
                {
                    case "AT100":
                        //clsAT100Parser objPacketParser = new clsAT100Parser(bytWorking);
                        //UnitID = objPacketParser.objHeader.GetMSN().ToString();
                        UnitID = new DecodeAT100ProtocolC(bytWorking).GetUnitID();
                        break;

                    case "STEPP":
                        UnitID = new DecodeStepp(bytWorking).GetUnitIMEI;
                        break;
                }
            }
            catch { }
            if (UnitID.Trim().Length > 0)
            {
                DBHelper newDB = new DBHelper();
                SqlCommand sqlCmd = new SqlCommand();
                try
                {
                    try
                    {
                        int iLen = bytWorking.Length;
                        Byte[] b = new Byte[1];
                        for (int i = bytWorking.Length - 1; i >= 0; --i)
                            if (bytWorking[i] != b[0])
                            {
                                iLen = i;
                                break;
                            }
                        Byte[] bytWorkingReduced = new Byte[iLen + 1];
                        Array.Copy(bytWorking, 0, bytWorkingReduced, 0, iLen + 1);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteToErrorLog(ex);
                    }
                    sqlCmd.Connection = newDB.GetDbConnection();
                    sqlCmd.CommandText = "insert into RawData (RawData, Model, unitID) values (@bData, @Model, @unitID)";
                    sqlCmd.Parameters.Add(new SqlParameter("@bData", (object)bytWorking));
                    sqlCmd.Parameters.Add(new SqlParameter("@Model", (object)strUnitModel));
                    sqlCmd.Parameters.Add(new SqlParameter("@unitID", (object)UnitID));
                    sqlCmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    Logger.WriteToErrorLog(ex);
                }
                finally
                {
                    sqlCmd.Connection.Close();
                    sqlCmd.Connection.Dispose();
                    newDB.CloseDbConnection();
                }
            }
        }
        #endregion
    }
}