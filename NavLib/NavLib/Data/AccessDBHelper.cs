using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using System.Configuration;
using System.Windows.Forms;
using System.Data.OleDb;
using System.IO;
using System.Threading;

namespace NavLib.Data
{
    public class AccessDBHelper
    {
        public AccessDBHelper()
        { }
        private readonly String AccessDatabase = "C:\\Documents and Settings\\All Users\\Application Data\\Tramigo\\M1 Fleet\\2.1.40.82\\M1Fleet.mdb";

        internal static string GetConnectionString(string filePath, bool isLegacy)
        {
            string str = "Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Jet OLEDB:Database Password=vircom42";
            if (!isLegacy)
            {
                str = str + ";OLE DB Services=-7";
            }
            return (str + ";Data Source=" + filePath);
        }
        public string ConnectionString
        {
            get
            {
                return GetConnectionString(AccessDatabase, false);
            }
        }
        internal virtual void TryOpenConnection(IDbConnection connection)
        {
            try
            {
                connection.Open();
            }
            catch (NullReferenceException)
            {
                throw;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public DataSet GetDataSet(String sSqlStmt)
        {
            return GetDataSet(sSqlStmt, String.Empty);
        }
        public DataSet GetDataSet(String sSqlStmt, String srcTable)
        {
            return GetDataSet(sSqlStmt, srcTable, null, null);
        }
        public DataSet GetDataSet(String sSqlStmt, String srcTable, OleDbConnection pSqlConn, OleDbTransaction pSqlTrans)
        {
            try
            {
                OleDbConnection connection = new OleDbConnection(this.ConnectionString);
                TryOpenConnection(connection);

                if (connection.State == ConnectionState.Closed)
                    TryOpenConnection(connection);

                OleDbCommand sqlcmd = new OleDbCommand(sSqlStmt);
                OleDbDataAdapter sqldata = new OleDbDataAdapter();
                DataSet locdataset = new DataSet();

                sqlcmd.Connection = connection;
                if (pSqlTrans != null)
                    sqlcmd.Transaction = pSqlTrans;

                sqldata.SelectCommand = sqlcmd;
                if (srcTable == null || srcTable == String.Empty)
                    sqldata.Fill(locdataset);
                else
                    sqldata.Fill(locdataset, srcTable);
                return locdataset;
            }
            catch (OleDbException ex)
            {
                throw (ex);
            }
            finally
            {
                if (pSqlTrans == null)
                    ReleaseConnectionCache();
            }
        }

        internal void ReleaseConnectionCache()
        {
            ReleaseConnectionCache(this.AccessDatabase);
        }
        internal static void ReleaseConnectionCacheAll()
        {
            try
            {
                OleDbConnection.ReleaseObjectPool();
            }
            catch (NotImplementedException)
            {
            }
            GC.Collect();
        }
        private static void ReleaseConnectionCache(string path)
        {
            ReleaseConnectionCacheAll();
            string str = path.Replace(".mdb", ".ldb");
            DateTime time = DateTime.UtcNow.AddSeconds(1.0);
            while (File.Exists(str) && (DateTime.UtcNow < time))
            {
                Thread.Sleep(0);
            }
        }
    }
}