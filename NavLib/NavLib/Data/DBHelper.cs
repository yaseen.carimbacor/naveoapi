using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using System.Configuration;
using System.Windows.Forms;
using NavLib.Globals;
using DBConn.Common;
using System.IO;

namespace NavLib.Data
{
    public class DBHelper
    {
        public DBHelper()
        { }

        private SqlConnection _SqlConnection;

        public void InitConnection(String sConnStr)
        {
            try
            {
                String str = GlobalVariables.GetsConnStr(sConnStr);

                if (_SqlConnection == null)
                    _SqlConnection = new SqlConnection();

                if (_SqlConnection.State == ConnectionState.Closed)
                    _SqlConnection.ConnectionString = str;
            }
            catch (SqlException ex)
            {
                throw (ex);
            }
        }

        public void OpenDbConnection()
        {
            OpenDbConnection("ConnStr");
        }
        public void OpenDbConnection(String sConnStr)
        {
            InitConnection(sConnStr);
            if (_SqlConnection.State == ConnectionState.Closed)
                _SqlConnection.Open();

            return;
        }

        public void CloseDbConnection()
        {
            if (_SqlConnection.State == ConnectionState.Open)
                _SqlConnection.Close();

            if (_SqlConnection != null)
                _SqlConnection.Dispose();

            return;
        }

        public SqlConnection GetDbConnection()
        {
            OpenDbConnection();
            return _SqlConnection;
        }
        public SqlConnection GetDbConnection(String sConnStr)
        {
            OpenDbConnection(sConnStr);
            return _SqlConnection;
        }

        public DataSet GetDataSet(String sSqlStmt)
        {
            return GetDataSet(sSqlStmt, String.Empty, "ConnStr");
        }
        public DataSet GetDataSet(String sSqlStmt, String srcTable, String sConnStr)
        {
            return GetDataSet(sSqlStmt, srcTable, null, null, sConnStr);
        }
        public DataSet GetDataSet(String sSqlStmt, String srcTable, SqlConnection pSqlConn, SqlTransaction pSqlTrans, String sConnStr)
        {
            try
            {
                if (pSqlConn != null)
                    _SqlConnection = pSqlConn;
                else
                    OpenDbConnection(sConnStr);

                if (_SqlConnection.State == ConnectionState.Closed)
                    OpenDbConnection(sConnStr); //_SqlConnection.Open();

                SqlCommand sqlcmd = new SqlCommand(sSqlStmt);
                SqlDataAdapter sqldata = new SqlDataAdapter();
                DataSet locdataset = new DataSet();

                sqlcmd.Connection = _SqlConnection;
                if (pSqlTrans != null)
                    sqlcmd.Transaction = pSqlTrans;

                sqldata.SelectCommand = sqlcmd;
                if (srcTable == null || srcTable == String.Empty)
                    sqldata.Fill(locdataset);
                else
                    sqldata.Fill(locdataset, srcTable);
                return locdataset;
            }
            catch (SqlException ex)
            {
                throw (ex);
            }
            finally
            {
                if (pSqlTrans == null)
                    CloseDbConnection();
            }
        }

        public String GetMessageToSend(String sUnitID, String sModel)
        {
            String sConnStr = "DB1";
            String sResult = String.Empty;
            String sql = @"
with cte as (
   select top (1) *
    from MessageData 
   where unitID = '" + sUnitID + "' and Model = '" + sModel + @"'
   order by iID
)
update cte set status = 1

select 'dtData' as TableName
select * from MessageData 
   where unitID = '" + sUnitID + @"' and status = 1 and Model = '" + sModel + @"'

delete from MessageData 
   where unitID = '" + sUnitID + @"' and status = 1 and Model = '" + sModel + @"'
";

            NaveoOneLib.DBCon.Connection c = new NaveoOneLib.DBCon.Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables.Contains("dtData"))
                if (ds.Tables["dtData"].Rows.Count == 1)
                    sResult = ds.Tables["dtData"].Rows[0]["UnitData"].ToString();

            return sResult;
        }
        public Boolean SendMessageToSend(String sUnitID, String sUnitModel, String sMessage)
        {
            String sConnStr = "DB1";
            String sql = "insert MessageData (UnitData, Model, UnitID) values ('" + sMessage + "', '" + sUnitModel + "', '" + sUnitID + "')";

            NaveoOneLib.DBCon.Connection c = new NaveoOneLib.DBCon.Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);

            DataSet dsResult = c.GetDataDS(dtSql, sConnStr);
            Boolean bResult = false;
            if (dsResult.Tables.Count == 0)
                bResult = true;

            return bResult;
        }

        public int Execute(String sSqlStmt)
        {
            return Execute(sSqlStmt, null, null, true, null, String.Empty);
        }
        public int Execute(String sSqlStmt, SqlConnection pSqlConn)
        {
            return Execute(sSqlStmt, pSqlConn, null, true, null, String.Empty);
        }
        public int Execute(String sSqlStmt, SqlConnection pSqlConn, SqlTransaction pSqlTrans)
        {
            return Execute(sSqlStmt, pSqlConn, pSqlTrans, false, null, String.Empty);
        }
        public int Execute(String sSqlStmt, SqlConnection pSqlConn, SqlTransaction pSqlTrans, Boolean pboolAutoCommit)
        {
            return Execute(sSqlStmt, pSqlConn, pSqlTrans, pboolAutoCommit, null, String.Empty);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sSqlStmt"></param>
        /// <param name="pSqlConn"></param>
        /// <param name="pSqlTrans"></param>
        /// <param name="pboolAutoCommit"></param>
        /// <param name="sqlcmd">Uses SqlStmt, SqlConnection or SqlTrans passed as params unless already specified in the SqlCommand object</param>
        /// <returns></returns>
        public int Execute(String sSqlStmt, SqlConnection pSqlConn, SqlTransaction pSqlTrans, Boolean pboolAutoCommit, SqlCommand sqlcmd, String sConnStr)
        {
            int lnRecordAffected = -1;
            Boolean boolTransaction = false;

            if (sConnStr == String.Empty)
                sConnStr = "ConnStr";

            if (sqlcmd == null)
                sqlcmd = new SqlCommand();

            try
            {
                if (pSqlConn != null)
                    _SqlConnection = pSqlConn;
                else
                    OpenDbConnection(sConnStr);

                if (_SqlConnection.State == ConnectionState.Closed)
                    OpenDbConnection(sConnStr); // _SqlConnection.Open();

                if (sqlcmd.Connection == null || (sqlcmd.Connection.State == ConnectionState.Closed))
                {
                    sqlcmd.Connection = _SqlConnection;
                }

                if (sqlcmd.Transaction == null)
                {
                    if (pSqlTrans != null)
                    {
                        sqlcmd.Transaction = pSqlTrans;
                        boolTransaction = true;
                    }
                }

                if (string.IsNullOrEmpty(sqlcmd.CommandText))
                {
                    sqlcmd.CommandText = sSqlStmt;
                }

                lnRecordAffected = sqlcmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw (ex);
            }
            finally
            {
                if (lnRecordAffected != -1 && boolTransaction == true && pboolAutoCommit == true)
                    sqlcmd.Transaction.Commit();
                else if (lnRecordAffected == -1 && boolTransaction == true && pboolAutoCommit == true)
                    sqlcmd.Transaction.Rollback();

                //Reza Close the connection
                CloseDbConnection();
            }
            return lnRecordAffected;
        }


        public int SaveSettings(String sSqlStmt, String pUserId, String pObjectId, String pObjectType, Byte[] pbuffer, SqlConnection pSqlConn)
        {
            int lnRecordAffected = -1;
            SqlCommand sqlcmd = new SqlCommand();

            try
            {
                if (pSqlConn != null)
                    _SqlConnection = pSqlConn;
                else
                    OpenDbConnection();


                if (_SqlConnection.State == ConnectionState.Closed)
                    OpenDbConnection(); //_SqlConnection.Open();

                sqlcmd.Connection = _SqlConnection;
                sqlcmd.CommandText = sSqlStmt;

                SqlParameter param0 = new SqlParameter("@UserId", SqlDbType.VarChar, 50);
                param0.Value = pUserId;
                sqlcmd.Parameters.Add(param0);

                SqlParameter param1 = new SqlParameter("@ObjectId", SqlDbType.VarChar, 50);
                param1.Value = pObjectId;
                sqlcmd.Parameters.Add(param1);

                SqlParameter param2 = new SqlParameter("@ObjectType", SqlDbType.VarChar, 50);
                param2.Value = pObjectType;
                sqlcmd.Parameters.Add(param2);

                SqlParameter param3 = new SqlParameter("@ObjectSetting", SqlDbType.Image);
                param3.Value = pbuffer;
                sqlcmd.Parameters.Add(param3);

                lnRecordAffected = sqlcmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw (ex);
            }
            return lnRecordAffected;
        }

        public int ExecuteScalar(String sSqlStmt, SqlConnection pSqlConn, SqlTransaction pSqlTrans, String sConnStr)
        {
            Int32 lnRecordAffected = -1;
            SqlCommand sqlcmd = new SqlCommand();

            try
            {
                if (pSqlConn != null)
                    _SqlConnection = pSqlConn;
                else
                    OpenDbConnection(sConnStr);


                if (_SqlConnection.State == ConnectionState.Closed)
                    OpenDbConnection(sConnStr); //_SqlConnection.Open();

                sqlcmd.Connection = _SqlConnection;

                if (pSqlTrans != null)
                    sqlcmd.Transaction = pSqlTrans;

                sqlcmd.CommandText = sSqlStmt;
                lnRecordAffected = (Int32)sqlcmd.ExecuteScalar();
            }
            catch (SqlException ex)
            {
                throw (ex);
            }
            return (int)lnRecordAffected;
        }

        public int ExecuteUpdateInsert(String sSqlUpdate, String sSqlInsert, SqlConnection pSqlConn, SqlTransaction pSqlTrans, Boolean pboolAutoCommit)
        {
            int lnRecordAffected = -1;

            try
            {
                lnRecordAffected = Execute(sSqlUpdate, pSqlConn, pSqlTrans, pboolAutoCommit);
                if (lnRecordAffected == 0)
                    lnRecordAffected = Execute(sSqlInsert, pSqlConn, pSqlTrans, pboolAutoCommit);
            }
            catch (SqlException ex)
            {
                throw (ex);
            }
            return lnRecordAffected;
        }

        public void BulkCopyWriteToServer(DataTable dtSource, String strDestinationTable)
        {
            SqlBulkCopy sbc = new SqlBulkCopy(GetDbConnection());
            sbc.DestinationTableName = strDestinationTable;
            sbc.WriteToServer(dtSource);
            sbc.Close();
        }

        public int GetNumberOfRecords(String pTable, String pColumn, String pWhere, String sConnStr)
        {
            int count = -1;
            String strSql;

            try
            {
                // Open the connection
                OpenDbConnection(sConnStr);

                strSql = "select count(" + pColumn + ") from " + pTable;

                if (pWhere != null || pWhere != "")
                    strSql += " Where " + pWhere;

                // 1. Instantiate a new command
                SqlCommand cmd = new SqlCommand(strSql, _SqlConnection);

                // 2. Call ExecuteNonQuery to send command
                count = (int)cmd.ExecuteScalar();
            }
            finally
            {
                // Close the connection
                CloseDbConnection();
            }
            return count;
        }
        public int GetNoOfRecords(String pTable, String pColumn, String pWhere)
        {
            int count = -1;
            String strSql;

            try
            {
                // Open the connection
                OpenDbConnection();

                strSql = "select count(" + pColumn + ") from " + pTable;

                if (pWhere != null || pWhere != "")
                    strSql += " Where " + pColumn + " = '" + pWhere + "'";

                // 1. Instantiate a new command
                SqlCommand cmd = new SqlCommand(strSql, _SqlConnection);

                // 2. Call ExecuteNonQuery to send command
                count = (int)cmd.ExecuteScalar();
            }
            finally
            {
                // Close the connection
                CloseDbConnection();
            }
            return count;
        }

        public static void LogError(Exception ex)
        {
            Logger.WriteToErrorLog(ex.Message, ex.StackTrace, ex.Source, Application.StartupPath.ToString());
        }

        public String BackupDB(String connStr)
        {
            return BackupDB(connStr, String.Empty);
        }
        public String BackupDB(String connStr, String SqlBakPath)
        {
            try
            {
                if (SqlBakPath == String.Empty)
                {
                    SqlBakPath = Application.StartupPath.ToString() + "\\SQLBak\\";
                    if (!(System.IO.Directory.Exists(SqlBakPath)))
                        System.IO.Directory.CreateDirectory(SqlBakPath);
                }

                String DatabaseName = GlobalVariables.GetDBName(connStr);
                String strPath = SqlBakPath + DatabaseName + DateTime.Today.DayOfWeek.ToString();

                if (DateTime.Today.DayOfWeek == DayOfWeek.Sunday)
                    strPath += ".BAK";
                else
                {
                    strPath += ".dif";
                    System.IO.File.Delete(strPath);
                }

                String sql = "Backup database " + DatabaseName
                    + " to disk = '" + strPath + "'";

                if (DateTime.Today.DayOfWeek == DayOfWeek.Sunday)
                    sql += " with format";
                else
                    sql += " with DIFFERENTIAL";

                DataTable dt = new Runme().GetSQLEdition();
                if (!dt.Rows[0]["Edition"].ToString().Contains("Express Edition"))
                    sql += ", COMPRESSION";

                if (!new Runme().RunSQLCMD(sql, connStr, 15000))
                    return strPath;
                return String.Empty;
            }
            catch (Exception ex)
            {
                Logger.WriteToErrorLog(ex);
                return String.Empty;
            }
        }

        public String BackupPostgreSQLDB(String sConnStr)
        {
            return BackupPostgreSQLDB(sConnStr, String.Empty);
        }
        public String BackupPostgreSQLDB(String sConnStr, String SqlBakPath)
        {
            try
            {
                if (SqlBakPath == String.Empty)
                {
                    SqlBakPath = Application.StartupPath.ToString() + "\\SQLBak\\";
                    if (!(System.IO.Directory.Exists(SqlBakPath)))
                        System.IO.Directory.CreateDirectory(SqlBakPath);
                }

                //https://www.postgresql.org/docs/current/libpq-pgpass.html
                //%APPDATA%\postgresql\pgpass.conf 
                //This file should contain lines of the following format:
                //hostname:port:database:username:password

                String pass = ConnName.GetPostGreSQLpgpass(sConnStr);
                if (!String.IsNullOrEmpty(pass))
                {
                    String postgreDir = GlobalVariables.GetAppDataFolder() + "\\postgresql";
                    String postgreFN = postgreDir + "\\pgpass.conf";
                    if (!(System.IO.Directory.Exists(postgreDir)))
                        System.IO.Directory.CreateDirectory(postgreDir);
                    if (!(System.IO.File.Exists(postgreFN)))
                        System.IO.File.Create(postgreFN).Close();

                    Boolean bFound = false;
                    String[] content = File.ReadAllText(postgreFN).Split('\n');
                    foreach (String s in content)
                        if (s == pass)
                            bFound = true;

                    if (!bFound)
                        File.AppendAllText(postgreFN, pass);
                }

                String DatabaseName = ConnName.GetDBName(sConnStr);
                String strPath = SqlBakPath + DatabaseName + DateTime.Today.DayOfWeek.ToString() + ".bak";
                String sql = @" --file ""C:\\Reza\\Tmp\\MHLLIVE.sql"" --host ""localhost"" --port ""5432"" --username ""postgres"" --verbose --format=c --blobs ""tripsdb""";
                sql = sql.Replace(@"C:\\Reza\\Tmp\\MHLLIVE.sql", strPath);
                sql = sql.Replace("tripsdb", DatabaseName);
                System.Diagnostics.Process.Start(@"""C:\Program Files\PostgreSQL\12\bin\pg_dump.exe""", sql).WaitForExit();

                long lBackupSize = 0;
                try
                {
                    FileInfo f = new FileInfo(strPath);
                    lBackupSize = f.Length;
                }
                catch { }
                String sResult = strPath + " " + lBackupSize.ToString();
                return sResult;
            }
            catch (Exception ex)
            {
                Logger.WriteToErrorLog(ex);
                return ex.ToString();
            }
        }
    }
}
