﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NavLib.Squarell
{
    public class DecodeSquarell
    {
        //$FMS4,234400,364038,105265,34.2,303,0,9563,143,8358,437,1132,0,14160,1452,0,0
        //$FMS3,1,3,1,0,0,0,0,0,0,0,1,0,5,0,0
        //$FMS1,349982.82,161000,13260.3,4.5,1183,7,0,14.8,0,0,1,0,0,87,0.1,0,0,0,0,0,0,0

        public String Odometer = String.Empty;
        public String TotalFuel = String.Empty;
        public String EngineHours = String.Empty;
        public String ActualSpeed = String.Empty;
        public String ActualEngineSpeed = String.Empty;
        public String ActualEngineTorque = String.Empty;
        public String KickdownSwitch = String.Empty;
        public String AcceleratorPedalPosition = String.Empty;
        public String BrakeSwitch = String.Empty;
        public String ClutchSwitch = String.Empty;
        public String CruiseActive = String.Empty;
        public String PTOActive = String.Empty;
        public String FuelLevel = String.Empty;
        public String EngineTemperature = String.Empty;
        public String TurboPressure = String.Empty;
        public String AxleWeight0 = String.Empty;
        public String AxleWeight1 = String.Empty;
        public String AxleWeight2 = String.Empty;
        public String AxleWeight3 = String.Empty;
        public String ServiceDistance = String.Empty;

        String[] MsgArray;
        System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();

        public DecodeSquarell(String[] strReport)
        {
            MsgArray = strReport;

            if (strReport[0] == "$FMS1")
            {
                Odometer = strReport[1];
                TotalFuel = strReport[2];
                EngineHours = strReport[3];
                ActualSpeed = strReport[4];
                ActualEngineSpeed = strReport[5];
                ActualEngineTorque = strReport[6];
                KickdownSwitch = strReport[7];
                AcceleratorPedalPosition = strReport[8];
                BrakeSwitch = strReport[9];
                ClutchSwitch = strReport[10];
                CruiseActive = strReport[11];
                PTOActive = strReport[12];
                FuelLevel = strReport[13];
                EngineTemperature = strReport[14];
                TurboPressure = strReport[15];
                AxleWeight0 = strReport[16];
                AxleWeight1 = strReport[17];
                AxleWeight2 = strReport[18];
                AxleWeight3 = strReport[19];
                ServiceDistance = strReport[20];
            }
        }
        public String AllData()
        {
            String strResult = String.Empty;
            for (int i = 0; i < MsgArray.Length; i++)
                strResult += MsgArray[i] + ",";

            return strResult;
        }
    }
}