﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NavLib.Globals;
using System.Data;
using NavLib.Data;

namespace NavLib.Inventia
{
    public class DecodeInventia
    {
        DataSet ds = new DataSet();
        public DecodeInventia(Byte[] bytPacketData)
        {
            Byte[] MyPacketData = Utils.CompressBytWorking(bytPacketData);
            ds = Functions.DecompressBytesToDs(MyPacketData);
        }

        public String GetUnitID()
        {
            String s = String.Empty;
            if (ds.Tables.Contains("dtParam"))
                s = ds.Tables["dtParam"].Rows[0]["UnitID"].ToString();

            return s;
        }

        public DateTime GetUTCDate()
        {
            DateTime dt = DateTime.MinValue;
            String s = String.Empty;
            if (ds.Tables.Contains("dtParam"))
                s = ds.Tables["dtParam"].Rows[0]["dtDateTime"].ToString();

            DateTime.TryParse(s, out dt);
            return dt;
        }

        public DataSet GetAllData()
        {
            return ds;
        }
    }
}
