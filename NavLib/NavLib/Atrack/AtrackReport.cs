﻿using System;
using System.Collections.Generic;
using System.Text;
using NavLib.Atrack.Mechatronics;
using NavLib.Globals;
using NavLib.Squarell;

namespace NavLib.Atrack
{
    public class AtrackReport
    {
        //20120917100755,20120917100755,20120917101741,57506294,-20225811,0,2,0,9,1,0,0,0,,2000,2000, 
        //updated with Temperature 3,4
        //20170913102950,20170913102951,20170913104108,57506464,-20225454,31,2,631326,11,1,0,0,317,,241,238,,3,166,241,239 
        //int OffsetGPSDate = 5 - 5;
        int OffsetRTCDate = 6 - 5;
        //int OffsetReportSendingTime = 7 - 5;
        int OffsetLon = 8 - 5;
        int OffsetLat = 9 - 5;
        //int OffsetAngle = 10 - 5;
        int OffsetReportID = 11 - 5;
        //int OffsetOdo = 12 - 5;
        int OffsetDHop = 13 - 5;
        int OffsetAllInputStatus = 14 - 5;
        int OffsetSpeed = 15 - 5;
        //int OffsetAllOutputStatus = 16 - 5;
        int OffsetAnalogInputValue = 17 - 5;
        int OffsetDriverID = 18 - 5;
        int OffsetTemperature1 = 19 - 5;
        int OffsetTemperature2 = 20 - 5;
        int OffsetTxtMsg = 21 - 5;
        int OffSetCustomInfo = 22 - 5;
        int OffsetTemperature3 = 26 - 5;
        int OffsetTemperature4 = 27 - 5;

        String[] MsgArray;
        public DecodeSquarell dSquarell;
        public Boolean bHasSquarellData = false;
        System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();

        public Boolean hasMechatronicsData = false;

        public AtrackReport(String[] strReport)
        {
            MsgArray = strReport;

            //Squarell Data
            //Still on test
            try
            {
                int iSqlSearch;
                for (iSqlSearch = 0; iSqlSearch < strReport.Length; iSqlSearch++)
                {
                    if (strReport[iSqlSearch] != null)
                    {
                        int strIndex = strReport[iSqlSearch].IndexOf("$FMS");
                        if (strIndex >= 0)
                        {
                            bHasSquarellData = true;
                            String[] strLineRep = new String[strReport.Length - iSqlSearch];
                            Array.Copy(strReport, iSqlSearch, strLineRep, 0, strReport.Length - iSqlSearch);
                            dSquarell = new DecodeSquarell(strLineRep);
                            break;
                        }
                    }
                }
            }
            catch { }
        }

        public Double GetLatitude()
        {
            Double LatX = 0.0;
            if (IsGPSValid())
            {
                String g = MsgArray[OffsetLat].Trim();
                int Lat = 3;
                if (g.Length == 8)
                    Lat = 2;

                LatX = Double.Parse(g.Substring(0, Lat), nfi);
                Double dblTempDegrees = Double.Parse("0." + g.Substring(Lat), nfi);
                LatX -= dblTempDegrees;
            }
            return LatX;
        }
        public Double GetLongitude()
        {
            Double LatX = 0.0;
            if (IsGPSValid())
            {
                int Lat = 2;
                String g = MsgArray[OffsetLon].Trim();
                LatX = Double.Parse(g.Substring(0, Lat), nfi);
                Double dblTempDegrees = Double.Parse("0." + g.Substring(Lat), nfi);
                LatX += dblTempDegrees;
            }
            return LatX;
        }
        public DateTime GetUTCDate()
        {
            //20120907141638    yyyymmddhhmmss
            String g = MsgArray[OffsetRTCDate];
            int yy = Convert.ToInt16(g.Substring(0, 4));
            int mm = Convert.ToInt16(g.Substring(4, 2));
            int dd = Convert.ToInt16(g.Substring(6, 2));
            int HH = Convert.ToInt16(g.Substring(8, 2));
            int MM = Convert.ToInt16(g.Substring(10, 2));
            int SS = Convert.ToInt16(g.Substring(12, 2));
            DateTime dt = new DateTime(yy, mm, dd, HH, MM, SS);
            return dt;
        }

        public Boolean IsGPSValid()
        {//should complete
            Boolean bResult = false;
            int i = Convert.ToInt16(MsgArray[OffsetDHop]);
            if (i <= 20)
                bResult = true;
            return bResult;
        }
        public Double GetSpeed()
        {
            //Km/h
            String str = MsgArray[OffsetSpeed];
            return Convert.ToDouble(str, nfi);
        }

        Boolean GetAux(int pos)
        {
            String strAllInput = MsgArray[OffsetAllInputStatus].Trim();
            Int64 iAllinput = Convert.ToInt64(strAllInput);
            strAllInput = Utils.ToBinary(iAllinput);
            Boolean bResult = false;
            if (strAllInput.Substring(strAllInput.Length - 1 - pos, 1) == "1")
                bResult = true;
            return bResult;
        }
        public Boolean GetAux0()
        {
            //Ignition
            return GetAux(0);
        }
        public Boolean GetAux1()
        {
            return GetAux(1);
        }
        public Boolean GetAux2()
        {
            return GetAux(2);
        }
        public Boolean GetAux3()
        {
            return GetAux(3);
        }

        public Double GetTemperature(int pos)
        {
            int SensorValue = 14;
            switch (pos)
            {
                case 1:
                    SensorValue = OffsetTemperature1;
                    break;

                case 2:
                    SensorValue = OffsetTemperature2;
                    break;

                case 3:
                    SensorValue = OffsetTemperature3;
                    break;

                case 4:
                    SensorValue = OffsetTemperature4;
                    break;
            }

            int i = 2000;
            String s = String.Empty;
            if (MsgArray.Length > SensorValue)
                s = MsgArray[SensorValue];
            if (!String.IsNullOrEmpty(s))
                int.TryParse(s.Trim(), out i);

            return (i * 0.1);
        }

        public String GetDriverID()
        {
            String strResult = String.Empty;
            String str = MsgArray[OffsetDriverID];
            if (str.Length == 12)
            {
                strResult = str.Substring(10, 2);
                strResult += str.Substring(8, 2);
                strResult += str.Substring(6, 2);
                strResult += str.Substring(4, 2);
                strResult += str.Substring(2, 2);
                strResult += str.Substring(0, 2);
            }

            return strResult;
        }

        public String GetLogReason()
        {
            String strResult = String.Empty;
            String str = MsgArray[OffsetReportID];
            switch (str.Trim())
            {
                case "2":
                    strResult = "2 Time (Heading)";
                    if (!GetAux0())
                        strResult = "2 TimeDistanceAngleIGgnOff Heartbeat (Heartbeat)";
                    break;
                case "4":
                    strResult = "4 Distance (Heading)";
                    if (!GetAux0())
                        strResult = "4 DistanceIGgnOff Heartbeat (Heartbeat)";
                    break;
                case "5":
                    strResult = "5 Heading (Heading)";
                    if (!GetAux0())
                        strResult = "5 HeadingIGgnOff Heartbeat (Heartbeat)";
                    break;
                case "10":
                    String s = String.Empty;
                    try
                    {
                        s = "[" + GetDriverID() + "]";
                    }
                    catch { }
                    strResult = "10 DriverID (DriverID)" + s;
                    break;
                case "25":
                    hasMechatronicsData = true;
                    strResult = "25 hasMechatronicsData";
                    break;
                case "101":
                    strResult = "101 IgnOn (Ignition)";
                    break;
                case "102":
                    strResult = "102 (IgnOff) (Ignition)";
                    break;
                case "103":
                    if (GetAux(0))
                        strResult = "103 (Time) Idle Start";
                    else
                        strResult = "103 Idle Start but ign off";
                    break;
                case "105":
                    strResult = "105 (Reset) main power lost";
                    break;
                case "106":
                    strResult = "106 (Reset) (main power up)";
                    break;
                case "107":
                    strResult = "107 (HarshAS) Harsh Acc (HarshAS_Start)";
                    break;
                case "108":
                    strResult = "108 (HarshAS) Harsh Acc (HarshAS_End)";
                    break;
                case "109":
                    strResult = "109 (HarshBS) Harsh brake (HarshBS_Start)";
                    break;
                case "110":
                    strResult = "110 (HarshBS) Harsh Brake (HarshBS_End)";
                    break;
                case "111":
                    strResult = "111 (Aux) (Aux1On)";
                    break;
                case "112":
                    strResult = "112 (Aux) (Aux1Off)";
                    break;
                case "113":
                    strResult = "113 (Aux) (Aux2On)";
                    break;
                case "114":
                    strResult = "114 (Aux) (Aux2Off)";
                    break;
                case "115":
                    strResult = "115 (Aux) (Aux3On)";
                    break;
                case "116":
                    strResult = "116 (Aux) (Aux3Off)";
                    break;
                case "125":
                    strResult = "125 (Impact) Impact";
                    break;
                case "126":
                    strResult = "126 (HarshCN) Harsh Corner";
                    break;
                default:
                    strResult = str + "(???)";
                    break;
            }

            return str + " > " + strResult + GetGPSStatus();
        }
        String GetGPSStatus()
        {
            //AT$FORM=0,,1,"%SS". last field is %SS. field 18
            String strGPSStatus = String.Empty;
            if (MsgArray.Length == 18)
            {
                Int64 i = Convert.ToInt64(MsgArray[17].Trim());
                String s = Utils.ToBinary(i);
                if (s.Substring(s.Length - 1 - 0, 1).Contains("1"))
                    strGPSStatus += " GPS signal reception Timeout (GpsShutdown)";
                if (s.Substring(s.Length - 1 - 1, 1).Contains("1"))
                    strGPSStatus += " GPS antenna cable short circuit (GpsShutdown)";
                if (s.Substring(s.Length - 1 - 2, 1).Contains("1"))
                    strGPSStatus += " GPS antenna Disconnected (GpsShutdown)";
            }
            return strGPSStatus;
        }

        public DecodeMechatronics GetMechatronicsData()
        {
            GetLogReason();
            if (hasMechatronicsData)
                return new DecodeMechatronics(MsgArray[OffsetTxtMsg].Trim());
            return new DecodeMechatronics(String.Empty);
        }
        public Double GetAnalogInputValue()
        {
            String str = MsgArray[OffsetAnalogInputValue];
            int i = Convert.ToInt32(str, nfi);
            //Trying to elimitate some records
            //if (GetUTCDate().Second > 15)
            //    i = 0;
            return i * 0.01;
        }

        String GetCustomInfo()
        {
            //@P,8EA5,408,35,356308047012056,20160112130159,20160112130159,20160112130403,57464126,-20220820,183,2,1088,10,1,17,0,0,,2000,2000,,%CI%VN%RP%MF%EL%TP%ET%FL%ML%OD%FC%MV,WF0DXXGBBDAU37766,1845,700,78,2000,84,49,0,1088,101,134
            //20160112130349,20160112130348,20160112130403,57468484,-20222170,187,2,1094,10,1,37,0,0,,2000,2000,,%CI%VN%RP%MF%EL%TP%ET%FL%ML%OD%FC%MV,WF0DXXGBBDAU37766,2616,333,74,2000,87,44,0,1094,102,136

            //Above has 
            //%CI%VN%RP%MF%EL%TP%ET%FL%ML%OD%FC%MV,WF0DXXGBBDAU37766,1845,700,78,2000,84,49,0,1088,101,134
            //%CI%VN%RP%MF%EL%TP%ET%FL%ML%OD%FC%MV,WF0DXXGBBDAU37766,2616,333,74,2000,87,44,0,1094,102,136
            if (MsgArray.Length <= OffSetCustomInfo)
                return String.Empty;

            String str = MsgArray[OffSetCustomInfo];
            return str;
        }
        String GetCustomInfo(String sCI)
        {
            String sResult = String.Empty;
            String strCustomInfo = GetCustomInfo();
            if (String.IsNullOrEmpty(strCustomInfo))
                return String.Empty;

            String[] sData = strCustomInfo.Split('%');
            int i = -2;
            Boolean b = false;

            if (!String.IsNullOrEmpty(strCustomInfo))
            {
                foreach (String s in sData)
                {
                    i++;
                    if (s == sCI)
                    {
                        b = true;
                        break;
                    }
                }
            }

            if (b)
            {
                sResult = "";
                try
                {
                    String[] OBD = new String[sData.Length - 1];
                    Array.Copy(MsgArray, OffSetCustomInfo, OBD, 0, sData.Length - 1);
                    sResult = OBD[i];
                }
                catch { }
            }

            return sResult;
        }
        public String GetOBDDataInMsg()
        {
            return GetCustomInfo("CI");
        }
        public String GetOBDFuel()
        {
            return GetCustomInfo("FL");
        }
        public String GetOBDVIN()
        {
            return GetCustomInfo("VN");
        }
        public String GetOBDMaximumEngineRPMReadingBetweenTwoReports()
        {
            return GetCustomInfo("RP");
        }
        public String GetOBDMassAirFlowRate()
        {
            //in 0.01grams/sec
            return GetCustomInfo("MF");
        }
        public String GetOBDEngineLoad()
        {
            //%
            return GetCustomInfo("EL");
        }
        public String GetOBDEngineCoolantTemperature()
        {
            //℃
            return GetCustomInfo("ET");
        }
        public String GetOBDMalfunctionIndicator()
        {
            return GetCustomInfo("ML");
        }
        public String GetOBDOdometer()
        {
            return GetCustomInfo("OD");
        }
        public String GetOBDFuelUsed()
        {
            //in 0.1 liter
            return GetCustomInfo("FC");
        }
        public String GetOBDMainPowerVoltage()
        {
            //in 0.1V
            return GetCustomInfo("FL");
        }
    }
}