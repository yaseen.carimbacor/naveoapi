﻿using System;
using System.Collections.Generic;
using System.Text;
using NavLib.Globals;
using System.Text.RegularExpressions;

namespace NavLib.Atrack
{
    public class DecodeAtrack
    {
        //@P,104B,783,18,352599044605009,
        //20120917100153,20120917100155,20120917101741,57506341,-20225771,0,101,0,0,1,0,0,0,,2000,2000,  
        //20120917100155,20120917100155,20120917101741,57506331,-20225805,0,2,0,26,1,0,0,0,,2000,2000,  
        //20120917100255,20120917100254,20120917101741,57506290,-20225805,0,103,0,9,1,0,0,0,,2000,2000,  
        //20120917100255,20120917100255,20120917101741,57506290,-20225805,0,2,0,9,1,0,0,0,,2000,2000,  
        //20120917100356,20120917100355,20120917101741,57506311,-20225830,0,2,0,9,1,0,0,0,,2000,2000,  
        //20120917100427,20120917100426,20120917101741,57506310,-20225823,0,102,0,11,0,0,0,0,,2000,2000,  
        //20120917100427,20120917100426,20120917101741,57506310,-20225823,0,104,0,11,0,0,0,0,,2000,2000,  
        //20120917100755,20120917100755,20120917101741,57506294,-20225811,0,2,0,9,1,0,0,0,,2000,2000,  

        //@P,17DF,104,3,354660048431723,1345114872,1345114872,1345114872,121562678,25083603,48,2,14,43,0,6,0,0,,2000,2000,
        //@P,960D,115,5,352599044679053,20120907141638,20120907141637,20120907141637,57506266,-20225846,72,2,0,9,1,11,0,0,,2000,2000,

        //0     @P - prefix  2 bytes
        //1     17DF - CRC  2 bytes
        //2     104 - packet length  2 bytes
        //3     3 - sequence ID  2 bytes
        //4     354660048431723 - unit ID 8 bytes
        //5     1345114872 - GPS date time
        //6     1345114872 - RTC date time
        //7     1345114872 - report sending time
        //8     121562678 - longitude  (S32)
        //9     25083603 - latitude  (S32)
        //10    48 - heading angle (U16)
        //11    2 - report ID (U8)
        //12    14 - odometer (U32)
        //13    43 - GPS HDOP (U16)
        //14    0 - All input status (U8)
        //15    6 - Speed (U16)
        //16    0 - All output status (U8)
        //17    0 - analog input value (U16)
        //18    ,, - Driver ID 
        //19    2000 - temperature value 1 (S16)
        //20    2000 - temperature value 2 (S16)
        //21    EOL

        //AT$INFO=?
        //$INFO=352599044605009,AT5,Rev.1.48,352599044605009,617100169676682,8923002401696766826,140,42,11,1,31,1,0  
        //$INFO=352599044605009,
        //AT5,
        //Rev.1.48,
        //352599044605009,
        //617100169676682,
        //8923002401696766826,
        //140,
        //42,
        //11,
        //1,
        //31,
        //1,
        //0  

        int OffsetHeader = 0;
        int OffsetCRC = 1;
        //int OffsetPacketLen = 2;
        int OffsetSeqID = 3;
        int OffsetUnitID = 4;

        String[] MsgArray;
        Byte[] bFullbytPacketData;
        System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
        public List<AtrackReport> aReports;

        public DecodeAtrack(Byte[] bytPacketData)
        {
            Byte[] MyPacketData = Utils.CompressBytWorking(bytPacketData);
            String Msg = Encoding.ASCII.GetString(MyPacketData);

            nfi.NumberDecimalSeparator = ".";
            MsgArray = Msg.Split(',');
            bFullbytPacketData = bytPacketData;

            //if (MsgArray[OffsetCRC] == "FFA5")
            //{ }

            if (!isIMSI())
            {
                int RepCount = 0;
                String[] Reports = Regex.Split(Msg, "\r\n");
                aReports = new List<AtrackReport>();
                foreach (String s in Reports)
                {
                    RepCount++;
                    String[] strLineRep = s.Split(',');
                    int iLen = strLineRep.Length;
                    if (iLen == 1) //Last s in Reports
                        continue;
                    String[] myLineRep = new String[iLen];
                    try
                    {
                        if (RepCount == 1)
                            Array.Copy(strLineRep, 5, myLineRep, 0, iLen - 5);
                        else
                            Array.Copy(strLineRep, 0, myLineRep, 0, iLen);

                        aReports.Add(new AtrackReport(myLineRep));
                    }
                    catch { }
                }
            }
        }
        void DecodeAtrackOld(Byte[] bytPacketData)
        {
            Byte[] MyPacketData = Utils.CompressBytWorking(bytPacketData);
            String Msg = Encoding.ASCII.GetString(MyPacketData);

            nfi.NumberDecimalSeparator = ".";
            MsgArray = Msg.Split(',');
            bFullbytPacketData = bytPacketData;

            if (MsgArray[OffsetCRC] == "FFA5")
            { }

            if (!isIMSI())
            {
                int RepCount = 0;
                String[] Reports = Regex.Split(Msg, "\r\n");
                aReports = new List<AtrackReport>();
                foreach (String s in Reports)
                {
                    RepCount++;
                    String[] strLineRep = s.Split(',');
                    String[] myLineRep = new String[17];
                    try
                    {
                        if (RepCount == 1)
                            Array.Copy(strLineRep, 5, myLineRep, 0, 17);
                        else
                            Array.Copy(strLineRep, 0, myLineRep, 0, 17);

                        aReports.Add(new AtrackReport(myLineRep));
                    }
                    catch { }
                }
            }
        }

        public String GetAckUnitID() 
        { 
            //KeepAlive	12 Bytes
            //yeah... byte 0 is the header 0xFE 0x02        
            //[12:00:05] Kelvin Wang: byte 2 is the unit ID             0 - 1
            //[12:00:11] Kelvin Wang: and byte 10 is the sequence ID    2 - 9
            //[12:00:33] Kelvin Wang: header is 2 bytes
            //[12:00:37] Kelvin Wang: unit ID is 8 bytes long
            //[12:00:42] Kelvin Wang: and sequence id is 2 bytes        10 - 11

            return Utils.BuildLong(bFullbytPacketData, 2, 9).ToString();
        }
        public String GetAckSequenceID()
        {
            return Utils.BuildLong(bFullbytPacketData, 10, 11).ToString();
        }
        public String GetReportAck()
        {                                 
            //ASCII			        Hex
            //FE02			        FE02                Header
            //352599044608870		000140AFDCCD5366    IMEI    (must zero pad to 8 bytes)
            //30			        001E                SeqID   (must zero pad to 2 bytes)

            String UnitID = GetUnitID();
            String sSeqID = GetSeqID();
            String sReply = "FE02";
            String g = Utils.BigIntToHex(Convert.ToInt64(UnitID));
            int iCount = 16 - g.Length;
            for (int i = 0; i < iCount; i++)
                g = "0" + g;
            sReply = sReply + g;

            g = Utils.BigIntToHex(Convert.ToInt64(sSeqID));
            iCount = 4 - g.Length;
            for (int i = 0; i < iCount; i++)
                g = "0" + g;
            sReply = sReply + g;

            return sReply;
        }

        public Boolean isIMSI()
        {
            Boolean bResult = false;
            if (GetHeader().Contains("INFO"))
                bResult = true;

            return bResult;
        }
        public String GetSeqID()
        {
            String strResult = MsgArray[OffsetSeqID];
            return strResult;
        }
        public String GetHeader()
        {
            //@P
            String strResult = MsgArray[OffsetHeader];
            return strResult;
        }
        public String GetUnitID()
        {
            String strResult = MsgArray[OffsetUnitID];
            if (isIMSI())
                strResult = MsgArray[3];
            return strResult;
        }
        public String GetImsi()
        {
            String strResult = String.Empty;
            if (isIMSI())
                strResult = MsgArray[4];
            return strResult;
        }
        public String GetCID()
        {
            String strResult = String.Empty;
            if (isIMSI())
                strResult = MsgArray[5];
            return strResult;
        }
    }
}
