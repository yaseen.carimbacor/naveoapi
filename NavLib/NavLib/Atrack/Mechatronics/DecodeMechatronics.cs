﻿using NavLib.Globals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NavLib.Atrack.Mechatronics
{
    public class DecodeMechatronics
    {
        //@P,0DD0,169,475,860041040665138,20200310055244,20200310055245,20200310055245,57482984,-20170389,3,25,6,11,0,0,2,0,,2000,2000,463D3039423820743D3143204E3D423836362E300D0A
        //463D3039423820743D3143204E3D423836362E300D0A
        //Convert this from HEX to ASCII and get: F=09B8 t=1C N=B866.0
        //Calibrated Litres N; Raw F
        //Convert HEX value 09B8 to decimal: 2488
        //Divide by 10 and get 248.8 litres to display directly on UI
        //Temperature of sensor head
        //Convert Temperature 1C to decimal: 28 °C


        Double Raw = 0;
        Double CalibratedLvlInLitres  = 0;
        int Temperature =0;
        Boolean hasData = false;
        public String allData = "No Data";

        public DecodeMechatronics(String data)
        {
            String[] mData = data.Split('\u001a');
            String[] MechatronicsData = Utils.Hex2ASCII(mData[0]).Split(' ');
            foreach(String s in MechatronicsData)
            {
                if (s.ToUpper().StartsWith("F"))
                    Raw = Convert.ToDouble(decodeData(s)) / 10;
                else if (s.ToUpper().StartsWith("N"))
                {
                    CalibratedLvlInLitres = Convert.ToDouble(decodeData(s)) / 10;
                    hasData = true;
                }
                else if (s.ToUpper().StartsWith("T"))
                    Temperature = decodeData(s);
            }

            if (hasData)
                allData = "Raw:" + Raw.ToString() + ":Sensor;[Fuel]:" + CalibratedLvlInLitres + ":Liters;[Temperature]:" + Temperature+":C";
        }

        int decodeData(String data)
        {
            String[] s = data.Split('=');
            String[] g = s[1].Split('.');
            return Utils.HexToDecimal(g[0].Trim());
        }
    }
}
