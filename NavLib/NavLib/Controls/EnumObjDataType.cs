using System;

namespace NavLib.Controls
{
    public enum EnumFormState { fmView, fmNew, fmModify, fmDelete, fmSave, fmNull }
    public enum EnumStringCase { None, Upper, Lower, Proper }
    public enum EnumObjDataType { String = 0x01, Numeric = 0x02, Decimal = 0x03, DateTime = 0x04 }
    public enum EnumObjectType { Desc, Data, Filter, Key, DataKey }
    public enum EnumObjectMask { None, DateOnly, PhoneWithArea, IpAddress, SSN, Decimal, Digit, Optics_Sphere, Optics_Add };

}
