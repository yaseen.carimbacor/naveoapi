using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace NavLib.Controls
{
    public partial class NaveoFormTab : Form
    {
		#region Form Delegates
		public delegate void FormStateHandler();
		public delegate void AddItemHandler();
		public delegate void EditItemHandler();
		public delegate void DeleteItemHandler();
		public delegate Boolean ApplyInsertHandler();
		public delegate Boolean ApplyModifyHandler();
		public delegate Boolean ApplyDeleteHandler();

		public delegate void CopyItemHandler();
		public delegate void RefreshItemHandler();
		public delegate void PrintItemHandler();
		public delegate void NavigatorItemClick();
		public delegate void UpdateBindingHandler();
		public delegate void TabIndexChangeHandler();

		#endregion

		#region Form Events
		public event FormStateHandler FormStateChange;
		public event AddItemHandler FormAddItem;
		public event EditItemHandler FormEditItem;
		public event DeleteItemHandler FormDeleteItem;
		public event ApplyInsertHandler FormApplyInsert;
		public event ApplyModifyHandler FormApplyModify;
		public event ApplyDeleteHandler FormApplyDelete;

		public event CopyItemHandler FormCopyItems;
		public event RefreshItemHandler FormRefresh;
		public event PrintItemHandler FormPrint;
		public event NavigatorItemClick FormNavigatorItemClick;
		public event UpdateBindingHandler FormUpdateBinding;
		public event TabIndexChangeHandler FormTabIndexChange;
		#endregion

		#region Subscriber Notification
		protected virtual void OnFormStateChange()
		{
			SetToolbar();
			if (FormStateChange != null)
			{
				FormStateChange();
			}
		}
		protected virtual void OnFormAddItem()
		{
			if (FormAddItem != null)
			{
				FormAddItem();
			}
		}
		protected virtual void OnFormEditItem()
		{
			if (FormEditItem != null)
			{
				FormEditItem();
			}
		}
		protected virtual void OnFormDeleteItem()
		{
			if (FormDeleteItem != null)
			{
				FormDeleteItem();
			}
		}
		protected virtual Boolean OnFormApplyInsert()
		{
			Boolean bOk = false;
			if (FormApplyInsert != null)
			{
				bOk = FormApplyInsert();
			}
			return bOk;
		}
		protected virtual Boolean OnFormApplyModify()
		{
			Boolean bOk = false;
			if (FormApplyModify != null)
			{
				bOk = FormApplyModify();
			} return bOk;

		}
		protected virtual Boolean OnFormApplyDelete()
		{
			Boolean bOk = false;
			if (FormApplyDelete != null)
			{
				bOk = FormApplyDelete();
			} return bOk;

		}
		protected virtual void OnFormCopyItems()
		{
			if (FormCopyItems != null)
			{
				FormCopyItems();
			}
		}
		protected virtual void OnFormRefresh()
		{
			if (FormRefresh != null)
			{
				FormRefresh();
			}
		}
		protected virtual void OnFormNavigatorItemClick()
		{
			if (FormNavigatorItemClick != null)
			{
				FormNavigatorItemClick();
			}
		}
		protected virtual void OnFormPrint()
		{
			if (FormPrint != null)
			{
				FormPrint();
			}
		}
		//protected virtual void OnFormClose()
		//{
		//    if (FormClose != null)
		//    {
		//        FormClose();
		//    }
		//}
		protected virtual void OnFormUpdateBinding()
		{
			if (FormUpdateBinding != null)
			{
				FormUpdateBinding();
			}
		}
		protected virtual void OnFormTabIndexChange()
		{
			if (FormTabIndexChange != null)
			{
				FormTabIndexChange();
			}
		}
		#endregion

		#region Variables
		private EnumFormState _FormState = EnumFormState.fmView;
		private Boolean _AllowEdit = false;
		private Boolean _AllowAdd = false;
		private Boolean _AllowDel = false;
		private Boolean _AllowCopy = false;
		private Boolean _AllowPrint = false;
		private TabControl _FrmTab = null;
		private String _ToolbarMessageText = String.Empty;
		#endregion

		#region Constructors
        public NaveoFormTab()
		{
			//if (Main.ValidateLibExpiryDate()) 
			InitializeComponent();
		}
		#endregion

		#region properties
		public EnumFormState FormState
		{
			get { return _FormState; }
			set
			{
				_FormState = value;
				OnFormStateChange();
			}
		}
		public Boolean AllowEdit
		{
			get { return _AllowEdit; }
			set { _AllowEdit = value; }
		}
		public Boolean AllowAdd
		{
			get { return _AllowAdd; }
			set { _AllowAdd = value; }
		}
		public Boolean AllowDel
		{
			get { return _AllowDel; }
			set { _AllowDel = value; }

		}
		public Boolean AllowCopy
		{
			get { return _AllowCopy; }
			set { _AllowCopy = value; }
		}
		public Boolean AllowPrint
		{
			get { return _AllowPrint; }
			set { _AllowPrint = value; }
		}
		public TabControl FrmTab
		{
			get { return _FrmTab; }
			set
			{
				_FrmTab = value;
				if (_FrmTab != null)
					_FrmTab.SelectedIndexChanged += new System.EventHandler(_FrmTab_SelectedIndexChanged);
			}
		}
		public String SetToolbarMessageText
		{
			get { return _ToolbarMessageText; }
			set
			{
				_ToolbarMessageText = value;
				bndNav.Items["tbTextMessage"].Text = _ToolbarMessageText;
			}
		}
		public void ForceResetToolbar()
		{
			SetToolbar();
		}
		#endregion

		#region Methods
		private void btnSave_Click(object sender, EventArgs e)
		{
			Boolean bOk = false;
			try
			{
				switch (_FormState)
				{
					case EnumFormState.fmNew:
						//fire OnApplyInsert Event
						bOk = OnFormApplyInsert();
						break;
					case EnumFormState.fmModify:
						//fire OnApplyModify Event
						bOk = OnFormApplyModify();
						break;
					case EnumFormState.fmDelete:
						//fire OnApplyDelete Event
						bOk = OnFormApplyDelete();
						break;
				}
			}
			finally
			{
				//fire OnRefresh
				if (bOk)
				{
					_FormState = EnumFormState.fmView;
					OnFormRefresh();
					SetToolbar();
				}
			}
		}

		private void tbAddItem_Click(object sender, EventArgs e)
		{
			_FormState = EnumFormState.fmNew;
			OnFormAddItem();
			SetToolbar();
		}

		private void tbEditItem_Click(object sender, EventArgs e)
		{
			_FormState = EnumFormState.fmModify;
			OnFormEditItem();
			SetToolbar();
		}

		private void btnDelete_Click(object sender, EventArgs e)
		{
			_FormState = EnumFormState.fmDelete;
			OnFormDeleteItem();
			SetToolbar();
		}

		private void btnCopy_Click(object sender, EventArgs e)
		{
			OnFormCopyItems();
		}

		private void btnRefresh_Click(object sender, EventArgs e)
		{
			_FormState = EnumFormState.fmView;
			OnFormRefresh();
			SetToolbar();
			if (bndNav.BindingSource != null)
				bndNav.BindingSource.ResetBindings(false);
		}

		private void btnPrint_Click(object sender, EventArgs e)
		{
			OnFormPrint();
		}

		private void btnMoveFirst_Click(object sender, EventArgs e)
		{
			OnFormNavigatorItemClick();
		}

		private void btnMovePrevious_Click(object sender, EventArgs e)
		{
			OnFormNavigatorItemClick();
		}

		private void btnMoveNext_Click(object sender, EventArgs e)
		{
			OnFormNavigatorItemClick();
		}

		private void btnNoveLast_Click(object sender, EventArgs e)
		{
			OnFormNavigatorItemClick();
		}


		private void SetToolbar()
		{
			Boolean bState = true;
			switch (_FormState)
			{
				case EnumFormState.fmDelete:
					btnCopy.Enabled = !bState;
					btnPrint.Enabled = !bState;

					tbAddItem.Enabled = !bState;
					tbEditItem.Enabled = !bState;
					btnDelete.Enabled = !bState;
					btnSave.Enabled = bState;

					bndNav.MoveFirstItem.Enabled = !bState;
					bndNav.MovePreviousItem.Enabled = !bState;
					bndNav.MoveNextItem.Enabled = !bState;
					bndNav.MoveLastItem.Enabled = !bState;
					break;
				case EnumFormState.fmModify:
					btnCopy.Enabled = !bState;
					btnPrint.Enabled = !bState;

					tbAddItem.Enabled = !bState;
					tbEditItem.Enabled = !bState;
					btnDelete.Enabled = !bState;
					btnSave.Enabled = bState;

					bndNav.MoveFirstItem.Enabled = !bState;
					bndNav.MovePreviousItem.Enabled = !bState;
					bndNav.MoveNextItem.Enabled = !bState;
					bndNav.MoveLastItem.Enabled = !bState;
					break;
				case EnumFormState.fmNew:
					btnCopy.Enabled = !bState;
					btnPrint.Enabled = !bState;

					tbAddItem.Enabled = !bState;
					tbEditItem.Enabled = !bState;
					btnDelete.Enabled = !bState;
					btnSave.Enabled = bState;

					bndNav.MoveFirstItem.Enabled = !bState;
					bndNav.MovePreviousItem.Enabled = !bState;
					bndNav.MoveNextItem.Enabled = !bState;
					bndNav.MoveLastItem.Enabled = !bState;
					break;
				case EnumFormState.fmView:
					btnCopy.Enabled = _AllowCopy;
					btnPrint.Enabled = _AllowPrint;

					tbAddItem.Enabled = _AllowAdd;
					tbEditItem.Enabled = _AllowEdit;
					btnDelete.Enabled = _AllowDel;

					btnSave.Enabled = !bState;

					bndNav.MoveFirstItem.Enabled = bState;
					bndNav.MovePreviousItem.Enabled = bState;
					bndNav.MoveNextItem.Enabled = bState;
					bndNav.MoveLastItem.Enabled = bState;
					break;
			}
			btnRefresh.Enabled = bState;
		}

		#endregion

		#region Overide Methods

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			SetToolbar();
		}
		protected override void OnFormClosing(FormClosingEventArgs e)
		{
			if (_FormState != EnumFormState.fmView)
			{
				if (MessageBox.Show(this.Text + " Screen is in Edit Mode. \r\nDo you want to cancel the changes made?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
					e.Cancel = true;
			}

			base.OnFormClosing(e);
		}

		/// <summary>
		/// Applies keyboard shortcuts to Buttons
		/// </summary>
		/// <param name="keyData"></param>
		/// <returns></returns>
		protected override bool ProcessDialogKey(Keys keyData)
		{
			// Declare a variable to keep track of the key modifiers (Ctrl,Alt,Shift,etc.)  
			string modifiers = "";

			// Get the actual integer value of the keystroke  
			int keyCode = (int)keyData;

			// Check to see if the control key is on  
			if ((keyCode & (int)Keys.Control) != 0)
			{
				modifiers += "Ctrl";
			}

			// Check to see if the alt key is on  
			if ((keyCode & (int)Keys.Alt) != 0)
			{
				modifiers += "";
			}

			// Check to see if the shift key is on  
			if ((keyCode & (int)Keys.Shift) != 0)
			{
				modifiers += "";
			}

			// Strip off the modifier keys  
			keyCode = keyCode & 0xFFFF;

			// Attempt to convert the remaining bits to the enum name  
			Keys key = (Keys)keyCode;

            if ((key != Keys.Menu) && (key != Keys.ControlKey) && (key != Keys.ShiftKey))
            {
                // If the user pressed the Ctrl-F1 combination  
                if (key == Keys.F1 && modifiers == "")
                {
                    // Do something special  
                }
                //Reza
                //if (modifiers == "Ctrl" && key == Keys.S)
                if (modifiers == "Ctrl")
                    switch (key)
                    {
                        case (Keys.N):
                            {
                                if (this.tbAddItem.Enabled == true)
                                    this.tbAddItem_Click(this.tbAddItem, new EventArgs());
                                break;
                            }

                        case (Keys.E):
                            {
                                if (this.tbEditItem.Enabled == true)
                                    this.tbEditItem_Click(this.tbEditItem, new EventArgs());
                                break;
                            }

                        case (Keys.D):
                            {
                                if (this.btnDelete.Enabled == true)
                                    this.btnDelete_Click(this.btnDelete, new EventArgs());
                                break;
                            }

                        case (Keys.S):
                            {
                                if (this.btnSave.Enabled == true)
                                    this.btnSave_Click(this.btnSave, new EventArgs());
                                break;
                            }

                        case (Keys.Q):
                            {
                                if (this.btnSave.Enabled == true)
                                    if (this.btnClose.Enabled == true)
                                    {
                                        this.btnSave_Click(this.btnSave, new EventArgs());
                                        this.btnClose_Click(this.btnClose, new EventArgs());
                                    }
                                break;
                            }

                        case (Keys.R):
                            {
                                if (this.btnRefresh.Enabled == true)
                                    this.btnRefresh_Click(this.btnRefresh, new EventArgs());
                                break;
                            }

                        case (Keys.P):
                            {
                                if (this.btnPrint.Enabled == true)
                                    this.btnPrint_Click(this.btnPrint, new EventArgs());
                                break;
                            }

                        case (Keys.X):
                            {
                                if (this.btnClose.Enabled == true)
                                    this.btnClose_Click(this.btnClose, new EventArgs());
                                break;
                            }
                    }
            }

			// Return control to the base function  
			return base.ProcessDialogKey(keyData);
		}
		#endregion

		private void btnClose_Click(object sender, EventArgs e)
		{
			this.Close();
		}
		private void _FrmTab_SelectedIndexChanged(object sender, EventArgs e)
		{
			base.OnTabIndexChanged(e);
			OnFormTabIndexChange();
			SetToolbar();
        }
	}
}