namespace NavLib.Controls
{
    partial class NaveoFormTab
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NaveoFormTab));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.stb_Message = new System.Windows.Forms.ToolStripStatusLabel();
            this.bndSrc = new System.Windows.Forms.BindingSource(this.components);
            this.bndNav = new System.Windows.Forms.BindingNavigator(this.components);
            this.tbAddItem = new System.Windows.Forms.ToolStripButton();
            this.bndCount = new System.Windows.Forms.ToolStripLabel();
            this.btnDelete = new System.Windows.Forms.ToolStripButton();
            this.btnMoveFirst = new System.Windows.Forms.ToolStripButton();
            this.btnMovePrevious = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bndPosition = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnMoveNext = new System.Windows.Forms.ToolStripButton();
            this.btnNoveLast = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tbEditItem = new System.Windows.Forms.ToolStripButton();
            this.btnSave = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnRefresh = new System.Windows.Forms.ToolStripButton();
            this.btnCopy = new System.Windows.Forms.ToolStripButton();
            this.btnPrint = new System.Windows.Forms.ToolStripButton();
            this.btnClose = new System.Windows.Forms.ToolStripButton();
            this.tbTextMessage = new System.Windows.Forms.ToolStripTextBox();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bndSrc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bndNav)).BeginInit();
            this.bndNav.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stb_Message});
            this.statusStrip1.Location = new System.Drawing.Point(0, 410);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(706, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // stb_Message
            // 
            this.stb_Message.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.stb_Message.Name = "stb_Message";
            this.stb_Message.Size = new System.Drawing.Size(691, 17);
            this.stb_Message.Spring = true;
            // 
            // bndNav
            // 
            this.bndNav.AddNewItem = this.tbAddItem;
            this.bndNav.CountItem = this.bndCount;
            this.bndNav.DeleteItem = this.btnDelete;
            this.bndNav.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnMoveFirst,
            this.btnMovePrevious,
            this.bindingNavigatorSeparator,
            this.bndPosition,
            this.bndCount,
            this.bindingNavigatorSeparator1,
            this.btnMoveNext,
            this.btnNoveLast,
            this.bindingNavigatorSeparator2,
            this.tbAddItem,
            this.tbEditItem,
            this.btnDelete,
            this.btnSave,
            this.toolStripSeparator1,
            this.btnRefresh,
            this.btnCopy,
            this.btnPrint,
            this.btnClose,
            this.tbTextMessage});
            this.bndNav.Location = new System.Drawing.Point(0, 0);
            this.bndNav.MoveFirstItem = this.btnMoveFirst;
            this.bndNav.MoveLastItem = this.btnNoveLast;
            this.bndNav.MoveNextItem = this.btnMoveNext;
            this.bndNav.MovePreviousItem = this.btnMovePrevious;
            this.bndNav.Name = "bndNav";
            this.bndNav.PositionItem = this.bndPosition;
            this.bndNav.Size = new System.Drawing.Size(706, 25);
            this.bndNav.TabIndex = 2;
            this.bndNav.Text = "bindingNavigator1";
            // 
            // tbAddItem
            // 
            this.tbAddItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbAddItem.Image = ((System.Drawing.Image)(resources.GetObject("tbAddItem.Image")));
            this.tbAddItem.Name = "tbAddItem";
            this.tbAddItem.RightToLeftAutoMirrorImage = true;
            this.tbAddItem.Size = new System.Drawing.Size(23, 22);
            this.tbAddItem.Text = "Add new";
            this.tbAddItem.ToolTipText = "Ctrl N";
            this.tbAddItem.Click += new System.EventHandler(this.tbAddItem_Click);
            // 
            // bndCount
            // 
            this.bndCount.Name = "bndCount";
            this.bndCount.Size = new System.Drawing.Size(36, 22);
            this.bndCount.Text = "of {0}";
            this.bndCount.ToolTipText = "Total number of items";
            // 
            // btnDelete
            // 
            this.btnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.RightToLeftAutoMirrorImage = true;
            this.btnDelete.Size = new System.Drawing.Size(23, 22);
            this.btnDelete.Text = "Delete";
            this.btnDelete.ToolTipText = "Ctrl D";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnMoveFirst
            // 
            this.btnMoveFirst.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnMoveFirst.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveFirst.Image")));
            this.btnMoveFirst.Name = "btnMoveFirst";
            this.btnMoveFirst.RightToLeftAutoMirrorImage = true;
            this.btnMoveFirst.Size = new System.Drawing.Size(23, 22);
            this.btnMoveFirst.Text = "Move first";
            this.btnMoveFirst.Click += new System.EventHandler(this.btnMoveFirst_Click);
            // 
            // btnMovePrevious
            // 
            this.btnMovePrevious.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnMovePrevious.Image = ((System.Drawing.Image)(resources.GetObject("btnMovePrevious.Image")));
            this.btnMovePrevious.Name = "btnMovePrevious";
            this.btnMovePrevious.RightToLeftAutoMirrorImage = true;
            this.btnMovePrevious.Size = new System.Drawing.Size(23, 22);
            this.btnMovePrevious.Text = "Move previous";
            this.btnMovePrevious.Click += new System.EventHandler(this.btnMovePrevious_Click);
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bndPosition
            // 
            this.bndPosition.AccessibleName = "Position";
            this.bndPosition.AutoSize = false;
            this.bndPosition.Name = "bndPosition";
            this.bndPosition.Size = new System.Drawing.Size(50, 21);
            this.bndPosition.Text = "0";
            this.bndPosition.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnMoveNext
            // 
            this.btnMoveNext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnMoveNext.Image = ((System.Drawing.Image)(resources.GetObject("btnMoveNext.Image")));
            this.btnMoveNext.Name = "btnMoveNext";
            this.btnMoveNext.RightToLeftAutoMirrorImage = true;
            this.btnMoveNext.Size = new System.Drawing.Size(23, 22);
            this.btnMoveNext.Text = "Move next";
            this.btnMoveNext.Click += new System.EventHandler(this.btnMoveNext_Click);
            // 
            // btnNoveLast
            // 
            this.btnNoveLast.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnNoveLast.Image = ((System.Drawing.Image)(resources.GetObject("btnNoveLast.Image")));
            this.btnNoveLast.Name = "btnNoveLast";
            this.btnNoveLast.RightToLeftAutoMirrorImage = true;
            this.btnNoveLast.Size = new System.Drawing.Size(23, 22);
            this.btnNoveLast.Text = "Move last";
            this.btnNoveLast.Click += new System.EventHandler(this.btnNoveLast_Click);
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // tbEditItem
            // 
            this.tbEditItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbEditItem.Image = ((System.Drawing.Image)(resources.GetObject("tbEditItem.Image")));
            this.tbEditItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbEditItem.Name = "tbEditItem";
            this.tbEditItem.Size = new System.Drawing.Size(23, 22);
            this.tbEditItem.Text = "toolStripButton1";
            this.tbEditItem.Click += new System.EventHandler(this.tbEditItem_Click);
            // 
            // btnSave
            // 
            this.btnSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(23, 22);
            this.btnSave.Text = "toolStripButton1";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnRefresh
            // 
            this.btnRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnRefresh.Image = ((System.Drawing.Image)(resources.GetObject("btnRefresh.Image")));
            this.btnRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(23, 22);
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnCopy
            // 
            this.btnCopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCopy.Image = ((System.Drawing.Image)(resources.GetObject("btnCopy.Image")));
            this.btnCopy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(23, 22);
            this.btnCopy.Text = "toolStripButton1";
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrint.Image = ((System.Drawing.Image)(resources.GetObject("btnPrint.Image")));
            this.btnPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(23, 22);
            this.btnPrint.Text = "toolStripButton2";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnClose
            // 
            this.btnClose.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(23, 22);
            this.btnClose.Text = "Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // tbTextMessage
            // 
            this.tbTextMessage.Name = "tbTextMessage";
            this.tbTextMessage.Size = new System.Drawing.Size(100, 25);
            // 
            // NaveoFormTab
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(706, 432);
            this.Controls.Add(this.bndNav);
            this.Controls.Add(this.statusStrip1);
            this.Name = "NaveoFormTab";
            this.Text = "NaveoFormTab";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bndSrc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bndNav)).EndInit();
            this.bndNav.ResumeLayout(false);
            this.bndNav.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource bndSrc;
        protected System.Windows.Forms.StatusStrip statusStrip1;
        protected System.Windows.Forms.ToolStripStatusLabel stb_Message;
        private System.Windows.Forms.ToolStripButton tbAddItem;
        private System.Windows.Forms.ToolStripLabel bndCount;
        private System.Windows.Forms.ToolStripButton btnDelete;
        private System.Windows.Forms.ToolStripButton btnMoveFirst;
        private System.Windows.Forms.ToolStripButton btnMovePrevious;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bndPosition;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton btnMoveNext;
        private System.Windows.Forms.ToolStripButton btnNoveLast;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton tbEditItem;
        private System.Windows.Forms.ToolStripButton btnSave;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnRefresh;
        private System.Windows.Forms.ToolStripButton btnCopy;
        private System.Windows.Forms.ToolStripButton btnPrint;
        private System.Windows.Forms.ToolStripButton btnClose;
        private System.Windows.Forms.ToolStripTextBox tbTextMessage;
        protected System.Windows.Forms.BindingNavigator bndNav;
    }
}