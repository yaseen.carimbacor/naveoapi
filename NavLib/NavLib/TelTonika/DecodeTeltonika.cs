﻿using NavLib.Globals;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Teltonika.DataParser.Client;

namespace NavLib.TelTonika
{
    public class DecodeTeltonika
    {
        public List<TeltonikaReport> tReports;

        public DecodeTeltonika(Byte[] bytPacketData)
        {
            tReports = new List<TeltonikaReport>();
            Byte[] bytData = bytPacketData;

            //var bytes = Utils.StringToBytes("000000000000007B080200000161DB7F5290002242CC8BF3FA342E00000000000000F00C06EF01F00050011503C800450206B50000B600004230D7180000430F9A440000000000000161DB7F2F68002242CC8BF3FA342E00000000000000000C06EF01F00150011503C800450206B50000B600004230D7180000430F9944000000000200005040");
            //bytPacketData = bytes;

            DecodeTcpData(bytPacketData);
        }

        private Teltonika.DataParser.Client.Data DecodeTcpData(byte[] bytes)
        {
            DataReader parser = new DataReader(bytes);
            var dataList = new[]
            {
                parser.ReadData(4, DataType.Preamble),
                parser.ReadData(4, DataType.AvlDataArrayLength),
                DecodeAvlData(parser),
                parser.ReadData(4, DataType.Crc)
            };

            var result = new Teltonika.DataParser.Client.Data("TCP AVL Data Packet")
            {
                SubItems = dataList
            };

            return result;
        }
        private Teltonika.DataParser.Client.Data DecodeAvlData(DataReader parser)
        {
            var codecIdData = parser.ReadData(1, DataType.CodecId);
            var countData = parser.ReadData(1, DataType.AvlDataCount);

            var avlDataList = new List<Teltonika.DataParser.Client.Data> { codecIdData, countData };
            var result = new Teltonika.DataParser.Client.Data("Data");

            for (var a = 0; a < Int32.Parse(countData.Value); a++)
            {
                var items = new PacketDecoder(parser, codecIdData.Value);
                tReports.Add(new TelTonika.TeltonikaReport(items));
            }
            return result;
        }

        private Teltonika.DataParser.Client.Data OldDecodeAvlData(DataReader parser)
        {
            var codecIdData = parser.ReadData(1, DataType.CodecId);
            var countData = parser.ReadData(1, DataType.AvlDataCount);

            var avlDataList = new List<Teltonika.DataParser.Client.Data> { codecIdData, countData };
            var result = new Teltonika.DataParser.Client.Data("Data");

            for (var a = 0; a < Int32.Parse(countData.Value); a++)
            {
                var items = new PacketDecoder(parser, codecIdData.Value);
                tReports.Add(new TelTonika.TeltonikaReport(items));

                if (String.IsNullOrEmpty(""))
                    continue;

                foreach (var v in items.AvlItems)
                {

                }
                foreach (var v in items.GpsElementSubItems)
                {

                }
                foreach (var v in items.IoElementSubItems)
                {

                }


                // Fake data to show nodes in tree
                var startArraySegment = items.AvlItems.First().ArraySegment;
                var avlData = new Teltonika.DataParser.Client.Data("AVL Data")
                {
                    ArraySegment =
                        new ArraySegment<byte>(startArraySegment.Array, startArraySegment.Offset,
                            items.IoElementSubItems.Last().ArraySegment.Offset - startArraySegment.Offset + 1)
                };

                var gpsElement = new Teltonika.DataParser.Client.Data("GPS Element")
                {
                    ArraySegment =
                        new ArraySegment<byte>(startArraySegment.Array, items.GpsElementSubItems.First().ArraySegment.Offset, 15)
                };

                var ioElement = new Teltonika.DataParser.Client.Data("I/O Element")
                {
                    ArraySegment =
                        new ArraySegment<byte>(startArraySegment.Array, items.IoElementSubItems.First().ArraySegment.Offset,
                            items.IoElementSubItems.Last().ArraySegment.Offset - items.IoElementSubItems.First().ArraySegment.Offset + 1)
                };

                gpsElement.SubItems = items.GpsElementSubItems.ToArray();
                ioElement.SubItems = items.IoElementSubItems.ToArray();

                var avlDataSubItems = new List<Teltonika.DataParser.Client.Data>();
                avlDataSubItems.AddRange(items.AvlItems);
                avlDataSubItems.Add(gpsElement);
                avlDataSubItems.Add(ioElement);

                avlData.SubItems = avlDataSubItems.ToArray();
                avlDataList.Add(avlData);
            }
            avlDataList.Add(parser.ReadData(1, DataType.AvlDataCount));
            result.SubItems = avlDataList.ToArray();
            result.ArraySegment = new ArraySegment<byte>(codecIdData.ArraySegment.Array, codecIdData.ArraySegment.Offset,
                avlDataList.Last().ArraySegment.Offset - codecIdData.ArraySegment.Offset + 1);

            return result;
        }
    }
}
