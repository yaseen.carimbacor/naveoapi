﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Teltonika.DataParser.Client;

namespace NavLib.TelTonika
{
    public class TeltonikaReport
    {
        PacketDecoder tReport;
        System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();


        #region constants
        const String ignition = "239";
        const String movement = "240";
        const String gpsFix = "69";
        const String dataMode = "80";
        const String gsmSignalStrength = "21";
        const String SleepMode = "200";
        const String SIMICCID1 = "11";
        const String SIMICCID2 = "14";
        const String TripOdometer="199";
        const String TotalOdometer = "16";
        const String Din1 = "1";
        const String Ain1 = "9";
        const String ExtVol = "66";
        const String Dout1 = "179";
        const String FuelUsedGPS = "12";
        const String AverageFuelUse = "13";
        #endregion

        public TeltonikaReport(PacketDecoder report)
        {
            tReport = report;
        }

        #region GpsElementSubItems
        public Double GetLongitude()
        {
            Double d = 0.0F;
            foreach (var v in tReport.GpsElementSubItems)
            {
                if (v.Name == "Longitude")
                    if (Double.TryParse(v.Value, out d))
                    {
                        Double x = Math.Abs(d);
                        String s = x.ToString();
                        String sDiv = 1.ToString();
                        for (int i = 0; i < s.Length - 2; i++)
                            sDiv += 0.ToString();
                        Double div = Convert.ToDouble(sDiv);

                        return d / div;
                    }
            }

            return d;
        }
        public Double GetLatitude()
        {
            Double d = 0.0F;
            foreach (var v in tReport.GpsElementSubItems)
            {
                if (v.Name == "Latitude")
                    if (Double.TryParse(v.Value, out d))
                    {
                        Double x = Math.Abs(d);
                        String s = x.ToString();
                        String sDiv = 1.ToString();
                        for (int i = 0; i < s.Length - 2; i++)
                            sDiv += 0.ToString();
                        Double div = Convert.ToDouble(sDiv);

                        return d / div;
                    }
            }

            return d;
        }
        public int GetAltitude()
        {
            int i = 0;
            foreach (var v in tReport.GpsElementSubItems)
            {
                if (v.Name == "Altitude")
                    if (Int32.TryParse(v.Value, out i))
                        return i;
            }

            return i;
        }
        public int GetAngle()
        {
            int i = 0;
            foreach (var v in tReport.GpsElementSubItems)
            {
                if (v.Name == "Angle")
                    if (Int32.TryParse(v.Value, out i))
                        return i;
            }

            return i;
        }
        public int GetNoOfSalettiles()
        {
            int i = 0;
            foreach (var v in tReport.GpsElementSubItems)
            {
                if (v.Name == "Satellites")
                    if (Int32.TryParse(v.Value, out i))
                        return i;
            }

            return i;
        }
        public int GetSpeed()
        {
            int i = 0;
            foreach (var v in tReport.GpsElementSubItems)
            {
                if (v.Name == "Speed")
                    if (Int32.TryParse(v.Value, out i))
                        return i;
            }

            return i;
        }
        #endregion

        #region AvlItems
        public DateTime GetTimeStamp()
        {
            DateTime d = DateTime.UtcNow;
            foreach (var v in tReport.AvlItems)
            {
                if (v.Name == "Timestamp")
                {
                    DateTime.TryParse(v.Value, out d);
                    return d;
                }
            }

            return d;
        }
        public int GetPriority()
        {
            int i = 0;
            foreach (var v in tReport.AvlItems)
            {
                if (v.Name == "Priority")
                    if (Int32.TryParse(v.Value, out i))
                        return i;
            }

            return i;
        }
        #endregion

        #region IoElementSubItems
        class IOElements
        {
            public Boolean bIDFound { get; set; }
            public int iIDfound { get; set; }
        }
        IOElements GetIOElements(String sValue)
        {
            Boolean bIDFound = false;
            int iIDfound = -1;
            foreach (var v in tReport.IoElementSubItems)
            {
                iIDfound++;

                if (v.Name == "ID")
                    if (v.Value == sValue)
                    {
                        bIDFound = true;
                        break;
                    }
            }

            IOElements io = new TelTonika.TeltonikaReport.IOElements();
            io.bIDFound = bIDFound;
            io.iIDfound = iIDfound + 1;

            return io;
        }

        public String GetLogReason()
        {
            String sLogReason = String.Empty;
            Boolean bContinue = true;
            foreach (var v in tReport.IoElementSubItems)
            {
                if (v.Name == "Event ID")
                {
                    switch (v.Value)
                    {
                        case ignition:
                            if (GetIgnition())
                                sLogReason += "Ignition change (Ignition)";
                            else
                                sLogReason += "Ignition change (IgnOff) (Ignition)";

                            bContinue = false;
                            break;
                    }
                }
            }

            if (bContinue)
            {
                if (GetMovement())
                    sLogReason += "Movement (Heading)";

                sLogReason += "; DataMode : " + GetDataMode();
            }
            return sLogReason;
        }
        public Boolean GetIgnition()
        {
            Boolean bResult = false;

            IOElements io = GetIOElements(ignition);
            if (io.bIDFound)
            {
                String s = tReport.IoElementSubItems[io.iIDfound].Value;
                if (s == "1")
                    bResult = true;
            }
            return bResult;
        }
        public Boolean GetMovement()
        {
            Boolean bResult = false;

            IOElements io = GetIOElements(movement);
            if (io.bIDFound)
            {
                String s = tReport.IoElementSubItems[io.iIDfound].Value;
                if (s == "1")
                    bResult = true;
            }
            return bResult;
        }
        public Boolean GetSatelliteValid()
        {
            Boolean bResult = false;

            IOElements io = GetIOElements(gpsFix);
            if (io.bIDFound)
            {
                String s = tReport.IoElementSubItems[io.iIDfound].Value;
                //0 - OFF 1 - ONwithfix 2 - ONwithoutfix 3 - Insleepstate
                if (s == "1")
                    bResult = true;
            }
            return bResult;
        }
        public String GetDataMode()
        {
            String sResult = "Not Found";
            IOElements io = GetIOElements(dataMode);
            if (io.bIDFound)
            {
                String s = tReport.IoElementSubItems[io.iIDfound].Value;
                switch (s)
                {
                    //0–HomeOnStop 1–HomeOnMoving 2–RoamingOnStop 3–RoamingOnMoving 4 - UnknownOnStop 5–UnknownOnMoving
                    case "0":
                        sResult = "HomeOnStop";
                        break;
                    case "1":
                        sResult = "HomeOnMoving";
                        break;
                    case "2":
                        sResult = "RoamingOnStop";
                        break;
                    case "3":
                        sResult = "RoamingOnMoving";
                        break;
                    case "4":
                        sResult = "UnknownOnStop";
                        break;
                    case "5":
                        sResult = "UnknownOnMoving";
                        break;
                }
            }
            return sResult;
        }
        public int GetGSMSignalStrength()
        {
            int i = 0;
            IOElements io = GetIOElements(gsmSignalStrength);
            if (io.bIDFound)
            {
                String s = tReport.IoElementSubItems[io.iIDfound].Value;
                int.TryParse(s, out i);
            }
            return i;
        }
        public String GetSleepMode()
        {
            String sResult = "Not Found";
            IOElements io = GetIOElements(dataMode);
            if (io.bIDFound)
            {
                String s = tReport.IoElementSubItems[io.iIDfound].Value;
                switch (s)
                {
                    //0–NoSleep;1–GPSSleep;2–DeepSleep;3-OnlineSlee
                    case "0":
                        sResult = "NoSleep";
                        break;
                    case "1":
                        sResult = "GPSSleep";
                        break;
                    case "2":
                        sResult = "DeepSleep";
                        break;
                    case "3":
                        sResult = "OnlineSleep";
                        break;
                }
            }
            return sResult;
        }
        public String GetSimIMSI()
        {
            String sResult = String.Empty;
            IOElements io = GetIOElements(SIMICCID1);
            if (io.bIDFound)
                sResult = tReport.IoElementSubItems[io.iIDfound].Value;

            io = GetIOElements(SIMICCID2);
            if (io.bIDFound)
                sResult += tReport.IoElementSubItems[io.iIDfound].Value;

            return sResult;
        }
        public int GetTripOdometerInMeters()
        {
            int i = 0;
            IOElements io = GetIOElements(TripOdometer);
            if (io.bIDFound)
            {
                String s = tReport.IoElementSubItems[io.iIDfound].Value;
                int.TryParse(s, out i);
            }
            return i;
        }
        public int GetTotalOdometerInMeters()
        {
            int i = 0;
            IOElements io = GetIOElements(TotalOdometer);
            if (io.bIDFound)
            {
                String s = tReport.IoElementSubItems[io.iIDfound].Value;
                int.TryParse(s, out i);
            }
            return i;
        }
        public Boolean GetDigitalInput1()
        {
            Boolean bResult = false;

            IOElements io = GetIOElements(Din1);
            if (io.bIDFound)
            {
                String s = tReport.IoElementSubItems[io.iIDfound].Value;
                if (s == "1")
                    bResult = true;
            }
            return bResult;
        }
        public int GetAnalogInput1()
        {
            int i = 0;
            IOElements io = GetIOElements(Ain1);
            if (io.bIDFound)
            {
                String s = tReport.IoElementSubItems[io.iIDfound].Value;
                int.TryParse(s, out i);
            }
            return i;
        }
        public int GetExternalVoltage()
        {
            int i = 0;
            IOElements io = GetIOElements(ExtVol);
            if (io.bIDFound)
            {
                String s = tReport.IoElementSubItems[io.iIDfound].Value;
                int.TryParse(s, out i);
            }
            return i;
        }
        public Boolean GetDigitalOutput1()
        {
            Boolean bResult = false;

            IOElements io = GetIOElements(Dout1);
            if (io.bIDFound)
            {
                String s = tReport.IoElementSubItems[io.iIDfound].Value;
                if (s == "1")
                    bResult = true;
            }
            return bResult;
        }
        public int GetFuelUsedGPSinMiliLiters()
        {
            int i = 0;
            IOElements io = GetIOElements(FuelUsedGPS);
            if (io.bIDFound)
            {
                String s = tReport.IoElementSubItems[io.iIDfound].Value;
                int.TryParse(s, out i);
            }
            return i;
        }
        public Double GetAverageFuelUsedPerKM()
        {
            //AverageFuelusein(Litersx100)/100km
            Double d = 0;
            IOElements io = GetIOElements(AverageFuelUse);
            if (io.bIDFound)
            {
                String s = tReport.IoElementSubItems[io.iIDfound].Value;
                Double.TryParse(s, out d);
                d = d / 100;
            }
            return d;
        }

        public int Test()
        {
            int i = 0;
            IOElements io = GetIOElements(gsmSignalStrength);
            if (io.bIDFound)
            {
                String s = tReport.IoElementSubItems[io.iIDfound].Value;
                int.TryParse(s, out i);
            }
            return i;
        }
        #endregion
    }
}
