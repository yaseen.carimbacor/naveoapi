﻿using System;
using System.Collections.Generic;
using System.Text;
using NavLib.Std;
using NavLib.Globals;

namespace NavLib.Xexun
{
    public class DecodeXexun
    {
        //120912040642,+2302562812,GPRMC,080642.134,A,2013.5458,S,05730.3696,E,0.89,0.00,120912,,,A*72,F,, imei:861785009769708,03,371.8,F:4.32V,1,137,52998,617,10,0072,56AF
        //120912040642,                                                             yymmddhhmmss                        0
        //+2302562812,                                                              Authorized no                       1
        //GPRMC,080642.134,A,2013.5458,S,05730.3696,E,0.89,0.00,120912,,,A*72,      Standard GPRMC message              2 - 14
        //F                                                                         F = Full GPS Signal, L = Low        15
        //,,                                                                        Help me = SOS                       16
        //imei:861785009769708,                                                     IMEI                                17    
        //03,                                                                       Satellite numbers                   18
        //371.8,                                                                    Altitude                            19
        //F:4.32V,                                                                  Battery Voltage                     20
        //1,                                                                        0=Not charged, 1=Charged            21
        //137,                                                                      Length of GPRS String               22
        //52998,                                                                    Checksum                            23
        //617,                                                                      Mobile Country code                 24
        //10,                                                                       Mobile Network Code                 25
        //0072,                                                                     Location Area Code                  26
        //56AF                                                                      Cell ID                             27
        int oSatNo = 3;
        int oIMEI = 17;

        public List<DecodeGPRMC> decodeGPRMC;
        String[] MsgArray;
        String[] MsgArrayInFull;
        System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();

        public DecodeXexun(Byte[] bytPacketData)
        {
            Byte[] MyPacketData = Utils.CompressBytWorking(bytPacketData);
            String Msg = Encoding.ASCII.GetString(MyPacketData);

            nfi.NumberDecimalSeparator = ".";
            MsgArray = Msg.Split(',');
            MsgArrayInFull = MsgArray;

            MyPacketData = bytPacketData;

            decodeGPRMC = new List<DecodeGPRMC>();
            int ReportStart = 2;

            String[] aReport = new String[15];
            Array.Copy(MsgArray, ReportStart, aReport, 0, 15);
            decodeGPRMC.Add(new DecodeGPRMC(aReport));
        }

        public String getImei()
        {
            String[] g=MsgArray[oIMEI].Split(':');
            return g[1];
        }

        public int GetSatelliteNos()
        {
            return Convert.ToInt16(MsgArray[oSatNo]);
        }
    }
}