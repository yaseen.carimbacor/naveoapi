﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace NavLib.AT100
{
    public class clsAT100Report
    {
        // The Default length of our C protcol packet.
        const int M_AT100_C_REPORT_LEN = 32;
        
        // These constants are offsets into a report packet and are consistent with Protocol C
        const int M_OFFSET_PACKET_SQN = 0; 
        const int M_OFFSET_LAT_START = 1; 
        const int M_OFFSET_LAT_END = 4; 
        const int M_OFFSET_LONG_START = 5; 
        const int M_OFFSET_LONG_END = 8;
        const int M_OFFSET_TIME_START = 9;
        const int M_OFFSET_TIME_END = 12; 
        const int M_OFFSET_SPEED = 13;
        const int M_OFFSET_DEGREES = 14;
        const int M_OFFSET_ALTITUDE = 15;
        const int M_OFFSET_REASON_CODE_MSB = 16;
        const int M_OFFSET_REASON_CODE_LSB = 17;
        const int M_OFFSET_STATUS = 18;
        const int M_OFFSET_GEO_FENCE_EVENT = 19;
        const int M_OFFSET_DIGITALS = 20;
        const int M_OFFSET_DIGITALS_CHANGE = 21;
        const int M_OFFSET_ADC2 = 22; 
        const int M_OFFSET_ADC1 = 23; 
        const int M_OFFSET_BATTERY = 24;
        const int M_OFFSET_EXTERNAL_INPUT_VOLTAGE = 25;
        const int M_OFFSET_MAX_JOURNEY_SPEED = 26;
        const int M_OFFSET_MAX_DECELERATION = 27;
        const int M_OFFSET_MAX_ACCELERATION = 28; 
        const int M_OFFSET_JOURNEY_DIST_TRAVELLED_START = 29;
        const int M_OFFSET_JOURNEY_DIST_TRAVELLED_END = 30;
        const int M_OFFSET_JOURNEY_IDLE_SECONDS_START = 31;
        const int M_OFFSET_JOURNEY_IDLE_SECONDS_END = 32;
        const int M_OFFSET_REZ_START = 33;
        const int M_OFFSET_REZ_END = 42;
        
        // Individual Bit Field Descriptors 
        /* Constants for BYTE M_OFFSET_REASON_CODE_LSB ( Reason Code Least Significant Byte ) */
        const byte M_REASON_CODE_TIME_INT_ELAPSED = 1;
        const byte M_REASON_CODE_DIST_TRAVELLED_EXCEEDED = 2;
        const byte M_REASON_CODE_POS_ON_DEMAND = 4;
        const byte M_REASON_CODE_GEO_FENCE = 8;
        const byte M_REASON_CODE_PANIC_SWITCH_ACTIVATED = 16;
        const byte M_REASON_CODE_EXT_INPUT_EVENT = 32;
        const byte M_REASON_CODE_JOURNEY_START = 64;
        const byte M_REASON_CODE_JOURNEY_STOP = 128;
        /* Constants for BYTE M_OFFSET_REASON_CODE_MSB ( Reason Code Most Significant Byte ) */
        const byte M_REASON_CODE_HEADING_CHANGE = 1;
        const byte M_REASON_CODE_LOW_BATTERY = 2;
        const byte M_REASON_CODE_EXT_POWER_EVENT = 4;
        const byte M_REASON_CODE_IDLING_START = 8;
        const byte M_REASON_CODE_IDLING_END = 16;
        const byte M_REASON_CODE_IDLING_ONGOING = 32;
        const byte M_REASON_CODE_ALARM = 64;
        const byte M_REASON_CODE_SPEED_OVER_THRESHOLD = 128;
        /* Constants for Status byte ( M_OFFSET_STATUS ) */
        const byte M_STATUS_IGNITION_ON = 1;
        const byte M_STATUS_POWER_ON_OR_RESTART = 2;
        const byte M_STATUS_GPS_TIMEOUT = 4;
        const byte M_STATUS_NETWORK_ROAMING = 8;
        const byte M_STATUS_REPORTS_TO_FOLLOW = 16;
        const byte M_STATUS_STORED_REPORT = 32;
        const byte M_STATUS_EXTENSION = 64;
        const byte M_STATUS_SERIAL_DEVICE_READ_ERROR = 128;


        // Class Level Data.
        Byte[] m_bytReportData;
        bool m_blnDataLengthCorrect;

        // Constructor. Only one. We need the packet data.
        public clsAT100Report(ArraySegment<byte> bytPacketData)
        {

            int intCounter = 0;
            if (bytPacketData.Count == M_AT100_C_REPORT_LEN)
            {
                for (intCounter = bytPacketData.Offset; intCounter <= bytPacketData.Count + bytPacketData.Offset; intCounter++)
                {
                    Array.Resize<Byte>(ref m_bytReportData, (null == m_bytReportData) ? 1 : m_bytReportData.Length + 1);
                    m_bytReportData[m_bytReportData.Length - 1] = bytPacketData.Array[intCounter];

                }
                m_blnDataLengthCorrect = true; 
            }
        }

        private bool IsValidReport()
        {
            return m_blnDataLengthCorrect; 
        }

        public byte GetPacketSequenceNumber()
        {
            // The Packet Sequence number rolls over at 255 and is stored in byte 10 of the packet;
            byte uintRet = 0;
            if (this.IsValidReport())
            {
                // Just read and return the byte
                uintRet = m_bytReportData[M_OFFSET_PACKET_SQN];
            }
            return uintRet;
        }


        public double GetLatitude()
        {
            // The Lattitude is stored in bytes 11,12,13 and 14 and is the degrees * 1,000,000
            double dblRet = 0;
            ulong lngLat;
            lngLat = BuildLong(M_OFFSET_LAT_START, M_OFFSET_LAT_END);
            dblRet = (double)(int)lngLat / (double)1000000;
            return dblRet;
        }

        public double GetLongitude()
        {
            // The Lattitude is stored in bytes 15,16,17 and 18 and is the degrees * 1,000,000
            double dblRet = 0;
            ulong lngLong;
            lngLong = BuildLong(M_OFFSET_LONG_START, M_OFFSET_LONG_END);
            dblRet = (double)(int)lngLong / (double)1000000;
            return dblRet;
        }

        public String GetTime()
        {
            // The date / time is recorded in seconds since the 6th of January 1980 at 00:00:00 
            // That number of second is stored in the bytes M_OFFSET_TIME_START to M_OFFSET_TIME_END

            // Initilise the DateTime to Jan 6th 1980 at midnight
            DateTime datBaseDate = new DateTime(1980, 1, 6, 0, 0, 0);
            ulong ulongSecondsSince = 0;
            if (this.IsValidReport())
            {
                ulongSecondsSince = BuildLong(M_OFFSET_TIME_START, M_OFFSET_TIME_END);
                datBaseDate = datBaseDate.AddSeconds(ulongSecondsSince);
                // I have chosen to reutrn the formatted string here
                // You can of course return the DateTime and format it when you use it.
            }
            return datBaseDate.ToString("dd MMM yyyy @ HH:mm:ss");
        }


        public int GetSpeedInKmh()
        {
            // The speed is stored in byte M_OFFSET_SPEED and is in Km/h divided by 2
            int intRet = 0;

            if (this.IsValidReport())
            {
                intRet = m_bytReportData[M_OFFSET_SPEED];
                intRet *= 2;
            }

            return intRet;
        }

        public int GetHeadingInDegrees()
        {
            // The speed is stored in byte 24 and is in degrees divided by 2
            int intRet = 0;

            if (this.IsValidReport())
            {
                intRet = m_bytReportData[M_OFFSET_DEGREES];
                intRet *= 2;
            }

            return intRet;
        }

        public int GetAltitudeInMeters()
        {
            // The speed is stored in byte M_OFFSET_ALTITUDE and is in meters divided by 20
            int intRet = 0;

            if (this.IsValidReport())
            {
                intRet = m_bytReportData[M_OFFSET_ALTITUDE];
                intRet *= 20;
            }

            return intRet;
        }

        public bool Reason_TimedIntervalElapsed()
        {
            // This bit is bit 0 of byte 27 ( Reason Code LSB )             
            bool blnRet = false;
            if (this.IsValidReport())
            {
                if ((m_bytReportData[M_OFFSET_REASON_CODE_LSB] & M_REASON_CODE_TIME_INT_ELAPSED) > 0)
                {
                    blnRet = true;
                }
            }
            return blnRet;
        }


        public bool Reason_DistanceTravelledExceeded()
        {
            // This bit is bit 1 of Reason Code LSB 
            bool blnRet = false;
            if (this.IsValidReport())
            {
                if ((m_bytReportData[M_OFFSET_REASON_CODE_LSB] & M_REASON_CODE_DIST_TRAVELLED_EXCEEDED) > 0)
                {
                    blnRet = true;
                }
            }
            return blnRet;
        }


        public bool Reason_PosOnDemand()
        {
            // This bit is bit 2 of Reason Code LSB 
            bool blnRet = false;
            if (this.IsValidReport())
            {
                if ((m_bytReportData[M_OFFSET_REASON_CODE_LSB] & M_REASON_CODE_POS_ON_DEMAND) > 0)
                {
                    blnRet = true;
                }
            }
            return blnRet;
        }


        public bool Reason_GeoFence()
        {
            // This bit is bit 3 of Reason Code LSB 
            bool blnRet = false;
            if (this.IsValidReport())
            {
                if ((m_bytReportData[M_OFFSET_REASON_CODE_LSB] & M_REASON_CODE_GEO_FENCE) > 0)
                {
                    blnRet = true;
                }
            }
            return blnRet;
        }

        public bool Reason_PanicSwitchActivated()
        {
            // This bit is bit 4 of Reason Code LSB 
            bool blnRet = false;
            if (this.IsValidReport())
            {
                if ((m_bytReportData[M_OFFSET_REASON_CODE_LSB] & M_REASON_CODE_PANIC_SWITCH_ACTIVATED) > 0)
                {
                    blnRet = true;
                }
            }
            return blnRet;
        }

        public bool Reason_ExtInputEvent()
        {
            // This bit is bit 5 of Reason Code LSB 
            bool blnRet = false;
            if (this.IsValidReport())
            {
                if ((m_bytReportData[M_OFFSET_REASON_CODE_LSB] & M_REASON_CODE_EXT_INPUT_EVENT) > 0)
                {
                    blnRet = true;
                }
            }
            return blnRet;
        }

        public bool Reason_JourneyStart()
        {
            // This bit is bit 6 of Reason Code LSB 
            bool blnRet = false;
            if (this.IsValidReport())
            {
                if ((m_bytReportData[M_OFFSET_REASON_CODE_LSB] & M_REASON_CODE_JOURNEY_START) > 0)
                {
                    blnRet = true;
                }
            }
            return blnRet;
        }

        public bool Reason_JourneyStop()
        {
            // This bit is bit 7 of Reason Code LSB 
            bool blnRet = false;
            if (this.IsValidReport())
            {
                if ((m_bytReportData[M_OFFSET_REASON_CODE_LSB] & M_REASON_CODE_JOURNEY_STOP) > 0)
                {
                    blnRet = true;
                }
            }
            return blnRet;
        }


        public bool Reason_HeadingChange()
        {
            // This bit is bit 0 of Reason Code MSB
            bool blnRet = false;
            if (this.IsValidReport())
            {
                if ((m_bytReportData[M_OFFSET_REASON_CODE_MSB] & M_REASON_CODE_HEADING_CHANGE) > 0)
                {
                    blnRet = true;
                }
            }
            return blnRet;
        }

        public bool Reason_LowBattery()
        {
            // This bit is bit 1 of Reason Code MSB
            bool blnRet = false;
            if (this.IsValidReport())
            {
                if ((m_bytReportData[M_OFFSET_REASON_CODE_MSB] & M_REASON_CODE_LOW_BATTERY) > 0)
                {
                    blnRet = true;
                }
            }
            return blnRet;
        }

        public bool Reason_ExtPowerEvent()
        {
            // This bit is bit 2 of Reason Code MSB
            bool blnRet = false;
            if (this.IsValidReport())
            {
                if ((m_bytReportData[M_OFFSET_REASON_CODE_MSB] & M_REASON_CODE_EXT_POWER_EVENT) > 0)
                {
                    blnRet = true;
                }
            }
            return blnRet;
        }


        public bool Reason_IdlingStarting()
        {
            // This bit is bit 3 of Reason Code MSB
            bool blnRet = false;
            if (this.IsValidReport())
            {
                if ((m_bytReportData[M_OFFSET_REASON_CODE_MSB] & M_REASON_CODE_IDLING_START) > 0)
                {
                    blnRet = true;
                }
            }
            return blnRet;
        }


        public bool Reason_IdlingEnd()
        {
            // This bit is bit 4 of Reason Code MSB
            bool blnRet = false;
            if (this.IsValidReport())
            {
                if ((m_bytReportData[M_OFFSET_REASON_CODE_MSB] & M_REASON_CODE_IDLING_END) > 0)
                {
                    blnRet = true;
                }
            }
            return blnRet;
        }

        public bool Reason_IdlingOnGoing()
        {
            // This bit is bit 5 of Reason Code MSB
            bool blnRet = false;
            if (this.IsValidReport())
            {
                if ((m_bytReportData[M_OFFSET_REASON_CODE_MSB] & M_REASON_CODE_IDLING_ONGOING) > 0)
                {
                    blnRet = true;
                }
            }
            return blnRet;
        }


        public bool Reason_Alarm()
        {
            // This bit is bit 6 of Reason Code MSB
            bool blnRet = false;
            if (this.IsValidReport())
            {
                if ((m_bytReportData[M_OFFSET_REASON_CODE_MSB] & M_REASON_CODE_ALARM) > 0)
                {
                    blnRet = true;
                }
            }
            return blnRet;
        }

        public bool Reason_SpeedOverThreshold()
        {
            // This bit is bit 7 of Reason Code MSB
            bool blnRet = false;
            if (this.IsValidReport())
            {
                if ((m_bytReportData[M_OFFSET_REASON_CODE_MSB] & M_REASON_CODE_SPEED_OVER_THRESHOLD) > 0)
                {
                    blnRet = true;
                }
            }
            return blnRet;
        }


        /* Status Byte Decoding */
        public bool Status_IgnitionOn()
        {
            // This bit is bit 0 of Status Byte
            bool blnRet = false;
            if (this.IsValidReport())
            {
                if ((m_bytReportData[M_OFFSET_STATUS] & M_STATUS_IGNITION_ON) > 0)
                {
                    blnRet = true;
                }
            }
            return blnRet;
        }

        public bool Status_PowerOnOrRestart()
        {
            // This bit is bit 1 of Status Byte
            bool blnRet = false;
            if (this.IsValidReport())
            {
                if ((m_bytReportData[M_OFFSET_STATUS] & M_STATUS_POWER_ON_OR_RESTART) > 0)
                {
                    blnRet = true;
                }
            }
            return blnRet;
        }


        public bool Status_GPSTimeOut()
        {
            // This bit is bit 2 of Status Byte
            bool blnRet = false;
            if (this.IsValidReport())
            {
                if ((m_bytReportData[M_OFFSET_STATUS] & M_STATUS_GPS_TIMEOUT) > 0)
                {
                    blnRet = true;
                }
            }
            return blnRet;
        }

        public bool Status_NetworkRoaming()
        {
            // This bit is bit 3 of Status Byte
            bool blnRet = false;
            if (this.IsValidReport())
            {
                if ((m_bytReportData[M_OFFSET_STATUS] & M_STATUS_NETWORK_ROAMING) > 0)
                {
                    blnRet = true;
                }
            }
            return blnRet;
        }

        public bool Status_ReportsToFollow()
        {
            // This bit is bit 4 of Status Byte
            bool blnRet = false;
            if (this.IsValidReport())
            {
                if ((m_bytReportData[M_OFFSET_STATUS] & M_STATUS_REPORTS_TO_FOLLOW) > 0)
                {
                    blnRet = true;
                }
            }
            return blnRet;
        }


        public bool Status_StoredReport()
        {
            // This bit is bit 5 of Status Byte
            bool blnRet = false;
            if (this.IsValidReport())
            {
                if ((m_bytReportData[M_OFFSET_STATUS] & M_STATUS_STORED_REPORT) > 0)
                {
                    blnRet = true;
                }
            }
            return blnRet;
        }

        public bool Status_Extension()
        {
            // This bit is bit 6 of Status Byte
            bool blnRet = false;
            if (this.IsValidReport())
            {
                if ((m_bytReportData[M_OFFSET_STATUS] & M_STATUS_EXTENSION) > 0)
                {
                    blnRet = true;
                }
            }
            return blnRet;
        }

        public bool Status_SerialDeviceReadError()
        {
            // This bit is bit 7 of Status Byte
            bool blnRet = false;
            if (this.IsValidReport())
            {
                if ((m_bytReportData[M_OFFSET_STATUS] & M_STATUS_SERIAL_DEVICE_READ_ERROR) > 0)
                {
                    blnRet = true;
                }
            }
            return blnRet;
        }


        public bool GeoFenceEntred()
        {
            /* The GeoFence Byte is byte 29 and has the format 
             * Bit 7 = Geo Fence Entered if 1 or 0 if "Left Geo Fence" 
             * Bit 0..6 Geo Fence Index that was entered or left. Values > 0 are valid indexes
             */
            bool blnRet = false;
            if (this.IsValidReport())
            {
                if ((m_bytReportData[M_OFFSET_GEO_FENCE_EVENT] & 0x80) > 0)
                {
                    blnRet = true;
                }
            }
            return blnRet;
        }

        public byte GetGeoFenceIndex()
        {
            // The Geofence Byte is stored at location M_OFFSET_GEO_FENCE_EVENT. See bool GeoFenceEntred for full description.
            // The low order 7 bits store the GeoFence index that was affected. Value 127 = congestion zone
            Byte bytGeoFenceIndex = 0;
            if (this.IsValidReport())
            {
                bytGeoFenceIndex = (byte)(m_bytReportData[M_OFFSET_GEO_FENCE_EVENT] & 0x7f);
            }

            // GeoFence Index 0 is represented as value 1.
            // Except for Fence 127 where the Congestion Zone is indicated.
            if (bytGeoFenceIndex != 127)
            {
                bytGeoFenceIndex--;
            }
            return bytGeoFenceIndex;
        }

        public bool GeoFenceCongestionZone()
        {
            // Just test GeoFenceIndex for == 127 to determine if this is a congestion zone event. 
            // Note we need not test the status of the data here as the function that is called does that.
            bool blnReturn = false;
            if ((this.GetGeoFenceIndex() & 0x7f) == 0x7f)
            {
                blnReturn = true;
            }
            return blnReturn;
        }



        public byte DigitalStatus()
        {
            // The digital input and output statuses are stored at byte M_OFFSET_DIGITALS.
            // There are 6 digital inputs and 2 digital outputs.
            byte bytDigitalStatus = 0;
            if (this.IsValidReport())
            {
                bytDigitalStatus = m_bytReportData[M_OFFSET_DIGITALS];
            }
            return bytDigitalStatus;
        }


        public byte DigitalChangeStatus()
        {
            // The change in digital inputs and outputs are stored in byte M_OFFSET_DIGITALS_CHANGE.
            // There are 6 digital inputs and 2 digital outputs.
            byte bytDigitalChange = 0;
            if (this.IsValidReport())
            {
                bytDigitalChange = m_bytReportData[M_OFFSET_DIGITALS_CHANGE];
            }
            return bytDigitalChange;
        }


        public byte GetADC2()
        {
            // ADC2 is byte M_OFFSET_ADC2 of the packet
            byte bytOut = 0;
            if (this.IsValidReport())
            {
                bytOut = m_bytReportData[M_OFFSET_ADC2];
            }
            return bytOut;
        }

        public byte GetADC1()
        {
            // ADC1 is byte M_OFFSET_ADC1 of the packet
            byte bytOut = 0;
            if (this.IsValidReport())
            {
                bytOut = m_bytReportData[M_OFFSET_ADC1];
            }
            return bytOut;
        }


        public byte GetBatteryPercentage()
        {
            // Battery Percentage is stored in byte M_OFFSET_BATTERY.
            byte bytOut = 0;
            if (this.IsValidReport())
            {
                bytOut = m_bytReportData[M_OFFSET_BATTERY];
            }
            return bytOut;
        }

        public float GetExternalInputVoltage()
        {
            // The exteral voltage stored in byte M_OFFSET_EXTERNAL_INPUT_VOLTAGE needs to be * 0.2 to get the correct value.
            float sngOut = 0;
            if (this.IsValidReport())
            {
                sngOut = ((float)m_bytReportData[M_OFFSET_EXTERNAL_INPUT_VOLTAGE]) * (float)0.2;
            }

            return sngOut;
        }



        public int GetMaxJourneySpeed()
        {
            // The Maximum Journey Speed is in km/h and is divided by 2. Stored in byte M_OFFSET_MAX_JOURNEY_SPEED
            int intOut = 0;
            if (this.IsValidReport())
            {
                intOut = m_bytReportData[M_OFFSET_MAX_JOURNEY_SPEED] * 2;
            }
            return intOut;
        }


        public int GetMaxDeceleration()
        {
            // The Maximum Deceleration is in ms2 and is stored in byte M_OFFSET_MAX_DECELERATION
            int intOut = 0;
            if (this.IsValidReport())
            {
                intOut = m_bytReportData[M_OFFSET_MAX_DECELERATION];
            }
            return intOut;
        }

        public int GetMaxAcceleration()
        {
            // The Maximum Acceleration is in ms2 and is stored in byte M_OFFSET_MAX_ACCELERATION
            int intOut = 0;
            if (this.IsValidReport())
            {
                intOut = m_bytReportData[M_OFFSET_MAX_ACCELERATION];
            }
            return intOut;
        }


        public float GetJourneyDistanceTravelled()
        {
            // The total distance travelled in this journey is stored in km and is multiplied by 10. Mul by 0.1 to get it back. 
            // Stored in bytes  M_OFFSET_JOURNEY_DIST_TRAVELLED_START and M_OFFSET_JOURNEY_DIST_TRAVELLED_END
            float sngOut = 0;
            if (this.IsValidReport())
            {
                sngOut = ((float)BuildLong(M_OFFSET_JOURNEY_DIST_TRAVELLED_START, M_OFFSET_JOURNEY_DIST_TRAVELLED_END)) * (float)0.1;
            }

            return sngOut;
        }

        public int GetJourneyIdleSeconds()
        {
            // How many seconds were Idle Seconds during the journey? The information is stored between bytes
            // M_OFFSET_JOURNEY_IDLE_SECONDS_START and M_OFFSET_JOURNEY_IDLE_SECONDS_END
            int intOut = 0;
            if (this.IsValidReport())
            {
                intOut = (int)BuildLong(M_OFFSET_JOURNEY_IDLE_SECONDS_START, M_OFFSET_JOURNEY_IDLE_SECONDS_END);

            }
            return intOut;
        }



        private ulong BuildLong(int intStartByte, int intEndByte)
        {
            ulong lngOut = 0;
            // Check if this is sane and within all the bounds!?
            if ((intEndByte > intStartByte) && (intEndByte <= m_bytReportData.GetUpperBound(0))) 
            {
                // For each needed byte.
                while (intStartByte < intEndByte)
                {
                    // Add the byte
                    lngOut += m_bytReportData[intStartByte++];
                    // Shift it into place.
                    lngOut <<= 8;
                }
                // And add the last byte ( we don't have to shift after this
                lngOut += m_bytReportData[intStartByte];
            }
            return lngOut;
        }

    }
}
