using System;
using System.Collections.Generic;
using System.Text;

namespace NavLib.AT100
{
    public class AT100Report
    {
        const int Offset_ReportLength = 33;

        //Header >  - 10
        const int Offset_PacketSeqNo = 10 - 10;
        const int Offset_LatStart = 11 - 10;
        const int Offset_LatEnd = 14 - 10;
        const int Offset_LonStart = 15 - 10;
        const int Offset_LonEnd = 18 - 10;
        const int Offset_TimeStart = 19 - 10;
        const int Offset_TimeEnd = 22 - 10;
        const int Offset_Speed = 23 - 10;
        const int Offset_Heading = 24 - 10;
        const int Offset_Altitude = 25 - 10;
        const int Offset_ReasonCodeEnd = 26 - 10;
        const int Offset_ReasonCodeStart = 27 - 10;
        const int Offset_StatusCode = 28 - 10;
        const int Offset_GeofenceEvent = 29 - 10;
        const int Offset_Digitals = 30 - 10;
        const int Offset_DigitalChange = 31 - 10;
        const int Offset_ADC2 = 32 - 10;
        const int Offset_ADC1 = 33 - 10;
        const int Offset_BatteryLevel = 34 - 10;
        const int Offset_ExternalInputVoltage = 35 - 10;
        const int Offset_MaxJourneySpeed = 36 - 10;
        const int Offset_MaxDeceleration = 37 - 10;
        const int Offset_MaxAcceleration = 38 - 10;
        const int Offset_JourneyDistanceTravelledStart = 39 - 10;
        const int Offset_JourneyDistanceTravelledEnd = 40 - 10;
        const int Offset_JourneyIdleTimeStart = 41 - 10;
        const int Offset_JourneyIdleTimeEnd = 42 - 10;
        byte[] MyPacketData;

        public AT100Report(byte[] bytPacketData)
        {
            MyPacketData = bytPacketData;
        }

        public Boolean IsReportValid()
        {
            Boolean bResult = false;
            if (MyPacketData.Length == Offset_ReportLength)
                bResult = true;

            return bResult;
        }
        private String ToBinary(Int64 Decimal)
        {
            //Int64 DecimalValue = Convert.ToInt64(MyPacketData[30].ToString());
            //Int64 BinToDec = Convert.ToInt64(Convert.ToInt64("1010", 2));

            // Declare a few variables we're going to need
            Int64 BinaryHolder;
            char[] BinaryArray;
            String BinaryResult = "";

            while (Decimal > 0)
            {
                BinaryHolder = Decimal % 2;
                BinaryResult += BinaryHolder;
                Decimal = Decimal / 2;
            }

            // The algoritm gives us the binary number in reverse order (mirrored)
            // We store it in an array so that we can reverse it back to normal
            BinaryArray = BinaryResult.ToCharArray();
            Array.Reverse(BinaryArray);
            BinaryResult = new string(BinaryArray);

            while (BinaryResult.Length < 8)
                BinaryResult = "0" + BinaryResult;

            return BinaryResult;
        }
        private Boolean GetBitValue(int Offset_Code, int pos)
        {
            Boolean bResult = false;
            String ReasonCode = ToBinary((Int64)MyPacketData[Offset_Code]);
            if (ReasonCode.Substring(pos, 1) == "1")
                bResult = true;
            return bResult;
        }
        private ulong BuildLong(int intStartByte, int intEndByte)
        {
            ulong lngOut = 0;
            // Check if this is sane and within all the bounds!?
            if ((intEndByte > intStartByte) && (intEndByte <= MyPacketData.GetUpperBound(0)))
            {
                // For each needed byte.
                while (intStartByte < intEndByte)
                {
                    // Add the byte
                    lngOut += MyPacketData[intStartByte++];
                    // Shift it into place.
                    //Reza... arithmetic shift
                    //If only 1 byte is needed, return directly
                    //However, if more that 1 byte is needed, Shift is needed
                    // i = 5 --> 0000 0101
                    // i <<= 3 
                    //   ans 1 = 40 > 0000 0101 000 > 0010 1000
                    lngOut <<= 8;
                }
                // And add the last byte ( we don't have to shift after this
                lngOut += MyPacketData[intStartByte];
            }
            return lngOut;
        }

        public int GetPacketSeqNo()
        {
            return (int)MyPacketData[Offset_PacketSeqNo];
        }
        public double GetLatitude()
        {
            // The Lattitude is stored in bytes 11,12,13 and 14 and is the degrees * 1,000,000
            double dblRet = (double)(int)BuildLong(Offset_LatStart, Offset_LatEnd) / (double)1000000;
            return dblRet;
        }
        public double GetLongitude()
        {
            // The Longitude is stored in bytes 15,16,17 and 18 and is the degrees * 1,000,000
            double dblRet = (double)(int)BuildLong(Offset_LonStart, Offset_LonEnd) / (double)1000000;
            return dblRet;
        }
        public DateTime GetDate()
        {
            // The date / time is recorded in seconds since the 6th of January 1980 at 00:00:00 
            // That number of second is stored in the bytes Offset_TimeStart to Offset_TimeEnd
            // return dt.ToString("dd MMM yyyy @ HH:mm:ss")
            DateTime dt = new DateTime(1980, 1, 6, 0, 0, 0);
            dt = dt.AddSeconds(BuildLong(Offset_TimeStart, Offset_TimeEnd));
            return dt;
        }
        public int GetSpeedInKmh()
        {
            // The speed is stored in byte Offset_Speed and is in Km/h divided by 2
            return (int)MyPacketData[Offset_Speed] * 2;
        }
        public int GetHeadingInDegrees()
        {
            // The Heading is stored in byte 24 and is in degrees divided by 2
            return (int)MyPacketData[Offset_Heading] * 2;
        }
        public int GetAltitudeInMeters()
        {
            // The Altitude is stored in byte Offset_Heading and is in meters divided by 20
            return (int)MyPacketData[Offset_Heading] * 20;
        }
        public String GetReasons()
        {
            String strResult = String.Empty;
            if (GetBitValue(Offset_ReasonCodeStart, 7))
                strResult += "TimeIntervalElasped(???)";
            if (GetBitValue(Offset_ReasonCodeStart, 6))
                strResult += "DistanceTravelledExceeded(Heading)";
            if (GetBitValue(Offset_ReasonCodeStart, 5))
                strResult += "Polled(???)";
            if (GetBitValue(Offset_ReasonCodeStart, 4))
                strResult += "Geofence(???)";
            if (GetBitValue(Offset_ReasonCodeStart, 3))
                strResult += "Panic(Aux2)";
            if (GetBitValue(Offset_ReasonCodeStart, 2))
                strResult += "ExternalInput(???)";
            if (GetBitValue(Offset_ReasonCodeStart, 1))
                strResult += "JourneyStart(Ignition)";
            if (GetBitValue(Offset_ReasonCodeStart, 0))
                strResult += "JourneyEnd(Ignition)";

            if (GetBitValue(Offset_ReasonCodeEnd, 7))
                strResult += "Degrees(Heading)";
            if (GetBitValue(Offset_ReasonCodeEnd, 6))
                strResult += "LowBattery(???)";
            if (GetBitValue(Offset_ReasonCodeEnd, 5))
                strResult += "ExternalPower(???)";
            if (GetBitValue(Offset_ReasonCodeEnd, 4))
                strResult += "IdlingStart(BeginOfStop)";
            if (GetBitValue(Offset_ReasonCodeEnd, 3))
                strResult += "IdlingEnd(EndOfStop)";
            if (GetBitValue(Offset_ReasonCodeEnd, 2))
                strResult += "IdlingOnGoing(Tiime)";
            if (GetBitValue(Offset_ReasonCodeEnd, 1))
                strResult += "TowingAlarm(???)";
            if (GetBitValue(Offset_ReasonCodeEnd, 0))
                strResult += "SpeedOverthreshold(???)";

            if (GetDigitalStatusChangeInput1())//Digital 1 = Ignition sense
                strResult += "DigitalInputChange1(IgnChange)";
            if (GetDigitalStatusChangeInput2())//Digital 2 = Panic sense
                strResult += "DigitalInputChange2(Aux2)";
            if (GetDigitalStatusChangeInput3())//Digital 3 
                strResult += "DigitalInputChange3(Aux3)";
            if (GetDigitalStatusChangeInput4())//Digital 4 
                strResult += "DigitalInputChange4(Aux4)";
            if (GetDigitalStatusChangeInput5())//Digital 5 
                strResult += "DigitalInputChange5(Aux1)";
            if (GetDigitalStatusChangeInput6())//Digital 6 
                strResult += "DigitalInputChange6(???)";
            if (GetDigitalStatusChangeOutput1())//Digital Output 1 
                strResult += "DigitalOutputChange1(???)";
            if (GetDigitalStatusChangeOutput2())//Digital Output 2 
                strResult += "DigitalOutputChange2(???)";

            strResult += " >>> StatusCode >>>";
            if (GetStatusIgnitionOn())
                strResult += "(IgnOn)";
            else
                strResult += "(IgnOff)";
            if (GetStatusPowerOn())
                strResult += "StatusPowerOn(Reset)";
            if (GetStatusGPSTimeOut())
                strResult += "StatusGPSTimeOut(GpsShutdown)";
            if (GetStatusNetworkRoaming())
                strResult += "StatusNetworkRoaming";
            if (GetStatusReportsToFollow())
                strResult += "StatusReportsToFollow";
            if (GetStatusStoredReport())
                strResult += "StoredReport";
            if (GetStatusExtensionDataFieldPresent())
                strResult += "StatusExtensionDataFieldPresent";
            if (GetStatusSerialDeviceReadError())
                strResult += "StatusSerialDeviceReadError";

            return strResult;
        }
        public Boolean GetStatusIgnitionOn()
        {
            return GetBitValue(Offset_StatusCode, 7);
        }
        public Boolean GetStatusPowerOn()
        {
            return GetBitValue(Offset_StatusCode, 6);
        }
        public Boolean GetStatusGPSTimeOut()
        {
            return GetBitValue(Offset_StatusCode, 5);
        }
        public Boolean GetStatusNetworkRoaming()
        {
            return GetBitValue(Offset_StatusCode, 4);
        }
        public Boolean GetStatusReportsToFollow()
        {
            return GetBitValue(Offset_StatusCode, 3);
        }
        public Boolean GetStatusStoredReport()
        {
            // 1 > True = Stored
            // 0 > False = Live
            return GetBitValue(Offset_StatusCode, 2);
        }
        public Boolean GetStatusExtensionDataFieldPresent()
        {
            return GetBitValue(Offset_StatusCode, 1);
        }
        public Boolean GetStatusSerialDeviceReadError()
        {
            return GetBitValue(Offset_StatusCode, 0);
        }
        public bool GeoFenceEntred()
        {
            /* The GeoFence Byte is byte 29 and has the format 
             * Bit 7 = Geo Fence Entered if 1 or 0 if "Left Geo Fence" 
             * Bit 0..6 Geo Fence Index that was entered or left. Values > 0 are valid indexes
             */
            return GetBitValue(Offset_GeofenceEvent, 0);
        }
        public byte GetGeoFenceIndex()
        {
            // The Geofence Byte is stored at location 29. See bool GeoFenceEntred for full description.
            // The low order 7 bits store the GeoFence index that was affected. Value 127 = congestion zone

            //Byte bytGeoFenceIndex = 0;
            //if (this.IsValidReport())
            //{
            //    bytGeoFenceIndex = (byte)(m_bytReportData[M_OFFSET_GEO_FENCE_EVENT] & 0x7f);
            //}

            // GeoFence Index 0 is represented as value 1.
            // Except for Fence 127 where the Congestion Zone is indicated.
            //if (bytGeoFenceIndex != 127)
            //{
            //    bytGeoFenceIndex--;
            //}
            //return bytGeoFenceIndex;

            return new byte();
        }
        public bool GeoFenceCongestionZone()
        {
            // Just test GeoFenceIndex for == 127 to determine if this is a congestion zone event. 
            // Note we need not test the status of the data here as the function that is called does that.
            //bool blnReturn = false;
            //if ((this.GetGeoFenceIndex() & 0x7f) == 0x7f)
            //{
            //    blnReturn = true;
            //}
            return false;
        }
        public int GetDigitals()
        { return (int)MyPacketData[Offset_Digitals]; }
        public Boolean GetDigitalInput1()
        {
            return GetBitValue(Offset_Digitals, 7);
        }
        public Boolean GetDigitalInput2()
        {
            return GetBitValue(Offset_Digitals, 6);
        }
        public Boolean GetDigitalInput3()
        {
            return GetBitValue(Offset_Digitals, 5);
        }
        public Boolean GetDigitalInput4()
        {
            return GetBitValue(Offset_Digitals, 4);
        }
        public Boolean GetDigitalInput5()
        {
            return GetBitValue(Offset_Digitals, 3);
        }
        public Boolean GetDigitalInput6()
        {
            return GetBitValue(Offset_Digitals, 2);
        }
        public Boolean GetDigitalOutput1()
        {
            return GetBitValue(Offset_Digitals, 1);
        }
        public Boolean GetDigitalOutput2()
        {
            return GetBitValue(Offset_Digitals, 0);
        }
        public Boolean GetDigitalStatusChangeInput1()
        {
            return GetBitValue(Offset_DigitalChange, 7);
        }
        public Boolean GetDigitalStatusChangeInput2()
        {
            return GetBitValue(Offset_DigitalChange, 6);
        }
        public Boolean GetDigitalStatusChangeInput3()
        {
            return GetBitValue(Offset_DigitalChange, 5);
        }
        public Boolean GetDigitalStatusChangeInput4()
        {
            return GetBitValue(Offset_DigitalChange, 4);
        }
        public Boolean GetDigitalStatusChangeInput5()
        {
            return GetBitValue(Offset_DigitalChange, 3);
        }
        public Boolean GetDigitalStatusChangeInput6()
        {
            return GetBitValue(Offset_DigitalChange, 2);
        }
        public Boolean GetDigitalStatusChangeOutput1()
        {
            return GetBitValue(Offset_DigitalChange, 1);
        }
        public Boolean GetDigitalStatusChangeOutput2()
        {
            return GetBitValue(Offset_DigitalChange, 0);
        }
        public float GetADC()
        {
            return (float)BuildLong(Offset_ADC2, Offset_ADC1);
        }
        public byte GetADC1()
        {
            // ADC1 is byte M_OFFSET_ADC1 of the packet
            return MyPacketData[Offset_ADC1];
        }
        public byte GetADC2()
        {
            // ADC2 is byte M_OFFSET_ADC2 of the packet
            return MyPacketData[Offset_ADC2];
        }
        public int GetBatteryLevelPercentage()
        {
            return (int)MyPacketData[Offset_BatteryLevel];
        }
        public float GetExternalInputVoltage()
        {
            return (float)MyPacketData[Offset_ExternalInputVoltage] * (float)0.2;
        }
        public int GetMaxJourneySpeed()
        {
            // The Maximum Journey Speed is in km/h and is divided by 2.
            return (int)MyPacketData[Offset_MaxJourneySpeed] * 2;
        }
        public int GetMaxDeceleration()
        {
            // The Maximum Deceleration is in m/s2
            return (int)MyPacketData[Offset_MaxDeceleration];
        }
        public int GetMaxAcceleration()
        {
            // The Maximum Acceleration is in m/s2
            return (int)MyPacketData[Offset_MaxAcceleration];
        }
        public float GetJourneyDistanceTravelled()
        {
            // The total distance travelled in this journey is stored in km and is multiplied by 10. Mul by 0.1 to get it back. 
            float distance = (float)BuildLong(Offset_JourneyDistanceTravelledStart, Offset_JourneyDistanceTravelledEnd);
            return distance * 0.1F;
        }
        public int GetJourneyIdleSeconds()
        {
            return (int)BuildLong(Offset_JourneyIdleTimeStart, Offset_JourneyIdleTimeEnd);
        }
    }
}
