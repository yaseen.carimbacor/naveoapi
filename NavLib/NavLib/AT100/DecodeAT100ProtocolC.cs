using System;
using System.Collections.Generic;
using System.Text;

namespace NavLib.AT100
{
    public class DecodeAT100ProtocolC
    {
        // Header
        const int Offset_Protocol = 0;
        const int Offset_PacketLengthStart = 1;
        const int Offset_PacketLengthEnd = 2;
        const int Offset_IMEIStart = 3;
        const int Offset_IMEIEnd = 9;
        const int Offset_UnitIDStart = 7;
        const int Offset_UnitIDEnd = 9;
        // Then reports...
        const int Offset_ReportLength = 33;

        byte[] MyPacketData = new byte[1024];
        public List<AT100Report> AT100Reports;

        public Boolean IsPacketDataValid = false;

        public DecodeAT100ProtocolC(byte[] bytPacketData)
        {
            // 1 Header
            // 8 Reports
            MyPacketData = bytPacketData;
            IsPacketDataValid = true;

            AT100Reports = new List<AT100Report>();
            int ReportStart = Offset_UnitIDEnd + 1;
            int ReportEnd = ReportStart + Offset_ReportLength - 1;

            while (ReportEnd <= MyPacketData.GetUpperBound(0))
            {
                byte[] b = new byte[Offset_ReportLength];
                Array.Copy(MyPacketData, ReportStart, b, 0, Offset_ReportLength);
                AT100Reports.Add(new AT100Report(b));

                ReportStart += Offset_ReportLength;
                ReportEnd = ReportStart + Offset_ReportLength - 1;
            }
            //uint x = ReadCheckSum(bytPacketData);
            //uint y = GenerateCheckSum(bytPacketData);
        }

        private String ToBinary(Int64 Decimal)
        {
            //Int64 DecimalValue = Convert.ToInt64(MyPacketData[30].ToString());
            //Int64 BinToDec = Convert.ToInt64(Convert.ToInt64("1010", 2));

            // Declare a few variables we're going to need
            Int64 BinaryHolder;
            char[] BinaryArray;
            String BinaryResult = "";

            while (Decimal > 0)
            {
                BinaryHolder = Decimal % 2;
                BinaryResult += BinaryHolder;
                Decimal = Decimal / 2;
            }

            // The algoritm gives us the binary number in reverse order (mirrored)
            // We store it in an array so that we can reverse it back to normal
            BinaryArray = BinaryResult.ToCharArray();
            Array.Reverse(BinaryArray);
            BinaryResult = new string(BinaryArray);

            while (BinaryResult.Length < 8)
                BinaryResult = "0" + BinaryResult;

            return BinaryResult;
        }
        private Boolean GetBitValue(int Offset_Code, int pos)
        {
            Boolean bResult = false;
            String ReasonCode = ToBinary((Int64)MyPacketData[Offset_Code]);
            if (ReasonCode.Substring(pos, 1) == "1")
                bResult = true;
            return bResult;
        }
        private ulong BuildLong(int intStartByte, int intEndByte)
        {
            ulong lngOut = 0;
            // Check if this is sane and within all the bounds!?
            if ((intEndByte > intStartByte) && (intEndByte <= MyPacketData.GetUpperBound(0)))
            {
                // For each needed byte.
                while (intStartByte < intEndByte)
                {
                    // Add the byte
                    lngOut += MyPacketData[intStartByte++];
                    // Shift it into place.
                    //Reza... arithmetic shift
                    //If only 1 byte is needed, return directly
                    //However, if more that 1 byte is needed, Shift is needed
                    // i = 5 --> 0000 0101
                    // i <<= 3 
                    //   ans 1 = 40 > 0000 0101 000 > 0010 1000
                    lngOut <<= 8;
                }
                // And add the last byte ( we don't have to shift after this
                lngOut += MyPacketData[intStartByte];
            }
            return lngOut;
        }

        #region Header
        public String GetProtocolType()
        {
            String strReadable = Encoding.ASCII.GetString(MyPacketData, Offset_Protocol, 1);
            String x = MyPacketData[Offset_Protocol].ToString("X");
            String i = MyPacketData[Offset_Protocol].ToString();
            return strReadable;
        }
        public int GetPacketLength()
        {
            ulong u = BuildLong(Offset_PacketLengthStart, Offset_PacketLengthEnd);
            return (int)u;
        }
        public String GetUnitIMEI()
        {
            // The TAC/FAC is stored in bytes 3,4,5 and 6 
            // The MSN (GetUnitID) is the serial number that will change per device. This is stored in bytes 7,8 and 9
            ulong u = BuildLong(Offset_IMEIStart, Offset_IMEIEnd);
            return u.ToString();
        }
        public String GetUnitID()
        {
            ulong u = BuildLong(Offset_UnitIDStart, Offset_UnitIDEnd);
            return u.ToString();
        }
        #endregion

        // This routine takes a byte array and calcualtes the CRC16 of it.
        // All data minus the last 2 bytes are taken into account.
        private uint GenerateCheckSum(byte[] bytPacketDatam)
        {
            uint[] laCrc16LookUpTable = 
            {
                0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301, 0x03C0, 0x0280, 0xC241, 0xC601, 0x06C0, 0x0780, 0xC741, 0x0500,
                0xC5C1, 0xC481, 0x0440, 0xCC01, 0x0CC0, 0x0D80, 0xCD41, 0x0F00, 0xCFC1, 0xCE81, 0x0E40, 0x0A00, 0xCAC1,
                0xCB81, 0x0B40, 0xC901, 0x09C0, 0x0880, 0xC841, 0xD801, 0x18C0, 0x1980, 0xD941, 0x1B00, 0xDBC1, 0xDA81,
                0x1A40, 0x1E00, 0xDEC1, 0xDF81, 0x1F40, 0xDD01, 0x1DC0, 0x1C80, 0xDC41, 0x1400, 0xD4C1, 0xD581, 0x1540, 
                0xD701, 0x17C0, 0x1680, 0xD641, 0xD201, 0x12C0, 0x1380, 0xD341, 0x1100, 0xD1C1, 0xD081, 0x1040, 0xF001,
                0x30C0, 0x3180, 0xF141, 0x3300, 0xF3C1, 0xF281, 0x3240, 0x3600, 0xF6C1, 0xF781, 0x3740, 0xF501, 0x35C0,
                0x3480, 0xF441, 0x3C00, 0xFCC1, 0xFD81, 0x3D40, 0xFF01, 0x3FC0, 0x3E80, 0xFE41, 0xFA01, 0x3AC0, 0x3B80, 
                0xFB41, 0x3900, 0xF9C1, 0xF881, 0x3840, 0x2800, 0xE8C1, 0xE981, 0x2940, 0xEB01, 0x2BC0, 0x2A80, 0xEA41, 
                0xEE01, 0x2EC0, 0x2F80, 0xEF41, 0x2D00, 0xEDC1, 0xEC81, 0x2C40, 0xE401, 0x24C0, 0x2580, 0xE541, 0x2700, 
                0xE7C1, 0xE681, 0x2640, 0x2200, 0xE2C1, 0xE381, 0x2340, 0xE101, 0x21C0, 0x2080, 0xE041, 0xA001, 0x60C0, 
                0x6180, 0xA141, 0x6300, 0xA3C1, 0xA281, 0x6240, 0x6600, 0xA6C1, 0xA781, 0x6740, 0xA501, 0x65C0, 0x6480, 
                0xA441, 0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01, 0x6FC0, 0x6E80, 0xAE41, 0xAA01, 0x6AC0, 0x6B80, 0xAB41, 
                0x6900, 0xA9C1, 0xA881, 0x6840, 0x7800, 0xB8C1, 0xB981, 0x7940, 0xBB01, 0x7BC0, 0x7A80, 0xBA41, 0xBE01, 
                0x7EC0, 0x7F80, 0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40, 0xB401, 0x74C0, 0x7580, 0xB541, 0x7700, 0xB7C1, 
                0xB681, 0x7640, 0x7200, 0xB2C1, 0xB381, 0x7340, 0xB101, 0x71C0, 0x7080, 0xB041, 0x5000, 0x90C1, 0x9181, 
                0x5140, 0x9301, 0x53C0, 0x5280, 0x9241, 0x9601, 0x56C0, 0x5780, 0x9741, 0x5500, 0x95C1, 0x9481, 0x5440, 
                0x9C01, 0x5CC0, 0x5D80, 0x9D41, 0x5F00, 0x9FC1, 0x9E81, 0x5E40, 0x5A00, 0x9AC1, 0x9B81, 0x5B40, 0x9901, 
                0x59C0, 0x5880, 0x9841, 0x8801, 0x48C0, 0x4980, 0x8941, 0x4B00, 0x8BC1, 0x8A81, 0x4A40, 0x4E00, 0x8EC1, 
                0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80, 0x8C41, 0x4400, 0x84C1, 0x8581, 0x4540, 0x8701, 0x47C0, 0x4680, 
                0x8641, 0x8201, 0x42C0, 0x4380, 0x8341, 0x4100, 0x81C1, 0x8081, 0x4040
            };

            uint llcrc = 0xFFFF; 	/* high byte of CRC initialized */
            uint llChar;
            int lnPos;

            for (lnPos = 0; lnPos < bytPacketDatam.GetUpperBound(0) - 1; lnPos++)
            {
                llChar = (llcrc ^ bytPacketDatam[lnPos]) & 0x00ff;
                llChar = laCrc16LookUpTable[llChar];
                llcrc = (llcrc >> 8) ^ llChar;
            }
            return (llcrc);
        }
        private uint ReadCheckSum(byte[] bytPacketDatam)
        {
            uint intCheckSum = bytPacketDatam[GetPacketLength() - 1];
            intCheckSum  <<= 8;
            intCheckSum += bytPacketDatam[GetPacketLength()];
            return intCheckSum;
        }
    }
}
        //public DecodeAT100ProtocolC(byte[] bytPacketData)
        //{
        //    // 1 Header
        //    // 8 Reports
        //    MyPacketData = bytPacketData;
        //    IsPacketDataValid = true;

        //    AT100Reports = new List<AT100Report>();
        //    int ReportStart = Offset_UnitIDEnd + 1;
        //    int ReportEnd = ReportStart + Offset_ReportLength;

        //    while (ReportEnd <= MyPacketData.GetUpperBound(0))
        //    {
        //        byte[] b = new byte[Offset_ReportLength];
        //        Array.Copy(MyPacketData, ReportStart, b, 0, Offset_ReportLength);
                
        //        AT100Reports.Add(new AT100Report(b));

        //        ReportStart += Offset_ReportLength;
        //        ReportEnd = ReportStart + Offset_ReportLength;
        //    }
        //    //uint x = ReadCheckSum(bytPacketData);
        //    //uint y = GenerateCheckSum(bytPacketData);
        //}
