﻿using System;
using System.Collections.Generic;
using System.Text;
using NavLib.Globals;

namespace NavLib.AT100
{
    public class DecodeAT100IMSI
    {
        String Msg;

        public DecodeAT100IMSI(Byte[] bytPacketData)
        {
            Byte[] MyPacketData = Utils.CompressBytWorking(bytPacketData);
            Msg = Encoding.ASCII.GetString(MyPacketData);
        }

        public String GetUnitID()
        {
            //IMEI: 352929010039240 \rIMSI: 617100109171594
            String IMEI = Msg.Substring(6, 15);
            String strUnitID = IMEI.Substring(8, 7);
            if (strUnitID.Substring(0, 1) == "0")
                strUnitID = strUnitID.Substring(1, 6);
            if (strUnitID.Substring(0, 1) == "0")
                strUnitID = strUnitID.Substring(1, 5);
            if (strUnitID.Substring(0, 1) == "0")
                strUnitID = strUnitID.Substring(1, 4);
            if (strUnitID.Substring(0, 1) == "0")
                strUnitID = strUnitID.Substring(1, 3);
            if (strUnitID.Substring(0, 1) == "0")
                strUnitID = strUnitID.Substring(1, 2);
            if (strUnitID.Substring(0, 1) == "0")
                strUnitID = strUnitID.Substring(1, 1);
            return strUnitID;
        }
        public String GetIMSI()
        {
            //IMEI: 352929010039240 \rIMSI: 617100109171594
            String str = Msg.Substring(28, 15);
            return str;
        }
    }
}