using System;
using System.Collections.Generic;
using System.Text;
using NavLib.Globals;

namespace NavLib.AT220
{
    public class DecodeAT220ProtocolG
    {
        // Header > 10 > 0...9
        const int Offset_Protocol = 0;
        const int Offset_PacketLengthStart = 1;
        const int Offset_PacketLengthEnd = 2;
        const int Offset_IMEIStart = 3;
        const int Offset_IMEIEnd = 9;
        const int Offset_UnitIDStart = 7;
        const int Offset_UnitIDEnd = 9;
        // Then reports...
        public const int Offset_StatusCodeEnd = 28 - 10;

        public const int Offset_ReportLength = 46;
        const int Offset_ReportLengthWithNoExtraData = 34;

        public Boolean IsPacketDataValid = false;
        Byte[] MyPacketData = new Byte[1024];
        public List<AT220Report> AT220Reports;

        public DecodeAT220ProtocolG(Byte[] bytPacketData)
        {
            // 1 Header
            // 8 Reports
            MyPacketData = bytPacketData;
            IsPacketDataValid = true;

            AT220Reports = new List<AT220Report>();
            int ReportStart = Offset_UnitIDEnd + 1;
            int ReportEnd = ReportStart + Offset_ReportLength - 1;

            while (ReportEnd <= MyPacketData.GetUpperBound(0))
            {
                Byte[] b = new Byte[Offset_ReportLength];
                int ActualReportLength = Offset_ReportLengthWithNoExtraData;
                Array.Copy(MyPacketData, ReportStart, b, 0, ActualReportLength);
                if (Utils.GetBitValue(b, Offset_StatusCodeEnd, 7))  //IsExtraDataPresent
                {
                    ActualReportLength = Offset_ReportLength;
                    Array.Copy(MyPacketData, ReportStart, b, 0, ActualReportLength);
                }
                    
                AT220Reports.Add(new AT220Report(b));

                ReportStart += ActualReportLength;
                ReportEnd = ReportStart + ActualReportLength - 1;
                //ReportEnd = ReportStart + Offset_ReportLength - 1;
            }
        }

        #region Header
        public String GetProtocolType()
        {
            String strReadable = Encoding.ASCII.GetString(MyPacketData, Offset_Protocol, 1);
            String x = MyPacketData[Offset_Protocol].ToString("X");
            String i = MyPacketData[Offset_Protocol].ToString();
            return strReadable;
        }
        public int GetPacketLength()
        {
            ulong u = Utils.BuildLong(MyPacketData, Offset_PacketLengthStart, Offset_PacketLengthEnd);
            return (int)u;
        }
        public String GetUnitIMEI()
        {
            // The TAC/FAC is stored in bytes 3,4,5 and 6 
            // The MSN (GetUnitID) is the serial number that will change per device. This is stored in bytes 7,8 and 9
            ulong u = Utils.BuildLong(MyPacketData, Offset_IMEIStart, Offset_IMEIEnd);
            return u.ToString();
        }
        public String GetUnitID()
        {
            ulong u = Utils.BuildLong(MyPacketData, Offset_UnitIDStart, Offset_UnitIDEnd);
            return u.ToString();
        }
        #endregion
    }
}
