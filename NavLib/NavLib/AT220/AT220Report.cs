using System;
using System.Collections.Generic;
using System.Text;
using NavLib.Globals;

namespace NavLib.AT220
{
    public class AT220Report
    {
        const int Offset_ReportLength = DecodeAT220ProtocolG.Offset_ReportLength;

        //Header >  - 10
        const int Offset_PacketSeqNo = 10 - 10;
        const int Offset_LatStart = 11 - 10;
        const int Offset_LatEnd = 14 - 10;
        const int Offset_LonStart = 15 - 10;
        const int Offset_LonEnd = 18 - 10;
        const int Offset_TimeStart = 19 - 10;
        const int Offset_TimeEnd = 22 - 10;
        const int Offset_Speed = 23 - 10;
        const int Offset_Heading = 24 - 10;
        const int Offset_ReasonCodeEnd = 25 - 10;
        const int Offset_ReasonCodeMiddle = 26 - 10;
        const int Offset_ReasonCodeStart = 27 - 10;
        const int Offset_StatusCodeEnd = DecodeAT220ProtocolG.Offset_StatusCodeEnd; //28 - 10
        const int Offset_StatusCodeStart = 29 - 10;
        const int Offset_DigitalIOStatusNstateChange = 30 - 10;
        const int Offset_ADC = 31 - 10;
        const int Offset_BatteryLevel = 32 - 10;
        const int Offset_ExternalInputVoltage = 33 - 10;
        const int Offset_MaxJourneySpeed = 34 - 10;
        const int Offset_MaxDeceleration = 35 - 10;
        const int Offset_MaxAcceleration = 36 - 10;
        const int Offset_JourneyDistanceTravelledStart = 37 - 10;
        const int Offset_JourneyDistanceTravelledEnd = 48 - 10;
        const int Offset_JourneyIdleTimeStart = 39 - 10;
        const int Offset_JourneyIdleTimeEnd = 40 - 10;
        const int Offset_Altitude = 41 - 10;
        const int Offset_SignalQuality = 42 - 10;
        const int Offset_Geofence = 43 - 10;
        const int Offset_exDriverIDStart = 44 - 10;
        const int Offset_exDriverIDEnd = 50 - 10;
        const int Offset_exOdoStart = 51 - 10;
        const int Offset_exOdoEnd = 53 - 10;
        const int Offset_exRunningTimeinHrsStart = 54 - 10;
        const int Offset_exRunningTimeinHrsEnd = 55 - 10;

        Byte[] MyPacketData;

        public AT220Report(Byte[] bytPacketData)
        {
            MyPacketData = bytPacketData;
        }

        public Byte[] GetAllBytes()
        {
            return MyPacketData;
        }

        ulong BuildLong(int intStartByte, int intEndByte)
        {
            return Utils.BuildLong(MyPacketData, intStartByte, intEndByte);
        }
        Boolean GetBitValue(int Offset_Code, int pos)
        {
            return Utils.GetBitValue(MyPacketData, Offset_Code, pos);
        }

        public Boolean IsReportValid()
        {
            Boolean bResult = false;
            if (MyPacketData.Length == Offset_ReportLength)
                bResult = true;

            return bResult;
        }

        public int GetPacketSeqNo()
        {
            return (int)MyPacketData[Offset_PacketSeqNo];
        }
        public double GetLatitude()
        {
            // The Lattitude is stored in bytes 11,12,13 and 14 and is the degrees * 1,000,000
            double dblRet = (double)(int)BuildLong(Offset_LatStart, Offset_LatEnd) / (double)1000000;
            return dblRet;
        }
        public double GetLongitude()
        {
            // The Longitude is stored in bytes 15,16,17 and 18 and is the degrees * 1,000,000
            double dblRet = (double)(int)BuildLong(Offset_LonStart, Offset_LonEnd) / (double)1000000;
            return dblRet;
        }
        public DateTime GetDate()
        {
            // The date / time is recorded in seconds since the 6th of January 1980 at 00:00:00 
            // That number of second is stored in the bytes Offset_TimeStart to Offset_TimeEnd
            // return dt.ToString("dd MMM yyyy @ HH:mm:ss")
            DateTime dt = new DateTime(1980, 1, 6, 0, 0, 0);
            dt = dt.AddSeconds(BuildLong(Offset_TimeStart, Offset_TimeEnd));
            return dt;
        }
        public int GetSpeedInKmh()
        {
            // The speed is stored in byte Offset_Speed and is in Km/h divided by 2
            return (int)MyPacketData[Offset_Speed] * 2;
        }
        public int GetHeadingInDegrees()
        {
            // The Heading is stored in byte 24 and is in degrees divided by 2
            return (int)MyPacketData[Offset_Heading] * 2;
        }

        public String GetReasons()
        {
            String strResult = String.Empty;
            if (GetBitValue(Offset_ReasonCodeStart, 7))
                strResult += "TimeIntervalElasped(???)";
            if (GetBitValue(Offset_ReasonCodeStart, 6))
                strResult += "DistanceTravelledExceeded(Heading)";
            if (GetBitValue(Offset_ReasonCodeStart, 5))
                strResult += "Polled(???)";
            if (GetBitValue(Offset_ReasonCodeStart, 4))
                strResult += "Geofence(???)";
            if (GetBitValue(Offset_ReasonCodeStart, 3))
                strResult += "Panic(Aux2)";
            if (GetBitValue(Offset_ReasonCodeStart, 2))
                strResult += "ExternalInput(???)";
            if (GetBitValue(Offset_ReasonCodeStart, 1))
                strResult += "JourneyStart(Ignition)";
            if (GetBitValue(Offset_ReasonCodeStart, 0))
                strResult += "JourneyEnd(Ignition)";

            if (GetBitValue(Offset_ReasonCodeMiddle, 7))
                strResult += "Degrees(Heading)";
            if (GetBitValue(Offset_ReasonCodeMiddle, 6))
                strResult += "LowBattery(???)";
            if (GetBitValue(Offset_ReasonCodeMiddle, 5))
                strResult += "ExternalPower(???)";
            if (GetBitValue(Offset_ReasonCodeMiddle, 4))
                strResult += "IdlingStart(BeginOfStop)";
            if (GetBitValue(Offset_ReasonCodeMiddle, 3))
                strResult += "IdlingEnd(EndOfStop)";
            if (GetBitValue(Offset_ReasonCodeMiddle, 2))
                strResult += "IdlingOnGoing(Time)";
            if (GetBitValue(Offset_ReasonCodeMiddle, 1))
                strResult += "RESERVED(???)";
            if (GetBitValue(Offset_ReasonCodeMiddle, 0))
                strResult += "SpeedOverthreshold(???)";

            if (GetBitValue(Offset_ReasonCodeEnd, 7))
                strResult += "Towing_MovingIgnOff(???)";
            if (GetBitValue(Offset_ReasonCodeEnd, 6))
                strResult += "UnauthorisedDriverAlarm_IgnOnUnknownDriver(???)";
            if (GetBitValue(Offset_ReasonCodeEnd, 5))
                strResult += "CollisionAlarm_ExcessiveGForcesDetected(???)";
            if (GetBitValue(Offset_ReasonCodeEnd, 4))
                strResult += "RESERVED(???)";
            if (GetBitValue(Offset_ReasonCodeEnd, 3))
                strResult += "RESERVED(???)";
            if (GetBitValue(Offset_ReasonCodeEnd, 2))
                strResult += "RESERVED(???)";
            if (GetBitValue(Offset_ReasonCodeEnd, 1))
                strResult += "RESERVED(???)";
            if (GetBitValue(Offset_ReasonCodeEnd, 0))
                strResult += "RESERVED(???)";

            if (GetDigitalStatusChangeInput1())//Digital 1 = Ignition sense
                strResult += "DigitalInputChange1(IgnChange)";
            if (GetDigitalStatusChangeInput2())//Digital 2 = Panic sense
                strResult += "DigitalInputChange2(Aux2)";
            if (GetDigitalStatusChangeInput3())//Digital 3 
                strResult += "DigitalInputChange3(Aux3)";
            if (GetDigitalStatusChangeOutput1())//Digital Output 1 
                strResult += "DigitalOutputChange1(???)";

            if (GetStatusIgnitionOn())
                strResult += "Status(IgnOn)";
            else
                strResult += "Status(IgnOff)";
            if (GetStatusPowerOn())
                strResult += "StatusPowerOn(Reset)";
            if (GetStatusIsGPSInvalid())
                strResult += "StatusGPSInvalid(InvalidGpsSignals)";
            if (GetStatusNetworkRoaming())
                strResult += "StatusNetworkRoaming";
            if (GetStatusReportsToFollow())
                strResult += "StatusReportsToFollow";
            if (GetStatusStoredReport())
                strResult += "StatusStoredReport";
            if (GetStatusIsOptionalExtensionDataPresent())
                strResult += "StatusOptionalExtensionDataFieldPresent";
            if (GetStatusSerialDeviceReadError())
                strResult += "StatusSerialDeviceReadError";
            if (GetStatusIsExtraDataPresent())
                strResult += "ExtraDataPresent";

            return strResult;
        }

        public Boolean GetStatusIgnitionOn()
        {
            return GetBitValue(Offset_StatusCodeStart, 7);
        }
        public Boolean GetStatusPowerOn()
        {
            return GetBitValue(Offset_StatusCodeStart, 6);
        }
        public Boolean GetStatusIsGPSInvalid()
        {
            // 0 > False = valid
            // 1 > True = invalid
            return GetBitValue(Offset_StatusCodeStart, 5);
        }
        public Boolean GetStatusNetworkRoaming()
        {
            return GetBitValue(Offset_StatusCodeStart, 4);
        }
        public Boolean GetStatusReportsToFollow()
        {
            return GetBitValue(Offset_StatusCodeStart, 3);
        }
        public Boolean GetStatusStoredReport()
        {
            // 1 > True = Stored
            // 0 > False = Live
            return GetBitValue(Offset_StatusCodeStart, 2);
        }
        public Boolean GetStatusIsOptionalExtensionDataPresent()
        {
            return GetBitValue(Offset_StatusCodeStart, 1);
        }
        public Boolean GetStatusSerialDeviceReadError()
        {
            return GetBitValue(Offset_StatusCodeStart, 0);
        }

        public Boolean GetStatusIsExtraDataPresent()
        {
            return GetBitValue(Offset_StatusCodeEnd, 7);
        }

        public Boolean GetDigitalInput1()
        {
            //Ignition
            return GetBitValue(Offset_DigitalIOStatusNstateChange, 7);
        }
        public Boolean GetDigitalInput2()
        {
            //Panic
            return GetBitValue(Offset_DigitalIOStatusNstateChange, 6);
        }
        public Boolean GetDigitalInput3()
        {
            return GetBitValue(Offset_DigitalIOStatusNstateChange, 5);
        }
        public Boolean GetDigitalOutput1()
        {
            return GetBitValue(Offset_DigitalIOStatusNstateChange, 4);
        }
        public Boolean GetDigitalStatusChangeInput1()
        {
            return GetBitValue(Offset_DigitalIOStatusNstateChange, 3);
        }
        public Boolean GetDigitalStatusChangeInput2()
        {
            return GetBitValue(Offset_DigitalIOStatusNstateChange, 2);
        }
        public Boolean GetDigitalStatusChangeInput3()
        {
            return GetBitValue(Offset_DigitalIOStatusNstateChange, 1);
        }
        public Boolean GetDigitalStatusChangeOutput1()
        {
            return GetBitValue(Offset_DigitalIOStatusNstateChange, 0);
        }

        public byte GetADC()
        {
            return MyPacketData[Offset_ADC];
        }
        public int GetBatteryLevelPercentage()
        {
            return (int)MyPacketData[Offset_BatteryLevel];
        }
        public float GetExternalInputVoltage()
        {
            return (float)MyPacketData[Offset_ExternalInputVoltage] * (float)0.2;
        }
        public int GetMaxJourneySpeed()
        {
            // The Maximum Journey Speed is in km/h and is divided by 2.
            return (int)MyPacketData[Offset_MaxJourneySpeed] * 2;
        }
        public int GetMaxDeceleration()
        {
            // The Maximum Deceleration is in m/s2
            return (int)MyPacketData[Offset_MaxDeceleration] / 10;
        }
        public int GetMaxAcceleration()
        {
            // The Maximum Acceleration is in m/s2
            return (int)MyPacketData[Offset_MaxAcceleration] / 10;
        }
        public float GetJourneyDistanceTravelled()
        {
            // The total distance travelled in this journey is stored in km and is multiplied by 10. Mul by 0.1 to get it back. 
            float distance = (float)BuildLong(Offset_JourneyDistanceTravelledStart, Offset_JourneyDistanceTravelledEnd);
            return distance * 0.1F;
        }
        public int GetJourneyIdleSeconds()
        {
            return (int)BuildLong(Offset_JourneyIdleTimeStart, Offset_JourneyIdleTimeEnd);
        }
        public int GetAltitudeInMeters()
        {
            // The Altitude is height above sea level and is in meters divided by 20
            return (int)MyPacketData[Offset_Heading] * 20;
        }

        public int GetSignalQuality()
        {
            // The Altitude is height above sea level and is in meters divided by 20
            return (int)MyPacketData[Offset_SignalQuality];
        }
        public int GetGeofenceEvents()
        {
            // The Altitude is height above sea level and is in meters divided by 20
            return (int)MyPacketData[Offset_Geofence];
        }

        //Optional Extra Data
        //Present when $PROT,3 and Journey_Start/Stop
        public String GetDriverID()
        {
            String bResult = String.Empty;
            if (GetStatusIsExtraDataPresent())
            {
                for (int i = Offset_exDriverIDStart; i <= Offset_exDriverIDEnd; i++)
                {
                    byte x = MyPacketData[i];
                    String strTemp = x.ToString("X");
                    if (strTemp.Length == 1)
                        strTemp = "0" + strTemp;
                    bResult += strTemp;
                }
            }
            else
                bResult = "GetLastDriverIDUsed";
            return bResult;
        }
        public int GetOdoInKM()
        {
            return (int)BuildLong(Offset_exOdoStart, Offset_exOdoEnd);
        }
        public int GetRunTimeInHrs()
        {
            return (int)BuildLong(Offset_exRunningTimeinHrsStart, Offset_exRunningTimeinHrsEnd);
        }
    }
}
