﻿using NaveoOneLib.Models.Zones;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using NaveoOneLib.Services.Users;
using NaveoOneLib.Models.Drivers;

namespace NaveoService.Services
{
    public static class CommonService
    {
        #region Methods

        /// <summary>
        /// Validate Assets from list of Asset Ids
        /// </summary>
        /// <param name="sUserToken"></param>
        /// <param name="lassetIds"></param>
        /// <param name="sConnStr"></param>
        /// <param name="myUID"></param>
        /// <returns></returns>
        public static List<int> lValidateAssets(Guid sUserToken, List<int> lassetIds, String sConnStr, out int myUID)
        {
            int UID = new NaveoOneLib.Services.Users.UserService().GetUserID(sUserToken, sConnStr);
            DataTable dtGMAsset = new NaveoOneLib.WebSpecifics.LibData().dtGMAsset(UID, sConnStr);

            //chk assetIds with dtGMAsset assetIds
            List<int> lIDs = new List<int>();
            foreach (int i in lassetIds)
            {
                DataRow[] dr = dtGMAsset.Select("StrucID = " + i);
                if (dr.Length > 0)
                    lIDs.Add(i);
            }

            myUID = UID;
            return lIDs.Distinct().ToList();
        }

        public static List<int> lValidateRules(Guid sUserToken, List<int> lRuleids, String sConnStr, out int myUID)
        {
            int UID = new NaveoOneLib.Services.Users.UserService().GetUserID(sUserToken, sConnStr);
            DataTable dtGMRule = new NaveoOneLib.WebSpecifics.LibData().dtGMRule(UID, sConnStr);

            //chk assetIds with dtGMAsset assetIds
            List<int> lIDs = new List<int>();
            foreach (int i in lRuleids)
            {
                DataRow[] dr = dtGMRule.Select("StrucID = " + i);
                if (dr.Length > 0)
                    lIDs.Add(i);
            }

            myUID = UID;
            return lIDs.Distinct().ToList();
        }

        /// <summary>
        /// Validate Assets
        /// </summary>
        /// <param name="sUserToken"></param>
        /// <param name="sConnStr"></param>
        /// <param name="myUID"></param>
        /// <returns></returns>
        public static List<int> lValidateAssets(Guid sUserToken, String sConnStr, out int myUID)
        {
            int UID = new NaveoOneLib.Services.Users.UserService().GetUserID(sUserToken, sConnStr);
            DataTable dtGMAsset = new NaveoOneLib.WebSpecifics.LibData().dtGMAsset(UID, sConnStr);

            List<int> lIDs = new List<int>();
            foreach (DataRow dr in dtGMAsset.Rows)
            {
                int i = 0;
                if (int.TryParse(dr["StrucID"].ToString(), out i))
                    if (i > 0)
                        lIDs.Add(i);
            }

            myUID = UID;
            return lIDs.Distinct().ToList();
        }


        public static List<string> lValidateBLUPs(Guid sUserToken, List<string> lBLUPIds, String sConnStr, out int myUID)
        {
            int UID = new NaveoOneLib.Services.Users.UserService().GetUserID(sUserToken, sConnStr);
            DataTable dtGMBlup = new NaveoOneLib.WebSpecifics.LibData().dtGMBlup(UID, sConnStr);
           
            List<string> lIDs = new List<string>();
            foreach (var i in lBLUPIds)
            {
                DataRow[] dr = dtGMBlup.Select("StrucID = " + i);
                if (dr.Length > 0)
                    lIDs.Add(i.ToString());
            }

            myUID = UID;
            return lIDs.Distinct().ToList();
        }

        public static List<int> lValidateDrivers(Guid sUserToken, List<int> lDriverIds, String sConnStr, out int myUID)
        {
            int UID = new NaveoOneLib.Services.Users.UserService().GetUserID(sUserToken, sConnStr);
            DataTable dtGMDriver = new NaveoOneLib.WebSpecifics.LibData().dtGMDriver(UID, sConnStr);

            //chk driverIds with dtGMDriver
            List<int> lIDs = new List<int>();
            foreach (int i in lDriverIds)
            {
                DataRow[] dr = dtGMDriver.Select("StrucID = " + i);
                if (dr.Length > 0)
                    lIDs.Add(i);
            }

            myUID = UID;
            return lIDs.Distinct().ToList();
        }

        public static List<int> lValidateMatrix(Guid sUserToken, List<int> lMatrixIds, String sConnStr, out int myUID)
        {
            int UID = new NaveoOneLib.Services.Users.UserService().GetUserID(sUserToken, sConnStr);
            DataTable dtGMMatrix = new NaveoOneLib.WebSpecifics.LibData().dtGMMatrix(UID, sConnStr);

            //chk matrixIds with dtGMDMatrix
            List<int> lIDs = new List<int>();
            foreach (int i in lMatrixIds)
            {
                DataRow[] dr = dtGMMatrix.Select("GMID = " + i);
                if (dr.Length > 0)
                    lIDs.Add(i);
            }

            myUID = UID;
            return lIDs.Distinct().ToList();
        }

        /// <summary>
        /// Validate Zones
        /// </summary>
        /// <param name="lZoneIds"></param>
        /// <returns></returns>
        public static List<int> lValidateZones(Guid sUserToken, List<int> lZoneIds, String sConnStr, out int myUID)
        {
            int UID = new NaveoOneLib.Services.Users.UserService().GetUserID(sUserToken, sConnStr);
            DataTable dtGMZone = new NaveoOneLib.WebSpecifics.LibData().dtGMZone(UID, sConnStr);

            //chk ZoneIds with dtGMZone
            List<int> lIDs = new List<int>();
            foreach (int i in lZoneIds)
            {
                DataRow[] dr = dtGMZone.Select("StrucID = " + i);
                if (dr.Length > 0)
                    lIDs.Add(i);
            }

            myUID = UID;
            return lIDs.Distinct().ToList();
        }



        public static List<int> lValidateZoneTpes(Guid sUserToken, List<int> lZoneTypeIds, String sConnStr, out int myUID)
        {
            int UID = new NaveoOneLib.Services.Users.UserService().GetUserID(sUserToken, sConnStr);
            DataTable dtGMZoneType = new NaveoOneLib.WebSpecifics.LibData().dtGMZoneType(UID, sConnStr);

            //chk ZoneIds with dtGMZone
            List<int> lIDs = new List<int>();
            foreach (int i in lZoneTypeIds)
            {
                DataRow[] dr = dtGMZoneType.Select("StrucID = " + i);
                if (dr.Length > 0)
                    lIDs.Add(i);
            }

            myUID = UID;
            return lIDs.Distinct().ToList();
        }


        public static List<int> lValidateResources(Guid sUserToken, List<Driver.ResourceType> lrt, String sConnStr, out int myUID)
        {
            int UID = new NaveoOneLib.Services.Users.UserService().GetUserID(sUserToken, sConnStr);
            DataTable dtGMDriver = new NaveoOneLib.WebSpecifics.LibData().dtGMDriver(UID, lrt, true, sConnStr);

            List<int> lIDs = new List<int>();
            foreach (DataRow dr in dtGMDriver.Rows)
            {
                int i = 0;
                if (int.TryParse(dr["StrucID"].ToString(), out i))
                    if (i > 0)
                        lIDs.Add(i);
            }

            myUID = UID;
            return lIDs.Distinct().ToList();

        }

        public static List<int> lValidatePlanning(Guid sUserToken, List<int> lPlanningIds, String sConnStr, out int myUID)
        {
            int UID = new NaveoOneLib.Services.Users.UserService().GetUserID(sUserToken, sConnStr);
            DataTable dtGMPlanning = new NaveoOneLib.WebSpecifics.LibData().dtGMPlanningRequest(UID, sConnStr);

            //chk planningIDs with dtGMPlanning
            List<int> lIDs = new List<int>();
            foreach (int i in lPlanningIds)
            {
                DataRow[] dr = dtGMPlanning.Select("StrucID = " + i);
                if (dr.Length > 0)
                    lIDs.Add(i);
            }

            myUID = UID;
            return lIDs.Distinct().ToList();
        }

        public static List<int> lValidateRoles(Guid sUserToken, List<int> lRoleIds, String sConnStr, out int myUID)
        {
            int UID = new NaveoOneLib.Services.Users.UserService().GetUserID(sUserToken, sConnStr);
            DataTable dtGMRoles = new NaveoOneLib.WebSpecifics.LibData().dtGMRoles(UID, sConnStr);

            //chk planningIDs with dtGMPlanning
            List<int> lIDs = new List<int>();
            foreach (int i in lRoleIds)
            {
                DataRow[] dr = dtGMRoles.Select("StrucID = " + i);
                if (dr.Length > 0)
                    lIDs.Add(i);
            }

            myUID = UID;
            return lIDs.Distinct().ToList();
        }


        /// <summary>
        /// Validate Users from list of User Ids
        /// </summary>
        /// <param name="sUserToken"></param>
        /// <param name="lUsers"></param>
        /// <param name="sConnStr"></param>
        /// <param name="myUID"></param>
        /// <returns></returns>
        public static List<int> lValidateUsers(Guid sUserToken, List<int> lUsers, String sConnStr, out int myUID)
        {
            int UID = new NaveoOneLib.Services.Users.UserService().GetUserID(sUserToken, sConnStr);
            DataTable dtGMUser = new NaveoOneLib.WebSpecifics.LibData().dtGMUser(UID, false, sConnStr);

            //chk assetIds with dtGMAsset assetIds
            List<int> lIDs = new List<int>();
            foreach (int i in lUsers)
            {
                DataRow[] dr = dtGMUser.Select("StrucID = " + i);
                if (dr.Length > 0)
                    lIDs.Add(i);
            }

            myUID = UID;
            return lIDs.Distinct().ToList();
        }

        /// <summary>
        /// Cast to Zone
        /// </summary>
        /// <param name="lZone"></param>
        /// <returns></returns>
        public static List<Models.Zone> castLApiZone(List<Zone> lZone)
        {
            List<Models.Zone> laz = new List<Models.Zone>();
            foreach (Zone z in lZone)
            {
                Models.Zone az = new Models.Zone();
                az.zoneID = z.zoneID;
                az.description = z.description;
                az.color = z.color;//HexConverter(Color.FromArgb(z.color));
                az.isMarker = z.isMarker;
                az.iBuffer = z.iBuffer;
                az.parent = z.lMatrix[0].GMID;
                //List<Models.ZoneDetail> ld = new List<Models.ZoneDetail>();
                //foreach (NaveoOneLib.Models.ZoneDetail zd in z.zoneDetails)
                //{
                //    Models.ZoneDetail a = new Models.ZoneDetail();
                //    a.latitude = zd.latitude;
                //    a.longitude = zd.longitude;
                //    a.cordOrder = zd.cordOrder;
                //    ld.Add(a);
                //}
                az.zoneDetails = z.zoneDetails;
                laz.Add(az);
            }
            return laz;
        }

        /// <summary>
        /// Cast to zone
        /// </summary>
        /// <param name="dsZones"></param>
        /// <returns></returns>
        public static List<Models.Zone> castLApiZone(DataSet dsZones)
        {
            List<Models.Zone> laz = new List<Models.Zone>();
            DataTable dtZoneH = dsZones.Tables["dtZoneH"];
            DataTable dtZoneD = dsZones.Tables["dtZoneD"];
            foreach (DataRow dr in dtZoneH.Rows)
            {
                Models.Zone z = new Models.Zone();
                z.zoneID = Convert.ToInt32(dr["ZoneID"]);
                z.description = dr["Description"].ToString();
                z.sColor = HexConverter(Color.FromArgb(Convert.ToInt32(dr["Color"])));
                z.isMarker = Convert.ToInt32(dr["isMarker"]);
                z.iBuffer = Convert.ToInt32(dr["iBuffer"]);
                //z.LookUpValue = Convert.ToInt32(dr["LookUpValue"]);

                DataRow[] dRows = dsZones.Tables["dtGMZone"].Select("StrucID = " + z.zoneID + "");
                z.parent = Convert.ToInt32(dRows[0]["ParentGMID"]);

                z.zoneDetails = new List<NaveoOneLib.Models.Zones.ZoneDetail>();
                DataRow[] drd = dtZoneD.Select("ZoneID = " + dr["ZoneID"].ToString());
                foreach (DataRow d in drd)
                {
                    Models.ZoneDetail zd = new Models.ZoneDetail();
                    zd.latitude = (Double)d["latitude"];
                    zd.longitude = (Double)d["longitude"];
                    zd.cordOrder = Convert.ToInt32(d["CordOrder"]);
                    z.zoneDetails.Add(zd);
                }
                laz.Add(z);
            }
            return laz;
        }

        public static int GetUserID(Guid sUserToken, String sConnStr)
        {
            int UID = new NaveoOneLib.Services.Users.UserService().GetUserID(sUserToken, sConnStr);
            return UID;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Convert to Hex
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public static string HexConverter(Color c)
        {
            return String.Format("#{0:X6}", c.ToArgb() & 0x00FFFFFF);
        }
        #endregion
    }
}