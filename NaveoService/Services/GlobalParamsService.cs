﻿using NaveoOneLib.Models.Common;
using System;
using System.Data;
using System.Data.Common;

namespace NaveoService.Services
{
    public class GlobalParamsService
    {
        private NaveoOneLib.Services.GlobalParamsService globalParamsService = new NaveoOneLib.Services.GlobalParamsService();
        #region Methods
        public DataTable GetGlobalParams(String sConnStr)
        {
            DataTable dt = new DataTable();
            dt = globalParamsService.GetGlobalParams(sConnStr);

            if(dt.Columns.Contains("iid"))
                dt.Columns["iid"].ColumnName = "iid";

            if (dt.Columns.Contains("paramname"))
                dt.Columns["paramname"].ColumnName = "ParamName";

            if (dt.Columns.Contains("pvalue"))
                dt.Columns["pvalue"].ColumnName = "PValue";

            if (dt.Columns.Contains("paramcomments"))
                dt.Columns["paramcomments"].ColumnName = "ParamComments";

            return dt;
        }

        public Boolean UpdateGlobalParams(String iid, String PValue, String sConnStr) {
            GlobalParam globalParamObj = globalParamsService.GetGlobalParamsById(iid, sConnStr);
            globalParamObj.PValue = PValue;
            return globalParamsService.UpdateGlobalParams(globalParamObj, null, sConnStr);
        }

        private UpdateGlobalParam castToUpdateParamObj(GlobalParam param) {
            UpdateGlobalParam update = new UpdateGlobalParam();
            update.ParamComments = param.ParamComments;
            update.ParamName = param.ParamName;
            update.PValue = param.PValue;
            return update;
        }
        #endregion
    }
}
