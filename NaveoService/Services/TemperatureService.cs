﻿using NaveoOneLib.Models.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Services
{
    public class TemperatureService
    {

        public dtData getCustomizedTemperature(Guid sUserToken, string sConnStr, List<int> lAssetIDs, DateTime dtFr, DateTime dtTo, List<String> lTypeID, int TimeInterval, int? Page, int? LimitPerPage)
        {

            int UID = -1;
            List<int> lValidatedAssets = CommonService.lValidateAssets(sUserToken, lAssetIDs, sConnStr, out UID);
            NaveoOneLib.Services.GPS.GPSDataService gpsDataService = new NaveoOneLib.Services.GPS.GPSDataService();

            if (TimeInterval == 0)
                TimeInterval = 30;

            DataTable ResultTable = new DataTable();
            ResultTable.Columns.Add("rowNum", typeof(int));
            ResultTable.Columns["rowNum"].AutoIncrement = true;
            ResultTable.Columns["rowNum"].AutoIncrementSeed = 1;
            ResultTable.Columns["rowNum"].AutoIncrementStep = 1;

            foreach (int i in lValidatedAssets)
            {
                DataTable dbugData = gpsDataService.GetDebugData(i, dtFr, dtTo, lTypeID, TimeInterval, sConnStr);
                ResultTable.Merge(dbugData);
            }
            int cnt = ResultTable.Rows.Count;

            Pagination p = Pagination.GetRowNums(Page, LimitPerPage);
            DataTable dt = new DataTable();
            DataRow[] drChk = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo);
            if (drChk != null && drChk.Length > 0)
                dt = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo).CopyToDataTable();
            else
                dt = ResultTable;

            dtData dtR = new dtData();
            dtR.Data = dt;
            dtR.Rowcnt = cnt;

            return dtR;
        }

    }
}
