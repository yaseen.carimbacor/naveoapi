﻿
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Users;
using NaveoOneLib.Services.GMatrix;
using NaveoService.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using NaveoOneLib.WebSpecifics;
using NaveoOneLib.Services.Users;
using NaveoOneLib.Services.Assets;
using NaveoOneLib.Services;
using NaveoOneLib.Models.Lookups;
using NaveoOneLib.Models.Drivers;

namespace NaveoService.Services
{
    public class DropDownService
    {
        #region Asset DropDown
        private DataTable getAssetTypes(Guid sUserToken, string sConnStr)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);
            DataTable dtAux = new AssetTypeMappingService().GetAssetTypeMapping(sConnStr);
            dtAux.Columns["IID"].ColumnName = "id";
            dtAux.Columns["Description"].ColumnName = "description";
            dtAux.TableName = "assetTypes";

            return dtAux;
        }

        private DataTable getTimeZones(string sConnStr)
        {
            DataTable dtFinal = new DataTable();
            dtFinal.Columns.Add("description");
            dtFinal.Columns.Add("id");

            #region TimeZones
            var tzone = TimeZoneInfo.GetSystemTimeZones();

            foreach (TimeZoneInfo tzi in tzone)
            {
                DataRow newRow = dtFinal.NewRow();
                newRow["description"] = tzi.StandardName;
                //newRow["iD"] = null;
                dtFinal.Rows.Add(newRow);
            }
            dtFinal.TableName = "timeZones";
            dtFinal.Columns.Remove("id");
            dtFinal.AcceptChanges();
            return dtFinal;

            #endregion
        }

        private DataTable getProductType(string sConnStr)
        {
            DataTable dtProductTypeLookupValues = new NaveoOneLib.Services.Lookups.LookUpValuesService().GetLookUpValuesByLookUpTypeName("CallibrationProduct", sConnStr);
            dtProductTypeLookupValues.Columns["Name"].ColumnName = "description";
            dtProductTypeLookupValues.Columns["VID"].ColumnName = "id";
            dtProductTypeLookupValues.TableName = "fuelProduct";

            List<String> lCols = new List<string>();
            foreach (DataColumn dc in dtProductTypeLookupValues.Columns)
                if (dc.ColumnName != "description")
                    if (dc.ColumnName != "id")
                        lCols.Add(dc.ColumnName);

            foreach (String s in lCols)
                dtProductTypeLookupValues.Columns.Remove(s);

            return dtProductTypeLookupValues;

        }

        private DataTable getVehicleTypes(Guid sUserToken, string sConnStr)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            DataTable dtVehicleTypes = new NaveoOneLib.WebSpecifics.LibData().dtVehicleTypes(UID, sConnStr);
            dtVehicleTypes.Columns["Description"].ColumnName = "description";
            dtVehicleTypes.Columns["VehicleTypeId"].ColumnName = "id";

            List<String> lCols = new List<string>();
            foreach (DataColumn dc in dtVehicleTypes.Columns)
                if (dc.ColumnName != "description")
                    if (dc.ColumnName != "id")
                        lCols.Add(dc.ColumnName);

            foreach (String s in lCols)
                dtVehicleTypes.Columns.Remove(s);

            dtVehicleTypes.TableName = "vehicleTypes";
            return dtVehicleTypes;
        }
        private DataTable getUsers(Guid sUserToken, string sConnStr)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);
            DataTable dtUsers = new NaveoOneLib.Services.Users.UserService().GetAllUsers(lm, sConnStr);
            DataView view = new DataView(dtUsers);
            DataTable dtResult = view.ToTable(false, "UID", "Names");
            dtResult.Columns["UID"].ColumnName = "id";
            dtResult.Columns["Names"].ColumnName = "description";
            dtResult.TableName = "users";
            return dtResult;
        }
        private DataTable getColor(string sConnStr)
        {
            DataTable dtColorLookupValues = new NaveoOneLib.Services.Lookups.LookUpValuesService().GetLookUpValuesByLookUpTypeName("Color", sConnStr);
            dtColorLookupValues.Columns["VID"].ColumnName = "id";
            dtColorLookupValues.Columns["Name"].ColumnName = "description";
            dtColorLookupValues.Columns.Remove("TID");
            dtColorLookupValues.TableName = "color";

            return dtColorLookupValues;

        }

        private DataTable getUom(string sConnStr)
        {
            DataTable dtUOM = new UOMService().GetUOM(sConnStr);
            dtUOM.Columns["UID"].ColumnName = "id";
            dtUOM.Columns["Description"].ColumnName = "description";
            dtUOM.TableName = "uom";

            return dtUOM;
        }

        private DataTable getAssetMake(string sConnStr)
        {
            DataTable dtMakeLookupValues = new NaveoOneLib.Services.Lookups.LookUpValuesService().GetLookUpValuesByLookUpTypeName("MAKE_TYPE", sConnStr);
            dtMakeLookupValues.Columns["VID"].ColumnName = "id";
            dtMakeLookupValues.Columns["Name"].ColumnName = "description";
            dtMakeLookupValues.Columns.Remove("TID");
            dtMakeLookupValues.TableName = "make";

            return dtMakeLookupValues;
        }

        private DataTable getSectionType(string sConnStr)
        {
            DataTable dtMakeLookupValues = new NaveoOneLib.Services.Lookups.LookUpValuesService().GetLookUpValuesByLookUpTypeName("DEPARTMENT", sConnStr);
            dtMakeLookupValues.Columns["VID"].ColumnName = "id";
            dtMakeLookupValues.Columns["Name"].ColumnName = "description";
            dtMakeLookupValues.Columns.Remove("TID");
            dtMakeLookupValues.TableName = "section";

            return dtMakeLookupValues;
        }

        private DataTable getAssetModel(string sConnStr)
        {
            DataTable dtModelLookupValues = new NaveoOneLib.Services.Lookups.LookUpValuesService().GetLookUpValuesByLookUpTypeName("Model", sConnStr);
            dtModelLookupValues.Columns["VID"].ColumnName = "id";
            dtModelLookupValues.Columns["Name"].ColumnName = "description";
            dtModelLookupValues.Columns.Remove("TID");
            dtModelLookupValues.TableName = "model";

            return dtModelLookupValues;
        }

        private DataTable getGroupstreeData(Guid sUserToken, string sConnStr)
        {

            NaveoOneLib.WebSpecifics.LibData nn = new NaveoOneLib.WebSpecifics.LibData();
            var dtGroups = nn.NaveoMasterData(sUserToken, sConnStr, NaveoModels.Groups, null, null);
            dtData n = new dtData();
            n.Data = dtGroups.Data;
            n.Data.TableName = "groupsTreeData";

            return n.Data;
        }

        private DataTable getRoadSpeedType(string sConnStr)
        {
            DataTable dtRoadSpeedType = new NaveoOneLib.Services.Assets.VehicleTypesService().GetRoadSpeedType(sConnStr);
            dtRoadSpeedType.Columns["iID"].ColumnName = "id";
            dtRoadSpeedType.Columns["VehicleType"].ColumnName = "description";

            dtRoadSpeedType.TableName = "roadSpeedType";

            return dtRoadSpeedType;

        }

        private DataSet getAssetDropDown(Guid sUserToken, string sConnStr)
        {
            DataSet dsAsset = new DataSet("assetDropDownValues");

            DataTable dtAssetTypes = getAssetTypes(sUserToken, sConnStr);
            DataTable dtTimezones = getTimeZones(sConnStr);
            DataTable dtProductTypes = getProductType(sConnStr);
            DataTable dtVechTypes = getVehicleTypes(sUserToken, sConnStr);
            DataTable dtVT = dtVechTypes.Copy();
            DataTable dtColor = getColor(sConnStr);
            DataTable dtUom = getUom(sConnStr);
            DataTable dtMake = getAssetMake(sConnStr);
            DataTable dtModel = getAssetModel(sConnStr);
            DataTable dtGroups = getGroupstreeData(sUserToken, sConnStr);
            DataTable dtRoadSpeedType = getRoadSpeedType(sConnStr);

            dsAsset.Tables.Add(dtAssetTypes);
            dsAsset.Tables.Add(dtTimezones);
            dsAsset.Tables.Add(dtProductTypes);
            dsAsset.Tables.Add(dtVT);
            dsAsset.Tables.Add(dtColor);
            dsAsset.Tables.Add(dtUom);
            dsAsset.Tables.Add(dtMake);
            dsAsset.Tables.Add(dtModel);
            dsAsset.Tables.Add(dtGroups);
            dsAsset.Tables.Add(dtRoadSpeedType);

            return dsAsset;

        }

        #endregion
        #region MatrixDropDown
        private DataSet getMatrixDown(Guid sUserToken, string sConnStr)
        {
            DataSet dsMatrix = new DataSet("matrixDropDownValues");
            DataTable dtNaveoModules = getNaveoModules(sUserToken, sConnStr);
            DataTable dtGroups = getGroupstreeData(sUserToken, sConnStr);

            dsMatrix.Tables.Add(dtNaveoModules);
            dsMatrix.Tables.Add(dtGroups);

            return dsMatrix;
        }
        #endregion
        #region UserDropDown
        private DataSet getUserDropDown(Guid sUserToken, string sConnStr)
        {
            DataSet dsUser = new DataSet("UserDropDownValues");

            DataTable dtZones = getZones(sUserToken, sConnStr);
            DataTable dtGroups = getGroupstreeData(sUserToken, sConnStr);
            DataTable dtRoles = getRoles(sUserToken, sConnStr);
            DataTable dtPlanningLkp = getInstitution(sConnStr);
            //DataTable dtUserApproval = getUserApprovalValues(sUserToken,sConnStr);
            //dsUser.Tables.Add(dtGroups);
            dsUser.Tables.Add(dtZones);
            dsUser.Tables.Add(dtRoles);
            dsUser.Tables.Add(dtPlanningLkp);
            //dsUser.Tables.Add(dtUserApproval);
            return dsUser;
        }
        private DataTable getRoles(Guid sUserToken, string sConnStr)
        {
            DataTable dtRoles = new NaveoOneLib.Services.Permissions.RolesService().GetRoles(sUserToken, sConnStr);
            dtRoles.Columns["iID"].ColumnName = "id";
            dtRoles.Columns["Description"].ColumnName = "description";
            dtRoles.TableName = "roles";
            return dtRoles;

        }
        private DataTable getNaveoModules(Guid sUserToken, string sConnStr)
        {
            DataTable dtGetNaveoModules = new NaveoOneLib.Services.Permissions.NaveoModulesService().GetNaveoModules(sUserToken, sConnStr);
            dtGetNaveoModules.Columns["iID"].ColumnName = "id";
            dtGetNaveoModules.Columns["Description"].ColumnName = "description";

            dtGetNaveoModules.TableName = "naveoModules";

            return dtGetNaveoModules;
        }
        #endregion
        #region Resource DropDown
        private DataSet getResoureDropDown(Guid UserToken, string sConnStr)
        {
            DataSet dsResource = new DataSet("resourceDropDownValues");

            DataTable dtCity = getCity(sConnStr);
            DataTable dtDistrict = getDistrict(sConnStr);
            DataTable dtInstitution = getInstitution(sConnStr);
            DataTable dtDriver = getDriverLookUPValues(sConnStr);
            DataTable dtGrADE = getGradeLookUPValues(sConnStr);
            DataTable dtZones = getZones(UserToken, sConnStr);
            DataTable dtVech = getVehicleTypes(UserToken, sConnStr);
            DataTable dtUser = getUsers(UserToken, sConnStr);
            DataTable dtVT = dtVech.Copy();

            dsResource.Tables.Add(dtCity);
            dsResource.Tables.Add(dtDistrict);
            dsResource.Tables.Add(dtInstitution);
            dsResource.Tables.Add(dtDriver);
            dsResource.Tables.Add(dtGrADE);
            dsResource.Tables.Add(dtZones);
            dsResource.Tables.Add(dtVT);
            dsResource.Tables.Add(dtUser);


            return dsResource;
        }
        private DataTable getCity(string sConnStr)
        {
            DataTable dtCityLookupValues = new NaveoOneLib.Services.Lookups.LookUpValuesService().GetLookUpValuesByLookUpTypeName("CITY", sConnStr);
            dtCityLookupValues.Columns["VID"].ColumnName = "id";
            dtCityLookupValues.Columns["Name"].ColumnName = "description";
            dtCityLookupValues.Columns.Remove("TID");
            dtCityLookupValues.TableName = "city";

            return dtCityLookupValues;

        }
        private DataTable getDistrict(string sConnStr)
        {
            DataTable dttDistrictLookupValues = new NaveoOneLib.Services.Lookups.LookUpValuesService().GetLookUpValuesByLookUpTypeName("DISTRICT", sConnStr);
            dttDistrictLookupValues.Columns["VID"].ColumnName = "id";
            dttDistrictLookupValues.Columns["Name"].ColumnName = "description";
            dttDistrictLookupValues.Columns.Remove("TID");
            dttDistrictLookupValues.TableName = "district";

            return dttDistrictLookupValues;

        }
        private DataTable getGradeLookUPValues(string sConnStr)
        {
            DataTable dtGradeLookupValues = new NaveoOneLib.Services.Lookups.LookUpValuesService().GetLookUpValuesByLookUpTypeName("GradeType", sConnStr);
            dtGradeLookupValues.Columns["VID"].ColumnName = "id";
            dtGradeLookupValues.Columns["Name"].ColumnName = "description";
            dtGradeLookupValues.Columns.Remove("TID");
            dtGradeLookupValues.TableName = "gradeType";

            return dtGradeLookupValues;

        }
        private DataTable getDriverLookUPValues(string sConnStr)
        {
            DataTable dtDriverLookupValues = new NaveoOneLib.Services.Lookups.LookUpValuesService().GetLookUpValuesByLookUpTypeName("DriverType", sConnStr);
            dtDriverLookupValues.Columns["VID"].ColumnName = "id";
            dtDriverLookupValues.Columns["Name"].ColumnName = "description";
            dtDriverLookupValues.Columns.Remove("TID");
            dtDriverLookupValues.TableName = "driverType";

            return dtDriverLookupValues;

        }
        private DataTable getZones(Guid UserToken, string sConnStr)
        {
            DataTable dtZones = new NaveoOneLib.Services.Zones.ZoneService().GetZone(UserToken, sConnStr);

            dtZones.Columns["ZoneID"].ColumnName = "id";
            dtZones.Columns["Description"].ColumnName = "description";
            dtZones.Columns.Remove("Displayed");
            dtZones.Columns.Remove("Comments");
            dtZones.Columns.Remove("Color");
            dtZones.Columns.Remove("ExternalRef");
            dtZones.Columns.Remove("isMarker");
            dtZones.Columns.Remove("iBuffer");
            dtZones.Columns.Remove("Ref2");
            dtZones.Columns.Remove("RowNumber");
            dtZones.TableName = "zone";

            return dtZones;

        }
        private DataTable getVCA(String sConnStr)
        {

            DataTable dtvca = new NaveoOneLib.Services.Zones.VCAHeaderService().GetVCAHeader(sConnStr);
            dtvca.Columns["VCAID"].ColumnName = "id";
            dtvca.Columns["VCAName"].ColumnName = "description";
            dtvca.Columns.Remove("DistrictID");
            //dtvca.Columns.Remove("Comments");
            dtvca.Columns.Remove("IColor");
            // dtvca.Columns.Remove("JsonGeom");
            dtvca.TableName = "vca";

            return dtvca;
        }

        #endregion
        #region Planning DropDown
        private DataSet getPlanningDropDown(Guid UserToken, string sConnStr)
        {
            DataSet dsPlanning = new DataSet("planningDropDownValues");

            DataTable dtLookTypes = getLookTypes(sConnStr);
            DataTable dtZones = getZones(UserToken, sConnStr);
            DataTable dtUOM = getUom(sConnStr);
            DataTable dtVCA = getVCA(sConnStr);
            DataTable dtPurpose = getPurposeValues(sConnStr);
            DataTable dtPostHeld = getPostHeldLookUpValues(UserToken, sConnStr);
            DataTable dtBaseOffice = getBaseOfficeLookUpValues(UserToken, sConnStr);
            DataTable dtAllResources = getAllResources(UserToken, sConnStr);

            dsPlanning.Tables.Add(dtLookTypes);
            dsPlanning.Tables.Add(dtZones);
            dsPlanning.Tables.Add(dtUOM);
            dsPlanning.Tables.Add(dtVCA);
            dsPlanning.Tables.Add(dtPostHeld);
            dsPlanning.Tables.Add(dtBaseOffice);
            dsPlanning.Tables.Add(dtPurpose);
            dsPlanning.Tables.Add(dtAllResources);


            return dsPlanning;
        }
        private DataTable getTripTypeValues(string sConnStr)
        {
            DataTable dtDeptValues = new NaveoOneLib.Services.Lookups.LookUpValuesService().GetLookUpValuesByLookUpTypeName("TRIP_TYPE", sConnStr);
            dtDeptValues.Columns["VID"].ColumnName = "id";
            dtDeptValues.Columns["Name"].ColumnName = "description";
            dtDeptValues.Columns.Remove("TID");
            dtDeptValues.TableName = "tripTypeValues";

            return dtDeptValues;
        }
        private DataTable getLookTypes(string sConnStr)
        {

            DataTable dtTypes = new DataTable();
            dtTypes.Columns.Add("id");
            dtTypes.Columns.Add("description");
            List<string> sTypes = new List<string>() { "DEPARTMENT", "Purpose", "Trip_Type", "Institution" };
            foreach (var t in sTypes)
            {
                LookUpTypes lt = new NaveoOneLib.Services.Lookups.LookUpTypesService().GetLookUpTypesByCode(t, sConnStr);
                if (lt != null)
                {


                    DataRow drValues = dtTypes.NewRow();
                    drValues["id"] = lt.TID;
                    drValues["description"] = lt.Code;
                    dtTypes.TableName = "lookupTypes";
                    dtTypes.Rows.Add(drValues);
                }


            }




            return dtTypes;

        }
        private DataTable getDepartmentValues(string sConnStr)
        {
            DataTable dtDeptValues = new NaveoOneLib.Services.Lookups.LookUpValuesService().GetLookUpValuesByLookUpTypeName("Department", sConnStr);
            dtDeptValues.Columns["VID"].ColumnName = "id";
            dtDeptValues.Columns["Name"].ColumnName = "description";
            dtDeptValues.Columns.Remove("TID");
            dtDeptValues.TableName = "departmentvalues";

            return dtDeptValues;
        }
        private DataTable getPurposeValues(string sConnStr)
        {
            DataTable dtpuproseValues = new NaveoOneLib.Services.Lookups.LookUpValuesService().GetLookUpValuesByLookUpTypeName("Purpose", sConnStr);
            dtpuproseValues.Columns["VID"].ColumnName = "id";
            dtpuproseValues.Columns["Name"].ColumnName = "description";
            dtpuproseValues.Columns.Remove("TID");
            dtpuproseValues.TableName = "purposeValues";

            return dtpuproseValues;
        }
        public DataTable getResources(Guid sUserToken, Driver.ResourceType rt, string sConnStr)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);

            DataTable dtResource = new NaveoOneLib.Services.Drivers.DriverService().GetDriver(lm, rt, true, sConnStr);
            DataView view = new DataView(dtResource);
            DataTable dtResult = view.ToTable(false, "DriverID", "sDriverName", "eMailAddress", "eMail");
            dtResult.Columns["DriverID"].ColumnName = "id";
            dtResult.Columns["sDriverName"].ColumnName = "description";
            dtResult.TableName = "resouces";

            foreach (DataRow dr in dtResult.Rows)
                if (String.IsNullOrEmpty(dr["eMailAddress"].ToString()))
                    dr["eMailAddress"] = dr["eMail"];
            dtResult.Columns.Remove("eMail");

            return dtResult;
        }
        public DataTable getPostHeldLookUpValues(Guid sUserToken, string sConnStr)
        {
            DataTable dtDeptValues = new NaveoOneLib.Services.Lookups.LookUpValuesService().GetLookUpValuesByLookUpTypeName("Post_Held", sConnStr);
            dtDeptValues.Columns["VID"].ColumnName = "id";
            dtDeptValues.Columns["Name"].ColumnName = "description";
            dtDeptValues.Columns.Remove("TID");
            dtDeptValues.TableName = "postHeldValues";

            return dtDeptValues;
        }
        private DataTable getBaseOfficeLookUpValues(Guid sUserToken, string sConnStr)
        {
            DataTable dtDeptValues = new NaveoOneLib.Services.Lookups.LookUpValuesService().GetLookUpValuesByLookUpTypeName("Base_Office", sConnStr);
            dtDeptValues.Columns["VID"].ColumnName = "id";
            dtDeptValues.Columns["Name"].ColumnName = "description";
            dtDeptValues.Columns.Remove("TID");
            dtDeptValues.TableName = "baseOfficeValues";

            return dtDeptValues;
        }
        #endregion

        #region ShiftDropDown
        public DataTable getShift(Guid sUserToken, string sConnstr)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnstr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnstr);

            DataTable dtShifts = new NaveoOneLib.Services.Shifts.ShiftService().GetShifts(lm, sConnstr);
            DataView view = new DataView(dtShifts);
            DataTable dtResult = view.ToTable(false, "SID", "sName");
            dtResult.Columns["SID"].ColumnName = "id";
            dtResult.Columns["sName"].ColumnName = "description";
            dtResult.TableName = "shifts";

            return dtResult;
        }

        public DataTable getShiftConcatTimeFromTo(Guid sUserToken, string sConnstr)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnstr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnstr);

            DataTable dtShifts = new NaveoOneLib.Services.Shifts.ShiftService().GetShifts(lm, sConnstr);
            DataTable dtResult = new DataTable();
            dtResult.Columns.Add("id", typeof(int));
            dtResult.Columns.Add("description", typeof(String));

            foreach (DataRow dr in dtShifts.Rows)
            {
                DateTime dtFrom = new DateTime();
                dtFrom = (DateTime)dr["TimeFr"];

                DateTime dtTo = new DateTime();
                dtTo = (DateTime)dr["TimeTo"];

                string shiftName = string.Empty;
                shiftName = dr["sName"].ToString();

                if (dr["sName"].ToString() == "D")
                {
                    shiftName = "Day Shift";
                }
                else if (dr["sName"].ToString() == "N")
                {
                    shiftName = "Night Shift";
                }
                else if (dr["sName"].ToString() == "O")
                {
                    shiftName = "OFF Shift";
                }

                string concatShiftdesc = shiftName + "(" + dtFrom.ToShortTimeString() + "-" + dtTo.ToShortTimeString() + ")";
                DataRow dr1 = dtResult.NewRow();
                dr1["id"] = dr["SID"];
                dr1["description"] = concatShiftdesc;
                dtResult.TableName = "shiftTemplateDescription";
                dtResult.Rows.Add(dr1);
            }
            return dtResult;
        }

        public DataTable getAllResources(Guid sUserToken, string sConnstr)
        {
            DataTable dtFinal = getResources(sUserToken, Driver.ResourceType.All, sConnstr);
            dtFinal.AcceptChanges();
            dtFinal.TableName = "resources";
            return dtFinal;
        }

        public DataTable getAssets(Guid sUserToken, string sConnstr)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnstr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnstr);

            DataTable dtAssets = new NaveoOneLib.Services.Assets.AssetService().GetAsset(lm, sConnstr);
            DataView view = new DataView(dtAssets);
            DataTable table2 = view.ToTable(false, "AssetID", "AssetNumber");
            table2.Columns["AssetID"].ColumnName = "id";
            table2.Columns["AssetNumber"].ColumnName = "description";
            table2.TableName = "assets";

            return table2;
        }

        public DataTable getShiftTemplate(Guid sUserToken, string sConnstr)
        {
            DataTable dtShiftTemplate = new NaveoOneLib.Services.Shifts.ShiftTemplateService().GetDistinctShiftTemplate(sConnstr);
            DataView view = new DataView(dtShiftTemplate);
            DataTable table2 = view.ToTable(false, "GroupID", "Description");
            table2.Columns["GroupID"].ColumnName = "id";
            table2.Columns["Description"].ColumnName = "description";
            table2.TableName = "shiftTemplate";
            return table2;
        }

        public DataSet getRosterDropDown(Guid sUserToken, string sConnStr)
        {
            DataSet dsRoster = new DataSet("rosterDropDownValues");

            DataTable dtShift = getShift(sUserToken, sConnStr);
            DataTable dtResources = getAllResources(sUserToken, sConnStr);
            DataTable dtAssets = getAssets(sUserToken, sConnStr);
            DataTable dtShiftTemplateDescription = getShiftConcatTimeFromTo(sUserToken, sConnStr);
            DataTable dtShiftTemplate = getShiftTemplate(sUserToken, sConnStr);
            dsRoster.Tables.Add(dtShift);
            dsRoster.Tables.Add(dtShiftTemplateDescription);
            dsRoster.Tables.Add(dtResources);
            dsRoster.Tables.Add(dtAssets);
            dsRoster.Tables.Add(dtShiftTemplate);
            return dsRoster;
        }

        #endregion
        #region CommonDropDown
        public DataTable getInstitution(string sConnStr)
        {
            DataTable dtInstitutionLookupValues = new NaveoOneLib.Services.Lookups.LookUpValuesService().GetLookUpValuesByLookUpTypeName("Institution", sConnStr);
            dtInstitutionLookupValues.Columns["VID"].ColumnName = "id";
            dtInstitutionLookupValues.Columns["Name"].ColumnName = "description";
            dtInstitutionLookupValues.Columns.Remove("TID");
            dtInstitutionLookupValues.TableName = "Institution";

            return dtInstitutionLookupValues;

        }
        #endregion
        #region Rules DropDown
        public DataTable getMaintenanceStatus(string sConnStr)
        {
            DataTable dtMaintStatus = new NaveoOneLib.Services.Assets.VehicleMaintStatusService().GetVehicleMaintStatus(sConnStr);
            //dtMaintStatus.TableName = "maintStatus";


            DataTable dt = new DataTable();
            dt.Columns.Add("id", typeof(String));
            dt.Columns.Add("description");
            dt.TableName = "maintStatus";


            foreach (DataRow dr in dtMaintStatus.Rows)
            {
                DataRow toInsert = dt.NewRow();
                toInsert["id"] = dr["MaintStatusId"];
                toInsert["description"] = dr["Description"];

                dt.Rows.Add(toInsert);

            }


            return dt;
        }
        public DataTable getAvailability(string sConnStr)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("id");
            dt.Columns.Add("description");
            dt.TableName = "availability";


            DataRow dr1 = dt.NewRow();
            dr1["id"] = 1;
            dr1["description"] = "Available";
            dt.Rows.Add(dr1);

            DataRow dr2 = dt.NewRow();
            dr2["id"] = 2;
            dr2["description"] = " Not Available";
            dt.Rows.Add(dr2);
            return dt;
        }
        public DataSet getRulesDropDown(string sConnStr)
        {
            DataSet dsRules = new DataSet("rulesDropDownValues");

            DataTable dtMaintStatus = getMaintenanceStatus(sConnStr);
            DataTable dtA = getAvailability(sConnStr);


            dsRules.Tables.Add(dtMaintStatus);
            dsRules.Tables.Add(dtA);

            return dsRules;
        }
        #endregion
        #region ZoneDropDown
        public DataTable getZoneTypes(Guid sUserToken, string sConnStr)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);

            DataTable dtZoneTypes = new NaveoOneLib.Services.Zones.ZoneTypeService().GetZoneTypes(lm, sConnStr);
            dtZoneTypes.Columns["ZoneTypeID"].ColumnName = "id";
            dtZoneTypes.Columns["sDescription"].ColumnName = "description";
            dtZoneTypes.Columns.Remove("sComments");
            dtZoneTypes.Columns.Remove("isSystem");
            dtZoneTypes.TableName = "zoneTypes";

            return dtZoneTypes;
        }

        public DataTable getCategoryLookup(Guid sUserToken, string sConnStr)
        {

            DataTable dtpuproseValues = new NaveoOneLib.Services.Lookups.LookUpValuesService().GetLookUpValuesByLookUpTypeName("Category", sConnStr);
            dtpuproseValues.Columns["VID"].ColumnName = "id";
            dtpuproseValues.Columns["Name"].ColumnName = "description";
            dtpuproseValues.Columns.Remove("TID");
            dtpuproseValues.TableName = "categoryValues";

            return dtpuproseValues;
        }


        public DataSet getzoneDropDown(Guid sUserToken, string sConnStr)
        {
            DataSet ds = new DataSet("zoneDropDown");

            DataTable dtZoneTypes = getZoneTypes(sUserToken, sConnStr);
            //DataTable dtZones = getZones(sUserToken, sConnStr);
            DataTable dtVca = getVCA(sConnStr);
            DataTable dtCategory = getCategoryLookup(sUserToken, sConnStr);
            DataTable dtSoilType = getSoilTypeValues(sConnStr);

            ds.Tables.Add(dtZoneTypes);
            ds.Tables.Add(dtVca);
            ds.Tables.Add(dtCategory);
            ds.Tables.Add(dtSoilType);

            return ds;
        }

        public DataSet getAccidentMngDropDown(Guid sUserToken, string sConnStr)
        {
            DataSet ds = new DataSet("accidentMngDropDown");

            //GetAssetId
            
            DataTable dtAsset = getAllAssets(sUserToken, sConnStr);
            //GetDriverId
            DataTable dtDriver = getResources(sUserToken, Driver.ResourceType.Driver, sConnStr);
            //get
            //GetVehicleType
            DataTable dtVehicleType = getVehicleTypes(sUserToken, sConnStr);
            //GetVehicleModel;
            DataTable dtVehicleModel = getAssetModel(sConnStr);
            dtVehicleModel.Columns.Remove("Code");
            //GetVehicleMake
            DataTable dtVehicleMake = getAssetMake(sConnStr);
            dtVehicleMake.Columns.Remove("Code");

            DataTable dtSectionType = getSectionType(sConnStr);
            dtSectionType.Columns.Remove("Code");

            DataTable dtTransport = new NaveoService.Services.ScheduleService().GetPlanningScheduledRequest(sUserToken,null, DateTime.Parse("1753/01/01"), DateTime.Parse("9999/12/31"), sConnStr, null, null, new List<string>(), false).Data;
            DataTable dtVT = dtVehicleType.Copy();

            ds.Tables.Add(dtAsset);
            ds.Tables.Add(dtDriver);
            ds.Tables.Add(dtVT);
            ds.Tables.Add(dtVehicleModel);
            ds.Tables.Add(dtVehicleMake);
            ds.Tables.Add(dtSectionType);
            ds.Tables.Add(dtTransport);

            return ds;
        }
        #endregion
        #region Permission DropDown
        public DataTable getPermissions(string sConnStr)
        {
            DataTable dtPermissions = new NaveoOneLib.Services.Permissions.PermissionsService().GetPermission(sConnStr);
            DataView view = new DataView(dtPermissions);
            DataTable table2 = view.ToTable(false, "iID", "ControllerName");
            table2.Columns["iID"].ColumnName = "id";
            table2.Columns["ControllerName"].ColumnName = "description";
            table2.TableName = "permissions";

            return table2;
        }

        #endregion



        #region Terra DropDown
        private DataSet getTerraDropDown(Guid UserToken, string sConnStr)
        {
            DataSet dsPlanning = new DataSet("terraDropDownValues");

            //DataTable dtLookTypes = getLookTypes(sConnStr);
            DataTable dtZones = getZones(UserToken, sConnStr);
            DataTable dtOperationTyep = getOperationTypeValues(sConnStr);
            DataTable dtToolType = getToolTypeValues(sConnStr);
            DataTable dtSoilType = getSoilTypeValues(sConnStr);
            DataTable dtTerraNonWorkingTypeValues = getTerraNonWorkingValues(sConnStr);
            //DataTable dtAsset = getAllAssets(UserToken,sConnStr);
            //DataTable dtDrivers = getResources(UserToken, Driver.ResourceType.Driver,sConnStr);

            dsPlanning.Tables.Add(dtZones);
            dsPlanning.Tables.Add(dtToolType);
            dsPlanning.Tables.Add(dtSoilType);
            dsPlanning.Tables.Add(dtOperationTyep);
            dsPlanning.Tables.Add(dtTerraNonWorkingTypeValues);
            //dsPlanning.Tables.Add(dtDrivers);



            return dsPlanning;
        }


        private DataTable getOperationTypeValues(string sConnStr)
        {
            DataTable dtDeptValues = new NaveoOneLib.Services.Lookups.LookUpValuesService().GetLookUpValuesByLookUpTypeName("OPERATION_TYPE", sConnStr);
            dtDeptValues.Columns["VID"].ColumnName = "id";
            dtDeptValues.Columns["Name"].ColumnName = "description";
            dtDeptValues.Columns.Remove("TID");
            dtDeptValues.TableName = "operationTypeValues";

            return dtDeptValues;
        }


        private DataTable getSoilTypeValues(string sConnStr)
        {
            DataTable dtDeptValues = new NaveoOneLib.Services.Lookups.LookUpValuesService().GetLookUpValuesByLookUpTypeName("SOIL_TYPE", sConnStr);
            dtDeptValues.Columns["VID"].ColumnName = "id";
            dtDeptValues.Columns["Name"].ColumnName = "description";
            dtDeptValues.Columns.Remove("TID");
            dtDeptValues.TableName = "soilTypeValues";

            return dtDeptValues;
        }


        private DataTable getToolTypeValues(string sConnStr)
        {
            DataTable dtDeptValues = new NaveoOneLib.Services.Lookups.LookUpValuesService().GetLookUpValuesByLookUpTypeName("TOOL_TYPE", sConnStr);
            dtDeptValues.Columns["VID"].ColumnName = "id";
            dtDeptValues.Columns["Name"].ColumnName = "description";
            dtDeptValues.Columns.Remove("TID");
            dtDeptValues.TableName = "toolTypeValues";

            return dtDeptValues;
        }
        #endregion

        public DataTable getAllAssets(Guid sUserToken, string sConnStr)
        {

            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);
            DataTable dtAssetData = new NaveoOneLib.Services.Assets.AssetService().GetAsset(lm, sConnStr);

            DataView view = new DataView(dtAssetData);
            DataTable table2 = view.ToTable(false, "AssetID", "AssetName");
            table2.Columns.Add("type", typeof(string));
            table2.Columns["AssetID"].ColumnName = "id";
            table2.Columns["AssetName"].ColumnName = "description";
            foreach (DataRow dr in table2.Rows)
            {
                dr["type"] = "Assets";
                //table2.Rows.Add(dr);
            }

            table2.TableName = "assets";

            return table2;
        }


        public DataTable getAllFixedAssets(Guid sUserToken, string sConnStr)
        {

            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);
            DataTable dtFixedAssetData = new NaveoOneLib.Services.Assets.FixedAssetService().GetFixedAsset(lm, sConnStr);

            NaveoOneLib.WebSpecifics.LibData nn = new NaveoOneLib.WebSpecifics.LibData();
            var dtGroups = nn.NaveoMasterData(sUserToken, sConnStr, NaveoModels.Groups, null, null);



            DataTable dt = new DataTable();
            dt.Columns.Add("id", typeof(int));
            dt.Columns.Add("description", typeof(string));
            dt.Columns.Add("type", typeof(string));

            //try
            //{
            //    foreach (DataRow dr in dtFixedAssetData.Rows)
            //    {
            //        int parentid = String.IsNullOrEmpty(dr["ParentGMID"].ToString()) == true ? 0 : Convert.ToInt32(dr["ParentGMID"]);
            //        int gmdid = String.IsNullOrEmpty(dr["GMID"].ToString()) == true ? 0 : Convert.ToInt32(dr["GMID"]);

            //        GroupMatrix gMatrixParent = new GroupMatrixService().GetGroupMatrixById(sUserToken, sConnStr, parentid);
            //        GroupMatrix gMatrixChild = new GroupMatrixService().GetGroupMatrixById(sUserToken, sConnStr, gmdid);


            //        DataRow row = dt.NewRow();
            //        int fixedAssetPrefix = Convert.ToInt32(dr["AssetID"]);

            //        row["id"] = fixedAssetPrefix;
            //        row["description"] =gMatrixParent.gmDescription + "-"+gMatrixChild.gmDescription + "-"+ dr["AssetCode"].ToString();

            //        dt.Rows.Add(row);
            //    }
            //    dt.TableName = "fixedAssets";
            //}

            try
            {
                foreach (DataRow dr in dtFixedAssetData.Rows)
                {
                    int parentid = String.IsNullOrEmpty(dr["ParentGMID"].ToString()) == true ? 0 : Convert.ToInt32(dr["ParentGMID"]);
                    string parentDesc = (from r in dtGroups.Data.AsEnumerable()
                                         where r.Field<int>("id") == parentid
                                         select r.Field<string>("description")).FirstOrDefault<string>();

                    DataRow row = dt.NewRow();
                    int fixedAssetPrefix = Convert.ToInt32(dr["AssetID"]);

                    row["id"] = fixedAssetPrefix;
                    row["description"] = dr["AssetType"].ToString() + "-" + dr["AssetCategory"].ToString() + "-" + dr["AssetCode"].ToString();
                    row["type"] = "FixedAsset";

                    dt.Rows.Add(row);
                }
                dt.TableName = "fixedAssets";
            }
            catch (Exception ex)
            {
            }

            return dt;
        }

        private DataTable getTerraNonWorkingValues(string sConnStr)
        {
            DataTable dtDeptValues = new NaveoOneLib.Services.Lookups.LookUpValuesService().GetLookUpValuesByLookUpTypeName("Terra_Non_Working_Values", sConnStr);
            dtDeptValues.Columns["VID"].ColumnName = "id";
            dtDeptValues.Columns["Name"].ColumnName = "description";
            dtDeptValues.Columns.Remove("TID");
            dtDeptValues.TableName = "terraNonWorkingTypeValues";

            return dtDeptValues;
        }

        public dtData MergeDT(Guid sUserToken, string sConnStr, string lstvalue)
        {

            string[] value = lstvalue.Split(',');

            dtData dt = new dtData();
            DataTable dt2 = new DataTable();

            foreach (string type in value)
            {
                dtData obj = new DropDownService().GetDropDownValues(sUserToken, sConnStr, type);
                if (obj.Data != null)
                {
                    if (obj.Data.Rows.Count > 0)
                        dt2.Merge(obj.Data);

                    dt.Data = dt2;
                    dt.Rowcnt = dt2.Rows.Count;
                }
                else
                {
                    dt = obj;
                }
            }

            return dt;
        }

        /// <summary>
        /// GetDropDownValues
        /// </summary>
        /// <param name="sUserToken"></param>
        /// <param name="sConnStr"></param>
        /// <param name="dropDownValue"></param>
        /// <returns></returns>
        public dtData GetDropDownValues(Guid sUserToken, string sConnStr, string dropDownValue)
        {
            dtData dt = new dtData();
            switch (dropDownValue)
            {
                case "AssetDropDown":
                    #region assetDropDown
                    DataSet ds = getAssetDropDown(sUserToken, sConnStr);
                    dt.Rowcnt = ds.Tables.Count;
                    dt.dsData = ds;
                    return dt;
                #endregion

                case "MatrixDropDown":
                    #region matrixDropDown
                    DataSet ds1 = getMatrixDown(sUserToken, sConnStr);
                    dt.dsData = ds1;
                    return dt;
                #endregion

                case "UserDropDown":
                    #region UserDropDown
                    DataSet dsUser = getUserDropDown(sUserToken, sConnStr);
                    dt.dsData = dsUser;
                    return dt;
                #endregion

                case "ResourceDropDown":
                    #region ResourceDropDown
                    DataSet dsResource = getResoureDropDown(sUserToken, sConnStr);
                    dt.dsData = dsResource;
                    return dt;
                #endregion

                case "PlanningDropDown":
                    #region PlanningDropDown
                    DataSet dsPlanning = getPlanningDropDown(sUserToken, sConnStr);
                    dt.dsData = dsPlanning;
                    return dt;
                #endregion

                case "RulesDropDown":
                    #region RulesDropDown
                    DataSet dsRules = getRulesDropDown(sConnStr);
                    dt.dsData = dsRules;
                    return dt;
                #endregion

                case "DepartmentValues":
                    #region DepartmentValuesDropDown
                    DataTable dtDepartmentValues = getDepartmentValues(sConnStr);
                    dt.Data = dtDepartmentValues;
                    return dt;
                #endregion

                case "TripTypeValues":
                    #region TripTypeValues
                    DataTable dtTripTypeValues = getTripTypeValues(sConnStr);
                    dt.Data = dtTripTypeValues;
                    return dt;
                #endregion

                case "InstitutionValues":
                    #region InstitutionValues
                    DataTable dtInstitutionValues = getInstitution(sConnStr);
                    dt.Data = dtInstitutionValues;
                    return dt;
                #endregion

                case "PurposeValues":
                    #region PurposeValues
                    DataTable dtPurposeValues = getPurposeValues(sConnStr);
                    dt.Data = dtPurposeValues;
                    return dt;
                #endregion

                case "Driver":
                    #region DriversDropDown
                    DataTable dtDrivers = getResources(sUserToken, Driver.ResourceType.Driver, sConnStr);
                    dt.Data = dtDrivers;
                    return dt;
                #endregion

                case "Officer":
                    #region OfficerDropDown
                    DataTable dtOfficer = getResources(sUserToken, Driver.ResourceType.Officer, sConnStr);
                    dt.Data = dtOfficer;
                    return dt;
                #endregion

                case "Loader":
                    #region LoaderDropDown
                    DataTable dtLoader = getResources(sUserToken, Driver.ResourceType.Loader, sConnStr);
                    dt.Data = dtLoader;
                    return dt;
                #endregion

                case "Attendant":
                    #region AttendantDropDown
                    DataTable dtAttendant = getResources(sUserToken, Driver.ResourceType.Attendant, sConnStr);
                    dt.Data = dtAttendant;
                    return dt;
                #endregion

                case "Other":
                    #region OtherDropDown
                    DataTable dtOther = getResources(sUserToken, Driver.ResourceType.Other, sConnStr);
                    dt.Data = dtOther;
                    return dt;
                #endregion

                case "RosterDropDown":
                    #region RosterDropDown
                    DataSet dsRoster = getRosterDropDown(sUserToken, sConnStr);
                    dt.dsData = dsRoster;
                    return dt;
                #endregion

                case "ZoneDropDown":
                    #region ZoneDropDown
                    DataSet dsZones = getzoneDropDown(sUserToken, sConnStr);
                    dt.dsData = dsZones;
                    return dt;
                #endregion
                case "AccidentMngDropDown":
                    #region AccidentMngDropDown
                    DataSet dsAccident = getAccidentMngDropDown(sUserToken, sConnStr);
                    dt.dsData = dsAccident;
                    return dt;
                #endregion

                case "PermissionDropDown":
                    #region ZoneDropDown
                    DataTable dtPermissons = getPermissions(sConnStr);
                    dt.Data = dtPermissons;
                    return dt;
                #endregion

                case "Assets":
                    #region Assets
                    DataTable dtAssets = getAllAssets(sUserToken, sConnStr);
                    dt.Data = dtAssets;
                    dt.Rowcnt = dtAssets.Rows.Count;
                    return dt;
                #endregion

                case "Resources":
                    #region Resources
                    DataTable dtResources = getAllResources(sUserToken, sConnStr);
                    dt.Data = dtResources;
                    dt.Rowcnt = dtResources.Rows.Count;
                    return dt;
                #endregion

                case "TerraPlanningDropDown":
                    #region Resources
                    DataSet dsTerraPlanning = getTerraDropDown(sUserToken, sConnStr);
                    dt.dsData = dsTerraPlanning;
                    dt.Rowcnt = dsTerraPlanning.Tables.Count;
                    return dt;
                #endregion

                case "FixedAssets":
                case "FixedAsset":
                    #region FixedAssets
                    DataTable dtFixedAsets = getAllFixedAssets(sUserToken, sConnStr);
                    dt.Data = dtFixedAsets;
                    dt.Rowcnt = dtFixedAsets.Rows.Count;
                    return dt;
                #endregion

                default:
                    return dt;
            }
        }
    }
}