﻿using NaveoOneLib.Models;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Services.Permissions;
using NaveoService.Constants;
using NaveoService.Helpers;
using NaveoService.Models;
using NaveoService.Models.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace NaveoService.Services
{
    public class GroupMatrixService
    {
        /// <summary>
        /// Deletes a group matrix
        /// </summary>
        /// <param name="groupMatrix"></param>
        /// <param name="sConnStr"></param>
        /// <returns></returns>
        /// TODO: [Aboo to Reza] Should pass a key for deletion
        public BaseDTO Delete(GroupMatrix groupMatrix, String sConnStr)
        {
            bool isgroupMatrixDeleted = new NaveoOneLib.Services.GMatrix.GroupMatrixService().DeleteGroupMatrix(groupMatrix, sConnStr);
            return isgroupMatrixDeleted.ToBaseDTO();
        }

        public DataTable GetNaveoModules(Guid gUserToken, String sConnStr)
        {
            DataTable dt = new NaveoModulesService().GetNaveoModules(gUserToken, sConnStr);
            return dt;
        }

        //public dtData GetMatrixData(Guid sUserToken, string sConnStr)
        //{
        //    int UID = -1;
        //    List<int> lValidatedMatrix = CommonService.lValidateMatrix(securityToken.securityToken.UserToken, p, securityToken.securityToken.sConnStr, out UID);
        //    DataTable dtMatrixValues = new NaveoOneLib.Services.GMatrix.GroupMatrixService().GetAllGMValues(lValidatedMatrix,)


        //    dtData dtR = new dtData();
        //    dtR.Data = dt;
        //    dtR.Rowcnt = iTotalRec;

        //    return dtR;

        //}
        public GroupMatrix GetGroupMatrixById(Guid sUserToken, string sConnStr, int gmId)
        {

            GroupMatrix gM = new NaveoOneLib.Services.GMatrix.GroupMatrixService().GetGroupMatrixById(gmId, sConnStr);


            return gM;
        }

        /// <summary>
        /// Save group matrix
        /// </summary>
        /// <param name="groupMatrix"></param>
        /// <param name="bInsert"></param>
        /// <param name="sConnStr"></param>
        /// <returns></returns>
         public BaseDTO SaveGroupMatrix(SecurityTokenExtended securityToken, GroupMatrix groupMatrix, Boolean bInsert)
        {
            //TODO:Pravish > Validations. Chk if user has access to ParentGMID

            int parentGMID = (int)groupMatrix.parentGMID;
            List<int> p = new List<int>();
            p.Add(parentGMID);

            int UID = -1;
            List<int> lValidatedMatrix = CommonService.lValidateMatrix(securityToken.securityToken.UserToken,p, securityToken.securityToken.sConnStr, out UID);

            var xx = groupMatrix.lNaveoModules;
            foreach (NaveoModule x in groupMatrix.lNaveoModules)

            {

                DataTable dt = new NaveoOneLib.Services.Permissions.NaveoModulesService().GetNaveoModuleFromMatrixById(x.iID.ToString(), groupMatrix.gmID.ToString(),securityToken.securityToken.sConnStr);

                if(dt.Rows.Count > 0)
                {
                    int mid = (int)dt.Rows[0][0];
                    x.mID = mid;
                    
                }

                

            }

            BaseModel groupMatrixObj = new BaseModel();
            if (lValidatedMatrix.Count == 0)
               {
                  groupMatrixObj.data = false;
                  groupMatrixObj.errorMessage = "User does not have access to  parentGMID :"+groupMatrix.parentGMID+" ";
                  return groupMatrixObj.ToBaseDTO(securityToken.securityToken);
               }

            groupMatrixObj = new NaveoOneLib.Services.GMatrix.GroupMatrixService().SaveGroupMatrix(groupMatrix, bInsert, securityToken.securityToken.sConnStr);
            return groupMatrixObj.ToBaseDTO(securityToken.securityToken);
        }

         public dtData GetMatrixData(Guid sUserToken, List<int> lMatrixIDs, string sConnStr, int? Page, int? LimitPerPage)
          {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);

            DataTable dtMatrixData = new NaveoOneLib.WebSpecifics.LibData().dtGMMatrix(UID, sConnStr);

            if (dtMatrixData.Columns.Contains("gmid"))
                dtMatrixData.Columns["gmid"].ColumnName = "GMID";

            if (dtMatrixData.Columns.Contains("gmdescription"))
                dtMatrixData.Columns["gmdescription"].ColumnName = "GMDescription";




            DataTable ResultTable = dtMatrixData.Clone();
            foreach (DataRow dr in dtMatrixData.Rows)
                foreach (int i in lMatrixIDs)
                    if (Convert.ToInt32(dr["GMID"]) == i)
                        ResultTable.Rows.Add(dr.ItemArray);

            int iTotalRec = ResultTable.Rows.Count;
            dtData dtR = new dtData();
            dtR.Data = ResultTable;
            dtR.Rowcnt = iTotalRec;

            return dtR;
        }


        public dtData  GetMatrixLegalName(Guid sUserToken,string sConnStr)
        {
            dtData bd = new dtData();

            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);
            int gmId = lm[0].GMID;
            DataTable dt = new NaveoOneLib.Services.GMatrix.GroupMatrixService().GetMatrixLegalName(gmId, sConnStr);

            bd.Data = dt;
            return bd;
        }


    }
}