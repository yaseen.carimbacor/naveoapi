﻿using NaveoOneLib.Models.Common;
using NaveoOneLib.Services.GMatrix;
using NaveoService.Models.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Shifts;

namespace NaveoService.Services
{
    public class ShiftService
    {

        public dtData GetShifts(Guid sUserToken, string sConnStr, int? Page, int? LimitPerPage)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);

            DataTable dtShifts = new NaveoOneLib.Services.Shifts.ShiftService().GetShifts(lm, sConnStr);

            int count = dtShifts.Rows.Count;

            DataTable ResultTable = dtShifts.Clone();
            ResultTable.Columns.Add("rowNum", typeof(int));
            ResultTable.Columns["rowNum"].AutoIncrement = true;
            ResultTable.Columns["rowNum"].AutoIncrementSeed = 1;
            ResultTable.Columns["rowNum"].AutoIncrementStep = 1;
            foreach (DataRow dr in dtShifts.Rows)
                ResultTable.Rows.Add(dr.ItemArray);

            int iTotalRec = ResultTable.Rows.Count;
            Pagination p = Pagination.GetRowNums(Page, LimitPerPage);
            DataTable dt = new DataTable();
            DataRow[] drChk = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo);
            if (drChk != null && drChk.Length > 0)
                dtShifts = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo).CopyToDataTable();
            else
                dtShifts = ResultTable;

            if (dtShifts.Columns.Contains("sid"))
            {
                dtShifts.Columns["sid"].ColumnName = "SID";
            }

            if (dtShifts.Columns.Contains("sname"))
            {
                dtShifts.Columns["sname"].ColumnName = "sName";
            }

            if (dtShifts.Columns.Contains("scomments"))
            {
                dtShifts.Columns["scomments"].ColumnName = "sComments";
            }

            if (dtShifts.Columns.Contains("timefr"))
            {
                dtShifts.Columns["timefr"].ColumnName = "TimeFr";
            }

            if (dtShifts.Columns.Contains("timeto"))
            {
                dtShifts.Columns["timeto"].ColumnName = "TimeTo";
            }

            dtData dtR = new dtData();
            dtR.Data = dtShifts;
            dtR.Rowcnt = iTotalRec;

            return dtR;
        }

        public BaseModel GetShiftById(Guid sUserToken, int SID, string sConnStr)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);

            NaveoOneLib.Models.Shifts.Shift shift = new  NaveoOneLib.Services.Shifts.ShiftService().GetShiftById(SID, sConnStr);
           

            BaseModel dtR = new BaseModel();
            dtR.data = shift;
            return dtR;
        }


        public Boolean SaveUpdateShift(Guid sUserToken, string sConnStr, ShiftDTO shiftDTO, Boolean bInsert)
        {

     
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);

            #region Matrix
            if (bInsert)
            {
                List<NaveoOneLib.Models.GMatrix.Matrix> lmatrix = new List<NaveoOneLib.Models.GMatrix.Matrix>();
                foreach (NaveoOneLib.Models.GMatrix.Matrix mm in lm)
                {


                    NaveoOneLib.Models.GMatrix.Matrix m = new NaveoOneLib.Models.GMatrix.Matrix();
                    m.GMDesc = mm.GMDesc;
                    m.GMID = mm.GMID;
                    m.iID = shiftDTO.shift.SID;
                    m.MID = 0;
                    m.oprType = DataRowState.Added;
                    lmatrix.Add(m);


               
                }

                shiftDTO.shift.lMatrix = lmatrix;
            }
            else
            {

                foreach (NaveoOneLib.Models.GMatrix.Matrix mm in lm)
                {
                    foreach (var apiMatrix in shiftDTO.shift.lMatrix)
                    {

                        NaveoOneLib.Models.GMatrix.Matrix m = new NaveoOneLib.Models.GMatrix.Matrix();
                        m.GMDesc = mm.GMDesc;
                        m.GMID = mm.GMID;
                        m.iID = mm.iID;
                        m.MID = mm.MID;

                        if (apiMatrix.GMID != mm.GMID)
                        {

                            m.oprType = DataRowState.Modified;


                        }
                        else
                        {

                            m.oprType = DataRowState.Unchanged;
                        }


                        shiftDTO.shift.lMatrix.Add(m);

                    }




                }

            }
            #endregion


            Boolean bSaveUpdate = new NaveoOneLib.Services.Shifts.ShiftService().SaveShift(shiftDTO.shift, bInsert, sConnStr);
            BaseModel bD = new BaseModel();
            bD.data = bSaveUpdate;
            return bD.data;

        }

        public Boolean DeleteShift(Guid sUserToken, string sConnStr, int sid)
        {

            Shift shift = new Shift();
            shift.SID = sid;

            Boolean deleteShift = new NaveoOneLib.Services.Shifts.ShiftService().DeleteShift(sUserToken, shift, sConnStr);

            BaseModel bD = new BaseModel();
            bD.data = deleteShift;
            return bD.data;

        }
    }
}
