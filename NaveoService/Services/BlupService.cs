﻿using NaveoOneLib.Models.Common;
using NaveoService.Helpers;
using NaveoService.Models;
using NaveoService.Models.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Services
{
    public class BlupService
    {
        public BaseModel GetBlupByLonLat(Guid sUserToken, int iDistance, Double lon, Double lat, String sConnStr)
        {
            NaveoOneLib.Services.BLUP.BLUPService blupSerice = new NaveoOneLib.Services.BLUP.BLUPService();
            DataSet ds = new DataSet();
            DataTable dtBlupData = blupSerice.dtGetBlupWithinDistanceInMeters(sUserToken, iDistance, lon, lat, sConnStr);
            if (dtBlupData.Rows.Count > 0)
            {
                dtBlupData.DefaultView.Sort = "distance asc";
                dtBlupData = dtBlupData.DefaultView.ToTable();

                int i = Convert.ToInt32(dtBlupData.Rows[0]["iID"].ToString());
                ds = blupSerice.GetBlupByiID(i, sConnStr);
            }
            BaseModel bM = new BaseModel();
            bM.data = ds;
            return bM;
        }
        public BaseModel GetBlupContextByLonLat(Guid sUserToken, int iDistance, Double lon, Double lat, String sConnStr)
        {
            NaveoOneLib.Services.BLUP.BLUPService blupSerice = new NaveoOneLib.Services.BLUP.BLUPService();
            DataTable dtBlupData = blupSerice.dtGetBlupContextWithinDistanceInMeters(sUserToken, iDistance, lon, lat, sConnStr);
            BaseModel bm = new BaseModel();
            bm.data = dtBlupData;
            return bm;
        }

        public BaseModel GetBlupWithinDistanceInMeters(Guid sUserToken, int iDistance, Double lon, Double lat, String sConnStr)
        {
            DataTable dtBlupData = new NaveoOneLib.Services.BLUP.BLUPService().dtGetBlupWithinDistanceInMeters(sUserToken, iDistance, lon, lat, sConnStr);
            String responseString = NaveoOneLib.Maps.NaveoWebMapsCall.GetFADataFromMapServer(dtBlupData, lon, lat, true, "Blup/CreateBlupWithinDistanceInMeters", sConnStr);

            BaseModel bM = new BaseModel();
            bM.data = responseString;
            return bM;
        }
        public BaseModel GetBLUPDataByIds(Guid UserToken, List<int> lids, Boolean bShowBuffers, string sConnStr)
        {
            BaseModel bm = new BaseModel();
            int iBuffer = 0;
            if (bShowBuffers)
                iBuffer = 300;
            DataTable dtBlup = new NaveoOneLib.Services.BLUP.BLUPService().dtGetBlupWithinDistanceInMeters(UserToken, iBuffer, lids, sConnStr);

            String responseString = NaveoOneLib.Maps.NaveoWebMapsCall.GetFADataFromMapServer(dtBlup, 0, 0, false, "Blup/CreateBlupWithinDistanceInMeters", sConnStr);
            bm.data = responseString;

            return bm;
        }
        public dtData GetTree()
        {
            DataTable dtBlupTree = new NaveoOneLib.Services.BLUP.BLUPService().dtTree();

            dtData dt = new dtData();
            dt.Data = dtBlupTree;
            dt.Rowcnt = dtBlupTree.Rows.Count;
            return dt;
        }

        public BaseModel GetBLUPByIds(Guid UserToken, int AssetID, string sConnStr)
        {
            BaseModel bm = new BaseModel();
            DataSet ds = new NaveoOneLib.Services.BLUP.BLUPService().GetBlupByiID(AssetID, sConnStr);
            bm.data = ds;
            return bm;
        }

        public BaseDTO SaveUpdateBlup(SecurityTokenExtended securityToken, BlupDTO blupDTO, Boolean bInsert)
        {
            BaseModel assetObj = new NaveoOneLib.Services.BLUP.BLUPService().SaveBLUP(blupDTO.blup, securityToken.securityToken.UID, bInsert, securityToken.securityToken.sConnStr);
            return assetObj.ToBaseDTO(securityToken.securityToken);
        }

        #region Nels Test
        public BaseModel GetBLUPByIds(String nelsID, string sConnStr)
        {
            BaseModel bm = new BaseModel();
            DataSet ds = new NaveoOneLib.Services.BLUP.BLUPService().GetBlupByAplID(nelsID, sConnStr);
            bm.data = ds;
            return bm;
        }
        #endregion
    }
}
