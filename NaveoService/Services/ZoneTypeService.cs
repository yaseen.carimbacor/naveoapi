﻿using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.Zones;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Services
{
   public  class ZoneTypeService
    {

        public dtData GetZoneListFromZoneIDs(Guid sUserToken, List<int> lZoneIDs, string sConnStr, int? Page, int? LimitPerPage)
        {
            int UID = -1;
            List<int> lValidatedZones = CommonService.lValidateZoneTpes(sUserToken, lZoneIDs, sConnStr, out UID);

            dtData zd = new NaveoOneLib.Services.Zones.ZoneTypeService().GetZoneListFromZoneIDs(sUserToken, lValidatedZones, sConnStr, Page, LimitPerPage);


            //dtPermissions = NaveoServiceUtils.dtLowerColumns(dtPermissions);

            return zd;
        }


        public dtData GetZoneTypeById(Guid sUserToken, string sConnStr,int zoneTypeID)
        {

            dtData dt = new  dtData();

            DataSet ds = new NaveoOneLib.Services.Zones.ZoneTypeService().GetZoneTypeID(zoneTypeID, sConnStr);

            dt.dsData = ds;

            return dt;
        }


        public Boolean SaveUpdate(ZoneType uZoneType, Boolean bInsert, String sConnStr)
        {

            BaseModel dt = new BaseModel();






            Boolean b = new NaveoOneLib.Services.Zones.ZoneTypeService().SaveZoneType(uZoneType, bInsert, sConnStr);


            return b;

        }


    }
}
