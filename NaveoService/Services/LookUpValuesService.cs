﻿
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.GMatrix;

using System;
using System.Collections.Generic;
using System.Data;
using NaveoOneLib.Services.GMatrix;
using NaveoService.Models.DTO;
using NaveoService.Helpers;
using NaveoService.Models.Custom;
using NaveoService.Models;
using NaveoOneLib.Constant;
using System.Net;
using Newtonsoft.Json;
using System.Text;
using NaveoOneLib.Models.Users;
using NaveoOneLib.Models.Lookups;

namespace NaveoService.Services
{
    public class LookUpValuesService
    {

        public String UploadLookUpValues(SecurityTokenExtended securityToken, DataTable dt)
        {

            int UID = CommonService.GetUserID(securityToken.securityToken.UserToken, securityToken.securityToken.sConnStr);

            Boolean lk = false;
            Boolean bTF = false;
            string countSuccess = string.Empty;
            int countCreated = 0;
            int countUpdated = 0;
            string lookupType = string.Empty;

            lookupType = dt.Rows[0][1].ToString();
            LookUpTypes lookUpType = new NaveoOneLib.Services.Lookups.LookUpTypesService().GetLookUpTypesByCode(lookupType, securityToken.securityToken.sConnStr);



            string statusOfCreated = string.Empty;
            string statusOfUpdated = string.Empty;

            foreach (DataRow dr in dt.Rows)
            {

                LookUpValues lookupValues = new NaveoOneLib.Services.Lookups.LookUpValuesService().GetLookUpValuesByName(dr["LookUpValue"].ToString(), securityToken.securityToken.sConnStr);


                string modifiedDescription = string.Empty;



                string concatValue = string.Empty;

                if (dr["LookUpCode"].ToString() != string.Empty)
                {
                    concatValue = dr["LookUpValue"].ToString() + "-@-" + dr["LookUpCode"].ToString();
                    modifiedDescription = concatValue;
                }
                else
                {
                    
                    modifiedDescription = dr["LookUpValue"].ToString();
                }
                   

                LookUpValues lV = new LookUpValues();
                if (lookupValues != null)
                {

                    lV.VID = lookupValues.VID;
                    lV.TID = lookUpType.TID;
                    lV.Name = dr["LookUpValue"].ToString();
                    lV.Description = modifiedDescription;
                    lV.Value = String.IsNullOrEmpty(dr["Value"].ToString()) ? 0.0 : Convert.ToDouble(dr["Value"]);
                    lV.Value2 = String.IsNullOrEmpty(dr["Value2"].ToString()) ? 0.0 : Convert.ToDouble(dr["Value2"]);
                    lV.UpdatedDate = DateTime.Now;
                    lV.UpdatedBy = UID;
                    lV.CreatedBy = lookupValues.CreatedBy;
                    lV.CreatedDate = lookupValues.CreatedDate;

                    statusOfUpdated = "updated";
                    countUpdated++;

                    bTF = false;
                } else
                {
                    
                    lV.TID = lookUpType.TID;
                    lV.Name = dr["LookUpValue"].ToString();
                    lV.Description = modifiedDescription;
                    lV.Value = String.IsNullOrEmpty(dr["Value"].ToString()) ? 0.0 : Convert.ToDouble(dr["Value"]);
                    lV.Value2 = String.IsNullOrEmpty(dr["Value2"].ToString()) ? 0.0 : Convert.ToDouble(dr["Value2"]);
                    lV.CreatedDate = DateTime.Now;
                    lV.CreatedBy = UID;
                    lV.UpdatedDate = lookUpType.UpdatedDate;
                    lV.UpdatedBy = lookUpType.UpdatedBy;

                    statusOfCreated = "created";
                    countCreated++;
                    bTF = true;
                }

               

               
            


                lk = new NaveoOneLib.Services.Lookups.LookUpValuesService().SaveLookUpValues(lV, bTF, securityToken.securityToken.sConnStr);
                

                if (lk == false)
                    return "Error in creating the lookupValue " + dr["Name"].ToString();

            }


            countSuccess = countCreated + " LookupValues has been "+statusOfCreated+" and "+countUpdated + " LookupValues has been "+statusOfUpdated+"  under the LookType " + lookupType;

            return countSuccess;
        }

    }
}
