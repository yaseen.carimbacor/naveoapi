﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NaveoOneLib.Models;
using NaveoService.Models;
using NaveoOneLib.Services.Rules;
using NaveoOneLib.Maps;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Models.Common;
using NaveoService.Models.DTO;
using OfficeOpenXml.FormulaParsing.Utilities;
using NaveoOneLib.DBCon;

namespace NaveoService.Services
{
    public class ExceptionService
    {
        #region Methods
        /// <summary>
        /// Datatable Alerts
        /// </summary>
        /// <param name="sUserToken"></param>
        /// <param name="lAssetIDs"></param>
        /// <param name="dtFr"></param>
        /// <param name="dtTo"></param>
        /// <param name="sConnStr"></param>
        /// <returns></returns>zones
        public Alert dtAlerts(Guid sUserToken, List<int> lAssetIDs, DateTime dtFr, DateTime dtTo, Boolean bShowZones, String sConnStr)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);
            List<int> lRules = new List<int>();

            DataTable dtRules = new RulesService().GetRules(lm, string.Empty, sConnStr);

            foreach (DataRow dr in dtRules.Rows)
            {
                int ruleId = Convert.ToInt32(dr["ParentRuleID"]);
                lRules.Add(ruleId);
            }


            DataSet dsAlerts = new ExceptionsService().GetExceptions(sUserToken, lAssetIDs, lRules, dtFr, dtTo, false, false, sConnStr);

            Alert a = new Models.Alert();
            a.dtAlerts = dsAlerts.Tables[0];
            return a;
        }
        public DataSet GetExceptions(Guid sUserToken, List<int> lAssetIDs, List<int> lRules, DateTime dtFrom, DateTime dtTo, Boolean bDriver, Boolean bAddress, Boolean bShowZones, String sConnStr)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);

            List<int> lValidatedRules = CommonService.lValidateRules(sUserToken, lRules, sConnStr, out UID);

            DataSet ds = new ExceptionsService().GetExceptions(sUserToken, lAssetIDs, lValidatedRules, dtFrom, dtTo, bDriver, bShowZones, sConnStr);

            foreach (DataRow row in ds.Tables["dtExceptionHeader"].Rows)
            {
                Connection c = new Connection(sConnStr);
                DataTable dtSql = c.dtSql();
                DataRow drSql = dtSql.NewRow();

                String sql = new NaveoOneLib.Services.GMatrix.GroupMatrixService().sGetAllParentsDescfromGMID(Convert.ToInt32(row["ParentGMID"].ToString()));
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dt";
                dtSql.Rows.Add(drSql);

                DataSet dsFinal = c.GetDataDS(dtSql, sConnStr);
                DataRow[] drChkURITrue = dsFinal.Tables["dt"].Select("level = " + 4);
                if (drChkURITrue.Length > 0)
                {
                    DataRow rowTrue = drChkURITrue[0];

                    row["sSite"] = rowTrue["GMDescription"];
                }

            }

            if (ds.Tables["dtExceptionHeader"].Columns.Contains("assetid"))
                ds.Tables["dtExceptionHeader"].Columns["assetid"].ColumnName = "AssetID";

            if (ds.Tables["dtExceptionHeader"].Columns.Contains("assetname"))
                ds.Tables["dtExceptionHeader"].Columns["assetname"].ColumnName = "assetName";

            if (ds.Tables["dtExceptionHeader"].Columns.Contains("ruldeid"))
                ds.Tables["dtExceptionHeader"].Columns["ruldeid"].ColumnName = "ruleID";

            if (ds.Tables["dtExceptionHeader"].Columns.Contains("rulename"))
                ds.Tables["dtExceptionHeader"].Columns["rulename"].ColumnName = "ruleName";

            if (ds.Tables["dtExceptionHeader"].Columns.Contains("datefrom"))
                ds.Tables["dtExceptionHeader"].Columns["datefrom"].ColumnName = "dateFrom";

            if (ds.Tables["dtExceptionHeader"].Columns.Contains("dateto"))
                ds.Tables["dtExceptionHeader"].Columns["dateto"].ColumnName = "dateTo";


            if (ds.Tables["dtExceptionHeader"].Columns.Contains("groupid"))
                ds.Tables["dtExceptionHeader"].Columns["groupid"].ColumnName = "groupID";


            if (ds.Tables["dtExceptionHeader"].Columns.Contains("dtlocalfrom"))
                ds.Tables["dtExceptionHeader"].Columns["dtlocalfrom"].ColumnName = "dtLocalFrom";


            if (ds.Tables["dtExceptionHeader"].Columns.Contains("dtlocaltrom"))
                ds.Tables["dtExceptionHeader"].Columns["dtlocaltrom"].ColumnName = "dtLocalTo";

            if (ds.Tables["dtExceptionHeader"].Columns.Contains("datetimetolocal"))
                ds.Tables["dtExceptionHeader"].Columns["dateTimeToLocal"].ColumnName = "dateTimeToLocal";

            if (ds.Tables["dtExceptionHeader"].Columns.Contains("szones"))
                ds.Tables["dtExceptionHeader"].Columns["szones"].ColumnName = "sZones";

            if (ds.Tables["dtExceptionHeader"].Columns.Contains("driverid"))
                ds.Tables["dtExceptionHeader"].Columns["driverid"].ColumnName = "driverID";

            if (ds.Tables["dtExceptionHeader"].Columns.Contains("sdrivername"))
                ds.Tables["dtExceptionHeader"].Columns["sdrivername"].ColumnName = "sDriverName";

            if (ds.Tables["dtExceptionHeader"].Columns.Contains("maxspeed"))
                ds.Tables["dtExceptionHeader"].Columns["maxspeed"].ColumnName = "maxSpeed";

            if (ds.Tables["dtExceptionHeader"].Columns.Contains("roadspeed"))
                ds.Tables["dtExceptionHeader"].Columns["roadspeed"].ColumnName = "roadSpeed";

            if (ds.Tables["dtExceptionHeader"].Columns.Contains("dateonly"))
                ds.Tables["dtExceptionHeader"].Columns["dateonly"].ColumnName = "DateOnly";

            if (ds.Tables["dtExceptionHeader"].Columns.Contains("timeonly"))
                ds.Tables["dtExceptionHeader"].Columns["timeonly"].ColumnName = "TimeOnly";

            if (ds.Tables["dtExceptionHeader"].Columns.Contains("ssite"))
                ds.Tables["dtExceptionHeader"].Columns["ssite"].ColumnName = "sSite";





            DataTable dtReport = ds.Tables["dtExceptionHeader"];
            DataTable dtDetails = new DataTable();// ds.Tables["dtExceptionDetails"];
            dtDetails.TableName = "dtExceptionDetails";
            if (bAddress)
            {
                dtReport.Columns.Add("address");
                DataRow[] drAddress = dtReport.Select();
                SimpleAddress.GetAddresses(drAddress, "address", "lon", "lat");
            }

            DataSet dsResult = new DataSet();
            dsResult.Merge(dtReport);
            dsResult.Merge(dtDetails);
            return dsResult;
        }

        //Get all rules base on Struc Planning
        //get distinct parentruleid
        //get results of all parentruleid --Asset, planning conditions
        //take assetids, planning rule condition and rule name pass it in generateAlertplanning function to get exception data



        public DataTable GetPlanningExceptions(Guid sUserToken, List<int> lAssetIDs, List<int> lRules, DateTime dtFrom, DateTime dtTo, Boolean bDriver, String sConnStr)
        {

            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);

            List<int> lValidatedRules = CommonService.lValidateRules(sUserToken, lRules, sConnStr, out UID);

            DataTable dt = new ExceptionsService().GetPlanningExceptionsReport(lAssetIDs, lRules, dtFrom, dtTo, bDriver, sConnStr);

            if (dt.Columns.Contains("iid"))
                dt.Columns["iid"].ColumnName = "iID";

            if (dt.Columns.Contains("ruleName"))
                dt.Columns["ruleName"].ColumnName = "RuleName";

            if (dt.Columns.Contains("asset"))
                dt.Columns["asset"].ColumnName = "Asset";

            if (dt.Columns.Contains("driver"))
                dt.Columns["driver"].ColumnName = "Driver";

            if (dt.Columns.Contains("viapoint"))
                dt.Columns["viapoint"].ColumnName = "ViaPoint";

            if (dt.Columns.Contains("expectedtime"))
                dt.Columns["expectedtime"].ColumnName = "ExpectedTime";


            if (dt.Columns.Contains("buffer"))
                dt.Columns["buffer"].ColumnName = "Buffer";

            if (dt.Columns.Contains("actualTime"))
                dt.Columns["actualTime"].ColumnName = "ActualTime";

            if (dt.Columns.Contains("durations"))
                dt.Columns["durations"].ColumnName = "Durations";

            if (dt.Columns.Contains("status"))
                dt.Columns["status"].ColumnName = "Status";

            return dt;


        }


        public DataTable GetFuelExceptions(Guid sUserToken, List<int> lAssetIDs, List<int> lRules, DateTime dtFrom, DateTime dtTo, Boolean bDriver, String sConnStr)
        {

            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);

            List<int> lValidatedRules = CommonService.lValidateRules(sUserToken, lRules, sConnStr, out UID);

            DataTable dt = new ExceptionsService().GetFuelExceptionsReport(lAssetIDs, lRules, dtFrom, dtTo, bDriver, sConnStr);
            dt.Columns.Add("RuleName");




            //458
            //459

            int ruleId = lRules[0];

            String status = string.Empty;
            if (ruleId == 458)
                status = "Fill";
            else
                status = "Drop";


            DataTable tblFiltered = (dt.AsEnumerable()
          .Where(row => row.Field<String>("ConditionType") == status).CopyToDataTable());


            foreach (DataRow dr in tblFiltered.Rows)
            {
                    if (ruleId == 458)
                        dr["RuleName"] = "Fuel Fill Detected";
                    else
                        dr["RuleName"] = "Fuel Drop Suspected";
            }

            if (true)
            {
                tblFiltered.Columns.Add("address");
                DataRow[] drAddress = tblFiltered.Select();
                SimpleAddress.GetAddresses(drAddress, "address", "lon", "lat");
            }



            if (tblFiltered.Columns.Contains("gpsuid"))
                tblFiltered.Columns["gpsuid"].ColumnName = "gpsUID";

            if (tblFiltered.Columns.Contains("ruleName"))
                tblFiltered.Columns["ruleName"].ColumnName = "RuleName";

            if (tblFiltered.Columns.Contains("asset"))
                tblFiltered.Columns["asset"].ColumnName = "Asset";

            if (tblFiltered.Columns.Contains("assetid"))
                tblFiltered.Columns["assetid"].ColumnName = "assetID";


            if (tblFiltered.Columns.Contains("convertedvalue"))
                tblFiltered.Columns["convertedvalue"].ColumnName = "convertedValue";


            if (tblFiltered.Columns.Contains("conditiontype"))
                tblFiltered.Columns["conditiontype"].ColumnName = "ConditionType";

            if (tblFiltered.Columns.Contains("differenceinlitres"))
                tblFiltered.Columns["differenceinlitres"].ColumnName = "DifferenceInLitres";

            if (tblFiltered.Columns.Contains("zone"))
                tblFiltered.Columns["zone"].ColumnName = "Zone";

            return tblFiltered;


        }


        #endregion
    }
}