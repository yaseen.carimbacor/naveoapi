﻿using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.Plannings;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Services.Lookups;
using NaveoOneLib.Services.Trips;
using NaveoService.Helpers;
using NaveoService.Models;
using NaveoService.Models.DTO;
using OfficeOpenXml.FormulaParsing.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Services
{
    public class CustomTripService
    {


        public BaseDTO SaveCustomTrips(SecurityTokenExtended securityToken, CustomizedTripsDTO customizedTripsDTO, Boolean bInsert)
        {


            int UID = CommonService.GetUserID(securityToken.securityToken.UserToken, securityToken.securityToken.sConnStr);

            //Field2 -HectarePlanned ,, Field3 -hectareCovered,,Field4 -OverTime ,,Field5 - Other ,,Field6 - Normal working hours

            List<string> lStopConstants = new List<string>();
            lStopConstants.Add(NaveoOneLib.Constant.NaveoEntity.TerraStopConstantLunch);
            lStopConstants.Add(NaveoOneLib.Constant.NaveoEntity.TerraStopConstantTeaBreak1);
            lStopConstants.Add(NaveoOneLib.Constant.NaveoEntity.TerraStopConstantTeaBreak2);

            //var result = customizedTripsDTO.customTrips.Where(e => lStopConstants.Contains(e.Description));

            foreach (var a in customizedTripsDTO.customTrips)
            {
                if (a.PID == 0)
                    a.PID = null;


                if (a.Description != null)
                {


                    var stopConstantLower = a.Description.ToLower();
                    var stopConstantUpper = a.Description.ToUpper();

                    if (lStopConstants.Contains(stopConstantLower) || lStopConstants.Contains(stopConstantUpper))
                    {
                        a.Field1 = "0.0";
                        a.Field3 = "0.0";
                    }

                }

            }


            BaseModel customTripsObj = new NaveoOneLib.Services.Plannings.CustomizedTripsService().SaveCustomizedTrips(customizedTripsDTO.customTrips, UID, bInsert, securityToken.securityToken.sConnStr);
            return customTripsObj.ToBaseDTO(securityToken.securityToken);
        }


        public BaseDTO SaveMokaCustomTrips(SecurityTokenExtended securityToken, CustomizedTripsDTO customizedTripsDTO, Boolean bInsert)
        {


            int UID = CommonService.GetUserID(securityToken.securityToken.UserToken, securityToken.securityToken.sConnStr);

            foreach(var a in customizedTripsDTO.customTrips)
            {
                if (a.IsStopped == null)
                    a.IsStopped = 0;

                if (a.CountAsWorkHour == null)
                    a.CountAsWorkHour = 0;

            }

            BaseModel customTripsObj = new NaveoOneLib.Services.Plannings.CustomizedTripsService().SaveCustomizedTrips(customizedTripsDTO.customTrips, UID, bInsert, securityToken.securityToken.sConnStr);
            return customTripsObj.ToBaseDTO(securityToken.securityToken);
        }


        public dtData GetCustomizedTrips(Guid sUserToken, DateTime dtFrom, DateTime dtTo, List<int> lAssetIDs, string sConnStr, List<String> sortColumns, Boolean sortOrderAsc)
        {

            DataTable dtCustomizedTrips = new NaveoOneLib.Services.Plannings.CustomizedTripsService().GetCustomizedTrips(dtFrom, dtTo, lAssetIDs, sConnStr, sortColumns, sortOrderAsc);


            dtData dtR = new dtData();
            dtR.Data = dtCustomizedTrips;
            dtR.Rowcnt = dtCustomizedTrips.Rows.Count;

            return dtR;
        }

        public BaseModel GetCustomTripsReport(Guid sUserToken, DateTime dtFrom, DateTime dtTo, List<int> lAssetIDs, string sConnStr, List<String> sortColumns, Boolean sortOrderAsc)
        {

            BaseModel bm = new BaseModel();

            //field1-targetwoekingRate,Field2 -HectarePlanned ,, Field3 -hectareCovered,,Field4 -Overtime  ,,Field5 - Other
            DataTable dtCustomizedTrips = new NaveoOneLib.Services.Plannings.CustomizedTripsService().GetCustomizedTrips(dtFrom, dtTo, lAssetIDs, sConnStr, sortColumns, sortOrderAsc);

            // List<CustomTripHeader> lCustomerTripHeader = new List<CustomTripHeader>();
            List<CustomTripHeader1> lCustomerTripHeader1 = new List<CustomTripHeader1>();
            List<CustomTrip> lCustomTrip1 = new List<CustomTrip>();


            if (dtCustomizedTrips == null)
            {
                bm.data = lCustomTrip1;
            }
                
            else
            {
                NaveoOneLib.Models.Plannings.Planning planning = new NaveoOneLib.Models.Plannings.Planning();
                //DateTime dtplanningStartTime = new DateTime();
                //DateTime dtplanningEndTime = new DateTime();
                DataView view = new DataView(dtCustomizedTrips);
                DataTable distinctValues = view.ToTable(true, "AssetNumber");
                DataTable distinctValuesDate = view.ToTable(true, "dtLocalTime");
                DataTable distinctPID = view.ToTable(true, "PID");
                double total1 = 0.0;
                double total2 = 0.0;
                double totalWorkingHoursInsideZone = 0.0;
                double totalNoOperationTime = 0.0;
                double totalNonWorkingTime = 0.0;
                double totalHectareCovered = 0.0;



                CustomTrip cT = new CustomTrip();
                string LOcODE = String.Empty;
                string fieldcode = string.Empty;
                foreach (DataRow drDistinct in distinctPID.Rows)
                {
                     int pid = 0;
                     if (drDistinct["PID"].ToString() != string.Empty)
                    {
                        pid = Convert.ToInt32(drDistinct["PID"].ToString());
                        planning = new NaveoOneLib.Services.Plannings.PlanningService().GetPlanningByPID(pid, sConnStr);
                        double buffer = Convert.ToDouble(planning.lPlanningViaPointH[0].Buffer);
                        //dtplanningStartTime = planning.lPlanningViaPointH[0].dtUTC.Value.AddMinutes(-buffer);
                        //dtplanningEndTime = planning.lPlanningViaPointH[0].dtUTC.Value.AddMinutes(buffer);
                        var operationType = (from vnd in planning.lPlanningLkup
                                             where vnd.LookupType == "OPERATION_TYPE"
                                             select vnd.LookupValueDesc).FirstOrDefault();

                        fieldcode = (from viapoint in planning.lPlanningViaPointH
                                         select viapoint.ZoneDesc).FirstOrDefault();


                        LOcODE = operationType;

                    }

                   
                }

                //DateTime date1 = dtplanningStartTime.ToLocalTime();
                //DateTime date2 = dtplanningEndTime.ToLocalTime();




                foreach (DataRow drDistinct in distinctValuesDate.Rows)
                {
                  

                    cT = new CustomTrip();

                    for (int o = 1; o <= 4; o++)
                    {

                        
                       
                        CustomTripHeader1 customTripHeader1 = new CustomTripHeader1();
                       
                        var rowColl = dtCustomizedTrips.AsEnumerable();
                        var drSelectedRows = rowColl.Where(r => r.Field<string>("dtLocalTime") == drDistinct["dtLocalTime"].ToString());
                        cT.Date = Convert.ToDateTime(drDistinct["dtLocalTime"].ToString());


                        var firstRow = drSelectedRows.Take(1);
                        var lastRow = drSelectedRows.Last();
                        var dateFrom = firstRow.Select(r => r.Field<DateTime>("DateFrom")).First();
                        var dateTo = lastRow.Field<DateTime>("DateTo");
                        //var dateFrom = new DateTime(2020, 5, 5, 11, 2, 21);
                        //var dateTo = new DateTime(2020, 5, 5, 20, 45, 58);
                        string Target = firstRow.Select(r => r.Field<String>("Field1")).First();
                        string Targetlast = lastRow.Field<String>("Field1");


                        if (Target == null)
                            Target = Targetlast;

                        customTripHeader1.AssetName =firstRow.Select(r => r.Field<String>("AssetNumber")).First();
                        //customTripHeader1.FieldCode = firstRow.Select(r => r.Field<String>("ZoneName")).First();
                        customTripHeader1.FieldCode = fieldcode + "--" + LOcODE;

                        
                        DateTime date1 = dateFrom;//dtplanningStartTime.ToLocalTime();
                        DateTime date2 = dateTo;//dtplanningEndTime.ToLocalTime();


                        TimeSpan ts = date2 - date1;
                        totalWorkingHoursInsideZone = ts.TotalHours;

                        TimeDetails timeDetails = new TimeDetails();

                        if (o == 1)
                        {
                            customTripHeader1.Title = "Time";
                            timeDetails.TimeStarted = dateFrom.ToShortTimeString();
                            timeDetails.TimeEnded = dateTo.ToShortTimeString();

                            string normal = firstRow.Select(r => r.Field<String>("Field6")).First();

                            timeDetails.Normal = Convert.ToDouble(normal);

                            string p = firstRow.Select(r => r.Field<String>("Field4")).First();
                            string t = firstRow.Select(r => r.Field<String>("Field5")).First();

                            timeDetails.Overtime = Convert.ToDouble(p);//dr["Field4"].ToString() == string.Empty ? 0.0 : Convert.ToDouble(dr["Field4"]); firstRow.Select(r => r.Field<Double>("Field4")).First();
                            timeDetails.LunchTeaTime = Convert.ToDouble(t); //dr["Field5"].ToString() == string.Empty ? 0.0 : Convert.ToDouble(dr["Field5"]);

                            



                            ////totalAvaliableTime (Nrmal hour + overtime) - Lunch
                            timeDetails.TotalAvailableTime = (timeDetails.Normal + timeDetails.Overtime) - timeDetails.LunchTeaTime;//timeDetails.Normal + timeDetails.LunchTeaTime; //to be reviewed
                            
                            customTripHeader1.lDetailTime.Add(timeDetails);
                        }

                        foreach (var dr in drSelectedRows)
                        {

                            int IsWorkHour = Convert.ToInt32(dr["CountAsWorkHour"]);
                            int IsStopped = Convert.ToInt32(dr["IsStopped"]);


                            if (o == 2)
                            {
                                customTripHeader1.Title = "No OPN (Hrs)";

                                NoOperationDetails noOperationDetails = new NoOperationDetails();

                                if (IsStopped == 1)
                                {
                             
                                    string StopDescription = dr["Description"].ToString();

                         
                                    List<string> lNOOperation = new List<string>();
                                    lNOOperation.Add("Field Not Available");
                                    lNOOperation.Add("Rain");
                                    lNOOperation.Add("Driver Absent/LL & SL");
                                    lNOOperation.Add("Tractor");
                                    lNOOperation.Add("Equipment");
                                    lNOOperation.Add("Overtime Control");
                                    lNOOperation.Add("Meetings/Briefing");
                                    lNOOperation.Add("Maintenance/Repair");



                                    if (lNOOperation.Contains(StopDescription))
                                    {
                                        Double nonOperationTimeInMinutes = Convert.ToDouble(dr["Duration"]);
                                        Double nonOperationTimeInHours = nonOperationTimeInMinutes / 60;

                                        noOperationDetails.NoOperationType = StopDescription;
                                        ///Double duration = Convert.ToDouble(dr["Duration"]);
                                        noOperationDetails.Value = Math.Round((Double)nonOperationTimeInHours, 2);

                                        total1 = total1 + noOperationDetails.Value;
                                        customTripHeader1.lDetailNoOperation.Add(noOperationDetails);
                                    }





                                }

                            }




                            if (o == 3)
                            {
                                customTripHeader1.Title = "Non Work Time (Hrs)";


                                if (IsStopped == 1)
                                {
                                    
                                    DataTable dtLk = new NaveoOneLib.Services.Lookups.LookUpValuesService().GetLookUpValuesByLookUpTypeName("Terra_Non_Working_Values", sConnStr);

                                    List<string> lNOWorkTime = new List<string>();

                                    foreach (DataRow drLK in dtLk.Rows)
                                    {
                                 

                                        string drLk = drLK["Name"].ToString();
                                        
                                        string StopDescription = dr["Description"].ToString();
                                        NonWorkTimeDetails noWorkTimeDETAILS = new NonWorkTimeDetails();


                                        if(StopDescription.ToLower() == "travel status")
                                        {
                                            string aaa = string.Empty;
                                        }

                                        if (drLk == StopDescription.ToString())
                                        {
                                            Double nonWorkTimeInMinutes = Convert.ToDouble(dr["Duration"]);
                                            Double nonWorkTimeInHours = nonWorkTimeInMinutes / 60;

                                            noWorkTimeDETAILS.NoWorkTimeType = StopDescription;
                                            noWorkTimeDETAILS.Value = Math.Round((Double)nonWorkTimeInHours, 2);

                                            total2 = total2 + noWorkTimeDETAILS.Value;
                                            customTripHeader1.lDetailNoWorkTime.Add(noWorkTimeDETAILS);
                                        }


                                    }


                                }
                            }



                            if (o == 4)
                            {
                                string z = dr["ZoneName"].ToString();

                                if (IsWorkHour == 1 && z != "")
                                {

                                    Double hectareCovered = dr["Field3"].ToString() == string.Empty ? 0.0 : Convert.ToDouble(dr["Field3"]);
                                   
                                    totalHectareCovered = totalHectareCovered + hectareCovered;


                                }
                                customTripHeader1.Title = "Work Done";


                            }

                        }
                     

                        if (o == 2)
                        {
                            NoOperationDetails noOperationDetails1 = new NoOperationDetails();
                            noOperationDetails1.NoOperationType = "Total No Operation Time (Hrs)";

                            double totalNoOperatimeInHours = total1;
                            Double dc = Math.Round((Double)totalNoOperatimeInHours, 2);

                            noOperationDetails1.Value = dc;
                            totalNoOperationTime = noOperationDetails1.Value;

                            NoOperationDetails noOperationDetails2 = new NoOperationDetails();
                            noOperationDetails2.NoOperationType = "Total No Operation Time (%)";
                            noOperationDetails2.Value = totalNoOperationTime * 100; //to be reviewed and calculated

                            customTripHeader1.lDetailNoOperation.Add(noOperationDetails1);
                            customTripHeader1.lDetailNoOperation.Add(noOperationDetails2);
                        }


                        if (o == 3)
                        {
                            NonWorkTimeDetails noWorkTimeDETAILS1 = new NonWorkTimeDetails();
                            noWorkTimeDETAILS1.NoWorkTimeType = "Total Non Working Time (Hrs)";

                            double totalNonWorkingTimeInHours = total2;
                            Double dc = Math.Round((Double)totalNonWorkingTimeInHours, 2);

                            noWorkTimeDETAILS1.Value = dc;
                            totalNonWorkingTime = noWorkTimeDETAILS1.Value;

                            NonWorkTimeDetails noWorkTimeDETAILS2 = new NonWorkTimeDetails();
                            noWorkTimeDETAILS2.NoWorkTimeType = "Total Non Working Time (%)";
                           

                            double caculated = (noWorkTimeDETAILS1.Value / totalWorkingHoursInsideZone) * 100;
                            Double dc1 = Math.Round((Double)caculated, 2);

                            noWorkTimeDETAILS2.Value = dc1;
                            

                            customTripHeader1.lDetailNoWorkTime.Add(noWorkTimeDETAILS1);
                            customTripHeader1.lDetailNoWorkTime.Add(noWorkTimeDETAILS2);
                        }



                     

                        if (o == 4)
                        {




                            #region fuel
                            DateTime EndDate = dateTo;
                            DateTime StartDate = dateFrom;
                            Double fuel_consumption_value = 0.0;

                            try
                            {
                                DataTable dtFuel = new FuelService().GetFuelData(sUserToken, lAssetIDs[0], (DateTime)StartDate.ToUniversalTime(), (DateTime)EndDate.ToUniversalTime(), sConnStr);

                                var rows = dtFuel.Rows;


                                

                                var fill_list = dtFuel
                                        .AsEnumerable()
                                        .Where(myRow => myRow.Field<string>("fType") == "Fill").ToList();



                                if (fill_list.Count != 0)
                                {
                                    //select all fuel data where type is not DROP
                                    var fuel_list_without_drop_type = dtFuel
                                                   .AsEnumerable()
                                                   .Where(myRow => myRow.Field<string>("fType") != "Drop").ToList();




                                    //select all datetime where type is not DROP
                                    var lselectAlltimeforfuel = dtFuel
                                                   .AsEnumerable()
                                                   .Where(myRow => myRow.Field<string>("fType") != "Drop").Select(m => m.Field<DateTime>("dateTimeGPS_UTC")).ToList();

                                    //compare planning start time with all datetime in fuel list
                                    var closestStart = (from t in lselectAlltimeforfuel
                                                        orderby (t - StartDate).Duration()
                                                        select t
                                                                  ).First();

                                    //select fuel value where closest planning time is equal to fuel datetime list
                                    var fuelreadingfirst = fuel_list_without_drop_type
                                               .AsEnumerable()
                                               .Where(myRow => myRow.Field<DateTime>("dateTimeGPS_UTC") == closestStart).Select(m => m.Field<Double>("convertedValue")).First();

                                    //select all fuel type 
                                    var listofType = fuel_list_without_drop_type
                                             .AsEnumerable().
                                             Select(m => m.Field<string>("fType")).ToList();

                                    //get index of type fill in list
                                    int index = listofType.FindIndex(aaa => aaa.Contains("Fill"));

                                    //get last index before fill type index
                                    int lastindexvaluebeforefill = index - 1;

                                    //get all convertvalues
                                    var allfuelvalues = fuel_list_without_drop_type
                                          .AsEnumerable()
                                          .Select(m => m.Field<Double>("convertedValue")).ToList();

                                    //last index before fill from all fuel values
                                    Double fuelvalue = allfuelvalues[lastindexvaluebeforefill];

                                    ////first calculation
                                    Double firstdifference = fuelreadingfirst - fuelvalue;



                                    var closestEnd = (from t in lselectAlltimeforfuel
                                                      orderby (t - EndDate).Duration()
                                                      select t
                                                                ).First();


                                    var fuelreadingsecond = fuel_list_without_drop_type
                                   .AsEnumerable()
                                   .Where(myRow => myRow.Field<DateTime>("dateTimeGPS_UTC") == closestEnd).Select(m => m.Field<Double>("convertedValue")).First();

                                    //get next fuel value index  after fill 
                                    int nextindexafterfill = index + 1;

                                    Double fuel_value_after_fill = allfuelvalues[nextindexafterfill];

                                    //second calucation
                                    Double seconddifference = fuel_value_after_fill - fuelreadingsecond;


                                    fuel_consumption_value = firstdifference + seconddifference;

                                }
                                else
                                {

                                    //select all fuel data where type is not DROP
                                    var fuel_list_without_drop_type = dtFuel
                                                   .AsEnumerable()
                                                   .Where(myRow => myRow.Field<string>("fType") != "Drop").ToList();

                                    //select all datetime where type is not DROP
                                    var lselectAlltimeforfuel = dtFuel
                                                   .AsEnumerable()
                                                   .Where(myRow => myRow.Field<string>("fType") != "Drop").Select(m => m.Field<DateTime>("dateTimeGPS_UTC")).ToList();

                                    //compare planning start time with all datetime in fuel list
                                    DateTime closestStart = (from t in lselectAlltimeforfuel
                                                             orderby (t - StartDate).Duration()
                                                             select t
                                                                  ).First();



                                    DateTime closestEnd = (from t in lselectAlltimeforfuel
                                                           orderby (t - EndDate).Duration()
                                                           select t
                                                                ).First();

                                    //select fuel value where closest planning time is equal to fuel datetime list

                                    Type fieldType = dtFuel.Columns["convertedValue"].DataType;

                                    Double fuelreadingfirst = (from r in fuel_list_without_drop_type.AsEnumerable()
                                                               where r.Field<DateTime>("dateTimeGPS_UTC") == closestStart
                                                               select r.Field<Double>("convertedValue")
                                                                 ).First();


                                    Double fuelreadingsecond = fuel_list_without_drop_type
                                            .AsEnumerable()
                                            .Where(myRow => myRow.Field<DateTime>("dateTimeGPS_UTC") == closestEnd).Select(m => m.Field<Double>("convertedValue")).First();



                                    fuel_consumption_value = fuelreadingfirst - fuelreadingsecond;
                                }
                            } catch (Exception ex)
                            {

                            }

                            #endregion


                            //totalAvaliableTime (Nrmal hour + overtime) - Lunch
                            WorkDone workDone1 = new WorkDone();
                            workDone1.WorkDoneType = "In-Field Work Hours (Hrs)";
                            Double first = totalWorkingHoursInsideZone;  //totalWorkingHoursInsideZone
                            //double final = first - totalNonWorkingTime;
                            Double dc1 = Math.Round((Double)first, 2);

                            workDone1.Value = dc1;

                            double noOperationHours = 0.0;

                            if (total1 != 0)
                            {
                                noOperationHours = total1;
                            }
                                
                            WorkDone workDone0 = new WorkDone();
                            workDone0.WorkDoneType = "In-Field Effective Hours (Hrs)";

                            Double effectivehours = (dc1 - totalNonWorkingTime);

                            Double finalEffectiveHours = effectivehours - noOperationHours;

                            Double dc0 = Math.Round((Double)finalEffectiveHours, 2);
                            workDone0.Value = dc0;


                            WorkDone workDone2 = new WorkDone();
                            workDone2.WorkDoneType ="Equivalent Work done (Ha)";
                            Double dc2 = Math.Round((Double)totalHectareCovered, 2);
                            workDone2.Value = dc2; //to be reviewed;

                            WorkDone workDone3 = new WorkDone();
                            workDone3.WorkDoneType = "Eq Hectares/Prodn Hr";
                            double caculationhectareProd = workDone2.Value / workDone0.Value;
                            Double dc3 = Math.Round((Double)caculationhectareProd, 2);
                            workDone3.Value = dc3; //to be reviewed;

                            WorkDone workDone4 = new WorkDone();
                            workDone4.WorkDoneType = "Efficiency (%)";
                          
                            double target = Convert.ToDouble(Target);
                            double calculatedEfficiency =  workDone3.Value / target;


                            if (Double.IsInfinity(calculatedEfficiency) == true)
                                          calculatedEfficiency = 0;

                            if(Double.IsNaN(calculatedEfficiency) == true)
                                       calculatedEfficiency = 0;


                            Double dc4 = Math.Round(calculatedEfficiency, 2);


                            workDone4.Value = dc4 * 100; //to be reviewed;
                           

                            WorkDone workDone5 = new WorkDone();
                            workDone5.WorkDoneType = "Utilisation (%)";
                            double calculatedUtiltisation = (workDone1.Value / totalWorkingHoursInsideZone) * target;
                            Double d5 = Math.Round((Double)calculatedUtiltisation, 2);
                            workDone5.Value = d5 * 100;//to be reviewed;


                            //Add litre per lt per engine hours
                            WorkDone workDone6 = new WorkDone();
                            workDone6.WorkDoneType = "Litre per engine hours";
                            workDone6.Value = Math.Round(fuel_consumption_value / effectivehours, 2);//fuel conumption divided by effectivehours


                            if (Double.IsNaN(workDone6.Value) == true)
                                workDone6.Value = 0;

                            //Add litre per hectare
                            WorkDone workDone7 = new WorkDone();
                            workDone7.WorkDoneType = "Litre per hectare";
                            workDone7.Value = Math.Round(fuel_consumption_value / dc2, 2);// fuel conumption divided by work done


                            if (Double.IsNaN(workDone7.Value) == true)
                                        workDone7.Value = 0;


                            customTripHeader1.lDetailWorkDone.Add(workDone1);
                            customTripHeader1.lDetailWorkDone.Add(workDone0);
                            customTripHeader1.lDetailWorkDone.Add(workDone2);
                            customTripHeader1.lDetailWorkDone.Add(workDone3);
                            customTripHeader1.lDetailWorkDone.Add(workDone4);
                            customTripHeader1.lDetailWorkDone.Add(workDone5);
                            customTripHeader1.lDetailWorkDone.Add(workDone6);
                            customTripHeader1.lDetailWorkDone.Add(workDone7);

                        }

                        //lCustomerTripHeader1.Add(customTripHeader1);
                        cT.lCustomTripHeader1.Add(customTripHeader1);

                    }


                    lCustomTrip1.Add(cT);
                }


               




            }


            bm.data = lCustomTrip1;
            bm.total = lCustomTrip1.Count;
            

            return bm;
                
        }


        public BaseModel GetMokaCustomTripsReport(Guid sUserToken, DateTime dtFrom, DateTime dtTo, List<int> lAssetIDs, string sConnStr, List<String> sortColumns, Boolean sortOrderAsc)
        {

            BaseModel bm = new BaseModel();

            //field1-netWeight,Field2 -voucher No ,, Field3 -GROSSWEIGHT,,Field4 -TARE WEIGHT  ,,Field5 - TRIP DISTANCE
            DataTable dtCustomizedTrips = new NaveoOneLib.Services.Plannings.CustomizedTripsService().GetCustomizedTrips(dtFrom, dtTo, lAssetIDs, sConnStr, sortColumns, sortOrderAsc);
            List<CustomTripHeader> lCustomerTripHeader = new List<CustomTripHeader>();


            if (dtCustomizedTrips == null)
            {
                bm.data = lCustomerTripHeader;
            }

            else
            {
                DataView view = new DataView(dtCustomizedTrips);
                DataTable distinctValues = view.ToTable(true, "AssetNumber");


                foreach (DataRow drDistinct in distinctValues.Rows)
                {


                        
                    
                        var rowColl = dtCustomizedTrips.AsEnumerable();
                        var drSelectedRows = rowColl.Where(r => r.Field<string>("AssetNumber") == drDistinct["AssetNumber"].ToString());



                        var firstRow = drSelectedRows.Take(1);
                        var lastRow = drSelectedRows.Last();
                        var dateFrom = firstRow.Select(r => r.Field<DateTime>("DateFrom")).First();
                        var dateTo = lastRow.Field<DateTime>("DateTo");
         
                        

                        foreach (var dr in drSelectedRows)
                        {
                          CustomTripHeader customTripHeader = new CustomTripHeader();
                           int IsWorkHour = Convert.ToInt32(dr["CountAsWorkHour"]);
                            int IsStopped = Convert.ToInt32(dr["IsStopped"]);

                            customTripHeader.AssetName = dr["AssetNumber"].ToString();
                            customTripHeader.NetWeight = dr["Field1"].ToString() == string.Empty ? 0.000 : Convert.ToDouble(dr["Field1"]);
                            customTripHeader.VoucherNo = dr["Field2"].ToString() == string.Empty ? 0.000 : Convert.ToDouble(dr["Field2"]);
                            customTripHeader.GrossWieght = dr["Field3"].ToString() == string.Empty ? 0.000 : Convert.ToDouble(dr["Field3"]);
                            customTripHeader.TareWeight = dr["Field4"].ToString() == string.Empty ? 0.000 : Convert.ToDouble(dr["Field4"]);
                            customTripHeader.TripDistance = dr["Field5"].ToString() == string.Empty ? 0.000 : Convert.ToDouble(dr["Field5"]);
                            
                            customTripHeader.TripStartTime = dr["DateFrom"].ToString() == string.Empty ? "" : dr["DateFrom"].ToString();
                            customTripHeader.TripEndTime = dr["DateTo"].ToString() == string.Empty ? "" : dr["DateTo"].ToString();

                            TimeSpan ts = TimeSpan.Parse(dr["Duration"].ToString());
                            customTripHeader.TimeTaken = ts.ToString(@"hh\:mm\:ss");

           
                        List<int> lZoneIDs = new List<int>();
                        int zoneId = dr["ZoneID"].ToString() == string.Empty ? 0 : Convert.ToInt32(dr["ZoneID"]);
                        if (zoneId != 0)
                        {
                            lZoneIDs.Add(zoneId);

                            ZoneData zd = new ZoneService().GetZoneById(sUserToken, sConnStr, lZoneIDs);

                            customTripHeader.NoOfHouseHold = zd.lZone[0].NoOfHouseHold;
                           // Math.Round((Double)zd.lZone[0].NoOfHouseHold,0);

                            Double zoneDistanceCovered = zd.lZone[0].externalRef.ToString() == string.Empty ? 0.0 : Convert.ToDouble(zd.lZone[0].externalRef);
                            customTripHeader.ZoneDistanceCovered = Math.Round((Double)zoneDistanceCovered, 1);
                            

                        }

                        lZoneIDs.Clear();
                        int AssetID = (int)dr["AssetID"];
                        int UID = CommonService.GetUserID(sUserToken, sConnStr);
                        //BaseModel dsGetFuelConsumption = new NaveoOneLib.Services.Fuels.FuelService().GetFuelConsumption(UID, dateFrom.AddHours(-4), dateTo.AddHours(-4), AssetID, sConnStr, null, null);
                        //DataTable dtFuel = new FuelService().GetFuelData(sUserToken, AssetID, dateFrom.AddHours(-4), dateTo.AddHours(-4), sConnStr);

                        //customTripHeader.Consumption

                        lCustomerTripHeader.Add(customTripHeader);

                 


                    }

                        

                    

                }




            }


            bm.data = lCustomerTripHeader;
            bm.total = lCustomerTripHeader.Count;


            return bm;

        }

    }
}
