﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NaveoOneLib.Models;
using NaveoService.Models;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Users;
using NaveoOneLib.Services.Trips;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Services.Assets;
using NaveoOneLib.Services.Users;
using NaveoOneLib.Services.Rules;

namespace NaveoService.Services
{
    public class TripService
    {
        #region Methods
        /// <summary>
        /// Get trip headers
        /// </summary>
        /// <param name="sUserToken"></param>
        /// <param name="lAssetIDs"></param>
        /// <param name="dtFr"></param>
        /// <param name="dtTo"></param>
        /// <param name="sConnStr"></param>
        /// <returns></returns>
        public Models.TripHeader GetTripHeaders(Guid sUserToken, List<int> lAssetIDs, DateTime dtFr, DateTime dtTo, Boolean byDriver, Double iMinTripDistance, List<string> lWorkHours, String sConnStr, int? Page, int? LimitPerPage, int? Odometre, int? AssetIdAPI, Boolean bHasExceptions, List<String> sortColumns, Boolean sortOrderAsc)
        {
            int UID = -1;
            List<int> lValidatedIds;

            if (byDriver)
                lValidatedIds = CommonService.lValidateDrivers(sUserToken, lAssetIDs, sConnStr, out UID);
            else
                lValidatedIds = CommonService.lValidateAssets(sUserToken, lAssetIDs, sConnStr, out UID);

            #region sWorkHours
            string sWorkHours = string.Empty;
            if (lWorkHours.Count > 0)
                sWorkHours = String.Join(",", lWorkHours.Select(s => s));

            #endregion

            List<Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);
            DataSet dsTrips = new TripHeaderService().GetTripHeaders(lValidatedIds, dtFr, dtTo, iMinTripDistance, sWorkHours, byDriver, false, true, lm, sConnStr, Page, LimitPerPage, bHasExceptions, sortColumns, sortOrderAsc);

            if (dsTrips != null)
            {
                DataTable TripData = dsTrips.Tables["TripHeader"];
                if (TripData.Columns.Contains("ExceptionFlag"))
                    TripData.Columns.Remove("ExceptionFlag");
                if (TripData.Columns.Contains("GPSDataStartUID"))
                    TripData.Columns.Remove("GPSDataStartUID");
                if (TripData.Columns.Contains("GPSDataEndUID"))
                    TripData.Columns.Remove("GPSDataEndUID");
                if (TripData.Columns.Contains("dtStart"))
                    TripData.Columns.Remove("dtStart");
                if (TripData.Columns.Contains("dtEnd"))
                    TripData.Columns.Remove("dtEnd");
                if (TripData.Columns.Contains("GroupDate"))
                    TripData.Columns.Remove("GroupDate");

                TripData.Columns.Add("LastOdometer");

                //Key AssetId, Values RealOdometer
                var list = new List<KeyValuePair<int, decimal>>();

                foreach (DataRow row in TripData.Rows)
                {
                    DataTable dtOdometer = new TripHeaderService().getInitialOdoInput(new List<int> { int.Parse(row["assetID"].ToString()) }, DateTime.Parse(row["to"].ToString()), sConnStr);

                    //DataRow[] drOdo = dtOdometer.Select("assetID = " + 41);
                    DataRow[] drOdo = dtOdometer.Select("assetID = " + row["assetID"]);
                    if (drOdo.Length > 0)
                    {
                        decimal.TryParse(drOdo[0]["RealOdometer"].ToString(), out decimal RealOdometer);
                        int.TryParse(drOdo[0]["assetID"].ToString(), out int AssetId);

                        KeyValuePair<int, decimal> v = list.SingleOrDefault(x => x.Key == AssetId);

                        if (v.Key == AssetId)
                        {
                            row["LastOdometer"] = Math.Round(v.Value + decimal.Parse(row["tripDistance"].ToString()));
                            list.RemoveAll(item => item.Key.Equals(v.Key));
                            list.Add(new KeyValuePair<int, decimal>(AssetId, decimal.Parse(row["LastOdometer"].ToString())));
                        }
                        else
                        {
                            if (Page > 1)
                            {
                                if (AssetIdAPI == AssetId)
                                {
                                    row["LastOdometer"] = Math.Round(decimal.Parse(Odometre.ToString()) + decimal.Parse(row["tripDistance"].ToString()));
                                    list.Add(new KeyValuePair<int, decimal>(AssetId, decimal.Parse(row["LastOdometer"].ToString())));
                                }
                                else
                                {
                                    row["LastOdometer"] = Math.Round(RealOdometer);
                                    list.Add(new KeyValuePair<int, decimal>(AssetId, RealOdometer));
                                }

                            }
                            else
                            {
                                row["LastOdometer"] = Math.Round(RealOdometer);
                                list.Add(new KeyValuePair<int, decimal>(AssetId, RealOdometer));
                            }

                        }
                    }
                }

                if (dsTrips.Tables["Totals"].Columns.Contains("totalstoptime"))
                    dsTrips.Tables["Totals"].Columns["totalstoptime"].ColumnName = "totalStopTime";

                TripHeader th = new TripHeader();
                th.dtTrips = TripData;
                th.dtTotals = dsTrips.Tables["Totals"];

                //Check if first value
                //foreach try data
                //th.dtOdometer = dtOdometer;

                int cnt = 0;
                if (dsTrips.Tables != null && dsTrips.Tables.Contains("Totals") && dsTrips.Tables["Totals"].Rows.Count > 0)
                    int.TryParse(dsTrips.Tables["Totals"].Rows[0][0].ToString(), out cnt);
                th.rowCount = cnt;
                return th;
            }
            return new TripHeader();
        }

        public DataTable getCalculatedOdo(Guid sUserToken, List<int> lAssetID, DateTime dtTo, String sConnStr)
        {
            int UID = -1;
            List<int> lValidatedAssets = CommonService.lValidateAssets(sUserToken, lAssetID, sConnStr, out UID);
            DataTable dtTrips = new TripHeaderService().getCalculatedOdo(lValidatedAssets, dtTo, sConnStr);
            return dtTrips;
        }
        /// <summary>
        /// Get trip details
        /// </summary>
        /// <param name="lTripHeaders"></param>
        /// <param name="sConnStr"></param>
        /// <returns></returns>
        public TripDetails GetTripDetails(List<int> lTripHeaders, SecurityTokenExtended securityToken)
        {
            DataSet ds = new TripHeaderService().getTripDetails(lTripHeaders, securityToken.securityToken.sConnStr);

            DataTable dtTripDetails = ds.Tables["TripDetails"];
            DataTable dtExceptions = ds.Tables["TripExceptions"];

            if (dtTripDetails.Columns.Contains("headerid"))
                dtTripDetails.Columns["headerid"].ColumnName = "headerID";

            if (dtTripDetails.Columns.Contains("gpsdatauid"))
                dtTripDetails.Columns["gpsdatauid"].ColumnName = "gpsDataUID";

            if (dtTripDetails.Columns.Contains("localtime"))
                dtTripDetails.Columns["localtime"].ColumnName = "localTime";

            if (dtTripDetails.Columns.Contains("datetimeserver"))
                dtTripDetails.Columns["datetimeserver"].ColumnName = "dateTimeServer";

            if (dtTripDetails.Columns.Contains("lonlatvalidflag"))
                dtTripDetails.Columns["lonlatvalidflag"].ColumnName = "lonLatValidFlag";

            if (dtTripDetails.Columns.Contains("roadspeed"))
                dtTripDetails.Columns["roadspeed"].ColumnName = "roadSpeed";

            if (dtTripDetails.Columns.Contains("engineon"))
                dtTripDetails.Columns["engineon"].ColumnName = "engineOn";

            if (dtTripDetails.Columns.Contains("rulename"))
                dtTripDetails.Columns["rulename"].ColumnName = "ruleName";

            if (dtTripDetails.Columns.Contains("ruleid"))
                dtTripDetails.Columns["ruleid"].ColumnName = "ruleID";

            if (dtTripDetails.Columns.Contains("assetid"))
                dtTripDetails.Columns["assetid"].ColumnName = "AssetID";

            if (dtTripDetails.Columns.Contains("grpid"))
                dtTripDetails.Columns["grpid"].ColumnName = "GrpID";

            if (dtTripDetails.Columns.Contains("excpstart"))
                dtTripDetails.Columns["excpstart"].ColumnName = "ExcpStart";

            if (dtTripDetails.Columns.Contains("devicedirection"))
                dtTripDetails.Columns["devicedirection"].ColumnName = "deviceDirection";

            if (dtTripDetails.Columns.Contains("duration"))
                dtTripDetails.Columns["duration"].ColumnName = "Duration";


            if (dtExceptions.Columns.Contains("RuleID"))
                dtExceptions.Columns["RuleID"].ColumnName = "ruleid";

            if (dtExceptions.Columns.Contains("RuleName"))
                dtExceptions.Columns["RuleName"].ColumnName = "rulename";

            if (dtExceptions.Columns.Contains("groupID"))
                dtExceptions.Columns["groupID"].ColumnName = "groupid";



            TripDetails td = new TripDetails();
            td.dtTripDetails = dtTripDetails;
            td.dtTripExceptions = ds.Tables["TripExceptions"];



            return td;
        }

        #region SummarizedReports
        /// <summary>
        /// GetDailySummarizedTrips
        /// </summary>
        /// <param name="sUserToken"></param>
        /// <param name="lAssetIDs"></param>
        /// <param name="dtFr"></param>
        /// <param name="dtTo"></param>
        /// <param name="sConnStr"></param>
        /// <returns></returns>
        public dtData GetDailySummarizedTrips(Guid sUserToken, List<int> lAssetIDs, DateTime dtFr, DateTime dtTo, Boolean bDriver, String sConnStr, int? Page, int? LimitPerPage, List<String> sortColumns, Boolean sortOrderAsc)
        {
            int UID = -1;
            List<int> lValidatedIds = new List<int>();

            if (bDriver)
                lValidatedIds = CommonService.lValidateDrivers(sUserToken, lAssetIDs, sConnStr, out UID);
            else
                lValidatedIds = CommonService.lValidateAssets(sUserToken, lAssetIDs, sConnStr, out UID);

            List<Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);
            DataSet dsTrips = new TripHeaderService().GetDailySummarizedTrips(lValidatedIds, dtFr, dtTo, bDriver, lm, sConnStr, sortColumns, sortOrderAsc);

            if (dsTrips.Tables.Contains("TripHeader"))
                dsTrips.Tables.Remove("TripHeader");

            if (dsTrips.Tables["Report"].Columns.Contains("registrationno"))
                dsTrips.Tables["Report"].Columns["registrationno"].ColumnName = "registrationNo";

            if (dsTrips.Tables["Report"].Columns.Contains("starttime"))
                dsTrips.Tables["Report"].Columns["starttime"].ColumnName = "startTime";

            if (dsTrips.Tables["Report"].Columns.Contains("endtime"))
                dsTrips.Tables["Report"].Columns["endtime"].ColumnName = "endTime";

            if (dsTrips.Tables["Report"].Columns.Contains("kmtravelled"))
                dsTrips.Tables["Report"].Columns["kmtravelled"].ColumnName = "kmTravelled";

            if (dsTrips.Tables["Report"].Columns.Contains("idlingtime"))
                dsTrips.Tables["Report"].Columns["idlingtime"].ColumnName = "idlingTime";

            if (dsTrips.Tables["Report"].Columns.Contains("inzonestoptime"))
                dsTrips.Tables["Report"].Columns["inzonestoptime"].ColumnName = "inZoneStopTime";

            if (dsTrips.Tables["Report"].Columns.Contains("outzonestoptime"))
                dsTrips.Tables["Report"].Columns["outzonestoptime"].ColumnName = "outZoneStopTime";

            dtData dtR = new dtData();
            dtR.dsData = dsTrips;

            return dtR;
        }
        public dtData GetSummarizedTrips(Guid sUserToken, List<int> lAssetIDs, DateTime dtFr, DateTime dtTo, Boolean bDriver, String sConnStr, int? Page, int? LimitPerPage, List<String> sortColumns, Boolean sortOrderAsc)
        {
            int UID = -1;
            List<int> lValidatedIds = new List<int>();

            if (bDriver)
                lValidatedIds = CommonService.lValidateDrivers(sUserToken, lAssetIDs, sConnStr, out UID);
            else
                lValidatedIds = CommonService.lValidateAssets(sUserToken, lAssetIDs, sConnStr, out UID);

            List<Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);
            DataTable dtTrips = new TripHeaderService().GetSummarizedTrips(lValidatedIds, dtFr, dtTo, bDriver, lm, sConnStr, sortColumns, sortOrderAsc);
            dtTrips.Columns.Add("fuelcost");
            dtTrips.Columns.Add("costperkm");
            dtTrips.Columns.Add("fuelinlitres");
            dtTrips.Columns.Add("fuelinlitresper100km");
            dtTrips.Columns.Add("totalcost");
            dtTrips.Columns.Add("totalper100km");

            NaveoService.Services.MaintAssetServices maintservice = new NaveoService.Services.MaintAssetServices();
            BaseModel bm = maintservice.FuelUsage(sConnStr, lValidatedIds, dtFr, dtTo);

            try
            {
                DataTable dtFuelCost = new DataTable();
                DataTable dtFuelQuantity = new DataTable();
                DataTable dtTotalMaintennaceCost = new DataTable();
                if (bm.dbResult != null)
                {
                    dtFuelCost = bm.dbResult.Tables["FuelCost"];
                    dtFuelQuantity = bm.dbResult.Tables["FuelQuantity"];
                    dtTotalMaintennaceCost = bm.dbResult.Tables["TotalMaintennaceCost"];
                }

                foreach (DataRow dr in dtTrips.Rows)
                {
                    int assetid = Convert.ToInt32(dr["assetid"].ToString());
                    var selectfuelcost = dtFuelCost.AsEnumerable().Where(a => a.Field<int>("AssetId") == assetid).Select(a => a.Field<Double>("TotalCost")).FirstOrDefault();
                    Double kmTravlelled = Convert.ToDouble(dr["kmTravelled"]);

                    if (selectfuelcost != 0)
                    {
                        dr["fuelcost"] = selectfuelcost;

                        if (kmTravlelled != 0 && selectfuelcost != 0)
                        {
                            dr["costperkm"] = Math.Round(selectfuelcost / kmTravlelled, 2);

                        }
                    }
                    else
                    {
                        dr["fuelcost"] = "0.00";
                    }


                    var selectFuelQuantity = dtFuelQuantity.AsEnumerable().Where(a => a.Field<int>("AssetId") == assetid).Select(a => a.Field<Double>("Quantity")).FirstOrDefault();

                    if (selectFuelQuantity != 0)
                    {
                        dr["fuelinlitres"] = selectFuelQuantity;

                        if (kmTravlelled != 0 && selectFuelQuantity != 0)
                        {
                            Double result = (selectFuelQuantity / kmTravlelled) * 100;
                            dr["fuelinlitresper100km"] = Math.Round(result, 2);

                        }
                    }
                    else
                    {
                        dr["fuelinlitres"] = "0.00";
                    }

                    var selectTotalMaintennaceCost = dtTotalMaintennaceCost.AsEnumerable().Where(a => a.Field<int>("AssetId") == assetid).Select(a => a.Field<Double>("TotalMaintCost")).FirstOrDefault();

                    if (selectTotalMaintennaceCost != 0)
                    {
                        dr["totalcost"] = selectTotalMaintennaceCost;

                        if (kmTravlelled != 0 && selectTotalMaintennaceCost != 0)
                        {
                            Double result = (selectTotalMaintennaceCost / kmTravlelled) * 100;
                            dr["totalper100km"] = Math.Round(result, 2);

                        }
                    }
                    else
                    {
                        dr["totalcost"] = "0.00";
                    }
                }

            }
            catch (Exception ex)
            {

            }

            DataTable ResultTable = new DataTable();
            ResultTable.Columns.Add("rowNum", typeof(int));
            ResultTable.Columns["rowNum"].AutoIncrement = true;
            ResultTable.Columns["rowNum"].AutoIncrementSeed = 1;
            ResultTable.Columns["rowNum"].AutoIncrementStep = 1;

            if (dtTrips != null)
                ResultTable.Merge(dtTrips);

            Pagination p = Pagination.GetRowNums(Page, LimitPerPage);
            DataTable dt = new DataTable();
            DataRow[] drChk = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo);
            if (drChk != null && drChk.Length > 0)
                dt = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo).CopyToDataTable();
            else
                dt = ResultTable;


            int dtTripsRows = 0;
            if (dtTrips != null)
            {
                dtTripsRows = dtTrips.Rows.Count;
            }


            ResultTable.Columns.Remove("rowNum");
            dtData dtR = new dtData();
            dtR.Data = dt;
            dtR.Rowcnt = dtTripsRows;

            return dtR;
        }

        public dtData GetDailyTripTime(Guid sUserToken, List<int> lAssetIDs, DateTime dtFr, DateTime dtTo, String sTotalField, Boolean bByDriver, String sConnStr, int? Page, int? LimitPerPage)
        {
            dtData dtR = new dtData();

            int UID = -1;
            List<int> lValidatedIds = CommonService.lValidateAssets(sUserToken, lAssetIDs, sConnStr, out UID);

            if (bByDriver)
                lValidatedIds = CommonService.lValidateDrivers(sUserToken, lAssetIDs, sConnStr, out UID);
            else
                lValidatedIds = CommonService.lValidateAssets(sUserToken, lAssetIDs, sConnStr, out UID);

            DataSet ds = new TripHeaderService().GetDailyTripTime(lValidatedIds, dtFr, dtTo, sTotalField, bByDriver, sConnStr);
            if (ds.Tables.Contains("Pivot"))
            {
                DataTable dtTrips = ds.Tables["Pivot"];

                DataTable ResultTable = new DataTable();
                ResultTable.Columns.Add("rowNum", typeof(int));
                ResultTable.Columns["rowNum"].AutoIncrement = true;
                ResultTable.Columns["rowNum"].AutoIncrementSeed = 1;
                ResultTable.Columns["rowNum"].AutoIncrementStep = 1;
                ResultTable.Merge(dtTrips);

                Pagination p = Pagination.GetRowNums(Page, LimitPerPage);
                DataTable dt = new DataTable();
                DataRow[] drChk = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo);
                if (drChk != null && drChk.Length > 0)
                    dt = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo).CopyToDataTable();
                else
                    dt = ResultTable;

                if (dt.Columns.Contains("Total"))   //SQL
                {
                    dt.Columns["Total"].SetOrdinal(dt.Columns.Count - 1);

                    //add code to convert day:hh:mm:ss into hh
                    foreach (DataRow row in dt.Rows)
                    {
                        string[] NumberOfDaysAndHours = row["Total"].ToString().Split(':');

                        string[] NumberOfDays = NumberOfDaysAndHours[0].Split('.');

                        int totalHours = int.Parse(NumberOfDays[0]) * 24 + int.Parse(NumberOfDaysAndHours[1]);

                        string NewTotal = string.Format("{0}:{1}:{2}", totalHours, NumberOfDaysAndHours[2], NumberOfDaysAndHours[3]);

                        row["Total"] = NewTotal;
                    }
                }
                else //PostgreSQL
                {
                    if (dt.Columns.Contains("assetname"))   //SQL
                        dt.Columns["assetname"].ColumnName = "sDesc";

                    TimeSpan tsTotal = new TimeSpan();
                    dt.Columns.Add("Total");
                    foreach (DataRow dr in dt.Rows)
                    {
                        foreach (DataColumn dc in dt.Columns)
                            if (dc.ColumnName.StartsWith("2"))
                                if (TimeSpan.TryParse(dr[dc].ToString(), out TimeSpan ts))
                                    tsTotal = tsTotal.Add(ts);

                        dr["Total"] = tsTotal;

                        if (tsTotal.TotalSeconds == 0)
                        {
                            Double dTotalTripDistance = 0;
                            foreach (DataColumn dc in dt.Columns)
                                if (dc.ColumnName.StartsWith("2"))
                                    if (Double.TryParse(dr[dc].ToString(), out Double d))
                                        dTotalTripDistance += d;

                            dr["Total"] = dTotalTripDistance;
                        }
                    }
                }

                ResultTable.Columns.Remove("rowNum");
                dtR.Data = dt;
                dtR.Rowcnt = dtTrips.Rows.Count;
            }
            return dtR;
        }
        #endregion


        /// <summary>
        /// GetAssetSummaryOutsideHO
        /// </summary>
        /// <param name="sUserToken"></param>
        /// <param name="lAssetIDs"></param>
        /// <param name="Zone"></param>
        /// <param name="lZoneType"></param>
        /// <param name="dtFr"></param>
        /// <param name="dtTo"></param>
        /// <param name="sConnStr"></param>
        /// <returns></returns>
        public dtData GetAssetSummaryOutsideHO(Guid sUserToken, List<int> lAssetIDs, Boolean byDriver, int iHOZone, List<int> lZoneType, DateTime dtFr, DateTime dtTo, String sConnStr, int? Page, int? LimitPerPage)
        {
            int UID = -1;
            List<int> lValidatedIds = CommonService.lValidateAssets(sUserToken, lAssetIDs, sConnStr, out UID);

            if (byDriver)
                lValidatedIds = CommonService.lValidateDrivers(sUserToken, lAssetIDs, sConnStr, out UID);
            else
                lValidatedIds = CommonService.lValidateAssets(sUserToken, lAssetIDs, sConnStr, out UID);

            List<int> lZoneHoIds = new List<int>();
            lZoneHoIds.Add(iHOZone);

            List<int> lValidatedZoneIds = CommonService.lValidateZones(sUserToken, lZoneHoIds, sConnStr, out UID);
            int ZoneHOid = lValidatedZoneIds[0];

            DataTable dtVehicleSummaryOutsideZone = new AssetSummaryService().VehicleSummaryOutsideZone(UID, ZoneHOid, lZoneType, lValidatedIds, dtFr, dtTo, byDriver, sConnStr);

            DataTable ResultTable = new DataTable();
            ResultTable.Columns.Add("rowNum", typeof(int));
            ResultTable.Columns["rowNum"].AutoIncrement = true;
            ResultTable.Columns["rowNum"].AutoIncrementSeed = 1;
            ResultTable.Columns["rowNum"].AutoIncrementStep = 1;
            ResultTable.Merge(dtVehicleSummaryOutsideZone);

            Pagination p = Pagination.GetRowNums(Page, LimitPerPage);
            DataTable dt = new DataTable();
            DataRow[] drChk = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo);
            if (drChk != null && drChk.Length > 0)
                dt = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo).CopyToDataTable();
            else
                dt = ResultTable;

            ResultTable.Columns.Remove("rowNum");
            dtData dtR = new dtData();
            dtR.Data = dt;
            dtR.Rowcnt = dtVehicleSummaryOutsideZone.Rows.Count;

            return dtR;
        }
        #endregion


        #region 
        public dtData GetDriverScoreCard(List<int> ids, Boolean IsDriver, DateTime dtFrom, DateTime dtTo, String sConnStr, int? Page, int? LimitPerPage)
        {

            dtData dtdata = new dtData();

            string sType = string.Empty;

            if (IsDriver)
                sType = "Drivers";

            ids = ids.Distinct().ToList();
            DataTable dt = new NaveoOneLib.Services.Trips.TripHeaderService().GetScoreCards(ids, sType, dtFrom, dtTo, sConnStr);

            if (dt.Columns.Contains("driverid"))
            {
                dt.Columns["driverid"].ColumnName = "DriverID";

            }

            if (dt.Columns.Contains("assetnumber"))
            {
                dt.Columns["assetnumber"].ColumnName = "AssetNumber";

            }

            if (dt.Columns.Contains("drivername"))
                dt.Columns["drivername"].ColumnName = "DriverName";

            if (dt.Columns.Contains("drivername"))
                dt.Columns["drivername"].ColumnName = "DriverName";

            if (dt.Columns.Contains("tripdistance"))
                dt.Columns["tripdistance"].ColumnName = "TripDistance";

            if (dt.Columns.Contains("Idlingtime"))
                dt.Columns["Idlingtime"].ColumnName = "Idlingtime";

            if (dt.Columns.Contains("triptime"))
                dt.Columns["triptime"].ColumnName = "TripTime";

            if (dt.Columns.Contains("overspeed1duration"))
                dt.Columns["overspeed1duration"].ColumnName = "OverSpeed1Duration";

            if (dt.Columns.Contains("overspeed2duration"))
                dt.Columns["overspeed2duration"].ColumnName = "OverSpeed2Duration";

            if (dt.Columns.Contains("accel"))
                dt.Columns["accel"].ColumnName = "Accel";

            if (dt.Columns.Contains("corner"))
                dt.Columns["corner"].ColumnName = "Corner";

            if (dt.Columns.Contains("accidents"))
                dt.Columns["accidents"].ColumnName = "Accidents";


            int rowCnt = 0;

            if (dt.Rows.Count > 0 && dt.Rows != null)
                rowCnt = dt.Rows.Count;



            dtdata.Data = dt;
            dtdata.Rowcnt = dt.Rows.Count;

            return dtdata;
        }



        #endregion


        #region Spatial Trip data
        public BaseModel CreateTripsSpatial(Guid sUserToken, List<int> lAssetIDs, DateTime dtFr, DateTime dtTo, Boolean byDriver, String sConnStr)
        {
            String responseString = NaveoOneLib.Maps.NaveoWebMapsCall.CreateTripsSpatial(sUserToken, lAssetIDs, dtFr, dtTo, byDriver, sConnStr);

            BaseModel bM = new BaseModel();
            bM.data = responseString;
            return bM;
        }
        #endregion
    }
}