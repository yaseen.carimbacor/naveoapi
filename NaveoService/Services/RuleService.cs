﻿using NaveoOneLib.Models;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Reportings;
using NaveoOneLib.Models.Rules;
using NaveoOneLib.Models.Users;
using NaveoOneLib.Services;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Services.Rules;
using NaveoOneLib.Services.Users;
using NaveoService.Models;
using NaveoService.Models.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using static NaveoService.Constants.RuleTemplate;
using Group = NaveoService.Models.DTO.Group;
using TreeSelection = NaveoService.Models.DTO.TreeSelection;

namespace NaveoService.Services
{
    public class RuleService
    {
        #region Methods
        /// <summary>
        /// Get Rule for fuel
        /// </summary>
        /// <param name="sUserToken"></param>
        /// <param name="fuelType"></param>
        /// <param name="sConnStr"></param>
        /// <returns></returns>
        public Models.FuelRule GetFuelRule(Guid sUserToken, String fuelType, String sConnStr)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);

            BaseModel bmFuel = new RulesService().GetFuelRuleNotif(UID, fuelType, sConnStr);
            Models.FuelRule ff = new Models.FuelRule();
            ff.lUsers = bmFuel.data;

            return ff;
        }

        /// <summary>
        /// update Rule for fuel
        /// </summary>
        /// <param name="sUserToken"></param>
        /// <param name="lUsers"></param>
        /// <param name="fuelType"></param>
        /// <param name="sConnStr"></param>
        /// <returns></returns>
        public Boolean UpdateFuelRule(Guid sUserToken, String fuelType, List<int> lUsers, List<notifyList> lnF, String sConnStr)
        {
            int UID = -1;
            List<int> lValidatedUsers = CommonService.lValidateUsers(sUserToken, lUsers, sConnStr, out UID);

            BaseModel bmFuel = new RulesService().UpdateFuelRuleNotif(UID, fuelType, lUsers, lnF, sConnStr);
            return bmFuel.data;
        }


        /// <summary>
        /// Get all Asset Data
        /// </summary>
        /// <param name="UID"></param>
        /// <param name="sConnStr"></param>
        /// <returns></returns>
        public dtData GetRules(Guid sUserToken,string ruleCategory,string sConnStr, int? Page, int? LimitPerPage)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);

            DataTable dtRules = new RulesService().GetRules(lm,ruleCategory,sConnStr);


            #region case sensitive logic for PostGres sql
            if (dtRules.Columns.Contains("ruleid"))
                dtRules.Columns["ruleid"].ColumnName = "RuleID";

            if (dtRules.Columns.Contains("parentruleid"))
                dtRules.Columns["parentruleid"].ColumnName = "ParentRuleID";

            if (dtRules.Columns.Contains("ruletype"))
                dtRules.Columns["ruletype"].ColumnName = "RuleType";

            if (dtRules.Columns.Contains("mintriggervalue"))
                dtRules.Columns["mintriggervalue"].ColumnName = "MinTriggerValue";

            if (dtRules.Columns.Contains("maxtriggervalue"))
                dtRules.Columns["maxtriggervalue"].ColumnName = "MaxTriggerValue";

            if (dtRules.Columns.Contains("struc"))
                dtRules.Columns["struc"].ColumnName = "Struc";

            if (dtRules.Columns.Contains("strucvalue"))
                dtRules.Columns["strucvalue"].ColumnName = "StrucValue";

            if (dtRules.Columns.Contains("struccondition"))
                dtRules.Columns["struccondition"].ColumnName = "StrucCondition";

            if (dtRules.Columns.Contains("structype"))
                dtRules.Columns["structype"].ColumnName = "StrucType";

            if (dtRules.Columns.Contains("rulename"))
                dtRules.Columns["rulename"].ColumnName = "RuleName";

            #endregion

            int count = dtRules.Rows.Count;

            DataTable ResultTable = dtRules.Clone();
            ResultTable.Columns.Add("rowNum", typeof(int));
            ResultTable.Columns["rowNum"].AutoIncrement = true;
            ResultTable.Columns["rowNum"].AutoIncrementSeed = 1;
            ResultTable.Columns["rowNum"].AutoIncrementStep = 1;
            foreach (DataRow dr in dtRules.Rows)
                ResultTable.Rows.Add(dr.ItemArray);

            int iTotalRec = ResultTable.Rows.Count;
            Pagination p = Pagination.GetRowNums(Page, LimitPerPage);
            DataTable dt = new DataTable();
            DataRow[] drChk = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo);
            if (drChk != null && drChk.Length > 0)
                dtRules = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo).CopyToDataTable();
            else
                dtRules = ResultTable;

            if (dtRules.Columns["RuleID"].ColumnName == "RULEID")
            {
                dtRules.Columns["RuleID"].ColumnName = "RuleID";
                dtRules.Columns["ParentRuleID"].ColumnName = "ParentRuleID";
                dtRules.Columns["RuleType"].ColumnName = "RuleType";
                dtRules.Columns["MinTriggerValue"].ColumnName = "MinTriggerValue";
                dtRules.Columns["MaxTriggerValue"].ColumnName = "MaxTriggerValue";
                dtRules.Columns["Struc"].ColumnName = "Struc";
                dtRules.Columns["StrucValue"].ColumnName = "StrucValue";
                dtRules.Columns["StrucCondition"].ColumnName = "StrucCondition";
                dtRules.Columns["StrucType"].ColumnName = "StrucType";
                dtRules.Columns["RuleName"].ColumnName = "RuleName";
            }

            dtData dtR = new dtData();
            dtR.Data = dtRules;
            dtR.Rowcnt = iTotalRec;

            return dtR;
        }

        public BaseModel GetRulesByParentRuleId(Guid sUserToken, string parentRuleId, string sConnStr)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);

            NaveoOneLib.Models.Rules.Rule Rules = new RulesService().GetRulesByParentId(parentRuleId, sConnStr);
            apiRule ruleDTO = new apiRule();
            ruleDTO.ruleCategory = Rules.StrucValue;
            
            ruleDTO.ruleName = Rules.RuleName;
            ruleDTO.ruleId = Rules.ParentRuleID;
            ruleDTO.oprType = Rules.oprType;

            Group g1 = new Group();
            Group g2 = new Group();


            if (Rules.Struc == "Fuel")
            {

                if (Rules.Struc == Struc_Fuel)
                {
                    ruleCondition rC = new ruleCondition();
                    rC.ruleId = Rules.RuleID;
                    rC.ruleType = Rules.RuleType;
                    rC.struc = Rules.Struc;
                    rC.strucValue = Rules.StrucValue;
                    rC.strucType = Rules.StrucType;
                    rC.strucCondition = Rules.StrucCondition;
                    rC.oprType = Rules.oprType;

                    ruleDTO.ruleConditions.Add(rC);
                }

            }
            else
            {


                foreach (var rules in Rules.lRules)
                {
                    if (rules.Struc == Struc_Zones || rules.Struc == Struc_ZoneTypes)
                    {
                        TreeSelection tsZones = new TreeSelection();
                        ruleDTO.zoneRules = new zoneRule();
                        ruleDTO.zoneRules.selectionCriteria = rules.StrucCondition;
                        ruleDTO.zoneRules.selectionType = rules.Struc;

                        g1.group = rules.Struc;

                        tsZones.id = Convert.ToInt32(rules.StrucValue);

                        string type = string.Empty;
                        if (rules.StrucType == "ID")
                            type = "Child";
                        else if (rules.StrucType == "GMID")
                            type = "Parent";

                        tsZones.ruleId = rules.RuleID;
                        tsZones.type = type;
                        tsZones.oprType = rules.oprType;

                        g1.lGroup.Add(tsZones);

                        ruleDTO.zoneRules.lTreeSelections = g1;
                    }

                    if (rules.Struc == Struct_Assets || rules.Struc == Struc_Drivers)
                    {
                        TreeSelection tsAssetDriver = new TreeSelection();
                        ruleDTO.assetDriverRules = new assetDriverRule();
                        ruleDTO.assetDriverRules.selectionType = rules.Struc;
                        tsAssetDriver.oprType = rules.oprType;

                        g2.group = rules.Struc;

                        tsAssetDriver.id = Convert.ToInt32(rules.StrucValue);

                        string type = string.Empty;
                        if (rules.StrucType == "ID")
                            type = "Child";
                        else if (rules.StrucType == "GMID")
                            type = "Parent";

                        tsAssetDriver.ruleId = rules.RuleID;
                        tsAssetDriver.type = type;
                        tsAssetDriver.oprType = rules.oprType;

                        g2.lGroup.Add(tsAssetDriver);

                        ruleDTO.assetDriverRules.lTreeSelections = g2;
                    }

                    if (rules.Struc == Struc_Speed)
                    {
                        ruleCondition rC = new ruleCondition();
                        rC.ruleId = rules.RuleID;
                        rC.ruleType = rules.RuleType;
                        rC.struc = rules.Struc;
                        rC.strucValue = rules.StrucValue;
                        rC.strucType = rules.StrucType;
                        rC.strucCondition = rules.StrucCondition;
                        rC.oprType = rules.oprType;
                        rC.minTriggerValue = rules.MinTriggerValue;

                        ruleDTO.ruleConditions.Add(rC);
                    }

                    if (rules.Struc == Struc_Time)
                    {
                        ruleCondition rC = new ruleCondition();
                        rC.ruleId = rules.RuleID;
                        rC.ruleType = rules.RuleType;
                        rC.struc = rules.Struc;
                        rC.strucValue = rules.StrucValue;
                        rC.strucType = rules.StrucType;
                        rC.strucCondition = rules.StrucCondition;
                        rC.oprType = rules.oprType;

                        ruleDTO.ruleConditions.Add(rC);
                    }

                    if (rules.Struc == Struc_PostedRoadSpeed)
                    {
                        ruleCondition rC = new ruleCondition();
                        rC.ruleId = rules.RuleID;
                        rC.ruleType = rules.RuleType;
                        rC.struc = rules.Struc;
                        rC.strucValue = rules.StrucValue;
                        rC.strucType = rules.StrucType;
                        rC.strucCondition = rules.StrucCondition;
                        rC.oprType = rules.oprType;

                        ruleDTO.ruleConditions.Add(rC);
                    }

                    if (rules.Struc == Struc_Duration)
                    {
                        ruleCondition rC = new ruleCondition();
                        rC.ruleId = rules.RuleID;
                        rC.ruleType = rules.RuleType;
                        rC.struc = rules.Struc;
                        rC.strucValue = rules.StrucValue;
                        rC.strucType = rules.StrucType;
                        rC.strucCondition = rules.StrucCondition;
                        rC.oprType = rules.oprType;

                        ruleDTO.ruleConditions.Add(rC);
                    }

                    if (rules.Struc == Struc_Aux)
                    {
                        ruleCondition rC = new ruleCondition();
                        rC.ruleId = rules.RuleID;
                        rC.ruleType = rules.RuleType;
                        rC.struc = rules.Struc;
                        rC.strucValue = rules.StrucValue;
                        rC.strucType = rules.StrucType;
                        rC.strucCondition = rules.StrucCondition;
                        rC.oprType = rules.oprType;

                        ruleDTO.ruleConditions.Add(rC);
                    }


                    if (rules.Struc == Struc_EngineAux)
                    {
                        ruleCondition rC = new ruleCondition();
                        rC.ruleId = rules.RuleID;
                        rC.ruleType = rules.RuleType;
                        rC.struc = rules.Struc;
                        rC.strucValue = rules.StrucValue;
                        rC.strucType = rules.StrucType;
                        rC.strucCondition = rules.StrucCondition;
                        rC.oprType = rules.oprType;

                        ruleDTO.ruleConditions.Add(rC);
                    }

                    if (rules.Struc == Struc_Day)
                    {
                        ruleCondition rC = new ruleCondition();
                        rC.ruleId = rules.RuleID;
                        rC.ruleType = rules.RuleType;
                        rC.struc = rules.Struc;
                        rC.strucValue = rules.StrucValue;
                        rC.strucType = rules.StrucType;
                        rC.strucCondition = rules.StrucCondition;
                        rC.oprType = rules.oprType;

                        ruleDTO.ruleConditions.Add(rC);
                    }

                    if (rules.Struc == Struc_Maintenance)
                    {
                        ruleCondition rC = new ruleCondition();
                        rC.ruleId = rules.RuleID;
                        rC.ruleType = rules.RuleType;
                        rC.struc = rules.Struc;
                        rC.strucValue = rules.StrucValue;
                        rC.strucType = rules.StrucType;
                        rC.strucCondition = rules.StrucCondition;
                        rC.oprType = rules.oprType;

                        ruleDTO.ruleConditions.Add(rC);
                    }

                    if (rules.Struc == Struc_Planning)
                    {
                        ruleCondition rC = new ruleCondition();
                        rC.ruleId = rules.RuleID;
                        rC.ruleType = rules.RuleType;
                        rC.struc = rules.Struc;
                        rC.strucValue = rules.StrucValue;
                        rC.strucType = rules.StrucType;
                        rC.strucCondition = rules.StrucCondition;
                        rC.oprType = rules.oprType;

                        ruleDTO.ruleConditions.Add(rC);
                    }


                    if (rules.Struc == Struc_AssetMaintenances)
                    {
                        ruleCondition rC = new ruleCondition();
                        rC.ruleId = rules.RuleID;
                        rC.ruleType = rules.RuleType;
                        rC.struc = rules.Struc;
                        rC.strucValue = rules.StrucValue;
                        rC.strucType = rules.StrucType;
                        rC.strucCondition = rules.StrucCondition;
                        rC.oprType = rules.oprType;

                        ruleDTO.ruleConditions.Add(rC);
                    }


                    if (rules.Struc == Struc_Fuel)
                    {
                        ruleCondition rC = new ruleCondition();
                        rC.ruleId = rules.RuleID;
                        rC.ruleType = rules.RuleType;
                        rC.struc = rules.Struc;
                        rC.strucValue = rules.StrucValue;
                        rC.strucType = rules.StrucType;
                        rC.strucCondition = rules.StrucCondition;
                        rC.oprType = rules.oprType;

                        ruleDTO.ruleConditions.Add(rC);
                    }

                }
            }
            var notifyList = Rules.NotifyList;

            foreach (var dllnL in notifyList)
            {
                NotificationList nl = new NotificationList();
                nl.arID = dllnL.ARID;
                nl.targetID = (int)dllnL.TargetID;
                nl.notificationType = (int)dllnL.ReportID;
                nl.oprType = dllnL.oprType;

                foreach (var notifydetails in dllnL.lDetails)
                {


                    notificationListdetails nld = new notificationListdetails();
                    nld.ardID = notifydetails.ARDID;
                    nld.arID = notifydetails.ARID;
                    nld.type = notifydetails.UIDType;
                    nld.userId = notifydetails.UID;
                    nld.oprType = notifydetails.oprType;

                    string category = string.Empty;
                    if (notifydetails.UIDCategory == "ID")
                        category = "Child";
                    else if (notifydetails.UIDCategory == "GMID")
                        category = "Parent";

                    nld.category = notifydetails.UIDCategory;

                    nl.lDetails.Add(nld);
                }
                ruleDTO.notificationLists.Add(nl);
            }

            BaseModel dtR = new BaseModel();
            dtR.data = ruleDTO;
            return dtR;
        }


        public Boolean SaveUpdateRule(Guid sUserToken, string sConnStr, RuleDTO ruleDTO, Boolean bInsert)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.Rules.Rule> lRules = new List<NaveoOneLib.Models.Rules.Rule>();
            NaveoOneLib.Models.Rules.Rule dllrule = new NaveoOneLib.Models.Rules.Rule();

            var ruleFromUI = ruleDTO.rule;
            BaseModel RulesToDeletedBaseModel = new BaseModel();

            if (!bInsert)
                RulesToDeletedBaseModel = new NaveoService.Services.RuleService().GetRulesByParentRuleId(sUserToken, ruleFromUI.ruleId.ToString(), sConnStr);

            var ruleTobBeDeleted = RulesToDeletedBaseModel.data;

            #region ruleHeader Exeption Type ,Engine or Driving Rule
            dllrule.RuleID = ruleFromUI.ruleId;
            dllrule.ParentRuleID = ruleFromUI.ruleId;
            dllrule.RuleName = ruleFromUI.ruleName;
            dllrule.RuleType = ExceptionType;
            dllrule.oprType = ruleFromUI.oprType;
            dllrule.Struc = Exception_Type_Desc;

          
            if (ruleFromUI.ruleCategory == FuelFill_Rule_Desc.ToString() || ruleFromUI.ruleCategory == FuelDrop_Rule_Desc.ToString())
             {

                dllrule.Struc = Struc_Fuel;
                dllrule.RuleType = RuleType_Fuel;
            }
            



            string strucValue = string.Empty;
            if (ruleFromUI.ruleCategory == Engine_Rule.ToString())
                strucValue = Engine_Rule.ToString();

            if (ruleFromUI.ruleCategory == Drving_Rule.ToString())
                strucValue = Drving_Rule.ToString();

            if (ruleFromUI.ruleCategory == Planning_Rule.ToString())
                strucValue = Planning_Rule.ToString();

            if (ruleFromUI.ruleCategory == AssetMaint_Rule.ToString())
                strucValue = AssetMaint_Rule.ToString();

            if (ruleFromUI.ruleCategory == FuelFill_Rule_Desc.ToString())
                strucValue = FuelFill_Rule_Desc.ToString();

            if (ruleFromUI.ruleCategory == FuelDrop_Rule_Desc.ToString())
                strucValue = FuelDrop_Rule_Desc.ToString();

            dllrule.StrucValue = strucValue;

            lRules.Add(dllrule);

            #region Notification List 
            List<AutoReportingConfig> LARC = new List<AutoReportingConfig>();
            List<AutoReportingConfig> LARC1 = new List<AutoReportingConfig>();
            int count = 0;
            foreach (var autoCOnfigHeader in ruleFromUI.notificationLists)
            {
                count++;
                count = count - 1;
                AutoReportingConfig autoR = new AutoReportingConfig();
                autoR.ARID = autoCOnfigHeader.arID;
                autoR.TargetID = ruleFromUI.ruleId;
                autoR.ReportID = autoCOnfigHeader.notificationType;
                autoR.oprType = autoCOnfigHeader.oprType;

                LARC.Add(autoR);

                if (!bInsert)
                {
                    if (autoCOnfigHeader.oprType == DataRowState.Added)
                    {
                        autoR.ARID = 0;
                        autoR.oprType = DataRowState.Added;
                    }
                }

                List<AutoReportingConfigDetail> LARCD = new List<AutoReportingConfigDetail>();
                var t = LARC.LastOrDefault();
                foreach (var autoConfigDetails in autoCOnfigHeader.lDetails)
                {
                    AutoReportingConfigDetail autoRDetails = new AutoReportingConfigDetail();
                    autoRDetails.ARDID = autoConfigDetails.ardID;
                    autoRDetails.ARID = autoCOnfigHeader.arID;
                    autoRDetails.UIDType = autoConfigDetails.type;
                    autoRDetails.oprType = autoCOnfigHeader.oprType;//autoConfigDetails.oprType; //DataRowState.Added;//autoConfigDetails.oprType;
                    autoRDetails.UIDType = autoConfigDetails.type;
                    autoRDetails.UID = autoConfigDetails.userId;

                    if (!bInsert)
                    {
                        if (autoCOnfigHeader.oprType == DataRowState.Added)
                        {
                            autoRDetails.oprType = DataRowState.Added;
                            autoRDetails.ARDID = 0;
                            autoRDetails.ARID = 0;
                        }
                    }

                    string UIDCategory = string.Empty;
                    if (autoConfigDetails.category == "Parent")
                        UIDCategory = "GMID";
                    else if (autoConfigDetails.category == "Child")
                        UIDCategory = "ID";

                    autoRDetails.UIDCategory = UIDCategory;

                    LARCD.Add(autoRDetails);

                    t.lDetails = LARCD;
                }
                LARC1.Add(t);
            }
            #endregion
            #endregion

            #region Zone or ZoneTypes
            if (ruleFromUI.zoneRules != null)
            {
                foreach (TreeSelection rule1 in ruleFromUI.zoneRules.lTreeSelections.lGroup)
                {
                    NaveoOneLib.Models.Rules.Rule zoneRule = new NaveoOneLib.Models.Rules.Rule();

                    zoneRule.RuleID = rule1.ruleId;
                    zoneRule.ParentRuleID = ruleFromUI.ruleId;
                    zoneRule.oprType = ruleFromUI.oprType;
                    if (ruleFromUI.zoneRules.selectionType == Struc_Zones)
                    {
                        zoneRule.RuleType = RuleType_Zones;
                        zoneRule.Struc = Struc_Zones;
                    }

                    if (ruleFromUI.zoneRules.selectionType == Struc_ZoneTypes)
                    {
                        zoneRule.RuleType = RuleType_ZoneTypes;
                        zoneRule.Struc = Struc_ZoneTypes;
                    }

                    zoneRule.StrucValue = rule1.id.ToString();
                    zoneRule.StrucCondition = ruleFromUI.zoneRules.selectionCriteria;
                    zoneRule.RuleName = ruleFromUI.ruleName;
                    zoneRule.oprType = rule1.oprType;

                    string type = string.Empty;

                    if (rule1.type == "Child")
                        type = "ID";
                    else if (rule1.type == "Parent")
                        type = "GMID";

                    zoneRule.StrucType = type;

                    lRules.Add(zoneRule);
                }
            }
            #endregion

            #region Assets or Drivers
            if(ruleFromUI.assetDriverRules != null && ruleFromUI.assetDriverRules.lTreeSelections != null)
            {
                foreach (TreeSelection rule2 in ruleFromUI.assetDriverRules.lTreeSelections.lGroup)
                {
                    NaveoOneLib.Models.Rules.Rule assetDriverRule = new NaveoOneLib.Models.Rules.Rule();

                    assetDriverRule.RuleID = rule2.ruleId;
                    assetDriverRule.ParentRuleID = ruleFromUI.ruleId;
                    assetDriverRule.oprType = ruleFromUI.oprType;
                    if (ruleFromUI.assetDriverRules.selectionType == Struct_Assets)
                    {
                        assetDriverRule.RuleType = RuleType_Assets;
                        assetDriverRule.Struc = Struct_Assets;
                    }

                    if (ruleFromUI.assetDriverRules.selectionType == Struc_Drivers)
                    {
                        assetDriverRule.RuleType = RuleType_Drivers;
                        assetDriverRule.Struc = Struc_Drivers;
                    }

                    assetDriverRule.StrucValue = rule2.id.ToString();
                    assetDriverRule.RuleName = ruleFromUI.ruleName;
                    assetDriverRule.oprType = rule2.oprType;

                    string type = string.Empty;

                    if (rule2.type == "Child")
                        type = "ID";
                    else if (rule2.type == "Parent")
                        type = "GMID";

                    assetDriverRule.StrucType = type;

                    lRules.Add(assetDriverRule);
                }
            }

            #endregion

            #region RuleConditions

            if (ruleFromUI.ruleCategory != FuelFill_Rule_Desc.ToString() && ruleFromUI.ruleCategory != FuelDrop_Rule_Desc.ToString())
            {
                List<ruleCondition> MergeListofdeletedAddrules = new List<ruleCondition>();
                if (!bInsert)
                {

                    apiRule aaaaa = new apiRule();
                    List<apiRule> lll = new List<apiRule>();
                    lll.Add(ruleTobBeDeleted);

                    foreach (var xx in lll)
                    {
                        if (xx.ruleConditions.Count != 0)
                        {
                            foreach (var rCc in xx.ruleConditions)
                            {
                                rCc.oprType = DataRowState.Deleted;
                            }
                        }

                        var l = xx.ruleConditions;
                        var ll = ruleFromUI.ruleConditions;
                        l.AddRange(ll);
                        MergeListofdeletedAddrules.AddRange(l);
                    }


                }
                else
                {
                    MergeListofdeletedAddrules = ruleFromUI.ruleConditions;
                }

                foreach (ruleCondition rule3 in MergeListofdeletedAddrules)
                {
                    NaveoOneLib.Models.Rules.Rule ruleCondition = new NaveoOneLib.Models.Rules.Rule();

                    ruleCondition.RuleID = rule3.ruleId;
                    ruleCondition.ParentRuleID = ruleFromUI.ruleId;
                    ruleCondition.oprType = ruleFromUI.oprType;
                    if (rule3.struc == Struc_Speed)
                    {
                        ruleCondition.RuleType = RuleType_Speed;
                        ruleCondition.Struc = Struc_Speed;
                    }
                    else if (rule3.struc == Struc_Time)
                    {
                        ruleCondition.RuleType = RuleType_Time;
                        ruleCondition.Struc = Struc_Time;
                    }
                    else if (rule3.struc == Struc_Duration)
                    {
                        ruleCondition.RuleType = RuleType_Duration;
                        ruleCondition.Struc = Struc_Duration;
                    }
                    else if (rule3.struc == Struc_Aux)
                    {
                        ruleCondition.RuleType = RuleType_Aux;
                        ruleCondition.Struc = Struc_Aux;
                    }
                    else if (rule3.struc == Struc_EngineAux)
                    {
                        ruleCondition.RuleType = RuleType_EngineAux;
                        ruleCondition.Struc = Struc_EngineAux;
                    }
                    else if (rule3.struc == Struc_Maintenance)
                    {
                        ruleCondition.RuleType = RuleType_Maintenance;
                        ruleCondition.Struc = Struc_Maintenance;
                    }

                    if (rule3.struc == Struc_Drivers)
                    {
                        ruleCondition.RuleType = RuleType_Drivers;
                        ruleCondition.Struc = Struc_Drivers;
                    }

                    if (rule3.struc == Struc_Day)
                    {
                        ruleCondition.RuleType = RuleType_Day;
                        ruleCondition.Struc = Struc_Day;
                    }

                    if (rule3.struc == Struc_Fuel)
                    {
                        ruleCondition.RuleType = RuleType_Fuel;
                        ruleCondition.Struc = Struc_Fuel;
                    }

                    if (rule3.struc == Struc_Planning)
                    {
                        ruleCondition.RuleType = RuleType_Planning;
                        ruleCondition.Struc = Struc_Planning;
                    }


                    if (rule3.struc == Struc_AssetMaintenances)
                    {
                        ruleCondition.RuleType = RuleType_AssetMaintenances;
                        ruleCondition.Struc = Struc_AssetMaintenances;
                    }

                    if (rule3.struc == Struc_PostedRoadSpeed)
                    {
                        ruleCondition.RuleType = RuleType_PostedRoadSpeed;
                        ruleCondition.Struc = Struc_PostedRoadSpeed;
                    }


                    ruleCondition.StrucValue = rule3.strucValue;
                    ruleCondition.StrucCondition = rule3.strucCondition;
                    ruleCondition.StrucType = rule3.strucType;
                    ruleCondition.MinTriggerValue = rule3.minTriggerValue;
                    ruleCondition.MaxTriggerValue = rule3.maxTriggerValue;

                    ruleCondition.oprType = rule3.oprType;

                    if (!bInsert && rule3.oprType == 0)
                    {
                        ruleCondition.RuleID = 0;
                        ruleCondition.oprType = DataRowState.Added;
                    }

                    ruleCondition.RuleName = ruleFromUI.ruleName;

                    lRules.Add(ruleCondition);
                }
            }
            #endregion

            if (!bInsert)
            {
                List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);
                foreach (NaveoOneLib.Models.GMatrix.Matrix mm in lm)
                {
                    NaveoOneLib.Models.GMatrix.Matrix m = new NaveoOneLib.Models.GMatrix.Matrix();
                    m.GMDesc = mm.GMDesc;
                    m.GMID = mm.GMID;
                    m.iID = mm.iID;
                    m.MID = mm.MID;
                    //m.oprType = DataRowState.Added;
                    lRules[0].lMatrix.Add(m);
                }
            }

            Boolean saveFuelRule = false;
            //if (!bInsert)
            //{
            saveFuelRule = new NaveoOneLib.Services.Rules.RulesService().SaveRulesList(lRules, LARC1, bInsert, UID, sConnStr);
            //} else
            //{
            //   saveFuelRule = new NaveoOneLib.Services.Rules.RulesService().SaveRulesList(lRules, LARC1, bInsert, UID, sConnStr);
            //}

            BaseModel bD = new BaseModel();
            bD.data = saveFuelRule;
            return bD.data;
        }

        public BaseDTO getRuleTemplate(string category)
        {
            RuleTemplateCiteria rTc = new RuleTemplateCiteria();

            if (category == "Driving Rule")
            {

                #region exceptionType
                ruleTemplateHeader rTExceptionType = new ruleTemplateHeader();

                ruleTemplateDetails sC = new ruleTemplateDetails();
                List<ruleTemplateDetails> lsc0 = new List<ruleTemplateDetails>();

                sC.strucValue.Add("0");
                sC.strucValue.Add("1");

                lsc0.Add(sC);

                rTExceptionType.id = 1;
                rTExceptionType.description = "Exception Type";
                rTExceptionType.ruleType = 8;
                rTExceptionType.ruleTemplateDetails = lsc0;

                rTc.ruleTemplate.Add(rTExceptionType);
                #endregion

                #region Speed
                ruleTemplateHeader rTSpeed = new ruleTemplateHeader();

                ruleTemplateDetails sC1 = new ruleTemplateDetails();
                List<ruleTemplateDetails> lsc = new List<ruleTemplateDetails>();

                sC1.strucCondition.Add("=");
                sC1.strucCondition.Add(">=");
                sC1.strucCondition.Add("<=");
                lsc.Add(sC1);

                rTSpeed.id = 2;
                rTSpeed.description = "Speed";
                rTSpeed.ruleType = 4;
                rTSpeed.ruleTemplateDetails = lsc;


                List<string> strucType = new List<string>();
                strucType.Add("Speed");
                strucType.Add("Idle");
                strucType.Add("Stopped");
                rTSpeed.structType = strucType;

                rTc.ruleTemplate.Add(rTSpeed);
                #endregion

                #region Asset
                ruleTemplateHeader rTHAsset = new ruleTemplateHeader();

                ruleTemplateDetails rTD = new ruleTemplateDetails();
                List<ruleTemplateDetails> lr = new List<ruleTemplateDetails>();

                rTHAsset.id = 3;
                rTHAsset.description = "Assets";
                rTHAsset.ruleType = 2;

                List<string> strucType1 = new List<string>();
                strucType1.Add("ID");
                strucType1.Add("GMID");
                rTHAsset.structType = strucType1;

                rTc.ruleTemplate.Add(rTHAsset);

                #endregion

                #region Zones
                ruleTemplateHeader rTZone = new ruleTemplateHeader();

                ruleTemplateDetails sC2 = new ruleTemplateDetails();
                List<ruleTemplateDetails> lr1 = new List<ruleTemplateDetails>();
                sC2.strucCondition.Add("Entering zone");
                sC2.strucCondition.Add("Leaving zone");
                sC2.strucCondition.Add("Stopped inside zone");
                sC2.strucCondition.Add("Stopped outside zone");
                sC2.strucCondition.Add("Inside zone bounds");
                sC2.strucCondition.Add("Outside zone bounds");
                lr1.Add(sC2);

                rTZone.id = 4;
                rTZone.description = "Zones";
                rTZone.ruleType = 1;
                rTZone.ruleTemplateDetails = lr1;

                List<string> strucType2 = new List<string>();
                strucType2.Add("ID");
                strucType2.Add("GMID");
                rTZone.structType = strucType2;
                rTc.ruleTemplate.Add(rTZone);
                #endregion

                #region ZoneTypes
                ruleTemplateHeader rTZoneTpes = new ruleTemplateHeader();

                ruleTemplateDetails sC3 = new ruleTemplateDetails();
                List<ruleTemplateDetails> lr2 = new List<ruleTemplateDetails>();
                sC3.strucCondition.Add("Entering zone");
                sC3.strucCondition.Add("Leaving zone");
                sC3.strucCondition.Add("Stop inside zone");
                sC3.strucCondition.Add("Stopped outside zone");
                sC3.strucCondition.Add("Inside zone bounds");
                sC3.strucCondition.Add("Outside zone bounds");
                lr2.Add(sC3);

                rTZoneTpes.id = 5;
                rTZoneTpes.description = "ZoneTypes";
                rTZoneTpes.ruleType = 0;
                rTZoneTpes.ruleTemplateDetails = lr2;

                List<string> strucType3 = new List<string>();
                strucType3.Add("ID");
                strucType3.Add("GMID");
                rTZoneTpes.structType = strucType3;
                rTc.ruleTemplate.Add(rTZoneTpes);

                #endregion

                #region Time
                ruleTemplateHeader rTTime = new ruleTemplateHeader();

                ruleTemplateDetails sC4 = new ruleTemplateDetails();
                List<ruleTemplateDetails> lr3 = new List<ruleTemplateDetails>();
                sC4.strucCondition.Add(">=");
                sC4.strucCondition.Add("<=");
                lr3.Add(sC4);

                rTTime.id = 6;
                rTTime.description = "Time";
                rTTime.ruleType = 5;
                rTTime.ruleTemplateDetails = lr3;

                rTc.ruleTemplate.Add(rTTime);

                #endregion

                #region Duration
                ruleTemplateHeader rTDuration = new ruleTemplateHeader();

                ruleTemplateDetails sC5 = new ruleTemplateDetails();
                List<ruleTemplateDetails> lr4 = new List<ruleTemplateDetails>();
                sC5.strucCondition.Add(">=");
                sC5.strucCondition.Add("<=");
                lr4.Add(sC5);

                rTDuration.id = 7;
                rTDuration.description = "Duration";
                rTDuration.ruleType = 7;
                rTDuration.ruleTemplateDetails = lr4;

                rTc.ruleTemplate.Add(rTDuration);
                #endregion

                #region Aux
                ruleTemplateHeader rTAUX = new ruleTemplateHeader();

                ruleTemplateDetails sCAux = new ruleTemplateDetails();
                List<ruleTemplateDetails> lscAux = new List<ruleTemplateDetails>();

                sCAux.strucValue.Add("Aux1");
                sCAux.strucValue.Add("Aux2");
                sCAux.strucValue.Add("Aux3");
                sCAux.strucValue.Add("Aux4");
                sCAux.strucValue.Add("Aux5");
                sCAux.strucValue.Add("Aux6");

                sCAux.strucCondition.Add("= 'On'");
                sCAux.strucCondition.Add("= 'Off'");
                lscAux.Add(sCAux);

                rTAUX.id = 8;
                rTAUX.description = "Aux";
                rTAUX.ruleType = 6;
                rTAUX.ruleTemplateDetails = lscAux;

                rTc.ruleTemplate.Add(rTAUX);

                #endregion

            }
            else if (category == "Engine Rule")
            {
                #region Option1 -EngineAux

                #region exceptionType
                ruleTemplateHeader rTExceptionType = new ruleTemplateHeader();

                ruleTemplateDetails sC = new ruleTemplateDetails();
                List<ruleTemplateDetails> lsc0 = new List<ruleTemplateDetails>();

                sC.strucValue.Add("0");
                sC.strucValue.Add("1");

                lsc0.Add(sC);

                rTExceptionType.id = 1;
                rTExceptionType.description = "Exception Type";
                rTExceptionType.ruleType = 8;
                rTExceptionType.ruleTemplateDetails = lsc0;

                rTc.ruleTemplate.Add(rTExceptionType);
                #endregion

                #region Temperature
                ruleTemplateHeader rTTemperature = new ruleTemplateHeader();

                ruleTemplateDetails sC6 = new ruleTemplateDetails();
                List<ruleTemplateDetails> lr5 = new List<ruleTemplateDetails>();
                sC6.strucValue.Add("Temperature");
                sC6.strucCondition.Add("=");
                sC6.strucCondition.Add(">=");
                sC6.strucCondition.Add("<=");
                lr5.Add(sC6);

                rTTemperature.id = 9;
                rTTemperature.description = "EngineAux";
                rTTemperature.ruleType = 6;
                rTTemperature.ruleTemplateDetails = lr5;

                rTc.ruleTemplate.Add(rTTemperature);

                #endregion


                #region Fuel
                ruleTemplateHeader rTFuel = new ruleTemplateHeader();

                ruleTemplateDetails sC7 = new ruleTemplateDetails();
                List<ruleTemplateDetails> lr6 = new List<ruleTemplateDetails>();
                sC7.strucValue.Add("Fuel");
                sC7.strucCondition.Add("=");
                sC7.strucCondition.Add(">=");
                sC7.strucCondition.Add("<=");
                lr6.Add(sC7);

                rTFuel.id = 10;
                rTFuel.description = "EngineAux";
                rTFuel.ruleType = 6;
                rTFuel.ruleTemplateDetails = lr6;
                rTc.ruleTemplate.Add(rTFuel);
                #endregion

                #region Harsh Acceleration
                ruleTemplateHeader rTAcceleration = new ruleTemplateHeader();

                ruleTemplateDetails sC8 = new ruleTemplateDetails();
                List<ruleTemplateDetails> lr7 = new List<ruleTemplateDetails>();
                sC8.strucValue.Add("Harsh Acceleration");
                sC8.strucCondition = null;
                lr7.Add(sC8);

                rTAcceleration.id = 11;
                rTAcceleration.description = "EngineAux";
                rTAcceleration.ruleType = 6;
                rTAcceleration.ruleTemplateDetails = lr7;
                rTc.ruleTemplate.Add(rTAcceleration);
                #endregion

                #region Harsh Breaking
                ruleTemplateHeader rTHarshBreaking = new ruleTemplateHeader();

                ruleTemplateDetails sC9 = new ruleTemplateDetails();
                List<ruleTemplateDetails> lr8 = new List<ruleTemplateDetails>();
                sC9.strucValue.Add("Harsh Breaking");
                sC9.strucCondition = null;
                lr8.Add(sC9);

                rTHarshBreaking.id = 12;
                rTHarshBreaking.description = "EngineAux";
                rTHarshBreaking.ruleType = 6;
                rTHarshBreaking.ruleTemplateDetails = lr8;
                rTc.ruleTemplate.Add(rTHarshBreaking);

                #endregion

                #region Power Reset
                ruleTemplateHeader rTHPowerReset = new ruleTemplateHeader();

                ruleTemplateDetails sC10 = new ruleTemplateDetails();
                List<ruleTemplateDetails> lr9 = new List<ruleTemplateDetails>();
                sC10.strucValue.Add("Power Reset");
                sC10.strucCondition = null;
                lr9.Add(sC10);

                rTHPowerReset.id = 13;
                rTHPowerReset.description = "EngineAux";
                rTHPowerReset.ruleType = 6;
                rTHPowerReset.ruleTemplateDetails = lr9;
                rTc.ruleTemplate.Add(rTHPowerReset);

                #endregion


                #region DriverToken
                ruleTemplateHeader rTHDriverToken = new ruleTemplateHeader();

                ruleTemplateDetails sC11 = new ruleTemplateDetails();
                List<ruleTemplateDetails> lr10 = new List<ruleTemplateDetails>();
                sC11.strucValue.Add("Driver Token");
                sC11.strucCondition = null;
                lr10.Add(sC11);

                rTHDriverToken.id = 14;
                rTHDriverToken.description = "EngineAux";
                rTHDriverToken.ruleType = 6;
                rTHDriverToken.ruleTemplateDetails = lr10;
                rTc.ruleTemplate.Add(rTHDriverToken);
                #endregion

                #region Not Reported
                ruleTemplateHeader rTHNotReported = new ruleTemplateHeader();

                ruleTemplateDetails sC12 = new ruleTemplateDetails();
                List<ruleTemplateDetails> lr11 = new List<ruleTemplateDetails>();
                sC12.strucValue.Add("Not Reported");
                sC12.strucCondition.Add("4");
                sC12.strucCondition.Add("12");
                sC12.strucCondition.Add("16");
                sC12.strucCondition.Add("24");
                sC12.strucCondition.Add("48");
                lr11.Add(sC12);

                rTHNotReported.id = 15;
                rTHNotReported.description = "EngineAux";
                rTHNotReported.ruleType = 6;
                rTHNotReported.ruleTemplateDetails = lr11;
                rTc.ruleTemplate.Add(rTHNotReported);
                #endregion

                #region GPS LOST
                ruleTemplateHeader rTHGPSlOST = new ruleTemplateHeader();

                ruleTemplateDetails sC13 = new ruleTemplateDetails();
                List<ruleTemplateDetails> lr12 = new List<ruleTemplateDetails>();
                sC13.strucValue.Add("Gps Lost");
                sC13.strucCondition = null;
                lr12.Add(sC13);

                rTHGPSlOST.id = 16;
                rTHGPSlOST.description = "EngineAux";
                rTHGPSlOST.ruleType = 6;
                rTHGPSlOST.ruleTemplateDetails = lr12;
                rTc.ruleTemplate.Add(rTHGPSlOST);
                #endregion

                #endregion

                #region Option 2 - Maintenance
                ruleTemplateHeader rTHMainenance = new ruleTemplateHeader();

                ruleTemplateDetails sC14 = new ruleTemplateDetails();
                List<ruleTemplateDetails> lr13 = new List<ruleTemplateDetails>();
                sC14.strucValue.Add("1");
                sC14.strucValue.Add("2");
                sC14.strucCondition.Add("Availability");
                sC14.strucCondition.Add("Status");


                lr13.Add(sC14);

                rTHMainenance.id = 17;
                rTHMainenance.description = "Maintenance";
                rTHMainenance.ruleType = 6;
                rTHMainenance.ruleTemplateDetails = lr13;

                rTc.ruleTemplate.Add(rTHMainenance);
                #endregion
            }



            BaseDTO bD = new BaseDTO();
            bD.data = rTc;
            bD.sQryResult = "Succeeded";
            return bD;
        }

        public Boolean Delete(Guid sUserToken, string parentRuleId, string ruleName, string sConnStr)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);

            NaveoOneLib.Models.Rules.Rule d = new NaveoOneLib.Models.Rules.Rule();
            d.ParentRuleID = Convert.ToInt32(parentRuleId);

            Boolean b = new NaveoOneLib.Services.Rules.RulesService().DeleteRules(d.ParentRuleID, ruleName, UID, sConnStr);

            BaseModel bm = new BaseModel();
            bm.data = b;
            return bm.data;
        }


        public BaseDTO GetExcepDashboardCounts(Guid sUserToken,string sConnStr)
        {
            BaseDTO bm = new BaseDTO();

            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);

            AlertCount alertCount = new RulesService().GetExcepDashboardCounts(lm, sConnStr);



            bm.data = alertCount;
            bm.totalItems = 6;
            return bm;

        }

        #endregion
    }
}