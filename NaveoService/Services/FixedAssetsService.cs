﻿using NaveoOneLib.Common;
using NaveoOneLib.Constant;
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Services.GMatrix;
using NaveoService.Helpers;
using NaveoService.Models;
using NaveoService.Models.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Services
{
    public class FixedAssetsService
    {
        public BaseModel GetFaDataByIds(int UID, List<int> lids, Boolean ShowBuffers, Boolean ShowDataOnly, List<int> statusId, List<int> typeId, RoadNetwork roadnetwork, string hostURL, string sConnStrMaint, string sConnStr)
        {
            BaseModel bm = new BaseModel();
            int iBuffer = 0;
            if (ShowBuffers)
                iBuffer = 300;

            lids = lids.Distinct().ToList();
            DataTable dtFa = new NaveoOneLib.Services.Assets.FixedAssetService().dtGetFaWithinDistanceInMeters(UID, iBuffer, lids, roadnetwork, sConnStr);

            #region Maintenance
            //Status is NONE
            if (statusId.Any(status => status == 9999))
            {
                hostURL = hostURL.ToLower();
                hostURL = hostURL.Replace(".naveo.mu", String.Empty);
                DataTable dtStatus = new NaveoOneLib.Services.Assets.FixedAssetService().dtGetMaintStatusType(UID, lids, statusId, typeId, hostURL, sConnStrMaint);
                dtFa.Columns.Add("Date");

                foreach (DataRow dr in dtStatus.Rows)
                {
                    DataRow[] dataRows = dtFa.Select("AssetID = " + dr["AssetIDfleet"] + "");
                    foreach (DataRow dr1 in dataRows)
                    {
                        dr1["AssetCategoryDesc"] = dr["STATUS"];
                        dr1["MaintType"] = dr["TYPE"];
                        dr1["MaintStatus"] = dr["STATUS"];
                        dr1["Date"] = dr["UpdatedDate"];
                    }
                }
            }
            else if (statusId.Count > 0)
            {
                hostURL = hostURL.ToLower();
                hostURL = hostURL.Replace(".naveo.mu", String.Empty);
                DataTable dtStatus = new NaveoOneLib.Services.Assets.FixedAssetService().dtGetMaintStatusType(UID, lids, statusId, typeId, hostURL, sConnStrMaint);
                dtFa.Columns.Add("Date");

                foreach (DataRow dr in dtStatus.Rows)
                {
                    //TO REMOVE HARD CODE VALUE /*dr["AssetIDfleet"]*/9880
                    DataRow[] dataRows = dtFa.Select("AssetID = " + dr["AssetIDfleet"] + "");
                    foreach (DataRow dr1 in dataRows)
                    {
                        dr1["AssetCategoryDesc"] = dr["STATUS"];
                        dr1["MaintType"] = dr["TYPE"];
                        dr1["MaintStatus"] = dr["STATUS"];
                        dr1["Date"] = dr["UpdatedDate"];
                    }
                }
                DataRow[] dataRowsFin = dtFa.Select("MaintStatus <> ''");
                DataTable dtFinal = dataRowsFin.CopyToDataTable();
                dtFa = dtFinal;
                dtFa.TableName = "dtResult";
            }

            #endregion
            DataSet ds = new DataSet();
            ds.Tables.Add(dtFa.Copy());
            bm.additionalData = ds;
            if (ShowDataOnly)
                bm.data = "ShowDataOnly";
            else
            {
                String responseString = NaveoOneLib.Maps.NaveoWebMapsCall.GetFADataFromMapServer(dtFa, 0, 0, false, "FixedAssets/CreateFAWithinDistanceInMeters", sConnStr);
                bm.data = responseString;
            }

            return bm;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UID"></param>
        /// <param name="lids"></param>
        /// <param name="ShowBuffers"></param>
        /// <param name="ShowDataOnly"></param>
        /// <param name="sConnStr"></param>
        /// <returns></returns>
        //public BaseModel GetMaintStatusType(int UID, List<int> lids, List<int> statusId, List<int> typeId, string hostURL, string sConnStr)
        //{
        //    BaseModel bm = new BaseModel();

        //    hostURL = hostURL.ToLower();
        //    hostURL = hostURL.Replace(".naveo.mu", String.Empty);

        //    DataTable dtStatus = new NaveoOneLib.Services.Assets.FixedAssetService().dtGetMaintStatusType(UID, lids, statusId, typeId, hostURL, sConnStr);
        //    bm.data = dtStatus;

        //    return bm;
        //}

        public BaseModel GetFixedAssetsByLonLat(Guid sUserToken, Double dDistance, Double lon, Double lat, String sConnStr)
        {
            NaveoOneLib.Services.Assets.FixedAssetService faSerice = new NaveoOneLib.Services.Assets.FixedAssetService();
            DataSet ds = new DataSet();
            DataTable dtFaData = faSerice.dtGetFaWithinDistanceInMeters(sUserToken, dDistance, lon, lat, sConnStr);
            if (dtFaData.Rows.Count > 0)
            {
                dtFaData.DefaultView.Sort = "distance asc";
                dtFaData = dtFaData.DefaultView.ToTable();

                int i = Convert.ToInt32(dtFaData.Rows[0]["AssetID"].ToString());
                ds = faSerice.GetFaByiID(i, sConnStr);

                List<String> PicNames = GetPicFileNames(sConnStr, ds.Tables["GFI_GIS_FixedAsset"].Rows[0]["AssetTypeDesc"].ToString(), ds.Tables["GFI_GIS_FixedAsset"].Rows[0]["AssetCode"].ToString());
                DataTable dtPicFileNames = new DataTable("PicFileNames");
                dtPicFileNames.Columns.Add("FN");
                foreach (String s in PicNames)
                    dtPicFileNames.Rows.Add(s);

                ds.Tables.Add(dtPicFileNames);
            }
            BaseModel bM = new BaseModel();
            bM.data = ds;
            return bM;
        }

        public BaseModel GetFAWithinDistanceInMeters(Guid sUserToken, Double dDistance, Double lon, Double lat, String sConnStr)
        {
            DataTable dtFaData = new NaveoOneLib.Services.Assets.FixedAssetService().dtGetFaWithinDistanceInMeters(sUserToken, dDistance, lon, lat, sConnStr);
            String responseString = NaveoOneLib.Maps.NaveoWebMapsCall.GetFADataFromMapServer(dtFaData, lon, lat, true, "FixedAssets/CreateFAWithinDistanceInMeters", sConnStr);

            BaseModel bM = new BaseModel();
            bM.data = responseString;
            return bM;
        }


        List<String> GetPicFileNames(String sConnStr, String AssetType, String AssetCode)
        {
            String dirPath = "FixedAssets\\" + sConnStr + "\\" + AssetType + "\\" + AssetCode + "\\";
            String path = Globals.DocsPath(dirPath);
            String[] files = Directory.GetFiles(path);
            List<String> fileNames = new List<string>();
            fileNames.Add(dirPath);
            foreach (string file in files)
                fileNames.Add(Path.GetFileName(file));

            return fileNames;
        }
        /// <summary>
        /// Get Asset details by Id
        /// </summary>
        /// <param name="sUserToken"></param>
        /// <param name="sConnStr"></param>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public BaseModel GetFixedAssetById(Guid sUserToken, string sConnStr, int assetId)
        {
            FixedAsset fixedasset = new NaveoOneLib.Services.Assets.FixedAssetService().GetFixedAssetById(sUserToken, assetId, sConnStr);
            fixedasset.PicNames = GetPicFileNames(sConnStr, fixedasset.gisAssetType.AssetType, fixedasset.AssetCode);
            BaseModel bm = new BaseModel();
            bm.data = fixedasset;

            return bm;
        }

        public BaseDTO SaveUpdateFixedAsset(SecurityTokenExtended securityToken, FixedAssetDTO fixedAssetDTO, Boolean bInsert)
        {
            String imageName = String.Empty;
            if (!String.IsNullOrEmpty(fixedAssetDTO.imageName))
                fixedAssetDTO.fixedAsset.PicNames = new List<string>() { fixedAssetDTO.imageName };
            else
                fixedAssetDTO.fixedAsset.PicNames = new List<string>() { "NoPic" };

            BaseModel assetObj = new NaveoOneLib.Services.Assets.FixedAssetService().SaveFixedAsset(fixedAssetDTO.fixedAsset, securityToken.securityToken.UID, bInsert, securityToken.securityToken.sConnStr);

            return assetObj.ToBaseDTO(securityToken.securityToken);
        }

        public BaseDTO SaveUpdateGISAssetCategory(SecurityTokenExtended securityToken, FixedAssetDTO fixedAssetDTO, Boolean bInsert)
        {
            Boolean assetObj = new NaveoOneLib.Services.Assets.FixedAssetService().SaveGISAssetCategory(fixedAssetDTO.gisAssetCategory, bInsert, securityToken.securityToken.sConnStr);

            BaseModel bm = new BaseModel();
            bm.data = assetObj;

            return bm.ToBaseDTO(securityToken.securityToken);
        }

        public BaseDTO SaveFixedAssetsFromShp(SecurityTokenExtended securityToken, String shpPath, int iMatrix)
        {
            BaseModel assetObj = new NaveoOneLib.Services.Assets.FixedAssetService().SaveFixedAssetsFromShp(securityToken.securityToken.UserToken, shpPath, iMatrix, securityToken.securityToken.sConnStr);

            return assetObj.ToBaseDTO(securityToken.securityToken);
        }

        public string FixedAssetCreationTOGMS(SecurityTokenExtended securityToken, FixedAsset fixedasset, string sServerName, String sConnStr)
        {
            string responseString = string.Empty;

            FixedAsset fa = new NaveoOneLib.Services.Assets.FixedAssetService().GetFixedAssetByAssetCode(securityToken.securityToken.UserToken, fixedasset.AssetCode, sConnStr);


            AssetDTO assetDTO = new AssetDTO();
            assetDTO.gmsAsset.asset.CustomerName = fa.lMatrix[0].GMDesc;
            assetDTO.gmsAsset.asset.IsFlagged = true;
            assetDTO.gmsAsset.asset.FleetServerId = 0;
            assetDTO.gmsAsset.asset.FleetServer = sServerName;

            assetDTO.gmsAsset.asset.FMSAssetId = fa.AssetID;


            assetDTO.gmsAsset.asset.IsInService = true;
            assetDTO.gmsAsset.asset.Number = fa.AssetCode;
            assetDTO.gmsAsset.asset.Remarks = "FixedAsset";

            string url = NaveoEntity.GMSAPIAddress + "InsertAsset";




            try
            {

                responseString = new NaveoService.Helpers.ApiHelper().ApiCall("Post", url, assetDTO.gmsAsset);
            }
            catch (Exception ex)
            {
                responseString = ex.ToString();
                System.IO.File.AppendAllText(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "\\Registers\\gms.dat", "\r\n Fixed Asset Creation \r\n" + responseString + "\r\n");
            }

            return responseString;
        }

        public BaseModel MigrateFixedAssetToGMS(SecurityTokenExtended securityToken)
        {
            Boolean BInsert = false;
            int UID = CommonService.GetUserID(securityToken.securityToken.UserToken, securityToken.securityToken.sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, securityToken.securityToken.sConnStr);
            DataTable dtFixedAssets = new NaveoOneLib.Services.Assets.FixedAssetService().GetFixedAsset(lm, securityToken.securityToken.sConnStr);
            int TotalFixedAssets = dtFixedAssets.Rows.Count;

            string sServerName = new NaveoService.Helpers.ApiHelper().getServerName();
            //sServerName = "BABY";
            int cntCreated = 0;
            int cntExist = 0;

            string ErrorServer = string.Empty;
            foreach (DataRow dr in dtFixedAssets.Rows)
            {
                Asset a = new Asset();
                int assetId = Convert.ToInt32(dr["AssetID"]);
                FixedAsset fa = new NaveoOneLib.Services.Assets.FixedAssetService().GetFixedAssetById(securityToken.securityToken.UserToken, assetId, securityToken.securityToken.sConnStr);
                string status = string.Empty;

                if (fa != null)
                {
                    try
                    {
                        status = FixedAssetCreationTOGMS(securityToken, fa, sServerName, securityToken.securityToken.sConnStr);

                        string createdOrExist = string.Empty;
                        if (status.Contains("Asset already exists"))
                        {
                            cntExist++;
                            createdOrExist = "already exists on GMS,Total: " + cntExist;
                        }
                        else if (status.Contains("Valid"))
                        {
                            cntCreated++;
                            createdOrExist = "created on GMS,Total: " + cntCreated;
                        }

                        string message = "Fixed Asset: " + fa.AssetCode + " " + createdOrExist;
                        System.IO.File.AppendAllText(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "\\Registers\\gms.dat", "\r\n Migration fixed Asset \r\n" + message + "\r\n");
                        BInsert = true;
                    }
                    catch (Exception ex)
                    {
                        System.IO.File.AppendAllText(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "\\Registers\\gms.dat", "\r\n Migration fixed Asset \r\n" + ex.Message + "\r\n" + ex.InnerException);
                    }
                }
                ErrorServer = status;
            }

            string messsag = cntCreated + "out of " + TotalFixedAssets + "has been migrated to GMS,";

            BaseModel bm = new BaseModel();
            bm.data = BInsert;
            bm.errorMessage = messsag + "Server :" + sServerName;
            return bm;
        }

    }
}
