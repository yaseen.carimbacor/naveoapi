﻿using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.Drivers;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Plannings;
using NaveoOneLib.Models.Users;
using NaveoOneLib.Services;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Services.Rules;
using NaveoOneLib.Services.Trips;
using NaveoService.Helpers;
using NaveoService.Models;
using NaveoService.Models.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matrix = NaveoOneLib.Models.GMatrix.Matrix;
using Planning = NaveoOneLib.Models.Plannings.Planning;

namespace NaveoService.Services
{
    public class PlanningService
    {
        public dtData GetPlanning(Guid sUserToken, DateTime dtFrom, DateTime dtTo, int? PID, int approval, string sConnStr, int? Page, int? LimitPerPage, List<String> sortColumns, Boolean sortOrderAsc)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);
            DataTable dtPlanning = new NaveoOneLib.Services.Plannings.PlanningService().GetPlanningRequest(dtFrom, dtTo, lm, PID, sConnStr, sortColumns, sortOrderAsc);


     



            DataTable dtFinal = new DataTable();
            dtFinal.Columns.Add("pId");
            dtFinal.Columns.Add("requestDate");
            dtFinal.Columns.Add("dateCreated");
            dtFinal.Columns.Add("startTime");
            dtFinal.Columns.Add("endTime");
            dtFinal.Columns.Add("requestCode");
            dtFinal.Columns.Add("departureId");
            dtFinal.Columns.Add("departure");
            dtFinal.Columns.Add("arrivalId");
            
            dtFinal.Columns.Add("arrival");
            dtFinal.Columns.Add("status");
            dtFinal.Columns.Add("processedBy");
            dtFinal.Columns.Add("processedDate");
            dtFinal.Columns.Add("coordinatesFrom");
            dtFinal.Columns.Add("coordinatesTo");
            dtFinal.Columns.Add("dateFrom");
            dtFinal.Columns.Add("dateTo");
            dtFinal.Columns.Add("type");
            dtFinal.Columns.Add("ApprovalID");

            if (approval == 0)
            {
                DataView view = new DataView(dtPlanning);
                //equal to 0
                view.RowFilter = "Approval ='0'";
                dtPlanning = view.ToTable();
            }


               DataView view1 = new DataView(dtPlanning);
               //not equal to 200
               view1.RowFilter = "Type <> '200'";
               dtPlanning = view1.ToTable();

            foreach (DataRow drPlanning in dtPlanning.Rows)
            {
                DataRow row = dtFinal.NewRow();

                row["departureId"] = drPlanning["DepartureID"].ToString() == String.Empty ? 0 : Convert.ToInt32(drPlanning["DepartureID"]);
                row["departure"] = drPlanning["Departure"].ToString();
                row["arrivalId"] = drPlanning["ArrivalID"].ToString() == String.Empty ? 0 : Convert.ToInt32(drPlanning["ArrivalID"]);

                row["pId"] = drPlanning["PID"].ToString();
                row["requestCode"] = drPlanning["RequestCode"].ToString();

                DateTime dtRequestDate = new DateTime();
                dtRequestDate = Convert.ToDateTime(drPlanning["dtFrom"]);
                row["requestDate"] = dtRequestDate.ToString("dd/MM/yyyy"); ;

                DateTime dtcreatedDate = new DateTime();
                dtcreatedDate = Convert.ToDateTime(drPlanning["CreatedDate"]);
                row["dateCreated"] = dtcreatedDate.ToString("dd/MM/yyyy");

                DateTime dateFrom = new DateTime();
                dateFrom = Convert.ToDateTime(drPlanning["dtFrom"]).ToLocalTime();
                row["startTime"] = dateFrom.ToShortTimeString();

                DateTime dateTo = new DateTime();
                dateTo = Convert.ToDateTime(drPlanning["dtTo"]).ToLocalTime();
                row["endTime"] = dateTo.ToShortTimeString();

                row["departure"] = drPlanning["Departure"].ToString();
                row["arrival"] = drPlanning["Arrival"].ToString();

                row["processedBy"] = drPlanning["processedBy"].ToString();
                row["processedDate"] = Convert.ToDateTime(drPlanning["ProccessedDate"]).ToLocalTime();

                row["coordinatesFrom"] = drPlanning["CoordinatesFrom"];
                row["coordinatesTo"] = drPlanning["CoordinatesTo"];

                if (drPlanning["CoordinatesFrom"].ToString() == string.Empty)
                    row["coordinatesFrom"] = drPlanning["Departure"];

                if (drPlanning["CoordinatesTo"].ToString() == string.Empty)
                    row["coordinatesTo"] = drPlanning["Arrival"];

                string status = "0";

                if (drPlanning["Approval"].ToString() == string.Empty)
                    status = "0";
                else
                    status = drPlanning["Approval"].ToString();


                status = drPlanning["Approval"].ToString();

                if (status == "0")
                    row["status"] = "Not Scheduled";
                else
                    row["status"] = "Scheduled";

                row["dateFrom"] = Convert.ToDateTime(drPlanning["dtFrom"]).ToLocalTime();
                row["dateTo"] = Convert.ToDateTime(drPlanning["dtTo"]).ToLocalTime();
                row["type"] = drPlanning["type"].ToString();
                row["ApprovalID"] = drPlanning["ApprovalID"].ToString();
                dtFinal.Rows.Add(row);
            }

            DataTable ResultTable = dtFinal.Clone();
            ResultTable.Columns.Add("rowNum", typeof(int));
            ResultTable.Columns["rowNum"].AutoIncrement = true;
            ResultTable.Columns["rowNum"].AutoIncrementSeed = 1;
            ResultTable.Columns["rowNum"].AutoIncrementStep = 1;
            foreach (DataRow dr in dtFinal.Rows)
                ResultTable.Rows.Add(dr.ItemArray);

            int iTotalRec = ResultTable.Rows.Count;
            Pagination p = Pagination.GetRowNums(Page, LimitPerPage);
            DataTable dt = new DataTable();
            DataRow[] drChk = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo);
            if (drChk != null && drChk.Length > 0)
                dt = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo).CopyToDataTable();
            else
                dt = ResultTable;

            dtData dtR = new dtData();
            dtR.Data = dt;
            dtR.Rowcnt = iTotalRec;

            return dtR;
        }


        public Models.DTO.apiPlanning GetPlanningByPID(Guid sUserToken, int PID, string sConnStr)
        {
            List<int> lPID = new List<int>();
            lPID.Add(PID);

            String sql = NaveoOneLib.Services.Plannings.ReqSchPlnService.sPlanningList(null, lPID, sConnStr);  //sPlanningList(null, lPIDs, sConnStr);

            int UID = -1;
            List<int> lValidatedPlanning = CommonService.lValidatePlanning(sUserToken, lPID, sConnStr, out UID);
            NaveoOneLib.Models.Plannings.Planning planning = new NaveoOneLib.Services.Plannings.PlanningService().GetPlanningByPID(lValidatedPlanning[0], sConnStr);
            Models.DTO.apiPlanning apiPlanning = new Models.DTO.apiPlanning();

            apiPlanning.pId = planning.PID;
            apiPlanning.approval = planning.Approval;
            apiPlanning.closureComments = planning.closureComments;
            apiPlanning.createdDate = Convert.ToDateTime(planning.CreatedDate);
            if (planning.CreatedBy != null)
            {
                NaveoService.Models.Custom.minifiedValues minifiedUser = new NaveoService.Models.Custom.minifiedValues();
                minifiedUser.id = Convert.ToInt32(planning.CreatedBy);
                minifiedUser.description = null;
                apiPlanning.createdUser = minifiedUser;
            }
            apiPlanning.updatedDate = Convert.ToDateTime(planning.UpdatedDate);
            if (planning.UpdatedBy != null)
            {
                NaveoService.Models.Custom.minifiedValues minifiedUpdatedUser = new NaveoService.Models.Custom.minifiedValues();
                minifiedUpdatedUser.id = Convert.ToInt32(planning.UpdatedBy);
                minifiedUpdatedUser.description = null;
                apiPlanning.updatedUser = minifiedUpdatedUser;
            }
            apiPlanning.reference = planning.Ref;
            apiPlanning.remarks = planning.Remarks;
            apiPlanning.requestCode = planning.requestCode;
            apiPlanning.parentRequestCode = planning.parentRequestCode;
            apiPlanning.sComments = planning.sComments;
            apiPlanning.closureComments = planning.closureComments;
            apiPlanning.type = planning.type;

            if (planning.urgent == 1)
                apiPlanning.urgent = true;
            else if (planning.urgent == 0)
                apiPlanning.urgent = false;

            apiPlanning.oprType = planning.OprType;

            #region Matrix
            if (planning.lMatrix != null)
            {
                foreach (Matrix m in planning.lMatrix)
                {
                    NaveoService.Models.DTO.apiMatrix apM = new NaveoService.Models.DTO.apiMatrix();
                    apM.iID = m.iID;
                    apM.gMID = m.GMID;
                    apM.gmDesc = m.GMDesc;
                    apM.miD = m.MID;
                    apM.oprType = m.oprType;

                    apiPlanning.lMatrix.Add(apM);
                }
            }
            #endregion

            #region PlanningLookUp
            if (planning.lPlanningLkup != null)
            {

                foreach (PlanningLkup lookup in planning.lPlanningLkup)
                {
                    NaveoService.Models.DTO.apiPlanningLkup apiPLKP = new NaveoService.Models.DTO.apiPlanningLkup();
                    apiPLKP.iID = lookup.iID;
                    apiPLKP.pId = lookup.PID;
                    apiPLKP.lookupType = lookup.LookupType;

                    if (lookup.LookupValueID != null)
                    {
                        NaveoService.Models.Custom.minifiedValues minifiedValues = new NaveoService.Models.Custom.minifiedValues();
                        minifiedValues.id = lookup.LookupValueID;
                        minifiedValues.description = lookup.LookupValueDesc;
                        apiPLKP.lookupValues = minifiedValues;
                    }

                    apiPlanning.lPlanningLkup.Add(apiPLKP);
                }
            }
            #endregion

            #region PlanningResource
            if (planning.lPlanningResource != null)
            {

                foreach (PlanningResource resource in planning.lPlanningResource)
                {
                    NaveoService.Models.DTO.apiPlanningResource apiReource = new NaveoService.Models.DTO.apiPlanningResource();
                    apiReource.iID = resource.iID;
                    apiReource.pId = resource.PID;
                    apiReource.ResourceType = resource.ResourceType;

                    if(resource.CustomType != null)
                         apiReource.customType = resource.CustomType.Value;

                    if (resource.MileageAllowance != null)
                        apiReource.mileageAllowance = resource.MileageAllowance.Value;

                    if (resource.TravelGrant != null)
                        apiReource.travelGrant = resource.TravelGrant.Value;

                    apiReource.remarks = resource.Remarks;

                    //if (resource.Remarks == "APPLICANT")
                    //    apiReource.remarks = string.Empty;

                    //if (resource.Remarks == "Approver")
                    //    apiReource.remarks = string.Empty;


                    if (resource.DriverID != null)
                    {
                        NaveoService.Models.Custom.minifiedValues minifiedResource = new NaveoService.Models.Custom.minifiedValues();
                        minifiedResource.id = resource.DriverID;
                        minifiedResource.description = resource.DriverName;
                        apiReource.driver = minifiedResource;

                    }


                    apiPlanning.lPlanningResource.Add(apiReource);
                }

            }
            #endregion

            #region PlanningViaPointH
            if (planning.lPlanningViaPointH != null)
            {

                foreach (PlanningViaPointH viaH in planning.lPlanningViaPointH)
                {
                    NaveoService.Models.DTO.apiPlanningViaPointH apiViaH = new NaveoService.Models.DTO.apiPlanningViaPointH();
                    apiViaH.iID = viaH.iID;
                    apiViaH.buffer = viaH.Buffer;
                    apiViaH.lat = viaH.Lat;
                    apiViaH.lon = viaH.Lon;
                    apiViaH.pid = viaH.PID;
                    apiViaH.remarks = viaH.Remarks;
                    apiViaH.seqNo = viaH.SeqNo;
                    apiViaH.dtUTC = viaH.dtUTC.Value.ToLocalTime();

                    if (viaH.minDuration != null)
                        apiViaH.minDuration = (int)viaH.minDuration;

                    if (viaH.maxDuration != null)
                        apiViaH.maxDuration = (int)viaH.maxDuration;


                    //apiViaH.dtUTC = viaH.dtUTC;

                    if (viaH.ZoneID != null)
                    {
                        NaveoService.Models.Custom.minifiedValues minifiedZones = new NaveoService.Models.Custom.minifiedValues();
                        minifiedZones.id = Convert.ToInt32(viaH.ZoneID.ToString());
                        minifiedZones.description = viaH.ZoneDesc;
                        apiViaH.zones = minifiedZones;

                    }


                    if (viaH.LocalityVCA != null)
                    {
                        NaveoService.Models.Custom.minifiedValues minifiedVCA = new NaveoService.Models.Custom.minifiedValues();
                        minifiedVCA.id = Convert.ToInt32(viaH.LocalityVCA.ToString());
                        minifiedVCA.description = viaH.LocalityVCADesc;
                        apiViaH.localityVca = minifiedVCA;

                    }


                    if (viaH.lDetail != null)
                    {
                        foreach (PlanningViaPointD viaD in viaH.lDetail)
                        {
                            NaveoService.Models.DTO.apiPlanningViaPointD apiViaD = new NaveoService.Models.DTO.apiPlanningViaPointD();
                            apiViaD.iID = viaD.iID;
                            apiViaD.hid = viaD.HID;
                            apiViaD.capacity = viaD.Capacity;
                            apiViaD.remarks = viaD.Remarks;

                            NaveoService.Models.Custom.minifiedValues minifieduom= new NaveoService.Models.Custom.minifiedValues();
                            minifieduom.id = (int)viaD.UOM.Value;
                            apiViaD.uom = minifieduom;
                            apiViaD.capacity = viaD.Capacity;

                            apiViaH.lDetail.Add(apiViaD);


                        }

                    }

                    apiPlanning.lPlanningViaPointH.Add(apiViaH);
                }

            }
            #endregion

            return apiPlanning;
        }

        public BaseDTO SaveUpdatePlanning(SecurityTokenExtended securityToken, PlanningDTO planningDTO, Boolean bInsert, string servername)
        {
            int userId = CommonService.GetUserID(securityToken.securityToken.UserToken, securityToken.securityToken.sConnStr);
            List<int> lUsers = new List<int>();
            lUsers.Add(userId);

            int uiD = -1;
            List<int> lValidatedUsers = CommonService.lValidateUsers(securityToken.securityToken.UserToken, lUsers, securityToken.securityToken.sConnStr, out uiD);
            int UID = lValidatedUsers[0];

            NaveoOneLib.Models.Plannings.Planning P = new NaveoOneLib.Models.Plannings.Planning();
            var x = planningDTO.planning;
            P.PID = x.pId;
            P.Approval = x.approval;
            P.closureComments = x.closureComments;
            P.Ref = x.reference;
            P.Remarks = x.remarks;
            P.requestCode = x.requestCode;
            P.parentRequestCode = x.parentRequestCode;
            P.sComments = x.sComments;
            P.closureComments = x.closureComments;
            P.type = x.type;

            if (x.urgent == true)
                P.urgent = 1;
            else if (x.urgent == false)
                P.urgent = 0;

            P.OprType = x.oprType;
            #region createUpdate USER
            //if (bInsert)
            //{
            //    P.CreatedDate = DateTime.Now;
            //    P.CreatedBy = UID;
            //    P.UpdatedDate = x.updatedDate;

            //    if(x.updatedUser != null)
            //       P.UpdatedBy = x.updatedUser.id;

            //}
            //else
            //{
            //    P.UpdatedDate = DateTime.Now;
            //    P.UpdatedBy = UID;
            //    P.CreatedDate = x.createdDate;

            //    if (x.createdUser != null)
            //        P.CreatedBy = x.createdUser.id;

            //}

            #endregion

            #region Matrix
            if (bInsert)
            {
                List<Matrix> lm = new MatrixService().GetMatrixByUserID(UID, securityToken.securityToken.sConnStr);
                foreach (Matrix mm in lm)
                {
                    Matrix m = new Matrix();
                    m.GMDesc = mm.GMDesc;
                    m.GMID = mm.GMID;
                    m.iID = x.pId;//mm.iID;
                    m.MID = 0;
                    m.oprType = DataRowState.Added;
                    P.lMatrix.Add(m);
                }
            }
            #endregion

            #region PlanningLkup

            if (x.lPlanningLkup != null)
            {
                foreach (apiPlanningLkup plk in x.lPlanningLkup)
                {
                    PlanningLkup dllPlk = new PlanningLkup();
                    dllPlk.PID = plk.pId;
                    dllPlk.iID = plk.iID;
                    dllPlk.LookupValueID = plk.lookupValues.id;
                    dllPlk.LookupValueDesc = plk.lookupValues.description;
                    dllPlk.oprType = plk.oprType;

                    P.lPlanningLkup.Add(dllPlk);
                }

            }
            #endregion

            #region planningResource

            if (x.lPlanningResource != null)
            {
                foreach (apiPlanningResource aR in x.lPlanningResource)
                {
                    PlanningResource dllResource = new PlanningResource();
                    dllResource.PID = aR.pId;
                    dllResource.iID = aR.iID;
                    dllResource.ResourceType = aR.ResourceType;
                    dllResource.DriverID = aR.driver.id;
                    dllResource.DriverName = aR.driver.description;
                    dllResource.CustomType = aR.customType;
                    dllResource.MileageAllowance = aR.mileageAllowance;
                    dllResource.TravelGrant = aR.travelGrant;
                    dllResource.Remarks = aR.remarks;

                    dllResource.oprType = aR.oprType;

                    P.lPlanningResource.Add(dllResource);
                }
            }
            #endregion

            #region planningViaHeaderDetails
            if (x.lPlanningViaPointH != null)
            {
                foreach (apiPlanningViaPointH viaH in x.lPlanningViaPointH)
                {
                    PlanningViaPointH planningViaPointH = new PlanningViaPointH();
                    planningViaPointH.PID = viaH.pid;
                    planningViaPointH.Buffer = viaH.buffer;
                    planningViaPointH.dtUTC = viaH.dtUTC;
                    planningViaPointH.iID = viaH.iID;
                    planningViaPointH.Lat = viaH.lat;
                    planningViaPointH.Lat = viaH.lat;
                    planningViaPointH.Lon = viaH.lon;
                    //planningViaPointH.ClientID = viaH.clientId;
                    planningViaPointH.minDuration = viaH.minDuration;
                    planningViaPointH.maxDuration = viaH.maxDuration;

                    if (viaH.zones != null)
                    {
                        planningViaPointH.ZoneID = viaH.zones.id;
                        planningViaPointH.ZoneDesc = viaH.zones.description;
                    }

                    if (viaH.localityVca != null)
                    {
                        planningViaPointH.LocalityVCA = viaH.localityVca.id;
                        planningViaPointH.LocalityVCADesc = viaH.localityVca.description;
                    }

                    planningViaPointH.Remarks = viaH.remarks;
                    planningViaPointH.SeqNo = viaH.seqNo;
                    planningViaPointH.oprType = viaH.oprType;

                    if (viaH.lDetail != null)
                    {
                        foreach (apiPlanningViaPointD apiviaD in viaH.lDetail)
                        {
                            PlanningViaPointD ViaD = new PlanningViaPointD();
                            ViaD.iID = apiviaD.iID;
                            ViaD.HID = apiviaD.hid;
                            ViaD.Remarks = apiviaD.remarks;
                            ViaD.UOM = apiviaD.uom.id;
                            ViaD.uomDesc = apiviaD.uom.description;
                            ViaD.oprType = apiviaD.oprType;
                            ViaD.Capacity = apiviaD.capacity;

                            planningViaPointH.lDetail.Add(ViaD);
                        }
                    }
                    P.lPlanningViaPointH.Add(planningViaPointH);
                }
            }
            #endregion-

            List<DateTime> lDates = new List<DateTime>();
            BaseModel saveObj = new NaveoOneLib.Services.Plannings.PlanningService().SavePlanning(P, lDates, UID, bInsert, securityToken.securityToken.sConnStr);

            if (saveObj.data == true)
            {
                if (P.type == "150")
                {
                    P.PID = saveObj.total;
                    string SentMail = SentMailForCreatePlanning(P, securityToken.securityToken.UserToken, servername, securityToken.securityToken.sConnStr);
                }

            }

            return saveObj.ToBaseDTO(securityToken.securityToken);
        }

        public string UploadPlanningRequest(SecurityTokenExtended securityToken, DataTable dt)
        {
            List<DateTime> lDates = new List<DateTime>();
            int UID = CommonService.GetUserID(securityToken.securityToken.UserToken, securityToken.securityToken.sConnStr);
            string errorMessage = string.Empty;
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(UID, securityToken.securityToken.sConnStr);
            BaseModel saveData = new BaseModel();
            DataView view = new DataView(dt);
            DataTable distinctValues = view.ToTable(true, "PID");
            var query = distinctValues.AsEnumerable().Where(r => r.Field<int>("PID") != 0);

            DataTable dtscheduledDetails = new DataTable();
            dtscheduledDetails.Columns.Add("OrderNo");
            dtscheduledDetails.Columns.Add("Resource");
            dtscheduledDetails.Columns.Add("Asset");
            int count = 0;
            int planningSaveCount = 0;
            int planningErroCount = 0;
            foreach (var q in query)
            {
                NaveoOneLib.Models.Plannings.Planning P = new NaveoOneLib.Models.Plannings.Planning();

                #region Matrix 
                foreach (Matrix mm in lm)
                {
                    Matrix m = new Matrix();
                    m.GMDesc = mm.GMDesc;
                    m.GMID = mm.GMID;
                    m.iID = 0;
                    m.MID = 0;
                    m.oprType = DataRowState.Added;

                    P.lMatrix.Add(m);
                }
                #endregion

                List<NaveoOneLib.Models.Plannings.PlanningViaPointH> lPlanningViaPoints = new List<NaveoOneLib.Models.Plannings.PlanningViaPointH>();
                List<NaveoOneLib.Models.Plannings.PlanningLkup> lPlanningLkup = new List<NaveoOneLib.Models.Plannings.PlanningLkup>();
                List<NaveoOneLib.Models.Plannings.PlanningViaPointD> lPlanningViaPointD = new List<NaveoOneLib.Models.Plannings.PlanningViaPointD>();

                foreach (DataRow dr in dt.Rows)
                {
                    planningErroCount++;
                    NaveoOneLib.Models.Plannings.PlanningViaPointH PH = new NaveoOneLib.Models.Plannings.PlanningViaPointH();
                    NaveoOneLib.Models.Plannings.PlanningLkup Lk = new NaveoOneLib.Models.Plannings.PlanningLkup();
                    NaveoOneLib.Models.Plannings.PlanningViaPointD PD = new NaveoOneLib.Models.Plannings.PlanningViaPointD();

                    int a = (int)q["PID"];
                    int b = (int)dr["PID"];
                    string sd = dr["StartDate"].ToString();

                    if (a == b && sd != "01/01/0001 00:00:00")
                    {
                        P.Approval = 0;
                        P.Remarks = dr["Remarks"].ToString();
                        P.urgent = (int)dr["Urgent"];
                        P.CreatedBy = UID;
                        P.CreatedDate = DateTime.Now;
                        P.OprType = DataRowState.Added;
                        P.type = "1";

                        string r = dr["Resource"].ToString();
                        int driverId = 0;
                        DataTable dtDriver = new NaveoOneLib.Services.Drivers.DriverService().GetDriverByName(r, securityToken.securityToken.sConnStr);

                        if (r != string.Empty && dtDriver == null)
                        {
                            errorMessage = "Error in creating planning with  PID :" + planningErroCount + "as " + r.ToUpper() + " is not a valid RESOURCE";
                            return errorMessage;
                        }

                        if (r != string.Empty)
                        {
                            driverId = (from p in dtDriver.AsEnumerable()
                                        where p.Field<string>("sDriverName") == dr["Resource"].ToString()
                                        select p.Field<int>("DriverID")).FirstOrDefault();
                        }

                        string assetFromFile = dr["Vehicle"].ToString();
                        int assetId = 0;

                        NaveoOneLib.Models.Assets.Asset asset = new NaveoOneLib.Models.Assets.Asset();


                        if (assetFromFile != string.Empty)
                            asset = new NaveoOneLib.Services.Assets.AssetService().GetAssetByName(assetFromFile, securityToken.securityToken.sConnStr);

                        if (assetFromFile != string.Empty && asset == null)
                        {
                            errorMessage = "Error in creating planning with  PID :" + planningErroCount + "as " + assetFromFile.ToUpper() + " is not a valid RESOURCE";
                            return errorMessage;
                        }

                        if (assetFromFile != string.Empty)
                        {
                            assetId = asset.AssetID;
                        }

                        count++;
                        DataRow toInsert = dtscheduledDetails.NewRow();
                        toInsert["OrderNo"] = count;
                        toInsert["Resource"] = Convert.ToInt32(driverId);
                        toInsert["Asset"] = Convert.ToInt32(assetId);
                        dtscheduledDetails.Rows.Add(toInsert);

                    }


                    if (a == b)
                    {
                        string viaPointName = dr["ViaPoints"].ToString();
                        #region PlanningViaPoints
                        string viaPointType = string.Empty;

                        DataTable dtZoneHeader = new NaveoOneLib.Services.Zones.ZoneService().GetZoneByZoneName(viaPointName, securityToken.securityToken.sConnStr);
                        int ooooo = dtZoneHeader.Rows.Count;
                        if (dtZoneHeader.Rows.Count != 0)
                        {
                            viaPointType = "zone";
                        }


                        var VcaHeader = new NaveoOneLib.Services.Zones.VCAHeaderService().GetVCAByName(viaPointName, securityToken.securityToken.sConnStr);
                        if (VcaHeader != null)
                            viaPointType = "vca";


                        string coord = dr["ViaPoints"].ToString();
                        if (dtZoneHeader.Rows.Count == 0 && VcaHeader == null && coord.Contains(','))
                            viaPointType = "coordinates";

                        DateTime date = new DateTime();
                        DateTime time = new DateTime();


                        if (viaPointType != string.Empty)
                        {
                            coord = dr["ViaPoints"].ToString();
                            if (coord.Contains(','))
                            {
                                string[] splitCoord = coord.Split(',');
                                string lat = splitCoord[0];
                                string lon = splitCoord[1];
                                PH.Lat = Convert.ToDouble(lat);
                                PH.Lon = Convert.ToDouble(lon);
                            }

                        }
                        else
                        {
                            //planningSaveCount = planningSaveCount + 1;
                            errorMessage = "Error in creating planning with  PID :" + planningErroCount + " as " + viaPointName.ToUpper() + " is not a valid VCA or ZONE!!!";
                            return errorMessage;
                        }

                        #region VCA 
                        if (viaPointType == "vca")
                        {

                            if (VcaHeader == null)
                                errorMessage = viaPointName + "is not a valid vca name";
                            else
                            {

                                var vcaId = (from p in VcaHeader.AsEnumerable()
                                             where p.Field<string>("Name") == viaPointName
                                             select p.Field<int>("VCAID")).FirstOrDefault();
                                PH.LocalityVCA = vcaId;

                            }
                        }
                        #endregion

                        #region Zone
                        if (viaPointType == "zone")
                        {
                            if (dtZoneHeader == null)
                                errorMessage = viaPointName + "is not a valid zone name";
                            else
                            {

                                var zoneId = (from p in dtZoneHeader.AsEnumerable()
                                              where p.Field<string>("Description") == viaPointName
                                              select p.Field<int>("ZoneID")).FirstOrDefault();

                                //viaPointId = zoneId;
                                PH.ZoneID = zoneId;
                            }
                        }
                        #endregion




                        date = (DateTime)dr["StartDate"];
                        time = (DateTime)dr["StartTime"];
                        var output = date.Date + time.TimeOfDay;

                        PH.Buffer = (int)dr["Buffer"];
                        PH.dtUTC = output;
                        PH.oprType = DataRowState.Added;

                        lPlanningViaPoints.Add(PH);

                        P.lPlanningViaPointH = lPlanningViaPoints;

                        #endregion

                        #region Department
                        DataTable dtLookups = new NaveoOneLib.Services.Lookups.LookUpValuesService().GetLookUpValuesByLookUpTypeName("Department", securityToken.securityToken.sConnStr);
                        foreach (DataRow drLKP in dtLookups.Rows)
                        {
                            if (dr["Department"].ToString() == drLKP["Name"].ToString())
                            {
                                Lk.LookupType = "Department";
                                Lk.LookupValueID = (int)drLKP["VID"];
                                Lk.oprType = DataRowState.Added;
                                lPlanningLkup.Add(Lk);
                                P.lPlanningLkup = lPlanningLkup;
                            }

                        }

                        #endregion
                    }
                }

                saveData = new NaveoOneLib.Services.Plannings.PlanningService().SavePlanning(P, lDates, UID, true, securityToken.securityToken.sConnStr);
                planningSaveCount++;
            }
            errorMessage = planningSaveCount + " planning request";

            DateTime dateFrom = DateTime.Now.AddMinutes(-2);
            DateTime dateTo = DateTime.Now;
            string dtFrom = dateFrom.ToString("MM/dd/yyyy hh:mm tt");
            string dtTo = dateTo.ToString("MM/dd/yyyy hh:mm tt");
            DataTable dtrecentlyCreatedPlanning = new NaveoOneLib.Services.Plannings.PlanningService().GetPlanning(securityToken.securityToken.sConnStr, dateFrom.ToUniversalTime(), dateTo.ToUniversalTime());
            DataTable dtScheduled = new DataTable();
            dtScheduled.Columns.Add("PID");
            dtScheduled.Columns.Add("Resource");
            dtScheduled.Columns.Add("Asset");

            int c = 0;
            int d = 0;
            foreach (DataRow drRecentlyCreatePlanning in dtrecentlyCreatedPlanning.Rows)
            {
                c++;

                foreach (DataRow dr2 in dtscheduledDetails.Rows)
                {
                    d++;

                    if (c == d)
                    {
                        DataRow toInsert = dtScheduled.NewRow();
                        toInsert["PID"] = drRecentlyCreatePlanning["PID"].ToString();
                        toInsert["Resource"] = dr2["Resource"].ToString();
                        toInsert["Asset"] = dr2["Asset"].ToString();
                        dtScheduled.Rows.Add(toInsert);
                    }
                }
                d = 0;
            }

            int shceduleMessage = UploadPlanningSchedule(securityToken, dtScheduled);
            string message = string.Empty;
            if (shceduleMessage != 0)
                message = errorMessage + " and " + shceduleMessage + " scheduled request has been created";
            else
                message = errorMessage + " has been created";

            return message;
        }

        public class ScheduledList
        {
            public List<int> PID { get; set; } = new List<int>();
            public int resource { get; set; }
            public int asset { get; set; }
        }

        public int UploadPlanningSchedule(SecurityTokenExtended securityToken, DataTable dt)
        {
            List<ScheduledList> schL = new List<ScheduledList>();
            DataView view = new DataView(dt);
            DataTable distinctValues = view.ToTable(true, "Resource", "Asset");

            foreach (DataRow drr in distinctValues.Rows)
            {
                ScheduledList s = new ScheduledList();
                s.PID = new List<int>();
                foreach (DataRow distinct in dt.Rows)
                {
                    if (distinct["Resource"].ToString() == drr["Resource"].ToString() && distinct["Asset"].ToString() == drr["Asset"].ToString())
                    {
                        int o = Convert.ToInt32(distinct["PID"]);
                        s.PID.Add(o);
                        s.resource = Convert.ToInt32(distinct["Resource"]);
                        s.asset = Convert.ToInt32(distinct["Asset"]);
                        schL.Add(s);

                    }
                }
            }

            List<ScheduledList> formatScheduleList = schL.Distinct().ToList();
            formatScheduleList.RemoveAll(item => item.asset == 0);
            formatScheduleList.RemoveAll(item => item.resource == 0);
            formatScheduleList.RemoveAll(item => item.asset == 0 && item.resource == 0);

            int scheduleCount = 0;
            foreach (var ss in formatScheduleList)
            {
                Scheduled sch = new Scheduled();
                sch.Remarks = "File Upload";
                sch.OprType = DataRowState.Added;

                List<ScheduledPlan> listScheduledPlan = new List<ScheduledPlan>();
                List<ScheduledAsset> schAsset = new List<ScheduledAsset>();
                foreach (var pid in ss.PID)
                {
                    ScheduledPlan sP = new ScheduledPlan();
                    sP.PID = pid;
                    sP.OprType = DataRowState.Added;
                    listScheduledPlan.Add(sP);
                    sch.lSchPlan = listScheduledPlan;
                }

                ScheduledAsset sA = new ScheduledAsset();
                sA.AssetID = ss.asset;
                sA.DriverID = ss.resource;
                sA.OprType = DataRowState.Added;
                schAsset.Add(sA);
                sch.lSchAsset = schAsset;

                Boolean ScheduledSaveUpdate = new ScheduleService().SaveUpdateScheduled(securityToken, sch, true);
                scheduleCount++;
            }

            int scheduleCountMessage = scheduleCount;
            return scheduleCountMessage;
        }

        

        public BaseModel GetViaPoints(Guid sUserToken, int Pid, String sConnStr)
        {
            List<viaPoints> lViaPoints = new List<viaPoints>();
            DataTable dtViaPoints = new NaveoOneLib.Services.Plannings.PlanningService().GetPlanningViaPoints(Pid, sConnStr);
            foreach (DataRow dr in dtViaPoints.Rows)
            {
                viaPoints VP = new viaPoints();
                VP.PID = (int)dr["PID"];
                VP.HID = (int)dr["HID"];
                VP.SeqNo = (int)dr["SeqNo"];
                VP.ZoneID = (int)dr["ZoneID"];
                VP.zoneName = dr["ZoneName"].ToString();

                BaseModel getZV = new ZoneService().getZoneVisitedDetails(sUserToken, sConnStr, (int)dr["ZoneID"]);
                VP.lZoneDetail = getZV.data;
                lViaPoints.Add(VP);
            }

            BaseModel BM = new BaseModel();
            BM.data = lViaPoints;
            return BM;
        }


        public class scheduleviaPointDetails
        {
            public int viapointid { get; set; }
            public string viapointName { get; set; }
            public DateTime viapointTime { get; set; }
            public List<string> pos { get; set; } = new List<string>();

            public string type { get; set; }

            public int coordOrder { get; set; }

            public int seqNo { get; set; }
            
        }
        public class ScheduledViaPointS
        {
            public int PID { get; set; }
            public string requestcode { get; set; }
            public List<scheduleviaPointDetails> lscheduleviaPointDetails { get; set; } = new List<scheduleviaPointDetails>();


        }

        public BaseModel GetScheduledViaPoints(Guid sUserToken,List<int> Pid, String sConnStr)
        {
            List<ScheduledViaPointS> lViaPoints = new List<ScheduledViaPointS>();
            DataTable dtViaPoints = new NaveoOneLib.Services.Plannings.PlanningService().GetScheduledPlannedViaPoints(Pid, sConnStr);

           DataTable dtDistinct =  dtViaPoints.DefaultView.ToTable(true, "pid");

            int pid = 0;
            try
            {

                foreach (DataRow drDistinct in dtDistinct.Rows)
                {


                    pid = Convert.ToInt32(drDistinct["pid"]);
                    var result = dtViaPoints
                          .AsEnumerable()
                           .Where(myRow => myRow.Field<int>("pid") == pid);

                    ScheduledViaPointS scp = new ScheduledViaPointS();
                    scp.PID = pid;




                    foreach (DataRow dr in result)
                    {
                        scp.requestcode = dr["requestcode"].ToString();

                        scheduleviaPointDetails svpds = new scheduleviaPointDetails();

                        svpds.viapointid = Convert.ToInt32(dr["viapointid"].ToString());
                        svpds.viapointTime = Convert.ToDateTime(dr["viapointTime"].ToString());
                        string coordCond = dr["coords"].ToString() == String.Empty ? "" : dr["coords"].ToString();
                        string vcaCond = dr["vca"].ToString() == String.Empty ? coordCond : dr["vca"].ToString();
                        svpds.viapointName = dr["zonename"].ToString() == String.Empty ? vcaCond : dr["zonename"].ToString();

                        string coordType = dr["coords"].ToString() == String.Empty ? "NULL" : "Coordinates";
                        string vcaType = dr["vca"].ToString() == String.Empty ? coordType : "VCA";
                        svpds.type = dr["zonename"].ToString() == String.Empty ? vcaType : "Zone";


                        svpds.seqNo = Convert.ToInt32(dr["seqno"].ToString());

                        int zoneid = 0;
                        int vcaid = 0;

                        if (dr["zoneid"].ToString() == "")
                            zoneid = 0;
                        else
                            zoneid = Convert.ToInt32(dr["zoneid"].ToString());


                        if (dr["vcaid"].ToString() == "")
                            vcaid = 0;
                        else
                            vcaid = Convert.ToInt32(dr["vcaid"].ToString());


                        if (zoneid != 0)
                        {

                            BaseModel getZV = new ZoneService().getZoneVisitedDetails(sUserToken, sConnStr, (int)dr["zoneId"]);
                            List<NaveoOneLib.Models.Zones.ZoneDetail> lzoneDetail = getZV.data;
                            foreach (var lzd in lzoneDetail)
                            {

                                string pos = lzd.latitude + "," + lzd.longitude;

                                svpds.pos.Add(pos);
                            }

                            scp.lscheduleviaPointDetails.Add(svpds);


                        }
                        else if (vcaid != 0)
                        {
                            BaseModel getZV = new ZoneService().getZoneVisitedDetails(sUserToken, sConnStr, (int)dr["vcaid"]);
                            List<NaveoOneLib.Models.Zones.ZoneDetail> lzoneDetail = getZV.data;
                            foreach (var lzd in lzoneDetail)
                            {

                                string pos = lzd.latitude + "," + lzd.longitude;

                                svpds.pos.Add(pos);
                            }

                            scp.lscheduleviaPointDetails.Add(svpds);
                        }


                        if (vcaid == 0 && zoneid == 0)
                        {


                            string pos = dr["coords"].ToString();

                            svpds.pos.Add(pos);


                            scp.lscheduleviaPointDetails.Add(svpds);
                        }


                    }


                    lViaPoints.Add(scp);
                }

            }
            catch (Exception ex)
            {

                int aaa = pid;

            }
            BaseModel BM = new BaseModel();
            BM.data = lViaPoints;
            BM.total = lViaPoints.Count;
            return BM;
        }



        public TripDetails GetTripDetails(SecurityTokenExtended securityToken, DateTime dtFrom, DateTime dtTo, int assetId)
        {
            List<int> assetIds = new List<int>();
            assetIds.Add(assetId);
            NaveoService.Models.TripHeader tripHeader = new TripService().GetTripHeaders(securityToken.securityToken.UserToken, assetIds, dtFrom, dtTo, false, 0.0, new List<string>(), securityToken.securityToken.sConnStr, null, null,null,null, true, null, false);
            List<int> TripHeaders = new List<int>();
            foreach (DataRow dr in tripHeader.dtTrips.Rows)
            {
                int ilD = (int)dr["iID"];
                TripHeaders.Add(ilD);
            }

            TripDetails tripDetails = new TripService().GetTripDetails(TripHeaders, securityToken);
            return tripDetails;
        }

        public class viaPoints
        {
            public int PID { get; set; }
            public int HID { get; set; }
            public int SeqNo { get; set; }
            public int ZoneID { get; set; }
            public string zoneName { get; set; }
            public List<NaveoOneLib.Models.Zones.ZoneDetail> lZoneDetail = new List<NaveoOneLib.Models.Zones.ZoneDetail>();
        }

        #region CWA
        public dtData ViewTransportRequest(Guid sUserToken, DateTime dtFrom, DateTime dtTo, int? PID, int approval, string sConnStr, int? Page, int? LimitPerPage, List<String> sortColumns, Boolean sortOrderAsc)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            int ApprovalId = new NaveoOneLib.Services.Users.UserService().GetUserApprovalProcessIdByUserID(UID, sConnStr);
            int ApprovalValue = new NaveoOneLib.Services.Users.UserService().GetApprovalProcessValue(ApprovalId, sConnStr);
            int userApproval = ApprovalValue - 10;
            //int ApprovalIDByValue = new NaveoOneLib.Services.Users.UserService().GetApprovalProcessIdByValue(ApprovalValue, sConnStr);
            dtData plnData = GetPlanning(sUserToken, dtFrom, dtTo, PID, approval, sConnStr, Page, LimitPerPage, sortColumns, sortOrderAsc);
            DataView view = new DataView(plnData.Data);
            //equal to 150
            view.RowFilter = "type ='150' and ApprovalID <> ''";
            plnData.Data = view.ToTable();

            List<String> sList = new List<String>();
            foreach (DataRow r in plnData.Data.Rows)
            {
                sList.Add(r["ApprovalID"].ToString());
            }

            if (sList.Count == 0)
                return plnData;

            DataTable dt = new NaveoOneLib.Services.Users.UserService().GetFilterUserApproval(sList, userApproval, sConnStr);

            List<string> iList = new List<string>();
            foreach (DataRow dr in dt.Rows)
            {
                iList.Add(dr["iID"].ToString());
            }

            var results = (from row in plnData.Data.AsEnumerable()
                           where iList.Contains(row.Field<string>("ApprovalID"))
                           select row).CopyToDataTable();

            plnData.Data = results;
            plnData.Rowcnt = results.Rows.Count;
            return plnData;
        }

        public BaseModel ApprovalProcess(Guid sUserToken, int PID, string statusApproval, string sConnStr)
        {
            Planning planningOld = new Planning();
            Planning planningNew = new Planning();
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            NaveoOneLib.Models.Plannings.Planning planning = new NaveoOneLib.Services.Plannings.PlanningService().GetPlanningByPID(PID, sConnStr);
            List<PlanningResource> planningResource = new NaveoOneLib.Services.Plannings.PlanningResourceService().GetPlanningResourceById(PID.ToString(), sConnStr);
            string sender = string.Empty;
            string status = string.Empty;
            if (statusApproval == "Approve")
            {
                status = "Approved";
                sender = "Transport Agent";
            }

            if (statusApproval == "Reject")
            {
                status = "Rejected";
                sender = "Requestor";
            }

            string SentMail = SentMailForApprovalProcess(planning, status, sUserToken, string.Empty, sConnStr);

            #region Audit & Update Planning
            int ApprovalID = new NaveoOneLib.Services.Users.UserService().GetUserApprovalProcessIdByUserID(UID, sConnStr);

            planningNew.PID = planning.PID;
            planningNew.ApprovalID = ApprovalID;

            Boolean updatePlanning = new NaveoOneLib.Services.Plannings.PlanningService().UpdatePlanningTableForApproval(planningNew, UID, sConnStr);

            planningOld.ApprovalID = planning.ApprovalID;
            Boolean Audit = new NaveoOneLib.Services.Audits.ApprovalAuditService().SaveApprovalAudit(planningOld, planningNew, "PlanningRequest", UID, true, sConnStr);
            #endregion

            BaseModel BM = new BaseModel();
            BM.data = "Planning with PID " + planning.PID + " has been " + status + " , A mail has been sent to the concerned " + sender;
            return BM;
        }

        public DataTable PlanningApprovalDetails(Planning P, Guid sUserToken, String sConnStr)
        {
            string From = string.Empty;
            string To = string.Empty;
            string coordFrom = string.Empty;
            string coordTo = string.Empty;

            DataTable dtDetails = new DataTable();
            dtDetails.Columns.Add("ApproverName");
            dtDetails.Columns.Add("ApproverEmail");
            dtDetails.Columns.Add("ApplicantName");
            dtDetails.Columns.Add("ApplicantEmail");
            dtDetails.Columns.Add("From");
            dtDetails.Columns.Add("To");

            int approverId = P.lPlanningResource.Where(p => p.Remarks == "Approver").Select(p => p.DriverID).FirstOrDefault();
            int applicantId = P.lPlanningResource.Where(p => p.Remarks == "APPLICANT").Select(p => p.DriverID).FirstOrDefault();

            DataTable dt = new NaveoService.Services.DropDownService().getAllResources(sUserToken, sConnStr);

            var rowColl = dt.AsEnumerable();
            string approverEmail = (from r in rowColl
                                    where r.Field<int>("ID") == approverId
                                    select r.Field<string>("eMailAddress")).First<string>();
            string approverName = (from r in rowColl
                                   where r.Field<int>("ID") == approverId
                                   select r.Field<string>("description")).First<string>();

            string applicantEmail = (from r in rowColl
                                     where r.Field<int>("ID") == applicantId
                                     select r.Field<string>("eMailAddress")).First<string>();
            string applicantName = (from r in rowColl
                                    where r.Field<int>("ID") == applicantId
                                    select r.Field<string>("description")).First<string>();

            string FromZone = (from r in P.lPlanningViaPointH
                               where r.SeqNo == 1
                               select r.ZoneDesc).First<string>();
            string ToZone = (from r in P.lPlanningViaPointH
                             where r.SeqNo == 999
                             select r.ZoneDesc).First<string>();

            string FromVCA = (from r in P.lPlanningViaPointH
                              where r.SeqNo == 1
                              select r.LocalityVCADesc).First<string>();
            string ToVCA = (from r in P.lPlanningViaPointH
                            where r.SeqNo == 999
                            select r.LocalityVCADesc).First<string>();

            string FromLat = (from r in P.lPlanningViaPointH
                              where r.SeqNo == 1
                              select r.Lat.ToString()).First<string>();
            string FromLon = (from r in P.lPlanningViaPointH
                              where r.SeqNo == 1
                              select r.Lon.ToString()).First<string>();
            string ToLat = (from r in P.lPlanningViaPointH
                            where r.SeqNo == 999
                            select r.Lat.ToString()).First<string>();
            string ToLon = (from r in P.lPlanningViaPointH
                            where r.SeqNo == 999
                            select r.Lon.ToString()).First<string>();


            if (FromLat != string.Empty && FromLon != string.Empty)
            {
                coordFrom = FromLat + "," + FromLon;
            }

            if (ToLat != string.Empty && ToLon != string.Empty)
            {
                coordTo = ToLat + "," + ToLon;
            }

            From = FromZone != string.Empty ? FromZone : FromVCA != string.Empty ? FromVCA : coordFrom != string.Empty ? coordFrom : string.Empty;
            To = ToZone != string.Empty ? ToZone : ToVCA != string.Empty ? ToVCA : coordTo != string.Empty ? coordTo : string.Empty;


            DataRow dr = dtDetails.NewRow();
            dr["ApproverName"] = approverName;
            dr["ApproverEmail"] = approverEmail;
            dr["ApplicantName"] = applicantName;
            dr["ApplicantEmail"] = applicantEmail;
            dr["From"] = From;
            dr["To"] = To;

            dtDetails.Rows.Add(dr);

            return dtDetails;
        }
        public string SentMailForCreatePlanning(Planning P, Guid sUserToken, string servername, String sConnStr)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            DataTable dt = PlanningApprovalDetails(P, sUserToken, sConnStr);
            string approverName = dt.Rows[0][0].ToString();
            string approverEmail = dt.Rows[0][1].ToString();
            string applicantName = dt.Rows[0][2].ToString();
            string applicantEmail = dt.Rows[0][3].ToString();
            string From = dt.Rows[0][4].ToString();
            string To = dt.Rows[0][5].ToString();

            User.NaveoDatabases myEnum;
            //Getting only the enum part
            servername = servername.Replace(".naveo.mu", String.Empty);
            servername = servername.Replace("https://", String.Empty);
            servername = servername.Replace("http://", String.Empty);
            if (Enum.TryParse(servername, true, out myEnum))
                servername = "https://" + servername + ".naveo.mu";
            else
                servername = "http://" + servername;



            String sBody = "Dear " + approverName.ToUpper() + " \r\n\r\n";
            sBody += "You have a new Transport Request to review and Action.\r\n\r\n";
            sBody += "Request Details: \r\n\r\n";
            sBody += "Requested By: " + applicantName.ToUpper() + "\r\n\r\n";
            sBody += "Request Date: " + P.lPlanningViaPointH[0].dtUTC.Value.ToLocalTime() + "\r\n\r\n";
            sBody += "Departure: " + From + "\r\n\r\n";
            sBody += "Arrival: " + To + "\r\n\r\n";

            sBody += "Click here to Approve or Reject the request by clicking on this url: " + servername + "/Planning/Planning/TransportRequest/" + P.PID + "/2";

            string m = new NaveoOneLib.Services.Plannings.PlanningService().sendPLanningMail(UID, approverEmail, "PlnRequest", " New Transport Request", sBody, sConnStr);

            return m;
        }
        public string SentMailForApprovalProcess(Planning P, string status, Guid sUserToken, string servername, String sConnStr)
        {
            string reason = string.Empty;
            String sBody = String.Empty;
            string m = string.Empty;

            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);

            DataTable dt = PlanningApprovalDetails(P, sUserToken, sConnStr);
            string approverName = dt.Rows[0][0].ToString();
            string approverEmail = dt.Rows[0][1].ToString();
            string applicantName = dt.Rows[0][2].ToString();
            string applicantEmail = dt.Rows[0][3].ToString();
            string From = dt.Rows[0][4].ToString();
            string To = dt.Rows[0][5].ToString();

            #region APPROVE
            if (status == "Approved")
            {
                DataTable dtTransportAgent = new NaveoService.Services.DropDownService().getResources(sUserToken, Driver.ResourceType.TransportAgent, sConnStr);


                foreach (DataRow dr in dtTransportAgent.Rows)
                {

                    string Body = string.Empty;
                    string transportAgentName = string.Empty;
                    string transportAgentEmail = string.Empty;
                    transportAgentName = dr["description"].ToString();
                    transportAgentEmail = dr["eMailAddress"].ToString();

                    Body = "Dear " + transportAgentName.ToUpper() + " \r\n\r\n";
                    Body += "Please note that " + approverName.ToUpper() + " has approved  Transport Request with below details:\r\n\r\n";
                    Body += "Request Date: " + P.lPlanningViaPointH[0].dtUTC.Value.ToLocalTime() + "\r\n\r\n";
                    Body += "Departure: " + From + "\r\n\r\n";
                    Body += "Arrival: " + To + "\r\n\r\n";

                    m = new NaveoOneLib.Services.Plannings.PlanningService().sendPLanningMail(UID, transportAgentEmail, "ApprovalProcess", "APPROVE/REJECT Transport Request", Body, sConnStr);


                }


            }
            #endregion

            #region Reject
            if (status == "Reject")
            {

                sBody = "Dear " + applicantName.ToUpper() + " \r\n\r\n";
                sBody += "Please note that " + approverName.ToUpper() + " has rejected Transport Request with below details:\r\n\r\n";
                sBody += "Request Date: " + P.lPlanningViaPointH[0].dtUTC.Value.ToLocalTime() + "\r\n\r\n";
                sBody += "Departure: " + From + "\r\n\r\n";
                sBody += "Arrival: " + To + "\r\n\r\n";
                sBody += "Reason: " + reason + "\r\n\r\n";

                m = new NaveoOneLib.Services.Plannings.PlanningService().sendPLanningMail(UID, applicantEmail, "Approval Process", "APPROVE/REJECT Transport Request", sBody, sConnStr);
            }
            #endregion


            return m;


        }

        #endregion

        #region ExtendTripInfoPlanningDetails
        public dtData GetPlanningDetails(Guid sUserToken, int AssetID, DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            #region TERRA
           // dtFrom = new DateTime(2020, 04, 27, 04, 37, 00);
            dtData dtTPlanningRequest = new ScheduleService().GetTerraPlanningRequest(sUserToken, dtFrom, dtTo, sConnStr, null, null, new List<String>(), false);
            DataTable dt = dtTPlanningRequest.Data;
            dt.Columns.Add("SeqNo");

            int seqNo = 0;
            DataView view = new DataView(dt);
            DataTable distinctValues = view.ToTable(true, "requestCode");

            var result = dt.AsEnumerable().Where(myRow => myRow.Field<int>("AssetID") == AssetID).CopyToDataTable();
            foreach (DataRow drr in distinctValues.Rows)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["requestCode"].ToString() == drr["requestCode"].ToString())
                    {
                        seqNo++;
                        dr["SeqNo"] = seqNo;
                    }

                }
                seqNo = 0;
            }
            #endregion

            #region MOLG

            #endregion

            dtData dtR = new dtData();

            

            dtR.Data = result;
            dtR.Rowcnt = result.Rows.Count;
            return dtR;
        }

        public dtData GetMokaPlanningDetails(Guid sUserToken, int AssetID, DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
   
            //dtData dtTPlanningRequest = new ScheduleService().GetTerraPlanningRequest(sUserToken, dtFrom, dtTo, sConnStr, null, null, new List<String>(), false);

            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);
            List<int> lAssetIds = new List<int>();
            lAssetIds.Add(AssetID);
            DataTable dtPlanningR = new NaveoOneLib.Services.Plannings.PlanningService().GetPlanningActual(dtFrom, dtTo, lAssetIds, null, sConnStr);

            DataTable dtFinal = new DataTable();
            dtFinal.Columns.Add("SeqNo", typeof(int));
            dtFinal.Columns.Add("pId", typeof(int));
            dtFinal.Columns.Add("scheduleId", typeof(int));
            dtFinal.Columns.Add("requestDate");
            dtFinal.Columns.Add("dateCreated");
            dtFinal.Columns.Add("startTime");
            dtFinal.Columns.Add("endTime");
            dtFinal.Columns.Add("dtStartTime");
            dtFinal.Columns.Add("dtEndTime");
            dtFinal.Columns.Add("departureId", typeof(int));
            dtFinal.Columns.Add("departure");
            dtFinal.Columns.Add("arrival");
            dtFinal.Columns.Add("arrivalId", typeof(int));
            dtFinal.Columns.Add("fieldCode");
            dtFinal.Columns.Add("noOfHouseholds");
            dtFinal.Columns.Add("zoneDistanceCovered");

            dtFinal.Columns.Add("asset");
            dtFinal.Columns.Add("driver");
            dtFinal.Columns.Add("assetID", typeof(int));
            dtFinal.Columns.Add("driverID", typeof(int));
            dtFinal.Columns.Add("status");
            dtFinal.Columns.Add("requestCode");
            dtFinal.Columns.Add("remarks");

            int count =0;
            foreach (DataRow drPlanning in dtPlanningR.Rows)
            {
                count++;
              
                int seqNo = Convert.ToInt32(drPlanning["SeqNo"]);
                if (seqNo == 999)
                    seqNo = count;

                int PID = Convert.ToInt32(drPlanning["PID"]);
                int HID = drPlanning["ScheduledID"].ToString() == string.Empty ? 0 : Convert.ToInt32(drPlanning["ScheduledID"]);
                //DataTable dtPlanningLKUP = new NaveoOneLib.Services.Plannings.ScheduledService().GetPlanningLookUp(PID, sConnStr);
                //DataTable dtScheduledResource = new NaveoOneLib.Services.Plannings.ScheduledAssetService().getScheduledResourcesByHID(HID, sConnStr);

                DataRow row = dtFinal.NewRow();
                double buffer = Convert.ToDouble(drPlanning["Buffer"]);
                DateTime dtStartTime = new DateTime();
                DateTime dtEndTime = new DateTime();
                dtStartTime = Convert.ToDateTime(drPlanning["dtUTC"]);
                dtStartTime = dtStartTime.AddMinutes(-buffer);
                dtEndTime = Convert.ToDateTime(drPlanning["dtUTC"]);
                dtEndTime = dtEndTime.AddMinutes(buffer);

                row["SeqNo"] = seqNo;
                row["startTime"] = dtStartTime.ToShortTimeString();
                row["endTime"] = dtEndTime.ToShortTimeString();
                row["dtStartTime"] = dtStartTime;
                row["dtEndTime"] = dtEndTime;
                row["pId"] = drPlanning["PID"].ToString() == string.Empty ? 0 : Convert.ToInt32(drPlanning["PID"]);
                //row["requestCode"] = drPlanning["ParentRequestCode"].ToString();
                row["requestDate"] = dtStartTime.ToShortDateString();//Convert.ToDateTime(drPlanning["RequestDate"]).ToShortDateString();
                //row["dateCreated"] = Convert.ToDateTime(drPlanning["ScheduledDate"]).ToShortDateString();
                //row["departure"] = drPlanning["Departure"].ToString();
                //row["arrival"] = drPlanning["Arrival"].ToString();
                row["assetID"] = drPlanning["AssetID"].ToString() == String.Empty ? 0 : Convert.ToInt32(drPlanning["AssetID"]);
                row["asset"] = drPlanning["AssetName"].ToString();

                //row["driverID"] = drPlanning["DriverID"].ToString() == string.Empty ? 0 : Convert.ToInt32(drPlanning["DriverID"]);
                row["scheduleId"] = HID;
                row["arrivalId"] = drPlanning["ZoneID"].ToString() == string.Empty ? 0 : Convert.ToInt32(drPlanning["ZoneID"]);
                //row["departureId"] = drPlanning["DepartureID"].ToString() == string.Empty ? 0 : Convert.ToInt32(drPlanning["DepartureID"]);
                row["fieldCode"] = drPlanning["ZoneName"].ToString();
                row["noOfHouseholds"] = drPlanning["NoOfHousehold"].ToString();
                row["zoneDistanceCovered"] = drPlanning["ZoneDistanceCovered"].ToString();



                row["driver"] = drPlanning["DriverName"].ToString();






                //if (drPlanning["Status"].ToString() == string.Empty)
                //    drPlanning["Status"] = 0;

                //int status = (int)drPlanning["Status"];
                //if (status != 0)
                //    row["status"] = "Scheduled";
                //else
                //    row["status"] = " Not Scheduled";

                dtFinal.Rows.Add(row);
            }

            dtData dtR = new dtData();
            dtR.Data = dtFinal;
            dtR.Rowcnt = dtFinal.Rows.Count;
            return dtR;
        }
        #endregion
    }
}