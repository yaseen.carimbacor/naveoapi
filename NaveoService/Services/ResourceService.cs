﻿using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.Drivers;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Services.GMatrix;
using NaveoService.Models.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using NaveoService.Models.Custom;
using NaveoService.Models;
using NaveoOneLib.WebSpecifics;

namespace NaveoService.Services
{
    public class ResourceService
    {
        #region Methods
        /// <summary>
        /// Get all Resource Data
        /// </summary>
        /// <param name="UID"></param>
        /// <param name="sConnStr"></param>
        /// <returns></returns>
        /// 
        public dtData GetResources(Guid sUserToken, List<int> lResourceIds, string sConnStr, string rt, int? Page, int? LimitPerPage)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);

            DataTable dtResource = new DataTable();

            string defaultdriverSms = string.Empty;

            Driver.ResourceType myEnum;
            if (Enum.TryParse(rt, true, out myEnum))
                dtResource = new NaveoOneLib.Services.Drivers.DriverService().GetDriver(lm, myEnum, false, sConnStr);

            DataTable dtFinal = new DataTable();
            dtFinal.Columns.Add("resourceId", typeof(string));
            dtFinal.Columns.Add("resourceName", typeof(string));
            dtFinal.Columns.Add("resourceType", typeof(string));
            dtFinal.Columns.Add("TokenId", typeof(string));
            dtFinal.Columns.Add("status", typeof(string));
            dtFinal.Columns.Add("createdDate", typeof(string));
            dtFinal.Columns.Add("updatedDate", typeof(string));

            foreach (var ii in lResourceIds)
            {
                DataRow[] result = dtResource.Select("DriverID = " + ii + "");
                foreach (DataRow dRow in result)
                {
                    DataRow toInsert = dtFinal.NewRow();

                    string driverName = dRow["sFirtName"] + " " + dRow["sLastName"];
                    string defaultDriver = dRow["sDriverName"].ToString();

                    if (defaultDriver != "Default Driver")
                    {
                        defaultdriverSms = "Default Driver is Not editable";

                        if (defaultDriver == "Default Driver")
                            toInsert["resourceName"] = defaultDriver;
                        else
                            toInsert["resourceName"] = driverName;

                        toInsert["resourceId"] = dRow["DriverID"];
                        toInsert["resourceType"] = dRow["EmployeeType"];
                        toInsert["TokenId"] = dRow["iButton"];
                        toInsert["status"] = dRow["Status"];
                        toInsert["createdDate"] = dRow["CreatedDate"];
                        toInsert["updatedDate"] = dRow["UpdatedDate"];
                        dtFinal.Rows.Add(toInsert);
                    }
                }
            }

            DataTable ResultTable = dtFinal.Clone();
            ResultTable.Columns.Add("rowNum", typeof(int));
            ResultTable.Columns["rowNum"].AutoIncrement = true;
            ResultTable.Columns["rowNum"].AutoIncrementSeed = 1;
            ResultTable.Columns["rowNum"].AutoIncrementStep = 1;
            foreach (DataRow dr in dtFinal.Rows)
                foreach (int i in lResourceIds)
                    if (Convert.ToInt32(dr["resourceId"]) == i)
                        ResultTable.Rows.Add(dr.ItemArray);

            int iTotalRec = ResultTable.Rows.Count;
            Pagination p = Pagination.GetRowNums(Page, LimitPerPage);
            DataTable dt = new DataTable();
            DataRow[] drChk = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo);
            if (drChk != null && drChk.Length > 0)
                dt = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo).CopyToDataTable();
            else
                dt = ResultTable;

            dtData dtR = new dtData();
            dtR.Data = dt;
            dtR.Rowcnt = iTotalRec;

            return dtR;
        }

        public Models.DTO.apiResource GetResoureById(Guid sUserToken, string sConnStr, int resourceId)
        {

            Driver driver = new NaveoOneLib.Services.Drivers.DriverService().GetDriverById(resourceId.ToString(), sConnStr);
            Models.DTO.apiResource driverDTO = new Models.DTO.apiResource();
            driverDTO.resourceId = driver.DriverID;
            driverDTO.resourceType = driver.EmployeeType;
            driverDTO.title = driver.Title;
            driverDTO.firstName = driver.sFirtName;
            driverDTO.lastName = driver.sLastName;
            if (driver.iButton.Contains("_inactive") || driver.iButton.Contains("_terminated"))
            {
                driver.iButton = driver.iButton.Substring(0, driver.iButton.IndexOf("_") + 1);
                driver.iButton = driver.iButton.TrimEnd('_');
            }
            driverDTO.driverTokenNo = driver.iButton;
            driverDTO.driverTokenNo = driver.iButton;
            driverDTO.status = driver.Status;
            driverDTO.nic = driver.Ref1;
            driverDTO.dateOfBirth = driver.DateOfBirth;
            driverDTO.employeeNumber = driver.EmployeeType;
            driverDTO.homeFlatNumber = driver.HomeNo;
            driverDTO.street1 = driver.Address1;
            driverDTO.street2 = driver.Address2;
            driverDTO.postalCode = driver.PostalCode;
            driverDTO.mobileNo = driver.MobileNumber;
            driverDTO.countryCode = driver.CountryCode;
            driverDTO.privateLiscenseDriverCategory = driver.LicenseNo2;
            driverDTO.privateLiscenseNumber = driver.LicenseNo1;
            driverDTO.privateLiscenceIssueDate = driver.LicenseNo1Expiry;
            driverDTO.privateLiscenseExpiryDate = driver.Licenseno2Expiry;
            driverDTO.comment = driver.sComments;
            driverDTO.reason = driver.Reason;
            driverDTO.email = driver.Email;
            driverDTO.officerLevel = driver.OfficerLevel;
            driverDTO.countryCode = driver.CountryCode;

            //driverDTO.createdBy = (int)driver.CreatedBy;
            //driverDTO.updatedBy = (int)driver.UpdatedBy;


            if (driver.CreatedBy != null)
                driverDTO.createdBy = Convert.ToInt32(driver.CreatedBy);

            if (driver.UpdatedBy != null)
                driverDTO.updatedBy = Convert.ToInt32(driver.UpdatedBy);

            if (driver.ZoneID != null)
            {
                minifiedValues minifiedZones = new minifiedValues();
                minifiedZones.id = (int)driver.ZoneID;
                minifiedZones.description = driver.ZoneDesc;
                driverDTO.minifiedZone = minifiedZones;

            }

            if (driver.Type_Institution != null)
            {
                minifiedValues minifiedInstitution = new minifiedValues();
                minifiedInstitution.id = (int)driver.Type_Institution;
                minifiedInstitution.description = driver.Type_InstitutionDesc;
                driverDTO.minifiednameOfInsitution = minifiedInstitution;


            }

            if (driver.OfficerTitle != string.Empty)
            {
                driverDTO.responsibleOfficer = driver.OfficerTitle;
            }

            if (driver.City != string.Empty)
            {
                minifiedValues minifiedValues = new minifiedValues();
                minifiedValues.id = Convert.ToInt32(driver.City);
                minifiedValues.description = driver.CityDesc;
                driverDTO.minifiedcity = minifiedValues;
            }

            if (driver.District != string.Empty)
            {

                minifiedValues minifiedValues = new minifiedValues();
                minifiedValues.id = Convert.ToInt32(driver.District);
                minifiedValues.description = driver.DistrictDesc;
                driverDTO.minifieddistrict = minifiedValues;
            }

            if (driver.Type_Driver != null)
            {
                minifiedValues minifiedValues = new minifiedValues();
                minifiedValues.id = Convert.ToInt32(driver.Type_Driver);
                minifiedValues.description = driver.Type_DriverDesc;
                driverDTO.minifieddriverType = minifiedValues;
            }

            if (driver.Type_Grade != null)
            {
                minifiedValues minifiedValues = new minifiedValues();
                minifiedValues.id = Convert.ToInt32(driver.Type_Grade);
                minifiedValues.description = driver.Type_GradeDesc;
                driverDTO.minifiedgradeType = minifiedValues;
            }


            if (driver.UserId != null)
            {
                minifiedValues minifiedUser = new minifiedValues();
                minifiedUser.id = Convert.ToInt32(driver.UserId);
                minifiedUser.description = driver.UserDesc;
                driverDTO.minifiedUsers = minifiedUser;


            }

            foreach (var d in driver.Licenses)
            {
                resourceLiscense rl = new resourceLiscense();
                rl.iID = d.iID;
                rl.DriverID = d.DriverID;
                rl.sCategory = d.sCategory;
                rl.sNumber = d.sNumber;
                rl.sType = d.sType;
                rl.ExpiryDate = d.ExpiryDate;
                rl.IssueDate = d.IssueDate;
                rl.oprType = d.oprType;
                driverDTO.resourceLiscense.Add(rl);

            }

            foreach (var m in driver.lMatrix)
            {
                aMatrix rM = new aMatrix();
                rM.iId = m.iID;
                rM.gMId = m.GMID;
                rM.gMDesc = m.GMDesc;
                rM.mId = m.MID;
                rM.oprType = m.oprType;

                driverDTO.lMatrix.Add(rM);
            }


            return driverDTO;
        }

        /// <summary>
        /// Save/Update Resource
        /// </summary>
        /// <param name="sUserToken"></param>
        /// <param name="ResourceModel"></param>
        /// <param name="sConnStr"></param>
        /// <returns></returns>
        public Boolean SaveUpdateResource(SecurityTokenExtended securityToken, ResourceDTO resourceDTO, bool bInsert)
        {
            int UID = CommonService.GetUserID(securityToken.securityToken.UserToken, securityToken.securityToken.sConnStr);

            #region preparing resoure Model to be save to dll function
            Driver d = new Driver();
            foreach (var resource in resourceDTO.resources)
            {
                #region ResourceMainDetails



                d.DriverID = resource.resourceId;
                d.EmployeeType = resource.resourceType;
                d.Title = resource.title;
                d.sFirtName = resource.firstName;
                d.sLastName = resource.lastName;
                d.sDriverName = resource.firstName + " " + resource.lastName;
                d.Ref1 = resource.nic;
                d.DateOfBirth = resource.dateOfBirth;
                d.sEmployeeNo = resource.employeeNumber;
                d.HomeNo = resource.homeFlatNumber;
                d.Address1 = resource.street1;
                d.Address2 = resource.street2;
                d.PostalCode = resource.postalCode;
                d.MobileNumber = resource.mobileNo;
                d.CountryCode = resource.countryCode;
                d.LicenseNo2 = resource.privateLiscenseDriverCategory;
                d.LicenseNo1 = resource.privateLiscenseNumber;
                d.LicenseNo1Expiry = resource.privateLiscenceIssueDate;
                d.Licenseno2Expiry = resource.privateLiscenseExpiryDate;
                d.sComments = resource.comment;
                d.Reason = resource.reason;
                d.Email = resource.email;
                d.OfficerLevel = resource.officerLevel;
                d.oprType = resource.oprType;

                if (resource.minifiedUsers != null)
                    d.UserId = resource.minifiedUsers.id;

                if (d.UserId == 0)
                {
                    d.UserId = null;
                }

                if (resource.minifiedZone != null)
                    d.ZoneID = resource.minifiedZone.id;

                if (bInsert)
                {
                    d.CreatedDate = DateTime.Now;
                    d.CreatedBy = UID;

                    //Correction of BUG Driver Creation Fails - 1hour
                    //d.UserId = UID;


                }

                else
                {
                    d.UpdatedDate = DateTime.Now;
                    d.UpdatedBy = UID;


                }


                #endregion
                #region LOOKUPVALUES
                if (resource.minifiedcity != null)
                    d.City = resource.minifiedcity.id.ToString();

                if (resource.minifieddistrict != null)
                    d.District = resource.minifieddistrict.id.ToString();

                if (resource.minifieddriverType != null)
                    d.Type_Driver = resource.minifieddriverType.id;

                if (resource.minifiedgradeType != null)
                    d.Type_Grade = resource.minifiedgradeType.id;

                if (resource.minifiednameOfInsitution != null)
                    d.Type_Institution = resource.minifiednameOfInsitution.id;


                if (resource.responsibleOfficer != null)
                    d.OfficerTitle = resource.responsibleOfficer;

                if (d.Type_Driver == 0)
                    d.Type_Driver = null;

                if (d.Type_Grade == 0)
                    d.Type_Grade = null;

                if (d.Type_Institution == 0)
                    d.Type_Institution = null;



                #endregion
                #region MATRIX
                foreach (var mt in resource.lMatrix)
                {
                    NaveoOneLib.Models.GMatrix.Matrix rM = new NaveoOneLib.Models.GMatrix.Matrix();
                    rM.iID = mt.iId;
                    rM.GMID = mt.gMId;
                    rM.GMDesc = mt.gMDesc;
                    rM.MID = mt.mId;
                    rM.oprType = mt.oprType;

                    if (!bInsert)
                        rM.iID = resource.resourceId;

                    d.lMatrix.Add(rM);

                }

                #endregion
                #region Status related with ibutton

                

                if (resource.driverTokenNo != null)
                {

                    d.iButton = string.Empty;

                    string date = DateTime.Now.ToString("MM/dd/yyyy");
                    string tokenInactive = resource.driverTokenNo + "_inactive_" + date;
                    string tokenTerminated = resource.driverTokenNo + "_terminated_" + date;

                    if (resource.status.ToUpper() == "INACTIVE")
                        resource.driverTokenNo = tokenInactive;

                    if (resource.status.ToUpper() == "TERMINATED")
                    {

                        resource.driverTokenNo = tokenTerminated;
                    }


                    if (resource.status.ToUpper() == "ACTIVE")
                    {
                        if (resource.driverTokenNo.Contains("_inactive") || resource.driverTokenNo.Contains("_terminated"))
                        {
                            resource.driverTokenNo = resource.driverTokenNo.Substring(0, resource.driverTokenNo.IndexOf("_") + 1);
                            resource.driverTokenNo = resource.driverTokenNo.TrimEnd('_');
                        }

                        d.iButton = resource.driverTokenNo;

                    }

                    d.Status = resource.status;
                    d.iButton = resource.driverTokenNo;


                } else
                {
                    d.Status = resource.status;
                    d.iButton = resource.driverTokenNo;
                }

                #endregion
                #region PHOTO
                if (resource.Photo != null)
                {
                    byte[] myByte = System.Text.ASCIIEncoding.Default.GetBytes(resource.Photo);
                    d.Photo = myByte;
                }
                #endregion
                #region ResourceLiscence
                if (resource.resourceLiscense != null)
                {
                    foreach (var r in resource.resourceLiscense)
                    {
                        //if (r.DriverID == 0 && r.oprType == DataRowState.Added)
                        //{

                        //} else
                        //{
                        NaveoOneLib.Models.Drivers.ResourceLicense rl = new NaveoOneLib.Models.Drivers.ResourceLicense();
                        rl.iID = r.iID;


                        if (bInsert)
                            rl.DriverID = r.DriverID;
                        else
                            rl.DriverID = d.DriverID;


                        rl.sType = "pppp";
                        rl.sCategory = r.sCategory;
                        rl.sNumber = r.sNumber;
                        rl.IssueDate = r.IssueDate;
                        rl.ExpiryDate = r.ExpiryDate;
                        rl.oprType = r.oprType;

                        d.Licenses.Add(rl);
                        //}




                    }
                }
                #endregion
            }
            #endregion

            BaseModel bm = new NaveoOneLib.Services.Drivers.DriverService().SaveDriver(d, bInsert, securityToken.securityToken.sConnStr);
            return bm.data;
        }

        public Boolean CheckIbutton(string ibutton, string sConnStr)
        {

            if (ibutton.Contains("_inactive") || ibutton.Contains("_terminated"))
            {
                ibutton = ibutton.Substring(0, ibutton.IndexOf("_") + 1);
                ibutton = ibutton.TrimEnd('_');
            }
            Boolean b = new NaveoOneLib.Services.Drivers.DriverService().CheckIFiButtonExist(ibutton, sConnStr);

            BaseModel bm = new BaseModel();
            bm.data = b;
            return bm.data;
        }

        public dtData GetTreeResourceType(Guid sUserToken, String sConnStr, string rt, int? Page, int? LimitPerPage)
        {
            Driver.ResourceType myEnum;
            if (Enum.TryParse(rt, true, out myEnum))
                return new NaveoOneLib.WebSpecifics.LibData().treeResources(sUserToken, sConnStr, myEnum, Page, LimitPerPage);
            return new dtData();
        }

        public BaseModel Delete(int driverId, string sConnStr)
        {
            Driver d = new Driver();
            d.DriverID = driverId;

            Driver defaultDriver = new NaveoOneLib.Services.Drivers.DriverService().GetDefaultDriver(sConnStr);

            string result = String.Empty;

            if (d.DriverID == defaultDriver.DriverID)
            {
                result = "Default Driver cannot be deleted";
            }
            else
            {
                Boolean b = new NaveoOneLib.Services.Drivers.DriverService().DeleteDriver(d, sConnStr);

                if (b)
                    result = "true";
                else
                    result = "false";

            }





            BaseModel bm = new BaseModel();
            bm.data = result;
            return bm;
        }


        public dtData GetDriverExpiry(Guid sUserToken, string sConnStr)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);
            DataTable dt = new NaveoOneLib.Services.Drivers.DriverService().GetLiscenseDriverExpiry(lm, false, sConnStr);
            if (dt == null)
                dt = new DataTable();
            NaveoOneLib.Services.GlobalParamsService globalParamsService = new NaveoOneLib.Services.GlobalParamsService();
            int b = Convert.ToInt32(globalParamsService.GetGlobalParamsByName("DriverLiscenseExpiry", sConnStr).PValue);
            string SystemAdminEmail = globalParamsService.GetGlobalParamsByName("SystemAdminEmail", sConnStr).PValue;

            try
            {
                List<DataRow> toDelete = new List<DataRow>();

                foreach (DataRow dr in dt.Rows)
                {

                    int driverDateDiff = 0;

                    if (dr["date"].ToString() == string.Empty)
                        driverDateDiff = 0;
                    else
                        driverDateDiff = Convert.ToInt32(dr["date"]);


                    if (b <= driverDateDiff || driverDateDiff == 0)
                    {
                        toDelete.Add(dr);
                    }
                }

                foreach (DataRow dr in toDelete)
                {
                    dt.Rows.Remove(dr);
                }

            }
            catch
            {

            }


            int iTotalRec = 0;

            try
            {
                string sMail = string.Empty;
                int count = 0;
                string sBody = string.Empty;

                if (dt.Rows.Count != 0)
                {
                    iTotalRec = dt.Rows.Count;

                    sBody = "Dear " + SystemAdminEmail.ToUpper() + " ,\r\n\r\n";
                    sBody += "Please note that the Following Drivers are going to Expire soon:\r\n\r\n";


                    foreach (DataRow drs in dt.Rows)
                    {
                        count++;
                        string drivername = drs["sdrivername"].ToString();
                        string expiraydate = drs["expirydate"].ToString();
                        string driverid = drs["driverid"].ToString();

                        sBody += "DriverID: " + driverid + " \r\n";
                        sBody += "DriverName: " + drivername.ToUpper() + "\r\n";
                        sBody += "Expiry Date: " + expiraydate + "\r\n\r\n";
                    }

                    sMail = new NaveoOneLib.Services.Plannings.PlanningService().sendPLanningMail(UID, SystemAdminEmail, "Naveo FLeet Management System", "Driver Liscense Expiry Reminder", sBody, sConnStr);
                }
            }
            catch
            {

            }

            dtData dtR = new dtData();
            dtR.Data = dt;
            dtR.Rowcnt = iTotalRec;
            return dtR;
        }
        #endregion
    }
}
