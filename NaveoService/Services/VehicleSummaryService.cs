﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NaveoService.Models;
using NaveoOneLib.Models.GPS;
using NaveoOneLib.Models.Users;
using NaveoOneLib.Services.Rules;
using NaveoOneLib.Services.Trips;
using NaveoOneLib.Services.Users;

namespace NaveoService.Services
{
    public class VehicleSummaryService
    {
        #region Methods
        /// <summary>
        /// Get Vehicle Summary
        /// </summary>
        /// <param name="sUserToken"></param>
        /// <param name="iAssetID"></param>
        /// <param name="dtFr"></param>
        /// <param name="dtTo"></param>
        /// <param name="sConnStr"></param>
        /// <returns></returns>
        public VehicleSummary GetVehicleSummary(Guid sUserToken, int iAssetID, DateTime dtFr, DateTime dtTo, double iMinTripDistance, String sConnStr)
        {
            VehicleSummary myResult = new VehicleSummary();
            myResult.assetId = iAssetID;

            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            DataTable dtGMAsset = new NaveoOneLib.WebSpecifics.LibData().dtGMAsset(UID, sConnStr);

            //chk assetid with dtGMAsset assetids
            DataRow[] dr = dtGMAsset.Select("StrucID = " + iAssetID);
            if (dr.Length == 0)
                return myResult;

            List<int> lIDs = new List<int>();
            lIDs.Add(iAssetID);

            List<GPSData> lData = new NaveoOneLib.Services.GPS.GPSDataService().GetLatestGPSData(lIDs, new List<string>(),false, UID, true, sConnStr);
            List<Live> l = castLApiLive(lData);

            DataTable Trips = new TripHeaderService().GetNumberOfTrips(iAssetID, dtFr, dtTo, iMinTripDistance, sConnStr);
            DataSet dsAlerts = new ExceptionsService().GetExceptions(lIDs, new List<int>(), dtFr, dtTo, String.Empty, false, true, null, sConnStr);
            int iAlerts = 0;
            if (dsAlerts.Tables.Count > 0)
                if (dsAlerts.Tables["dtExceptionHeader"].Rows.Count > 0)
                    int.TryParse(dsAlerts.Tables["dtExceptionHeader"].Rows[0]["cnt"].ToString(), out iAlerts);

            myResult.noOfAlerts = iAlerts;

            int iPossibleDrops = 0;
            DataSet dsGetFuelConsumption = new NaveoOneLib.Services.Fuels.FuelService().GetFuelConsumption(UID, null, null, iAssetID, sConnStr, 1, 1).data;
            if (dsGetFuelConsumption.Tables.Contains("FuelData"))
                if (dsGetFuelConsumption.Tables["FuelData"].Rows.Count > 0)
                {
                    myResult.hasFuel = true;
                    if (int.TryParse(dsGetFuelConsumption.Tables["FuelData"].Rows[0]["possibleDrops"].ToString(), out iPossibleDrops))
                        myResult.PossibleDropsID = int.Parse(dsGetFuelConsumption.Tables["FuelData"].Rows[0]["fuelDataUID"].ToString());
                }
            myResult.possibleDrops = iPossibleDrops;

            if (l.Count > 0)
                myResult.liveData = l[0];

            if (Trips.Rows.Count > 0)
            {
                myResult.noOfTrips = (int)Trips.Rows[0]["TotalTrips"];
                myResult.tripDistance = (int)(Double)Trips.Rows[0]["TripDistance"];
                myResult.tripTime = (TimeSpan)Trips.Rows[0]["tsTripTime"];
                myResult.idleTime = (TimeSpan)Trips.Rows[0]["tsIdlingTime"];
            }
            return myResult;
        }

        #endregion

        #region Private Methods
        private List<Live> castLApiLive(List<GPSData> lData)
        {
            List<Live> l = new List<Live>();
            foreach (GPSData g in lData)
            {
                Live al = new Models.Live();
                al.assetId = g.AssetID;
                al.driverId = g.DriverID;
                al.isGPSValid = g.LongLatValidFlag;
                al.latitude = g.Latitude;
                al.longitude = g.Longitude;
                al.speed = (int)g.Speed;

                String[] sParts = g.Availability.Split('|');
                al.activity = sParts[0].Replace(".png", String.Empty);

                int iZone = -1;
                if (int.TryParse(sParts[1], out iZone))
                    al.zoneId = iZone;
                al.address = sParts[2];

                al.dtLastReported = g.LastReportedInLocalTime;
                al.dtGPS = g.dtLocalTime;
                l.Add(al);
            }
            return l;
        }
        #endregion

    }
}