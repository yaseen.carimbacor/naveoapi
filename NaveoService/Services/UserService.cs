﻿using NaveoOneLib.Models;
using NaveoService.Constants;
using NaveoService.Helpers;
using NaveoService.Models;
using NaveoService.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using static NaveoService.Constants.Enums;
using NaveoOneLib.Services;
using NaveoOneLib.Common;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.Users;
using NaveoService.Models.DTO;
using NaveoService.Models.Custom;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Services.Rules;
using NaveoOneLib.Constant;
using System.Net;
using System.Text;


namespace NaveoService.Services
{
    public class UserService
    {
        public dtData GetUserData(Guid sUserToken, List<int> lAssetIDs, string sConnStr, int? Page, int? LimitPerPage)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);
            DataTable dtUsers = new NaveoOneLib.Services.Users.UserService().GetAllUsers(lm, sConnStr);

            if (dtUsers.Columns.Contains("accesslist"))
                dtUsers.Columns["accessList"].ColumnName = "AccessList";

            if (dtUsers.Columns.Contains("accesstemplateid"))
                dtUsers.Columns["accesstemplateId"].ColumnName = "AccessTemplateId";

            if (dtUsers.Columns.Contains("createdby"))
                dtUsers.Columns["createdBy"].ColumnName = "createdby";

            if (dtUsers.Columns.Contains("createddate"))
                dtUsers.Columns["createddate"].ColumnName = "CreatedDate";

            if (dtUsers.Columns.Contains("datejoined"))
                dtUsers.Columns["datejoined"].ColumnName = "DateJoined";

            if (dtUsers.Columns.Contains("dummy1"))
                dtUsers.Columns["dummy1"].ColumnName = "Dummy1";

            if (dtUsers.Columns.Contains("email"))
                dtUsers.Columns["email"].ColumnName = "Email";

            if (dtUsers.Columns.Contains("externalaccess"))
                dtUsers.Columns["externalaccess"].ColumnName = "ExternalAccess";

            if (dtUsers.Columns.Contains("lastname"))
                dtUsers.Columns["lastname"].ColumnName = "LastName";

            if (dtUsers.Columns.Contains("loginattempt"))
                dtUsers.Columns["loginattempt"].ColumnName = "LoginAttempt";

            if (dtUsers.Columns.Contains("loginCount"))
                dtUsers.Columns["loginCount"].ColumnName = "LoginCount";

            if (dtUsers.Columns.Contains("maxdaymail"))
                dtUsers.Columns["maxdaymail"].ColumnName = "MaxDayMail";

            if (dtUsers.Columns.Contains("mobileaccess"))
                dtUsers.Columns["mobileaccess"].ColumnName = "MobileAccess";

            if (dtUsers.Columns.Contains("mobileno"))
                dtUsers.Columns["mobileno"].ColumnName = "MobileNo";

            if (dtUsers.Columns.Contains("names"))
                dtUsers.Columns["names"].ColumnName = "Names";

            if (dtUsers.Columns.Contains("plnlkpval"))
                dtUsers.Columns["plnlkpval"].ColumnName = "PlnLkpVal";

            if (dtUsers.Columns.Contains("status_b"))
                dtUsers.Columns["status_b"].ColumnName = "Status_b";

            if (dtUsers.Columns.Contains("tel"))
                dtUsers.Columns["tel"].ColumnName = "Tel";

            if (dtUsers.Columns.Contains("uid"))
                dtUsers.Columns["uid"].ColumnName = "UID";

            if (dtUsers.Columns.Contains("zoneid"))
                dtUsers.Columns["zoneid"].ColumnName = "ZoneID";

            if (dtUsers.Columns.Contains("username"))
                dtUsers.Columns["username"].ColumnName = "Username";


            if (dtUsers.Columns.Contains("rowNum"))
                dtUsers.Columns["rowNum"].ColumnName = "rowNum";


            DataTable ResultTable = dtUsers.Clone();
            ResultTable.Columns.Add("rowNum", typeof(int));
            ResultTable.Columns["rowNum"].AutoIncrement = true;
            ResultTable.Columns["rowNum"].AutoIncrementSeed = 1;
            ResultTable.Columns["rowNum"].AutoIncrementStep = 1;
            foreach (DataRow dr in dtUsers.Rows)
                foreach (int i in lAssetIDs)
                    if (Convert.ToInt32(dr["UID"]) == i)
                        ResultTable.Rows.Add(dr.ItemArray);

            int iTotalRec = ResultTable.Rows.Count;
            Pagination p = Pagination.GetRowNums(Page, LimitPerPage);
            DataTable dt = new DataTable();
            DataRow[] drChk = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo);
            if (drChk != null && drChk.Length > 0)
                dt = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo).CopyToDataTable();
            else
                dt = ResultTable;

            dtData dtR = new dtData();
            dtR.Data = dt;
            dtR.Rowcnt = iTotalRec;

            return dtR;
        }

        private UserExpiryDetails getExpiryDates(String sConnStr) {
            NaveoOneLib.Services.GlobalParamsService globalParamsService = new NaveoOneLib.Services.GlobalParamsService();
            UserExpiryDetails ud = new UserExpiryDetails();
            ud.ExpiryDay = Convert.ToInt32(globalParamsService.GetGlobalParamsByName("EmailExpiryInterval", sConnStr).PValue);
            ud.ExpiryWarnDay = Convert.ToInt32(globalParamsService.GetGlobalParamsByName("EmailExpiryWarnInterval", sConnStr).PValue);
            ud.WillExpire = ud.ExpiryDay > 0;

            return ud;

        }

        public Boolean ChangePassword(String sToken, String sOldPassword, String sNewPassword, String sConnStr)
        {
            UserExpiryDetails ud = this.getExpiryDates(sConnStr);



            if (sNewPassword != string.Empty)
                sNewPassword = NaveoOneLib.Services.BaseService.RSADecrypt(sNewPassword);

            return new NaveoOneLib.Services.Users.UserService().ChangePassword(sToken, sOldPassword, sNewPassword, sConnStr, ud);
        }

        /// <summary>
        /// Creates or updates user based on bool (bInsert) 
        /// </summary>
        /// <param name="userDTO"></param>
        /// <param name="bInsert">true for create and false for update</param>
        /// <param name="sConnStr"></param>
        /// <returns></returns>
        public BaseDTO SaveUser(SecurityTokenExtended securityToken, UserDTO userDTO, bool bInsert, string servername)
        {
            //TODO : Pravish Validations

            int userId = CommonService.GetUserID(securityToken.securityToken.UserToken, securityToken.securityToken.sConnStr);
            List<int> lUsers = new List<int>();
            lUsers.Add(userId);

            int uiD = -1;
            List<int> lValidatedUsers = CommonService.lValidateUsers(securityToken.securityToken.UserToken, lUsers, securityToken.securityToken.sConnStr, out uiD);
            int UID = lValidatedUsers[0];
            
            #region GFI_SYS_User


            User u = new User();
            UserExpiryDetails ud = this.getExpiryDates(securityToken.securityToken.sConnStr);

            ServiceHelper.DefaultValues(u);
            foreach (Models.DTO.apiUser luser in userDTO.users)
            {
                u.UID = luser.UID;
                u.Username = luser.Username;

                u.LastName = luser.LastName;
                u.Names = luser.Names; //+ " " + luser.LastName;
                u.Email = luser.Email;
                u.MobileNo = luser.MobileNo;
                u.Status_b = (luser.Status_b == "Active") ? AccountStatus.Active : AccountStatus.Inactive;
                //u.Password = luser.Password;
                u.Tel = luser.Tel;
                u.MaxDayMail = luser.MaxDayMail;
                u.ExternalAccess = luser.ExternalAccess;
                u.Dummy1 = luser.Title;
                u.oprType = luser.oprType;


                if (ud.WillExpire) {
                    u.PswdExpiryDate = DateTime.Now.AddDays(ud.ExpiryDay);
                    u.PswdExpiryWarnDate = DateTime.Now.AddDays(ud.ExpiryDay - ud.ExpiryWarnDay);
                }

                if (luser.Zone != null)
                    u.ZoneID = luser.Zone.id;

                if (luser.PlnLkpVal != null)
                    u.PlnLkpVal = luser.PlnLkpVal.id;

                u.lMatrix = luser.lMatrix;
                u.lRoles = luser.lRoles;
                u.userExtension = luser.userExtension;

                if (bInsert)
                {
                    u.CreatedBy = UID;
                }
                else
                {
                    u.UpdatedBy = UID;
                }

            }


            #endregion

            BaseModel userObj = new NaveoOneLib.Services.Users.UserService().SaveUser(u, bInsert, securityToken.securityToken.sConnStr);

            if (userObj.data == true)
            { 
                if (bInsert)
                {
                    int USERID = CommonService.GetUserID(securityToken.securityToken.UserToken, securityToken.securityToken.sConnStr);
                    String s = new NaveoOneLib.Services.Users.UserService().RequestPasswordReset(USERID,u.Email, bInsert, securityToken.securityToken.sConnStr, servername);
                    

                }
            }

            //userObj.errorMessage += reponse;
            return userObj.ToBaseDTO(securityToken.securityToken);
        }

        public Boolean UpdateUserStatus(int id, String status, String sConnStr) {
            User u = new NaveoOneLib.Services.Users.UserService().GetUserById(id, sConnStr);
            u.Status_b = status;
            return new NaveoOneLib.Services.Users.UserService().UpdateUserStatus(u, null, sConnStr);
        }

        public BaseDTO logoutUserByToken(SecurityTokenExtended securityToken, String sConnstr) {
            BaseModel obj = new NaveoOneLib.Services.Users.UserService().logoutUserByToken(securityToken.securityToken.UserToken.ToString(), sConnstr);
            return obj.ToBaseDTO(securityToken.securityToken);
        }

        /// <summary>
        /// Get all users by owner
        /// </summary>
        /// <param name="UID"></param>
        /// <param name="sConnStr"></param>
        /// <returns></returns>
        //public DataTable dtUsers(int UID, string sConnStr)
        //{
        //    // TODO: [Aboo to Reza] Should return users directly from method lGetUsers (return type should be List<User>)
        //    // Used entity framework as test for now
        //    //List<int> li = new NaveoOneLib.Models.User().lGetUsers(UID, sConnStr);
        //    NaveoDBFirstEntities naveoDbContext = new NaveoDBFirstEntities(sConnStr);
        //    List<GFI_SYS_User> users = naveoDbContext.GFI_SYS_User.Where(u => u.CreatedBy == UID).ToList();

        //    DataTable dt = new DataTable();
        //    List<String> cols = new List<String>() { "UID", "Title", "Department", "Tel", "MobileNo", "Email", "UserName" };
        //    cols.ForEach(col => { dt.Columns.Add(col); });

        //    DataRow dr;
        //    if (users.Any())
        //    {
        //        users.ForEach(user =>
        //        {
        //            dr = dt.NewRow();
        //            dr[0] = user.UID;
        //            //dr["Title"] = user.Title + " " + user.Names + " " + user.LastName;
        //            dr[1] = "Mr.";
        //            dr[2] = "";//user.Department;
        //            dr[3] = user.Tel;
        //            dr[4] = user.MobileNo;
        //            dr[5] = user.Email;
        //            dr[6] = user.Names;
        //            dt.Rows.Add(dr);
        //        });
        //    }

        //    return dt;
        //}

        /// <summary>
        /// Get specific user by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="sConnStr"></param>
        /// <returns></returns>
        public apiUser GetUserById(int id, String sConnStr)
        {

            User dllUser = new NaveoOneLib.Services.Users.UserService().GetUserById(id, User.EditModePswd, sConnStr);
            Models.DTO.apiUser userDTO = new Models.DTO.apiUser();
            userDTO.UID = dllUser.UID;
            userDTO.lMatrix = dllUser.lMatrix;
            userDTO.lRoles = dllUser.lRoles;
            userDTO.userExtension = dllUser.userExtension;
            userDTO.Username = dllUser.Username;
            userDTO.Names = dllUser.Names;
            userDTO.LastName = dllUser.LastName;
            userDTO.Email = dllUser.Email;
            userDTO.Tel = dllUser.Tel;
            userDTO.MobileNo = dllUser.MobileNo;
            userDTO.Title = dllUser.Dummy1;
            userDTO.oprType = dllUser.oprType;
            userDTO.Status_b = dllUser.Status_b;
            if (dllUser.Status_b == "AC")
                userDTO.Status_b = "Active";

            if (dllUser.Status_b == "IN")
                userDTO.Status_b = "InActive";

            if (dllUser.Status_b == "CP")
                userDTO.Status_b = "CP";

            //userDTO.Status_b = dllUser.Status_b;
            userDTO.AccessTemplateId = dllUser.AccessTemplateId;
            userDTO.MaxDayMail = dllUser.MaxDayMail;

            minifiedValues minifiedzone = new minifiedValues();
            minifiedValues minifiedLkp = new minifiedValues();
            if (dllUser.ZoneID != null)
            {
                minifiedzone.id = Convert.ToInt32(dllUser.ZoneID);
                minifiedzone.description = dllUser.zoneDesc;
                userDTO.Zone = minifiedzone;
            }

            if (dllUser.PlnLkpVal != null)
            {
                minifiedLkp.id = Convert.ToInt32(dllUser.PlnLkpVal);
                minifiedLkp.description = dllUser.PlnKpValDesc;
                userDTO.PlnLkpVal = minifiedLkp;
            }

            userDTO.MobileAccess = dllUser.MobileAccess;
            userDTO.ExternalAccess = dllUser.ExternalAccess;
            userDTO.TwoStepVerif = dllUser.TwoStepVerif;
            userDTO.bTwoWayAuthEnabled = dllUser.bTwoWayAuthEnabled;
            userDTO.UserToken = dllUser.UserToken;

            return userDTO;
        }

        public List<apiUser> GetInactiveUsers(String sConnStr)
        {
            List<User> dllUsers = new NaveoOneLib.Services.Users.UserService().GetInactiveUsers(sConnStr);
            List<Models.DTO.apiUser> usersDTO = new List<Models.DTO.apiUser>();
            foreach (User dllUser in dllUsers) {
                Models.DTO.apiUser userDTO = new Models.DTO.apiUser();
                userDTO.UID = dllUser.UID;
                userDTO.lMatrix = dllUser.lMatrix;
                userDTO.lRoles = dllUser.lRoles;
                userDTO.userExtension = dllUser.userExtension;
                userDTO.Username = dllUser.Username;
                userDTO.Names = dllUser.Names;
                userDTO.LastName = dllUser.LastName;
                userDTO.Email = dllUser.Email;
                userDTO.Tel = dllUser.Tel;
                userDTO.MobileNo = dllUser.MobileNo;
                userDTO.Title = dllUser.Dummy1;
                userDTO.oprType = dllUser.oprType;
                userDTO.Status_b = dllUser.Status_b;
                if (dllUser.Status_b == "AC")
                    userDTO.Status_b = "Active";

                if (dllUser.Status_b == "IN")
                    userDTO.Status_b = "InActive";

                if (dllUser.Status_b == "CP")
                    userDTO.Status_b = "CP";

                //userDTO.Status_b = dllUser.Status_b;
                userDTO.AccessTemplateId = dllUser.AccessTemplateId;
                userDTO.MaxDayMail = dllUser.MaxDayMail;

                minifiedValues minifiedzone = new minifiedValues();
                minifiedValues minifiedLkp = new minifiedValues();
                if (dllUser.ZoneID != null)
                {
                    minifiedzone.id = Convert.ToInt32(dllUser.ZoneID);
                    minifiedzone.description = dllUser.zoneDesc;
                    userDTO.Zone = minifiedzone;
                }

                if (dllUser.PlnLkpVal != null)
                {
                    minifiedLkp.id = Convert.ToInt32(dllUser.PlnLkpVal);
                    minifiedLkp.description = dllUser.PlnKpValDesc;
                    userDTO.PlnLkpVal = minifiedLkp;
                }

                userDTO.MobileAccess = dllUser.MobileAccess;
                userDTO.ExternalAccess = dllUser.ExternalAccess;
                userDTO.TwoStepVerif = dllUser.TwoStepVerif;
                userDTO.bTwoWayAuthEnabled = dllUser.bTwoWayAuthEnabled;
                userDTO.UserToken = dllUser.UserToken;
                userDTO.AccessedOn = dllUser.AccessedOn;

                usersDTO.Add(userDTO);
            }
            

            return usersDTO;
        }

        /// <summary>
        /// Deletes a user
        /// </summary>
        /// <param name="user"></param>
        /// <param name="sConnStr"></param>
        /// <returns></returns>
        public BaseDTO Delete(SecurityTokenExtended securityToken, string userEmail)
        {

            User user = new User();
           
            user = new NaveoOneLib.Services.Users.UserService().GetUserByeMail(userEmail, String.Empty, securityToken.securityToken.sConnStr, false, false);
            //TODO : Pravish Validation
            //User user = new User()
            //{
            //    UID = id
            //};
            BaseModel deleteResult = new NaveoOneLib.Services.Users.UserService().DeleteUser(user, securityToken.securityToken.sConnStr);
            return deleteResult.ToBaseDTO(securityToken.securityToken);
        }

        public BaseModel ResetPassword(String email, String sConnStr)
        {
            BaseModel resetPassword = new NaveoOneLib.Services.Users.UserService().ResetToDefaultPassword(email, sConnStr);
            return resetPassword;
        }

        public dtData getUserApprovalValues(int gmID, string sConnStr)
        {
            dtData dt = new NaveoOneLib.Services.Users.UserService().GetUserApprovalProcess(gmID, sConnStr);

            dt.Data.Columns["iID"].ColumnName = "id";
            dt.Data.Columns["Source"].ColumnName = "description";
            dt.Data.Columns.Remove("ApprovalValue");
            dt.Data.TableName = "approval";
            dt.Rowcnt = dt.Data.Rows.Count;
            return dt;
        }

        public Boolean PrepareDataNotif(int UID, DateTime dtSent, string Err, string sBody, string Type, string Title,string sConnStr)
        {
           
            DataTable dtNotification = NotificationService.NotificationDT();
            foreach(DataRow dr in dtNotification.Rows)
            {
                
                dr["UID"] = UID;
                dr["SendAt"] = Convert.ToDateTime(dtSent);
                dr["Err"] = Err.ToString() ;
                dr["sBody"] = sBody.ToString();
                dr["Type"] = Type.ToString();
                dr["Title"] = Title.ToString();
                dtNotification.Rows.Add(dr);

            }



            Boolean bSave = new NotificationService().SaveNotification(dtNotification, sConnStr);
        
            return bSave;

        }

        public string UserCreationTOGMS(User user, String sConnStr)
        {


            string sServerName = new NaveoService.Helpers.ApiHelper().getServerName();
            //sServerName = "Earth";

            String responseString = String.Empty;

            try
            {
                User u = new NaveoOneLib.Services.Users.UserService().GetUserByeMail(user.Email, User.EditModePswd, sConnStr, false, false);
                String FleetUserName = "FleetMS";
                String FleetPassword = "Nave0@2019";
                String FleetServer = sServerName;
                String FleetUserToken = u.UserToken.ToString();
                String Email = user.Email;
                String Title = user.Dummy1;
                String FirstName = user.Names;
                String LastName = user.LastName;

               
                string parameters = "FleetUsername=" + FleetUserName + "&FleetPassword=" + FleetPassword + "&FleetServer=" + FleetServer + "&FleetUserTOKEN=" + FleetUserToken + "&Email=" + Email + "&Title=" + Title + "&FirstName=" + FirstName + "&LastName=" + LastName + "";

                string url = NaveoEntity.GMSWebAddress + "Account/CreateFleetUser?" + parameters;
      
                responseString = new NaveoService.Helpers.ApiHelper().ApiCall("Get", url, null);
            }
            catch (Exception ex)
            {
                responseString = ex.ToString();
                System.IO.File.AppendAllText(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "\\Registers\\gms.dat", "\r\n User Creation \r\n" + responseString + "\r\n");
            }

            return responseString;


        }

        public BaseModel MigrateUserToGMS(SecurityTokenExtended securityToken)
        {
            Boolean BInsert = false;
            int UID = CommonService.GetUserID(securityToken.securityToken.UserToken, securityToken.securityToken.sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, securityToken.securityToken.sConnStr);
            DataTable dtUsers = new NaveoOneLib.Services.Users.UserService().GetAllUsers(lm, securityToken.securityToken.sConnStr);
            int TotalUsers = dtUsers.Rows.Count;

            string sServerName = new NaveoService.Helpers.ApiHelper().getServerName();
            //sServerName = "Earth";
            int cntCreated = 0;
            int cntExist = 0;
        
         
            string ErrorServer = string.Empty;
            foreach (DataRow dr in dtUsers.Rows)
            {
                User u = new User();
                User uEmail = new NaveoOneLib.Services.Users.UserService().GetUserByeMail(dr["Email"].ToString(), User.EditModePswd, securityToken.securityToken.sConnStr, false, false);
                u.UserToken = uEmail.UserToken;
                u.Email = uEmail.Email.ToString();
                u.Dummy1 = dr["Dummy1"].ToString();
                u.Names = dr["Names"].ToString();
                u.LastName = dr["LastName"].ToString();

                string status = string.Empty;
                if (uEmail != null)
                {

                    try
                    {
                        status = UserCreationTOGMS(u, securityToken.securityToken.sConnStr);

                        string createdOrExist = string.Empty;
                        if (status.Contains("User Creation Failed"))
                        {
                            cntExist++;
                            createdOrExist = "already exists on GMS,Total: " + cntExist;



                        }
                        else if (status.Contains("OK"))
                        {
                            cntCreated++;
                            createdOrExist = "created on GMS,Total: " + cntCreated;


                        }

                        string message = "User: " + u.Email + " " + createdOrExist;

                        System.IO.File.AppendAllText(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "\\Registers\\gms.dat", "\r\n Migration User \r\n" + message + "\r\n");

                        BInsert = true;

                    }
                    catch (Exception ex)
                    {
                        System.IO.File.AppendAllText(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "\\Registers\\gms.dat", "\r\n Migration User \r\n" + ex.Message + "\r\n" + ex.InnerException);
                    }
                }

                ErrorServer = status;


            }


            string messsag = cntCreated + " out of " + TotalUsers + "has been created";

            BaseModel bm = new BaseModel();
            bm.data = BInsert;
            bm.errorMessage = messsag + "Server :" + sServerName;

            return bm;
        }

    }
}