﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using NaveoOneLib.Models;
using NaveoService.Models;
using NaveoOneLib.Models.GPS;

namespace NaveoService.Services
{
    public class LiveService
    {
        //public List<GPSData> GetLive(int UID, List<String> lAssets)
        //{
        //    //List<GPSData> l = new GPSData().GetLatestGPSData(UID, lAssets, String.Empty);
        //    return l;
        //}

        #region Methods
        /// <summary>
        /// Get Live data
        /// </summary>
        /// <param name="sUserToken"></param>
        /// <param name="lAssetIDs"></param>
        /// <param name="sConnStr"></param>
        /// <returns></returns>
        public List<GPSData> GetLive(Guid sUserToken, List<int> lAssetIDs, List<String> lType,Boolean byDriver,String sConnStr)
        {
            int UID = 0;

            List<int> lValidatedIds;

            if (byDriver)
            {
                lValidatedIds = CommonService.lValidateDrivers(sUserToken, lAssetIDs, sConnStr, out UID);
                lValidatedIds.RemoveAll(item => item == 1);
            }
            else
                lValidatedIds = CommonService.lValidateAssets(sUserToken, lAssetIDs, sConnStr, out UID);

            if (lValidatedIds.Count == 0)
                return new List<GPSData>();

            List<GPSData> lData = new NaveoOneLib.Services.GPS.GPSDataService().GetLatestGPSData(lValidatedIds, lType, byDriver, UID, true, sConnStr);
            return lData;
        }
        #endregion
    }
}