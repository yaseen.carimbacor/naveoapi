﻿using NaveoOneLib.Models.GPS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace NaveoService.Services
{
    public class GPSDataService
    {
        #region Methods
        /// <summary>
        /// Get Debug Data
        /// </summary>
        /// <param name="sUserToken"></param>
        /// <param name="lAssetIDs"></param>
        /// <param name="dtFr"></param>
        /// <param name="dtTo"></param>
        /// <param name="sConnStr"></param>
        /// <returns></returns>
        public Models.DebugData GetDebugData(Guid sUserToken, List<int> lAssetIDs, DateTime dtFr, DateTime dtTo, List<String> lTypeID, String sConnStr, int? Page, int? LimitPerPage)
        {
            int UID = -1;
            List<int> lValidatedAssets = CommonService.lValidateAssets(sUserToken, lAssetIDs, sConnStr, out UID);
            NaveoOneLib.Services.GPS.GPSDataService gpsDataService = new NaveoOneLib.Services.GPS.GPSDataService();

            DataTable dbugData = gpsDataService.GetDebugData(lValidatedAssets, dtFr, dtTo, String.Empty, null, null, lTypeID, sConnStr, Page, LimitPerPage);

            Models.DebugData dd = new Models.DebugData();
            dd.dtData = dbugData;

            int cnt = gpsDataService.GetDebugDataCnt(lValidatedAssets, dtFr, dtTo, String.Empty, null, null, lTypeID, sConnStr);
            dd.rowCount = cnt;
            return dd;
        }

        /// <summary>
        /// Get Debug Data
        /// </summary>
        /// <param name="sUserToken"></param>
        /// <param name="lAssetIDs"></param>
        /// <param name="dtFr"></param>
        /// <param name="dtTo"></param>
        /// <param name="sConnStr"></param>
        /// <Author>Yaseen</Author>
        /// <returns></returns>
        public Models.DebugData GetAuxData(Guid sUserToken, List<int> lAssetIDs, DateTime dtFr, DateTime dtTo, List<String> lTypeID, String sConnStr, int? Page, int? LimitPerPage)
        {
            int UID = -1;
            List<int> lValidatedAssets = CommonService.lValidateAssets(sUserToken, lAssetIDs, sConnStr, out UID);

            NaveoOneLib.Services.GPS.GPSDataService gpsDataService = new NaveoOneLib.Services.GPS.GPSDataService();
            DataTable dbugData = gpsDataService.GetAuxData(UID, lValidatedAssets, dtFr, dtTo, String.Empty, null, null, lTypeID, sConnStr, Page, LimitPerPage);

            Models.DebugData dd = new Models.DebugData();
            dd.dtData = dbugData;

            int cnt = gpsDataService.GetDebugDataCnt(lValidatedAssets, dtFr, dtTo, String.Empty, null, null, lTypeID, sConnStr);
            dd.rowCount = cnt;
            return dd;
        }
        #endregion
    }
}