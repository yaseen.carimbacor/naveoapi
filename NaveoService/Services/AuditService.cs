﻿using NaveoOneLib.Models;
using NaveoOneLib.Models.Audits;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Services.Audits;
using NaveoOneLib.Services.GMatrix;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Web;

namespace NaveoService.Services
{
    public class AuditService
    {
        public DataTable GetLastLoginInfo(string userName)
        {
            return new AuditLoginService().GetLastLoginInfo(userName);
        }

        public bool SaveAuditLogin(ExpandoObject obj)
        {
            var values = (IDictionary<string, object>)obj;

            AuditLogin audit = new AuditLogin()
            {
                AuditType = values["AuditType"].ToString(),
                AuditUser = values["AuditUser"].ToString()
            };

            return (true == new AuditLoginService().SaveAuditLoginService(audit)) ? true : false;
        }

        public Boolean bSaveAuditLogin(int UID, String sType, String HostAddress, String HostName, String sConnStr)
        {
            //NaveoDBFirstEntities dbfirstcontext = new NaveoDBFirstEntities(sConnStr);
            //GFI_SYS_LoginAudit LoginAudit = new GFI_SYS_LoginAudit();

            //LoginAudit.UserID = UID;
            //LoginAudit.auditType = sType;
            //LoginAudit.clientGroupMachine = HostAddress;
            //LoginAudit.machineName = HostName;
            //LoginAudit.PostedDate = DateTime.Now;
            //LoginAudit.auditDate = DateTime.Now;
            Boolean bResult = false;
            try
            {
                //dbfirstcontext.GFI_SYS_LoginAudit.Add(LoginAudit);
                //dbfirstcontext.SaveChanges();
                bResult = true;
            }
            catch { }

            return bResult;
        }

        /// <summary>
        /// Overall Trace
        /// </summary>
        /// <param name="userToken"></param>
        /// <param name="controller"></param>
        /// <param name="action"></param>
        /// <param name="isAccessGranted"></param>
        /// <param name="connStr"></param>
        /// <returns></returns>
        public BaseModel Trace(AuditData sAudit, string connStr, Boolean bInsert)
        {
            return new NaveoOneLib.Services.Audits.AuditService().Trace(sAudit, connStr, bInsert);
        }

        /// <summary>
        /// Login Trace
        /// </summary>
        /// <param name="userToken"></param>
        /// <param name="controller"></param>
        /// <param name="action"></param>
        /// <param name="isAccessGranted"></param>
        /// <param name="connStr"></param>
        /// <returns></returnss
        public bool Trace(string email, string sAction, bool isAccessGranted, String sConnStr)
        {
            return new NaveoOneLib.Services.Audits.AuditService().Trace(email, sAction, isAccessGranted, sConnStr);
        }


        public dtData GetAuditData(Guid sUserToken, DateTime dtFrom,DateTime dtTo,string sConnStr, int? Page, int? LimitPerPage, List<String> sortColumns, Boolean sortOrderAsc, List<int> userIds, Boolean filter = false, String type = "")
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);
            DataTable dtAssetData = new NaveoOneLib.Services.Audits.AuditTraceService().GetAuditData(dtFrom,dtTo,lm, sConnStr, sortColumns, sortOrderAsc, userIds, filter, type);


            if (dtAssetData.Columns.Contains("audittraceid"))
                dtAssetData.Columns["audittraceid"].ColumnName = "AuditTraceId";

            if (dtAssetData.Columns.Contains("names"))
                dtAssetData.Columns["names"].ColumnName = "Names";

            if (dtAssetData.Columns.Contains("module"))
                dtAssetData.Columns["module"].ColumnName = "Module";

            if (dtAssetData.Columns.Contains("action"))
                dtAssetData.Columns["action"].ColumnName = "Action";

            if (dtAssetData.Columns.Contains("accessedon"))
                dtAssetData.Columns["accessedon"].ColumnName = "AccessedOn";

            if (dtAssetData.Columns.Contains("rawrequest"))
                dtAssetData.Columns["rawrequest"].ColumnName = "RawRequest";

            if (dtAssetData.Columns.Contains("sid"))
                dtAssetData.Columns["sid"].ColumnName = "sID";

            DataTable ResultTable = dtAssetData.Clone();
            ResultTable.Columns.Add("rowNum", typeof(int));
            ResultTable.Columns["rowNum"].AutoIncrement = true;
            ResultTable.Columns["rowNum"].AutoIncrementSeed = 1;
            ResultTable.Columns["rowNum"].AutoIncrementStep = 1;
            foreach (DataRow dr in dtAssetData.Rows)
                     ResultTable.Rows.Add(dr.ItemArray);

            int iTotalRec = ResultTable.Rows.Count;
            Pagination p = Pagination.GetRowNums(Page, LimitPerPage);
            DataTable dt = new DataTable();
            DataRow[] drChk = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo);
            if (drChk != null && drChk.Length > 0)
                dt = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo).CopyToDataTable();
            else
                dt = ResultTable;

            if (dt.Columns.Contains("rolename"))
            {

            }


            dtData dtR = new dtData();
            dtR.Data = dt;
            dtR.Rowcnt = iTotalRec;

            return dtR;
        }

        public dtData GetDrilledAuditData(string controller, int id, string sConnStr, int? Page, int? LimitPerPage) {
            DataTable dtAssetData = new NaveoOneLib.Services.Audits.AuditTraceService().GetDrilledAuditById(controller, id, sConnStr);
            if (dtAssetData.Columns.Contains("audittraceid"))
                dtAssetData.Columns["audittraceid"].ColumnName = "AuditTraceId";

            if (dtAssetData.Columns.Contains("names"))
                dtAssetData.Columns["names"].ColumnName = "Names";

            if (dtAssetData.Columns.Contains("module"))
                dtAssetData.Columns["module"].ColumnName = "Module";

            if (dtAssetData.Columns.Contains("action"))
                dtAssetData.Columns["action"].ColumnName = "Action";

            if (dtAssetData.Columns.Contains("accessedon"))
                dtAssetData.Columns["accessedon"].ColumnName = "AccessedOn";

            if (dtAssetData.Columns.Contains("rawrequest"))
                dtAssetData.Columns["rawrequest"].ColumnName = "RawRequest";

            if (dtAssetData.Columns.Contains("sid"))
                dtAssetData.Columns["sid"].ColumnName = "sid";

            DataTable ResultTable = dtAssetData.Clone();
            ResultTable.Columns.Add("rowNum", typeof(int));
            ResultTable.Columns["rowNum"].AutoIncrement = true;
            ResultTable.Columns["rowNum"].AutoIncrementSeed = 1;
            ResultTable.Columns["rowNum"].AutoIncrementStep = 1;
            foreach (DataRow dr in dtAssetData.Rows)
                ResultTable.Rows.Add(dr.ItemArray);

            int iTotalRec = ResultTable.Rows.Count;
            Pagination p = Pagination.GetRowNums(Page, LimitPerPage);
            DataTable dt = new DataTable();
            DataRow[] drChk = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo);
            if (drChk != null && drChk.Length > 0)
                dt = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo).CopyToDataTable();
            else
                dt = ResultTable;

            if (dt.Columns.Contains("rolename"))
            {

            }


            dtData dtR = new dtData();
            dtR.Data = dt;
            dtR.Rowcnt = iTotalRec;

            return dtR;
        }

    }

}