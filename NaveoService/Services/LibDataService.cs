﻿using NaveoOneLib.Models;
using NaveoOneLib.WebSpecifics;
using NaveoService.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;

namespace NaveoService.Services
{
    public class LibDataService
    {
        public DataTable GetLookupValues(int UID, String sLookupType, string sConnStr)
        {
            LibData libdata = new LibData();
            DataTable dt = libdata.dtLookupValues(UID, sLookupType, sConnStr);

            return dt;
        }
        //public UserViewModel GetLookupValuesViewModel(int UID, List<String> lTypes, String sConnStr)
        //{
        //    ApplicationDbContext context = new ApplicationDbContext(sConnStr);

        //    DataTable dtZone = new ZoneService().GetZoneHeaders(UID, sConnStr);

        //    UserViewModel userviewmodel = new UserViewModel();
        //    foreach (String s in lTypes)
        //    {
        //        DataTable dtDpt = new LibDataService().GetLookupValues(UID, s, sConnStr);
        //        switch (s.ToUpper())
        //        {
        //            case "DEPARTMENT":
        //                //userviewmodel.DepartmentList = new SelectList(dtDpt.AsDataView(), "VID", "Name");
        //                break;

        //            case "INSTITUTION":
        //                //userviewmodel.PlnLkpValList = new SelectList(dtDpt.AsDataView(), "VID", "Name");
        //                break;
        //        }
        //    }

        //    return userviewmodel;
        //}
    }
}