﻿using Ionic.Zip;
using NaveoOneLib.Common;
using NaveoOneLib.Constant;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Models.Reportings;
using NaveoOneLib.Models.Users;
using NaveoOneLib.Services;
using NaveoOneLib.Services.Reportings;
using NaveoOneLib.Services.Rules;
using NaveoOneLib.Services.Users;
using NaveoService.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Services
{
    public class AutomaticReportingService
    {
        /*
    select * from GFI_FLT_AutoReportingConfig
        report criteria
        Additional Params not in GFI_FLT_AutoReportingConfigDetails

    select * from GFI_FLT_AutoReportingConfigDetails
        Assets\users

    TargetType > Web
    TargetID > null for AR, RuleID for notif
    ReportID > 110/111/112/113 > notif, 1 - 100 Auto Report
    TriggerTime
    TriggerInterval
    ReportPeriod > 1,2,3,4 yesterday, last week...
    ReportType > Excel
    GridXML > Params

    ReportID
        Auto Report will call Export memory Stream
            1.      Exceptions Report
            2.      Trip History Report
            3.      Live Report --V2
            4.      Auxillaries Report
            5.      Visited not visited Report
            6.      Device Health Report
            7.      Daily Summarized Trips Report --V2
            8.      Summarized Trips Report -- V2
            9.      Daily Trip Time  Report --V2
            10.     Asset Summary Outside HO Report

            11.     Customer Visit Report          
            12.     Trip Summary Report
            13.     Passenger Report
            14.     Zone Visited Report
            15.     Asset Summary Report
            16.     Specific Zone Visited Report
            17.     Maintenance Cost Report
            
            
            

        Notif
            110.    SMS Notification
            111.    Popup
            112.    Email Notification
            113.    Send Push Notification to Smartphone
            
*/

        Boolean IsReportSendAlready(int sARID, String sConnStr)
        {
            String sqlString = @"SELECT *
	                                    FROM GFI_SYS_Notification n, GFI_FLT_AutoReportingConfig o
                                    WHERE
	                                    n.ARID = o.ARID
	                                    and n.Status = 2 
	                                    and n.ARID = ?
	                                    and dateadd(dd, datediff(dd, 0, n.SendAt), 0) = dateadd(dd, datediff(dd, 0, GETDATE()), 0)";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, sARID.ToString(), sConnStr);
            if (dt.Rows.Count == 0)
                return false;
            return true;
        }
        private DataTable GenerateAndSendReport(AutoReportingConfig arc, DateTime dtFrom, DateTime dtTo, List<int> lAssets, List<int> lDrivers, List<User> lUsers, List<int> lZones, int ARID, List<int> lZoneTypes, List<int> lRules, String[] sOptions, String sConnStr)
        {
            Entity entity = new Models.Entity();
            SecurityToken securityToken = new SecurityToken();
            String sReport = String.Empty;

            securityToken.Page = null;
            securityToken.LimitPerPage = null;
            securityToken.sConnStr = sConnStr;
            securityToken.UserToken = lUsers[0].UserToken;

            #region Generate Report
            switch (arc.ReportID)
            {
                case 1: //Exceptions Report
                    #region Exceptions
                    sReport = "Exception Report";
                    entity.entityName = NaveoEntity.EXCEPTION_LIST;
                    entity.exception = new Exceptions();
                    entity.exception.assetIds = lAssets;
                    entity.exception.ruleIds = lRules;
                    entity.exception.dateFrom = dtFrom;
                    entity.exception.dateTo = dtTo;
                    break;
                #endregion

                case 2: //Trip History Report
                    #region Trip
                    sReport = "Trip History Report";
                    entity.entityName = NaveoEntity.TRIPS;
                    entity.trip = new Trips();
                    entity.trip.assetIds = lAssets;
                    entity.trip.dateFrom = dtFrom;
                    entity.trip.dateTo = dtTo;

                    foreach (String s in sOptions)
                    {
                        String[] tuple = s.Split(':');
                        foreach (String t in tuple)
                        {
                            if (t.ToString().Trim() == "iMinTripDistance")
                            {
                                entity.trip.iMinTripDistance = Convert.ToDouble(tuple[1].ToString().Trim());
                                break;
                            }
                        }
                    }
                    break;
                #endregion

                case 3: //Live Report
                    #region Live
                    sReport = "Live Report";
                    entity.entityName = NaveoEntity.LIVE;
                    entity.trip = new Trips();
                    entity.trip.assetIds = lAssets;
                    entity.trip.dateFrom = dtFrom;
                    entity.trip.dateTo = dtTo;
                    break;
                #endregion

                case 7: //Daily Summarized Trip Report
                    #region Daily Summary Report
                    sReport = "Daily Summarized Trip Report";
                    entity.entityName = NaveoEntity.DAILY_SUMMARIZED_TRIPS;
                    entity.trip = new Trips();
                    entity.trip.assetIds = lAssets;
                    entity.trip.dateFrom = dtFrom;
                    entity.trip.dateTo = dtTo;
                    break;
                #endregion

                case 8: //Summarized Trips Report
                    #region Summarized Trips
                    sReport = "Summarized Trip Report";
                    entity.entityName = NaveoEntity.SUMMARIZED_TRIPS;
                    entity.trip = new Trips();
                    entity.trip.assetIds = lAssets;
                    entity.trip.dateFrom = dtFrom;
                    entity.trip.dateTo = dtTo;
                    break;
                #endregion

                case 9: //Daily Trip Time Report
                    #region Daily Trip Time Report
                    sReport = "Daily Trip Time Report";
                    entity.entityName = NaveoEntity.DAILY_TRIP_TIME;
                    entity.trip = new Trips();
                    entity.trip.assetIds = lAssets;
                    entity.trip.dateFrom = dtFrom;
                    entity.trip.dateTo = dtTo;

                    foreach (String s in sOptions)
                    {
                        String[] tuple = s.Split(':');
                        foreach (String t in tuple)
                        {
                            if (t.ToString().Trim() == "Total Type")
                            {
                                entity.trip.sTotal = tuple[1].ToString().Trim();
                                break;
                            }
                        }
                    }
                    break;
                #endregion

                case 10://Asset Summary Outside HO Report
                    #region Asset Summary Outside HO Report
                    int iHOZone = 0;
                    if (lZones.Count != 0)
                        iHOZone = lZones[0];

                    sReport = "Asset Summary Outside HO Report";
                    entity.entityName = NaveoEntity.ASSET_SUMMARY_OUTSIDE_HO;
                    entity.trip = new Trips();
                    entity.trip.assetIds = lAssets;
                    entity.trip.iHOZone = iHOZone;
                    entity.trip.zoneTypes = lZoneTypes;
                    entity.trip.dateFrom = dtFrom;
                    entity.trip.dateTo = dtTo;
                    break;
                #endregion

                case 14://Zone Visited Report
                    #region Zone Visited Report

                    string sType = string.Empty;
                    List<int> zIDS = new List<int>();

                    if (lZones.Count >= 0)
                    {
                        sType = "Zones";
                        zIDS = lZones;
                    }
                    else if (lZoneTypes.Count >= 0)
                    {
                        sType = "ZoneTypes";
                        zIDS = lZoneTypes;
                    }

                    sReport = "Zone Visited Report";
                    entity.entityName = NaveoEntity.Zone_Visited_NotVisited;
                    entity.zoneVisited = new ZoneVisitedReport();
                    entity.zoneVisited.assetIds = lAssets;
                    entity.zoneVisited.sType = sType;
                    entity.zoneVisited.zIds = zIDS;
                    entity.zoneVisited.dateFrom = dtFrom;
                    entity.zoneVisited.dateTo = dtTo;

                    break;
                #endregion

                case 17: //Maintenace Cost Report
                    sReport = "Maintenance Cost Report";
                    entity.entityName = NaveoEntity.MaintenanceCostReport;
                    entity.maintenanceAndFuelCost = new MaintenanceAndFuelCost();
                    entity.maintenanceAndFuelCost.assetIds = lAssets;//new List<int>() { 66, 125, 20 };
                    entity.maintenanceAndFuelCost.Description = sOptions.ToList();//new List<string>() { "FuelUsage", "SERVICING - 10000 KM OR 6 MONTHS", "SERVICING - 5000 KM" };
                    entity.maintenanceAndFuelCost.DateTimeStart = dtFrom;//new DateTime(2019, 01, 01, 20, 20, 20);
                    entity.maintenanceAndFuelCost.DateTimeEnd = dtTo;//new DateTime(2020, 07, 05, 20, 20, 20);


                    break;

            }

            DataTable dtNotification = NotificationService.NotificationDT();
            if (String.IsNullOrEmpty(sReport))
                return dtNotification;

            MemoryStream ms = ExportDataService.GenerateExcelStreamToExport(entity, securityToken);
            #endregion

            #region Send Report
            if (ms.Length > 0)
            {
                MailSender mailSender = new MailSender();
                String myDir = Globals.ExportTempPath(Guid.NewGuid().ToString());
                String myReport = myDir + sReport + ".xlsx";
                String sZipFN = myReport.Replace(".xlsx", ".zip");

                File.WriteAllBytes(myReport, ms.ToArray());

                ZipFile zip = new ZipFile();
                zip.AddFile(myReport, String.Empty);
                zip.Save(sZipFN);
                zip.Name = sReport.Trim();

                String sTitle = sReport;
                String sBody = @"
Dear Valued Customer,

Please find attached the scheduled report " + arc.ReportFormat + @".
Kindly review and contact Naveo Support on (230) 260-6060 or support@naveo.mu if any issues.

Thanks and regards,
Naveo Support Team


Confidentiality Statement: This email and file(s) transmitted with are confidential and intended solely for the use of the individual or entity to whom they are addressed.  
If you are not the intended recipient, you are requested to delete this email including any attachment(s) and are hereby notified that any disclosure, copying or distribution, or the taking of any action 
based on the contents of this email is strictly prohibited.";

                foreach (User u in lUsers)
                {
                    if (u.Email.ToString() != String.Empty)
                    {
                        DataRow r = dtNotification.NewRow();

                        r["UIDCategory"] = "";
                        r["ReportID"] = arc.ReportID;
                        r["Lat"] = "";
                        r["Lon"] = "";
                        r["StartLat"] = null;
                        r["StartLon"] = null;
                        r["Asset"] = "";
                        r["Driver"] = "";
                        r["AssetID"] = null;
                        r["DriverID"] = null;
                        r["RuleName"] = "";
                        r["Email"] = u.Email.ToString();
                        r["DateTimeGPS_UTC"] = null;
                        r["ARID"] = ARID;
                        r["ExceptionID"] = null;
                        r["UID"] = u.UID.ToString();
                        r["Type"] = "AutoR";
                        if (arc.SaveToDisk == 1)
                        {
                            r["Status"] = 2;
                            r["SendAt"] = DateTime.Now.ToString();
                            r["sBody"] = "Saved to disk";
                            try
                            {
                                FtpClient.FTPServerCredentials cred = (FtpClient.FTPServerCredentials)Newtonsoft.Json.JsonConvert.DeserializeObject(arc.GridXML, typeof(FtpClient.FTPServerCredentials));
                                //cred.password = NaveoOneLib.Services.BaseService.DecryptMe(cred.password);
                                string sPassword = cred.password;

                                if(sPassword != string.Empty)
                                        cred.password = NaveoOneLib.Services.BaseService.RSADecrypt(sPassword);

                                FtpClient ftpClient = new FtpClient(cred.FTPAddress, cred.user, cred.password, cred.enableSSL);
                                String s = ftpClient.upload(cred.FileDir + sReport + ".xlsx", myReport);
                                r["Err"] = s;

                                //String sDestinationFN = arc.ReportPath;
                                //if (!Path.HasExtension(sDestinationFN))
                                //    sDestinationFN = arc.ReportPath + "\\" + Guid.NewGuid();

                                //File.Copy(myReport, sDestinationFN);
                            }
                            catch (Exception ex)
                            {
                                r["Status"] = 0;
                                r["SendAt"] = null;
                                r["Err"] = ex.ToString();
                                r["sBody"] = "Not Saved to Disk" + arc.ReportPath;
                            }
                        }
                        else
                        {
                            String s = mailSender.SendMail(u.Email.ToString(), sTitle, sBody, sZipFN, sConnStr);
                            if (s == "Mail Sent")
                            {
                                r["Status"] = 2;
                                r["SendAt"] = DateTime.Now.ToString();
                            }
                            else
                            {
                                r["Status"] = 0;
                                r["SendAt"] = null;
                            }

                            r["Err"] = s;
                            r["sBody"] = sBody;
                            r["Title"] = sTitle;
                        }
                        dtNotification.Rows.Add(r);
                    }
                }

                if (Globals.IsFileAvailable(myReport))
                    File.Delete(myReport);
                if (Globals.IsFileAvailable(sZipFN))
                    File.Delete(sZipFN);
                if ((System.IO.Directory.Exists(myDir)))
                    Directory.Delete(myDir, true);
            }
            else //No data detected
            {
                DataRow r = dtNotification.NewRow();

                r["UIDCategory"] = "";
                r["ReportID"] = arc.ReportID;
                r["Lat"] = "";
                r["Lon"] = "";
                r["StartLat"] = null;
                r["StartLon"] = null;
                r["Asset"] = "";
                r["Driver"] = "";
                r["AssetID"] = null;
                r["DriverID"] = null;
                r["RuleName"] = "";
                r["Email"] = "NotSent@naveo.mu";
                r["DateTimeGPS_UTC"] = null;
                r["ARID"] = ARID;
                r["ExceptionID"] = null;
                r["UID"] = null;
                r["Type"] = "AutoR";

                r["Status"] = 2;
                r["SendAt"] = DateTime.Now.ToString();
                r["Err"] = "No Data found for selected criteria";
                r["sBody"] = "No Data found for selected criteria";
                dtNotification.Rows.Add(r);
            }
            #endregion

            return dtNotification;
        }

        public String ProcessAutomaticReports(String sConnStr)
        {
            String sResult = "Processing Automatic Reports. ";
            DataTable dtNotification = NotificationService.NotificationDT();
            List<AutoReportingConfig> lARC = new NaveoOneLib.Services.Reportings.AutoReportingConfigService().GetAutoReportingConfigList(sConnStr);

            if (lARC == null)
                return sResult + "No Automatic Report Data detected";

            foreach (AutoReportingConfig arc in lARC)
            {
               
                try
                {
                    if (arc.TargetType != "V2")
                        continue;

                    List<int> lAssets = new List<int>();
                    List<int> lDrivers = new List<int>();
                    List<User> lUsers = new List<User>();
                    List<int> lZones = new List<int>();
                    List<int> lZoneTypes = new List<int>();
                    List<int> lRules = new List<int>();
                    String[] sOptions = arc.GridXML.Split(',');

                    String sFrom = String.Empty;
                    String sTo = String.Empty;

                    List<AutoReportingConfigDetail> lARCD = new List<AutoReportingConfigDetail>();
                    AutoReportingConfigDetail tempARCD = new AutoReportingConfigDetail();
                    lARCD = new AutoReportingConfigDetailsService().GetAutoReportingConfigDetailsByIdList(arc.ARID.ToString(), sConnStr);

                    if (IsReportSendAlready(arc.ARID, sConnStr))
                        continue;

                    //Both lines commented for test
                    //Checking if its time to process automatic reporting
                    TimeSpan ts = Convert.ToDateTime(arc.TriggerTime.ToString()).TimeOfDay;
                    if (System.DateTime.Now.TimeOfDay >= ts && System.DateTime.Now.TimeOfDay <= ts.Add(new TimeSpan(0, 30, 0)))
                    {
                        switch (arc.ReportPeriod)
                        {
                            case 1: // Yesterday
                                sFrom = DateTime.Now.AddDays(-1).ToString("dd/MMM/yyyy 00:00:01");
                                sTo = DateTime.Now.AddDays(-1).AddSeconds(-1).ToString("dd/MMM/yyyy 23:59:59");
                                break;

                            case 2: // Last Week
                                sFrom = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek - 6).ToString("dd/MMM/yyyy 00:00:01");
                                sTo = DateTime.Now.AddDays(-(int)DateTime.Now.DayOfWeek).ToString("dd/MMM/yyyy 23:59:59");
                                break;

                            case 3: // Last Month
                                var month = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                                sFrom = month.AddMonths(-1).ToString("dd/MMM/yyyy 00:00:01");
                                sTo = month.AddDays(-1).ToString("dd/MMM/yyyy 23:59:59");
                                break;
                        }

                        DateTime dtFrom = Convert.ToDateTime(sFrom).ToUniversalTime();
                        DateTime dtTo = Convert.ToDateTime(sTo).ToUniversalTime();

                        /// Populate all the required lists
                        if (lARCD != null)
                        {
                            NaveoOneLib.Services.Users.UserService userService = new NaveoOneLib.Services.Users.UserService();
                            NaveoOneLib.Services.GMatrix.GroupMatrixService groupMatrixService = new NaveoOneLib.Services.GMatrix.GroupMatrixService();
                            #region UIDType
                            foreach (AutoReportingConfigDetail loopARCD in lARCD)
                            {
                                switch (loopARCD.UIDType.ToString())
                                {
                                    case "Assets":
                                        if (loopARCD.UIDCategory == "GMID")
                                        {
                                            List<int> lGMID = new List<int>();
                                            lGMID.Add(loopARCD.UID);

                                            DataTable dt = groupMatrixService.GetAllGMValues(lGMID, NaveoModels.Assets, sConnStr);

                                            lAssets.Clear();
                                            DataRow[] dr = dt.Select("GMID < 0");
                                            foreach (DataRow dri in dr)
                                                lAssets.Add(Convert.ToInt32(dri["StrucID"].ToString()));
                                        }
                                        else
                                            lAssets.Add(loopARCD.UID);
                                        break;

                                    case "Drivers":
                                        if (loopARCD.UIDCategory == "GMID")
                                        {
                                            List<int> lGMID = new List<int>();
                                            lGMID.Add(loopARCD.UID);
                                            DataTable dt = groupMatrixService.GetAllGMValues(lGMID, NaveoModels.Drivers, sConnStr);

                                            lDrivers.Clear();
                                            DataRow[] dr = dt.Select("GMID < 0");
                                            foreach (DataRow dri in dr)
                                                lDrivers.Add(Convert.ToInt32(dri["StrucID"].ToString()));
                                        }
                                        else
                                            lDrivers.Add(loopARCD.UID);
                                        break;

                                    case "Users":
                                        if (loopARCD.UIDCategory == "GMID")
                                        {
                                            List<int> lGMID = new List<int>();
                                            lGMID.Add(loopARCD.UID);
                                            DataTable dt = groupMatrixService.GetAllGMValues(lGMID, NaveoModels.Users, sConnStr);

                                            List<int> li = new List<int>();
                                            li.Add(Convert.ToInt32(loopARCD.UID));
                                            List<User> lu = userService.lGetUsers(li, sConnStr);
                                            foreach (User u in lu)
                                                lUsers.Add(u);
                                        }
                                        else
                                            lUsers.Add(userService.GetUserById(loopARCD.UID, User.EditModePswd, sConnStr));
                                        break;

                                    case "Zones":
                                        if (loopARCD.UIDCategory == "GMID")
                                        {
                                            List<int> lGMID = new List<int>();
                                            lGMID.Add(loopARCD.UID);
                                            DataTable dt = groupMatrixService.GetAllGMValues(lGMID, NaveoModels.Zones, sConnStr);

                                            lZones.Clear();
                                            DataRow[] dr = dt.Select("GMID < 0");
                                            foreach (DataRow dri in dr)
                                                lZones.Add(Convert.ToInt32(dri["StrucID"].ToString()));
                                        }
                                        else
                                            lZones.Add(loopARCD.UID);
                                        break;

                                    case "ZoneTypes":
                                        if (loopARCD.UIDCategory == "GMID")
                                        {
                                            List<int> lGMID = new List<int>();
                                            lGMID.Add(loopARCD.UID);
                                            DataTable dt = groupMatrixService.GetAllGMValues(lGMID, NaveoModels.ZoneTypes, sConnStr);

                                            lZoneTypes.Clear();
                                            DataRow[] dr = dt.Select("GMID < 0");
                                            foreach (DataRow dri in dr)
                                                lZoneTypes.Add(Convert.ToInt32(dri["StrucID"].ToString()));
                                        }
                                        else
                                            lZoneTypes.Add(loopARCD.UID);
                                        break;


                                    case "Rules":
                                        if (loopARCD.UIDCategory == "GMID")
                                        {
                                            List<int> lGMID = new List<int>();
                                            lGMID.Add(loopARCD.UID);
                                            DataTable dt = groupMatrixService.GetAllGMValues(lGMID, NaveoModels.Rules, sConnStr);

                                            lRules.Clear();
                                            DataRow[] dr = dt.Select("GMID < 0");
                                            foreach (DataRow dri in dr)
                                                lRules.Add(Convert.ToInt32(dri["StrucID"].ToString()));
                                        }
                                        else
                                            lRules.Add(loopARCD.UID);
                                        break;
                                }
                            }
                            #endregion

                            DataTable dtNotif = new DataTable();
                            #region Generate report
                            switch (arc.ReportPeriod)
                            {
                                case 1: // Yesterday
                                    dtNotif = GenerateAndSendReport(arc, dtFrom, dtTo, lAssets, lDrivers, lUsers, lZones, arc.ARID, lZoneTypes, lRules, sOptions, sConnStr);
                                    break;

                                case 2: // Last Week
                                    if (System.DateTime.Now.DayOfWeek.ToString() == "Monday")
                                        dtNotif = GenerateAndSendReport(arc, dtFrom, dtTo, lAssets, lDrivers, lUsers, lZones, arc.ARID, lZoneTypes, lRules, sOptions, sConnStr);
                                    break;

                                case 3: // Last Month
                                        //First day of the month
                                    DateTime FirstDayOfMonth = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                                    if (FirstDayOfMonth.Date == System.DateTime.Now.Date)
                                        dtNotif = GenerateAndSendReport(arc, dtFrom, dtTo, lAssets, lDrivers, lUsers, lZones, arc.ARID, lZoneTypes, lRules, sOptions, sConnStr);
                                    break;
                            }

                            if (dtNotif.Rows.Count > 0)
                                dtNotification.Merge(dtNotif);
                            #endregion
                        }
                    }
                }
                catch (Exception ex)
                {
                    Errors.WriteToErrorLog(ex, " \r\n ****** ARID : " + arc.ARID.ToString());
                }
            }

            Boolean b = new NotificationService().SaveNotification(dtNotification, sConnStr);
            sResult += "Automatic Reporting is Successful " + b;
            return sResult;
        }
    }
}
