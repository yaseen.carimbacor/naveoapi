﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NaveoOneLib.Models.Common;
using NaveoOneLib.DBCon;
using System.Data;
using NaveoOneLib.Models.Users;
using System.Data.SqlTypes;
using NaveoOneLib.Services.Rules;

namespace NaveoService.Services
{
    public class MaintAssetServicesZ
    {
        DataTable CustomerDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "Customer";
            dt.Columns.Add("Id", typeof(int));
            dt.Columns.Add("Type", typeof(int));
            dt.Columns.Add("CustomerId", typeof(int));
            dt.Columns.Add("CustomerRef", typeof(String));
            dt.Columns.Add("CustomerFullName", typeof(String));
            dt.Columns.Add("CustomerTelephone", typeof(String));
            dt.Columns.Add("CustomerAddress", typeof(String));
            dt.Columns.Add("CustomerJDECode", typeof(String));
            dt.Columns.Add("CustomerVATNo", typeof(String));
            dt.Columns.Add("CustomerBRN", typeof(String));
            dt.Columns.Add("CustomerEmailAddress", typeof(String));
            dt.Columns.Add("CustomerFax", typeof(String));
            dt.Columns.Add("CustomerMarkUpCategory", typeof(int));
            dt.Columns.Add("UseMarkup", typeof(int));
            dt.Columns.Add("Department", typeof(String));
            dt.Columns.Add("CreatedBy", typeof(String));
            dt.Columns.Add("DateCreated", typeof(DateTime));
            dt.Columns.Add("UpdatedBy", typeof(String));
            dt.Columns.Add("DateUpdated", typeof(DateTime));
            dt.Columns.Add("IsFlagged", typeof(int));
            dt.Columns.Add("oprType", typeof(DataRowState));

            return dt;
        }

        DataTable CustomerVehicleDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "CustomerVehicle";
            dt.Columns.Add("Id", typeof(int));
            dt.Columns.Add("CustomerId", typeof(int));
            dt.Columns.Add("Number", typeof(String));
            dt.Columns.Add("OriginalNumber", typeof(String));
            dt.Columns.Add("Make", typeof(int));
            dt.Columns.Add("Model", typeof(int));
            dt.Columns.Add("CustomerName", typeof(String));
            dt.Columns.Add("FuelType", typeof(int));
            dt.Columns.Add("ChassisNo", typeof(String));
            dt.Columns.Add("Category", typeof(int));
            dt.Columns.Add("Status", typeof(int));
            dt.Columns.Add("RoadTaxExpiryDate", typeof(DateTime));
            dt.Columns.Add("PSVLicenseExpiryDate", typeof(DateTime));
            dt.Columns.Add("FitnessExpiryDate", typeof(DateTime));
            dt.Columns.Add("RoadTaxAmount", typeof(double));
            dt.Columns.Add("FitnessAmount", typeof(double));
            dt.Columns.Add("PSVAmount", typeof(double));
            dt.Columns.Add("CreatedBy", typeof(DateTime));
            dt.Columns.Add("DateCreated", typeof(DateTime));
            dt.Columns.Add("UpdatedBy", typeof(String));
            dt.Columns.Add("DateUpdated", typeof(DateTime));
            dt.Columns.Add("IsFlagged", typeof(int));
            dt.Columns.Add("FleetServerId", typeof(int));
            dt.Columns.Add("FMSAssetId", typeof(int));
            dt.Columns.Add("FMSAssetType", typeof(int));
            dt.Columns.Add("Description", typeof(String));
            dt.Columns.Add("Section", typeof(int));
            dt.Columns.Add("VehicleType", typeof(int));
            dt.Columns.Add("Colour", typeof(String));
            dt.Columns.Add("SupplierId", typeof(int));
            dt.Columns.Add("YearManufactured", typeof(int));
            dt.Columns.Add("PurchasedDate", typeof(DateTime));
            dt.Columns.Add("RegistrationDate", typeof(DateTime));
            dt.Columns.Add("PurchasedValue", typeof(double));
            dt.Columns.Add("ExpectedLifetime", typeof(int));
            dt.Columns.Add("ResidualValue", typeof(String));
            dt.Columns.Add("Reference", typeof(String));
            dt.Columns.Add("IsInService", typeof(int));
            dt.Columns.Add("EngineSerialNumber", typeof(String));
            dt.Columns.Add("EngineType", typeof(String));
            dt.Columns.Add("EngineCapacity", typeof(String));
            dt.Columns.Add("EnginePower", typeof(String));
            dt.Columns.Add("StandardConsumption", typeof(String));
            dt.Columns.Add("Department", typeof(String));
            dt.Columns.Add("Remarks", typeof(String));
            dt.Columns.Add("oprType", typeof(DataRowState));

            return dt;
        }

        public string getServerName()
        {

            string sServerName = System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            User.NaveoDatabases myEnum;
            //Getting only the enum part
            sServerName = sServerName.Replace(".naveo.mu", String.Empty);
            //sServerName = sServerName.Replace(":44393", String.Empty);
            sServerName = sServerName.Replace("https://", String.Empty);
            sServerName = sServerName.Replace("http://", String.Empty);
            if (Enum.TryParse(sServerName, true, out myEnum))
                sServerName = sServerName;
            else
                sServerName = sServerName;



            sServerName = sServerName.ToUpper();


            return sServerName;
        }

        public BaseModel SaveFixedAssetMaint(NaveoOneLib.Models.Assets.FixedAsset fixedAsset, int uLogin, Boolean bInsert, String sConnStr)
        {
            BaseModel baseModel = new BaseModel();
            Boolean bResult = false;
            baseModel.data = bResult;

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            DataSet dsProcess = new DataSet();
            DataTable dt = CustomerDT();

            //Table FleetServers
            //sServerName                
            string sServerName = getServerName();
            string serverId = string.Empty;
            string customerId = string.Empty;

            //Get Server Id

            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sqlGetServerId = "select * from FleetServers where Name = '" + sServerName + "'";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sqlGetServerId;
            drSql["sTableName"] = "dt";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, "MaintDB");
            DataTable dtGM = ds.Tables[0];

            if (dtGM.Rows.Count > 0)
            {
                DataRow row = dtGM.Rows[0];

                serverId = row["Id"].ToString();
            }



            //check if customer exist
            String sqlGetCustomer = "select * from Customer where CustomerFullName = '" + fixedAsset.lMatrix[0].GMDesc + "'";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sqlGetServerId;
            drSql["sTableName"] = "dt";
            dtSql.Rows.Add(drSql);

            DataSet dsCustomer = c.GetDataDS(dtSql, "MaintDB");
            DataTable dtCustomer = ds.Tables[0];

            if (dtCustomer.Rows.Count > 0)
            {
                DataRow row = dtCustomer.Rows[0];

                customerId = row["Id"].ToString();


                if (int.TryParse(customerId, out int result))
                {
                    //Customer already exist
                }
                else
                {
                    //Customer table
                    DataRow drCustomer = dt.NewRow();
                    drCustomer["CustomerName"] = fixedAsset.lMatrix[0].GMDesc;
                    drCustomer["Type"] = 1;
                    drCustomer["CreatedBy"] = "Asset.Webservice";
                    drCustomer["DateCreated"] = DateTime.Now;
                    drCustomer["CustomerRef"] = "WEBSERV" + DateTime.Now.ToString().Replace("/", "").Replace(" ", "").Replace(":", "");
                    drCustomer["IsFlagged"] = 0;
                    drCustomer["CustomerMarkUpCategory"] = 0;
                    drCustomer["UseMarkup"] = 0;
                    drCustomer["oprType"] = DataRowState.Added;
                    dt.Rows.Add(drCustomer);

                    //Create  customer
                    /*
             customer = new Customer();
                    customer.CustomerFullName = newAsset.asset.CustomerName;
                    customer.Type = 1;
                    customer.CreatedBy = "Asset.Webservice";
                    customer.DateCreated = DateTime.Now;
                    customer.CustomerRef = "WEBSERV" + DateTime.Now.ToString().Replace("/", "").Replace(" ", "").Replace(":", "");
                    customer.IsFlagged = false;
                    customer.CustomerMarkUpCategory = 0;
                    customer.UseMarkup = false;
             */


                }
            }

            //check if customer exist
            String sqlGetCustomerVehicle = "select * from CustomerVehicle where FMSAssetId = '" + fixedAsset.lMatrix[0].GMDesc + "' AND FleetServerId  = '" + fixedAsset.lMatrix[0].GMDesc + "'";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sqlGetCustomerVehicle;
            drSql["sTableName"] = "dt";
            dtSql.Rows.Add(drSql);

            DataSet dsCustomerVehicle = c.GetDataDS(dtSql, "MaintDB");
            DataTable dtCustomerVehicle = ds.Tables[0];

            if (dtCustomer.Rows.Count > 0)
            {
                DataRow row = dtCustomerVehicle.Rows[0];

                string customerVehcileId = row["Id"].ToString();


                if (int.TryParse(customerVehcileId, out int result))
                {
                    //Customer already exist
                }
                else
                {
                    //Customer table
                    DataRow drCustomerVehicle = dtCustomerVehicle.NewRow();
                    drCustomerVehicle["CreatedBy"] = "Asset.Webservice";
                    drCustomerVehicle["DateCreated"] = DateTime.Now;
                    drCustomerVehicle["DateUpdated"] = (DateTime)SqlDateTime.MinValue;
                    drCustomerVehicle["Category"] = fixedAsset.AssetCategory;
                    drCustomerVehicle["ChassisNo"] = "";
                    drCustomerVehicle["Colour"] = "";
                    drCustomerVehicle["CustomerId"] = customerId;
                    drCustomerVehicle["CustomerName"] = fixedAsset.AssetName;
                    drCustomerVehicle["Department"] = 0;
                    drCustomerVehicle["Description"] = fixedAsset.Descript;
                    drCustomerVehicle["EngineCapacity"] = 0;
                    drCustomerVehicle["EnginePower"] = 0;
                    drCustomerVehicle["EngineSerialNumber"] = "";
                    drCustomerVehicle["EngineType"] = 0;
                    drCustomerVehicle["ExpectedLifetime"] = 0;
                    drCustomerVehicle["FitnessAmount"] = 0;
                    drCustomerVehicle["FitnessExpiryDate"] = (DateTime)SqlDateTime.MinValue;
                    drCustomerVehicle["FleetServerId"] = 0;
                    drCustomerVehicle["FMSAssetId"] = fixedAsset.AssetID;
                    drCustomerVehicle["FMSAssetType"] = 0;
                    drCustomerVehicle["FuelType"] = 0;
                    drCustomerVehicle["IsFlagged"] = 0;
                    drCustomerVehicle["IsInService"] = 1;
                    drCustomerVehicle["Make"] = 9;
                    drCustomerVehicle["Model"] = 8;
                    drCustomerVehicle["Number"] = 0;
                    drCustomerVehicle["OriginalNumber"] = 0;
                    drCustomerVehicle["PSVAmount"] = 0;
                    drCustomerVehicle["PSVLicenseExpiryDate"] = (DateTime)SqlDateTime.MinValue;
                    drCustomerVehicle["PurchasedDate"] = (DateTime)SqlDateTime.MinValue;
                    drCustomerVehicle["PurchasedValue"] = 0;
                    drCustomerVehicle["Reference"] = 0;
                    drCustomerVehicle["RegistrationDate"] = (DateTime)SqlDateTime.MinValue;
                    drCustomerVehicle["Remarks"] = "FixedAsset";
                    drCustomerVehicle["ResidualValue"] = 0;
                    drCustomerVehicle["RoadTaxAmount"] = 0;
                    drCustomerVehicle["RoadTaxExpiryDate"] = (DateTime)SqlDateTime.MinValue;
                    drCustomerVehicle["Section"] = 0;
                    drCustomerVehicle["StandardConsumption"] = 0;
                    drCustomerVehicle["Status"] = 0;
                    drCustomerVehicle["SupplierId"] = 0;
                    drCustomerVehicle["VehicleType"] = 0;
                    drCustomerVehicle["YearManufactured"] = 0;
                    drCustomerVehicle["oprType"] = DataRowState.Added;
                    dt.Rows.Add(drCustomerVehicle);

                    //Create  customer


                }
            }

            //IF not create customer

            //check if assetid belong to a customer
            //If 
            /*
                        
             */

            //call gms api
            //Add Value to gms_customer
            //Check if asset exit in customervehicle
            //else insert into customer vehicle
            //fixedasset
            //fa.lMatrix[0].GMDesc

            if (bInsert)
            {
                fixedAsset.oprType = DataRowState.Added;
                fixedAsset.dtCreated = DateTime.UtcNow;
            }
            //if (fixedAsset.AssetCategory.HasValue)
            //    if (fixedAsset.AssetCategory.Value > NaveoEntity.FixedAssetGroup)
            //        fixedAsset.AssetCategory = fixedAsset.AssetCategory - NaveoEntity.FixedAssetGroup;

            //Customer table
            DataRow dr = dt.NewRow();
            dr["AssetID"] = fixedAsset.AssetID;
            dr["AssetCode"] = fixedAsset.AssetCode;
            dr["dtCreated"] = Convert.ToDateTime(fixedAsset.dtCreated);
            dr["AssetType"] = fixedAsset.AssetType;
            dr["AssetName"] = fixedAsset.AssetName != null ? fixedAsset.AssetName : (object)DBNull.Value;
            dr["descript"] = fixedAsset.Descript != null ? fixedAsset.Descript : (object)DBNull.Value;
            dr["RoadID"] = fixedAsset.RoadID != null ? fixedAsset.RoadID : (object)DBNull.Value;
            dr["LocalityID"] = fixedAsset.LocalityID != null ? fixedAsset.LocalityID : (object)DBNull.Value;
            dr["VillageID"] = fixedAsset.VillageID != null ? fixedAsset.VillageID : (object)DBNull.Value;
            dr["AssetCategory"] = fixedAsset.AssetCategory != null ? fixedAsset.AssetCategory : (object)DBNull.Value;
            dr["AssetFamily"] = fixedAsset.AssetFamily != null ? fixedAsset.AssetFamily : (object)DBNull.Value;
            dr["Owners"] = fixedAsset.Owners != null ? fixedAsset.Owners : (object)DBNull.Value;
            dr["Height"] = fixedAsset.Height != null ? fixedAsset.Height : (object)DBNull.Value;
            dr["Wattage"] = fixedAsset.Wattage != null ? fixedAsset.Wattage : (object)DBNull.Value;
            dr["Notes"] = fixedAsset.Notes != null ? fixedAsset.Notes : (object)DBNull.Value;
            dr["MaintLink"] = fixedAsset.MaintLink != null ? fixedAsset.MaintLink : (object)DBNull.Value;
            dr["SurfaceArea"] = fixedAsset.SurfaceArea != null ? fixedAsset.SurfaceArea : (object)DBNull.Value;
            dr["sValue"] = fixedAsset.sValue != null ? fixedAsset.sValue : (object)DBNull.Value;
            dr["Price_m2"] = fixedAsset.Price_m2 != null ? fixedAsset.Price_m2 : (object)DBNull.Value;
            dr["ExpDate"] = fixedAsset.ExpDate != null ? fixedAsset.ExpDate : (object)DBNull.Value;
            dr["MarketValue"] = fixedAsset.MarketValue != null ? fixedAsset.MarketValue : (object)DBNull.Value;
            dr["TitleDeed"] = fixedAsset.TitleDeed != null ? fixedAsset.TitleDeed : (object)DBNull.Value;
            dr["Distance"] = fixedAsset.Distance != null ? fixedAsset.Distance : (object)DBNull.Value;
            dr["DistanceUnit"] = fixedAsset.DistanceUnit != null ? fixedAsset.DistanceUnit : (object)DBNull.Value;
            dr["NumberOfP"] = fixedAsset.NumberOfP != null ? fixedAsset.NumberOfP : (object)DBNull.Value;
            dr["Pin"] = fixedAsset.Pin != null ? fixedAsset.Pin : (object)DBNull.Value;
            dr["GeomData"] = fixedAsset.GeomData != null ? fixedAsset.GeomData : (object)DBNull.Value;
            dr["oprType"] = fixedAsset.oprType;
            dt.Rows.Add(dr);

            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AssetID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = fixedAsset.AssetID;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dt);


            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                {
                    //baseModel.dbResult = dt;
                    baseModel.dbResult.Tables["CTRLTBL"].TableName = "ResultCtrlTbl";
                    baseModel.dbResult.Merge(dsProcess);
                    bResult = false;
                }
                else
                    bResult = true;
            }
            else
                bResult = false;

            if (bResult)
            {



            }

            baseModel.data = bResult;
            return baseModel;
        }


        public class statusMessage
        {
            public int ruleID { get; set; }

            public Boolean status { get; set; }
        }

        //public BaseModel getMaintenanceExceptions(String sConnStr)
        //    {

        //        List<statusMessage> sMessge = new List<statusMessage>();
        //        BaseModel bm = new BaseModel();

        //        DataTable dtRulesByPlanningStruc = new RulesService().GetRulesbyStruc("AssetMaintenance", sConnStr);

        //        DataView view = new DataView(dtRulesByPlanningStruc);
        //        DataTable distinctParentRuleID = view.ToTable(true, "ParentRuleID");

        //        //loop all distinct parentruleid
        //        foreach (DataRow dr in distinctParentRuleID.Rows)
        //        {
        //            int ParentRuleID = Convert.ToInt32(dr["ParentRuleID"].ToString());

        //            //check if parentruleid exist in table and take all data

        //            DataTable dtSelectedParentRuleIdRows = new RulesService().GetRulesByParentRuleID(ParentRuleID.ToString(), sConnStr);


        //            List<int> lUris = new List<int>();

        //            if (dtSelectedParentRuleIdRows != null)
        //            {
        //                //loop through all selected parentruleid and take uris
        //                foreach (DataRow drRows in dtSelectedParentRuleIdRows.Rows)
        //                {



        //                    //Maintenance RuleConditions
        //                    if (drRows["struc"].ToString() == "AssetMaintenance")
        //                    {

        //                        string maintenaceStrucValue = drRows["strucValue"].ToString();
        //                        int value = Convert.ToInt32(maintenaceStrucValue);
        //                        lUris.Add(value);

        //                    }

        //                }



        //                //if rule has uris then get exceptions data and save to notification table
        //                if (lUris.Count() != 0)
        //                {
        //                    //get exceptions data for planning
        //                    BaseModel bmGenerateAlertMaintenance = new NaveoOneLib.Services.Rules.AlertService().GenerateAlertMaintenance(ParentRuleID, lUris, sConnStr);

        //                    if (bmGenerateAlertMaintenance.data != null)
        //                    {
        //                        //save to Nofication table
        //                        DataTable dtNotification = NotificationService.NotificationDT();
        //                        foreach (DataRow drG in bmGenerateAlertMaintenance.data.Rows)
        //                        {
        //                            DataRow drN = dtNotification.NewRow();
        //                            //dr["UID"] = UID;
        //                            //dr["SendAt"] = Convert.ToDateTime(dtSent);
        //                            //dr["Err"] = Err.ToString();
        //                            //dr["sBody"] = sBody.ToString();
        //                            //dr["Type"] = Type.ToString();
        //                            //dr["Title"] = Title.ToString();
        //                            dtNotification.Rows.Add(drN);



        //                        }


        //                        Boolean bSave = new NotificationService().SaveNotification(dtNotification, sConnStr);
        //                        statusMessage sm = new statusMessage();
        //                        sm.ruleID = ParentRuleID;
        //                        sm.status = bSave;
        //                        sMessge.Add(sm);


        //                    }

        //                }
        //            }

        //        }

        //        bm.data = sMessge;
        //        return bm;

        //    }

        }
}
