﻿using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.Common;
using NaveoService.Helpers;
using NaveoService.Models;
using NaveoService.Models.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Services
{
    public class RoadNetworkService
    {
        public BaseDTO SaveRoadNetworkFromShp(SecurityTokenExtended securityToken, String shpPath, String sType, int iMatrix)
        {
            BaseModel assetObj = new NaveoOneLib.Services.Assets.RoadNetworkService().RoadNetworkFromShp(securityToken.securityToken.UserToken, shpPath, sType, iMatrix, securityToken.securityToken.sConnStr);
            return assetObj.ToBaseDTO(securityToken.securityToken);
        }

        public BaseModel GetRoadNetworkByIds(int UID, List<TreeIDs> lids, Boolean ShowDataOnly, string sConnStr)
        {
            BaseModel bm = new BaseModel();
            DataSet ds = new NaveoOneLib.Services.Assets.RoadNetworkService().GetRoadNetworkByIds(UID, lids, sConnStr);

            if (ShowDataOnly)
                bm.data = ds;
            else
            {
                String responseString = NaveoOneLib.Maps.NaveoWebMapsCall.GetDataFromMapServer(ds, 0, 0, false, "FixedAssets/CreateRoadNetwork", sConnStr);
                bm.data = responseString;
            }

            return bm;
        }
    }
}
