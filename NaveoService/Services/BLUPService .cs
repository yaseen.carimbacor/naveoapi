﻿
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.GMatrix;

using System;
using System.Collections.Generic;
using System.Data;
using NaveoOneLib.Services.GMatrix;
using NaveoService.Models.DTO;
using NaveoService.Helpers;
using NaveoService.Models.Custom;
using NaveoService.Models;

namespace NaveoService.Services
{
    public class BLUPService
    {
        #region Methods

        public dtData GetBLUPDataByIds(Guid sUserToken, string sConnStr, List<string> iID)
        {
            int UID = -1;
            List<string> lValidatedBLUP = CommonService.lValidateBLUPs(sUserToken, iID, sConnStr, out UID);

            DataTable dtBlup = new NaveoOneLib.Services.BLUP.BLUPService().GetBlupByAplIDs(lValidatedBLUP,sConnStr);
  

            dtData dt = new dtData();
            dt.Data = dtBlup;
            dt.Rowcnt = dtBlup.Rows.Count;
            return dt;
        }

       

        #endregion
    }
}
