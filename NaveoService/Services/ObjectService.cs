﻿using NaveoOneLib.Models;
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.Users;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using NaveoOneLib.Services.Users;

namespace NaveoService.Services
{
    public class ObjectService
    {
        //Objects
        public Models.Object dsAPIObjects(Guid sUserToken, String sConnStr, Boolean bUsingDS)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            if (UID == -1)
                return new Models.Object();

            //Assets and groups
            DataTable dtGMAsset = new NaveoOneLib.WebSpecifics.LibData().dtGMAsset(UID, sConnStr);

            dtGMAsset.Columns.Remove("myStatus");
            dtGMAsset.Columns.Remove("MID");
            dtGMAsset.Columns.Remove("MatrixiID");

            DataTable dtGroup = dtGMAsset.Clone();
            DataTable dtAsset = dtGMAsset.Clone();

            foreach (DataRow dr in dtGMAsset.Rows)
            {
                int i = -1;
                int.TryParse(dr["GMID"].ToString(), out i);
                if (i > 0)
                    dtGroup.Rows.Add(dr.ItemArray);
                else
                    dtAsset.Rows.Add(dr.ItemArray);
            }

            dtGroup.TableName = "dtGroup";
            dtGroup.Columns.Remove("StrucID");
            dtGroup.Columns["GMID"].ColumnName = "id";
            dtGroup.Columns["GMDescription"].ColumnName = "desc";
            dtGroup.Columns["ParentGMID"].ColumnName = "parent";

            dtAsset.TableName = "dtAssets";
            dtAsset.Columns.Remove("GMID");
            dtAsset.Columns["StrucID"].ColumnName = "id";
            dtAsset.Columns["GMDescription"].ColumnName = "desc";
            dtAsset.Columns["ParentGMID"].ColumnName = "parent";
            dtAsset.Columns.Add("TimeZone");
            List<Asset> la = new NaveoOneLib.WebSpecifics.LibData().lAsset(UID, sConnStr);
            foreach (Asset a in la)
            {
                DataRow[] dr = dtAsset.Select("id = " + a.AssetID);
                if (dr.Length > 0)
                    dr[0]["TimeZone"] = a.TimeZoneTS;
            }

            //Drivers
            DataTable dtGMDriver = new NaveoOneLib.WebSpecifics.LibData().dtGMDriver(UID, sConnStr);
            dtGMDriver.Columns.Remove("myStatus");
            dtGMDriver.Columns.Remove("MID");
            dtGMDriver.Columns.Remove("MatrixiID");

            DataTable dtDriver = dtGMDriver.Clone();
            foreach (DataRow dr in dtGMDriver.Rows)
            {
                int i = -1;
                int.TryParse(dr["GMID"].ToString(), out i);
                if (i < 0)
                    dtDriver.Rows.Add(dr.ItemArray);
            }

            dtDriver.TableName = "dtDrivers";
            dtDriver.Columns.Remove("GMID");
            dtDriver.Columns["StrucID"].ColumnName = "id";
            dtDriver.Columns["GMDescription"].ColumnName = "desc";
            dtDriver.Columns["ParentGMID"].ColumnName = "parent";

            Models.Object obj = new Models.Object();
            obj.groups = dtGroup;
            obj.assets = dtAsset;
            obj.drivers = dtDriver;

            if (bUsingDS)
                obj.zones = CommonService.castLApiZone(new NaveoOneLib.WebSpecifics.LibData().dsZones(UID, sConnStr, false, 1, 25)); //1, 25 for testing. was null, null
            else
                obj.zones = CommonService.castLApiZone(new NaveoOneLib.WebSpecifics.LibData().lZone(UID, sConnStr, null, null));

            return obj;
        }

        public dtData NaveoMasterData(Guid sUserToken, String sConnStr, NaveoModels nm, int? Page, int? LimitPerPage)
        {
            return new NaveoOneLib.WebSpecifics.LibData().NaveoMasterData(sUserToken, sConnStr, nm, Page, LimitPerPage);
        }

        public dtData NaveoMaintenanceData(Guid sUserToken, String sConnStr)
        {
            return new NaveoOneLib.WebSpecifics.LibData().NaveoMaintenanceData(sUserToken, sConnStr);
        }

        public dtData NaveoMaintenanceDataFAOnly(Guid sUserToken, String sConnStr)
        {
            return new NaveoOneLib.WebSpecifics.LibData().NaveoMaintenanceDataFAOnly(sUserToken, sConnStr);
        }
    }
}