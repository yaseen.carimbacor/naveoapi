﻿using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.Drivers;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Lookups;
using NaveoOneLib.Models.Plannings;
using NaveoOneLib.Services;
using NaveoOneLib.Services.GMatrix;
using NaveoService.Helpers;
using NaveoService.Models;
using NaveoService.Models.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models.Users;
using Matrix = NaveoOneLib.Models.GMatrix.Matrix;
using Planning = NaveoOneLib.Models.Plannings.Planning;
using Newtonsoft.Json;
using System.Net;
using System.Web.Script.Serialization;
using System.Net.Http;
using static NaveoService.Services.PlanningService;
using System.IO;
using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Common;

namespace NaveoService.Services
{
    public class ScheduleService
    {
        public dtData GetPlanningScheduledRequest(Guid sUserToken, List<int> lassetids, DateTime dtFrom, DateTime dtTo, string sConnStr, int? Page, int? LimitPerPage, List<String> sortColumns, Boolean sortOrderAsc)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);


            //lassetids = new List<int>() { 1,2,3,4,5,6,7,8,9 };

            DataTable dtPlanning = new NaveoOneLib.Services.Plannings.ScheduledService().GetPlanningScheduleRequest(dtFrom, dtTo, lm, lassetids, sConnStr, sortColumns, sortOrderAsc);
            DataTable dtFinal = new DataTable();
            dtFinal.Columns.Add("pId", typeof(int));
            dtFinal.Columns.Add("scheduleId", typeof(int));
            dtFinal.Columns.Add("requestDate");
            dtFinal.Columns.Add("dateCreated");
            dtFinal.Columns.Add("startTime");
            dtFinal.Columns.Add("endTime");
            dtFinal.Columns.Add("startDateTime", typeof(DateTime));
            dtFinal.Columns.Add("endDateTime", typeof(DateTime));
            dtFinal.Columns.Add("departureId");
            dtFinal.Columns.Add("departure");
            dtFinal.Columns.Add("arrivalId");
            dtFinal.Columns.Add("arrival");
            dtFinal.Columns.Add("asset");
            dtFinal.Columns.Add("driver");
            dtFinal.Columns.Add("assetID", typeof(int));
            dtFinal.Columns.Add("driverID", typeof(int));
            dtFinal.Columns.Add("status");
            dtFinal.Columns.Add("tripType");
            dtFinal.Columns.Add("tripTypeColor");
            dtFinal.Columns.Add("coordsFrom");
            dtFinal.Columns.Add("coordsTo");

            int status = 0;


                DataView view = new DataView(dtPlanning);
                //not equal to 200
                view.RowFilter = "Type <> '200'";
                dtPlanning = view.ToTable();
            


            foreach (DataRow drPlanning in dtPlanning.Rows)
            {
                if (drPlanning["Status"].ToString() != string.Empty)
                {
                    status = (int)drPlanning["Status"];
                }

                if (status != 0)
                {
                    DataRow row = dtFinal.NewRow();

                    row["pId"] = drPlanning["PID"].ToString() == string.Empty ? 0 : Convert.ToInt32(drPlanning["PID"]);
                    row["requestDate"] = this.convertDatetoString(drPlanning["RequestDate"].ToString());
                    row["dateCreated"] = this.convertDatetoString(drPlanning["ScheduledDate"].ToString());
                    row["startTime"] = drPlanning["StartTime"].ToString() == String.Empty ? "" : Convert.ToDateTime(drPlanning["StartTime"]).ToLocalTime().ToShortTimeString();
                    row["startDateTime"] = drPlanning["dtFrom"].ToString() == String.Empty ? new DateTime() : Convert.ToDateTime(drPlanning["dtFrom"]);
                    row["endTime"] = drPlanning["endTime"].ToString() == String.Empty ? "" : Convert.ToDateTime(drPlanning["EndTime"]).ToLocalTime().ToShortTimeString();
                    row["endDateTime"] = drPlanning["dtTo"].ToString() == String.Empty ? new DateTime() : Convert.ToDateTime(drPlanning["dtTo"]);
                    row["departureId"] = drPlanning["DepartureID"].ToString() == String.Empty ? 0 : Convert.ToInt32(drPlanning["DepartureID"]);
                    row["departure"] = drPlanning["Departure"].ToString();
                    row["arrivalId"] = drPlanning["ArrivalID"].ToString() == String.Empty ? 0 : Convert.ToInt32(drPlanning["ArrivalID"]);
                    row["arrival"] = drPlanning["Arrival"].ToString();
                    row["assetID"] = drPlanning["AssetID"].ToString() == String.Empty ? 0 : Convert.ToInt32(drPlanning["AssetID"]);
                    row["asset"] = drPlanning["Asset"].ToString();
                    row["driver"] = drPlanning["Driver"].ToString();
                    row["driverID"] = drPlanning["DriverID"].ToString() == string.Empty ? 0 : Convert.ToInt32(drPlanning["DriverID"]);
                    row["scheduleId"] = drPlanning["ScheduleId"].ToString() == string.Empty ? 0 : Convert.ToInt32(drPlanning["ScheduleId"]);
                    row["tripType"] = drPlanning["TripType"].ToString();
                    row["tripTypeColor"] = drPlanning["TripTypeColor"].ToString();
                    row["coordsFrom"] = drPlanning["CoordinatesFrom"].ToString();
                    row["coordsTo"] = drPlanning["CoordinatesTo"].ToString();

                    if (status != 0)
                        row["status"] = "Scheduled";

                    dtFinal.Rows.Add(row);
                }


            }



            DataTable ResultTable = dtFinal.Clone();
            ResultTable.Columns.Add("rowNum", typeof(int));
            ResultTable.Columns["rowNum"].AutoIncrement = true;
            ResultTable.Columns["rowNum"].AutoIncrementSeed = 1;
            ResultTable.Columns["rowNum"].AutoIncrementStep = 1;
            foreach (DataRow dr in dtFinal.Rows)
                ResultTable.Rows.Add(dr.ItemArray);

            int iTotalRec = ResultTable.Rows.Count;
            Pagination p = Pagination.GetRowNums(Page, LimitPerPage);
            DataTable dt = new DataTable();
            DataRow[] drChk = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo);
            if (drChk != null && drChk.Length > 0)
                dt = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo).CopyToDataTable();
            else
                dt = ResultTable;

            dtData dtR = new dtData();
            dtR.Data = dt;
            dtR.Rowcnt = iTotalRec;

            return dtR;
        }

        public Scheduled GetScheduledByHID(int hid, string sConnStr)
        {
            Scheduled scheduled = new NaveoOneLib.Services.Plannings.ScheduledService().GetScheduledByHID(hid, sConnStr);
            return scheduled;
        }
        private string convertDatetoString(string inputDate)
        {   
            DateTime dateTime = Convert.ToDateTime(inputDate);
            return dateTime.Month + "/" + dateTime.Day + "/" + dateTime.Year;
        }


        //public Scheduled GetSechduledRequestById(int Hid ,string sConnStr)
        //{

            


        //}

        public dtData GetRosterDataForScheduling(DateTime dtFrom, DateTime dtTo, Guid sUserToken, string sConnStr, int? Page, int? LimitPerPage, List<String> sortColumns, Boolean sortOrderAsc)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);

            DataTable dtRoster = new NaveoOneLib.Services.Shifts.RosterService().GetRoster(dtFrom, dtTo, lm, sConnStr, sortColumns, sortOrderAsc);

            int count = dtRoster.Rows.Count;
            DataTable dtFinal = new DataTable();
            dtFinal.Columns.Add("vehicle");
            dtFinal.Columns.Add("vehicleId");
            dtFinal.Columns.Add("driver");
            dtFinal.Columns.Add("driverId");
            dtFinal.Columns.Add("resource");
            dtFinal.Columns.Add("resourceId");
            dtFinal.Columns.Add("dateFrom");
            dtFinal.Columns.Add("dateTo");

            foreach (DataRow dr in dtRoster.Rows)
            {
                DataRow row = dtFinal.NewRow();
                row["vehicle"] = dr["Asset"].ToString();
                row["vehicleId"] = (int)dr["vehicleId"];

                if (dr["ResourceType"].ToString() == "Driver")
                {
                    row["driver"] = dr["ResourceName"].ToString();
                    row["driverId"] = (int)dr["ResourceID"];

                }
                else if (dr["ResourceType"].ToString() == "Officer" || dr["ResourceType"].ToString() == "Attendant" || dr["ResourceType"].ToString() == "Loader")
                {
                    row["resource"] = dr["ResourceName"].ToString();
                    row["resourceId"] = (int)dr["ResourceID"];
                }

                row["dateFrom"] = dr["dtFrom"].ToString();
                row["dateTo"] = dr["dtTo"].ToString();


                dtFinal.Rows.Add(row);

            }

            dtData dtR = new dtData();
            dtR.Data = dtFinal;
            dtR.Rowcnt = count;

            return dtR;
        }

        public dtData GetScheduledViaPoints(int PID, string sConnStr)
        {
            DataTable dtScheduleViaPoints = new NaveoOneLib.Services.Plannings.ScheduledService().GetSchedueldViaPoints(sConnStr, PID.ToString());

            dtData dtR = new dtData();
            dtR.Data = dtScheduleViaPoints;
            //dtR.Rowcnt = count;
            return dtR;

        }


        public BaseDTO GetPublicScheduledViaPoints(string guid, SecurityToken securityToken)
        {
            try
            {
                string json = Globals.getFile(guid + ".dat");
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                SaveScheduledRouting ret = serializer.Deserialize<SaveScheduledRouting>(json.ToString());
                return new BaseDTO
                {
                    data = ret,
                    oneTimeToken = securityToken.OneTimeToken,
                    sQryResult = "Succeeded",
                    errorMessage = ""
                };
            }
            catch (Exception e)
            {
                return new BaseDTO
                {
                    data = false,
                    oneTimeToken = securityToken.OneTimeToken,
                    sQryResult = "Succeeded",
                    errorMessage = ""
                };
            }
        }


        public Boolean SaveUpdateScheduled(SecurityTokenExtended securityToken, Scheduled sch, Boolean bInsert)
        {

            int userId = CommonService.GetUserID(securityToken.securityToken.UserToken, securityToken.securityToken.sConnStr);
            List<int> lUsers = new List<int>();
            lUsers.Add(userId);

            int uiD = -1;
            List<int> lValidatedUsers = CommonService.lValidateUsers(securityToken.securityToken.UserToken, lUsers, securityToken.securityToken.sConnStr, out uiD);
            int UID = lValidatedUsers[0];

            sch.CreatedBy = UID;
            sch.CreatedDate = DateTime.Now;

            List<PlanningResource> additionalResources = new List<PlanningResource>();
            foreach (var la in sch.lAdditionalResources)
            {
                PlanningResource pR = new PlanningResource();
                pR.iID = la.iID;
                pR.PID = la.PID;
                pR.DriverID = la.DriverID;
                pR.ResourceType = la.ResourceType;
                pR.oprType = la.oprType;


                additionalResources.Add(pR);


            }


            List<ScheduledAsset> schAssets = new List<ScheduledAsset>();
            foreach (var s in sch.lSchAsset)
            {
                ScheduledAsset sA = new ScheduledAsset();
                sA.iID = s.iID;
                sA.AssetID = s.AssetID;
                sA.DriverID = s.DriverID;
                sA.HID = s.HID;
                sA.OprType = s.OprType;



            }

            var saveObj = true;
            string guid = "";
            if (true)
            {
                List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, securityToken.securityToken.sConnStr);
                #region Matrix
                if (bInsert)
                {

                    foreach (NaveoOneLib.Models.GMatrix.Matrix mm in lm)
                    {


                        NaveoOneLib.Models.GMatrix.Matrix m = new NaveoOneLib.Models.GMatrix.Matrix();
                        m.GMDesc = mm.GMDesc;
                        m.GMID = mm.GMID;
                        m.iID = sch.HID;
                        m.MID = 0;
                        m.oprType = DataRowState.Added;
                        sch.lMatrix = new List<NaveoOneLib.Models.GMatrix.Matrix>();


                        sch.lMatrix.Add(m);
                    }
                }



                #endregion


                dynamic dResult = new NaveoOneLib.Services.Plannings.ScheduledService().SaveScheduled(sch, additionalResources, UID, bInsert, securityToken.securityToken.sConnStr, true);
                if (dResult.GetType() == typeof(int))
                {
                    int HID = dResult;
                    Scheduled scheduled = GetScheduledByHID(HID, securityToken.securityToken.sConnStr);
                    List<int> pid = new List<int>();
                    foreach (ScheduledPlan scheduledPlan in scheduled.lSchPlan) pid.Add(scheduledPlan.PID);
                    List<ScheduledViaPointS> lViaPoints = new NaveoService.Services.PlanningService().GetScheduledViaPoints(securityToken.securityToken.UserToken, pid, securityToken.securityToken.sConnStr).data;
                    SaveScheduledRouting saveScheduledRouting = new SaveScheduledRouting();
                    saveScheduledRouting.Scheduled = sch;
                    saveScheduledRouting.lViaPoints = lViaPoints;
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    string json = serializer.Serialize(saveScheduledRouting);
                    guid = Globals.SaveFile(json);
                }
                else
                    saveObj = false;

                if (saveObj == true)
                {
                    var plan = sch.lSchPlan;
                    var p = new NaveoOneLib.Services.Plannings.PlanningService().GetPlanningById(plan[0].PID.ToString(), securityToken.securityToken.sConnStr);
                    if (p.type == "150")
                    {
                        string SentMail = SentScheduledMail(UID, securityToken.securityToken.UserToken, sch, securityToken.securityToken.sConnStr);
                    }


                }
            }

            var notificationResult = true;
            var notificationMessage = "";
            if (saveObj)
            {
                try
                {
                    #region Driver Notification @author Nurmaan

                    NaveoService.Services.ResourceService resourceService = new NaveoService.Services.ResourceService();
                    var resource = resourceService.GetResoureById(securityToken.securityToken.UserToken, securityToken.securityToken.sConnStr, Convert.ToInt32(sch.lSchAsset[0].DriverID));
                    //if (!String.IsNullOrEmpty(sch.driverNotification.sEmail) || sch.driverNotification.sMobile != "0" || sch.driverNotification.sMobile != "")
                    //{
                    //    List<String> coordinates = sch.driverNotification.lCoordinates;
                    //    shortenUrls(ref coordinates);
                    //    sch.driverNotification.lCoordinates = coordinates;
                    //}
                    string sServerName = getServerName();
                    if (sch.driverNotification != null)
                    {
                        if (!String.IsNullOrEmpty(sch.driverNotification.sEmail))
                        {// email driver

                            //update driver if email not existed
                            if (resource.email == "" || true)
                            {
                                resource.email = sch.driverNotification.sEmail;
                                List<apiResource> resources = new List<apiResource>();
                                ResourceDTO resourceDTO = new ResourceDTO();
                                resources.Add(resource);
                                resourceDTO.resources = resources;
                                resourceService.SaveUpdateResource(securityToken, resourceDTO, false);
                            }

                            //send email
                            MailSender ms = new MailSender();
                            StringBuilder sb = new StringBuilder();
                            sb.Append("Dear " + resource.firstName + ", \r\n\r\nPlease find details of your scheduled planning on " + sch.driverNotification.lSummary[0].time + ". \r\nFollow the below link\r\n\r\n https://" + sServerName + ".naveo.mu/V3/v2/routing/" + guid + "\r\n");

                            if (sch.driverNotification.lSummary != null)
                            {
                                foreach (coordinatesSummary s in sch.driverNotification.lSummary)
                                {
                                    sb.Append(s.description + "\t" + s.time.ToString("h:mm tt") + "\r\n");
                                }
                            }

                            sb.Append("\r\nKind Regards,\r\nNaveo Team");
                            String sMail = ms.SendMail(sch.driverNotification.sEmail, "New Planning", sb.ToString(), String.Empty, securityToken.securityToken.sConnStr);
                            notificationResult = sMail.ToLower() == "mail sent";
                            notificationMessage = sMail;
                        }
                        else if (sch.driverNotification.sMobile != "0" && !String.IsNullOrEmpty(sch.driverNotification.sMobile))
                        {//send sms to driver

                            //update driver if mobile not existed
                            if (resource.mobileNo == "" || resource.countryCode == null || resource.countryCode == "" || true)
                            {
                                resource.mobileNo = sch.driverNotification.sMobile;
                                resource.countryCode = sch.driverNotification.sCountryCode;
                                List<apiResource> resources = new List<apiResource>();
                                ResourceDTO resourceDTO = new ResourceDTO();
                                resources.Add(resource);
                                resourceDTO.resources = resources;
                                resourceService.SaveUpdateResource(securityToken, resourceDTO, false);
                            }
                            NaveoOneLib.Services.SmsSender sms = new NaveoOneLib.Services.SmsSender();
                            StringBuilder sb = new StringBuilder();
                            sb.Append("Dear " + resource.firstName + ", \r\n\r\nPlease find details of your scheduled planning on " + sch.driverNotification.currentDate + ". \r\nFollow the below link\r\n\r\n https://" + sServerName + ".naveo.mu/V3/v2/routing/" + guid + "\r\n");
                            sb.Append("\r\nPlease find details of your itinerary below\r\n\r\n");
                            foreach (coordinatesSummary s in sch.driverNotification.lSummary)
                            {
                                sb.Append(s.description + "\t" + s.time.ToString("h:mm tt") + "\r\n");
                            }
                            sb.Append("\r\nKind Regards,\r\nNaveo Team");
                            String sas = sms.SendSMS(sch.driverNotification.sCountryCode + sch.driverNotification.sMobile, sb.ToString());
                            notificationResult = sas.ToLower() == "sms sent";
                            notificationMessage = sas;
                        }
                    }
                    #endregion

                }
                catch (Exception ex)
                {

                }
            }


            Models.DTO.BaseDTO bD = new Models.DTO.BaseDTO();
            bD.data = saveObj && notificationResult;
            if (!notificationResult) securityToken.securityToken.DebugMessage = notificationMessage;
            return bD.data;


        }

        public Boolean sendEmailToClient(GetPlanningUserEmailApi lDetails, SecurityTokenExtended securityToken)
        {

            dtData mydtData = new GroupMatrixService().GetMatrixLegalName(securityToken.securityToken.UserToken, securityToken.securityToken.sConnStr);
            String legalName = "Naveo";
            if (mydtData.Data != null && mydtData.Data.Rows.Count != 0)
                legalName = mydtData.Data.Rows[0]["legalName"].ToString();

            Asset asset = new NaveoOneLib.Services.Assets.AssetService().GetAssetById(lDetails.assetID, -1, true, securityToken.securityToken.sConnStr);
            Driver driver = new NaveoOneLib.Services.Drivers.DriverService().GetDriverById(lDetails.driverID.ToString(), securityToken.securityToken.sConnStr);
            var success = true;

            foreach (GetPlanningUserEmailApiData emailToSend in lDetails.lDetails)
            {
                string body = @"Dear Sir/Madam,
We are pleased to inform you that a visit has been scheduled as follows.
Date and Time: " + emailToSend.viaPointTime + @"
Location: " + emailToSend.viaPointName + @"
Vehicle: " + asset.AssetName + @"
Driver: " + driver.sDriverName + @"
Kind Regards, 
"+legalName+" Team";
                if (lDetails.cancel)
                {
                    body = @"Dear Sir/Madam,
We would like to inform you that the following scheduled visit has been CANCELLED.
Date and Time: " + emailToSend.viaPointTime + @"
Location: " + emailToSend.viaPointName + @"
Vehicle: " + asset.AssetName + @"
Driver: " + driver.sDriverName + @"
Kind Regards,
" + legalName + " Team";
                }
                MailSender ms = new MailSender();
                String sMail = ms.SendMail(emailToSend.email, "Visit Notification", body, String.Empty, securityToken.securityToken.sConnStr);
                if (sMail.ToLower() != "mail sent")
                {
                    success = false;
                    securityToken.securityToken.DebugMessage = sMail;
                }
                    
            }
            return success;

        }


        public void shortenUrls(ref List<String> coordinates, int i = 0)
        {
            if(i < coordinates.Count)
            {
                Dictionary<String, String> headers = new Dictionary<string, string>();
                headers.Add("apikey", "5152e529e66a48beac0166044c0bb604");
                headers.Add("workspace", "b2c8ca190a1d4941bcd2db9b353cf5fa");
                var payload = new
                {
                    destination = coordinates[i],
                    domain = new
                    {
                        fullName = "rebrand.ly"
                    }
                };
                var result = externalApi("https://api.rebrandly.com/v1/links", payload, headers);
                if(!(result is Boolean))
                {
                    Dictionary<String, dynamic> sResult = result;
                    foreach(KeyValuePair<String, dynamic> keyValuePair in sResult)
                    {
                        if (keyValuePair.Key.ToLower() == "shorturl")
                        {
                            coordinates[i] = "http://www." + keyValuePair.Value;
                            break;
                        }
                    }
                }
                
                shortenUrls(ref coordinates, ++i);
                
            }
        }

        public Boolean UnScheduled(string scheduleID, SecurityTokenExtended securityToken)
        {
            int userId = CommonService.GetUserID(securityToken.securityToken.UserToken, securityToken.securityToken.sConnStr);
            List<int> lUsers = new List<int>();
            lUsers.Add(userId);

            int uiD = -1;
            List<int> lValidatedUsers = CommonService.lValidateUsers(securityToken.securityToken.UserToken, lUsers, securityToken.securityToken.sConnStr, out uiD);
            int UID = lValidatedUsers[0];
            Scheduled scheduled = new Scheduled();
            scheduled.HID = Convert.ToInt32(scheduleID);

            Boolean unScheduled = new NaveoOneLib.Services.Plannings.ScheduledService().DeleteScheduled(scheduled, UID, securityToken.securityToken.sConnStr);

            Models.DTO.BaseDTO bD = new Models.DTO.BaseDTO();
            bD.data = unScheduled;
            return bD.data;

        }

        public string SentScheduledMail(int UID, Guid sUserToken, Scheduled sch, String sConnStr)
        {

            string sMail = string.Empty;
            foreach (var schPID in sch.lSchPlan)
            {

                NaveoOneLib.Models.Plannings.Planning planning = new NaveoOneLib.Services.Plannings.PlanningService().GetPlanningByPID(schPID.PID, sConnStr);

                if (planning.type != "150")
                    sMail = "No Mail Sent.";


                if (planning.type == "150")
                {
                    int assetId = Convert.ToInt32(sch.lSchAsset[0].AssetID);
                    Asset asset = new NaveoOneLib.Services.Assets.AssetService().GetAssetById(assetId, UID, true, sConnStr);
                    Driver driver = new NaveoOneLib.Services.Drivers.DriverService().GetDriverById(sch.lSchAsset[0].DriverID.ToString(), sConnStr);

                    DataTable dt = new NaveoService.Services.PlanningService().PlanningApprovalDetails(planning, sUserToken, sConnStr);
                    //string approverName = dt.Rows[0][0].ToString();
                    //string approverEmail = dt.Rows[0][1].ToString();
                    string applicantName = dt.Rows[0][2].ToString();
                    string applicantEmail = dt.Rows[0][3].ToString();
                    string From = dt.Rows[0][4].ToString();
                    string To = dt.Rows[0][5].ToString();

                    string sBody = "Dear " + applicantName.ToUpper() + " \r\n\r\n";
                    sBody += "Your Transport Request with PID " + schPID.PID + " has been Scheduled with below details:\r\n\r\n";
                    sBody += "Scheduling Details: \r\n\r\n";
                    sBody += "Requested By: " + applicantName.ToUpper() + "\r\n\r\n";
                    sBody += "Request Date: " + planning.lPlanningViaPointH[0].dtUTC.Value.ToLocalTime() + "\r\n\r\n";
                    sBody += "Departure: " + From + "\r\n\r\n";
                    sBody += "Arrival: " + To + "\r\n\r\n";
                    sBody += "Vehicle: " + asset.AssetName + "\r\n\r\n";
                    sBody += "Driver: " + driver.sDriverName + "\r\n\r\n";


                    sMail = new NaveoOneLib.Services.Plannings.PlanningService().sendPLanningMail(UID, applicantEmail, "PlnRequest", "New Scheduled Request", sBody, sConnStr);


                }

            }

            return sMail;

        }


        #region Terra
        public dtData GetTerraPlanningRequest(Guid sUserToken, DateTime dtFrom, DateTime dtTo, string sConnStr, int? Page, int? LimitPerPage, List<String> sortColumns, Boolean sortOrderAsc)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);
            DataTable dtPlanning = new NaveoOneLib.Services.Plannings.ScheduledService().GetPlanningScheduleRequest(dtFrom, dtTo, lm, null, sConnStr, sortColumns, sortOrderAsc);
           

            DataView view = new DataView(dtPlanning);
            //equal to 0
            view.RowFilter = "Type ='200'";
            dtPlanning = view.ToTable();

            DataTable dtFinal = new DataTable();
            dtFinal.Columns.Add("pId", typeof(int));
            dtFinal.Columns.Add("scheduleId", typeof(int));
            dtFinal.Columns.Add("requestDate");
            dtFinal.Columns.Add("dateCreated");
            dtFinal.Columns.Add("startTime");
            dtFinal.Columns.Add("endTime");
            dtFinal.Columns.Add("dtStartTime");
            dtFinal.Columns.Add("dtEndTime");
            dtFinal.Columns.Add("departureId", typeof(int));
            dtFinal.Columns.Add("departure");
            dtFinal.Columns.Add("arrival");
            dtFinal.Columns.Add("arrivalId", typeof(int));
            dtFinal.Columns.Add("fieldCode");
            dtFinal.Columns.Add("hectare");
            dtFinal.Columns.Add("soilType");
            dtFinal.Columns.Add("operationType");
            dtFinal.Columns.Add("toolType");
            dtFinal.Columns.Add("conversionFactor");
            dtFinal.Columns.Add("toolWidth");
            dtFinal.Columns.Add("targetWorkingRate");
            dtFinal.Columns.Add("asset");
            dtFinal.Columns.Add("driver");
            dtFinal.Columns.Add("assetID", typeof(int));
            dtFinal.Columns.Add("driverID", typeof(int));
            dtFinal.Columns.Add("status");
            dtFinal.Columns.Add("requestCode");
            dtFinal.Columns.Add("planningOperationCode");
            dtFinal.Columns.Add("remarks");

            foreach (DataRow drPlanning in dtPlanning.Rows)
            {
                int PID = Convert.ToInt32(drPlanning["PID"]);

                if(PID == 2763 )
                {

                }
                int HID = drPlanning["ScheduleId"].ToString() == string.Empty ? 0 : Convert.ToInt32(drPlanning["ScheduleId"]);
                DataTable dtPlanningLKUP = new NaveoOneLib.Services.Plannings.ScheduledService().GetPlanningLookUp(PID, sConnStr);
                DataTable dtScheduledResource = new NaveoOneLib.Services.Plannings.ScheduledAssetService().getScheduledResourcesByHID(HID, sConnStr);

                DataRow row = dtFinal.NewRow();
                double buffer = Convert.ToDouble(drPlanning["Buffer"]);
                DateTime dtStartTime = new DateTime();
                DateTime dtEndTime = new DateTime();
                dtStartTime = Convert.ToDateTime(drPlanning["dtTo"]);
                dtStartTime = dtStartTime.AddMinutes(-buffer);
                dtEndTime = Convert.ToDateTime(drPlanning["dtTo"]);
                dtEndTime = dtEndTime.AddMinutes(buffer);

                row["startTime"] = dtStartTime.ToLocalTime().ToShortTimeString();
                row["endTime"] = dtEndTime.ToLocalTime().ToShortTimeString();
                row["dtStartTime"] = dtStartTime.ToLocalTime();
                row["dtEndTime"] = dtEndTime.ToLocalTime();
                row["pId"] = drPlanning["PID"].ToString() == string.Empty ? 0 : Convert.ToInt32(drPlanning["PID"]);

                if (drPlanning["PlanningOperationCode"].ToString() != string.Empty)
                    row["planningOperationCode"] = drPlanning["PlanningOperationCode"].ToString();
                else
                    row["planningOperationCode"] = drPlanning["ParentRequestCode"].ToString();

                row["requestCode"] = drPlanning["ParentRequestCode"].ToString();
                row["requestDate"] = dtStartTime.ToShortDateString();//Convert.ToDateTime(drPlanning["RequestDate"]).ToShortDateString();
                row["dateCreated"] = Convert.ToDateTime(drPlanning["ScheduledDate"]).ToShortDateString();
                row["departure"] = drPlanning["Departure"].ToString();
                row["arrival"] = drPlanning["Arrival"].ToString();
                row["assetID"] = drPlanning["AssetID"].ToString() == String.Empty ? 0 : Convert.ToInt32(drPlanning["AssetID"]);
                row["asset"] = drPlanning["Asset"].ToString();

                //row["driverID"] = drPlanning["DriverID"].ToString() == string.Empty ? 0 : Convert.ToInt32(drPlanning["DriverID"]);
                row["scheduleId"] = HID;
                row["arrivalId"] = drPlanning["ArrivalID"].ToString() == string.Empty ? 0 : Convert.ToInt32(drPlanning["ArrivalID"]);
                row["departureId"] = drPlanning["DepartureID"].ToString() == string.Empty ? 0 : Convert.ToInt32(drPlanning["DepartureID"]);
                row["fieldCode"] = drPlanning["Arrival"].ToString();


                if (dtScheduledResource != null)
                {

                    string conCatName = string.Empty;
                    foreach (DataRow drResources in dtScheduledResource.Rows)
                    {
                        conCatName += drResources["sDriverName"].ToString() + ", ";
                    }

                    row["driver"] = conCatName;
                }


                //row["driver"] = drPlanning["Driver"].ToString();


                if (drPlanning["ZoneArea"].ToString() != string.Empty)
                {
                    decimal hectare = Convert.ToDecimal(drPlanning["ZoneArea"].ToString());
                    row["hectare"] = Math.Round(hectare, 2);
                }
                else
                {
                    row["hectare"] = 0.0;
                }



                row["soilType"] = drPlanning["SoilType"].ToString();
                row["remarks"] = drPlanning["Remarks"].ToString();

                foreach (DataRow drLookUP in dtPlanningLKUP.Rows)
                {
                    if (drLookUP["LookType"].ToString() == "OPERATION_TYPE")
                    {
                        row["operationType"] = drLookUP["LookupValues"].ToString();
                        row["conversionFactor"] = drLookUP["value"].ToString() == string.Empty ? 0.0 : Convert.ToDouble(drLookUP["value"].ToString());
                    }

                }


                foreach (DataRow drLookUP in dtPlanningLKUP.Rows)
                {
                    if (drLookUP["LookType"].ToString() == "TOOL_TYPE")
                    {
                        row["toolType"] = drLookUP["LookupValues"].ToString();
                        row["toolWidth"] = drLookUP["value"].ToString() == string.Empty ? 0.0 : Convert.ToDouble(drLookUP["value"].ToString());
                        row["targetWorkingRate"] = drLookUP["Value2"].ToString() == string.Empty ? 0.0 : Convert.ToDouble(drLookUP["Value2"].ToString());
                    }

                }

                if (drPlanning["Status"].ToString() == string.Empty)
                    drPlanning["Status"] = 0;

                int status = (int)drPlanning["Status"];
                if (status != 0)
                    row["status"] = "Scheduled";
                else
                    row["status"] = " Not Scheduled";

                dtFinal.Rows.Add(row);
            }

            DataTable ResultTable = dtFinal.Clone();
            ResultTable.Columns.Add("rowNum", typeof(int));
            ResultTable.Columns["rowNum"].AutoIncrement = true;
            ResultTable.Columns["rowNum"].AutoIncrementSeed = 1;
            ResultTable.Columns["rowNum"].AutoIncrementStep = 1;
            foreach (DataRow dr in dtFinal.Rows)
                ResultTable.Rows.Add(dr.ItemArray);

            int iTotalRec = ResultTable.Rows.Count;
            Pagination p = Pagination.GetRowNums(Page, LimitPerPage);
            DataTable dt = new DataTable();
            DataRow[] drChk = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo);
            if (drChk != null && drChk.Length > 0)
                dt = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo).CopyToDataTable();
            else
                dt = ResultTable;

            dtData dtR = new dtData();
            dtR.Data = dt;
            dtR.Rowcnt = iTotalRec;

            return dtR;
        }

        public BaseModel GetPlanningRequestByRquestCode(Guid sUserToken, string ParentRequestCode, string sConnStr)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);
            DataTable dtPlanning = new NaveoOneLib.Services.Plannings.PlanningService().GetPlanningByParentRequestCode(ParentRequestCode, sConnStr);
            DataTable dtPlanningLookUp = new NaveoOneLib.Services.Plannings.PlanningService().GetPlanningLookupByParentRequestCode(ParentRequestCode, sConnStr);
            List<apiPlanning> lapiPlanning = new List<apiPlanning>();


            foreach (DataRow x in dtPlanning.Rows)
            {

                Models.DTO.apiPlanning apiPlanning = new Models.DTO.apiPlanning();
                apiPlanning.pId = Convert.ToInt32(x["PID"]);
                //apiPlanning.approval = Convert.ToInt32(x["Approval"]);

                apiPlanning.remarks = x["Remarks"].ToString();
                //apiPlanning.requestCode = x.requestCode;
                apiPlanning.parentRequestCode = x["ParentRequestCode"].ToString();
                apiPlanning.requestCode = x["RequestCode"].ToString();
                apiPlanning.approval = String.IsNullOrEmpty(x["Approval"].ToString()) ? 0 : Convert.ToInt32(x["Approval"]);
                apiPlanning.ApprovalID = Convert.ToInt32(x["ApprovalID"]);
                apiPlanning.type = x["Type"].ToString();
                //apiPlanning.oprType = x.OprType;


                #region PlanningLookUp


                DataRow[] drLookUps = dtPlanningLookUp.Select("PID='" + apiPlanning.pId + "'");
                foreach (var xx in drLookUps)
                {
                    NaveoService.Models.DTO.apiPlanningLkup apiPLKP = new NaveoService.Models.DTO.apiPlanningLkup();
                    apiPLKP.iID = Convert.ToInt32(xx["iID"]);
                    apiPLKP.pId = Convert.ToInt32(xx["PID"]);
                    apiPLKP.lookupType = xx["LookupTypes"].ToString();


                    NaveoService.Models.Custom.minifiedValues minifiedValues = new NaveoService.Models.Custom.minifiedValues();
                    minifiedValues.id = Convert.ToInt32(xx["LookupValueID"]);
                    minifiedValues.description = xx["LookUpValue"].ToString();
                    apiPLKP.lookupValues = minifiedValues;


                    apiPlanning.lPlanningLkup.Add(apiPLKP);

                }
                #endregion


                #region PlanningViaPointH



                NaveoService.Models.DTO.apiPlanningViaPointH apiViaH = new NaveoService.Models.DTO.apiPlanningViaPointH();
                apiViaH.iID = Convert.ToInt32(x["viaHiID"]);
                apiViaH.buffer = Convert.ToInt32(x["Buffer"]);

                apiViaH.pid = Convert.ToInt32(x["PID"]);

                apiViaH.remarks = x["Soil_Type"].ToString();
                apiViaH.seqNo = Convert.ToInt32(x["seqNo"]);

                DateTime? dtUTC = Convert.ToDateTime(x["dtUTC"]);
                Double time = Convert.ToDouble(apiViaH.buffer);
                apiViaH.dtUTC = dtUTC.Value.ToLocalTime().AddMinutes(-time);
                apiViaH.dtUTC1 = dtUTC.Value.ToLocalTime().AddMinutes(time);

                NaveoService.Models.Custom.minifiedValues minifiedZones = new NaveoService.Models.Custom.minifiedValues();
                minifiedZones.id = Convert.ToInt32(x["ZoneID"]);
                minifiedZones.description = x["Description"].ToString();
                apiViaH.zones = minifiedZones;

                apiPlanning.lPlanningViaPointH.Add(apiViaH);

                NaveoService.Models.DTO.apiPlanningViaPointD apiViaD = new NaveoService.Models.DTO.apiPlanningViaPointD();
                apiViaD.iID = String.IsNullOrEmpty(x["viaDiID"].ToString()) ? 0 : Convert.ToInt32(x["viaDiID"]);
                apiViaD.hid = String.IsNullOrEmpty(x["HID"].ToString()) ? 0 : Convert.ToInt32(x["HID"]);
                apiViaD.remarks = x["AreaInHectares"].ToString();

                NaveoService.Models.Custom.minifiedValues minifiedValuesUOM = new NaveoService.Models.Custom.minifiedValues();
                minifiedValuesUOM.id = String.IsNullOrEmpty(x["AreaInHectaresID"].ToString()) ? 0 : Convert.ToInt32(x["AreaInHectaresID"]);
                minifiedValuesUOM.description = apiViaD.remarks;
                apiViaD.uom = minifiedValuesUOM;


                if (apiViaD.iID != 0)
                    apiViaH.lDetail.Add(apiViaD);


                #endregion

                lapiPlanning.Add(apiPlanning);
            }








            BaseModel dtR = new BaseModel();
            dtR.data = lapiPlanning;
            dtR.total = lapiPlanning.Count;

            return dtR;

        }

        public BaseModel GetPlanningScheduledDetails(Guid sUserToken, string ParentRequestCode, string sConnStr)
        {
            DataTable dtPlanning = new NaveoOneLib.Services.Plannings.PlanningService().GetPlanningByParentRequestCode(ParentRequestCode, sConnStr);
            List<ScheduledPlan> lscheduledPlan = new List<ScheduledPlan>();
            List<ScheduledAsset> lscheduledAsset = new List<ScheduledAsset>();
            Scheduled scheduled = new Scheduled();
            foreach (DataRow dr in dtPlanning.Rows)
            {
                string ScheduledID = dr["ScheduledID"].ToString();
                if (ScheduledID != string.Empty)
                {
                    scheduled.HID = Convert.ToInt32(dr["ScheduledID"]);


                    ScheduledPlan scheduledPlan = new ScheduledPlan();
                    scheduledPlan.iID = String.IsNullOrEmpty(dr["schPlaniID"].ToString()) ? 0 : Convert.ToInt32(dr["schPlaniID"]);
                    scheduledPlan.HID = String.IsNullOrEmpty(dr["schPlanHID"].ToString()) ? 0 : Convert.ToInt32(dr["schPlanHID"]);
                    scheduledPlan.PID = String.IsNullOrEmpty(dr["schPlanPID"].ToString()) ? 0 : Convert.ToInt32(dr["schPlanPID"]);
                    lscheduledPlan.Add(scheduledPlan);
                    scheduled.lSchPlan = lscheduledPlan;


                    ScheduledAsset scheduledAsset = new ScheduledAsset();
                    scheduledAsset.iID = String.IsNullOrEmpty(dr["ScheduledAssetiID"].ToString()) ? 0 : Convert.ToInt32(dr["ScheduledAssetiID"]);
                    scheduledAsset.HID = String.IsNullOrEmpty(dr["ScheduledAssetHID"].ToString()) ? 0 : Convert.ToInt32(dr["ScheduledAssetHID"]);
                    scheduledAsset.AssetID = String.IsNullOrEmpty(dr["AssetID"].ToString()) ? 0 : Convert.ToInt32(dr["AssetID"]);
                    scheduledAsset.DriverID = String.IsNullOrEmpty(dr["DriverID"].ToString()) ? 0 : Convert.ToInt32(dr["DriverID"]);
                    lscheduledAsset.Add(scheduledAsset);

                    var myDistinctList = lscheduledAsset.GroupBy(i => i.iID)
                    .Select(g => g.First()).ToList();
                    scheduled.lSchAsset = myDistinctList;

                }
            }
            BaseModel dtR = new BaseModel();
            dtR.data = scheduled;


            return dtR;
        }

        public BaseDTO SaveUpdateTerraPlanning(SecurityTokenExtended securityToken, PlanningDTO planningDTO, Boolean bInsert)
        {
            BaseModel saveObj = new BaseModel();
            int userId = CommonService.GetUserID(securityToken.securityToken.UserToken, securityToken.securityToken.sConnStr);
            List<int> lUsers = new List<int>();
            lUsers.Add(userId);

            int uiD = -1;
            List<int> lValidatedUsers = CommonService.lValidateUsers(securityToken.securityToken.UserToken, lUsers, securityToken.securityToken.sConnStr, out uiD);
            int UID = lValidatedUsers[0];


            var xx = planningDTO.lPlanning;
            DateTime todaysDate = DateTime.Now.Date;
            string todaysDateToString = todaysDate.ToShortDateString();
            string[] splitOperationCode = todaysDateToString.Split('/');
            //string output = lookupValues.Description.Substring(lookupValues.Description.IndexOf("-@-") + 3);

            string day = splitOperationCode[0];
            string month = splitOperationCode[1];
            string year = todaysDate.ToString("yy");
            //int year = Convert.ToInt32(dttt.ToString());




            //int seq = new NaveoOneLib.Services.Plannings.ScheduledService().getCount(securityToken.securityToken.sConnStr);
            //seq++;
            List<Planning> lPlanning = new List<Planning>();
            foreach (var x in xx)
            {
                NaveoOneLib.Models.Plannings.Planning P = new NaveoOneLib.Models.Plannings.Planning();

                #region Matrix
                if (bInsert)
                {
                    List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, securityToken.securityToken.sConnStr);
                    foreach (NaveoOneLib.Models.GMatrix.Matrix mm in lm)
                    {
                        NaveoOneLib.Models.GMatrix.Matrix m = new NaveoOneLib.Models.GMatrix.Matrix();
                        m.GMDesc = mm.GMDesc;
                        m.GMID = mm.GMID;
                        m.iID = x.pId;//mm.iID;
                        m.MID = 0;
                        m.oprType = DataRowState.Added;
                        P.lMatrix.Add(m);
                    }
                }
                #endregion


                P.PID = x.pId;
                P.Approval = 0;
                P.Remarks = x.remarks;
                P.urgent = 0;
                P.type = "200";
                P.parentRequestCode = x.parentRequestCode;
                P.requestCode = x.requestCode;
                P.Approval = x.approval;
                P.ApprovalID = x.ApprovalID;
                P.OprType = x.oprType;







                #region PlanningLkup

                if (x.lPlanningLkup != null)
                {
                    foreach (apiPlanningLkup plk in x.lPlanningLkup)
                    {
                        PlanningLkup dllPlk = new PlanningLkup();
                        dllPlk.PID = plk.pId;
                        dllPlk.iID = plk.iID;
                        dllPlk.LookupValueID = plk.lookupValues.id;
                        dllPlk.LookupValueDesc = plk.lookupValues.description;
                        dllPlk.oprType = plk.oprType;

                        string terraOperationCode = string.Empty;
                        LookUpValues lookupValues = new NaveoOneLib.Services.Lookups.LookUpValuesService().GetLookUpValuesByName(plk.lookupValues.description, securityToken.securityToken.sConnStr);


                        if (lookupValues != null)
                        {


                            //if (bInsert)
                            //{
                            if (plk.lookupType == "OPERATION_TYPE")
                            {
                                if (lookupValues.Description.Contains("-@-"))
                                {
                                    string output = lookupValues.Description.Substring(lookupValues.Description.IndexOf("-@-") + 3);
                                    terraOperationCode = output + "-" + day.ToString() + month.ToString() + year.ToString();
                                    P.closureComments = terraOperationCode;
                                }

                            }
                            //}
                        }

                        P.lPlanningLkup.Add(dllPlk);
                    }

                }



                #endregion

                #region planningViaHeaderDetails
                if (x.lPlanningViaPointH != null)
                {
                    foreach (apiPlanningViaPointH viaH in x.lPlanningViaPointH)
                    {
                        PlanningViaPointH planningViaPointH = new PlanningViaPointH();
                        planningViaPointH.PID = viaH.pid;


                        planningViaPointH.iID = viaH.iID;

                        DateTime startTime = new DateTime();
                        DateTime endTime = new DateTime();
                        startTime = Convert.ToDateTime(viaH.dtUTC);
                        endTime = Convert.ToDateTime(viaH.dtUTC1);
                        System.TimeSpan diff = endTime.Subtract(startTime);

                        Double time = diff.TotalMinutes;

                        time = time / 2;
                        planningViaPointH.Buffer = Convert.ToInt32(time);
                        planningViaPointH.dtUTC = viaH.dtUTC.Value.AddMinutes(time);

                        if (viaH.zones != null)
                        {
                            planningViaPointH.ZoneID = viaH.zones.id;
                            planningViaPointH.ZoneDesc = viaH.zones.description;
                        }


                        //THIS FIELD IS STORING SOIL_TYPE FOR TERRA
                        planningViaPointH.Remarks = viaH.remarks;

                        planningViaPointH.SeqNo = viaH.seqNo;
                        planningViaPointH.oprType = viaH.oprType;




                        if (viaH.lDetail != null)
                        {

                            DataTable dtUOM = new UOMService().GetUOM(securityToken.securityToken.sConnStr);
                            int uomId = (from p in dtUOM.AsEnumerable()
                                         where p.Field<string>("Description") == "Area in Hectares"
                                         select p.Field<int>("UID")).FirstOrDefault();


                            foreach (apiPlanningViaPointD apiviaD in viaH.lDetail)
                            {
                                PlanningViaPointD ViaD = new PlanningViaPointD();


                                ViaD.iID = apiviaD.iID;
                                ViaD.HID = viaH.iID;
                                ViaD.UOM = uomId;
                                //ViaD.uomDesc = apiviaD.uom.description;
                                ViaD.oprType = apiviaD.oprType;
                                ViaD.Remarks = apiviaD.remarks;


                                planningViaPointH.lDetail.Add(ViaD);
                            }
                        }
                        P.lPlanningViaPointH.Add(planningViaPointH);
                    }
                }
                #endregion


                lPlanning.Add(P);


            }

            List<DateTime> lDates = new List<DateTime>();
            saveObj = new NaveoOneLib.Services.Plannings.PlanningService().SaveTerraPlanning(lPlanning, lDates, UID, bInsert, securityToken.securityToken.sConnStr);
            return saveObj.ToBaseDTO(securityToken.securityToken);
        }

        public string UploadPlanningRequest(SecurityTokenExtended securityToken, DataTable dt)
        {
            List<DateTime> lDates = new List<DateTime>();
            int UID = CommonService.GetUserID(securityToken.securityToken.UserToken, securityToken.securityToken.sConnStr);
            string errorMessage = string.Empty;
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, securityToken.securityToken.sConnStr);
            BaseModel saveData = new BaseModel();
            DataView view = new DataView(dt);
            DataTable distinctValues = view.ToTable(true, "PID");
            var query = distinctValues.AsEnumerable().Where(r => r.Field<int>("PID") != 0);

            DataTable dtscheduledDetails = new DataTable();
            dtscheduledDetails.Columns.Add("OrderNo");
            dtscheduledDetails.Columns.Add("Resource");
            dtscheduledDetails.Columns.Add("Asset");
            int count = 0;
            int planningSaveCount = 0;
            int planningErroCount = 0;

            List<Planning> lPlanning = new List<Planning>();

            foreach (var q in query)
            {
                NaveoOneLib.Models.Plannings.Planning P = new NaveoOneLib.Models.Plannings.Planning();

                #region Matrix 
                foreach (Matrix mm in lm)
                {
                    Matrix m = new Matrix();
                    m.GMDesc = mm.GMDesc;
                    m.GMID = mm.GMID;
                    m.iID = 0;
                    m.MID = 0;
                    m.oprType = DataRowState.Added;

                    P.lMatrix.Add(m);
                }
                #endregion



                saveData = new NaveoOneLib.Services.Plannings.PlanningService().SaveTerraPlanning(lPlanning, lDates, UID, true, securityToken.securityToken.sConnStr);
                planningSaveCount++;
            }
            errorMessage = planningSaveCount + " planning request";

            DateTime dateFrom = DateTime.Now.AddMinutes(-2);
            DateTime dateTo = DateTime.Now;
            string dtFrom = dateFrom.ToString("MM/dd/yyyy hh:mm tt");
            string dtTo = dateTo.ToString("MM/dd/yyyy hh:mm tt");
            DataTable dtrecentlyCreatedPlanning = new NaveoOneLib.Services.Plannings.PlanningService().GetPlanning(securityToken.securityToken.sConnStr, dateFrom.ToUniversalTime(), dateTo.ToUniversalTime());
            DataTable dtScheduled = new DataTable();
            dtScheduled.Columns.Add("PID");
            dtScheduled.Columns.Add("Resource");
            dtScheduled.Columns.Add("Asset");

            int c = 0;
            int d = 0;
            foreach (DataRow drRecentlyCreatePlanning in dtrecentlyCreatedPlanning.Rows)
            {
                c++;

                foreach (DataRow dr2 in dtscheduledDetails.Rows)
                {
                    d++;

                    if (c == d)
                    {
                        DataRow toInsert = dtScheduled.NewRow();
                        toInsert["PID"] = drRecentlyCreatePlanning["PID"].ToString();
                        toInsert["Resource"] = dr2["Resource"].ToString();
                        toInsert["Asset"] = dr2["Asset"].ToString();
                        dtScheduled.Rows.Add(toInsert);
                    }
                }
                d = 0;
            }

            //int shceduleMessage = UploadPlanningSchedule(securityToken, dtScheduled);
            string message = string.Empty;
            //if (shceduleMessage != 0)
            //    message = errorMessage + " and " + shceduleMessage + " scheduled request has been created";
            //else
            //    message = errorMessage + " has been created";

            return message;
        }
        #endregion

        public string getServerName()
        {
            string sServerName = System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            User.NaveoDatabases myEnum;
            //Getting only the enum part
            sServerName = sServerName.Replace(".naveo.mu", String.Empty);
            //sServerName = sServerName.Replace(":44393", String.Empty);
            sServerName = sServerName.Replace("https://", String.Empty);
            sServerName = sServerName.Replace("http://", String.Empty);
            if (Enum.TryParse(sServerName, true, out myEnum))
                sServerName = sServerName;
            else
                sServerName = sServerName;



            sServerName = sServerName.ToUpper();


            return sServerName;
        }

        public BaseModel GetMaintenanceStatus(DateTime dtFrom, DateTime dtTo, List<int> AssetIds, string sConnStr)
        {
            AssetIds.Add(10);
            AssetIds.Add(11);
            AssetIds.Add(12);
            //Get if any maintenance on vehicle
            #region Maintenance status
            Connection c = new Connection(sConnStr);

#if DEBUG
            string sServerName = "BLACKWIDOW";
#else
             string sServerName = getServerName();
#endif

            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            string strRowCommaSeparated = "";

            int cpt = 0;
            foreach (int dr in AssetIds)
            {

                if (cpt == 0)
                {
                    strRowCommaSeparated = dr.ToString();
                }
                else
                {
                    strRowCommaSeparated += ", " + dr.ToString();
                }

                cpt++;
            }

            string typeOfAsset = "Assets";

            String sql = "select 'MaintenanceStatus' as TableName;\r\n";
            sql += "select cv.\"FMSAssetId\", vmt.\"MaintCatId_cbo\", vm.\"StartDate\", vm.\"EndDate\", vm.\"MaintStatusId_cbo\", vm.\"AssetStatus\" from dbo.\"CustomerVehicle\" cv, dbo.\"GFI_AMM_VehicleMaintenance\" vm, dbo.\"FleetServer\" fs, dbo.\"GFI_AMM_VehicleMaintTypes\" vmt where cv.\"Id\" = vm.\"AssetId\" and cv.\"FleetServerId\" = fs.\"Id\"  and cv.\"FMSAssetId\" IN (" + strRowCommaSeparated + ") and fs.\"Name\" = '" + sServerName + "' AND cv.\"Remarks\" = '" + typeOfAsset + "' and vmt.\"MaintTypeId\" = vm.\"MaintTypeId_cbo\"; ";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);

            DataSet dsMaint = c.GetDataDS(dtSql, NaveoOneLib.Constant.NaveoEntity.MaintDB);
            DataTable customerTable = dsMaint.Tables["MaintenanceStatus"];
            //Loop in initial list remove assetid
            AssetIds = new List<int>();

            foreach (DataRow dr in customerTable.Rows)
            {
                if (!string.IsNullOrEmpty(dr["StartDate"].ToString()) && !string.IsNullOrEmpty(dr["EndDate"].ToString()))
                {
                    if (dr["MaintCatId_cbo"].ToString() == "1" && (dr["MaintStatusId_cbo"].ToString() == "3" || dr["MaintStatusId_cbo"].ToString() == "7") && DateTime.Parse(dr["StartDate"].ToString()) >= dtFrom && DateTime.Parse(dr["EndDate"].ToString()) >= dtTo)
                    {
                        AssetIds.Add(int.Parse(dr["FMSAssetId"].ToString()));
                    }
                    else if (dr["MaintCatId_cbo"].ToString() == "4" && dr["AssetStatus"].ToString() == "1" && DateTime.Parse(dr["StartDate"].ToString()) >= dtFrom && DateTime.Parse(dr["EndDate"].ToString()) >= dtTo)
                    {
                        AssetIds.Add(int.Parse(dr["FMSAssetId"].ToString()));
                    }
                }
            }
            #endregion


            BaseModel baseModel = new BaseModel();

            baseModel.data = AssetIds;

            return baseModel;
        }

        public dtData GetDriverAsset(Guid sUserToken, DateTime dtFrom, DateTime dtTo, string sConnStr)
        {

            DataTable dtAssets = new NaveoOneLib.Services.Plannings.ScheduledService().GetScheduledAssets(dtFrom, dtTo, sConnStr);
            DataTable dtDrivers = new NaveoOneLib.Services.Plannings.ScheduledService().GetScheduledDrivers(dtFrom, dtTo, sConnStr);



            dtData dtAllAssets = new NaveoOneLib.WebSpecifics.LibData().NaveoMasterData(sUserToken, sConnStr, NaveoModels.Assets, null, null);
            dtData dtAllDrivers = new NaveoOneLib.WebSpecifics.LibData().NaveoMasterData(sUserToken, sConnStr, NaveoModels.Drivers, null, null);

            DataSet ds = new DataSet();
            DataTable dtA = new DataTable();


            DataTable dtD = new DataTable();


            foreach (DataRow dr in dtAssets.Rows)
            {
                int assetId = Convert.ToInt32(dr["AssetID"].ToString());


                dtA = dtAllAssets.Data.AsEnumerable()
                 .Where(r => r.Field<int>("id") != assetId)
                 .CopyToDataTable();

            }



            foreach (DataRow dr in dtDrivers.Rows)
            {

                int driverId = Convert.ToInt32(dr["DriverID"].ToString());


                dtD = dtAllDrivers.Data.AsEnumerable()
                .Where(r => r.Field<int>("id") != driverId)
                .CopyToDataTable();

            }

            dtA.TableName = "Assets";
            dtD.TableName = "Drivers";
            ds.Tables.Add(dtA);
            ds.Tables.Add(dtD);






            dtData dtR = new dtData();
            dtR.dsData = ds;

            return dtR;
        }




        public dtData GetNearestDriverVech(Guid sUserToken, int zoneid, string sConnStr)
        {


            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);



            DataTable dt = new DataTable();

            if (zoneid != 0)
                dt = new NaveoOneLib.Services.Plannings.ScheduledService().GetNearestZone(zoneid, sConnStr);
            else
            {
                dt = new NaveoOneLib.Services.Drivers.DriverService().GetDriver(lm, NaveoOneLib.Models.Drivers.Driver.ResourceType.Driver, true, sConnStr);
                dt.Columns["DriverID"].ColumnName = "id";
                dt.Columns["sdrivername"].ColumnName = "description";

            }


            dtData dtR = new dtData();
            dtR.Data = dt;
            return dtR;
        }

        public dynamic externalApi(String url, dynamic dBody, Dictionary<String, String> headers)
        {
            String jsData = JsonConvert.SerializeObject(dBody);
            byte[] body = Encoding.ASCII.GetBytes(jsData);

            //Ssl.EnableTrustedHosts();
            // var request = new HttpClient();
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:2.0) Gecko/20100101 Firefox/4.0";
            request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            request.ContentType = "application/json";
            request.ContentLength = body.Length;

            //add headers
            foreach (KeyValuePair<String, String> header in headers)
            {
                request.Headers.Add(header.Key, header.Value);
            }

            //var content = new StringContent(jsData.ToString(), Encoding.UTF8, "application/json");
            //var response = await request.PostAsync(url, content);

            //return response;
            //Sending
            using (System.IO.Stream stream = request.GetRequestStream())
            {
                stream.Write(body, 0, body.Length);
            }

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            var responseString = response.StatusDescription;

            if (response.StatusDescription == "OK")
            {

                HttpWebResponse resp = (HttpWebResponse)request.GetResponse();
                Encoding encoding = ASCIIEncoding.ASCII;
                using (var reader = new System.IO.StreamReader(resp.GetResponseStream(), encoding))
                {
                    string responseText = reader.ReadToEnd();
                    dynamic sData = new JavaScriptSerializer().Deserialize<dynamic>(responseText);
                    return sData;
                }
            }
            else
            {
                return false;
            }

        }


        public class SaveScheduledRouting
        {
            public Scheduled Scheduled { get; set; }
            public List<ScheduledViaPointS> lViaPoints { get; set; }
        }


    }
}
