﻿using NaveoOneLib.Models;
using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Services.Permissions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Web;

namespace NaveoService.Services
{
    /// <summary>
    /// Permission Service
    /// </summary>
    public class PermissionService
    {
        /// <summary>
        /// Get permission for a user
        /// </summary>
        /// <param name="sUserToken"></param>
        /// <param name="sConnStr"></param>
        /// <returns></returns>
        public string GetUserPermissions(Guid sUserToken, String sConnStr)
        {
            return PermissionsService.GetUserPermissions(sUserToken.ToString(), sConnStr);
        }
    }

}