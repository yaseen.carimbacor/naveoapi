﻿using NaveoOneLib.Constant;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.GPS;
using NaveoOneLib.Services.Rules;
using NaveoOneLib.Services.Trips;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Services
{
    public class DashboardService
    {
        public NaveoOneLib.Models.Common.dtData GetDashboard(Guid sUserToken, string Dashboard, String sConnStr)
        {
            DataTable dtResult = new DataTable();
            dtResult.Columns.Add("report");
            dtResult.Columns.Add("type");
            dtResult.Columns.Add("cnt", typeof(int));
            dtResult.Columns.Add("id", typeof(int));
            dtResult.Columns.Add("idDesc");

            DateTime dtTo = DateTime.Now;
            DateTime dtFrom = DateTime.Now;
            dtFrom = dtFrom.Subtract(dtFrom.TimeOfDay);

            int UID = new NaveoOneLib.Services.Users.UserService().GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new NaveoOneLib.Services.GMatrix.MatrixService().GetMatrixByUserID(UID, sConnStr);

            DataTable dtGMAsset = new NaveoOneLib.WebSpecifics.LibData().dtGMAsset(UID, sConnStr);
            List<int> lAssetIDs = new List<int>();
            foreach (DataRow dr in dtGMAsset.Rows)
            {
                int i = 0;
                if (int.TryParse(dr["StrucID"].ToString(), out i))
                    lAssetIDs.Add(i);
            }
            lAssetIDs = lAssetIDs.Distinct().ToList();

            DataTable dtGMZone = new NaveoOneLib.WebSpecifics.LibData().dtGMZone(UID, sConnStr);
            List<int> lZoneIDs = new List<int>();
            foreach (DataRow dr in dtGMZone.Rows)
            {
                int i = 0;
                if (int.TryParse(dr["StrucID"].ToString(), out i))
                    lZoneIDs.Add(i);
            }
            lZoneIDs = lZoneIDs.Distinct().ToList();



            if (Dashboard == "Live")
            {
                #region Live
                List<GPSData> lData = new NaveoOneLib.Services.GPS.GPSDataService().GetLatestGPSData(lAssetIDs, new List<string>(),false, UID, true, sConnStr);
                var groupedCustomerList = lData.GroupBy(u => u.Availability.Split('|')[0]);

                foreach (var group in groupedCustomerList)
                {


                    DataRow dr = dtResult.NewRow();
                    dr["report"] = "Live";
                    dr["type"] = group.Key;
                    dr["cnt"] = group.Count();

                    string type = dr["type"].ToString();
                    if (type == "Heading")
                        dr["type"] = "Driving";

                    dtResult.Rows.Add(dr);
                }
                #endregion
            }
            else if (Dashboard == "ZoneVisited")
            {
                #region Top 5 zones visited
                DataSet dsZones = new NaveoOneLib.Services.GPS.IdleService().getIdles(lAssetIDs, dtFrom, dtTo, "Visited", 5, 50, true, "Zones", lZoneIDs, lm, false, sConnStr, null, null);
                if (dsZones.Tables["idles"].Rows.Count > 0)
                {
                    DataTable dtZones = dsZones.Tables["idles"].AsEnumerable()
                                                         .GroupBy(r => new { Col1 = r["iZone"], Col2 = r["sZone"] })
                                                         .Select(g =>
                                                         {
                                                             var row = dtResult.NewRow();

                                                             row["report"] = "ZonesVisited";
                                                             row["type"] = g.Key.Col2;
                                                             row["cnt"] = g.Count();
                                                             return row;
                                                         })
                                                         .CopyToDataTable();
                    DataView dv = dtZones.DefaultView;
                    dv.Sort = "cnt desc";
                    dtZones = dv.ToTable();
                    int iRowCnt = 0;
                    foreach (DataRow dr in dtZones.Rows)
                    {
                        if (iRowCnt > 5)
                            break;

                        if (String.IsNullOrEmpty(dr["type"].ToString()))
                            continue;

                        iRowCnt++;
                        dtResult.ImportRow(dr);
                    }
                }
                #endregion

            }
            else if (Dashboard == "TripDistance")
            {
                #region TripDistance
                DataSet dsTrips = new TripHeaderService().GetTripHeaders(lAssetIDs, dtFrom.AddDays(-5), dtTo, NaveoEntity.GenericBypass, String.Empty, false, false, false, lm, sConnStr, null, null, false, null, false);
                if (dsTrips.Tables["TripHeader"].Rows.Count > 0)
                {
                    DataTable dtTrips = dsTrips.Tables["TripHeader"].AsEnumerable()
                                                        .GroupBy(r => new { Col1 = r["AssetID"], Col2 = r["Vehicle"] })
                                                        .Select(g =>
                                                        {
                                                            var row = dtResult.NewRow();

                                                            row["report"] = "Trips";
                                                            row["id"] = g.Key.Col1;
                                                            row["type"] = g.Key.Col2;
                                                            row["idDesc"] = "Asset";
                                                            row["cnt"] = Convert.ToInt32(g.Sum(r => r.Field<Double>("TripDistance")));
                                                            return row;
                                                        })
                                                        .CopyToDataTable();
                    DataView dv = dtTrips.DefaultView;
                    dv.Sort = "cnt desc";
                    dtTrips = dv.ToTable();
                    int iRowCnt = 0;
                    foreach (DataRow dr in dtTrips.Rows)
                    {
                        if (iRowCnt > 5)
                            break;

                        iRowCnt++;
                        dtResult.ImportRow(dr);
                    }
                }
                #endregion
            }
            else if (Dashboard == "Exceptions")
            {
                #region Exceptions
                DataSet dsExceptions = new ExceptionsService().GetExceptions(lAssetIDs, null, dtFrom, dtTo, String.Empty, false, true, null, sConnStr);
                foreach (DataRow dr in dsExceptions.Tables["dtExceptionHeader"].Rows)
                {
                    DataRow r = dtResult.NewRow();
                    r["report"] = "Exceptions";
                    r["type"] = dr["RuleName"];
                    r["cnt"] = dr["cnt"];
                    r["id"] = dr["RuleID"];
                    r["idDesc"] = "Rule";
                    dtResult.Rows.Add(r);
                }
                #endregion
            }



            dtData dtR = new dtData();
            dtR.Data = dtResult;
            //dtR.Rowcnt = iTotalRec;

            return dtR;
        }
    }
}
