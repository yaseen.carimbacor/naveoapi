﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Models.GPS;
using NaveoService.Models;
using NaveoService.Constants;
using NaveoOneLib.Constant;
using NaveoOneLib.Common;
using NaveoService.Helpers;
using System.Configuration;
using iTextSharp.text.pdf;
using iTextSharp.text;
using NaveoService.Models.DTO;
using NaveoService.Models.Custom;

namespace NaveoService.Services
{
    public static class ExportDataService
    {
        #region EXCEL
        /// <summary>
        /// Generate excel stream based on entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static MemoryStream GenerateExcelStreamToExport(Entity entity, SecurityToken securityToken)  //MemoryStream FileStream
        {
            exclPak ePak = excelPak(entity, securityToken);

            // Results Output
            MemoryStream output = new MemoryStream();
            if (String.IsNullOrEmpty("DDD"))
            {
                string exportFilesPath = Path.Combine(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings[ConfigurationKeys.ExportLocation]), securityToken.UserToken.ToString());

                // Determine whether the directory exists.
                if (!Directory.Exists(exportFilesPath))
                    Directory.CreateDirectory(exportFilesPath);

                ePak.excelPackage.SaveAs(new FileInfo(Path.Combine(exportFilesPath, FileHelper.NameFormat(entity.entityName, FileSystem.Extension.XLSX))));
            }
            else
            {
                if (ePak.ex != null)
                {
                    System.IO.File.AppendAllText(Globals.ErrorsPath(), "\r\n Export \r\n" + ePak.ex.Message + "\r\n" + ePak.ex.InnerException);
                    return null;
                }
                else
                    ePak.excelPackage.SaveAs(output);
            }
            return output;
        }

        /// <summary>
        /// Generate excel export and save it in a specific location
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="securityToken"></param>
        /// <returns></returns>
        public static bool SaveExcelExport(Entity entity, SecurityToken securityToken)  //MemoryStream FileStream
        {
            exclPak ePak = excelPak(entity, securityToken);

            if (ePak.ex != null)
            {
                // Create a text file in the location where the excel files are to be generated if error in excel saving
                string exportFilesPath = Path.Combine(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings[ConfigurationKeys.ExportLocation]), securityToken.UserToken.ToString());
                if (!Directory.Exists(exportFilesPath))
                    Directory.CreateDirectory(exportFilesPath);

                string errorTxtFile = Path.Combine(exportFilesPath, FileHelper.NameFormat(entity.entityName, FileSystem.Extension.TXT));
                File.WriteAllText(errorTxtFile, ePak.ex.Message + Environment.NewLine + ePak.ex.InnerException);
                return false;
            }
            else if (ePak.excelPackage != null)
            {
                string exportFilesPath = Path.Combine(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings[ConfigurationKeys.ExportLocation]), securityToken.UserToken.ToString());
                // Determine whether the directory exists.
                if (!Directory.Exists(exportFilesPath))
                    Directory.CreateDirectory(exportFilesPath);

                ePak.excelPackage.SaveAs(new FileInfo(Path.Combine(exportFilesPath, FileHelper.NameFormat(entity.entityName, FileSystem.Extension.XLSX))));
                return true;
            }

            //This should never hit
            return false;
        }

        #region Private methods and classes
        static exclPak excelPak(Entity entity, SecurityToken securityToken)
        {
            exclPak ePak = new Services.ExportDataService.exclPak();

            ExcelPackage package = null;
            int row = 2; // First excel row

            try
            {
                switch (entity.entityName)
                {
                    #region User excel export test
                    case NaveoEntity.USERS:
                        package = ExportHelper.XcelUser(row);
                        break;
                    #endregion

                    #region Trip excel export
                    case NaveoEntity.TRIPS:
                        TripHeader tripHeader = new TripService().GetTripHeaders(securityToken.UserToken, entity.trip.assetIds, entity.trip.dateFrom, entity.trip.dateTo, entity.trip.byDriver, entity.trip.iMinTripDistance, entity.trip.sWorkHours, securityToken.sConnStr, securityToken.Page, securityToken.LimitPerPage,null, null, true, entity.trip.sortColumns, entity.trip.sortOrderAsc);
                        //var thhh = tripHeader.ToTripDTO(securityToken);
                        package = ExportHelper.XcelTrip(tripHeader, row);
                        break;
                    #endregion

                    #region Live excel export
                    case NaveoEntity.LIVE:
                        List<GPSData> liveData = new LiveService().GetLive(securityToken.UserToken, entity.trip.assetIds, entity.trip.lTypeID,entity.trip.byDriver, securityToken.sConnStr);
                        package = ExportHelper.XcelLive(liveData, row);
                        break;
                    #endregion

                    #region DAILYSUMMARIZEDTRIPS
                    case NaveoEntity.DAILY_SUMMARIZED_TRIPS:
                        dtData dtGetSummarizedTrips = new TripService().GetDailySummarizedTrips(securityToken.UserToken, entity.trip.assetIds, entity.trip.dateFrom, entity.trip.dateTo, entity.trip.byDriver, securityToken.sConnStr, securityToken.Page, securityToken.LimitPerPage, entity.trip.sortColumns, entity.trip.sortOrderAsc);
                        package = ExportHelper.XcelDailySummarizedTrip(dtGetSummarizedTrips.dsData, row);
                        break;
                    #endregion

                    #region SUMMARIZEDTRIPS
                    case NaveoEntity.SUMMARIZED_TRIPS:
                        dtData dtSummarizedTrips = new TripService().GetSummarizedTrips(securityToken.UserToken, entity.trip.assetIds, entity.trip.dateFrom, entity.trip.dateTo, entity.trip.byDriver, securityToken.sConnStr, securityToken.Page, securityToken.LimitPerPage, entity.trip.sortColumns, entity.trip.sortOrderAsc);
                        package = ExportHelper.XcelSummarizedTrip(dtSummarizedTrips.Data, row);
                        break;
                    #endregion

                    #region DAILYTRIPTIME
                    case NaveoEntity.DAILY_TRIP_TIME:
                        dtData dtDailyTripTime = new TripService().GetDailyTripTime(securityToken.UserToken, entity.trip.assetIds, entity.trip.dateFrom, entity.trip.dateTo, entity.trip.sTotal, entity.trip.byDriver, securityToken.sConnStr, securityToken.Page, securityToken.LimitPerPage);
                        package = ExportHelper.XcelDynamic1(dtDailyTripTime.Data, row);
                        break;
                    #endregion

                    #region ASSET_SUMMARY_OUTSIDE_HO
                    case NaveoEntity.ASSET_SUMMARY_OUTSIDE_HO:
                        dtData dtASOZ = new TripService().GetAssetSummaryOutsideHO(securityToken.UserToken, entity.trip.assetIds, entity.trip.byDriver, entity.trip.iHOZone, entity.trip.zoneTypes, entity.trip.dateFrom, entity.trip.dateTo, securityToken.sConnStr, securityToken.Page, securityToken.LimitPerPage);
                        package = ExportHelper.XcelDynamic1(dtASOZ.Data, row);
                        break;
                    #endregion

                    #region AssetList
                    case NaveoEntity.ASSET_LIST:
                        dtData dtAssetList = new AssetService().GetAssetData(securityToken.UserToken, entity.trip.assetIds, securityToken.sConnStr, securityToken.Page, securityToken.LimitPerPage);
                        package = ExportHelper.XcelDynamic1(dtAssetList.Data, row);
                        break;
                    #endregion

                    #region Exception
                    case NaveoEntity.EXCEPTION_LIST:
                        DataSet dsAlerts = new ExceptionService().GetExceptions(securityToken.UserToken, entity.exception.assetIds, entity.exception.ruleIds, entity.exception.dateFrom, entity.exception.dateTo, entity.exception.byDriver, entity.exception.bAddress, true, securityToken.sConnStr);
                        package = ExportHelper.XcelException(dsAlerts, row);
                        break;
                    #endregion

                    #region Fuel Export
                    case NaveoEntity.FUEL:
                        DataTable dtFuel = new FuelService().GetFuelData(securityToken.UserToken, entity.fuelexport.assetIds[0], entity.fuelexport.dateFrom, entity.fuelexport.dateTo, securityToken.sConnStr);
                        package = ExportHelper.XcelFuel(dtFuel, row);
                        break;
                    #endregion

                    #region ZoneVisited
                    case NaveoEntity.Zone_Visited_NotVisited:
                        BaseModel bmZV = new ZoneService().getZoneVisited(securityToken.UserToken, entity.zoneVisited.assetIds, entity.zoneVisited.sType, entity.zoneVisited.reportType, entity.zoneVisited.zIds, entity.zoneVisited.idleInMinutes, entity.zoneVisited.idlingSpeed, entity.zoneVisited.bGetZones, entity.zoneVisited.dateFrom, entity.zoneVisited.dateTo, entity.zoneVisited.byDriver, securityToken.sConnStr, securityToken.Page, securityToken.LimitPerPage);
                        package = ExportHelper.XcelZoneVisited(bmZV.data, row);
                        break;

                    #endregion

                    #region TripHistory by data Logs
                    case NaveoEntity.TripHistory_By_DataLog:
                        DebugData debugDATA = new GPSDataService().GetDebugData(securityToken.UserToken, entity.trip.assetIds, entity.trip.dateFrom, entity.trip.dateTo, new List<String> { }, securityToken.sConnStr, securityToken.Page, securityToken.LimitPerPage);
                        package = ExportHelper.XcelTripHistoryByDataLogs(debugDATA.dtData, row);
                        break;

                    #endregion

                    #region Temperature Report
                    case NaveoEntity.Temperature_Report:
                        NaveoService.Models.DebugData dd = new GPSDataService().GetDebugData(securityToken.UserToken, entity.trip.assetIds, entity.trip.dateFrom, entity.trip.dateTo, new List<string> { "Temperature", "Temperature 2", "Temperature 3" }, securityToken.sConnStr, securityToken.Page, securityToken.LimitPerPage);
                        package = ExportHelper.XcelTemperature(dd, row);
                        break;
                    #endregion

                    #region CustomizedTemperature Report
                    case NaveoEntity.Customized_Temperature_Report:
                        dtData getCustTemp = new TemperatureService().getCustomizedTemperature(securityToken.UserToken, securityToken.sConnStr, entity.trip.assetIds, entity.trip.dateFrom, entity.trip.dateTo, new List<string> { "Temperature", "Temperature 2", "Temperature 3" }, 30, securityToken.Page, securityToken.LimitPerPage);
                        package = ExportHelper.XcelDynamic1(getCustTemp.Data, row);
                        break;
                    #endregion

                    #region SensorData Report
                    case NaveoEntity.SensorData_Report:
                        NaveoService.Models.DebugData sesnorData = new GPSDataService().GetDebugData(securityToken.UserToken, entity.trip.assetIds, entity.trip.dateFrom, entity.trip.dateTo, entity.trip.lTypeID, securityToken.sConnStr, securityToken.Page, securityToken.LimitPerPage);
                        package = ExportHelper.XcelSensorData(sesnorData, row);
                        break;
                    #endregion

                    #region Planning
                    case NaveoEntity.Planning_Report:
                        dtData planningReport = new PlanningService().GetPlanning(securityToken.UserToken, entity.planning.dateFrom, entity.planning.dateTo, entity.planning.PID, 1, securityToken.sConnStr, securityToken.Page, securityToken.LimitPerPage, entity.planning.sortColumns, entity.planning.sortOrderAsc);
                        package = ExportHelper.XcelSchedulePlanningRequest(planningReport.Data, "request", row);
                        break;
                    #endregion

                    #region Schedule
                    case NaveoEntity.Schedule_Report:
                        dtData schedulereport = new ScheduleService().GetPlanningScheduledRequest(securityToken.UserToken, new List<int>(), entity.planning.dateFrom, entity.planning.dateTo, securityToken.sConnStr, securityToken.Page, securityToken.LimitPerPage, entity.planning.sortColumns, entity.planning.sortOrderAsc);
                        package = ExportHelper.XcelSchedulePlanningRequest(schedulereport.Data, "scheduled", row);
                        break;
                    #endregion

                    #region FuelReport
                    case NaveoEntity.Fuel_Report:
                        BaseModel fuelReport = new FuelService().GetFuelConsumptionReport(securityToken.UserToken, entity.fuel.dtFrom, entity.fuel.dtTo, entity.fuel.lassetIds, securityToken.Page, securityToken.LimitPerPage, securityToken.sConnStr, entity.fuel.sortColumns, entity.fuel.sortOrderAsc);
                        package = ExportHelper.XcelFuelReport(fuelReport.data, row);
                        break;
                    #endregion

                    #region TerraPlanningList
                    case NaveoEntity.Terra_Planning_Report:
                        dtData Terraplanningschedulereport = new ScheduleService().GetTerraPlanningRequest(securityToken.UserToken, entity.planning.dateFrom, entity.planning.dateTo, securityToken.sConnStr, securityToken.Page, securityToken.LimitPerPage, entity.planning.sortColumns, entity.planning.sortOrderAsc);
                        package = ExportHelper.XcelSchedulePlanningRequest(Terraplanningschedulereport.Data, "TerraPlanningRequest", row);
                        break;
                    #endregion

                    #region TerraPlanningList
                    case NaveoEntity.Customized_Trip_Report:
                        BaseModel Customizedreport = new CustomTripService().GetCustomTripsReport(securityToken.UserToken, entity.planning.dateFrom, entity.planning.dateTo, entity.planning.assetdIds, securityToken.sConnStr, entity.planning.sortColumns, entity.planning.sortOrderAsc);
                        package = ExportHelper.XcelCustomizedReport(Customizedreport.data, 1, entity.planning.dateFrom, entity.planning.dateTo);
                        break;
                    #endregion


                    #region AuxReport
                    case NaveoEntity.Auxiliary_Report:
                        NaveoService.Models.DebugData auxReport = new GPSDataService().GetAuxData(securityToken.UserToken, entity.trip.assetIds, entity.trip.dateFrom, entity.trip.dateTo, entity.trip.lTypeID, securityToken.sConnStr, securityToken.Page, securityToken.LimitPerPage);
                        package = ExportHelper.XcelAuxReport(auxReport, row);
                        break;
                    #endregion

                    #region AmiranReport
                    case NaveoEntity.Amiran_Report:
                        BaseModel bmAmiran = new NaveoService.Services.AssetService().GetMaintenanceFuelCost(securityToken.UserToken, securityToken.sConnStr, entity.maintenanceAndFuelCost.assetIds, entity.maintenanceAndFuelCost.DateTimeStart, entity.maintenanceAndFuelCost.DateTimeEnd);
                        package = ExportHelper.XcelDynamic(bmAmiran.data, entity.maintenanceAndFuelCost.DateTimeStart.Value, entity.maintenanceAndFuelCost.DateTimeEnd.Value, row);
                        break;
                    #endregion

                    #region Moka_CustomizedReport
                    case NaveoEntity.MOLG_CustomizedReport:
                        BaseModel customTripDTO = new NaveoService.Services.CustomTripService().GetMokaCustomTripsReport(securityToken.UserToken, entity.planning.dateFrom, entity.planning.dateTo, entity.planning.assetdIds, securityToken.sConnStr, entity.planning.sortColumns, entity.planning.sortOrderAsc);
                        DataTable dt = new myConverter().ListToDataTable<CustomTripHeader>(customTripDTO.data);
                        package = ExportHelper.XcelDynamic(dt, entity.planning.dateFrom, entity.planning.dateTo, row);
                        break;
                    #endregion

                    #region AccidentMng
                    case NaveoEntity.AccidentMng:
                        BaseModel bm = new NaveoOneLib.Services.AccidentManagement.AccidentManagementServices().ListAccidentManagement(entity.lstAccidentMng, securityToken.UserToken, securityToken.sConnStr);
                        //BaseModel customTripDTO = new NaveoService.Services.CustomTripService().GetMokaCustomTripsReport(securityToken.UserToken, entity.planning.dateFrom, entity.planning.dateTo, entity.planning.assetdIds, securityToken.sConnStr, entity.planning.sortColumns, entity.planning.sortOrderAsc);

                        //DataTable dtAccidentMng = new myConverter().ListToDataTable<NaveoOneLib.Models.AccidentManagement.AccidentManagementDetailDto>(bm.data);
                        package = ExportHelper.XcelDynamic(bm.dbResult.Tables["AccidentManagement"], entity.lstAccidentMng.StartDate, entity.lstAccidentMng.EndDate, row);
                        break;
                    #endregion


                    #region Shift
                    case NaveoEntity.Shift:

                        dtData mydtData = new NaveoService.Services.ShiftService().GetShifts(securityToken.UserToken, securityToken.sConnStr, securityToken.Page, securityToken.LimitPerPage);
                        package = ExportHelper.XcelShift(mydtData.Data,row);
                        break;
                    #endregion


                    #region Roster
                    case NaveoEntity.Roster:

                        dtData dtRostetDta = new NaveoService.Services.RosterService().GetRoster(entity.r.dateFrom, entity.r.dateTo, securityToken.UserToken, securityToken.sConnStr, securityToken.Page, securityToken.LimitPerPage,entity.r.sortColumns, entity.r.sortOrderAsc);
                        package = ExportHelper.XcelRoster(dtRostetDta.Data, row);
                        break;
                    #endregion


                    #region PlanningExceptionReport
                    case NaveoEntity.PlanningExceptions:

                        DataTable dtPlanningExceptions = new NaveoService.Services.ExceptionService().GetPlanningExceptions(securityToken.UserToken,entity.exception.assetIds, entity.exception.ruleIds,entity.exception.dateFrom, entity.r.dateTo, entity.exception.byDriver, securityToken.sConnStr);
                        package = ExportHelper.XcelPlanningExceptions(dtPlanningExceptions, row);
                        break;
                    #endregion


                    #region FuelExceptionReport
                    case NaveoEntity.FuelExceptions:

                        DataTable dtFuelExceptions = new NaveoService.Services.ExceptionService().GetFuelExceptions(securityToken.UserToken, entity.exception.assetIds, entity.exception.ruleIds, entity.exception.dateFrom, entity.r.dateTo, entity.exception.byDriver, securityToken.sConnStr);
                        package = ExportHelper.XcelFuelExceptions(dtFuelExceptions, row);
                        break;
                    #endregion

                    #region MaintenanceCostReport
                    case NaveoEntity.MaintenanceCostReport:

                        DataTable dtMaintenanceCostReport = new NaveoService.Services.AutoReportingConfigService().AutomaticReportingMaintenace(entity.maintenanceAndFuelCost.assetIds, entity.maintenanceAndFuelCost.DateTimeStart.ToString(), entity.maintenanceAndFuelCost.DateTimeEnd.ToString(), entity.maintenanceAndFuelCost.Description);
                        package = ExportHelper.XcelMaintenanceCostReport(dtMaintenanceCostReport, row);
                        break;
                    #endregion

                    default:
                        break;
                }

                ePak.excelPackage = package;
            }
            catch (Exception ex)
            {
                ePak.excelPackage = null;
                ePak.ex = ex;
            }

            return ePak;
        }

        class exclPak
        {
            public ExcelPackage excelPackage { get; set; }
            public Exception ex { get; set; }
        }

        #endregion


        #endregion

        #region PDF
        public static bool SavePDFExport(Entity entity, SecurityToken securityToken)  //MemoryStream FileStream
        {
            BaseModel bm = pdfPak(entity, securityToken);



            //if (ePak.ex != null)
            //{
            //    // Create a text file in the location where the excel files are to be generated if error in excel saving
            //    string exportFilesPath = Path.Combine(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings[ConfigurationKeys.ExportLocation]), securityToken.UserToken.ToString());
            //    if (!Directory.Exists(exportFilesPath))
            //        Directory.CreateDirectory(exportFilesPath);

            //    string errorTxtFile = Path.Combine(exportFilesPath, FileHelper.NameFormat(entity.entityName, FileSystem.Extension.TXT));
            //    File.WriteAllText(errorTxtFile, ePak.ex.Message + Environment.NewLine + ePak.ex.InnerException);
            //    return false;
            //}
            //else if (ePak.excelPackage != null)
            //{
            //string exportFilesPath = Path.Combine(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings[ConfigurationKeys.ExportLocation]), securityToken.UserToken.ToString());
            //// Determine whether the directory exists.
            //if (!Directory.Exists(exportFilesPath))
            //    Directory.CreateDirectory(exportFilesPath);

            //Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 50f, 40f);
            //string path = Path.Combine(exportFilesPath, FileHelper.NameFormat(entity.entityName, FileSystem.Extension.PDF));

            //PdfWriter.GetInstance(pdfDoc, new FileStream(path, FileMode.Create));
            //pdfDoc.Close();
            //ePak.excelPackage.SaveAs(new FileInfo(Path.Combine(exportFilesPath, FileHelper.NameFormat(entity.entityName, FileSystem.Extension.PDF))));
            return bm.data;
            //}

            //This should never hit
            //return false;
        }

        public static BaseModel pdfPak(Entity entity, SecurityToken securityToken)
        {
            BaseModel bm = new BaseModel();
            DataTable dt = new DataTable();

            try
            {
                switch (entity.entityName)
                {

                    #region LIVE 
                    case NaveoEntity.LIVE:
                        List<GPSData> liveData = new LiveService().GetLive(securityToken.UserToken, entity.trip.assetIds, entity.trip.lTypeID,entity.trip.byDriver, securityToken.sConnStr);

                        DataSet dtLive = ExportHelper.PDFlive(liveData);

                        bm = new PdfService().GeneratePDF(dtLive, NaveoEntity.LIVE, DateTime.Now, DateTime.Now, securityToken);
                        break;
                    #endregion


                    #region Trip PDF export
                    case NaveoEntity.TRIPS:
                        TripHeader tripHeader = new TripService().GetTripHeaders(securityToken.UserToken, entity.trip.assetIds, entity.trip.dateFrom, entity.trip.dateTo, entity.trip.byDriver, entity.trip.iMinTripDistance, entity.trip.sWorkHours, securityToken.sConnStr, securityToken.Page, securityToken.LimitPerPage,null, null, true, entity.trip.sortColumns, entity.trip.sortOrderAsc);
                        DataSet dtTrips = ExportHelper.PDFTrip(tripHeader);
                        bm = new PdfService().GeneratePDF(dtTrips, NaveoEntity.TRIPS, entity.trip.dateFrom, entity.trip.dateTo, securityToken);
                        break;
                    #endregion



                    #region Daily SUMMARIZEDTRIPS
                    case NaveoEntity.DAILY_SUMMARIZED_TRIPS:
                        dtData dtDailySummarizedTrips = new TripService().GetDailySummarizedTrips(securityToken.UserToken, entity.trip.assetIds, entity.trip.dateFrom, entity.trip.dateTo, entity.trip.byDriver, securityToken.sConnStr, securityToken.Page, securityToken.LimitPerPage, entity.trip.sortColumns, entity.trip.sortOrderAsc);
                        DataSet dsDailySummarizedTrips = ExportHelper.PDFDailySummarizedTrips(dtDailySummarizedTrips);
                        bm = new PdfService().GeneratePDF(dsDailySummarizedTrips, NaveoEntity.DAILY_SUMMARIZED_TRIPS, entity.trip.dateFrom, entity.trip.dateTo, securityToken);
                        break;


                    #endregion

                    #region SUMMARIZEDTRIPS
                    case NaveoEntity.SUMMARIZED_TRIPS:
                        dtData dtSummarizedTrips = new TripService().GetSummarizedTrips(securityToken.UserToken, entity.trip.assetIds, entity.trip.dateFrom, entity.trip.dateTo, entity.trip.byDriver, securityToken.sConnStr, securityToken.Page, securityToken.LimitPerPage, entity.trip.sortColumns, entity.trip.sortOrderAsc);
                        DataSet dsSummarizedTrips = ExportHelper.PDFSummarizedTrips(dtSummarizedTrips);
                        bm = new PdfService().GeneratePDF(dsSummarizedTrips, NaveoEntity.SUMMARIZED_TRIPS, entity.trip.dateFrom, entity.trip.dateTo, securityToken);
                        break;
                    #endregion

                    #region DAILYTRIPTIME
                    case NaveoEntity.DAILY_TRIP_TIME:
                        dtData dtDailyTripTime = new TripService().GetDailyTripTime(securityToken.UserToken, entity.trip.assetIds, entity.trip.dateFrom, entity.trip.dateTo, entity.trip.sTotal, entity.trip.byDriver, securityToken.sConnStr, securityToken.Page, securityToken.LimitPerPage);
                        DataSet dsDailyTripTime = ExportHelper.PDFDailyTripTime(dtDailyTripTime);
                        bm = new PdfService().GeneratePDF(dsDailyTripTime, NaveoEntity.DAILY_TRIP_TIME, entity.trip.dateFrom, entity.trip.dateTo, securityToken);
                        break;
                    #endregion



                    #region ZoneVisited
                    case NaveoEntity.Zone_Visited_NotVisited:
                        BaseModel bmZV = new ZoneService().getZoneVisited(securityToken.UserToken, entity.zoneVisited.assetIds, entity.zoneVisited.sType, entity.zoneVisited.reportType, entity.zoneVisited.zIds, entity.zoneVisited.idleInMinutes, entity.zoneVisited.idlingSpeed, entity.zoneVisited.bGetZones, entity.zoneVisited.dateFrom, entity.zoneVisited.dateTo, entity.zoneVisited.byDriver, securityToken.sConnStr, securityToken.Page, securityToken.LimitPerPage);
                        DataSet dsZoneVisited = ExportHelper.PDFZoneVisited(bmZV.data);
                        bm = new PdfService().GeneratePDF(dsZoneVisited, NaveoEntity.Zone_Visited_NotVisited, entity.zoneVisited.dateFrom, entity.zoneVisited.dateTo, securityToken);
                        break;
                    #endregion


                    #region Exception
                    case NaveoEntity.EXCEPTION_LIST:
                        DataSet dsAlerts = new ExceptionService().GetExceptions(securityToken.UserToken, entity.exception.assetIds, entity.exception.ruleIds, entity.exception.dateFrom, entity.exception.dateTo, entity.exception.byDriver, entity.exception.bAddress, true, securityToken.sConnStr);
                        DataSet dsExceptionHeaders = ExportHelper.PDFExceptions(dsAlerts, true);
                        bm = new PdfService().GeneratePDF(dsExceptionHeaders, NaveoEntity.EXCEPTION_LIST, entity.exception.dateFrom, entity.exception.dateTo, securityToken);
                        break;
                    #endregion


                    #region Temperature Report
                    case NaveoEntity.Temperature_Report:
                        NaveoService.Models.DebugData dd = new GPSDataService().GetDebugData(securityToken.UserToken, entity.trip.assetIds, entity.trip.dateFrom, entity.trip.dateTo, new List<string> { "Temperature", "Temperature 2", "Temperature 3" }, securityToken.sConnStr, securityToken.Page, securityToken.LimitPerPage);
                        DataSet dsExceptions = ExportHelper.PDFTemperature(dd);
                        bm = new PdfService().GeneratePDF(dsExceptions, NaveoEntity.Temperature_Report, entity.trip.dateFrom, entity.trip.dateTo, securityToken);
                        break;
                    #endregion



                    #region CustomizedTemperature Report
                    case NaveoEntity.Customized_Temperature_Report:
                        dtData getCustTemp = new TemperatureService().getCustomizedTemperature(securityToken.UserToken, securityToken.sConnStr, entity.trip.assetIds, entity.trip.dateFrom, entity.trip.dateTo, new List<string> { "Temperature", "Temperature 2", "Temperature 3" }, 30, securityToken.Page, securityToken.LimitPerPage);
                        DataSet dsCustTemp = ExportHelper.PDFCustTemperature(getCustTemp);
                        bm = new PdfService().GeneratePDF(dsCustTemp, NaveoEntity.Customized_Temperature_Report, entity.trip.dateFrom, entity.trip.dateTo, securityToken);
                        break;
                    #endregion

                    #region SensorData Report
                    case NaveoEntity.SensorData_Report:
                        NaveoService.Models.DebugData sesnorData = new GPSDataService().GetDebugData(securityToken.UserToken, entity.trip.assetIds, entity.trip.dateFrom, entity.trip.dateTo, entity.trip.lTypeID, securityToken.sConnStr, securityToken.Page, securityToken.LimitPerPage);
                        //package = ExportHelper.XcelSensorData(sesnorData, row);
                        break;
                    #endregion

                    #region Fuel Report Export
                    case NaveoEntity.Fuel_Report:
                        BaseModel dtfuelReport = new FuelService().GetFuelConsumptionReport(securityToken.UserToken, entity.fuel.dtFrom, entity.fuel.dtTo, entity.fuel.lassetIds, securityToken.Page, securityToken.LimitPerPage, securityToken.sConnStr, entity.fuel.sortColumns, entity.fuel.sortOrderAsc);
                        DataSet dsFuel = ExportHelper.PDFFuel(dtfuelReport.data);
                        bm = new PdfService().GeneratePDF(dsFuel, NaveoEntity.Fuel_Report, entity.fuel.dateFrom, entity.fuel.dateTo, securityToken);
                        break;
                    #endregion

                    #region Fuel Graph Export
                    case NaveoEntity.FUEL:
                        DataTable dtFuelGraphReport = new FuelService().GetFuelData(securityToken.UserToken, entity.fuelexport.assetIds[0], entity.fuelexport.dateFrom, entity.fuelexport.dateTo, securityToken.sConnStr);
                        DataSet dsGraphFuel = ExportHelper.PDFFuelGraph(dtFuelGraphReport);
                        bm = new PdfService().GeneratePDF(dsGraphFuel, NaveoEntity.FUEL, entity.fuelexport.dateFrom, entity.fuelexport.dateTo, securityToken);
                        break;
                    #endregion

                    #region AuxReport
                    case NaveoEntity.Auxiliary_Report:
                        NaveoService.Models.DebugData auxReport = new GPSDataService().GetAuxData(securityToken.UserToken, entity.trip.assetIds, entity.trip.dateFrom, entity.trip.dateTo, entity.trip.lTypeID, securityToken.sConnStr, securityToken.Page, securityToken.LimitPerPage);
                        DataSet dsAux = ExportHelper.PDFAuxReport(auxReport.dtData);
                        bm = new PdfService().GeneratePDF(dsAux, NaveoEntity.Auxiliary_Report, entity.trip.dateFrom, entity.trip.dateTo, securityToken);
                        break;
                    #endregion

                    #region AmiranReport
                    case NaveoEntity.Amiran_Report:
                        BaseModel bmAmiran = new NaveoService.Services.AssetService().GetMaintenanceFuelCost(securityToken.UserToken, securityToken.sConnStr, entity.maintenanceAndFuelCost.assetIds, entity.maintenanceAndFuelCost.DateTimeStart, entity.maintenanceAndFuelCost.DateTimeEnd);
                        DataSet dsAmiran = ExportHelper.PDFAmiranReport(bmAmiran.data);
                        bm = new PdfService().GeneratePDF(dsAmiran, NaveoEntity.Amiran_Report, entity.maintenanceAndFuelCost.DateTimeStart.Value, entity.maintenanceAndFuelCost.DateTimeEnd.Value, securityToken);
                        break;
                    #endregion


                    #region Moka_CustomizedReport
                    case NaveoEntity.MOLG_CustomizedReport:
                        BaseModel customTripDTO = new NaveoService.Services.CustomTripService().GetMokaCustomTripsReport(securityToken.UserToken, entity.planning.dateFrom, entity.planning.dateTo, entity.planning.assetdIds, securityToken.sConnStr, entity.planning.sortColumns, entity.planning.sortOrderAsc);
                        DataSet dsMoka = ExportHelper.PDFmolgReport(customTripDTO.data);
                        bm = new PdfService().GeneratePDF(dsMoka, NaveoEntity.MOLG_CustomizedReport, entity.planning.dateFrom, entity.planning.dateTo, securityToken);
                        break;
                    #endregion


                    #region PlanningRequet
                    case NaveoEntity.Planning_Report:
                        dtData planningReport = new PlanningService().GetPlanning(securityToken.UserToken, entity.planning.dateFrom, entity.planning.dateTo, entity.planning.PID, 1, securityToken.sConnStr, securityToken.Page, securityToken.LimitPerPage, entity.planning.sortColumns, entity.planning.sortOrderAsc);
                        DataSet dsplanningReport = ExportHelper.PDFplanningReport(planningReport.Data);
                        bm = new PdfService().GeneratePDF(dsplanningReport, NaveoEntity.Planning_Report, entity.planning.dateFrom, entity.planning.dateTo, securityToken);
                        break;
                    #endregion

                    #region AccidentMgt
                    case NaveoEntity.AccidentMng:
                        BaseModel bmAccidentMng = new NaveoOneLib.Services.AccidentManagement.AccidentManagementServices().ListAccidentManagement(entity.lstAccidentMng, securityToken.UserToken, securityToken.sConnStr);

                        bm = new PdfService().GeneratePDF(bmAccidentMng.dbResult, NaveoEntity.AccidentMng, entity.lstAccidentMng.StartDate, entity.lstAccidentMng.EndDate, securityToken);
                        break;
                    #endregion


                    #region Shift
                    case NaveoEntity.Shift:

                        dtData mydtData = new NaveoService.Services.ShiftService().GetShifts(securityToken.UserToken, securityToken.sConnStr, securityToken.Page, securityToken.LimitPerPage);
                        DataSet dsShift = ExportHelper.PdfShiftReport(mydtData.Data);
                        bm = new PdfService().GeneratePDF(dsShift,NaveoEntity.Shift ,DateTime.Now, DateTime.Now, securityToken);
                        break;
                    #endregion

                    #region Roster
                    case NaveoEntity.Roster:

                        dtData mydtDataRoster = new NaveoService.Services.RosterService().GetRoster(entity.r.dateFrom, entity.r.dateTo,securityToken.UserToken, securityToken.sConnStr, securityToken.Page, securityToken.LimitPerPage, entity.r.sortColumns, entity.r.sortOrderAsc);
                        DataSet dsRoster = ExportHelper.PdfRosterReport(mydtDataRoster.Data);
                        bm = new PdfService().GeneratePDF(dsRoster, NaveoEntity.Roster, entity.r.dateFrom, entity.r.dateTo, securityToken);
                        break;


                    #endregion

                    #region PlanningExceptionReport
                    case NaveoEntity.PlanningExceptions:

                        DataTable dtPlanningExceptions = new NaveoService.Services.ExceptionService().GetPlanningExceptions(securityToken.UserToken, entity.exception.assetIds, entity.exception.ruleIds, entity.exception.dateFrom, entity.r.dateTo, entity.exception.byDriver, securityToken.sConnStr);
                        DataSet dsPlanningExceptions = ExportHelper.PdfPlanningExceptionsReport(dtPlanningExceptions);
                        bm = new PdfService().GeneratePDF(dsPlanningExceptions, NaveoEntity.PlanningExceptions, entity.r.dateFrom, entity.r.dateTo, securityToken);

                        break;
                    #endregion


                    #region FuelExceptionReport
                    case NaveoEntity.FuelExceptions:

                        DataTable dtFuelExceptions = new NaveoService.Services.ExceptionService().GetFuelExceptions(securityToken.UserToken, entity.exception.assetIds, entity.exception.ruleIds, entity.exception.dateFrom, entity.r.dateTo, entity.exception.byDriver, securityToken.sConnStr);
                        DataSet dsFuelExceptions = ExportHelper.PdfFuelExceptionsReport(dtFuelExceptions);
                        bm = new PdfService().GeneratePDF(dsFuelExceptions, NaveoEntity.FuelExceptions, entity.r.dateFrom, entity.r.dateTo, securityToken);
                        break;
                    #endregion



                    default:
                        break;
                }

                bm.data = true;
            }
            catch (Exception ex)
            {
                bm.data = null;
                bm.errorMessage = ex.ToString();
            }
            return bm;
        }
        #endregion
    }
}