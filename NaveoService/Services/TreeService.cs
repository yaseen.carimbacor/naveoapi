﻿using NaveoOneLib.Models.Drivers;
using NaveoService.Models;
using NaveoService.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace NaveoService.Services
{
    public class TreeService
    {
        #region Static Fields

        private static readonly object Padlock = new object();

        private static TreeService _instance;

        #endregion

        #region Constructors and Destructors

        private TreeService()
        {

        }

        #endregion

        #region Public Properties

        public static TreeService Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (Padlock)
                    {
                        if (_instance == null)
                        {
                            _instance = new TreeService();
                        }
                    }
                }
                return _instance;
            }
        }

        #endregion

        #region Properties

        #endregion

        #region Public Methods and Operators
        public List<JsTreeModel> GetTreeViewFilter(string GMModule, List<string> selectedElements, string type, int UID, String sConnStr, bool isAllSelected = true, bool withoutChild = false)
        {
            DataTable dt;
            string icon = "";

            switch (GMModule)
            {
                case "ZONE":
                    dt = new NaveoOneLib.WebSpecifics.LibData().dtGMZone(UID, sConnStr);
                    icon = Constant.FONTAWESOME_MAP;
                    break;
                case "ZONE_TYPE":
                    dt = new NaveoOneLib.WebSpecifics.LibData().dtGMZoneType(UID, sConnStr);
                    icon = Constant.FONTAWESOME_MAP;
                    break;
                case "DRIVER":
                    dt = new NaveoOneLib.WebSpecifics.LibData().dtGMDriver(UID, sConnStr);
                    icon = Constant.FONTAWESOME_USER;
                    break;
                case "OFFICER":
                    dt = new NaveoOneLib.WebSpecifics.LibData().dtGMDriver(UID, Driver.ResourceType.Officer, sConnStr);
                    icon = Constant.FONTAWESOME_USER;
                    break;
                case "ATTENDANT":
                    dt = new NaveoOneLib.WebSpecifics.LibData().dtGMDriver(UID, Driver.ResourceType.Attendant, sConnStr);
                    icon = Constant.FONTAWESOME_USER;
                    break;
                case "LOADER":
                    dt = new NaveoOneLib.WebSpecifics.LibData().dtGMDriver(UID, Driver.ResourceType.Loader, sConnStr);
                    icon = Constant.FONTAWESOME_USER;
                    break;
                case "OTHER":
                    dt = new NaveoOneLib.WebSpecifics.LibData().dtGMDriver(UID, Driver.ResourceType.Other, sConnStr);
                    icon = Constant.FONTAWESOME_USER;
                    break;
                case "USER":
                    dt = new NaveoOneLib.WebSpecifics.LibData().dtGMUser(UID, false, sConnStr);
                    icon = Constant.FONTAWESOME_USER;
                    break;
                case "RULE":
                    dt = new NaveoOneLib.WebSpecifics.LibData().dtGMRule(UID, sConnStr);
                    icon = Constant.FONTAWESOME_USER;
                    break;
                case "VCA":
                    dt = new NaveoOneLib.WebSpecifics.LibData().dtGMVCA(sConnStr);
                    icon = Constant.FONTAWESOME_MAP;
                    break;

                case "MATRIX":
                    dt = new NaveoOneLib.WebSpecifics.LibData().dtGMMatrix(UID, sConnStr);
                    icon = Constant.FONTAWESOME_MAP;
                    break;

                case "OfficerAttendant":
                    List<Driver.ResourceType> lrt = new List<Driver.ResourceType>();
                    lrt.Add(Driver.ResourceType.Officer);
                    lrt.Add(Driver.ResourceType.Attendant);
                    dt = new NaveoOneLib.WebSpecifics.LibData().dtGMDriver(UID, lrt, true, sConnStr);
                    icon = Constant.FONTAWESOME_USER;
                    break;

                default:
                    dt = new NaveoOneLib.WebSpecifics.LibData().dtGMAsset(UID, sConnStr);
                    icon = Constant.FONTAWESOME_CAR;
                    break;
            }

            var initialTree = GetTreeView(dt);
            var jsTreeModel = GetJsTreeModel(initialTree, selectedElements, type, icon, isAllSelected, withoutChild);
            return jsTreeModel;
        }

        public List<JsTreeModel> GetRosterTreeViewFilter(int UID, List<int?> lAssets, String sConnStr)
        {
            DataTable dtGMAsset = new NaveoOneLib.WebSpecifics.LibData().dtGMAsset(UID, sConnStr);
            String icon = Constant.FONTAWESOME_CAR;

            DataTable dt = dtGMAsset.Clone();
            foreach (DataRow dr in dtGMAsset.Rows)
            {
                if (dr["StrucID"].ToString().Contains("-"))
                {
                    dt.Rows.Add(dr.ItemArray);
                    continue;
                }

                var item = lAssets.SingleOrDefault(x => x.Value == (int)dr["StrucID"]);
                if (item != null)
                    dt.Rows.Add(dr.ItemArray);
            }

            var initialTree = GetTreeView(dt);
            var jsTreeModel = GetJsTreeModel(initialTree, new List<string>(), String.Empty, icon);
            return jsTreeModel;
        }

        public List<TemporaryTree> GetTreeView(DataTable gm)
        {
            var temporaryList = new List<TemporaryTree>();
            foreach (DataRow row in gm.Rows)
            {
                if (row.RowState != DataRowState.Deleted)
                {
                    var temp = new TemporaryTree
                    {
                        GMID = Convert.ToInt32(row["GMID"].ToString()),
                        GMID_Description = row["GMDescription"].ToString(),
                        parentGMID = Convert.ToInt32(row["ParentGMID"].ToString()),
                        MID = Convert.ToInt32(row["StrucID"].ToString())
                    };
                    temporaryList.Add(temp);
                }
            }
            return temporaryList;
        }

        public List<JsTreeModel> GetJsTreeModel(List<TemporaryTree> listTemporaryTree, List<string> selectedElements, string type, string iconChild, bool allSelected = true, bool withoutChild = false)
        {
            var initialState = new JsTreeState();
            if (allSelected && (selectedElements == null || selectedElements.Count == 0))
            {
                initialState.selected = true;
            }
            var parents = new List<JsTreeModel>();
            var childs = new List<JsTreeModel>();
            var selectedParent = new List<string>();
            var reinitialize = false;
            foreach (var elmt in listTemporaryTree)
            {
                if (reinitialize)
                {
                    initialState = new JsTreeState
                    {
                        selected = false,
                    };
                    reinitialize = false;
                }
                #region Initialize preselected value
                if (selectedElements != null && selectedElements.Count > 0)
                {
                    var tmp = elmt;
                    var prop = "";
                    if (string.IsNullOrEmpty(type) || type == Constant.TREE_TYPE_PARENT)
                        prop = tmp.GMID.ToString();
                    else if (type == Constant.TREE_TYPE_CHILDREN)
                        prop = tmp.MID.ToString();

                    string isSelected;
                    if (type == Constant.TREE_TYPE_MIXED)
                    {
                        var SelectedItem = new List<string>();
                        bool isParent = false;

                        if (tmp.GMID > 0)
                        {
                            //Parent GMID
                            prop = tmp.GMID.ToString();

                            foreach (var x in selectedElements)
                            {
                                var temp = x.Split('|');
                                if (temp.Length > 1 && temp[1] == "GMID")
                                {
                                    SelectedItem.Add(temp[0]);
                                    isParent = true;
                                }
                            }
                        }
                        else
                        {
                            //child MID
                            prop = tmp.MID.ToString();
                            foreach (var x in selectedElements)
                            {
                                var temp = x.Split('|');
                                //  if (temp.Length > 1 && temp[1] == "GMID")
                                //     SelectedItem.Add(temp[0]);

                                if (temp.Length > 1 && temp[1] == "ID")
                                    SelectedItem.Add(temp[0]);
                            }
                        }

                        isSelected = SelectedItem.FirstOrDefault(x => x == prop);

                        if (isSelected != null)
                        {
                            initialState = new JsTreeState
                            {
                                selected = true,
                                opened = true
                            };
                            reinitialize = true;

                            if (isParent)
                                selectedParent.Add(prop);
                        }
                    }
                    else
                    {
                        isSelected = selectedElements.FirstOrDefault(x => x == prop);

                        if (isSelected != null)
                        {
                            initialState = new JsTreeState
                            {
                                selected = true,
                                opened = true
                            };
                            reinitialize = true;
                            selectedParent.Add(prop);
                        }
                    }
                }
                #endregion

                JsTreeModel tempModel;
                if (elmt.parentGMID < 0)
                {
                    var stateParent = new JsTreeState { opened = true, disabled = false, selected = allSelected };
                    if (reinitialize)
                    {
                        stateParent = initialState;
                    }
                    if (elmt.GMID < 0)
                        continue;
                    tempModel = new JsTreeModel
                    {
                        text = elmt.GMID_Description,
                        id = string.Concat("P", elmt.GMID.ToString()),
                        parent = "#",
                        icon = Constant.FONTAWESOME_FOLDER,
                        state = stateParent,
                    };
                    parents.Add(tempModel);
                }
                else
                {
                    if (!string.IsNullOrEmpty(elmt.GMID_Description))
                    {
                        if (elmt.GMID < 0)
                        {
                            if (withoutChild) continue;
                            tempModel = new JsTreeModel
                            {
                                text = elmt.GMID_Description,
                                parent = string.Concat("P", elmt.parentGMID.ToString()),
                                id = elmt.MID.ToString(),
                                icon = iconChild,
                                state = initialState
                            };
                            childs.Add(tempModel);
                        }
                        else
                        {
                            var prefix = (elmt.GMID_Description.Contains("DEFAULT")) ? "P" : "P"; // "D" : "P";
                            if (selectedParent.Contains(elmt.parentGMID.ToString()))
                            {
                                if (type == Constant.TREE_TYPE_MIXED)
                                {
                                    initialState = new JsTreeState
                                    {
                                        disabled = false,
                                        opened = true,
                                    };
                                }
                                else
                                {
                                    initialState = new JsTreeState
                                    {
                                        disabled = true,
                                        opened = true
                                    };
                                }
                                selectedParent.Add(elmt.GMID.ToString());
                                reinitialize = true;
                            }
                            if (string.Equals(prefix, "P") && selectedElements != null && selectedElements.Count > 0)
                                initialState.opened = true;
                            tempModel = new JsTreeModel
                            {
                                text = elmt.GMID_Description,
                                parent = string.Concat("P", elmt.parentGMID.ToString()),
                                id = string.Concat(prefix, elmt.GMID.ToString()),
                                icon = Constant.FONTAWESOME_FOLDER,
                                state = initialState
                            };
                            parents.Add(tempModel);
                        }
                    }
                }
            }

            foreach (JsTreeModel c in childs)
            {
                Boolean b = false;
                foreach (JsTreeModel p in parents)
                    if (p.id == c.parent)
                    {
                        b = true;
                        continue;
                    }

                if (!b)
                {
                    foreach (JsTreeModel p in parents)
                        if (p.parent == "#")
                        {
                            c.parent = p.id;
                            break;
                        }
                }
            }
            var result = parents.Concat(childs).ToList();
            return result;
        }
        #endregion
    }
}