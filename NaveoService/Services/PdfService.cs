﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using NaveoOneLib.Common;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.Permissions;
using NaveoService.Constants;
using NaveoService.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace NaveoService.Services
{

    public class PdfService
    {
        public string GetReportDetails(string reportName, DateTime DateFrom, DateTime DateTo)
        {
           // string s = string.Empty;
            return reportName +","+DateFrom.ToShortDateString()+ ","+ DateTo.ToShortDateString();
        }

        public static string reportDetails = "";

        public BaseModel GeneratePDF(DataSet dsDataTable, string entity, DateTime DateFrom, DateTime DateTo, SecurityToken securityToken)
        {
            BaseModel bM = new BaseModel();
            Boolean b = false;
            Totals totals = new Totals();
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 50f, 40f);
            try
            {

                DataTable dt = new DataTable();
                DataTable dtTotals = new DataTable();
                string displayReportName = string.Empty;
                string reportName = string.Empty;
                if (entity == "TRIPS")
                {
                    reportName = "Trip_History_Report";
                    displayReportName = "Trip History Report";

                     dt = dsDataTable.Tables["dtTrips"];
                     dtTotals = dsDataTable.Tables["Totals"];
                }


                if (entity == "LIVE")
                {
                    reportName = "Live_Report";
                    displayReportName = "Live Report";
                    dt = dsDataTable.Tables["dtLive"];
                }


                if(entity == "DAILY_SUMMARIZED_TRIPS")
                {
                    reportName = "DAILY_SUMMARIZED_TRIPS";
                    displayReportName = "Daily Summarized Report";
                    dt = dsDataTable.Tables["Report"];
                    dtTotals = dsDataTable.Tables["Totals"];
                }


                if(entity == "SUMMARIZED_TRIPS")
                {
                    reportName = "SUMMARIZED_TRIPS";
                    displayReportName = "Periodic Vehicle Summary Report";
                    dt = dsDataTable.Tables["dtReports"];
             
                }


                if (entity == "DAILY_TRIP_TIME")
                {
                    reportName = "DAILY_TRIP_TIME";
                    displayReportName = "Daily Engine HOURS /Kms Report";
                    dt = dsDataTable.Tables["dtReports"];

                }

                if(entity == "Zone_Visited_NotVisited")
                {
                    reportName = "Zone_Visited_NotVisited";
                    displayReportName = "Zone Visited Report";
                    dt = dsDataTable.Tables["dtZoneVisited"];

                }

                if(entity == "EXCEPTION_LIST")
                {
                    reportName = "EXCEPTION_LIST";
                    displayReportName = "Exception Report";
                    dt = dsDataTable.Tables["dtExceptionHeader"];


                }


                if (entity == "Temperature_Report")
                {
                    reportName = "Temperature_Report";
                    displayReportName = "Temperature Report";
                    dt = dsDataTable.Tables["dtTemperature"];


                }


                if(entity == "Customized_Temperature_Report")
                {
                    reportName = "Customized_Temperature_Report";
                    displayReportName = "Customized Temperature Report";
                    dt = dsDataTable.Tables["dtCustTemperature"];
                }


                if(entity == "Fuel_Report")
                {
                    reportName = "CustomizedFuel_Report_Temperature_Report";
                    displayReportName = "Fuel Report";
                    dt = dsDataTable.Tables["dtFuelReport"];
                }

                if (entity == "FUEL")
                {
                    reportName = "Fuel Graph Report";
                    displayReportName = "Fuel Graph Report";
                    dt = dsDataTable.Tables["dtFuelgraphReport"];
                }


                if (entity == "AuxiliaryReport")
                {
                    reportName = "Auxiliary_Report";
                    displayReportName = "Auxiliary Report";
                    dt = dsDataTable.Tables["dtAuxReport"];
                }


                if (entity == "AmiranReport")
                {
                    reportName = "Amiran_Fleet_Report";
                    displayReportName = "Amiran Fleet Report";
                    dt = dsDataTable.Tables["dtAmiranReport"];
                }

                if (entity == "MOLG_CustomizedReport")
                {
                    reportName = "MOLG_CustomizedReport";
                    displayReportName = "MOLG Customized Report";
                    dt = dsDataTable.Tables["dtMolgReport"];
                }


                if (entity == "Planning_Report")
                {
                    reportName = "Planning_Report";
                    displayReportName = "Planning Request Report";
                    dt = dsDataTable.Tables["dtPlanningReport"];
                }

                if (entity == "AccidentManagement")
                {
                    reportName = "AccidentManagement_Report";
                    displayReportName = "Accident management Report";
                    dt = dsDataTable.Tables["AccidentManagement"];
                }



                if (entity == "Shift")
                {
                    reportName = "Shift_Report";
                    displayReportName = "Shift Report";
                    dt = dsDataTable.Tables["dtShift"];
                }

                if (entity == "Roster")
                {
                    reportName = "Roster_Report";
                    displayReportName = "Roster Report";
                    dt = dsDataTable.Tables["dtRoster"];
                }

                if (entity == "PlanningExceptions")
                {
                    reportName = "Planning_Exceptions";
                    displayReportName = "Planning Exceptions Report";
                    dt = dsDataTable.Tables["dtPlanningExceptions"];
                }

                if (entity == "FuelExceptions")
                {
                    reportName = "Fuel_Exceptions";
                    displayReportName = "Fuel Exceptions Report";
                    dt = dsDataTable.Tables["dtFuelExceptions"];
                }


              reportDetails = GetReportDetails(displayReportName, DateFrom, DateTo);


                string exportFilesPath = Path.Combine(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings[ConfigurationKeys.ExportLocation]), securityToken.UserToken.ToString());
                // Determine whether the directory exists.
                if (!Directory.Exists(exportFilesPath))
                    Directory.CreateDirectory(exportFilesPath);

                string filename = FileHelper.NameFormat(reportName, FileSystem.Extension.PDF);

                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, new FileStream(exportFilesPath + "\\" + filename, FileMode.Create));
                writer.PageEvent = new HeaderFooter();
                
              

                pdfDoc.SetPageSize(PageSize.A4.Rotate());
                pdfDoc.Open();

                Font font8 = FontFactory.GetFont("ARIAL", 7);
                Font font6 = FontFactory.GetFont("ARIAL", 6);

                
                

               


                if (dt != null)
                {
                    //Craete instance of the pdf table and set the number of column in that table

                    int columncount = dt.Columns.Count;
                    PdfPTable PdfTable = new PdfPTable(columncount);

                    foreach (DataColumn c in dt.Columns)
                    {
                
                        PdfTable.AddCell(new Phrase(c.ColumnName, font8));
                    }

                    PdfPCell PdfPCell = null;
                    
                    for (int rows = 0; rows < dt.Rows.Count; rows++)
                    {
                        
                        for (int column = 0; column < dt.Columns.Count; column++)
                        {

                            PdfPCell = new PdfPCell(new Phrase(new Chunk(dt.Rows[rows][column].ToString(), font6)));
                            
                           

                            #region Trips
                            if (entity == "TRIPS")
                            {
                                string colorDZ = dt.Rows[rows][2].ToString();
                                string colorAZ = dt.Rows[rows][5].ToString();

                                if (colorDZ == "")
                                    colorDZ = "#FFFFFF";

                                if (colorAZ == "")
                                    colorAZ = "#FFFFFF";

                                if (column == 3)
                                {
                                    PdfPCell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml(colorDZ));
                                } 

                                if (column == 6)
                                {
                                    PdfPCell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml(colorAZ));
                                }
                                


                            }

                            #endregion

                            PdfTable.AddCell(PdfPCell);

                        }
                    }

                       #region DailySummarizedTrips

                    //if (entity == "DAILY_SUMMARIZED_TRIPS")
                    //{

                    //    for (int column = 0; column < dt.Columns.Count; column++)
                    //    {

                    //        if (column == 3)
                    //        {
                    //             string tripsTotal = dtTotals.Rows[0][0].ToString();
                    //             PdfPCell  PdfPCell1 = new PdfPCell(new Phrase(new Chunk(tripsTotal, font6)));
                    //             PdfTable.AddCell(PdfPCell1);

                    //        }

                    //    }

                    //}
                        #endregion

                        pdfDoc.Add(PdfTable); // add pdf table to the document   
                }
                pdfDoc.Close();
                bM.data = b;
            }
            catch (DocumentException de)
            {
                bM.data = b;
                bM.errorMessage = de.ToString();
                return bM;
            }
            catch (IOException ioEx)
            {
                bM.data = b;
                bM.errorMessage = ioEx.ToString();
                return bM;
            }
            catch (Exception ex)
            {
                bM.data = b;
                bM.errorMessage = ex.ToString();
                return bM;
            }
            return bM;
        }


        public class HeaderFooter : PdfPageEventHelper
        {
            public string variable = reportDetails;
            public override void OnEndPage(PdfWriter writer, Document document)
            {
                //string exportFilesPath = Path.Combine(HttpContext.Current.Server.MapPath("/Content/"), "logo-naveo.png");
                //string imageFilePath = exportFilesPath;
                //iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imageFilePath);
                ////Resize image depend upon your need   
                //jpg.ScaleToFit(110, 110);
                //jpg.SetAbsolutePosition(770, 550);

                ////jpg.Alignment = Element.ALIGN_RIGHT;
                //PdfContentByte cB = new PdfContentByte(writer);
                //cB.AddImage(jpg);
                //writer.DirectContent.AddImage(jpg);

                //base.OnEndPage(writer, document);


                PdfPTable tHeader = new PdfPTable(3);
                tHeader.TotalWidth = document.PageSize.Width - document.LeftMargin - document.RightMargin;
                tHeader.DefaultCell.Border = 0;

                tHeader.AddCell(new Paragraph());

                string[] list = reportDetails.Split(',');
                string reportName = list[0];
                string dateFrom  = list[1];
                string dateTo= list[2];
              

                string text = reportName+"@"+ " From : "+ dateFrom + " To : "+ dateTo;
                text = text.Replace("@", System.Environment.NewLine);

                Paragraph p1 = new Paragraph(text, new Font(Font.FontFamily.TIMES_ROMAN, 10));
                p1.Alignment = Element.ALIGN_LEFT;
                PdfPCell _cell = new PdfPCell();
                _cell.AddElement(p1);
                _cell.PaddingLeft = -270f;
                _cell.Border = 0;
                // _cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;

                tHeader.AddCell(_cell);
                tHeader.AddCell(new Paragraph());
                tHeader.WriteSelectedRows(0, -1, document.LeftMargin, writer.PageSize.GetTop(document.TopMargin) + 40, writer.DirectContent);

                PdfPTable tFooter = new PdfPTable(3);
                tFooter.TotalWidth = document.PageSize.Width - document.LeftMargin - document.RightMargin;
                tFooter.DefaultCell.Border = 0;
                tFooter.AddCell(new Paragraph());
                DateTime localDate = DateTime.Now;

                Paragraph p2 = new Paragraph("Date Extraced: " + localDate.ToString(), new Font(Font.FontFamily.TIMES_ROMAN, 10));
                _cell = new PdfPCell(p2);
                _cell.Border = 0;
                _cell.PaddingLeft = -270f;
                //_cell.HorizontalAlignment = Element.ALIGN_LEFT;
                tFooter.AddCell(_cell);

                Paragraph p3 = new Paragraph("Page " + writer.PageNumber, new Font(Font.FontFamily.TIMES_ROMAN, 10));
                _cell = new PdfPCell(p3);
                _cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                _cell.Border = 0;
                tFooter.AddCell(_cell);
                tFooter.WriteSelectedRows(0, -1, document.LeftMargin, writer.PageSize.GetBottom(document.BottomMargin) - 5, writer.DirectContent);
            }
        }

        public class Totals
        {
            public double subtotal = 0;
            public double total = 0;
        }

        //public Totals getTotals(DataTable dt )
        //{
        //    Totals t = new Totals();
             

        //    return 
        //}

    

        public class SubTotalEvent : IPdfPCellEvent
        {
            Double price;
            Totals totals;
            bool printTotals = false;       //If we just whant to print out the Subtotal, this will be true.
            bool CellWrittenOnce = false;   //Bool to SUM price just once (if row flows to a second page)

            public SubTotalEvent(Totals totals, double price)
            {
                printTotals = false;
                this.totals = totals;
                this.price = price;
            }

            public SubTotalEvent(Totals totals)
            {
                this.totals = totals;
                printTotals = true;
            }

            public void CellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases)
            {
                if (printTotals)
                {
                    PdfContentByte canvas = canvases[PdfPTable.TEXTCANVAS];
                    ColumnText.ShowTextAligned(canvas, Element.ALIGN_LEFT, new Phrase(totals.subtotal.ToString()), position.GetLeft(0) + 2, position.GetBottom(0) + 2, 0);
                    return;
                }
                if (!CellWrittenOnce)
                {
                    totals.subtotal += price;
                    totals.total += price;
                }
                CellWrittenOnce = true;
            }
        }
    }
}
