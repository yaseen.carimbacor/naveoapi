﻿using NaveoOneLib.Models;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Models.Users;
using NaveoOneLib.Services.Permissions;
using NaveoOneLib.Services.Users;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace NaveoService.Services
{
    public class dbUpdateService
    {
        public void dbUpdate(String sConnStr)
        {
            new NaveoOneLib.DBCon.SystemInit().UpdateDB(sConnStr);
            updateDBData(sConnStr);
        }

        void updateDBData(String sConnStr)
        { }

        void ExamplesForDll()
        {
            String sConnStr = String.Empty;
            RolesService roleService = new RolesService();
            #region Roles
            if (String.IsNullOrEmpty("XXX"))
            {
                Role ro = new Role();
                ro.Description = "Pena Role";
                GroupMatrix gmRoot = new GroupMatrix();
                gmRoot = new NaveoOneLib.Services.GMatrix.GroupMatrixService().GetRoot(sConnStr);

                Matrix m = new Matrix();
                m.GMID = gmRoot.gmID;
                m.oprType = DataRowState.Added;
                m.iID = ro.RoleID;
                ro.lMatrix = new List<Matrix>();
                ro.lMatrix.Add(m);

                RolePermission r = new RolePermission();
                r.RoleID = ro.RoleID;
                r.PermissionID = 1;
                r.iCreate = 1;
                r.oprType = DataRowState.Added;
                ro.lPermissions = new List<RolePermission>();
                ro.lPermissions.Add(r);

                roleService.SaveRoles(ro, true, sConnStr);
            }
            #endregion

            #region User
            if (String.IsNullOrEmpty("XXX"))
            {
                User u = new User();
                u.Email = "wawa@naveo.mu";
                u.Password = "Test1234.";
                u.Names = "sss";
                u.Status_b = "AC";
                u.LoginCount = 0;
                u.MaxDayMail = 1;

                GroupMatrix gmRoot = new GroupMatrix();
                gmRoot = new NaveoOneLib.Services.GMatrix.GroupMatrixService().GetRoot(sConnStr);

                Matrix m = new Matrix();
                m.GMID = gmRoot.gmID;
                m.oprType = DataRowState.Added;
                m.iID = u.UID;
                u.lMatrix.Add(m);

                Role r = new Role();
                r.RoleID = 1;
                r.oprType = DataRowState.Added;
                u.lRoles.Add(r);

                BaseModel b = new NaveoOneLib.Services.Users.UserService().SaveUser(u, true, sConnStr);
            }
            #endregion

            //Example for new modules\permissions. actually in dll
            #region new modules\permissions
            NaveoModulesService naveoModulesService = new NaveoModulesService();
            NaveoModule n = naveoModulesService.GetNaveoModulesByName("ADMIN", sConnStr);
            if (n != null)
            {
                Boolean b = false;
                foreach (Permission perm in n.lPermissions)
                    if (perm.ControllerName == "User")
                        b = true;
                if (!b)
                {
                    if (n.lMatrix.Count == 0)
                    {
                        GroupMatrix gmRoot = new GroupMatrix();
                        gmRoot = new NaveoOneLib.Services.GMatrix.GroupMatrixService().GetRoot(sConnStr);

                        Matrix m = new Matrix();
                        m.GMID = gmRoot.gmID;
                        m.oprType = DataRowState.Added;
                        m.iID = n.iID;
                        n.lMatrix.Add(m);
                    }

                    Permission p = new Permission();
                    p.ControllerName = "User";
                    p.ModuleID = n.iID;
                    p.oprType = DataRowState.Added;
                    n.lPermissions.Add(p);
                    naveoModulesService.SaveNaveoModules(n, false, sConnStr);
                }
            }

            n = naveoModulesService.GetNaveoModulesByName("GIS", sConnStr);
            if (n == null)
            {
                n = new NaveoModule();
                n.description = "GIS";
                n.lMatrix = new List<Matrix>();
                n.lPermissions = new List<Permission>();

                GroupMatrix gmRoot = new GroupMatrix();
                gmRoot = new NaveoOneLib.Services.GMatrix.GroupMatrixService().GetRoot(sConnStr);

                Matrix m = new Matrix();
                m.GMID = gmRoot.gmID;
                m.oprType = DataRowState.Added;
                n.lMatrix.Add(m);

                Permission p = new Permission();
                p.ControllerName = "Map";
                p.oprType = DataRowState.Added;
                n.lPermissions.Add(p);

                naveoModulesService.SaveNaveoModules(n, true, sConnStr);
            }
            #endregion
        }
    }
}