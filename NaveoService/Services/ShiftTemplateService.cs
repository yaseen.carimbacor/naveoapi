﻿using NaveoOneLib.Models.Common;
using NaveoOneLib.Services.GMatrix;
using NaveoService.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Services
{
    public class ShiftTemplateService
    {


        public BaseModel SaveUpdateShiftTemplate(Guid sUserToken, string sConnStr, ShiftTemplateDTO shiftTemplateDTO, Boolean bInsert)
        {


            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.Shifts.ShiftTemplate> lShiftTemplate = new List<NaveoOneLib.Models.Shifts.ShiftTemplate>();


            foreach (var x in shiftTemplateDTO.shiftTemplate)
            {
                NaveoOneLib.Models.Shifts.ShiftTemplate dllShiftTemplate = new NaveoOneLib.Models.Shifts.ShiftTemplate();
                dllShiftTemplate.iID = dllShiftTemplate.iID;
                dllShiftTemplate.OrderNo = x.OrderNo;
                dllShiftTemplate.SID = x.ShiftType.id;
                dllShiftTemplate.oprType = x.oprType;
                dllShiftTemplate.Description = x.Description;

                lShiftTemplate.Add(dllShiftTemplate);
            }

            BaseModel bD = new BaseModel();
            bD.data = new NaveoOneLib.Services.Shifts.ShiftTemplateService().SaveShiftTemplateList(lShiftTemplate, bInsert, UID, sConnStr);

            return bD.data;

        }


        public BaseModel GetShiftTemplateDetailsByGroupdId(int groupId , string sConnStr)
        {
            BaseModel bD = new BaseModel();
            bD.data = new NaveoOneLib.Services.Shifts.ShiftTemplateService().GetShiftTemplateWithShiftDetailsByGroupId(groupId,sConnStr);

            return bD;
        }



    }
}
