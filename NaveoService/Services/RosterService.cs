﻿using NaveoOneLib.Models.Common;
using NaveoOneLib.Services.GMatrix;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NaveoService.Models.DTO;
using NaveoService.Models;
using NaveoOneLib.Models.Shifts;

namespace NaveoService.Services
{
    public class RosterService
    {
        public dtData GetRoster(DateTime dtFrom ,DateTime dtTo,Guid sUserToken, string sConnStr, int? Page, int? LimitPerPage, List<String> sortColumns, Boolean sortOrderAsc)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);

            DataTable dtRoster = new NaveoOneLib.Services.Shifts.RosterService().GetRoster(dtFrom,dtTo,lm, sConnStr, sortColumns, sortOrderAsc);
            
       

            DataTable ResultTable = dtRoster.Clone();
            ResultTable.Columns.Add("rowNum", typeof(int));
            ResultTable.Columns["rowNum"].AutoIncrement = true;
            ResultTable.Columns["rowNum"].AutoIncrementSeed = 1;
            ResultTable.Columns["rowNum"].AutoIncrementStep = 1;
            foreach (DataRow dr in dtRoster.Rows)
                ResultTable.Rows.Add(dr.ItemArray);

            int iTotalRec = ResultTable.Rows.Count;
            Pagination p = Pagination.GetRowNums(Page, LimitPerPage);
            DataTable dt = new DataTable();
            DataRow[] drChk = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo);
            if (drChk != null && drChk.Length > 0)
                dtRoster = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo).CopyToDataTable();
            else
                dtRoster = ResultTable;

            dtData dtR = new dtData();

            if (dtRoster.Columns.Contains("sid"))
            {
                dtRoster.Columns["sid"].ColumnName = "SID";
            }

            if (dtRoster.Columns.Contains("rosterperiod"))
            {
                dtRoster.Columns["rosterperiod"].ColumnName = "RosterPeriod";
            }



            if (dtRoster.Columns.Contains("sname"))
            {
                dtRoster.Columns["sname"].ColumnName = "sName";
            }

            if (dtRoster.Columns.Contains("rosterdate"))
            {
                dtRoster.Columns["rosterdate"].ColumnName = "RosterDate";
            }

            if (dtRoster.Columns.Contains("rostername"))
            {
                dtRoster.Columns["rostername"].ColumnName = "RosterName";
            }

            if (dtRoster.Columns.Contains("driverid"))
            {
                dtRoster.Columns["driverid"].ColumnName = "DriverID";
            }

            if (dtRoster.Columns.Contains("driverid"))
            {
                dtRoster.Columns["driverid"].ColumnName = "DriverID";
            }

            if (dtRoster.Columns.Contains("resourcename"))
            {
                dtRoster.Columns["resourcename"].ColumnName = "ResoourceName";
            }

            if (dtRoster.Columns.Contains("assetid"))
            {
                dtRoster.Columns["assetid"].ColumnName = "AssetID";
            }

            if (dtRoster.Columns.Contains("asset"))
            {
                dtRoster.Columns["asset"].ColumnName = "Asset";
            }

            if (dtRoster.Columns.Contains("createddate"))
            {
                dtRoster.Columns["createddate"].ColumnName = "CreatedDate";
            }

            if (dtRoster.Columns.Contains("dtfrom"))
            {
                dtRoster.Columns["dtfrom"].ColumnName = "dtFrom";
            }

            if (dtRoster.Columns.Contains("dtto"))
            {
                dtRoster.Columns["dtto"].ColumnName = "dtTo";
            }

            if (dtRoster.Columns.Contains("sremarks"))
            {
                dtRoster.Columns["sremarks"].ColumnName = "sRemarks";
            }

            if (dtRoster.Columns.Contains("grid"))
            {
                dtRoster.Columns["grid"].ColumnName = "GRID";
            }



            dtR.Data = dtRoster;
            dtR.Rowcnt = iTotalRec;

            return dtR;
        }

        public BaseModel GetRosterByGroupId(Guid sUserToken, int groupId, string sConnStr)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);

            NaveoOneLib.Models.Shifts.Roster roster = new NaveoOneLib.Services.Shifts.RosterService().GetRosterById(groupId.ToString(), sConnStr);

            Models.DTO.rosterheader apiRoster = new Models.DTO.rosterheader();
            #region RosterHeader
            if (roster != null)
            {
                apiRoster.ild = (int)roster.iID;
                apiRoster.rosterName = roster.RosterName;
                apiRoster.dtFrom = roster.dtFrom;
                apiRoster.dtTo = roster.dtTo;
                apiRoster.sRemarks = roster.sRemarks;

                #region rosterDetails
                List<rosterdetails> lRosterDetails = new List<rosterdetails>();
                foreach (RosterDetails rD in roster.lRosterDetails)
                {
                    rosterdetails api = new rosterdetails();
                    api.rid = rD.RID;
                    api.grid = rD.GRID;
                    api.rDate = rD.rDate;
                    api.shifttemplateId = rD.ShiftTemplateId;
                    api.shifttemplatedesc = rD.ShiftTemplateDesc;

                    NaveoService.Models.Custom.minifiedValues minifiedValues = new NaveoService.Models.Custom.minifiedValues();

                    if(rD.SID != 0)
                    {
                        minifiedValues.id = Convert.ToInt32(rD.SID);
                        minifiedValues.description = rD.Shiftdesc;
                        api.shiftType = minifiedValues;

                    } else if (rD.ShiftTemplateId != 0)
                    {
                        minifiedValues.id = Convert.ToInt32(rD.ShiftTemplateId);
                        minifiedValues.description = rD.ShiftTemplateDesc;
                        api.shiftType = minifiedValues;
                    }
                  


                    lRosterDetails.Add(api);

                }

                apiRoster.lrosterDetails = lRosterDetails;

                #endregion

                #region rosterResource
                List<rosterResource> lRosterResource = new List<rosterResource>();
                foreach (RosterResources rR in roster.lResources)
                {
                    rosterResource api = new rosterResource();
                    api.rid = (int)rR.RID;
                    api.iId = (int)rR.iID;

                    NaveoService.Models.Custom.minifiedValues minifiedResources = new NaveoService.Models.Custom.minifiedValues();
                    minifiedResources.id = Convert.ToInt32(rR.DriverID);
                    minifiedResources.description = rR.DriverName;
                    api.minifiedResources = minifiedResources;

                    lRosterResource.Add(api);

                }

                apiRoster.lrosterResource = lRosterResource;

                #endregion

                #region Asset
                rosterAsset rA = new rosterAsset();
                rA.ild = (int)roster.rosterAsset.iID;
                rA.rid = (int)roster.rosterAsset.RID;

                NaveoService.Models.Custom.minifiedValues minifiedAssets = new NaveoService.Models.Custom.minifiedValues();
                minifiedAssets.id = Convert.ToInt32(roster.rosterAsset.AssetID);
                minifiedAssets.description = roster.rosterAsset.AssetName;
                rA.minifiedAssets = minifiedAssets;

                apiRoster.rosterAsset = rA;

                #endregion

                #region Matrix
                apiRoster.lMatrix = roster.lMatrix;
                #endregion

            }



            #endregion



            BaseModel dtR = new BaseModel();
            dtR.data = apiRoster;
            return dtR;
        }

        public Boolean SaveUpdateRoster (SecurityTokenExtended securityToken, string sConnStr, RosterDTO rosterDto, Boolean bInsert)
        {
            int UID = CommonService.GetUserID(securityToken.securityToken.UserToken, sConnStr);
            NaveoOneLib.Models.Shifts.Roster dllroster = new NaveoOneLib.Models.Shifts.Roster();

            var x = rosterDto;
            dllroster.iID =x.roster.ild;
            dllroster.RosterName = x.roster.rosterName;
            dllroster.dtFrom = x.roster.dtFrom;
            dllroster.dtTo = x.roster.dtTo;
            dllroster.sRemarks = x.roster.sRemarks;
            dllroster.oprType = x.roster.oprType;

            if (x.roster.iLocked.HasValue)
                dllroster.isLocked = x.roster.iLocked;

            if (x.roster.iOriginal.HasValue)
                dllroster.isOriginal = x.roster.iOriginal;



            foreach (var rosterDetails in x.roster.lrosterDetails)
            {

                NaveoOneLib.Models.Shifts.RosterDetails dllrosterDetils = new NaveoOneLib.Models.Shifts.RosterDetails();

             if (rosterDetails.shiftType.id != 0)
                { 
                    dllrosterDetils.RID = rosterDetails.rid;
                dllrosterDetils.GRID = x.roster.ild;

                //dllrosterDetils.RID = rosterDetails.grid;

                dllrosterDetils.SID = rosterDetails.shiftType.id;

                dllrosterDetils.rDate = rosterDetails.rDate;

                if (dllrosterDetils.SID == 0)
                    rosterDetails.oprType = 0;

                dllrosterDetils.oprType = rosterDetails.oprType;

                dllroster.lRosterDetails.Add(dllrosterDetils);

                }
                
            }


            foreach (var rosterResource in x.roster.lrosterResource)
            {

                NaveoOneLib.Models.Shifts.RosterResources dllrosterResource = new NaveoOneLib.Models.Shifts.RosterResources();
                dllrosterResource.iID = rosterResource.iId;
                dllrosterResource.RID = rosterResource.rid;

                dllrosterResource.DriverID = rosterResource.minifiedResources.id;
                dllrosterResource.oprType = rosterResource.oprType;
              
                dllroster.lResources.Add(dllrosterResource);

                
            }



            dllroster.rosterAsset.iID = x.roster.rosterAsset.ild;
            dllroster.rosterAsset.RID = x.roster.rosterAsset.rid;
            dllroster.rosterAsset.AssetID = x.roster.rosterAsset.minifiedAssets.id;
            dllroster.rosterAsset.oprType = x.roster.rosterAsset.oprType;



            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, securityToken.securityToken.sConnStr);
            #region Matrix
            if (bInsert)
            {
               
                foreach(NaveoOneLib.Models.GMatrix.Matrix mm in lm)
                {

                   
                    NaveoOneLib.Models.GMatrix.Matrix m = new NaveoOneLib.Models.GMatrix.Matrix();
                    m.GMDesc = mm.GMDesc;
                    m.GMID = mm.GMID;
                    m.iID = x.roster.ild;
                    m.MID = 0;
                    m.oprType = DataRowState.Added;
                    dllroster.lMatrix.Add(m);
                }
            }
            else
            {

                foreach (NaveoOneLib.Models.GMatrix.Matrix mm in lm)
                {
                    foreach(var apiMatrix in x.roster.lMatrix)
                    {

                        NaveoOneLib.Models.GMatrix.Matrix m = new NaveoOneLib.Models.GMatrix.Matrix();
                        m.GMDesc = apiMatrix.GMDesc;
                        m.GMID = mm.GMID;
                        m.iID = apiMatrix.iID;
                        m.MID = apiMatrix.MID;

                        if (apiMatrix.GMID != mm.GMID)
                        {
                          
                            m.oprType = DataRowState.Modified;
                            

                        } else
                        {

                            m.oprType = DataRowState.Unchanged;
                        }


                        dllroster.lMatrix.Add(m);

                    }

                  
                      
                   
                }

            }
            #endregion


            Boolean saveRoster = new NaveoOneLib.Services.Shifts.RosterService().SaveRoster(dllroster, UID,bInsert, sConnStr);

            BaseModel bD = new BaseModel();
            bD.data = saveRoster;
            return bD.data;

        }
    }
}
