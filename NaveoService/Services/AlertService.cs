﻿using NaveoOneLib.DBCon;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Services.GMatrix;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Services
{
    public class AlertService
    {

        //#region  ALERT ON PLANNING


        //public BaseModel GenerateAlertPlanning(List<Matrix> lm,int ruleid, List<string> lalertValue,List<int> LAssetIds ,string sConnStr)
        //{
        //    Guid sUserToken = new Guid("ED20F533-2919-465B-AC9A-4D6DDC125B1F");


        //    //Variables
        //    #region variables

        //    DataTable dt = new DataTable();
        //    dt.Columns.Add("ScheduleID");
        //    dt.Columns.Add("PID");
            
        //    dt.Columns.Add("RuleName");
        //    dt.Columns.Add("AssetID");
        //    dt.Columns.Add("Asset");
        //    dt.Columns.Add("DriverID");
        //    dt.Columns.Add("Driver");
        //    dt.Columns.Add("ViaPoint");
        //    dt.Columns.Add("ExceptedTime");
        //    dt.Columns.Add("Buffer");
        //    dt.Columns.Add("ActualTime");
        //    dt.Columns.Add("DurationInsideZone");
        //    dt.Columns.Add("Status");
        //    BaseModel bM = new BaseModel();


        //    #endregion


        //    //build new datatable to be triggered as exceptions and save also on table exceptions
        //    #region build new datatable to be triggered as exceptions and save also on table exceptions
        //    //DateTime dtFrom = DateTime.Now.ToUniversalTime();
        //    //DateTime dtTo = DateTime.Now.ToUniversalTime();
        //    //2020-02-26 02:15:38.000 
        //    dtData mydtData = new NaveoService.Services.PlanningService().GetPlanningActual(sUserToken, lm, new DateTime(2020, 05, 04, 03, 00, 00), new DateTime(2020, 05, 05, 00, 00, 00), LAssetIds, null,sConnStr);

        //    mydtData.Data.DefaultView.Sort = "ScheduledID asc";
        //    mydtData.Data = mydtData.Data.DefaultView.ToTable();

        //    foreach (DataRow dr in mydtData.Data.Rows)
        //    {
        //        int count = -1;
        //        String Status = dr["Status"].ToString().ToLower();
        //        String Time = dr["Time"].ToString().ToLower();
        //        String ScheduleId = dr["ScheduledID"].ToString();
        //        String PID = dr["PID"].ToString();

        //        if (Status == string.Empty)
        //            Status = "Not Visited";

        //        Status = Status.ToLower();
        //        string[] splitstatus = Status.Split(',');
                

        //        string[] splittime = Time.Split(',');
                
                
        //        foreach (var x in lalertValue)
        //        {
        //            //count++;

        //            var filteredstatus= (from ss in splitstatus
        //                                 where ss.Equals(x.ToLower())
        //                                 select ss).FirstOrDefault();

        //            if (filteredstatus != null)
        //            {
        //                count++;

        //                DataRow newRow = dt.NewRow();
        //                newRow["RuleName"] = ruleid.ToString();
        //                newRow["ScheduleID"] = ScheduleId;
        //                newRow["PID"] = PID;
        //                newRow["Status"] = filteredstatus;
        //                newRow["AssetID"] = dr["AssetID"].ToString();
        //                newRow["DriverID"] = dr["DriverID"].ToString();
        //                newRow["Asset"] = dr["AssetName"].ToString();
        //                newRow["Driver"] = dr["DriverName"].ToString();
        //                newRow["ViaPoint"] = dr["ZoneName"].ToString();
        //                newRow["ExceptedTime"] = dr["dtUTC"].ToString();
        //                newRow["Buffer"] = dr["Buffer"].ToString();
        //                newRow["ActualTime"] = splittime[count].ToString();
        //                newRow["DurationInsideZone"] = dr["Duration"].ToString();

        //                dt.Rows.Add(newRow);
        //            }


        //        }



        //    }
        //    #endregion


          

        //    bM.data = dt;
        //    return bM;
           

        //}

        //#endregion


        //#region ALERT ON MAINTENANCE


        //public BaseModel GenerateAlertMaintenance(int ruleId, List<int> luri, String sConnStr)
        //{
        //    BaseModel bm = new BaseModel();

        //    NaveoService.Services.MaintAssetServices maintAssetService = new NaveoService.Services.MaintAssetServices();

        //    BaseModel bmRecurMaint = new NaveoService.Services.MaintAssetServices().GetRecurringMaint();
        //    DataTable dtRecurData = bmRecurMaint.data;
        //    List<String> lAssetName = new List<string>();
        //    List<String> lMaintType = new List<string>();

        //    foreach (var x in luri)
        //    {

        //        var assetName = dtRecurData.AsEnumerable().Where(recur => recur.Field<int>("URI") == x).Select(r => r.Field<string>("Number")).First();
        //        var maintypeName = dtRecurData.AsEnumerable().Where(recur => recur.Field<int>("URI") == x).Select(r => r.Field<string>("Description")).First();


        //        lAssetName.Add(assetName);
        //        lMaintType.Add(maintypeName);

        //    }






        //    BaseModel mmm = maintAssetService.GetMaintenanceStatus(luri,lAssetName,lMaintType);
        //    DataTable dt = mmm.data;
        //    dt.Columns.Add("RuleID");


        //    DataTable dtRulesByPlanningStruc = new NaveoOneLib.Services.Rules.RulesService().GetRulesbyStruc("AssetMaintenance", sConnStr);

        //    foreach (DataRow dr in dt.Rows)
        //    {

        //        //var xx = luri.Where(a ==)

        //        dr["RuleID"] = ruleId;
        //    }

        //    bm.data = dt;
               
        //    return bm;

        //}

        //#endregion

    }
}
