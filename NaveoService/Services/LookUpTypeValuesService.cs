﻿using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.Lookups;
using NaveoOneLib.Models.Zones;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Services
{

   public  class LookUpTypeValuesService
    {

        #region lookuptypes
        public dtData GetLookUpTypes(string sConnStr)
        {
             dtData dtd = new dtData();
             DataTable dt = new NaveoOneLib.Services.Lookups.LookUpTypesService().GetLookUpTypes(sConnStr);
             dtd.Data = dt;


            if (dt.Columns.Contains("Description"))
                dt.Columns["Description"].ColumnName = "description";


            if (dt.Columns.Contains("Code"))
                dt.Columns["Code"].ColumnName = "code";



            if (dt.Columns.Contains("TID"))
                dt.Columns["TID"].ColumnName = "tid";

       

            return dtd;
        }


        public BaseModel GetLookUpTypesId(Guid sUserToken, string sConnStr,int lookupTypeId)
        {

            BaseModel dt = new  BaseModel();

            LookUpTypes lookuptypes = new NaveoOneLib.Services.Lookups.LookUpTypesService().GetLookUpTypesById(lookupTypeId.ToString(),sConnStr);

            dt.data = lookuptypes;

            return dt;
        }


        public Boolean SaveLookUpTypes(LookUpTypes uLookUpTypes, Boolean bInsert, String sConnStr)
        {
        
            Boolean b = new NaveoOneLib.Services.Lookups.LookUpTypesService().SaveLookUpTypes(uLookUpTypes,bInsert,sConnStr);

            return b;

        }



        public Boolean UpdateLookUpTypes(LookUpTypes uLookUpTypes, Boolean bInsert, String sConnStr)
        {

            Boolean b = new NaveoOneLib.Services.Lookups.LookUpTypesService().SaveLookUpTypes(uLookUpTypes, bInsert, sConnStr);

            return b;

        }


        #endregion


        #region 
        public dtData GetLookUpValuesByLTName(string name , string sConnStr)
        {
            dtData dtd = new dtData();
            DataTable dt = new NaveoOneLib.Services.Lookups.LookUpValuesService().GetLookUpValuesByLTName(name,sConnStr);
            if (dt.Columns.Contains("vid")) dt.Columns["vid"].ColumnName = "vid";
            if (dt.Columns.Contains("tid")) dt.Columns["tid"].ColumnName = "tid";
            if (dt.Columns.Contains("name")) dt.Columns["name"].ColumnName = "name";
            if (dt.Columns.Contains("description")) dt.Columns["description"].ColumnName = "description";
            if (dt.Columns.Contains("createddate")) dt.Columns["createddate"].ColumnName = "createddate";
            if (dt.Columns.Contains("updateddate")) dt.Columns["updateddate"].ColumnName = "updateddate";
            if (dt.Columns.Contains("createdby")) dt.Columns["createdby"].ColumnName = "createdby";
            if (dt.Columns.Contains("rootmatrixid")) dt.Columns["rootmatrixid"].ColumnName = "rootmatrixid";
            if (dt.Columns.Contains("value")) dt.Columns["value"].ColumnName = "value";
            if (dt.Columns.Contains("value2")) dt.Columns["value2"].ColumnName = "value2";
            dtd.Data = dt;



            return dtd;
        }


        public BaseModel GetLookUpValuesId(Guid sUserToken, string sConnStr, int lookupValueId)
        {

            BaseModel dt = new BaseModel();

            LookUpValues lookuptypes = new NaveoOneLib.Services.Lookups.LookUpValuesService().GetLookUpValuesById(lookupValueId.ToString(), sConnStr);

            dt.data = lookuptypes;

            return dt;
        }


        public Boolean SaveLookUpValues(LookUpValues uLookUpTypes, Boolean bInsert, String sConnStr)
        {

            Boolean b = new NaveoOneLib.Services.Lookups.LookUpValuesService().SaveLookUpValues(uLookUpTypes, bInsert, sConnStr);

            return b;

        }



        public Boolean UpdateLookUpValues(LookUpValues uLookUpTypes, Boolean bInsert, String sConnStr)
        {

            Boolean b = new NaveoOneLib.Services.Lookups.LookUpValuesService().SaveLookUpValues(uLookUpTypes, bInsert, sConnStr);

            return b;

        }

        #endregion


    }
}
