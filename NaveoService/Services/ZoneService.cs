﻿using NaveoService.Models;
using System;
using System.Collections.Generic;
using System.Data;
using NaveoOneLib.Models.Common;
using System.Drawing;
using Newtonsoft.Json;
using NaveoService.Models.DTO;
using NaveoService.Helpers;
using NaveoOneLib.Common;
using System.Linq;
using NaveoService.Models.Custom;
using ZoneData = NaveoService.Models.ZoneData;
using NaveoOneLib.Models.Lookups;

namespace NaveoService.Services
{
    public class ZoneService
    {
        #region public Methods
        /// <summary>
        /// Get zone list
        /// </summary>
        /// <param name="UID"></param>
        /// <param name="bOnlyHeaderDetails"></param>
        /// <param name="connStr"></param>
        /// <returns></returns>
        public List<NaveoOneLib.Models.Zones.Zone> GetZoneList(int UID, Boolean bOnlyHeaderDetails, string connStr)
        {
            List<NaveoOneLib.Models.Zones.Zone> lZones = new NaveoOneLib.WebSpecifics.LibData().lZone(UID, connStr, null, null, bOnlyHeaderDetails);
            return lZones;
        }

        public DataTable GetZoneHeaders(int UID, string connStr)
        {
            DataTable dt = new NaveoOneLib.WebSpecifics.LibData().dtZoneHeader(UID, connStr, null, null);
            return dt;
        }

        /// <summary>
        /// Get zones
        /// </summary>
        /// <param name="userToken"></param>
        /// <param name="sConnStr"></param>
        /// <returns></returns>
        public DataTable Zones(Guid userToken, string sConnStr)
        {
            int UID = CommonService.GetUserID(userToken, sConnStr);
            DataTable dtGMZone = new NaveoOneLib.WebSpecifics.LibData().dtGMZone(UID, sConnStr);

            dtGMZone.Columns.Remove("myStatus");
            dtGMZone.Columns.Remove("MID");
            dtGMZone.Columns.Remove("MatrixiID");

            dtGMZone.Columns.Add("TimeZoneID");

            return dtGMZone;
        }

        /// <summary>
        /// Return zones
        /// </summary>
        /// <param name="sUserToken"></param>
        /// <param name="sConnStr"></param>
        /// <param name="bUsingDS"></param>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public ZoneData GetZones(Guid sUserToken, String sConnStr, Boolean bUsingDS, int? page, int? limitPerPage)
        {
            ZoneData zd = new ZoneData();

            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            if (UID == -1)
                return zd;

            List<Models.Zone> zones;
            if (bUsingDS)
                zones = CommonService.castLApiZone(new NaveoOneLib.WebSpecifics.LibData().dsZones(UID, sConnStr, false, page, limitPerPage));
            else
                zones = CommonService.castLApiZone(new NaveoOneLib.WebSpecifics.LibData().lZone(UID, sConnStr, page, limitPerPage, false));

            zd.lZone = zones;
            int cnt = new NaveoOneLib.Services.Zones.ZoneService().GetNumberOfZones(sUserToken, sConnStr);
            zd.rowCount = cnt;

            return zd;
        }
        public ZoneData GetZoneById(Guid sUserToken, string sConnStr, List<int> lZoneIDs)
        {
            int UID = -1;
            List<int> lValidatedZones = CommonService.lValidateZones(sUserToken, lZoneIDs, sConnStr, out UID);

            ZoneData zd = new ZoneData();
            zd.lZone = new List<Models.Zone>();
            foreach (int i in lValidatedZones)
            {
                NaveoOneLib.Models.Zones.Zone z = new NaveoOneLib.Services.Zones.ZoneService().GetZoneById(i, false, sConnStr);
                Zone zone = CastData(z);
                zd.lZone.Add(zone);
            }
            zd.rowCount = zd.lZone.Count;

            return zd;
        }

        public BaseDTO DeleteZoneById(SecurityTokenExtended securityToken, List<int> lZoneIDs)
        {
            int UID = CommonService.GetUserID(securityToken.securityToken.UserToken, securityToken.securityToken.sConnStr);
            List<int> lValidatedZones = CommonService.lValidateZones(securityToken.securityToken.UserToken, lZoneIDs, securityToken.securityToken.sConnStr, out UID);

            List<int> lz = new List<int>();
            foreach (int i in lValidatedZones)
                lz.Add(i);

            BaseModel bm = new NaveoOneLib.Services.Zones.ZoneService().DeleteZones(lz, UID, securityToken.securityToken.sConnStr);
            return bm.ToBaseDTO(securityToken.securityToken);
        }

        public dtData GetZoneListFromZoneIDs(Guid sUserToken, List<int> lZoneIDs, string sConnStr, int? Page, int? LimitPerPage)
        {
            int UID = -1;
            List<int> lValidatedZones = CommonService.lValidateZones(sUserToken, lZoneIDs, sConnStr, out UID);

            dtData zd = new NaveoOneLib.Services.Zones.ZoneService().GetZoneListFromZoneIDs(sUserToken, lValidatedZones, sConnStr, Page, LimitPerPage);


            //dtPermissions = NaveoServiceUtils.dtLowerColumns(dtPermissions);

            return zd;
        }

        /// <summary>
        /// Return zone visited and not visited
        /// </summary>
        /// <param name="sUserToken"></param>
        /// <param name="lAssetIDs"></param>
        /// <param name="sType"></param>
        ///<param name="lZids"></param>
        /// <param name="idleInMinutes"></param>
        /// <param name="bZones"></param>
        /// <param name="dtFr"></param>
        /// <param name="dtTo"></param>
        /// <param name="sConnStr"></param>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public BaseModel getZoneVisited(Guid sUserToken, List<int> lAssetIDs, string sType, string reportType, List<int> lZids, int idleInMinutes, int idleSpeed, bool bZones, DateTime dtFr, DateTime dtTo, Boolean byDriver, String sConnStr, int? Page, int? LimitPerPage)
        {
            int UID = -1;
            List<int> lValidateIds = new List<int>();

            if (byDriver)
                lValidateIds = CommonService.lValidateDrivers(sUserToken, lAssetIDs, sConnStr, out UID);
            else
                lValidateIds = CommonService.lValidateAssets(sUserToken, lAssetIDs, sConnStr, out UID);

            #region validateZoneORZoneTypes
            List<int> lValidatedZoneIds = new List<int>();
            if (sType == "Zones")
                lValidatedZoneIds = CommonService.lValidateZones(sUserToken, lZids, sConnStr, out UID);
            else if (sType == "ZoneTypes")
                lValidatedZoneIds = lZids;
            #endregion


            int uId = new NaveoOneLib.Services.Users.UserService().GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new NaveoOneLib.Services.GMatrix.MatrixService().GetMatrixByUserID(uId, sConnStr);
            DataSet ds = new NaveoOneLib.Services.GPS.IdleService().getIdles(lValidateIds, dtFr, dtTo, reportType, idleInMinutes, idleSpeed, bZones, sType, lValidatedZoneIds, lm, byDriver, sConnStr, Page, LimitPerPage);

            apiZoneVisited apiZone = new apiZoneVisited();

            #region Visited
            DataTable dtTotal = ds.Tables["ZoneVisitedTotal"];

            if (reportType == "Visited" || reportType == "Both")
            {
                DateTime dateTime = new DateTime();
                DataTable dtVisited = ds.Tables["idles"];
                DataTable dtzIds = ds.Tables["ZonesVisited"];

                foreach (DataRow drVisited in dtVisited.Rows)
                {
                    dateTime = Convert.ToDateTime(drVisited["dtLocal"].ToString());
                    TimeSpan time = Convert.ToDateTime(drVisited["dtLocalTime"]).TimeOfDay;
                    DateTime dtLocalTime = dateTime.Date.Add(time);
                    string formattedDateTime = dateTime.ToString("dd/MM/yyyy");
                    zoneFormat zF = new zoneFormat();
                    zF.date = formattedDateTime;
                    zF.zone = drVisited["sZones"].ToString();
                    zF.asset = drVisited["Asset"].ToString();
                    zF.arrivalTime = drVisited["dtLocalFrom"].ToString();
                    zF.depatureTime = dtLocalTime.ToString();
                    zF.durationInZones = drVisited["sDuration"].ToString();
                    zF.type = "Visited";

                    string a = drVisited["iArrivalZones"].ToString();
                    string[] ss = a.Split(',');
                    List<string> z = new List<string>();
                    foreach (var x in ss)
                    {
                        if (!String.IsNullOrEmpty(x))
                            z.Add(x);
                        zF.zoneIds = z;
                    }

                    if (drVisited["sDuration"].ToString() == "0 Day(s), 00:00:00")
                        zF.color = "Red";

                    if (drVisited["sDuration"].ToString() == "0 Day(s), 00:00:00" && drVisited["sZones"].ToString() == null)
                        zF.color = "Red";

                    apiZone.zoneData.Add(zF);
                }
                foreach (DataRow drTotal in dtTotal.Rows)
                {
                    ZoneVisitedTotal zTotal = new ZoneVisitedTotal();
                    zTotal.Asset = drTotal["Asset"].ToString();
                    zTotal.Zones = drTotal["sZone"].ToString();
                    zTotal.ZoneID = Convert.ToInt32(drTotal["iZone"].ToString());
                    zTotal.Count = Convert.ToInt32(drTotal["cnt"]);

                    apiZone.zoneVisitedTotal.Add(zTotal);
                }

            }
            #endregion

            #region notVisited

            DataTable dtzIdsNotVisited = ds.Tables["NotVisited"];
            if (reportType == "Not Visited" || reportType == "Both")
            {
                DataTable dtNotVisited = ds.Tables["NotVisited"];
                DateTime dt = new DateTime();
                dt = dtFr.AddDays(1);
                foreach (DataRow drNotVisited in dtNotVisited.Rows)
                {
                    zoneFormat zF = new zoneFormat();

                    string formatedTime = dt.ToString("dd/MM/yyyy");
                    zF.date = formatedTime;
                    zF.asset = "Not Visited";
                    zF.zone = drNotVisited["Description"].ToString();
                    //zF.zoneId = (int)drNotVisited["ZoneID"];
                    zF.arrivalTime = "00:00:00";
                    zF.depatureTime = "00:00:00";
                    zF.durationInZones = "00:00:00";
                    zF.type = "Not Visited";

                    string a = drNotVisited["ZoneID"].ToString();
                    string[] ss = a.Split(',');
                    List<string> z = new List<string>();
                    foreach (var x in ss)
                    {
                        if (!String.IsNullOrEmpty(x))
                            z.Add(x);
                        zF.zoneIds = z;
                    }

                    apiZone.zoneData.Add(zF);
                }

            }
            #endregion

            #region ZoneVisitedCountPerDay
            DataTable dtZoneVisitedPerDay = ds.Tables["PivotData"];
            if (dtZoneVisitedPerDay != null)
            {
                List<String> lCols = new List<string>();
                foreach (DataColumn dc in dtZoneVisitedPerDay.Columns)
                    if (dc.ColumnName.Trim().ToLower() != "rownum")
                        if (dc.ColumnName.Trim().ToLower() != "sdesc")
                            lCols.Add(dc.ColumnName);

                List<pivotDataRow> lData = new List<pivotDataRow>();
                foreach (DataRow dr in dtZoneVisitedPerDay.Rows)
                {
                    pivotDataRow p = new pivotDataRow();
                    //p.rowNum = Convert.ToInt32(dr["RowNum"]);
                    //p.rowDesc = dr["sDesc"].ToString();

                    List<object> lo = new List<object>();
                    foreach (DataColumn dc in dtZoneVisitedPerDay.Columns)
                        if (dc.ColumnName.Trim().ToLower() != "rownum")
                            if (dc.ColumnName.Trim().ToLower() != "sdesc")
                                lo.Add(dr[dc]);

                    p.colData = lo;

                    lData.Add(p);
                }


                pivotData pData = new pivotData();
                pData.lData = lData;
                pData.lColumnNames = lCols;

                apiZone.zonesVisitedPerDay = pData;
            }
            #endregion

            BaseModel dm = new BaseModel();
            dm.data = apiZone;
            dm.total = apiZone.zoneData.Count;

            return dm;
        }


        public BaseModel getZoneVisitedDetails(Guid sUserToken, string sConnStr, int ZoneId)
        {
            List<NaveoOneLib.Models.Zones.ZoneDetail> lzoneDetail = new NaveoOneLib.Services.Zones.ZoneDetailService().GetZoneDetailsByZoneId(ZoneId, sConnStr);

            BaseModel bm = new BaseModel();
            bm.data = lzoneDetail;
            bm.total = lzoneDetail.Count;
            return bm;
        }

        public Boolean SaveZone(SecurityTokenExtended securityToken, string sConnStr, ZoneData zoneDto, Boolean bInsert)
        {
            int UID = CommonService.GetUserID(securityToken.securityToken.UserToken, sConnStr);
            NaveoOneLib.Models.Zones.Zone dllZone = CastData(zoneDto.lZone[0]);

            #region soiltype for terra
            if (dllZone.LookUpValue == 0)
            {
                NaveoOneLib.Models.Lookups.LookUpValues lkV = new NaveoOneLib.Services.Lookups.LookUpValuesService().GetLookUpValuesByName("Default Soil Type", sConnStr);
                dllZone.LookUpValue = lkV.VID;
            }
            #endregion

            Boolean saveZone = new NaveoOneLib.Services.Zones.ZoneService().SaveZone(dllZone, bInsert, UID, sConnStr);

            BaseModel bm = new BaseModel();
            bm.data = saveZone;
            return bm.data;
        }
        public BaseDTO SaveZonesFromShp(SecurityTokenExtended securityToken, String shpPath)
        {
            BaseModel assetObj = new NaveoOneLib.Services.Zones.ZoneService().SaveZonesFromShp(securityToken.securityToken.UserToken, shpPath, securityToken.securityToken.sConnStr);
            return assetObj.ToBaseDTO(securityToken.securityToken);
        }

        public DataSet UploadZone(SecurityTokenExtended securityToken, DataTable dt, ZoneData zD)
        {
            DataSet dsResult = new DataSet();

            string saveZone = string.Empty;
            int UID = CommonService.GetUserID(securityToken.securityToken.UserToken, securityToken.securityToken.sConnStr);
            DataView view = new DataView(dt);
            DataTable distinctValues = view.ToTable(true, "ZoneName");
            var query = distinctValues.AsEnumerable().Where(r => r.Field<string>("ZoneName") != string.Empty);
            NaveoOneLib.Models.Zones.Zone dllZone = new NaveoOneLib.Models.Zones.Zone(); //CastData(zoneDto.lZone[0]);
            int c = 0;
            List<string> lzoneName = new List<string>();
            string errorMessag = string.Empty;
            string zoneName = string.Empty;




            foreach (var drZoneName in query)
            {
                string zN = drZoneName["ZoneName"].ToString();
                DataTable dtZ = new NaveoOneLib.Services.Zones.ZoneService().GetZoneByZoneName(zN, securityToken.securityToken.sConnStr);
                if (dtZ.Rows.Count == 0)
                {


                    c++;
                    var rowColl = dt.AsEnumerable();
                    var drselectedZoneData = rowColl.Where(r => r.Field<string>("ZoneName") == drZoneName["ZoneName"].ToString());


                    List<Zone> lZone = new List<Zone>();
                    List<ZoneDetail> zoneDetails = new List<ZoneDetail>();
                    Zone z = new Zone();



                    Color color = ColorTranslator.FromHtml(zD.lZone[0].sColor);
                    z.color = color.ToArgb();

                    z.comments = zD.lZone[0].comments;
                    z.externalRef = zD.lZone[0].externalRef;
                    z.ref2 = zD.lZone[0].ref2;
                    z.ZoneExpiry = zD.lZone[0].ZoneExpiry;
                    //z.NoOfHouseHold = zD.lZone[0].NoOfHouseHold;

                    z.lMatrix = zD.lZone[0].lMatrix;
                    z.lZoneType = zD.lZone[0].lZoneType;
                    //z.iBuffer = zD.lZone[0].
                    z.oprType = DataRowState.Added;
                    //    z.oprType = DataRowState.Added;

                    int numberOfRows = drselectedZoneData.Count();

                    foreach (var dr in drselectedZoneData)
                    {
                        string count = c.ToString();
                        ZoneDetail zDetails = new ZoneDetail();
                        LookUpValues lookUpValue = new NaveoOneLib.Services.Lookups.LookUpValuesService().GetLookUpValuesByName(dr["LookUpValue"].ToString(), securityToken.securityToken.sConnStr);

                        zoneName = dr["ZoneName"].ToString();
                        z.externalRef = dr["AREA"].ToString();
                        z.ref2 = dr["PERIMETER"].ToString();

                        if (dr["HOUSEHOLD"].ToString() == string.Empty)
                            z.NoOfHouseHold = 0;
                        else
                            z.NoOfHouseHold = Convert.ToInt32(dr["HOUSEHOLD"].ToString());

                        if (lookUpValue != null)
                            z.LookUpValue = lookUpValue.VID;

                        //if (zoneName.Contains("_"))
                        //{
                        //    zoneName = zoneName.Substring(0, zoneName.LastIndexOf("_") + 1);
                        //    zoneName = zoneName + count;
                        //}

                        z.description = zoneName.ToString();

                        Double iRadius = Convert.ToDouble(dr["Radius"]);
                        //Double iRadiusInMetres = iRadius * 1000;
                        int newRadius = Convert.ToInt32(iRadius);
                        #region Radius
                        if (iRadius != 0)
                        {
                            Double[] d = GeoCalc.getBoundingBox(Convert.ToDouble(dr["Latitude"]), Convert.ToDouble(dr["Longitude"]), newRadius);
                            ZoneDetail zd = new ZoneDetail();
                            zd.latitude = d[2];
                            zd.longitude = d[1];
                            zd.oprType = DataRowState.Added;
                            z.zoneDetails.Add(zd);

                            zd = new ZoneDetail();
                            zd.latitude = d[2];
                            zd.longitude = d[3];
                            zd.oprType = DataRowState.Added;
                            z.zoneDetails.Add(zd);

                            zd = new ZoneDetail();
                            zd.latitude = d[0];
                            zd.longitude = d[3];
                            zd.oprType = DataRowState.Added;
                            z.zoneDetails.Add(zd);

                            zd = new ZoneDetail();
                            zd.latitude = d[0];
                            zd.longitude = d[1];
                            zd.oprType = DataRowState.Added;
                            z.zoneDetails.Add(zd);

                            zd = new ZoneDetail();
                            zd.latitude = d[2];
                            zd.longitude = d[1];
                            zd.oprType = DataRowState.Added;
                            z.zoneDetails.Add(zd);

                        }
                        #endregion


                        if (iRadius == 0)
                        {
                            zDetails.latitude = Convert.ToDouble(dr["Latitude"]);
                            zDetails.longitude = Convert.ToDouble(dr["Longitude"]);
                            zDetails.oprType = DataRowState.Added;
                            z.zoneDetails.Add(zDetails);
                        }

                    }


                    Boolean saveZone1 = new NaveoOneLib.Services.Zones.ZoneService().SaveZone(z, true, UID, securityToken.securityToken.sConnStr);
                    lzoneName.Add(zoneName);

                }
            }

            errorMessag = c + " Zones have been created !!";

            DataTable dtResult = new DataTable();
            dtResult.TableName = "dtResult";
            dtResult.Columns.Add("ID");
            dtResult.Columns.Add("Result");


            DataRow drResult = dtResult.NewRow();
            drResult["Result"] = errorMessag.ToString();
            dtResult.Rows.Add(drResult);


            DataTable dtOutput = new DataTable();
            dtOutput.Columns.Add("ZoneID");
            dtOutput.Columns.Add("ZoneName");
            dtOutput.TableName = "dtData";

            DataTable dtZoneName = new DataTable();

            foreach (var s in lzoneName)
            {
                if (s != string.Empty)
                {

                    dtZoneName = new NaveoOneLib.Services.Zones.ZoneService().GetZoneByZoneName(s, securityToken.securityToken.sConnStr);

                    foreach (DataRow drZ in dtZoneName.Rows)
                    {
                        DataRow drOutPut = dtOutput.NewRow();
                        drOutPut["ZoneID"] = Convert.ToInt32(drZ["ZoneID"]);
                        drOutPut["ZoneName"] = drZ["Description"].ToString();
                        dtOutput.Rows.Add(drOutPut);

                    }
                }
            }



            dsResult.Tables.Add(dtResult);
            dsResult.Tables.Add(dtOutput);
            BaseModel bm = new BaseModel();
            bm.data = dsResult;
            return bm.data;

        }

        public Boolean UpdateZone(SecurityTokenExtended securityToken, DataTable dt)
        {


            Boolean BInsert = false;

            foreach (DataRow drZoneName in dt.Rows)
            {
                Zone Z = new Zone();
                DataTable dtZone = new NaveoOneLib.Services.Zones.ZoneService().GetZoneByZoneName(drZoneName[""].ToString(), securityToken.securityToken.sConnStr);



            }



            return BInsert;
        }

        public string UpdateZoneLookUpValues(SecurityTokenExtended securityToken, DataTable dt)
        {
            string message = string.Empty;
            int UID = CommonService.GetUserID(securityToken.securityToken.UserToken, securityToken.securityToken.sConnStr);
            Zone z = new Zone();
            Boolean saveZone1 = false;
            int count = 0;


            foreach (DataRow dr in dt.Rows)
            {
                LookUpValues lookUpValue = new LookUpValues();

                int lookupValueID = 0;

                if (dr["LookUpValue"].ToString() == "Not Defined" || dr["LookUpValue"].ToString() == string.Empty || dr["LookUpValue"].ToString() == null)
                {
                    lookUpValue = new NaveoOneLib.Services.Lookups.LookUpValuesService().GetLookUpValuesByName("Default Soil Type", securityToken.securityToken.sConnStr);
                    lookupValueID = lookUpValue.VID;
                }
                else
                {
                    lookUpValue = new NaveoOneLib.Services.Lookups.LookUpValuesService().GetLookUpValuesByName(dr["LookUpValue"].ToString(), securityToken.securityToken.sConnStr);
                    lookupValueID = lookUpValue.VID;
                }


                DataTable dtZoneName = new NaveoOneLib.Services.Zones.ZoneService().GetZoneByZoneName(dr["ZoneName"].ToString(), securityToken.securityToken.sConnStr);

                z.description = dtZoneName.Rows[0][1].ToString();

                if (Convert.ToDouble(dr["Area"]) == 0.0)
                    z.externalRef = dtZoneName.Rows[0][5].ToString();
                else
                    z.externalRef = dr["Area"].ToString();


                if (Convert.ToDouble(dr["Perimeter"]) == 0.0)
                    z.ref2 = dtZoneName.Rows[0][6].ToString();
                else
                    z.ref2 = dr["Perimeter"].ToString();


                z.zoneID = Convert.ToInt32(dtZoneName.Rows[0][0]);

                z.color = Convert.ToInt32(dtZoneName.Rows[0][4]);
                z.isMarker = Convert.ToInt32(dtZoneName.Rows[0][7]);
                z.GeomData = dtZoneName.Rows[0][9].ToString();
                z.LookUpValue = lookupValueID;
                z.oprType = DataRowState.Modified;

                List<ZoneDetail> lZoneDetail = new List<ZoneDetail>();
                NaveoOneLib.Models.Zones.Zone zone = new NaveoOneLib.Services.Zones.ZoneService().GetZoneById(z.zoneID, true, securityToken.securityToken.sConnStr);
                z.zoneDetails = zone.zoneDetails;

                saveZone1 = new NaveoOneLib.Services.Zones.ZoneService().SaveZone(z, false, UID, securityToken.securityToken.sConnStr);

                count++;
            }


            if (saveZone1 == true)
                message = count + " Zones Updated";
            else
                message = "Error in updating zones...";

            return message;

        }
        #endregion

        #region private methods
        Zone CastData(NaveoOneLib.Models.Zones.Zone z)
        {
            Models.Zone zone = new Zone();
            zone.zoneID = z.zoneID;
            zone.color = z.color;
            zone.comments = z.comments;
            zone.description = z.description;
            zone.displayed = z.displayed;
            zone.externalRef = z.externalRef;
            zone.iBuffer = z.iBuffer;
            zone.isMarker = z.isMarker;
            zone.lMatrix = z.lMatrix;
            zone.lZoneType = z.lZoneType;
            zone.zoneDetails = z.zoneDetails;
            zone.oprType = z.oprType;
            zone.ref2 = z.ref2;
            zone.ZoneExpiry = z.ZoneExpiry;
            zone.LookUpValue = z.LookUpValue;

            zone.sColor = CommonService.HexConverter(Color.FromArgb(z.color));
            zone.GeomData = z.GeomData;/*JsonConvert.DeserializeObject(z.GeomData);*/

            return zone;
        }

        NaveoOneLib.Models.Zones.Zone CastData(Zone z)
        {
            NaveoOneLib.Models.Zones.Zone zone = new NaveoOneLib.Models.Zones.Zone();

            zone.zoneID = z.zoneID;
            zone.comments = z.comments;
            zone.description = z.description;
            zone.displayed = z.displayed;
            zone.externalRef = z.externalRef;
            zone.iBuffer = z.iBuffer;
            zone.isMarker = z.isMarker;
            zone.lMatrix = z.lMatrix;
            zone.lZoneType = z.lZoneType;
            zone.zoneDetails = z.zoneDetails;
            zone.oprType = z.oprType;
            zone.ref2 = z.ref2;
            zone.ZoneExpiry = z.ZoneExpiry;
            //zone.GeomData = JsonConvert.SerializeObject(z.GeomData);
            zone.GeomData = z.GeomData1;
            zone.LookUpValue = z.LookUpValue;

            Color color = ColorTranslator.FromHtml(z.sColor);
            zone.color = color.ToArgb();

            return zone;
        }
        #endregion

        #region VCA
        public BaseDTO SaveVCAFromShp(SecurityTokenExtended securityToken, String shpPath)
        {
            BaseModel assetObj = new NaveoOneLib.Services.Zones.VCAHeaderService().SaveVCAFromShp(shpPath, securityToken.securityToken.sConnStr);
            return assetObj.ToBaseDTO(securityToken.securityToken);
        }
        #endregion

    }
}