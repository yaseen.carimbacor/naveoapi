﻿using NaveoOneLib.Common;
using NaveoOneLib.Models;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Services.Permissions;
using NaveoService.Helpers;
using NaveoService.Models;
using NaveoService.Models.DTO;
using NaveoService.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;


namespace NaveoService.Services
{
    public class RoleService
    {
        //public DataTable GetRoles(String sConnStr)
        //{
        //    ApplicationDbContext context = new ApplicationDbContext(sConnStr);
        //    var roles = context.Roles.OrderBy(x => x.Name).Select(x => x.Name);
        //    List<RoleAccess> Roles = new List<RoleAccess>();

        //    List<RoleAccess> RolesMatrix = new List<RoleAccess>();

        //    foreach (var role in roles)
        //    {
        //        RoleAccess r = new RoleAccess();
        //        r.ControllerName = role.Split('_')[0];
        //        Roles.Add(r);
        //    }

        //    List<RoleAccess> RolesRefined = new List<RoleAccess>(Roles.GroupBy(x => x.ControllerName)
        //           .Select(grp => grp.First())
        //           .ToList());

        //    DataTable dt = new DataTable();
        //    dt.Columns.Add("ControllerName");
        //    dt.Columns.Add("View");
        //    dt.Columns.Add("Add");
        //    dt.Columns.Add("Modify");
        //    dt.Columns.Add("Delete");

        //    foreach (var role in RolesRefined)
        //    {
        //        DataRow dr = dt.NewRow();
        //        dr["ControllerName"] = role.ControllerName;
        //        dr["View"] = role.ControllerName + "_View";
        //        dr["Add"] = role.ControllerName + "_Add";
        //        dr["Modify"] = role.ControllerName + "_Update";
        //        dr["Delete"] = role.ControllerName + "_Delete";
        //        dt.Rows.Add(dr);
        //    }

        //    return dt;
        //}

        public dtData GetRoles(Guid sUserToken,string sConnStr)
        {


            DataTable dtRoster = new RolesService().GetRoles(sUserToken, sConnStr);

            if (dtRoster.Columns.Contains("iid"))
                dtRoster.Columns["iid"].ColumnName = "iID";

            if (dtRoster.Columns.Contains("description"))
                dtRoster.Columns["description"].ColumnName = "Description";

            dtData dtR = new dtData();
            dtR.Data = dtRoster;
            dtR.Rowcnt = dtRoster.Rows.Count;

            return dtR;
        }


        public BaseModel GetRoleById(SecurityTokenExtended securityToken, string sConnStr, int roleId)
        {

            BaseModel bM = new BaseModel();
            int userId = CommonService.GetUserID(securityToken.securityToken.UserToken, securityToken.securityToken.sConnStr);
            List<int> lRoleID = new List<int>();
            lRoleID.Add(roleId);

            int UID = -1;
            List<int> lValidatedRoles = CommonService.lValidateRoles(securityToken.securityToken.UserToken, lRoleID, securityToken.securityToken.sConnStr, out UID);

            if(lValidatedRoles.Count ==0)
            {
                //bM.data = false;
                bM.errorMessage = "You don't have access to this role";
                return bM;
            }

            Role role = new NaveoOneLib.Services.Permissions.RolesService().GetRolesById(lValidatedRoles[0], sConnStr);
            if (role.Description == "### Default_Role ###")
            {
                //bM.data = false;
                bM.errorMessage = "### Default_Role ### is not editable";
                return bM;
            }


            bM.data = role;
            return bM;

        }

        /// <summary>
        /// Creates a role
        /// </summary>
        /// <param name="role"></param>
        /// <param name="securityToken"></param>
        /// <returns></returns>
        public BaseDTO CreateSaveRole(SecurityTokenExtended securityToken, Boolean bInsert,NaveoOneLib.Models.Permissions.Role role)
        {
            //TODO: Pravish > validations

            int userId = CommonService.GetUserID(securityToken.securityToken.UserToken, securityToken.securityToken.sConnStr);
            List<int> lUsers = new List<int>();
            lUsers.Add(userId);

            int uiD = -1;
            List<int> lValidatedUsers = CommonService.lValidateUsers(securityToken.securityToken.UserToken, lUsers, securityToken.securityToken.sConnStr, out uiD);
            int UID = lValidatedUsers[0];

            role.lMatrix = new List<NaveoOneLib.Models.GMatrix.Matrix>();
            #region Matrix
            if (bInsert)
            {
                
                List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, securityToken.securityToken.sConnStr);

                foreach (NaveoOneLib.Models.GMatrix.Matrix mm in lm)
                {
                    NaveoOneLib.Models.GMatrix.Matrix m = new NaveoOneLib.Models.GMatrix.Matrix();
                    m.GMDesc = mm.GMDesc;
                    m.GMID = mm.GMID;
                    m.iID = role.RoleID;
                    m.MID =0;
                    m.oprType = DataRowState.Added;
                    role.lMatrix.Add(m);
                }
            }
            #endregion

            BaseModel roleObj = new RolesService().SaveRoles(role, bInsert, securityToken.securityToken.sConnStr);
            return roleObj.ToBaseDTO(securityToken.securityToken);
        }

        /// <summary>
        /// Updates a role
        /// </summary>
        /// <param name="role"></param>
        /// <param name="connStr"></param>
        /// <returns></returns>
        public BaseDTO UpdateRole(SecurityTokenExtended securityToken, NaveoOneLib.Models.Permissions.Role role)
        {
            //TODO : Pravish > Validations
            BaseModel roleObj = new RolesService().SaveRoles(role, false, securityToken.securityToken.sConnStr);
            return roleObj.ToBaseDTO(securityToken.securityToken);
        }

        public BaseDTO DeleteRoles(SecurityTokenExtended securityToken, int roleID)
        {
            Role role = new Role();
            int userId = CommonService.GetUserID(securityToken.securityToken.UserToken, securityToken.securityToken.sConnStr);
            List<int> lRoleID = new List<int>();
            lRoleID.Add(roleID);

            int UID = -1;
            List<int> lValidatedRoles = CommonService.lValidateRoles(securityToken.securityToken.UserToken,lRoleID, securityToken.securityToken.sConnStr, out UID);



            BaseModel roleObj = new BaseModel();
            if (lValidatedRoles.Count == 0)
            {
                roleObj.data = false;
                roleObj.errorMessage = "User does not have access to  roleID :" + roleID + " ";
                return roleObj.ToBaseDTO(securityToken.securityToken);
            } else
            {
                role.RoleID = lValidatedRoles[0];
            }



            roleObj.data = new RolesService().DeleteRoles(role, userId, securityToken.securityToken.sConnStr);
            return roleObj.ToBaseDTO(securityToken.securityToken);
        }

        public dtData GetPermissionsByMatrix(Guid sUserToken,string sConnstr)
        {

            int UID = CommonService.GetUserID(sUserToken, sConnstr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnstr);

            DataTable dtPermissions = new NaveoOneLib.Services.Permissions.PermissionsService().GetPermissionByUserMatrix(lm, sConnstr);


            if (dtPermissions.Columns.Contains("permissionid"))
                dtPermissions.Columns["permissionid"].ColumnName = "PermissionID";

            if (dtPermissions.Columns.Contains("controllername"))
                dtPermissions.Columns["controllername"].ColumnName = "ControllerName";



            dtData dtR = new dtData();
            dtR.Data = dtPermissions;
            dtR.Rowcnt = dtPermissions.Rows.Count;

            return dtR;
        }


        public dtData GetPermissionsByUID(Guid sUserToken, string sConnstr)
        {

            int UID = CommonService.GetUserID(sUserToken, sConnstr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnstr);

            DataTable dtPermissions = new NaveoOneLib.Services.Permissions.PermissionsService().GetPermissionByUserID(UID, sConnstr);
            //dtPermissions = NaveoServiceUtils.dtLowerColumns(dtPermissions);
            if (dtPermissions.Columns.Contains("rolename"))
            {
              dtPermissions.Columns["rolename"].ColumnName = "roleName";

            }  if (dtPermissions.Columns.Contains("modulename"))
            {
                dtPermissions.Columns["modulename"].ColumnName = "ModuleName";
            }
             if (dtPermissions.Columns.Contains("icreate"))
            {
                dtPermissions.Columns["icreate"].ColumnName = "iCreate";
            }
             if (dtPermissions.Columns.Contains("iupdate"))
            {
                dtPermissions.Columns["iupdate"].ColumnName = "iUpdate";
            }
             if (dtPermissions.Columns.Contains("iread"))
            {
                dtPermissions.Columns["iread"].ColumnName = "iRead";
            }
             if (dtPermissions.Columns.Contains("idelete"))
            {
                dtPermissions.Columns["idelete"].ColumnName = "iDelete";
            }
             if (dtPermissions.Columns.Contains("report1"))
            {
                dtPermissions.Columns["report1"].ColumnName = "Report1";
            }
             if (dtPermissions.Columns.Contains("report2"))
            {
                dtPermissions.Columns["report2"].ColumnName = "Report2";
            }

             if (dtPermissions.Columns.Contains("report3"))
            {
                dtPermissions.Columns["report3"].ColumnName = "Report3";
            }

             if (dtPermissions.Columns.Contains("report4"))
            {
                dtPermissions.Columns["report4"].ColumnName = "Report4";
            }


            dtData dtR = new dtData();
            dtR.Data = dtPermissions;
            dtR.Rowcnt = dtPermissions.Rows.Count;

            return dtR;
        }

        public BaseModel GetNaveoReports()
        {
            N1Controller n1 = new N1Controller();
            var x = n1.lNaveoReport;

            #region Trip
            Naveo_Report nr1 = new Naveo_Report();
            nr1.ControllerName = "Trip";


            report rTrip1 = new report();
            rTrip1.DisplayName = "Daily Summarized Report";
            rTrip1.ReportName = "Report1";

            report rTrip2 = new report();
            rTrip2.DisplayName = "Periodic Vehicle Summary";
            rTrip2.ReportName = "Report2";


            report rTrip3 = new report();
            rTrip3.DisplayName = "Daily Engine Hours";
            rTrip3.ReportName = "Report3";

            report rTrip4 = new report();
            rTrip4.DisplayName = "Activity Outside HO";
            rTrip4.ReportName = "Report4";

            nr1.Report1 =rTrip1;
            nr1.Report2 = rTrip2;
            nr1.Report3 = rTrip3;
            nr1.Report4 = rTrip4;
            x.Add(nr1);

            #endregion

            #region Zone

            Naveo_Report nr2 = new Naveo_Report();
            nr2.ControllerName = "Zones";

           
            report rZone1 = new report();
            rZone1.DisplayName = "Zone Visited Report";
            rZone1.ReportName = "Report1";

            nr2.Report1 =rZone1;
     
            x.Add(nr2);
            #endregion

            #region Temperature

            Naveo_Report nr3 = new Naveo_Report();
            nr3.ControllerName = "Temperature";

        
            report rTemperature1 = new report();
            rTemperature1.DisplayName = "Temperature Report";
            rTemperature1.ReportName = "Report1";

            report rTemperature2 = new report();
            rTemperature2.DisplayName = "Custom Temperature Report";
            rTemperature2.ReportName = "Report2";

            nr3.Report1 =rTemperature1;
            nr3.Report2 =rTemperature2;
            x.Add(nr3);
            #endregion

            #region Exception

            Naveo_Report nr4 = new Naveo_Report();
            nr4.ControllerName = "Exception";

            List<report> lreportException = new List<report>();
            report rException1 = new report();
            rException1.DisplayName = "Exception Report";
            rException1.ReportName = "Report1";

            nr4.Report1 =rException1;
            x.Add(nr4);
            #endregion

            #region Fuel

            Naveo_Report nr5 = new Naveo_Report();
            nr5.ControllerName = "Fuel";

            
            report rFuel1 = new report();
            rFuel1.DisplayName = "Fuel Report";
            rFuel1.ReportName = "Report1";

            nr5.Report1 = rFuel1;
            x.Add(nr5);
            #endregion

            #region SensorData
            Naveo_Report nr6 = new Naveo_Report();
            nr6.ControllerName = "GPSData";

            report rgpsData = new report();
            rgpsData.DisplayName = "Sensor Data Report";
            rgpsData.ReportName = "Report1";

            nr6.Report1 = rgpsData;
            x.Add(nr6);
            #endregion

            #region CustomizedTrips
            Naveo_Report nr7 = new Naveo_Report();
            nr7.ControllerName = "CustomizedTrips";

            //report custData1 = new report();
            //custData1.DisplayName = "Customized Trips List";
            //custData1.ReportName = "Report1";

            report custData1 = new report();
            custData1.DisplayName = "Customized Report";
            custData1.ReportName = "Report1";

            nr7.Report1 = custData1;
            x.Add(nr7);
            #endregion

            #region MokaExtendedTrips
            Naveo_Report nr8 = new Naveo_Report();
            nr8.ControllerName = "MokaExtendedTrips";

            report mokAData1 = new report();
            mokAData1.DisplayName = "MOLG Extended Trips Report";
            mokAData1.ReportName = "Report1";

            nr8.Report1 = mokAData1;
         
            x.Add(nr8);


            #endregion

            #region Montoring
            Naveo_Report nr9 = new Naveo_Report();
            nr9.ControllerName = "Configurations";

            report Configurations1 = new report();
            Configurations1.DisplayName = "Montoring Tool Report";
            Configurations1.ReportName = "Report1";

            report Configurations2 = new report();
            Configurations2.DisplayName = "Unit Configuration Report";
            Configurations2.ReportName = "Report2";

            nr9.Report1 = Configurations1;
            nr9.Report2 = Configurations2;

            x.Add(nr9);


            #endregion

            int count = x.Count;
            BaseModel bM = new BaseModel();
            bM.data = x;
            bM.total = count;
            return bM;
        }


    }
}