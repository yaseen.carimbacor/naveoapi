﻿
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.GMatrix;
using System;
using System.Collections.Generic;
using System.Data;
using NaveoOneLib.Services.GMatrix;
using NaveoService.Models.DTO;
using NaveoService.Helpers;
using NaveoService.Models.Custom;
using NaveoService.Models;
using NaveoOneLib.Constant;
using System.Net;
using Newtonsoft.Json;
using System.Text;
using NaveoOneLib.Models.Users;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using NaveoOneLib.Common;
using System.Drawing.Imaging;
using System.Linq;
using NaveoOneLib.Services.Trips;
using NaveoOneLib.DBCon;

namespace NaveoService.Services
{
    public class AssetService
    {
        #region Methods
        /// <summary>
        /// Get all Asset Data
        /// </summary>
        /// <param name="UID"></param>
        /// <param name="sConnStr"></param>
        /// <returns></returns>
        public dtData GetAssetData(Guid sUserToken, List<int> lAssetIDs, string sConnStr, int? Page, int? LimitPerPage)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);
            DataTable dtAssetData = new NaveoOneLib.Services.Assets.AssetService().GetAsset(lm, sConnStr);

            string assetID = "AssetID";
            if (dtAssetData.Columns.Contains("AssetID"))
            {
                dtAssetData.Columns["AssetID"].ColumnName = "assetId";
                assetID = "assetId";
            }

            if (dtAssetData.Columns.Contains("AssetNumber"))
                dtAssetData.Columns["AssetNumber"].ColumnName = "assetNumber";

            if (dtAssetData.Columns.Contains("Status_B"))
                dtAssetData.Columns["Status_B"].ColumnName = "status_B";

            if (dtAssetData.Columns.Contains("TimeZoneID"))
                dtAssetData.Columns["TimeZoneID"].ColumnName = "timeZoneId";

            if (dtAssetData.Columns.Contains("Odometer"))
                dtAssetData.Columns["Odometer"].ColumnName = "odometer";

            if (dtAssetData.Columns.Contains("AssetType"))
                dtAssetData.Columns.Remove("AssetType");
            if (dtAssetData.Columns.Contains("AssetName"))
                dtAssetData.Columns.Remove("AssetName");

            dtAssetData.Columns.Add("deviceId", typeof(string));
            dtAssetData.Columns.Add("matrix", typeof(String));
            dtAssetData.Columns.Add("logReason", typeof(String));
            dtAssetData.Columns.Add("lastReport", typeof(String));
            dtAssetData.Columns.Add("notReportedDays", typeof(String));
            dtAssetData.Columns.Add("color", typeof(String));
            dtAssetData.Columns.Add("speed", typeof(String));

            DataTable ResultTable = dtAssetData.Clone();
            ResultTable.Columns.Add("rowNum", typeof(int));
            ResultTable.Columns["rowNum"].AutoIncrement = true;
            ResultTable.Columns["rowNum"].AutoIncrementSeed = 1;
            ResultTable.Columns["rowNum"].AutoIncrementStep = 1;
            foreach (DataRow dr in dtAssetData.Rows)
                foreach (int i in lAssetIDs)
                    if (Convert.ToInt32(dr[assetID]) == i)
                        ResultTable.Rows.Add(dr.ItemArray);

            int iTotalRec = ResultTable.Rows.Count;
            Pagination p = Pagination.GetRowNums(Page, LimitPerPage);
            DataTable dt = new DataTable();
            DataRow[] drChk = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo);
            if (drChk != null && drChk.Length > 0)
                dt = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo).CopyToDataTable();
            else
                dt = ResultTable;

            List<int> lAssetID = new List<int>();
            foreach (DataRow dr in dt.Rows)
                lAssetID.Add(Convert.ToInt32(dr[assetID]));
            DataTable dtAssetStatus = new NaveoOneLib.Services.Assets.AssetService().GetAssetsStatus(lAssetID, lm, sConnStr);

            foreach (DataRow dr in dt.Rows)
            {
                DataRow[] r = dtAssetStatus.Select(assetID + "= " + dr[assetID]);
                if (r.Length > 0)
                {
                    dr["deviceId"] = r[0]["deviceID"];
                    dr["matrix"] = r[0]["gmDescription"];
                    dr["logReason"] = r[0]["details"];
                    dr["lastReport"] = r[0]["dtLastRptd"];
                    dr["notReportedDays"] = r[0]["daysNoReport"];
                    dr["color"] = r[0]["color"];
                    dr["speed"] = r[0]["speed"];
                }
            }

            DataRow[] drNoMatrix = dtAssetStatus.Select("GMDescription = '*** No Matrix ***'");
            foreach (DataRow r in drNoMatrix)
            {
                DataRow dr = dt.NewRow();
                dr["deviceId"] = r["deviceID"];
                dr["matrix"] = r["gmDescription"];
                dr["logReason"] = r["details"];
                dr["lastReport"] = r["dtLastRptd"];
                dr["notReportedDays"] = r["daysNoReport"];
                dr["color"] = r["color"];
                dr["speed"] = r["speed"];

                dr["AssetID"] = r["AssetID"];
                dt.Rows.Add(dr);
            }

            dtData dtR = new dtData();
            dtR.Data = dt;
            dtR.Rowcnt = iTotalRec;

            return dtR;
        }

        public dtData SearchAssets(Guid sUserToken, NaveoModels nm, String SearchValue, String sConnStr)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            DataTable dt = new NaveoOneLib.WebSpecifics.LibData().SearchAsset(UID, nm, SearchValue, sConnStr);
            dtData dtR = new dtData();
            dtR.Data = dt;
            dtR.Rowcnt = dt.Rows.Count;

            return dtR;
        }

        /// <summary>
        /// Get Asset details by Id
        /// </summary>
        /// <param name="sUserToken"></param>
        /// <param name="sConnStr"></param>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public Models.DTO.apiAsset GetAssetDataById(Guid sUserToken, string sConnStr, int assetId)
        {
            List<int> lAssetID = new List<int>();
            lAssetID.Add(assetId);

            int UID = -1;
            List<int> lValidatedAssets = CommonService.lValidateAssets(sUserToken, lAssetID, sConnStr, out UID);

            Asset asset = new NaveoOneLib.Services.Assets.AssetService().GetAssetById(lValidatedAssets[0], UID, true, sConnStr);
            Models.DTO.apiAsset assetDTO = new Models.DTO.apiAsset();

            #region Asset main Details
            assetDTO.assetId = asset.AssetID;
            assetDTO.assetNumber = asset.AssetNumber;
            assetDTO.status_B = asset.Status_b;
            //assetDTO.createdDate = asset.CreatedDate;
            //assetDTO.updatedDate = asset.UpdatedDate;
            assetDTO.timeZoneId = asset.TimeZoneID;
            assetDTO.timeZoneTs = asset.TimeZoneTS;
            assetDTO.YearManufactured = asset.YearManufactured;
            assetDTO.PurchasedDate = asset.PurchasedDate;
            assetDTO.PurchasedValue = asset.PurchasedValue;
            assetDTO.RefA = asset.RefA;
            assetDTO.RefB = asset.RefB;
            assetDTO.RefC = asset.RefC;
            assetDTO.ExternalRef = asset.ExternalRef;
            assetDTO.EngineSerialNumber = asset.EngineSerialNumber;
            assetDTO.EngineCapacity = asset.EngineCapacity;
            assetDTO.EnginePower = asset.EnginePower;


            #region roadType 
            if (asset.RoadSpeedTypeId.HasValue)
            {
                minifiedValues minifiedRoadSpeedType = new minifiedValues();
                minifiedRoadSpeedType.id = (int)asset.RoadSpeedTypeId;
                minifiedRoadSpeedType.description = asset.sRoadSpeedType;
                assetDTO.roadTypeSpeed = minifiedRoadSpeedType;

            }

            #endregion


            //assetDTO.roadTypeSpeed = asset.dtRoadSpeedType;

            //assetDTO.

            #region Matrix
            if (asset.lMatrix != new List<NaveoOneLib.Models.GMatrix.Matrix>())
            {
                foreach (NaveoOneLib.Models.GMatrix.Matrix matrix in asset.lMatrix)
                {
                    var minifiedMatrix = new NaveoService.Models.DTO.AssetMatrix
                    {
                        oprType = matrix.oprType,
                        mId = matrix.MID,
                        iId = matrix.iID,
                        gMId = matrix.GMID,
                        gMDesc = matrix.GMDesc
                    };

                    assetDTO.lMatrix.Add(minifiedMatrix);
                }
            }
            #endregion

            #region AssetTypes
            minifiedValues mv = new minifiedValues();
            mv.id = asset.AssetTypeMappingObject.IID;
            mv.description = asset.AssetTypeMappingObject.Description;
            assetDTO.assetType = mv;
            #endregion

            #region minifiedUser CreatedBy
            if (asset.CreatedBy.HasValue)
            {
                DataRow[] drr = asset.dtUsers.Select("UID = " + asset.CreatedBy.Value);
                if (drr.Length > 0)
                {
                    MinifiedUser mu = new MinifiedUser();
                    mu.userID = Convert.ToInt32(drr[0]["UID"]);
                    mu.description = drr[0]["email"].ToString();
                    assetDTO.createdUser = mu;
                }
            }
            #endregion

            #region minifiedUser UpdatedBy
            if (asset.UpdatedBy.HasValue)
            {
                DataRow[] drr = asset.dtUsers.Select("UID = " + asset.UpdatedBy.Value);
                if (drr.Length > 0)
                {
                    MinifiedUser mu = new MinifiedUser();
                    mu.userID = Convert.ToInt32(drr[0]["UID"]);
                    mu.description = drr[0]["email"].ToString();
                    assetDTO.updatedUser = mu;
                }
            }
            #endregion

            #region minifiedMake
            if (asset.MakeID.HasValue)
            {
                minifiedValues minifiedMake = new minifiedValues();
                minifiedMake.id = (int)asset.MakeID;
                minifiedMake.description = asset.sMake;
                assetDTO.make = minifiedMake;
            }
            #endregion

            #region minifiedModel
            if (asset.ModelID.HasValue)
            {
                minifiedValues minifiedModel = new minifiedValues();
                minifiedModel.id = (int)asset.ModelID;
                minifiedModel.description = asset.sModel;
                assetDTO.model = minifiedModel;
            }
            #endregion

            #region minifiedColor
            if (asset.ColorID.HasValue)
            {
                minifiedValues minifiedColor = new minifiedValues();
                minifiedColor.id = (int)asset.ColorID;
                minifiedColor.description = asset.sColor;
                assetDTO.color = minifiedColor;
            }
            #endregion

            #region minifiedVehicle
            if (asset.VehicleTypeId.HasValue)
            {
                minifiedValues minifiedvehicleType = new minifiedValues();
                minifiedvehicleType.id = (int)asset.VehicleTypeId;
                minifiedvehicleType.description = asset.VehicleType;
                assetDTO.vehicleType = minifiedvehicleType;
            }
            #endregion

            #region Device Map
            if (asset.assetDeviceMap != new NaveoOneLib.Models.Assets.AssetDeviceMap())
            {
                assetDTO.assetDeviceMap = new NaveoService.Models.DTO.AssetDeviceMap();
                assetDTO.assetDeviceMap.mapId = asset.assetDeviceMap.MapID;
                assetDTO.assetDeviceMap.assetId = asset.assetDeviceMap.AssetID;
                assetDTO.assetDeviceMap.deviceId = asset.assetDeviceMap.DeviceID;
                assetDTO.assetDeviceMap.serialNumber = asset.assetDeviceMap.SerialNumber;
                assetDTO.assetDeviceMap.status_B = asset.assetDeviceMap.Status_b;
                assetDTO.assetDeviceMap.isFuel = false;


                //if (!String.IsNullOrEmpty(asset.assetDeviceMap.DeviceID))
                //assetDTO.assetDeviceMap.isFuel = true;

                assetDTO.assetDeviceMap.oprType = asset.assetDeviceMap.oprType;

                if (asset.assetDeviceMap.deviceAuxilliaryMap != new NaveoOneLib.Models.Assets.DeviceAuxilliaryMap())
                {
                    NaveoOneLib.Models.Assets.DeviceAuxilliaryMap lauxType = asset.assetDeviceMap.deviceAuxilliaryMap;

                    if (asset.RefD == string.Empty)
                        asset.RefD = "0.0";

                    minifiedValues mvReadingUOM = new minifiedValues();
                    minifiedValues mvConvertedUOM = new minifiedValues();

                    NaveoService.Models.DTO.DeviceAuxilliaryMap daM = new NaveoService.Models.DTO.DeviceAuxilliaryMap();
                    daM.uId = lauxType.Uid;
                    daM.mapId = lauxType.MapId;
                    daM.deviceId = lauxType.DeviceId;
                    daM.thresholdHigh = lauxType.Thresholdhigh;
                    daM.thresholdLow = lauxType.Thresholdlow;
                    daM.oprType = lauxType.oprType;
                    daM.stdConsumption = Convert.ToDecimal(asset.RefD);
                    daM.readingUOM = mvReadingUOM;
                    daM.convertedUOM = mvConvertedUOM;

                    if (lauxType.Field3 != null)
                    {
                        minifiedValues minifiedfuelProduct = new minifiedValues();
                        minifiedfuelProduct.id = (int)lauxType.Field3;
                        minifiedfuelProduct.description = lauxType.Field3Desc;
                        daM.fuelProduct = minifiedfuelProduct;
                        //assetDTO.assetDeviceMap.isFuel = true;

                    }

                    if (daM.mapId != null)
                    {
                        assetDTO.assetDeviceMap.isFuel = true;
                    }

                    assetDTO.assetDeviceMap.deviceAuxilliaryMap = daM;

                    foreach (var cal in lauxType.lCalibration)
                    {
                        if (asset.dtAllUOM != null)
                        {
                            DataRow[] drReadingUOM = asset.dtAllUOM.Select("UID = " + cal.ReadingUOM);
                            DataRow[] drConvertedUOM = asset.dtAllUOM.Select("UID = " + cal.ConvertedUOM);

                            if (drReadingUOM.Length > 0)
                            {
                                mvReadingUOM.id = cal.ReadingUOM;
                                mvReadingUOM.description = drReadingUOM[0]["Description"].ToString();
                            }

                            if (drConvertedUOM.Length > 0)
                            {
                                mvConvertedUOM.id = cal.ConvertedUOM;
                                mvConvertedUOM.description = drConvertedUOM[0]["Description"].ToString();
                            }
                        }
                        NaveoService.Models.DTO.CalibrationChart minifiedCal = new NaveoService.Models.DTO.CalibrationChart()
                        {
                            iId = cal.IID,
                            assetId = cal.AssetID,
                            deviceId = cal.DeviceID,
                            auxId = cal.AuxID,
                            reading = cal.Reading,
                            convertedValue = cal.ConvertedValue,
                            oprType = cal.oprType,
                        };

                        daM.lCalibration.Add(minifiedCal);
                    }
                }
            }

            #endregion

            #region UOM
            if (asset.lUOM != null)
            {

                foreach (NaveoOneLib.Models.Assets.AssetUOM uom in asset.lUOM)
                {
                    NaveoService.Models.DTO.AssetUOM u = new NaveoService.Models.DTO.AssetUOM();
                    DataRow[] dr = asset.dtAllUOM.Select("UID = " + uom.UOM);
                    if (dr.Length > 0)
                    {
                        mv = new minifiedValues();
                        mv.id = uom.UOM;
                        mv.description = dr[0]["Description"].ToString();

                        NaveoService.Models.DTO.AssetUOM minifiedUOM = new NaveoService.Models.DTO.AssetUOM
                        {
                            iId = uom.iID,
                            assetId = uom.AssetID,
                            capacity = uom.Capacity,
                            oprType = uom.oprType,
                            uom = mv
                        };
                        u = minifiedUOM;
                    }
                    assetDTO.lUOM.Add(u);
                }

            }
            #endregion
            #endregion

            return assetDTO;
        }

        /// <summary>
        /// Save/edit Asset
        /// </summary>
        /// <param name="sUserToken"></param>
        /// <param name="AssetModel"></param>
        /// <param name="UID"></param>
        /// <param name="sConnStr"></param>
        /// <returns></returns>
        public BaseDTO SaveUpdateAsset(SecurityTokenExtended securityToken, AssetDTO assetDTO, Boolean bInsert)
        {
            int userId = CommonService.GetUserID(securityToken.securityToken.UserToken, securityToken.securityToken.sConnStr);
            List<int> lUsers = new List<int>();
            lUsers.Add(userId);

            int uiD = -1;
            List<int> lValidatedUsers = CommonService.lValidateUsers(securityToken.securityToken.UserToken, lUsers, securityToken.securityToken.sConnStr, out uiD);
            int UID = lValidatedUsers[0];

            NaveoOneLib.Models.Assets.Asset a = new NaveoOneLib.Models.Assets.Asset();
            NaveoOneLib.Models.Assets.AssetDeviceMap dllAssetDeviceMap = new NaveoOneLib.Models.Assets.AssetDeviceMap();
            NaveoOneLib.Models.Assets.DeviceAuxilliaryMap dllDeviceAuxilliaryMap = new NaveoOneLib.Models.Assets.DeviceAuxilliaryMap();

            ServiceHelper.AssetDefaultValues(a);
            foreach (Models.DTO.apiAsset lasset in assetDTO.assets)
            {
                a.AssetID = lasset.assetId;
                a.AssetName = lasset.assetNumber;
                a.AssetNumber = lasset.assetNumber;
                a.Status_b = lasset.status_B;
                a.AssetType = lasset.assetType.id;
                a.TimeZoneID = lasset.timeZoneId;
                a.TimeZoneTS = lasset.timeZoneTs;
                a.oprType = lasset.oprType;
                a.YearManufactured = lasset.YearManufactured;
                a.PurchasedValue = lasset.PurchasedValue;
                a.RefA = lasset.RefA;
                a.RefB = lasset.RefB;
                a.RefC = lasset.RefC;
                a.ExternalRef = lasset.ExternalRef;
                a.EngineSerialNumber = lasset.EngineSerialNumber;
                a.EngineCapacity = lasset.EngineCapacity;
                a.EnginePower = lasset.EnginePower;
                a.oprType = lasset.oprType;

                if (lasset.PurchasedDate == null)
                    a.PurchasedDate = NaveoOneLib.Common.Constants.NullDateTime;
                else a.PurchasedDate = lasset.PurchasedDate;

                if (lasset.make != null)
                    a.MakeID = lasset.make.id;

                if (lasset.model != null)
                    a.ModelID = lasset.model.id;

                if (lasset.color != null)
                    a.ColorID = lasset.color.id;

                if (lasset.vehicleType != null)
                    a.VehicleTypeId = lasset.vehicleType.id;

                if (lasset.roadTypeSpeed != null)
                    a.RoadSpeedTypeId = lasset.roadTypeSpeed.id;

                #region  UOM
                foreach (Models.DTO.AssetUOM u in lasset.lUOM)
                {
                    NaveoOneLib.Models.Assets.AssetUOM dllAssetUOM = new NaveoOneLib.Models.Assets.AssetUOM();
                    a.lUOM = new List<NaveoOneLib.Models.Assets.AssetUOM>();
                    dllAssetUOM.AssetID = u.assetId;
                    dllAssetUOM.Capacity = u.capacity;
                    dllAssetUOM.iID = u.iId;
                    dllAssetUOM.UOM = u.uom.id;
                    dllAssetUOM.oprType = u.oprType;
                    a.lUOM.Add(dllAssetUOM);
                }
                #endregion

                #region Matrix
                foreach (Models.DTO.AssetMatrix aM in lasset.lMatrix)
                {
                    NaveoOneLib.Models.GMatrix.Matrix dllAssetMatrix = new NaveoOneLib.Models.GMatrix.Matrix();
                    dllAssetMatrix.GMID = aM.gMId;
                    dllAssetMatrix.GMDesc = aM.gMDesc;
                    dllAssetMatrix.iID = aM.iId;
                    dllAssetMatrix.oprType = aM.oprType;
                    dllAssetMatrix.MID = aM.mId;
                    a.lMatrix.Add(dllAssetMatrix);
                }
                #endregion

                #region Asset Device Mapping
                if (lasset.assetDeviceMap != null)
                {
                    dllAssetDeviceMap.MapID = lasset.assetDeviceMap.mapId;
                    dllAssetDeviceMap.AssetID = lasset.assetDeviceMap.assetId;
                    dllAssetDeviceMap.DeviceID = lasset.assetDeviceMap.deviceId;
                    dllAssetDeviceMap.SerialNumber = lasset.assetDeviceMap.serialNumber;
                    dllAssetDeviceMap.Status_b = lasset.assetDeviceMap.status_B;
                    dllAssetDeviceMap.oprType = lasset.assetDeviceMap.oprType;
                    dllAssetDeviceMap.StartDate = a.assetDeviceMap.StartDate;
                    dllAssetDeviceMap.EndDate = a.assetDeviceMap.EndDate;
                    a.assetDeviceMap = dllAssetDeviceMap;

                    if (lasset.assetDeviceMap.deviceAuxilliaryMap != null)
                    {
                        dllDeviceAuxilliaryMap.lCalibration = new List<NaveoOneLib.Models.Assets.CalibrationChart>();
                        dllDeviceAuxilliaryMap.Uid = lasset.assetDeviceMap.deviceAuxilliaryMap.uId;
                        dllDeviceAuxilliaryMap.MapId = lasset.assetDeviceMap.deviceAuxilliaryMap.mapId;
                        dllDeviceAuxilliaryMap.DeviceId = lasset.assetDeviceMap.deviceAuxilliaryMap.deviceId;

                        if (lasset.assetDeviceMap.deviceAuxilliaryMap.fuelProduct != null)
                            dllDeviceAuxilliaryMap.Field3 = lasset.assetDeviceMap.deviceAuxilliaryMap.fuelProduct.id;

                        dllDeviceAuxilliaryMap.Thresholdhigh = lasset.assetDeviceMap.deviceAuxilliaryMap.thresholdHigh;
                        dllDeviceAuxilliaryMap.Thresholdlow = lasset.assetDeviceMap.deviceAuxilliaryMap.thresholdLow;
                        dllDeviceAuxilliaryMap.oprType = lasset.assetDeviceMap.deviceAuxilliaryMap.oprType;
                        a.RefD = lasset.assetDeviceMap.deviceAuxilliaryMap.stdConsumption.ToString();
                        a.assetDeviceMap.deviceAuxilliaryMap = dllDeviceAuxilliaryMap;

                        foreach (var cal in lasset.assetDeviceMap.deviceAuxilliaryMap.lCalibration)
                        {
                            NaveoOneLib.Models.Assets.CalibrationChart dllCalibrationChart = new NaveoOneLib.Models.Assets.CalibrationChart();
                            dllCalibrationChart.IID = cal.iId;
                            dllCalibrationChart.AssetID = cal.assetId;
                            dllCalibrationChart.DeviceID = cal.deviceId;
                            dllCalibrationChart.AuxID = cal.auxId;
                            dllCalibrationChart.ReadingUOM = lasset.assetDeviceMap.deviceAuxilliaryMap.readingUOM.id;
                            dllCalibrationChart.Reading = cal.reading;
                            dllCalibrationChart.ConvertedUOM = lasset.assetDeviceMap.deviceAuxilliaryMap.convertedUOM.id;
                            dllCalibrationChart.ConvertedValue = cal.convertedValue;
                            dllCalibrationChart.oprType = cal.oprType;

                            #region logic added due to UOM changes on grid for calibration
                            if (lasset.assetDeviceMap.deviceAuxilliaryMap.oprType == DataRowState.Modified)
                                if (cal.oprType == 0)
                                    dllCalibrationChart.oprType = DataRowState.Modified;
                            #endregion

                            dllDeviceAuxilliaryMap.lCalibration.Add(dllCalibrationChart);
                        }

                        #region delete calibration if check isFuel is false on UI
                        if (lasset.assetDeviceMap.isFuel == false)
                            a.assetDeviceMap.deviceAuxilliaryMap.oprType = DataRowState.Deleted;
                        #endregion
                    }
                }
                #endregion
            }
            BaseModel assetObj = new NaveoOneLib.Services.Assets.AssetService().Save(a, bInsert, UID, securityToken.securityToken.sConnStr);

            return assetObj.ToBaseDTO(securityToken.securityToken);
        }

        /// <summary>
        /// Delete Asset
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="sConnStr"></param>
        /// <returns></returns>
        public BaseModel DeleteAsset(int assetId, SecurityTokenExtended securityToken)
        {

            BaseModel bm = new BaseModel();
            List<int> lAssetID = new List<int>();
            lAssetID.Add(assetId);

            int UID = -1;
            List<int> lValidatedAssets = CommonService.lValidateAssets(securityToken.securityToken.UserToken, lAssetID, securityToken.securityToken.sConnStr, out UID);

            if (lValidatedAssets.Count == 0)
            {
                bm.data = false;
                bm.errorMessage = "User does not have access to this AssetId " + assetId;
                return bm;
            }

            Asset asset = new Asset();
            foreach (int a in lValidatedAssets)
            {
                asset.AssetID = a;
            }


            Boolean bmAsset = new NaveoOneLib.Services.Assets.AssetService().DeleteAsset(asset, securityToken.securityToken.sConnStr);
            #region Delete in Maintenance
            if (bmAsset)
            {
                Connection _connection = new Connection(NaveoOneLib.Constant.NaveoEntity.MaintDB);
                if (_connection.GenDelete("dbo.\"CustomerVehicle\"", "\"FMSAssetId\" = '" + assetId + "'", NaveoOneLib.Constant.NaveoEntity.MaintDB))
                    bmAsset = true;
                else
                    bmAsset = false;
            }
            #endregion
            bm.data = bmAsset;
            return bm;
        }
        #endregion

        #region Garage
        public string AssetCreationTOGMS(Asset asset, String sServerName, String sConnStr)
        {


            string responseString = string.Empty;
            //sServerName = "KIDS";


            try
            {


                string url = NaveoEntity.GMSAPIAddress + "InsertAsset";


                Asset assetName = new NaveoOneLib.Services.Assets.AssetService().GetAssetByName(asset.AssetNumber, sConnStr);
                AssetDTO assetDTO = new AssetDTO();
                assetDTO.gmsAsset.asset.CustomerName = asset.lMatrix[0].GMDesc;
                assetDTO.gmsAsset.asset.Number = asset.AssetNumber;
                assetDTO.gmsAsset.asset.Make = asset.MakeID;
                assetDTO.gmsAsset.asset.Model = asset.ModelID;
                assetDTO.gmsAsset.asset.FuelType = asset.assetDeviceMap.deviceAuxilliaryMap.Field3;
                //chassisNo
                //Category
                //Status
                //RoadTaxExpiryDate
                //PSVLicenseExpiryDate
                //FitnessExpiryDate
                //RoadTaxAmount
                //FitnessAmount
                //PSVAmount
                assetDTO.gmsAsset.asset.IsFlagged = true;
                assetDTO.gmsAsset.asset.FleetServerId = 0;
                assetDTO.gmsAsset.asset.FleetServer = sServerName;
                assetDTO.gmsAsset.asset.FMSAssetId = assetName.AssetID;
                assetDTO.gmsAsset.asset.FMSAssetType = asset.AssetType;
                //assetDTO.gmsAsset.asset.Description = asset.AssetType;
                //Section
                assetDTO.gmsAsset.asset.VehicleType = asset.VehicleTypeId;
                assetDTO.gmsAsset.asset.Colour = asset.sColor;
                //SupplierId
                assetDTO.gmsAsset.asset.YearManufactured = asset.YearManufactured;
                assetDTO.gmsAsset.asset.PurchasedDate = asset.PurchasedDate;
                //assetDTO.gmsAsset.asset.RegistrationDate = asset.Re;
                assetDTO.gmsAsset.asset.PurchasedValue = asset.PurchasedValue;
                //assetDTO.gmsAsset.asset.ExpectedLifetime = asset.Ex;
                //assetDTO.gmsAsset.asset.ResidualValue = asset.
                assetDTO.gmsAsset.asset.Reference = asset.ExternalRef;
                assetDTO.gmsAsset.asset.IsInService = true;
                assetDTO.gmsAsset.asset.EngineSerialNumber = asset.EngineSerialNumber;
                //assetDTO.gmsAsset.asset.EngineType = asset.Ene;
                //assetDTO.gmsAsset.asset.EngineCapacity = asset.EngineCapacity;
                //assetDTO.gmsAsset.asset.EnginePower = asset.EnginePower;
                assetDTO.gmsAsset.asset.StandardConsumption = asset.RefD;
                //assetDTO.gmsAsset.asset.Department = asset.RefD;
                assetDTO.gmsAsset.asset.Remarks = "Assets";



                //if (assetDTO.gmsAsset.asset.CustomerName == "### All Clients ###" || assetDTO.gmsAsset.asset.CustomerName == "Assets deleted" || assetDTO.gmsAsset.asset.CustomerName == "Deleted Assets" || assetDTO.gmsAsset.asset.CustomerName == "Deleted Users" || assetDTO.gmsAsset.asset.CustomerName == "tftfj" || assetDTO.gmsAsset.asset.CustomerName == "AbdeltestApproval" || assetDTO.gmsAsset.asset.CustomerName == "dsadsad" || assetDTO.gmsAsset.asset.CustomerName == "groupTest" || assetDTO.gmsAsset.asset.CustomerName == "TestGroupV2")
                //{
                //    responseString = "AVOID";
                //    //return responseString;

                //}
                //else
                //{
                //    responseString = new NaveoService.Helpers.ApiHelper().ApiCall("Post", url, assetDTO.gmsAsset);
                //}

                responseString = new NaveoService.Helpers.ApiHelper().ApiCall("Post", url, assetDTO.gmsAsset);
            }
            catch (Exception ex)
            {
                responseString = ex.ToString();
                System.IO.File.AppendAllText(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "\\Registers\\gms.dat", "\r\n Asset Creation \r\n" + responseString + "\r\n");
            }



            return responseString;
        }

        public BaseModel GetAssetMaintDetailsByAssetId(SecurityTokenExtended securityToken, int AssetID, DateTime? dtStart, DateTime? dtEnd, int inputOdo, int intputEngineHrs)
        {
            List<int> lAssetID = new List<int>();
            lAssetID.Add(AssetID);


            DataTable dtTable = new DataTable();
            dtTable.Columns.Add("Odometer", typeof(int));
            dtTable.Columns.Add("EngineHours", typeof(int));


            BaseModel bm = new BaseModel();

            int UID = -1;
            List<int> lValidatedAssets = CommonService.lValidateAssets(securityToken.securityToken.UserToken, lAssetID, securityToken.securityToken.sConnStr, out UID);

            if (lValidatedAssets.Count == 0)
            {
                bm.data = "You don't have access to this asset";
                return bm;
            }

            DataRow drNew = dtTable.NewRow();

            if (dtStart.HasValue || dtEnd.HasValue)
            {
                DateTime dateTimeStart = Convert.ToDateTime(dtStart);
                DateTime dateTimeEnd = Convert.ToDateTime(dtEnd);
                DataTable dt = new NaveoOneLib.Services.Assets.AssetService().GetAssetMntDetails(lValidatedAssets[0], dateTimeStart, dateTimeEnd, securityToken.securityToken.sConnStr);

                foreach (DataRow dr in dt.Rows)
                {
                    int EngineHours = Convert.ToInt32(dr["EngineHours"]);
                    int Odometers = Convert.ToInt32(dr["Odometer"]);
                    EngineHours = EngineHours / 3600;

                    drNew["EngineHours"] = EngineHours + intputEngineHrs;
                    drNew["Odometer"] = Odometers + inputOdo;


                }
            }
            else
            {
                drNew["EngineHours"] = 0;
                drNew["Odometer"] = 0;
            }


            dtTable.Rows.Add(drNew);


            bm.data = dtTable;
            bm.total = dtTable.Rows.Count;
            return bm;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="securityToken"></param>
        /// <param name="AssetIDs"></param>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <returns></returns>
        public BaseModel GetMaintenanceFuelCost(Guid sUserToken, String sConnStr, List<int> AssetIDs, DateTime? dtStart, DateTime? dtEnd)
        {
            BaseModel bm = new BaseModel();

            DataTable dtTable = new DataTable();

            string responseString = string.Empty;

            try
            {
                string url = NaveoEntity.GMSAPIAddress + "GetFuelData";  //"https://gmstest.naveo.mu/gmsapi/GetFuelData"; 

                MaintenanceAndFuelCostData maintenanceAndFuelCostData = new MaintenanceAndFuelCostData();

                //Assetname
                dtTable.Columns.Add("ASSET NAME", typeof(string));

                //API Garage Fuel COst
                dtTable.Columns.Add("FUEL", typeof(double));

                //Monthly fuel liters used/Monthly Total Engine hour
                dtTable.Columns.Add("FUEL (Lt/Hr)", typeof(double));

                //KM cover on TripHeader, Totals, totalTripDistance
                dtTable.Columns.Add("KM", typeof(int));

                //Litre used API Garage Fuel Quantity
                dtTable.Columns.Add("LITERS USED", typeof(int));

                //Litre Used per 100KM modulo
                dtTable.Columns.Add("LITERS PER 100KM", typeof(decimal));

                //API Garage Maintenance Cost
                dtTable.Columns.Add("SERVICING", typeof(decimal));

                //API Garage Repairs
                dtTable.Columns.Add("REPAIRS", typeof(decimal));

                dtTable.Columns.Add("TOTAL SPEND", typeof(decimal));

                int UID = -1;
                List<int> lValidatedAssets = CommonService.lValidateAssets(sUserToken, AssetIDs, sConnStr, out UID);

                List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);

                if (lValidatedAssets.Count == 0)
                {
                    bm.data = "You don't have access to this asset";
                    return bm;
                }

                //Add Total
                DataSet ds = new TripHeaderService().GetDailyTripTime(lValidatedAssets, (DateTime)dtStart, (DateTime)dtEnd, "", false, sConnStr);

                DataTable dtTrips = ds.Tables["Pivot"];

                maintenanceAndFuelCostData.AssetsId = lValidatedAssets;

                maintenanceAndFuelCostData.FMSServer = new NaveoService.Helpers.ApiHelper().getServerName().ToUpper(); //"KIDS"

                maintenanceAndFuelCostData.From = (DateTime)dtStart;
                maintenanceAndFuelCostData.To = (DateTime)dtEnd;

                apigarageFuelData fuelDate = new apigarageFuelData();

                //Add API CAll here
                fuelDate = new NaveoService.Helpers.ApiHelper().ApiCallToUse("Post", url, maintenanceAndFuelCostData);

                DataTable dtTH = new NaveoOneLib.Services.Trips.TripHeaderService().GetTripHeaders(lValidatedAssets, (DateTime)dtStart, (DateTime)dtEnd, NaveoEntity.GenericBypass, "DailySummary", false, false, true, lm, sConnStr, null, null, true, new List<string>(), false).Tables["Totals"];

                DateTime dateStarting = new DateTime();

                dateStarting = (DateTime)dtStart;

                if (dtStart.HasValue)
                {

                    foreach (garageFuelData garageValues in fuelDate.lgarageFuelData)
                    {
                        DataRow row = dtTable.NewRow();

                        string AssetNumber = new AssetService().GetAssetDataById(sUserToken, sConnStr, garageValues.AssetId).assetNumber;

                        row["ASSET NAME"] = AssetNumber;

                        DataTable dtFuel = new FuelService().GetFuelData(sUserToken, garageValues.AssetId, (DateTime)dtStart, (DateTime)dtEnd, sConnStr);

                        var rows = dtFuel.Rows;

                        List<int> litreUsed = new List<int>();

                        int litreUsedOverall = 0;

                        int litreRemaining = 0;

                        foreach (DataRow dr in rows)
                        {

                            if (dr["fType"].ToString() == "Fill")
                            {
                                //Take diff in litre
                                //litre = (int)dr["diff"];

                                int diff = 0;

                                if (dr["diff"].ToString() == string.Empty)
                                    diff = 0;
                                else
                                    diff = int.Parse(dr["diff"].ToString());

                                litreRemaining = int.Parse(dr["convertedValue"].ToString()) - diff;

                                litreUsedOverall = diff - litreRemaining;

                                litreUsed.Add(litreUsedOverall);

                            }

                        }

                        //KM FILL
                        row["KM"] = dtTH.Rows[0]["totalTripDistance"].ToString();

                        if (garageValues.ServicingCost > 0)
                        {
                            row["SERVICING"] = Math.Round((decimal)garageValues.ServicingCost, 2);
                        }

                        if (garageValues.RepairCost > 0)
                        {
                            row["REPAIRS"] = Math.Round((decimal)garageValues.RepairCost, 2);
                        }

                        decimal totalFuelCost = 0;

                        decimal KM = int.Parse(dtTH.Rows[0]["totalTripDistance"].ToString());

                        decimal litreUsedSum = litreUsed.Sum();

                        decimal LitrePerKM = litreUsedSum / KM * 100;

                        decimal FueluantityGarage = (decimal)garageValues.FuelQuantity;

                        decimal KMGarage = int.Parse(dtTH.Rows[0]["totalTripDistance"].ToString());

                        decimal LitrePerKMGarage = FueluantityGarage / KMGarage * 100;

                        //if (garageValues.FuelCost > 0)
                        //{
                        row["FUEL"] = Math.Round((double)garageValues.FuelCost, 2);

                        if (rows.Count > 0 && litreUsedSum > 0)
                        {
                            row["LITERS USED"] = litreUsed.Sum();

                            foreach (DataRow trips in dtTrips.Rows)
                            {

                                if (trips["sDesc"].ToString() == AssetNumber)
                                {
                                    string[] NumberOfDaysAndHours = trips["Total"].ToString().Split(':');

                                    string[] NumberOfDays = NumberOfDaysAndHours[0].Split('.');

                                    int totalHours = int.Parse(NumberOfDays[0]) * 24 + int.Parse(NumberOfDaysAndHours[1]);

                                    string NewTotal = string.Format("{0}:{1}:{2}", totalHours, NumberOfDaysAndHours[2], NumberOfDaysAndHours[3]);

                                    string[] NumberOfDaysFinal = NewTotal.Split(':');

                                    TimeSpan ts = new TimeSpan(int.Parse(NumberOfDaysFinal[0]), int.Parse(NumberOfDaysFinal[1]), int.Parse(NumberOfDaysFinal[2]));
                                    double totalHoursFinal = ts.TotalHours;

                                    //Litre / Hours
                                    row["FUEL (Lt/Hr)"] = Math.Round(litreUsed.Sum() / totalHoursFinal, 2);
                                }

                            }

                            row["LITERS PER 100KM"] = Math.Round(LitrePerKM, 2);
                        }
                        else
                        {
                            row["LITERS USED"] = garageValues.FuelQuantity;
                            row["LITERS PER 100KM"] = Math.Round(LitrePerKMGarage, 2);//garageValues.FuelQuantity / int.Parse(dtTH.Rows[0]["totalTripDistance"].ToString()) * 100;
                        }

                        totalFuelCost = Math.Round(decimal.Parse(garageValues.FuelCost.ToString()), 2);
                        //}

                        decimal totalSpend = totalFuelCost + (decimal)garageValues.ServicingCost + (decimal)garageValues.RepairCost;

                        row["TOTAL SPEND"] = Math.Round(totalSpend, 2);

                        dtTable.Rows.Add(row);
                    }
                }


            }
            catch (Exception ex)
            {
                return bm.data = ex.Message;
                //throw;
            }


            bm.data = dtTable;
            bm.total = dtTable.Rows.Count;
            return bm;
        }

        public BaseModel MigrateAssetToGMS(SecurityTokenExtended securityToken)
        {
            Boolean BInsert = false;
            int UID = CommonService.GetUserID(securityToken.securityToken.UserToken, securityToken.securityToken.sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, securityToken.securityToken.sConnStr);
            DataTable dtAssets = new NaveoOneLib.Services.Assets.AssetService().GetAllAssets(securityToken.securityToken.sConnStr);
            int TotalAssets = dtAssets.Rows.Count;

            string sServerName = new NaveoService.Helpers.ApiHelper().getServerName();
            //sServerName = "BABY";
            int cntCreated = 0;
            int cntExist = 0;

            string ErrorServer = string.Empty;
            foreach (DataRow dr in dtAssets.Rows)
            {
                Asset a = new Asset();
                int assetId = Convert.ToInt32(dr["AssetID"]);
                string status = string.Empty;

                try
                {
                    Asset asset = new NaveoOneLib.Services.Assets.AssetService().GetAssetById(assetId, UID, true, securityToken.securityToken.sConnStr);



                    if (asset != null)
                    {
                        try
                        {
                            status = AssetCreationTOGMS(asset, sServerName, securityToken.securityToken.sConnStr);

                            string createdOrExist = string.Empty;
                            if (status.Contains("Asset already exists"))
                            {
                                cntExist++;
                                createdOrExist = "already exists on GMS,Total: " + cntExist;



                            }
                            else if (status.Contains("Valid"))
                            {
                                cntCreated++;
                                createdOrExist = "created on GMS,Total: " + cntCreated;


                            }

                            string message = "Asset: " + asset.AssetNumber + " " + createdOrExist;

                            System.IO.File.AppendAllText(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "\\Registers\\gms.dat", "\r\n Migration Asset \r\n" + message + "\r\n");

                            BInsert = true;
                        }
                        catch (Exception ex)
                        {

                            System.IO.File.AppendAllText(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "\\Registers\\gms.dat", "\r\n Migration Asset \r\n" + ex.Message + "\r\n" + ex.InnerException);
                        }
                    }

                }
                catch (Exception ex)
                {
                    System.IO.File.AppendAllText(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "\\Registers\\gms.dat", "\r\n Migration Asset \r\n" + ex.Message + "\r\n" + ex.InnerException + dr["AssetNumber"].ToString());
                }

                ErrorServer = status;
            }


            string messsag = cntCreated + "out of " + TotalAssets + "has been migrated to GMS";

            BaseModel bm = new BaseModel();
            bm.data = BInsert;
            bm.errorMessage = messsag + "Server :" + sServerName;



            return bm;
        }

        /// <summary>
        /// Processing to return a treeview
        /// </summary>
        /// <author>Yaseen</author>
        /// <returns>DataTable</returns>
        public dtData treeViewSource(Guid sUserToken, String sConnStr, NaveoModels nm, int? Page, int? LimitPerPage)
        {
            try
            {
                int UID = new NaveoOneLib.Services.Users.UserService().GetUserID(sUserToken, sConnStr);
                if (UID == -1)
                    return new dtData();

                List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);
                List<int> iParentIDs = new List<int>();
                foreach (NaveoOneLib.Models.GMatrix.Matrix m in lm)
                    iParentIDs.Add(m.GMID);

                NaveoOneLib.DBCon.Connection c = new NaveoOneLib.DBCon.Connection(sConnStr);
                DataTable dtSql = c.dtSql();
                DataRow drSql = dtSql.NewRow();

                String sql = new NaveoOneLib.Services.GMatrix.GroupMatrixService().sGetAllGMValues(iParentIDs, nm);
                if (nm == NaveoModels.Users)
                    sql = sql.Replace("a.Names", "a.Names + '(' + a.Email + ')'");
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dt";
                dtSql.Rows.Add(drSql);

                DataSet ds = c.GetDataDS(dtSql, sConnStr);
                DataTable dtGM = ds.Tables[0];

                //dtGM.Columns.Remove("myStatus");
                dtGM.Columns.Remove("MID");
                dtGM.Columns.Remove("MatrixiID");
                dtGM.Columns.Add("type");
                dtGM.Columns.Add("id", typeof(Int32));
                //Data retrieve from database
                DataTable myData = dtGM.Clone();
                DataTable myGrp = dtGM.Clone();

                if (nm == NaveoModels.Groups)
                    foreach (DataRow dr in dtGM.Rows)
                    {
                        int i = -1;
                        int.TryParse(dr["GMID"].ToString(), out i);
                        if (i > 0)
                        {
                            dr["Type"] = "G";
                            dr["id"] = dr["GMID"];
                            myData.Rows.Add(dr.ItemArray);
                        }
                    }
                else
                    //Parent at Top not configure here
                    //foreach (DataRow drParent in dtGM.Rows)
                    //{
                    //    int iParent = -1;

                    //    //Check if an integar
                    //    int.TryParse(drParent["GMID"].ToString(), out iParent);

                    //    if (iParent < 0 != true)
                    //    {
                    //        if (drParent["GMDescription"].ToString() == "Default Driver")
                    //            //ParentId 1
                    //            if (drParent["ParentGMID"].ToString() == "1")
                    //            {
                    //                DataRow[] drd = dtGM.Select("GMID = 1");
                    //                if (drd.Length == 0)
                    //                    drParent["ParentGMID"] = "-1";
                    //                drParent["Type"] = "G";
                    //                drParent["id"] = drParent["GMID"];
                    //                myGrp.Rows.Add(drParent.ItemArray);
                    //                break;
                    //            }
                    //    }


                    //}
                    foreach (DataRow dr in dtGM.Rows) //Always enter here as values = assets
                    {
                        int i = -1;
                        int iChild = -1;

                        //Check if an integar
                        int.TryParse(dr["GMID"].ToString(), out i);
                        //If i < 0 its of type G
                        if (i < 0 != true)
                        {
                            dr["Type"] = "G";
                            dr["id"] = dr["GMID"];
                            myGrp.Rows.Add(dr.ItemArray);

                            //look for child if any
                            foreach (DataRow drChild in dtGM.Rows) //Always enter here as values = assets
                            {
                                int.TryParse(drChild["GMID"].ToString(), out iChild);

                                if (iChild < 0)
                                {
                                    if (drChild["ParentGMID"].ToString() == dr["id"].ToString())
                                    {
                                        drChild["Type"] = "O";
                                        drChild["id"] = drChild["StrucID"];
                                        myGrp.Rows.Add(drChild.ItemArray);
                                    }

                                }
                            }
                        }


                        //To remove
                        //if (i < 0)
                        //{
                        //    if (dr["GMDescription"].ToString() == "Default Driver")
                        //        //ParentId 1
                        //        if (dr["ParentGMID"].ToString() == "1")
                        //        {
                        //            DataRow[] drd = dtGM.Select("GMID = 1");
                        //            if (drd.Length == 0)
                        //                dr["ParentGMID"] = "-1";
                        //        }
                        //    dr["Type"] = "O";
                        //    dr["id"] = dr["StrucID"];
                        //    myData.Rows.Add(dr.ItemArray);
                        //}
                        //else
                        //{
                        //    dr["Type"] = "G";
                        //    dr["id"] = dr["GMID"];
                        //    myGrp.Rows.Add(dr.ItemArray);
                        //}
                    }
                //myData.Columns.Remove("GMID");
                //myData.Columns.Remove("StrucID");
                myGrp.Columns.Remove("GMID");
                myGrp.Columns.Remove("StrucID");

                if (nm == NaveoModels.Assets)
                {
                    myGrp.Columns.Add("TimeZone");

                    DataRow[] dr0 = myGrp.Select("Type = 'O'");
                    if (dr0.Length > 0)
                    {
                        List<Asset> la = new NaveoOneLib.WebSpecifics.LibData().lAsset(UID, sConnStr);
                        foreach (Asset a in la)
                        {
                            DataRow[] dr = myGrp.Select("id = '" + a.AssetID + "'");
                            if (dr.Length > 0)
                                foreach (DataRow d in dr)
                                    d["TimeZone"] = a.TimeZoneTS;
                        }



                    }
                }

                DataTable ResultTable = new DataTable();
                ResultTable.Columns.Add("rowNum", typeof(int));
                ResultTable.Columns["rowNum"].AutoIncrement = true;
                ResultTable.Columns["rowNum"].AutoIncrementSeed = 1;
                ResultTable.Columns["rowNum"].AutoIncrementStep = 1;
                ResultTable.Merge(myGrp);

                Pagination p = Pagination.GetRowNums(Page, LimitPerPage);
                DataTable dt = new DataTable();
                DataRow[] drChk = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo);
                if (drChk != null && drChk.Length > 0)
                    dt = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo).CopyToDataTable();

                dt.TableName = "dt" + nm.ToString();

                //Inversing add mygrp 1st
                /*Old Code 
                //dt.Merge(myGrp);
                dt.Columns["GMDescription"].ColumnName = "description";
                dt.Columns["ParentGMID"].ColumnName = "parentID";
                dtData dtD = new dtData();
                dtD.Data = dt;
                 */


                //New code
                //myGrp.Merge(dt);

                myGrp.Columns["GMDescription"].ColumnName = "text";
                myGrp.Columns["ParentGMID"].ColumnName = "parent";

                dtData dtD = new dtData();
                dtD.Data = myGrp; //End New code
                dtD.Rowcnt = myGrp.Rows.Count;

                return dtD;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        #endregion
    }
}
