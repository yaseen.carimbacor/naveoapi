﻿using NaveoOneLib.Models;
using NaveoOneLib.Models.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace NaveoService.Services
{
    public class FuelService
    {
        public DataTable GetFuelData(Guid sUserToken, int AssetID, DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            BaseModel bmData = new NaveoOneLib.Services.Fuels.FuelService().GetFuelData(UID, AssetID, dtFrom, dtTo, sConnStr);

            #region case sensitive logic for PostGres sql
            if (bmData.data.Columns.Contains("gpsuid"))
                bmData.data.Columns["gpsuid"].ColumnName = "gpsUID";

            if (bmData.data.Columns.Contains("assetid"))
                bmData.data.Columns["assetid"].ColumnName = "assetID";

            if (bmData.data.Columns.Contains("convertedvalue"))
                bmData.data.Columns["convertedvalue"].ColumnName = "convertedValue";

            if (bmData.data.Columns.Contains("assetname"))
                bmData.data.Columns["assetname"].ColumnName = "AssetName";

            if (bmData.data.Columns.Contains("datetimegps_utc"))
                bmData.data.Columns["datetimegps_utc"].ColumnName = "dateTimeGPS_UTC";

            if (bmData.data.Columns.Contains("ftype"))
                bmData.data.Columns["ftype"].ColumnName = "fType";

     

            #endregion


            return bmData.data;
        }



        public BaseModel GetFuelConsumption(Guid sUserToken, int AssetID, int? Page, int? LimitPerPage, String sConnStr)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            BaseModel dsGetFuelConsumption = new NaveoOneLib.Services.Fuels.FuelService().GetFuelConsumption(UID, null, null, AssetID, sConnStr, Page, LimitPerPage);

            return dsGetFuelConsumption;
        }

        public BaseModel GetFuelConsumptionReport(Guid sUserToken, DateTime? dtFrom, DateTime? dtTo, List<int> lAssetIds, int? Page, int? LimitPerPage, String sConnStr, List<String> sortColumns, Boolean sortOrderAsc)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            BaseModel dsGetFuelConsumption = new NaveoOneLib.Services.Fuels.FuelService().GetFuelConsumptionReport(UID, dtFrom, dtTo, lAssetIds, sConnStr, Page, LimitPerPage, sortColumns, sortOrderAsc);

            return dsGetFuelConsumption;
        }

        public BaseModel GetPeriodicFuelSummary(Guid sUserToken, DateTime? dtFrom, DateTime? dtTo, List<int> lAssetIds, int? Page, int? LimitPerPage, String sConnStr, List<String> sortColumns, Boolean sortOrderAsc)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            BaseModel dsGetFuelConsumption = new NaveoOneLib.Services.Fuels.FuelService().GetFuelConsumptionReport(UID, dtFrom, dtTo, lAssetIds, sConnStr, Page, LimitPerPage, sortColumns, sortOrderAsc);

            return dsGetFuelConsumption;
        }

        public BaseModel GetPossibleFuelDrops(Guid sUserToken, int fuelDataUID, String sConnStr)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            BaseModel GetPossibleFuelDrops = new NaveoOneLib.Services.Fuels.FuelService().GetPossibleFuelDrops(UID, fuelDataUID, sConnStr);
            return GetPossibleFuelDrops;
        }


        public BaseModel GetFuelReport(Guid sUserToken,DateTime DateFrom, DateTime DateTo, List<int> lAssetID, int? Page, int? LimitPerPage, String sConnStr)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            DataTable dtFuelFleet = new DataTable();

            foreach (int AssetID in lAssetID)
            {

                BaseModel dsGetFuelConsumption = new NaveoOneLib.Services.Fuels.FuelService().GetFuelConsumption(UID, null, null, AssetID, sConnStr, Page, LimitPerPage);
                DataTable dt = new DataTable();
                DataSet ds = new DataSet();
                ds = dsGetFuelConsumption.data;
                dt = ds.Tables["FuelData"];
                dtFuelFleet.Merge(dt.Copy());
               
            }

            NaveoService.Services.MaintAssetServices maintservice = new NaveoService.Services.MaintAssetServices();

            BaseModel bm = maintservice.GetFuelData(lAssetID, DateFrom, DateTo, sConnStr);
            DataTable dtFueltMaint = bm.data;
            DataTable dtFinal = new DataTable();
            dtFinal.Columns.Add("date");
            dtFinal.Columns.Add("drivername");
            dtFinal.Columns.Add("previousodometer");
            dtFinal.Columns.Add("lastodometer");
            dtFinal.Columns.Add("kmtravelled");
            dtFinal.Columns.Add("litres(indent)");
            dtFinal.Columns.Add("litres(sensor)");
            dtFinal.Columns.Add("avgconsumptionindent");
            dtFinal.Columns.Add("avgconsumptionsensor");
            dtFinal.Columns.Add("amount");
            dtFinal.Columns.Add("fillingstation");

            foreach(DataRow dr in dtFuelFleet.Rows)
            {
                
                DataRow drNew = dtFinal.NewRow();
                //drNew["date"] = 
                //drNew["drivername"] =
                drNew["previousodometer"] = dtFueltMaint.Rows[1]["Mileage"].ToString();
                drNew["lastodometer"] = dtFueltMaint.Rows[0]["Mileage"].ToString();
                drNew["lastodometer"] = dtFueltMaint.Rows[0]["Mileage"].ToString();
                drNew["kmtravelled"] = dtFueltMaint.Rows[0]["Mileage"].ToString();
                drNew["litres(indent)"] = dtFueltMaint.Rows[0]["Mileage"].ToString();
                //drNew["litres(sensor)"] = dr["ConvertedValue"].ToString(); //ConvertedValue
                //drNew["avgconsumptionindent"] = dtFueltMaint.Rows[0]["Mileage"].ToString();
                //drNew["avgconsumptionsensor"] = dtFueltMaint.Rows[0]["Mileage"].ToString();
                //drNew["amount"] = dtFueltMaint.Rows[0]["Mileage"].ToString();
                //drNew["fillingstation"] = dtFueltMaint.Rows[0]["Mileage"].ToString();
            }


            BaseModel bmfinal = new BaseModel();
            bmfinal.data = dtFuelFleet;

            return bmfinal;
        }

    }
}