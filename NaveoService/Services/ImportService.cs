﻿using NaveoOneLib.Common;
using NaveoOneLib.Constant;
using NaveoService.Models;
using NaveoService.Models.DTO;
using NaveoService.Models.ImportTemplates;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;

namespace NaveoService.Services
{
    public class ImportService
    {
        public async Task<BaseDTO> UploadFile(SecurityTokenExtended securityTokenObj)
        {
            BaseDTO baseDTO = new BaseDTO();
            if (HttpContext.Current.Request.Files.AllKeys.Any())
            {
                String entity = String.Empty;
                String json = String.Empty;
                String AssetID = String.Empty;

                var re = HttpContext.Current.Request;
                var headers = re.Headers;
                foreach (var h in headers)
                {
                    if (h.ToString() == "entity")
                        entity = headers.GetValues("entity").First();

                    if (h.ToString() == "data")
                    {
                        var jsonFromHeader = headers.GetValues("data").First();
                        json = jsonFromHeader.ToString();
                        json = GetNestedStringFromStartPos(4, json, '{', '}');
                        json = "{" + json + "}";
                    }

                    if (h.ToString().ToLower() == "assetid")
                        AssetID = headers.GetValues("assetid").First();
                }

                // Get the posted file
                HttpPostedFile uploadedFile = HttpContext.Current.Request.Files[0];

                //if (NaveoWebApi.Helpers.UtilityHelper.File.IsValid(excelFile) && excelFile != null)
                //{
                /// Also have a way to save file on server, then access it to loop
                //UtilityHelper.File.Save(excelFile, filePath);
                //FileInfo file = new FileInfo(filePath);
                // using (ExcelPackage excelPackage = new ExcelPackage(file))
                String result = String.Empty;
                try
                {
                    entity = entity.ToUpper();
                    switch (entity)
                    {
                        case NaveoEntity.USERS:
                            List<UserTemplateRow> userTemplateRows = await NaveoService.Helpers.UtilityHelper.Document.FillUserExcelTemplateRowAsync(uploadedFile);
                            DataTable userDataTable = NaveoService.Helpers.UtilityHelper.Datatable.ToDataTable(userTemplateRows);
                            return null;

                        case NaveoEntity.PLANNING:
                            List<PlanningTemplateRow> planningTemplateRows = new NaveoService.Helpers.UtilityHelper.Document().FillPlanningExcelTemplateRow(uploadedFile);
                            DataTable planningDataTable = NaveoService.Helpers.UtilityHelper.Datatable.ToDataTable(planningTemplateRows);
                            result = new NaveoService.Services.PlanningService().UploadPlanningRequest(securityTokenObj, planningDataTable);
                            baseDTO.errorMessage = result;

                            if (!result.Contains("Error"))
                                baseDTO.sQryResult = "Succeeded";

                            return baseDTO;

                        case NaveoEntity.ZONES:
                            JavaScriptSerializer serializer = new JavaScriptSerializer();
                            ZoneData jsonSaveObject = serializer.Deserialize<ZoneData>(json.ToString());

                            List<ZoneTemplateRow> zoneTemplateRows = new NaveoService.Helpers.UtilityHelper.Document().FillZoneExcelTemplateRow(uploadedFile);
                            DataTable zoneDataTable = NaveoService.Helpers.UtilityHelper.Datatable.ToDataTable(zoneTemplateRows);
                            DataSet ds = new NaveoService.Services.ZoneService().UploadZone(securityTokenObj, zoneDataTable, jsonSaveObject);

                            DataTable dt = ds.Tables["dtResult"];
                            DataTable dtData = ds.Tables["dtData"];
                            result = dt.Rows[0].Field<string>(1);
                            baseDTO.errorMessage = result;
                            baseDTO.data = dtData;

                            if (!result.Contains("Error"))
                                baseDTO.sQryResult = "Succeeded";

                            return baseDTO;

                        case NaveoEntity.TERRAPLANNING:
                            List<TerraPlanningTemplateRow> terraPlanningTemplateRows = new NaveoService.Helpers.UtilityHelper.Document().FillTerraPlanningExcelTemplateRow(uploadedFile);
                            DataTable terraplanningDataTable = NaveoService.Helpers.UtilityHelper.Datatable.ToDataTable(terraPlanningTemplateRows);
                            result = new NaveoService.Services.ScheduleService().UploadPlanningRequest(securityTokenObj, terraplanningDataTable);
                            baseDTO.errorMessage = result;

                            if (!result.Contains("Error"))
                                baseDTO.sQryResult = "Succeeded";

                            return baseDTO;

                        case NaveoEntity.LOOKUPVALUES:
                            List<LookUpValuesTemplateRow> lookupValuesTemplateRows = new NaveoService.Helpers.UtilityHelper.Document().FillUploadExcelTemplateRow(uploadedFile);
                            DataTable lookupValuesTemplateRowsplanningDataTable = NaveoService.Helpers.UtilityHelper.Datatable.ToDataTable(lookupValuesTemplateRows);
                            result = new NaveoService.Services.LookUpValuesService().UploadLookUpValues(securityTokenObj, lookupValuesTemplateRowsplanningDataTable);
                            baseDTO.errorMessage = result;

                            if (!result.Contains("Error"))
                                baseDTO.sQryResult = "Succeeded";

                            return baseDTO;

                        case NaveoEntity.ZONES_UPDATE:
                            List<ZoneTemplateRow> zoneUpdateTemplateRows = new NaveoService.Helpers.UtilityHelper.Document().FillZoneUpdateExcelTemplateRow(uploadedFile);
                            DataTable zoneUpdateDataTable = NaveoService.Helpers.UtilityHelper.Datatable.ToDataTable(zoneUpdateTemplateRows);
                            result = new NaveoService.Services.ZoneService().UpdateZoneLookUpValues(securityTokenObj, zoneUpdateDataTable);
                            baseDTO.errorMessage = result;

                            if (!result.Contains("Error"))
                                baseDTO.sQryResult = "Succeeded";

                            return baseDTO;

                        case NaveoEntity.FixedAssetImage:
                            String imageName = Guid.NewGuid().ToString() + ".jpg";
                            uploadedFile.SaveAs(Globals.DocsPath("FixedAssets\\" + AssetID) + imageName);
                            baseDTO.data = imageName;
                            baseDTO.sQryResult = "Succeeded";
                            break;

                        default:
                            break;
                    }
                }
                catch (Exception e)
                {
                    baseDTO.errorMessage = "An error occured while filling the template rows or process on datatable has failed!!! " + e.Message;
                    return baseDTO;
                }

                /// Delete File
                // UtilityHelper.File.Delete(filePath);

                //    BD.errorMessage = "File format not supported. Use a file with .xls or .xlsx extension";
                //    return BD;
            }

            baseDTO.errorMessage = NaveoEntity.NoUploadedFile;
            return baseDTO;
        }
        public Boolean DeleteFixedAssetPicture(NaveoOneLib.Models.Assets.FixedAsset fixedAsset)
        {
            Boolean b = false;
            File.Delete(Globals.DocsPath("FixedAssets\\" + fixedAsset.AssetID) + fixedAsset.PictureName);
            b = true;

            return b;
        }

        static string GetNestedStringFromStartPos(int StartPos, string str, char start, char end)
        {
            int s = -1;
            int i = StartPos;
            while (++i < str.Length)
                if (str[i] == start)
                {
                    s = i;
                    break;
                }
            int e = -1;
            int depth = 0;
            while (++i < str.Length)
                if (str[i] == end)
                {
                    e = i;
                    if (depth == 0)
                        break;
                    else
                        --depth;
                }
                else if (str[i] == start)
                    ++depth;
            if (e > s)
                return str.Substring(s + 1, e - s - 1);
            return null;
        }
    }
}
