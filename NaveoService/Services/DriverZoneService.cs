﻿using NaveoOneLib.Models.Common;
using NaveoOneLib.Services.GMatrix;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NaveoService.Models.DTO;
using NaveoService.Models;
using NaveoOneLib.Models.Shifts;
using NaveoOneLib.DBCon;

namespace NaveoService.Services
{
    public class DriverZoneService
    {

        public BaseModel GetDriverZone(DateTime dtFrom, DateTime dtTo, Guid sUserToken, string sConnStr)
        {

            BaseModel baseModel = new BaseModel();

            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);

            DataTable dtDriverData = new NaveoOneLib.Services.Drivers.DriverService().GetDriver(lm, NaveoOneLib.Models.Drivers.Driver.ResourceType.Driver, true, sConnStr);

            string strRowCommaSeparated = "";

            int cpt = 0;
            foreach (DataRow dr in dtDriverData.Rows)
            {

                if (cpt == 0)
                {
                    strRowCommaSeparated = dr["DriverID"].ToString();
                }
                else
                {
                    strRowCommaSeparated += ", " + dr["DriverID"].ToString();
                }



                cpt++;
            }

            Connection c = new Connection(sConnStr);

            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = "select distinct GFI_FLT_Driver.DriverID, GFI_FLT_Driver.sDriverName, GFI_FLT_ZoneHeader.ZoneID, GFI_FLT_ZoneHeader.Description FROM GFI_FLT_Driver, GFI_FLT_ZoneHeader, GFI_FLT_ZoneDetail where GFI_FLT_Driver.ZoneID = GFI_FLT_ZoneHeader.ZoneID and GFI_FLT_ZoneHeader.ZoneID = GFI_FLT_ZoneDetail.ZoneID and GFI_FLT_Driver.DriverID IN (" + strRowCommaSeparated + ") ";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            
            drSql["sTableName"] = "DriverHome";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            baseModel.data = ds;
            return baseModel;
        }
    }
}
