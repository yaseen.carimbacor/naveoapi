﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NaveoOneLib.Models;
using System.Data;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Services;

namespace NaveoService.Services
{
    public class ParamService
    {
        public List<Models.GlobalParam> GetParams(String sConnStr)
        {
            DataTable newList = new NaveoOneLib.Services.GlobalParamsService().GetGlobalParams(sConnStr);

            if (newList != null)
            {
                return newList.AsEnumerable().Select(row =>
                            new Models.GlobalParam
                            {
                                Id = row.Field<int>("Iid"),
                                ParamComments = row.Field<string>("ParamComments"),
                                ParamName = row.Field<string>("ParamName"),
                                PValue = row.Field<string>("PValue")
                            }).ToList();

            }

            return null;
        }

        public bool SaveParam(Models.GlobalParam param, bool insert, String sConnStr)
        {
            GlobalParam newParam = new GlobalParam()
            {
                Iid = param.Id,
                ParamComments = param.ParamComments ?? string.Empty,
                ParamName = param.ParamName ?? string.Empty,
                PValue = param.PValue ?? string.Empty
            };
            return new NaveoOneLib.Services.GlobalParamsService().SaveGlobalParams(newParam, insert, sConnStr);
        }

        public bool DeleteParam(string id, String sConnStr)
        {
            var obj = new NaveoOneLib.Services.GlobalParamsService().GetGlobalParamsById(id, sConnStr);
            return new NaveoOneLib.Services.GlobalParamsService().DeleteGlobalParams(obj, sConnStr);
        }

        public Models.GlobalParam GetGlobalParamsById(string id, String sConnStr)
        {
            var obj = new NaveoOneLib.Services.GlobalParamsService().GetGlobalParamsById(id, sConnStr);
            if (obj != null)
            {
                return new Models.GlobalParam
                {
                    Id = obj.Iid,
                    ParamComments = obj.ParamComments,
                    ParamName = obj.ParamName,
                    PValue = obj.PValue
                };

            }
            return null;
        }

        public Models.GlobalParam GetGlobalParamsByName(string name, String sConnStr)
        {
            var obj = new NaveoOneLib.Services.GlobalParamsService().GetGlobalParamsByName(name, sConnStr);
            if (obj != null)
            {
                return new Models.GlobalParam
                {
                    Id = obj.Iid,
                    ParamComments = obj.ParamComments,
                    ParamName = obj.ParamName,
                    PValue = obj.PValue
                };

            }

            return null;
        }
    }

}