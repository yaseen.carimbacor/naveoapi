﻿using NaveoOneLib.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace NaveoService.Services
{
    /// <summary>
    /// Token-Based Security for API Access
    /// </summary>
    public class SecurityService
    {
        #region Obsolete. Done in api memory
        ///// <summary>
        ///// Generate One time token
        ///// </summary>
        //public static SecurityToken GenerateAuthToken(SecurityToken securityToken, string sConnStr)
        //{
        //    return new SecurityToken().InsertSecurityToken(securityToken, sConnStr);
        //}

        ///// <summary>
        ///// Verify one time token
        ///// </summary>
        ///// <param name="securityToken"></param>
        ///// <param name="sConnStr"></param>
        ///// <returns></returns>
        //public static SecurityToken VerifyToken(Guid? authToken, string sConnStr, String sUserHostAddress)
        //{
        //    SecurityToken securityToken = new SecurityToken { IsAuthorized = false };
        //    //if authToken is not in header, api will check if client ip is whitelisted
        //    if (SecurityService.isIPWhiteListed(sUserHostAddress))
        //    {
        //        securityToken.IsAuthorized = true;
        //        return securityToken;
        //    }
        //    try
        //    {
        //        // TODO: [Reza] Check existence and expiry of token in dll
        //        if (true)
        //        {
        //            securityToken = new SecurityToken().UpdateSecurityToken((Guid)authToken, sConnStr);
        //            if (securityToken == null)
        //            {
        //                securityToken = new SecurityToken
        //                {
        //                    IsAuthorized = false,
        //                    DebugMessage = "Invalid Token",
        //                    OneTimeToken = authToken.HasValue ? authToken.Value : new Guid("00000000-0000-0000-0000-000000000000")
        //                };
        //            }
        //        }
        //        securityToken.IsAuthorized = true;
        //    }
        //    catch (Exception e)
        //    {
        //        // One time token could not be updated in database. Maybe there is a connection problem.
        //        securityToken = new SecurityToken
        //        {
        //            IsAuthorized = false,
        //            DebugMessage = "Invalid Token",
        //            OneTimeToken = authToken.HasValue ? authToken.Value : new Guid("00000000-0000-0000-0000-000000000000")
        //        };
        //    }
        //    return securityToken;
        //}
        #endregion

        /// <summary>
        /// Retrieve whitelisted ips
        /// </summary>
        /// <returns></returns>
        //TODO : Retrieve ips from database
        //public static List<string> WhiteListedIps()
        //{
        //    return new List<string>() { "::1", "localhost", "rezahp", "192.168.1.112", " 173.45.225.150" };
        //}
        public static Boolean isIPWhiteListed(String ip)
        {
            String CurrentPath = HostingEnvironment.ApplicationPhysicalPath;
            if (DoesLineExist(CurrentPath + "\\Registers\\whiteListIPs.dat", ip))
                return true;
            return false;
        }
        public static Boolean DoesLineExist(String sPath, String needle)
        {
            if (File.Exists(sPath))
            {
                String[] Lines = System.IO.File.ReadAllLines(sPath);
                foreach (String line in Lines)
                    if (line.Trim().ToLower() == needle.Trim().ToLower())
                        return true;
            }
            return false;
        }
    }
}