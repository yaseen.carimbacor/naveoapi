﻿using NaveoOneLib.Common;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.Reportings;
using NaveoOneLib.Services.GMatrix;
using NaveoService.Helpers;
using NaveoService.Models;
using NaveoService.Models.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.Script.Serialization;

namespace NaveoService.Services
{
    public class AutoReportingConfigService
    {

        public dtData GetAutoReportingConfig(Guid sUserToken, string sConnStr)
        {
            int UID = CommonService.GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);
            DataTable dtGetAutoReportingConfig = new NaveoOneLib.Services.Reportings.AutoReportingConfigService().GetAutoReportingConfig(lm, sConnStr);

            if (dtGetAutoReportingConfig.Columns.Contains("arid"))
            {
                dtGetAutoReportingConfig.Columns["arid"].ColumnName = "ARID";

            }

            if (dtGetAutoReportingConfig.Columns.Contains("targetid"))
            {
                dtGetAutoReportingConfig.Columns["targetid"].ColumnName = "TargetID";

            }


            if (dtGetAutoReportingConfig.Columns.Contains("targettype"))
            {
                dtGetAutoReportingConfig.Columns["targettype"].ColumnName = "TargetType";

            }

            if (dtGetAutoReportingConfig.Columns.Contains("reportid"))
            {
                dtGetAutoReportingConfig.Columns["reportid"].ColumnName = "ReportID";

            }


            if (dtGetAutoReportingConfig.Columns.Contains("reportperiod"))
            {
                dtGetAutoReportingConfig.Columns["reportperiod"].ColumnName = "ReportPeriod";

            }

            if (dtGetAutoReportingConfig.Columns.Contains("triggertime"))
            {
                dtGetAutoReportingConfig.Columns["triggertime"].ColumnName = "TriggerTime";

            }
            if (dtGetAutoReportingConfig.Columns.Contains("triggerinterval"))
            {
                dtGetAutoReportingConfig.Columns["triggerinterval"].ColumnName = "TriggerInterval";

            }
            if (dtGetAutoReportingConfig.Columns.Contains("createdby"))
            {
                dtGetAutoReportingConfig.Columns["createdby"].ColumnName = "CreatedBy";

            }
            if (dtGetAutoReportingConfig.Columns.Contains("reportformat"))
            {
                dtGetAutoReportingConfig.Columns["reportformat"].ColumnName = "ReportFormat";

            }
            if (dtGetAutoReportingConfig.Columns.Contains("reporttype"))
            {
                dtGetAutoReportingConfig.Columns["reporttype"].ColumnName = "ReportType";

            }
            if (dtGetAutoReportingConfig.Columns.Contains("savetodisk"))
            {
                dtGetAutoReportingConfig.Columns["savetodisk"].ColumnName = "SaveToDisk";

            }
            if (dtGetAutoReportingConfig.Columns.Contains("reportpath"))
            {
                dtGetAutoReportingConfig.Columns["reportpath"].ColumnName = "ReportPath";

            }

            if (dtGetAutoReportingConfig.Columns.Contains("gridxml"))
            {
                dtGetAutoReportingConfig.Columns["gridxml"].ColumnName = "GridXML";

            }





            int iTotalRec = dtGetAutoReportingConfig.Rows.Count;
            dtData dtR = new dtData();
            dtR.Data = dtGetAutoReportingConfig;
            dtR.Rowcnt = iTotalRec;

            return dtR;
        }

        public BaseModel GetAutoReportingConfigByID(int ARID, string sConnStr)
        {

            AutoReportingConfig dtGetAutoReportingConfig = new NaveoOneLib.Services.Reportings.AutoReportingConfigService().GetAutoReportingById(ARID.ToString(), sConnStr);



            AutoReportingConfigDTO autoReportingConfigDTO = new AutoReportingConfigDTO();

            AutomaticReportingConfig autoReportingConfig = new AutomaticReportingConfig();

            autoReportingConfig.ARID = dtGetAutoReportingConfig.ARID;
            autoReportingConfig.reportId = dtGetAutoReportingConfig.ReportID;
            autoReportingConfig.reportPeriod = dtGetAutoReportingConfig.ReportPeriod;
            autoReportingConfig.triggerTime = dtGetAutoReportingConfig.TriggerTime;
            autoReportingConfig.triggerInterval = dtGetAutoReportingConfig.TriggerInterval;
            autoReportingConfig.reportFormat = dtGetAutoReportingConfig.ReportFormat;
            autoReportingConfig.reportType = dtGetAutoReportingConfig.ReportType;
            autoReportingConfig.saveToDisk = dtGetAutoReportingConfig.SaveToDisk;
            autoReportingConfig.oprType = DataRowState.Unchanged;



            ftpdata ftpdata = new ftpdata();

            try
            {
                if (dtGetAutoReportingConfig.GridXML != null)
                {
                    ftpdata = Newtonsoft.Json.JsonConvert.DeserializeObject<ftpdata>(dtGetAutoReportingConfig.GridXML);
                    autoReportingConfig.ftpdata = ftpdata;

                }

            }catch
            {

            }


     

            if (dtGetAutoReportingConfig.lDetails != null)
            {

                Recepients r = new Recepients();

                foreach (var x in dtGetAutoReportingConfig.lDetails)
                {
                   

                    if (x.UIDType == "Users")
                    {

                        r.ARDID = x.ARDID;
                        r.ARID = autoReportingConfig.ARID;

                        if (x.UIDCategory == "GMID")
                            r.type = "PARENT";
                        else
                            if (x.UIDCategory == "ID")
                            r.type = "CHILD";


                        r.lIds.Add(x.UID);
                        r.oprType = DataRowState.Unchanged;

                        autoReportingConfig.recepients = r;
                    }



                    if (x.UIDType == "Assets")
                    {


                        foreach (var xx in dtGetAutoReportingConfig.lDetails)
                        {

                            Tree t = new Tree();

                            TreeSelection ts = new TreeSelection();

                            Group g = new Group();

                            g.group = "Assets";


                            if (xx.UIDCategory == "GMID")
                            {
                                ts.type = "PARENT";
                            }
                            else if (xx.UIDCategory == "ID")
                            {
                                ts.type = "CHILD";
                            }



                            ts.lIds.Add(xx.UID);


                            g.lGroup.Add(ts);
                            t.lTreeSelections.Add(g);
                            autoReportingConfig.tree.Add(t);


                        }
                    }



                }

                autoReportingConfigDTO.automaticReporting.Add(autoReportingConfig);
            }




            BaseModel autoDTO = new BaseModel();
            autoDTO.data = autoReportingConfigDTO;


            return autoDTO;

        }


        public BaseDTO FTPHealthCheck(PutDTO healthCheck)
        {

            BaseDTO baseDTO = new BaseDTO();
            
            try
            {
                //Convert to string
                //string input = ;

                //input.Replace("{{", "{").Replace("}}", "}");


                FtpClient.FTPServerCredentials cred = (FtpClient.FTPServerCredentials)Newtonsoft.Json.JsonConvert.DeserializeObject(Convert.ToString(healthCheck.data), typeof(FtpClient.FTPServerCredentials));

                //Remove encryption
                string sPassword = cred.password;

                if (sPassword != string.Empty)
                    cred.password = NaveoOneLib.Services.BaseService.RSADecrypt(sPassword);

                //cred.password = NaveoOneLib.Services.BaseService.DecryptMe(cred.password);
                FtpClient ftpClient = new FtpClient(cred.FTPAddress, cred.user, cred.password, cred.enableSSL);


                if (ftpClient.IsConnected(cred.FileDir))
                {
                    baseDTO.data = true;
                }
            }
            catch (Exception ex)
            {
            }

            return baseDTO;
        }

        public BaseDTO FTPAPICheck(SecurityTokenExtended securityToken, AutoReportingConfigDTO autoReportingConfigDTO, Boolean bInsert)
        {

            int userId = CommonService.GetUserID(securityToken.securityToken.UserToken, securityToken.securityToken.sConnStr);

            #region GFI_FLT_AutoReportingConfig
            NaveoOneLib.Models.Reportings.AutoReportingConfig autoReportingConfig = new NaveoOneLib.Models.Reportings.AutoReportingConfig();
            ServiceHelper.autoReportingConfigDefaultValues(autoReportingConfig);
            foreach (AutomaticReportingConfig aRc in autoReportingConfigDTO.automaticReporting)
            {
                autoReportingConfig.TargetID = aRc.targetId;
                autoReportingConfig.ARID = aRc.ARID;
                autoReportingConfig.ReportID = aRc.reportId;
                autoReportingConfig.ReportPeriod = aRc.reportPeriod;
                autoReportingConfig.TriggerTime = aRc.triggerTime;
                autoReportingConfig.TriggerInterval = aRc.triggerInterval;
                autoReportingConfig.ReportFormat = aRc.reportFormat;
                autoReportingConfig.ReportType = aRc.reportType;
                autoReportingConfig.SaveToDisk = aRc.saveToDisk;
                autoReportingConfig.ReportPath = aRc.reportPath;
                autoReportingConfig.CreatedBy = userId.ToString();

                if (aRc.other != null)
                {
                    foreach (var o in aRc.other)
                    {
                        if (o.value == null || o.value == string.Empty)
                        { o.value = "0.0"; }
                        string mergeValues = o.id + " : " + o.value;
                        autoReportingConfig.GridXML += mergeValues + ",";
                    }
                    autoReportingConfig.GridXML = autoReportingConfig.GridXML.TrimEnd(',');
                }
                autoReportingConfig.lDetails = new List<AutoReportingConfigDetail>();

                foreach (var t in aRc.tree)
                {
                    foreach (var s in t.lTreeSelections)
                    {
                        foreach (var ts in s.lGroup)
                        {
                            foreach (var id in ts.lIds)
                            {
                                AutoReportingConfigDetail dllaRcd = new AutoReportingConfigDetail();



                                dllaRcd.UIDType = s.group;
                                if (ts.type.ToUpper() == "PARENT")
                                {
                                    dllaRcd.UIDCategory = "GMID";
                                }
                                else if (ts.type.ToUpper() == "CHILD")
                                {
                                    dllaRcd.UIDCategory = "ID";
                                }
                                dllaRcd.UID = id;


                                if (bInsert)
                                    dllaRcd.oprType = DataRowState.Added;
                                else
                                    dllaRcd.oprType = ts.oprType;


                                autoReportingConfig.lDetails.Add(dllaRcd);
                            }
                        }
                    }
                }

                if (aRc.recepients != null)
                {

                    foreach (var sIds in aRc.recepients.lIds)
                    {
                        AutoReportingConfigDetail dllaRcd1 = new AutoReportingConfigDetail();
                        dllaRcd1.UID = sIds;

                        if (bInsert)
                            dllaRcd1.oprType = DataRowState.Added;
                        else
                            dllaRcd1.oprType = aRc.recepients.oprType;



                        dllaRcd1.UIDType = "Users";
                        if (aRc.recepients.type.ToUpper() == "PARENT")
                        {
                            dllaRcd1.UIDCategory = "GMID";
                        }
                        else if (aRc.recepients.type.ToUpper() == "CHILD")
                        {
                            dllaRcd1.UIDCategory = "ID";
                        }

                        dllaRcd1.ARDID = aRc.recepients.ARDID;
                        dllaRcd1.ARID = aRc.recepients.ARID;
                        autoReportingConfig.lDetails.Add(dllaRcd1);
                    }

                }

            }
            #endregion

            #region GFI_FLT_AutoReportingConfigDetails
            DataTable dtARConfigDetails = new myConverter().ListToDataTable<AutoReportingConfigDetail>(autoReportingConfig.lDetails);
            dtARConfigDetails.TableName = "GFI_FLT_AutoReportingConfigDetails";
            #endregion

            BaseModel autoReportingConfigSaveUpdate = new NaveoOneLib.Services.Reportings.AutoReportingConfigService().SaveAutoReportingConfig(userId, autoReportingConfig, dtARConfigDetails, bInsert, securityToken.securityToken.sConnStr);
            return autoReportingConfigSaveUpdate.ToBaseDTO(securityToken.securityToken);
        }

        public BaseDTO SaveAutoReporting(SecurityTokenExtended securityToken, AutoReportingConfigDTO autoReportingConfigDTO,Boolean bInsert)
        {
            int userId = CommonService.GetUserID(securityToken.securityToken.UserToken, securityToken.securityToken.sConnStr);

            #region GFI_FLT_AutoReportingConfig
            NaveoOneLib.Models.Reportings.AutoReportingConfig autoReportingConfig = new NaveoOneLib.Models.Reportings.AutoReportingConfig();
            ServiceHelper.autoReportingConfigDefaultValues(autoReportingConfig);
            foreach (AutomaticReportingConfig aRc in autoReportingConfigDTO.automaticReporting)
            {
                autoReportingConfig.TargetID = aRc.targetId;
                autoReportingConfig.ARID = aRc.ARID;
                autoReportingConfig.ReportID = aRc.reportId;
                autoReportingConfig.ReportPeriod = aRc.reportPeriod;
                autoReportingConfig.TriggerTime = aRc.triggerTime;
                autoReportingConfig.TriggerInterval = aRc.triggerInterval;
                autoReportingConfig.ReportFormat = aRc.reportFormat;
                autoReportingConfig.ReportType = aRc.reportType;
                autoReportingConfig.SaveToDisk = aRc.saveToDisk;
                autoReportingConfig.ReportPath = aRc.reportPath;
                autoReportingConfig.CreatedBy = userId.ToString();

                string ftpdata = string.Empty;


                if (aRc.ftpdata != null)
                    ftpdata = new JavaScriptSerializer().Serialize(aRc.ftpdata);

                if (!string.IsNullOrEmpty(ftpdata))
                {
                    autoReportingConfig.GridXML = ftpdata;
                }

                if (aRc.other != null)
                {
                    foreach (var o in aRc.other)
                    {
                        if (o.value == null || o.value == string.Empty)
                        { o.value = "0.0"; }
                        string mergeValues = o.id + " : " + o.value;
                        autoReportingConfig.GridXML += mergeValues + ",";
                    }
                    autoReportingConfig.GridXML = autoReportingConfig.GridXML.TrimEnd(',');
                }
                autoReportingConfig.lDetails = new List<AutoReportingConfigDetail>();

                foreach (var t in aRc.tree)
                {
                    foreach (var s in t.lTreeSelections)
                    {
                        foreach (var ts in s.lGroup)
                        {
                            foreach (var id in ts.lIds)
                            {
                                AutoReportingConfigDetail dllaRcd = new AutoReportingConfigDetail();



                                dllaRcd.UIDType = s.group;
                                if (ts.type.ToUpper() == "PARENT")
                                {
                                    dllaRcd.UIDCategory = "GMID";
                                }
                                else if (ts.type.ToUpper() == "CHILD")
                                {
                                    dllaRcd.UIDCategory = "ID";
                                }
                                dllaRcd.UID = id;


                                if (bInsert)
                                    dllaRcd.oprType = DataRowState.Added;
                                else
                                    dllaRcd.oprType = ts.oprType;


                                autoReportingConfig.lDetails.Add(dllaRcd);
                            }
                        }
                    }
                }

                if (aRc.recepients != null)
                {




                    foreach (var sIds in aRc.recepients.lIds)
                    {
                        AutoReportingConfigDetail dllaRcd1 = new AutoReportingConfigDetail();
                        dllaRcd1.UID = sIds;

                        if (bInsert)
                            dllaRcd1.oprType = DataRowState.Added;



                        if (!bInsert)
                        {
                            DataTable xx = new NaveoOneLib.Services.Reportings.AutoReportingConfigService().GetAutoRprtDetails(aRc.ARID.ToString(), securityToken.securityToken.sConnStr);

                            var exist = xx.AsEnumerable().Where(op => op.Field<int>("UID") == sIds).Select(op => op.Field<int>("UID")).FirstOrDefault();

                            if (exist != 0)
                            {
                                dllaRcd1.UID = sIds;
                                dllaRcd1.oprType = DataRowState.Unchanged;
                            }

                            var notexist = xx.AsEnumerable().Where(op => op.Field<int>("UID") == sIds).Select(op => op.Field<int>("UID")).FirstOrDefault();

                            if (notexist == 0)
                            {
                                dllaRcd1.UID = sIds;
                                dllaRcd1.oprType = DataRowState.Added;
                            }


                        }


                        dllaRcd1.UIDType = "Users";
                        if (aRc.recepients.type.ToUpper() == "PARENT")
                        {
                            dllaRcd1.UIDCategory = "GMID";
                        }
                        else if (aRc.recepients.type.ToUpper() == "CHILD")
                        {
                            dllaRcd1.UIDCategory = "ID";
                        }

                        dllaRcd1.ARDID = aRc.recepients.ARDID;
                        dllaRcd1.ARID = aRc.recepients.ARID;
                        autoReportingConfig.lDetails.Add(dllaRcd1);
                    }


                }

                if (!bInsert)
                {
                    if (aRc.recepients != null)
                    {







                        DataTable xx = new NaveoOneLib.Services.Reportings.AutoReportingConfigService().GetAutoRprtDetails(aRc.ARID.ToString(), securityToken.securityToken.sConnStr);

                        foreach (DataRow deleted in xx.Rows)
                        {

                            int uid = Convert.ToInt32(deleted["UID"]);
                            int arid = Convert.ToInt32(deleted["ARID"]);
                            int ardid = Convert.ToInt32(deleted["ARDID"]);

                            if (!aRc.recepients.lIds.Contains(uid))
                            {
                                AutoReportingConfigDetail dllaRcd1 = new AutoReportingConfigDetail();
                                dllaRcd1.UID = uid;
                                dllaRcd1.oprType = DataRowState.Deleted;

                                dllaRcd1.UIDType = "Users";
                                if (aRc.recepients.type.ToUpper() == "PARENT")
                                {
                                    dllaRcd1.UIDCategory = "GMID";
                                }
                                else if (aRc.recepients.type.ToUpper() == "CHILD")
                                {
                                    dllaRcd1.UIDCategory = "ID";
                                }


                                dllaRcd1.ARDID = ardid;
                                dllaRcd1.ARID = arid;
                                autoReportingConfig.lDetails.Add(dllaRcd1);
                            }





                        }




                    }
                }
            }           
            
            
            #endregion

            #region GFI_FLT_AutoReportingConfigDetails
            DataTable dtARConfigDetails = new myConverter().ListToDataTable<AutoReportingConfigDetail>(autoReportingConfig.lDetails);
            dtARConfigDetails.TableName = "GFI_FLT_AutoReportingConfigDetails";
            #endregion

            BaseModel autoReportingConfigSaveUpdate = new NaveoOneLib.Services.Reportings.AutoReportingConfigService().SaveAutoReportingConfig(userId, autoReportingConfig, dtARConfigDetails, bInsert, securityToken.securityToken.sConnStr);
            return autoReportingConfigSaveUpdate.ToBaseDTO(securityToken.securityToken);
        }

        public BaseModel DeleteAutoReportingConfig(SecurityTokenExtended securityToken, int arId)
        {
            BaseModel bm = new BaseModel();
            int UID = CommonService.GetUserID(securityToken.securityToken.UserToken, securityToken.securityToken.sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, securityToken.securityToken.sConnStr);
            DataTable dtGetAutoReportingConfig = new NaveoOneLib.Services.Reportings.AutoReportingConfigService().GetAutoReportingConfig(lm, securityToken.securityToken.sConnStr);
            DataRow[] dr = dtGetAutoReportingConfig.Select("ARID = " + arId);
            if (dr.Length == 0)
            {
                bm.data = false;
                bm.errorMessage = "User does not have access to This Automatic Report";
                return bm;
            }

            bm = new NaveoOneLib.Services.Reportings.AutoReportingConfigService().DeleteARC(arId, UID, securityToken.securityToken.sConnStr);
            return bm;
        }


        public DataTable AutomaticReportingMaintenace(List<int> AssetIds, string Start, string End, List<string> Description)
        {


            DateTime StartDate = DateTime.MinValue;

            DateTime EndDate = DateTime.MaxValue;


            if (Start != null)
            {
                StartDate = DateTime.Parse(Start);
            }

            if (End != null)
            {
                EndDate = DateTime.Parse(End);
            }


            DataTable dtFinal = new DataTable();
            dtFinal.Columns.Add("MaintId");
            dtFinal.Columns.Add("AssetId");
            dtFinal.Columns.Add("AssetName");
            dtFinal.Columns.Add("Maintenance_Type");
            dtFinal.Columns.Add("Description");
            dtFinal.Columns.Add("StartDate");
            dtFinal.Columns.Add("EndDate");
            dtFinal.Columns.Add("AssetStatus");
            dtFinal.Columns.Add("FuelVATAmount");
            dtFinal.Columns.Add("FuelTotalCost");
            dtFinal.Columns.Add("FuelCostExVAT");
            dtFinal.Columns.Add("VATAmount");
            dtFinal.Columns.Add("TotalCost");
            dtFinal.Columns.Add("CostExVAT");
           

            DataTable dtprefinal = new DataTable();

            DataTable dt1 = new NaveoOneLib.Services.Reportings.AutoReportingConfigService().GetPart1();

            DataTable dt2 = new NaveoOneLib.Services.Reportings.AutoReportingConfigService().GetPart2();

            


            foreach (int assetId in AssetIds)
            {
                foreach (string type in Description)
                {
                    if (type == "FuelUsage")
                    {
                        var x = dt2.AsEnumerable().Where(e => e.Field<DateTime?>("StartDate") >= StartDate & e.Field<DateTime?>("EndDate") <= EndDate & e.Field<int>("AssetId") == assetId & e.Field<string>("Description").ToLower() == type.ToLower());

                        if (x.Any())
                            dtprefinal.Merge(x.CopyToDataTable());
                    }
                    else if (type == "ODOMETER/ENGINE HOURS ENTRY")
                    {
                        var x = dt1.AsEnumerable().Where(e => e.Field<DateTime?>("StartDate") >= StartDate & e.Field<DateTime?>("EndDate") <= EndDate & e.Field<int>("AssetId") == assetId & e.Field<string>("Description").ToLower() == type.ToLower());

                        if (x.Any())
                            dtprefinal.Merge(x.CopyToDataTable());
                    }
                    else
                    {
                        var x = dt1.AsEnumerable().Where(e => e.Field<DateTime?>("StartDate") >= StartDate & e.Field<DateTime?>("EndDate") <= EndDate & e.Field<int>("AssetId") == assetId & e.Field<string>("Description").ToLower() == type.ToLower());
                      
                        if(x.Any())
                          dtprefinal.Merge(x.CopyToDataTable());
                    }

                }
            }



            foreach (DataRow item in dtprefinal.Rows)
            {
                DataRow dr = dtFinal.NewRow();
                dr["MaintId"] = item["MaintId"];
                dr["AssetId"] = item["AssetId"];
                dr["AssetName"] = item["AssetName"];
                dr["Maintenance_Type"] = item["Maintenance_Type"];
                dr["Description"] = item["Description"];
                dr["AssetName"] = item["AssetName"];
                dr["StartDate"] = item["StartDate"];
                dr["EndDate"] = item["EndDate"];
                dr["AssetStatus"] = item["AssetStatus"];


                double a = 0.0;

                if (dtprefinal.Columns.Contains("FuelVATAmount"))
                    a = String.IsNullOrEmpty(item["FuelVATAmount"].ToString()) ? 0.0 : Convert.ToDouble(item["FuelVATAmount"]);


                if (a !=0)
                {
                    //maintenanceFinal.FuelVATAmount = double.Parse(item.FuelVATAmount.ToString("#.00", CultureInfo.InvariantCulture));
               
                    dr["FuelVATAmount"] = double.Parse(a.ToString("#.00", CultureInfo.InvariantCulture));
                }


                double b = 0.0;

                if (dtprefinal.Columns.Contains("FuelTotalCost"))
                    b = String.IsNullOrEmpty(item["FuelTotalCost"].ToString()) ? 0.0 : Convert.ToDouble(item["FuelTotalCost"]);



                if (b != 0)
                {
                    //maintenanceFinal.FuelTotalCost = double.Parse(item.FuelTotalCost.ToString("#.00", CultureInfo.InvariantCulture));
                    dr["FuelTotalCost"] = double.Parse(b.ToString("#.00", CultureInfo.InvariantCulture));

                }


                double c = 0.0;

                if (dtprefinal.Columns.Contains("FuelCostExVAT"))
                    c =String.IsNullOrEmpty(item["FuelCostExVAT"].ToString()) ? 0.0 : Convert.ToDouble(item["FuelCostExVAT"]);

                if (c != 0)
                {
                    //maintenanceFinal.FuelCostExVAT = double.Parse(item.FuelCostExVAT.ToString("#.00", CultureInfo.InvariantCulture));
                    dr["FuelCostExVAT"] = double.Parse(c.ToString("#.00", CultureInfo.InvariantCulture));
                }

                double d = 0.0;
                if (dtprefinal.Columns.Contains("VATAmount"))
                    d = String.IsNullOrEmpty(item["VATAmount"].ToString()) ? 0.0 : Convert.ToDouble(item["VATAmount"]);


                if (d != 0)
                {
                    //maintenanceFinal.VATAmount = decimal.Parse(item.VATAmount.ToString("#.00", CultureInfo.InvariantCulture));
                    dr["VATAmount"] = double.Parse(d.ToString("#.00", CultureInfo.InvariantCulture));
                }

                
                double e = 0.0;
                if (dtprefinal.Columns.Contains("TotalCost"))
                    e =  String.IsNullOrEmpty(item["TotalCost"].ToString()) ? 0.0 : Convert.ToDouble(item["TotalCost"]);

                if (e != 0)
                {
                    // maintenanceFinal.TotalCost = decimal.Parse(item.TotalCost.ToString("#.00", CultureInfo.InvariantCulture));
                    dr["TotalCost"] = double.Parse(e.ToString("#.00", CultureInfo.InvariantCulture));
                }



                double f = 0.0;
                if (dtprefinal.Columns.Contains("CostExVAT"))
                    f = String.IsNullOrEmpty(item["CostExVAT"].ToString()) ? 0.0 : Convert.ToDouble(item["CostExVAT"]); 

                if (f != 0)
                {
                //maintenanceFinal.CostExVAT = decimal.Parse(item.CostExVAT.ToString("#.00", CultureInfo.InvariantCulture
                dr["CostExVAT"] = double.Parse(f.ToString("#.00", CultureInfo.InvariantCulture));
                }
         


                dtFinal.Rows.Add(dr);
            }



            return dtFinal;
        }


    }
}
