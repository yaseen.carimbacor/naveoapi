﻿using NaveoOneLib.Models;
using NaveoOneLib.Models.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;

namespace NaveoService.ViewModel
{
    public class UserViewModel : User
    {
        //Reza 17 Jan 18 - Removing warnings
        //[Required]
        //[Display(Name = "Email")]
        //[EmailAddress]
        //public string Email { get; set; }

        //[Required]
        //[DataType(DataType.Password)]
        //[Display(Name = "Password")]
        //public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }

        public String MatrixString { get; set; }
        public String SignForget { get; set; }

        [Display(Name = "Role")]
        public int RoleId { get; set; }
        public List<String> controllerList { get; set; }
        public String users { get; set; }
        public User user { get; set; }
        public string companyToken { get; set; }

    }
    public class MatrixSelection
    {
        public string id { get; set; }
        public string text { get; set; }
    }
}