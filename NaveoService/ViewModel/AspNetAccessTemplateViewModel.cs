﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NaveoService.Models;

namespace NaveoService.ViewModel
{
    public class AspNetAccessTemplateViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string sData { get; set; }
    }
}