﻿using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Reportings;
using NaveoOneLib.Models.Users;
using NaveoService.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace NaveoService.Helpers
{
    public static class ServiceHelper
    {
        /// <summary>
        /// Group matrix for user
        /// </summary>
        /// <param name="matrixJson"></param>
        /// <param name="UID"></param>
        /// <returns></returns>
        public static List<Matrix> UserGroupMatrix(string matrixJson, int UID)
        {
            List<Matrix> lMatrix = new List<Matrix>();
            var lMatrixView = new JavaScriptSerializer().Deserialize<List<MatrixSelection>>(matrixJson);
            foreach (var x in lMatrixView)
            {
                if (x.id.Contains("P"))
                {
                    var tmp = new Matrix
                    {
                        GMID = Convert.ToInt32(x.id.Substring(x.id.LastIndexOf('P') + 1)),
                        GMDesc = x.text,
                        oprType = DataRowState.Added,
                        iID = UID
                    };
                    lMatrix.Add(tmp);
                }
            }

            return lMatrix;
        }

        /// <summary>
        /// Default values
        /// </summary>
        /// <param name="u"></param>
        public static void DefaultValues(User u)
        {
            // Default Values
            u.LoginAttempt = 0;
            u.LoginCount = 0;
            //u.DateJoined = DateTime.Now;
            u.MaxDayMail = 10; // TODO: To change. Put as default value for now
        }

        public static void AssetDefaultValues(Asset a)
        {
            a.assetDeviceMap.StartDate= DateTime.Now.AddYears(-1);
            a.assetDeviceMap.EndDate = DateTime.Now.AddYears(100);
        }

        public static void autoReportingConfigDefaultValues(AutoReportingConfig autoReportingConfig)
        {
            autoReportingConfig.TargetType = "V2";
            autoReportingConfig.SaveToDisk = 0;
        }
    }
}