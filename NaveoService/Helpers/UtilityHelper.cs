﻿using NaveoService.Constants;
using NaveoService.Models.ImportTemplates;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;

namespace NaveoService.Helpers
{
    public class UtilityHelper
    {
        public static string exportFilesParentPath { get; set; }

        #region Constants
        public const char SEMICOLON = ';';
        public const char COMMA = ',';
        #endregion

        /// <summary>
        ///  A static helper class for Datatable operations
        /// </summary>
        public static class Datatable
        {
            /// <summary>
            /// Convert Template Row to Datatable
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="templateRows"></param>
            /// <returns></returns>
            public static DataTable ToDataTable<T>(List<T> templateRows)
            {
                if (templateRows.Any())
                {
                    DataTable dataTable = new DataTable(typeof(T).Name);

                    //Get all the properties
                    PropertyInfo[] properties = typeof(T).GetProperties();//(BindingFlags.NonPublic | BindingFlags.Instance);
                    foreach (PropertyInfo property in properties)
                    {
                        //Defining type of data column gives proper data table 
                        var type = (property.PropertyType.IsGenericType && property.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(property.PropertyType) : property.PropertyType);
                        //Setting column names as Property names
                        dataTable.Columns.Add(property.Name, type);
                    }

                    foreach (T templateRow in templateRows)
                    {
                        var propertyValues = new object[properties.Length];
                        for (int i = 0; i < properties.Length; i++)
                        {
                            //inserting property values to datatable rows
                            propertyValues[i] = properties[i].GetValue(templateRow, null);
                        }
                        dataTable.Rows.Add(propertyValues);
                    }
                    //put a breakpoint here and check datatable
                    return dataTable;
                }
                return null;
            }
        }

        /// <summary>
        /// A static helper class for filling model template rows
        /// </summary>
        public class Document
        {
            /// <summary>
            /// Fills the user template (used to validate type for each field)
            /// </summary>
            /// <param name="file"></param>
            /// <returns></returns>
            #region Fill Template Row 

            #region User

            /// <summary>
            /// Fill User Template row from excel file
            /// </summary>
            /// <param name="csvFile"></param>
            /// <returns></returns>
            public async static Task<List<UserTemplateRow>> FillUserExcelTemplateRowAsync(HttpPostedFile excelFile)
            {
                return await Task.Run(() =>
                {
                    if (excelFile != null)
                    {
                        using (ExcelPackage excelPackage = new ExcelPackage(excelFile.InputStream))
                        {
                            List<UserTemplateRow> userTemplateRows = new List<UserTemplateRow>();
                            ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.First();

                            var maxRows = worksheet.Dimension.End.Row;
                            for (var row = Headers.DEFAULT_START_ROW; row <= maxRows; row++)
                            {
                                #region Fill User Template rows

                                #region Excel dropdowns values handling
                                string mobileAccess = worksheet.Cells[row, (int)UsersXlsxColumns.MobileAccess].Value?.ToString();
                                string externalAccess = worksheet.Cells[row, (int)UsersXlsxColumns.ExternalAccess].Value?.ToString();
                                string twoStepVerif = worksheet.Cells[row, (int)UsersXlsxColumns.TwoStepVerif].Value?.ToString();
                                string twoStepAttempts = worksheet.Cells[row, (int)UsersXlsxColumns.TwoStepAttempts].Value?.ToString();
                                #endregion

                                UserTemplateRow userTemplateRow = new UserTemplateRow
                                {
                                    Username = worksheet.Cells[row, (int)UsersXlsxColumns.Username].Value?.ToString(),
                                    Names = worksheet.Cells[row, (int)UsersXlsxColumns.Names].Value?.ToString(),
                                    LastName = worksheet.Cells[row, (int)UsersXlsxColumns.LastName].Value?.ToString(),
                                    Email = worksheet.Cells[row, (int)UsersXlsxColumns.Email].Value?.ToString(),
                                    Tel = worksheet.Cells[row, (int)UsersXlsxColumns.Tel].Value?.ToString(),
                                    MobileNo = worksheet.Cells[row, (int)UsersXlsxColumns.MobileNo].Value?.ToString(),
                                    Status = worksheet.Cells[row, (int)UsersXlsxColumns.Status].Value?.ToString(),
                                    Password = worksheet.Cells[row, (int)UsersXlsxColumns.Password].Value?.ToString(),
                                    AccessList = worksheet.Cells[row, (int)UsersXlsxColumns.AccessList].Value?.ToString(),
                                    StoreUnit = worksheet.Cells[row, (int)UsersXlsxColumns.StoreUnit].Value?.ToString(),
                                    UType_cbo = worksheet.Cells[row, (int)UsersXlsxColumns.UType_cbo].Value?.ToString(),
                                    MaxDayMail = int.Parse(worksheet.Cells[row, (int)UsersXlsxColumns.MaxDayMail].Value?.ToString()),
                                    //UserTypeID = int.Parse(worksheet.Cells[row, (int)UsersXlsxColumns.UserTypeID].Value?.ToString()),
                                    UserTypeDescription = worksheet.Cells[row, (int)UsersXlsxColumns.UserTypeDescription].Value?.ToString(),
                                    ZoneID = int.Parse(worksheet.Cells[row, (int)UsersXlsxColumns.ZoneID].Value?.ToString()),
                                    //PlanningLookUpValue = int.Parse(worksheet.Cells[row, (int)UsersXlsxColumns.PlanningLookUpValue].Value?.ToString()),
                                    MobileAccess = (string.IsNullOrEmpty(mobileAccess) || mobileAccess == "No") ? 0 : 1,
                                    ExternalAccess = (string.IsNullOrEmpty(externalAccess) || externalAccess == "No") ? 0 : 1,
                                    TwoStepVerif = (string.IsNullOrEmpty(twoStepVerif) || twoStepVerif == "No") ? 0 : 1,
                                    TwoStepAttempts = (string.IsNullOrEmpty(twoStepAttempts) || twoStepAttempts == "No") ? 0 : 1
                                };
                                #endregion

                                userTemplateRows.Add(userTemplateRow);
                            }
                            return userTemplateRows;
                        }
                    }
                    else
                        return new List<UserTemplateRow>();
                });
            }

            /// <summary>
            /// Fill User Template row from csv file
            /// </summary>
            /// <param name="csvFile"></param>
            /// <returns></returns>
            public async static Task<List<UserTemplateRow>> FillUserCsvTemplateRowAsync(HttpPostedFile csvFile)
            {
                return await Task.Run(() =>
                {
                    if (csvFile != null)
                    {
                        string[] lines = System.IO.File.ReadAllLines(csvFile.FileName);
                        string errorMessage = string.Empty;
                        List<UserTemplateRow> UserTemplateRows = new List<UserTemplateRow>();
                        foreach (var line in lines)
                        {
                            if (line.Contains(SEMICOLON))
                                line.Replace(SEMICOLON, COMMA);

                            string[] values = line.Split(COMMA);
                            UserTemplateRow userRow = new UserTemplateRow
                            {
                                #region UserTemplateRow
                                Username = values[0],
                                LastName = values[1],
                                Email = values[2],
                                Tel = values[3],
                                MobileNo = values[4],
                                Status = values[5],
                                Password = values[6],
                                AccessList = values[7],
                                StoreUnit = values[8],
                                UType_cbo = values[9],
                                MaxDayMail = int.Parse(values[10]),
                                //UserTypeID = int.Parse(worksheet.Cells[row, (int)UsersXlsxColumns.UserTypeID].Value?.ToString()),
                                UserTypeDescription = values[11],
                                ZoneID = int.Parse(values[12]),
                                //PlanningLookUpValue = int.Parse(worksheet.Cells[row, (int)UsersXlsxColumns.PlanningLookUpValue].Value?.ToString()),
                                MobileAccess = int.Parse(values[13]),
                                ExternalAccess = int.Parse(values[14]),
                                TwoStepVerif = int.Parse(values[15]),
                                TwoStepAttempts = int.Parse(values[16])
                                #endregion
                            };
                            UserTemplateRows.Add(userRow);
                        }
                        return UserTemplateRows;
                    }
                    else
                        return new List<UserTemplateRow>();
                });
            }

            #endregion
            #region Planning
            public List<PlanningTemplateRow> FillPlanningExcelTemplateRow(HttpPostedFile excelFile)
            {

                if (excelFile != null)
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(excelFile.InputStream))
                    {
                        List<PlanningTemplateRow> planningTemplateRows = new List<PlanningTemplateRow>();
                        ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.First();

                        var maxRows = worksheet.Dimension.End.Row;
                        string uniquePID = string.Empty;
                        for (var row = Headers.DEFAULT_START_ROW; row <= maxRows; row++)
                        {
                            #region Fill Planning Template rows


                            #region Excel dropdowns values handling
                            string PID = worksheet.Cells[row, (int)PlanningXlsxColumns.PID].Value?.ToString();
                            if (PID != null)
                                uniquePID = PID;

                            if (PID == null)
                                PID = uniquePID;

                            string Department = worksheet.Cells[row, (int)PlanningXlsxColumns.Department].Value?.ToString();
                            if (Department == null)
                                Department = string.Empty;

                            string Remarks1 = worksheet.Cells[row, (int)PlanningXlsxColumns.Remarks1].Value?.ToString();
                            if (Remarks1 == null)
                                Remarks1 = string.Empty;

                            string IsUrgent = worksheet.Cells[row, (int)PlanningXlsxColumns.IsUrgent].Value?.ToString();
                            if (IsUrgent == null)
                                IsUrgent = string.Empty;

                            string buffer = worksheet.Cells[row, (int)PlanningXlsxColumns.Buffer].Value?.ToString();
                            if (buffer == null)
                                buffer = string.Empty;

                            string UOM = worksheet.Cells[row, (int)PlanningXlsxColumns.UOM].Value?.ToString();
                            if (UOM == null)
                                UOM = string.Empty;

                            string capacity = worksheet.Cells[row, (int)PlanningXlsxColumns.Capacity].Value?.ToString();
                            if (capacity == null)
                                capacity = string.Empty;

                            string remarks2 = worksheet.Cells[row, (int)PlanningXlsxColumns.Remarks2].Value?.ToString();
                            if (remarks2 == null)
                                remarks2 = string.Empty;

                            string resource = worksheet.Cells[row, (int)PlanningXlsxColumns.Resource].Value?.ToString();
                            if (resource == null)
                                resource = string.Empty;

                            string asset = worksheet.Cells[row, (int)PlanningXlsxColumns.Vehicel].Value?.ToString();
                            if (asset == null)
                                asset = string.Empty;

                            #endregion

                            PlanningTemplateRow planningTemplateRow = new PlanningTemplateRow();

                            planningTemplateRow.PID = (string.IsNullOrEmpty(PID) || PID == string.Empty) ? 0 : Convert.ToInt32(PID.ToString());
                            planningTemplateRow.Department = (string.IsNullOrEmpty(Department) || Department == string.Empty) ? string.Empty : Department.ToString();
                            planningTemplateRow.Remarks = (string.IsNullOrEmpty(Remarks1) || Remarks1 == string.Empty) ? string.Empty : Remarks1.ToString();
                            planningTemplateRow.Urgent = (IsUrgent == "Yes") ? 1 : 0;
                            planningTemplateRow.StartDate = Convert.ToDateTime(worksheet.Cells[row, (int)PlanningXlsxColumns.StartDate].Value);
                            planningTemplateRow.StartTime = (DateTime)worksheet.Cells[row, (int)PlanningXlsxColumns.StartTime].Value;
                            planningTemplateRow.ViaPoints = worksheet.Cells[row, (int)PlanningXlsxColumns.ViaPoints].Value.ToString();
                            planningTemplateRow.Buffer = (string.IsNullOrEmpty(buffer) || buffer == string.Empty) ? 0 : Convert.ToInt32(buffer.ToString());
                            planningTemplateRow.UOM = (string.IsNullOrEmpty(UOM) || UOM == string.Empty) ? string.Empty : UOM.ToString();
                            planningTemplateRow.Capacity = (string.IsNullOrEmpty(capacity) || capacity == string.Empty) ? string.Empty : capacity.ToString();
                            planningTemplateRow.Remarks2 = (string.IsNullOrEmpty(remarks2) || remarks2 == string.Empty) ? string.Empty : remarks2.ToString();
                            planningTemplateRow.Resource = (string.IsNullOrEmpty(resource) || resource == string.Empty) ? string.Empty : resource.ToString();
                            planningTemplateRow.Vehicle = (string.IsNullOrEmpty(asset) || asset == string.Empty) ? string.Empty : asset.ToString();


                            #endregion

                            planningTemplateRows.Add(planningTemplateRow);
                        }
                        return planningTemplateRows;
                    }
                }
                else
                {
                    return new List<PlanningTemplateRow>();
                }
                

            }

            #endregion


            #region Zone
            public List<ZoneTemplateRow> FillZoneExcelTemplateRow(HttpPostedFile excelFile)
            {

                if (excelFile != null)
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(excelFile.InputStream))
                    {
                        List<ZoneTemplateRow> zoneTemplateRows = new List<ZoneTemplateRow>();
                        ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.First();

                        var maxRows = worksheet.Dimension.End.Row;
                        string uniqueZoneName = string.Empty;
                        for (var row = Headers.DEFAULT_START_ROW; row <= maxRows; row++)
                        {
                            #region Fill Planning Template rows
                            string zoneName = worksheet.Cells[row, (int)ZonesXlsxColumns.ZoneName].Value?.ToString();

                            if (zoneName != null)
                            {
                                if (zoneName != null)
                                    uniqueZoneName = zoneName;

                                if (zoneName == null)
                                    zoneName = uniqueZoneName;

                                string lookupvalue = worksheet.Cells[row, (int)ZonesXlsxColumns.LookUpValue].Value?.ToString();
                                if (lookupvalue == null)
                                    lookupvalue = string.Empty;

                                string household = worksheet.Cells[row, (int)ZonesXlsxColumns.HouseHold].Value?.ToString();
                                if (household == null)
                                    household = string.Empty;


                                #region Excel dropdowns values handling


                                #endregion

                                ZoneTemplateRow zoneTemplateRow = new ZoneTemplateRow();
                                zoneTemplateRow.ZoneName = zoneName.ToString();
                                zoneTemplateRow.TextString = worksheet.Cells[row, (int)ZonesXlsxColumns.TextString].Value?.ToString();
                                zoneTemplateRow.Area = Convert.ToDouble(worksheet.Cells[row, (int)ZonesXlsxColumns.Area].Value);
                                zoneTemplateRow.Perimeter = Convert.ToDouble(worksheet.Cells[row, (int)ZonesXlsxColumns.Perimeter].Value);
                                zoneTemplateRow.Latitude = Convert.ToDouble(worksheet.Cells[row, (int)ZonesXlsxColumns.Latitude].Value);
                                zoneTemplateRow.Longitude = Convert.ToDouble(worksheet.Cells[row, (int)ZonesXlsxColumns.Longitude].Value);
                                zoneTemplateRow.ZoneName = worksheet.Cells[row, (int)ZonesXlsxColumns.ZoneName].Value?.ToString();
                                zoneTemplateRow.LookUpValue = lookupvalue.ToString();
                                zoneTemplateRow.HouseHold = household.ToString();
                                zoneTemplateRow.Radius = Convert.ToDouble(worksheet.Cells[row, (int)ZonesXlsxColumns.Radius].Value);

                                #endregion

                                zoneTemplateRows.Add(zoneTemplateRow);
                            }
                        }
                        return zoneTemplateRows;
                    }
                }
                else
                {
                    return new List<ZoneTemplateRow>();

                }
            }


            public List<ZoneTemplateRow> FillZoneUpdateExcelTemplateRow(HttpPostedFile excelFile)
            {

                if (excelFile != null)
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(excelFile.InputStream))
                    {
                        List<ZoneTemplateRow> zoneTemplateRows = new List<ZoneTemplateRow>();
                        ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.First();

                        var maxRows = worksheet.Dimension.End.Row;
                        string uniqueZoneName = string.Empty;
                        for (var row = Headers.DEFAULT_START_ROW; row <= maxRows; row++)
                        {
                          
                            string zoneName = worksheet.Cells[row, 1].Value?.ToString();
                            if (zoneName != null)
                                uniqueZoneName = zoneName;

                            if (zoneName == null)
                                zoneName = uniqueZoneName;

                            string lookupvalue = worksheet.Cells[row, 7].Value?.ToString();
                            if (lookupvalue == null)
                                lookupvalue = string.Empty;


                            string lat = worksheet.Cells[row, 5].Value?.ToString();
                            if (lat == null)
                                lat = string.Empty;

                            string lon = worksheet.Cells[row, 6].Value?.ToString();
                            if (lon == null)
                                lon = string.Empty;


                            string area = worksheet.Cells[row, 3].Value?.ToString();
                            if (area == null)
                                area = string.Empty;

                            string perimeter = worksheet.Cells[row, 4].Value?.ToString();
                            if (perimeter == null)
                                perimeter = string.Empty;




                            ZoneTemplateRow zoneTemplateRow = new ZoneTemplateRow();
                            zoneTemplateRow.ZoneName = zoneName.ToString();
                            zoneTemplateRow.LookUpValue = lookupvalue.ToString();
                            zoneTemplateRow.Area = area == string.Empty ? 0.0 : Convert.ToDouble(area);
                            zoneTemplateRow.Perimeter = perimeter == string.Empty ? 0.0 : Convert.ToDouble(perimeter);
                            zoneTemplateRow.Latitude = lat == string.Empty ? 0.0 : Convert.ToDouble(lat);     //Convert.ToDouble(lat);//Convert.ToDouble(worksheet.Cells[row, (int)ZonesXlsxColumns.Latitude].Value);
                            zoneTemplateRow.Longitude = lon == string.Empty ? 0.0 : Convert.ToDouble(lon);//Convert.ToDouble(worksheet.Cells[row, (int)ZonesXlsxColumns.Longitude].Value);
                            zoneTemplateRow.ZoneName = worksheet.Cells[row, (int)ZonesXlsxColumns.ZoneName].Value?.ToString();
                            zoneTemplateRow.LookUpValue = lookupvalue.ToString();
                            zoneTemplateRow.Radius = Convert.ToInt32(worksheet.Cells[row, (int)ZonesXlsxColumns.Radius].Value);

                            zoneTemplateRows.Add(zoneTemplateRow);
                        }
                        return zoneTemplateRows;
                    }
                }
                else
                {
                    return new List<ZoneTemplateRow>();
                }

            }

            #endregion

            #region terraPlanning
            public List<TerraPlanningTemplateRow> FillTerraPlanningExcelTemplateRow(HttpPostedFile excelFile)
            {

                if (excelFile != null)
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(excelFile.InputStream))
                    {
                        List<TerraPlanningTemplateRow> terraTemplateRows = new List<TerraPlanningTemplateRow>();
                        ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.First();

                        var maxRows = worksheet.Dimension.End.Row;
                        string uniquePID = string.Empty;
                        for (var row = Headers.DEFAULT_START_ROW; row <= maxRows; row++)
                        {
                            #region Fill Planning Template rows
                            string PID = worksheet.Cells[row, 1].Value?.ToString();
                            if (PID != null)
                                uniquePID = PID;

                            if (PID == null)
                                PID = uniquePID;

                            string fieldcode = worksheet.Cells[row, 5].Value?.ToString();
                            if (fieldcode == null)
                                fieldcode = string.Empty;

                            string hectare = worksheet.Cells[row, 6].Value?.ToString();
                            if (fieldcode == null)
                                fieldcode = string.Empty;

                            string soilType = worksheet.Cells[row, 7].Value?.ToString();
                            if (soilType == null)
                                soilType = string.Empty;

                            string OperationType = worksheet.Cells[row, 8].Value?.ToString();
                            if (OperationType == null)
                                OperationType = string.Empty;

                            string Asset = worksheet.Cells[row, 9].Value?.ToString();
                            if (Asset == null)
                                Asset = string.Empty;

                            string Driver = worksheet.Cells[row, 10].Value?.ToString();
                            if (Driver == null)
                                Driver = string.Empty;

                            #endregion

                            TerraPlanningTemplateRow terraTemplateRow = new TerraPlanningTemplateRow();
                            //terraTemplateRow.PID = (string.IsNullOrEmpty(PID) || PID == string.Empty) ? 0 : Convert.ToInt32(PID.ToString());
                            //terraTemplateRow.Date = Convert.ToDateTime(worksheet.Cells[row, 2].Value);
                            //terraTemplateRow.Asset = Convert.ToDateTime(worksheet.Cells[row, 3].Value);
                            //terraTemplateRow.OperationType = Convert.ToDateTime(worksheet.Cells[row, 4].Value);
                            //terraTemplateRow.ToolType = (string.IsNullOrEmpty(fieldcode) || fieldcode == string.Empty) ? string.Empty : fieldcode.ToString();
                            //terraTemplateRow.Hectare = (string.IsNullOrEmpty(hectare) || hectare == string.Empty) ? string.Empty : hectare.ToString();
                            //terraTemplateRow.SoilType = (string.IsNullOrEmpty(soilType) || soilType == string.Empty) ? string.Empty : soilType.ToString();
                            //terraTemplateRow.OperationType = (string.IsNullOrEmpty(OperationType) || OperationType == string.Empty) ? string.Empty : OperationType.ToString();
                            //terraTemplateRow.Asset = (string.IsNullOrEmpty(Asset) || Asset == string.Empty) ? string.Empty : Asset.ToString();
                            //terraTemplateRow.Driver = (string.IsNullOrEmpty(Driver) || Driver == string.Empty) ? string.Empty : Driver.ToString();




                            terraTemplateRows.Add(terraTemplateRow);
                        }
                        return terraTemplateRows;
                    }
                }
                else
                {
                    return new List<TerraPlanningTemplateRow>();

                }

            }
            #endregion


            #region LOOKUPVALUES
            public List<LookUpValuesTemplateRow> FillUploadExcelTemplateRow(HttpPostedFile excelFile)
            {

                if (excelFile != null)
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(excelFile.InputStream))
                    {
                        List<LookUpValuesTemplateRow> objectTemplateRows = new List<LookUpValuesTemplateRow>();
                        ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.First();

                        var maxRows = worksheet.Dimension.End.Row;

                        for (var row = Headers.DEFAULT_START_ROW; row <= maxRows; row++)
                        {


                            #region Excel dropdowns values handling
                            string LOOKUPTYPE = worksheet.Cells[row, 1].Value?.ToString();

                            if (LOOKUPTYPE == null)
                                LOOKUPTYPE = string.Empty;

                            string lookupCode = worksheet.Cells[row, 5].Value?.ToString();

                            if (lookupCode == null)
                                lookupCode = string.Empty;


                            #endregion

                            LookUpValuesTemplateRow objectTemplateRow = new LookUpValuesTemplateRow();
                            objectTemplateRow.LookUpType = LOOKUPTYPE.ToString();
                            objectTemplateRow.LookUpValue = worksheet.Cells[row, 2].Value?.ToString();
                            objectTemplateRow.Value = worksheet.Cells[row, 3].Value?.ToString();
                            objectTemplateRow.Value2 = worksheet.Cells[row, 4].Value?.ToString();
                            objectTemplateRow.lookupcode = lookupCode.ToString();

                            objectTemplateRows.Add(objectTemplateRow);
                        }

                        return objectTemplateRows;
                    }
                }
                else
                {
                    return new List<LookUpValuesTemplateRow>();

                }
            }
            #endregion


            #endregion


        }
    }

}

