﻿using NaveoOneLib.Constant;
using NaveoOneLib.Models.Users;
using NaveoService.Models;
using NaveoService.Models.DTO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace NaveoService.Helpers
{
    public class ApiHelper
    {

        public string ApiCall(string method, string url, dynamic data)
        {

            string responseString = string.Empty;




            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = method;
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:2.0) Gecko/20100101 Firefox/4.0";
            request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            request.ContentType = "application/json";

            if (data != null)
            {
                String jsData = JsonConvert.SerializeObject(data);
                byte[] body = Encoding.ASCII.GetBytes(jsData);

                request.ContentLength = body.Length;

                //Sending
                using (System.IO.Stream stream = request.GetRequestStream())
                {
                    stream.Write(body, 0, body.Length);
                }

            }

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                System.IO.StreamReader reader = new System.IO.StreamReader(response.GetResponseStream());
                responseString = reader.ReadToEnd();

            }



            return responseString;



        }

        public apigarageFuelData ApiCallToUse(string method, string url, dynamic apiBody)
        {

            dynamic dataLog = string.Empty;

            apigarageFuelData garageData = new apigarageFuelData();
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = method;
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:2.0) Gecko/20100101 Firefox/4.0";
            request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            request.ContentType = "application/json";

            if (apiBody != null)
            {
                String jsData = JsonConvert.SerializeObject(apiBody);
                byte[] body = Encoding.ASCII.GetBytes(jsData);

                request.ContentLength = body.Length;

                //Sending
                using (System.IO.Stream stream = request.GetRequestStream())
                {
                    stream.Write(body, 0, body.Length);
                }

            }

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            if (response.StatusCode == HttpStatusCode.OK)
            {

                System.IO.StreamReader reader = new System.IO.StreamReader(response.GetResponseStream());
                String d = reader.ReadToEnd().ToString();
                d = "{\"lgarageFuelData\":" + d + "}";
                garageData = JsonConvert.DeserializeObject<apigarageFuelData>(d);

            }
            return garageData;



        }

        public string getServerName()
        {

            string sServerName = System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            User.NaveoDatabases myEnum;
            //Getting only the enum part
            sServerName = sServerName.Replace(".naveo.mu", String.Empty);
            //sServerName = sServerName.Replace(":44393", String.Empty);
            sServerName = sServerName.Replace("https://", String.Empty);
            sServerName = sServerName.Replace("http://", String.Empty);
            if (Enum.TryParse(sServerName, true, out myEnum))
                sServerName = sServerName;
            else
                sServerName = sServerName;



            sServerName = sServerName.ToUpper();


            return sServerName;
        }
    }

}
