﻿using NaveoService.Constants;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace NaveoService.Helpers
{
    public static class FileHelper
    {
        #region Reader
        public static Boolean DoesLineExist(String sPath, String needle)
        {
            if (File.Exists(sPath))
            {
                String[] Lines = System.IO.File.ReadAllLines(sPath);
                foreach (String line in Lines)
                    if (line.Trim() == needle.Trim())
                        return true;
            }
            return false;
        }
        public static Boolean ChkCompanyToken(String sPath, String Token)
        {
            if (File.Exists(sPath))
            {
                String[] Lines = System.IO.File.ReadAllLines(sPath);
                foreach (String line in Lines)
                    if (line.Split('|')[1] == Token.Trim())
                        return true;
            }
            return false;
        }

        static Boolean DoesLineExistObsolete(String sPath, String needle)
        {
            Boolean b = false;
            if (File.Exists(sPath))
            {
                String line;
                // Read the file and display it line by line.
                System.IO.StreamReader file = new System.IO.StreamReader(sPath);
                while ((line = file.ReadLine()) != null)
                {
                    if (line.Trim() == needle.Trim())
                    {
                        b = true;
                        break;
                    }
                }
                file.Close();
            }
            return b;
        }
        #endregion

        /// <summary>
        /// Delete files of a specific format
        /// </summary>
        /// <param name="fileExtension"></param>
        /// <param name="userToken"></param>
        /// <returns></returns>
        public static bool TryDeleteFiles(string location, string fileExtension)
        {
            try
            {
                DirectoryInfo directory = new DirectoryInfo(location);
                FileInfo[] files = directory.GetFiles("*" + fileExtension);
                Parallel.ForEach(files, file =>
                {
                    File.Delete(file.FullName);

                });
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete a file or folder + all childs
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static bool TryDeleteFile(string file)
        {
            try
            {
                File.Delete(file);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete a folder if it is empty
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static bool TryDeleteEmptyFolder(string path)
        {
            try
            {
                DirectoryInfo directory = new DirectoryInfo(path);
                FileInfo[] files = directory.GetFiles("*");
                if (files.Length == 0)
                {
                    Directory.Delete(path);
                    return true;
                }
            }
            catch { }
            return false;
        }


        /// <summary>
        /// Verifies if files of a specific extension exists in given location
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static bool FileExists(string path, string fileExtension)
        {
            DirectoryInfo directory = new DirectoryInfo(path);
            FileInfo[] files = directory.GetFiles("*" + fileExtension);
            if (files != null && files.Any())
                return true;

            return false;
        }

        /// <summary>
        /// Get list of files of a specific extension
        /// </summary>
        /// <param name="path"></param>
        /// <param name="fileExtension"></param>
        /// <returns></returns>
        public static FileInfo[] GetFiles(string path, string fileExtension)
        {
            DirectoryInfo directory = new DirectoryInfo(path);
            FileInfo[] files = directory.GetFiles("*" + fileExtension);
            return files;
        }

        /// <summary>
        /// Return content type based on file format
        /// </summary>
        /// <param name="fileExt"></param>
        /// <returns></returns>
        public static string ContentType(string fileExtension)
        {
            switch (fileExtension)
            {
                case FileSystem.Extension.ZIP:
                    return FileSystem.ContentType.ZIP;
                case FileSystem.Extension.XLSX:
                case FileSystem.Extension.CSV:
                    return FileSystem.ContentType.XLSX; // application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
                case FileSystem.Extension.PDF:
                    return FileSystem.ContentType.PDF;
                default:
                    return FileSystem.ContentType.DEFAULT;
            }
        }

        /// <summary>
        /// Converts Stream to file
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static HttpResponseMessage ConvertStreamToFile(MemoryStream stream, string fileName)  //MemoryStream
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var excelStream = stream;
            excelStream.Position = 0;
            result.Content = new StreamContent(stream);
            result.Content.Headers.Add("content-disposition", String.Format(CultureInfo.InvariantCulture, "attachment;filename={0}.xlsx", fileName));
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

            //From Nabla
            result.Content.Headers.Add("x-filename", String.Format(CultureInfo.InvariantCulture, "{0}.xlsx", fileName));
            return result;
        }

        /// <summary>
        /// File name Format
        /// </summary>
        /// <returns></returns>
        public static string NameFormat(string fileName, string extension)
        {
            return DateTime.Now.ToString(ConfigurationKeys.ExportFileName_DateTime_Convention) + "_" + fileName + extension;
        }
    }
}