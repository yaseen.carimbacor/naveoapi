﻿using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.GPS;
using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Models.Users;
using NaveoService.Constants;
using NaveoService.Models;
using NaveoService.Models.Custom;
using NaveoService.Models.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


namespace NaveoService.Helpers
{
    public static class ExtensionHelper
    {
        #region Constants
        public const string AUTHORIZED = Headers.AUTHORIZED;
        public const string FAILED = Headers.FAILED;
        #endregion

        #region ObjectDTO
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="oneTimeToken"></param>
        /// <returns></returns>
        public static ObjectDTO ToObjectDTO(this dtData obj, SecurityToken securityToken)
        {
            if (obj.dsData != null)
            {
                dsDataItems dsData = new dsDataItems();
                dsData.dsData = obj.dsData;

                return new ObjectDTO
                {
                    data = dsData,
                    totalItems = obj.Rowcnt,
                    oneTimeToken = securityToken.OneTimeToken,
                    sQryResult = AUTHORIZED,
                    errorMessage = securityToken.DebugMessage
                };
            }

            MasterDataItems masterData = new MasterDataItems();
            masterData.masterDataItems = obj.Data;
            return new ObjectDTO
            {
                data = masterData,
                totalItems = obj.Rowcnt,
                oneTimeToken = securityToken.OneTimeToken,
                sQryResult = AUTHORIZED,
                errorMessage = securityToken.DebugMessage
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="oneTimeToken"></param>
        /// <author>Yaseen</author>
        /// <returns></returns>
        public static DTOGarage ToObjectDTOGarage(this dtData obj, SecurityToken securityToken)
        {
            string sServerName = new NaveoService.Helpers.ApiHelper().getServerName();

            if (obj.dsData != null)
            {
                dsDataItems dsData = new dsDataItems();
                dsData.dsData = obj.dsData;

                return new DTOGarage
                {
                    core = dsData,
                    ServerName = sServerName,
                    totalItems = obj.Rowcnt,
                    oneTimeToken = securityToken.OneTimeToken,
                    sQryResult = AUTHORIZED,
                    errorMessage = securityToken.DebugMessage
                };
            }

            Models.Custom.Data mData = new Models.Custom.Data();
            mData.data = obj.Data;
            return new DTOGarage
            {
                core = mData,
                ServerName = sServerName,
                totalItems = obj.Rowcnt,
                oneTimeToken = securityToken.OneTimeToken,
                sQryResult = AUTHORIZED,
                errorMessage = securityToken.DebugMessage
            };
        }
        public static NaveoOneLibBaseModelDTO ToNaveoOneLibBaseModelDTO(this BaseModel baseModel, Guid oneTimeToken)
        {
            String sError = baseModel.errorMessage;
            if (baseModel.dbResult != null)
            {
                if (baseModel.dbResult.Tables.Contains("ResultCtrlTbl"))
                {
                    DataRow[] dRow = baseModel.dbResult.Tables["ResultCtrlTbl"].Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");
                    if (dRow.Length > 0)
                        foreach (DataRow dr in baseModel.dbResult.Tables["ResultCtrlTbl"].Rows)
                            foreach (DataColumn dc in baseModel.dbResult.Tables["ResultCtrlTbl"].Columns)
                                sError += dc.ToString() + " > " + dr[dc].ToString() + "; ";
                }
            }

            return new NaveoOneLibBaseModelDTO
            {
                data = baseModel.data,
                oneTimeToken = oneTimeToken,
                sQryResult = AUTHORIZED,
                errorMessage = sError
            };
        }

        public static ObjectDTO ToObjectDTO(this bool result, SecurityToken securityToken)
        {
            return new ObjectDTO
            {
                data = result,
                oneTimeToken = securityToken.OneTimeToken,
                sQryResult = AUTHORIZED,
                errorMessage = securityToken.DebugMessage
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="oneTimeToken"></param>
        /// <returns></returns>
        public static ObjectDTO ToObjectDTO(this NaveoService.Models.Object obj, SecurityToken securityToken)
        {
            return new ObjectDTO
            {
                data = obj,
                oneTimeToken = securityToken.OneTimeToken,
                sQryResult = AUTHORIZED,
                errorMessage = securityToken.DebugMessage
            };
        }
        #endregion

        #region UserDTO
        public static ObjectDTO ToUserDTO(this apiUser obj, SecurityToken securityToken)
        {
            return new ObjectDTO
            {
                data = obj,
                oneTimeToken = securityToken.OneTimeToken,
                sQryResult = AUTHORIZED,
                errorMessage = securityToken.DebugMessage
            };
        }
        #endregion

        #region AssetDTO
        public static BaseDTO ToAssetDTO(this apiAsset result, SecurityToken securityToken)
        {
            return new BaseDTO
            {
                data = result,
                oneTimeToken = securityToken.OneTimeToken,
                sQryResult = AUTHORIZED,
                errorMessage = securityToken.DebugMessage,
                totalItems = 1
            };
        }


        #endregion

        #region AssetDTO
        public static BaseDTO ToAutoMaticDTO(this AutoReportingConfigDTO result, SecurityToken securityToken)
        {
            return new BaseDTO
            {
                data = result,
                oneTimeToken = securityToken.OneTimeToken,
                sQryResult = AUTHORIZED,
                errorMessage = securityToken.DebugMessage,
                totalItems = 1
            };
        }


        #endregion

        #region ResourceDTO
        public static BaseDTO ToResourceDTO(this apiResource result, SecurityToken securityToken)
        {
            return new BaseDTO
            {
                data = result,
                oneTimeToken = securityToken.OneTimeToken,
                sQryResult = AUTHORIZED,
                errorMessage = securityToken.DebugMessage,
                totalItems = 1
            };
        }
        #endregion

        #region LiveDTO
        public static LiveDTO ToLiveDTO(this apiDataLive lData, List<GPSData> lGPSData, SecurityToken securityToken)
        {
            apiDataLive apiData = new apiDataLive();

            try
            {
                //remove all asset that never reported from lGPSData
                lGPSData.RemoveAll(y => y.Availability == "Suspect||No access to asset");

                foreach (GPSData g in lGPSData)
                {
                    LiveData al = new LiveData();
                    al.vehicle.assetID = g.AssetID;
                    al.vehicle.description = g.AssetNum;

                    String[] sParts = g.Availability.Split('|');
                    if (g.DriverID != 1)
                    {
                        MinifiedDriver d = new MinifiedDriver();
                        d.driverID = g.DriverID;
                        d.description = sParts[3];
                        al.driver = d;
                    }

                    al.isGPSValid = g.LongLatValidFlag;
                    al.latitude = g.Latitude;
                    al.longitude = g.Longitude;
                    al.speed = (int)g.Speed;


                    al.activity = sParts[0];

                    String[] sZones = sParts[1].Split(':');
                    string zoneName = string.Empty;

                    if (sZones.Length > 1)
                        zoneName = sZones[1];

                    string status = sParts[0];
                    if (status == "Stop" && zoneName != string.Empty)
                        al.activity = "Stopped inside zone";

                    if (al.activity == "Heading")
                        al.activity = "Driving";

                    int iZone = -1;
                    String[] sZone = sParts[1].Split(':');
                    if (int.TryParse(sZone[0], out iZone))
                    {
                        MinifiedZone z = new MinifiedZone();
                        z.zoneID = iZone;
                        if (sZone.Length > 1)
                            z.description = sZone[1];
                        al.zone = z;
                    }
                    if (sParts.Length > 2)
                        al.address = sParts[2];

                    al.dtLastReported = g.LastReportedInLocalTime;
                    al.dtGPS = g.dtLocalTime;

                    al.MaintenanceStatus = g.MaintenanceStatus;
                    al.LastOdometer = g.LastOdometer;

                    #region fuel
                    if (g.lastFuel != null)
                    {
                        FuelData fD = new FuelData();
                        fD.assetId = g.lastFuel.AssetID;
                        fD.convertedValue = g.lastFuel.ConvertedValue;
                        fD.description = g.lastFuel.Description;
                        fD.dtLocalTime = g.lastFuel.dtLocalTime;
                        fD.fType = g.lastFuel.fType;
                        fD.gpsdataId = g.lastFuel.GPSDataId;
                        fD.lat = g.lastFuel.Latitude;
                        fD.lon = g.Longitude;
                        fD.uom = g.lastFuel.UOM;

                        al.lastFueldata = fD;
                    }


                    #endregion




                    apiData.liveData.Add(al);
                }

            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "\\Registers\\live.dat", "\r\n one Asset Creation on fleet\r\n Asset :" + 999 + ex.ToString() + "\r\n");

            }
            return new LiveDTO
            {
                data = apiData,
                oneTimeToken = securityToken.OneTimeToken,
                sQryResult = AUTHORIZED,
                errorMessage = securityToken.DebugMessage
            };
        }
        #endregion

        #region TripDTO
        public static BaseDTO ToTripDTO(this NaveoService.Models.TripHeader tripHeader, SecurityToken securityToken)
        {
            lTrips l = new lTrips();
            if (tripHeader == null)
            {
                tripHeader = new TripHeader();
                tripHeader.dtTrips = new DataTable();
            }

            foreach (DataRow dr in tripHeader.dtTrips.Rows)
            {
                TripsRpt tr = new TripsRpt();

                tr.vehicle = new MinifiedAsset
                {
                    description = dr["Vehicle"].ToString(),
                    assetID = Convert.ToInt32(dr["AssetID"].ToString())
                };

                if (dr["DriverID"].ToString() != "1")
                    tr.driver = new MinifiedDriver
                    {
                        description = dr["Driver"].ToString(),
                        driverID = Convert.ToInt32(dr["DriverID"].ToString())
                    };

                tr.accel = Convert.ToInt32(dr["Accel"].ToString());
                tr.arrivalAddress = dr["ArrivalAddress"].ToString();
                tr.brake = Convert.ToInt32(dr["Brake"].ToString());
                tr.corner = Convert.ToInt32(dr["Corner"].ToString());
                tr.departureAddress = dr["DepartureAddress"].ToString();
                tr.from = Convert.ToDateTime(dr["From"].ToString());
                tr.to = Convert.ToDateTime(dr["To"].ToString());
                tr.iID = Convert.ToInt32(dr["iID"].ToString());
                tr.maxSpeed = Convert.ToInt32(dr["MaxSpeed"].ToString());
                tr.tripDistance = Convert.ToDouble(dr["TripDistance"].ToString());
                if (!string.IsNullOrEmpty(dr["LastOdometer"].ToString()))
                {
                    tr.lastOdometer = Convert.ToDecimal(dr["LastOdometer"].ToString());
                }
                else
                {
                    tr.lastOdometer = 0;
                }

                try
                {
                    tr.overSpeed1Time = String.IsNullOrEmpty(dr["OverSpeed1Time"].ToString()) ? new TimeSpan(0) : (TimeSpan)(dr["OverSpeed1Time"]);
                    tr.overSpeed2Time = String.IsNullOrEmpty(dr["OverSpeed2Time"].ToString()) ? new TimeSpan(0) : (TimeSpan)(dr["OverSpeed2Time"]);
                    tr.stopTime = String.IsNullOrEmpty(dr["StopTime"].ToString()) ? new TimeSpan(0) : (TimeSpan)(dr["StopTime"]);
                    tr.tripTime = String.IsNullOrEmpty(dr["TripTime"].ToString()) ? new TimeSpan(0) : (TimeSpan)(dr["TripTime"]);
                    tr.idlingTime = String.IsNullOrEmpty(dr["IdlingTime"].ToString()) ? new TimeSpan(0) : (TimeSpan)(dr["IdlingTime"]);
                }
                catch   //oracle
                {
                    tr.overSpeed1Time = String.IsNullOrEmpty(dr["OverSpeed1Time"].ToString()) ? new TimeSpan(0) : TimeSpan.Parse(dr["OverSpeed1Time"].ToString());
                    tr.overSpeed2Time = String.IsNullOrEmpty(dr["OverSpeed2Time"].ToString()) ? new TimeSpan(0) : TimeSpan.Parse(dr["OverSpeed2Time"].ToString());
                    tr.stopTime = String.IsNullOrEmpty(dr["StopTime"].ToString()) ? new TimeSpan(0) : TimeSpan.Parse(dr["StopTime"].ToString());
                    tr.tripTime = String.IsNullOrEmpty(dr["TripTime"].ToString()) ? new TimeSpan(0) : TimeSpan.Parse(dr["TripTime"].ToString());
                    tr.idlingTime = String.IsNullOrEmpty(dr["IdlingTime"].ToString()) ? new TimeSpan(0) : TimeSpan.Parse(dr["IdlingTime"].ToString());
                }
                tr.arrivalLat = Convert.ToDouble(dr["fEndLat"].ToString());
                tr.arrivalLon = Convert.ToDouble(dr["fEndLon"].ToString());
                tr.departureLat = Convert.ToDouble(dr["fStartLat"].ToString());
                tr.departureLon = Convert.ToDouble(dr["fStartLon"].ToString());

                int i = 0;
                if (int.TryParse(dr["ArrivalZoneID"].ToString(), out i))
                {
                    MinifiedZone z = new MinifiedZone();
                    z.zoneID = i;
                    z.description = dr["ArrivalZone"].ToString();
                    if (int.TryParse(dr["iColor"].ToString(), out i))
                        z.sColor = NaveoService.Services.CommonService.HexConverter(Color.FromArgb(i));

                    tr.arrivalZone = z;
                }

                if (int.TryParse(dr["DepartureZoneId"].ToString(), out i))
                {
                    MinifiedZone z = new MinifiedZone();
                    z.zoneID = i;
                    z.description = dr["DepartureZone"].ToString();
                    if (int.TryParse(dr["iColorDZ"].ToString(), out i))
                        z.sColor = NaveoService.Services.CommonService.HexConverter(Color.FromArgb(i));

                    tr.departureZone = z;
                }
                tr.rowNumPerPage = Convert.ToInt16(dr["rowNum"]);
                int eCnt = 0;
                int.TryParse(dr["exceptionsCnt"].ToString(), out eCnt);
                tr.exceptionsCnt = eCnt;
                tr.dtUTCStart = Convert.ToDateTime(dr["dtUTCStart"].ToString());
                tr.dtUTCEnd = Convert.ToDateTime(dr["dtUTCEnd"].ToString());

                l.trips.Add(tr);
            }

            l.totals = tripHeader.dtTotals;

            var sQryResult = FAILED;
            if (tripHeader.dtTrips == null)
                securityToken.DebugMessage = "Could not retrieve data";
            else if (tripHeader.dtTrips.TableName == "dtErr")
                securityToken.DebugMessage = tripHeader.dtTrips.Rows[0]["sError"].ToString();
            else
                sQryResult = AUTHORIZED;

            return new BaseDTO
            {
                data = l,
                oneTimeToken = securityToken.OneTimeToken,
                sQryResult = sQryResult,
                errorMessage = securityToken.DebugMessage,
                totalItems = tripHeader.rowCount
            };
        }

        public static BaseDTO ToOdoDTO(this DataTable dt, SecurityToken securityToken)
        {
            tripDataOdo tripData = new tripDataOdo();
            tripData.odoData = dt;
            return new BaseDTO
            {
                data = tripData,
                oneTimeToken = securityToken.OneTimeToken,
                sQryResult = AUTHORIZED,
                errorMessage = securityToken.DebugMessage,
                totalItems = dt.Rows.Count
            };
        }

        public static BaseDTO ToTripDTO(this TripDetails tripDetails, SecurityToken securityToken)
        {
            var sQryResult = FAILED;
            if (tripDetails.dtTripDetails == null)
                securityToken.DebugMessage = "Could not retrieve data";
            else if (tripDetails.dtTripDetails.TableName == "dtErr")
                securityToken.DebugMessage = tripDetails.dtTripDetails.Rows[0]["sError"].ToString();
            else
                sQryResult = AUTHORIZED;

            return new BaseDTO
            {
                data = tripDetails,
                oneTimeToken = securityToken.OneTimeToken,
                sQryResult = sQryResult,
                errorMessage = securityToken.DebugMessage,
                totalItems = tripDetails.dtTripDetails.Rows.Count
            };
        }
        #endregion

        #region ZoneDTO
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtZones"></param>
        /// <param name="oneTimeToken"></param>
        /// <returns></returns>
        public static ZoneDTO ToZoneDTO(this DataTable dtZones, SecurityToken securityToken)
        {
            NaveoService.Models.Custom.ZoneData zoneData = new NaveoService.Models.Custom.ZoneData();
            zoneData.dtZones = dtZones;
            return new ZoneDTO
            {
                data = zoneData,
                oneTimeToken = securityToken.OneTimeToken,
                sQryResult = AUTHORIZED,
                errorMessage = securityToken.DebugMessage
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="zoneDataResult"></param>
        /// <param name="oneTimeToken"></param>
        /// <returns></returns>
        public static ZoneDTO ToZoneDTO(this NaveoService.Models.ZoneData zoneDataResult, SecurityToken securityToken)
        {
            //NaveoService.Models.Custom.ZoneData zoneData = new NaveoService.Models.Custom.ZoneData();
            //zoneData.zoneList = zoneDataResult.lZone;

            return new ZoneDTO
            {
                data = zoneDataResult,
                totalItems = zoneDataResult.rowCount,
                oneTimeToken = securityToken.OneTimeToken,
                sQryResult = AUTHORIZED,
                errorMessage = securityToken.DebugMessage
            };
        }

        public static ZoneDTO ToZoneDTO(this NaveoOneLib.Models.Zones.ZoneType zoneDataTypeResult, SecurityToken securityToken)
        {
            //NaveoService.Models.Custom.ZoneData zoneData = new NaveoService.Models.Custom.ZoneData();
            //zoneData.zoneList = zoneDataResult.lZone;

            return new ZoneDTO
            {
                data = zoneDataTypeResult,
                //totalItems = zoneDataTypeResult.rowCount,
                oneTimeToken = securityToken.OneTimeToken,
                sQryResult = AUTHORIZED,
                errorMessage = securityToken.DebugMessage
            };
        }



        #endregion

        #region Account

        #region LoginDTO
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="oneTimeToken"></param>
        /// <returns></returns>
        public static LoginDTO ToLoginDTO(this User user, SecurityToken securityToken)
        {
            UserData userData = new UserData();

            return new LoginDTO
            {
                data = userData,
                oneTimeToken = securityToken.OneTimeToken,
                sQryResult = AUTHORIZED,
                errorMessage = securityToken.DebugMessage
            };
        }
        #endregion

        #region UserDTO
        public static BaseDTO ToBaseDTO(this BaseModel response, SecurityToken securityToken)
        {
            String sError = response.errorMessage;
            if (response.dbResult != null)
            {
                if (response.dbResult.Tables.Contains("ResultCtrlTbl"))
                {
                    DataRow[] dRow = response.dbResult.Tables["ResultCtrlTbl"].Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");
                    if (dRow.Length > 0)
                        sError += JsonConvert.SerializeObject(response.dbResult.Tables["ResultCtrlTbl"], Formatting.Indented);
                }
            }

            return new BaseDTO
            {
                data = response.data,
                sQryResult = AUTHORIZED,
                errorMessage = sError,
                totalItems = response.total,
                oneTimeToken = securityToken.OneTimeToken,
                additionalData = response.additionalData
            };
        }

        public static BaseDTO ToBaseDTO(this bool response)
        {
            return new BaseDTO
            {
                data = response,
                sQryResult = AUTHORIZED,
                totalItems = 1
            };
        }

        #endregion

        public static AccountDTO ToAccountDTO(this Account accountModel, SecurityToken securityToken)
        {
            Account am = new Account();
            AccountDTO accountDTO = new AccountDTO
            {
                data = accountModel,
                oneTimeToken = securityToken.OneTimeToken,
                errorMessage = securityToken.DebugMessage
            };

            if (accountModel.success)
                accountDTO.sQryResult = AUTHORIZED;
            else
                accountDTO.sQryResult = Headers.FAILED;

            return accountDTO;
        }

        #endregion

        #region BaseDTO
        public static BaseDTO ToBaseDTO(this bool result, SecurityToken securityToken)
        {
            return new BaseDTO
            {
                data = result,
                oneTimeToken = securityToken.OneTimeToken,
                sQryResult = AUTHORIZED,
                errorMessage = securityToken.DebugMessage,
                totalItems = 1
            };
        }
        #endregion

        #region GPSDataDTO
        public static BaseDTO ToDebugDataDTO(this DebugData debugData, SecurityToken securityToken)
        {
            var sQryResult = FAILED;
            if (debugData.dtData == null)
                securityToken.DebugMessage = "Could not retrieve data";
            else if (debugData.dtData.TableName == "dtErr")
                securityToken.DebugMessage = debugData.dtData.Rows[0]["sError"].ToString();
            else
                sQryResult = AUTHORIZED;

            DbugData dd = new DbugData();
            List<apiDebugData> l = new List<apiDebugData>();
            DataTable dt = debugData.dtData;
            dt.Columns["EngineOn"].ColumnName = "EngineOn";
            foreach (DataRow dr in dt.Rows)
            {
                MinifiedAsset asset = new MinifiedAsset
                {
                    assetID = Convert.ToInt32(dr["assetID"]),
                    description = dr["assetName"].ToString()
                };

                MinifiedDriver driver = new MinifiedDriver
                {
                    driverID = Convert.ToInt32(dr["driverID"]),
                    description = dr["sDriverName"].ToString()
                };

                List<details> ldetails = new List<details>();

                if (dr["details"].ToString() != string.Empty)
                {
                    String[] sDetails = dr["details"].ToString().Split('|');
                    foreach (String d in sDetails)
                    {
                        String[] s = d.Split(':');
                        ldetails.Add(new details
                        {
                            type = s[0].Trim(),
                            value = s[1].Trim().Length == 0 ? "1" : s[1].Trim(),
                            Unit = s[2].Trim()
                        });
                    }
                }

                double convertedSpeed = Math.Round(Convert.ToDouble(dr["speed"]), 0);
                //TimeSpan time = TimeSpan.FromSeconds(Convert.ToDouble(dr["tripTime"]));
                //string tripTime = time.ToString(@"hh\:mm\:ss");

                apiDebugData api = new apiDebugData
                {
                    asset = asset,
                    driver = driver,
                    rowNum = Convert.ToInt32(dr["rowNum"]),
                    uid = Convert.ToInt32(dr["uid"]),
                    dateTimeGPS_UTC = Convert.ToDateTime(dr["localTime"]),
                    dateTimeServer = Convert.ToDateTime(dr["dateTimeServer"]),
                    longitude = Convert.ToDouble(dr["longitude"]),
                    latitude = Convert.ToDouble(dr["latitude"]),
                    longLatValidFlag = Convert.ToInt32(dr["longLatValidFlag"]),
                    speed = Convert.ToInt32(convertedSpeed),
                    engineOn = Convert.ToInt32(dr["EngineOn"]),
                    stopFlag = Convert.ToInt32(dr["stopFlag"]),
                    tripDistance = Convert.ToDouble(dr["tripDistance"]),
                    tripTime = dr["tripTime"].ToString(),
                    workHour = Convert.ToInt32(dr["workHour"]),
                    roadSpeed = dr["roadSpeed"].ToString().Trim().Length == 0 ? (int?)null : Convert.ToInt32(dr["roadSpeed"]),
                    ldetails = ldetails
                };

                l.Add(api);
                dd.debugData = l;
            }

            return new BaseDTO
            {
                data = dd,
                oneTimeToken = securityToken.OneTimeToken,
                sQryResult = sQryResult,
                errorMessage = securityToken.DebugMessage,
                totalItems = debugData.rowCount
            };
        }

        public static BaseDTO ToAuxDTO(this DebugData debugData, SecurityToken securityToken)
        {
            var sQryResult = FAILED;
            if (debugData.dtData == null)
                securityToken.DebugMessage = "Could not retrieve data";
            else if (debugData.dtData.TableName == "dtErr")
                securityToken.DebugMessage = debugData.dtData.Rows[0]["sError"].ToString();
            else
                sQryResult = AUTHORIZED;

            //DbugData dd = new DbugData();
            //List<apiDebugData> l = new List<apiDebugData>();
            //DataTable dt = debugData.dtData;
            //dt.Columns["EngineOn"].ColumnName = "EngineOn";
            //foreach (DataRow dr in dt.Rows)
            //{
            //    MinifiedAsset asset = new MinifiedAsset
            //    {
            //        assetID = Convert.ToInt32(dr["assetID"]),
            //        description = dr["assetName"].ToString()
            //    };

            //    MinifiedDriver driver = new MinifiedDriver
            //    {
            //        driverID = Convert.ToInt32(dr["driverID"]),
            //        description = dr["sDriverName"].ToString()
            //    };

            //    List<details> ldetails = new List<details>();
            //    String[] sDetails = dr["details"].ToString().Split('|');
            //    foreach (String d in sDetails)
            //    {
            //        String[] s = d.Split(':');
            //        ldetails.Add(new details
            //        {
            //            type = s[0].Trim(),
            //            value = s[1].Trim().Length == 0 ? "1" : s[1].Trim(),
            //            Unit = s[2].Trim()
            //        });
            //    }

            //    double convertedSpeed = Math.Round(Convert.ToDouble(dr["speed"]), 0);
            //    //TimeSpan time = TimeSpan.FromSeconds(Convert.ToDouble(dr["tripTime"]));
            //    //string tripTime = time.ToString(@"hh\:mm\:ss");

            //    apiDebugData api = new apiDebugData
            //    {
            //        asset = asset,
            //        driver = driver,
            //        rowNum = Convert.ToInt32(dr["rowNum"]),
            //        uid = Convert.ToInt32(dr["uid"]),
            //        dateTimeGPS_UTC = Convert.ToDateTime(dr["localTime"]),
            //        dateTimeServer = Convert.ToDateTime(dr["dateTimeServer"]),
            //        longitude = Convert.ToDouble(dr["longitude"]),
            //        latitude = Convert.ToDouble(dr["latitude"]),
            //        longLatValidFlag = Convert.ToInt32(dr["longLatValidFlag"]),
            //        speed = Convert.ToInt32(convertedSpeed),
            //        engineOn = Convert.ToInt32(dr["EngineOn"]),
            //        stopFlag = Convert.ToInt32(dr["stopFlag"]),
            //        tripDistance = Convert.ToDouble(dr["tripDistance"]),
            //        tripTime = dr["tripTime"].ToString(),
            //        workHour = Convert.ToInt32(dr["workHour"]),
            //        roadSpeed = dr["roadSpeed"].ToString().Trim().Length == 0 ? (int?)null : Convert.ToInt32(dr["roadSpeed"]),
            //        ldetails = ldetails
            //    };

            //    l.Add(api);
            //    dd.debugData = l;
            //}

            return new BaseDTO
            {
                data = debugData.dtData,
                oneTimeToken = securityToken.OneTimeToken,
                sQryResult = sQryResult,
                errorMessage = securityToken.DebugMessage,
                totalItems = debugData.rowCount
            };
        }
        #endregion

        #region RuleDTO
        /// <summary>
        /// 
        /// </summary>
        /// <param name="lUsers"></param>
        /// <param name="oneTimeToken"></param>
        /// <returns></returns>
        public static BaseDTO ToFuelRuleDTO(this NaveoService.Models.FuelRule fuelRule, SecurityToken securityToken)
        {
            return new BaseDTO
            {
                data = fuelRule.lUsers,
                oneTimeToken = securityToken.OneTimeToken,
                sQryResult = AUTHORIZED,
                errorMessage = securityToken.DebugMessage,
                totalItems = fuelRule.lUsers.Count
            };
        }

        #endregion

        #region FuelDTO
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtZones"></param>
        /// <param name="oneTimeToken"></param>
        /// <returns></returns>
        public static BaseDTO ToFuelDataDTO(this DataTable dt, SecurityToken securityToken)
        {
            fuelDataItems fuelData = new fuelDataItems();
            fuelData.fuelData = dt;
            return new BaseDTO
            {
                data = fuelData,
                oneTimeToken = securityToken.OneTimeToken,
                sQryResult = AUTHORIZED,
                errorMessage = securityToken.DebugMessage,
                totalItems = dt.Rows.Count
            };
        }
        #endregion

        #region ExceptionDTO
        public static BaseDTO ToExceptionDTO(this DataSet ds, SecurityToken securityToken)
        {
            dsDataItems exceptionData = new dsDataItems();
            exceptionData.dsData = ds;
            return new BaseDTO
            {
                data = ds,
                oneTimeToken = securityToken.OneTimeToken,
                sQryResult = AUTHORIZED,
                errorMessage = securityToken.DebugMessage,
                totalItems = ds.Tables.Count
            };
        }


        public static BaseDTO ToPlanningFuelExceptionDTO(this DataTable dt, SecurityToken securityToken)
        {
  
            return new BaseDTO
            {
                data = dt,
                oneTimeToken = securityToken.OneTimeToken,
                sQryResult = AUTHORIZED,
                errorMessage = securityToken.DebugMessage,
                totalItems = dt.Rows.Count
            };
        }
        #endregion

        #region PivotData
        public static ObjectDTO ToPivotDTO(this dtData obj, SecurityToken securityToken)
        {
            List<String> lCols = new List<string>();
            foreach (DataColumn dc in obj.Data.Columns)
                if (dc.ColumnName.Trim().ToLower() != "rownum")
                    if (dc.ColumnName.Trim().ToLower() != "sdesc")
                        lCols.Add(dc.ColumnName);

            List<pivotDataRow> lData = new List<pivotDataRow>();
            foreach (DataRow dr in obj.Data.Rows)
            {
                pivotDataRow p = new pivotDataRow();
                p.rowNum = Convert.ToInt32(dr["RowNum"]);
                p.rowDesc = dr["sDesc"].ToString();

                List<object> lo = new List<object>();
                foreach (DataColumn dc in obj.Data.Columns)
                    if (dc.ColumnName.Trim().ToLower() != "rownum")
                        if (dc.ColumnName.Trim().ToLower() != "sdesc")
                            lo.Add(dr[dc]);
                p.colData = lo;

                lData.Add(p);
            }

            pivotData pData = new pivotData();
            pData.lData = lData;
            pData.lColumnNames = lCols;

            return new ObjectDTO
            {
                data = pData,
                totalItems = obj.Rowcnt,
                oneTimeToken = securityToken.OneTimeToken,
                sQryResult = AUTHORIZED,
                errorMessage = securityToken.DebugMessage
            };
        }
        #endregion

        #region DynamicDataTableDTO
        public static ObjectDTO ToDynamicDataTableDTO(this dtData obj, SecurityToken securityToken)
        {
            List<String> lCols = new List<string>();
            foreach (DataColumn dc in obj.Data.Columns)
                lCols.Add(dc.ColumnName);

            List<pivotDataRow> lData = new List<pivotDataRow>();
            foreach (DataRow dr in obj.Data.Rows)
            {
                pivotDataRow p = new pivotDataRow();
                p.rowNum = Convert.ToInt32(dr["RowNum"]);

                List<object> lo = new List<object>();
                foreach (DataColumn dc in obj.Data.Columns)
                    lo.Add(dr[dc]);
                p.colData = lo;

                lData.Add(p);
            }

            pivotData pData = new pivotData();
            pData.lData = lData;
            pData.lColumnNames = lCols;

            return new ObjectDTO
            {
                data = pData,
                totalItems = obj.Rowcnt,
                oneTimeToken = securityToken.OneTimeToken,
                sQryResult = AUTHORIZED,
                errorMessage = securityToken.DebugMessage
            };
        }
        #endregion

        #region MatrixDTO
        public static BaseDTO ToMatrixDTO(this GroupMatrix result, SecurityToken securityToken)
        {
            return new BaseDTO
            {
                data = result,
                oneTimeToken = securityToken.OneTimeToken,
                sQryResult = AUTHORIZED,
                errorMessage = securityToken.DebugMessage,
                totalItems = 1
            };
        }
        #endregion

        #region HeartBeatDTO

        public static HeartBeatDTO ToHeartBeatDTO(this HeartBeatResult result, SecurityToken securityToken)
        {
            HeartBeatResult hBeatResult = new HeartBeatResult(false);
            if (result != null)
            {
                hBeatResult.hasDownloads = result.hasDownloads;
                hBeatResult.errorList = result.errorList;

                return new HeartBeatDTO
                {
                    data = hBeatResult,
                    oneTimeToken = securityToken.OneTimeToken,
                    sQryResult = AUTHORIZED,
                    errorMessage = securityToken.DebugMessage
                };
            }
            else
            {
                return new HeartBeatDTO
                {
                    data = hBeatResult,
                    oneTimeToken = securityToken.OneTimeToken,
                    sQryResult = AUTHORIZED,
                    errorMessage = securityToken.DebugMessage
                };
            }
        }

        #endregion

        public static ObjectDTO ToSummarizedDailyTrip(this dtData obj, SecurityToken securityToken)
        {
            if (obj.dsData != null)
            {
                dsDataItems dsData = new dsDataItems();
                dsData.dsData = obj.dsData;

                return new ObjectDTO
                {
                    data = dsData,
                    totalItems = obj.Rowcnt,
                    oneTimeToken = securityToken.OneTimeToken,
                    sQryResult = AUTHORIZED,
                    errorMessage = securityToken.DebugMessage
                };
            }

            MasterDataItems masterData = new MasterDataItems();
            masterData.masterDataItems = obj.Data;
            return new ObjectDTO
            {
                data = masterData,
                totalItems = obj.Rowcnt,
                oneTimeToken = securityToken.OneTimeToken,
                sQryResult = AUTHORIZED,
                errorMessage = securityToken.DebugMessage
            };
        }

        #region PlanningDTO
        public static BaseDTO ToPlanningDTO(this apiPlanning result, SecurityToken securityToken)
        {
            return new BaseDTO
            {
                data = result,
                oneTimeToken = securityToken.OneTimeToken,
                sQryResult = AUTHORIZED,
                errorMessage = securityToken.DebugMessage,
                totalItems = 1
            };
        }
        #endregion
    }
}
