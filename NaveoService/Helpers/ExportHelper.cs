﻿using NaveoOneLib.Common;
using NaveoOneLib.Maps;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.GPS;
using NaveoOneLib.Models.Trips;
using NaveoService.Constants;
using NaveoService.Models;
using NaveoService.Models.Custom;
using NaveoService.Models.DTO;
using NaveoService.Models.ImportTemplates;
using NaveoService.Services;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace NaveoService.Helpers
{
    public static class ExportHelper
    {
        #region Properties

        private static string color { get; set; }

        #endregion

        #region Constructor

        #endregion

        #region EXCEL
        public static ExcelPackage XcelUser(int row)
        {
            String templateDocument = Globals.ExportPath(ExportTemplate.User);
            // Open Template
            using (FileStream excelTemplate = File.OpenRead(templateDocument))
            {
                // Create Excel EPPlus Package based on template stream
                ExcelPackage package = new ExcelPackage(excelTemplate);
                // Grab the 1st sheet with the template.
                ExcelWorksheet sheet = package.Workbook.Worksheets.First();

                #region Data to export to excel
                List<UserTemplateRow> userRows = new List<UserTemplateRow>
                                {
                                    new UserTemplateRow { UID = 1, Names = "John", Email = "john@naveo.mu" },
                                    new UserTemplateRow { UID = 2, Names = "James", Email = "james@naveo.mu" },
                                    new UserTemplateRow { UID = 3, Names = "Mark", Email = "mark@naveo.mu" }
                                };
                #endregion
                // Insert data into template
                if (sheet != null)
                {
                    int listIndex = 0;
                    int listCount = userRows.Count;
                    for (int column = 1; column <= listCount; column++)
                    {
                        sheet.Cells[row, column].Value = userRows[listIndex].UID;
                        sheet.Cells[row, column].Value = userRows[listIndex].Names;
                        sheet.Cells[row, column].Value = userRows[listIndex].Email;
                        row++; listIndex++;
                    }
                }
                return package;
            }
        }
        public static ExcelPackage XcelTrip(NaveoService.Models.TripHeader th, int row)
        {

            //BaseDTO b = data;

   
            String templateDocument = Globals.ExportPath(ExportTemplate.Trip);

            using (FileStream excelTemplate = File.OpenRead(templateDocument))
            {
                // Create Excel EPPlus Package based on template stream
                ExcelPackage package = new ExcelPackage(excelTemplate);

                // Grab the 1st sheet with the template.
                ExcelWorksheet sheet = package.Workbook.Worksheets.First();
                // Insert data into template
                if (sheet != null && th != null)
                {
                    if (th.dtTrips.Rows.Count == 0)
                        return package;

                    try
                    {
                        //TimeSpan totalTripTime = new TimeSpan();
                        //Double totalTripDistance = 0;
                        //TimeSpan totalStopTime = new TimeSpan();
                        //TimeSpan totalIdleTime = new TimeSpan();
                        //TimeSpan totaloverSpeed1Time = new TimeSpan();
                        //TimeSpan totaloverSpeed2Time = new TimeSpan();
                        //Double totalMaxSpeed = 0;
                        //Double totalAcceleration = 0;
                        //Double totalBrake = 0;
                        //Double totalCorner = 0;
                        string tripTime = "";
                        string tripDistance = "";
                        string stopTime = "";
                        string idleTime = "";
                        string maxSpeed = "";
                        string acceleration = "";
                        string brake = "";
                        string corner = "";
                        string overSpeed1Time = "";
                        string overSpeed2Time = "";

                        var rows = th.dtTrips.Rows;
                        var rowTotals = th.dtTotals.Rows;
                        foreach (DataRow dataRow in rows)
                        {
                            sheet.Cells[row, (int)TripsXlsxColumns.TripId].Value = dataRow["iID"];
                            sheet.Cells[row, (int)TripsXlsxColumns.AssetId].Value = dataRow["AssetID"];
                            sheet.Cells[row, (int)TripsXlsxColumns.AssetName].Value = dataRow["Vehicle"];
                            sheet.Cells[row, (int)TripsXlsxColumns.DriverId].Value = dataRow["DriverID"];

                            if (!dataRow["Driver"].ToString().Equals("Default Driver"))
                            {
                                sheet.Column((int)TripsXlsxColumns.DriverName).Hidden = false;
                                sheet.Cells[row, (int)TripsXlsxColumns.DriverName].Value = dataRow["Driver"].ToString();
                            }

                            tripTime = dataRow["TripTime"].ToString();
                            tripDistance = dataRow["TripDistance"].ToString();
                            stopTime = dataRow["StopTime"].ToString();
                            string from = Convert.ToDateTime(dataRow["From"]).ToString("dd/MM/yy hh:mm:tt");
                            string to = Convert.ToDateTime(dataRow["To"]).ToString("dd/MM/yy hh:mm:tt ");

                            sheet.Cells[row, (int)TripsXlsxColumns.StartDate].Value = from;
                            sheet.Cells[row, (int)TripsXlsxColumns.TripTime].Value = tripTime;
                            sheet.Cells[row, (int)TripsXlsxColumns.EndDate].Value = to;
                            sheet.Cells[row, (int)TripsXlsxColumns.TripDistance].Value = tripDistance;



                            sheet.Cells[row, (int)TripsXlsxColumns.Odometer].Value = null; //dataRow["From"].ToString();
                            sheet.Cells[row, (int)TripsXlsxColumns.EngineHours].Value = null;//dataRow["From"].ToString();
                            sheet.Cells[row, (int)TripsXlsxColumns.StopTime].Value = stopTime;
                            sheet.Cells[row, (int)TripsXlsxColumns.DepartureAddress].Value = dataRow["DepartureAddress"];
                            sheet.Cells[row, (int)TripsXlsxColumns.ArrivalAddress].Value = dataRow["ArrivalAddress"];

                            int i = 0;
                            if (int.TryParse(dataRow["iColorDZ"].ToString(), out i))
                            {
                                sheet.Cells[row, (int)TripsXlsxColumns.DepartureZone].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                sheet.Cells[row, (int)TripsXlsxColumns.DepartureZone].Style.Fill.BackgroundColor.SetColor(ConvertToColor(i));
                                sheet.Cells[row, (int)TripsXlsxColumns.DepartureZone].Value = dataRow["DepartureZone"];
                            }

                            if (int.TryParse(dataRow["iColor"].ToString(), out i))
                            {
                                sheet.Cells[row, (int)TripsXlsxColumns.ArrivalZone].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                sheet.Cells[row, (int)TripsXlsxColumns.ArrivalZone].Style.Fill.BackgroundColor.SetColor(ConvertToColor(i));
                                sheet.Cells[row, (int)TripsXlsxColumns.ArrivalZone].Value = dataRow["ArrivalZone"];
                            }

                            idleTime = dataRow["IdlingTime"].ToString();
                            maxSpeed = dataRow["MaxSpeed"].ToString();
                            acceleration = dataRow["Accel"].ToString();
                            brake = dataRow["Brake"].ToString();
                            corner = dataRow["Corner"].ToString();
                            overSpeed1Time = dataRow["OverSpeed1Time"].ToString();
                            overSpeed2Time = dataRow["OverSpeed1Time"].ToString();
                            sheet.Cells[row, (int)TripsXlsxColumns.IdleTime].Value = idleTime;
                            sheet.Cells[row, (int)TripsXlsxColumns.MaxSpeed].Value = maxSpeed;
                            sheet.Cells[row, (int)TripsXlsxColumns.Acceleration].Value = acceleration;
                            sheet.Cells[row, (int)TripsXlsxColumns.Brake].Value = brake;
                            sheet.Cells[row, (int)TripsXlsxColumns.Corner].Value = corner;
                            sheet.Cells[row, (int)TripsXlsxColumns.Alerts].Value = dataRow["exceptionsCnt"].ToString();
                            sheet.Cells[row, (int)TripsXlsxColumns.SpeedingTime1].Value = overSpeed1Time;
                            sheet.Cells[row, (int)TripsXlsxColumns.SpeedingTime2].Value = overSpeed2Time;

                            //totalTripDistance = totalTripDistance + Convert.ToDouble(tripDistance);
                            //TimeSpan ts = new TimeSpan();
                            //Double d = 0.0;
                            //if (TimeSpan.TryParse(tripTime, out ts))
                            //    totalTripTime = totalTripTime.Add(ts);
                            //if (TimeSpan.TryParse(stopTime, out ts))
                            //    totalStopTime = totalStopTime.Add(ts);
                            //if (TimeSpan.TryParse(idleTime, out ts))
                            //    totalIdleTime = totalIdleTime.Add(ts);
                            //if (Double.TryParse(maxSpeed, out d))
                            //    if (d > totalMaxSpeed)
                            //        totalMaxSpeed = d;
                            //if (Double.TryParse(acceleration, out d))
                            //    totalAcceleration += d;
                            //if (Double.TryParse(brake, out d))
                            //    totalBrake += d;
                            //if (Double.TryParse(corner, out d))
                            //    totalCorner += d;
                            //if (TimeSpan.TryParse(overSpeed1Time, out ts))
                            //    totaloverSpeed1Time = totaloverSpeed1Time.Add(ts);
                            //if (TimeSpan.TryParse(overSpeed2Time, out ts))
                            //    totaloverSpeed2Time = totaloverSpeed1Time.Add(ts);
                            row++;
                        }

                        TotalBorderStyle(sheet, row, (int)TripsXlsxColumns.TripTime);
                        TotalBorderStyle(sheet, row, (int)TripsXlsxColumns.TripDistance);
                        TotalBorderStyle(sheet, row, (int)TripsXlsxColumns.StopTime);
                        TotalBorderStyle(sheet, row, (int)TripsXlsxColumns.IdleTime);
                        TotalBorderStyle(sheet, row, (int)TripsXlsxColumns.MaxSpeed);
                        //TotalBorderStyle(sheet, row, (int)TripsXlsxColumns.Acceleration);
                        //TotalBorderStyle(sheet, row, (int)TripsXlsxColumns.Brake);
                        //TotalBorderStyle(sheet, row, (int)TripsXlsxColumns.Corner);
                        // TotalBorderStyle(sheet, row, (int)TripsXlsxColumns.SpeedingTime1);
                        // TotalBorderStyle(sheet, row, (int)TripsXlsxColumns.SpeedingTime2);
                        //TotalBorderStyle(sheet, row, (int)TripsXlsxColumns.SpeedingTime1);
                        //TotalBorderStyle(sheet, row, (int)TripsXlsxColumns.SpeedingTime1);


                        //Totals
                        foreach (DataRow dataRowTotal in rowTotals)
                        {
                            double totalTripTime = Convert.ToDouble(dataRowTotal["totalTripTime"]);
                            TimeSpan time = TimeSpan.FromSeconds(totalTripTime);
                  
      

                            Double totalStopTime = Convert.ToDouble(dataRowTotal["totalStopTime"]);
                            TimeSpan time2 = TimeSpan.FromSeconds(totalStopTime);
            

                            Double totalIdlingTime = Convert.ToDouble(dataRowTotal["totalIdlingTime"]);
                            TimeSpan time3 = TimeSpan.FromSeconds(totalIdlingTime);
                    

                            sheet.Cells[row, (int)TripsXlsxColumns.TripTime].Value = time;
                            sheet.Cells[row, (int)TripsXlsxColumns.TripDistance].Value = dataRowTotal["totalTripDistance"].ToString();
                            sheet.Cells[row, (int)TripsXlsxColumns.MaxSpeed].Value = dataRowTotal["maxSpeed"].ToString();
                            sheet.Cells[row, (int)TripsXlsxColumns.StopTime].Value = time2;
                            sheet.Cells[row, (int)TripsXlsxColumns.IdleTime].Value = time3;
                        }

                        //sheet.Cells[row, (int)TripsXlsxColumns.Acceleration].Value = totalAcceleration.ToString();
                        //sheet.Cells[row, (int)TripsXlsxColumns.Brake].Value = totalBrake.ToString();
                        //sheet.Cells[row, (int)TripsXlsxColumns.Corner].Value = totalCorner.ToString();
                        //sheet.Cells[row, (int)TripsXlsxColumns.SpeedingTime1].Value = totaloverSpeed1Time.ToString();
                        //sheet.Cells[row, (int)TripsXlsxColumns.SpeedingTime2].Value = totaloverSpeed2Time.ToString();

                    } catch (Exception ex )
                    {

                    }
                }
                return package;
            }
        }
        public static ExcelPackage XcelLive(List<GPSData> liveData, int row)
        {
            String templateDocument = Globals.ExportPath(ExportTemplate.Live);
            using (FileStream excelTemplate = File.OpenRead(templateDocument))
            {
                // Create Excel EPPlus Package based on template stream
                ExcelPackage package = new ExcelPackage(excelTemplate);
                // Grab the 1st sheet with the template.
                ExcelWorksheet sheet = package.Workbook.Worksheets.First();
                //Insert data into template
                if (sheet != null)
                {
                    foreach (GPSData item in liveData)
                    {
                        try
                        {
                            if (item.Availability != "Suspect||No access to asset")
                            {
                                String[] sParts = item.Availability.Split('|');
                                string Availability = sParts[0];

                                string zone = string.Empty;
                                if (sParts[1] != string.Empty)
                                {
                                    String[] sZone = sParts[1].Split(':');
                                    zone = sZone[1];
                                }

                                string address = sParts[2];

                                string driver = String.Empty;
                                if (sParts.Length > 3)
                                    driver = sParts[3];

                                String[] sZones = sParts[1].Split(':');
                                string zoneName = string.Empty;

                                if (sZones.Length > 1)
                                    zoneName = sZones[1];

                                string status = sParts[0];
                                if (status == "Stop" && zoneName != string.Empty)
                                    Availability = "Stopped inside zone";

                                if (Availability == "Heading")
                                    Availability = "Driving";




                                Double NoOfDays = (DateTime.Now - (DateTime)item.LastReportedInLocalTime).TotalDays;
                                int x = (int)Math.Round(NoOfDays);
                                sheet.Cells[row, (int)LiveDataXlsxColumns.Status].Value = Availability;
                                sheet.Cells[row, (int)LiveDataXlsxColumns.AssetID].Value = item.AssetID;
                                sheet.Cells[row, (int)LiveDataXlsxColumns.Asset].Value = item.AssetNum;//d.sDriverName;
                                sheet.Cells[row, (int)LiveDataXlsxColumns.Driver].Value = driver;
                                sheet.Cells[row, (int)LiveDataXlsxColumns.Zone].Value = zone;
                                sheet.Cells[row, (int)LiveDataXlsxColumns.Speed].Value = item.Speed;
                                sheet.Cells[row, (int)LiveDataXlsxColumns.Latitude].Value = item.Latitude;
                                sheet.Cells[row, (int)LiveDataXlsxColumns.Longitude].Value = item.Longitude;
                                sheet.Cells[row, (int)LiveDataXlsxColumns.Address].Value = address;
                                sheet.Cells[row, (int)LiveDataXlsxColumns.Last_Reported_Date].Value = item.LastReportedInLocalTime.ToString("dd/MM/yyyy HH:mm");//x + " Days";
                                sheet.Cells[row, (int)LiveDataXlsxColumns.Time].Value = item.LastReportedInLocalTime.ToString("HH:mm");
                                sheet.Cells[row, (int)LiveDataXlsxColumns.GPS_Date].Value = item.dtLocalTime.ToString("dd/MM/yyyy HH:mm");
                                sheet.Cells[row, (int)LiveDataXlsxColumns.Fuel].Value = item.lastFuel.ConvertedValue;
                                sheet.Cells[row, (int)LiveDataXlsxColumns.Track].Value = 0;


                            }
                        }
                        catch { }


                        row++;
                    }

                }
                return package;
            }

        }
        public static ExcelPackage XcelDailySummarizedTrip(DataSet ds, int row)
        {
            String templateDocument = Globals.ExportPath(ExportTemplate.DailySummarizedTrip);
            using (FileStream excelTemplate = File.OpenRead(templateDocument))
            {
                // Create Excel EPPlus Package based on template stream
                ExcelPackage package = new ExcelPackage(excelTemplate);
                // Grab the 1st sheet with the template.
                ExcelWorksheet sheet = package.Workbook.Worksheets.First();

                // Insert data into template
                if (sheet != null)
                {


                    DataTable dt = ds.Tables["Report"];
                    DataTable dtTotals = ds.Tables["Totals"];

                    //int i = 0;
                    foreach (DataRow dr in dt.Rows)
                    {
                        sheet.Cells[row, (int)GetDailySummarizedTripsXlsxColumns.date].Value = Convert.ToDateTime(dr["date"]).ToString("dd/MM/yyyy");
                        sheet.Cells[row, (int)GetDailySummarizedTripsXlsxColumns.registrationNo].Value = dr["registrationNo"].ToString();
                        sheet.Cells[row, (int)GetDailySummarizedTripsXlsxColumns.driver].Value = dr["driver"].ToString();
                        sheet.Cells[row, (int)GetDailySummarizedTripsXlsxColumns.trips].Value = dr["trips"].ToString();
                        sheet.Cells[row, (int)GetDailySummarizedTripsXlsxColumns.startTime].Value = Convert.ToDateTime(dr["startTime"]).ToString("HH:mm");
                        sheet.Cells[row, (int)GetDailySummarizedTripsXlsxColumns.endTime].Value = Convert.ToDateTime(dr["endTime"]).ToString("HH:mm");
                        sheet.Cells[row, (int)GetDailySummarizedTripsXlsxColumns.kmTravelled].Value = dr["kmTravelled"].ToString();
                        sheet.Cells[row, (int)GetDailySummarizedTripsXlsxColumns.idlingTime].Value = dr["idlingTime"].ToString();
                        //sheet.Cells[row, (int)GetDailySummarizedTripsXlsxColumns.drivingTime].Value = dr["drivingTime"].ToString();
                        sheet.Cells[row, (int)GetDailySummarizedTripsXlsxColumns.inZoneStopTime].Value = dr["inZoneStopTime"].ToString();
                        sheet.Cells[row, (int)GetDailySummarizedTripsXlsxColumns.outZoneStopTime].Value = dr["outZoneStopTime"].ToString();

                        //if (!String.IsNullOrEmpty(dr["trips"].ToString()))
                        //    if (int.TryParse(dr["trips"].ToString(), out i))
                        //        totalTrips += i;

                        //if (!String.IsNullOrEmpty(dr["kmTravelled"].ToString()))
                        //    if (int.TryParse(dr["kmTravelled"].ToString(), out i))
                        //        totalKmTravelled += i;

                        //TimeSpan ts = new TimeSpan();
                        //if (TimeSpan.TryParse(dr["idlingTime"].ToString(), out ts))
                        //    totalIdleTime = totalIdleTime.Add(ts);

                        row++;
                    }

                    var borderStyle = ExcelBorderStyle.Thick;
                    sheet.Cells[row, (int)GetDailySummarizedTripsXlsxColumns.trips].Style.Border.Top.Style = borderStyle;
                    sheet.Cells[row, (int)GetDailySummarizedTripsXlsxColumns.trips].Style.Border.Bottom.Style = borderStyle;
                    sheet.Cells[row, (int)GetDailySummarizedTripsXlsxColumns.kmTravelled].Style.Border.Top.Style = borderStyle;
                    sheet.Cells[row, (int)GetDailySummarizedTripsXlsxColumns.kmTravelled].Style.Border.Bottom.Style = borderStyle;
                    sheet.Cells[row, (int)GetDailySummarizedTripsXlsxColumns.idlingTime].Style.Border.Top.Style = borderStyle;
                    sheet.Cells[row, (int)GetDailySummarizedTripsXlsxColumns.idlingTime].Style.Border.Bottom.Style = borderStyle;

                    foreach (DataRow rowTotals in dtTotals.Rows)
                    {
                        sheet.Cells[row, (int)GetDailySummarizedTripsXlsxColumns.trips].Value = rowTotals["totalTrips"].ToString();
                        sheet.Cells[row, (int)GetDailySummarizedTripsXlsxColumns.kmTravelled].Value = rowTotals["totalTripDistance"].ToString();
                        sheet.Cells[row, (int)GetDailySummarizedTripsXlsxColumns.idlingTime].Value = rowTotals["totalIdlingTime"].ToString();
                    }




                }
                return package;
            }
        }
        public static ExcelPackage XcelSummarizedTrip(DataTable dt, int row)
        {
            String templateDocument = Globals.ExportPath(ExportTemplate.SummarizedTrip);
            using (FileStream excelTemplate = File.OpenRead(templateDocument))
            {
                // Create Excel EPPlus Package based on template stream
                ExcelPackage package = new ExcelPackage(excelTemplate);

                // Grab the 1st sheet with the template.
                ExcelWorksheet sheet = package.Workbook.Worksheets.First();

                // Insert data into template
                if (sheet != null)
                {
                    var rows = dt.Rows;
                    foreach (DataRow dr in rows)
                    {

                        TimeSpan t = TimeSpan.FromSeconds(Convert.ToDouble(dr["drivingTime"].ToString()));
                        TimeSpan t1 = TimeSpan.FromSeconds(Convert.ToDouble(dr["idlingTime"].ToString()));



                        sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.RegistrationNo].Value = dr["registrationNo"].ToString();
                        sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.Driver].Value = dr["driver"].ToString();
                        sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.MaxSpeed].Value = dr["MaxSpeed"].ToString();
                        sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.KmTravelled].Value = dr["kmTravelled"].ToString();
                        sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.CurrentOdometer].Value = dr["CurrentOdometer"].ToString();
                        sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.Stops].Value = dr["Stops"].ToString();
                        sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.StopLess30min].Value = dr["stopLess30min"].ToString();
                        sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.StopLess60min].Value = dr["stopLess60min"].ToString();
                        sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.StopMore60min].Value = dr["stopMore60Min"].ToString();
                        sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.StopLess5Hrs].Value = dr["stopLess5Hrs"].ToString();
                        sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.StopMore5Hrs].Value = dr["stopMore5Hrs"].ToString();
                        sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.Trips].Value = dr["trips"].ToString();
                        sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.DrivingTime].Value = t.ToString(@"d'd, 'hh\:mm\:ss");
                        sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.IdlingTime].Value = t1.ToString(@"d'd, 'hh\:mm\:ss");
                        sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.SpeedMore70Kms].Value = dr["speedMore70Kms"].ToString();
                        sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.SpeedMore80Kms].Value = dr["speedMore80Kms"].ToString();
                        sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.SpeedMore110Kms].Value = dr["speedMore110Kms"].ToString();
                        sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.fuelcost].Value = dr["speedMore110Kms"].ToString();
                        sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.fuelcost].Value = dr["fuelcost"].ToString();
                        sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.costperkm].Value = dr["costperkm"].ToString();
                        sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.fuelinlitres].Value = dr["fuelinlitres"].ToString();
                        sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.fuelinlitresper100].Value = dr["fuelinlitresper100km"].ToString();
                        sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.totalcost].Value = dr["totalcost"].ToString();
                        sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.totalcostper100].Value = dr["totalper100km"].ToString();
                        row++;
                    }
                }
                return package;
            }
        }
        public static ExcelPackage XcelDynamic(DataTable dt,DateTime DateFrom,DateTime DateTimeTo,int row)
        {
            ExcelPackage package = new ExcelPackage();

            // Grab the 1st sheet with the template.
            ExcelWorksheet sheet = package.Workbook.Worksheets.Add("Naveo");
            sheet = package.Workbook.Worksheets.First();
            row = 3;

 
            string reportName = "Report Extracted for the Period of " + DateFrom.ToString() + " To " + DateTimeTo.ToString();
            sheet.Cells[1, 5].Value = reportName.ToUpper();
            //sheet.Column(5).Width = 100;
            //sheet.GetMergeCellId(1, 50);
            sheet.Column(5).AutoFit();

            Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#00BFFF");
            sheet.Cells[1, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[1, 1].Style.Fill.BackgroundColor.SetColor(colFromHex);
            sheet.Cells[1, 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[1, 2].Style.Fill.BackgroundColor.SetColor(colFromHex);

            sheet.Cells[1, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[1, 3].Style.Fill.BackgroundColor.SetColor(colFromHex);
            sheet.Cells[1, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[1, 4].Style.Fill.BackgroundColor.SetColor(colFromHex);
            sheet.Cells[1, 5].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[1, 5].Style.Fill.BackgroundColor.SetColor(colFromHex);
            sheet.Cells[1, 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[1, 6].Style.Fill.BackgroundColor.SetColor(colFromHex);
            sheet.Cells[1, 7].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[1, 7].Style.Fill.BackgroundColor.SetColor(colFromHex);
            sheet.Cells[1, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[1, 8].Style.Fill.BackgroundColor.SetColor(colFromHex);
            sheet.Cells[1, 9].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[1, 9].Style.Fill.BackgroundColor.SetColor(colFromHex);
            sheet.Cells[1, 10].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[1, 10].Style.Fill.BackgroundColor.SetColor(colFromHex);
            sheet.Cells[1, 11].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[1, 11].Style.Fill.BackgroundColor.SetColor(colFromHex);
            sheet.Cells[1, 12].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[1, 12].Style.Fill.BackgroundColor.SetColor(colFromHex);

            sheet.Cells[1, 5].Style.Font.Size = 15;
            sheet.Cells[1, 5].Style.Font.Color.SetColor(Color.White);
            sheet.Cells[1, 5].Style.Font.Bold = true;


            // Insert data
            if (sheet != null)
            {
                var rows = dt.Rows;
                foreach (DataRow dr in rows)
                {
                    int countColumn = 0;
                    foreach (DataColumn dc in dt.Columns)
                    {
                        countColumn++;
                        if (countColumn > 0)
                        {
                            string colData = dc.ColumnName.ToString();
                            sheet.Cells[2, countColumn].Value = colData;

                            if (colData == "EngineOn")
                            {
                                //colData = "EngineOn";
                                sheet.Cells[2, countColumn].Value = "EngineOn";
                                sheet.Cells[row, countColumn].Value = dr[colData].ToString();
                            }



                            if (colData == "TripDistance")
                            {
                                decimal tD = Convert.ToDecimal(dr[colData]);
                                dr[colData] = Math.Round(tD, 2);
                            }


                            sheet.Cells[row, countColumn].Value = dr[colData].ToString();
                        }
                    }
                    row++;
                }

            }
            return package;
        }

        public static ExcelPackage XcelDynamic1(DataTable dt, int row)
        {
            ExcelPackage package = new ExcelPackage();

            // Grab the 1st sheet with the template.
            ExcelWorksheet sheet = package.Workbook.Worksheets.Add("Naveo");
            sheet = package.Workbook.Worksheets.First();
            row = 3;

            // Insert data
            if (sheet != null)
            {
                var rows = dt.Rows;
                foreach (DataRow dr in rows)
                {
                    int countColumn = 0;
                    foreach (DataColumn dc in dt.Columns)
                    {
                        countColumn++;
                        if (countColumn > 0)
                        {
                            string colData = dc.ColumnName.ToString();
                            sheet.Cells[2, countColumn].Value = colData;

                            if (colData == "EngineOn")
                            {
                                //colData = "EngineOn";
                                sheet.Cells[2, countColumn].Value = "EngineOn";
                                sheet.Cells[row, countColumn].Value = dr[colData].ToString();
                            }



                            if (colData == "TripDistance")
                            {
                                decimal tD = Convert.ToDecimal(dr[colData]);
                                dr[colData] = Math.Round(tD, 2);
                            }


                            sheet.Cells[row, countColumn].Value = dr[colData].ToString();
                        }
                    }
                    row++;
                }

            }
            return package;
        }
        public static ExcelPackage XcelException(DataSet ds, int row)
        {
            String templateDocument = Globals.ExportPath(ExportTemplate.Exception);
            using (FileStream excelTemplate = File.OpenRead(templateDocument))
            {
                // Create Excel EPPlus Package based on template stream
                ExcelPackage package = new ExcelPackage(excelTemplate);

                try
                {


                    // Grab the 1st sheet with the template.
                    ExcelWorksheet sheet = package.Workbook.Worksheets.First();

                    // Insert data into template
                    if (sheet != null)
                    {
                        DataTable dt = ds.Tables["dtExceptionHeader"];

                        dt.Columns.Add("addresss");
                        DataRow[] drAddress = dt.Select();
                        SimpleAddress.GetAddresses(drAddress, "addresss", "lon", "lat");
                        //OfficeOpenXml.DataValidation.Contracts.IExcelDataValidationDateTime validationDateTime= null;

                        //OfficeOpenXml.DataValidation.IRangeDataValidation rangeDataValidation = null;

                        //validationDateTime = rangeDataValidation.AddDateTimeDataValidation();


                        var rows = dt.Rows;
                        foreach (DataRow dr in rows)
                        {
                            sheet.Cells[row, (int)GetExceptions.ruleName].Value = dr["ruleName"].ToString();
                            sheet.Cells[row, (int)GetExceptions.assetName].Value = dr["assetName"].ToString();
                            sheet.Cells[row, (int)GetExceptions.site].Value = dr["sSite"].ToString();
                            sheet.Cells[row, (int)GetExceptions.speed].Value = dr["maxspeed"].ToString();
                            sheet.Cells[row, (int)GetExceptions.postRoadSpeed].Value = dr["roadSpeed"].ToString();
                            //rangeDataValidation = sheet.Cells[row, (int)GetExceptions.DateTime].DataValidation;
                            //sheet.Cells[row, (int)GetExceptions.DateTime].Style.Numberformat.Format = "dd/mm/yyyy";
                            sheet.Cells[row, (int)GetExceptions.Date].Value = dr["DateOnly"].ToString();
                            sheet.Column(6).Style.Numberformat.Format = "dd/MM/yyyy";
                            sheet.Cells[row, (int)GetExceptions.Time].Value = dr["TimeOnly"].ToString();
                            sheet.Cells[row, (int)GetExceptions.address].Value = dr["addresss"].ToString();
                            sheet.Cells[row, (int)GetExceptions.duration].Value = dr["duration"].ToString();
                            sheet.Cells[row, (int)GetExceptions.zone].Value = dr["sZones"].ToString();
                            sheet.Cells[row, (int)GetExceptions.driverName].Value = dr["sDriverName"].ToString();





                            row++;
                        }
                    }

                }catch(Exception ex)
                {
                    System.IO.File.AppendAllText(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "\\Registers\\gms.dat", "\r\n Asset Creation \r\n" + ex.ToString() + "\r\n");
                }

                return package;
            }
        }
        public static ExcelPackage XcelFuel(DataTable dt, int row)
        {
            String templateDocument = Globals.ExportPath(ExportTemplate.Fuel);
            using (FileStream excelTemplate = File.OpenRead(templateDocument))
            {
                // Create Excel EPPlus Package based on template stream
                ExcelPackage package = new ExcelPackage(excelTemplate);

                // Grab the 1st sheet with the template.
                ExcelWorksheet sheet = package.Workbook.Worksheets.First();

                // Insert data into template
                if (sheet != null && dt != null)
                {
                    var rows = dt.Rows;
                    foreach (DataRow dr in rows)
                    {
                        //sheet.Cells[row, (int)GetFuels.1].Value = dr["gpsUID"].ToString();
                        //sheet.Cells[row, (int)GetFuels.AssetID].Value = dr["assetID"].ToString();
                        //sheet.Cells[row, (int)GetFuels.DateTimeGPS_UTC].Value = dr["dateTimeGPS_UTC"].ToString();
                        //sheet.Cells[row, (int)GetFuels.ConvertedValue].Value = Convert.ToInt32(dr["convertedValue"]);
                        //sheet.Cells[row, (int)GetFuels.Uom].Value = dr["uom"].ToString();
                        //sheet.Cells[row, (int)GetFuels.Ftype].Value = dr["fType"].ToString();
                        //sheet.Cells[row, (int)GetFuels.Diff].Value = dr["diff"].ToString();
                        //sheet.Cells[row, (int)GetFuels.Lon].Value = dr["lon"].ToString();
                        //sheet.Cells[row, (int)GetFuels.Lat].Value = dr["lat"].ToString();


                        sheet.Cells[row, 1].Value = dr["AssetName"].ToString();
                        sheet.Cells[row, 2].Value = dr["fType"].ToString();
                        sheet.Cells[row, 3].Value = Convert.ToInt32(dr["convertedValue"]);
                        sheet.Cells[row, 4].Value = dr["uom"].ToString();
                        sheet.Cells[row,5].Value = dr["diff"].ToString();
                        sheet.Cells[row, 6].Value = dr["dateTimeGPS_UTC"].ToString();
                     

                        row++;
                    }
                }
                return package;
            }
        }

        public static ExcelPackage XcelDailyTripTime(DataTable dt, int row)
        {

            String templateDocument = Globals.ExportPath(ExportTemplate.DailyTripTime);
            using (FileStream excelTemplate = File.OpenRead(templateDocument))
            {
                // Create Excel EPPlus Package based on template stream
                ExcelPackage package = new ExcelPackage(excelTemplate);

                // Grab the 1st sheet with the template.
                ExcelWorksheet sheet = package.Workbook.Worksheets.First();

                // Insert data into template
                if (sheet != null)
                {

                    foreach (DataRow dr in dt.Rows)
                    {
                        sheet.Cells[row, (int)GetDailyTripTimeXlsxColumns.AssetName].Value = dr["sDesc"].ToString();

                        int countColumn = 0;
                        foreach (DataColumn dc in dt.Columns)
                        {
                            string colData = dc.ColumnName.ToString();
                            countColumn++;
                            if (countColumn > 0 && colData != "sDesc" && colData != "rowNum")
                            {

                                sheet.Cells[1, countColumn].Value = colData;
                                sheet.Cells[row, countColumn].Value = dr[colData].ToString();
                            }
                        }

                        row++;
                    }
                }
                return package;
            }

        }

        public static ExcelPackage XcelZoneVisited(apiZoneVisited lZoneVisited, int row)
        {

            String templateDocument = Globals.ExportPath(ExportTemplate.ZoneVisited);
            using (FileStream excelTemplate = File.OpenRead(templateDocument))
            {
                // Create Excel EPPlus Package based on template stream
                ExcelPackage package = new ExcelPackage(excelTemplate);

                // Grab the 1st sheet with the template.
                ExcelWorksheet sheet = package.Workbook.Worksheets.First();

                // Insert data into template
                if (sheet != null)
                {

                    //var rows = dt.Rows;
                    foreach (var dr in lZoneVisited.zoneData)
                    {
                        //string date = dr.date.ToString("dd/MM/yyyy");
                        sheet.Cells[row, (int)GetZoneVisited.date].Value = dr.date;
                        sheet.Cells[row, (int)GetZoneVisited.assetName].Value = dr.asset;
                        sheet.Cells[row, (int)GetZoneVisited.zone].Value = dr.zone;
                        sheet.Cells[row, (int)GetZoneVisited.arrivalTime].Value = dr.arrivalTime;
                        sheet.Cells[row, (int)GetZoneVisited.depatureTime].Value = dr.depatureTime;
                        sheet.Cells[row, (int)GetZoneVisited.durationInZones].Value = dr.durationInZones;


                        row++;
                    }
                }
                return package;
            }

        }

        public static ExcelPackage XcelTemperature(NaveoService.Models.DebugData debugData, int row)
        {


            DbugData dd = new DbugData();
            List<apiDebugData> l = new List<apiDebugData>();
            DataTable dt = debugData.dtData;
            dt.Columns["EngineOn"].ColumnName = "EngineOn";
            foreach (DataRow dr in dt.Rows)
            {
                MinifiedAsset asset = new MinifiedAsset
                {
                    assetID = Convert.ToInt32(dr["assetID"]),
                    description = dr["assetName"].ToString()
                };

                MinifiedDriver driver = new MinifiedDriver
                {
                    driverID = Convert.ToInt32(dr["driverID"]),
                    description = dr["sDriverName"].ToString()
                };

                List<details> ldetails = new List<details>();
                String[] sDetails = dr["details"].ToString().Split('|');
                foreach (String d in sDetails)
                {
                    String[] s = d.Split(':');
                    ldetails.Add(new details
                    {
                        type = s[0].Trim(),
                        value = s[1].Trim().Length == 0 ? "1" : s[1].Trim(),
                        Unit = s[2].Trim()
                    });
                }

                double convertedSpeed = Math.Round(Convert.ToDouble(dr["speed"]), 0);
                //TimeSpan time = TimeSpan.FromSeconds(Convert.ToDouble(dr["tripTime"]));
                //string tripTime = time.ToString(@"hh\:mm\:ss");

                apiDebugData api = new apiDebugData
                {
                    asset = asset,
                    driver = driver,
                    rowNum = Convert.ToInt32(dr["rowNum"]),
                    uid = Convert.ToInt32(dr["uid"]),
                    dateTimeGPS_UTC = Convert.ToDateTime(dr["localTime"]),
                    dateTimeServer = Convert.ToDateTime(dr["dateTimeServer"]),
                    longitude = Convert.ToDouble(dr["longitude"]),
                    latitude = Convert.ToDouble(dr["latitude"]),
                    longLatValidFlag = Convert.ToInt32(dr["longLatValidFlag"]),
                    speed = Convert.ToInt32(convertedSpeed),
                    engineOn = Convert.ToInt32(dr["EngineOn"]),
                    stopFlag = Convert.ToInt32(dr["stopFlag"]),
                    tripDistance = Convert.ToDouble(dr["tripDistance"]),
                    tripTime = dr["tripTime"].ToString(),
                    workHour = Convert.ToInt32(dr["workHour"]),
                    roadSpeed = dr["roadSpeed"].ToString().Trim().Length == 0 ? (int?)null : Convert.ToInt32(dr["roadSpeed"]),
                    ldetails = ldetails
                };

                l.Add(api);
                //dd.debugData = l;
            }
            String templateDocument = Globals.ExportPath(ExportTemplate.Temperature);
            using (FileStream excelTemplate = File.OpenRead(templateDocument))
            {
                // Create Excel EPPlus Package based on template stream
                ExcelPackage package = new ExcelPackage(excelTemplate);

                // Grab the 1st sheet with the template.
                ExcelWorksheet sheet = package.Workbook.Worksheets.First();

                // Insert data into template
                if (sheet != null && debugData.dtData != null)
                {

                    int countColumn = 0;
                    foreach (var ll in l)
                    {
                        countColumn++;
                        string dddd = ll.dateTimeGPS_UTC.ToString();
                        int ssss = ll.speed;
                        sheet.Cells[row, (int)GetTemperature.asset].Value = ll.asset.description.ToString();
                        sheet.Cells[row, (int)GetTemperature.driver].Value = ll.driver.description;
                        sheet.Cells[row, (int)GetTemperature.date].Value = dddd;
                        sheet.Cells[row, (int)GetTemperature.speed].Value = ssss;


                        foreach (var xx in ll.ldetails)
                        {

                            if (xx.type == "Temperature")
                            {




                                sheet.Cells[1, 5].Value = xx.type;
                                sheet.Cells[row, 5].Value = xx.value;


                            }
                            else if (xx.type == "Temperature 2")
                            {

                                sheet.Cells[1, 6].Value = xx.type;
                                sheet.Cells[row, 6].Value = xx.value;

                            }
                            else if (xx.type == "Temperature 3")
                            {

                                sheet.Cells[1, 7].Value = xx.type;
                                sheet.Cells[row, 7].Value = xx.value;

                            }

                        }

                        row++;
                    }

                }
                return package;
            }
        }

        public static ExcelPackage XcelSensorData(NaveoService.Models.DebugData debugData, int row)
        {


            DbugData dd = new DbugData();
            List<apiDebugData> l = new List<apiDebugData>();
            DataTable dt = debugData.dtData;
            dt.Columns["EngineOn"].ColumnName = "EngineOn";
            foreach (DataRow dr in dt.Rows)
            {
                MinifiedAsset asset = new MinifiedAsset
                {
                    assetID = Convert.ToInt32(dr["assetID"]),
                    description = dr["assetName"].ToString()
                };

                MinifiedDriver driver = new MinifiedDriver
                {
                    driverID = Convert.ToInt32(dr["driverID"]),
                    description = dr["sDriverName"].ToString()
                };

                List<details> ldetails = new List<details>();
                String[] sDetails = dr["details"].ToString().Split('|');
                foreach (String d in sDetails)
                {
                    String[] s = d.Split(':');
                    ldetails.Add(new details
                    {
                        type = s[0].Trim(),
                        value = s[1].Trim().Length == 0 ? "1" : s[1].Trim(),
                        Unit = s[2].Trim()
                    });
                }

                double convertedSpeed = Math.Round(Convert.ToDouble(dr["speed"]), 0);
                //TimeSpan time = TimeSpan.FromSeconds(Convert.ToDouble(dr["tripTime"]));
                //string tripTime = time.ToString(@"hh\:mm\:ss");

                apiDebugData api = new apiDebugData
                {
                    asset = asset,
                    driver = driver,
                    rowNum = Convert.ToInt32(dr["rowNum"]),
                    uid = Convert.ToInt32(dr["uid"]),
                    dateTimeGPS_UTC = Convert.ToDateTime(dr["localTime"]),
                    dateTimeServer = Convert.ToDateTime(dr["dateTimeServer"]),
                    longitude = Convert.ToDouble(dr["longitude"]),
                    latitude = Convert.ToDouble(dr["latitude"]),
                    longLatValidFlag = Convert.ToInt32(dr["longLatValidFlag"]),
                    speed = Convert.ToInt32(convertedSpeed),
                    engineOn = Convert.ToInt32(dr["EngineOn"]),
                    stopFlag = Convert.ToInt32(dr["stopFlag"]),
                    tripDistance = Convert.ToDouble(dr["tripDistance"]),
                    tripTime = dr["tripTime"].ToString(),
                    workHour = Convert.ToInt32(dr["workHour"]),
                    roadSpeed = dr["roadSpeed"].ToString().Trim().Length == 0 ? (int?)null : Convert.ToInt32(dr["roadSpeed"]),
                    ldetails = ldetails
                };

                l.Add(api);
                //dd.debugData = l;
            }
            String templateDocument = Globals.ExportPath(ExportTemplate.SensorData);
            using (FileStream excelTemplate = File.OpenRead(templateDocument))
            {
                // Create Excel EPPlus Package based on template stream
                ExcelPackage package = new ExcelPackage(excelTemplate);

                // Grab the 1st sheet with the template.
                ExcelWorksheet sheet = package.Workbook.Worksheets.First();

                // Insert data into template
                if (sheet != null && debugData.dtData != null)
                {

                    int countColumn = 0;
                    foreach (var ll in l)
                    {
                        countColumn++;
                        string dddd = ll.dateTimeGPS_UTC.ToString();
                        int ssss = ll.speed;
                        double lat = ll.latitude;
                        double lon = ll.longitude;
                        sheet.Cells[row, (int)GetSensorData.uid].Value = ll.uid;
                        sheet.Cells[row, (int)GetSensorData.Asset].Value = ll.asset.description.ToString();
                        sheet.Cells[row, (int)GetSensorData.AssetId].Value = ll.asset.assetID;
                        sheet.Cells[row, (int)GetSensorData.Driver].Value = ll.driver.description.ToString();
                        sheet.Cells[row, (int)GetSensorData.Date].Value = dddd;
                        sheet.Cells[row, (int)GetSensorData.Longitude].Value = lon;
                        sheet.Cells[row, (int)GetSensorData.Lattitude].Value = lat;
                        sheet.Cells[row, (int)GetSensorData.LonLatValidFlag].Value = ll.longLatValidFlag;
                        sheet.Cells[row, (int)GetSensorData.RoadSpeed].Value = ll.roadSpeed;
                        sheet.Cells[row, (int)GetSensorData.Speed].Value = ll.speed;
                        sheet.Cells[row, (int)GetSensorData.StopFlag].Value = ll.stopFlag;
                        sheet.Cells[row, (int)GetSensorData.TripDistance].Value = ll.tripDistance;
                        sheet.Cells[row, (int)GetSensorData.TripTime].Value = ll.tripTime;
                        sheet.Cells[row, (int)GetSensorData.WorkHour].Value = ll.workHour;



                        foreach (var xx in ll.ldetails)
                        {


                            sheet.Cells[1, 14].Value = xx.type;
                            sheet.Cells[row, 14].Value = xx.value;



                        }

                        row++;
                    }

                }
                return package;
            }
        }

        public static ExcelPackage XcelSchedulePlanningRequest(DataTable dt, string type, int row)
        {
            String templateDocument = string.Empty;
            if (type == "request")
                templateDocument = Globals.ExportPath(ExportTemplate.PlanningData);
            else if (type == "scheduled")
                templateDocument = Globals.ExportPath(ExportTemplate.ScheduledData);
            else if (type == "TerraPlanningRequest")
                templateDocument = Globals.ExportPath(ExportTemplate.TerraPlanningScheduledData);

            using (FileStream excelTemplate = File.OpenRead(templateDocument))
            {
                // Create Excel EPPlus Package based on template stream
                ExcelPackage package = new ExcelPackage(excelTemplate);

                // Grab the 1st sheet with the template.
                ExcelWorksheet sheet = package.Workbook.Worksheets.First();

                // Insert data into template
                if (sheet != null)
                {

                    var rows = dt.Rows;
                    foreach (DataRow dr in rows)
                    {


                        if (type == "scheduled")
                        {
                            sheet.Cells[row, (int)getScheduleData.pId].Value = dr["pId"].ToString();
                            sheet.Cells[row, (int)getScheduleData.scheduleId].Value = dr["scheduleId"].ToString();
                            //sheet.Cells[row, (int)getScheduleData.requestCode].Value = dr["requestCode"].ToString();
                            sheet.Cells[row, (int)getScheduleData.requestDate].Value = dr["requestDate"].ToString();
                            sheet.Cells[row, (int)getScheduleData.dateCreated].Value = dr["dateCreated"].ToString();
                            sheet.Cells[row, (int)getScheduleData.startTime].Value = dr["startTime"].ToString();
                            sheet.Cells[row, (int)getScheduleData.endTime].Value = dr["endTime"].ToString();
                            sheet.Cells[row, (int)getScheduleData.departure].Value = dr["departure"].ToString();
                            sheet.Cells[row, (int)getScheduleData.arrival].Value = dr["arrival"].ToString();
                            sheet.Cells[row, (int)getScheduleData.asset].Value = dr["asset"].ToString();
                            sheet.Cells[row, (int)getScheduleData.driver].Value = dr["driver"].ToString();
                            sheet.Cells[row, (int)getScheduleData.status].Value = dr["status"].ToString();
                        }

                        if (type == "request")
                        {
                            sheet.Cells[row, (int)getScheduleData.pId].Value = dr["pId"].ToString();
                            sheet.Cells[row, 2].Value = dr["requestCode"].ToString();
                            sheet.Cells[row, 3].Value = dr["requestDate"].ToString();
                            sheet.Cells[row, 4].Value = dr["dateCreated"].ToString();
                            sheet.Cells[row, 5].Value = dr["startTime"].ToString();
                            sheet.Cells[row, 6].Value = dr["endTime"].ToString();
                            sheet.Cells[row, 7].Value = dr["departure"].ToString();
                            sheet.Cells[row, 8].Value = dr["arrival"].ToString();
                            sheet.Cells[row, 9].Value = dr["status"].ToString();
                        }


                        if (type == "TerraPlanningRequest")
                        {
                            sheet.Cells[row, 1].Value = dr["planningOperationCode"].ToString();
                            sheet.Cells[row, 2].Value = dr["requestDate"].ToString();
                            sheet.Cells[row, 3].Value = dr["startTime"].ToString();
                            sheet.Cells[row, 4].Value = dr["endTime"].ToString();
                            sheet.Cells[row, 5].Value = dr["fieldCode"].ToString();
                            sheet.Cells[row, 6].Value = dr["hectare"].ToString();
                            sheet.Cells[row, 7].Value = dr["soilType"].ToString();
                            sheet.Cells[row, 8].Value = dr["operationType"].ToString();
                            sheet.Cells[row, 9].Value = dr["tooltype"].ToString();
                            sheet.Cells[row, 10].Value = dr["remarks"].ToString();
                            sheet.Cells[row, 11].Value = dr["asset"].ToString();
                            sheet.Cells[row, 12].Value = dr["driver"].ToString();

                        }
                        row++;
                    }
                }
                return package;
            }



        }

        public static ExcelPackage XcelFuelReport(DataTable dt, int row)
        {


            String templateDocument = Globals.ExportPath(ExportTemplate.FuelReport);

            using (FileStream excelTemplate = File.OpenRead(templateDocument))
            {
                // Create Excel EPPlus Package based on template stream
                ExcelPackage package = new ExcelPackage(excelTemplate);

                // Grab the 1st sheet with the template.
                ExcelWorksheet sheet = package.Workbook.Worksheets.First();

                // Insert data into template
                if (sheet != null)
                {

                    var rows = dt.Rows;
                    foreach (DataRow dr in rows)
                    {

                        sheet.Cells[row, (int)fuelreport.assetId].Value = dr["AssetID"].ToString();
                        sheet.Cells[row, (int)fuelreport.asset].Value = dr["Asset"].ToString();
                        //sheet.Cells[row, (int)getScheduleData.requestCode].Value = dr["requestCode"].ToString();
                        sheet.Cells[row, (int)fuelreport.dateLastFill].Value = dr["Date Last Fill"].ToString();
                        sheet.Cells[row, (int)fuelreport.LitresLastFill].Value = dr["No of Litres(LastFill)"].ToString();
                        sheet.Cells[row, (int)fuelreport.kmTravelled].Value = dr["KM Travelled"].ToString();
                        sheet.Cells[row, (int)fuelreport.AvgConsumptionLastFill].Value = dr["Avg Consumption Last Fill (L/100 KM)"].ToString();
                        sheet.Cells[row, (int)fuelreport.AvgConsumption].Value = dr["Avg Consumption(All Time)"].ToString();
                        sheet.Cells[row, (int)fuelreport.VechType].Value = dr["Vehicle Type"].ToString();
                        sheet.Cells[row, (int)fuelreport.make].Value = dr["Make"].ToString();
                        sheet.Cells[row, (int)fuelreport.model].Value = dr["Model"].ToString();
                        sheet.Cells[row, (int)fuelreport.drops].Value = dr["No of Possible Drops"].ToString();
                        sheet.Cells[row, (int)fuelreport.acceleration].Value = dr["No of Hash Acceleration"].ToString();


                        row++;
                    }
                }
                return package;
            }



        }

        public static ExcelPackage XcelCustomizedReport(List<CustomTrip> lcustomTripHeaders,int row,DateTime DateFrom , DateTime DateTo)
        {

            string templateDocument = Globals.ExportPath(ExportTemplate.CustomizedTripReport);

            using (FileStream excelTemplate = File.OpenRead(templateDocument))
            {
                // Create Excel EPPlus Package based on template stream
                ExcelPackage package = new ExcelPackage(excelTemplate);

                // Grab the 1st sheet with the template.
                ExcelWorksheet sheet = package.Workbook.Worksheets.First();
                int NoWorkTimerOW = 0;
                int lastRow = 0;
                //int NoWorkDonerOW = 0;
                int rowH = 1;
                // Insert data into template
                if (sheet != null)
                {
                    int rowHeader = 1;
                    int countHeader = lcustomTripHeaders.Count;
                    int colNoOPN = 0;
                    int rowNoOPN = 0;
                    int rowTime = 0;
                    int colNonWorkTime = 0;
                    int rowNonWorkTime = 0;
                    int colWorkTime = 0;
                    int rowWorkTime = 0;

                    int countDate = 1;
                    int objectCount = 1;
                    
                    foreach (var lt in lcustomTripHeaders)
                    {
                        objectCount++;
                        countDate++;
                        sheet.Cells[2, countDate].Value = lt.Date;

                        foreach (var l in lt.lCustomTripHeader1)
                        {

                            sheet.Cells[1, 1].Value = "Asset : " + l.AssetName + " , " +  "FieldCode : " + l.FieldCode;
                            

                            foreach(var ltime in l.lDetailTime)
                            {

                                //sheet.Cells[3, 1].Value = l.Title;
                                sheet.Cells[4, 1].Value = "TimeStarted";
                                sheet.Cells[5, 1].Value = "TimeEnded";
                                sheet.Cells[6, 1].Value = "Normal";
                                sheet.Cells[7, 1].Value = "LunchTeaTime";
                                sheet.Cells[8, 1].Value = "Overtime";
                                sheet.Cells[9, 1].Value = "TotalAvailableTime";



                                    sheet.Cells[3, 1].Value = l.Title;
                                    sheet.Cells[4, objectCount].Value = ltime.TimeStarted;
                                    sheet.Cells[5, objectCount].Value = ltime.TimeEnded;
                                    sheet.Cells[6, objectCount].Value = ltime.Normal;
                                    sheet.Cells[7, objectCount].Value = ltime.LunchTeaTime;
                                    sheet.Cells[8, objectCount].Value = ltime.Overtime;
                                    sheet.Cells[9, objectCount].Value = ltime.TotalAvailableTime;

                            }

                            int countNOPNrOW = 11;
                            foreach(var lNOPN in l.lDetailNoOperation)
                            {
                    
                                countNOPNrOW++;
                                sheet.Cells[11, 1].Value = l.Title;

                                sheet.Cells[countNOPNrOW, 1].Value = lNOPN.NoOperationType;
                                sheet.Cells[countNOPNrOW, objectCount].Value = lNOPN.Value;
                                NoWorkTimerOW = countNOPNrOW;

                            }



                            int NoWorkDonerOW = NoWorkTimerOW;

                            //NoWorkDonerOW = NoWorkDonerOW + 1;
                            int countforNonWork = 0;
                            foreach (var lNoWorkTime in l.lDetailNoWorkTime)
                            {
                                countforNonWork++;
                                NoWorkDonerOW++;

                                int a = NoWorkDonerOW + 1;
                                
                                a++;


                                if(countforNonWork ==1)
                                {
                                    sheet.Cells[a, 1].Value = l.Title;
                                    a = a + 1;
                                    NoWorkDonerOW++;
                                }
                                    

                                
                                sheet.Cells[a, 1].Value = lNoWorkTime.NoWorkTimeType;



                                sheet.Cells[a, objectCount].Value = lNoWorkTime.Value;
                                lastRow = a;


                            }

                            string d = string.Empty;
                            int WorkDoneRow = lastRow;
                            int countDetailWorkDone = 0;
                            foreach (var lWorkTime in l.lDetailWorkDone)
                            {
                                countDetailWorkDone++;
                                WorkDoneRow++;
                                int a = WorkDoneRow + 1;
                                a++;

                                //int b = a - 1;
                                sheet.Cells[a , 1].Value = l.Title;
                                

                                    
                                sheet.Cells[a, 1].Value = lWorkTime.WorkDoneType;
                                sheet.Cells[a, objectCount].Value = lWorkTime.Value;

                            }


                        }
                    }

                }
                return package;
            }
        }

        public static ExcelPackage XcelTripHistoryByDataLogs(DataTable dt, int row)
        {

             ExcelPackage package = new ExcelPackage();

                // Grab the 1st sheet with the template.
                ExcelWorksheet sheet = package.Workbook.Worksheets.Add("Naveo");
                sheet = package.Workbook.Worksheets.First();

            List<string> lCols = new List<string>();
            
            // Insert data into template
            if (sheet != null)
                {

                //var rows = dt.Rows;
                var rows = dt.Rows;
                foreach (DataRow dr in rows)
                {
                    int countColumn = 0;
                    foreach (DataColumn dc in dt.Columns)
                    {
                        countColumn++;
                        if (countColumn > 0)
                        {
                            string colheaderName = dc.ColumnName.ToString();
                            string coldata = dr[colheaderName].ToString();

                            if (colheaderName == "RowNum")
                            {
                                
                                sheet.Cells[1, countColumn].Value = "No";
                                sheet.Cells[row, countColumn].Value = coldata;

                            }
                            if (colheaderName == "assetName")
                            {
                                sheet.Cells[1, 2].Value = "Asset"; 
                                sheet.Cells[row, 2].Value = coldata;
                                    
                            }
                            if (colheaderName == "sDriverName")
                            {
                                sheet.Cells[1, 3].Value = "Driver";
                                sheet.Cells[row, 3].Value = coldata;

                            }
                            
                            if (colheaderName == "DateTimeServer")
                            {
                                sheet.Cells[1, 4].Value = "Date";
                                sheet.Cells[row, 4].Value = coldata;

                            }
                            if (colheaderName == "Longitude")
                            {
                                sheet.Cells[1, 5].Value = colheaderName;
                                sheet.Cells[row, 5].Value = coldata;

                            }
                            if (colheaderName == "Latitude")
                            {
                                sheet.Cells[1, 6].Value = colheaderName;
                                sheet.Cells[row, 6].Value = coldata;

                            }
                             if (colheaderName == "LongLatValidFlag")
                            {
                                sheet.Cells[1, 7].Value = colheaderName;
                                sheet.Cells[row, 7].Value = coldata;

                            }

                            if (colheaderName == "RoadSpeed")
                            {
                                sheet.Cells[1, 8].Value = colheaderName;
                                sheet.Cells[row, 8].Value = coldata;

                            }

                            if (colheaderName == "Speed")
                            {
                                sheet.Cells[1, 9].Value = colheaderName;
                                sheet.Cells[row, 9].Value = coldata;

                            }

                            if (colheaderName == "Speed")
                            {
                                sheet.Cells[1, 9].Value = colheaderName;
                                sheet.Cells[row, 9].Value = coldata;

                            }

                            if (colheaderName == "TripDistance")
                            {
                                sheet.Cells[1, 10].Value = colheaderName;
                                sheet.Cells[row, 10].Value = coldata;

                            }

                            if (colheaderName == "TripTime")
                            {
                                sheet.Cells[1, 11].Value = colheaderName;
                                sheet.Cells[row, 11].Value = coldata;

                            }


                            if (colheaderName == "details")
                            {
                                sheet.Cells[1, 12].Value = "details";
                                sheet.Cells[row, 12].Value = coldata;

                                //String[] sDetails = coldata.Split('|');
                                //int countDynamic = 11;
                                //foreach (var x in sDetails)
                                //{
                                //    countDynamic++;
                                //    string o = x + ">";
                                //    if( x != string.Empty)
                                //    {
                                //        String[] detailsParts = x.Split(':');
                                //        string col = detailsParts[0];
                                //        string value = detailsParts[1] + " " + detailsParts[2];



                                //        sheet.Cells[1, countDynamic].Value = "details";
                                //        sheet.Cells[row, countDynamic].Value = value;

                                //    }

                                //List<details> ldetails = new List<details>();
                                //String[] sDetails = dr["details"].ToString().Split('|');
                                //foreach (String d in sDetails)
                                //{
                                //    String[] s = d.Split(':');
                                //    ldetails.Add(new details
                                //    {
                                //        type = s[0].Trim(),
                                //        value = s[1].Trim().Length == 0 ? "1" : s[1].Trim(),
                                //        Unit = s[2].Trim()
                                //    });
                                //}


                                //string a = string.Empty;
                                //List<string> header = new List<string>();

                                //foreach(var aaa in ldetails)
                                //{
                                //    if(header.IndexOf(aaa.type) < 0)
                                //    {
                                //        header.Add(aaa.type);
                                //    }
                                //}

                            }



                            if (colheaderName == "WorkHour")
                            {
                                sheet.Cells[1, 13].Value = colheaderName;
                                sheet.Cells[row, 13].Value = coldata;

                            }


                            //if (colData == "TripDistance")
                            //{
                            //    decimal tD = Convert.ToDecimal(dr[colData]);
                            //    dr[colData] = Math.Round(tD, 2);
                            //}


                            //sheet.Cells[row, countColumn].Value = dr[colData].ToString();
                        }
                    }
                    row++;
                }
            }


                return package;
            

        }

        public static ExcelPackage XcelAuxReport(DebugData debugData, int row)
        {

            ExcelPackage package = new ExcelPackage();
            //Auxiliary_Template
            // Grab the 1st sheet with the template.
            ExcelWorksheet sheet = package.Workbook.Worksheets.Add("Naveo");
            sheet = package.Workbook.Worksheets.First();


            // Insert data into template
            if (sheet != null)
            {

                //var rows = dt.Rows;
                var rows = debugData.dtData.Rows;
                foreach (DataRow dr in rows)
                {
                    int countColumn = 0;
                    foreach (DataColumn dc in debugData.dtData.Columns)
                    {
                        countColumn++;
                        if (countColumn > 0)
                        {
                            string colheaderName = dc.ColumnName.ToString();
                            string coldata = dr[colheaderName].ToString();


                            if (colheaderName == "AssetNumber")
                            {
                                sheet.Cells[1, 1].Value = "Asset Name";
                                sheet.Cells[row, 1].Value = coldata;

                            }
                            if (colheaderName == "Auxiliary")
                            {
                                sheet.Cells[1, 2].Value = colheaderName;
                                sheet.Cells[row, 2].Value = coldata;

                            }
                            if (colheaderName == "AuxiliaryDescription")
                            {
                                sheet.Cells[1, 3].Value = "Auxiliary Description";
                                sheet.Cells[row, 3].Value = coldata;

                            }

                            if (colheaderName == "DateTimeON")
                            {
                                sheet.Cells[1, 4].Value = "Date Time On";
                                sheet.Cells[row, 4].Value = coldata;

                            }
                            if (colheaderName == "DateTimeOff")
                            {
                                sheet.Cells[1, 5].Value = "Date Time Off";
                                sheet.Cells[row, 5].Value = coldata;

                            }
                            if (colheaderName == "Duration")
                            {
                                sheet.Cells[1, 6].Value = colheaderName;
                                sheet.Cells[row, 6].Value = coldata;

                            }
                            if (colheaderName == "address")
                            {
                                sheet.Cells[1, 7].Value = "Address";
                                sheet.Cells[row, 7].Value = coldata;

                            }

                            if (colheaderName == "Zone")
                            {
                                sheet.Cells[1, 8].Value = colheaderName;
                                sheet.Cells[row, 8].Value = coldata;

                            }

                            if (colheaderName == "sDriverName")
                            {
                                sheet.Cells[1, 9].Value = "Driver Name";
                                sheet.Cells[row, 9].Value = coldata;

                            }



                        }


                    }


                    row++;
                }
            }


            return package;


        }

        public static ExcelPackage XcelShift(DataTable dt, int row)
        {

            String templateDocument = Globals.ExportPath(ExportTemplate.Shift);
            using (FileStream excelTemplate = File.OpenRead(templateDocument))
            {
                // Create Excel EPPlus Package based on template stream
                ExcelPackage package = new ExcelPackage(excelTemplate);

                // Grab the 1st sheet with the template.
                ExcelWorksheet sheet = package.Workbook.Worksheets.First();

                // Insert data into template
                if (sheet != null)
                {

                    //var rows = dt.Rows;
                    foreach (DataRow dr in dt.Rows)
                    {
                        //string date = dr.date.ToString("dd/MM/yyyy");
                        DateTime dtFrom = Convert.ToDateTime(dr["TimeFr"].ToString());
                        DateTime dtTo = Convert.ToDateTime(dr["TimeTo"].ToString());
                        sheet.Cells[row, 1].Value = dr["sName"].ToString();
                        sheet.Cells[row, 2].Value = dtFrom.ToShortTimeString();
                        sheet.Cells[row, 3].Value = dtTo.ToShortTimeString();
                        sheet.Cells[row, 4].Value = dr["sComments"].ToString();
          

                        row++;
                    }
                }
                return package;
            }

        }
        public static ExcelPackage XcelRoster(DataTable dt, int row)
        {

            String templateDocument = Globals.ExportPath(ExportTemplate.Roster);
            using (FileStream excelTemplate = File.OpenRead(templateDocument))
            {
                // Create Excel EPPlus Package based on template stream
                ExcelPackage package = new ExcelPackage(excelTemplate);

                // Grab the 1st sheet with the template.
                ExcelWorksheet sheet = package.Workbook.Worksheets.First();

                // Insert data into template
                if (sheet != null)
                {

                    //var rows = dt.Rows;
                    foreach (DataRow dr in dt.Rows)
                    {
                        //string date = dr.date.ToString("dd/MM/yyyy");
    
               
                        sheet.Cells[row, 1].Value = dr["RosterPeriod"].ToString();
                        sheet.Cells[row, 2].Value = dr["RosterDate"].ToString();
                        sheet.Cells[row, 3].Value = dr["RosterName"].ToString();
                        sheet.Cells[row, 4].Value = dr["Asset"].ToString();
                        sheet.Cells[row, 5].Value = dr["ResoourceName"].ToString();
                        sheet.Cells[row, 6].Value = dr["sRemarks"].ToString();

                        row++;
                    }
                }
                return package;
            }

        }
        public static ExcelPackage XcelPlanningExceptions(DataTable dt, int row)
        {

            String templateDocument = Globals.ExportPath(ExportTemplate.PlanningExceptions);
            using (FileStream excelTemplate = File.OpenRead(templateDocument))
            {
                // Create Excel EPPlus Package based on template stream
                ExcelPackage package = new ExcelPackage(excelTemplate);

                // Grab the 1st sheet with the template.
                ExcelWorksheet sheet = package.Workbook.Worksheets.First();

                // Insert data into template
                if (sheet != null)
                {

                    //var rows = dt.Rows;
                    foreach (DataRow dr in dt.Rows)
                    {
              


                        sheet.Cells[row, 1].Value = dr["RuleName"].ToString();
                        sheet.Cells[row, 2].Value = dr["Asset"].ToString();
                        sheet.Cells[row, 3].Value = dr["Driver"].ToString();
                        sheet.Cells[row, 4].Value = dr["ViaPoint"].ToString();
                        sheet.Cells[row, 5].Value = Convert.ToDateTime(dr["ExpectedTime"].ToString());
                        sheet.Cells[row, 6].Value = dr["Buffer"].ToString();
                        sheet.Cells[row, 7].Value = Convert.ToDateTime(dr["ActualTime"].ToString());
                        sheet.Cells[row, 8].Value = dr["Durations"].ToString();

                        row++;
                    }
                }
                return package;
            }

        }


        public static ExcelPackage XcelFuelExceptions(DataTable dt, int row)
        {

            String templateDocument = Globals.ExportPath(ExportTemplate.FuelExceptions);
            using (FileStream excelTemplate = File.OpenRead(templateDocument))
            {
                // Create Excel EPPlus Package based on template stream
                ExcelPackage package = new ExcelPackage(excelTemplate);

                // Grab the 1st sheet with the template.
                ExcelWorksheet sheet = package.Workbook.Worksheets.First();

                // Insert data into template
                if (sheet != null)
                {

                    //var rows = dt.Rows;
                    foreach (DataRow dr in dt.Rows)
                    {



                        sheet.Cells[row, 1].Value = dr["RuleName"].ToString();
                        sheet.Cells[row, 2].Value = dr["Asset"].ToString();
                        sheet.Cells[row, 3].Value = dr["DateTimeRule"].ToString();
                        sheet.Cells[row, 4].Value = dr["ConditionType"].ToString();
                        sheet.Cells[row, 5].Value = Convert.ToDateTime(dr["DifferenceInLitres"].ToString());
                        sheet.Cells[row, 6].Value = dr["Zone"].ToString();
                        sheet.Cells[row, 7].Value = Convert.ToDateTime(dr["address"].ToString());
           

                        row++;
                    }
                }
                return package;
            }

        }

        public static ExcelPackage XcelMaintenanceCostReport(DataTable dt, int row)
        {

            String templateDocument = Globals.ExportPath(ExportTemplate.MaintenanceCost);
            using (FileStream excelTemplate = File.OpenRead(templateDocument))
            {
                // Create Excel EPPlus Package based on template stream
                ExcelPackage package = new ExcelPackage(excelTemplate);

                // Grab the 1st sheet with the template.
                ExcelWorksheet sheet = package.Workbook.Worksheets.First();

                // Insert data into template
                if (sheet != null)
                {

                    //var rows = dt.Rows;
                    foreach (DataRow dr in dt.Rows)
                    {



                        sheet.Cells[row, 1].Value = dr["MaintId"].ToString();
                        sheet.Cells[row, 2].Value = dr["AssetId"].ToString();
                        sheet.Cells[row, 3].Value = dr["AssetName"].ToString();
                        sheet.Cells[row, 4].Value = dr["Maintenance_Type"].ToString();
                        sheet.Cells[row, 5].Value = dr["Description"].ToString();
                        sheet.Cells[row, 6].Value = Convert.ToDateTime(dr["StartDate"].ToString());
                        sheet.Cells[row, 7].Value = dr["EndDate"].ToString();
                        sheet.Cells[row, 8].Value = dr["AssetStatus"].ToString();
                        sheet.Cells[row, 9].Value = Convert.ToDateTime(dr["CostExVAT"].ToString());
                        sheet.Cells[row, 10].Value = Convert.ToDateTime(dr["VATAmount"].ToString());
                        sheet.Cells[row, 11].Value = Convert.ToDateTime(dr["TotalCost"].ToString());
                    
                    
                        row++;
                    }
                }
                return package;
            }

        }

        #endregion

        #region PDF
        public static DataSet PDFTrip(NaveoService.Models.TripHeader tH)
        {
            DataTable dt = new DataTable("dtTrips");
            DataSet ds = new DataSet();
            //dt.Columns.Add("iColorDZ");
            //dt.Columns.Add("iColor");
            dt.Columns.Add("Asset");
            dt.Columns.Add("Departure Address");
            dt.Columns.Add("iColorDZ");
            dt.Columns.Add("Departure Zone");
            dt.Columns.Add("Arrival Address");
            dt.Columns.Add("iColor");
            dt.Columns.Add("Arrival Zone");
            dt.Columns.Add("Start");
            dt.Columns.Add("End");
            dt.Columns.Add("Max Speed");
            dt.Columns.Add("Trip Distance");
            dt.Columns.Add("Trip Time");
            dt.Columns.Add("Stop Time");
            dt.Columns.Add("Idle Time");
            dt.Columns.Add("Driver");
            dt.Columns.Add("Alerts");



            if ( tH != null)
                {
                    try
                    {

                        var rows = tH.dtTrips.Rows;
                        var rowTotals = tH.dtTotals.Rows;

                        foreach (DataRow dataRow in rows)
                        {
                        DataRow toInsert = dt.NewRow();

                        string from = Convert.ToDateTime(dataRow["From"]).ToString("dd/MM/yy hh:mm:tt");
                        string to = Convert.ToDateTime(dataRow["To"]).ToString("dd/MM/yy hh:mm:tt ");

                        toInsert["iColorDZ"] = dataRow["iColorDZ"].ToString();
                        toInsert["iColor"] = dataRow["iColor"].ToString();
                        toInsert["Asset"] = dataRow["Vehicle"];
                        toInsert["Departure Address"] = dataRow["DepartureAddress"];
                        toInsert["Departure Zone"] = dataRow["DepartureZone"];
                        toInsert["Arrival Address"] = dataRow["ArrivalAddress"];
                        toInsert["Arrival Zone"] = dataRow["ArrivalZone"];
                        toInsert["Start"] = from;
                        toInsert["End"] = to;
                        toInsert["Max Speed"] = dataRow["MaxSpeed"].ToString();
                        toInsert["Trip Distance"] = dataRow["TripDistance"].ToString();
                        toInsert["Trip Time"] = dataRow["TripTime"].ToString();
                        toInsert["Stop Time"] = dataRow["StopTime"].ToString();

                        toInsert["Idle Time"] = dataRow["IdlingTime"].ToString();
                        toInsert["Driver"] = dataRow["Driver"].ToString();
                        toInsert["Alerts"] = dataRow["exceptionsCnt"].ToString();
       

                        dt.Rows.Add(toInsert);


                    
                            //int i = 0;
                            //if (int.TryParse(dataRow["iColorDZ"].ToString(), out i))
                            //{
                            //    sheet.Cells[row, (int)TripsXlsxColumns.DepartureZone].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //    sheet.Cells[row, (int)TripsXlsxColumns.DepartureZone].Style.Fill.BackgroundColor.SetColor(ConvertToColor(i));
                            //    sheet.Cells[row, (int)TripsXlsxColumns.DepartureZone].Value = dataRow["DepartureZone"];
                            //}

                            //if (int.TryParse(dataRow["iColor"].ToString(), out i))
                            //{
                            //    sheet.Cells[row, (int)TripsXlsxColumns.ArrivalZone].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //    sheet.Cells[row, (int)TripsXlsxColumns.ArrivalZone].Style.Fill.BackgroundColor.SetColor(ConvertToColor(i));
                            //    sheet.Cells[row, (int)TripsXlsxColumns.ArrivalZone].Value = dataRow["ArrivalZone"];
                            //}

                

                        }

                        //TotalBorderStyle(sheet, row, (int)TripsXlsxColumns.TripTime);
                        //TotalBorderStyle(sheet, row, (int)TripsXlsxColumns.TripDistance);
                        //TotalBorderStyle(sheet, row, (int)TripsXlsxColumns.StopTime);
                        //TotalBorderStyle(sheet, row, (int)TripsXlsxColumns.IdleTime);
                        //TotalBorderStyle(sheet, row, (int)TripsXlsxColumns.MaxSpeed);
                        //TotalBorderStyle(sheet, row, (int)TripsXlsxColumns.Acceleration);
                        //TotalBorderStyle(sheet, row, (int)TripsXlsxColumns.Brake);
                        //TotalBorderStyle(sheet, row, (int)TripsXlsxColumns.Corner);
                        // TotalBorderStyle(sheet, row, (int)TripsXlsxColumns.SpeedingTime1);
                        // TotalBorderStyle(sheet, row, (int)TripsXlsxColumns.SpeedingTime2);
                        //TotalBorderStyle(sheet, row, (int)TripsXlsxColumns.SpeedingTime1);
                        //TotalBorderStyle(sheet, row, (int)TripsXlsxColumns.SpeedingTime1);


                        //Totals
                        //foreach (DataRow dataRowTotal in rowTotals)
                        //{
                        //    sheet.Cells[row, (int)TripsXlsxColumns.TripTime].Value = dataRowTotal["totalTripTime"].ToString();
                        //    sheet.Cells[row, (int)TripsXlsxColumns.TripDistance].Value = dataRowTotal["totalTripDistance"].ToString();
                        //    sheet.Cells[row, (int)TripsXlsxColumns.MaxSpeed].Value = dataRowTotal["maxSpeed"].ToString();
                        //    sheet.Cells[row, (int)TripsXlsxColumns.StopTime].Value = dataRowTotal["totalStopTime"].ToString();
                        //    sheet.Cells[row, (int)TripsXlsxColumns.IdleTime].Value = dataRowTotal["totalIdlingTime"].ToString();
                        //}




                    }
                    catch
                    {

                    }
                }

              DataTable dtTotals = tH.dtTotals.Copy();

               ds.Tables.Add(dt);
               //ds.AcceptChanges();
               ds.Tables.Add(dtTotals);

            return ds;
            
        }

        public static DataSet PDFlive(List<GPSData> lGPSData)
        {
            DataTable dt = new DataTable("dtLive");
            DataSet ds = new DataSet();

            dt.Columns.Add("Status");
            dt.Columns.Add("AssetID");
            dt.Columns.Add("Asset");
            dt.Columns.Add("Driver");
            dt.Columns.Add("Zone");
            dt.Columns.Add("Speed");
            dt.Columns.Add("Latitude");
            dt.Columns.Add("Longitude");
            dt.Columns.Add("Address");
            dt.Columns.Add("Last Reported Date");
            dt.Columns.Add("Time");
            dt.Columns.Add("GPS Date");
            dt.Columns.Add("Fuel(L)");

            if( lGPSData.Count != 0 )
            {

                try
                {
                   
                    foreach(GPSData item in lGPSData)
                    {
                        DataRow toInsert = dt.NewRow();

                        if (item.Availability != "Suspect||No access to asset")
                        {
                            String[] sParts = item.Availability.Split('|');
                            string Availability = sParts[0];

                            string zone = string.Empty;
                            if (sParts[1] != string.Empty)
                            {
                                String[] sZone = sParts[1].Split(':');
                                zone = sZone[1];
                            }

                            string address = sParts[2];

                            string driver = String.Empty;
                            if (sParts.Length > 3)
                                driver = sParts[3];

                            String[] sZones = sParts[1].Split(':');
                            string zoneName = string.Empty;

                            if (sZones.Length > 1)
                                zoneName = sZones[1];

                            string status = sParts[0];
                            if (status == "Stop" && zoneName != string.Empty)
                                Availability = "Stopped inside zone";

                            if (Availability == "Heading")
                                Availability = "Driving";




                            Double NoOfDays = (DateTime.Now - (DateTime)item.LastReportedInLocalTime).TotalDays;
                            int x = (int)Math.Round(NoOfDays);
                            toInsert["Status"] = Availability;
                            toInsert["AssetID"] = item.AssetID;
                            toInsert["Asset"] = item.AssetNum;//d.sDriverName;
                            toInsert["Driver"] = driver;
                            toInsert["Zone"] = zone;
                            toInsert["Speed"] = item.Speed;
                            toInsert["Latitude"] = item.Latitude;
                            toInsert["Longitude"] = item.Longitude;
                            toInsert["Address"] = address;
                            toInsert["Last Reported Date"] = item.LastReportedInLocalTime.ToString("dd/MM/yyyy HH:mm"); ; //x + " Days";
                            toInsert["Time"] = item.LastReportedInLocalTime.ToString("HH:mm");                                                                                  //sheet.Cells[row, (int)LiveDataXlsxColumns.Time].Value = item.LastReportedInLocalTime.ToString("HH:mm");
                            toInsert["GPS Date"] = item.dtLocalTime.ToString("dd/MM/yyyy HH:mm");

                            if(item.lastFuel != null)
                                 toInsert["Fuel(L)"] = item.lastFuel.ConvertedValue;

                            dt.Rows.Add(toInsert);
                        }


                    }
                }
                catch (Exception ex)
                {


                }


            }

            ds.Tables.Add(dt);

            return ds;

        }

        public static DataSet PDFDailySummarizedTrips(dtData dtD)
        {
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

      
            dtD.dsData.Tables["Report"].Columns["date"].ColumnName = "Date";
            dtD.dsData.Tables["Report"].Columns["registrationNo"].ColumnName = "Registration No";
            dtD.dsData.Tables["Report"].Columns["driver"].ColumnName = "Driver";
            dtD.dsData.Tables["Report"].Columns["trips"].ColumnName = "Trips";
            dtD.dsData.Tables["Report"].Columns["startTime"].ColumnName = "Start Time";
            dtD.dsData.Tables["Report"].Columns["endTime"].ColumnName = "End Time";
            dtD.dsData.Tables["Report"].Columns["kmTravelled"].ColumnName = "Km Travlled";
            dtD.dsData.Tables["Report"].Columns["idlingTime"].ColumnName = "Idling Time";
            dtD.dsData.Tables["Report"].Columns["inZoneStopTime"].ColumnName = "InZone Stop Time";
            dtD.dsData.Tables["Report"].Columns["outZoneStopTime"].ColumnName = "OutZone Stop Time";
            dtD.dsData.Tables["Report"].Columns.Remove("drivingTime");

            DataTable dtTotals = dtD.dsData.Tables["Totals"].Copy();
            ds.Tables.Add(dtTotals);
            DataTable dtReport = dtD.dsData.Tables["Report"].Copy();
            ds.Tables.Add(dtReport);


            return ds;
        }

        public static DataSet PDFSummarizedTrips(dtData dtD)
        {
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            dtD.Data.Columns["rowNum"].ColumnName = "Row Num";
            dtD.Data.Columns["registrationNo"].ColumnName = "Registration No";
            dtD.Data.Columns["driver"].ColumnName = "Driver";
            dtD.Data.Columns["MaxSpeed"].ColumnName = "Max Speed";
            dtD.Data.Columns["kmTravelled"].ColumnName = "Km Travelled";

        
            dtD.Data.Columns["stopLess30Min"].ColumnName = "Stop Less than 30 mins";
            dtD.Data.Columns["stopLess5Hrs"].ColumnName = "Stop Less than 5Hrs";
            dtD.Data.Columns["stopMore5Hrs"].ColumnName = "Stop more than 5Hrs";
            dtD.Data.Columns["trips"].ColumnName = "Trips";
            dtD.Data.Columns["drivingTime"].ColumnName = "Driving Time";
            dtD.Data.Columns["idlingTime"].ColumnName = "Idling Time";
            dtD.Data.Columns["speedMore70Kms"].ColumnName = "Speed More Than 70 Kms";
            dtD.Data.Columns["speedMore80Kms"].ColumnName = "Speed More Than 80 Kms";
            dtD.Data.Columns["speedMore110Kms"].ColumnName = "Speed More Than 110 Kms";
            dtD.Data.Columns["fuelcost"].ColumnName = "Fuel Cost";
            dtD.Data.Columns["costperkm"].ColumnName = "Cost/Km(Approx.)";
            dtD.Data.Columns["fuelinlitres"].ColumnName = "Fuel In Litres";
            dtD.Data.Columns["fuelinlitresper100km"].ColumnName = "Fuel in Litres per 100 km";
            dtD.Data.Columns["totalcost"].ColumnName = "Total Cost";
            dtD.Data.Columns["totalper100km"].ColumnName = "Total cost per 100 km";


            dtD.Data.TableName = "dtReports";
            ds.Tables.Add(dtD.Data);


            return ds;
        }

        public static DataSet PDFDailyTripTime(dtData dtD)
        {
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            dtD.Data.Columns.Remove("rowNum");
            dtD.Data.Columns["sDesc"].ColumnName = "Asset";
          
            dtD.Data.TableName = "dtReports";
            ds.Tables.Add(dtD.Data);


            return ds;
        }

        public static DataSet PDFZoneVisited(apiZoneVisited apiZone)
        {
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            DataTable dtZoneVisited = new myConverter().ListToDataTable<zoneFormat>(apiZone.zoneData);

            dtZoneVisited.Columns["date"].ColumnName = "Date";
            dtZoneVisited.Columns["asset"].ColumnName = "Asset";
            dtZoneVisited.Columns["zone"].ColumnName = "Zone";
            dtZoneVisited.Columns["type"].ColumnName = "Type";
            dtZoneVisited.Columns["arrivalTime"].ColumnName = "Arrival Time in Zone Selected";
            dtZoneVisited.Columns["depatureTime"].ColumnName = "Departure Time in Zone Selected";
            //dtZoneVisited.Columns["kmTravelled"].ColumnName = "Duration In Zone";
            dtZoneVisited.Columns["durationInZones"].ColumnName = "Duration In Zone";
            //dtZoneVisited.Columns["kmTravelled"].ColumnName = "Duration In Zone";
            dtZoneVisited.Columns.Remove("zoneId");
            dtZoneVisited.Columns.Remove("color");
            dtZoneVisited.Columns.Remove("zoneIds");

            dtZoneVisited.TableName = "dtZoneVisited";

            ds.Tables.Add(dtZoneVisited);

            return ds;
        }


        public static DataSet PDFExceptions(DataSet ds,Boolean bAddress)
        {





            if (bAddress)
            {
                ds.Tables["dtExceptionHeader"].Columns.Add("Address");
                DataRow[] drAddress = ds.Tables["dtExceptionHeader"].Select();
                SimpleAddress.GetAddresses(drAddress, "Address", "lon", "lat");
            }


            ds.Tables["dtExceptionHeader"].Columns["ruleName"].SetOrdinal(0);
            ds.Tables["dtExceptionHeader"].Columns["assetName"].SetOrdinal(1);
            ds.Tables["dtExceptionHeader"].Columns["sSite"].SetOrdinal(2);
            ds.Tables["dtExceptionHeader"].Columns["maxSpeed"].SetOrdinal(3);
            ds.Tables["dtExceptionHeader"].Columns["roadSpeed"].SetOrdinal(4);
            ds.Tables["dtExceptionHeader"].Columns["dateFrom"].SetOrdinal(5);
            ds.Tables["dtExceptionHeader"].Columns["Address"].SetOrdinal(6);
            ds.Tables["dtExceptionHeader"].Columns["duration"].SetOrdinal(7);
            ds.Tables["dtExceptionHeader"].Columns["sZones"].SetOrdinal(8);
            ds.Tables["dtExceptionHeader"].Columns["sDriverName"].SetOrdinal(9);



            // ds.Tables["dtExceptionHeader"].ColumnName = "Asset";
            //dt.Columns["zone"].ColumnName = "Zone";
            //dt.Columns["type"].ColumnName = "Type";
            //dt.Columns["arrivalTime"].ColumnName = "Arrival Time in Zone Selected";
            //dt.Columns["depatureTime"].ColumnName = "Departure Time in Zone Selected";
            ////dtZoneVisited.Columns["kmTravelled"].ColumnName = "Duration In Zone";
            //dt.Columns["durationInZones"].ColumnName = "Duration In Zone";
            ////dtZoneVisited.Columns["kmTravelled"].ColumnName = "Duration In Zone";
            //dt.Columns.Remove("zoneId");
            //dt.Columns.Remove("color");
            //dt.Columns.Remove("zoneIds");

            ds.Tables["dtExceptionHeader"].Columns["ruleName"].ColumnName = "Rule Name";
            ds.Tables["dtExceptionHeader"].Columns["assetName"].ColumnName = "Asset";
            ds.Tables["dtExceptionHeader"].Columns["sSite"].ColumnName = "Site";
            ds.Tables["dtExceptionHeader"].Columns["maxSpeed"].ColumnName = "Speed(km/h)";
            ds.Tables["dtExceptionHeader"].Columns["roadSpeed"].ColumnName = "Road Speed";
            ds.Tables["dtExceptionHeader"].Columns["dateFrom"].ColumnName = "Date From";
            ds.Tables["dtExceptionHeader"].Columns["Address"].ColumnName = "Address";
            ds.Tables["dtExceptionHeader"].Columns["duration"].ColumnName = "Duration";
            ds.Tables["dtExceptionHeader"].Columns["sZones"].ColumnName = "Zone";
            ds.Tables["dtExceptionHeader"].Columns["sDriverName"].ColumnName = "Driver Name";
            ds.Tables["dtExceptionHeader"].Columns["DateOnly"].ColumnName = "Date";
            ds.Tables["dtExceptionHeader"].Columns["TimeOnly"].ColumnName = "Time";
            ds.Tables["dtExceptionHeader"].Columns["uid"].ColumnName = "UID";
            ds.Tables["dtExceptionHeader"].Columns.Remove("dateTo");
            ds.Tables["dtExceptionHeader"].Columns.Remove("groupID");
            ds.Tables["dtExceptionHeader"].Columns.Remove("dtLocalFrom");
            ds.Tables["dtExceptionHeader"].Columns.Remove("dtLocalTo");
            ds.Tables["dtExceptionHeader"].Columns.Remove("driverID");
            ds.Tables["dtExceptionHeader"].Columns.Remove("lon");
            ds.Tables["dtExceptionHeader"].Columns.Remove("lat");
            ds.Tables["dtExceptionHeader"].Columns.Remove("assetID");
            ds.Tables["dtExceptionHeader"].Columns.Remove("ruleID");
            ds.Tables["dtExceptionHeader"].Columns.Remove("dateTimeToLocal");
            ds.Tables["dtExceptionHeader"].Columns.Remove("UID");
            ds.Tables["dtExceptionHeader"].Columns.Remove("Date From");


            return ds;
        }

        public static DataSet PDFTemperature(NaveoService.Models.DebugData dt)
        {




            DataSet ds = new DataSet();
         
            
            dt.dtData.Columns["RowNum"].SetOrdinal(0);
            dt.dtData.Columns["assetName"].SetOrdinal(1);
            dt.dtData.Columns["sDriverName"].SetOrdinal(2);
            dt.dtData.Columns["localTime"].SetOrdinal(3);
            dt.dtData.Columns["Speed"].SetOrdinal(4);
            dt.dtData.Columns["details"].SetOrdinal(5);

            dt.dtData.Columns["RowNum"].ColumnName = "No";
            dt.dtData.Columns["assetName"].ColumnName = "Asset";
            dt.dtData.Columns["sDriverName"].ColumnName = "Driver";

            dt.dtData.Columns["localTime"].ColumnName = "Date";

            dt.dtData.Columns["details"].ColumnName = "Temperature";



            dt.dtData.Columns.Remove("UID");
            dt.dtData.Columns.Remove("DateTimeServer");

      
            dt.dtData.Columns.Remove("Longitude");
            dt.dtData.Columns.Remove("Latitude");
            dt.dtData.Columns.Remove("EngineOn");
            dt.dtData.Columns.Remove("TripDistance");
            dt.dtData.Columns.Remove("TripTime");
            dt.dtData.Columns.Remove("WorkHour");
            dt.dtData.Columns.Remove("DriverID");
            dt.dtData.Columns.Remove("DateTimeGPS_UTC");
            dt.dtData.Columns.Remove("details1");
            dt.dtData.Columns.Remove("RoadSpeed");
            dt.dtData.Columns.Remove("StopFlag");
            dt.dtData.Columns.Remove("LongLatValidFLag");
        
            dt.dtData.Columns.Remove("AssetID");

            dt.dtData.TableName = "dtTemperature";
            ds.Tables.Add(dt.dtData);
            return ds;
        }


        public static DataSet PDFCustTemperature(dtData dt)
        {



            DataSet ds = new DataSet();
            DataTable dtCustTemp = dt.Data;
            dtCustTemp.Columns.Remove("rowNum");
            dtCustTemp.Columns.Remove("GrpDate");
            dtCustTemp.Columns.Remove("AssetID");
           


            dtCustTemp.TableName = "dtCustTemperature";
            ds.Tables.Add(dtCustTemp);
            return ds;
        }


        public static DataSet PDFFuelGraph(DataTable dt)
        {
            DataSet ds = new DataSet();
            DataTable dtCopy = new DataTable();
          
            dt.Columns["AssetName"].SetOrdinal(0);
            dt.Columns["ftype"].SetOrdinal(1);
            dt.Columns["convertedValue"].SetOrdinal(2);
            dt.Columns["uom"].SetOrdinal(3);
            dt.Columns["diff"].SetOrdinal(4);
            dt.Columns["dateTimeGPS_UTC"].SetOrdinal(5);

            dt.Columns["ftype"].ColumnName = "Type";
            dt.Columns["dateTimeGPS_UTC"].ColumnName = "Date Time";
            dt.Columns["diff"].ColumnName = "Difference";
            dt.Columns["uom"].ColumnName = "Unit of Measure";

            dt.Columns.Remove("gpsUID");
            dt.Columns.Remove("assetID");
            dt.Columns.Remove("lat");
            dt.Columns.Remove("lon");
            dt.TableName = "dtFuelgraphReport";
            dtCopy = dt.Copy();
            ds.Tables.Add(dtCopy);
           


            return ds;
        }




        public static DataSet PDFFuel(DataTable dt)
        {


            DataSet ds = new DataSet();



            dt.TableName = "dtFuelReport";
            ds.Tables.Add(dt);
            return ds;
        }

        public static DataSet PDFAuxReport(DataTable dt)
        {


            DataSet ds = new DataSet();

            dt.Columns["AssetNumber"].SetOrdinal(0);
            dt.Columns["Auxiliary"].SetOrdinal(1);
            dt.Columns["AuxiliaryDescription"].SetOrdinal(2);
            dt.Columns["DateTimeON"].SetOrdinal(3);
            dt.Columns["DateTimeOff"].SetOrdinal(4);
            dt.Columns["Duration"].SetOrdinal(5);
            dt.Columns["address"].SetOrdinal(6);
            dt.Columns["Zone"].SetOrdinal(7);
            dt.Columns["sDriverName"].SetOrdinal(8);

            dt.Columns["AssetNumber"].ColumnName = "Asset Name";
            dt.Columns["DateTimeON"].ColumnName = "Date Time On";
            dt.Columns["DateTimeOff"].ColumnName = "Date Time Off";
            dt.Columns["address"].ColumnName = "Address";
            dt.Columns["address"].ColumnName = "Address";
            dt.Columns["AuxiliaryDescription"].ColumnName = "Auxiliary Description";
            dt.Columns["sDriverName"].ColumnName = "Driver Name";

            dt.Columns.Remove("RowNumber");
            dt.Columns.Remove("ZoneID");
            dt.Columns.Remove("Longitude");
            dt.Columns.Remove("Latitude");
            dt.Columns.Remove("Color");

            dt.TableName = "dtAuxReport";
            ds.Tables.Add(dt);
            return ds;
        }
        public static DataSet PDFAmiranReport(DataTable dt)
        {


            DataSet ds = new DataSet();

   

            dt.TableName = "dtAmiranReport";
            ds.Tables.Add(dt);
            return ds;
        }

        public static DataSet PDFmolgReport(List<CustomTripHeader> customTripHeader)
        {


            DataSet ds = new DataSet();
            DataTable dt = new myConverter().ListToDataTable<CustomTripHeader>(customTripHeader);


            dt.TableName = "dtMolgReport";
            ds.Tables.Add(dt);
            return ds;
        }

        public static DataSet PDFplanningReport(DataTable dt)
        {


            DataSet ds = new DataSet();

            dt.TableName = "dtPlanningReport";

            dt.Columns["pid"].SetOrdinal(0);
            dt.Columns["requestCode"].SetOrdinal(1);
            dt.Columns["requestDate"].SetOrdinal(2);
            dt.Columns["dateCreated"].SetOrdinal(3);
            dt.Columns["startTime"].SetOrdinal(4);
            dt.Columns["endTime"].SetOrdinal(5);
            dt.Columns["departure"].SetOrdinal(6);
            dt.Columns["arrival"].SetOrdinal(7);
            dt.Columns["status"].SetOrdinal(8);

            dt.Columns["pid"].ColumnName = "Pid";
            dt.Columns["requestCode"].ColumnName = "Request Code";
            dt.Columns["requestDate"].ColumnName = "Request Date";
            dt.Columns["dateCreated"].ColumnName = "Date Created";
            dt.Columns["startTime"].ColumnName = "Start Time";
            dt.Columns["endTime"].ColumnName = "End Time";
            dt.Columns["departure"].ColumnName = "Departure";

            dt.Columns["arrival"].ColumnName = "Arrival";

            dt.Columns["status"].ColumnName = "Approval";

            dt.Columns.Remove("departureId");
            dt.Columns.Remove("arrivalId");
            dt.Columns.Remove("processedBy");
            dt.Columns.Remove("processedDate");
            dt.Columns.Remove("coordinatesFrom");
            dt.Columns.Remove("coordinatesTo");
            dt.Columns.Remove("dateFrom");
            dt.Columns.Remove("dateTo");
            dt.Columns.Remove("Type");
            dt.Columns.Remove("ApprovalID");
            dt.Columns.Remove("rowNum");


            ds.Tables.Add(dt);
            return ds;
        }

        public static DataSet PdfShiftReport(DataTable dt)
        {


            DataSet ds = new DataSet();

            dt.TableName = "dtShift";

            dt.Columns["sName"].SetOrdinal(0);
            dt.Columns["TimeFr"].SetOrdinal(1);
            dt.Columns["TimeTo"].SetOrdinal(2);
            dt.Columns["sComments"].SetOrdinal(3);
   

            dt.Columns["sName"].ColumnName = "Shift Description";
            dt.Columns["TimeFr"].ColumnName = "Time From";
            dt.Columns["TimeTo"].ColumnName = "Time To";
            dt.Columns["sComments"].ColumnName = "Remarks";

            dt.Columns.Remove("SID");
            dt.Columns.Remove("rowNum");

            ds.Tables.Add(dt);
            return ds;
        }

        public static DataSet PdfRosterReport(DataTable dt)
        {


            DataSet ds = new DataSet();

            dt.TableName = "dtRoster";

            dt.Columns["RosterPeriod"].SetOrdinal(0);
            dt.Columns["RosterDate"].SetOrdinal(1);
            dt.Columns["RosterName"].SetOrdinal(2);
            dt.Columns["Asset"].SetOrdinal(3);
            dt.Columns["ResoourceName"].SetOrdinal(3);
            dt.Columns["sRemarks"].SetOrdinal(3);

            dt.Columns["RosterPeriod"].ColumnName = "Roster Period";
            dt.Columns["RosterDate"].ColumnName = "Roster Date";
            dt.Columns["ResoourceName"].ColumnName = "Resource Name";
            dt.Columns["sRemarks"].ColumnName = "Remarks";

            dt.Columns.Remove("SID");
            dt.Columns.Remove("dtFrom");
            dt.Columns.Remove("dtTo");
            dt.Columns.Remove("sName");
            dt.Columns.Remove("GRID");
            dt.Columns.Remove("AssetID");
            dt.Columns.Remove("ResourceID");
            dt.Columns.Remove("ResourceType");
            dt.Columns.Remove("rowNum");
            ds.Tables.Add(dt);
            return ds;
        }


        public static DataSet PdfFuelExceptionsReport(DataTable dt)
        {


            DataSet ds = new DataSet();

            dt.TableName = "dtFuelExceptions";

            dt.Columns["RuleName"].SetOrdinal(0);
            dt.Columns["Asset"].SetOrdinal(1);
            dt.Columns["DateTimeRule"].SetOrdinal(2);
            dt.Columns["ConditionType"].SetOrdinal(3);
            dt.Columns["DifferenceInLitres"].SetOrdinal(4);
            dt.Columns["Zone"].SetOrdinal(5);
            dt.Columns["address"].SetOrdinal(6);

            dt.Columns["RuleName"].ColumnName = "Rule Name";
            dt.Columns["DateTimeRule"].ColumnName = "DateTime";
            dt.Columns["ConditionType"].ColumnName = "Condition Type";
            dt.Columns["DifferenceInLitres"].ColumnName = "Difference In Litres";
            dt.Columns["address"].ColumnName = "Address";

            dt.Columns.Remove("lat");
            dt.Columns.Remove("lon");
            dt.Columns.Remove("gpsUID");
            dt.Columns.Remove("assetID");
            dt.Columns.Remove("uom");
            dt.Columns.Remove("convertedValue");
            ds.Tables.Add(dt);
            return ds;
        }

        public static DataSet PdfPlanningExceptionsReport(DataTable dt)
        {


            DataSet ds = new DataSet();

            dt.TableName = "dtPlanningExceptions";

       
            dt.Columns["Rule Name"].ColumnName = "Rule Name";
            dt.Columns["ViaPoint"].ColumnName = "Via Point";
            dt.Columns["ExpectedTime"].ColumnName = "Expected Time";
            dt.Columns["ActualTime"].ColumnName = "Actual Time";

            dt.Columns.Remove("Status");
            ds.Tables.Add(dt);
            return ds;
        }

        #endregion

        #region Private

        /// <summary>
        /// Convert int to color
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        private static Color ConvertToColor(int i)
        {
            color = CommonService.HexConverter(Color.FromArgb(i));
            return ColorTranslator.FromHtml(color);
        }

        /// <summary>
        /// Styling of cell: total sum 
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="row"></param>
        /// <param name="XlsxColumn"></param>
        private static void TotalBorderStyle(ExcelWorksheet sheet, int row, int XlsxColumn)
        {
            var borderStyle = ExcelBorderStyle.Thick;
            sheet.Cells[row, XlsxColumn].Style.Border.Top.Style = borderStyle;
            sheet.Cells[row, XlsxColumn].Style.Border.Bottom.Style = borderStyle;
        }

        #endregion
    }
}
