﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Models
{
    public class Fuel
    {

        public int fuelDataUID { get; set; }
        public int assetID { get; set; }
        public DateTime dateFrom { get; set; }
        public DateTime dateTo { get; set; }

        public DateTime? dtFrom { get; set; }

        public DateTime? dtTo { get; set; }

        public List<int> lassetIds { get; set; }
        public List<String> sortColumns { get; set; } = new List<String>();
        public Boolean sortOrderAsc { get; set; } = true;

        public int? page { get; set; }
        public int? limitPerPage { get; set; }
    }
}
