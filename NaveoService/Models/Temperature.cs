﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Models
{
    public class Temperature
    { 
        public List<int> assetIds { get; set; }
        public DateTime dateFrom { get; set; }
        public DateTime dateTo { get; set; }
        public List<String> lTypeID { get; set; } = new List<string>();
        public int TimeInterval { get; set; }
        public List<String> sortColumns { get; set; } = new List<String>();
        public Boolean sortOrderAsc { get; set; } = true;
        
    }
}
