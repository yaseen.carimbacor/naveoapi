﻿namespace NaveoService.Models
{
    public class JsTreeModel
    {
        public string text { get; set; }
        public string parent { get; set; }
        public string icon { get; set; }
        public string id { get; set; }
        public JsTreeState state { get; set; }

    }

    public class JsTreeState
    {
        public JsTreeState()
        {
            opened = false;
            disabled = false;
            selected = false;
        }

        public bool opened { get; set; }
        public bool disabled { get; set; }
        public bool selected { get; set; }
    }

    public class TemporaryTree
    {
        public int GMID { get; set; }
        public string GMID_Description { get; set; }
        public int parentGMID { get; set; }
        public int MID { get; set; }
    }
}