﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Models
{
    public class CustomTrips
    {

        public DateTime dateFrom { get; set; }
        public DateTime dateTo { get; set; }
        public List<String> sortColumns { get; set; } = new List<String>();
        public Boolean sortOrderAsc { get; set; } = false;
        public List<int> assetdIds { get; set; } = new List<int>();
        public int assetId { get; set; }

    }
}
