﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Models
{
    public class GlobalParam
    {
        public int Id { get; set; }

        [Display(Name = "Comments")]
        public string ParamComments { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string ParamName { get; set; }

        [Required]
        [Display(Name = "Value")]
        public string PValue { get; set; }

    }
}
