﻿using NaveoService.Models.Custom;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Models.DTO
{
    public class ShiftTemplateDTO
    {

        public List<apiShiftTemplate> shiftTemplate { get; set; } = new List<apiShiftTemplate>();

    }


    public class  apiShiftTemplate

    {

        public int iID { get; set; }
        public int GroupID { get; set; }
        public minifiedValues ShiftType { get; set; }
        public int OrderNo { get; set; }
        public String Description { get; set; }
        public DataRowState oprType { get; set; }

    }


}
