﻿using NaveoService.Models.Custom;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace NaveoService.Models.DTO
{
    public class AssetDTO : BaseDTO
    {
        public List<apiAsset> assets { get; set; } = new List<apiAsset>();

        public gmsAsset gmsAsset { get; set; } = new gmsAsset();


    }
    public class apiAsset
    {
        public int assetId { get; set; }
        public String assetNumber { get; set; }
        public String status_B { get; set; }
        //public DateTime createdDate { get; set; }
        //public DateTime updatedDate { get; set; }
        public String timeZoneId { get; set; }
        public TimeSpan timeZoneTs { get; set; }
        public DataRowState oprType { get; set; }

        public int? YearManufactured { get; set; }
        public DateTime? PurchasedDate { get; set; }
        public Double? PurchasedValue { get; set; }
        public String RefA { get; set; }
        public String RefB { get; set; }
        public String RefC { get; set; }
        public String ExternalRef { get; set; }
        public String EngineSerialNumber { get; set; }
        public int? EngineCapacity { get; set; }
        public int? EnginePower { get; set; }

        public MinifiedUser createdUser { get; set; }
        public MinifiedUser updatedUser { get; set; }

        public minifiedValues assetType { get; set; }
        public minifiedValues make { get; set; }
        public minifiedValues model { get; set; }
        public minifiedValues color { get; set; }
        public minifiedValues vehicleType { get; set; }

        public minifiedValues roadTypeSpeed { get; set; }
        public AssetDeviceMap assetDeviceMap { get; set; }
  
        public List<AssetMatrix> lMatrix { get; set; } = new List<AssetMatrix>();
        public List<AssetUOM> lUOM { get; set; } = new List<AssetUOM>();
    }
    public class AssetUOM
    {
        public int iId { get; set; }
        public int assetId { get; set; }
        public minifiedValues uom { get; set; }
        public int? capacity { get; set; }
        public DataRowState oprType { get; set; }
    }
    public class DeviceAuxilliaryMap
    {
        public int uId { get; set; }
        public int? mapId { get; set; }
        public String deviceId { get; set; }
        public minifiedValues fuelProduct { get; set; }
        public int? thresholdHigh { get; set; }
        public int? thresholdLow { get; set; }
        public int tankCapacity { get; set; }
        public Decimal stdConsumption { get; set; }
        public minifiedValues readingUOM { get; set; }
        public minifiedValues convertedUOM { get; set; }
        public DataRowState oprType { get; set; }
        private List<CalibrationChart> lcalibration = new List<CalibrationChart>();
        public List<CalibrationChart> lCalibration { get { return lcalibration; } set { lcalibration = value; } }

    }
    public class CalibrationChart
    {
        public Int64 iId { get; set; }
        public int? assetId { get; set; }
        public String deviceId { get; set; }
        public int? auxId { get; set; }
        public float reading { get; set; }
        public float convertedValue { get; set; }
        public DataRowState oprType { get; set; }
    }
    public class AssetDeviceMap
    {
        public int mapId { get; set; }
        public int assetId { get; set; }
        public String deviceId { get; set; }
        public String serialNumber { get; set; }
        public String status_B { get; set; }
        public Boolean isFuel { get; set; }
        public DataRowState oprType { get; set; }
        public DeviceAuxilliaryMap deviceAuxilliaryMap { get; set; }


    }
    public class AssetMatrix
    {
        public DataRowState oprType { get; set; }
        public int mId { get; set; }
        public int iId { get; set; }
        public int gMId { get; set; }
        public String gMDesc { get; set; }

    }
    public class AssetExtProVehicles
    {
        public minifiedValues vehicleType { get; set; }
        public minifiedValues color { get; set; }
        public String make { get; set; }
        public String model { get; set; }
        public List<AssetUOM> lUOM { get; set; } = new List<AssetUOM>();

        public String description { get; set; }
        public string manufacturer { get; set; }
        public int yearManufactured { get; set; }
        public DateTime purchasedDate { get; set; }
        public Decimal purchasedValue { get; set; }
        public string poRef { get; set; }
        public int expectedLifetime { get; set; }
        public Decimal residualValue { get; set; }
        public String externalReference { get; set; }
        public String inService_b { get; set; }
        public String engineSerialNumber { get; set; }
        public String engineCapacity { get; set; }
        public String enginePower { get; set; }
        public DataRowState oprType { get; set; }
    }

    public class gmsAsset
    {
        public GarageMgtAsset asset { get; set; } = new GarageMgtAsset();

    }

    public class GarageMgtAsset
    {
        public int Id { get; set; }

        public int CustomerId { get; set; }

        public string Number { get; set; }

        public string OriginalNumber { get; set; }

        public int? Make { get; set; }

        public int? Model { get; set; }

        public string CustomerName {get;set;}

        public int?  FuelType { get; set; } 

        public string ChassisNo { get; set; }

        public int Category { get; set; }

        public int Status { get; set; }

        public DateTime? RoadTaxExpiryDate { get; set; }

        public DateTime? PSVLicenseExpiryDate { get; set; }

        public DateTime? FitnessExpiryDate { get; set; }

        public Double RoadTaxAmount { get; set; }

        public Double FitnessAmount { get; set; }

        public Double PSVAmount { get; set; }

        public Boolean IsFlagged { get; set; } = true;

        public int FleetServerId { get; set; }

        public string FleetServer { get; set; }

        public int FMSAssetId { get; set; }

        public int FMSAssetType { get; set; }

        public string Description { get; set; }

        public string Section { get; set; }

        public int? VehicleType { get; set; }

        public string Colour { get; set; }

        public int SupplierId { get; set; }
        public int? YearManufactured { get; set; }

        public DateTime? PurchasedDate { get; set; }

        public DateTime? RegistrationDate { get; set; }

        public Double? PurchasedValue { get; set; }

        public int ExpectedLifetime { get; set; }

        public int ResidualValue { get; set; }

        public string Reference { get; set; }

        public Boolean IsInService { get; set; } = true;

        public string EngineSerialNumber { get; set; }

        public string EngineType { get; set; }

        public string EngineCapacity { get; set; }

        public string EnginePower { get; set; }

        public string StandardConsumption { get; set; }

        public string Department { get; set; }

        public string Remarks { get; set; }

    }


    public class MaintenanceAndFuelCostData
    {
        public List<int> AssetsId { get; set; }
        public string FMSServer { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }

    }
    public class apigarageFuelData
    {
        public List<garageFuelData> lgarageFuelData;
    }


    public class garageFuelData
    {
        public int AssetId { get; set; }
        public string Description { get; set; }
        public decimal? ServicingCost { get; set; }
        public decimal? RepairCost { get; set; }
        public decimal? MaintenanceCost { get; set; }
        public decimal? FuelQuantity { get; set; }
        public double? FuelCost { get; set; }
    }
}