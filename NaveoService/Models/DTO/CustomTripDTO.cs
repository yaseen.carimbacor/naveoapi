﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Models.DTO
{
    public class CustomTripDTO
    {
        public List<CustomTripHeader> lCustomTripHeader { get; set; } = new List<CustomTripHeader>();

        public List<CustomTrip> lCustomTrip { get; set; } = new List<CustomTrip>();

    }



    public class CustomTrip
    {
        public DateTime Date { get; set; }
       public List<CustomTripHeader1> lCustomTripHeader1 { get; set; } = new List<CustomTripHeader1>();
    }


    public class CustomTripHeader1
    {
        public Boolean parentFlag = true;

        public string Title { get; set; }

        public string AssetName { get; set; }

        public string FieldCode { get; set; }

        public List<TimeDetails> lDetailTime { get; set; } = new List<TimeDetails>();

        public List<NoOperationDetails> lDetailNoOperation { get; set; } = new List<NoOperationDetails>();

        public List<NonWorkTimeDetails> lDetailNoWorkTime { get; set; } = new List<NonWorkTimeDetails>();

        public List<WorkDone> lDetailWorkDone { get; set; } = new List<WorkDone>();
    }

    public class TimeDetails
    {
        public String TimeStarted { get; set; }

        public String TimeEnded { get; set; }

        public Double Normal { get; set; }


        public Double LunchTeaTime { get; set; }

        public Double Overtime { get; set; }

        public Double TotalAvailableTime { get; set; }
    }

    public class  NoOperationDetails
    {
        //public Double NoAvailableTime { get; set; }

        //public Double Rain { get; set; }

        //public Double DriverAbsent { get; set; }

        //public Double TotalNoOperationTime { get; set; }

        public string NoOperationType { get; set; }

        public Double Value { get; set; }

        //public Double TotalNoOperationTime { get; set; }

        //public Double TotalNoOperationTimePercent { get; set; }
    }

    public class NonWorkTimeDetails
    {
        public string NoWorkTimeType { get; set; }

        public Double Value { get; set; }

        //public Double TotalNoWorkingTime { get; set; }

        //public Double TotalNoOperationTimePercent { get; set; }
    }

    public class WorkDone
    {
       public string WorkDoneType { get; set; }

        public Double Value { get; set; }

    }

}


    public class CustomTripHeader
    {
      
        public String AssetName { get; set; }
        public Double NetWeight { get; set; }
        public Double GrossWieght { get; set; }
        public Double VoucherNo { get; set; }
        public Double TareWeight { get; set; }
        public int NoOfHouseHold { get; set; }
        public Double ZoneDistanceCovered { get; set; }
        public String TripStartTime { get; set; }
        public String TripEndTime { get; set; }

        public String TimeTaken { get; set; }
        public Double Consumption { get; set; }
        public Double TripDistance { get; set; }

        //public List<CustomTripDetails> lCustomTripDetails { get; set; } = new List<CustomTripDetails>();


   }

    public class CustomTripDetails
    {
        public String DateFrom { get; set; }
        public String DateTo { get; set; }
        //public String Stop { get; set; }
        public String Duration { get; set; }
        public String Description { get; set; }
        public String Tool { get; set; }
        public String Field { get; set; }
        public Double? HectareCovered { get; set; }
        public Double ActualEfficiency { get; set; }
    }

