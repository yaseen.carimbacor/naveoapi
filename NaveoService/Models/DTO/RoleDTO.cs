﻿using NaveoOneLib.Models.Permissions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Models.DTO
{
    public class RoleDTO
    {
        public Role role { get; set; } = new Role();
    }
}
