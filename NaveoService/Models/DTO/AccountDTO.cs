﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Models.DTO
{
    public class AccountDTO : BaseDTO
    {
    }

    public class ActivityDTO
    {
        public int idleWait { get; set; }
        public int maxLoginAttempts { get; set; }
    }
}