﻿using NaveoService.Models.Custom;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Models.DTO
{
    public class RosterDTO : BaseDTO
    {


        public rosterheader roster { get; set; } = new rosterheader();


    }


    public class rosterheader
    {
        public int ild { get; set; }

        public string rosterName { get; set; }
        public DateTime dtFrom { get; set; }
        public DateTime dtTo { get; set; }
        public string sRemarks { get; set; }
        public int? iLocked { get; set; }
        public int? iOriginal { get; set; }
  
        public DataRowState oprType { get; set; }

        public List<rosterResource> lrosterResource { get; set; } = new List<rosterResource>();

        public rosterAsset rosterAsset { get; set; } = new rosterAsset();

        public List<rosterdetails> lrosterDetails { get; set; } = new List<rosterdetails>();

        public List<NaveoOneLib.Models.GMatrix.Matrix> lMatrix = new List<NaveoOneLib.Models.GMatrix.Matrix>();
    }

    public class rosterdetails
    {
        public int rid { get; set; }
        public int grid { get; set; }

        public int shifttemplateId { get; set; }
        public string shifttemplatedesc { get; set; }
        public minifiedValues shiftType { get; set; }
        //public minifiedValues resource { get; set; }
        //public minifiedValues driver { get; set; }
        public DateTime rDate { get; set; }
        public DataRowState oprType { get; set; }

    }

    public class rosterResource
    {
        public int iId { get; set; }

        public int rid { get; set; }

        public minifiedValues minifiedResources { get; set; }

        public DataRowState oprType { get; set; }

    }

    public class rosterAsset
    {
        public int ild { get; set; }

        public int rid { get; set; }

        public minifiedValues minifiedAssets { get; set; }
        public DataRowState oprType { get; set; }

    }

}
