﻿using NaveoOneLib.Models.GMatrix;
using NaveoService.Models.Custom;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Models.DTO
{
    public class ResourceDTO :BaseDTO
    {

        public List<apiResource> resources { get; set; } = new List<apiResource>();

    }


    public class apiResource
    {
        public int resourceId { get; set; }
        public string resourceType { get; set; }
        public string title { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string driverTokenNo { get; set; }
        public string nic { get; set; }
        public string status { get; set; }
        public DateTime? dateOfBirth { get; set; }
        public string employeeNumber { get; set; }
        public int? homeFlatNumber { get; set; }
        public minifiedValues minifiedUsers { get; set; }
        public string street1 { get; set; }
        public string street2 { get; set; }
        public minifiedValues minifiedcity { get; set; }
        public string postalCode { get; set; }
        public minifiedValues minifieddistrict { get; set; }
        public string mobileNo { get; set; }
        public string countryCode { get; set; }
        public DateTime? privateLiscenceIssueDate { get; set; }
        public DateTime? privateLiscenseExpiryDate { get; set; }
        public minifiedValues minifiedZone { get; set; }
        public string comment { get; set; }
        public string reason { get; set; }
        public string email { get; set; }
        public string officerLevel { get; set; }
        public string responsibleOfficer { get; set; }
        public minifiedValues minifiednameOfInsitution { get; set; }
        public minifiedValues minifieddriverType { get; set; }
        public minifiedValues minifiedgradeType { get; set; }
        public string privateLiscenseDriverCategory { get; set; }
        public  string privateLiscenseNumber { get; set; }
        public List<resourceLiscense> resourceLiscense { get; set; }  = new List<resourceLiscense> { };
        public List<aMatrix> lMatrix { get; set; } = new List<aMatrix> { };
        //public DateTime createdDate { get; set; }
        //public DateTime updatedDate { get; set; }
        public int createdBy { get; set; }
        public int updatedBy { get; set; }
        public string Photo { get; set; }
        public DataRowState oprType { get; set; }

    }

    public class aMatrix
    {

        public DataRowState oprType { get; set; }

        public int mId { get; set; }
        public int iId { get; set; }
        public int gMId { get; set; }
        public String gMDesc { get; set; }

    }

    public class resourceLiscense
    {
        public int iID { get; set; }
        public int DriverID { get; set; }
        public String sType { get; set; }
        public String sCategory { get; set; }
        public String sNumber { get; set; }
        public DateTime? IssueDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public DataRowState oprType { get; set; }
    }

}
