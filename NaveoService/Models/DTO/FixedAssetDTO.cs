﻿using NaveoOneLib.Models.Assets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Models.DTO
{
    public class FixedAssetDTO : BaseDTO
    {
        public FixedAsset fixedAsset { get; set; } = new FixedAsset();

        public GISAssetCategory gisAssetCategory { get; set; } = new GISAssetCategory();

        public String imageName { get; set; }
    }
}
