﻿using NaveoService.Models.Custom;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace NaveoService.Models.DTO
{
    public class TripDTO: BaseDTO
    {
        //Important structural changes to be made. MobiMove will also be Impacted 
        public NaveoService.Models.TripDetails tripDetails { get; set; }
        public NaveoService.Models.TripHeader tripHeader { get; set; }
    }

    public class TripsRpt
    {
        public MinifiedAsset vehicle { get; set; }
        public MinifiedDriver driver { get; set; }
        public DateTime from { get; set; }
        public TimeSpan tripTime { get; set; }
        public DateTime to { get; set; }
        public double tripDistance { get; set; }
        public TimeSpan stopTime { get; set; }
        public String departureAddress { get; set; }
        public double departureLon { get; set; }
        public double departureLat { get; set; }
        public String arrivalAddress { get; set; }
        public double arrivalLon { get; set; }
        public double arrivalLat { get; set; }
        public MinifiedZone departureZone { get; set; }
        public MinifiedZone arrivalZone { get; set; }
        public TimeSpan idlingTime { get; set; }
        public double maxSpeed { get; set; }
        public int iID { get; set; }
        public int accel { get; set; }
        public int brake { get; set; }
        public int corner { get; set; }
        public int? iColor { get; set; }
        public TimeSpan overSpeed1Time { get; set; }
        public TimeSpan overSpeed2Time { get; set; }
        public int rowNumPerPage { get; set; }
        public int exceptionsCnt { get; set; }
        public DateTime dtUTCStart { get; set; }
        public DateTime dtUTCEnd { get; set; }

        public decimal lastOdometer { get; set; }

    }

    public class lTrips
    {
        public List<TripsRpt> trips { get; set; } = new List<TripsRpt>();
        public DataTable totals { get; set; }
    }

    public class TripsHeaderDTO : BaseDTO
    {
    }

    //Amented classes using right dto. Above to move accordingly
    public class TripDetailsDTO : BaseDTO
    {
    }

}