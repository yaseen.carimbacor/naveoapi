﻿using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Models.Users;
using NaveoService.Models.Custom;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace NaveoService.Models.DTO
{
    public class UserDTO : BaseDTO
    {
        public List<apiUser> users { get; set; }
    }


    public class apiUser
    {
   
        public List<Role> lRoles { get; set; } = new List<Role>();
        public List<NaveoOneLib.Models.GMatrix.Matrix> lMatrix { get; set; } = new List<NaveoOneLib.Models.GMatrix.Matrix>();
        public UserExtension userExtension { get; set; }  = new UserExtension();
        public int UID { get; set; }
        public String Username { get; set; }
        public String Names { get; set; }
        public String LastName { get; set; }
        public String Email { get; set; }
        public String Tel { get; set; }
        public string Title { get; set; }
        public String MobileNo { get; set; }
        public DateTime DateJoined { get; set; }
        public String Status_b { get; set; }
        public String Password { get; set; }

        public int AccessTemplateId { get; set; }
        public int MaxDayMail { get; set; }
        public minifiedValues Zone { get; set; }

        public minifiedValues PlnLkpVal { get; set; }

 
        public int? MobileAccess { get; set; }
        public int? ExternalAccess { get; set; }

        public String TwoStepVerif { get; set; }


        public Boolean bTwoWayAuthEnabled { get; set; }
        public Guid UserToken { get; set; }

        public DataRowState oprType { get; set; }

        public String AccessedOn { get; set; }

    }
}