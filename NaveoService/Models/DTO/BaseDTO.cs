﻿using NaveoOneLib.Models;
using NaveoService.Constants;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace NaveoService.Models.DTO
{
    public class BaseDTO
    {
        public dynamic data { get; set; }
        public dynamic additionalData { get; set; }
        public int totalItems { get; set; }
        public Guid oneTimeToken { get; set; }
        public string sQryResult { get; set; }
        public string errorMessage { get; set; }
        public string MapServerResponse { get; set; }

        /// <summary>
        /// UnAuthorized API return call
        /// </summary>
        /// <returns></returns>
        public BaseDTO Unauthorized()
        {
            return new BaseDTO { errorMessage = Headers.UNAUTHORIZED };
        }

        /// <summary>
        /// UnAuthorized API return call with additional message
        /// </summary>
        /// <param name="securityToken"></param>
        /// <returns></returns>
        public BaseDTO Unauthorized(string message)
        {
            return new BaseDTO { errorMessage = message };
        }
    }

    public class PutDTO
    {
        public dynamic data { get; set; }
        public bool newData { get; set; } = true;

        //public dynamic ftpdata { get; set; }
    }
}