﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NaveoOneLib.Models.BLUP;

namespace NaveoService.Models.DTO
{
    public class BlupDTO
    {
        public BLUP blup { get; set; } = new BLUP();
    }
}
