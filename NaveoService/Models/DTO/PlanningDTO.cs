﻿using NaveoOneLib.Models.GMatrix;
using NaveoService.Models.Custom;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Models.DTO
{
    public class PlanningDTO : BaseDTO
    {
        public apiPlanning planning { get; set; } = new apiPlanning();

        public List<apiPlanning> lPlanning { get; set; } = new List<apiPlanning>();
    }

    public class apiPlanning
    {
        public int pId { get; set; }
        public int? approval { get; set; }
        public String remarks { get; set; }
        public minifiedValues createdUser { get; set; }
        public minifiedValues updatedUser { get; set; }
        public DateTime createdDate { get; set; }
        public DateTime updatedDate { get; set; }
        public bool urgent { get; set; }
        public String requestCode { get; set; }
        public String parentRequestCode { get; set; }
        public String type { get; set; }
        public String sComments { get; set; }
        public String closureComments { get; set; }
        public String reference { get; set; }
        public int ApprovalID { get; set; }
        //public minifiedValues postHeld { get; set; }
        //public minifiedValues baseoffice { get; set; }
        public DataRowState oprType { get; set; }
        public List<apiMatrix> lMatrix { get; set; } = new List<apiMatrix>();
        public List<apiPlanningLkup> lPlanningLkup { get; set; } = new List<apiPlanningLkup>();
        public List<apiPlanningResource> lPlanningResource { get; set; } = new List<apiPlanningResource>();
        public List<apiPlanningViaPointH> lPlanningViaPointH { get; set; } = new List<apiPlanningViaPointH>();
    }


    public class apiMatrix
    {
        public DataRowState oprType { get; set; }

        public int miD { get; set; }
        public int iID { get; set; }
        public int gMID { get; set; }
        public String gmDesc { get; set; }
    }

    public class apiPlanningLkup
    {
        public int iID { get; set; }
        public int pId { get; set; }
        public minifiedValues lookupValues { get; set; }
        public String lookupType { get; set; }
        public DataRowState oprType { get; set; }
    }

    public class apiPlanningResource
    {
        public int iID { get; set; }
        public int pId { get; set; }
        public minifiedValues driver { get; set; }
        public String ResourceType { get; set; }
        public int customType { get; set; }
        public int mileageAllowance { get; set; }
        public int travelGrant { get; set; }
        public string remarks { get; set; }
        public DataRowState oprType { get; set; }

    }

    public class apiPlanningViaPointH
    {
        public int iID { get; set; }
        public int pid { get; set; }
        public int seqNo { get; set; }
        public minifiedValues zones { get; set; }
        public Double? lat { get; set; }
        public Double? lon { get; set; }
        public minifiedValues localityVca { get; set; }
        public int? buffer { get; set; }
        public String remarks { get; set; }
        public DateTime? dtUTC { get; set; }
        public DateTime dtUTC1 { get; set; }
        public int clientId { get; set; }
        public int minDuration { get; set; }
        public int maxDuration { get; set; }
        public DataRowState oprType { get; set; }
        public List<apiPlanningViaPointD> lDetail { get; set; } = new List<apiPlanningViaPointD>();
    }


    public class apiPlanningViaPointD
    {
        public int iID { get; set; }
        public int hid { get; set; }
        public minifiedValues uom { get; set; }
        public int? capacity { get; set; }
        public String remarks { get; set; }
        public DataRowState oprType { get; set; }

    }
}

