﻿
using NaveoOneLib.Models.Reportings;
using System.Collections.Generic;
using System.Data;

namespace NaveoService.Models.DTO
{
    public class RuleDTO : BaseDTO
    {
        public apiRule rule { get; set; } = new apiRule();
    }


    public class apiRule
    {
        public int ruleId { get; set; }
        public string ruleName { get; set; }
        public string ruleCategory{ get;set; }
        public zoneRule zoneRules { get; set; }
        public DataRowState oprType { get; set; }
        public List<ruleCondition> ruleConditions { get; set; } = new List<ruleCondition>();
        public assetDriverRule assetDriverRules { get; set; } 
        public List<NotificationList> notificationLists { get; set; } = new List<NotificationList>();
    }

    public class zoneRule
    {
        public string selectionType { get; set; }
        public string selectionCriteria { get; set; }
        public Group lTreeSelections { get; set; }
        public DataRowState oprType { get; set; }
    }


    public class assetDriverRule
    {
        public string selectionType { get; set; }
        public Group lTreeSelections { get; set; } 
        public DataRowState oprType { get; set; }

    }


    public class ruleCondition
    {
        public int ruleId { get; set; }
        public int ruleType { get; set; }
        public string struc { get; set; }
        public string strucValue { get; set; }
        public string strucCondition { get; set; }
        public string strucType { get; set; }
        public int minTriggerValue { get; set; }
        public int maxTriggerValue { get; set; }

        public DataRowState oprType { get; set; }

    }


    public class NotificationList
    {
        public int arID { get; set; }
        public int targetID { get; set; }
        public int notificationType { get; set; }
        public DataRowState oprType { get; set; }
        public List<notificationListdetails> lDetails { get; set; } = new List<notificationListdetails>();
    }

    public class notificationListdetails
    {
        public int ardID { get; set; }

        public int arID { get; set; }

        public string type { get; set; }
        public int userId { get; set; }
        public string category { get; set; }

        public DataRowState oprType { get; set; }
    }


}

    
