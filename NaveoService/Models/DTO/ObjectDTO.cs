﻿using NaveoOneLib.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace NaveoService.Models.DTO
{
    public class ObjectDTO : BaseDTO
    {
    }


    public class DTOGarage
    {
        /// <summary>
        /// Core as per jstree requirenment
        /// </summary>
        public dynamic core { get; set; }

        /// <summary>
        /// Total item in the tree
        /// </summary>
        public int totalItems { get; set; }

        /// <summary>
        /// oneTimeToken used to authenticate user
        /// </summary>
        public Guid oneTimeToken { get; set; }

        /// <summary>
        /// Result obtain from the sql generated
        /// </summary>
        public string sQryResult { get; set; }

        /// <summary>
        /// Error code returned to frontend
        /// </summary>
        public string errorMessage { get; set; }

        /// <summary>
        /// Any repond to map server
        /// </summary>
        public string MapServerResponse { get; set; }

        /// <summary>
        /// Any repond to map server
        /// </summary>
        public string ServerName { get; set; }

        /// <summary>
        /// UnAuthorized API return call
        /// </summary>
        /// <returns></returns>
        public BaseDTO Unauthorized()
        {
            return new BaseDTO { errorMessage = NaveoService.Constants.Headers.UNAUTHORIZED };
        }

        /// <summary>
        /// UnAuthorized API return call with additional message
        /// </summary>
        /// <param name="securityToken"></param>
        /// <returns></returns>
        public BaseDTO Unauthorized(string message)
        {
            return new BaseDTO { errorMessage = message };
        }
    }
}