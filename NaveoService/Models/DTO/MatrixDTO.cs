﻿using NaveoOneLib.Models.GMatrix;
using NaveoService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Models.DTO
{
    public class MatrixDTO : BaseDTO
    {
        public GroupMatrix groupMatrix { get; set; } = new GroupMatrix();
    }


    public class Matrix
    {
        public int gmId { get; set; }

    }
}