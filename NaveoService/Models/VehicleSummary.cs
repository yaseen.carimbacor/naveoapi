﻿using NaveoOneLib.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace NaveoService.Models
{
    public class VehicleSummary
    {
        public int assetId { get; set; }
        public Live liveData { get; set; }
        public int noOfTrips { get; set; }
        public int noOfAlerts { get; set; }
        public int tripDistance { get; set; }
        public int possibleDrops { get; set; }
        public int PossibleDropsID { get; set; }
        public TimeSpan tripTime { get; set; }
        public TimeSpan idleTime { get; set; }
        public Boolean hasFuel { get; set; } = false;
    }
}