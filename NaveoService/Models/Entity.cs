﻿using NaveoService.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Models
{
    public class Entity
    {
        public string entityName { get; set; }
        public string format { get; set; }
        public dynamic reportData { get; set; }
        public Trips trip { get; set; }
        public Planning planning { get; set; }
        public List<int> assetIds { get; set; }
        public Exceptions exception { get; set; }
        public FuelExport fuelexport { get; set; }
        public liveAssets lAssets { get; set; }
        public ZoneVisitedReport zoneVisited { get; set; }
        public Fuel fuel { get; set; }
        public MaintenanceAndFuelCost maintenanceAndFuelCost { get; set; }

        public Monitoring.clsMonitoring monitoring { get; set; }

        public Roster r { get; set; }

        public NaveoOneLib.Models.AccidentManagement.lstAccidentManagementDto lstAccidentMng { get; set; }

    }
}