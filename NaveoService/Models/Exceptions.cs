﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Models
{
   public class Exceptions
   {
        public List<int> assetIds { get; set; }
        public DateTime dateFrom { get; set; }
        public DateTime dateTo { get; set; }
        public Boolean byDriver { get; set; } = false;
        public Boolean bAddress { get; set; } = false;
        public Boolean bShowZones { get; set; } = false;
        public List<int> ruleIds { get; set; } = new List<int>();
    }
}
