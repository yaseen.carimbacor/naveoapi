﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Models
{
   public   class Audit
    {
        public DateTime dateFrom { get; set; }
        public DateTime dateTo { get; set; }
        public List<String> sortColumns { get; set; } = new List<String>();
        public Boolean sortOrderAsc { get; set; } = false;


    }

    public class AuditFilter {
        public DateTime dateFrom { get; set; }
        public DateTime dateTo { get; set; }

        public List<int> userIds { get; set; }

        public String type { get; set; }

        public List<String> sortColumns { get; set; } = new List<String>();
        public Boolean sortOrderAsc { get; set; } = false;
    }

    public class AuditDrill
    {
        public int id { get; set; }

        public string controller { get; set; }
    }
}
