﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Models
{
    public class Rule
    {

        public string parentRuleId { get; set; }
        public string category { get; set; }

        public string ruleName { get; set; }
    }

    public class FuelRule
    {
        public List<int> lUsers { get; set; }
    }


    public class RuleTemplateCiteria
    {

        public List<ruleTemplateHeader> ruleTemplate = new List<ruleTemplateHeader>();
        

    }
    public class ruleTemplateHeader
    {
        public int id { get; set; }

        public string description { get; set; }
        public int ruleType { get; set; }
        public List<ruleTemplateDetails> ruleTemplateDetails { get; set; } = new List<ruleTemplateDetails>();

        public List<String> structType { get; set; } = new List<String>();


    }
    public class ruleTemplateDetails
    {
        //public string equalTo { get; set; }
        //public string greaterEqualTo { get; set; }
        //public string lessThanEqualTo { get; set; }

        public List<string> strucCondition { get; set; } = new List<String>();
        public List<string> strucValue { get; set; } = new List<String>();
    }

    }
