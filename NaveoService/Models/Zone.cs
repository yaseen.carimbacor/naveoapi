﻿using NaveoService.Models.Custom;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace NaveoService.Models
{
    public class Zone: NaveoOneLib.Models.Zones.Zone
    {
        public String sColor { get; set; }
        public int parent { get; set; }
        public dynamic GeomData1 { get; set; }
    }

    public class ZoneData
    {
        public List<Zone> lZone { get; set; }
        public int rowCount { get; set; }
    }


    public class data
    {
        public List<Zone> lZone { get; set; }
        public int rowCount { get; set; }
    }

    public class ZoneVisitedReport
    {
        public List<int> assetIds { get; set; }
        public DateTime dateFrom { get; set; }
        public DateTime dateTo { get; set; }
        public string sType { get; set; }
        public string reportType { get; set; }
        public int idleInMinutes { get; set; }
        public int idlingSpeed { get; set; }
        public Boolean bGetZones { get; set; } = true;
        public Boolean byDriver { get; set; } = false;
        public List<int> zIds { get; set; } = new List<int>();

    }


    public class ZoneVisitedTotal
    {
        public string Asset { get; set; }

        public string Zones { get; set; }
        public int ZoneID { get; set; }

        public int Count { get; set; }

        public pivotData pData { get; set; } = new pivotData();

    }
  

    public class apiZoneVisited
    {
        public List<zoneFormat> zoneData { get; set; } = new List<zoneFormat>();

        public List<ZoneVisitedTotal> zoneVisitedTotal { get; set; } = new List<ZoneVisitedTotal>();

        public pivotData zonesVisitedPerDay { get; set; } = new pivotData();


    }
    public class zoneFormat
    {
        public string date { get; set; }

        public string asset { get; set; }

        public string zone { get; set; }
        public int zoneId { get; set; }
        public string arrivalTime { get; set; }

        public string depatureTime { get; set; }

        public string durationInZones { get; set; }

        public string type { get; set; }

        public string color { get; set; }

        public List<string> zoneIds { get; set; } = new List<string>();
    }

    public class lZones
    {
        public List<int> lZoneIDs { get; set; }
    }


    public class ZoneTypes
    {
        public int zoneId { get; set; }

        public List<int> lZoneIDs { get; set; }
    }

}