﻿using NaveoOneLib.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace NaveoService.Models
{
    public class Trips
    {
        public List<int> trips { get; set; }
        public List<int> assetIds { get; set; }
        public DateTime dateFrom { get; set; }
        public DateTime dateTo { get; set; }
        public Double iMinTripDistance { get; set; } = 0.0;
        public Boolean byDriver { get; set; } = false;
        public List<String> lTypeID { get; set; } = new List<string>();
        public int iHOZone { get; set; }
        public List<int> zoneTypes { get; set; } = new List<int>();
        public String sTotal { get; set; } = String.Empty;
        public List<String> sortColumns { get; set; } = new List<String>();
        public Boolean sortOrderAsc { get; set; } = true;
        public List<String> sWorkHours { get; set; } = new List<String>();
        public int? Odometre { get; set; }
        public int? assetId { get; set; }

    }
    public class Trip
    {
        public int assetID { get; set; }
        public Double distance { get; set; }
        public int duration { get; set; } // In seconds
    }

    public class TripHeader
    {
        public DataTable dtTrips { get; set; }
        public DataTable dtTotals { get; set; }

        public DataTable dtOdometer { get; set; }

        public int rowCount { get; set; }
    }
    public class TripDetails
    {
        public DataTable dtTripDetails { get; set; }
        public DataTable dtTripExceptions { get; set; }
    }

    public class SearchAsset
    {
        public string NaveoModel { get; set; }

        public string SearchValue { get; set; }
    }


    public class AssetGarage
    {
        public int AssetID { get; set; }

        public DateTime? DateTimeStart { get; set; }

        public DateTime? DateTimeEnd { get; set; }

        public int Odometer { get; set; }

        public int EngineHours { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class MaintenanceAndFuelCost
    {
        public List<int> assetIds { get; set; }

        public DateTime? DateTimeStart { get; set; }

        public DateTime? DateTimeEnd { get; set; }

        public List<string> Description { get; set; }
     
    }
}