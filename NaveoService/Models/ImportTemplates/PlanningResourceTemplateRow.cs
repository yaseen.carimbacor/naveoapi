﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Models.ImportTemplates
{
    public class PlanningResourceTemplateRow
    {
        #region Planning Resource Template Row
        public int Id { get; set; }
        public int PID { get; set; }
        public int DriverID { get; set; }
        public string ResourceType { get; set; }
        #endregion
    }
}