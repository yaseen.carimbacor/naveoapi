﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Models.ImportTemplates
{
    public class AssetTemplateRow : ITemplateRow
    {

        #region Asset Template Row
        public int RowNumber { get; set; }
        public int AssetID { get; set; }
        public string AssetNumber { get; set; }
        public string Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? UpdatedBy { get; set; }
        public string TimeZoneID { get; set; }
        public int Odometer { get; set; }
        public string AssetName { get; set; }
        public int AssetType { get; set; }
        public int? VehicleTypeId { get; set; }
        public int? ColorID { get; set; }
        #endregion
       
    }
}