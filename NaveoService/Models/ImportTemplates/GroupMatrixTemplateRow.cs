﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Models.ImportTemplates
{
    public class GroupMatrixTemplateRow
    {
        #region Group Matrix Template Row
        public int GMID { get; set; }
        public string GMDescription { get; set; }
        public int? ParentGMID { get; set; }
        public char Remarks { get; set; }
        public int? IsCompany { get; set; }
        public string CompanyCode { get; set; }
        public string LegalName { get; set; }
        #endregion
    }
}