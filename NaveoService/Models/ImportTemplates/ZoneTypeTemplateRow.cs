﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Models.ImportTemplates
{
    public class ZoneTypeTemplateRow
    {
        #region Zone Type Template Row
        public int ZoneTypeId { get; set; }
        public string Description { get; set; }
        public string Comments { get; set; }
        public string IsSystem { get; set; }
        #endregion
    }
}