﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Models.ImportTemplates
{
    public class ZoneHeaderTypeRowTemplate
    {
        #region Zone Header Zone Type Row Template
        public int ZoneHeadID { get; set; }
        public int ZoneTypeID { get; set; }
        public int Id { get; set; }
        #endregion    
    }
}