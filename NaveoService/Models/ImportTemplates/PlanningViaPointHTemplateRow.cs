﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Models.ImportTemplates
{
    public class PlanningViaPointHTemplateRow
    {
        #region Planning Via Point H Template Row
        public int Id { get; set; }
        public int PID { get; set; }
        public int SequenceNo { get; set; }
        public int? ZoneID { get; set; }
        public float? Latitude { get; set; }
        public float? Longitude { get; set; }
        public int? LocalityVCA { get; set; }
        public int? Buffer { get; set; }
        public string Remarks { get; set; }
        public DateTime? dtUTC { get; set; }
        public int? ClientId { get; set; }
        #endregion
    }
}