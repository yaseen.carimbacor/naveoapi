﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Models.ImportTemplates
{
    public class PlanningTemplateRow : ITemplateRow
    {
        #region Planning Template Row
        public int RowNumber { get; set; }
        public int PID { get; set; }
        public string Department { get; set; }
        public string Remarks { get; set; }
        public int Urgent { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime StartTime { get; set; }
        public string ViaPoints { get; set; }
        public int Buffer { get; set; }
        public string UOM { get; set; }
        public string Capacity { get; set; }
        public string Remarks2 { get; set; }
        public string Resource { get; set; }
        public string Vehicle { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }

        #endregion


    }




   


}