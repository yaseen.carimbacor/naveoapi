﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Models.ImportTemplates
{
    public class ObjectTemplateRow
    {
        public AssetTemplateRow Asset { get; set; }
        public DriverTemplateRow Driver { get; set; }
        public GroupTemplateRow Group { get; set; }
        public ZoneTemplateRow Zone { get; set; }


    }

}
