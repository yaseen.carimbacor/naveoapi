﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Models.ImportTemplates
{
    public class TripHeaderTemplateRow
    {
        #region Trip Header Template Row
        public int Id { get; set; }
        public int AssetId { get; set; }
        public int DriverId { get; set; }
        public int ExceptionFlag { get; set; }
        public int MaxSpeed { get; set; }
        public int IdleTime { get; set; }
        public int StopTime { get; set; }
        public int TripDistance { get; set; }
        public int TripTime { get; set; }
        public int GPSDataStartUID { get; set; }
        public int GPSDataEndUID { get; set; }
        public int Start { get; set; }
        public int StartLongitude { get; set; }
        public int StartLatitude { get; set; }
        public int End { get; set; }
        public int EndLongitude { get; set; }
        public int EndtLatitude { get; set; }
        public int OverSpeedTime1 { get; set; }
        public int OverSpeedTime2 { get; set; }
        public int Acceleration { get; set; }
        public int Brake { get; set; }
        public int Corner { get; set; }
        #endregion
    }
}