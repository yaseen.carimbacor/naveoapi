﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Models.ImportTemplates
{
    public class DriverTemplateRow //: ITemplateRow
    {
        #region Driver
        public int RowNumber { get; set; }
        public int DriverID { get; set; }
        public string DriverName { get; set; }
        public string Button { get; set; }
        public string EmployeeNo { get; set; }
        public string Comments { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string Nationality { get; set; }
        public string sFirtName { get; set; }
        public string LicenseNo1 { get; set; }
        public string LicenseNo2 { get; set; }
        public DateTime LicenseNo1Expiry { get; set; }
        public DateTime Licenseno2Expiry { get; set; }
        public string Ref1 { get; set; }
        public int? ZoneID { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int? UpdatedBy { get; set; }
        public string Status { get; set; }
        public string District { get; set; }
        public string PostalCode { get; set; }
        public string MobileNumber { get; set; }
        public string Reason { get; set; }
        public string Department { get; set; }
        public string Email { get; set; }
        public string LastName { get; set; }
        public string OfficerLevel { get; set; }
        public string OfficerTitle { get; set; }
        public string Phone { get; set; }
        public string Title { get; set; }
        public string ASPUser { get; set; }
        public int? UserId { get; set; }
        public byte?[] Photo { get; set; }
        public int? InstitutionType { get; set; }
        public int? DriverType { get; set; }
        public int? GradeType { get; set; }
        public int? HomeNo { get; set; }
        public string EmployeeType { get; set; }

        #endregion
    }
}