﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Models.ImportTemplates
{
    public class PlanningLookUpTemplateRow
    {
        #region Planning LookUp Template Row
        public int Id { get; set; }
        public int PID { get; set; }
        public int LookupValueID { get; set; }
        public string LookupType { get; set; }
        #endregion
    }

}