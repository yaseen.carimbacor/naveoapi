﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Models.ImportTemplates
{
    /// <summary>
    /// User template row for upload
    /// 'internal' used so as to be able to target only needed properties for construction of datatable
    /// </summary>
    public class UserTemplateRow : ITemplateRow 
    {
        public int RowNumber { get; set; }
        public int UID { get; set; }
        internal string Username { get; set; }
        internal string Names { get; set; }
        internal string LastName { get; set; }
        internal string Email { get; set; }
        internal string Tel { get; set; }
        internal string MobileNo { get; set; }
        public DateTime? DateJoined { get; set; }
        internal string Status { get; set; }
        internal string Password { get; set; }
        internal string AccessList { get; set; }
        internal string StoreUnit { get; set; }
        internal string UType_cbo { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string Dummy { get; set; }
        public int LoginCount { get; set; }
        public int AccessTemplateId { get; set; }
        public int LoginAttempt { get; set; }
        public string PreviousPassword { get; set; }
        public DateTime PasswordUpdateddate { get; set; }
        internal int MaxDayMail { get; set; }
        internal int UserTypeID { get; set; }
        internal string UserTypeDescription { get; set; }
        internal int? ZoneID { get; set; }
        internal int? PlanningLookUpValue { get; set; }
        internal int? MobileAccess { get; set; }
        internal int? ExternalAccess { get; set; }
        internal int? TwoStepVerif { get; set; }
        internal int? TwoStepAttempts { get; set; }
        public Guid UserToken { get; set; }
        public DateTime? PwdExpDate { get; set; }
        public DateTime? PwdExpWarningDate { get; set; }
    }
}
