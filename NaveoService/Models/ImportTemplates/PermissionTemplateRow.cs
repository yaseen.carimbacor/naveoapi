﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Models.ImportTemplates
{
    public class PermissionTemplateRow
    {
        #region PermissionTemplateRow
        public int Id { get; set; }
        public string Description { get; set; }
        public int ModuleId { get; set; }
        #endregion
    }
}