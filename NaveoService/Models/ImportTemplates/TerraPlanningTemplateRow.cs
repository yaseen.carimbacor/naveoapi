﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Models.ImportTemplates
{
  
    public class TerraPlanningTemplateRow : ITemplateRow
    {
        public int RowNumber { get; set; }
        public int PID { get; set; }
        public DateTime Date { get; set; }
        public string Asset { get; set; }
        public string OperationType { get; set; }
        public string ToolType { get; set; }
        public string FieldNo { get; set; }
        public DateTime Time { get; set; }
        public string Driver { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }


}