﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Models.ImportTemplates
{
    public class ZoneTemplateRow
    {
        #region Zone Template Row
        public int RowNumber { get; set; }
        public string TextString { get; set; }
        public double Area { get; set; }
        public double Perimeter { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string ZoneName { get; set; }
        public string LookUpValue { get; set; }

        public string HouseHold { get; set; }

        public Double Radius { get; set; }
        #endregion
    }
}