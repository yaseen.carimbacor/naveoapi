﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Models.ImportTemplates
{
    public class NaveoModuleTemplateRow
    {
        #region Naveo Module Template Row
        public int Id { get; set; }
        public string Description { get; set; }
        #endregion
    }
}