﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Models.ImportTemplates
{
    public class ZoneDetailTemplateRow
    {
        #region Zone Detail Template Row
        public int Id { get; set; }
        public float? Latitude { get; set; }
        public float? Longitude { get; set; }
        public int? ZoneID { get; set; }
        public int? CordOrder { get; set; }
        #endregion
    }
}