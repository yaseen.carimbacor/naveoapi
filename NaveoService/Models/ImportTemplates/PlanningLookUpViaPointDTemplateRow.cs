﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Models.ImportTemplates
{
    public class PlanningViaPointDTemplateRow
    {
        #region Planning Via Point D Template Row
        public int Id { get; set; }
        public int HID { get; set; }
        public int? UOM { get; set; }
        public int? Capacity { get; set; }
        public string Remarks { get; set; }
        #endregion

    }
}