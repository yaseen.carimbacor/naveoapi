﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Models.ImportTemplates
{
    public class TripDetailTemplateRow
    {
        #region Trip Detail Template Row
        public int ID { get; set; }
        public int HeaderId { get; set; }
        public int GPSDataUID { get; set; }
        #endregion
    }
}