﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Models.ImportTemplates
{
    public class ModuleTemplateRow
    {
        #region Module Template Row
        public int RowNumber { get; set; }
        public int UID { get; set; }
        public string ModuleName { get; set; }
        public string Description { get; set; }
        public string Command { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public int? MenuHeaderId { get; set; }
        public int? MenuGroupId { get; set; }
        public int? OrderIndex { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        #endregion
    }
}