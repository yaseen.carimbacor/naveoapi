﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Models.ImportTemplates
{
    public class ZoneHeaderTemplateRow
    {
        #region Zone Header Template Row
        public int ZoneID { get; set; }
        public string Description { get; set; }
        public int Displayed { get; set; }
        public string Comments { get; set; }
        public int Color { get; set; }
        public string ExternalRef { get; set; }
        public string Ref { get; set; }
        public int IsMarker { get; set; }
        public int Buffer { get; set; }
        #endregion
    }
}