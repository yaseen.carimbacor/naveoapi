﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Models.ImportTemplates
{
    public class RolePermissionTemplateRow
    {
        #region RolePermissionTemplateRow
        public int RoleId { get; set; }
        public int PermissionId { get; set; }
        public int? Create { get; set; }
        public int? Read { get; set; }
        public int? Update { get; set; }
        public int? Delete { get; set; }
        #endregion
    }
}