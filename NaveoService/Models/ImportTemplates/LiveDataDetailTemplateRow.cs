﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Models.ImportTemplates
{
    public class LiveDataDetailTemplateRow
    {
        #region Live Dat aDetail Template Row
        public int GPSDetailId { get; set; }
        public int? UID { get; set; }
        public int TypeId { get; set; }
        public int TypeValue { get; set; }
        public int UOM { get; set; }
        public int Remarks { get; set; }
        #endregion
    }
}