﻿using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Models.GMatrix;
using NaveoService.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;

namespace NaveoService.Models
{
    public class GroupMatrixViewModel : GroupMatrix
    {
        public int naveoModuleId { get; set; }
        public string naveoModule { get; set; }
        public string groupMatrixJson { get; set; }
        public List<NaveoModule> naveoModules { get; set; }


    }
}