﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Models
{
    public class Export
    {
        public List<int> assetIds { get; set; }
        public DateTime dateFrom { get; set; }
        public DateTime dateTo { get; set; }
    }

    public class FuelExport : Export
    {
        public string a { get; set; }
        public string b { get; set; }


    }
}
