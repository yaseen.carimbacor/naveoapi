﻿using NaveoOneLib.Models.Drivers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Models
{
    public class Resource
    {
        
        public List<int> resourceIds { get; set; }
        public string resourceType { get; set; }
        public List<String> sortColumns { get; set; } = new List<String>();
        public Boolean sortOrderAsc { get; set; } = true;
        
    }

 
}
