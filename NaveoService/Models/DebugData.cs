﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace NaveoService.Models
{
    public class DebugData
    {
        public DataTable dtData { get; set; }
        public int rowCount { get; set; }
    }
}