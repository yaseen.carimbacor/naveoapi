﻿using NaveoOneLib.Models.GMatrix;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Models
{
    public class Planning
    {
        public int? PID { get; set; }
        public int scheduledID { get; set; }
        public DateTime dateFrom { get; set; }
        public DateTime dateTo { get; set; }
        public List<String> sortColumns { get; set; } = new List<String>();
        public Boolean sortOrderAsc { get; set; } = false;
        public List<int> assetdIds { get; set; } = new List<int>();
        public string ParentRequestCode { get; set; }
        public int assetId { get; set; }

        public string statusApproval { get; set; }

        public int fieldCode { get; set; }

        public List<int> lpids { get; set; } = new List<int>();

        public List<int> assetIds { get; set; } = new List<int>();

        public List<Matrix> lm { get; set; } = new List<Matrix>();


    }

    public class GetPlanningUserEmailApi
    { 
        public bool cancel { get; set; }
        public int assetID { get; set; }
        public int driverID { get; set; }
        public List<GetPlanningUserEmailApiData> lDetails { get; set; }

    }

    public class GetPlanningUserEmailApiData{
       
        public string email { get; set; }

        public string viaPointName { get; set; }
        public string viaPointTime { get; set; }
    }

}
