﻿using NaveoService.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace NaveoService.Models.Custom
{
    public class TripData
    {
        public MinifiedAsset vehicle { get; set; } 
        public MinifiedDriver driver { get; set; }
        public DateTime from { get; set; }
        public TimeSpan tripTime { get; set; }
        public DateTime to { get; set; }
        public double tripDistance { get; set; }
        public TimeSpan stopTime { get; set; }
        public string departureAddress { get; set; }
        public string arrivalAddress { get; set; }
        public Zone departureZone { get; set; }
        public Zone arrivalZone { get; set; }
        public TimeSpan idlingTime { get; set; }
        public int maxSpeed { get; set; }
        public int iID { get; set; }
        public int accel { get; set; }
        public int brake { get; set; }
        public int corner { get; set; }
        public string iColor{ get; set; }
        public TimeSpan overSpeed1Time{ get; set; }
        public TimeSpan overSpeed2Time { get; set; }
        public int rowNumPerPage { get; set; }
    }

    public class tripDataOdo
    {
        public DataTable odoData { get; set; }
    }

    public class pivotDataRow
    {
        public int rowNum { get; set; }
        public String rowDesc { get; set; }
        public List<object> colData { get; set; }
    }
    
    public class pivotData
    {
        public List<pivotDataRow> lData { get; set; }
        public List<String> lColumnNames { get; set; }
    }
}