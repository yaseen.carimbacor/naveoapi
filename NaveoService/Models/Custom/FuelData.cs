﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Models.Custom
{
  public  class FuelData
    {
        public int assetId { get; set; }
        public int convertedValue { get; set; }
        public String fType { get; set; }
        public String uom { get; set; }
        public int gpsdataId { get; set; }
        public DateTime dtLocalTime { get; set; }
        public Double lat { get; set; }
        public Double lon { get; set; }
        public String description { get; set; }


    }
}
