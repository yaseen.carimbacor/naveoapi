﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Models.Custom
{
    public class apiDataLive
    {
        public List<LiveData> liveData { get; set; } = new List<LiveData>();
    }

    public class LiveData
    {
        public MinifiedAsset vehicle { get; set; } = new MinifiedAsset();
        public MinifiedDriver driver { get; set; }
        public MinifiedZone zone { get; set; }

        public int isGPSValid { get; set; }
        public int speed { get; set; }
        public Double latitude { get; set; }
        public Double longitude { get; set; }
        public String address { get; set; }

        public DateTime dtLastReported { get; set; }
        public DateTime dtGPS { get; set; }
        public String activity { get; set; }
        public FuelData lastFueldata { get; set; }

        public int MaintenanceStatus { get; set; }

        public decimal LastOdometer { get; set; }

    }
}


    