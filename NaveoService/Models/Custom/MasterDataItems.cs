﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace NaveoService.Models.Custom
{
    public class MasterDataItems
    {
        public DataTable masterDataItems { get; set; }
    }

    public class Data
    {
        public DataTable data { get; set; }
    }

    public class fuelDataItems
    {
        public DataTable fuelData { get; set; }
    }

    public class dsDataItems
    {
        public DataSet dsData { get; set; }
    }
}