﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Models.Custom
{
    public class HeartBeatResult
    {
        public bool hasDownloads { get; set; }
        public List<string> errorList { get; set; }

        #region Constructors

        // Default
        public HeartBeatResult()
        {
        }

        public HeartBeatResult(bool _hasDownloads)
        {
            hasDownloads = _hasDownloads;
        }

        #endregion
    }
}
