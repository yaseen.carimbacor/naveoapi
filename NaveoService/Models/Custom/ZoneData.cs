﻿using NaveoOneLib.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace NaveoService.Models.Custom
{
    public class ZoneData
    {
        public DataTable dtZones { get; set; }
        public List<NaveoService.Models.Zone> zoneList { get; set; }
    }
}