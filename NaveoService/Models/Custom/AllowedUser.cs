﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Models.Custom
{
    public class AllowedUser
    {
        //public AuthToken authenticationToken { get; set; }
        public Guid issuedTo { get; set; }  
        public DateTime issuedDate { get; set; }
        public DateTime expiryDate { get; set; }
        public DateTime accessedAt { get; set; }
        public Guid oneTimeToken { get; set; }
        public String sEmail { get; set; }
        public int UserID { get; set; } 

        #region constructor
        public AllowedUser(Guid authToken, int UserID, Guid oneTime, String sEmail)
        {
            var dateNow = DateTime.Now;

            this.issuedTo = authToken;
            this.UserID = UserID;
            this.issuedDate = dateNow;
            this.accessedAt = dateNow;
            this.expiryDate = dateNow.AddHours(24);
            this.oneTimeToken = oneTime;
            this.sEmail = sEmail;
        }
        #endregion
    }
}