﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Models.Custom
{
    public class MinifiedObjects
    {

    }
    public class MinifiedAsset
    {
        public int assetID { get; set; }
        public String description { get; set; }
    }
    public class MinifiedDriver
    {
        public int driverID { get; set; }
        public String description { get; set; }
    }
    public class MinifiedZone
    {
        public int zoneID { get; set; }
        public String description { get; set; }
        public String sColor { get; set; }
    }

    public class MinifiedUser
    {
        public int? userID { get; set; }
        public string description { get; set;}
    }

    public class minifiedValues
    {
        public int id { get; set; }
        public string description { get; set; }
    }


}