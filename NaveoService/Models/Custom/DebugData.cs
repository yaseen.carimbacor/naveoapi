﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Models.Custom
{
    public class DbugData
    {
        public List<apiDebugData> debugData { get; set; }
    }
    public class apiDebugData
    {
        public int rowNum { get; set; }
        public int uid { get; set; }
        public MinifiedAsset asset { get; set; }
        public MinifiedDriver driver { get; set; }
        public DateTime dateTimeGPS_UTC { get; set; }
        public DateTime dateTimeServer { get; set; }
        public Double longitude { get; set; }
        public Double latitude { get; set; }
        public int longLatValidFlag { get; set; }
        public int speed { get; set; }
        public int engineOn { get; set; }
        public int stopFlag { get; set; }
        public Double tripDistance { get; set; }
        public string tripTime { get; set; }
        public int workHour { get; set; }
        public int? roadSpeed { get; set; }
        public List<details> ldetails { get; set; }
    }

    public class details
    {
        public String type { get; set; }
        public String value { get; set; }
        public String Unit { get; set; }
    }
}