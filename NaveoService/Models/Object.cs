﻿using NaveoOneLib.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace NaveoService.Models
{
    public class Object
    {
        public DataTable groups { get; set; }
        public DataTable assets { get; set; }
        public DataTable drivers { get; set; }
        public List<Zone> zones { get; set; }
    }
}