﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Models
{
    public class Monitoring
    {
        public class clsMonitoring
        {
            public DateTime? DateTimeStart { get; set; }

            public DateTime? DateTimeEnd { get; set; }

        }
    }
}
