﻿using NaveoOneLib.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace NaveoService.Models
{
    public class Live
    {
        public int assetId { get; set; }
        public int driverId { get; set; }
        public int? zoneId { get; set; }
        public int isGPSValid { get; set; }
        public int speed { get; set; }
        public Double latitude { get; set; }
        public Double longitude { get; set; }
        public String address { get; set; }

        public DateTime dtLastReported { get; set; }
        public DateTime dtGPS { get; set; }
        public String activity { get; set; }
    }

    public class liveAssets
    {
        public List<int> assetIds { get; set; }
        public List<String> lType { get; set; }

        public Boolean byDriver { get; set; } = false;
    }
}