﻿using NaveoOneLib.Models.Permissions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Models
{
    public class SecurityTokenExtended
    {
        public SecurityToken securityToken { get; set; } = new SecurityToken();

        public SecurityTokenExtended()
        {

        }
        public SecurityTokenExtended(bool isAuthorized)
        {
            securityToken.IsAuthorized = isAuthorized;
        }

        public SecurityTokenExtended(Guid oneTimeToken)
        {
            securityToken.OneTimeToken = oneTimeToken;
        }
    }
}
