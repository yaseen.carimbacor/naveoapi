﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Models
{
    public class Roster
    {

        public int GRID { get; set; }
        public List<String> sortColumns { get; set; } = new List<String>();
        public Boolean sortOrderAsc { get; set; } = true;
        public DateTime dateFrom { get; set; }
        public DateTime dateTo { get; set; }


    }
}
