﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Constants
{
    public static class NaveoModules
    {
        #region Module Names
        private const string ADMIN = "Admin";
        private const string FLEET = "Fleet";
        private const string MAINTENANCE = "Maintenance";
        #endregion

        public static List<String> controllerList = new List<String>() { ADMIN, FLEET, MAINTENANCE };
    }
}