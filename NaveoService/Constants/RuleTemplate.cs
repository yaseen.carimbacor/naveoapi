﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Constants
{
    public static class RuleTemplate
    {


        public const int Engine_Rule = 0;
        public const string Engine_Rule_Desc = "Engine Rule";

        public const int Drving_Rule = 1;
        public const string Driving_Rule_Desc = "Driving Rule";


        public const int Planning_Rule = 2;
        public const string Planning_Rule_Desc = "Planning Rule";

        public const int AssetMaint_Rule = 3;
        public const string AssetMaint_Rule_Desc = "Maintenance Rule";

      
        public const string FuelFill_Rule_Desc = "Fill";
        public const string FuelDrop_Rule_Desc = "Drop";

        public const int RuleType_ZoneTypes = 0;
        public const string Struc_ZoneTypes = "ZoneTypes";

        public const int RuleType_Zones = 1;
        public const string Struc_Zones = "Zones";

        public const int RuleType_Assets = 2;
        public const string Struct_Assets = "Assets";

        public const int RuleType_Drivers = 3;
        public const string Struc_Drivers = "Drivers";

        public const int RuleType_Speed = 4;
        public const string Struc_Speed = "Speed";

        public const int RuleType_Time = 5;
        public const string Struc_Time = "Time";

        public const int RuleType_Aux = 6;
        public const string Struc_Aux = "Aux";

        public const int RuleType_EngineAux = 6;
        public const string Struc_EngineAux = "EngineAux";

        public const int RuleType_Maintenance = 6;
        public const string Struc_Maintenance = "Maintenance";

        public const int RuleType_Duration = 7;
        public const string Struc_Duration = "Duration";

        public const int ExceptionType = 8;
        public const string Exception_Type_Desc = "Exception Type";

        public const int RuleType_Fuel = 9;
        public const string Struc_Fuel = "Fuel";

        public const int RuleType_PostedRoadSpeed = 10;
        public const string Struc_PostedRoadSpeed = "RoadSpeed";

        public const int RuleType_Day = 11;
        public const string Struc_Day = "Day";

        public const int RuleType_Planning = 12;
        public const string Struc_Planning = "Planning";

        public const int RuleType_AssetMaintenances = 13;
        public const string Struc_AssetMaintenances = "AssetMaintenance";

    }
}
