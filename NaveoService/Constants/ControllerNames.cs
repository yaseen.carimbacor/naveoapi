﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Constants
{
    public static class ControllerNames
    {
        #region Controller Names
        /// <summary>
        /// User Controller
        /// </summary>
        private const string USER = "User";
        /// <summary>
        /// Asset Controller
        /// </summary>
        private const string ASSET = "Asset";
        /// <summary>
        /// Dashboard Controller
        /// </summary>
        private const string DASHBOARD = "Dashboard";
        /// <summary>
        /// Home Controller
        /// </summary>
        private const string HOME = "Home";
        /// <summary>
        /// Matrix Controller
        /// </summary>
        private const string MATRIX = "Matrix";
        /// <summary>
        /// Tree Controller
        /// </summary>
        private const string TREE = "Tree";
        /// <summary>
        /// Zone Controller
        /// </summary>
        private const string ZONE = "Zone";

        #endregion

        public static List<String> controllerList = new List<String>() { USER, ASSET, DASHBOARD, HOME, MATRIX, TREE, ZONE };

    }
}