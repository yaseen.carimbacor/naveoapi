﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Constants
{
    public struct FileSystem
    {
        /// <summary>
        /// Extension structure
        /// </summary>
        public struct Extension
        {
            public const string XLSX = ".xlsx";
            public const string PDF = ".pdf";
            public const string CSV = ".csv";
            public const string ZIP = ".zip";
            public const string TXT = ".txt";
        }

        /// <summary>
        /// Content Type Structure
        /// </summary>
        public struct ContentType
        {
            public const string ZIP = "application/zip";
            public const string XLSX = "application/vnd.ms-excel"; // application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
            public const string PDF = "application/pdf";
            public const string DEFAULT = "application/octet-stream";
        }
    }
}
