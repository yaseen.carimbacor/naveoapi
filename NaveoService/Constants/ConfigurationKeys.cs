﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Constants
{
    /// <summary>
    /// Configuration keys
    /// </summary>
    /// TODO: Put all config keys in this class
    public static class ConfigurationKeys
    {
        public const string ExportLocation = "ExportLocation";
        public const string ImportLocation = "ImportLocation";
        public const string ExportTemplatesLocation = "ExportTemplatesLocation";
        public const string ExportFileName_DateTime_Convention = "yyyy-MM-dd_HHmmss";
        public const string uploadFoto = "UploadPhotoLocation";

    }
}