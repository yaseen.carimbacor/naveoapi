﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Constants
{
    /// <summary>
    /// Account Status
    /// </summary>
    public class AccountStatus
    {
        // TODO: Change the constant names as per real meaning
        public const string ChangePassword = "CP";
        public const string Active = "AC";
        public const string Inactive = "IN";
    }
}