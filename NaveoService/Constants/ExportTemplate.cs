﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Constants
{
    public struct ExportTemplate
    {
        public const string Trip = "Trip_Export_Template.xlsx";
        public const string Live = "Live_Export_Template.xlsx";
        public const string AssetSummaryOutsideHO = "AssetSummaryOutsideHO_Export_Template.xlsx";
        public const string Exception = "Exception_Export_Template.xlsx";
        public const string DailySummarizedTrip = "DailySummarizedTrip_Export_Template.xlsx";
        public const string Fuel = "Fuel_Export_Template.xlsx";
        public const string User = "User_Export_Template.xlsx";
        public const string DailyTripTime = "DailyTripTime_Export_Template.xlsx";
        public const string SummarizedTrip = "SummarizedTrip_ExportTemplate.xlsx";
        public const string ZoneVisited = "ZoneVisited_NotVisited_Export_Template.xlsx";
        public const string Temperature = "Temperature.xlsx";
        public const string SensorData = "Sensor_Data_Template.xlsx";
        public const string PlanningData = "Planning_Request_Template.xlsx";
        public const string ScheduledData = "Schedule_Request_Template.xlsx";
        public const string FuelReport = "FuelReport_Export_Template.xlsx";
        public const string TerraPlanningScheduledData = "TerraPlanning_Schedule_Request_Template.xlsx";
        public const string CustomizedTripReport = "Customized_Report_Template.xlsx";
        public const string Shift = "Shift_Export_Template.xlsx";
        public const string Roster = "Roster_Export_Template.xlsx";
        public const string PlanningExceptions = "Planning_Exceptions_Template.xlsx";
        public const string FuelExceptions = "Fuel_Exceptions_Template.xlsx";
        public const string MaintenanceCost = "Maintenance_Cost_Template.xlsx";
    }
}