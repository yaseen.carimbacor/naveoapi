﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Constants
{
    /// <summary>
    /// Enums
    /// </summary>
    public class Enums
    {
        /// <summary>
        /// Roles 
        /// </summary>
        public enum Roles
        {
            // TODO: Should be replaced by exact roles 
            Admin = 0,
            SuperAdmin = 1,
            Viewer = 2,
            Salesman = 3
        }

        /// <summary>
        /// CRUD operations
        /// </summary>
        public enum OperationAllowed
        {
            No = 0,
            Yes = 1,
        }

        /// <summary>
        /// Access through mobile 
        /// </summary>
        public enum MobileAccessAllowed
        {
            No = 0,
            Yes = 1,
        }
    }

    [Flags]
    public enum UsersXlsxColumns
    {
        UID = 1,
        Username = 2,
        Names = 3,
        LastName = 4,
        Email = 5,
        Tel = 6,
        MobileNo = 7,
        Status = 8,
        Password = 9,
        AccessList = 10,
        StoreUnit = 11,
        UType_cbo = 12,
        MaxDayMail = 13,
        UserTypeID = 14,
        UserTypeDescription = 15,
        ZoneID = 16,
        PlanningLookUpValue = 17,
        MobileAccess = 18,
        ExternalAccess = 19,
        TwoStepVerif = 20,
        TwoStepAttempts = 21,
    }

    [Flags]
    public enum PlanningXlsxColumns
    {
        PID = 1,
        Department = 2,
        Remarks1 = 3,
        IsUrgent = 4,
        StartDate = 5,
        StartTime = 6,
        ViaPoints = 7,
        Buffer = 8,
        UOM = 9,
        Capacity = 10,
        Remarks2 = 11,
        Resource = 12,
        Vehicel = 13

    }

    [Flags]
    public enum ZonesXlsxColumns
    {
        ZoneName = 1,
        TextString = 2,
        Area = 3,
        Perimeter = 4,
        Latitude = 5,
        Longitude = 6,
        LookUpValue = 7,
        HouseHold = 8,
        Radius = 9,


    }

    /// <summary>
    /// Trips excel columns
    /// </summary>
    public enum TripsXlsxColumns
    {
        //TripId = 1,
        //VehicleId = 2,
        //VehicleName = 3,
        //driverId = 4,
        //DriverName = 5,
        //DepartureZone = 6,
        //DepartureAddress = 7,
        //StartDate = 8,
        //TripTime = 9,
        //EndDate = 10,
        //TripDistance = 11,
        //StopTime = 12,
        //IdleTime = 13,
        //ArrivalZone = 14,
        //ArrivalAddress = 15,
        //MaximumSpeed = 16,
        //Acceleration = 17,
        //Brake = 18,
        //Corner = 19,
        //SpeedingTime1 = 20,
        //SpeedingTime2 = 21

        TripId = 1,
        AssetId = 2,
        AssetName = 3,
        DepartureAddress = 4,
        DepartureZone = 5,
        ArrivalAddress = 6,
        ArrivalZone = 7,
        StartDate = 8,
        EndDate = 9,
        MaxSpeed = 10,
        TripDistance = 11,
        Odometer = 12,
        EngineHours = 13,
        TripTime = 14,
        StopTime = 15,
        IdleTime = 16,
        DriverId = 17,
        DriverName = 18,
        Alerts = 19,
        Acceleration = 20,
        Brake = 21,
        Corner = 22,
        SpeedingTime1 = 23,
        SpeedingTime2 = 24

    }

    /// <summary>
    /// Live data excel columns
    /// </summary>
    public enum LiveDataXlsxColumns
    {
        //UID = 1,
        //AssetID = 2,
        //AssetNum = 3,
        //DriverId = 4,
        //Availability = 5,
        //Latitude = 6,
        //Longitude = 7,
        //RoadSpeed = 8,
        //Speed = 9,
        //TripDistance = 10,
        //TripTime = 11,
        //GPSDetailID = 12,
        //Remarks = 13,
        //TypeID = 14,
        //TypeValue = 15,
        //UOM = 16,
        //WorkHour = 17,
        //StopFlag = 18,
        ////DateTimeGPS_UTC = 18,
        ////DateTimeServer = 19,
        ////DateTimeTimeZone = 20,
        //DeviceID = 19,
        //DirectionImage = 20,
        //EngineOn = 21,
        //iButton = 22,
        //LastReportedInLocalTime = 23,
        //LongLatValidFlag = 24

        //AssetID =1,
        //Asset = 2,
        //Driver =3,
        //Zone = 4,
        //Speed =5,
        //Latitude =6,
        //Longitude =7,
        //Address = 8,
        //Last_Reported_Date =9,
        //Time =10,
        //GPS_Date =11,
        //Activity=12,
        //Track =13

        Status = 1,
        AssetID = 2,
        Asset = 3,
        Driver = 4,
        Zone = 5,
        Speed = 6,
        Latitude = 7,
        Longitude = 8,
        Address = 9,
        Last_Reported_Date = 10,
        Time = 11,
        GPS_Date = 12,
        Fuel = 13,
        Track = 14,
    }
    /// <summary>
    /// DailySummarizedTrips
    /// </summary>

    public enum GetDailySummarizedTripsXlsxColumns
    {
        date = 1,
        registrationNo = 2,
        driver = 3,
        trips = 4,
        startTime = 5,
        endTime = 6,
        kmTravelled = 7,
        idlingTime = 8,
        //drivingTime = 10,
        inZoneStopTime = 9,
        outZoneStopTime = 10,

    }

    /// <summary>
    /// SummarizedTrips
    /// </summary>
    public enum GetSummarizedTripsXlsxColumns
    {

        RegistrationNo = 1,
        Driver = 2,
        MaxSpeed = 3,
        KmTravelled = 4,
        CurrentOdometer = 5,
        Stops = 6,
        StopLess30min = 7,
        StopLess60min = 8,
        StopMore60min = 9,
        StopLess5Hrs = 10,
        StopMore5Hrs = 11,
        Trips = 12,
        DrivingTime = 13,
        IdlingTime = 14,
        SpeedMore70Kms = 15,
        SpeedMore80Kms = 16,
        SpeedMore110Kms = 17,
        fuelcost = 18,
        costperkm = 19,
        fuelinlitres = 20,
        fuelinlitresper100 = 21,
        totalcost = 22,
        totalcostper100 = 23

    }

    public enum GetDailyTripTimeXlsxColumns
    {

        AssetName = 1

    }

    public enum GetExceptions
    {
        ruleName = 1,
        assetName = 2,
        site = 3,
        speed = 4,
        postRoadSpeed = 5,
        Date = 6,
        Time = 7,
        address = 8,
        duration = 9,
        zone = 10,
        driverName = 11,

    }

    public enum GetFuels
    {
        GpsUID = 1,
        AssetID = 2,
        DateTimeGPS_UTC = 3,
        ConvertedValue = 4,
        Uom = 5,
        Ftype = 6,
        Diff = 7,
        Lon = 8,
        Lat = 9

    }

    public enum GetZoneVisited
    {
        date = 1,
        assetName = 2,
        zone = 3,
        arrivalTime = 4,
        depatureTime = 5,
        durationInZones = 6,

    }


    public enum GetTemperature
    {
        asset = 1,
        driver = 2,
        date = 3,
        speed = 4

    }

    public enum GetSensorData
    {

        uid = 1,
        Asset = 2,
        AssetId = 3,
        Driver = 4,
        Date = 5,
        Longitude = 6,
        Lattitude = 7,
        LonLatValidFlag = 8,
        RoadSpeed = 9,
        Speed = 10,
        StopFlag = 11,
        TripDistance = 12,
        TripTime = 13,
        WorkHour = 14,



    }

    public enum getScheduleData
    {
        pId = 1,
        scheduleId = 2,
        requestDate = 3,
        dateCreated = 4,
        startTime = 5,
        endTime = 6,
        departure = 7,
        arrival = 8,
        asset = 9,
        driver = 10,
        status = 11

    }


    public enum fuelreport
    {
        assetId = 1,
        asset = 2,
        dateLastFill = 3,
        LitresLastFill = 4,
        kmTravelled = 5,
        AvgConsumptionLastFill = 6,
        AvgConsumption = 7,
        VechType = 8,
        make = 9,
        model = 10,
        drops = 11,
        acceleration = 12

    }

    // TODO: Put all enums in this class

}