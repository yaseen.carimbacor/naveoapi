﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Constants
{
    public struct ImportTemplate
    {
        public const string Planning_Schedule = "Planning_Scheduling_Import_Template.zip";
        public const string Zone = "Zone_Import_Template.zip";


    }
}