﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoService.Utility
{
    public static class Constant
    {
        public static String LoginUser = "LoginUser";
        public static String LoginUID = "LoginUID";
        public static String sConnStr = "sConnStr";

        public static string MATRIX = "MATRIX";

        public static string MATRIX_NAVEO = "MATRIX_NAVEO";

        public static string PASSWORD_EXP_START = "PasswordExpStart";

        public static string PASSWORD_EXP_END = "PasswordExpEnd";

        public static string PASSWORD_WARNING = "PasswordWarning";


        public static string FONTAWESOME_MAP = "fa fa-map";

        public static string FONTAWESOME_CAR = "fa fa-car";

        public static string FONTAWESOME_USER = "fa fa-user";

        public static string FONTAWESOME_FOLDER = "fa fa-folder-open-o";

        public static string TREE_ZONE = "ZONE";

        public static string TREE_ASSET = "ASSET";

        public static string TREE_TYPE_PARENT = "PARENT";

        public static string TREE_TYPE_CHILDREN = "CHILDREN";

        public static string TREE_TYPE_MIXED = "MIXED";

        //      public static string TREE_ZONE_TYPE = "ZONE_TYPE";

        //      public static string TREE_ATTENDANT = "ATTENDANT";

        public static string TREE_DRIVER = "DRIVER";

        public static string TREE_OFFICER = "OFFICER";

        //      public static string TREE_LOADER = "LOADER";

        //      public static string TREE_OTHER = "OTHER    ";

        public static string TREE_VCA = "VCA";

        //      public static string TREE_USER = "USER";

        //      public static string TREE_RULE = "RULE";

        public static string Tree_Officer_Attendant = "OfficerAttendant";

        //      public static string CHECKBOX = "CHECKBOX";

        //      public static string DATETIMEPICKER = "DATETIMEPICKER";

        //      public static string COMBOBOX = "COMBOBOX";

        //      public static string TEXTBOX = "TEXTBOX";

        public static string ACTIVE = "Active";

        public static string INACTIVE = "Inactive";

        //      public static string CONTRACTOR_TYPE = "CONTRACTOR_TYPE";

        //      public static string FILLING_TYPE = "FILLING_TYPE";

        //      public static string CITY_NAMES = "CITY"; 

        //      public static string FUEL_TYPE = "FUEL_TYPE";

        //      public static string MAKE_TYPE = "MAKE_TYPE";

        //      public static string VEHICLE_TYPE = "VEHICLE_TYPE";

        //      public static string REQUEST_TYPE = "REQUEST_TYPE";

        //      public static string DISTRICT = "DISTRICT";

        //      public static string ACCIDENT_STATUS = "ACCIDENTSTATUS";

        //      public static string OFFICER_TITLE = "OFFICERTITLE";

        //      public static string DEPARTMENT = "DEPARTMENT";

        //      public static string ENGINE_TYPE = "ENGINE_TYPE";

        //      public static string COMPANY_NAMES = "COMPANY_NAMES";

    }
}