﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoService.Utility
{
    public static class NaveoServiceUtils
    {
        public static DataTable dtLowerColumns(DataTable dt)
        {
            foreach (DataColumn dc in dt.Columns)
                dc.ColumnName = dc.ColumnName.ToLower();
            return dt;
        }
    }
}
