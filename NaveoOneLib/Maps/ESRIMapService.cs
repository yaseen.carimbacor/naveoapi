﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EGIS.ShapeFileLib;
using System.Xml;
using NaveoOneLib.Common;
using System.Drawing;
using System.Data;
using Ionic.Zip;
using System.IO;
using NaveoOneLib.Services;
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.GPS;
using NaveoOneLib.Models.Zones;
using NaveoOneLib.Models.Trips;
using NaveoOneLib.Models.Drivers;
using NaveoOneLib.Services.Zones;
using NaveoOneLib.Services.GPS;

namespace NaveoOneLib.Maps
{
    public class ESRIMapService
    {
        //String strMapPath = Globals.GetAppFolder() + Globals.mpat + @"webMarch2014.egp";
        String strMapPath = Globals.GetNaveoAppFolder("BaseESRIMaps") + @"webMarch2014.egp";
        public EGIS.Controls.SFMap sfMap1 = new EGIS.Controls.SFMap();
        EGIS.Controls.SFMap sfMapAddress = new EGIS.Controls.SFMap();
        HEREAPICall hereAPI = null;
        BINGAPICall bingAPI = null;

        public Boolean LoadMapNeeded()
        {
            return !Globals.bFileExists(strMapPath);
        }
        public void LoadMap()
        {
            String s = String.Empty;
            if (String.IsNullOrEmpty(Globals.sWebSitePath))
                s = Globals.AppPath() + "\\core.core";
            else
                s = Globals.sWebSitePath + "\\core.core";

            using (ZipFile zip = ZipFile.Read(s))
            {
                zip.FlattenFoldersOnExtract = true;
                foreach (ZipEntry z in zip)
                {
                    if (!Globals.bFileExists(Globals.GetNaveoAppFolder("BaseESRIMaps") + Path.GetFileName(z.FileName)))
                        z.ExtractWithPassword(Globals.GetNaveoAppFolder("BaseESRIMaps"), ExtractExistingFileAction.OverwriteSilently, BaseService.strLacle);
                }

                Globals.Maploaded = true;
            }

            LoadWorldMap();
        }
        public void unLoadMap()
        {
            if (Globals.Maploaded)
            {
                File.SetAttributes(Globals.GetNaveoAppFolder("BaseESRIMaps"), FileAttributes.Normal);
                if (Directory.Exists(Globals.GetNaveoAppFolder("BaseESRIMaps")))
                    Globals.DeleteDirectory(Globals.GetNaveoAppFolder("BaseESRIMaps"));
            }
        }
        public static void LoadWorldMap()
        {
            String s = String.Empty;
            if (String.IsNullOrEmpty(Globals.sWebSitePath))
                s = Globals.AppPath() + "\\World.zip";
            else
                s = Globals.sWebSitePath + "\\World.zip";

            using (ZipFile zip = ZipFile.Read(s))
            {
                zip.FlattenFoldersOnExtract = true;
                foreach (ZipEntry z in zip)
                {
                    if (!Globals.bFileExists(Globals.GetNaveoAppFolder("BaseESRIMaps") + Path.GetFileName(z.FileName)))
                        z.Extract(Globals.GetNaveoAppFolder("BaseESRIMaps"), ExtractExistingFileAction.OverwriteSilently);
                }
            }
        }

        public void ReadAddMapFolder()
        {
            String s = Globals.GetNaveoAppFolder("Maps");
            DataTable dt = new DataTable();
            if (Globals.bFileExists(s + "\\Maps.geo"))
            {
                dt = Utils.ReadTxtFile(s + "\\Maps.geo", true);
                dt.Rows.Cast<DataRow>().Where(r => r.ItemArray[0].ToString().Contains("//")).ToList().ForEach(r => r.Delete());
            }

            try
            {
                if (dt.Rows.Count > 0)
                {
                    dt.Columns[1].ColumnName = "iSort";
                    dt.DefaultView.Sort = "iSort desc";
                    dt = dt.DefaultView.ToTable(true);
                }
            }
            catch { }

            Globals.dtAdditionalMapLayers = dt;
        }
        public void ReadProjectNLoadMap()
        {
            ReadProject(strMapPath);
            sfMapAddress = new EGIS.Controls.SFMap();

            String fileName = "World84";
            sfMapAddress.AddShapeFile(Globals.GetNaveoAppFolder("BaseESRIMaps") + fileName + ".shp", fileName, "");

            fileName = "RwandaRoadNetwork84";
            sfMapAddress.AddShapeFile(Globals.GetNaveoAppFolder("BaseESRIMaps") + fileName + ".shp", fileName, "");

            fileName = "MozambiqueRoadNetwork84";
            sfMapAddress.AddShapeFile(Globals.GetNaveoAppFolder("BaseESRIMaps") + fileName + ".shp", fileName, "");

            fileName = "ZambiaRoadNetwork84";
            sfMapAddress.AddShapeFile(Globals.GetNaveoAppFolder("BaseESRIMaps") + fileName + ".shp", fileName, "");
        }
        private void ReadProject(string path)
        {
            try
            {
                if (path.Contains("Documents and Settings"))
                {
                    String Str = System.IO.File.ReadAllText(path);
                    if (Str.Contains("<path>coast</path>"))
                    {
                        Str = Str.Replace("<path>", "<path>" + System.IO.Path.GetDirectoryName(path) + "\\");
                        System.IO.File.WriteAllText(path, Str);
                    }
                }

                if (!Globals.bFileExists(path))
                    LoadMap();

                XmlDocument doc = new XmlDocument();
                doc.Load(path);
                XmlElement prjElement = (XmlElement)doc.GetElementsByTagName("sfproject").Item(0);
                this.sfMap1.ReadXml(prjElement);
            }
            catch
            {
                //shx files not present
                try
                {
                    File.SetAttributes(Globals.GetNaveoAppFolder("BaseESRIMaps"), FileAttributes.Normal);
                    if (Directory.Exists(Globals.GetNaveoAppFolder("BaseESRIMaps")))
                        Globals.DeleteDirectory(Globals.GetNaveoAppFolder("BaseESRIMaps"));
                }
                catch { }
            }
        }

        public void UnloadProject()
        {
            for (int i = 0; i < sfMap1.ShapeFileCount; i++)
            {
                ShapeFile sfd = sfMap1[i];
                sfMap1.RemoveShapeFile(sfd);
                i = i - 1;
                sfd.Close();
                sfd = null;
            }
            sfMap1.Dispose();
        }

        public List<TripHeader> GetZonesOnly(List<TripHeader> lTrips, List<Zone> lz, String sConnStr)
        {
            ZoneService zoneService = new ZoneService();
            foreach (TripHeader t in lTrips)
            {
                foreach (Zone z in lz)//PossiblyIn)
                    if (t.GPSDataStart != null)
                        if (zoneService.IsPointInside(z, new Coordinate(t.GPSDataStart.Latitude, t.GPSDataStart.Longitude), sConnStr))
                        {
                            t.DepartureZoneID = z.zoneID;
                            t.DepartureZone = z.description;
                            break;
                        }
                foreach (Zone z in lz)//PossiblyIn)
                    if (t.GPSDataEnd != null)
                        if (zoneService.IsPointInside(z, new Coordinate(t.GPSDataEnd.Latitude, t.GPSDataEnd.Longitude), sConnStr))
                        {
                            t.ArrivalZoneID = z.zoneID;
                            t.ArrivalZone = z.description;
                            t.ArrivalZoneColor = z.color;
                            break;
                        }
            }

            return lTrips;
        }
        public List<TripHeader> GetZonesOnly(List<TripHeader> lTrips, DataSet dsZones, String sConnStr)
        {
            DataTable dtZoneH = dsZones.Tables["dtZoneH"];
            DataTable dtZoneD = dsZones.Tables["dtZoneD"];
            ZoneService zoneService = new ZoneService();
            foreach (TripHeader t in lTrips)
            {
                foreach (DataRow dr in dtZoneH.Rows)
                {
                    DataRow[] drd = dtZoneD.Select("ZoneID = " + dr["ZoneID"].ToString());
                    Zone z = new Zone();
                    z.zoneID = (int)dr["ZoneID"];
                    z.zoneDetails = new List<ZoneDetail>();
                    foreach (DataRow d in drd)
                    {
                        ZoneDetail zd = new ZoneDetail();
                        zd.latitude = (Double)d["Latitude"];
                        zd.longitude = (Double)d["Longitude"];
                        z.zoneDetails.Add(zd);
                    }

                    int iFound = 0;

                    if (String.IsNullOrEmpty(t.DepartureZone))
                        if (t.GPSDataStart != null)
                            if (zoneService.IsPointInside(z, new Coordinate(t.GPSDataStart.Latitude, t.GPSDataStart.Longitude), sConnStr))
                            {
                                t.DepartureZoneID = z.zoneID;
                                t.DepartureZone = z.description;
                                iFound++;
                            }

                    if (String.IsNullOrEmpty(t.ArrivalZone))
                        if (t.GPSDataEnd != null)
                            if (zoneService.IsPointInside(z, new Coordinate(t.GPSDataEnd.Latitude, t.GPSDataEnd.Longitude), sConnStr))
                            {
                                t.ArrivalZoneID = z.zoneID;
                                t.ArrivalZone = z.description;
                                t.ArrivalZoneColor = z.color;
                                iFound++;
                            }

                    if (iFound >= 2)
                        break;
                }
            }

            return lTrips;
        }

        public List<TripHeader> GetAddress(List<TripHeader> lTrips, List<Zone> lz, String sConnStr)
        {
            Boolean bGetAddressCreated = false;
            if (sfMap1 == null || sfMap1.ShapeFileCount == 0)
            {
                bGetAddressCreated = true;
                ReadProjectNLoadMap();
            }

            System.Data.DataTable dtXY = new System.Data.DataTable();
            dtXY.Columns.Add("TripID");
            dtXY.Columns.Add("Lat");
            dtXY.Columns.Add("Lon");

            ZoneService zoneService = new ZoneService();
            foreach (TripHeader t in lTrips)
            {
                if (t.GPSDataStart != null)
                {
                    t.DepartureAddress = GetAddress(new PointD(t.GPSDataStart.Longitude, t.GPSDataStart.Latitude));
                    if (t.DepartureAddress == String.Empty)
                    {
                        DataRow dr = dtXY.NewRow();
                        dr["TripID"] = t.iID;
                        dr["Lat"] = t.GPSDataStart.Latitude;
                        dr["Lon"] = t.GPSDataStart.Longitude;
                        dtXY.Rows.Add(dr);
                    }
                }
                if (t.GPSDataEnd != null)
                {
                    t.ArrivalAddress = GetAddress(new PointD(t.GPSDataEnd.Longitude, t.GPSDataEnd.Latitude));
                    if (t.ArrivalAddress == String.Empty)
                    {
                        DataRow dr = dtXY.NewRow();
                        dr["TripID"] = "-" + t.iID;
                        dr["Lat"] = t.GPSDataEnd.Latitude;
                        dr["Lon"] = t.GPSDataEnd.Longitude;
                        dtXY.Rows.Add(dr);
                    }
                }

                //List<Zone> PossiblyIn = lPossiblyIn(t.GPSDataStart.Longitude, t.GPSDataStart.Latitude, lz);
                foreach (Zone z in lz)//PossiblyIn)
                    if (t.GPSDataStart != null)
                        if (zoneService.IsPointInside(z, new Coordinate(t.GPSDataStart.Latitude, t.GPSDataStart.Longitude), sConnStr))
                        {
                            t.DepartureZoneID = z.zoneID;
                            t.DepartureZone = z.description;
                            break;
                        }

                //PossiblyIn = lPossiblyIn(t.GPSDataEnd.Longitude, t.GPSDataEnd.Latitude, lz);
                foreach (Zone z in lz)//PossiblyIn)
                    if (t.GPSDataEnd != null)
                        if (zoneService.IsPointInside(z, new Coordinate(t.GPSDataEnd.Latitude, t.GPSDataEnd.Longitude), sConnStr))
                        {
                            t.ArrivalZoneID = z.zoneID;
                            t.ArrivalZone = z.description;
                            t.ArrivalZoneColor = z.color;
                            break;
                        }
            }

            if (dtXY.Rows.Count > 0)
            {
                //Doing 1 by 1 for now
                foreach (TripHeader t in lTrips)
                {
                    if (t.DepartureAddress == String.Empty)
                        t.DepartureAddress = ArcGisCall.GetAddress(t.GPSDataStart.Latitude, t.GPSDataStart.Longitude);

                    if (t.ArrivalAddress == String.Empty)
                        t.ArrivalAddress = ArcGisCall.GetAddress(t.GPSDataEnd.Latitude, t.GPSDataEnd.Longitude);
                }

                //Batch Reverse Geocoding
                //Here no longer working
                //TODO : OSM Batch Reverse Geocoding

                //DataTable dtAddresses = HereHttp.MultiReverseGeoCoding(dtXY);
                //foreach (TripHeader t in lTrips)
                //{
                //    if (t.DepartureAddress == String.Empty)
                //    {
                //        DataRow[] result = dtAddresses.Select("TripID = '" + t.iID + "'");
                //        foreach (DataRow row in result)
                //            t.DepartureAddress = row["Label"].ToString();
                //    }

                //    if (t.ArrivalAddress == String.Empty)
                //    {
                //        DataRow[] result = dtAddresses.Select("TripID = '-" + t.iID + "'");
                //        foreach (DataRow row in result)
                //            t.ArrivalAddress = row["Label"].ToString();
                //    }
                //}
            }

            if (bGetAddressCreated)
                UnloadProject();

            return lTrips;
        }
        public List<TripHeader> GetAddress(List<TripHeader> lTrips, DataSet dsZones, String sConnStr)
        {
            Boolean bGetAddressCreated = false;
            if (sfMap1 == null || sfMap1.ShapeFileCount == 0)
            {
                bGetAddressCreated = true;
                ReadProjectNLoadMap();
            }

            System.Data.DataTable dtXY = new System.Data.DataTable();
            dtXY.Columns.Add("TripID");
            dtXY.Columns.Add("Lat");
            dtXY.Columns.Add("Lon");

            ZoneService zoneService = new ZoneService();
            foreach (TripHeader t in lTrips)
            {
                if (t.GPSDataStart != null)
                {
                    t.DepartureAddress = GetAddress(new PointD(t.GPSDataStart.Longitude, t.GPSDataStart.Latitude));
                    if (t.DepartureAddress == String.Empty)
                    {
                        DataRow dr = dtXY.NewRow();
                        dr["TripID"] = t.iID;
                        dr["Lat"] = t.GPSDataStart.Latitude;
                        dr["Lon"] = t.GPSDataStart.Longitude;
                        dtXY.Rows.Add(dr);
                    }
                }
                if (t.GPSDataEnd != null)
                {
                    t.ArrivalAddress = GetAddress(new PointD(t.GPSDataEnd.Longitude, t.GPSDataEnd.Latitude));
                    if (t.ArrivalAddress == String.Empty)
                    {
                        DataRow dr = dtXY.NewRow();
                        dr["TripID"] = "-" + t.iID;
                        dr["Lat"] = t.GPSDataEnd.Latitude;
                        dr["Lon"] = t.GPSDataEnd.Longitude;
                        dtXY.Rows.Add(dr);
                    }
                }

                int iFound = 0;
                DataTable dtZoneH = dsZones.Tables["dtZoneH"];
                DataTable dtZoneD = dsZones.Tables["dtZoneD"];
                foreach (DataRow dr in dtZoneH.Rows)
                {
                    Boolean bPossiblyIn = false;
                    if (String.IsNullOrEmpty(t.DepartureZone))
                        if (t.GPSDataStart.Longitude <= (Double)dr["MaxLon"]
                            && t.GPSDataStart.Longitude >= (Double)dr["MinLon"]
                            && t.GPSDataStart.Latitude >= (Double)dr["MinLat"]
                            && t.GPSDataStart.Latitude <= (Double)dr["MaxLat"])
                            bPossiblyIn = true;

                    if (!bPossiblyIn)
                        if (String.IsNullOrEmpty(t.ArrivalZone))
                            if (t.GPSDataEnd.Longitude <= (Double)dr["MaxLon"]
                                && t.GPSDataEnd.Longitude >= (Double)dr["MinLon"]
                                && t.GPSDataEnd.Latitude >= (Double)dr["MinLat"]
                                && t.GPSDataEnd.Latitude <= (Double)dr["MaxLat"])
                                bPossiblyIn = true;

                    if (!bPossiblyIn)
                        continue;

                    List<Coordinate> poly = new List<Coordinate>();
                    DataRow[] drd = dtZoneD.Select("ZoneID = " + dr["ZoneID"].ToString());
                    Zone z = new Zone();
                    z.zoneID = (int)dr["ZoneID"];
                    z.description = dr["Description"].ToString();
                    z.color = (int)dr["Color"];
                    z.zoneDetails = new List<ZoneDetail>();
                    foreach (DataRow d in drd)
                    {
                        ZoneDetail zd = new ZoneDetail();
                        zd.latitude = (Double)d["Latitude"];
                        zd.longitude = (Double)d["Longitude"];
                        z.zoneDetails.Add(zd);
                        poly.Add(new Coordinate(zd.latitude, zd.longitude));
                    }

                    if (String.IsNullOrEmpty(t.DepartureZone))
                        if (t.GPSDataStart != null)
                            if (zoneService.IsPointInside(z, new Coordinate(t.GPSDataStart.Latitude, t.GPSDataStart.Longitude), sConnStr))
                            {
                                t.DepartureZoneID = z.zoneID;
                                t.DepartureZone = z.description;
                                iFound++;
                            }

                    if (String.IsNullOrEmpty(t.ArrivalZone))
                        if (t.GPSDataEnd != null)
                            if (zoneService.IsPointInside(z, new Coordinate(t.GPSDataEnd.Latitude, t.GPSDataEnd.Longitude), sConnStr))
                            {
                                t.ArrivalZoneID = z.zoneID;
                                t.ArrivalZone = z.description;
                                t.ArrivalZoneColor = z.color;
                                iFound++;
                            }

                    if (iFound >= 2)
                        break;
                }
            }

            if (dtXY.Rows.Count > 0)
            {
                //Batch Reverse Geocoding
                //Here no longer working
                //TODO : OSM Batch Reverse Geocoding

                //Doing 1 by 1 for now
                foreach (TripHeader t in lTrips)
                {
                    if (t.DepartureAddress == String.Empty)
                        t.DepartureAddress = ArcGisCall.GetAddress(t.GPSDataStart.Latitude, t.GPSDataStart.Longitude);

                    if (t.ArrivalAddress == String.Empty)
                        t.ArrivalAddress = ArcGisCall.GetAddress(t.GPSDataEnd.Latitude, t.GPSDataEnd.Longitude);
                }
            }

            if (bGetAddressCreated)
                UnloadProject();

            return lTrips;
        }

        public String GetAddress(Double X, Double Y)
        {
            PointD p = new PointD(X, Y);
            String s = GetAddress(p);
            if (String.IsNullOrEmpty(s))
                s = GetAddressFromShp(X, Y);
            if (String.IsNullOrEmpty(s))
                s = ArcGisCall.GetAddress(Y, X);

            return s;
        }
        public String GetAddress(PointD pt, NaveoOneLib.Common.Globals.MapType eMapType)
        {
            return GetAddress(pt.X, pt.Y, eMapType);
        }
        String GetAddress(PointD pt)
        {
            //return MireoMapsCall.GetAddress(pt.Y, pt.X);
            String sAddress = String.Empty;

            try
            {
                List<sdd> ls = new List<sdd>();
                int iDelta = 500000;
                while (sAddress == String.Empty)
                {
                    iDelta = iDelta / 10;
                    if (iDelta <= 5)
                        break;

                    ls = new List<sdd>();

                    double delta = 8.0 / sfMap1.ZoomLevel;
                    delta = 8.0 / iDelta;
                    PointF ptf = new PointF((float)pt.X, (float)pt.Y);
                    for (int l = sfMap1.ShapeFileCount - 1; l >= 0; l--)
                    {
                        if (sfMap1[l].Name == "RoadResidential.shp"
                                || sfMap1[l].Name == "RoadB2014.shp"
                                || sfMap1[l].Name == "RoadA2014.shp"
                                || sfMap1[l].Name == "Poi.shp"
                                || sfMap1[l].Name == "Motorway2014.shp"
                                || sfMap1[l].Name == "Village_limit.shp"
                                || sfMap1[l].Name == "Township_Area.shp"
                            )
                        {
                            int selectedIndex = sfMap1[l].GetShapeIndexContainingPoint(pt, delta);
                            if (selectedIndex >= 0)
                            {
                                String[] fNames = sfMap1[l].RenderSettings.DbfReader.GetFieldNames();
                                int iFname = 0;
                                Boolean bFound = false;
                                foreach (String s in fNames)
                                {
                                    if (s == "NAME")
                                    {
                                        bFound = true;
                                        break;
                                    }
                                    iFname++;
                                }

                                if (bFound)
                                {
                                    sdd s = new Maps.ESRIMapService.sdd();
                                    s.shp = sfMap1[l].Name;
                                    s.data = sfMap1[l].RenderSettings.DbfReader.GetField(selectedIndex, iFname).Trim();
                                    s.distance = selectedIndex;
                                    ls.Add(s);
                                }
                            }
                        }
                    }
                    sdd sddNearest = ls.Where(o => o.shp != "Village_limit.shp" && o.shp != "Township_Area.shp").OrderBy(x => x.distance).FirstOrDefault();
                    if (sddNearest != null)
                    {
                        sAddress = sddNearest.data;
                        if (sddNearest.shp == "Poi.shp")
                            sAddress = "Near " + sddNearest.data;
                        else if (sddNearest.shp == "RoadB2014.shp")
                        {
                            sAddress = sddNearest.data;
                            if (sAddress == "ROAD")
                            {
                                sddNearest = ls.Where(o => o.shp != "Village_limit.shp" && o.shp != "Township_Area.shp" && o.shp != "RoadB2014.shp").OrderBy(x => x.distance).FirstOrDefault();
                                sAddress = sddNearest.data;
                                if (sddNearest.shp == "Poi.shp")
                                    sAddress = "Near " + sddNearest.data;
                            }
                        }
                    }

                    if (sAddress.Trim().Length > 0)
                        if (sAddress[0] == '{')
                            sAddress = sAddress.Substring(sAddress.IndexOf("@") + 1);
                }
                //Town & Village
                sdd sddTownVillage = ls.Where(o => o.shp == "Village_limit.shp" || o.shp == "Township_Area.shp").OrderBy(x => x.distance).FirstOrDefault();
                if (sddTownVillage != null)
                {
                    if (sAddress.Length > 0)
                        sAddress += ", ";

                    sAddress += sddTownVillage.data;
                }
            }
            catch { }

            //if (sAddress == String.Empty)
            //    sAddress = GetAddress(pt.X, pt.Y, NaveoOneLib.Common.Globals.MapType.HereMaps);

            return sAddress;
        }
        String GetAddress(Double X, Double Y, NaveoOneLib.Common.Globals.MapType eMapType)
        {
            String s = String.Empty;

            switch (eMapType)
            {
                case Globals.MapType.None:
                    s = String.Empty;
                    break;

                case NaveoOneLib.Common.Globals.MapType.ESRI:
                    s = GetAddress(X, Y);
                    //s = MireoMapsCall.GetAddress(X, Y);
                    break;

                case NaveoOneLib.Common.Globals.MapType.HereHybrid:
                    if (hereAPI == null)
                        hereAPI = new HEREAPICall();

                    s = hereAPI.GetAddress(X, Y);
                    break;

                case NaveoOneLib.Common.Globals.MapType.HereMaps:
                    if (hereAPI == null)
                        hereAPI = new HEREAPICall();

                    s = hereAPI.GetAddress(X, Y);
                    break;

                case NaveoOneLib.Common.Globals.MapType.HereSatellite:
                    if (hereAPI == null)
                        hereAPI = new HEREAPICall();

                    s = hereAPI.GetAddress(X, Y);
                    break;

                case NaveoOneLib.Common.Globals.MapType.HereStreet:
                    if (hereAPI == null)
                        hereAPI = new HEREAPICall();

                    s = hereAPI.GetAddress(X, Y);
                    break;

                case NaveoOneLib.Common.Globals.MapType.BingMaps:
                    if (bingAPI == null)
                        bingAPI = new BINGAPICall();

                    s = bingAPI.GetAddress(X, Y);
                    break;

                default:
                    s = String.Empty;
                    break;
            }
            return s;
        }

        public String GetAddressFromShp(Double x, Double y)
        {
            PointD p = new PointD(x, y);
            DataTable dt = GetFeature(p, "World84");
            String CountryCode = GetValue(dt, "CountryCod");
            if (CountryCode == "MU")
                return String.Empty;
            if (String.IsNullOrEmpty(CountryCode))
                return String.Empty;

            String DistrictProvince = GetValue(dt, "DistProv");
            String VcaDistrict = GetValue(dt, "VcaDist");
            String Locality = GetValue(dt, "Locality");

            String Country = GetValue(dt, "Country");
            String rn = Country + "RoadNetwork84";
            dt = GetFeature(p, rn);
            String RoadName = GetValue(dt, "Name");

            //dt = GetFeature(p, "WorldPOI84");
            //String poi = GetValue(dt, "Name");
            //dt = GetFeature(p, "RoadResidential");

            String sResult = String.Empty;
            if (!String.IsNullOrEmpty(RoadName))
                sResult += RoadName;
            if (!String.IsNullOrEmpty(Locality))
                sResult += " - " + Locality;
            if (!String.IsNullOrEmpty(VcaDistrict))
                sResult += " - " + VcaDistrict;
            if (!String.IsNullOrEmpty(DistrictProvince))
                sResult += " - " + DistrictProvince;
            if (!String.IsNullOrEmpty(CountryCode))
                sResult += " - " + CountryCode;

            sResult = sResult.Trim();
            if (sResult.StartsWith("-"))
                sResult = sResult.Substring(1).Trim();
            return sResult;
        }
        DataTable GetFeature(PointD pt, String fileName)
        {
            DataTable dt = new DataTable();
            Double minDistance = 0.0016;

            String shp = Globals.GetNaveoAppFolder("BaseESRIMaps") + fileName;
            int selectedIndex = sfMapAddress[shp].GetClosestShape(pt, minDistance);
            if (selectedIndex >= 0)
            {
                int iFname = -1;
                String[] fNames = sfMapAddress[shp].RenderSettings.DbfReader.GetFieldNames();
                dt.Columns.Add("fn");
                dt.Columns.Add("data");
                foreach (String s in fNames)
                {
                    iFname++;
                    String data = sfMapAddress[shp].RenderSettings.DbfReader.GetField(selectedIndex, iFname).Trim();
                    dt.Rows.Add(s, data);

                    //var z = sfMap[shp].GetShapeDataD(selectedIndex);
                    //Double dist = ConversionFunctions.DistanceBetweenLatLongPoints(EGIS.ShapeFileLib.ConversionFunctions.RefEllipse, pt.X, pt.Y, z[0][0].X, z[0][0].Y);
                }
                dt.Rows.Add("pid", selectedIndex);
            }
            return dt;
        }
        String GetValue(DataTable dt, String fieldName)
        {
            foreach (DataRow dr in dt.Rows)
                if (dr["fn"].ToString().ToLower() == fieldName.ToLower())
                    return dr["data"].ToString();

            return String.Empty;
        }

        public String[] GetRoadSpeed(Double X, Double Y)
        {
            PointD p = new PointD(X, Y);
            return GetRoadSpeed(p);
        }
        String[] GetRoadSpeed(PointD pt)
        {
            String sSpeed = String.Empty;
            String sRoadType = String.Empty;
            try
            {
                List<sdd> ls = new List<sdd>();
                int iDelta = 500000;
                while (sSpeed == String.Empty)
                {
                    iDelta = iDelta / 10;
                    if (iDelta <= 5)
                        break;

                    ls = new List<sdd>();

                    double delta = 8.0 / sfMap1.ZoomLevel;
                    delta = 8.0 / iDelta;
                    for (int l = sfMap1.ShapeFileCount - 1; l >= 0; l--)
                    {
                        if (sfMap1[l].Name == "RoadResidential.shp"
                                || sfMap1[l].Name == "RoadB2014.shp"
                                || sfMap1[l].Name == "RoadA2014.shp"
                                || sfMap1[l].Name == "Motorway2014.shp"
                            )
                        {
                            int selectedIndex = sfMap1[l].GetShapeIndexContainingPoint(pt, delta);
                            if (selectedIndex >= 0)
                            {
                                String[] fNames = sfMap1[l].RenderSettings.DbfReader.GetFieldNames();
                                int iFname = 0;
                                Boolean bFound = false;
                                foreach (String s in fNames)
                                {
                                    if (s == "SPD_LIMIT")
                                    {
                                        bFound = true;
                                        break;
                                    }
                                    iFname++;
                                }

                                if (bFound)
                                {
                                    sdd s = new Maps.ESRIMapService.sdd();
                                    s.shp = sfMap1[l].Name;
                                    s.data = sfMap1[l].RenderSettings.DbfReader.GetField(selectedIndex, iFname).Trim();
                                    s.distance = selectedIndex;
                                    ls.Add(s);
                                }
                            }
                        }
                    }
                    sdd sddNearest = ls.OrderBy(x => x.distance).FirstOrDefault();
                    if (sddNearest == null)
                        return new String[] { sSpeed, sRoadType };

                    if (sddNearest.shp == "RoadResidential.shp")
                    {
                        sSpeed = sddNearest.data;
                        sRoadType = "RoadC";
                    }
                    else if (sddNearest.shp == "RoadB2014.shp")
                    {
                        sSpeed = sddNearest.data;
                        sRoadType = "RoadB";
                    }
                    else if (sddNearest.shp == "RoadA2014.shp")
                    {
                        sSpeed = sddNearest.data;
                        sRoadType = "RoadA";
                    }
                    else if (sddNearest.shp == "Motorway2014.shp")
                    {
                        sSpeed = sddNearest.data;
                        sRoadType = "Motorway";
                    }
                }
            }
            catch { }

            return new String[] { sSpeed, sRoadType };
        }

        public String GetFillingStation(Double X, Double Y)
        {
            PointD p = new PointD(X, Y);
            String s = GetFillingStation(p);

            return s;
        }
        String GetFillingStation(PointD pt)
        {
            //return MireoMapsCall.GetAddress(pt.Y, pt.X);
            String sAddress = String.Empty;

            try
            {
                List<sdd> ls = new List<sdd>();
                int iDelta = 500000;
                while (sAddress == String.Empty)
                {
                    iDelta = iDelta / 10;
                    if (iDelta <= 5)
                        break;

                    ls = new List<sdd>();

                    double delta = 8.0 / sfMap1.ZoomLevel;
                    delta = 8.0 / iDelta;
                    PointF ptf = new PointF((float)pt.X, (float)pt.Y);
                    for (int l = sfMap1.ShapeFileCount - 1; l >= 0; l--)
                    {
                        if (sfMap1[l].Name == "RoadResidential.shp"
                                || sfMap1[l].Name == "RoadB2014.shp"
                                || sfMap1[l].Name == "RoadA2014.shp"
                                || sfMap1[l].Name == "FILLING_STATION_2019.shp"
                                || sfMap1[l].Name == "Motorway2014.shp"
                                || sfMap1[l].Name == "Village_limit.shp"
                                || sfMap1[l].Name == "Township_Area.shp"
                            )
                        {
                            int selectedIndex = sfMap1[l].GetShapeIndexContainingPoint(pt, delta);
                            if (selectedIndex >= 0)
                            {
                                String[] fNames = sfMap1[l].RenderSettings.DbfReader.GetFieldNames();
                                int iFname = 0;
                                Boolean bFound = false;
                                foreach (String s in fNames)
                                {
                                    if (s == "NAME")
                                    {
                                        bFound = true;
                                        break;
                                    }
                                    iFname++;
                                }

                                if (bFound)
                                {
                                    sdd s = new Maps.ESRIMapService.sdd();
                                    s.shp = sfMap1[l].Name;
                                    s.data = sfMap1[l].RenderSettings.DbfReader.GetField(selectedIndex, iFname).Trim();
                                    s.distance = selectedIndex;
                                    ls.Add(s);
                                }
                            }
                        }
                    }
                    sdd sddNearest = ls.Where(o => o.shp != "Village_limit.shp" && o.shp != "Township_Area.shp").OrderBy(x => x.distance).FirstOrDefault();
                    if (sddNearest != null)
                    {
                        sAddress = sddNearest.data;
                        if (sddNearest.shp == "FILLING_STATION_2019.shp")
                            sAddress = "Near " + sddNearest.data;
                        else if (sddNearest.shp == "RoadB2014.shp")
                        {
                            sAddress = sddNearest.data;
                            if (sAddress == "ROAD")
                            {
                                sddNearest = ls.Where(o => o.shp != "Village_limit.shp" && o.shp != "Township_Area.shp" && o.shp != "RoadB2014.shp").OrderBy(x => x.distance).FirstOrDefault();
                                sAddress = sddNearest.data;
                                if (sddNearest.shp == "Poi.shp")
                                    sAddress = "Near " + sddNearest.data;
                            }
                        }
                    }

                    if (sAddress.Trim().Length > 0)
                        if (sAddress[0] == '{')
                            sAddress = sAddress.Substring(sAddress.IndexOf("@") + 1);
                }
                //Town & Village
                sdd sddTownVillage = ls.Where(o => o.shp == "Village_limit.shp" || o.shp == "Township_Area.shp").OrderBy(x => x.distance).FirstOrDefault();
                if (sddTownVillage != null)
                {
                    if (sAddress.Length > 0)
                        sAddress += ", ";

                    sAddress += sddTownVillage.data;
                }
            }
            catch { }

            //if (sAddress == String.Empty)
            //    sAddress = GetAddress(pt.X, pt.Y, NaveoOneLib.Common.Globals.MapType.HereMaps);

            return sAddress;
        }

        class sdd
        {
            public String shp { get; set; }
            public String data { get; set; }
            public int distance { get; set; }
        }

        String GetZoneWorking(Double X, Double Y, List<Zone> lz, String sConnStr)
        {
            List<Zone> newList = lz.Where(m => m.zoneDetails != null)
                .Select(m => new Zone
                {
                    zoneDetails = (m.zoneDetails.Where(o =>
                           X <= m.zoneDetails.Max(y => y.longitude)
                        && X >= m.zoneDetails.Min(y => y.longitude)
                        && Y >= m.zoneDetails.Min(y => y.latitude)
                        && Y <= m.zoneDetails.Max(y => y.latitude)
                    ).ToList()),
                    zoneID = m.zoneID,
                    color = m.color,
                    comments = m.comments,
                    description = m.description,
                    displayed = m.displayed,
                    externalRef = m.externalRef,
                    lMatrix = m.lMatrix,
                    lZoneType = m.lZoneType,
                    ref2 = m.ref2
                }).ToList();

            List<Zone> PossiblyIn = newList.Where(o => o.zoneDetails.Count > 0).ToList();
            ZoneService zoneService = new ZoneService();
            String bResult = String.Empty;
            foreach (Zone z in PossiblyIn) //lz)
                if (zoneService.IsPointInside(z, new Coordinate(Y, X), sConnStr))
                {
                    bResult = z.description;
                    break;
                }
            return bResult;
        }
        public String GetZone(Double X, Double Y, List<Zone> lz, String sConnStr)
        {
            ZoneService zoneService = new ZoneService();
            //List<Zone> PossiblyIn = lPossiblyIn(X, Y, lz);
            String bResult = String.Empty;
            foreach (Zone z in lz)
                if (zoneService.IsPointInside(z, new Coordinate(Y, X), sConnStr))
                {
                    bResult = z.description;
                    break;
                }
            return bResult;
        }
        public String GetZone(Double X, Double Y, DataSet dsZones, String sConnStr)
        {
            String bResult = String.Empty;
            DataTable dtZoneH = dsZones.Tables["dtZoneH"];
            DataTable dtZoneD = dsZones.Tables["dtZoneD"];
            ZoneService zoneService = new ZoneService();
            foreach (DataRow dr in dtZoneH.Rows)
            {
                Boolean bPossiblyIn = false;
                if (X <= (Double)dr["MaxLon"]
                    && X >= (Double)dr["MinLon"]
                    && Y >= (Double)dr["MinLat"]
                    && Y <= (Double)dr["MaxLat"])
                    bPossiblyIn = true;

                if (!bPossiblyIn)
                    continue;

                DataRow[] drd = dtZoneD.Select("ZoneID = " + dr["ZoneID"].ToString());
                Zone z = new Zone();
                z.zoneID = (int)dr["ZoneID"];
                z.description = dr["Description"].ToString();
                z.color = (int)dr["Color"];
                z.zoneDetails = new List<ZoneDetail>();
                foreach (DataRow d in drd)
                {
                    ZoneDetail zd = new ZoneDetail();
                    zd.latitude = (Double)d["Latitude"];
                    zd.longitude = (Double)d["Longitude"];
                    z.zoneDetails.Add(zd);
                }

                if (zoneService.IsPointInside(z, new Coordinate(Y, X), sConnStr))
                {
                    bResult = z.description;
                    break;
                }
            }

            return bResult;
        }

        public Zone GetZoneZ(Double X, Double Y, List<Zone> lz, String sConnStr)
        {
            Zone zResult = null;
            ZoneService zoneService = new ZoneService();
            foreach (Zone z in lz)
                if (zoneService.IsPointInside(z, new Coordinate(Y, X), sConnStr))
                {
                    zResult = z;
                    break;
                }
            return zResult;
        }
        public Zone GetZoneZ(Double X, Double Y, DataSet dsZones)
        {
            Zone zResult = null;
            DataTable dtZoneH = dsZones.Tables["dtZoneH"];
            DataTable dtZoneD = dsZones.Tables["dtZoneD"];
            ZoneService zoneService = new ZoneService();
            foreach (DataRow dr in dtZoneH.Rows)
            {
                Boolean bPossiblyIn = false;

                if (X <= (Double)dr["MaxLon"]
                    && X >= (Double)dr["MinLon"]
                    && Y >= (Double)dr["MinLat"]
                    && Y <= (Double)dr["MaxLat"])
                    bPossiblyIn = true;

                if (!bPossiblyIn)
                    continue;

                List<Coordinate> poly = new List<Coordinate>();
                DataRow[] drd = dtZoneD.Select("ZoneID = " + dr["ZoneID"].ToString());
                Zone z = new Zone();
                z.zoneID = (int)dr["ZoneID"];
                z.description = dr["Description"].ToString();
                z.color = (int)dr["Color"];
                z.zoneDetails = new List<ZoneDetail>();
                foreach (DataRow d in drd)
                {
                    ZoneDetail zd = new ZoneDetail();
                    zd.latitude = (Double)d["Latitude"];
                    zd.longitude = (Double)d["Longitude"];
                    z.zoneDetails.Add(zd);
                    poly.Add(new Coordinate(zd.latitude, zd.longitude));
                }

                if (zoneService.IsPointInside(z, new Coordinate(Y, X), String.Empty))
                {
                    zResult = z;
                    break;
                }
            }
            return zResult;
        }

        public List<Zone> GetZones(Double X, Double Y, List<Zone> lz, String sConnStr)
        {
            List<Zone> lZones = new List<Zone>();
            ZoneService zoneService = new ZoneService();
            foreach (Zone z in lz)
                if (zoneService.IsPointInside(z, new Coordinate(Y, X), sConnStr))
                    lZones.Add(z);

            return lZones;
        }

        List<Zone> lPossiblyIn(Double X, Double Y, List<Zone> lz)
        {
            List<Zone> PossiblyIn = lz.Where(m => m.zoneDetails.Count > 2)
                .Select(m => new Zone
                {
                    zoneDetails = (m.zoneDetails.Where(o =>
                           X <= m.zoneDetails.Max(n => n.longitude)
                        && X >= m.zoneDetails.Min(n => n.longitude)
                        && Y >= m.zoneDetails.Min(n => n.latitude)
                        && Y <= m.zoneDetails.Max(n => n.latitude)
                    ).ToList()),
                    zoneID = m.zoneID,
                    color = m.color,
                    comments = m.comments,
                    description = m.description,
                    displayed = m.displayed,
                    externalRef = m.externalRef,
                    lMatrix = m.lMatrix,
                    lZoneType = m.lZoneType,
                    ref2 = m.ref2
                }).Where(m => m.zoneDetails.Count > 0).ToList();

            return PossiblyIn;
        }

        public String GetZone(Double X, Double Y, List<Zone> lz, List<int> li, String sConnStr)
        {
            String bResult = String.Empty;

            foreach (int i in li)
            {
                List<Zone> lzi = lz.Where(m => m.zoneID == i)
                    .Select(m => new Zone
                    {
                        zoneDetails = m.zoneDetails,
                        zoneID = m.zoneID,
                        color = m.color,
                        comments = m.comments,
                        description = m.description,
                        displayed = m.displayed,
                        externalRef = m.externalRef,
                        lMatrix = m.lMatrix,
                        lZoneType = m.lZoneType,
                        ref2 = m.ref2
                    }).Where(m => m.zoneID == i).ToList();
                ZoneService zoneService = new ZoneService();
                foreach (Zone z in lzi)
                    if (zoneService.IsPointInside(z, new Coordinate(Y, X), sConnStr))
                        return z.description;
            }
            return bResult;
        }
        public Zone GetZoneZ(Double X, Double Y, List<Zone> lz, List<int> li, String sConnStr)
        {
            Zone zResult = null;
            foreach (int i in li)
            {
                List<Zone> lzi = lz.Where(m => m.zoneID == i)
                    .Select(m => new Zone
                    {
                        zoneDetails = m.zoneDetails,
                        zoneID = m.zoneID,
                        color = m.color,
                        comments = m.comments,
                        description = m.description,
                        displayed = m.displayed,
                        externalRef = m.externalRef,
                        lMatrix = m.lMatrix,
                        lZoneType = m.lZoneType,
                        ref2 = m.ref2
                    }).Where(m => m.zoneID == i).ToList();
                ZoneService zoneService = new ZoneService();
                foreach (Zone z in lzi)
                    if (zoneService.IsPointInside(z, new Coordinate(Y, X), sConnStr))
                        return z;
            }
            return zResult;
        }

        public DataSet DisplayLive(List<int> LAssetID, Boolean bShowSuspectOnly, String sConnStr)
        {
            return DisplayLive(LAssetID, bShowSuspectOnly, String.Empty, Globals.lAsset, Globals.lDriver, sConnStr);
        }
        public DataSet DisplayLive(List<int> LAssetID, Boolean bShowSuspectOnly, String strIconPaths, List<Asset> lAsset, List<Driver> lDriver, String sConnStr, NaveoOneLib.Common.Utils.OptionalOut<List<GPSData>> outResult = null)
        {
            //Idling	
            //Ignition ON	
            //Ignition OFF	
            //Driving	
            //Main Power OFF	
            //Main Power ON	
            //RESET	
            //AUX 1 OFF	
            //AUX 1 ON	
            //AUX 2 OFF	
            //AUX 2 ON	
            //AUX 3 OFF	
            //AUX 3 ON	
            //Harsh Acceleration Begin	
            //Harsh Acceleration End	
            //Harsh braking Begin	
            //Harsh braking End	
            //Data Log	All unknown

            //Live Tracking
            DataTable dtLive = new DataTable("dtLive");
            dtLive.Columns.Add("Cordinate", typeof(EGIS.ShapeFileLib.PointD));
            dtLive.Columns.Add("MarkerFile", typeof(String));
            dtLive.Columns.Add("AssetNumber", typeof(String));
            dtLive.Columns.Add("MouseOver", typeof(String));
            dtLive.Columns.Add("Availability", typeof(String));
            dtLive.Columns.Add("AssetID", typeof(Int32));
            dtLive.Columns.Add("Address", typeof(String));
            //Web
            dtLive.Columns.Add("wLongitude", typeof(Double));
            dtLive.Columns.Add("wLatitude", typeof(Double));

            List<int> ListAssetID = new List<int>();
            List<GPSData> PreviousGPSData = new List<GPSData>();
            String strDirectionPath = Globals.DeviceDirectionPath();
            if (!String.IsNullOrEmpty(strIconPaths))
                strDirectionPath = strIconPaths;

            DataTable dtSummary = new DataTable("dtSummary");
            dtSummary.Columns.Add("Status", typeof(String));
            dtSummary.Columns.Add("DSLR", typeof(String));
            dtSummary.TableName = "DeviceHealth_Summary";
            dtSummary.Columns.Add("RegistrationNo", typeof(string));
            dtSummary.Columns.Add("DriverName", typeof(string));
            dtSummary.Columns.Add("dtLocal", typeof(DateTime));
            dtSummary.Columns.Add("Latitude", typeof(Double));
            dtSummary.Columns.Add("Longitude", typeof(Double));
            dtSummary.Columns.Add("Address", typeof(String));
            dtSummary.Columns.Add("DeviceId", typeof(String));
            dtSummary.Columns.Add("DirectionImage", typeof(String));
            dtSummary.Columns.Add("AssetID", typeof(int));
            dtSummary.Columns.Add("LogReason", typeof(String));
            dtSummary.Columns.Add("DriverID", typeof(int));
            dtSummary.Columns.Add("Odometer", typeof(String));
            dtSummary.Columns.Add("Availability", typeof(String));
            //dtSummary.Columns.Add("MarkerFile", typeof(String));

            String sStatus = String.Empty;
            String sDevice = String.Empty;
            String sDriver = String.Empty;
            String sLastRpt = String.Empty;
            //String sAddress = String.Empty;
            String sOdo = String.Empty;
            String sLR = String.Empty;
            String sSpeed = String.Empty;
            String sDSLR = String.Empty;
            String sDeviceID = String.Empty;
            List<GPSData> LatestGPSData = new List<GPSData>();
            try
            {
                ListAssetID = LAssetID;
                LatestGPSData = new GPSDataService().GetLatestGPSData(ListAssetID, new List<string>(),false, 0, false, sConnStr);
                if (outResult != null)
                    outResult.Result = LatestGPSData.ToList();
                LatestGPSData.OrderBy(o => o.AssetID).ToList();

                //force refresh
                Double dPrevious = 0;
                Double dNow = 0;
                foreach (GPSData g in PreviousGPSData)
                    dPrevious += g.Latitude;
                foreach (GPSData g in LatestGPSData)
                    dNow += g.Latitude;

                if (dNow != dPrevious)
                    PreviousGPSData = LatestGPSData;

                //int iFileName = 0;
                int PrevAsset = 0;
                for (int i = 0; i < LatestGPSData.Count; i++)
                {
                    //in case only 1 rec came for 1 asset. should be 2 rec per assets
                    if (PrevAsset != LatestGPSData[i].AssetID)
                    {
                        try
                        {
                            if (LatestGPSData[i].AssetID != LatestGPSData[i + 1].AssetID)
                            {
                                sDeviceID = String.Empty;
                                Asset ai = new Asset();
                                ai = lAsset.Find(o => o.AssetID == LatestGPSData[i].AssetID);
                                if (ai != null)
                                {
                                    AssetDeviceMap adm = ai.assetDeviceMap;
                                    if (adm.StartDate <= DateTime.Now && adm.EndDate >= DateTime.Now)
                                        sDeviceID = adm.DeviceID;
                                }

                                sStatus = "Suspect";
                                Double locXi = LatestGPSData[i].Longitude;
                                Double locYi = LatestGPSData[i].Latitude;
                                PointD di = new PointD(locXi, locYi);

                                DataRow drLivei = dtLive.NewRow();
                                drLivei["Cordinate"] = di;
                                //web
                                drLivei["wLongitude"] = locXi;
                                drLivei["wLatitude"] = locYi;

                                drLivei["MarkerFile"] = strDirectionPath + getPointImage(33.3, true, true, 1, false);
                                LatestGPSData[i].DirectionImage = drLivei["MarkerFile"].ToString();
                                drLivei["AssetNumber"] = LatestGPSData[i].AssetNum;
                                drLivei["AssetID"] = LatestGPSData[i].AssetID;

                                sDevice = drLivei["AssetNumber"].ToString();
                                sLastRpt = LatestGPSData[i].LastReportedInLocalTime.ToString();  //DateTimeGPS_UTC

                                String sWordingi = "Status\t\t: " + sStatus;
                                sWordingi += "\r\nVehicle\t\t: " + sDevice;

                                sDriver = String.Empty;
                                foreach (Driver dv in lDriver)
                                    if (LatestGPSData[i].DriverID == dv.DriverID)
                                    {
                                        sDriver = dv.sDriverName;
                                        break;
                                    }
                                if (sDriver != "Default Driver")
                                    sWordingi += " Driver : " + sDriver;

                                if (sStatus == "Driving")
                                    if (sSpeed != String.Empty)
                                        sWordingi += " at " + sSpeed;
                                sWordingi += "\r\nDate\t\t: " + LatestGPSData[i].dtLocalTime.ToString();

                                if (LatestGPSData[i].LastReportedInLocalTime > LatestGPSData[i].DateTimeGPS_UTC)
                                    sWordingi += "\r\nReported\t: " + sLastRpt;

                                sWordingi += "\r\nOdometer\t: " + sOdo;
                                if (LatestGPSData[i].Availability != null)
                                    sWordingi += "\r\nAvailability : " + "No: " + LatestGPSData[i].Availability;

                                if (Math.Round(LatestGPSData[i].TripDistance, 2) > 0)
                                    if (!drLivei["MarkerFile"].ToString().Contains("Stop.png"))
                                        sWordingi += "\r\nTrip Distance\t: " + Math.Round(LatestGPSData[i].TripDistance, 2).ToString() + " kms";

                                drLivei["MouseOver"] = sWordingi;
                                drLivei["Availability"] = LatestGPSData[i].Availability;

                                dtLive.Rows.Add(drLivei);

                                DataRow drSummaryi = dtSummary.NewRow();
                                drSummaryi["RegistrationNo"] = LatestGPSData[i].AssetNum;
                                drSummaryi["dtLocal"] = LatestGPSData[i].dtLocalTime;
                                drSummaryi["Latitude"] = LatestGPSData[i].Latitude;
                                drSummaryi["Longitude"] = LatestGPSData[i].Longitude;
                                drSummaryi["Status"] = sStatus;
                                drSummaryi["DeviceId"] = sDeviceID;
                                drSummaryi["DirectionImage"] = LatestGPSData[i].DirectionImage;
                                drSummaryi["AssetID"] = LatestGPSData[i].AssetID;
                                drSummaryi["DriverID"] = LatestGPSData[i].DriverID;
                                drSummaryi["DriverName"] = sDriver;
                                drSummaryi["LogReason"] = sLR;
                                drSummaryi["Odometer"] = sOdo;
                                drSummaryi["DSLR"] = sDSLR;
                                if (LatestGPSData[i].Availability == null)
                                    drSummaryi["Availability"] = "Yes";
                                else
                                    drSummaryi["Availability"] = "No: " + LatestGPSData[i].Availability;

                                //drSummary["Address"] = GetAddress(LatestGPSData[i].Longitude, LatestGPSData[i].Latitude);
                                dtSummary.Rows.Add(drSummaryi);
                                continue;
                            }
                        }
                        catch { }


                        PrevAsset = LatestGPSData[i].AssetID;

                        Double dPrevLon = LatestGPSData[i + 1].Longitude;
                        Double dPrevLat = LatestGPSData[i + 1].Latitude;

                        Double locX = LatestGPSData[i].Longitude;
                        Double locY = LatestGPSData[i].Latitude;

                        PointD d = new PointD(locX, locY);
                        Double bBearing = calculateBearingTo(dPrevLat, dPrevLon, locY, locX);

                        Boolean isStop = false;
                        if (bBearing == 0)
                            isStop = true;
                        foreach (GPSDataDetail gpsd in LatestGPSData[i].GPSDataDetails)
                        {
                            sLR = gpsd.TypeID + " " + gpsd.TypeValue;
                            if (gpsd.TypeID == "Ignition")
                            {
                                if (gpsd.TypeValue == "Off" || gpsd.TypeValue == "0")
                                {
                                    isStop = true;
                                    sStatus = "Stopped";
                                }
                                else
                                {
                                    bBearing = -999999.999;
                                    isStop = false;
                                    sStatus = "Driving";
                                }
                            }
                            else if (gpsd.TypeID == "Heading")
                            {
                                isStop = false;
                                sStatus = "Driving";
                            }
                            else if (gpsd.TypeID == "Idling")
                            {
                                isStop = false;
                                sStatus = "Idling";
                            }
                            else if (gpsd.TypeID == "InvalidGpsSignals")
                            {
                                isStop = true;
                                sStatus = "Suspect";
                            }
                            else
                                sStatus = "Unknown";

                            //Force Stop icon if needed. 
                            //122 Stop by time
                            if (LatestGPSData[i].StopFlag == 122 || LatestGPSData[i + 1].StopFlag == 122)
                            {
                                isStop = true;
                                if (sStatus != "sStatus")
                                    sStatus = "Stopped";
                            }
                            else if (LatestGPSData[i].DateTimeGPS_UTC.AddMinutes(10) < DateTime.UtcNow)//Comparing system time and GPS time
                            {
                                isStop = true;
                                if (sStatus != "sStatus")
                                    sStatus = "Stopped";
                            }

                            sOdo = gpsd.Remarks;

                            //14 Dec 2014. Taking only 1 LR
                            break;
                        }

                        if (sStatus.Contains("Driving"))
                        {
                            Double dSpeed = System.Math.Round(LatestGPSData[i].Speed, 0);
                            if (dSpeed > 0)
                                sSpeed = dSpeed.ToString() + " km/h";
                        }
                        if (lDriver.Count > 0)
                        {
                            Driver myDriver = lDriver[0];
                            foreach (Driver dr in lDriver)
                                if (LatestGPSData[i].DriverID == dr.DriverID)
                                    myDriver = dr;
                            sDriver = myDriver.sDriverName;
                        }

                        //DSLR
                        int iAssetId = LatestGPSData[i].AssetID;
                        int? iMaxNoLogDays = 0;
                        Asset a = new Asset();
                        a = lAsset.Find(o => o.AssetID == iAssetId);
                        if (a != null)
                        {
                            AssetDeviceMap adm = a.assetDeviceMap;
                            if (adm.StartDate <= DateTime.Now && adm.EndDate >= DateTime.Now)
                            {
                                iMaxNoLogDays = adm.MaxNoLogDays;
                                sDeviceID = adm.DeviceID;
                            }
                        }

                        DateTime dtCurrent = DateTime.Now;
                        DateTime dtLastRpt = LatestGPSData[i].LastReportedInLocalTime; //.DateTimeGPS_UTC;
                        DateTime dtTrigger = dtCurrent.AddDays(-iMaxNoLogDays.Value);
                        int iDSLR = (dtCurrent - dtLastRpt).Days;

                        if (dtLastRpt < dtTrigger)
                            sStatus = "Suspect";

                        if (sStatus == String.Empty)
                            sStatus = "Suspect";

                        if (sStatus != "Suspect")
                            sDSLR = "OK";
                        else
                        {
                            if (dtLastRpt < dtTrigger)
                                sDSLR = "Not Reported Since " + dtLastRpt.ToString() + " (" + iDSLR.ToString() + " Days)";
                            else
                            {
                                sDSLR = sStatus;
                                if (LatestGPSData[i].GPSDataDetails != null)
                                    if (LatestGPSData[i].GPSDataDetails.Count > 0)
                                        sDSLR += " " + LatestGPSData[i].GPSDataDetails[0].TypeID;
                            }
                        }
                        Boolean isSuspect = false;
                        if (sStatus == "Suspect")
                            isSuspect = true;

                        //files 0 - 5 => 6 files, starting from 0
                        int iFileName = LatestGPSData[i].AssetID;
                        while (iFileName >= 6)
                            iFileName = iFileName - 6;

                        DataRow drLive = dtLive.NewRow();
                        drLive["Cordinate"] = d;
                        //web
                        drLive["wLongitude"] = locX;
                        drLive["wLatitude"] = locY;

                        if (sStatus == "Idling")
                            bBearing = -888888.88;
                        drLive["MarkerFile"] = strDirectionPath + getPointImage(bBearing, isSuspect, isStop, iFileName, false);
                        drLive["AssetNumber"] = LatestGPSData[i].AssetNum;
                        drLive["AssetID"] = LatestGPSData[i].AssetID;

                        sDevice = drLive["AssetNumber"].ToString();
                        sLastRpt = LatestGPSData[i].LastReportedInLocalTime.ToString();  //DateTimeGPS_UTC

                        String sWording = "Status\t\t: " + sStatus;
                        sWording += "\r\nVehicle\t\t: " + sDevice;
                        if (sDriver != "Default Driver")
                            sWording += " driven by " + sDriver;
                        if (sStatus == "Driving")
                            if (sSpeed != String.Empty)
                                sWording += " at " + sSpeed;
                        sWording += "\r\nDate\t\t: " + LatestGPSData[i].dtLocalTime.ToString();

                        if (LatestGPSData[i].LastReportedInLocalTime > LatestGPSData[i].dtLocalTime)
                            sWording += "\r\nReported\t: " + sLastRpt;

                        sWording += "\r\nOdometer\t: " + sOdo;
                        if (LatestGPSData[i].Availability != null)
                            sWording += "\r\nAvailability : " + "No: " + LatestGPSData[i].Availability;

                        if (Math.Round(LatestGPSData[i].TripDistance, 2) > 0)
                            if (!drLive["MarkerFile"].ToString().Contains("Stop.png"))
                                sWording += "\r\nTrip Distance\t: " + Math.Round(LatestGPSData[i].TripDistance, 2).ToString() + " kms";

                        drLive["MouseOver"] = sWording;
                        drLive["Availability"] = LatestGPSData[i].Availability;

                        dtLive.Rows.Add(drLive);

                        if (LatestGPSData[i].DirectionImage == "SecuMonitor")
                        {
                            LatestGPSData[i].DirectionImage = strDirectionPath + "Panic.png";
                            LatestGPSData[i + 1].DirectionImage = strDirectionPath + "Panic.png";
                            drLive["MarkerFile"] = strDirectionPath + "Panic.png";
                            Console.Beep(500, 500);
                        }
                        else
                        {
                            LatestGPSData[i].DirectionImage = drLive["MarkerFile"].ToString();
                            LatestGPSData[i + 1].DirectionImage = drLive["MarkerFile"].ToString();
                        }

                        DataRow drSummary = dtSummary.NewRow();
                        drSummary["RegistrationNo"] = LatestGPSData[i].AssetNum;
                        drSummary["dtLocal"] = LatestGPSData[i].dtLocalTime;
                        drSummary["Latitude"] = LatestGPSData[i].Latitude;
                        drSummary["Longitude"] = LatestGPSData[i].Longitude;
                        drSummary["Status"] = sStatus;
                        drSummary["DeviceId"] = sDeviceID;
                        drSummary["DirectionImage"] = LatestGPSData[i].DirectionImage;
                        drSummary["AssetID"] = LatestGPSData[i].AssetID;
                        drSummary["DriverID"] = LatestGPSData[i].DriverID;
                        drSummary["DriverName"] = sDriver;
                        drSummary["LogReason"] = sLR;
                        drSummary["Odometer"] = sOdo;
                        drSummary["DSLR"] = sDSLR;
                        if (LatestGPSData[i].Availability == null)
                            drSummary["Availability"] = "Yes";
                        else
                            drSummary["Availability"] = "No: " + LatestGPSData[i].Availability;

                        //drSummary["Address"] = GetAddress(LatestGPSData[i].Longitude, LatestGPSData[i].Latitude);
                        dtSummary.Rows.Add(drSummary);

                        //Skipping 2
                        i++;
                    }
                }

                //unit never reported.
                foreach (int i in LAssetID)
                {
                    sDevice = String.Empty;
                    sDeviceID = String.Empty;
                    DataRow[] dr = dtSummary.Select("AssetID = " + i.ToString());
                    if (dr.Length == 0)
                    {
                        Asset a = new Asset();
                        a = lAsset.Find(o => o.AssetID == i);
                        if (a != null)
                        {
                            sDevice = a.AssetNumber;
                            AssetDeviceMap adm = a.assetDeviceMap;
                            if (adm.StartDate <= DateTime.Now && adm.EndDate >= DateTime.Now)
                                sDeviceID = adm.DeviceID;
                        }

                        DataRow dri = dtSummary.NewRow();
                        dri["RegistrationNo"] = sDevice;
                        dri["Status"] = "Suspect";
                        dri["DSLR"] = "Never reported";
                        dri["DeviceId"] = sDeviceID;
                        dri["AssetID"] = i;
                        dri["DirectionImage"] = strDirectionPath + "Suspect.png";
                        dtSummary.Rows.Add(dri);
                    }
                }

                if (bShowSuspectOnly)
                    dtSummary = dtSummary.Select("Status = 'Suspect'").CopyToDataTable();

                dtSummary.DefaultView.Sort = "dtLocal Desc";
                dtSummary = dtSummary.DefaultView.ToTable();
            }
            catch
            {
                //MessageBox.Show(ex.ToString());
            }

            DataSet ds = new DataSet();
            ds.Tables.Add(dtSummary);
            ds.Tables.Add(dtLive);
            return ds;
        }

        //For API
        public List<GPSData> LatestGPSData(List<GPSData> lGPSData, DataSet dsZones)
        {
            lGPSData.OrderBy(o => o.AssetID).ToList();

            String sStatus = String.Empty;
            for (int i = 0; i < lGPSData.Count - 1; i++)
            {
                int myAsset = lGPSData[i].AssetID;
                int myNextAsset = lGPSData[i + 1].AssetID;

                if (myAsset == myNextAsset)
                {
                    try
                    {
                        Double dPrevLon = lGPSData[i + 1].Longitude;
                        Double dPrevLat = lGPSData[i + 1].Latitude;

                        Double locX = lGPSData[i].Longitude;
                        Double locY = lGPSData[i].Latitude;

                        PointD d = new PointD(locX, locY);
                        Double bBearing = calculateBearingTo(dPrevLat, dPrevLon, locY, locX);

                        Boolean isStop = false;
                        if (bBearing == 0)
                            isStop = true;
                        if (lGPSData[i].EngineOn == 0)
                            isStop = true;

                        Boolean isSuspect = false;
                        if (sStatus == "Suspect")
                            isSuspect = true;

                        int iMaxNoLogDays = 1;
                        DateTime dtCurrent = DateTime.Now;
                        DateTime dtLastRpt = lGPSData[i].LastReportedInLocalTime;
                        DateTime dtTrigger = dtCurrent.AddDays(-iMaxNoLogDays);
                        if (dtLastRpt < dtTrigger)
                            isSuspect = true;

                        lGPSData[i + 1].Availability = "ToDel";
                        if (lGPSData[i].DateTimeGPS_UTC.AddMinutes(10) < DateTime.UtcNow)
                        {
                            isStop = true;
                            lGPSData[i].Speed = 0;
                        }
                        lGPSData[i].Availability = getVehicleAction(bBearing, isSuspect, isStop, false);

                        i++;
                    }
                    catch { }
                }
                else
                {
                    lGPSData[i].Availability = getPointImage(0, true, true, 3, false);

                    if (lGPSData[i].Availability == String.Empty)
                    {
                        lGPSData[i].Availability = "Suspect";
                    }
                    else if (lGPSData[i].Availability.Contains(".png"))
                    {
                        lGPSData[i].Availability = lGPSData[i].Availability.Replace(".png", String.Empty);
                    }
                }
            }

            lGPSData.RemoveAll(o => o.Availability == "ToDel");

            //Sending in Availability ZoneID|Address
            Boolean bGetAddressCreated = false;
            if (sfMap1 == null || sfMap1.ShapeFileCount == 0)
            {
                bGetAddressCreated = true;
                ReadProjectNLoadMap();
            }

            System.Data.DataTable dtXY = new System.Data.DataTable();
            dtXY.Columns.Add("TripID");
            dtXY.Columns.Add("Lat");
            dtXY.Columns.Add("Lon");

            foreach (GPSData g in lGPSData)
            {
                String sAddress = GetAddress(new PointD(g.Longitude, g.Latitude));
                //if (sAddress == String.Empty)
                //{
                //    DataRow dr = dtXY.NewRow();
                //    dr["TripID"] = g.UID;
                //    dr["Lat"] = g.Latitude;
                //    dr["Lon"] = g.Longitude;
                //    dtXY.Rows.Add(dr);

                //    //To do Batch multi reverse
                //    //doing 1 by 1
                //    sAddress = ArcGisCall.GetAddress(g.Latitude, g.Longitude);
                //}

                String sDriverName = String.Empty;
                DataRow[] drDriver = dsZones.Tables["GPSDriver"].Select("DriverID = " + g.DriverID);
                if (drDriver.Length > 0)
                    sDriverName = drDriver[0]["sDriverName"].ToString();

                String sZone = String.Empty;
                if (g.Zone != ":")
                    sZone = g.Zone;

                g.Availability += "|" + sZone + "|" + sAddress + "|" + sDriverName;
            }

            if (bGetAddressCreated)
                UnloadProject();

            if (dtXY.Rows.Count > 0)
            {
                //To do Batch multi reverse
            }

            return lGPSData;
        }

        public DataTable dtLive1ToDel(DataTable dt, String strIconPaths)
        {
            String strDirectionPath = strIconPaths;
            String sStatus = String.Empty;
            String sDevice = String.Empty;
            String sDriver = String.Empty;
            String sLastRpt = String.Empty;
            String sOdo = String.Empty;
            String sLR = String.Empty;
            String sSpeed = String.Empty;
            String sDSLR = String.Empty;
            String sDeviceID = String.Empty;
            for (int i = 0; i < dt.Rows.Count - 1; i++)
            {
                int myAsset = (Int32)dt.Rows[i]["AssetID"];
                int myNextAsset = (Int32)dt.Rows[i + 1]["AssetID"];

                if (myAsset == myNextAsset)
                {
                    try
                    {
                        Double dPrevLon = (Double)dt.Rows[i]["Longitude"];
                        Double dPrevLat = (Double)dt.Rows[i]["Latitude"];

                        Double locX = (Double)dt.Rows[i + 1]["Longitude"];
                        Double locY = (Double)dt.Rows[i + 1]["Latitude"];

                        PointD d = new PointD(locX, locY);
                        Double bBearing = calculateBearingTo(dPrevLat, dPrevLon, locY, locX);

                        Boolean isStop = false;
                        if (bBearing == 0)
                            isStop = true;

                        Boolean isSuspect = false;
                        if (sStatus == "Suspect")
                            isSuspect = true;

                        int iFileName = myAsset;
                        while (iFileName >= 6)
                            iFileName = iFileName - 6;

                        dt.Rows[i + 1]["FN"] = strDirectionPath + getPointImage(bBearing, isSuspect, isStop, iFileName, false);
                        //dt.Rows[i + 1]["AssetName"] = BaseService.Decrypt(dt.Rows[i + 1]["AssetName"].ToString());
                    }
                    catch { }
                }
            }

            return dt;
        }

        public static Double calculateBearingTo(Double lat1, Double lng1, Double lat2, Double lng2)
        {
            //calc bearing from prev point to this one
            Double rLat1 = degreesToRadians(lat1);
            Double rLat2 = degreesToRadians(lat2);
            Double rDeltaLng = degreesToRadians(lng2 - lng1);

            Double y = Math.Sin(rDeltaLng) * Math.Cos(rLat2);
            Double x = Math.Cos(rLat1) * Math.Sin(rLat2) -
                    Math.Sin(rLat1) * Math.Cos(rLat2) * Math.Cos(rDeltaLng);
            Double brng = Math.Atan2(y, x);

            return (radiansToDegrees(brng) + 360) % 360;
        }
        public static Double degreesToRadians(Double degrees)
        { return degrees * Math.PI / 180; }
        public static Double radiansToDegrees(Double radians)
        { return radians * 180 / Math.PI; }
        public static String getPointImage(Double bearing, Boolean isSuspect, Boolean isStop, int iVehicleCount, Boolean isException)
        {
            if (isSuspect)
                return "Suspect.png";
            if (isStop)
                return "Stop.png";
            if (bearing == -999999.999)
                return "Start.png";
            if (bearing == -888888.88)
                return "Idle.png";

            //files 0 - 5 => 6 files, starting from 0
            while (iVehicleCount >= 6)
                iVehicleCount = iVehicleCount - 6;

            //Format is M[00]Direction[00]iVehicleCount[0]Exception
            String strFilename = iVehicleCount.ToString();
            if (iVehicleCount <= 9)
                strFilename = "0" + iVehicleCount.ToString();

            if (isException)
                strFilename += "1.png;";
            else
                strFilename += "0.png;";

            Double imagesCount = 16;
            Double degreesPerImage = 360 / imagesCount;
            String s = String.Empty;
            for (int i = 0; i < imagesCount; i++)
                if (i <= 9)
                    s += ("M0" + i.ToString() + strFilename);
                else
                    s += ("M" + i.ToString() + strFilename);

            String[] arrImage = s.Split(';');

            Double imageIndex = Math.Floor(bearing / degreesPerImage),
                midlineDistance = bearing % degreesPerImage;
            if (midlineDistance > degreesPerImage / 2)
            {
                imageIndex++;
            }
            imageIndex = imageIndex % arrImage.Length;

            if (imageIndex >= 16)
                imageIndex = 0;

            return arrImage[Convert.ToInt16(imageIndex)];
        }
        public static String getVehicleAction(Double bearing, Boolean isSuspect, Boolean isStop, Boolean isException)
        {
            if (isSuspect)
                return "Suspect";
            if (isStop)
                return "Stop";
            if (bearing == -999999.999)
                return "Start";
            if (bearing == -888888.88)
                return "Idle";
            if (isException)
                return "Warning;";

            return "Heading";
        }

        public DataTable dtLive(DataTable dt, String strIconPaths)
        {
            String strDirectionPath = strIconPaths;
            String sStatus = String.Empty;
            String sDevice = String.Empty;
            String sDriver = String.Empty;
            String sLastRpt = String.Empty;
            String sOdo = String.Empty;
            String sLR = String.Empty;
            String sSpeed = String.Empty;
            String sDSLR = String.Empty;
            String sDeviceID = String.Empty;
            for (int i = 0; i < dt.Rows.Count - 1; i++)
            {
                int myAsset = (Int32)dt.Rows[i]["AssetID"];
                int myNextAsset = (Int32)dt.Rows[i + 1]["AssetID"];

                if (myAsset == myNextAsset)
                {
                    try
                    {
                        Double dPrevLon = (Double)dt.Rows[i]["Longitude"];
                        Double dPrevLat = (Double)dt.Rows[i]["Latitude"];

                        Double locX = (Double)dt.Rows[i + 1]["Longitude"];
                        Double locY = (Double)dt.Rows[i + 1]["Latitude"];

                        PointD d = new PointD(locX, locY);
                        Double bBearing = calculateBearingTo(dPrevLat, dPrevLon, locY, locX);

                        Boolean isStop = false;
                        if (bBearing == 0)
                            isStop = true;

                        Boolean isSuspect = false;
                        if (sStatus == "Suspect")
                            isSuspect = true;

                        int iFileName = myAsset;
                        while (iFileName >= 6)
                            iFileName = iFileName - 6;

                        dt.Rows[i + 1]["FN"] = strDirectionPath + getPointImage(bBearing, isSuspect, isStop, iFileName, false);
                        //dt.Rows[i + 1]["AssetName"] = BaseService.Decrypt(dt.Rows[i + 1]["AssetName"].ToString());
                    }
                    catch { }
                }
            }

            return dt;
        }

        Double minX = 999;
        Double minY = 999;
        Double maxX = 0;
        Double maxY = 0;
        public List<TripHeader> AddIcon(List<TripHeader> lth)
        {
            int myID = 0;
            String strDirectionPath = @"/Resources/Images/deviceDirections/";
            strDirectionPath = @"/DeviceDirections/";

            foreach (TripHeader th in lth)
            {
                int iVehicleCount = th.AssetID;
                //files 0 - 5 => 6 files, starting from 0
                while (iVehicleCount >= 6)
                    iVehicleCount = iVehicleCount - 6;
                //int irndn = iColor % 6;

                Double dPrevLon = 0;
                Double dPrevLat = 0;
                minX = minY = 999;
                maxX = maxY = 0;
                String StrVeh = th.myAsset.AssetNumber;
                String StrDriver = th.myDriver.sDriverName;
                foreach (TripDetail td in th.TDetails)
                {
                    String sStatus = String.Empty;
                    String sLR = String.Empty;

                    Double locX = td.GpsData.Longitude;
                    Double locY = td.GpsData.Latitude;
                    PointD d = new PointD(locX, locY);
                    GetExtent(d);
                    Double bBearing = 0;
                    Boolean isStop = false;

                    sStatus = " Asset   :" + StrVeh + "\n Driver  : " + StrDriver + "";

                    if (td.GpsData.GPSDataDetails != null)
                        foreach (GPSDataDetail gpsd in td.GpsData.GPSDataDetails)
                        {
                            sLR = gpsd.TypeID + " : " + gpsd.TypeValue + " ";
                            if (gpsd.TypeID == "Ignition")
                            {
                                if (gpsd.TypeValue == "Off" || gpsd.TypeValue == "0")
                                {
                                    isStop = true;
                                    sStatus += "\n Status  : Stopped";
                                }
                                else
                                {
                                    bBearing = -999999.999;
                                    isStop = false;
                                    sStatus += "\n Status  : Driving";
                                }
                            }
                            else if (gpsd.TypeID == "Heading")
                            {
                                isStop = false;
                                sStatus += "\n Status  : Driving";
                            }
                            else if (gpsd.TypeID == "Idling")
                            {
                                isStop = false;
                                sStatus += "\n Status  : Idling";
                            }
                            else
                                sStatus += "\n " + sLR;
                        }

                    if (td.GPSDataUID == th.GPSDataStartUID)
                    {
                        isStop = false;
                        bBearing = -999999.999;
                    }
                    else if (td.GPSDataUID == th.GPSDataEndUID)
                    {
                        isStop = true;
                        bBearing = -999999.999;
                    }

                    if (bBearing == 0)
                        bBearing = calculateBearingTo(dPrevLat, dPrevLon, locY, locX);
                    dPrevLon = locX;
                    dPrevLat = locY;

                    string strDateTime = td.GpsData.DateTimeGPS_UTC.Add(th.myAsset.TimeZoneTS).ToString();

                    //String Alert = new Exceptions().GetExceptionsCommentByGPSDataID(td.GpsData.UID.ToString());
                    String Alert = String.Empty;
                    if (!String.IsNullOrEmpty(td.GpsData.Availability))
                        Alert = "\nAlert :" + td.GpsData.Availability;
                    if (Alert == String.Empty)
                        td.GpsData.DirectionImage = strDirectionPath + getPointImage(bBearing, false, isStop, iVehicleCount, false);
                    else
                        td.GpsData.DirectionImage = strDirectionPath + "Suspect.png";

                    sStatus += "\n Speed   : " + td.GpsData.Speed.ToString();

                    td.GpsData.Availability = "ActionImg : " + getPointImage(bBearing, false, isStop, iVehicleCount, false) + "\n" + sStatus + "\nDate    : " + strDateTime + "\n" + Alert;
                    myID++;
                }
            }
            return lth;
        }
        void GetExtent(PointD d)
        {
            if (d.X == 0)
                return;
            if (d.Y == 0)
                return;

            if (Math.Abs(d.X) < Math.Abs(minX))
                minX = d.X;
            if (Math.Abs(d.Y) < Math.Abs(minY))
                minY = d.Y;
            if (Math.Abs(d.X) > Math.Abs(maxX))
                maxX = d.X;
            if (Math.Abs(d.Y) > Math.Abs(maxY))
                maxY = d.Y;
        }
    }
}
