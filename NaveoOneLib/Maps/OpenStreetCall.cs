﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;

namespace NaveoOneLib.Maps
{
    public class OpenStreetCall
    {
        public string GetAddress(double lat, double lon)
        {
            string sResult = string.Empty;
            string name = string.Empty;
            try
            {
                //Explanations
                //https://nominatim.openstreetmap.org/reverse.php?format=html&lat=-20.272970229882326&lon=57.56732940673829&zoom=18
                //https://wiki.openstreetmap.org/wiki/Downloading_data#Choose_your_region
                //http://extract.bbbike.org/

                //http://wiki.openstreetmap.org/wiki/Nominatim#Parameters_2
                //http://nominatim.openstreetmap.org/reverse.php?format=html&lat=-9.69894970215551&lon=27.439454197883606&zoom=
                string sReverseGeoURL = string.Format("http://nominatim.openstreetmap.org/reverse?email=Reza.Dowlut@naveo.mu&format=json&lat={0}&lon={1}&zoom=18&addressdetails=1", lat, lon);

                var req = (HttpWebRequest)WebRequest.Create(sReverseGeoURL);
                req.Method = "GET";
                req.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:2.0) Gecko/20100101 Firefox/4.0";
                req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                req.ContentType = "application/x-www-form-urlencoded";
                req.Timeout = 5000;

                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
                Encoding encoding = ASCIIEncoding.ASCII;
                using (var reader = new System.IO.StreamReader(resp.GetResponseStream(), encoding))
                {
                    string responseText = reader.ReadToEnd();
                    dynamic sData = new JavaScriptSerializer().Deserialize<dynamic>(responseText);
                    var values = (IDictionary<string, object>)sData;
                    name = values["display_name"].ToString();
                }
            }
            catch
            {
                name = String.Empty;
                //return "Timeout Issue " + ex.ToString();
            }

            return name;
        }
    }
}
