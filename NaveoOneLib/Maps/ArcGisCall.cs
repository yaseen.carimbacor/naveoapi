﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace NaveoOneLib.Maps
{
    public static class ArcGisCall
    {
        public static string GetAddress(double lat, double lon)
        {
            string sResult = string.Empty;
            string name = string.Empty;
            try
            {
                //Explanations
                //http://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/reverseGeocode?f=pjson&featureTypes=&location=31.097406,-17.892156

                string sReverseGeoURL = string.Format("http://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/reverseGeocode?f=pjson&featureTypes=&location={0},{1}", lon, lat);

                var req = (HttpWebRequest)WebRequest.Create(sReverseGeoURL);
                req.Method = "GET";
                req.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:2.0) Gecko/20100101 Firefox/4.0";
                req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                req.ContentType = "application/x-www-form-urlencoded";
                req.Timeout = 5000;

                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
                Encoding encoding = ASCIIEncoding.ASCII;
                using (var reader = new System.IO.StreamReader(resp.GetResponseStream(), encoding))
                {
                    string responseText = reader.ReadToEnd();
                    dynamic sData = new JavaScriptSerializer().Deserialize<dynamic>(responseText);
                    try
                    {
                        if (sData.Count == 1)
                        {
                            var err = sData["error"];
                            if (err["details"][0].ToString() == "Unable to find address for the specified location.")
                                return err["details"][0].ToString();
                        }
                    }
                    catch { }
                    var values = sData["address"];
                    name = values["LongLabel"].ToString();
                }
            }
            catch
            {
                name = new BINGAPICall().GetAddress(lat, lon);
            }
            return name;
        }
    }
}
