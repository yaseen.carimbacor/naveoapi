﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EGIS.ShapeFileLib;
using System.Net;
using System.IO;
using System.Xml;
using System.Data;
using NaveoOneLib.Common;

namespace NaveoOneLib.Maps
{
    public class BINGAPICall
    {
        string sResult = string.Empty;
        public string GetAddress(double lat, double lon)
        {
            try
            {
                string sReverseGeoURL = string.Format("http://dev.virtualearth.net/REST/v1/Locations/{0},{1}?o=xml&key=AkmYu5eglcqE2oT9ImUXXAo0fNUTdSPCTiJH6mcPUcJq5VuTJ725kB8gW1w4ck_k", lat, lon);

                //Make Request                
                XmlDocument locationsResponse = new XmlDocument();
                try
                {
                    HttpWebRequest request = WebRequest.Create(sReverseGeoURL) as HttpWebRequest;
                    HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                    locationsResponse.Load(response.GetResponseStream());
                }
                catch 
                {
                    return new OpenStreetCall().GetAddress(lat, lon);
                }

                //Process Request

                //Create namespace manager
                XmlNamespaceManager nsmgr = new XmlNamespaceManager(locationsResponse.NameTable);
                nsmgr.AddNamespace("rest", "http://schemas.microsoft.com/search/local/ws/rest/v1");

                //Get formatted addresses: Option 1
                //Get all locations in the response and then extract the formatted address for each location
                XmlNodeList locationElements = locationsResponse.SelectNodes("//rest:Location", nsmgr);

                //Console.WriteLine("Show all formatted addresses: Option 1");
                foreach (XmlNode location in locationElements)
                {
                    String s = (location.SelectSingleNode(".//rest:FormattedAddress", nsmgr).InnerText);
                    if (s.Length > sResult.Length)
                        sResult = s;
                }
                //Console.WriteLine();

                //Get formatted addresses: Option 2
                //Get all formatted addresses directly. This works because there is only one formatted address for each location.
                //XmlNodeList formattedAddressElements = locationsResponse.SelectNodes("//rest:FormattedAddress", nsmgr);
                //Console.WriteLine("Show all formatted addresses: Option 2");
                //foreach (XmlNode formattedAddress in formattedAddressElements)
                //{
                //    sResult = formattedAddress.InnerText;
                //}

                ////Get the Geocode Points to use for display for each Location
                //XmlNodeList locationElementsForGP = locationsResponse.SelectNodes("//rest:Location", nsmgr);
                //Console.WriteLine("Show Goeocode Point Data");
                //foreach (XmlNode location in locationElements)
                //{
                //    XmlNodeList displayGeocodePoints = location.SelectNodes(".//rest:GeocodePoint/rest:UsageType[.='Display']/parent::node()", nsmgr);
                //    Console.Write(location.SelectSingleNode(".//rest:FormattedAddress", nsmgr).InnerText);
                //    Console.WriteLine(" has " + displayGeocodePoints.Count.ToString() + " display geocode point(s).");
                //}
                //Console.WriteLine();

                ////Get all locations that have a MatchCode=Good and Confidence=High
                //XmlNodeList matchCodeGoodElements = locationsResponse.SelectNodes("//rest:Location/rest:MatchCode[.='Good']/parent::node()", nsmgr);
                //Console.WriteLine("Show all addresses with MatchCode=Good and Confidence=High");
                //foreach (XmlNode location in matchCodeGoodElements)
                //{
                //    if (location.SelectSingleNode(".//rest:Confidence", nsmgr).InnerText == "High")
                //    {
                //        Console.WriteLine(location.SelectSingleNode(".//rest:FormattedAddress", nsmgr).InnerText);
                //    }
                //}               
            }
            catch
            {
                return new OpenStreetCall().GetAddress(lat, lon);
            }
            return sResult;
        }
    }
}
