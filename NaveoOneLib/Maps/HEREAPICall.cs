﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EGIS.ShapeFileLib;
using System.Net;
using System.IO;
using System.Xml;
using System.Data;
using NaveoOneLib.Common;

namespace NaveoOneLib.Maps
{
    public class Route
    {
        private int routeid = Int32.MinValue;
        
        public int Routeid
        {
            get { return routeid; }
            set { routeid = value; }
        }
        private string instruction = string.Empty;

        public string Instruction
        {
            get { return instruction; }
            set { instruction = value; }
        }
        private PointD point;

        public PointD Point
        {
            get { return point; }
            set { point = value; }
        }
        private string traveltime = string.Empty;

        public string Traveltime
        {
            get { return traveltime; }
            set { traveltime = value; }
        }
        private double lenght = 0.0;

        public double Lenght
        {
            get { return lenght; }
            set { lenght = value; }
        }
    }

    public class HEREAPICall
    {
        public static String AppID = "prdeR4xupruda5aXubac";        //DemoAppId01082013GAL
        public static String AppCode = "r20APwFEHdA1AqJdVcKZ9A";    //AJKnXv84fjrb0KIHawS0Tg

        string sResult = string.Empty;
        List<Route> lRoutes = new List<Route>();

        public DataTable GetRoute()
        {

            string sRouteURL = string.Format("http://route.cit.api.here.com/routing/7.2/calculateroute.xml?app_id=DemoAppId01082013GAL&app_code=AJKnXv84fjrb0KIHawS0Tg&waypoint0=geo!-20.22511,57.50616&waypoint1=geo!-20.23032,57.47056&mode=fastest;car;traffic:disabled");
            var req = (HttpWebRequest)WebRequest.Create(sRouteURL);
            req.Method = "GET";

            req.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:2.0) Gecko/20100101 Firefox/4.0";
            req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            req.ContentType = "application/x-www-form-urlencoded";

            req.Timeout = 5000;
            var resp = (HttpWebResponse)req.GetResponse();

            var ms = new MemoryStream();
            var rs = resp.GetResponseStream();
            if (rs != null)
            {
                var buf = new byte[4096];
                int len = 0;
                while ((len = rs.Read(buf, 0, buf.Length)) > 0)
                {
                    ms.Write(buf, 0, len);
                }
                rs.Close();
            }

            var xml = new XmlDocument();
            xml.LoadXml(Encoding.UTF8.GetString(ms.ToArray()));

            XmlNamespaceManager nsmanager = new XmlNamespaceManager(xml.NameTable);
            nsmanager.AddNamespace("rtcr", "http://www.navteq.com/lbsp/Routing-CalculateRoute/4");


            var status = xml.SelectNodes("/rtcr:CalculateRoute/Response/Route/Leg[1]/Maneuver", nsmanager);


            if (status.Count > 0)
            {
                int count = status.Count;
                while (count > 0)
                {
                    //Instructions
                    var length = xml.SelectNodes("/rtcr:CalculateRoute/Response/Route/Leg[1]/Maneuver[" + count + "]/Instruction", nsmanager);
                    sResult = length.Item(0).InnerXml.ToString();

                    sResult = sResult.Replace("&lt", "#").Replace("&gt", "@").Replace(";", "");
                    for (int i = 0; i < sResult.Length; i++)
                    {
                        if (sResult.IndexOf('#') > 0)
                        {
                            int s = sResult.IndexOf("#");
                            int e = sResult.IndexOf("@");
                            sResult = sResult.Remove(s, (e - s) + 1);
                        }
                    }

                    //Position
                    PointD point = new PointD();
                    var lat = xml.SelectNodes("/rtcr:CalculateRoute/Response/Route/Leg[1]/Maneuver[" + count + "]/Position/Latitude", nsmanager);
                    var lon = xml.SelectNodes("/rtcr:CalculateRoute/Response/Route/Leg[1]/Maneuver[" + count + "]/Position/Longitude", nsmanager);

                    point.X = Convert.ToDouble(lat.Item(0).InnerXml.ToString());
                    point.Y = Convert.ToDouble(lon.Item(0).InnerXml.ToString());

                    //TravelTime
                    var traveltime = xml.SelectNodes("/rtcr:CalculateRoute/Response/Route/Leg[1]/Maneuver[" + count + "]/TravelTime", nsmanager);

                    //Length
                    var distance = xml.SelectNodes("/rtcr:CalculateRoute/Response/Route/Leg[1]/Maneuver[" + count + "]/Length", nsmanager);
                    lRoutes.Add(new Route
                    {

                        Instruction = sResult,
                        Lenght = Convert.ToDouble(distance.Item(0).InnerXml.ToString()),
                        Point = point,
                        Traveltime = traveltime.Item(0).InnerXml.ToString()
                    });
                    count--;
                }
            }
            DataTable dt = new myConverter().ListToDataTable(lRoutes);
            return dt;
        }
        public PointD GetPoint(string address)
        {
            PointD point = new PointD();
            string sGeoURL = string.Format("http://geocoder.cit.api.here.com/6.2/geocode.xml?app_id=DemoAppId01082013GAL&app_code=AJKnXv84fjrb0KIHawS0Tg&gen=8&searchtext={0}&responseattributes=none&locationattributes=none&maxresults=1", "Riviere du Rempart, Mauritius");
            var req = (HttpWebRequest)WebRequest.Create(sGeoURL);
            req.Method = "GET";

            req.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:2.0) Gecko/20100101 Firefox/4.0";
            req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            req.ContentType = "application/x-www-form-urlencoded";

            req.Timeout = 5000;
            var resp = (HttpWebResponse)req.GetResponse();

            var ms = new MemoryStream();
            var rs = resp.GetResponseStream();
            if (rs != null)
            {
                var buf = new byte[4096];
                int len = 0;
                while ((len = rs.Read(buf, 0, buf.Length)) > 0)
                {
                    ms.Write(buf, 0, len);
                }
                rs.Close();
            }

            var xml = new XmlDocument();
            xml.LoadXml(Encoding.UTF8.GetString(ms.ToArray()));

            XmlNamespaceManager nsmanager = new XmlNamespaceManager(xml.NameTable);
            nsmanager.AddNamespace("ns2", "http://www.navteq.com/lbsp/Search-Search/4");


            var status = xml.SelectNodes("/ns2:Search/Response/View/Result[1]/Location/NavigationPosition", nsmanager);
            if (status.Count == 1)
            {
                var lat = xml.SelectNodes("/ns2:Search/Response/View/Result[1]/Location/NavigationPosition/Latitude", nsmanager);
                var lng = xml.SelectNodes("/ns2:Search/Response/View/Result[1]/Location/NavigationPosition/Longitude", nsmanager);

                point.X = Convert.ToDouble(lat.Item(0).InnerXml.ToString());
                point.Y = Convert.ToDouble(lng.Item(0).InnerXml.ToString());
            }
            return point;
        }
        public string GetAddress(double lat, double lon)
        {
            try
            {
                AppID = "DemoAppId01082013GAL";        //DemoAppId01082013GAL
                AppCode = "AJKnXv84fjrb0KIHawS0Tg";    //AJKnXv84fjrb0KIHawS0Tg

                //{0},{1},50 > 50 m radius
                string sReverseGeoURL = string.Format("http://reverse.geocoder.cit.api.here.com/6.2/reversegeocode.xml?app_id=" + AppID + "&app_code=" + AppCode + "&gen=8&prox={0},{1},50&mode=retrieveAddresses", lon, lat);
                var req = (HttpWebRequest)WebRequest.Create(sReverseGeoURL);
                req.Method = "GET";

                req.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:2.0) Gecko/20100101 Firefox/4.0";
                req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                req.ContentType = "application/x-www-form-urlencoded";

                req.Timeout = 5000;
                var resp = (HttpWebResponse)req.GetResponse();

                var ms = new MemoryStream();
                var rs = resp.GetResponseStream();
                if (rs != null)
                {
                    var buf = new byte[4096];
                    int len = 0;
                    while ((len = rs.Read(buf, 0, buf.Length)) > 0)
                    {
                        ms.Write(buf, 0, len);
                    }
                    rs.Close();
                }

                var xml = new XmlDocument();
                xml.LoadXml(Encoding.UTF8.GetString(ms.ToArray()));

                XmlNamespaceManager nsmanager = new XmlNamespaceManager(xml.NameTable);
                nsmanager.AddNamespace("ns2", "http://www.navteq.com/lbsp/Search-Search/4");
                var status = xml.SelectNodes("/ns2:Search/Response/View/Result[1]/Location/Address", nsmanager);
                if (status.Count == 1)
                {
                    var address = xml.SelectNodes("/ns2:Search/Response/View/Result[1]/Location/Address/Label", nsmanager);
                    sResult = address.Item(0).InnerXml.ToString();
                }
                return sResult;
            }
            catch 
            {
                return "Timeout Issue";
            }
        }
    }
}
