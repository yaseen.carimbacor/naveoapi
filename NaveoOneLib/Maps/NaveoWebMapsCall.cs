﻿using NaveoOneLib.Constant;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Maps;
using NaveoOneLib.Services;
using NaveoOneLib.Services.BLUP;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Services.Zones;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace NaveoOneLib.Maps
{
    public static class NaveoWebMapsCall
    {
        static String Post = "POST";
        static String Get = "GET";
        static HttpWebRequest httpWebRequest(String Method, String url)
        {
            //string sReverseGeoURL = string.Format("http://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/reverseGeocode?f=pjson&featureTypes=&location={0},{1}", lon, lat);
            Ssl.EnableTrustedHosts();

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = Method;
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:2.0) Gecko/20100101 Firefox/4.0";
            request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            request.ContentType = "application/json";
            //request.Timeout = 5000;

            //request.Headers.Add("X-APPLICATION-CODE", "NAVEO");

            return request;
        }

        #region Zones
        class ZoneCreation
        {
            public Guid UserToken { get; set; }
            public DataTable dtData { get; set; }
        }
        public static string CreateZones(Guid UserToken, String sConnStr)
        {
            String responseString = String.Empty;

            try
            {
                //Body
                DataTable dtData = new ZoneService().dtGetZonesSpatial(UserToken, sConnStr);
                if(dtData != null)
                {
                    if (dtData.Rows.Count == 0)
                        return "No Zones found";

                    ZoneCreation zc = new Maps.NaveoWebMapsCall.ZoneCreation();
                    zc.dtData = dtData;
                    zc.UserToken = UserToken;
                    String jsData = JsonConvert.SerializeObject(zc);
                    byte[] body = Encoding.ASCII.GetBytes(jsData);

                    HttpWebRequest request = httpWebRequest(Post, NaveoEntity.NaveoWebMapAddress + "InMemory/CreateZones");
                    request.ContentLength = body.Length;

                    //Sending
                    using (System.IO.Stream stream = request.GetRequestStream())
                    {
                        stream.Write(body, 0, body.Length);
                    }

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    responseString = response.StatusDescription;
                }

            }
            catch (Exception ex)
            {
                responseString = ex.ToString();
            }
            return responseString;
        }
        public static string CreateShp(Guid UserToken, String sConnStr)
        {
            String responseString = String.Empty;

            try
            {
                //Body
                DataTable dtData = new ZoneService().dtGetZonesSpatial(UserToken, sConnStr);
                if (dtData.Rows.Count == 0)
                    return "No Zones found";

                ZoneCreation zc = new Maps.NaveoWebMapsCall.ZoneCreation();
                zc.dtData = dtData;
                zc.UserToken = UserToken;
                String jsData = JsonConvert.SerializeObject(zc);
                byte[] body = Encoding.ASCII.GetBytes(jsData);

                HttpWebRequest request = httpWebRequest(Post, NaveoEntity.NaveoWebMapAddress + "InMemory/CreateZones");
                request.ContentLength = body.Length;

                //Sending
                using (System.IO.Stream stream = request.GetRequestStream())
                {
                    stream.Write(body, 0, body.Length);
                }

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                responseString = response.StatusDescription;
            }
            catch (Exception ex)
            {
                responseString = ex.ToString();
            }
            return responseString;
        }

        #endregion

        #region Trips
        class TripData
        {
            public Guid UserToken { get; set; }
            public DataTable dtData { get; set; }
        }

        public static String CreateTripsSpatial(Guid UserToken, List<int> lAssetID, DateTime dtFrom, DateTime dtTo, Boolean byDriver, String sConnStr)
        {
            String responseString = String.Empty;

            try
            {
                //Body
                List<Matrix> lm = new MatrixService().GetMatrixByUserToken(UserToken, sConnStr);
                DataTable dt = new Services.Trips.TripHeaderService().GetTripsSpatial(lAssetID, dtFrom, dtTo, byDriver, lm, sConnStr);
                if (dt.Rows.Count == 0)
                    return "No Trips found";

                TripData td = new Maps.NaveoWebMapsCall.TripData();
                td.dtData = dt;
                td.UserToken = UserToken;
                String jsData = JsonConvert.SerializeObject(td);
                byte[] body = Encoding.ASCII.GetBytes(jsData);

                HttpWebRequest request = httpWebRequest(Post, NaveoEntity.NaveoWebMapAddress + "ShapeFiles/CreateTrips");
                request.ContentLength = body.Length;

                //Sending
                using (System.IO.Stream stream = request.GetRequestStream())
                {
                    stream.Write(body, 0, body.Length);
                }

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                responseString = response.StatusDescription;
            }
            catch (Exception ex)
            {
                responseString = ex.ToString();
            }
            return responseString;
        }
        #endregion

        #region Routing
        public class LonLat
        {
            public Double lon { get; set; }
            public Double lat { get; set; }
        }

        public static String CreateRouting(RoutingData routing)
        {
            String responseString = String.Empty;

            try
            {
                //Body
                String jsData = JsonConvert.SerializeObject(routing);
                byte[] body = Encoding.ASCII.GetBytes(jsData);

                HttpWebRequest request = httpWebRequest(Post, NaveoEntity.NaveoWebMapAddress + "Routing/CreateRoute");
                request.ContentLength = body.Length;

                //Sending
                using (System.IO.Stream stream = request.GetRequestStream())
                {
                    stream.Write(body, 0, body.Length);
                }

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                responseString = response.StatusDescription;
                if (response.Headers.AllKeys.Contains("path"))
                    responseString = response.Headers["path"];
            }
            catch (Exception ex)
            {
                responseString = ex.ToString();
            }
            return responseString;
        }
        #endregion

        #region BLUP
        class BlupCreation
        {
            public Guid BlupPath { get; set; }
            public DataTable dtData { get; set; }
            public String dtTableName { get; set; }
            public Double lon { get; set; }
            public Double lat { get; set; }
            public Boolean bShowBuffers { get; set; }

            public DataSet ds { get; set; }
        }
        public static string CreateBLUP(Guid UserToken, String sConnStr)
        {
            String responseString = String.Empty;

            try
            {
                //Body
                DataTable dtData = new BLUPService().dtGetBlupSpatial(UserToken, sConnStr);
                if (dtData.Rows.Count == 0)
                    return "No BLUP found";

                ZoneCreation zc = new Maps.NaveoWebMapsCall.ZoneCreation();
                zc.dtData = dtData;
                zc.UserToken = UserToken;
                String jsData = JsonConvert.SerializeObject(zc);
                byte[] body = Encoding.ASCII.GetBytes(jsData);

                HttpWebRequest request = httpWebRequest(Post, NaveoEntity.NaveoWebMapAddress + "ShapeFiles/CreateBLUP");
                request.ContentLength = body.Length;

                //Sending
                using (System.IO.Stream stream = request.GetRequestStream())
                {
                    stream.Write(body, 0, body.Length);
                }

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                responseString = response.StatusDescription;
            }
            catch (Exception ex)
            {
                responseString = ex.ToString();
            }
            return responseString;
        }
        public static string GetFADataFromMapServer(DataTable dtBlupData, Double lon, Double lat, Boolean bShowBuffers, String MapPath, String sConnStr)
        {
            String responseString = String.Empty;
            try
            {
                //Body
                if (dtBlupData.Rows.Count == 0)
                    return "No Fixed Assets found";

                BlupCreation zc = new Maps.NaveoWebMapsCall.BlupCreation();
                zc.dtData = dtBlupData;
                zc.dtTableName = dtBlupData.TableName;
                zc.BlupPath = Guid.NewGuid();
                zc.lon = lon;
                zc.lat = lat;
                zc.bShowBuffers = bShowBuffers;
                String jsData = JsonConvert.SerializeObject(zc);
                byte[] body = Encoding.ASCII.GetBytes(jsData);

                HttpWebRequest request = httpWebRequest(Post, NaveoEntity.NaveoWebMapAddress + MapPath);
                request.ContentLength = body.Length;

                //Sending
                using (System.IO.Stream stream = request.GetRequestStream())
                {
                    stream.Write(body, 0, body.Length);
                }

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    System.IO.StreamReader reader = new System.IO.StreamReader(response.GetResponseStream());
                    responseString = reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                responseString = ex.ToString();
            }
            return responseString;
        }
        #endregion

        #region Fixed Assets
        class FixedAssetsCreation
        {
            public String sPath { get; set; }
            public String sFeatureID { get; set; }
        }

        public static List<String> GetAllFeatureIDs(String shpPath)
        {
            List<String> l = new List<string>();
            //Body
            FixedAssetsCreation fac = new FixedAssetsCreation();
            fac.sPath = shpPath;
            String jsData = JsonConvert.SerializeObject(fac);
            byte[] body = Encoding.ASCII.GetBytes(jsData);

            HttpWebRequest request = httpWebRequest(Post, NaveoEntity.NaveoWebMapAddress + "ShapeFiles/GetAllFeatureIDs");
            request.ContentLength = body.Length;

            //Sending
            using (System.IO.Stream stream = request.GetRequestStream())
            {
                stream.Write(body, 0, body.Length);
            }

            //Deserializing
            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            using (Stream answer = response.GetResponseStream())
            {
                DataContractSerializer xmlSer = new DataContractSerializer(typeof(List<String>));
                l = (List<String>)xmlSer.ReadObject(answer);
            }

            return l;
        }
        public static List<KeyValuePair<string, string>> GetAllFeaturesByID(String shpPath, String FeatureID)
        {
            List<KeyValuePair<string, string>> lFeatures= new List<KeyValuePair<string, string>>();
            try
            {
                //Body
                FixedAssetsCreation fac = new FixedAssetsCreation();
                fac.sPath = shpPath;
                fac.sFeatureID = FeatureID;
                String jsData = JsonConvert.SerializeObject(fac);
                byte[] body = Encoding.ASCII.GetBytes(jsData);

                HttpWebRequest request = httpWebRequest(Post, NaveoEntity.NaveoWebMapAddress + "ShapeFiles/GetAllFeaturesByID");
                request.ContentLength = body.Length;

                //Sending
                using (System.IO.Stream stream = request.GetRequestStream())
                {
                    stream.Write(body, 0, body.Length);
                }

                //Deserializing
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                using (Stream answer = response.GetResponseStream())
                {
                    DataContractSerializer xmlSer = new DataContractSerializer(typeof(List<KeyValuePair<string, string>>));
                    lFeatures = (List<KeyValuePair<string, string>>)xmlSer.ReadObject(answer);
                }
            }
            catch(Exception ex) {
                KeyValuePair<String, String> k = new KeyValuePair<string, string>("err", ex.ToString());
                lFeatures.Add(k);
            }
            return lFeatures;
        }

        #endregion
        public static string GetDataFromMapServer(DataSet ds, Double lon, Double lat, Boolean bShowBuffers, String MapPath, String sConnStr)
        {
            String responseString = String.Empty;
            try
            {
                //Body

                BlupCreation zc = new Maps.NaveoWebMapsCall.BlupCreation();
                zc.ds = ds;
                zc.BlupPath = Guid.NewGuid();
                zc.lon = lon;
                zc.lat = lat;
                zc.bShowBuffers = bShowBuffers;
                String jsData = JsonConvert.SerializeObject(zc);
                byte[] body = Encoding.ASCII.GetBytes(jsData);

                HttpWebRequest request = httpWebRequest(Post, NaveoEntity.NaveoWebMapAddress + MapPath);
                request.ContentLength = body.Length;

                //Sending
                using (System.IO.Stream stream = request.GetRequestStream())
                {
                    stream.Write(body, 0, body.Length);
                }

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    System.IO.StreamReader reader = new System.IO.StreamReader(response.GetResponseStream());
                    responseString = reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                responseString = ex.ToString();
            }
            return responseString;
        }

    }
}
