﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoOneLib.Maps
{
    public static class SimpleAddress
    {
        public static DataRow[] GetAddresses(DataRow[] drAddresses, String Address, String Lon, String Lat)
        {
            NaveoOneLib.Maps.ESRIMapService esriMap = new NaveoOneLib.Maps.ESRIMapService();
            Boolean b = esriMap.LoadMapNeeded();
            if (b)
                esriMap.LoadMap();
            esriMap.ReadProjectNLoadMap();

            foreach (DataRow dr in drAddresses)
                dr[Address] = esriMap.GetAddress(Convert.ToDouble(dr[Lon]), Convert.ToDouble(dr[Lat])).Replace("'", " ");

            esriMap.UnloadProject();
            esriMap = null;

            return drAddresses;
        }

        public static String GetAddresses(Double Lon, Double Lat)
        {
            NaveoOneLib.Maps.ESRIMapService esriMap = new NaveoOneLib.Maps.ESRIMapService();
            Boolean b = esriMap.LoadMapNeeded();
            if (b)
                esriMap.LoadMap();
            esriMap.ReadProjectNLoadMap();

            String s = esriMap.GetAddress(Lon, Lat).Replace("'", " ");

            esriMap.UnloadProject();
            esriMap = null;

            return s;
        }

        public static String GetFillingStation(Double Lon, Double Lat)
        {
            NaveoOneLib.Maps.ESRIMapService esriMap = new NaveoOneLib.Maps.ESRIMapService();
            Boolean b = esriMap.LoadMapNeeded();
            if (b)
                esriMap.LoadMap();
            esriMap.ReadProjectNLoadMap();

            String s = esriMap.GetFillingStation(Lon, Lat).Replace("'", " ");

            esriMap.UnloadProject();
            esriMap = null;

            return s;
        }

    }
}
