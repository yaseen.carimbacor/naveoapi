﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace NaveoOneLib.Maps
{
    public static class MireoMapsCall
    {
        public static string GetAddress(double lat, double lon)
        {
            string sResult = string.Empty;
            string name = string.Empty;
            try
            {
                string sReverseGeoURL = string.Format("http://159.8.86.167/geocode/json?query=&lng={0}&lat={1}", lon, lat);

                var req = (HttpWebRequest)WebRequest.Create(sReverseGeoURL);
                req.Method = "GET";
                req.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:2.0) Gecko/20100101 Firefox/4.0";
                req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                req.ContentType = "application/x-www-form-urlencoded";
                req.Timeout = 5000;

                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
                Encoding encoding = ASCIIEncoding.ASCII;
                using (var reader = new System.IO.StreamReader(resp.GetResponseStream(), encoding))
                {
                    string responseText = reader.ReadToEnd();
                    dynamic sData = new JavaScriptSerializer().Deserialize<dynamic>(responseText);
                    var values = sData[0];
                    name = values["formatted_address"].ToString();
                }
            }
            catch { }
            return name;
        }
    }
}
