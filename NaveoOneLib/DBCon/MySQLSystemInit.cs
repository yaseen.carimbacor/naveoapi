﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using NaveoOneLib.Models;
using NaveoOneLib.Common;
using System.Reflection;

namespace NaveoOneLib.DbCon
{
    public class MySQLSystemInit
    {
        Connection _SqlConn;
        String myStrConn = String.Empty;
        String strDB = String.Empty;
        int dbExecute(String sql)
        {
            sql = sql.Replace("[dbo].", "");
            sql = sql.Replace("]", "");
            sql = sql.Replace("[", "");
            return _SqlConn.dbExecute(sql, myStrConn);
        }
        int dbExecute(String sql, MySql.Data.MySqlClient.MySqlTransaction tran, int i)
        {
            return _SqlConn.dbExecute(sql, tran, i, myStrConn);
        }
        int dbExecuteWithoutTransaction(String sql, int i)
        {
            _SqlConn.ScriptExecute(sql, myStrConn);
            return 1;
        }
        DataTable GetDataDT(String sql)
        {
            return _SqlConn.GetDataDT(sql, myStrConn);
        }

        public void UpdateDB(String sConnStr)
        {
            myStrConn = sConnStr;
            strDB = DBConn.DbCon.MySQLConn.GetDBName(sConnStr);

            DBConn.DbCon.MySQLConn.CreateDatabaseIfNotExist(sConnStr);

            String sql = String.Empty;
            String strUpd = String.Empty;
            DataTable dt = new DataTable();
            _SqlConn = new Connection(sConnStr);

            if (GetDataDT(ExistTableSql("GFI_SYS_INIT")).Rows.Count == 0)
                dbExecute("create table GFI_SYS_INIT( upd_id varchar(10) not null, upd_desc varchar(300) not null, upd_date datetime not null )");
            try
            {
                //Addinional fields for users should be in update 7 as well
                //Addinional fields for Driver should be update 127 as well
                #region init
                #region Update 94. Init Clean Ids
                strUpd = "Rez000094";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"
                            update GFI_SYS_INIT set upd_id = 'Rez000001' where upd_id = 'DIN0000001';
                            update GFI_SYS_INIT set upd_id = 'Rez000002' where upd_id = 'DIN000002';

                            update GFI_SYS_INIT set upd_id = 'Rez000018' where upd_id = 'CPR000018';
                            update GFI_SYS_INIT set upd_id = 'Rez000019' where upd_id = 'CPR000019';

                            update GFI_SYS_INIT set upd_id = 'Rez000055' where upd_id = 'DIN000055';
                            update GFI_SYS_INIT set upd_id = 'Rez000101' where upd_id = 'DIN0000101';
                            update GFI_SYS_INIT set upd_id = 'Rez000103' where upd_id = 'DIN0000103';
                            update GFI_SYS_INIT set upd_id = 'Rez000104' where upd_id = 'DIN0000104';
                            update GFI_SYS_INIT set upd_id = 'Rez000105' where upd_id = 'DIN0000105';
                            update GFI_SYS_INIT set upd_id = 'Rez000106' where upd_id = 'DIN0000106';
                            update GFI_SYS_INIT set upd_id = 'Rez000107' where upd_id = 'DIN0000107';

                            update GFI_SYS_INIT set upd_id = 'Rez000086' where upd_id = 'Vis0000086';
                            update GFI_SYS_INIT set upd_id = 'Rez000087' where upd_id = 'Vis000087';
                            update GFI_SYS_INIT set upd_id = 'Rez000090' where upd_id = 'DIN0000090';
                            update GFI_SYS_INIT set upd_id = 'Rez000091' where upd_id = 'DIN0000091';";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "Init Clean Ids");
                }
                #endregion

                #region Update 1 Audit Table
                strUpd = "Rez000001";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE SYS_Audit
                        (
	                          AuditId int NOT NULL AUTO_INCREMENT
	                        , AuditTypes NVARCHAR(6) NOT NULL
	                        , ReferenceCode NVARCHAR(20) NOT NULL
	                        , ReferenceDesc NVARCHAR(30) NULL
	                        , CreatedDate DATETIME NULL
	                        , AuditUser NVARCHAR(20) NULL
	                        , ReferenceTable NVARCHAR(50) NULL
	                        , CallerFunction NVARCHAR(50) NULL
	                        , SQLRemarks NVARCHAR(1000) NULL
	                        , CONSTRAINT PK_SYS_Audit PRIMARY KEY (AuditId ASC)
                        )";
                    if (GetDataDT(ExistTableSql("SYS_Audit")).Rows.Count == 0)
                        dbExecute(sql);
                    InsertDBUpdate(strUpd, "Audit Table");
                }
                #endregion
                #region Update 2. User
                strUpd = "Rez000002";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "CREATE TABLE GFI_SYS_LookUp(";
                    sql += "\r\n	LookUpId int AUTO_INCREMENT NOT NULL,";
                    sql += "\r\n	LookUpCode nvarchar(20) NOT NULL,";
                    sql += "\r\n	LookUpDesc nvarchar(50) NOT NULL,";
                    sql += "\r\n	LookUpParentID int NOT NULL,";
                    sql += "\r\n	CreatedBy nvarchar(20) NOT NULL,";
                    sql += "\r\n	CreatedDate datetime NOT NULL,";
                    sql += "\r\n	UpdatedBy nvarchar(20) NULL,";
                    sql += "\r\n	UpdatedDate datetime NULL,";
                    sql += "\r\n CONSTRAINT PK_GFI_SYS_LookUp PRIMARY KEY  ";
                    sql += "\r\n (";
                    sql += "\r\n    LookUpId ASC";
                    sql += "\r\n )";
                    sql += "\r\n)";
                    if (GetDataDT(ExistTableSql("GFI_SYS_LookUp")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "GFI_SYS_LookUp");
                }
                #endregion
                #region Update 3. Matrix
                strUpd = "Rez000003";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_SYS_Matrix
                            (
	                              MID INT AUTO_INCREMENT NOT NULL
	                            , GMID INT NOT NULL
	                            , SourceTable NVARCHAR(50) NULL
	                            , SourceID NVARCHAR(20) NULL
	                            , SourceIDField NVARCHAR(30) NULL
                                , CONSTRAINT PK_GFI_SYS_Matrix PRIMARY KEY (MID ASC)
                            )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_Matrix")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_SYS_GroupMatrix
                            (
	                              GMID INT NOT NULL AUTO_INCREMENT
	                            , GMDescription NVARCHAR(100) NULL
	                            , ParentGMID INT NULL
	                            , Remarks NCHAR(100) NULL
                                , CONSTRAINT PK_GFI_SYS_GroupMatrix PRIMARY KEY  ( GMID ASC)
                            )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrix")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_SYS_GroupMatrix ADD IsCompany int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_GroupMatrix", "IsCompany")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_SYS_GroupMatrix ADD CompanyCode nvarchar(15) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_GroupMatrix", "CompanyCode")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_SYS_GroupMatrix ADD LegalName nvarchar(150) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_GroupMatrix", "LegalName")).Rows.Count == 0)
                        dbExecute(sql);

                    GroupMatrix gmRoot = new GroupMatrix();
                    gmRoot = new NaveoOneLib.Services.GroupMatrixService().GetRoot(sConnStr);
                    if (gmRoot == null)
                    {
                        gmRoot = new GroupMatrix();
                        gmRoot.gmDescription = "ROOT";
                        gmRoot.remarks = "Root";
                        Boolean b = gmRoot.Save(gmRoot, true, sConnStr).data;
                        gmRoot = new NaveoOneLib.Services.GroupMatrixService().GetRoot(sConnStr);

                        GroupMatrix gm = new GroupMatrix();
                        gm.gmDescription = "Security";
                        gm.parentGMID = gmRoot.gmID;
                        gm.remarks = "Security Root";
                        gm.Save(gm, true, sConnStr);

                        gm = new GroupMatrix();
                        gm.gmDescription = "All_Clients";
                        gm.parentGMID = gmRoot.gmID;
                        gm.remarks = "Clients Root";
                        gm.Save(gm, true, sConnStr);
                    }
                    InsertDBUpdate(strUpd, "Matrix Tables");
                }
                #endregion
                #region Update 4. user
                strUpd = "Rez000004";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_SYS_User
                        (
	                          Username NVARCHAR(20) NOT NULL
	                        , Names NVARCHAR(50) NOT NULL
	                        , Email NVARCHAR(50) NULL
	                        , Tel NVARCHAR(10) NULL
	                        , MobileNo NVARCHAR(10) NULL
	                        , DateJoined DATETIME NULL
	                        , Status_b NVARCHAR(2) NOT NULL
	                        , Password NVARCHAR(50) NULL
	                        , MatrixID INT NOT NULL
	                        , AccessList NVARCHAR(50) NULL
	                        , StoreUnit_b NVARCHAR(2) NULL
	                        , UType_cbo NVARCHAR(20) NULL
	                        , CreatedBy NVARCHAR(20) NULL
	                        , CreatedDate DATETIME NULL
	                        , UpdatedBy NVARCHAR(20) NULL
	                        , UpdatedDate DATETIME NULL
	                        , Dummy1 NVARCHAR(50) NULL
	                        , LoginCount INT NULL
	                        , CONSTRAINT PK_SYS_BY_User PRIMARY KEY (Username ASC)
                        )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_User")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "User Tables");
                }
                #endregion
                #region Update 5. User Table updated
                strUpd = "Rez000005";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_SYS_User DROP PRIMARY KEY";
                    dbExecute(sql);
                    sql = @"ALTER TABLE GFI_SYS_User add UID int NOT NULL AUTO_INCREMENT Primary Key";
                    dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD AccessTemplateId int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "AccessTemplateId")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "User Table updated");
                }
                #endregion
                #region Update 6. FLT tables
                strUpd = "Rez000006";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_FLT_Asset(
	                    AssetID int AUTO_INCREMENT NOT NULL,
	                    AssetType nvarchar(20) NULL,
	                    MatrixID int NULL,
	                    Make nvarchar(30) NULL,
	                    Model nvarchar(50) NULL,
	                    Status_b nvarchar(50) NULL,
	                    CreatedDate datetime NULL,
	                    CreatedBy nvarchar(30) NULL,
	                    UpdatedDate datetime NULL,
	                    UpdatedBy nvarchar(30) NULL, 
	                    AssetNumber nvarchar(30) NULL, CONSTRAINT PK_GFI_FLT_Asset PRIMARY KEY (AssetID ASC)
                    )";
                    if (GetDataDT(ExistTableSql("GFI_FLT_Asset")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_FLT_AssetDeviceMap(
	                        MapID int AUTO_INCREMENT NOT NULL,
	                        AssetID int NULL,
	                        DeviceID nvarchar(50) NULL,
	                        SerialNumber nvarchar(50) NULL,
	                        Status_b nvarchar(2) NULL,
	                        CreatedBy nvarchar(30) NULL,
	                        CreatedDate datetime NULL,
	                        UpdatedBy nvarchar(30) NULL,
	                        UpdatedDate datetime NULL,
                         CONSTRAINT PK_GFI_FLT_AssetDeviceMap PRIMARY KEY  ( MapID ASC)
                        )";
                    if (GetDataDT(ExistTableSql("GFI_FLT_AssetDeviceMap")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_GPS_GPSData(
	                        UID int AUTO_INCREMENT NOT NULL,
	                        AssetID int NULL,
	                        DeviceID nvarchar(50) NOT NULL,
	                        DirverID nvarchar(50) NULL,
	                        DateTimeGPS_UTC datetime NULL,
	                        DateTimeServer datetime NULL,
	                        Longitude float NULL,
	                        Latitude float NULL,
	                        LongLatValidFlag tinyint NULL,
	                        Speed float NULL,
	                        EgineOn tinyint NULL,
	                        StopFlag tinyint NULL,
	                        TripDistance float NULL,
	                        TripTime float NULL,
	                        WorkHour tinyint NULL,
                         CONSTRAINT PK_GFI_GPS_GPSData PRIMARY KEY  ( UID ASC)
                        )";
                    if (GetDataDT(ExistTableSql("GFI_GPS_GPSData")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_GPS_GPSDataDetail(
	                        GPSDetailID bigint AUTO_INCREMENT NOT NULL,
	                        UID int NULL,
	                        TypeID nvarchar(30) NULL,
	                        TypeVaule nvarchar(30) NULL,
	                        UOM nvarchar(15) NULL,
	                        Remarks nvarchar(50) NULL, CONSTRAINT PK_GFI_GPS_GPSDataDetail PRIMARY KEY  ( GPSDetailID ASC)
                        )";
                    if (GetDataDT(ExistTableSql("GFI_GPS_GPSDataDetail")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_SYS_GroupMatrix(
	                        GMID int AUTO_INCREMENT NOT NULL,
	                        GMDescription nvarchar(100) NULL,
	                        ParentGMID int NULL,
	                        Remarks nchar(100) NULL, CONSTRAINT PK_GFI_SYS_GroupMatrix PRIMARY KEY  ( GMID ASC)
                        )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrix")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "FLT tables");
                }
                #endregion

                //Addinional fields for users should be here as well
                #region Update 7. TripHistory Tables
                strUpd = "Rez000007";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_GPS_TripHeader
                        (
	                          iID int AUTO_INCREMENT NOT NULL
	                        , StartLon float NOT NULL
	                        , StartLat float NULL
	                        , EndLon float NULL
	                        , EndLat float NULL
	                        , AssetID int NOT NULL
	                        , DriverID int NOT NULL
	                        , ExceptionFlag tinyint NULL
	                        , CONSTRAINT PK_GFI_GPS_TripHeader PRIMARY KEY (iID ASC)
                        )";
                    if (GetDataDT(ExistTableSql("GFI_GPS_TripHeader")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_GPS_TripDetail
                        (
	                          iID int AUTO_INCREMENT NOT NULL
	                        , HeaderiID int NOT NULL
	                        ,GPSDataUID int NOT NULL
	                        , CONSTRAINT PK_GFI_GPS_TripDetail PRIMARY KEY (iID ASC)
                        )";
                    if (GetDataDT(ExistTableSql("GFI_GPS_TripDetail")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"drop table GFI_SYS_Matrix";
                    if (GetDataDT(ExistTableSql("GFI_SYS_Matrix")).Rows.Count != 0)
                        dbExecute(sql);

                    sql = @"drop table GFI_SYS_GroupMatrixUser";
                    if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrixUser")).Rows.Count != 0)
                        dbExecute(sql);
                    sql = @"drop table GFI_SYS_User";
                    if (GetDataDT(ExistTableSql("GFI_SYS_User")).Rows.Count != 0)
                        dbExecute(sql);
                    sql = @"CREATE TABLE GFI_SYS_User
                        (
                              UID int NOT NULL AUTO_INCREMENT
	                        , Username NVARCHAR(20) NOT NULL
	                        , Names NVARCHAR(50) NOT NULL
	                        , Email NVARCHAR(50) NOT NULL
	                        , Tel NVARCHAR(10) NULL
	                        , MobileNo NVARCHAR(10) NULL
	                        , DateJoined DATETIME NULL
	                        , Status_b NVARCHAR(2) NOT NULL
	                        , Password NVARCHAR(50) NULL
	                        , AccessList NVARCHAR(10) NULL
	                        , StoreUnit_b NVARCHAR(2) NULL
	                        , UType_cbo NVARCHAR(20) NULL
	                        , CreatedBy int null 
	                        , CreatedDate DATETIME NULL
	                        , UpdatedBy int null 
	                        , UpdatedDate DATETIME NULL
	                        , Dummy1 NVARCHAR(50) NULL
	                        , LoginCount INT NULL
	                        , CONSTRAINT PK_SYS_BY_User PRIMARY KEY (UID ASC)
                        )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_User")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"CREATE UNIQUE INDEX IX_GFI_SYS_User ON GFI_SYS_User (Email)";
                    if (GetDataDT(ExistsSQLConstraint("IX_GFI_SYS_User", "GFI_SYS_User")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"CREATE TABLE GFI_SYS_GroupMatrixUser
                            (
	                              MID INT AUTO_INCREMENT NOT NULL
	                            , GMID INT NOT NULL
	                            , iID INT NOT NULL
                                , CONSTRAINT PK_GFI_SYS_GroupMatrixUser PRIMARY KEY  (MID ASC)
                             )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrixUser")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixUser 
                            ADD CONSTRAINT FK_GFI_SYS_GroupMatrixUser_GFI_SYS_GroupMatrix 
                                FOREIGN KEY (GMID) 
                                REFERENCES GFI_SYS_GroupMatrix (GMID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixUser_GFI_SYS_GroupMatrix", "GFI_SYS_GroupMatrixUser")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixUser 
                            ADD CONSTRAINT FK_GFI_SYS_GroupMatrixUser_GFI_SYS_User 
                                FOREIGN KEY(iID) 
                                REFERENCES GFI_SYS_User(UID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixUser_GFI_SYS_User", "GFI_SYS_GroupMatrixUser")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD AccessTemplateId int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "AccessTemplateId")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD LoginAttempt int Not NULL Default 1";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "LoginAttempt")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD PreviousPassword nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "PreviousPassword")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD PasswordUpdateddate DateTime NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "PasswordUpdateddate")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD MaxDayMail int not NULL Default 25";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "MaxDayMail")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD UsrTypeID int NOT NULL Default 0";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "UsrTypeID")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD UsrTypeDesc nvarchar(50) NOT NULL Default 'Std User'";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "UsrTypeDesc")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD ZoneID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "ZoneID")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD PlnLkpVal int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "PlnLkpVal")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD MobileAccess int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "MobileAccess")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD ExternalAccess int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "ExternalAccess")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD TwoStepVerif nvarchar(10) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "TwoStepVerif")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD TwoStepAttempts int not NULL Default 0";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "TwoStepAttempts")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD UserToken VARCHAR(64) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "UserToken")).Rows.Count == 0)
                    {
                        dbExecute(sql);

                        sql = @"DELIMITER ;;
                                CREATE TRIGGER before_insert_GFI_SYS_User
                                BEFORE INSERT ON GFI_SYS_User
                                FOR EACH ROW
                                BEGIN
                                    IF new.UserToken IS NULL THEN
                                    SET new.UserToken = uuid();
                                    END IF;
                                END
                                ;;";
                        _SqlConn.ScriptExecute(sql, sConnStr);
                    }

                    sql = @"CREATE UNIQUE INDEX IX_UserToken_GFI_SYS_User ON GFI_SYS_User (UserToken)";
                    if (GetDataDT(ExistsSQLConstraint("IX_UserToken_GFI_SYS_User", "GFI_SYS_User")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD PswdExpiryDate datetime NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "PswdExpiryDate")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD PswdExpiryWarnDate datetime NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "PswdExpiryWarnDate")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD LastName nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "LastName")).Rows.Count == 0)
                        dbExecute(sql);

                    User u = new User();
                    u = u.GetUserByeMail("Installer@naveo.mu", "SysInitPass", sConnStr, false, false);
                    GroupMatrix gmRoot = new GroupMatrix();
                    gmRoot = new NaveoOneLib.Services.GroupMatrixService().GetRoot(sConnStr);
                    if (u.QryResult == User.UserResult.InvalidCredentials && u.UID == -999)
                    {
                        u = new User();
                        u.Username = "INSTALLER";
                        u.Email = "Installer@naveo.mu";
                        u.Names = "Installer";
                        u.Password = "CoreAdmin@123.";
                        u.Status_b = "AC";

                        Matrix m = new Matrix();
                        m.GMID = gmRoot.gmID;
                        m.oprType = DataRowState.Added;
                        u.lMatrix.Add(m);

                        u.Save(u, sConnStr, true);
                    }
                    u = new User();
                    u = u.GetUserByeMail("ADMIN@naveo.mu", "SysInitPass", sConnStr, false, false);
                    if (u.QryResult == User.UserResult.InvalidCredentials && u.UID == -999)
                    {
                        u = new User();
                        u.Username = "ADMIN";
                        u.Email = "ADMIN@naveo.mu";
                        u.Names = "Administrator";
                        u.Password = "Password@123.";
                        u.Status_b = "AC";

                        Matrix m = new Matrix();
                        m.GMID = gmRoot.gmID;
                        m.oprType = DataRowState.Added;
                        u.lMatrix.Add(m);

                        u.Save(u, sConnStr, true);
                    }

                    InsertDBUpdate(strUpd, "TripHistory Tables and matrix review");
                }
                #endregion
                #region Update 8. Asset Matrix review
                strUpd = "Rez000008";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"alter table GFI_GPS_GPSData drop column DirverID";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_GPSData", "DirverID")).Rows.Count != 0)
                        dbExecute(sql);

                    sql = @"alter table GFI_FLT_Asset drop column MatrixID";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Asset", "MatrixID")).Rows.Count != 0)
                        dbExecute(sql);
                    sql = @"CREATE TABLE GFI_SYS_GroupMatrixAsset
                            (
	                              MID INT AUTO_INCREMENT NOT NULL
	                            , GMID INT NOT NULL
	                            , iID INT NOT NULL
                                , CONSTRAINT PK_GFI_SYS_GroupMatrixAsset PRIMARY KEY  (MID ASC)
                             )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrixAsset")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixAsset 
                            ADD CONSTRAINT FK_GFI_SYS_GroupMatrixAsset_GFI_SYS_GroupMatrix 
                                FOREIGN KEY (GMID) 
                                REFERENCES GFI_SYS_GroupMatrix (GMID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixAsset_GFI_SYS_GroupMatrix", "GFI_SYS_GroupMatrixAsset")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixAsset 
                            ADD CONSTRAINT FK_GFI_SYS_GroupMatrixAsset_GFI_FLT_Asset 
                                FOREIGN KEY(iID) 
                                REFERENCES GFI_FLT_Asset(AssetID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixAsset_GFI_FLT_Asset", "GFI_SYS_GroupMatrixAsset")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_FLT_AssetDeviceMap 
                            ADD CONSTRAINT FK_GFI_FLT_AssetDeviceMap_GFI_FLT_Asset 
                                FOREIGN KEY(AssetID) 
                                REFERENCES GFI_FLT_Asset(AssetID) 
                             ON UPDATE  NO ACTION 
	                         ON DELETE  NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_AssetDeviceMap_GFI_FLT_Asset", "GFI_FLT_AssetDeviceMap")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_GPS_TripDetail 
                            ADD CONSTRAINT FK_GFI_GPS_TripDetail_GFI_GPS_TripHeader 
                                FOREIGN KEY(HeaderiID) 
                                REFERENCES GFI_GPS_TripHeader(iID) 
                             ON UPDATE  NO ACTION 
	                         ON DELETE  NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_TripDetail_GFI_GPS_TripHeader", "GFI_GPS_TripDetail")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_GPS_GPSDataDetail 
                            ADD CONSTRAINT FK_GFI_GPS_GPSDataDetail_GFI_GPS_GPSData 
                                FOREIGN KEY(UID) 
                                REFERENCES GFI_GPS_GPSData(UID)
                             ON UPDATE  NO ACTION 
	                         ON DELETE  NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_GPSDataDetail_GFI_GPS_GPSData", "GFI_GPS_GPSDataDetail")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_FLT_Driver(
	                        DriverID INT AUTO_INCREMENT NOT NULL,
	                        sDriverName nvarchar(50) NOT NULL,
	                        iButton nvarchar(50) NULL,
	                        sEmployeeNo nvarchar(50) NULL,
	                        sComments nvarchar(1024) NULL,
                             CONSTRAINT PK_GFI_FLT_Driver PRIMARY KEY  (DriverID ASC)
                        )";
                    if (GetDataDT(ExistTableSql("GFI_FLT_Driver")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"CREATE TABLE GFI_SYS_GroupMatrixDriver
                            (
	                              MID INT AUTO_INCREMENT NOT NULL
	                            , GMID INT NOT NULL
	                            , iID INT NOT NULL
                                , CONSTRAINT PK_GFI_SYS_GroupMatrixDriver PRIMARY KEY  (MID ASC)
                             )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrixDriver")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixDriver 
                            ADD CONSTRAINT FK_GFI_SYS_GroupMatrixDriver_GFI_SYS_GroupMatrix 
                                FOREIGN KEY (GMID) 
                                REFERENCES GFI_SYS_GroupMatrix (GMID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixDriver_GFI_SYS_GroupMatrix", "GFI_SYS_GroupMatrixDriver")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixDriver 
                            ADD CONSTRAINT FK_GFI_SYS_GroupMatrixDriver_GFI_FLT_Driver 
                                FOREIGN KEY(iID) 
                                REFERENCES GFI_FLT_Driver(DriverID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixDriver_GFI_FLT_Driver", "GFI_SYS_GroupMatrixDriver")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_GPS_TripHeader 
                            ADD CONSTRAINT FK_GFI_GPS_TripHeader_GFI_FLT_Asset 
                                FOREIGN KEY(AssetID) 
                                REFERENCES GFI_FLT_Asset(AssetID) 
                            ON UPDATE  NO ACTION 
	                        ON DELETE  NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_TripHeader_GFI_FLT_Asset", "GFI_GPS_TripHeader")).Rows.Count != 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_GPS_TripHeader 
                            ADD CONSTRAINT FK_GFI_GPS_TripHeader_GFI_FLT_Driver 
                                FOREIGN KEY(DriverID) 
                                REFERENCES GFI_FLT_Driver(DriverID) 
                            ON UPDATE  NO ACTION 
	                        ON DELETE  NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_TripHeader_GFI_FLT_Driver", "GFI_GPS_TripHeader")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_GPS_GPSData 
                            ADD CONSTRAINT FK_GFI_GPS_GPSData_GFI_FLT_Asset 
                                FOREIGN KEY(AssetID) 
                                REFERENCES GFI_FLT_Asset(AssetID) 
                            ON UPDATE  NO ACTION 
	                        ON DELETE  NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_GPSData_GFI_FLT_Asset", "GFI_GPS_GPSData")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"alter table GFI_GPS_GPSData drop column DeviceID";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_GPSData", "DeviceID")).Rows.Count != 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_GPS_GPSData add DriverID int NOT NULL DEFAULT 1";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_GPSData", "DriverID")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_GPS_GPSData 
                            ADD CONSTRAINT FK_GFI_GPS_GPSData_GFI_FLT_Driver 
                                FOREIGN KEY(DriverID) 
                                REFERENCES GFI_FLT_Driver(DriverID) 
                            ON UPDATE  NO ACTION 
	                        ON DELETE  NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_GPSData_GFI_FLT_Driver", "GFI_GPS_GPSData")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "Asset Matrix review and other Table Links");
                }
                #endregion

                //Addinional fields for Driver should be here as well
                #region Update 127. GFI_FLT_Driver Additional fields
                strUpd = "Rez000127";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_FLT_Driver ADD DateOfBirth datetime NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "DateOfBirth")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Gender nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Gender")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Address1 nvarchar(100) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Address1")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Address2 nvarchar(100) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Address2")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD City nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "City")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Nationality nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Nationality")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD sFirtName nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "sFirtName")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD LicenseNo1 nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "LicenseNo1")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD LicenseNo1Expiry datetime NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "LicenseNo1Expiry")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD LicenseNo2 nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "LicenseNo2")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Licenseno2Expiry datetime NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Licenseno2Expiry")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Ref1 nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Ref1")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD ZoneID nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "ZoneID")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD CreatedDate datetime NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "CreatedDate")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD CreatedBy nvarchar(50)  NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "CreatedBy")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD UpdatedDate datetime NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "UpdatedDate")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD UpdatedBy nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "UpdatedBy")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Status nvarchar(50)  NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Status")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Address3 nvarchar(50)  NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Address3")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD District nvarchar(25)  NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "District")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD PostalCode nvarchar(15)  NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "PostalCode")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD MobileNumber nvarchar(15)  NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "MobileNumber")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Reason nvarchar(500)  NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Reason")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Department nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Department")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Email nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Email")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD sLastName nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "sLastName")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD OfficerLevel nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "OfficerLevel")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD OfficerTitle nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "OfficerTitle")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Phone nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Phone")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Title nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Title")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD ASPUser nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "ASPUser")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD UserId int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "UserId")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Photo varBinary (1024) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Photo")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Type_Institution int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Type_Institution")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Type_Driver int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Type_Driver")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Type_Grade int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Type_Grade")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD HomeNo int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "HomeNo")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "GFI_FLT_Driver Additional fields");
                }
                #endregion
                #region Update 9. Default Driver
                strUpd = "Rez000009";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_FLT_Driver ADD EmployeeType nvarchar(15) NOT NULL Default 'Driver'";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "EmployeeType")).Rows.Count == 0)
                        dbExecute(sql);

                    Driver d = new Driver();
                    d = d.GetDefaultDriver(sConnStr);
                    GroupMatrix gmRoot = new GroupMatrix();
                    gmRoot = new NaveoOneLib.Services.GroupMatrixService().GetRoot(sConnStr);
                    if (d == null)
                    {
                        d = new Driver();
                        d.sDriverName = "Default Driver";
                        d.sComments = "Driver for vehicles with no Driver";
                        d.sEmployeeNo = "INSTALLER";
                        d.iButton = "00000000";

                        Matrix m = new Matrix();
                        m.GMID = gmRoot.gmID;
                        m.oprType = DataRowState.Added;
                        d.lMatrix.Add(m);

                        d.Save(d, true, sConnStr);
                    }

                    sql = @"ALTER TABLE GFI_FLT_AssetDeviceMap add StartDate DATETIME NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "StartDate")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_FLT_AssetDeviceMap add EndDate DATETIME NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "EndDate")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_GPS_GPSData modify column LongLatValidFlag INT NULL";
                    dbExecute(sql);
                    sql = @"ALTER TABLE GFI_GPS_GPSData modify column EgineOn INT NULL";
                    dbExecute(sql);
                    sql = @"ALTER TABLE GFI_GPS_GPSData modify column StopFlag INT NULL";
                    dbExecute(sql);
                    sql = @"ALTER TABLE GFI_GPS_GPSData modify column WorkHour INT NULL";
                    dbExecute(sql);

                    sql = @"ALTER TABLE GFI_GPS_TripHeader modify column ExceptionFlag INT NULL";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "Default Driver");
                }
                #endregion
                #region Update 10. TripHeader
                strUpd = "Rez000010";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_GPS_TripHeader add MaxSpeed int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "MaxSpeed")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_GPS_TripHeader add IdlingTime float NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "IdlingTime")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_GPS_TripHeader add StopTime float NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "StopTime")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_GPS_TripHeader add TripDistance float NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "TripDistance")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_GPS_TripHeader add TripTime float NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "TripTime")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_GPS_TripHeader add GPSDataStartUID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "GPSDataStartUID")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_GPS_TripHeader add GPSDataEndUID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "GPSDataEndUID")).Rows.Count == 0)
                        dbExecute(sql);
                    InsertDBUpdate(strUpd, "TripHeader");
                }
                #endregion
                #region Update 11. Zone
                strUpd = "Rez000011";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_FLT_ZoneHeader(
	                            ZoneID int AUTO_INCREMENT NOT NULL,
	                            Desciption nvarchar(255) NULL,
	                            Displayed int DEFAULT 1 NOT NULL,
	                            Comments nvarchar(1024) NULL,
	                            Color int DEFAULT 1 NOT NULL,
 	                            ZoneTypeID int DEFAULT 1 NOT NULL,
                            CONSTRAINT PK_GFI_FLT_ZoneHeader PRIMARY KEY  
                                (ZoneID ASC)
                         ) ";
                    if (GetDataDT(ExistTableSql("GFI_FLT_ZoneHeader")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"CREATE TABLE GFI_FLT_ZoneDetail(
	                        iID int AUTO_INCREMENT NOT NULL,
	                        Latitude float NULL,
	                        Longitude float NULL,
	                        ZoneID int NULL,
	                        CordOrder int null,
                            CONSTRAINT PK_GFI_FLT_ZoneDetail PRIMARY KEY  
                            (iID ASC)
                        )";
                    if (GetDataDT(ExistTableSql("GFI_FLT_ZoneDetail")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_FLT_ZoneDetail 
                            ADD CONSTRAINT FK_GFI_FLT_ZoneDetail_GFI_FLT_ZoneHeader 
                                FOREIGN KEY(ZoneID)
                                REFERENCES GFI_FLT_ZoneHeader (ZoneID)
                            ON DELETE CASCADE";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ZoneDetail_GFI_FLT_ZoneHeader", "GFI_FLT_ZoneDetail")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_SYS_GroupMatrixZone
                        (
	                        MID INT AUTO_INCREMENT NOT NULL
	                        , GMID INT NOT NULL
	                        , iID INT NOT NULL
                            , CONSTRAINT PK_GFI_SYS_GroupMatrixZone PRIMARY KEY  (MID ASC)
                        )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrixZone")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixZone 
                            ADD CONSTRAINT FK_GFI_SYS_GroupMatrixZone_GFI_SYS_GroupMatrix 
                                FOREIGN KEY (GMID) 
                                REFERENCES GFI_SYS_GroupMatrix (GMID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixZone_GFI_SYS_GroupMatrix", "GFI_SYS_GroupMatrixZone")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixZone 
                            ADD CONSTRAINT FK_GFI_SYS_GroupMatrixZone_GFI_FLT_ZoneHeader 
                                FOREIGN KEY(iID) 
                                REFERENCES GFI_FLT_ZoneHeader(ZoneID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixZone_GFI_FLT_ZoneHeader", "GFI_SYS_GroupMatrixZone")).Rows.Count == 0)
                        dbExecute(sql);
                    InsertDBUpdate(strUpd, "Zone");
                }
                #endregion
                #region Update 12. Timezone
                strUpd = "Rez000012";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_FLT_Asset add TimeZoneID nvarchar(50) NOT NULL DEFAULT 'Arabian Standard Time'";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Asset", "TimeZoneID")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = "ALTER TABLE GFI_FLT_Asset add Odometer int NOT NULL DEFAULT 0";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Asset", "Odometer")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = "ALTER TABLE GFI_FLT_AssetDeviceMap add MaxNoLogDays int NOT NULL DEFAULT 1";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "MaxNoLogDays")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "TimeZone");
                }
                #endregion
                #region Update 13. Asset Fields
                strUpd = "Rez000013";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"alter table GFI_FLT_Asset drop column Make";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Asset", "Make")).Rows.Count != 0)
                        dbExecute(sql);

                    sql = @"alter table GFI_FLT_Asset drop column Model";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Asset", "Model")).Rows.Count != 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_FLT_Asset add AssetName nvarchar(120) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Asset", "AssetName")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "CREATE TABLE CM_TH(";
                    sql += "\r\n	iId int AUTO_INCREMENT NOT NULL,";
                    sql += "\r\n	tId int NOT NULL,";
                    sql += "\r\n	CoretId int NOT NULL,";
                    sql += "\r\n CONSTRAINT PK_CM_TH PRIMARY KEY  ";
                    sql += "\r\n (";
                    sql += "\r\n    iId ASC";
                    sql += "\r\n )";
                    sql += "\r\n)";
                    if (GetDataDT(ExistTableSql("CM_TH")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"alter table GFI_GPS_TripHeader drop column StartLon";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "StartLon")).Rows.Count != 0)
                        dbExecute(sql);
                    sql = @"alter table GFI_GPS_TripHeader drop column StartLat";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "StartLat")).Rows.Count != 0)
                        dbExecute(sql);
                    sql = @"alter table GFI_GPS_TripHeader drop column EndLon";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "EndLon")).Rows.Count != 0)
                        dbExecute(sql);
                    sql = @"alter table GFI_GPS_TripHeader drop column EndLat";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "EndLat")).Rows.Count != 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_GPS_TripDetail DROP FOREIGN KEY FK_GFI_GPS_TripDetail_GFI_GPS_TripHeader";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_TripDetail_GFI_GPS_TripHeader", "GFI_GPS_TripDetail")).Rows.Count > 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_GPS_TripDetail WITH CHECK 
                            ADD CONSTRAINT FK_GFI_GPS_TripDetail_GFI_GPS_TripHeade
                                FOREIGN KEY(HeaderiID)
                                REFERENCES GFI_GPS_TripHeader(iID)
                            ON DELETE CASCADE";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_TripDetail_GFI_GPS_TripHeader", "GFI_GPS_TripDetail")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_GPS_GPSDataDetail DROP FOREIGN KEY FK_GFI_GPS_GPSDataDetail_GFI_GPS_GPSData";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_GPSDataDetail_GFI_GPS_GPSData", "GFI_GPS_GPSDataDetail")).Rows.Count > 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_GPS_GPSDataDetail 
                            ADD CONSTRAINT FK_GFI_GPS_GPSDataDetail_GFI_GPS_GPSData 
                                FOREIGN KEY(UID) 
                                REFERENCES GFI_GPS_GPSData(UID)
                            ON DELETE CASCADE";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_GPSDataDetail_GFI_GPS_GPSData", "GFI_GPS_GPSDataDetail")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "Asset Fields");
                }
                #endregion
                #region Update 14. Indexes
                strUpd = "Rez000014";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE UNIQUE INDEX my_IX_GFI_GPS_GPSData ON GFI_GPS_GPSData
                            (
	                            UID ASC,
	                            AssetID ASC,
	                            DateTimeGPS_UTC ASC,
	                            DriverID ASC
                            )";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSData", "my_IX_GFI_GPS_GPSData")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE INDEX my_IX_GFI_GPS_GPSDataDetail ON GFI_GPS_GPSDataDetail 
                            (
	                            UID ASC
                            )";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSDataDetail", "my_IX_GFI_GPS_GPSDataDetail")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE INDEX my_IX_GFI_GPS_TripDetail ON GFI_GPS_TripDetail 
                            (
	                            HeaderiID ASC
                            )";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripDetail", "my_IX_GFI_GPS_TripDetail")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE UNIQUE INDEX my_IX_GFI_GPS_TripHeader ON GFI_GPS_TripHeader
                            (
	                            iID ASC,
	                            AssetID ASC,
	                            DriverID ASC,
	                            ExceptionFlag ASC,
	                            GPSDataStartUID ASC,
	                            GPSDataEndUID ASC
                            )";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "my_IX_GFI_GPS_TripHeader")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE UNIQUE  INDEX my_IX_CM_TH ON CM_TH
                            (
	                            iId ASC,
	                            tId ASC
                            )";
                    if (GetDataDT(ExistsIndex("CM_TH", "my_IX_CM_TH")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "Indexes");
                }
                #endregion
                #region Update 15. CM_TH Extended
                strUpd = "Rez000015";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "CREATE TABLE CM_THExisting(";
                    sql += "\r\n	ExistingtId int NOT NULL,";
                    sql += "\r\n CONSTRAINT PK_CM_THExisting PRIMARY KEY  ";
                    sql += "\r\n (";
                    sql += "\r\n    ExistingtId ASC";
                    sql += "\r\n )";
                    sql += "\r\n)";
                    if (GetDataDT(ExistTableSql("CM_THExisting")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE UNIQUE  INDEX my_IX_CM_THExisting ON CM_THExisting 
                            (
	                            ExistingtId ASC
                            )";
                    if (GetDataDT(ExistsIndex("CM_TH", "my_IX_CM_THExisting")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "CM_TH Extended");
                }
                #endregion
                #region Update 16. Rules & Exceptions
                strUpd = "Rez000016";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_GPS_Rules(
	                            RuleID int NOT NULL,
	                            ParentRuleID int NULL,
	                            RuleType int NULL,
	                            AssetGMID int NULL,
	                            DriverGMID int NULL,
	                            MinTriggerValue int NULL,
	                            MaxTriggerValue int NULL,
	                            Field nvarchar(20) NULL,
	                            Value nvarchar(20) NULL,
                             CONSTRAINT PK_GFI_GPS_Rules PRIMARY KEY  
                            (
	                            RuleID ASC
                            )
                           )";
                    if (GetDataDT(ExistTableSql("GFI_GPS_Rules")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_GPS_Exceptions(
	                        iID int NOT NULL,
	                        RuleID int NULL,
	                        GPSDataID bigint NULL,
	                        AssetID int NULL,
	                        DriverID int NULL,
	                        Severity nvarchar(100) NULL,
                         CONSTRAINT PK_GFI_GPS_Exceptions PRIMARY KEY  
                        (
	                       iID ASC
                        ))";
                    if (GetDataDT(ExistTableSql("GFI_GPS_Exceptions")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "Rules and Exceptions");
                }
                #endregion
                #region Update 17. CMTrips
                strUpd = "Rez000017";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE CM_THAll
                            (
	                              TripID INT NOT NULL
	                            , DeviceName NVARCHAR(50) NULL
	                            , iButton NVARCHAR(50) NULL
	                            , iGPSDataID INT NULL
	                            , MaximumSpeed FLOAT NULL
	                            , IdlingTime INT NULL
	                            , StopTime FLOAT NULL
	                            , TripDistance FLOAT NULL
	                            , TripTime FLOAT NULL
	                            , TripStartTime DATETIME NULL
	                            , TripEndTime DATETIME NULL
	                            , CONSTRAINT PK_CM_THAll PRIMARY KEY (TripID ASC)
                            )";
                    if (GetDataDT(ExistTableSql("CM_THAll")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"drop table CM_THExisting";
                    if (GetDataDT(ExistTableSql("CM_THExisting")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "CMTrips");
                }
                #endregion

                User usrInst = new User();
                usrInst = usrInst.GetUserInstaller(sConnStr);
                Globals.uLogin = usrInst;

                #region Update 18. Assets Management Module
                strUpd = "Rez000018";
                if (!CheckUpdateExist(strUpd))
                {
                    #region Tables

                    // -- Table: GFI_AMM_VehicleMaintCat
                    sql = @"CREATE TABLE GFI_AMM_VehicleMaintCat
                            (
                             MaintCatId int NOT NULL,
                             Description nvarchar(120) NULL,
                             CreatedDate datetime NULL,
                             CreatedBy nvarchar(30) NULL,
                             UpdatedDate datetime NULL,
                             UpdatedBy nvarchar(30) NULL,
                             CONSTRAINT PK_GFI_AMM_VehicleMaintCat PRIMARY KEY  
                                (
                                 MaintCatId ASC
                                )
                            ) /*ON PRIMARY*/";
                    if (GetDataDT(ExistTableSql("GFI_AMM_VehicleMaintCat")).Rows.Count == 0)
                        dbExecute(sql);

                    // -- Table: GFI_AMM_AssetExtProFields
                    sql = @"CREATE TABLE GFI_AMM_AssetExtProFields(
	                        FieldId int NOT NULL,
	                        AssetType nvarchar(120) NULL,
	                        Category nvarchar(120) NULL,
	                        FieldName nvarchar(120) NULL,
	                        Label nvarchar(120) NULL,
	                        Description nvarchar(255) NULL,
	                        Type nvarchar(120) NULL,
	                        UnitOfMeasure nvarchar(120) NULL,
	                        KeepHistory char(1) NULL,
	                        DataRetentionDays int NULL,
	                        THWarning_Value int NULL,
	                        THWarning_TimeBased int NULL,
	                        THCritical_Value int NULL,
	                        THCritical_TimeBased int NULL,
	                        DisplayOrder int NULL,
	                        CreatedDate datetime NULL,
	                        CreatedBy nvarchar(30) NULL,
	                        UpdatedDate datetime NULL,
	                        UpdatedBy nvarchar(30) NULL,
                         CONSTRAINT PK_GFI_AMM_ExtP_Fields PRIMARY KEY  
                        (
	                        FieldId ASC
                        ) /*/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/
                        ) /*ON PRIMARY*/";

                    if (GetDataDT(ExistTableSql("GFI_AMM_AssetExtProFields")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE  INDEX IX_AssetCategory ON GFI_AMM_AssetExtProFields 
                        (
	                        Category ASC
                        ) /*/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/ */";

                    if (GetDataDT(ExistsIndex("GFI_AMM_AssetExtProFields", "IX_AssetCategory")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE  INDEX IX_AssetType ON GFI_AMM_AssetExtProFields 
                        (
	                       AssetType ASC
                        ) /*/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/ */";

                    if (GetDataDT(ExistsIndex("GFI_AMM_AssetExtProFields", "IX_AssetType")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE  INDEX IX_FieldName ON GFI_AMM_AssetExtProFields
                        (
	                        FieldName ASC
                        ) /*/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/ */";

                    if (GetDataDT(ExistsIndex("GFI_AMM_AssetExtProFields", "IX_FieldName")).Rows.Count == 0)
                        dbExecute(sql);


                    // -- Table: GFI_AMM_InsCoverTypes
                    sql = @"CREATE TABLE GFI_AMM_InsCoverTypes(
	                        CoverTypeId int NOT NULL,
	                        Description nvarchar(120) NULL,
	                        CreatedDate datetime NULL,
	                        CreatedBy nvarchar(30) NULL,
	                        UpdatedDate datetime NULL,
	                        UpdatedBy nvarchar(30) NULL,
                         CONSTRAINT PK_GFI_AMM_InsCoverTypes PRIMARY KEY  
                        (
	                        CoverTypeId ASC
                        ) /*/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/
                        ) /*ON PRIMARY*/";

                    if (GetDataDT(ExistTableSql("GFI_AMM_InsCoverTypes")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE  INDEX IX_CoverTypeDescription ON GFI_AMM_InsCoverTypes
                        (
	                        Description ASC
                        ) /*/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/";

                    if (GetDataDT(ExistsIndex("GFI_AMM_InsCoverTypes", "IX_CoverTypeDescription")).Rows.Count == 0)
                        dbExecute(sql);


                    // -- Table: GFI_AMM_VehicleTypes
                    sql = @"CREATE TABLE GFI_AMM_VehicleTypes(
	                        VehicleTypeId int NOT NULL,
	                        Description nvarchar(120) NULL,
	                        CreatedDate datetime NULL,
	                        CreatedBy nvarchar(30) NULL,
	                        UpdatedDate datetime NULL,
	                        UpdatedBy nvarchar(30) NULL,
                         CONSTRAINT PK_GFI_AMM_VehicleTypes PRIMARY KEY  
                        (
	                        VehicleTypeId ASC
                        )/*/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/
                        ) /*ON PRIMARY*/";

                    if (GetDataDT(ExistTableSql("GFI_AMM_VehicleTypes")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE  INDEX IX_Description ON GFI_AMM_VehicleTypes 
                        (
	                        Description ASC
                        )/*/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/";

                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleTypes", "IX_Description")).Rows.Count == 0)
                        dbExecute(sql);


                    // -- Table: GFI_AMM_VehicleMaintStatus
                    sql = @"CREATE TABLE GFI_AMM_VehicleMaintStatus(
	                        MaintStatusId int NOT NULL,
	                        Description nvarchar(120) NULL,
	                        SortOrder int NULL,
	                        CreatedDate datetime NULL,
	                        CreatedBy nvarchar(30) NULL,
	                        UpdatedDate datetime NULL,
	                        UpdatedBy nvarchar(30) NULL,
                         CONSTRAINT PK_GFI_AMM_VehicleMaintStatus PRIMARY KEY  
                        (
	                        MaintStatusId ASC
                        )/*/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/
                        ) /*ON PRIMARY]*/";

                    if (GetDataDT(ExistTableSql("GFI_AMM_VehicleMaintStatus")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE  INDEX IX_Description ON GFI_AMM_VehicleMaintStatus
                        (
	                       Description ASC
                        )/*/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/";

                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintStatus", "IX_Description")).Rows.Count == 0)
                        dbExecute(sql);


                    // -- Table: GFI_AMM_VehicleMaintTypes
                    sql = @"CREATE TABLE GFI_AMM_VehicleMaintTypes(
	                        MaintTypeId int AUTO_INCREMENT NOT NULL,
	                        MaintCatId_cbo int NULL,
	                        Description nvarchar(120) NULL,
	                        OccurrenceType int NULL,
	                        OccurrenceFixedDate datetime NULL,
	                        OccurrenceFixedDateTh int NULL,
                            OccurrenceDuration int NULL,
	                        OccurrenceDurationTh int NULL,
                            OccurrencePeriod_cbo nvarchar(30) NULL,
                            OccurrenceKM int NULL,
                            OccurrenceKMTh int NULL,
	                        OccurrenceEngineHrs int NULL,	                       
	                        OccurrenceEngineHrsTh int NULL,
	                        CreatedDate datetime NULL,
	                        CreatedBy nvarchar(30) NULL,
	                        UpdatedDate datetime NULL,
	                        UpdatedBy nvarchar(30) NULL,
                         CONSTRAINT PK_GFI_AMM_VehicleMaintTypes PRIMARY KEY  
                        (
	                        MaintTypeId ASC
                        )/*/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/
                        ) /*ON PRIMARY*/";

                    if (GetDataDT(ExistTableSql("GFI_AMM_VehicleMaintTypes")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE  INDEX IX_Description ON GFI_AMM_VehicleMaintTypes 
                        (
	                        Description ASC
                        )/*/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/";

                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintTypes", "IX_Description")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE  INDEX IX_MaintCatId ON GFI_AMM_VehicleMaintTypes
                        (
	                        MaintCatId_cbo ASC
                        )/*/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/";

                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintTypes", "IX_MaintCatId")).Rows.Count == 0)
                        dbExecute(sql);


                    // -- Table: GFI_AMM_AssetExtProXT
                    sql = @"CREATE TABLE GFI_AMM_AssetExtProXT(
	                URI bigint AUTO_INCREMENT NOT NULL,
	                AssetId int NULL,
	                FieldId int NULL,
	                XTValue nvarchar(255) NULL,
	                CreatedDate datetime NULL,
	                CreatedBy nvarchar(30) NULL,
	                UpdatedDate datetime NULL,
	                UpdatedBy nvarchar(30) NULL,
                 CONSTRAINT PK_GFI_AMM_AssetExtProXT PRIMARY KEY  
                (
	                URI ASC
                )/*/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/
                ) /*ON PRIMARY*/";

                    if (GetDataDT(ExistTableSql("GFI_AMM_AssetExtProXT")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE  INDEX IX_AssetId ON GFI_AMM_AssetExtProXT 
                    (
	                    AssetId ASC
                    )/*/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/";

                    if (GetDataDT(ExistsIndex("GFI_AMM_AssetExtProXT", "IX_AssetId")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE  INDEX IX_FieldId ON GFI_AMM_AssetExtProXT 
                    (
	                    FieldId ASC
                    )/*/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/";

                    if (GetDataDT(ExistsIndex("GFI_AMM_AssetExtProXT", "IX_FieldId")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE  INDEX IX_XTValue ON GFI_AMM_AssetExtProXT 
                    (
	                    XTValue ASC
                    )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/";

                    if (GetDataDT(ExistsIndex("GFI_AMM_AssetExtProXT", "IX_XTValue")).Rows.Count == 0)
                        dbExecute(sql);


                    // -- Table: GFI_AMM_AssetExtProVehicles
                    sql = @"CREATE TABLE GFI_AMM_AssetExtProVehicles(
	                        AssetId int NOT NULL,
	                        VIN nvarchar(120) NULL,
	                        Description nvarchar(120) NULL,
	                        Make nvarchar(120) NULL,
	                        Model nvarchar(120) NULL,
	                        VehicleTypeId_cbo int NULL,
	                        Manufacturer nvarchar(120) NULL,
	                        YearManufactured int NULL,
	                        PurchasedDate datetime NULL,
	                        PurchasedValue numeric(18, 2) NULL,
	                        PORef nvarchar(120) NULL,
	                        ExpectedLifetime int NULL,
	                        ResidualValue numeric(18, 2) NULL,
	                        EngineSerialNumber nvarchar(120) NULL,
	                        EngineType nvarchar(120) NULL,
	                        EngineCapacity int NULL,
	                        EnginePower int NULL,
	                        FuelType nvarchar(120) NULL,
	                        StdConsumption numeric(18, 2) NULL,
	                        ExternalReference nvarchar(120) NULL,
	                        InService_b char(1) NULL,
	                        AdditionalInfo nvarchar(1000) NULL,
	                        CreatedDate datetime NOT NULL,
	                        CreatedBy nvarchar(30) NOT NULL,
	                        UpdatedDate datetime NULL,
	                        UpdatedBy nvarchar(30) NULL,
                         CONSTRAINT PK_GFI_AMM_Asset_ExtPro PRIMARY KEY  
                        (
	                        AssetId ASC
                        )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/
                        ) /*ON PRIMARY*/";

                    if (GetDataDT(ExistTableSql("GFI_AMM_AssetExtProVehicles")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE  INDEX IX_Make ON GFI_AMM_AssetExtProVehicles 
                    (
	                    Make ASC
                    )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/";

                    if (GetDataDT(ExistsIndex("GFI_AMM_AssetExtProVehicles", "IX_Make")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE  INDEX IX_Manuafacturer ON GFI_AMM_AssetExtProVehicles
                    (
	                    Manufacturer ASC
                    )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/";

                    if (GetDataDT(ExistsIndex("GFI_AMM_AssetExtProVehicles", "IX_Manuafacturer")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE  INDEX IX_Model ON GFI_AMM_AssetExtProVehicles 
                    (
	                    Model ASC
                    )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/";

                    if (GetDataDT(ExistsIndex("GFI_AMM_AssetExtProVehicles", "IX_Model")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE  INDEX IX_VehicleTypeId ON GFI_AMM_AssetExtProVehicles 
                    (
	                   VehicleTypeId_cbo ASC
                    )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) /*ON PRIMARY*/";

                    if (GetDataDT(ExistsIndex("GFI_AMM_AssetExtProVehicles", "IX_VehicleTypeId")).Rows.Count == 0)
                        dbExecute(sql);


                    // -- Table: GFI_AMM_AssetExtProVehicles
                    sql = @"CREATE TABLE GFI_AMM_VehicleMaintenance(
	                URI int AUTO_INCREMENT NOT NULL,
	                AssetId int NULL,
	                MaintTypeId_cbo int NULL,
	                CompanyName nvarchar(255) NULL,
	                PhoneNumber nvarchar(120) NULL,
	                CompanyRef nvarchar(120) NULL,
	                MaintDescription nvarchar(512) NULL,
	                StartDate datetime NULL,
	                EndDate datetime NULL,
	                MaintStatusId_cbo int NULL,
	                EstimatedValue numeric(18, 2) NULL,
	                TotalCost numeric(18, 2) NULL,
	                CoverTypeId_cbo int NULL,
	                CalculatedOdometer int NULL,
	                ActualOdometer int NULL,
	                CalculatedEngineHrs int NULL,
	                ActualEngineHrs int NULL,
	                AdditionalInfo nvarchar(1000) NULL,
	                CreatedDate datetime NULL,
	                CreatedBy nvarchar(30) NULL,
	                UpdatedDate datetime NULL,
	                UpdatedBy nvarchar(30) NULL,
                 CONSTRAINT PK_GFI_AMM_VehicleMaint PRIMARY KEY  
                (
	                URI ASC
                )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/
                ) /*ON PRIMARY*/";

                    if (GetDataDT(ExistTableSql("GFI_AMM_VehicleMaintenance")).Rows.Count == 0)
                        dbExecute(sql);

                    //--Indexes
                    sql = @"CREATE  INDEX IX_ActualCompletionDate ON GFI_AMM_VehicleMaintenance 
                (
	                EndDate ASC
                )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/";

                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintenance", "IX_ActualCompletionDate")).Rows.Count == 0)
                        dbExecute(sql);

                    //--
                    sql = @"CREATE  INDEX IX_ActualStartDate ON GFI_AMM_VehicleMaintenance 
                (
	                StartDate ASC
                )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/";

                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintenance", "IX_ActualStartDate")).Rows.Count == 0)
                        dbExecute(sql);

                    //--
                    sql = @"CREATE  INDEX IX_AssetId ON GFI_AMM_VehicleMaintenance 
                (
	                AssetId ASC
                )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/";

                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintenance", "IX_AssetId")).Rows.Count == 0)
                        dbExecute(sql);

                    //--
                    sql = @"CREATE  INDEX IX_CompanyName ON GFI_AMM_VehicleMaintenance
                (
	               CompanyName ASC
                )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/";

                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintenance", "IX_CompanyName")).Rows.Count == 0)
                        dbExecute(sql);

                    //--
                    sql = @"CREATE  INDEX IX_CoverTypeId ON GFI_AMM_VehicleMaintenance 
                (
	                CoverTypeId_cbo ASC
                )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/";

                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintenance", "IX_CoverTypeId")).Rows.Count == 0)
                        dbExecute(sql);

                    //--
                    sql = @"CREATE  INDEX IX_EndDate ON GFI_AMM_VehicleMaintenance
                (
	                EndDate ASC
                )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/";

                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintenance", "IX_EndDate")).Rows.Count == 0)
                        dbExecute(sql);

                    //--
                    sql = @"CREATE  INDEX IX_MaintStatusId ON GFI_AMM_VehicleMaintenance 
                (
	                MaintStatusId_cbo ASC
                )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/";

                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintenance", "IX_MaintStatusId")).Rows.Count == 0)
                        dbExecute(sql);

                    //--
                    sql = @"CREATE  INDEX IX_MaintTypeId ON GFI_AMM_VehicleMaintenance 
                (
	               MaintTypeId_cbo ASC
                )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/";

                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintenance", "IX_MaintTypeId")).Rows.Count == 0)
                        dbExecute(sql);

                    //--
                    sql = @"CREATE  INDEX IX_StartDate ON GFI_AMM_VehicleMaintenance 
                (
	                StartDate ASC
                )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/";

                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintenance", "IX_StartDate")).Rows.Count == 0)
                        dbExecute(sql);


                    // -- Table: GFI_AMM_AssetExtProVehicles
                    sql = @"CREATE TABLE GFI_AMM_VehicleMaintTypesLink(
	                        URI int AUTO_INCREMENT NOT NULL,
	                        MaintURI int NULL,
	                        AssetId int NULL,
	                        MaintTypeId int NULL,
	                        NextMaintDate datetime NULL,
	                        NextMaintDateTG datetime NULL,
	                        CurrentOdometer int NULL,
	                        NextMaintOdometer int NULL,
	                        NextMaintOdometerTG int NULL,
	                        CurrentEngHrs int NULL,
	                        NextMaintEngHrs int NULL,
	                        NextMaintEngHrsTG int NULL,
	                        Status int NULL,
	                        CreatedDate datetime NULL,
	                        CreatedBy nvarchar(30) NULL,
	                        UpdatedDate datetime NULL,
	                        UpdatedBy nvarchar(30) NULL,
                         CONSTRAINT PK_GFI_AMM_VehicleMaintTypesLink PRIMARY KEY  
                        (
	                        URI ASC
                        )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/
                        ) /*ON PRIMARY*/";

                    if (GetDataDT(ExistTableSql("GFI_AMM_VehicleMaintTypesLink")).Rows.Count == 0)
                        dbExecute(sql);

                    //--
                    sql = @"CREATE  INDEX IX_AssetId ON GFI_AMM_VehicleMaintTypesLink
                    (
	                    AssetId ASC
                    )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/";

                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintTypesLink", "IX_AssetId")).Rows.Count == 0)
                        dbExecute(sql);

                    //--
                    sql = @"CREATE  INDEX IX_MaintTypeId ON GFI_AMM_VehicleMaintTypesLink
                    (
	                    MaintTypeId ASC
                    )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/";

                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintTypesLink", "IX_MaintTypeId")).Rows.Count == 0)
                        dbExecute(sql);


                    // -- Table: GFI_AMM_VehicleMaintItems
                    sql = @"CREATE TABLE GFI_AMM_VehicleMaintItems(
	                    URI int AUTO_INCREMENT NOT NULL,
	                    MaintURI int NOT NULL,
	                    ItemCode nvarchar(50) NULL,
	                    Description nvarchar(120) NULL,
	                    Quantity int NULL,
	                    UnitCost numeric(18, 2) NULL,
	                    CreatedDate datetime NULL,
	                    CreatedBy nvarchar(30) NULL,
	                    UpdatedDate datetime NULL,
	                    UpdatedBy nvarchar(30) NULL,
                     CONSTRAINT PK_GFI_AMM_VehicleMaintItems PRIMARY KEY  
                    (
	                    URI ASC
                    )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/
                    ) /*ON PRIMARY*/";

                    if (GetDataDT(ExistTableSql("GFI_AMM_VehicleMaintItems")).Rows.Count == 0)
                        dbExecute(sql);

                    //--
                    sql = @"CREATE  INDEX IX_ItemCode ON GFI_AMM_VehicleMaintItems
                    (
	                    ItemCode ASC
                    )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/";

                    //--
                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintItems", "IX_ItemCode")).Rows.Count == 0)
                        dbExecute(sql);

                    //--
                    sql = @"CREATE  INDEX IX_MaintURI ON GFI_AMM_VehicleMaintItems
                    (
	                   MaintURI ASC
                    )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/";

                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintItems", "IX_MaintURI")).Rows.Count == 0)
                        dbExecute(sql);


                    //--
                    sql = @"ALTER TABLE GFI_AMM_AssetExtProVehicles  ADD  CONSTRAINT FK_GFI_AMM_Asset_ExtPro_Vehicles_GFI_AMM_VehicleTypes FOREIGN KEY(VehicleTypeId_cbo)
                REFERENCES GFI_AMM_VehicleTypes (VehicleTypeId)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_Asset_ExtPro_Vehicles_GFI_AMM_VehicleTypes", "GFI_AMM_AssetExtProVehicles")).Rows.Count == 0)
                        dbExecute(sql);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_AssetExtProVehicles CHECK CONSTRAINT FK_GFI_AMM_Asset_ExtPro_Vehicles_GFI_AMM_VehicleTypes";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_Asset_ExtPro_Vehicles_GFI_AMM_VehicleTypes", "GFI_AMM_AssetExtProVehicles")).Rows.Count == 0)
                        dbExecute(sql);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_AssetExtProXT ADD  CONSTRAINT FK_GFI_AMM_AssetExtProXT_GFI_AMM_AssetExtProFields FOREIGN KEY(FieldId)
                REFERENCES GFI_AMM_AssetExtProFields (FieldId)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_AssetExtProXT_GFI_AMM_AssetExtProFields", "GFI_AMM_AssetExtProXT")).Rows.Count == 0)
                        dbExecute(sql);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_AssetExtProXT CHECK CONSTRAINT FK_GFI_AMM_AssetExtProXT_GFI_AMM_AssetExtProFields";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_AssetExtProXT_GFI_AMM_AssetExtProFields", "GFI_AMM_AssetExtProXT")).Rows.Count == 0)
                        dbExecute(sql);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintenance  ADD  CONSTRAINT FK_GFI_AMM_VehicleMaint_GFI_AMM_VehicleMaintStatus FOREIGN KEY(MaintStatusId_cbo)
                REFERENCES GFI_AMM_VehicleMaintStatus (MaintStatusId)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaint_GFI_AMM_VehicleMaintStatus", "GFI_AMM_VehicleMaintenance")).Rows.Count == 0)
                        dbExecute(sql);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintenance CHECK CONSTRAINT FK_GFI_AMM_VehicleMaint_GFI_AMM_VehicleMaintStatus";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaint_GFI_AMM_VehicleMaintStatus", "GFI_AMM_VehicleMaintenance")).Rows.Count == 0)
                        dbExecute(sql);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintenance  ADD  CONSTRAINT FK_GFI_AMM_VehicleMaintenance_GFI_AMM_Asset_ExtPro_Vehicles FOREIGN KEY(AssetId)
                REFERENCES GFI_AMM_AssetExtProVehicles (AssetId)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintenance_GFI_AMM_Asset_ExtPro_Vehicles", "GFI_AMM_VehicleMaintenance")).Rows.Count == 0)
                        dbExecute(sql);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintenance CHECK CONSTRAINT FK_GFI_AMM_VehicleMaintenance_GFI_AMM_Asset_ExtPro_Vehicles";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintenance_GFI_AMM_Asset_ExtPro_Vehicles", "GFI_AMM_VehicleMaintenance")).Rows.Count == 0)
                        dbExecute(sql);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintenance  ADD  CONSTRAINT FK_GFI_AMM_VehicleMaintenance_GFI_AMM_VehicleMaintTypes FOREIGN KEY(MaintTypeId_cbo)
                REFERENCES GFI_AMM_VehicleMaintTypes (MaintTypeId)";

                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintenance_GFI_AMM_VehicleMaintTypes", "GFI_AMM_VehicleMaintenance")).Rows.Count == 0)
                        dbExecute(sql);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintenance CHECK CONSTRAINT FK_GFI_AMM_VehicleMaintenance_GFI_AMM_VehicleMaintTypes";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintenance_GFI_AMM_VehicleMaintTypes", "GFI_AMM_VehicleMaintenance")).Rows.Count == 0)
                        dbExecute(sql);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintItems  ADD  CONSTRAINT FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance FOREIGN KEY (MaintURI)
                REFERENCES GFI_AMM_VehicleMaintenance (URI)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance", "GFI_AMM_VehicleMaintItems")).Rows.Count == 0)
                        dbExecute(sql);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintItems CHECK CONSTRAINT FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance", "GFI_AMM_VehicleMaintItems")).Rows.Count == 0)
                        dbExecute(sql);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintTypes  ADD  CONSTRAINT FK_GFI_AMM_VehicleMaintTypes_GFI_AMM_VehicleMaintCat FOREIGN KEY(MaintCatId_cbo)
                REFERENCES GFI_AMM_VehicleMaintCat (MaintCatId)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintTypes_GFI_AMM_VehicleMaintCat", "GFI_AMM_VehicleMaintTypes")).Rows.Count == 0)
                        dbExecute(sql);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintTypes CHECK CONSTRAINT FK_GFI_AMM_VehicleMaintTypes_GFI_AMM_VehicleMaintCat";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintTypes_GFI_AMM_VehicleMaintCat", "GFI_AMM_VehicleMaintTypes")).Rows.Count == 0)
                        dbExecute(sql);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintTypesLink  ADD  CONSTRAINT FK_GFI_AMM_VehicleMaintTypesLink_GFI_AMM_AssetExtProVehicles FOREIGN KEY(AssetId)
                REFERENCES GFI_AMM_AssetExtProVehicles (AssetId)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintTypesLink_GFI_AMM_AssetExtProVehicles", "GFI_AMM_VehicleMaintTypesLink")).Rows.Count == 0)
                        dbExecute(sql);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintTypesLink CHECK CONSTRAINT FK_GFI_AMM_VehicleMaintTypesLink_GFI_AMM_AssetExtProVehicles";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintTypesLink_GFI_AMM_AssetExtProVehicles", "GFI_AMM_VehicleMaintTypesLink")).Rows.Count == 0)
                        dbExecute(sql);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintTypesLink  ADD  CONSTRAINT FK_GFI_AMM_VehicleMaintTypesLink_GFI_AMM_VehicleMaintTypes FOREIGN KEY(MaintTypeId)
                REFERENCES GFI_AMM_VehicleMaintTypes (MaintTypeId)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintTypesLink_GFI_AMM_VehicleMaintTypes", "GFI_AMM_VehicleMaintTypesLink")).Rows.Count == 0)
                        dbExecute(sql);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintTypesLink CHECK CONSTRAINT FK_GFI_AMM_VehicleMaintTypesLink_GFI_AMM_VehicleMaintTypes";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintTypesLink_GFI_AMM_VehicleMaintTypes", "GFI_AMM_VehicleMaintTypesLink")).Rows.Count == 0)
                        dbExecute(sql);
                    #endregion

                    #region indexes
                    //Index 1
                    sql = @"DROP INDEX IX_CoverTypeDescription ON GFI_AMM_InsCoverTypes /*WITH ( ONLINE = OFF )*/";

                    if (GetDataDT(ExistsIndex("GFI_AMM_InsCoverTypes", "IX_CoverTypeDescription")).Rows.Count == 1)
                        dbExecute(sql);

                    sql = @"CREATE UNIQUE  INDEX IX_CoverTypeDescription ON GFI_AMM_InsCoverTypes 
                        (
	                        Description ASC
                        )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/";

                    if (GetDataDT(ExistsIndex("GFI_AMM_InsCoverTypes", "IX_CoverTypeDescription")).Rows.Count == 0)
                        dbExecute(sql);

                    //Index 2
                    sql = @"DROP INDEX IX_MaintCatDescription ON GFI_AMM_VehicleMaintCat /*WITH ( ONLINE = OFF )*/";

                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintCat", "IX_MaintCatDescription")).Rows.Count == 1)
                        dbExecute(sql);

                    sql = @"CREATE UNIQUE  INDEX IX_MaintCatDescription ON GFI_AMM_VehicleMaintCat 
                        (
	                        Description ASC
                        )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/";
                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintCat", "IX_MaintCatDescription")).Rows.Count == 0)
                        dbExecute(sql);

                    //Index 3
                    sql = @"DROP INDEX IX_Description ON GFI_AMM_VehicleMaintStatus /*WITH ( ONLINE = OFF )*/";

                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintStatus", "IX_Description")).Rows.Count == 1)
                        dbExecute(sql);

                    sql = @"CREATE UNIQUE  INDEX IX_Description ON GFI_AMM_VehicleMaintStatus
                        (
	                        Description ASC
                        )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/";
                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintStatus", "IX_Description")).Rows.Count == 0)
                        dbExecute(sql);

                    //Index 4
                    sql = @"DROP INDEX IX_Description ON GFI_AMM_VehicleMaintTypes /*WITH ( ONLINE = OFF )*/";

                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintTypes", "IX_Description")).Rows.Count == 1)
                        dbExecute(sql);

                    sql = @"CREATE UNIQUE  INDEX IX_Description ON GFI_AMM_VehicleMaintTypes
                        (
	                        Description ASC
                        )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/
                        ";
                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintTypes", "IX_Description")).Rows.Count == 0)
                        dbExecute(sql);

                    //Index 5
                    sql = @"DROP INDEX IX_Description ON GFI_AMM_VehicleTypes /*WITH ( ONLINE = OFF )*/";

                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleTypes", "IX_Description")).Rows.Count == 1)
                        dbExecute(sql);

                    sql = @"CREATE UNIQUE  INDEX IX_Description ON GFI_AMM_VehicleTypes 
                        (
	                        Description ASC
                        )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) /*ON PRIMARY*/";

                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleTypes", "IX_Description")).Rows.Count == 0)
                        dbExecute(sql);

                    #endregion

                    #region inserts
                    //--------------- Insert Default Values -------------//
                    //
                    // 1. GFI_AMM_AssetExtProFields
                    // 2. GFI_AMM_InsCoverTypes
                    // 3. GFI_AMM_VehicleMaintCat
                    // 4. GFI_AMM_VehicleMaintStatus
                    // 5. GFI_AMM_VehicleMaintTypes
                    // 6. GFI_AMM_VehicleTypes
                    //
                    //---------------------------------------------------//

                    AssetExtProFields objAssetExtProFields = new AssetExtProFields();
                    objAssetExtProFields = objAssetExtProFields.GetAssetExtProFieldsByFieldName("FUEL", sConnStr);
                    if (objAssetExtProFields == null)
                    {
                        objAssetExtProFields = new AssetExtProFields();
                        objAssetExtProFields.FieldId = 1;
                        objAssetExtProFields.AssetType = "VEHICLE";
                        objAssetExtProFields.Category = "COUNTERS";
                        objAssetExtProFields.FieldName = "FUEL";
                        objAssetExtProFields.Description = "FUEL";
                        objAssetExtProFields.Type = "STRING";
                        objAssetExtProFields.UnitOfMeasure = "LITRES";
                        objAssetExtProFields.KeepHistory = "Y";
                        objAssetExtProFields.DataRetentionDays = 99999999;
                        objAssetExtProFields.THWarning_Value = 99999999;
                        objAssetExtProFields.THWarning_TimeBased = 99999999;
                        objAssetExtProFields.THCritical_Value = 99999999;
                        objAssetExtProFields.THCritical_TimeBased = 99999999;
                        objAssetExtProFields.DisplayOrder = 1;
                        objAssetExtProFields.CreatedDate = DateTime.Now;
                        objAssetExtProFields.CreatedBy = usrInst.Username;
                        objAssetExtProFields.UpdatedDate = DateTime.Now;
                        objAssetExtProFields.UpdatedBy = usrInst.Username;
                        objAssetExtProFields.Save(objAssetExtProFields, sConnStr);
                    }

                    objAssetExtProFields = new AssetExtProFields();
                    objAssetExtProFields = objAssetExtProFields.GetAssetExtProFieldsByFieldName("CARGOTEMP", sConnStr);
                    if (objAssetExtProFields == null)
                    {
                        objAssetExtProFields = new AssetExtProFields();
                        objAssetExtProFields.FieldId = 2;
                        objAssetExtProFields.AssetType = "VEHICLE";
                        objAssetExtProFields.Category = "COUNTERS";
                        objAssetExtProFields.FieldName = "CARGOTEMP";
                        objAssetExtProFields.Description = "CARGOTEMP";
                        objAssetExtProFields.Type = "STRING";
                        objAssetExtProFields.UnitOfMeasure = "LITRES";
                        objAssetExtProFields.KeepHistory = "Y";
                        objAssetExtProFields.DataRetentionDays = 99999999;
                        objAssetExtProFields.THWarning_Value = 99999999;
                        objAssetExtProFields.THWarning_TimeBased = 99999999;
                        objAssetExtProFields.THCritical_Value = 99999999;
                        objAssetExtProFields.THCritical_TimeBased = 99999999;
                        objAssetExtProFields.DisplayOrder = 2;
                        objAssetExtProFields.CreatedDate = DateTime.Now;
                        objAssetExtProFields.CreatedBy = usrInst.Username;
                        objAssetExtProFields.UpdatedDate = DateTime.Now;
                        objAssetExtProFields.UpdatedBy = usrInst.Username;
                        objAssetExtProFields.Save(objAssetExtProFields, sConnStr);
                    }

                    //---------------------------------------------------//

                    InsCoverTypes objInsCoverTypes = new InsCoverTypes();
                    objInsCoverTypes = objInsCoverTypes.GetInsCoverTypesByDescription("COMPREHENSIVE", sConnStr);
                    if (objInsCoverTypes == null)
                    {
                        objInsCoverTypes = new InsCoverTypes();
                        objInsCoverTypes.CoverTypeId = 1;
                        objInsCoverTypes.Description = "COMPREHENSIVE";
                        objInsCoverTypes.CreatedDate = DateTime.Now;
                        objInsCoverTypes.CreatedBy = usrInst.Username;
                        objInsCoverTypes.UpdatedDate = DateTime.Now;
                        objInsCoverTypes.UpdatedBy = usrInst.Username;
                        objInsCoverTypes.Save(objInsCoverTypes, sConnStr);
                    }

                    objInsCoverTypes = new InsCoverTypes();
                    objInsCoverTypes = objInsCoverTypes.GetInsCoverTypesByDescription("THIRD PARTY", sConnStr);
                    if (objInsCoverTypes == null)
                    {
                        objInsCoverTypes = new InsCoverTypes();
                        objInsCoverTypes.CoverTypeId = 2;
                        objInsCoverTypes.Description = "THIRD PARTY";
                        objInsCoverTypes.CreatedDate = DateTime.Now;
                        objInsCoverTypes.CreatedBy = usrInst.Username;
                        objInsCoverTypes.UpdatedDate = DateTime.Now;
                        objInsCoverTypes.UpdatedBy = usrInst.Username;
                        objInsCoverTypes.Save(objInsCoverTypes, sConnStr);
                    }

                    //---------------------------------------------------//

                    VehicleMaintCat objVehicleMaintCat = new VehicleMaintCat();
                    objVehicleMaintCat = objVehicleMaintCat.GetVehicleMaintCatByDescription("Maintenance", sConnStr);
                    if (objVehicleMaintCat == null)
                    {
                        objVehicleMaintCat = new VehicleMaintCat();
                        objVehicleMaintCat.MaintCatId = 1;
                        objVehicleMaintCat.Description = "MAINTENANCE";
                        objVehicleMaintCat.CreatedDate = DateTime.Now;
                        objVehicleMaintCat.CreatedBy = usrInst.Username;
                        objVehicleMaintCat.UpdatedDate = DateTime.Now;
                        objVehicleMaintCat.UpdatedBy = usrInst.Username;
                        objVehicleMaintCat.Save(objVehicleMaintCat, sConnStr);
                    }

                    objVehicleMaintCat = new VehicleMaintCat();
                    objVehicleMaintCat = objVehicleMaintCat.GetVehicleMaintCatByDescription("Inspection", sConnStr);
                    if (objVehicleMaintCat == null)
                    {
                        objVehicleMaintCat = new VehicleMaintCat();
                        objVehicleMaintCat.MaintCatId = 2;
                        objVehicleMaintCat.Description = "INSPECTION";
                        objVehicleMaintCat.CreatedDate = DateTime.Now;
                        objVehicleMaintCat.CreatedBy = usrInst.Username;
                        objVehicleMaintCat.UpdatedDate = DateTime.Now;
                        objVehicleMaintCat.UpdatedBy = usrInst.Username;
                        objVehicleMaintCat.Save(objVehicleMaintCat, sConnStr);
                    }

                    objVehicleMaintCat = new VehicleMaintCat();
                    objVehicleMaintCat = objVehicleMaintCat.GetVehicleMaintCatByDescription("Registration", sConnStr);
                    if (objVehicleMaintCat == null)
                    {
                        objVehicleMaintCat = new VehicleMaintCat();
                        objVehicleMaintCat.MaintCatId = 3;
                        objVehicleMaintCat.Description = "REGISTRATION";
                        objVehicleMaintCat.CreatedDate = DateTime.Now;
                        objVehicleMaintCat.CreatedBy = usrInst.Username;
                        objVehicleMaintCat.UpdatedDate = DateTime.Now;
                        objVehicleMaintCat.UpdatedBy = usrInst.Username;
                        objVehicleMaintCat.Save(objVehicleMaintCat, sConnStr);
                    }

                    objVehicleMaintCat = new VehicleMaintCat();
                    objVehicleMaintCat = objVehicleMaintCat.GetVehicleMaintCatByDescription("Insurance", sConnStr);
                    if (objVehicleMaintCat == null)
                    {
                        objVehicleMaintCat = new VehicleMaintCat();
                        objVehicleMaintCat.MaintCatId = 4;
                        objVehicleMaintCat.Description = "INSURANCE";
                        objVehicleMaintCat.CreatedDate = DateTime.Now;
                        objVehicleMaintCat.CreatedBy = usrInst.Username;
                        objVehicleMaintCat.UpdatedDate = DateTime.Now;
                        objVehicleMaintCat.UpdatedBy = usrInst.Username;
                        objVehicleMaintCat.Save(objVehicleMaintCat, sConnStr);
                    }

                    //---------------------------------------------------//

                    VehicleMaintTypes objVehicleMaintTypes = new VehicleMaintTypes();
                    objVehicleMaintTypes = objVehicleMaintTypes.GetVehicleMaintTypesByDescription("SERVICING - 5000 KM", sConnStr);
                    if (objVehicleMaintTypes == null)
                    {
                        objVehicleMaintTypes = new VehicleMaintTypes();
                        objVehicleMaintTypes.MaintCatId_cbo = 1;
                        objVehicleMaintTypes.Description = "SERVICING - 5000 KM";
                        objVehicleMaintTypes.OccurrenceType = 2;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceDuration = 0;
                        objVehicleMaintTypes.OccurrenceDurationTh = 0;
                        objVehicleMaintTypes.OccurrenceKM = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrs = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceKMTh = 0;
                        objVehicleMaintTypes.CreatedDate = DateTime.Now;
                        objVehicleMaintTypes.CreatedBy = usrInst.Username;
                        objVehicleMaintTypes.UpdatedDate = DateTime.Now;
                        objVehicleMaintTypes.UpdatedBy = usrInst.Username;
                        objVehicleMaintTypes.Save(objVehicleMaintTypes, sConnStr);
                    }

                    objVehicleMaintTypes = new VehicleMaintTypes();
                    objVehicleMaintTypes = objVehicleMaintTypes.GetVehicleMaintTypesByDescription("SERVICING - 10000 KM OR 6 MONTHS", sConnStr);
                    if (objVehicleMaintTypes == null)
                    {
                        objVehicleMaintTypes = new VehicleMaintTypes();
                        objVehicleMaintTypes.MaintCatId_cbo = 1;
                        objVehicleMaintTypes.Description = "SERVICING - 10000 KM OR 6 MONTHS";
                        objVehicleMaintTypes.OccurrenceType = 2;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceDuration = 0;
                        objVehicleMaintTypes.OccurrenceDurationTh = 0;
                        objVehicleMaintTypes.OccurrenceKM = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrs = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceKMTh = 0;
                        objVehicleMaintTypes.CreatedDate = DateTime.Now;
                        objVehicleMaintTypes.CreatedBy = usrInst.Username;
                        objVehicleMaintTypes.UpdatedDate = DateTime.Now;
                        objVehicleMaintTypes.UpdatedBy = usrInst.Username;
                        objVehicleMaintTypes.Save(objVehicleMaintTypes, sConnStr);
                    }

                    objVehicleMaintTypes = new VehicleMaintTypes();
                    objVehicleMaintTypes = objVehicleMaintTypes.GetVehicleMaintTypesByDescription("VEHICLE INSPECTION", sConnStr);
                    if (objVehicleMaintTypes == null)
                    {
                        objVehicleMaintTypes = new VehicleMaintTypes();
                        objVehicleMaintTypes.MaintCatId_cbo = 2;
                        objVehicleMaintTypes.Description = "VEHICLE INSPECTION";
                        objVehicleMaintTypes.OccurrenceType = 2;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceDuration = 0;
                        objVehicleMaintTypes.OccurrenceDurationTh = 0;
                        objVehicleMaintTypes.OccurrenceKM = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrs = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceKMTh = 0;
                        objVehicleMaintTypes.CreatedDate = DateTime.Now;
                        objVehicleMaintTypes.CreatedBy = usrInst.Username;
                        objVehicleMaintTypes.UpdatedDate = DateTime.Now;
                        objVehicleMaintTypes.UpdatedBy = usrInst.Username;
                        objVehicleMaintTypes.Save(objVehicleMaintTypes, sConnStr);
                    }

                    objVehicleMaintTypes = new VehicleMaintTypes();
                    objVehicleMaintTypes = objVehicleMaintTypes.GetVehicleMaintTypesByDescription("INSURANCE - 6 MONTHS", sConnStr);
                    if (objVehicleMaintTypes == null)
                    {
                        objVehicleMaintTypes = new VehicleMaintTypes();
                        objVehicleMaintTypes.MaintCatId_cbo = 4;
                        objVehicleMaintTypes.Description = "INSURANCE - 6 MONTHS";
                        objVehicleMaintTypes.OccurrenceType = 2;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceDuration = 0;
                        objVehicleMaintTypes.OccurrenceDurationTh = 0;
                        objVehicleMaintTypes.OccurrenceKM = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrs = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceKMTh = 0;
                        objVehicleMaintTypes.CreatedDate = DateTime.Now;
                        objVehicleMaintTypes.CreatedBy = usrInst.Username;
                        objVehicleMaintTypes.UpdatedDate = DateTime.Now;
                        objVehicleMaintTypes.UpdatedBy = usrInst.Username;
                        objVehicleMaintTypes.Save(objVehicleMaintTypes, sConnStr);
                    }

                    objVehicleMaintTypes = new VehicleMaintTypes();
                    objVehicleMaintTypes = objVehicleMaintTypes.GetVehicleMaintTypesByDescription("INSURANCE - 12 MONTHS", sConnStr);
                    if (objVehicleMaintTypes == null)
                    {
                        objVehicleMaintTypes = new VehicleMaintTypes();
                        objVehicleMaintTypes.MaintCatId_cbo = 4;
                        objVehicleMaintTypes.Description = "INSURANCE - 12 MONTHS";
                        objVehicleMaintTypes.OccurrenceType = 2;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceDuration = 0;
                        objVehicleMaintTypes.OccurrenceDurationTh = 0;
                        objVehicleMaintTypes.OccurrenceKM = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrs = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceKMTh = 0;
                        objVehicleMaintTypes.CreatedDate = DateTime.Now;
                        objVehicleMaintTypes.CreatedBy = usrInst.Username;
                        objVehicleMaintTypes.UpdatedDate = DateTime.Now;
                        objVehicleMaintTypes.UpdatedBy = usrInst.Username;
                        objVehicleMaintTypes.Save(objVehicleMaintTypes, sConnStr);
                    }

                    objVehicleMaintTypes = new VehicleMaintTypes();
                    objVehicleMaintTypes = objVehicleMaintTypes.GetVehicleMaintTypesByDescription("TYRE CHANGE - 12 MONTHS OR 50,000 KM", sConnStr);
                    if (objVehicleMaintTypes == null)
                    {
                        objVehicleMaintTypes = new VehicleMaintTypes();
                        objVehicleMaintTypes.MaintCatId_cbo = 1;
                        objVehicleMaintTypes.Description = "TYRE CHANGE - 12 MONTHS OR 50,000 KM";
                        objVehicleMaintTypes.OccurrenceType = 2;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceDuration = 0;
                        objVehicleMaintTypes.OccurrenceDurationTh = 0;
                        objVehicleMaintTypes.OccurrenceKM = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrs = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceKMTh = 0;
                        objVehicleMaintTypes.CreatedDate = DateTime.Now;
                        objVehicleMaintTypes.CreatedBy = usrInst.Username;
                        objVehicleMaintTypes.UpdatedDate = DateTime.Now;
                        objVehicleMaintTypes.UpdatedBy = usrInst.Username;
                        objVehicleMaintTypes.Save(objVehicleMaintTypes, sConnStr);
                    }

                    objVehicleMaintTypes = new VehicleMaintTypes();
                    objVehicleMaintTypes = objVehicleMaintTypes.GetVehicleMaintTypesByDescription("VEHICLE REGISTRATION - 12 MONTHS", sConnStr);
                    if (objVehicleMaintTypes == null)
                    {
                        objVehicleMaintTypes = new VehicleMaintTypes();
                        objVehicleMaintTypes.MaintCatId_cbo = 3;
                        objVehicleMaintTypes.Description = "VEHICLE REGISTRATION - 12 MONTHS";
                        objVehicleMaintTypes.OccurrenceType = 2;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceDuration = 0;
                        objVehicleMaintTypes.OccurrenceDurationTh = 0;
                        objVehicleMaintTypes.OccurrenceKM = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrs = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceKMTh = 0;
                        objVehicleMaintTypes.CreatedDate = DateTime.Now;
                        objVehicleMaintTypes.CreatedBy = usrInst.Username;
                        objVehicleMaintTypes.UpdatedDate = DateTime.Now;
                        objVehicleMaintTypes.UpdatedBy = usrInst.Username;
                        objVehicleMaintTypes.Save(objVehicleMaintTypes, sConnStr);
                    }

                    objVehicleMaintTypes = new VehicleMaintTypes();
                    objVehicleMaintTypes = objVehicleMaintTypes.GetVehicleMaintTypesByDescription("VEHICLE WASH", sConnStr);
                    if (objVehicleMaintTypes == null)
                    {
                        objVehicleMaintTypes = new VehicleMaintTypes();
                        objVehicleMaintTypes.MaintCatId_cbo = 1;
                        objVehicleMaintTypes.Description = "VEHICLE WASH";
                        objVehicleMaintTypes.OccurrenceType = 2;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceDuration = 0;
                        objVehicleMaintTypes.OccurrenceDurationTh = 0;
                        objVehicleMaintTypes.OccurrenceKM = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrs = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceKMTh = 0;
                        objVehicleMaintTypes.CreatedDate = DateTime.Now;
                        objVehicleMaintTypes.CreatedBy = usrInst.Username;
                        objVehicleMaintTypes.UpdatedDate = DateTime.Now;
                        objVehicleMaintTypes.UpdatedBy = usrInst.Username;
                        objVehicleMaintTypes.Save(objVehicleMaintTypes, sConnStr);
                    }

                    //---------------------------------------------------//

                    VehicleMaintStatus objVehicleMaintStatus = new VehicleMaintStatus();
                    objVehicleMaintStatus = objVehicleMaintStatus.GetVehicleMaintStatusByDescription("Overdue", sConnStr);
                    if (objVehicleMaintStatus == null)
                    {
                        objVehicleMaintStatus = new VehicleMaintStatus();
                        objVehicleMaintStatus.MaintStatusId = 1;
                        objVehicleMaintStatus.Description = "OVERDUE";
                        objVehicleMaintStatus.SortOrder = 1;
                        objVehicleMaintStatus.CreatedDate = DateTime.Now;
                        objVehicleMaintStatus.CreatedBy = usrInst.Username;
                        objVehicleMaintStatus.UpdatedDate = DateTime.Now;
                        objVehicleMaintStatus.UpdatedBy = usrInst.Username;
                        objVehicleMaintStatus.Save(objVehicleMaintStatus, sConnStr);
                    }

                    objVehicleMaintStatus = new VehicleMaintStatus();
                    objVehicleMaintStatus = objVehicleMaintStatus.GetVehicleMaintStatusByDescription("To Schedule", sConnStr);
                    if (objVehicleMaintStatus == null)
                    {
                        objVehicleMaintStatus = new VehicleMaintStatus();
                        objVehicleMaintStatus.MaintStatusId = 2;
                        objVehicleMaintStatus.Description = "TO SCHEDULE";
                        objVehicleMaintStatus.SortOrder = 2;
                        objVehicleMaintStatus.CreatedDate = DateTime.Now;
                        objVehicleMaintStatus.CreatedBy = usrInst.Username;
                        objVehicleMaintStatus.UpdatedDate = DateTime.Now;
                        objVehicleMaintStatus.UpdatedBy = usrInst.Username;
                        objVehicleMaintStatus.Save(objVehicleMaintStatus, sConnStr);
                    }

                    objVehicleMaintStatus = new VehicleMaintStatus();
                    objVehicleMaintStatus = objVehicleMaintStatus.GetVehicleMaintStatusByDescription("Scheduled", sConnStr);
                    if (objVehicleMaintStatus == null)
                    {
                        objVehicleMaintStatus = new VehicleMaintStatus();
                        objVehicleMaintStatus.MaintStatusId = 3;
                        objVehicleMaintStatus.Description = "SCHEDULED";
                        objVehicleMaintStatus.SortOrder = 3;
                        objVehicleMaintStatus.CreatedDate = DateTime.Now;
                        objVehicleMaintStatus.CreatedBy = usrInst.Username;
                        objVehicleMaintStatus.UpdatedDate = DateTime.Now;
                        objVehicleMaintStatus.UpdatedBy = usrInst.Username;
                        objVehicleMaintStatus.Save(objVehicleMaintStatus, sConnStr);
                    }

                    objVehicleMaintStatus = new VehicleMaintStatus();
                    objVehicleMaintStatus = objVehicleMaintStatus.GetVehicleMaintStatusByDescription("In Progress", sConnStr);
                    if (objVehicleMaintStatus == null)
                    {
                        objVehicleMaintStatus = new VehicleMaintStatus();
                        objVehicleMaintStatus.MaintStatusId = 4;
                        objVehicleMaintStatus.Description = "IN PROGRESS";
                        objVehicleMaintStatus.SortOrder = 4;
                        objVehicleMaintStatus.CreatedDate = DateTime.Now;
                        objVehicleMaintStatus.CreatedBy = usrInst.Username;
                        objVehicleMaintStatus.UpdatedDate = DateTime.Now;
                        objVehicleMaintStatus.UpdatedBy = usrInst.Username;
                        objVehicleMaintStatus.Save(objVehicleMaintStatus, sConnStr);
                    }

                    objVehicleMaintStatus = new VehicleMaintStatus();
                    objVehicleMaintStatus = objVehicleMaintStatus.GetVehicleMaintStatusByDescription("Completed", sConnStr);
                    if (objVehicleMaintStatus == null)
                    {
                        objVehicleMaintStatus = new VehicleMaintStatus();
                        objVehicleMaintStatus.MaintStatusId = 5;
                        objVehicleMaintStatus.Description = "COMPLETED";
                        objVehicleMaintStatus.SortOrder = 5;
                        objVehicleMaintStatus.CreatedDate = DateTime.Now;
                        objVehicleMaintStatus.CreatedBy = usrInst.Username;
                        objVehicleMaintStatus.UpdatedDate = DateTime.Now;
                        objVehicleMaintStatus.UpdatedBy = usrInst.Username;
                        objVehicleMaintStatus.Save(objVehicleMaintStatus, sConnStr);
                    }

                    objVehicleMaintStatus = new VehicleMaintStatus();
                    objVehicleMaintStatus = objVehicleMaintStatus.GetVehicleMaintStatusByDescription("Valid", sConnStr);
                    if (objVehicleMaintStatus == null)
                    {
                        objVehicleMaintStatus = new VehicleMaintStatus();
                        objVehicleMaintStatus.MaintStatusId = 6;
                        objVehicleMaintStatus.Description = "VALID";
                        objVehicleMaintStatus.SortOrder = 6;
                        objVehicleMaintStatus.CreatedDate = DateTime.Now;
                        objVehicleMaintStatus.CreatedBy = usrInst.Username;
                        objVehicleMaintStatus.UpdatedDate = DateTime.Now;
                        objVehicleMaintStatus.UpdatedBy = usrInst.Username;
                        objVehicleMaintStatus.Save(objVehicleMaintStatus, sConnStr);
                    }

                    objVehicleMaintStatus = new VehicleMaintStatus();
                    objVehicleMaintStatus = objVehicleMaintStatus.GetVehicleMaintStatusByDescription("Expired", sConnStr);
                    if (objVehicleMaintStatus == null)
                    {
                        objVehicleMaintStatus = new VehicleMaintStatus();
                        objVehicleMaintStatus.MaintStatusId = 7;
                        objVehicleMaintStatus.Description = "EXPIRED";
                        objVehicleMaintStatus.SortOrder = 7;
                        objVehicleMaintStatus.CreatedDate = DateTime.Now;
                        objVehicleMaintStatus.CreatedBy = usrInst.Username;
                        objVehicleMaintStatus.UpdatedDate = DateTime.Now;
                        objVehicleMaintStatus.UpdatedBy = usrInst.Username;
                        objVehicleMaintStatus.Save(objVehicleMaintStatus, sConnStr);
                    }

                    //---------------------------------------------------//

                    VehicleTypes objVehicleTypes = new VehicleTypes();
                    objVehicleTypes = objVehicleTypes.GetVehicleTypesByDescription("Car", sConnStr);
                    if (objVehicleTypes == null)
                    {
                        objVehicleTypes = new VehicleTypes();
                        objVehicleTypes.VehicleTypeId = 1;
                        objVehicleTypes.Description = "CAR";
                        objVehicleTypes.CreatedDate = DateTime.Now;
                        objVehicleTypes.CreatedBy = usrInst.Username;
                        objVehicleTypes.UpdatedDate = DateTime.Now;
                        objVehicleTypes.UpdatedBy = usrInst.Username;
                        objVehicleTypes.Save(objVehicleTypes, sConnStr);
                    }

                    objVehicleTypes = new VehicleTypes();
                    objVehicleTypes = objVehicleTypes.GetVehicleTypesByDescription("Van", sConnStr);
                    if (objVehicleTypes == null)
                    {
                        objVehicleTypes = new VehicleTypes();
                        objVehicleTypes.VehicleTypeId = 2;
                        objVehicleTypes.Description = "VAN";
                        objVehicleTypes.CreatedDate = DateTime.Now;
                        objVehicleTypes.CreatedBy = usrInst.Username;
                        objVehicleTypes.UpdatedDate = DateTime.Now;
                        objVehicleTypes.UpdatedBy = usrInst.Username;
                        objVehicleTypes.Save(objVehicleTypes, sConnStr);
                    }

                    objVehicleTypes = new VehicleTypes();
                    objVehicleTypes = objVehicleTypes.GetVehicleTypesByDescription("Truck", sConnStr);
                    if (objVehicleTypes == null)
                    {
                        objVehicleTypes = new VehicleTypes();
                        objVehicleTypes.VehicleTypeId = 3;
                        objVehicleTypes.Description = "TRUCK";
                        objVehicleTypes.CreatedDate = DateTime.Now;
                        objVehicleTypes.CreatedBy = usrInst.Username;
                        objVehicleTypes.UpdatedDate = DateTime.Now;
                        objVehicleTypes.UpdatedBy = usrInst.Username;
                        objVehicleTypes.Save(objVehicleTypes, sConnStr);
                    }

                    objVehicleTypes = new VehicleTypes();
                    objVehicleTypes = objVehicleTypes.GetVehicleTypesByDescription("Bus", sConnStr);
                    if (objVehicleTypes == null)
                    {
                        objVehicleTypes = new VehicleTypes();
                        objVehicleTypes.VehicleTypeId = 4;
                        objVehicleTypes.Description = "BUS";
                        objVehicleTypes.CreatedDate = DateTime.Now;
                        objVehicleTypes.CreatedBy = usrInst.Username;
                        objVehicleTypes.UpdatedDate = DateTime.Now;
                        objVehicleTypes.UpdatedBy = usrInst.Username;
                        objVehicleTypes.Save(objVehicleTypes, sConnStr);
                    }

                    objVehicleTypes = new VehicleTypes();
                    objVehicleTypes = objVehicleTypes.GetVehicleTypesByDescription("Trailer", sConnStr);
                    if (objVehicleTypes == null)
                    {
                        objVehicleTypes = new VehicleTypes();
                        objVehicleTypes.VehicleTypeId = 5;
                        objVehicleTypes.Description = "TRAILER";
                        objVehicleTypes.CreatedDate = DateTime.Now;
                        objVehicleTypes.CreatedBy = usrInst.Username;
                        objVehicleTypes.UpdatedDate = DateTime.Now;
                        objVehicleTypes.UpdatedBy = usrInst.Username;
                        objVehicleTypes.Save(objVehicleTypes, sConnStr);
                    }

                    objVehicleTypes = new VehicleTypes();
                    objVehicleTypes = objVehicleTypes.GetVehicleTypesByDescription("Motor Cycle", sConnStr);
                    if (objVehicleTypes == null)
                    {
                        objVehicleTypes = new VehicleTypes();
                        objVehicleTypes.VehicleTypeId = 6;
                        objVehicleTypes.Description = "MOTOR CYCLE";
                        objVehicleTypes.CreatedDate = DateTime.Now;
                        objVehicleTypes.CreatedBy = usrInst.Username;
                        objVehicleTypes.UpdatedDate = DateTime.Now;
                        objVehicleTypes.UpdatedBy = usrInst.Username;
                        objVehicleTypes.Save(objVehicleTypes, sConnStr);
                    }
                    #endregion

                    InsertDBUpdate(strUpd, "Asset Management");
                }

                #endregion
                #region Update 19. Assets Management Stored Procedures
                strUpd = "Rez000019";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"
DELIMITER //
	DROP PROCEDURE IF EXISTS sp_AMM_VehicleMaint_CreateChildRecords;
	CREATE PROCEDURE sp_AMM_VehicleMaint_CreateChildRecords
	(
	   iAssetId int,
	   iMaintTypeId int
	)
	BEGIN
      select @iMaintStatusId := (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'To Schedule');
      select @NextMaintDate :=  (SELECT NextMaintDate FROM GFI_AMM_VehicleMaintTypesLink WHERE AssetId = @iAssetId AND MaintTypeId = @iMaintTypeId);

      IF (@NextMaintDate IS NULL) then
			set @NextMaintDate = NOW();
		end if;

      INSERT GFI_AMM_VehicleMaintenance (AssetId, MaintTypeId_cbo, MaintStatusId_cbo, StartDate, EndDate, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy) 
      VALUES (@iAssetId, @iMaintTypeId, @iMaintStatusId, @NextMaintDate, @NextMaintDate, NOW(), CURRENT_USER, NOW(), CURRENT_USER);

      select @iMaintURI = (SELECT LAST_INSERT_ID());

      UPDATE GFI_AMM_VehicleMaintTypesLink
      	SET Status = @iMaintStatusId, MaintURI = @iMaintURI
      WHERE AssetId = @iAssetId AND MaintTypeId = @iMaintTypeId;
	END //
DELIMITER ;";
                    _SqlConn.ScriptExecute(sql, sConnStr);

                    sql = @"DELIMITER //
DROP PROCEDURE IF EXISTS sp_AMM_VehicleMaint_ProcessRecords;
CREATE PROCEDURE sp_AMM_VehicleMaint_ProcessRecords()
sp_lbl:

	BEGIN
	DECLARE NOT_FOUND INT DEFAULT 0;
	    /***********************************************************************************************************************
	
	    Version: 1.0.0.1
	    Modifed: 01/07/2014
	    Modified By: Perry Ramen
	    Description: Process list of Maintenance Types for each vehicle to determine if they are To Schedule] or Overdue]
	
	    ***********************************************************************************************************************/
	     BEGIN
	      DECLARE v_iRecCountProcessed int;
	
	      DECLARE v_iMaintStatusId_Overdue int;
	      DECLARE v_iMaintStatusId_ToSchedule int;
	      DECLARE v_iMaintStatusId_Scheduled int;
	      DECLARE v_iMaintStatusId_InProgress int;
	      DECLARE v_iMaintStatusId_Completed int;
	      DECLARE v_iMaintStatusId_Valid int;
	      DECLARE v_iMaintStatusId_Expired int;
	
	      DECLARE v_URI int;
	      DECLARE v_MaintURI int;
	      DECLARE v_AssetId int;
	      DECLARE v_MaintTypeId int;
	      DECLARE v_NextMaintDate DateTime(3);
	      DECLARE v_NextMaintDateTG DateTime(3); 
	      DECLARE v_CurrentOdometer int;
	      DECLARE v_NextMaintOdometer int;
	      DECLARE v_NextMaintOdometerTG int;
	      DECLARE v_CurrentEngHrs int;
	      DECLARE v_NextMaintEngHrs int;
	      DECLARE v_NextMaintEngHrsTG int;
	      DECLARE v_MaintStatusId int;
	
	      DECLARE v_MaintURIUpdate int;
	
	DECLARE csrData CURSOR FOR
	      SELECT URI, 
	          MaintURI,
	          AssetId,
	          MaintTypeId,
	         NextMaintDate, 
	         NextMaintDateTG, 
	         CurrentOdometer, 
	         NextMaintOdometer, 
	         NextMaintOdometerTG, 
	         CurrentEngHrs,
	         NextMaintEngHrs,
	         NextMaintEngHrsTG,
	         Status
	      FROM GFI_AMM_VehicleMaintTypesLink;
	
	      OPEN csrData;
	
	
	      SET v_iMaintStatusId_Overdue = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Overdue');
	      SET v_iMaintStatusId_ToSchedule = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'To Schedule');
	      SET v_iMaintStatusId_Scheduled = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Scheduled');
	      SET v_iMaintStatusId_InProgress = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'In Progress');		
	      SET v_iMaintStatusId_Completed = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Completed');				
	      SET v_iMaintStatusId_Valid = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Valid');		
	      SET v_iMaintStatusId_Expired = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Expired');				
	
	      
	      FETCH NEXT FROM csrData INTO v_URI, v_MaintURI, v_AssetId, v_MaintTypeId, v_NextMaintDate, v_NextMaintDateTG, v_CurrentOdometer, v_NextMaintOdometer, v_NextMaintOdometerTG, v_CurrentEngHrs, v_NextMaintEngHrs, v_NextMaintEngHrsTG, v_MaintStatusId;
	
	      SET v_iRecCountProcessed = 0;
	
	      WHILE NOT_FOUND = 0
	      DO
	         IF v_NextMaintDate IS NULL THEN SET v_NextMaintDate = NOW();
				END IF;
	
	         -- Flag As: To Schedule
	         IF (((v_NextMaintDateTG <= NOW()) OR (v_NextMaintOdometerTG < v_CurrentOdometer) OR (v_NextMaintEngHrsTG < v_CurrentEngHrs)) AND ((v_MaintStatusId = v_iMaintStatusId_Completed) OR (v_MaintStatusId = v_iMaintStatusId_Expired)))
	         THEN				
	            UPDATE GFI_AMM_VehicleMaintTypesLink
	            SET Status = v_iMaintStatusId_ToSchedule, UpdatedDate = NOW(), UpdatedBy = CURRENT_USER
	            WHERE URI = v_URI;
	
	            INSERT GFI_AMM_VehicleMaintenance (AssetId, MaintTypeId_cbo, MaintStatusId_cbo, StartDate, EndDate, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy) 
	            SELECT (v_AssetId, v_MaintTypeId, v_iMaintStatusId_ToSchedule, v_NextMaintDate, v_NextMaintDate, NOW(), CURRENT_USER, NOW(), CURRENT_USER);
	
	            SET v_MaintURIUpdate = (SELECT LAST_INSERT_ID());
	
	            UPDATE GFI_AMM_VehicleMaintTypesLink
	            SET MaintURI = v_MaintURIUpdate
	            WHERE URI = v_URI;
	         END IF;
	
	
	         -- Flag As: Overdue
	         IF (((v_NextMaintDate <= NOW()) OR (v_NextMaintOdometer < v_CurrentOdometer) OR (v_NextMaintEngHrs < v_CurrentEngHrs)) AND (v_MaintStatusId = v_iMaintStatusId_Completed) OR (v_MaintStatusId = v_iMaintStatusId_Expired))
	         THEN				
	            UPDATE GFI_AMM_VehicleMaintTypesLink
	            SET Status = v_iMaintStatusId_Overdue, UpdatedDate = NOW(), UpdatedBy = CURRENT_USER
	            WHERE URI = v_URI;
	
	            UPDATE GFI_AMM_VehicleMaintenance 
	            SET MaintStatusId_cbo = v_iMaintStatusId_Overdue, UpdatedDate = NOW(), UpdatedBy = CURRENT_USER
	            WHERE URI = v_MaintURI;
	         END IF;
	
	         SET v_iRecCountProcessed = v_iRecCountProcessed + 1;
	
	         FETCH NEXT FROM csrData INTO v_URI, v_MaintURI, v_AssetId, v_MaintTypeId, v_NextMaintDate, v_NextMaintDateTG, v_CurrentOdometer, v_NextMaintOdometer, v_NextMaintOdometerTG, v_CurrentEngHrs, v_NextMaintEngHrs, v_NextMaintEngHrsTG, v_MaintStatusId;
	      END WHILE;
	
	      CLOSE csrData;
	
	    END;
	END;
	//

DELIMITER ;";
                    _SqlConn.ScriptExecute(sql, sConnStr);

                    InsertDBUpdate(strUpd, "Asset Management Stored Procedures");
                }
                #endregion
                #region Update 20. Rules and Exceptions
                strUpd = "Rez000020";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_FLT_AssetDeviceMap add Aux1Name_cbo int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "Aux1Name_cbo")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = "ALTER TABLE GFI_FLT_AssetDeviceMap add Aux2Name_cbo int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "Aux2Name_cbo")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = "ALTER TABLE GFI_FLT_AssetDeviceMap add Aux3Name_cbo int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "Aux3Name_cbo")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = "ALTER TABLE GFI_FLT_AssetDeviceMap add Aux4Name_cbo int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "Aux4Name_cbo")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = "ALTER TABLE GFI_FLT_AssetDeviceMap add Aux5Name_cbo int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "Aux5Name_cbo")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = "ALTER TABLE GFI_FLT_AssetDeviceMap add Aux6Name_cbo int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "Aux6Name_cbo")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "drop table GFI_GPS_Rules]";
                    if (GetDataDT(ExistTableSql("GFI_GPS_Rules")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"CREATE TABLE GFI_GPS_Rules](
                             RuleID int AUTO_INCREMENT NOT NULL,
                             ParentRuleID int NULL,
                             RuleType int NULL,
                             MinTriggerValue int NULL,
                             MaxTriggerValue int NULL,
                             Struc nvarchar](20) NOT NULL,
                             StrucValue nvarchar](20) NOT NULL,
                             StrucCondition nvarchar](100) NULL,
                             RuleName nvarchar](300) NULL,
                             StrucType nvarchar](20) NULL,
                             CONSTRAINT PK_GFI_GPS_Rules PRIMARY KEY  ([RuleID ASC)
                            )";
                    if (GetDataDT(ExistTableSql("GFI_GPS_Rules")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "drop table GFI_GPS_Exceptions]";
                    if (GetDataDT(ExistTableSql("GFI_GPS_Exceptions")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"CREATE TABLE GFI_GPS_Exceptions](
                             iID int AUTO_INCREMENT NOT NULL,
                             RuleID int NULL,
                             GPSDataID bigint NULL,
                             AssetID int NULL,
                             DriverID int NULL,
                             Severity nvarchar](100) NULL,
                             CONSTRAINT PK_GFI_GPS_Exceptions PRIMARY KEY  ([iID ASC)
                            )";
                    if (GetDataDT(ExistTableSql("GFI_GPS_Exceptions")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "Rules and Exceptions updated");
                }
                #endregion
                #region Update 21. Assets Encrypted
                strUpd = "Rez000021";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "Alter table GFI_FLT_Asset modify column AssetNumber nvarchar(50) null";
                    dt = GetDataDT(GetFieldSchema("GFI_FLT_Asset", "AssetNumber"));
                    if (dt.Rows[0]["CHARACTER_MAXIMUM_LENGTH"].ToString() != "50")
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_AMM_VehicleMaintItems DROP foreign key FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance; ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance", "GFI_AMM_VehicleMaintItems")).Rows.Count > 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE Tmp_GFI_AMM_VehicleMaintItems
	                            (
	                                URI int NOT NULL AUTO_INCREMENT,
	                                MaintURI int NOT NULL,
	                                ItemCode nvarchar(50) NULL,
	                                Description nvarchar(120) NULL,
	                                Quantity numeric(18, 2) NULL,
	                                UnitCost numeric(18, 2) NULL,
	                                CreatedDate datetime NULL,
	                                CreatedBy nvarchar(30) NULL,
	                                UpdatedDate datetime NULL,
	                                UpdatedBy nvarchar(30) NULL,
                                    CONSTRAINT PK_GFI_AMM_VehicleMaintItems PRIMARY KEY (URI ASC)
	                            );
                                
                            INSERT INTO Tmp_GFI_AMM_VehicleMaintItems (URI, MaintURI, ItemCode, Description, Quantity, UnitCost, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy)
	                            SELECT URI, MaintURI, ItemCode, Description, Quantity, UnitCost, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy FROM GFI_AMM_VehicleMaintItems;

                            DROP TABLE GFI_AMM_VehicleMaintItems;
                                
                            RENAME TABLE Tmp_GFI_AMM_VehicleMaintItems To GFI_AMM_VehicleMaintItems;
                                
                            CREATE INDEX IX_ItemCode ON GFI_AMM_VehicleMaintItems (ItemCode);                            
                            CREATE  INDEX IX_MaintURI ON GFI_AMM_VehicleMaintItems (MaintURI);
                            ALTER TABLE GFI_AMM_VehicleMaintItems 
									 	ADD CONSTRAINT FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance 
										 FOREIGN KEY (MaintURI) 
										 REFERENCES GFI_AMM_VehicleMaintenance(URI) 
										 	ON UPDATE  NO ACTION 
	                              ON DELETE  NO ACTION;";
                    dt = GetDataDT(GetFieldSchema("GFI_AMM_VehicleMaintItems", "Quantity"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "numeric")
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "Assets Encrypted");
                }
                #endregion
                #region Update 22. Group Matrix updated
                strUpd = "Rez000022";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"update GFI_SYS_GroupMatrix set GMDescription = '### ROOT ###' where GMID = 1";
                    dbExecute(sql);
                    sql = @"update GFI_SYS_GroupMatrix set GMDescription = '### Security ###' where GMID = 2";
                    dbExecute(sql);
                    sql = @"update GFI_SYS_GroupMatrix set GMDescription = '### All Clients ###' where GMID = 3";
                    dbExecute(sql);

                    sql = @"update GFI_SYS_GroupMatrix set ParentGMID = 3 where ParentGMID = 1 and GMID > 3";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "Group Matrix updated");
                }
                #endregion
                #region Update 23. Rule Group Matrix
                strUpd = "Rez000023";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_SYS_GroupMatrixRule
                        (
	                        MID INT AUTO_INCREMENT NOT NULL
	                        , GMID INT NOT NULL
	                        , iID INT NOT NULL
                            , CONSTRAINT PK_GFI_SYS_GroupMatrixRule PRIMARY KEY  (MID ASC)
                        )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrixRule")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixRule 
                            ADD CONSTRAINT FK_GFI_SYS_GroupMatrixRule_GFI_SYS_GroupMatrix 
                                FOREIGN KEY (GMID) 
                                REFERENCES GFI_SYS_GroupMatrix (GMID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixRule_GFI_SYS_GroupMatrix", "GFI_SYS_GroupMatrixRule")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixRule 
                            ADD CONSTRAINT FK_GFI_SYS_GroupMatrixRule_GFI_GPS_Rules 
                                FOREIGN KEY(iID) 
                                REFERENCES GFI_GPS_Rules(RuleID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixRule_GFI_GPS_Rules", "GFI_SYS_GroupMatrixRule")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "Rule Group Matrix");
                }
                #endregion
                #region Update 24. Admin User
                strUpd = "Rez000024";
                if (!CheckUpdateExist(strUpd))
                {
                    User u = new User();
                    u = u.GetUserByeMail("ADMIN@naveo.mu", "SysInitPass", sConnStr, false, true);
                    u.Password = "Core4dmin@123";
                    u.Update(u, sConnStr);

                    InsertDBUpdate(strUpd, "Admin User");
                }
                #endregion
                #region Update 25. Zone Types
                strUpd = "Rez000025";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_FLT_ZoneType
                        (
	                        ZoneTypeID  int  AUTO_INCREMENT NOT NULL,
	                        sDescription  varchar (50) NULL,
	                        sComments  nvarchar (1024) NULL,
                            isSystem  nvarchar  (1) NOT NULL DEFAULT 0,
                            CONSTRAINT PK_GFI_FLT_ZoneType  PRIMARY KEY  (ZoneTypeID ASC)
                        )";
                    if (GetDataDT(ExistTableSql("GFI_FLT_ZoneType")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_SYS_GroupMatrixZoneType 
                        (
	                        MID  INT AUTO_INCREMENT NOT NULL
	                        , GMID  INT NOT NULL
	                        , iID  INT NOT NULL
                            , CONSTRAINT PK_GFI_SYS_GroupMatrixZoneType  PRIMARY KEY  (MID ASC)
                        )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrixZoneType")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixZoneType 
                            ADD CONSTRAINT FK_GFI_SYS_GroupMatrixZoneType_GFI_SYS_GroupMatrix 
                                FOREIGN KEY (GMID) 
                                REFERENCES GFI_SYS_GroupMatrix (GMID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixZoneType_GFI_SYS_GroupMatrix", "GFI_SYS_GroupMatrixZoneType")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixZoneType 
                            ADD CONSTRAINT FK_GFI_SYS_GroupMatrixZoneType_GFI_FLT_ZoneType 
                                FOREIGN KEY(iID) 
                                REFERENCES GFI_FLT_ZoneType(ZoneTypeID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixZoneType_GFI_FLT_ZoneType", "GFI_SYS_GroupMatrixZoneType")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_FLT_ZoneHeadZoneType 
                        (
	                        ZoneHeadID  int  NOT NULL,
	                        ZoneTypeID  int  NOT NULL,
	                        iID  int  AUTO_INCREMENT NOT NULL,
                            CONSTRAINT PK_GFI_FLT_ZoneHeadZoneType  PRIMARY KEY (iID ASC)
                        )";
                    if (GetDataDT(ExistTableSql("GFI_FLT_ZoneHeadZoneType")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_ZoneHeadZoneType
                            ADD CONSTRAINT FK_GFI_FLT_ZoneHeadZoneType_GFI_FLT_ZoneHeader 
                                FOREIGN KEY(ZoneHeadID)
                                REFERENCES GFI_FLT_ZoneHeader  (ZoneID)
                            ON DELETE CASCADE";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ZoneHeadZoneType_GFI_FLT_ZoneHeader", "GFI_FLT_ZoneHeadZoneType")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_ZoneHeadZoneType 
                            ADD CONSTRAINT FK_GFI_FLT_ZoneHeadZoneType_GFI_FLT_ZoneType 
                                FOREIGN KEY(ZoneTypeID)
                                REFERENCES GFI_FLT_ZoneType  (ZoneTypeID)
                            ON DELETE CASCADE";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ZoneHeadZoneType_GFI_FLT_ZoneType", "GFI_FLT_ZoneHeadZoneType")).Rows.Count == 0)
                        dbExecute(sql);

                    GroupMatrix gmRoot = new GroupMatrix();
                    gmRoot = new NaveoOneLib.Services.GroupMatrixService().GetRoot(sConnStr);
                    ZoneType zt = new ZoneType().GetZoneTypeByComments("1", sConnStr);
                    if (zt == null)
                    {
                        zt = new ZoneType();
                        zt.sDescription = "Address Lookup Zone";
                        zt.sComments = "1";
                        zt.isSystem = "1";

                        Matrix m = new Matrix();
                        m.GMID = gmRoot.gmID;
                        m.oprType = DataRowState.Added;
                        zt.lMatrix.Add(m);

                        zt.Save(zt, true, sConnStr);
                    }

                    zt = new ZoneType().GetZoneTypeByComments("2", sConnStr);
                    if (zt == null)
                    {
                        zt = new ZoneType();
                        zt.sDescription = "Customer Zone";
                        zt.sComments = "2";
                        zt.isSystem = "1";

                        Matrix m = new Matrix();
                        m.GMID = gmRoot.gmID;
                        m.oprType = DataRowState.Added;
                        zt.lMatrix.Add(m);

                        zt.Save(zt, true, sConnStr);
                    }

                    zt = new ZoneType().GetZoneTypeByComments("3", sConnStr);
                    if (zt == null)
                    {
                        zt = new ZoneType();
                        zt.sDescription = "Office/Depot Zone";
                        zt.sComments = "3";
                        zt.isSystem = "1";

                        Matrix m = new Matrix();
                        m.GMID = gmRoot.gmID;
                        m.oprType = DataRowState.Added;
                        zt.lMatrix.Add(m);

                        zt.Save(zt, true, sConnStr);
                    }

                    zt = new ZoneType().GetZoneTypeByComments("4", sConnStr);
                    if (zt == null)
                    {
                        zt = new ZoneType();
                        zt.sDescription = "Home Zone";
                        zt.sComments = "4";
                        zt.isSystem = "1";

                        Matrix m = new Matrix();
                        m.GMID = gmRoot.gmID;
                        m.oprType = DataRowState.Added;
                        zt.lMatrix.Add(m);

                        zt.Save(zt, true, sConnStr);
                    }

                    if (GetDataDT(ExistColumnSql("GFI_FLT_ZoneHeader", "ZoneTypeID")).Rows[0][0].ToString() == "1")
                    {
                        sql = "select * from GFI_FLT_ZoneHeader";
                        DataTable dtz = GetDataDT(sql);
                        Boolean b = true;
                        foreach (DataRow drz in dtz.Rows)
                        {
                            int cmID = (int)drz["ZoneTypeID"];
                            zt = new ZoneType().GetZoneTypeByComments(cmID.ToString(), sConnStr);
                            if (zt == null)
                                zt = new ZoneType().GetZoneTypeByComments("1", sConnStr);

                            ZoneHeadZoneType zhzt = new ZoneHeadZoneType();
                            zhzt.ZoneHeadID = (int)(drz["ZoneID"]);
                            zhzt.ZoneTypeID = zt.ZoneTypeID;
                            b = b & zhzt.Save(zhzt, sConnStr);
                        }

                        if (b)
                        {
                            sql = @"SELECT object_name(default_object_id)
                             FROM sys.columns
                             WHERE object_id = object_id('[GFI_FLT_ZoneHeader]')
                               AND name = 'ZoneTypeID'";
                            dtz = GetDataDT(sql);
                            if (dtz.Rows.Count > 0)
                            {
                                String strConstrainName = dtz.Rows[0][0].ToString();
                                sql = "ALTER TABLE GFI_FLT_ZoneHeader] DROP FOREIGN KEY " + strConstrainName;
                                dbExecute(sql);

                                sql = "ALTER TABLE GFI_FLT_ZoneHeader] drop column ZoneTypeID]";
                                dbExecute(sql);
                            }
                        }
                    }
                    InsertDBUpdate(strUpd, "Zone Types");
                }
                #endregion
                #region Update 26. Vehicle Maintenance Stored Proc
                strUpd = "Rez000026";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE  INDEX IX_GFI_GPS_GPSData_AssetID_DateTimeGPS ON GFI_GPS_GPSData 
                        (
	                        AssetID ASC,
	                        DateTimeGPS_UTC ASC
                        )";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSData", "IX_GFI_GPS_GPSData_AssetID_DateTimeGPS")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE  INDEX IX_GFI_FLT_AssetDeviceMap_DeviceId_Status ON GFI_FLT_AssetDeviceMap 
                        (
	                        DeviceId ASC,
	                        Status_b ASC
                        )";
                    if (GetDataDT(ExistsIndex("GFI_FLT_AssetDeviceMap", "IX_GFI_FLT_AssetDeviceMap_DeviceId_Status")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE  INDEX IX_GFI_FLT_Driver_iButton ON GFI_FLT_Driver 
                        (
	                        iButton ASC
                        )";
                    if (GetDataDT(ExistsIndex("GFI_FLT_Driver", "IX_GFI_FLT_Driver_iButton")).Rows.Count == 0)
                        dbExecute(sql);

                    Globals.uLogin = usrInst;
                    #region Inserts
                    //----------------------- GFI_AMM_VehicleMaintCat ----------------------------//
                    VehicleTypes objVehicleTypes = new VehicleTypes();
                    objVehicleTypes = objVehicleTypes.GetVehicleTypesByDescription("Van", sConnStr);
                    if (objVehicleTypes == null)
                    {
                        objVehicleTypes = new VehicleTypes();
                        objVehicleTypes.VehicleTypeId = 2;
                        objVehicleTypes.Description = "VAN";
                        objVehicleTypes.CreatedDate = DateTime.Now;
                        objVehicleTypes.CreatedBy = usrInst.Username;
                        objVehicleTypes.UpdatedDate = DateTime.Now;
                        objVehicleTypes.UpdatedBy = usrInst.Username;
                        objVehicleTypes.Save(objVehicleTypes, sConnStr);
                    }

                    VehicleMaintCat objVehicleMaintCat = new VehicleMaintCat();
                    objVehicleMaintCat = objVehicleMaintCat.GetVehicleMaintCatByDescription("Maintenance", sConnStr);
                    if (objVehicleMaintCat == null)
                    {
                        objVehicleMaintCat.MaintCatId = 1;
                        objVehicleMaintCat.Description = "PLANNED MAINTENANCE";
                        objVehicleMaintCat.CreatedDate = DateTime.Now;
                        objVehicleMaintCat.CreatedBy = usrInst.Username;
                        objVehicleMaintCat.UpdatedDate = DateTime.Now;
                        objVehicleMaintCat.UpdatedBy = usrInst.Username;
                        objVehicleMaintCat.Save(objVehicleMaintCat, sConnStr);
                    }

                    objVehicleMaintCat = new VehicleMaintCat();
                    objVehicleMaintCat = objVehicleMaintCat.GetVehicleMaintCatByDescription("UNPLANNED MAINTENANCE", sConnStr);
                    if (objVehicleMaintCat == null)
                    {
                        objVehicleMaintCat = new VehicleMaintCat();
                        objVehicleMaintCat.MaintCatId = 5;
                        objVehicleMaintCat.Description = "UNPLANNED MAINTENANCE";
                        objVehicleMaintCat.CreatedDate = DateTime.Now;
                        objVehicleMaintCat.CreatedBy = usrInst.Username;
                        objVehicleMaintCat.UpdatedDate = DateTime.Now;
                        objVehicleMaintCat.UpdatedBy = usrInst.Username;
                        objVehicleMaintCat.Save(objVehicleMaintCat, sConnStr);
                    }

                    objVehicleMaintCat = new VehicleMaintCat();
                    objVehicleMaintCat = objVehicleMaintCat.GetVehicleMaintCatByDescription("FUEL MANAGEMENT", sConnStr);
                    if (objVehicleMaintCat == null)
                    {
                        objVehicleMaintCat = new VehicleMaintCat();
                        objVehicleMaintCat.MaintCatId = 6;
                        objVehicleMaintCat.Description = "FUEL MANAGEMENT";
                        objVehicleMaintCat.CreatedDate = DateTime.Now;
                        objVehicleMaintCat.CreatedBy = usrInst.Username;
                        objVehicleMaintCat.UpdatedDate = DateTime.Now;
                        objVehicleMaintCat.UpdatedBy = usrInst.Username;
                        objVehicleMaintCat.Save(objVehicleMaintCat, sConnStr);
                    }

                    objVehicleMaintCat = new VehicleMaintCat();
                    objVehicleMaintCat = objVehicleMaintCat.GetVehicleMaintCatByDescription("ACCIDENT MANAGEMENT", sConnStr);
                    if (objVehicleMaintCat == null)
                    {
                        objVehicleMaintCat = new VehicleMaintCat();
                        objVehicleMaintCat.MaintCatId = 7;
                        objVehicleMaintCat.Description = "ACCIDENT MANAGEMENT";
                        objVehicleMaintCat.CreatedDate = DateTime.Now;
                        objVehicleMaintCat.CreatedBy = usrInst.Username;
                        objVehicleMaintCat.UpdatedDate = DateTime.Now;
                        objVehicleMaintCat.UpdatedBy = usrInst.Username;
                        objVehicleMaintCat.Save(objVehicleMaintCat, sConnStr);
                    }

                    objVehicleMaintCat = new VehicleMaintCat();
                    objVehicleMaintCat = objVehicleMaintCat.GetVehicleMaintCatByDescription("GPS UNIT MANAGEMENT", sConnStr);
                    if (objVehicleMaintCat == null)
                    {
                        objVehicleMaintCat = new VehicleMaintCat();
                        objVehicleMaintCat.MaintCatId = 8;
                        objVehicleMaintCat.Description = "GPS UNIT MANAGEMENT";
                        objVehicleMaintCat.CreatedDate = DateTime.Now;
                        objVehicleMaintCat.CreatedBy = usrInst.Username;
                        objVehicleMaintCat.UpdatedDate = DateTime.Now;
                        objVehicleMaintCat.UpdatedBy = usrInst.Username;
                        objVehicleMaintCat.Save(objVehicleMaintCat, sConnStr);
                    }

                    objVehicleMaintCat = new VehicleMaintCat();
                    objVehicleMaintCat = objVehicleMaintCat.GetVehicleMaintCatByDescription("FOLLOWUP", sConnStr);
                    if (objVehicleMaintCat == null)
                    {
                        objVehicleMaintCat = new VehicleMaintCat();
                        objVehicleMaintCat.MaintCatId = 9;
                        objVehicleMaintCat.Description = "FOLLOWUP";
                        objVehicleMaintCat.CreatedDate = DateTime.Now;
                        objVehicleMaintCat.CreatedBy = usrInst.Username;
                        objVehicleMaintCat.UpdatedDate = DateTime.Now;
                        objVehicleMaintCat.UpdatedBy = usrInst.Username;
                        objVehicleMaintCat.Save(objVehicleMaintCat, sConnStr);
                    }

                    //----------------------- GFI_AMM_VehicleMaintTypes ----------------------------//

                    VehicleMaintTypes objVehicleMaintTypes = new VehicleMaintTypes();
                    objVehicleMaintTypes = objVehicleMaintTypes.GetVehicleMaintTypesByDescription("REPAIRS", sConnStr);
                    if (objVehicleMaintTypes == null)
                    {
                        objVehicleMaintTypes = new VehicleMaintTypes();
                        objVehicleMaintTypes.MaintCatId_cbo = 5;
                        objVehicleMaintTypes.Description = "REPAIRS";
                        objVehicleMaintTypes.OccurrenceType = 0;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceDuration = 0;
                        objVehicleMaintTypes.OccurrenceDurationTh = 0;
                        objVehicleMaintTypes.OccurrenceKM = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrs = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceKMTh = 0;
                        objVehicleMaintTypes.CreatedDate = DateTime.Now;
                        objVehicleMaintTypes.CreatedBy = usrInst.Username;
                        objVehicleMaintTypes.UpdatedDate = DateTime.Now;
                        objVehicleMaintTypes.UpdatedBy = usrInst.Username;
                        objVehicleMaintTypes.Save(objVehicleMaintTypes, sConnStr);
                    }

                    objVehicleMaintTypes = new VehicleMaintTypes();
                    objVehicleMaintTypes = objVehicleMaintTypes.GetVehicleMaintTypesByDescription("VEHICLE CHECK", sConnStr);
                    if (objVehicleMaintTypes == null)
                    {
                        objVehicleMaintTypes = new VehicleMaintTypes();
                        objVehicleMaintTypes.MaintCatId_cbo = 5;
                        objVehicleMaintTypes.Description = "VEHICLE CHECK";
                        objVehicleMaintTypes.OccurrenceType = 0;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceDuration = 0;
                        objVehicleMaintTypes.OccurrenceDurationTh = 0;
                        objVehicleMaintTypes.OccurrenceKM = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrs = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceKMTh = 0;
                        objVehicleMaintTypes.CreatedDate = DateTime.Now;
                        objVehicleMaintTypes.CreatedBy = usrInst.Username;
                        objVehicleMaintTypes.UpdatedDate = DateTime.Now;
                        objVehicleMaintTypes.UpdatedBy = usrInst.Username;
                        objVehicleMaintTypes.Save(objVehicleMaintTypes, sConnStr);
                    }

                    objVehicleMaintTypes = new VehicleMaintTypes();
                    objVehicleMaintTypes = objVehicleMaintTypes.GetVehicleMaintTypesByDescription("PETROL", sConnStr);
                    if (objVehicleMaintTypes == null)
                    {
                        objVehicleMaintTypes = new VehicleMaintTypes();
                        objVehicleMaintTypes.MaintCatId_cbo = 6;
                        objVehicleMaintTypes.Description = "FUEL TYPE - PETROL";
                        objVehicleMaintTypes.OccurrenceType = 0;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceDuration = 0;
                        objVehicleMaintTypes.OccurrenceDurationTh = 0;
                        objVehicleMaintTypes.OccurrenceKM = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrs = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceKMTh = 0;
                        objVehicleMaintTypes.CreatedDate = DateTime.Now;
                        objVehicleMaintTypes.CreatedBy = usrInst.Username;
                        objVehicleMaintTypes.UpdatedDate = DateTime.Now;
                        objVehicleMaintTypes.UpdatedBy = usrInst.Username;
                        objVehicleMaintTypes.Save(objVehicleMaintTypes, sConnStr);
                    }

                    objVehicleMaintTypes = new VehicleMaintTypes();
                    objVehicleMaintTypes = objVehicleMaintTypes.GetVehicleMaintTypesByDescription("DIESEL", sConnStr);
                    if (objVehicleMaintTypes == null)
                    {
                        objVehicleMaintTypes = new VehicleMaintTypes();
                        objVehicleMaintTypes.MaintCatId_cbo = 6;
                        objVehicleMaintTypes.Description = "FUEL TYPE - DIESEL";
                        objVehicleMaintTypes.OccurrenceType = 0;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceDuration = 0;
                        objVehicleMaintTypes.OccurrenceDurationTh = 0;
                        objVehicleMaintTypes.OccurrenceKM = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrs = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceKMTh = 0;
                        objVehicleMaintTypes.CreatedDate = DateTime.Now;
                        objVehicleMaintTypes.CreatedBy = usrInst.Username;
                        objVehicleMaintTypes.UpdatedDate = DateTime.Now;
                        objVehicleMaintTypes.UpdatedBy = usrInst.Username;
                        objVehicleMaintTypes.Save(objVehicleMaintTypes, sConnStr);
                    }

                    objVehicleMaintTypes = new VehicleMaintTypes();
                    objVehicleMaintTypes = objVehicleMaintTypes.GetVehicleMaintTypesByDescription("GAS", sConnStr);
                    if (objVehicleMaintTypes == null)
                    {
                        objVehicleMaintTypes = new VehicleMaintTypes();
                        objVehicleMaintTypes.MaintCatId_cbo = 6;
                        objVehicleMaintTypes.Description = "FUEL TYPE - GAS";
                        objVehicleMaintTypes.OccurrenceType = 0;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceDuration = 0;
                        objVehicleMaintTypes.OccurrenceDurationTh = 0;
                        objVehicleMaintTypes.OccurrenceKM = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrs = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceKMTh = 0;
                        objVehicleMaintTypes.CreatedDate = DateTime.Now;
                        objVehicleMaintTypes.CreatedBy = usrInst.Username;
                        objVehicleMaintTypes.UpdatedDate = DateTime.Now;
                        objVehicleMaintTypes.UpdatedBy = usrInst.Username;
                        objVehicleMaintTypes.Save(objVehicleMaintTypes, sConnStr);
                    }

                    objVehicleMaintTypes = new VehicleMaintTypes();
                    objVehicleMaintTypes = objVehicleMaintTypes.GetVehicleMaintTypesByDescription("ELECTRIC POWER", sConnStr);
                    if (objVehicleMaintTypes == null)
                    {
                        objVehicleMaintTypes = new VehicleMaintTypes();
                        objVehicleMaintTypes.MaintCatId_cbo = 6;
                        objVehicleMaintTypes.Description = "FUEL TYPE - ELECTRIC POWER";
                        objVehicleMaintTypes.OccurrenceType = 0;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceDuration = 0;
                        objVehicleMaintTypes.OccurrenceDurationTh = 0;
                        objVehicleMaintTypes.OccurrenceKM = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrs = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceKMTh = 0;
                        objVehicleMaintTypes.CreatedDate = DateTime.Now;
                        objVehicleMaintTypes.CreatedBy = usrInst.Username;
                        objVehicleMaintTypes.UpdatedDate = DateTime.Now;
                        objVehicleMaintTypes.UpdatedBy = usrInst.Username;
                        objVehicleMaintTypes.Save(objVehicleMaintTypes, sConnStr);
                    }

                    objVehicleMaintTypes = new VehicleMaintTypes();
                    objVehicleMaintTypes = objVehicleMaintTypes.GetVehicleMaintTypesByDescription("ACCIDENT", sConnStr);
                    if (objVehicleMaintTypes == null)
                    {
                        objVehicleMaintTypes = new VehicleMaintTypes();
                        objVehicleMaintTypes.MaintCatId_cbo = 7;
                        objVehicleMaintTypes.Description = "ACCIDENT";
                        objVehicleMaintTypes.OccurrenceType = 0;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceDuration = 0;
                        objVehicleMaintTypes.OccurrenceDurationTh = 0;
                        objVehicleMaintTypes.OccurrenceKM = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrs = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceKMTh = 0;
                        objVehicleMaintTypes.CreatedDate = DateTime.Now;
                        objVehicleMaintTypes.CreatedBy = usrInst.Username;
                        objVehicleMaintTypes.UpdatedDate = DateTime.Now;
                        objVehicleMaintTypes.UpdatedBy = usrInst.Username;
                        objVehicleMaintTypes.Save(objVehicleMaintTypes, sConnStr);
                    }

                    objVehicleMaintTypes = new VehicleMaintTypes();
                    objVehicleMaintTypes = objVehicleMaintTypes.GetVehicleMaintTypesByDescription("GPS UNIT INSTALLATION", sConnStr);
                    if (objVehicleMaintTypes == null)
                    {
                        objVehicleMaintTypes = new VehicleMaintTypes();
                        objVehicleMaintTypes.MaintCatId_cbo = 8;
                        objVehicleMaintTypes.Description = "GPS UNIT INSTALLATION";
                        objVehicleMaintTypes.OccurrenceType = 0;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceDuration = 0;
                        objVehicleMaintTypes.OccurrenceDurationTh = 0;
                        objVehicleMaintTypes.OccurrenceKM = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrs = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceKMTh = 0;
                        objVehicleMaintTypes.CreatedDate = DateTime.Now;
                        objVehicleMaintTypes.CreatedBy = usrInst.Username;
                        objVehicleMaintTypes.UpdatedDate = DateTime.Now;
                        objVehicleMaintTypes.UpdatedBy = usrInst.Username;
                        objVehicleMaintTypes.Save(objVehicleMaintTypes, sConnStr);
                    }

                    objVehicleMaintTypes = new VehicleMaintTypes();
                    objVehicleMaintTypes = objVehicleMaintTypes.GetVehicleMaintTypesByDescription("GPS UNIT REPLACEMENT", sConnStr);
                    if (objVehicleMaintTypes == null)
                    {
                        objVehicleMaintTypes = new VehicleMaintTypes();
                        objVehicleMaintTypes.MaintCatId_cbo = 8;
                        objVehicleMaintTypes.Description = "GPS UNIT REPLACEMENT";
                        objVehicleMaintTypes.OccurrenceType = 0;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceDuration = 0;
                        objVehicleMaintTypes.OccurrenceDurationTh = 0;
                        objVehicleMaintTypes.OccurrenceKM = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrs = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceKMTh = 0;
                        objVehicleMaintTypes.CreatedDate = DateTime.Now;
                        objVehicleMaintTypes.CreatedBy = usrInst.Username;
                        objVehicleMaintTypes.UpdatedDate = DateTime.Now;
                        objVehicleMaintTypes.UpdatedBy = usrInst.Username;
                        objVehicleMaintTypes.Save(objVehicleMaintTypes, sConnStr);
                    }

                    objVehicleMaintTypes = new VehicleMaintTypes();
                    objVehicleMaintTypes = objVehicleMaintTypes.GetVehicleMaintTypesByDescription("REMINDER", sConnStr);
                    if (objVehicleMaintTypes == null)
                    {
                        objVehicleMaintTypes = new VehicleMaintTypes();
                        objVehicleMaintTypes.MaintCatId_cbo = 9;
                        objVehicleMaintTypes.Description = "REMINDER";
                        objVehicleMaintTypes.OccurrenceType = 0;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceDuration = 0;
                        objVehicleMaintTypes.OccurrenceDurationTh = 0;
                        objVehicleMaintTypes.OccurrenceKM = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrs = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceKMTh = 0;
                        objVehicleMaintTypes.CreatedDate = DateTime.Now;
                        objVehicleMaintTypes.CreatedBy = usrInst.Username;
                        objVehicleMaintTypes.UpdatedDate = DateTime.Now;
                        objVehicleMaintTypes.UpdatedBy = usrInst.Username;
                        objVehicleMaintTypes.Save(objVehicleMaintTypes, sConnStr);
                    }
                    #endregion
                    Globals.uLogin = null;

                    InsertDBUpdate(strUpd, "Vehicle Maintenance SP and indexes");
                }
                #endregion
                #region Update 27. Assets Management Stored Proc (Insurance Valid/To Schedule)
                strUpd = "Rez000027";
                if (!CheckUpdateExist(strUpd))
                {
                    InsertDBUpdate(strUpd, "Assets Management Stored Proc (Insurance Valid/To Schedule)");
                }
                #endregion

                #region Update 28. Odo and Hr Meter
                strUpd = "Rez000028";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "DROP FUNCTION `GetCalcOdo`;";
                    if (GetDataDT(ExistsFunction("GetCalcOdo")).Rows.Count > 0)
                        _SqlConn.ScriptExecute(sql, sConnStr);

                    sql = @"DELIMITER //
                            CREATE function GetCalcOdo (p_AssetID int)
                                                    returns double
                                                    Begin
                                                    return
                                                    (
	                                                    select COALESCE(SUM(h.TripDistance),0) 
		                                                    + COALESCE(
			                                                    (select ActualOdometer 
				                                                    from GFI_AMM_VehicleMaintenance 
				                                                    where AssetId = p_AssetID 
					                                                    and ActualOdometer is not null
					                                                    and ActualOdometer > 0
				                                                    order by URI desc limit 1
			                                                    ),0)
	                                                    from GFI_GPS_TripHeader h, GFI_GPS_GPSData g
		                                                    where h.AssetID = p_AssetID 
			                                                    and h.GPSDataEndUID = g.UID
			                                                    and g.DateTimeGPS_UTC >= 
			                                                    COALESCE(
					                                                    (select EndDate 
						                                                    from GFI_AMM_VehicleMaintenance 
						                                                    where AssetId = p_AssetID 
							                                                    and ActualOdometer is not null
							                                                    and ActualOdometer > 0
						                                                    order by URI desc limit 1
					                                                    ),TIMESTAMPADD(YEAR, -50, NOW(3)))
                                                    );
                                                    end;
                            //
                            DELIMITER ;";
                    if (GetDataDT(ExistsFunction("GetCalcOdo")).Rows.Count == 0)
                        _SqlConn.ScriptExecute(sql, sConnStr);

                    sql = "DROP FUNCTION `GetCalcEngHrs`;";
                    if (GetDataDT(ExistsFunction("GetCalcEngHrs")).Rows.Count > 0)
                        _SqlConn.ScriptExecute(sql, sConnStr);

                    sql = @"DELIMITER //
                        CREATE function GetCalcEngHrs (AssetID int)
                        returns float
                        Begin
                        return
                        (
	                        select COALESCE(SUM(h.TripTime),0) 
		                        + COALESCE(
			                        (select ActualEngineHrs 
				                        from GFI_AMM_VehicleMaintenance 
				                        where AssetId = @AssetID 
					                        and ActualEngineHrs is not null
					                        and ActualEngineHrs > 0
				                        order by URI desc limit 1
			                        ),0)
	                        from GFI_GPS_TripHeader h, GFI_GPS_GPSData g
		                        where h.AssetID = @AssetID 
			                        and h.GPSDataEndUID = g.UID
			                        and g.DateTimeGPS_UTC >= 
			                        COALESCE(
					                        (select EndDate 
						                        from GFI_AMM_VehicleMaintenance 
						                        where AssetId = @AssetID 
							                        and ActualEngineHrs is not null
							                        and ActualEngineHrs > 0
						                        order by URI desc limit 1
					                        ),DATEADD(YY, -50, GETDATE()))
                        );
                        end;
                        //
                        DELIMITER ;";
                    if (GetDataDT(ExistsFunction("GetCalcEngHrs")).Rows.Count == 0)
                        _SqlConn.ScriptExecute(sql, sConnStr);

                    sql = "CREATE TABLE GFI_GPS_DriversDriving(";
                    sql += "\r\n    unitID varchar(20) NOT NULL,";
                    sql += "\r\n    DriverID varchar(20) NULL,";
                    sql += "\r\n    dtDateTime datetime NULL,";
                    sql += "\r\n CONSTRAINT PK_GFI_GPS_DriversDriving PRIMARY KEY  ";
                    sql += "\r\n(";
                    sql += "\r\n    unitID ASC";
                    sql += "\r\n)";
                    sql += "\r\n)";
                    if (GetDataDT(ExistTableSql("GFI_GPS_DriversDriving")).Rows.Count == 0)
                        dbExecute(sql);

                    VehicleMaintCat objVehicleMaintCat = new VehicleMaintCat();
                    objVehicleMaintCat = objVehicleMaintCat.GetVehicleMaintCatByDescription("ADMIN FUNCTIONS", sConnStr);
                    if (objVehicleMaintCat == null)
                    {
                        objVehicleMaintCat = new VehicleMaintCat();
                        objVehicleMaintCat.MaintCatId = 10;
                        objVehicleMaintCat.Description = "ADMIN FUNCTIONS";
                        objVehicleMaintCat.CreatedDate = DateTime.Now;
                        objVehicleMaintCat.CreatedBy = usrInst.Username;
                        objVehicleMaintCat.UpdatedDate = DateTime.Now;
                        objVehicleMaintCat.UpdatedBy = usrInst.Username;
                        objVehicleMaintCat.Save(objVehicleMaintCat, sConnStr);
                    }
                    VehicleMaintTypes objVehicleMaintTypes = new VehicleMaintTypes();
                    objVehicleMaintTypes = objVehicleMaintTypes.GetVehicleMaintTypesByDescription("ODOMETER/ENGINE HOURS ENTRY", sConnStr);
                    if (objVehicleMaintTypes == null)
                    {
                        objVehicleMaintTypes = new VehicleMaintTypes();
                        objVehicleMaintTypes.MaintCatId_cbo = 10;
                        objVehicleMaintTypes.Description = "ODOMETER/ENGINE HOURS ENTRY";
                        objVehicleMaintTypes.OccurrenceType = 0;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceDuration = 0;
                        objVehicleMaintTypes.OccurrenceDurationTh = 0;
                        objVehicleMaintTypes.OccurrenceKM = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrs = 0;
                        objVehicleMaintTypes.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintTypes.OccurrenceFixedDateTh = 0;
                        objVehicleMaintTypes.OccurrenceKMTh = 0;
                        objVehicleMaintTypes.CreatedDate = DateTime.Now;
                        objVehicleMaintTypes.CreatedBy = usrInst.Username;
                        objVehicleMaintTypes.UpdatedDate = DateTime.Now;
                        objVehicleMaintTypes.UpdatedBy = usrInst.Username;
                        objVehicleMaintTypes.Save(objVehicleMaintTypes, sConnStr);
                    }
                    InsertDBUpdate(strUpd, "Odo and Hr Meter and GFI_GPS_DriversDriving");
                }
                #endregion
                #region Update 29. Vehicle Maint SP & Process Table
                strUpd = "Rez000029";
                if (!CheckUpdateExist(strUpd))
                {
                    # region NEW MAINTENANCE TYPES

                    //VEHICLE INSPECTION (FITNESS) 6 MONTHS
                    VehicleMaintTypes objVMT = new VehicleMaintTypes();
                    objVMT = new VehicleMaintTypes();
                    objVMT = objVMT.GetVehicleMaintTypesByDescription("VEHICLE INSPECTION", sConnStr);
                    if (objVMT == null)
                    {
                        objVMT = new VehicleMaintTypes();
                        objVMT.MaintCatId_cbo = 2;
                        objVMT.Description = "VEHICLE INSPECTION - 6 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 6;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        objVMT.Save(objVMT, sConnStr);
                    }
                    else
                    {
                        objVMT.MaintCatId_cbo = 2;
                        objVMT.Description = "VEHICLE INSPECTION - 6 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 6;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        objVMT.Update(objVMT, sConnStr);
                    }

                    //VEHICLE INSPECTION (FITNESS) 12 MONTHS
                    objVMT = new VehicleMaintTypes();
                    objVMT = objVMT.GetVehicleMaintTypesByDescription("VEHICLE INSPECTION - 12 MONTHS", sConnStr);
                    if (objVMT == null)
                    {
                        objVMT = new VehicleMaintTypes();
                        objVMT.MaintCatId_cbo = 2;
                        objVMT.Description = "VEHICLE INSPECTION - 12 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 12;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        objVMT.Save(objVMT, sConnStr);
                    }

                    //VEHICLE INSPECTION (FITNESS) 24 MONTHS
                    objVMT = new VehicleMaintTypes();
                    objVMT = objVMT.GetVehicleMaintTypesByDescription("VEHICLE INSPECTION - 24 MONTHS", sConnStr);
                    if (objVMT == null)
                    {
                        objVMT = new VehicleMaintTypes();
                        objVMT.MaintCatId_cbo = 2;
                        objVMT.Description = "VEHICLE INSPECTION - 24 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 24;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        objVMT.Save(objVMT, sConnStr);
                    }

                    //VEHICLE REGISTRATION (ROAD TAX)  3 MONTHS
                    objVMT = new VehicleMaintTypes();
                    objVMT = objVMT.GetVehicleMaintTypesByDescription("VEHICLE REGISTRATION - 3 MONTHS", sConnStr);
                    if (objVMT == null)
                    {
                        objVMT = new VehicleMaintTypes();
                        objVMT.MaintCatId_cbo = 3;
                        objVMT.Description = "VEHICLE REGISTRATION - 3 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 3;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        objVMT.Save(objVMT, sConnStr);
                    }

                    //VEHICLE REGISTRATION (ROAD TAX)  6 MONTHS
                    objVMT = objVMT.GetVehicleMaintTypesByDescription("VEHICLE REGISTRATION - 6 MONTHS", sConnStr);
                    if (objVMT == null)
                    {
                        objVMT = new VehicleMaintTypes();
                        objVMT.MaintCatId_cbo = 3;
                        objVMT.Description = "VEHICLE REGISTRATION - 6 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 6;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        objVMT.Save(objVMT, sConnStr);
                    }

                    //VEHICLE REGISTRATION (ROAD TAX)  12 MONTHS
                    objVMT = objVMT.GetVehicleMaintTypesByDescription("VEHICLE REGISTRATION - 12 MONTHS", sConnStr);
                    if (objVMT == null)
                    {
                        objVMT = new VehicleMaintTypes();
                        objVMT.MaintCatId_cbo = 3;
                        objVMT.Description = "VEHICLE REGISTRATION - 12 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 12;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        objVMT.Save(objVMT, sConnStr);
                    }
                    else
                    {
                        objVMT.MaintCatId_cbo = 3;
                        objVMT.Description = "VEHICLE REGISTRATION - 12 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 12;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        objVMT.Update(objVMT, sConnStr);
                    }

                    //SERVICING - 5000 KM
                    objVMT = objVMT.GetVehicleMaintTypesByDescription("SERVICING - 5000 KM", sConnStr);
                    if (objVMT != null)
                    {
                        objVMT.OccurrenceKM = 5000;
                        objVMT.OccurrenceKMTh = 500;
                        objVMT.UpdatedDate = DateTime.Now; ;
                        objVMT.Update(objVMT, sConnStr);
                    }

                    //SERVICING - 10000 KM OR 6 MONTHS
                    objVMT = new VehicleMaintTypes();
                    objVMT = objVMT.GetVehicleMaintTypesByDescription("SERVICING - 10000 KM OR 6 MONTHS", sConnStr);
                    if (objVMT == null)
                    {
                        objVMT = new VehicleMaintTypes();
                        objVMT.MaintCatId_cbo = 1;
                        objVMT.Description = "SERVICING - 10000 KM OR 6 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 6;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 10000;
                        objVMT.OccurrenceKMTh = 9500;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        objVMT.Save(objVMT, sConnStr);
                    }
                    else
                    {
                        objVMT.MaintCatId_cbo = 1;
                        objVMT.Description = "SERVICING - 10000 KM OR 6 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 6;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 10000;
                        objVMT.OccurrenceKMTh = 9500;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        objVMT.Update(objVMT, sConnStr);
                    }

                    //INSURANCE - 6 MONTHS
                    objVMT = new VehicleMaintTypes();
                    objVMT = objVMT.GetVehicleMaintTypesByDescription("INSURANCE - 6 MONTHS", sConnStr);
                    if (objVMT == null)
                    {
                        objVMT = new VehicleMaintTypes();
                        objVMT.MaintCatId_cbo = 4;
                        objVMT.Description = "INSURANCE - 6 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 6;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        objVMT.Save(objVMT, sConnStr);
                    }
                    else
                    {
                        objVMT.MaintCatId_cbo = 4;
                        objVMT.Description = "INSURANCE - 6 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 6;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        objVMT.Update(objVMT, sConnStr);
                    }

                    //INSURANCE - 12 MONTHS
                    objVMT = new VehicleMaintTypes();
                    objVMT = objVMT.GetVehicleMaintTypesByDescription("INSURANCE - 12 MONTHS", sConnStr);
                    if (objVMT == null)
                    {
                        objVMT = new VehicleMaintTypes();
                        objVMT.MaintCatId_cbo = 4;
                        objVMT.Description = "INSURANCE - 12 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 12;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        objVMT.Save(objVMT, sConnStr);
                    }
                    else
                    {
                        objVMT.MaintCatId_cbo = 4;
                        objVMT.Description = "INSURANCE - 12 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 12;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        objVMT.Update(objVMT, sConnStr);
                    }
                    #endregion


                    sql = @"CREATE TABLE GFI_GPS_ProcessPending(
	                        iID int AUTO_INCREMENT NOT NULL,
	                        AssetID int NOT NULL,
	                        dtDateFrom datetime NOT NULL DEFAULT NOW(),
	                        ProcessCode nvarchar (50) NULL,
                            Processing int  NOT NULL DEFAULT 0,
                         CONSTRAINT PK_ProcessPending PRIMARY KEY  
	                        (iID  ASC)
                        )";
                    if (GetDataDT(ExistTableSql("GFI_GPS_ProcessPending")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_GPS_ProcessPending  
	                        ADD CONSTRAINT FK_GFI_GPS_ProcessPending_GFI_FLT_Asset  FOREIGN KEY(AssetID )
                        REFERENCES GFI_FLT_Asset (AssetID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_ProcessPending_GFI_FLT_Asset", "GFI_GPS_ProcessPending")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "DROP PROCEDURE RegisterDevicesForProcessing";
                    if (GetDataDT(ExistsSP("RegisterDevicesForProcessing")).Rows.Count > 0)
                        _SqlConn.ScriptExecute(sql, sConnStr);
                    sql = @"DELIMITER //
                            CREATE Procedure RegisterDevicesForProcessing 
                            (
                                p_AssetID int,
                                p_dtDateFrom datetime(3),
                                p_ProcessCode nvarchar (50)
                            )
                            Begin
                                if exists (select * from GFI_GPS_ProcessPending where AssetID = p_AssetID and ProcessCode = p_ProcessCode and Processing = 0) then
	                                if(p_dtDateFrom < (select dtDateFrom from GFI_GPS_ProcessPending where AssetID = p_AssetID and ProcessCode = p_ProcessCode and Processing = 0)) then
		                                update GFI_GPS_ProcessPending set dtDateFrom = p_dtDateFrom where AssetID = p_AssetID and ProcessCode = p_ProcessCode and Processing = 0;
                            	     end if;
                                else
	                                insert GFI_GPS_ProcessPending (AssetID, dtDateFrom, ProcessCode)
		                                select (p_AssetID, p_dtDateFrom, p_ProcessCode);
		                          end if;
                            end;
                            //
                            delimiter ;";
                    if (GetDataDT(ExistsSP("RegisterDevicesForProcessing")).Rows.Count == 0)
                        _SqlConn.ScriptExecute(sql, sConnStr);

                    sql = @"CREATE TABLE GFI_SYS_Ticker(
                         Id int AUTO_INCREMENT NOT NULL,
                         TickDate datetime NULL,
                         Message nvarchar(4000) NULL,
                         CreatedBy nchar(255) NULL,
                         AuthorizedBy nchar(255) NULL,
                         CONSTRAINT PK_GFI_SYS_Ticker PRIMARY KEY (Id ASC)
                        )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_Ticker")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "Vehicle Maintenance Bulk Assign SP and Process Table");
                }
                #endregion
                #region Update 30. Maint Ref and Vat
                strUpd = "Rez000030";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_AMM_VehicleMaintenance DROP FOREIGN KEY FK_GFI_AMM_VehicleMaintenance_GFI_AMM_Asset_ExtPro_Vehicles";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintenance_GFI_AMM_Asset_ExtPro_Vehicles", "GFI_AMM_VehicleMaintenance")).Rows.Count > 0)
                        dbExecute(sql);
                    sql = "ALTER TABLE GFI_AMM_VehicleMaintenance DROP FOREIGN KEY FK_GFI_AMM_VehicleMaint_GFI_AMM_VehicleMaintStatus";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaint_GFI_AMM_VehicleMaintStatus", "GFI_AMM_VehicleMaintenance")).Rows.Count > 0)
                        dbExecute(sql);
                    sql = "ALTER TABLE GFI_AMM_VehicleMaintenance DROP FOREIGN KEY FK_GFI_AMM_VehicleMaintenance_GFI_AMM_VehicleMaintTypes";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintenance_GFI_AMM_VehicleMaintTypes", "GFI_AMM_VehicleMaintenance")).Rows.Count > 0)
                        dbExecute(sql);

                    sql = @"
                    CREATE TABLE Tmp_GFI_AMM_VehicleMaintenance
	                    (
	                    URI int NOT NULL AUTO_INCREMENT,
	                    AssetId int NULL,
	                    MaintTypeId_cbo int NULL,
	                    CompanyName nvarchar(255) NULL,
	                    PhoneNumber nvarchar(120) NULL,
	                    CompanyRef nvarchar(120) NULL,
	                    CompanyRef2 nvarchar(120) NULL,
	                    MaintDescription nvarchar(512) NULL,
	                    StartDate datetime NULL,
	                    EndDate datetime NULL,
	                    MaintStatusId_cbo int NULL,
	                    EstimatedValue numeric(18, 2) NULL,
	                    VATInclInItemsAmt nchar(1) NULL,
	                    VATAmount numeric(18, 2) NULL,
	                    TotalCost numeric(18, 2) NULL,
	                    CoverTypeId_cbo int NULL,
	                    CalculatedOdometer int NULL,
	                    ActualOdometer int NULL,
	                    CalculatedEngineHrs int NULL,
	                    ActualEngineHrs int NULL,
	                    AdditionalInfo nvarchar(500) NULL,
	                    CreatedDate datetime NULL,
	                    CreatedBy nvarchar(30) NULL,
	                    UpdatedDate datetime NULL,
	                    UpdatedBy nvarchar(30) NULL,
                        CONSTRAINT PK_GFI_AMM_VehicleMaint PRIMARY KEY (URI ASC)
	                    ) ; 

	                     INSERT INTO Tmp_GFI_AMM_VehicleMaintenance (URI, AssetId, MaintTypeId_cbo, CompanyName, PhoneNumber, CompanyRef, MaintDescription, StartDate, EndDate, MaintStatusId_cbo, EstimatedValue, TotalCost, CoverTypeId_cbo, CalculatedOdometer, ActualOdometer, CalculatedEngineHrs, ActualEngineHrs, AdditionalInfo, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy)
		                    SELECT URI, AssetId, MaintTypeId_cbo, CompanyName, PhoneNumber, CompanyRef, MaintDescription, StartDate, EndDate, MaintStatusId_cbo, EstimatedValue, TotalCost, CoverTypeId_cbo, CalculatedOdometer, ActualOdometer, CalculatedEngineHrs, ActualEngineHrs, AdditionalInfo, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy FROM GFI_AMM_VehicleMaintenance;

                    ALTER TABLE GFI_AMM_VehicleMaintItems
	                    DROP FOREIGN KEY FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance;

                    DROP TABLE GFI_AMM_VehicleMaintenance;

                    RENAME TABLE Tmp_GFI_AMM_VehicleMaintenance To GFI_AMM_VehicleMaintenance;

                    CREATE  INDEX IX_ActualCompletionDate ON GFI_AMM_VehicleMaintenance(EndDate);

                    CREATE  INDEX IX_ActualStartDate ON GFI_AMM_VehicleMaintenance(StartDate);

                    CREATE  INDEX IX_AssetId ON GFI_AMM_VehicleMaintenance(AssetId);

                    CREATE  INDEX IX_CompanyName ON GFI_AMM_VehicleMaintenance(CompanyName);

                    CREATE  INDEX IX_CoverTypeId ON GFI_AMM_VehicleMaintenance(CoverTypeId_cbo);
                    CREATE  INDEX IX_EndDate ON GFI_AMM_VehicleMaintenance(EndDate);

                    CREATE  INDEX IX_MaintStatusId ON GFI_AMM_VehicleMaintenance(MaintStatusId_cbo);

                    CREATE  INDEX IX_MaintTypeId ON GFI_AMM_VehicleMaintenance(MaintTypeId_cbo);

                    CREATE  INDEX IX_StartDate ON GFI_AMM_VehicleMaintenance(StartDate);

                    ALTER TABLE GFI_AMM_VehicleMaintenance ADD CONSTRAINT
	                    FK_GFI_AMM_VehicleMaintenance_GFI_AMM_VehicleMaintTypes FOREIGN KEY
	                    (
	                    MaintTypeId_cbo
	                    ) REFERENCES GFI_AMM_VehicleMaintTypes
	                    (
	                    MaintTypeId
	                    ) ON UPDATE  NO ACTION 
	                     ON DELETE  NO ACTION ;
	
                    ALTER TABLE GFI_AMM_VehicleMaintenance ADD CONSTRAINT
	                    FK_GFI_AMM_VehicleMaint_GFI_AMM_VehicleMaintStatus FOREIGN KEY
	                    (
	                    MaintStatusId_cbo
	                    ) REFERENCES GFI_AMM_VehicleMaintStatus
	                    (
	                    MaintStatusId
	                    ) ON UPDATE  NO ACTION 
	                     ON DELETE  NO ACTION ;

                    ALTER TABLE GFI_AMM_VehicleMaintenance ADD CONSTRAINT
	                    FK_GFI_AMM_VehicleMaintenance_GFI_AMM_Asset_ExtPro_Vehicles FOREIGN KEY
	                    (
	                    AssetId
	                    ) REFERENCES GFI_AMM_AssetExtProVehicles
	                    (
	                    AssetId
	                    ) ON UPDATE  NO ACTION 
	                     ON DELETE  NO ACTION ;
	

                    ALTER TABLE GFI_AMM_VehicleMaintItems ADD CONSTRAINT
	                    FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance FOREIGN KEY
	                    (
	                    MaintURI
	                    ) REFERENCES GFI_AMM_VehicleMaintenance
	                    (
	                    URI
	                    ) ON UPDATE  NO ACTION 
	                     ON DELETE  NO ACTION ;
                        ";
                    dbExecute(sql);

                    sql = @"
DELIMITER //
	DROP PROCEDURE IF EXISTS sp_AMM_VehicleMaint_CreateChildRecords;
	CREATE PROCEDURE sp_AMM_VehicleMaint_CreateChildRecords
	(
	   iAssetId int,
	   iMaintTypeId int
	)
	BEGIN
      select @VMTLURI := (SELECT MAX(URI) FROM GFI_AMM_VehicleMaintTypesLink  WHERE AssetId = @iAssetId AND MaintTypeId = iMaintTypeId);
      select @iMaintStatusId := (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'To Schedule');
      select @NextMaintDate :=  (SELECT NextMaintDate FROM GFI_AMM_VehicleMaintTypesLink WHERE URI = @VMTLURI);

      IF (@NextMaintDate IS NULL) then
			set @NextMaintDate = NOW() + INTERVAL 1 DAY;
		end if;

      INSERT GFI_AMM_VehicleMaintenance (AssetId, MaintTypeId_cbo, MaintStatusId_cbo, StartDate, EndDate, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy) 
      VALUES (@iAssetId, @iMaintTypeId, @iMaintStatusId, @NextMaintDate, @NextMaintDate, NOW(), CURRENT_USER, NOW(), CURRENT_USER);

      select @iMaintURI = (SELECT LAST_INSERT_ID());

      UPDATE GFI_AMM_VehicleMaintTypesLink
      	SET NextMaintDate = @NextMaintDate, Status = @iMaintStatusId, MaintURI = @iMaintURI
      WHERE URI = @VMTLURI;
	END //
DELIMITER ;";
                    _SqlConn.ScriptExecute(sql, sConnStr);

                    InsertDBUpdate(strUpd, "Maint Ref and Vat");
                }
                #endregion
                #region Update 31. Time zone
                strUpd = "Rez000031";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "update GFI_FLT_Asset set TimeZoneID = 'Arabian Standard Time' where TimeZoneID = ''";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "Time zone updated");
                }
                #endregion
                #region Update 32. MaintType Group Matrix
                strUpd = "Rez000032";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_SYS_GroupMatrixMaintType
                        (
	                        MID INT AUTO_INCREMENT NOT NULL
	                        , GMID INT NOT NULL
	                        , iID INT NOT NULL
                            , CONSTRAINT PK_GFI_SYS_GroupMatrixMaintType PRIMARY KEY  (MID ASC)
                        )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrixMaintType")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixMaintType 
                            ADD CONSTRAINT FK_GFI_SYS_GroupMatrixMaintType_GFI_SYS_GroupMatrix 
                                FOREIGN KEY (GMID) 
                                REFERENCES GFI_SYS_GroupMatrix (GMID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixMaintType_GFI_SYS_GroupMatrix", "GFI_SYS_GroupMatrixMaintType")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixMaintType 
                            ADD CONSTRAINT FK_GFI_SYS_GroupMatrixRule_GFI_AMM_VehicleMaintTypes 
                                FOREIGN KEY(iID) 
                                REFERENCES GFI_AMM_VehicleMaintTypes(MaintTypeId) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixMaintType_GFI_SYS_GroupMatrix", "GFI_SYS_GroupMatrixMaintType")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "MaintType Group Matrix");
                }
                #endregion
                #region Update 33. Trip Process index
                strUpd = "Rez000033";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE INDEX IX_GFI_GPS_GPSData_ProcessTrips ON GFI_GPS_GPSData 
                            (
	                            AssetID ASC,
	                            DateTimeGPS_UTC ASC,
	                            LongLatValidFlag ASC,
	                            StopFlag ASC
                            )";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSData", "IX_GFI_GPS_GPSData_ProcessTrips")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "Trip Process index");
                }
                #endregion
                #region Update 34. Reconfigure Vehicle Maint Cat
                strUpd = "Rez000034";
                if (!CheckUpdateExist(strUpd))
                {
                    //Maint Cat - Insurance renamed to Renewals
                    VehicleMaintCat objVehicleMaintCat = new VehicleMaintCat();
                    objVehicleMaintCat = objVehicleMaintCat.GetVehicleMaintCatByDescription("INSURANCE", sConnStr);
                    if (objVehicleMaintCat != null)
                    {
                        objVehicleMaintCat = new VehicleMaintCat();
                        objVehicleMaintCat.MaintCatId = 4;
                        objVehicleMaintCat.Description = "RENEWALS";
                        objVehicleMaintCat.UpdatedDate = DateTime.Now;
                        objVehicleMaintCat.UpdatedBy = usrInst.Username;
                        objVehicleMaintCat.Update(objVehicleMaintCat, sConnStr);
                    }

                    InsertDBUpdate(strUpd, "Reconfigure Vehicle Maint Cat");
                }
                #endregion
                #region Update 35. Reconfigure Vehicle Maint Types
                strUpd = "Rez000035";
                if (!CheckUpdateExist(strUpd))
                {
                    //VEHICLE INSPECTION (FITNESS) 6 MONTHS
                    VehicleMaintTypes objVMT = new VehicleMaintTypes();
                    objVMT = new VehicleMaintTypes();
                    objVMT = objVMT.GetVehicleMaintTypesByDescription("VEHICLE INSPECTION - 6 MONTHS", sConnStr);
                    if (objVMT != null)
                    {
                        objVMT.MaintCatId_cbo = 4;
                        objVMT.Description = "FITNESS - 6 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 6;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        objVMT.Update(objVMT, sConnStr);
                    }

                    //VEHICLE INSPECTION (FITNESS) 12 MONTHS
                    objVMT = new VehicleMaintTypes();
                    objVMT = objVMT.GetVehicleMaintTypesByDescription("VEHICLE INSPECTION - 12 MONTHS", sConnStr);
                    if (objVMT != null)
                    {
                        objVMT.MaintCatId_cbo = 4;
                        objVMT.Description = "FITNESS - 12 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 12;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        objVMT.Update(objVMT, sConnStr);
                    }

                    //VEHICLE INSPECTION (FITNESS) 24 MONTHS
                    objVMT = new VehicleMaintTypes();
                    objVMT = objVMT.GetVehicleMaintTypesByDescription("VEHICLE INSPECTION - 24 MONTHS", sConnStr);
                    if (objVMT != null)
                    {
                        objVMT.MaintCatId_cbo = 4;
                        objVMT.Description = "FITNESS - 24 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 24;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        objVMT.Update(objVMT, sConnStr);
                    }

                    //VEHICLE REGISTRATION (ROAD TAX)  3 MONTHS
                    objVMT = new VehicleMaintTypes();
                    objVMT = objVMT.GetVehicleMaintTypesByDescription("VEHICLE REGISTRATION - 3 MONTHS", sConnStr);
                    if (objVMT != null)
                    {
                        objVMT.MaintCatId_cbo = 4;
                        objVMT.Description = "VEHICLE REGISTRATION - 3 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 3;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        objVMT.Update(objVMT, sConnStr);
                    }

                    //VEHICLE REGISTRATION (ROAD TAX)  6 MONTHS
                    objVMT = new VehicleMaintTypes();
                    objVMT = objVMT.GetVehicleMaintTypesByDescription("VEHICLE REGISTRATION - 6 MONTHS", sConnStr);
                    if (objVMT != null)
                    {
                        objVMT.MaintCatId_cbo = 4;
                        objVMT.Description = "VEHICLE REGISTRATION - 6 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 6;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        objVMT.Update(objVMT, sConnStr);
                    }

                    //VEHICLE REGISTRATION (ROAD TAX)  12 MONTHS
                    objVMT = new VehicleMaintTypes();
                    objVMT = objVMT.GetVehicleMaintTypesByDescription("VEHICLE REGISTRATION - 12 MONTHS", sConnStr);
                    if (objVMT != null)
                    {
                        objVMT.MaintCatId_cbo = 4;
                        objVMT.Description = "VEHICLE REGISTRATION - 12 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 12;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        objVMT.Update(objVMT, sConnStr);
                    }

                    //Re-categorize VEHICLE WASH as UNPLANNED MAINTENANCE
                    objVMT = new VehicleMaintTypes();
                    objVMT = objVMT.GetVehicleMaintTypesByDescription("VEHICLE WASH", sConnStr);
                    if (objVMT != null)
                    {
                        VehicleMaintCat objMaintCat = new VehicleMaintCat();
                        objMaintCat = objMaintCat.GetVehicleMaintCatByDescription("UNPLANNED MAINTENANCE", sConnStr);
                        if (objMaintCat != null)
                        {
                            objVMT.MaintCatId_cbo = objMaintCat.MaintCatId;
                            objVMT.Update(objVMT, sConnStr);
                        }
                    }

                    //Delete Maint Cat INSPECTION]
                    VehicleMaintCat objVehicleMaintCat = new VehicleMaintCat();
                    objVehicleMaintCat = objVehicleMaintCat.GetVehicleMaintCatByDescription("INSPECTION", sConnStr);
                    if (objVehicleMaintCat != null)
                    {
                        objVehicleMaintCat.Delete(objVehicleMaintCat, sConnStr);
                    }

                    //Delete Maint Cat REGISTRATION]
                    objVehicleMaintCat = new VehicleMaintCat();
                    objVehicleMaintCat = objVehicleMaintCat.GetVehicleMaintCatByDescription("REGISTRATION", sConnStr);
                    if (objVehicleMaintCat != null)
                    {
                        objVehicleMaintCat.Delete(objVehicleMaintCat, sConnStr);
                    }

                    InsertDBUpdate(strUpd, "Reconfigure Vehicle Maint Types");
                }
                #endregion
                #region Update 36. Asset Constraints
                strUpd = "Rez000036";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_FLT_AssetDeviceMap DROP FOREIGN KEY FK_GFI_FLT_AssetDeviceMap_GFI_FLT_Asset";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_AssetDeviceMap_GFI_FLT_Asset", "GFI_FLT_AssetDeviceMap")).Rows.Count > 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_FLT_AssetDeviceMap 
                            ADD CONSTRAINT FK_GFI_FLT_AssetDeviceMap_GFI_FLT_Asset 
                                FOREIGN KEY(AssetID) 
                                REFERENCES GFI_FLT_Asset(AssetID) 
                             ON UPDATE  NO ACTION 
	                         ON DELETE  Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_AssetDeviceMap_GFI_FLT_Asset", "GFI_FLT_AssetDeviceMap")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_GPS_TripHeader DROP FOREIGN KEY FK_GFI_GPS_TripHeader_GFI_FLT_Asset";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_TripHeader_GFI_FLT_Asset", "GFI_GPS_TripHeader")).Rows.Count > 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_GPS_TripHeader 
                            ADD CONSTRAINT FK_GFI_GPS_TripHeader_GFI_FLT_Asset 
                                FOREIGN KEY(AssetID) 
                                REFERENCES GFI_FLT_Asset(AssetID) 
                            ON UPDATE  NO ACTION 
	                        ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_TripHeader_GFI_FLT_Asset", "GFI_GPS_TripHeader")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_GPS_GPSData DROP FOREIGN KEY FK_GFI_GPS_GPSData_GFI_FLT_Asset";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_GPSData_GFI_FLT_Asset", "GFI_GPS_GPSData")).Rows.Count > 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_GPS_GPSData 
                            ADD CONSTRAINT FK_GFI_GPS_GPSData_GFI_FLT_Asset 
                                FOREIGN KEY(AssetID) 
                                REFERENCES GFI_FLT_Asset(AssetID) 
                            ON UPDATE  NO ACTION 
	                        ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_GPSData_GFI_FLT_Asset", "GFI_GPS_GPSData")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_GPS_Exceptions DROP FOREIGN KEY FK_GFI_GPS_Exceptions_GFI_FLT_Asset";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_Exceptions_GFI_FLT_Asset", "GFI_GPS_Exceptions")).Rows.Count > 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_GPS_Exceptions 
                            ADD CONSTRAINT FK_GFI_GPS_Exceptions_GFI_FLT_Asset 
                                FOREIGN KEY(AssetID) 
                                REFERENCES GFI_FLT_Asset(AssetID) 
                            ON UPDATE  NO ACTION 
	                        ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_Exceptions_GFI_FLT_Asset", "GFI_GPS_Exceptions")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintenance
                            DROP FOREIGN KEY FK_GFI_AMM_VehicleMaintenance_GFI_AMM_Asset_ExtPro_Vehicles";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintenance_GFI_AMM_Asset_ExtPro_Vehicles", "GFI_AMM_VehicleMaintenance")).Rows.Count > 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintTypesLink
	                        DROP FOREIGN KEY FK_GFI_AMM_VehicleMaintTypesLink_GFI_AMM_AssetExtProVehicles";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintTypesLink_GFI_AMM_AssetExtProVehicles", "GFI_AMM_VehicleMaintTypesLink")).Rows.Count > 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintItems
	                        DROP FOREIGN KEY FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance", "GFI_AMM_VehicleMaintItems")).Rows.Count > 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintenance ADD CONSTRAINT
	                        FK_GFI_AMM_VehicleMaintenance_GFI_AMM_Asset_ExtPro_Vehicles FOREIGN KEY
	                        (
	                        AssetId
	                        ) REFERENCES GFI_AMM_AssetExtProVehicles
	                        (
	                        AssetId
	                        ) ON UPDATE  NO ACTION 
	                         ON DELETE  CASCADE";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintenance_GFI_AMM_Asset_ExtPro_Vehicles", "GFI_AMM_VehicleMaintenance")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintItems ADD CONSTRAINT
	                        FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance FOREIGN KEY
	                        (
	                        MaintURI
	                        ) REFERENCES GFI_AMM_VehicleMaintenance
	                        (
	                        URI
	                        ) ON UPDATE  NO ACTION 
	                         ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance", "GFI_AMM_VehicleMaintItems")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintTypesLink ADD CONSTRAINT
	                        FK_GFI_AMM_VehicleMaintTypesLink_GFI_AMM_AssetExtProVehicles FOREIGN KEY
	                        (
	                        AssetId
	                        ) REFERENCES GFI_AMM_AssetExtProVehicles
	                        (
	                        AssetId
	                        ) ON UPDATE  NO ACTION 
	                         ON DELETE  CASCADE";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintTypesLink_GFI_AMM_AssetExtProVehicles", "GFI_AMM_VehicleMaintTypesLink")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_AMM_AssetExtProVehicles
	                        DROP FOREIGN KEY FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_Asset";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_Asset", "GFI_AMM_AssetExtProVehicles")).Rows.Count > 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_AMM_AssetExtProVehicles ADD CONSTRAINT
	                        FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_Asset FOREIGN KEY
	                        (
	                        AssetId
	                        ) REFERENCES GFI_FLT_Asset
	                        (
	                        AssetID
	                        ) ON UPDATE  NO ACTION 
	                            ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_Asset", "GFI_AMM_AssetExtProVehicles")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "update GFI_FLT_Asset set Status_b = 'AC' where Status_b = ''";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "Asset Constraints");
                }
                #endregion
                #region Update 37. Odo and Hr Meter
                strUpd = "Rez000037";
                if (!CheckUpdateExist(strUpd))
                {
                    InsertDBUpdate(strUpd, "Get Odometer & Engine Hours functions");
                }
                #endregion
                #region Update 38. Add field ContactDetails to table GFI_AMM_VehicleMaintenance
                strUpd = "Rez000038";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_AMM_VehicleMaintenance DROP FOREIGN KEY FK_GFI_AMM_VehicleMaintenance_GFI_AMM_Asset_ExtPro_Vehicles";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintenance_GFI_AMM_Asset_ExtPro_Vehicles", "GFI_AMM_VehicleMaintenance")).Rows.Count > 0)
                        dbExecute(sql);
                    sql = "ALTER TABLE GFI_AMM_VehicleMaintenance DROP FOREIGN KEY FK_GFI_AMM_VehicleMaint_GFI_AMM_VehicleMaintStatus";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaint_GFI_AMM_VehicleMaintStatus", "GFI_AMM_VehicleMaintenance")).Rows.Count > 0)
                        dbExecute(sql);
                    sql = "ALTER TABLE GFI_AMM_VehicleMaintenance DROP FOREIGN KEY FK_GFI_AMM_VehicleMaintenance_GFI_AMM_VehicleMaintTypes";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintenance_GFI_AMM_VehicleMaintTypes", "GFI_AMM_VehicleMaintenance")).Rows.Count > 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_AMM_VehicleMaintItems DROP FOREIGN KEY FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance", "GFI_AMM_VehicleMaintItems")).Rows.Count > 0)
                        dbExecute(sql);

                    sql = @"
                    CREATE TABLE Tmp_GFI_AMM_VehicleMaintenance
	                    (
	                    URI int NOT NULL AUTO_INCREMENT,
	                    AssetId int NULL,
	                    MaintTypeId_cbo int NULL,
	                    ContactDetails nvarchar(120) NULL,
	                    CompanyName nvarchar(255) NULL,
	                    PhoneNumber nvarchar(120) NULL,
	                    CompanyRef nvarchar(120) NULL,
	                    CompanyRef2 nvarchar(120) NULL,
	                    MaintDescription nvarchar(512) NULL,
	                    StartDate datetime NULL,
	                    EndDate datetime NULL,
	                    MaintStatusId_cbo int NULL,
	                    EstimatedValue numeric(18, 2) NULL,
	                    VATInclInItemsAmt nchar(1) NULL,
	                    VATAmount numeric(18, 2) NULL,
	                    TotalCost numeric(18, 2) NULL,
	                    CoverTypeId_cbo int NULL,
	                    CalculatedOdometer int NULL,
	                    ActualOdometer int NULL,
	                    CalculatedEngineHrs int NULL,
	                    ActualEngineHrs int NULL,
	                    AdditionalInfo nvarchar(500) NULL,
	                    CreatedDate datetime NULL,
	                    CreatedBy nvarchar(30) NULL,
	                    UpdatedDate datetime NULL,
	                    UpdatedBy nvarchar(30) NULL,
                        CONSTRAINT PK_Tmp_GFI_AMM_VehicleMaintenance PRIMARY KEY (URI ASC)
	                    );

	                     INSERT INTO Tmp_GFI_AMM_VehicleMaintenance (URI, AssetId, MaintTypeId_cbo, CompanyName, PhoneNumber, CompanyRef, CompanyRef2, MaintDescription, StartDate, EndDate, MaintStatusId_cbo, EstimatedValue, VATInclInItemsAmt, VATAmount, TotalCost, CoverTypeId_cbo, CalculatedOdometer, ActualOdometer, CalculatedEngineHrs, ActualEngineHrs, AdditionalInfo, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy)
		                    SELECT URI, AssetId, MaintTypeId_cbo, CompanyName, PhoneNumber, CompanyRef, CompanyRef2, MaintDescription, StartDate, EndDate, MaintStatusId_cbo, EstimatedValue, VATInclInItemsAmt, VATAmount, TotalCost, CoverTypeId_cbo, CalculatedOdometer, ActualOdometer, CalculatedEngineHrs, ActualEngineHrs, AdditionalInfo, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy FROM GFI_AMM_VehicleMaintenance ;

                    DROP TABLE GFI_AMM_VehicleMaintenance;

                    RENAME Table Tmp_GFI_AMM_VehicleMaintenance TO GFI_AMM_VehicleMaintenance;

                    CREATE  INDEX IX_ActualCompletionDate ON GFI_AMM_VehicleMaintenance
	                    (
	                    EndDate
	                    );

                    CREATE  INDEX IX_ActualStartDate ON GFI_AMM_VehicleMaintenance
	                    (
	                    StartDate
	                    );

                    CREATE  INDEX IX_AssetId ON GFI_AMM_VehicleMaintenance
	                    (
	                    AssetId
	                    );

                    CREATE  INDEX IX_CompanyName ON GFI_AMM_VehicleMaintenance
	                    (
	                    CompanyName
	                    );

                    CREATE  INDEX IX_CoverTypeId ON GFI_AMM_VehicleMaintenance
	                    (
	                    CoverTypeId_cbo
	                    );

                    CREATE  INDEX IX_EndDate ON GFI_AMM_VehicleMaintenance
	                    (
	                    EndDate
	                    );

                    CREATE  INDEX IX_MaintStatusId ON GFI_AMM_VehicleMaintenance
	                    (
	                    MaintStatusId_cbo
	                    );

                    CREATE  INDEX IX_MaintTypeId ON GFI_AMM_VehicleMaintenance
	                    (
	                    MaintTypeId_cbo
	                    );

                    CREATE  INDEX IX_StartDate ON GFI_AMM_VehicleMaintenance
	                    (
	                    StartDate
	                    );

                    ALTER TABLE GFI_AMM_VehicleMaintenance ADD CONSTRAINT
	                    FK_GFI_AMM_VehicleMaintenance_GFI_AMM_VehicleMaintTypes FOREIGN KEY
	                    (
	                    MaintTypeId_cbo
	                    ) REFERENCES GFI_AMM_VehicleMaintTypes
	                    (
	                    MaintTypeId
	                    ) ON UPDATE  NO ACTION 
	                     ON DELETE  NO ACTION ;

                    ALTER TABLE GFI_AMM_VehicleMaintenance ADD CONSTRAINT
	                    FK_GFI_AMM_VehicleMaint_GFI_AMM_VehicleMaintStatus FOREIGN KEY
	                    (
	                    MaintStatusId_cbo
	                    ) REFERENCES GFI_AMM_VehicleMaintStatus
	                    (
	                    MaintStatusId
	                    ) ON UPDATE  NO ACTION 
	                     ON DELETE  NO ACTION ;

                    ALTER TABLE GFI_AMM_VehicleMaintenance ADD CONSTRAINT
	                    FK_GFI_AMM_VehicleMaintenance_GFI_AMM_Asset_ExtPro_Vehicles FOREIGN KEY
	                    (
	                    AssetId
	                    ) REFERENCES GFI_AMM_AssetExtProVehicles
	                    (
	                    AssetId
	                    ) ON UPDATE  NO ACTION 
	                     ON DELETE  CASCADE; 
	
                    ALTER TABLE GFI_AMM_VehicleMaintItems ADD CONSTRAINT
	                    FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance FOREIGN KEY
	                    (
	                    MaintURI
	                    ) REFERENCES GFI_AMM_VehicleMaintenance
	                    (
	                    URI
	                    ) ON UPDATE  NO ACTION 
	                     ON DELETE  CASCADE ;";

                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "Add field ContactDetails to table GFI_AMM_VehicleMaintenance");
                }
                #endregion
                #region Update 39. GFI_FLT_AutoReportingConfig
                strUpd = "Rez000039";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_FLT_AutoReportingConfig](
                            ARID] int] AUTO_INCREMENT NOT NULL,
                            TargetID] int] NULL,
                            TargetType] nvarchar](20) NULL,
                            ReportID] int] NULL,
                            ReportPeriod] int] NULL,
                            TriggerTime] nvarchar](50) NULL,
                            TriggerInterval] nvarchar](50) NULL,
                            CreatedBy] nchar](10) NOT NULL,
                            CONSTRAINT PK_GFI_FLT_AutoReportingConfig] PRIMARY KEY  
                        (
                            ARID] ASC
                        )
                        )";

                    if (GetDataDT(ExistTableSql("GFI_FLT_AutoReportingConfig")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_FLT_AutoReprotingConfigDetails](
                             ARDID] int] AUTO_INCREMENT NOT NULL,
                             ARID] int] NOT NULL,
                             UID] int] NOT NULL,
                             UIDType] nvarchar](50) NULL,
                             UIDCategory] nvarchar](50) NULL,
                             CONSTRAINT PK_GFI_FLT_AutoReprotingConfigDetails_1] PRIMARY KEY  
                            (
                             ARDID] ASC
                            )
                            )";

                    if (GetDataDT(ExistTableSql("GFI_FLT_AutoReprotingConfigDetails")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE  INDEX IX_GFI_FLT_AutoReprotingConfigDetails] ON GFI_FLT_AutoReprotingConfigDetails] (ARID ASC)";
                    if (GetDataDT(ExistsIndex("GFI_FLT_AutoReprotingConfigDetails", "IX_GFI_FLT_AutoReprotingConfigDetails")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_AutoReprotingConfigDetails]  ADD  CONSTRAINT FK_GFI_FLT_ARCD_GFI_FLT_ARC FOREIGN KEY([ARID])
                        REFERENCES GFI_FLT_AutoReportingConfig] ([ARID])";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ARCD_GFI_FLT_ARC", "GFI_FLT_AutoReprotingConfigDetails")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "GFI_FLT_AutoReportingConfig");
                }
                #endregion
                #region Update 40. Rules and Exceptions
                strUpd = "Rez000040";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "DROP TRIGGER trgAfterInsert_GFI_AMM_VehicleMaintTypesLink]";
                    if (ExistTrigger("trgAfterInsert_GFI_AMM_VehicleMaintTypesLink"))
                        dbExecute(sql);

                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[isPointInZone]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
                            DROP FUNCTION isPointInZone]";
                    dbExecute(sql);
                    sql = @"CREATE function isPointInZone] (@Lon float, @Lat float, @iZoneID int)
                        returns int
                        as Begin
	                        Declare @iResult int
	                        set @iResult = 0

	                        DECLARE	@Longitude float] 
	                        DECLARE	@Latitude float]
	                        DECLARE	@nLongitude float] 
	                        DECLARE	@nLatitude float]
	                        DECLARE	@myLongitude float] 
	                        DECLARE	@myLatitude float]
	                        set	@myLongitude = @Lon
	                        set	@myLatitude = @Lat

	                        declare @c bit
	                        set @c = 0
	                        declare xyCursor cursor  LOCAL SCROLL STATIC FOR  
			                        select d.Latitude, d.Longitude from GFI_FLT_ZoneHeader h, GFI_FLT_ZoneDetail d where h.ZoneID = @iZoneID and h.ZoneID = d.ZoneID order by CordOrder

	                        open xyCursor
		                        fetch next from xyCursor into @Latitude, @Longitude
		                        WHILE @@FETCH_STATUS = 0   
		                        BEGIN 
		                        fetch PRIOR from xyCursor into @Latitude, @Longitude
		                        fetch next from xyCursor into @nLatitude, @nLongitude

					                        if ((((@Latitude <= @myLatitude) and (@myLatitude < @nLatitude))
							                        or ((@nLatitude <= @myLatitude) and (@myLatitude < @Latitude)))
							                        and (@myLongitude < (@nLongitude - @Longitude) * (@myLatitude - @Latitude)
								                        / (@nLatitude - @Latitude) + @Longitude))

						                        if(@c = 0)
							                        set @c = 1
						                        else
							                        set @c = 0
			                        fetch next from xyCursor into @Latitude, @Longitude
		                        END
	                        close xyCursor
	                        deallocate xyCursor

	                        if(@c = 0)
		                        set @iResult = 0
	                        else
		                        set @iResult = 1		

	                        return @iResult
                        end";
                    dbExecute(sql);


                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[GetDriverIDFromGMID]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
                            DROP FUNCTION GetDriverIDFromGMID]";
                    dbExecute(sql);
                    sql = @"CREATE function GetDriverIDFromGMID] (@GMID int)
                        returns nvarchar(max)
                        as Begin
	                        declare @t table (GMID int, GMDescription nvarchar(1000), ParentGMID int, StrucID int)
	                        ;WITH 
		                        parent AS 
		                        (
			                        SELECT distinct ParentGMID
			                        FROM GFI_SYS_GroupMatrix
			                        WHERE ParentGMID in (@GMID)
		                        ), 
		                        tree AS 
		                        (
			                        SELECT 
				                        x.GMID
				                        , x.GMDescription
				                        , x.ParentGMID
			                        FROM GFI_SYS_GroupMatrix x
			                        INNER JOIN parent ON x.ParentGMID = parent.ParentGMID
			                        UNION ALL
			                        SELECT 
				                        y.GMID
				                        , y.GMDescription
				                        , y.ParentGMID
			                        FROM GFI_SYS_GroupMatrix y
			                        INNER JOIN tree t ON y.ParentGMID = t.GMID
		                        )
	                        insert into @t
		                        SELECT Distinct GMID, GMDescription, ParentGMID, -2 StrucID FROM tree
		                        union all
			                        SELECT z.GMID, z.GMDescription, -1, -3 Struc
			                        FROM GFI_SYS_GroupMatrix z where z.GMID in (@GMID)
	                        union
							                        select -4, a.sDriverName, tree.GMID as ParentGMID, a.DriverID  
										                        from GFI_FLT_Driver a, GFI_SYS_GroupMatrixDriver m, tree
									                        where tree.GMID = m.GMID and a.DriverID = m.iID
								                        union all
									                        select -5, a.sDriverName, m.GMID as ParentGMID, a.DriverID  
										                        from GFI_FLT_Driver a, GFI_SYS_GroupMatrixDriver m
									                        where m.GMID in (@GMID) and a.DriverID = m.iID
								                        --union 
									                        --select -5, a.sDriverName, m.GMID as ParentGMID, a.DriverID  
									                        --	from GFI_FLT_Driver a, GFI_SYS_GroupMatrixDriver m
									                        --where iButton = '00000000' and a.DriverID = m.iID 

	                        Declare @Result nvarchar(max)
	                        set @Result = ''
	                        Declare @ID nvarchar(15)
	                        Declare myCursor cursor for
		                        select StrucID from @t where StrucID > 0 order by StrucID
	                        open myCursor
		                        fetch next from myCursor into @ID
		                        WHILE @@FETCH_STATUS = 0   
		                        BEGIN
			                        set @Result = @Result + @ID + ','
			                        fetch next from myCursor into @ID
		                        END
	                        close myCursor
	                        deallocate myCursor

	                        if(len(@Result) > 0)
		                        set @Result = SUBSTRING(@Result, 0, len(@Result))

	                        return @Result
                        end";
                    dbExecute(sql);

                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[GetAssetIDFromGMID]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
                            DROP FUNCTION GetAssetIDFromGMID]";
                    dbExecute(sql);
                    sql = @"Create function GetAssetIDFromGMID] (@GMID int)
                        returns nvarchar(max)
                        as Begin
	                        declare @t table (GMID int, GMDescription nvarchar(1000), ParentGMID int, StrucID int)
	                        ;WITH 
		                        parent AS 
		                        (
			                        SELECT distinct ParentGMID
			                        FROM GFI_SYS_GroupMatrix
			                        WHERE ParentGMID in (@GMID)
		                        ), 
		                        tree AS 
		                        (
			                        SELECT 
				                        x.GMID
				                        , x.GMDescription
				                        , x.ParentGMID
			                        FROM GFI_SYS_GroupMatrix x
			                        INNER JOIN parent ON x.ParentGMID = parent.ParentGMID
			                        UNION ALL
			                        SELECT 
				                        y.GMID
				                        , y.GMDescription
				                        , y.ParentGMID
			                        FROM GFI_SYS_GroupMatrix y
			                        INNER JOIN tree t ON y.ParentGMID = t.GMID
		                        )
	                        insert into @t
		                        SELECT Distinct GMID, GMDescription, ParentGMID, -2 StrucID FROM tree
		                        union all
			                        SELECT z.GMID, z.GMDescription, -1, -3 Struc
			                        FROM GFI_SYS_GroupMatrix z where z.GMID in (@GMID)
		                        union all
			                        select -4, a.AssetNumber, tree.GMID as ParentGMID, a.AssetID
				                        from GFI_FLT_Asset a, GFI_SYS_GroupMatrixAsset m, tree
			                        where tree.GMID = m.GMID and a.AssetID = m.iID
                                union all
			                        select -5, a.AssetNumber, m.GMID as ParentGMID, a.AssetID 
				                        from GFI_FLT_Asset a, GFI_SYS_GroupMatrixAsset m
			                        where m.GMID in (@GMID) and a.AssetID = m.iID

	                        Declare @Result nvarchar(max)
	                        set @Result = ''
	                        Declare @ID nvarchar(15)
	                        Declare myCursor cursor for
		                        select StrucID from @t where StrucID > 0 order by StrucID
	                        open myCursor
		                        fetch next from myCursor into @ID
		                        WHILE @@FETCH_STATUS = 0   
		                        BEGIN
			                        set @Result = @Result + @ID + ','
			                        fetch next from myCursor into @ID
		                        END
	                        close myCursor
	                        deallocate myCursor

	                        if(len(@Result) > 0)
		                        set @Result = SUBSTRING(@Result, 0, len(@Result))

	                        return @Result
                        end";
                    dbExecute(sql);

                    sql = @"CREATE  INDEX myIX_GFI_GPS_TripDetail_GPSDataUID]
                            ON GFI_GPS_TripDetail] ([GPSDataUID])
                            INCLUDE ([iID],[HeaderiID])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripDetail", "myIX_GFI_GPS_TripDetail_GPSDataUID")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "Rules and Exceptions");
                }
                #endregion
                #region Update 41. Access Profiles
                strUpd = "Rez000041";
                if (!CheckUpdateExist(strUpd))
                {
                    #region Tables

                    // -- Table: GFI_SYS_Modules
                    sql = @"CREATE TABLE GFI_SYS_Modules](
	                        UID] int] AUTO_INCREMENT NOT NULL,
	                        ModuleName] nvarchar](255) NULL,
	                        Description] nvarchar](255) NULL,
	                        Command] nvarchar](255) NULL,
	                        CreatedDate] datetime] NULL,
	                        CreatedBy] nvarchar](30) NULL,
	                        UpdatedDate] datetime] NULL,
	                        UpdatedBy] nvarchar](30) NULL,
                         CONSTRAINT PK_GFI_SYS_Modules] PRIMARY KEY  
                        (
	                        UID] ASC
                        )
                        )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_Modules")).Rows.Count == 0)
                        dbExecute(sql);

                    // -- Table: GFI_SYS_AccessTemplates
                    sql = @"CREATE TABLE GFI_SYS_AccessTemplates](
	                        UID] int] AUTO_INCREMENT NOT NULL,
	                        TemplateName] nvarchar](120) NULL,
	                        Description] nvarchar](255) NULL,
	                        CreatedDate] datetime] NULL,
	                        CreatedBy] nvarchar](30) NULL,
	                        UpdatedDate] datetime] NULL,
	                        UpdatedBy] nvarchar](30) NULL,
                         CONSTRAINT PK_GFI_SYS_AccessTemplates] PRIMARY KEY  
                        (
	                        UID] ASC
                        )
                        )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_AccessTemplates")).Rows.Count == 0)
                        dbExecute(sql);

                    // -- Table: GFI_SYS_AccessProfiles
                    sql = @"CREATE TABLE GFI_SYS_AccessProfiles](
	                        UID] int] AUTO_INCREMENT NOT NULL,
	                        TemplateId] int] NULL,
	                        ModuleId] int] NULL,
	                        Command] nvarchar](255) NULL,
	                        AllowRead] int] NULL,
	                        AllowNew] int] NULL,
	                        AllowEdit] int] NULL,
	                        AllowDelete] int] NULL,
	                        CreatedDate] datetime] NULL,
	                        CreatedBy] nvarchar](30) NULL,
	                        UpdatedDate] datetime] NULL,
	                        UpdatedBy] nvarchar](30) NULL,
                         CONSTRAINT PK_GFI_SYS_AccessProfiles] PRIMARY KEY  
                        (
	                        UID] ASC
                        )
                        )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_AccessProfiles")).Rows.Count == 0)
                        dbExecute(sql);

                    #endregion

                    #region Indexes
                    sql = @"CREATE  INDEX IX_ModuleId] ON GFI_SYS_AccessProfiles] 
                        (
	                        ModuleId] ASC
                        )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)";

                    if (GetDataDT(ExistsIndex("GFI_SYS_AccessProfiles", "IX_ModuleId")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE  INDEX IX_TemplateId] ON GFI_SYS_AccessProfiles] 
                        (
	                        TemplateId] ASC
                        )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)";

                    if (GetDataDT(ExistsIndex("GFI_SYS_AccessProfiles", "IX_TemplateId")).Rows.Count == 0)
                        dbExecute(sql);


                    #endregion

                    #region Constraints
                    sql = @"ALTER TABLE GFI_SYS_AccessProfiles]  ADD  CONSTRAINT FK_GFI_SYS_AccessProfiles_GFI_SYS_AccessTemplates] FOREIGN KEY([TemplateId])
                        REFERENCES GFI_SYS_AccessTemplates] ([UID])";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_AccessProfiles_GFI_SYS_AccessTemplates", "GFI_SYS_AccessProfiles")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_AccessProfiles]  ADD  CONSTRAINT FK_GFI_SYS_AccessProfiles_GFI_SYS_Modules] FOREIGN KEY([ModuleId])
                        REFERENCES GFI_SYS_Modules] ([UID])";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_AccessProfiles_GFI_SYS_Modules", "GFI_SYS_AccessProfiles")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_AccessProfiles] CHECK CONSTRAINT FK_GFI_SYS_AccessProfiles_GFI_SYS_AccessTemplates]";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_AccessProfiles_GFI_SYS_AccessTemplates", "GFI_SYS_AccessProfiles")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_AccessProfiles] CHECK CONSTRAINT FK_GFI_SYS_AccessProfiles_GFI_SYS_Modules]";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_AccessProfiles_GFI_SYS_Modules", "GFI_SYS_AccessProfiles")).Rows.Count == 0)
                        dbExecute(sql);

                    #endregion

                    InsertDBUpdate(strUpd, "Access Profiles");
                }
                #endregion
                #region Update 42. Reconfigure Vehicle Maint Status.
                strUpd = "Rez000042";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_GPS_Exceptions] add GroupID] int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_Exceptions", "GroupID")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"CREATE  INDEX IX_GroupID] ON GFI_GPS_Exceptions] 
                    (
	                    GroupID] ASC
                    )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)";

                    if (GetDataDT(ExistsIndex("GFI_GPS_Exceptions", "IX_GroupID")).Rows.Count == 0)
                        dbExecute(sql);


                    //COMPLETED
                    VehicleMaintStatus objVMS = new VehicleMaintStatus();
                    objVMS = objVMS.GetVehicleMaintStatusByDescription("COMPLETED", sConnStr);
                    if (objVMS != null)
                    {
                        objVMS.SortOrder = 6;
                        objVMS.UpdatedDate = DateTime.Now;
                        objVMS.UpdatedBy = usrInst.Username;
                        objVMS.Update(objVMS, sConnStr);
                    }

                    objVMS = objVMS.GetVehicleMaintStatusByDescription("VALID", sConnStr);
                    if (objVMS != null)
                    {
                        objVMS.SortOrder = 5;
                        objVMS.UpdatedDate = DateTime.Now;
                        objVMS.UpdatedBy = usrInst.Username;
                        objVMS.Update(objVMS, sConnStr);
                    }

                    InsertDBUpdate(strUpd, "Reconfigure Vehicle Maint Status N Exception GroupID");
                }
                #endregion
                #region Update 43. Add field AccessTemplateId] to table GFI_SYS_User]
                strUpd = "Rez000043";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_SYS_User ADD AccessTemplateId int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "AccessTemplateId")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "Add field AccessTemplateId] to table GFI_SYS_User]");
                }
                #endregion
                #region Update 44. Add Application Modules to the Modules Tables
                strUpd = "Rez000044";
                if (!CheckUpdateExist(strUpd))
                {
                    NaveoOneLib.Models.Module module = new NaveoOneLib.Models.Module();
                    module = module.GetModulesByName("MAPS --> LIVE MAP", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Module();
                        module.ModuleName = "MAPS --> LIVE MAP";
                        module.Description = "MAPS --> LIVE MAP";
                        module.Command = "CmdLiveMap";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        module.Save(module, sConnStr);
                    }

                    module = module.GetModulesByName("MAPS --> TRIP HISTORY", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Module();
                        module.ModuleName = "MAPS --> TRIP HISTORY";
                        module.Description = "MAPS --> TRIP HISTORY";
                        module.Command = "CmdTripHistory";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        module.Save(module, sConnStr);
                    }

                    module = module.GetModulesByName("MAPS --> Zone VisitedNotVisited", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Module();
                        module.ModuleName = "MAPS --> Zone VisitedNotVisited";
                        module.Description = "MAPS --> Zone VisitedNotVisited";
                        module.Command = "CmdZoneVisitedNotVisited";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        module.Save(module, sConnStr);
                    }

                    module = module.GetModulesByName("MAPS --> Test Trip History", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Module();
                        module.ModuleName = "MAPS --> Test Trip History";
                        module.Description = "MAPS --> Test Trip History";
                        module.Command = "CmdTestTripHistory";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        module.Save(module, sConnStr);
                    }

                    module = module.GetModulesByName("Vehicles & Assets --> Current Status", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Module();
                        module.ModuleName = "Vehicles & Assets --> Current Status";
                        module.Description = "Vehicles & Assets --> Current Status";
                        module.Command = "CmdCurrentStatus";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        module.Save(module, sConnStr);
                    }

                    module = module.GetModulesByName("Vehicles & Assets --> Asset List", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Module();
                        module.ModuleName = "Vehicles & Assets --> Asset List";
                        module.Description = "Vehicles & Assets --> Asset List";
                        module.Command = "cmdAssetVehList";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        module.Save(module, sConnStr);
                    }

                    module = module.GetModulesByName("Vehicles & Assets --> Asset Maint. Dashboard", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Module();
                        module.ModuleName = "Vehicles & Assets --> Asset Maint. Dashboard";
                        module.Description = "Vehicles & Assets --> Asset Maint. Dashboard";
                        module.Command = "cmdAssetMainStatusRep";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        module.Save(module, sConnStr);
                    }

                    module = module.GetModulesByName("Vehicles & Assets --> Assign Maint. Types", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Module();
                        module.ModuleName = "Vehicles & Assets --> Assign Maint. Types";
                        module.Description = "Vehicles & Assets --> Assign Maint. Types";
                        module.Command = "cmdBulkAssignMaintTypes";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        module.Save(module, sConnStr);
                    }

                    module = module.GetModulesByName("Vehicles & Assets --> Assets Management", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Module();
                        module.ModuleName = "Vehicles & Assets --> Assets Management";
                        module.Description = "Vehicles & Assets --> Assets Management";
                        module.Command = "CmdAssetsMgmt";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        module.Save(module, sConnStr);
                    }

                    module = module.GetModulesByName("Vehicles & Assets --> Drivers Management", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Module();
                        module.ModuleName = "Vehicles & Assets --> Drivers Management";
                        module.Description = "Vehicles & Assets --> Drivers Management";
                        module.Command = "cmdDriversMgmt";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        module.Save(module, sConnStr);
                    }

                    module = module.GetModulesByName("Rules & Messaging --> Exceptions", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Module();
                        module.ModuleName = "Rules & Messaging --> Exceptions";
                        module.Description = "Rules & Messaging --> Exceptions";
                        module.Command = "CmdExceptions";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        module.Save(module, sConnStr);
                    }

                    module = module.GetModulesByName("Rules & Messaging --> Manage Exceptions", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Module();
                        module.ModuleName = "Rules & Messaging --> Manage Exceptions";
                        module.Description = "Rules & Messaging --> Manage Exceptions";
                        module.Command = "CmdManageExceptions";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        module.Save(module, sConnStr);
                    }

                    module = module.GetModulesByName("Rules & Messaging --> Messaging", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Module();
                        module.ModuleName = "Rules & Messaging --> Messaging";
                        module.Description = "Rules & Messaging --> Messaging";
                        module.Command = "CmdMessaging";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        module.Save(module, sConnStr);
                    }

                    module = module.GetModulesByName("Rules & Messaging --> Rules", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Module();
                        module.ModuleName = "Rules & Messaging --> Rules";
                        module.Description = "Rules & Messaging --> Rules";
                        module.Command = "CmdRules";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        module.Save(module, sConnStr);
                    }

                    module = module.GetModulesByName("Rules & Messaging --> Reprocess Rules", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Module();
                        module.ModuleName = "Rules & Messaging --> Reprocess Rules";
                        module.Description = "Rules & Messaging --> Reprocess Rules";
                        module.Command = "cmdReprocessRules";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        module.Save(module, sConnStr);
                    }

                    module = module.GetModulesByName("Zone Management --> Zone Types", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Module();
                        module.ModuleName = "Zone Management --> Zone Types";
                        module.Description = "Zone Management --> Zone Types";
                        module.Command = "CmdZoneTypes";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        module.Save(module, sConnStr);
                    }

                    module = module.GetModulesByName("Zone Management --> Zone Management", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Module();
                        module.ModuleName = "Zone Management --> Zone Management";
                        module.Description = "Zone Management --> Zone Management";
                        module.Command = "CmdZoneManagement";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        module.Save(module, sConnStr);
                    }

                    module = module.GetModulesByName("Reports --> Zones Visited", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Module();
                        module.ModuleName = "Reports --> Zones Visited";
                        module.Description = "Reports --> Zones Visited";
                        module.Command = "CmdZonesVisited";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        module.Save(module, sConnStr);
                    }

                    module = module.GetModulesByName("Reports --> Fuel Consumption", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Module();
                        module.ModuleName = "Reports --> Fuel Consumption";
                        module.Description = "Reports --> Fuel Consumption";
                        module.Command = "CmdFuelConsumption";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        module.Save(module, sConnStr);
                    }

                    module = module.GetModulesByName("Reports --> Exception Report", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Module();
                        module.ModuleName = "Reports --> Exception Report";
                        module.Description = "Reports --> Exception Report";
                        module.Command = "CmdExceptionReport";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        module.Save(module, sConnStr);
                    }

                    module = module.GetModulesByName("Reports --> Asset Summary", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Module();
                        module.ModuleName = "Reports --> Asset Summary";
                        module.Description = "Reports --> Asset Summary";
                        module.Command = "CmdAssetSummary";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        module.Save(module, sConnStr);
                    }

                    module = module.GetModulesByName("Reports --> Daily Summary", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Module();
                        module.ModuleName = "Reports --> Daily Summary";
                        module.Description = "Reports --> Daily Summary";
                        module.Command = "CmdDailySummary";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        module.Save(module, sConnStr);
                    }

                    module = module.GetModulesByName("Reports --> Auxilliary Report", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Module();
                        module.ModuleName = "Reports --> Auxilliary Report";
                        module.Description = "Reports --> Auxilliary Report";
                        module.Command = "CmdAuxilliaryReport";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        module.Save(module, sConnStr);
                    }

                    module = module.GetModulesByName("Reports --> Automatic Reporting", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Module();
                        module.ModuleName = "Reports --> Automatic Reporting";
                        module.Description = "Reports --> Automatic Reporting";
                        module.Command = "cmdAutoReportingConfig";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        module.Save(module, sConnStr);
                    }

                    module = module.GetModulesByName("Reports --> Maint. Costs Report", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Module();
                        module.ModuleName = "Reports --> Maint. Costs Report";
                        module.Description = "Reports --> Maint. Costs Report";
                        module.Command = "cmdMaintCostReport";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        module.Save(module, sConnStr);
                    }

                    module = module.GetModulesByName("Advanced --> Asset Maintenance", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Module();
                        module.ModuleName = "Advanced --> Asset Maintenance";
                        module.Description = "Advanced --> Asset Maintenance";
                        module.Command = "CmdAdvAssetMaintenance";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        module.Save(module, sConnStr);
                    }

                    module = module.GetModulesByName("Advanced --> Change Password", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Module();
                        module.ModuleName = "Advanced --> Change Password";
                        module.Description = "Advanced --> Change Password";
                        module.Command = "CmdChangePassword";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        module.Save(module, sConnStr);
                    }

                    module = module.GetModulesByName("Advanced --> Users Management", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Module();
                        module.ModuleName = "Advanced --> Users Management";
                        module.Description = "Advanced --> Users Management";
                        module.Command = "CmdUsersMgmt";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        module.Save(module, sConnStr);
                    }

                    module = module.GetModulesByName("Advanced --> User Access Templates", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Module();
                        module.ModuleName = "Advanced --> User Access Templates";
                        module.Description = "Advanced --> User Access Templates";
                        module.Command = "cmdUserAccessTemplate";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        module.Save(module, sConnStr);
                    }

                    InsertDBUpdate(strUpd, "Add Application Modules to the Modules Tables");
                }
                #endregion
                #region Update 45. Zone refs
                strUpd = "Rez000045";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_FLT_ZoneHeader ADD ExternalRef nvarchar(30) NOT NULL Default ''";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ZoneHeader", "ExternalRef")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_ZoneHeader ADD Ref2 nvarchar(50) NOT NULL Default ''";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ZoneHeader", "Ref2")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE  INDEX my_IX_GFI_GPS_TripDetail] ON GFI_GPS_TripDetail]
                            (
	                            HeaderiID] ASC
                            )";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripDetail", "my_IX_GFI_GPS_TripDetail")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "Zone refs");
                }
                #endregion
                #region Update 46. User Settings
                strUpd = "Rez000046";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_SYS_UserSettings](
	                        iID] int] AUTO_INCREMENT NOT NULL,
	                        UserID] int] NOT NULL,
	                        ModuleID] nvarchar](50) NOT NULL,
	                        ParamType] nvarchar](50) NOT NULL,
	                        Parameter] nvarchar](100) NOT NULL,
	                        ParamValue1] nvarchar](max) NULL,
	                        ParamValue2] nvarchar](max) NULL,
	                        ParamValue3] nvarchar](max) NULL,
	                        ParamDate1] datetime] NULL,
	                        ParamDate2] datetime] NULL,
                         CONSTRAINT PK_GFI_SYS_UserSettings] PRIMARY KEY  
                        (
	                        iID] ASC
                        )
                        ) TEXTIMAGE_ON PRIMARY]";

                    if (GetDataDT(ExistTableSql("GFI_SYS_UserSettings")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_UserSettings 
                            ADD CONSTRAINT FK_GFI_SYS_UserSettings_GFI_SYS_User 
                                FOREIGN KEY(UserID) 
                                REFERENCES GFI_SYS_User(UID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_UserSettings_GFI_SYS_User", "GFI_SYS_UserSettings")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE  INDEX IX_UserSettings] ON GFI_SYS_UserSettings] 
                        (
	                        UserID] ASC
                        )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)";

                    if (GetDataDT(ExistsIndex("GFI_SYS_UserSettings", "IX_ModuleId")).Rows.Count == 0)
                        dbExecute(sql);


                    sql = @"ALTER TABLE GFI_FLT_Driver ADD EmployeeType nvarchar(15) NOT NULL Default 'Driver'";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "EmployeeType")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"CREATE  INDEX my_IX_EmployeeType] ON GFI_FLT_Driver]
                            (
	                            EmployeeType] ASC
                            )";
                    if (GetDataDT(ExistsIndex("GFI_FLT_Driver", "my_IX_EmployeeType")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "User Settings and EmployeeType");
                }
                #endregion
                #region Update 47. TripHeader and GPSData indexes
                strUpd = "Rez000047";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE  INDEX my_IX_AssetID_GFI_GPS_TripHeader] ON GFI_GPS_TripHeader] ([AssetID])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "my_IX_AssetID_GFI_GPS_TripHeader")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"DROP INDEX my_IX_GFI_GPS_TripHeader] ON GFI_GPS_TripHeader]";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "my_IX_GFI_GPS_TripHeader")).Rows.Count > 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX my_IX_GFI_GPS_TripHeader] ON GFI_GPS_TripHeader]
                        (
                            AssetID] ASC,
                            DriverID] ASC,
                            GPSDataStartUID] ASC,
                            GPSDataEndUID] ASC
                        )/*WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "my_IX_GFI_GPS_TripHeader")).Rows.Count > 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX my_IX_EndUID_GFI_GPS_TripHeader ON GFI_GPS_TripHeader] ([GPSDataEndUID])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "my_IX_EndUID_GFI_GPS_TripHeader")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"ALTER TABLE GFI_GPS_Exceptions ADD Posted int NOT NULL Default 0";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_Exceptions", "Posted")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "TH indexes and Exception fields");
                }
                #endregion
                #region Update 48. Notification Table
                strUpd = "Rez000048";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_SYS_Notification](
                             NID] int] AUTO_INCREMENT NOT NULL,
                             ARID] int] NULL,
                             ExceptionID] int] NULL,
                             UID] int null,
                             Status] tinyint] NULL Default (0),
                             InsertedAt] datetime] NULL DEFAULT (getdate()),
                             SendAt] datetime] NULL,
                             CONSTRAINT PK_Notification] PRIMARY KEY  
                                ([NID] ASC )
                            
                        )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_Notification")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE  INDEX ARID_ExID] ON GFI_SYS_Notification] ([ARID] ASC, ExceptionID] ASC)";
                    if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "ARID_ExID")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "Notification Table");
                }
                #endregion
                #region Update 49. RegisterDevicesForProcessing 5 days max
                strUpd = "Rez000049";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "delete from GFI_GPS_ProcessPending where dtDateFrom < DATEADD(day, -5, getdate())";
                    dbExecute(sql);

                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[RegisterDevicesForProcessing]') AND type in (N'P', N'PC'))
                            DROP PROCEDURE RegisterDevicesForProcessing]";
                    dbExecute(sql);
                    sql = @"CREATE Procedure RegisterDevicesForProcessing]
                        @AssetID int,
                        @dtDateFrom datetime,
                        @ProcessCode nvarchar (50)
                        as Begin
							declare @ValidDateTime datetime
							set @ValidDateTime = DATEADD(day, -5, getdate())
							if(@dtDateFrom > @ValidDateTime)
							begin
								if exists (select * from GFI_GPS_ProcessPending where AssetID = @AssetID and ProcessCode = @ProcessCode and Processing = 0)
									begin
									if(@dtDateFrom < (select dtDateFrom from GFI_GPS_ProcessPending where AssetID = @AssetID and ProcessCode = @ProcessCode and Processing = 0))
										update GFI_GPS_ProcessPending set dtDateFrom = @dtDateFrom where AssetID = @AssetID and ProcessCode = @ProcessCode and Processing = 0
									end
								else
									insert GFI_GPS_ProcessPending (AssetID, dtDateFrom, ProcessCode)
										values (@AssetID, @dtDateFrom, @ProcessCode)
							end
							else	--ignoring
							begin
								set @ProcessCode = @ProcessCode + 'Ignored'
								if exists (select * from GFI_GPS_ProcessPending where AssetID = @AssetID and ProcessCode = @ProcessCode and Processing = 0)
									begin
									if(@dtDateFrom < (select dtDateFrom from GFI_GPS_ProcessPending where AssetID = @AssetID and ProcessCode = @ProcessCode and Processing = 0))
										update GFI_GPS_ProcessPending set dtDateFrom = @dtDateFrom where AssetID = @AssetID and ProcessCode = @ProcessCode and Processing = 0
									end
								else
									insert GFI_GPS_ProcessPending (AssetID, dtDateFrom, ProcessCode)
										values (@AssetID, @dtDateFrom, @ProcessCode)
							end
                        end";
                    dbExecute(sql);
                    InsertDBUpdate(strUpd, "RegisterDevicesForProcessing 5 days max");
                }
                #endregion
                #region Update 50. GFI_AMM_VehicleMaintTypes GM assignment
                strUpd = "Rez000050";
                if (!CheckUpdateExist(strUpd))
                {
                    DataTable dtMainType = GetDataDT("SELECT * FROM GFI_AMM_VehicleMaintTypes");
                    foreach (DataRow r in dtMainType.Rows)
                    {
                        VehicleMaintTypes obj = new VehicleMaintTypes().GetVehicleMaintTypesById(r["MaintTypeId"].ToString(), sConnStr);
                        if (obj.lMatrix.Count == 0)
                        {
                            obj.lMatrix = usrInst.lMatrix;
                            foreach (Matrix m in obj.lMatrix)
                            {
                                m.iID = obj.MaintTypeId;
                                m.oprType = DataRowState.Added;
                            }
                            obj.Update(obj, sConnStr);
                        }
                    }
                    foreach (DataRow r in dtMainType.Rows)
                    {
                        VehicleMaintTypes obj = new VehicleMaintTypes().GetVehicleMaintTypesById(r["MaintTypeId"].ToString(), sConnStr);
                        if (obj.CreatedBy == String.Empty)
                        {
                            obj.CreatedBy = usrInst.Username;
                            obj.Update(obj, sConnStr);
                        }
                    }
                    InsertDBUpdate(strUpd, "GFI_AMM_VehicleMaintTypes GM assignment");
                }
                #endregion
                #region Update 51. AccessTemplate Group Matrix.
                strUpd = "Rez000051";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_SYS_GroupMatrixAccessTemplate]
                        (
	                        MID] INT AUTO_INCREMENT NOT NULL
	                        , GMID] INT NOT NULL
	                        , iID] INT NOT NULL
                            , CONSTRAINT PK_GFI_SYS_GroupMatrixAccessTemplate] PRIMARY KEY  ([MID] ASC)
                        )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrixAccessTemplate")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixAccessTemplate 
                            ADD CONSTRAINT FK_GFI_SYS_GroupMatrixAccessTemplate_GFI_SYS_GroupMatrix 
                                FOREIGN KEY (GMID) 
                                REFERENCES GFI_SYS_GroupMatrix (GMID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixAccessTemplate_GFI_SYS_GroupMatrix", "GFI_SYS_GroupMatrixAccessTemplate")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixAccessTemplate 
                            ADD CONSTRAINT FK_GFI_SYS_GroupMatrixAccessTemplate_GFI_SYS_AccessTemplates 
                                FOREIGN KEY(iID) 
                                REFERENCES GFI_SYS_AccessTemplates(UID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixAccessTemplate_GFI_SYS_AccessTemplates", "GFI_SYS_GroupMatrixAccessTemplate")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "AccessTemplate Group Matrix");
                }
                #endregion
                #region Update 52. GFI_SYS_AccessTemplates GM assignment
                strUpd = "Rez000052";
                if (!CheckUpdateExist(strUpd))
                {
                    DataTable dtAT = GetDataDT("SELECT * FROM GFI_SYS_AccessTemplates");
                    foreach (DataRow r in dtAT.Rows)
                    {
                        AccessTemplate obj = new AccessTemplate().GetAccessTemplatesById(r["UID"].ToString(), sConnStr);
                        if (obj.lMatrix.Count == 0)
                        {
                            obj.lMatrix = usrInst.lMatrix;
                            obj.CreatedBy = usrInst.Username;
                            obj.UpdatedBy = usrInst.Username;
                            foreach (Matrix m in obj.lMatrix)
                            {
                                m.iID = obj.UID;
                                m.oprType = DataRowState.Added;
                            }
                            obj.Save(obj, false, new DataTable(), sConnStr);
                        }
                    }
                    InsertDBUpdate(strUpd, "GFI_SYS_AccessTemplates GM assignment");
                }
                #endregion
                #region Update 53. TripDetail UID now BigInt.
                strUpd = "Rez000053";
                if (!CheckUpdateExist(strUpd))
                {
                    dt = GetDataDT(GetFieldSchema("GFI_GPS_TripDetail", "iID"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "bigint")
                    {
                        sql = "ALTER TABLE GFI_GPS_TripDetail DROP FOREIGN KEY PK_GFI_GPS_TripDetail";
                        if (GetDataDT(ExistsSQLConstraint("PK_GFI_GPS_TripDetail", "GFI_GPS_TripDetail")).Rows.Count > 0)
                            dbExecuteWithoutTransaction(sql, 4000);

                        sql = "DROP INDEX myIX_GFI_GPS_TripDetail_GPSDataUID] ON GFI_GPS_TripDetail]";
                        if (GetDataDT(ExistsIndex("GFI_GPS_TripDetail", "myIX_GFI_GPS_TripDetail_GPSDataUID")).Rows.Count > 0)
                            dbExecuteWithoutTransaction(sql, 4000);

                        sql = "ALTER TABLE GFI_GPS_TripDetail ALTER COLUMN iID bigint";
                        dbExecuteWithoutTransaction(sql, 4000);

                        sql = @"ALTER TABLE GFI_GPS_TripDetail] ADD  CONSTRAINT PK_GFI_GPS_TripDetail] PRIMARY KEY  
                            (
                                iID] ASC
                            )";
                        dbExecuteWithoutTransaction(sql, 4000);

                        sql = @"CREATE  INDEX myIX_GFI_GPS_TripDetail_GPSDataUID] ON GFI_GPS_TripDetail]
                            (
                                GPSDataUID] ASC
                            )
                            INCLUDE ([iID], HeaderiID])";
                        dbExecuteWithoutTransaction(sql, 4000);
                    }
                    InsertDBUpdate(strUpd, "TripDetail UID now BigInt");
                }
                #endregion
                #region Update 54. Group Matrix IsCompany field
                strUpd = "Rez000054";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_SYS_GroupMatrix ADD IsCompany int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_GroupMatrix", "IsCompany")).Rows.Count == 0)
                    {
                        dbExecute(sql);

                        sql = @"
                        --ALTER TABLE GFI_SYS_GroupMatrix SET (LOCK_ESCALATION = TABLE)
                        
                        Update GFI_SYS_GroupMatrix set IsCompany = 1 where ParentGMID = 3

                        DECLARE @GMID int
                        DECLARE @GMDESCRITION nvarchar(100)
                        Declare @ParentGMID int
                        DECLARE @Remarks nchar(100)
                        DECLARE @IsCompany int

                        DECLARE curgm CURSOR FOR SELECT GMID
                              ,GMDescription
                              ,ParentGMID
                              ,Remarks
                              ,IsCompany
                          FROM GFI_SYS_GroupMatrix WHERE ISCompany = 1 and GMDescription <>''

                        OPEN curgm
                        --IF @@CURSOR_ROWS > 0
                         --BEGIN
                         FETCH NEXT FROM curgm INTO @GMID,@GMDESCRITION,@ParentGMID,@Remarks,@IsCompany
                          WHILE @@Fetch_status = 0
                          BEGIN
                          --SELECT @GMID = GMID ,@GMDESCRITION =  GMDESCRITION, 
                          --INSERT INTO GFI_SYS_GroupMatrix (GMDescription,ParentGMID,Remarks,IsCompany) Values(CONCAT('### DEFAULT_',@GMDESCRITION,' ###' ), @GMID,'Default Content Group',0 )
                          INSERT INTO GFI_SYS_GroupMatrix (GMDescription,ParentGMID,Remarks,IsCompany) Values('### DEFAULT_'+ @GMDESCRITION + ' ###', @GMID,'Default Content Group',0 )
  
                          FETCH NEXT FROM curgm INTO @GMID,@GMDESCRITION,@ParentGMID,@Remarks,@IsCompany
                          END

                         --END

                        CLOSE curgm
                        DEALLOCATE curgm";
                        dbExecute(sql);
                    }
                    InsertDBUpdate(strUpd, "Group Matrix IsCompany field");
                }
                #endregion
                #region Update 55. GFI_SYS_PARAMETERSTable with
                strUpd = "Rez000055";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_SYS_GlobalParams] (
                              iid] int IDENTITY(1, 1) NOT NULL,
                              ParamName] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
                              PValue] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
                              ParamComments] nvarchar(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
                              PRIMARY KEY  ([iid] ),
                              CONSTRAINT U_dbo_GFI_SYS_GlobalParams_1]
                              UNIQUE  ([iid] , ParamName] )
                              /*WITH ( PAD_INDEX = OFF,
                              FILLFACTOR = 100,
                              IGNORE_DUP_KEY = OFF,
                              STATISTICS_NORECOMPUTE = OFF,
                              ALLOW_ROW_LOCKS = ON,
                              ALLOW_PAGE_LOCKS = ON
                              --  , DATA_COMPRESSION = NONE 
                              )
                              
                              )
                             ;
                              
                              --Reza Commented for sql 2005
                              --ALTER TABLE GFI_SYS_GlobalParams] SET (LOCK_ESCALATION = TABLE);
                            
                              INSERT INTO GFI_SYS_GlobalParams] (
                                 ParamName
                                ,PValue
                                ,ParamComments
                              ) VALUES (
                                 'EXCEPSRC'  -- ParamName - varchar(20)
                                ,'NaveoOne'  -- PValue - varchar(50)
                                ,'SPECIFIES SOURCE OF EXCEPTIONS' -- ParamComments - nvarchar(MAX)
                              )";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "PARAMETER TABLE WITH DEFAULT Value");
                }
                #endregion
                #region Update 56. my_IX_MaxGrpID on GFI_GPS_Exceptions
                strUpd = "Rez000056";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE  INDEX my_IX_MaxGrpID
                            ON GFI_GPS_Exceptions] ([AssetID])
                            INCLUDE ([GroupID])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_Exceptions", "my_IX_MaxGrpID")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "my_IX_MaxGrpID on GFI_GPS_Exceptions");
                }
                #endregion
                #region Update 57. myIdxDT on GFI_GPS_GPSData
                strUpd = "Rez000057";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE  INDEX myIdxDT]
                            ON GFI_GPS_GPSData] ([DateTimeGPS_UTC])
                            INCLUDE ([UID])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSData", "myIdxDT")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "myIdxDT on GFI_GPS_GPSData");
                }
                #endregion
                #region Update 58. GFI_FLT_AutoReportingConfig columns
                strUpd = "Rez000058";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_FLT_AutoReportingConfig ADD ReportFormat nvarchar(100) NOT NULL Default ''";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AutoReportingConfig", "ReportFormat")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "GFI_FLT_AutoReportingConfig columns");
                }
                #endregion
                #region Update 59. Indexes
                strUpd = "Rez000059";
                if (!CheckUpdateExist(strUpd))
                {
                    //GPSData
                    sql = "DROP INDEX IX_GFI_GPS_GPSData_AssetID_DateTimeGPS] ON GFI_GPS_GPSData]";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSData", "IX_GFI_GPS_GPSData_AssetID_DateTimeGPS")).Rows.Count > 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "DROP INDEX IX_GFI_GPS_GPSData_ProcessTrips] ON GFI_GPS_GPSData]";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSData", "IX_GFI_GPS_GPSData_ProcessTrips")).Rows.Count > 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "DROP INDEX MyIdxAssetID] ON GFI_GPS_GPSData]";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSData", "MyIdxAssetID")).Rows.Count > 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "DROP INDEX my_IX_GFI_GPS_GPSData] ON GFI_GPS_GPSData]";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSData", "my_IX_GFI_GPS_GPSData")).Rows.Count > 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "drop INDEX MyIdxTripProcessor ON GFI_GPS_GPSData] ";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSData", "MyIdxTripProcessor")).Rows.Count > 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX IX_GFI_GPS_GPSData_ProcessTrips]
                            ON GFI_GPS_GPSData] ([AssetID], DateTimeGPS_UTC])
                            INCLUDE ([UID],[DateTimeServer],[Longitude],[Latitude],[LongLatValidFlag],[Speed],[EgineOn],[StopFlag],[TripDistance],[TripTime],[WorkHour],[DriverID])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSData", "IX_GFI_GPS_GPSData_ProcessTrips")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    //GFI_GPS_GPSDataDetail
                    sql = "drop INDEX MyIdxDTripProcessor ON GFI_GPS_GPSDataDetail] ";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSDataDetail", "MyIdxDTripProcessor")).Rows.Count > 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX MyIdxDTripProcessor
                            ON GFI_GPS_GPSDataDetail] ([TypeID])
                            INCLUDE ([UID],[TypeVaule],[UOM])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSDataDetail", "MyIdxDTripProcessor")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    //GFI_FLT_ZoneDetail
                    sql = "drop INDEX myIdxGFI_FLT_ZoneDetailZoneID ON GFI_FLT_ZoneDetail] ";
                    if (GetDataDT(ExistsIndex("GFI_FLT_ZoneDetail", "myIdxGFI_FLT_ZoneDetailZoneID")).Rows.Count > 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX myIdxGFI_FLT_ZoneDetailZoneID
                            ON GFI_FLT_ZoneDetail] ([ZoneID])";
                    if (GetDataDT(ExistsIndex("GFI_FLT_ZoneDetail", "myIdxGFI_FLT_ZoneDetailZoneID")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    //GFI_GPS_Exceptions
                    sql = "drop INDEX MyIxExceptionProcessor ON GFI_GPS_Exceptions] ";
                    if (GetDataDT(ExistsIndex("GFI_GPS_Exceptions", "MyIxExceptionProcessor")).Rows.Count > 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX MyIxExceptionProcessor
                            ON GFI_GPS_Exceptions] ([AssetID],[GPSDataID],[iID])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_Exceptions", "MyIxExceptionProcessor")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    //GFI_GPS_TripHeader
                    sql = "DROP INDEX my_IX_AssetID_GFI_GPS_TripHeader] ON GFI_GPS_TripHeader]";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "my_IX_AssetID_GFI_GPS_TripHeader")).Rows.Count > 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "DROP INDEX my_IX_EndUID_GFI_GPS_TripHeader] ON GFI_GPS_TripHeader]";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "my_IX_EndUID_GFI_GPS_TripHeader")).Rows.Count > 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "DROP INDEX MyIXAssetDriver] ON GFI_GPS_TripHeader]";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "MyIXAssetDriver")).Rows.Count > 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX MyIXAsset
                            ON GFI_GPS_TripHeader] ([AssetID])
                            INCLUDE ([iID],[GPSDataStartUID],[GPSDataEndUID])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "MyIXAsset")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX MyIXDriver
                            ON GFI_GPS_TripHeader] ([DriverID])
                            INCLUDE ([iID],[GPSDataStartUID],[GPSDataEndUID])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "MyIXDriver")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "Indexes");
                }
                #endregion
                #region Update 60. TripHeader Additional Fields.
                strUpd = "Rez000060";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_GPS_TripHeader] add dtStart] datetime NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "dtStart")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = "ALTER TABLE GFI_GPS_TripHeader] add fStartLon] float] NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "fStartLon")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = "ALTER TABLE GFI_GPS_TripHeader] add fStartLat] float] NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "fStartLat")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_GPS_TripHeader] add dtEnd] datetime NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "dtEnd")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = "ALTER TABLE GFI_GPS_TripHeader] add fEndLon] float] NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "fEndLon")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = "ALTER TABLE GFI_GPS_TripHeader] add fEndtLat] float] NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "fEndtLat")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE  INDEX IX_Trip] ON GFI_GPS_TripHeader] 
                    (
	                    dtStart, dtEnd
                    )/*WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "IX_Trip")).Rows.Count == 0)
                        dbExecute(sql, null, 8000);

                    sql = @"update GFI_GPS_TripHeader set dtStart = g.DateTimeGPS_UTC, fStartLon = g.Longitude, fStartLat = g.Latitude
	                        from GFI_GPS_TripHeader h
	                        inner join GFI_GPS_GPSData g on h.GPSDataStartUID = g.UID
                        where dtStart is null";
                    dbExecute(sql, null, 8000);

                    sql = @"update GFI_GPS_TripHeader set dtEnd = g.DateTimeGPS_UTC, fEndLon = g.Longitude, fEndtLat = g.Latitude
	                        from GFI_GPS_TripHeader h
	                        inner join GFI_GPS_GPSData g on h.GPSDataEndUID = g.UID
                        where dtEnd is null";
                    dbExecute(sql, null, 8000);

                    InsertDBUpdate(strUpd, "TripHeader Additional Fields");
                }
                #endregion
                #region Update 61. GFI_GPS_TripDetail Index
                strUpd = "Rez000061";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "DROP INDEX my_IX_GFI_GPS_TripDetail] ON GFI_GPS_TripDetail]";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripDetail", "my_IX_GFI_GPS_TripDetail")).Rows.Count > 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX my_IX_GFI_GPS_TripDetail] ON GFI_GPS_TripDetail]
                            (
	                            HeaderiID] ASC
                            )";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripDetail", "my_IX_GFI_GPS_TripDetail")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "GFI_GPS_TripDetail Indexes");
                }
                #endregion
                #region Update 62. GPSData Live.
                strUpd = "Rez000062";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_GPS_LiveData](
	                        UID] int] NOT NULL,
	                        AssetID] int] NULL,
	                        DateTimeGPS_UTC] datetime] NULL,
	                        DateTimeServer] datetime] NULL,
	                        Longitude] float] NULL,
	                        Latitude] float] NULL,
	                        LongLatValidFlag] int] NULL,
	                        Speed] float] NULL,
	                        EgineOn] int] NULL,
	                        StopFlag] int] NULL,
	                        TripDistance] float] NULL,
	                        TripTime] float] NULL,
	                        WorkHour] int] NULL,
	                        DriverID] int] NOT NULL,
                         CONSTRAINT PK_GFI_GPS_LiveData] PRIMARY KEY ([UID] ASC)
                        )";
                    if (GetDataDT(ExistTableSql("GFI_GPS_LiveData")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_GPS_LiveDataDetail](
	                        GPSDetailID] bigint] AUTO_INCREMENT NOT NULL,
	                        UID] int] NULL,
	                        TypeID] nvarchar](30) NULL,
	                        TypeVaule] nvarchar](30) NULL,
	                        UOM] nvarchar](15) NULL,
	                        Remarks] nvarchar](50) NULL,
                         CONSTRAINT PK_GFI_GPS_LiveDataDetail] PRIMARY KEY  ([GPSDetailID] ASC)
                        )";
                    if (GetDataDT(ExistTableSql("GFI_GPS_LiveDataDetail")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_GPS_LiveDataDetail]
                            ADD  CONSTRAINT FK_GFI_GPS_LiveDataDetail_GFI_GPS_LiveData]
                                FOREIGN KEY([UID])
                            REFERENCES GFI_GPS_LiveData] ([UID])
                            ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_LiveDataDetail_GFI_GPS_LiveData", "GFI_GPS_LiveDataDetail")).Rows.Count == 0)
                    {
                        dbExecute(sql);

                        sql = @";with cte as (
                            select uid, AssetID, 
                                DateTimeGPS_UTC, 
                                DateTimeServer, 
                                Longitude, 
                                Latitude, 
                                LongLatValidFlag, 
                                Speed, 
                                EgineOn, 
                                StopFlag, 
                                TripDistance, 
                                TripTime, 
		                        WorkHour,
		                        DriverID,
                                row_number() over(partition by AssetID order by DateTimeGPS_UTC desc) as RowNum
	                        from GFI_GPS_GPSData
                            WHERE AssetID in (select AssetID from GFI_FLT_Asset)
                                and DateTimeGPS_UTC < getdate() + 1
                                and LongLatValidFlag = 1 
	                           )
                        insert GFI_GPS_LiveData]
                        select uid, AssetID, 
                                DateTimeGPS_UTC, 
                                DateTimeServer, 
                                Longitude, 
                                Latitude, 
                                LongLatValidFlag, 
                                Speed, 
                                EgineOn, 
                                StopFlag, 
                                TripDistance, 
                                TripTime, 
		                        WorkHour,
		                        DriverID  
                        from cte
                            where RowNum <= 10
                        ";
                        dbExecute(sql, null, 8000);

                        sql = @"insert GFI_GPS_LiveDataDetail
	                        SELECT UID, TypeID, TypeVaule, UOM, Remarks from GFI_GPS_GPSDataDetail d
		                        where d.uid in (select uid from GFI_GPS_LiveData)";
                        dbExecute(sql, null, 8000);
                    }

                    InsertDBUpdate(strUpd, "GPSData Live");
                }
                #endregion
                #region Update 63. Referential Constraints
                strUpd = "Rez000063";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_GPS_ProcessPending] DROP FOREIGN KEY FK_GFI_GPS_ProcessPending_GFI_FLT_Asset";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_ProcessPending_GFI_FLT_Asset", "GFI_GPS_ProcessPending")).Rows.Count > 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_GPS_ProcessPending 
                            ADD CONSTRAINT FK_GFI_GPS_ProcessPending_GFI_FLT_Asset 
                                FOREIGN KEY(AssetID) 
                                REFERENCES GFI_FLT_Asset(AssetID) 
	                     ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_ProcessPending_GFI_FLT_Asset", "GFI_GPS_ProcessPending")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_GPS_Exceptions 
                            ADD CONSTRAINT FK_GFI_GPS_Exceptions_GFI_FLT_Asset 
                                FOREIGN KEY(AssetID) 
                                REFERENCES GFI_FLT_Asset(AssetID) 
	                     ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_Exceptions_GFI_FLT_Asset", "GFI_GPS_Exceptions")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_GPS_Exceptions 
                            ADD CONSTRAINT FK_GFI_GPS_Exceptions_GFI_FLT_Driver 
                                FOREIGN KEY(DriverID) 
                                REFERENCES GFI_FLT_Driver(DriverID) 
	                     ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_Exceptions_GFI_FLT_Driver", "GFI_GPS_Exceptions")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_GPS_Exceptions 
                            ADD CONSTRAINT FK_GFI_GPS_Exceptions_GFI_GPS_Rules 
                                FOREIGN KEY(RuleID) 
                                REFERENCES GFI_GPS_Rules(RuleID) 
	                     ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_Exceptions_GFI_GPS_Rules", "GFI_GPS_Exceptions")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_GPS_LiveData 
                            ADD CONSTRAINT FK_GFI_GPS_LiveData_GFI_FLT_Asset 
                                FOREIGN KEY(AssetID) 
                                REFERENCES GFI_FLT_Asset(AssetID) 
	                     ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_LiveData_GFI_FLT_Asset", "GFI_GPS_LiveData")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_GPS_LiveData 
                            ADD CONSTRAINT FK_GFI_GPS_LiveData_GFI_FLT_Driver 
                                FOREIGN KEY(DriverID) 
                                REFERENCES GFI_FLT_Driver(DriverID) 
	                     ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_LiveData_GFI_FLT_Driver", "GFI_GPS_LiveData")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_AutoReprotingConfigDetails 
                            ADD CONSTRAINT FK_ARCD_ARC 
                                FOREIGN KEY(ARID) 
                                REFERENCES GFI_FLT_AutoReportingConfig(ARID) 
	                     ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_ARCD_ARC", "GFI_FLT_AutoReprotingConfigDetails")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "Referential Constraints");
                }
                #endregion
                #region Update 64. Odo and Hr Meter
                strUpd = "Rez000064";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[GetCalcOdo]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
                            DROP FUNCTION GetCalcOdo]";
                    dbExecute(sql);
                    sql = @"Create function GetCalcOdo] (@AssetID int)
                        returns int
                        as Begin
                        
                        DECLARE @fOdo float
                        
                        SET @fOdo = 
                        (
	                        select COALESCE(SUM(h.TripDistance),0) 
		                        + COALESCE(
			                        (select top 1 ActualOdometer 
				                        from GFI_AMM_VehicleMaintenance 
				                        where AssetId = @AssetID 
					                        and ActualOdometer is not null
					                        and ActualOdometer > 0
				                        order by URI desc
			                        ),0)
	                        from GFI_GPS_TripHeader h
		                        where h.AssetID = @AssetID 
			                        and h.dtEnd >= 
			                        COALESCE(
					                        (select top 1 EndDate 
						                        from GFI_AMM_VehicleMaintenance 
						                        where AssetId = @AssetID 
							                        and ActualOdometer is not null
							                        and ActualOdometer > 0
						                        order by URI desc
					                        ),DATEADD(YY, -50, GETDATE()))
                        )
                        
                        return CAST(@fOdo AS int)
                        end";
                    dbExecute(sql);

                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[GetCalcEngHrs]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
                            DROP FUNCTION GetCalcEngHrs]";
                    dbExecute(sql);
                    sql = @"CREATE function GetCalcEngHrs] (@AssetID int)
                        returns int
                        as Begin
                        
                        DECLARE @fEng float
                        
                        set @fEng = 
                        (
	                        select COALESCE(SUM(h.TripTime),0) 
		                        + COALESCE(
			                        (select top 1 ActualEngineHrs 
				                        from GFI_AMM_VehicleMaintenance 
				                        where AssetId = @AssetID 
					                        and ActualEngineHrs is not null
					                        and ActualEngineHrs > 0
				                        order by URI desc
			                        ),0)
	                        from GFI_GPS_TripHeader h
		                        where h.AssetID = @AssetID 
			                        and h.dtEnd >= 
			                        COALESCE(
					                        (select top 1 EndDate 
						                        from GFI_AMM_VehicleMaintenance 
						                        where AssetId = @AssetID 
							                        and ActualEngineHrs is not null
							                        and ActualEngineHrs > 0
						                        order by URI desc
					                        ),DATEADD(YY, -50, GETDATE()))
                        )
                        
                        return CAST(@fEng AS int)
                        end";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "Odo and Hr Meter optimized");
                }
                #endregion
                #region Update 65. GFI_SYS_Notification updated
                strUpd = "Rez000065";
                if (!CheckUpdateExist(strUpd))
                {
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "Driver")).Rows.Count == 0)
                    {
                        sql = "truncate table GFI_SYS_Notification";
                        dbExecute(sql);

                        sql = @"alter table GFI_SYS_Notification add 
	                    UIDCategory nvarchar(10) not null
                        , ReportID int null
	                    , Lat float null
	                    , Lon float null
	                    , Driver nvarchar(100) null
	                    , Asset nvarchar(100) null
	                    , RuleName nvarchar(100) null
	                    , email nvarchar(50) null
	                    , DateTimeGPS_UTC datetime null
	                    , name varchar(100) null";
                        dbExecute(sql);
                    }

                    sql = @"ALTER TABLE GFI_FLT_AutoReportingConfig 
                            DROP FOREIGN KEY FK_GFI_FLT_ARC_GFI_GPS_Rules";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ARC_GFI_GPS_Rules", "GFI_FLT_AutoReportingConfig")).Rows.Count > 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "GFI_SYS_Notification updated");
                }
                #endregion
                #region Update 66. Odo and LiveMap enhanced
                strUpd = "Rez000066";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "DROP INDEX myIX_LiveData] ON GFI_GPS_LiveData]";
                    if (GetDataDT(ExistsIndex("GFI_GPS_LiveData", "myIX_LiveData")).Rows.Count > 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX myIX_LiveData] ON GFI_GPS_LiveData]
                        (
	                        LongLatValidFlag] ASC,
	                        DateTimeGPS_UTC] ASC
                        )
                        INCLUDE ( 	
                            UID],
	                        AssetID],
	                        DateTimeServer],
	                        Longitude],
	                        Latitude],
	                        Speed],
	                        EgineOn],
	                        StopFlag],
	                        TripDistance],
	                        TripTime],
	                        DriverID]
                                )";
                    if (GetDataDT(ExistsIndex("GFI_GPS_LiveData", "myIX_LiveData")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "DROP INDEX MyIXCalOdoEng] ON GFI_GPS_TripHeader]";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "MyIXCalOdoEng")).Rows.Count > 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX MyIXCalOdoEng
                            ON GFI_GPS_TripHeader] ([AssetID])
                            INCLUDE ([TripDistance],[TripTime],[dtEnd])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "MyIXCalOdoEng")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[GetCalcOdo]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
                            DROP FUNCTION GetCalcOdo]";
                    dbExecute(sql);
                    sql = @"Create function GetCalcOdo] (@AssetID int)
                            returns int
                            as Begin
                        
                            DECLARE @fOdo float
                        
                            ;with cta as
                            (
                                select *, 
                                    row_number() over(partition by AssetID order by URI desc) as RowNum
	                            from GFI_AMM_VehicleMaintenance
	                            where AssetId = @AssetID
		                            and ActualOdometer is not null
		                            and ActualOdometer > 0
                            )
                            select @fOdo = COALESCE(SUM(h.TripDistance),0) + COALESCE(max(cta.ActualOdometer),0)
                            from GFI_GPS_TripHeader h
                            left outer join cta on h.AssetID = cta.AssetId and cta.RowNum = 1
	                            where 
		                             h.dtEnd >= COALESCE(cta.EndDate,DATEADD(YY, -50, GETDATE()))
		                             and h.AssetId = @AssetID

                            return CAST(@fOdo AS int)
                            end";
                    dbExecute(sql);

                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[GetCalcEngHrs]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
                            DROP FUNCTION GetCalcEngHrs]";
                    dbExecute(sql);
                    sql = @"CREATE function GetCalcEngHrs] (@AssetID int)
                            returns int
                            as Begin
                        
                            DECLARE @fEng float
                        
                            ;with cta as
                            (
                                select *, 
                                    row_number() over(partition by AssetID order by URI desc) as RowNum
	                            from GFI_AMM_VehicleMaintenance
	                            where AssetId = @AssetID
		                            and ActualEngineHrs is not null
		                            and ActualEngineHrs > 0
                            )
                            select @fEng = COALESCE(SUM(h.TripTime),0) + COALESCE(max(cta.ActualEngineHrs),0)
                            from GFI_GPS_TripHeader h
                            left outer join cta on h.AssetID = cta.AssetId and cta.RowNum = 1
	                            where 
		                             h.dtEnd >= COALESCE(cta.EndDate,DATEADD(YY, -50, GETDATE()))
		                             and h.AssetId = @AssetID

                            return CAST(@fEng AS int)
                            end";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "Odo and LiveMap enhanced");
                }
                #endregion
                #region Update 67. ARC Additional Fields and LiveMap index
                strUpd = "Rez000067";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "DROP INDEX myIX_LiveData] ON GFI_GPS_LiveData]";
                    if (GetDataDT(ExistsIndex("GFI_GPS_LiveData", "myIX_LiveData")).Rows.Count > 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX myIX_LiveData] ON GFI_GPS_LiveData]
                        (
	                        AssetID] ASC,
	                        DateTimeGPS_UTC] ASC
                        )
                        INCLUDE ( 	
                            UID],
	                        DateTimeServer],
	                        Longitude],
	                        Latitude],
	                        LongLatValidFlag],	                        
                            Speed],
	                        EgineOn],
	                        StopFlag],
	                        TripDistance],
	                        TripTime],
	                        DriverID]
                                )";
                    if (GetDataDT(ExistsIndex("GFI_GPS_LiveData", "myIX_LiveData")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "DROP INDEX MyIX_LiveDataDetail] ON GFI_GPS_LiveDataDetail] ";
                    if (GetDataDT(ExistsIndex("GFI_GPS_LiveDataDetail", "MyIX_LiveDataDetail")).Rows.Count > 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX MyIX_LiveDataDetail
                            ON GFI_GPS_LiveDataDetail] ([UID])
                            INCLUDE ([GPSDetailID])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_LiveDataDetail", "MyIX_LiveDataDetail")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);


                    sql = "ALTER TABLE GFI_FLT_AutoReportingConfig] add ReportType] nvarchar](25) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AutoReportingConfig", "ReportType")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_FLT_AutoReportingConfig] add SaveToDisk] int NOT NULL Default 0";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AutoReportingConfig", "SaveToDisk")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = "ALTER TABLE GFI_FLT_AutoReportingConfig] add ReportPath] nvarchar](250) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AutoReportingConfig", "ReportPath")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_AutoReportingConfig ADD GridXML nvarchar(max) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AutoReportingConfig", "GridXML")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "ARC Additional Fields and LiveMap index");
                }
                #endregion
                #region Update 68. Trip and Exception enhanced.
                strUpd = "Rez000068";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "DROP INDEX MyIXTripProcess] ON GFI_GPS_TripHeader] ";
                    if (GetDataDT(ExistsIndex("[GFI_GPS_TripHeader]", "MyIXTripProcess")).Rows.Count > 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX MyIXTripProcess
                            ON GFI_GPS_TripHeader] ([AssetID],[StopTime])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "MyIXTripProcess")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"update GFI_GPS_Rules set StrucType = struc where Struc = 'Speed' and StrucType = ''";
                    dbExecute(sql);

                    sql = @"update GFI_GPS_Rules set MinTriggerValue = 2 
	                    where StrucCondition = '<=' 
		                    and ParentRuleID in (select ParentRuleID from GFI_GPS_Rules where Struc = 'Speed' group by ParentRuleID having COUNT(ParentRuleID) > 1)";
                    dbExecute(sql);

                    sql = @"update GFI_GPS_Rules set StrucType = 'Idle'
	                        where Struc = 'Speed' and StrucValue = '5' and StrucCondition= '<=' and StrucType = ''";
                    dbExecute(sql);

                    sql = @"update GFI_GPS_Rules set StrucType = 'Stopped'
	                        where Struc = 'Speed' and StrucValue= '0' and StrucCondition= '=' and StrucType = ''";
                    dbExecute(sql);

                    User u = new User();
                    u = u.GetUserByeMail("ADMIN@naveo.mu", "SysInitPass", sConnStr, false, true);
                    u.Password = "NewCore4dmin@123";
                    u.Update(u, sConnStr);

                    InsertDBUpdate(strUpd, "Trip and Exception enhanced");
                }
                #endregion
                #region Update 69. ARC data initialized. SMPT settings added
                strUpd = "Rez000069";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "update GFI_FLT_AutoReportingConfig set ReportType = 'EXCEL' where ReportType = ''";
                    dbExecute(sql);

                    sql = "update GFI_FLT_AutoReportingConfig set ReportType = 'EXCEL' where ReportType is null";
                    dbExecute(sql);

                    GlobalParams g = new GlobalParams();
                    g.ParamName = "SmtpClientHost";
                    g.PValue = "smtp.gmail.com";
                    g.ParamComments = "Name or IP of host used for SMTP Transactions";
                    g.Save(g, true, sConnStr);

                    g = new GlobalParams();
                    g.ParamName = "SmtpClientPort";
                    g.PValue = "587";
                    g.ParamComments = "Port used for SMTP Transactions";
                    g.Save(g, true, sConnStr);

                    g = new GlobalParams();
                    g.ParamName = "SmtpClientEnableSsl";
                    g.PValue = "1";
                    g.ParamComments = "1 True, 0 False";
                    g.Save(g, true, sConnStr);

                    g = new GlobalParams();
                    g.ParamName = "SmtpClientUsrName";
                    g.PValue = "NaveoOne@gmail.com";
                    g.ParamComments = "UserName used for SMTP Transactions";
                    g.Save(g, true, sConnStr);

                    g = new GlobalParams();
                    g.ParamName = "PwdSmtpClientUsr";
                    g.PValue = "ebs4dmin";
                    g.ParamComments = "Corresponding password used for SMTP Transactions";
                    g.Save(g, true, sConnStr);

                    g = new GlobalParams();
                    g.ParamName = "SmtpClientSenderMail";
                    g.PValue = "naveo.mu1@gmail.com";
                    g.ParamComments = "Serder appearing on mail";
                    g.Save(g, true, sConnStr);

                    InsertDBUpdate(strUpd, "ARC data initialized. SMPT settings added");
                }
                #endregion
                #region Update 70. GFI_GPS_ProcessErrors and Notif updated
                strUpd = "Rez000070";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "DROP INDEX MyIdx_TD_Del] ON GFI_GPS_TripDetail] ";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripDetail", "MyIdx_TD_Del")).Rows.Count > 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    //sql = @"CREATE  INDEX MyIdx_TD_Del
                    //            ON GFI_GPS_TripDetail] ([HeaderiID]) INCLUDE ([iID],[GPSDataUID])";
                    //if (GetDataDT(ExistsIndex("GFI_GPS_TripDetail", "MyIdx_TD_Del")).Rows.Count == 0)
                    //    dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"ALTER TABLE GFI_SYS_User ADD AccessTemplateId int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "AccessTemplateId")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD LoginAttempt int Not NULL Default 1";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "LoginAttempt")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD PreviousPassword nvarchar](50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "PreviousPassword")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD PasswordUpdateddate DateTime NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "PasswordUpdateddate")).Rows.Count == 0)
                        dbExecute(sql);

                    User u = new User();
                    u = u.GetUserByeMail("Support@naveo.mu", "SysInitPass", sConnStr, false, false);
                    GroupMatrix gmRoot = new GroupMatrix();
                    if (u.QryResult == User.UserResult.InvalidCredentials && u.UID == -999)
                    {
                        gmRoot = new NaveoOneLib.Services.GroupMatrixService().GetRoot(sConnStr);

                        u = new User();
                        u.Username = "NaveoSupport";
                        u.Email = "Support@naveo.mu";
                        u.Names = "NaveoSupport";
                        u.Password = "ebs@3Admin";
                        u.Status_b = "AC";

                        Matrix m = new Matrix();
                        m.GMID = gmRoot.gmID;
                        m.oprType = DataRowState.Added;
                        u.lMatrix.Add(m);

                        u.Save(u, sConnStr, true);
                    }

                    sql = @"CREATE TABLE GFI_GPS_ProcessErrors](
	                        iID] int] AUTO_INCREMENT NOT NULL,
	                        sError] nvarchar](max) NULL,
	                        sOrigine] nchar](10) NOT NULL,
	                        sFieldName] nchar](50) NULL,
	                        sFieldValue] nchar](50) NULL,
	                        AssetID] int null,
	                        ServerDate] datetime Default GetDate(),
	                        dtUTC] datetime null,
                         CONSTRAINT PK_GFI_GPS_ProcessErrors] PRIMARY KEY  ([iID] ASC)
                        )";
                    if (GetDataDT(ExistTableSql("GFI_GPS_ProcessErrors")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_Notification ADD ParentRuleID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "ParentRuleID")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_Notification ADD GPSDataUID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "GPSDataUID")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "GFI_GPS_ProcessErrors and Notif updated");
                }
                #endregion
                #region Update 71. Notif updated. New Menu
                strUpd = "Rez000071";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_SYS_Notification ADD Err nvarchar(MAX)";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "Err")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_SYS_MenuGroup]
                        (
	                        MenuId] int] NOT NULL,
	                        MenuName] nvarchar](50) NULL,
	                        CONSTRAINT PK_GFI_SYS_MenuGroup] PRIMARY KEY  ([MenuId] ASC)
                        ) 
                        ";
                    if (GetDataDT(ExistTableSql("GFI_SYS_MenuGroup")).Rows.Count == 0)
                    {
                        dbExecute(sql);

                        sql = "insert GFI_SYS_MenuGroup (MenuID, MenuName) values (1, 'Functions')";
                        dbExecute(sql);

                        sql = "insert GFI_SYS_MenuGroup (MenuID, MenuName) values (2, 'Reports')";
                        dbExecute(sql);

                        sql = "insert GFI_SYS_MenuGroup (MenuID, MenuName) values (3, 'Settings')";
                        dbExecute(sql);
                    }

                    sql = @"CREATE TABLE GFI_SYS_MenuHeader]
                        (
	                        MenuHeaderId] int] NOT NULL,
	                        MenuHeader] nvarchar](50) NULL,
	                        CONSTRAINT PK_GFI_SYS_MenuHeader] PRIMARY KEY  
	                        (
		                        MenuHeaderId] ASC
	                        )
                        )
                        ";
                    if (GetDataDT(ExistTableSql("GFI_SYS_MenuHeader")).Rows.Count == 0)
                    {
                        dbExecute(sql);

                        sql = "insert GFI_SYS_MenuHeader (MenuHeaderId, MenuHeader) values (1, 'Maps & GIS')";
                        dbExecute(sql);
                        sql = "insert GFI_SYS_MenuHeader (MenuHeaderId, MenuHeader) values (2, 'Vehicles & Assets')";
                        dbExecute(sql);
                        sql = "insert GFI_SYS_MenuHeader (MenuHeaderId, MenuHeader) values (3, 'Fuel & Telemetry')";
                        dbExecute(sql);
                        sql = "insert GFI_SYS_MenuHeader (MenuHeaderId, MenuHeader) values (4, 'Geofencing')";
                        dbExecute(sql);
                        sql = "insert GFI_SYS_MenuHeader (MenuHeaderId, MenuHeader) values (5, 'Rules & Alerts')";
                        dbExecute(sql);
                        sql = "insert GFI_SYS_MenuHeader (MenuHeaderId, MenuHeader) values (6, 'Settings')";
                        dbExecute(sql);
                    }

                    sql = @"ALTER TABLE GFI_SYS_Modules ADD MenuHeaderId int";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Modules", "MenuHeaderId")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_SYS_Modules ADD MenuGroupId int";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Modules", "MenuGroupId")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_SYS_Modules ADD OrderIndex int";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Modules", "OrderIndex")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_SYS_Modules ADD Title nvarchar(200)";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Modules", "Title")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_SYS_Modules ADD Summary nvarchar(200)";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Modules", "Summary")).Rows.Count == 0)
                        dbExecute(sql);

                    #region Module
                    NaveoOneLib.Models.Module m = new NaveoOneLib.Models.Module();
                    m = m.GetModulesByName("Maps & GIS -->Function -->Live Map", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Maps & GIS -->Function -->Live Map";
                        m.Description = "Maps & GIS -->Function -->Live Map";
                        m.Command = "CmdLiveMap";
                        m.MenuHeaderId = 1;
                        m.MenuGroupId = 1;
                        m.OrderIndex = 1;
                        m.Title = "Live Map";
                        m.Summary = "A live geographical view of all vehicles and other connected assets";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Maps & GIS -->Function -->Trip History", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Maps & GIS -->Function -->Trip History";
                        m.Description = "Maps & GIS -->Function -->Trip History";
                        m.Command = "CmdTripHistory";
                        m.MenuHeaderId = 1;
                        m.MenuGroupId = 1;
                        m.OrderIndex = 2;
                        m.Title = "Trip History";
                        m.Summary = "Detailed vehicle/asset activity over a period of time";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Maps & GIS -->Reports -->Trip Summary", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Maps & GIS -->Reports -->Trip Summary";
                        m.Description = "Maps & GIS -->Reports -->Trip Summary";
                        m.Command = "CmdTripSummary";
                        m.MenuHeaderId = 1;
                        m.MenuGroupId = 2;
                        m.OrderIndex = 1;
                        m.Title = "Trip Summary";
                        m.Summary = "Summarized activity data for moving assets";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Vehicles & Assets -->Functions -->Maintenance Dashboard", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Vehicles & Assets -->Functions -->Maintenance Dashboard";
                        m.Description = "Vehicles & Assets -->Functions -->Maintenance Dashboard";
                        m.Command = "CmdMaintenance";
                        m.MenuHeaderId = 2;
                        m.MenuGroupId = 1;
                        m.OrderIndex = 1;
                        m.Title = "Maintenance Dashboard";
                        m.Summary = "Status of ongoing maintenance jobs";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Vehicles & Assets -->Functions -->Bulk Maintenance Assignment", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Vehicles & Assets -->Functions -->Bulk Maintenance Assignment";
                        m.Description = "Vehicles & Assets -->Functions -->Bulk Maintenance Assignment";
                        m.Command = "CmdBulkMaintenance";
                        m.MenuHeaderId = 2;
                        m.MenuGroupId = 1;
                        m.OrderIndex = 2;
                        m.Title = "Bulk Maintenance Assignment";
                        m.Summary = "An easy tool to assign maintenancce type to multiple assets";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Vehicles & Assets -->Functions -->Asset Master Details", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Vehicles & Assets -->Functions -->Asset Master Details";
                        m.Description = "Vehicles & Assets -->Functions -->Asset Master Details";
                        m.Command = "CmdAssetMasterDetails";
                        m.MenuHeaderId = 2;
                        m.MenuGroupId = 1;
                        m.OrderIndex = 3;
                        m.Title = "Asset Master Details";
                        m.Summary = "Vehicle/asset masterfile details";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Vehicles & Assets -->Functions -->Asset Master Details", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Vehicles & Assets -->Functions -->Asset Master Details";
                        m.Description = "Vehicles & Assets -->Functions -->Asset Master Details";
                        m.Command = "CmdAssetMasterDetails";
                        m.MenuHeaderId = 2;
                        m.MenuGroupId = 1;
                        m.OrderIndex = 3;
                        m.Title = "Asset Master Details";
                        m.Summary = "Vehicle/asset masterfile details";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Vehicles & Assets -->Reports -->Maintenance Cost", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Vehicles & Assets -->Reports -->Maintenance Cost";
                        m.Description = "Vehicles & Assets -->Reports -->Maintenance Cost";
                        m.Command = "CmdMaintenanceCost";
                        m.MenuHeaderId = 2;
                        m.MenuGroupId = 2;
                        m.OrderIndex = 1;
                        m.Title = "Maintenance Cost";
                        m.Summary = "Detailed vehicle/asset maintenance expenses over a period of time";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Vehicles & Assets -->Reports -->Maintenance Cost", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Vehicles & Assets -->Reports -->Maintenance Cost";
                        m.Description = "Vehicles & Assets -->Reports -->Maintenance Cost";
                        m.Command = "CmdMaintenanceCost";
                        m.MenuHeaderId = 2;
                        m.MenuGroupId = 2;
                        m.OrderIndex = 1;
                        m.Title = "Maintenance Cost";
                        m.Summary = "Detailed vehicle/asset maintenance expenses over a period of time";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Vehicles & Assets -->Reports -->Asset Summary", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Vehicles & Assets -->Reports -->Asset Summary";
                        m.Description = "Vehicles & Assets -->Reports -->Asset Summary";
                        m.Command = "CmdAssetSummary";
                        m.MenuHeaderId = 2;
                        m.MenuGroupId = 2;
                        m.OrderIndex = 2;
                        m.Title = "Asset Summary";
                        m.Summary = "Summarized asset activity data over a period of time";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Vehicles & Assets -->Reports -->Daily Summary", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Vehicles & Assets -->Reports -->Daily Summary";
                        m.Description = "Vehicles & Assets -->Reports -->Daily Summary";
                        m.Command = "CmdDailySummary";
                        m.MenuHeaderId = 2;
                        m.MenuGroupId = 2;
                        m.OrderIndex = 3;
                        m.Title = "Daily Summary";
                        m.Summary = "Summarized daily asset activity data";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Vehicles & Assets -->Settings -->Maintenance Types", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Vehicles & Assets -->Settings -->Maintenance Types";
                        m.Description = "Vehicles & Assets -->Settings -->Maintenance Types";
                        m.Command = "CmdMaintenanceTypes";
                        m.MenuHeaderId = 2;
                        m.MenuGroupId = 3;
                        m.OrderIndex = 1;
                        m.Title = "Maintenance Types";
                        m.Summary = "Add/maintain maintenance types";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Vehicles & Assets -->Settings -->Maintenance Status", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Vehicles & Assets -->Settings -->Maintenance Status";
                        m.Description = "Vehicles & Assets -->Settings -->Maintenance Status";
                        m.Command = "CmdMaintenanceStatus";
                        m.MenuHeaderId = 2;
                        m.MenuGroupId = 3;
                        m.OrderIndex = 2;
                        m.Title = "Maintenance Status";
                        m.Summary = "Add/maintain maintenance status workflow";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Vehicles & Assets -->Settings -->Vehicle Types", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Vehicles & Assets -->Settings -->Vehicle Types";
                        m.Description = "Vehicles & Assets -->Settings -->Vehicle Types";
                        m.Command = "CmdVehicleTypes";
                        m.MenuHeaderId = 2;
                        m.MenuGroupId = 3;
                        m.OrderIndex = 3;
                        m.Title = "Vehicle Types";
                        m.Summary = "Add/maintain vehicle types";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Vehicles & Assets -->Settings -->Insurance Cover Types", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Vehicles & Assets -->Settings -->Insurance Cover Types";
                        m.Description = "Vehicles & Assets -->Settings -->Insurance Cover Types";
                        m.Command = "CmdInsuranceCoverTypes";
                        m.MenuHeaderId = 2;
                        m.MenuGroupId = 3;
                        m.OrderIndex = 4;
                        m.Title = "Insurance Cover Types";
                        m.Summary = "Add/maintain insurance cover types";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Vehicles & Assets -->Settings -->Assets Extended Properties", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Vehicles & Assets -->Settings -->Assets Extended Properties";
                        m.Description = "Vehicles & Assets -->Settings -->Assets Extended Properties";
                        m.Command = "CmdAssetsExtendedProperties";
                        m.MenuHeaderId = 2;
                        m.MenuGroupId = 3;
                        m.OrderIndex = 5;
                        m.Title = "Assets Extended Properties";
                        m.Summary = "Assets Extended Properties";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Fuel & Telemetry -->Functions -->Tank Levels", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Fuel & Telemetry -->Functions -->Tank Levels";
                        m.Description = "Fuel & Telemetry -->Functions -->Tank Levels";
                        m.Command = "CmdTankLevels";
                        m.MenuHeaderId = 3;
                        m.MenuGroupId = 1;
                        m.OrderIndex = 1;
                        m.Title = "Tank Levels";
                        m.Summary = "Online status of connected fixed tanks";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Fuel & Telemetry -->Functions -->Fuel Data Entry", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Fuel & Telemetry -->Functions -->Fuel Data Entry";
                        m.Description = "Fuel & Telemetry -->Functions -->Fuel Data Entry";
                        m.Command = "CmdFuelDataEntry";
                        m.MenuHeaderId = 3;
                        m.MenuGroupId = 1;
                        m.OrderIndex = 2;
                        m.Title = "Fuel Data Entry";
                        m.Summary = "Fuel Data Entry";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Fuel & Telemetry -->Reports -->Fuel Graph", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Fuel & Telemetry -->Reports -->Fuel Graph";
                        m.Description = "Fuel & Telemetry -->Reports -->Fuel Graph";
                        m.Command = "CmdFuelGraph";
                        m.MenuHeaderId = 3;
                        m.MenuGroupId = 2;
                        m.OrderIndex = 1;
                        m.Title = "Fuel Graph";
                        m.Summary = "Graphical historic data for connected fuel sensors (moving assets)";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Fuel & Telemetry -->Reports -->Fuel Consumption", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Fuel & Telemetry -->Reports -->Fuel Consumption";
                        m.Description = "Fuel & Telemetry -->Reports -->Fuel Consumption";
                        m.Command = "CmdFuelConsumption";
                        m.MenuHeaderId = 3;
                        m.MenuGroupId = 2;
                        m.OrderIndex = 2;
                        m.Title = "Fuel Consumption";
                        m.Summary = "Vehicle/asset fuel consumption data over a period of time";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Fuel & Telemetry -->Reports -->Temperature Graph", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Fuel & Telemetry -->Reports -->Temperature Graph";
                        m.Description = "Fuel & Telemetry -->Reports -->Temperature Graph";
                        m.Command = "CmdTemperatureGraph";
                        m.MenuHeaderId = 3;
                        m.MenuGroupId = 2;
                        m.OrderIndex = 3;
                        m.Title = "Temperature Graph";
                        m.Summary = "Graphical historic data for connected temperature sensors";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Fuel & Telemetry -->Reports -->Auxilliary Report", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Fuel & Telemetry -->Reports -->Auxilliary Report";
                        m.Description = "Fuel & Telemetry -->Reports -->Auxilliary Report";
                        m.Command = "CmdAuxilliaryReport";
                        m.MenuHeaderId = 3;
                        m.MenuGroupId = 2;
                        m.OrderIndex = 4;
                        m.Title = "Auxilliary Report";
                        m.Summary = "Output data received from connected auxilliaries/sensors";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Geofencing -->Functions -->Create new zone", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Geofencing -->Functions -->Create new zone";
                        m.Description = "Geofencing -->Functions -->Create new zone";
                        m.Command = "CmdCreatenewzone";
                        m.MenuHeaderId = 4;
                        m.MenuGroupId = 1;
                        m.OrderIndex = 1;
                        m.Title = "Create new zone";
                        m.Summary = "Add/maintain geofences on the map";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Geofencing -->Reports -->Zones Visited", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Geofencing -->Reports -->Zones Visited";
                        m.Description = "Geofencing -->Reports -->Zones Visited";
                        m.Command = "CmdZonesVisited";
                        m.MenuHeaderId = 4;
                        m.MenuGroupId = 2;
                        m.OrderIndex = 1;
                        m.Title = "Zones Visited";
                        m.Summary = "List of zones visited over a period of time";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Geofencing -->Reports -->Selective Zones Visited", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Geofencing -->Reports -->Selective Zones Visited";
                        m.Description = "Geofencing -->Reports -->Selective Zones Visited";
                        m.Command = "CmdSelectiveZonesVisited";
                        m.MenuHeaderId = 4;
                        m.MenuGroupId = 2;
                        m.OrderIndex = 2;
                        m.Title = "Selective Zones Visited";
                        m.Summary = "List of zones visited over a period of time based on user selection";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Geofencing -->Reports -->Zones Visited Summarized", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Geofencing -->Reports -->Zones Visited Summarized";
                        m.Description = "Geofencing -->Reports -->Zones Visited Summarized";
                        m.Command = "CmdZonesVisitedSummarized";
                        m.MenuHeaderId = 4;
                        m.MenuGroupId = 2;
                        m.OrderIndex = 3;
                        m.Title = "Zones Visited Summarized";
                        m.Summary = "Summary of zones visited (count)";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Geofencing -->Settings -->Zone Types", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Geofencing -->Settings -->Zone Types";
                        m.Description = "Geofencing -->Settings -->Zone Types";
                        m.Command = "CmdZoneTypes";
                        m.MenuHeaderId = 4;
                        m.MenuGroupId = 3;
                        m.OrderIndex = 1;
                        m.Title = "Zone Types";
                        m.Summary = "Add/maintain groups of zones";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Rules & Alerts -->Functions -->Reprocess Exception Rules", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Rules & Alerts -->Functions -->Reprocess Exception Rules";
                        m.Description = "Rules & Alerts -->Functions -->Reprocess Exception Rules";
                        m.Command = "CmdReprocessExceptionRules";
                        m.MenuHeaderId = 5;
                        m.MenuGroupId = 1;
                        m.OrderIndex = 1;
                        m.Title = "Reprocess Exception Rules";
                        m.Summary = "A tool to apply a rule to historic data";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Rules & Alerts -->Functions -->Test Automatic Reporting", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Rules & Alerts -->Functions -->Test Automatic Reporting";
                        m.Description = "Rules & Alerts -->Functions -->Test Automatic Reporting";
                        m.Command = "CmdTestAutomaticReporting";
                        m.MenuHeaderId = 5;
                        m.MenuGroupId = 1;
                        m.OrderIndex = 2;
                        m.Title = "Test Automatic Reporting";
                        m.Summary = "A tool to test the proper functioning of automatic reports";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Rules & Alerts -->Reports -->Exceptions Broken", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Rules & Alerts -->Reports -->Exceptions Broken";
                        m.Description = "Rules & Alerts -->Reports -->Exceptions Broken";
                        m.Command = "CmdExceptionsBroken";
                        m.MenuHeaderId = 5;
                        m.MenuGroupId = 2;
                        m.OrderIndex = 1;
                        m.Title = "Exceptions Broken";
                        m.Summary = "List of rules broken over a period of time";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Rules & Alerts -->Settings -->Setup New Rules", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Rules & Alerts -->Settings -->Setup New Rules";
                        m.Description = "Rules & Alerts -->Settings -->Setup New Rules";
                        m.Command = "CmdSetupNewRules";
                        m.MenuHeaderId = 5;
                        m.MenuGroupId = 3;
                        m.OrderIndex = 1;
                        m.Title = "Setup New Rules";
                        m.Summary = "Add/maintain rules";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Rules & Alerts -->Settings -->Setup Automatic Reporting", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Rules & Alerts -->Settings -->Setup Automatic Reporting";
                        m.Description = "Rules & Alerts -->Settings -->Setup Automatic Reporting";
                        m.Command = "CmdSetupAutomaticReporting";
                        m.MenuHeaderId = 5;
                        m.MenuGroupId = 3;
                        m.OrderIndex = 2;
                        m.Title = "Setup Automatic Reporting";
                        m.Summary = "Add/maintain automatic reports";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Settings -->Settings -->User Management", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Settings -->Settings -->User Management";
                        m.Description = "Settings -->Settings -->User Management";
                        m.Command = "CmdUserManagement";
                        m.MenuHeaderId = 6;
                        m.MenuGroupId = 3;
                        m.OrderIndex = 1;
                        m.Title = "User Management";
                        m.Summary = "Add/maintain users";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Settings -->Settings -->User Access Templates", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Settings -->Settings -->User Access Templates";
                        m.Description = "Settings -->Settings -->User Access Templates";
                        m.Command = "CmdUserAccessTemplates";
                        m.MenuHeaderId = 6;
                        m.MenuGroupId = 3;
                        m.OrderIndex = 2;
                        m.Title = "User Access Templates";
                        m.Summary = "Add/maintain permissions for users";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Settings -->Settings -->Change Password", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Settings -->Settings -->Change Password";
                        m.Description = "Settings -->Settings -->Change Password";
                        m.Command = "CmdChangePassword";
                        m.MenuHeaderId = 6;
                        m.MenuGroupId = 3;
                        m.OrderIndex = 3;
                        m.Title = "Change Password";
                        m.Summary = "User-specific function for changing password";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Settings -->Settings -->Setup Drivers", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Settings -->Settings -->Setup Drivers";
                        m.Description = "Settings -->Settings -->Setup Drivers";
                        m.Command = "CmdSetupDrivers";
                        m.MenuHeaderId = 6;
                        m.MenuGroupId = 3;
                        m.OrderIndex = 4;
                        m.Title = "Setup Drivers";
                        m.Summary = "Add/maintain drivers";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Settings -->Settings -->Setup Assets", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Settings -->Settings -->Setup Assets";
                        m.Description = "Settings -->Settings -->Setup Assets";
                        m.Command = "CmdSetupAssets";
                        m.MenuHeaderId = 6;
                        m.MenuGroupId = 3;
                        m.OrderIndex = 5;
                        m.Title = "Setup Assets";
                        m.Summary = "Add/maintain assets";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }
                    #endregion
                    #region Access Templates
                    m = new NaveoOneLib.Models.Module();
                    DataTable dtAccessModules = m.GetModules(sConnStr);
                    DataTable dtAccessProfiles = AccessProfile.initAccessProfile();
                    foreach (DataRow dr in dtAccessModules.Rows)
                    {
                        DataRow drA = dtAccessProfiles.Rows.Add();
                        drA["ModuleId"] = dr["UID"];
                        drA["AllowRead"] = 1;
                        drA["AllowNew"] = 1;
                        drA["AllowEdit"] = 1;
                        drA["AllowDelete"] = 1;
                    }

                    AccessTemplate ac = new AccessTemplate();
                    ac.TemplateName = "System Default";
                    ac.Description = "System Default";
                    ac.CreatedDate = DateTime.Now;
                    ac.CreatedBy = usrInst.Username;
                    ac.UpdatedDate = Constants.NullDateTime;
                    ac.UpdatedBy = String.Empty;
                    ac.lMatrix = usrInst.lMatrix;

                    DataSet dsResult = new DataSet();
                    Boolean b = ac.Save(ac, true, dtAccessProfiles, out dsResult, sConnStr);
                    #endregion

                    InsertDBUpdate(strUpd, "Notif updated, New Menu");
                }

                #endregion
                #region Update 72. Telemetry.
                strUpd = "Rez000072";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"
                            CREATE TABLE GFI_TEL_TelDataHeader](
	                            iID] int] AUTO_INCREMENT NOT NULL,
	                            MsgHeader] nvarchar](10) NOT NULL,
	                            Type] nvarchar](20) NOT NULL,
	                            AssetID] int] NOT NULL,
	                            UTCdt] datetime] NOT NULL,
	                            MsgStatus] nvarchar](10) NOT NULL,
	                            isMaintenanceMode] int] NOT NULL,
	                            isFailedCallout] int] NOT NULL,
	                            Temperature] float] NOT NULL,
	                            isBatteryLow] int] NOT NULL,
	                            isAutoConfig] int] NOT NULL,
	                            Carrier] nvarchar](50) NOT NULL,
	                            SignalStrength] int] NOT NULL,
	                            CONSTRAINT PK_GFI_TEL_TelDataHeader] PRIMARY KEY  ([iID] ASC)
                            )";
                    if (GetDataDT(ExistTableSql("GFI_TEL_TelDataHeader")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"
                            CREATE TABLE GFI_TEL_TelDataDetail](
	                            iID] int] AUTO_INCREMENT NOT NULL,
	                            HeaderiID] int] NOT NULL,
	                            UTCdt] datetime] NULL,
	                            Sensor] int] NULL,
	                            SensorReading] float] NULL,
	                            CONSTRAINT PK_GFI_TEL_TelDataDetail] PRIMARY KEY  ([iID] ASC)
                            )";
                    if (GetDataDT(ExistTableSql("GFI_TEL_TelDataDetail")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_TEL_TelDataDetail] WITH CHECK 
                            ADD CONSTRAINT FK_GFI_TEL_TelDataDetail_GFI_TEL_TelDataHeader] 
                                FOREIGN KEY([HeaderiID])
                                REFERENCES GFI_TEL_TelDataHeader] (iID)
                            ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_TEL_TelDataDetail_GFI_TEL_TelDataHeader", "GFI_TEL_TelDataDetail")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_TEL_TelDataHeader 
                            ADD CONSTRAINT FK_GFI_TEL_TelDataHeader_GFI_FLT_Asset 
                                FOREIGN KEY(AssetID) 
                                REFERENCES GFI_FLT_Asset(AssetID) 
                            ON UPDATE  NO ACTION 
	                        ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_TEL_TelDataHeader_GFI_FLT_Asset", "GFI_TEL_TelDataHeader")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "Telemetry");
                }
                #endregion
                #region Update 73. Menu updated
                strUpd = "Rez000073";
                if (!CheckUpdateExist(strUpd))
                {
                    NaveoOneLib.Models.Module m = new NaveoOneLib.Models.Module();
                    m = m.GetModulesByName("Drivers & Passengers -->Reports -->Driver Activity", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Drivers & Passengers -->Reports -->Driver Activity";
                        m.Description = "Drivers & Passengers -->Reports -->Driver Activity";
                        m.Command = "cmdDriverActivity";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Drivers & Passengers -->Reports -->Passenger Activity", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Drivers & Passengers -->Reports -->Passenger Activity";
                        m.Description = "Drivers & Passengers -->Reports -->Passenger Activity";
                        m.Command = "cmdPassengerActivity";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Drivers & Passengers -->Reports -->Passenger Occupancy", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Drivers & Passengers -->Reports -->Passenger Occupancy";
                        m.Description = "Drivers & Passengers -->Reports -->Passenger Activity";
                        m.Command = "cmdPassengerOccupancy";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Drivers & Passengers -->Reports -->Driver Master Details", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Drivers & Passengers -->Reports -->Driver Master Details";
                        m.Description = "Drivers & Passengers -->Reports -->Driver Master Details";
                        m.Command = "cmdDriverMasterDetails";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Drivers & Passengers -->Reports -->Passenger Master Details", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Drivers & Passengers -->Reports -->Passenger Master Details";
                        m.Description = "Drivers & Passengers -->Reports -->Passenger Master Details";
                        m.Command = "cmdPassengerMasterDetail";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = m.GetModulesByName("Geofencing -->Reports -->Zones - Visited v/s Not Visited", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Geofencing -->Reports -->Zones - Visited v/s Not Visited";
                        m.Description = "Geofencing -->Reports -->Zones - Visited v/s Not Visited";
                        m.Command = "cmdZonesVisitedNotVisited";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    InsertDBUpdate(strUpd, "Menu updated");
                }
                #endregion
                #region Update 74. isPointInCircle and Archiving tables.
                strUpd = "Rez000074";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[isPointInCircle]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
                            DROP FUNCTION isPointInCircle]";
                    dbExecute(sql);
                    sql = @"
CREATE function isPointInCircle] (@CentreLon float, @CentreLat float, @Radius float, @toTestLg float, @toTestLt float)
returns int
as Begin
	Declare @iResult int
	set @iResult = 0

    Declare @dX float
    Declare @dY float
    Declare @sumOfSquares float
    Declare @distance float

	set @dX = Abs(@toTestLg - @CentreLon);
    set @dY = Abs(@toTestLt - @CentreLat);
    set @sumOfSquares = @dX * @dX + @dY * @dY;
    set @distance = Sqrt(@sumOfSquares);
    if (@Radius >= @distance)
		set @iResult = 1
	else
		set @iResult = 0		

	return @iResult
end
";
                    dbExecute(sql);

                    sql = @"CREATE TABLE GFI_ARC_GPSData]
(
	[UID] int] AUTO_INCREMENT NOT NULL,
	[AssetID] int] NULL,
	[DateTimeGPS_UTC] datetime] NULL,
	[DateTimeServer] datetime] NULL,
	[Longitude] float] NULL,
	[Latitude] float] NULL,
	[LongLatValidFlag] int] NULL,
	[Speed] float] NULL,
	[EgineOn] int] NULL,
	[StopFlag] int] NULL,
	[TripDistance] float] NULL,
	[TripTime] float] NULL,
	[WorkHour] int] NULL,
	[DriverID] int] NOT NULL DEFAULT ((1)),
 CONSTRAINT PK_GFI_ARC_GPSData] PRIMARY KEY  ([UID] ASC)
) ";
                    if (GetDataDT(ExistTableSql("GFI_ARC_GPSData")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"
ALTER TABLE GFI_ARC_GPSData]  
	ADD CONSTRAINT FK_GFI_ARC_GPSData_GFI_FLT_Asset] 
		FOREIGN KEY([AssetID]) REFERENCES GFI_FLT_Asset] ([AssetID])
	ON DELETE CASCADE
";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_ARC_GPSData_GFI_FLT_Asset")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"
ALTER TABLE GFI_ARC_GPSData]  
	ADD CONSTRAINT FK_GFI_ARC_GPSData_GFI_FLT_Driver] 
		FOREIGN KEY([DriverID]) REFERENCES GFI_FLT_Driver] ([DriverID])
	ON DELETE CASCADE
";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_ARC_GPSData_GFI_FLT_Driver")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"
CREATE TABLE GFI_ARC_GPSDataDetail]
(
	[GPSDetailID] bigint] AUTO_INCREMENT NOT NULL,
	[UID] int] NULL,
	[TypeID] nvarchar](30) NULL,
	[TypeVaule] nvarchar](30) NULL,
	[UOM] nvarchar](15) NULL,
	[Remarks] nvarchar](50) NULL,
 CONSTRAINT PK_GFI_ARC_GPSDataDetail] PRIMARY KEY  ([GPSDetailID] ASC)
)
";
                    if (GetDataDT(ExistTableSql("GFI_ARC_GPSDataDetail")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"
ALTER TABLE GFI_ARC_GPSDataDetail]  
	ADD  CONSTRAINT FK_GFI_ARC_GPSDataDetail_GFI_ARC_GPSData] 
		FOREIGN KEY([UID])
		REFERENCES GFI_ARC_GPSData] ([UID])
	ON DELETE CASCADE
";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_ARC_GPSDataDetail_GFI_ARC_GPSData")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE  INDEX myARCIdxDT] ON GFI_ARC_GPSData] ([DateTimeGPS_UTC] ASC) INCLUDE ([UID])";
                    if (GetDataDT(ExistsIndex("GFI_ARC_GPSData", "myARCIdxDT")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX my_IX_GFI_ARC_GPSDataDetail] ON GFI_ARC_GPSDataDetail] ([UID] ASC)";
                    if (GetDataDT(ExistsIndex("GFI_ARC_GPSDataDetail", "my_IX_GFI_ARC_GPSDataDetail")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"
CREATE TABLE GFI_ARC_TripHeader]
(
	[iID] int] AUTO_INCREMENT NOT NULL,
	[AssetID] int] NOT NULL,
	[DriverID] int] NOT NULL,
	[ExceptionFlag] int] NULL,
	[MaxSpeed] int] NULL,
	[IdlingTime] float] NULL,
	[StopTime] float] NULL,
	[TripDistance] float] NULL,
	[TripTime] float] NULL,
	[GPSDataStartUID] int] NULL,
	[GPSDataEndUID] int] NULL,
	[dtStart] datetime] NULL,
	[fStartLon] float] NULL,
	[fStartLat] float] NULL,
	[dtEnd] datetime] NULL,
	[fEndLon] float] NULL,
	[fEndtLat] float] NULL,
 CONSTRAINT PK_GFI_ARC_TripHeader] PRIMARY KEY  ([iID] ASC)
)";
                    if (GetDataDT(ExistTableSql("GFI_ARC_TripHeader")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"
ALTER TABLE GFI_ARC_TripHeader]
	ADD CONSTRAINT FK_GFI_ARC_TripHeader_GFI_FLT_Asset] 
		FOREIGN KEY([AssetID])
		REFERENCES GFI_FLT_Asset] ([AssetID])
	ON DELETE CASCADE";

                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_ARC_TripHeader_GFI_FLT_Asset")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"
CREATE TABLE GFI_ARC_TripDetail]
(
	[iID] bigint] AUTO_INCREMENT NOT NULL,
	[HeaderiID] int] NOT NULL,
	[GPSDataUID] int] NOT NULL,
 CONSTRAINT PK_GFI_ARC_TripDetail] PRIMARY KEY  ([iID] ASC)
)";
                    if (GetDataDT(ExistTableSql("GFI_ARC_TripDetail")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"
ALTER TABLE GFI_ARC_TripDetail] 
	ADD CONSTRAINT FK_GFI_ARC_TripDetail_GFI_ARC_TripHeader] 
		FOREIGN KEY([HeaderiID])
		REFERENCES GFI_ARC_TripHeader] ([iID])
	ON DELETE CASCADE
";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_ARC_TripDetail_GFI_ARC_TripHeader")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE  INDEX myIX_GFI_ARC_TripDetail_GPSDataUID] ON GFI_ARC_TripDetail] ([GPSDataUID] ASC) INCLUDE ([iID], HeaderiID]) ";
                    if (GetDataDT(ExistsIndex("GFI_ARC_TripDetail", "myIX_GFI_ARC_TripDetail_GPSDataUID")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);
                    sql = @"CREATE  INDEX my_IX_GFI_ARC_TripDetail] ON GFI_ARC_TripDetail] ([HeaderiID] ASC)";
                    if (GetDataDT(ExistsIndex("GFI_ARC_TripDetail", "my_IX_GFI_ARC_TripDetail")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);
                    sql = @"CREATE  INDEX MyIXDriver] ON GFI_ARC_TripHeader] ([DriverID] ASC) INCLUDE ([iID], GPSDataStartUID], GPSDataEndUID])";
                    if (GetDataDT(ExistsIndex("GFI_ARC_TripHeader", "MyIXDriver")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);
                    sql = @"CREATE  INDEX MyIXCalOdoEng] ON GFI_ARC_TripHeader]	([AssetID] ASC)	INCLUDE ([TripDistance], TripTime], dtEnd])";
                    if (GetDataDT(ExistsIndex("GFI_ARC_TripHeader", "MyIXCalOdoEng")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);
                    sql = @"CREATE  INDEX IX_Trip] ON GFI_ARC_TripHeader] ([dtStart] ASC, dtEnd] ASC)";
                    if (GetDataDT(ExistsIndex("GFI_ARC_TripHeader", "IX_Trip")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);
                    sql = @"CREATE  INDEX MyIXAsset] ON GFI_ARC_TripHeader] ([AssetID] ASC) INCLUDE ([iID], GPSDataStartUID], GPSDataEndUID])";
                    if (GetDataDT(ExistsIndex("GFI_ARC_TripHeader", "MyIXAsset")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "isPointInCircle and Archieving tables");
                }
                #endregion
                #region Update 75. Exceptions Table updated. Exception Archiving
                strUpd = "Rez000075";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"alter table GFI_GPS_Exceptions add InsertedAt datetime default GetUTCDate() not null";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_Exceptions", "InsertedAt")).Rows.Count == 0)
                    {
                        dbExecuteWithoutTransaction(sql, 4000);

                        sql = @"update GFI_GPS_Exceptions 
	                                set InsertedAt = g.DateTimeGPS_UTC
                                from GFI_GPS_Exceptions t1
	                                inner join GFI_GPS_GPSData g on g.UID = t1.GPSDataID";
                        dbExecuteWithoutTransaction(sql, 4000);
                    }

                    sql = @"
CREATE TABLE GFI_ARC_Exceptions]
(
	[iID] int] AUTO_INCREMENT NOT NULL,
	[RuleID] int] NULL,
	[GPSDataID] bigint] NULL,
	[AssetID] int] NULL,
	[DriverID] int] NULL,
	[Severity] nvarchar](100) NULL,
	[GroupID] int] NULL,
	[Posted] int] NOT NULL DEFAULT ((0)),
	[InsertedAt] datetime] NOT NULL DEFAULT (getutcdate()),
 CONSTRAINT PK_GFI_ARC_Exceptions] PRIMARY KEY  ([iID] ASC)
)";
                    if (GetDataDT(ExistTableSql("GFI_ARC_Exceptions")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"
ALTER TABLE GFI_ARC_Exceptions] 
	ADD CONSTRAINT FK_GFI_ARC_Exceptions_GFI_FLT_Asset] 
		FOREIGN KEY([AssetID]) REFERENCES GFI_FLT_Asset] ([AssetID])
	ON DELETE CASCADE
";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_ARC_Exceptions_GFI_FLT_Asset")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"
ALTER TABLE GFI_ARC_Exceptions] 
	ADD CONSTRAINT FK_GFI_ARC_Exceptions_GFI_GPS_Rules] 
		FOREIGN KEY([RuleID]) REFERENCES GFI_GPS_Rules] ([RuleID])
	ON DELETE CASCADE
";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_ARC_Exceptions_GFI_GPS_Rules")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "Exceptions Table updated. Exception Archiving");
                }
                #endregion

                #region Update 101 . ZoneXtPro Tables
                strUpd = "Rez000101";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_SYS_LoginAudit](
							[auditId] int] AUTO_INCREMENT NOT NULL,
							[auditUser] nvarchar](20) NULL,
							[auditDate] datetime] NULL,
							[auditType] nvarchar](2) NULL,
							[machineName] nvarchar](50) NULL,
							[clientGroupMachine] nvarchar](50) NULL,
							[PostedDate] datetime] NULL,
							[PostedFlag] nvarchar](1) NULL
						)";
                    if (GetDataDT(ExistTableSql("GFI_SYS_LoginAudit")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_SYS_Audit (
							[auditId] int] AUTO_INCREMENT NOT NULL,
							[auditUser] nvarchar](20) NULL,
							[auditDate] datetime] NULL,
							[auditType] nvarchar](2) NULL,
							ReferenceCode	 nvarchar](20) ,
							ReferenceDesc	 nvarchar](50) ,
							ReferenceTable	 nvarchar](20) ,
							CreatedDate	 datetime] NULL,
							CallerFunction	 nvarchar](20) ,
							SQLRemarks	 nvarchar](200) ,
							NewValue	 nvarchar](200) ,
							OldValue	 nvarchar](200) ,
							PostedDate	 datetime] NULL,
							PostedFlag	[nvarchar](1) 
						)";
                    if (GetDataDT(ExistTableSql("GFI_SYS_Audit")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_SYS_AuditAccess](
						 IId] int] AUTO_INCREMENT NOT NULL,
						 auditUser] nvarchar](20) NOT NULL,
						 Control] nvarchar](50) NOT NULL,
						 AuditDate] datetime] NOT NULL,
						  PostedDate	 datetime] NULL,
						  PostedFlag	[nvarchar](1) 
						)";

                    if (GetDataDT(ExistTableSql("GFI_SYS_AuditAccess")).Rows.Count == 0)
                        dbExecute(sql);
                    InsertDBUpdate(strUpd, "Audit Tables updated");
                }
                #endregion
                #region Update 103 . Active Users
                strUpd = "Rez000103";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_SYS_ActiveSession](
						 UID] int] NOT NULL,
						 Username] nvarchar](20) NOT NULL,
						 LoginDate] datetime] NOT NULL,
						 MachineName] nvarchar](50) NOT NULL,
						 UserProfile] nvarchar](50) NOT NULL,
						 Company] nvarchar](50) NOT NULL,
						 AppVersion] nvarchar](50) NOT NULL,
						 PRIMARY KEY  ([UID] ASC)
                           )
						";

                    if (GetDataDT(ExistTableSql("GFI_SYS_ActiveSession")).Rows.Count == 0)
                        dbExecute(sql);
                    InsertDBUpdate(strUpd, "Active User");
                }

                #endregion
                #region Update 104 . Extended trip info type
                strUpd = "Rez000104";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_FLT_ExtTripInfoType](
                                    IID] int] AUTO_INCREMENT NOT NULL,
                                    TypeName] varchar](20) NOT NULL,
                                    Field1Name] varchar](50) NOT NULL,
                                    Field1Label] varchar](50) NOT NULL,
                                    Field1Type] varchar](50) NOT NULL,
                                    Field2Name] nvarchar](50) NULL,
                                    Field2Label] varchar](50) NULL,
                                    Field2Type] varchar](50) NULL,
                                    CreatedBy] nvarchar](30) NOT NULL,
                                    CreatedDate] datetime] NOT NULL,
                                    UpdatedBy] nvarchar](30) NULL,
                                    UpdatedDate] datetime] NULL
                                   ) ";

                    if (GetDataDT(ExistTableSql("GFI_FLT_ExtTripInfoType")).Rows.Count == 0)
                        dbExecute(sql);
                    InsertDBUpdate(strUpd, "Extended trip info type");
                }

                #endregion

                #region Update 105 . Extended trip info type mapping
                strUpd = "Rez000105";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_FLT_ExtTripInfoMapping](
                                         IID] int] AUTO_INCREMENT NOT NULL,
                                         Description] nvarchar](255) NOT NULL,
                                         ExtTripInfoId] int] NOT NULL,
                                         CreatedBy] nvarchar](50) NULL,
                                         CreatedDate] datetime] NULL,
                                         UpdatedBy] nvarchar](50) NULL,
                                         UpdatedDate] datetime] NULL,
                                         MappingId] int] NOT NULL
                                        ) ";

                    if (GetDataDT(ExistTableSql("GFI_FLT_ExtTripInfoMapping")).Rows.Count == 0)
                        dbExecute(sql);
                    InsertDBUpdate(strUpd, "Extended trip info type mapping");
                }

                #endregion
                #region Update 106 . Extended trip info Table
                strUpd = "Rez000106";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_FLT_ExtTripInfo](
                               IID] int] AUTO_INCREMENT NOT NULL,
                               ExtTripID] nvarchar](50) NULL,
                               Field1Name] nvarchar](50) NULL,
                               Field1Value] nvarchar](50) NULL,
                               Field2Name] nvarchar](50) NULL,
                               Field2Value] nvarchar](50) NULL,
                               Field3Name] nvarchar](50) NULL,
                               Field3Value] nvarchar](50) NULL,
                               CreatedBy] nvarchar](50) NULL,
                               CreatedDate] date] NULL,
                               UpdatedBy] nvarchar](50) NULL,
                               UpdatedDate] date] NULL,
                               AssetName] nvarchar](50) NULL,
                               CONSTRAINT PK_GFI_FLT_ExtTripInfo] 
                                    PRIMARY KEY  ([IID] ASC)
                              )";

                    if (GetDataDT(ExistTableSql("GFI_FLT_ExtTripInfo")).Rows.Count == 0)
                        dbExecute(sql);
                    InsertDBUpdate(strUpd, "Extended trip info Table");
                }
                #endregion
                #region Update 107 . Extended trip detail info Table
                strUpd = "Rez000107";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_FLT_ExtTripInfoDetail](
                          IID] int] AUTO_INCREMENT NOT NULL,
                          ExtTripId] nvarchar](50) NOT NULL,
                          TripId] int] NOT NULL,
                          CONSTRAINT PK_GFI_FLT_ExtTripInfoDetail] PRIMARY KEY  ([IID] ASC)
                         )
                         ";

                    if (GetDataDT(ExistTableSql("GFI_FLT_ExtTripInfoDetail")).Rows.Count == 0)
                        dbExecute(sql);
                    InsertDBUpdate(strUpd, "Extended trip detail info Table");
                }
                #endregion

                #region Update 108. GFI_SYS_LoginAudit
                strUpd = "Rez000108";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_SYS_LoginAudit] add UserID] int NOT NULL Default 1";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_LoginAudit", "UserID")).Rows.Count == 0)
                    {
                        dbExecute(sql);
                        sql = @"update GFI_SYS_LoginAudit set UserID = T2.UID
                                    FROM        GFI_SYS_LoginAudit T1
                                    INNER JOIN  GFI_SYS_User T2 ON T2.Username = T1.auditUser";
                        dbExecute(sql);
                    }

                    sql = @"ALTER TABLE GFI_SYS_LoginAudit 
                                    ADD CONSTRAINT FK_GFI_SYS_LoginAudit_GFI_SYS_User 
                                        FOREIGN KEY(UserID) 
                                        REFERENCES GFI_SYS_User(UID) 
                                    --ON UPDATE  NO ACTION 
	                                --ON DELETE  NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_LoginAudit_GFI_SYS_User")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_Audit] add UserID] int NOT NULL Default 1";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Audit", "UserID")).Rows.Count == 0)
                    {
                        dbExecute(sql);
                        sql = @"update GFI_SYS_Audit set UserID = T2.UID
                                    FROM        GFI_SYS_Audit T1
                                    INNER JOIN  GFI_SYS_User T2 ON T2.Username = T1.auditUser";
                        dbExecute(sql);
                    }

                    sql = @"ALTER TABLE GFI_SYS_Audit 
                                ADD CONSTRAINT FK_GFI_SYS_Audit_GFI_SYS_User 
                                    FOREIGN KEY(UserID) 
                                    REFERENCES GFI_SYS_User(UID) 
                                ON UPDATE  NO ACTION 
	                            ON DELETE  NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_Audit_GFI_SYS_User")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_AuditAccess] add UserID] int NOT NULL Default 1";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_AuditAccess", "UserID")).Rows.Count == 0)
                    {
                        dbExecute(sql);
                        sql = @"update GFI_SYS_AuditAccess set UserID = T2.UID
                                    FROM        GFI_SYS_AuditAccess T1
                                    INNER JOIN  GFI_SYS_User T2 ON T2.Username = T1.auditUser";
                        dbExecute(sql);
                    }

                    sql = @"ALTER TABLE GFI_SYS_AuditAccess 
                                ADD CONSTRAINT FK_GFI_SYS_AuditAccess_GFI_SYS_User 
                                    FOREIGN KEY(UserID) 
                                    REFERENCES GFI_SYS_User(UID) 
                                ON UPDATE  NO ACTION 
	                            ON DELETE  NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_AuditAccess_GFI_SYS_User")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_ActiveSession] add UserID] int NOT NULL Default 1";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_ActiveSession", "UserID")).Rows.Count == 0)
                    {
                        dbExecute(sql);
                        sql = @"update GFI_SYS_ActiveSession set UserID = T2.UID
                                    FROM        GFI_SYS_ActiveSession T1
                                    INNER JOIN  GFI_SYS_User T2 ON T2.Username = T1.Username";
                        dbExecute(sql);
                    }

                    sql = @"ALTER TABLE GFI_SYS_ActiveSession 
                                ADD CONSTRAINT FK_GFI_SYS_ActiveSession_GFI_SYS_User 
                                    FOREIGN KEY(UserID) 
                                    REFERENCES GFI_SYS_User(UID) 
                                ON UPDATE  NO ACTION 
	                            ON DELETE  Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_ActiveSession_GFI_SYS_User")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "User Constraints");
                }
                #endregion
                #region Update 109. GFI_FLT_ExtTripInfo
                strUpd = "Rez000109";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfoMapping] ADD  CONSTRAINT PK_GFI_FLT_ExtTripInfoMapping] PRIMARY KEY  ([iID] ASC)";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_FLT_ExtTripInfoMapping")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo] ADD  CONSTRAINT UK_GFI_FLT_ExtTripInfo] Unique ([ExtTripID] ASC)";
                    if (GetDataDT(ExistsSQLConstraint("UK_GFI_FLT_ExtTripInfo")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfoDetail 
                            ADD CONSTRAINT FK_GFI_FLT_ExtTripInfoDetail_GFI_FLT_ExtTripInfo
                                FOREIGN KEY(ExtTripID) 
                                REFERENCES GFI_FLT_ExtTripInfo(ExtTripID) 
                            ON UPDATE  NO ACTION 
	                        ON DELETE  Cascade";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfoDetail_GFI_FLT_ExtTripInfo")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD LoginAttempt int Not NULL Default 1";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "LoginAttempt")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD Field4Name nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "Field4Name")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD Field4Value nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "Field4Value")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD Field5Name nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "Field5Name")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD Field5Value nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "Field5Value")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD Field6Name nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "Field6Name")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD Field6Value nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "Field6Value")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD AdditionalField1 nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "AdditionalField1")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD AdditionalField2 nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "AdditionalField2")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD AdditionalValue1 nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "AdditionalValue1")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD AdditionalValue2 nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "AdditionalValue2")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "GFI_FLT_ExtTripInfo");
                }
                #endregion
                #region Update 110. GFI_AMM_AssetZoneMap
                strUpd = "Rez000110";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_AMM_AssetZoneMap] 
                            (
	                            Id] int IDENTITY(1, 1) NOT NULL,
	                            AssetID] int NOT NULL,
	                            ZoneID] int NOT NULL,
	                            Param1] nchar(10) NULL,
	                            Param2] nchar(10) NULL,
	                            Param3] nchar(10) NULL,
	                            CreatedBy] nvarchar(50) NULL,
	                            CreatedDate] datetime NULL,
	                            ModifiedBy] nvarchar(50) NULL,
	                            ModifiedDate] datetime NULL
                            )";
                    if (GetDataDT(ExistTableSql("GFI_AMM_AssetZoneMap")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_AMM_AssetZoneMap] ADD  CONSTRAINT PK_GFI_AMM_AssetZoneMap] PRIMARY KEY  ([ID] ASC)";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_FLT_ExtTripInfoMapping")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_AMM_AssetZoneMap 
                                    ADD CONSTRAINT FK_GFI_AMM_AssetZoneMap_GFI_FLT_Asset 
                                        FOREIGN KEY(AssetID) 
                                        REFERENCES GFI_FLT_Asset(AssetID) 
                                    ON UPDATE  NO ACTION 
	                                ON DELETE  Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_AssetZoneMap_GFI_FLT_Asset")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_AMM_AssetZoneMap 
                                    ADD CONSTRAINT FK_GGFI_AMM_AssetZoneMap_GFI_FLT_ZoneHeader
                                        FOREIGN KEY(ZoneID) 
                                        REFERENCES GFI_FLT_ZoneHeader(ZoneID) 
                                    --ON UPDATE  NO ACTION 
	                                --ON DELETE  Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GGFI_AMM_AssetZoneMap_GFI_FLT_ZoneHeader")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "GFI_AMM_AssetZoneMap");
                }
                #endregion
                #region Update 111 . GFI_AMM_ExtZoneProp Tables
                strUpd = "Rez000111";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_FLT_AuxilliaryType] 
                                (
	                                Uid] int IDENTITY(1, 1) NOT NULL,
	                                AuxilliaryTypeName] varchar(30) NULL,
	                                AuxilliaryTypeDescription] varchar(30) NULL,
	                                Field1] varchar(50) NULL,
	                                Field1Label] varchar(30) NULL,
	                                Field1Type] varchar(30) NULL,
	                                Field2] varchar(50) NULL,
	                                Field2Label] varchar(30) NULL,
	                                Field2Type] varchar(30) NULL,
	                                Field3] varchar(50) NULL,
	                                Field3Label] varchar(30) NULL,
	                                Field3Type] varchar(30) NULL,
	                                Field4] varchar(50) NULL,
	                                Field4Label] varchar(30) NULL,
	                                Field4Type] varchar(30) NULL,
	                                Field5] varchar(50) NULL,
	                                Field5Label] varchar(30) NULL,
	                                Field5Type] varchar(30) NULL,
	                                Field6] varchar(50) NULL,
	                                Field6Label] varchar(30) NULL,
	                                Field6Type] varchar(30) NULL,
	                                Field7] varchar(50) NULL,
	                                Field7Label] varchar(30) NULL,
	                                Field7Type] varchar(30) NULL,
	                                CreatedDate] datetime NULL,
	                                CreatedBy] nvarchar(30) NULL,
	                                UpdatedDate] datetime NULL,
	                                UpdatedBy] nvarchar(30) NULL
                                )";
                    if (GetDataDT(ExistTableSql("GFI_FLT_AuxilliaryType")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_AuxilliaryType] ADD  CONSTRAINT PK_GFI_FLT_AuxilliaryType] PRIMARY KEY  ([UID] ASC)";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_FLT_AuxilliaryType")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_AMM_ExtZoneProps] 
                            (
	                            Uid] bigint IDENTITY(1, 1) NOT NULL,
	                            ZoneId] int NOT NULL,
	                            ZoneType] nvarchar(50) NULL,
	                            ApplicationNum] nvarchar(50) NULL,
	                            ApplicationType] nvarchar(50) NULL,
	                            ApplicationDate] datetime NULL,
	                            ONames] nvarchar(100) NULL,
	                            Surname] nvarchar(50) NULL,
	                            AddressL1] nvarchar(50) NULL,
	                            TVNumber] nvarchar(50) NULL,
	                            LinkPDF] nvarchar(100) NULL,
	                            LinkIMG] nvarchar(100) NULL
                            )";
                    if (GetDataDT(ExistTableSql("GFI_AMM_ExtZoneProps")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_AMM_ExtZoneProps] ADD  CONSTRAINT PK_GFI_AMM_ExtZoneProps] PRIMARY KEY  ([UID] ASC)";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_AMM_ExtZoneProps")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_HRM_Team] 
                            (
	                            TeamCode] nvarchar(50) NOT NULL,
	                            TeamSize] nvarchar(50) NULL,
	                            TeamLeader] nvarchar(200) NULL,
	                            MainRole] nvarchar(200) NULL,
	                            CreatedBy] nvarchar(200) NULL,
	                            CreatedDate] datetime NULL,
	                            UpdatedBy] nvarchar(200) NULL,
	                            UpdatedDate] datetime NULL
                            )
                            ";
                    if (GetDataDT(ExistTableSql("GFI_HRM_Team")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_HRM_Team] ADD  CONSTRAINT PK_GFI_HRM_Team] PRIMARY KEY  ([TeamCode] ASC)";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_HRM_Team")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_AMM_ExtZoneProp] 
                            (
	                            APL_ID] nvarchar(50) NOT NULL,				
	                            APL_NAME] nvarchar(255) NULL,
	                            APL_FNAME] nvarchar(255) NULL,
	                            APL_DATE] datetime NULL,
	                            APL_EFF_DATE] datetime NULL,
	                            APL_ADDR1] nvarchar(255) NULL,
	                            APL_ADDR2] nvarchar(255) NULL,
	                            APL_ADDR3] nvarchar(255) NULL,
	                            APL_LOC_PLAN] nvarchar(255) NULL,
	                            APL_TV_NO] nvarchar(255) NULL,
	                            APL_PROP_DEVP] nvarchar(255) NULL,
	                            APL_EXTENT] float NULL,
	                            Link_Pdf] nvarchar(255) NULL,
	                            TYPE] nvarchar(255) NULL,
	                            BUFFERSIZE] float NULL,
	                            POINT_X] float NULL,
	                            POINT_Y] float NULL,
	                            IID] int IDENTITY(1, 1) NOT NULL,
	                            ZoneID] int NULL,
	                            Status] nvarchar(50) NULL
                            )";
                    if (GetDataDT(ExistTableSql("GFI_AMM_ExtZoneProp")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_AMM_ExtZoneProp] ADD  CONSTRAINT PK_GFI_AMM_ExtZoneProp] PRIMARY KEY  ([IID] ASC)";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_AMM_ExtZoneProp")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_AMM_ExtZonePropAssessment] 
                            (
	                            UID] int IDENTITY(1, 1) NOT NULL,
	                            APL_Id] nvarchar(50) NULL,
	                            ASSESS_Code] nvarchar(50) NULL,
	                            ASSESS_Date] datetime NULL,
	                            ASSESS_Detail] nvarchar(400) NULL,
	                            ASSESS_Recommend] nvarchar(400) NULL,
	                            ASSESS_Status] nvarchar(5) NULL,
	                            CreatedBy] nvarchar(50) NULL,
	                            CreatedDate] datetime NULL,
	                            UpdatedBy] nvarchar(50) NULL,
	                            UpdatedDate] datetime NULL,
	                            ASSESS_By] nvarchar(50) NULL
                            )";
                    if (GetDataDT(ExistTableSql("GFI_AMM_ExtZonePropAssessment")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_AMM_ExtZonePropAssessment] ADD  CONSTRAINT PK_GFI_AMM_ExtZonePropAssessment] PRIMARY KEY  ([UID] ASC)";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_AMM_ExtZonePropAssessment")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_AMM_ExtZoneProp] ADD  CONSTRAINT UK_GFI_AMM_ExtZoneProp] Unique ([APL_Id] ASC)";
                    if (GetDataDT(ExistsSQLConstraint("UK_GFI_AMM_ExtZoneProp")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_AMM_ExtZonePropAssessment 
                                    ADD CONSTRAINT FK_GFI_AMM_ExtZonePropAssessment_GFI_AMM_ExtZoneProp 
                                        FOREIGN KEY(APL_Id) 
                                        REFERENCES GFI_AMM_ExtZoneProp(APL_Id) 
                                    ON UPDATE  NO ACTION 
	                                ON DELETE  Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_ExtZonePropAssessment_GFI_AMM_ExtZoneProp")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_AMM_ExtZonePropDetails] 
                            (
                                APL_ID] nvarchar(50) NOT NULL, 
                                BCM_CDATE] datetime NULL,
                                BCM_CMT] nvarchar(255) NULL,
                                REF_DESC] nvarchar(MAX) NULL,
                                UID] int IDENTITY(1, 1) NOT NULL,
                                BCM_STATUS] nvarchar(10) NULL
                            )";
                    if (GetDataDT(ExistTableSql("GFI_AMM_ExtZonePropDetails")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_AMM_ExtZonePropDetails] ADD  CONSTRAINT PK_GFI_AMM_ExtZonePropDetails] PRIMARY KEY  ([UID] ASC)";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_AMM_ExtZonePropDetails")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_AMM_ExtZonePropDetails 
                                    ADD CONSTRAINT FK_GFI_AMM_ExtZonePropDetails_GFI_AMM_ExtZoneProp 
                                        FOREIGN KEY(APL_Id) 
                                        REFERENCES GFI_AMM_ExtZoneProp(APL_Id) 
                                    ON UPDATE  NO ACTION 
	                                ON DELETE  Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_ExtZonePropDetails_GFI_AMM_ExtZoneProp")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_PLA_DataSnapshot](
                                 UId] int] AUTO_INCREMENT NOT NULL,
                                 GroupingId] int] NULL,
                                 FieldName] nvarchar](50) NULL,
                                 FieldValue] nvarchar](max) NULL,
                                 CreatedBy] nvarchar](50) NULL,
                                 CreatedDate] datetime] NULL,
                                 UpdatedBy] nvarchar](50) NULL,
                                 UpdatedDate] datetime] NULL,
                                 CONSTRAINT PK_GFI_PLA_DataSnapshort] PRIMARY KEY  ([UId] ASC)
                                ) ";
                    if (GetDataDT(ExistTableSql("GFI_PLA_DataSnapshot")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_PLA_DataSnapshot]  
	                            ADD  CONSTRAINT FK_GFI_PLA_DataSnapshot_GFI_AMM_ExtZonePropDetails] 
	                            FOREIGN KEY([GroupingId])
	                            REFERENCES GFI_AMM_ExtZonePropDetails] ([UID])
                                    ON UPDATE  NO ACTION 
	                                ON DELETE  Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLA_DataSnapshot_GFI_AMM_ExtZonePropDetails")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "GFI_AMM_ExtZoneProp Tables");
                }
                #endregion
                #region Update 112. GFI_SYS_NaaAddress
                strUpd = "Rez000112";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_SYS_NaaAddress] 
                            (
	                            NaaCode] nvarchar(50) NOT NULL,
	                            LastName] nvarchar(200) NULL,
	                            OtherName] nvarchar(200) NULL,
	                            Street1] nvarchar(400) NULL,
	                            Street2] nvarchar(400) NULL,
	                            Locality] nvarchar(400) NULL,
	                            City] nvarchar(200) NULL,
	                            Tel] nvarchar(50) NULL,
	                            Email] nvarchar(200) NULL,
	                            Mobile] nvarchar(50) NULL,
	                            CreatedBy] nvarchar(50) NULL,
	                            CreatedDate] datetime NULL,
	                            UpdatedBy] nvarchar(50) NULL,
	                            UpdatedDate] datetime NULL
                            )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_NaaAddress")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_NaaAddress] ADD  CONSTRAINT PK_GFI_SYS_NaaAddress] PRIMARY KEY  ([NaaCode] ASC)";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_SYS_NaaAddress")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_HRM_EmpBase] 
                            (
	                            EmpCode] nvarchar(50) NOT NULL,
	                            NaaCode] nvarchar(50) NULL,
	                            EmpType] nvarchar(50) NULL,
	                            EmpCategory] nvarchar(50) NULL,
	                            DateJoined] datetime NULL,
	                            CreatedBy] nvarchar(200) NULL,
	                            CreatedDate] datetime NULL,
	                            UpdatedBy] nvarchar(200) NULL,
	                            UpdatedDate] datetime NULL
                            )";
                    if (GetDataDT(ExistTableSql("GFI_HRM_EmpBase")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_HRM_EmpBase] ADD  CONSTRAINT PK_GFI_HRM_EmpBase] PRIMARY KEY  ([EmpCode] ASC)";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_HRM_EmpBase")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_HRM_EmpBase]  
	                            ADD  CONSTRAINT FK_GFI_HRM_EmpBase_GFI_SYS_NaaAddress]
	                            FOREIGN KEY([NaaCode])
	                            REFERENCES GFI_SYS_NaaAddress] ([NaaCode])
                                    ON UPDATE NO ACTION 
	                                ON DELETE Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_HRM_EmpBase_GFI_SYS_NaaAddress")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_FLT_ExtTripInfoResources] 
                            (
	                            EmpCode] nvarchar(50) NULL,
	                            NaaCodeValue] nvarchar(50) NULL,						
	                            ExtTripID] nvarchar(50) NULL,
	                            IID] int IDENTITY(1, 1) NOT NULL
                            )";
                    if (GetDataDT(ExistTableSql("GFI_FLT_ExtTripInfoResources")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfoResources] ADD  CONSTRAINT PK_GFI_FLT_ExtTripInfoResources] PRIMARY KEY  ([IID] ASC)";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_FLT_ExtTripInfoResources")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfoResources]  
	                            ADD  CONSTRAINT FK_GFI_FLT_ExtTripInfoResources_GFI_HRM_EmpBase]
	                            FOREIGN KEY([EmpCode])
	                            REFERENCES GFI_HRM_EmpBase] ([EmpCode])
                                    ON UPDATE NO ACTION 
	                                ON DELETE Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfoResources_GFI_HRM_EmpBase")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfoResources]  
	                            ADD  CONSTRAINT FK_GFI_FLT_ExtTripInfoResources_GFI_FLT_ExtTripInfo]
	                            FOREIGN KEY([ExtTripID])
	                            REFERENCES GFI_FLT_ExtTripInfo] ([ExtTripID])
                                    ON UPDATE NO ACTION 
	                                ON DELETE Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfoResources_GFI_FLT_ExtTripInfo")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "GFI_SYS_NaaAddress");
                }
                #endregion

                #region MOLG n Security
                #region Update 76. Asset Wizard.
                strUpd = "Rez000076";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"alter table GFI_FLT_AssetDeviceMap drop column Aux1Name_cbo";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "Aux1Name_cbo")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"alter table GFI_FLT_AssetDeviceMap drop column Aux2Name_cbo";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "Aux2Name_cbo")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"alter table GFI_FLT_AssetDeviceMap drop column Aux3Name_cbo";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "Aux3Name_cbo")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"alter table GFI_FLT_AssetDeviceMap drop column Aux4Name_cbo";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "Aux4Name_cbo")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"alter table GFI_FLT_AssetDeviceMap drop column Aux5Name_cbo";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "Aux5Name_cbo")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"alter table GFI_FLT_AssetDeviceMap drop column Aux6Name_cbo";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "Aux6Name_cbo")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_SYS_UOM]
                            (
	                            UID] int] AUTO_INCREMENT NOT NULL,
	                            UOM] nvarchar](20) NOT NULL,
	                            CONSTRAINT PK_SYS_BY_UOM] PRIMARY KEY  ([UID] ASC)
                            )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_UOM")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_FLT_DeviceAuxilliaryMap](
                                Uid] int] AUTO_INCREMENT NOT NULL,
                                MapID] int NULL,
                                Auxilliary] varchar](30) NULL,
                                AuxilliaryType] int] NULL,
                                Field1] varchar](200) NULL,
                                Field2] varchar](200) NULL,
                                Field3] int] NULL,
                                Field4] int] NULL,
                                Field5] varchar](20) NULL,
                                Field6] datetime] NULL,
                                CreatedDate] datetime] NULL,
                                CreatedBy] nvarchar](30) NULL,
                                UpdatedDate] datetime] NULL,
                                UpdatedBy] nvarchar](30) NULL,
                                Uom] int NULL,
                                Thresholdhigh] int] NULL,
                                Thresholdlow] int] NULL,
                                Capacity] int] NULL,
                                CONSTRAINT PK_GFI_FLT_DeviceAuxilliaryMap] PRIMARY KEY  ([Uid] ASC)
                            )";
                    if (GetDataDT(ExistTableSql("GFI_FLT_DeviceAuxilliaryMap")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_DeviceAuxilliaryMap 
                            ADD CONSTRAINT FK_GFI_FLT_DeviceAuxilliaryMap_GFI_FLT_AssetDeviceMap 
                                FOREIGN KEY(MapID) 
                                REFERENCES GFI_FLT_AssetDeviceMap(MapID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_DeviceAuxilliaryMap_GFI_FLT_AssetDeviceMap")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_DeviceAuxilliaryMap 
                            ADD CONSTRAINT FK_GFI_FLT_DeviceAuxilliaryMap_GFI_FLT_AuxilliaryType 
                                FOREIGN KEY(AuxilliaryType) 
                                REFERENCES GFI_FLT_AuxilliaryType(UID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_DeviceAuxilliaryMap_GFI_FLT_AuxilliaryType")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_DeviceAuxilliaryMap 
                            ADD CONSTRAINT FK_GFI_FLT_DeviceAuxilliaryMap_GFI_SYS_UOM 
                                FOREIGN KEY(UOM) 
                                REFERENCES GFI_SYS_UOM(UID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_DeviceAuxilliaryMap_GFI_SYS_UOM")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_FLT_AssetType]
                            (
                                 Uid] int] AUTO_INCREMENT NOT NULL,
                                 AssetTypeName] varchar](30) NULL,
                                 AssetTypeDescription] varchar](30) NULL,
                                 Field1] varchar](50) NULL,
                                 Field1Label] varchar](30) NULL,
                                 Field1Type] varchar](30) NULL,
                                 Field2] varchar](50) NULL,
                                 Field2Label] varchar](30) NULL,
                                 Field2Type] varchar](30) NULL,
                                 Field3] varchar](50) NULL,
                                 Field3Label] varchar](30) NULL,
                                 Field3Type] varchar](30) NULL,
                                 Field4] varchar](50) NULL,
                                 Field4Label] varchar](30) NULL,
                                 Field4Type] varchar](30) NULL,
                                 Field5] varchar](50) NULL,
                                 Field5Label] varchar](30) NULL,
                                 Field5Type] varchar](30) NULL,
                                 Field6] varchar](50) NULL,
                                 Field6Label] varchar](30) NULL,
                                 Field6Type] varchar](30) NULL,
	                                CONSTRAINT PK_GFI_FLT_AssetType] PRIMARY KEY  ([UID] ASC)
                            )";
                    if (GetDataDT(ExistTableSql("GFI_FLT_AssetType")).Rows.Count == 0)
                        dbExecute(sql);

                    //AssetType at = new AssetType();
                    //at = at.GetAssetTypeByName("*** Default ***");
                    //if (at == null)
                    //{
                    //    at = new AssetType();
                    //    at.AssetTypeName = "*** Default ***";
                    //    at.AssetTypeDescription = "*** Default ***";

                    //    at.Save(at, true);
                    //}
                    //at = at.GetAssetTypeByName("*** Default ***");

                    sql = @"alter table GFI_FLT_Asset Add AssetType int not null Default " + 1;
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Asset", "AssetType")).Rows.Count == 0)
                        dbExecute(sql);


                    dt = GetDataDT(GetFieldSchema("GFI_FLT_Asset", "AssetType"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                    {
                        sql = @"alter table GFI_FLT_Asset drop column AssetType";
                        dbExecute(sql);

                        //sql = @"alter table GFI_FLT_Asset Add AssetType int not null Default " + at.Uid.ToString();
                        sql = @"alter table GFI_FLT_Asset Add AssetType int not null Default " + 1;
                        dbExecute(sql);
                    }

                    InsertDBUpdate(strUpd, "Asset Wizard");
                }
                #endregion
                #region Update 77. ExtTripInfoType.
                strUpd = "Rez000077";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_FLT_ExtTripInfoType] add sDataSource] NVARCHAR(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfoType", "sDataSource")).Rows.Count == 0)
                        dbExecute(sql);

                    ExtTripInfoType et = new ExtTripInfoType();
                    et = et.GetExtTripInfoTypeByTypeName("NETWEIGHT", sConnStr);
                    if (et == null)
                    {
                        et = new ExtTripInfoType();
                        et.TypeName = "NETWEIGHT";
                        et.Field1Name = "NETWEIGHT";
                        et.Field1Label = "NETWEIGHT";
                        et.Field1Type = "Numeric";
                        et.CreatedBy = usrInst.Username;
                        et.CreatedDate = DateTime.Now;
                        et.Save(et, true, sConnStr);
                    }

                    et = new ExtTripInfoType();
                    et = et.GetExtTripInfoTypeByTypeName("VOUCHER_NO", sConnStr);
                    if (et == null)
                    {
                        et = new ExtTripInfoType();
                        et.TypeName = "VOUCHER_NO";
                        et.Field1Name = "VOUCHER_NO";
                        et.Field1Label = "VOUCHER_NO";
                        et.Field1Type = "Text";
                        et.CreatedBy = usrInst.Username;
                        et.CreatedDate = DateTime.Now;
                        et.Save(et, true, sConnStr);
                    }

                    et = new ExtTripInfoType();
                    et = et.GetExtTripInfoTypeByTypeName("GROSSWGT", sConnStr);
                    if (et == null)
                    {
                        et = new ExtTripInfoType();
                        et.TypeName = "GROSSWGT";
                        et.Field1Name = "GROSSWGT";
                        et.Field1Label = "GROSSWGT";
                        et.Field1Type = "Numeric";
                        et.CreatedBy = usrInst.Username;
                        et.CreatedDate = DateTime.Now;
                        et.Save(et, true, sConnStr);
                    }

                    et = new ExtTripInfoType();
                    et = et.GetExtTripInfoTypeByTypeName("TAREWGT", sConnStr);
                    if (et == null)
                    {
                        et = new ExtTripInfoType();
                        et.TypeName = "TAREWGT";
                        et.Field1Name = "TAREWGT";
                        et.Field1Label = "TAREWGT";
                        et.Field1Type = "Numeric";
                        et.CreatedBy = usrInst.Username;
                        et.CreatedDate = DateTime.Now;
                        et.Save(et, true, sConnStr);
                    }

                    GlobalParams g = new GlobalParams();
                    g = g.GetGlobalParamsByName("O_IMP_TABLES", sConnStr);
                    if (g == null)
                    {
                        g = new GlobalParams();
                        g.ParamName = "O_IMP_TABLES";
                        g.PValue = "applications,assessments";
                        g.ParamComments = "Tables to be imported from Oracle";
                        g.Save(g, true, sConnStr);
                    }

                    g = g.GetGlobalParamsByName("O_IMP_FL_PATH", sConnStr);
                    if (g == null)
                    {
                        g = new GlobalParams(); g.ParamName = "O_IMP_FL_PATH";
                        g.PValue = "S:\\IMPORT_REPOSITORY";
                        g.ParamComments = "Oracle Files Import Path";
                        g.Save(g, true, sConnStr);
                    }
                    g = g.GetGlobalParamsByName("FILES_REP", sConnStr);
                    if (g == null)
                    {
                        g = new GlobalParams(); g.ParamName = "FILES_REP";
                        g.PValue = "S:\\REPOSITORY";
                        g.ParamComments = "Files Respository";
                        g.Save(g, true, sConnStr);
                    }
                    g = g.GetGlobalParamsByName("O_CONN", sConnStr);
                    if (g == null)
                    {
                        g = new GlobalParams(); g.ParamName = "O_CONN";
                        g.PValue = "127.0.0.1,XE,1521,naveo";
                        g.ParamComments = "Oracle Connection String  Servername,SID,PORT,schema";
                        g.Save(g, true, sConnStr);
                    }
                    g = g.GetGlobalParamsByName("PWD_O", sConnStr);
                    if (g == null)
                    {
                        g = new GlobalParams();
                        g.ParamName = "PWD_O";
                        g.PValue = "KZkSzmSyhh1Ug8IPw0C8nA==";
                        g.ParamComments = "Oracle Password";
                        g.Save(g, true, sConnStr);
                    }
                    g = g.GetGlobalParamsByName("IDLEWAIT", sConnStr);
                    if (g == null)
                    {
                        g = new GlobalParams(); g.ParamName = "IDLEWAIT";
                        g.PValue = "30";
                        g.ParamComments = "IDLE WAIT TIME IN MINUTES";
                        g.Save(g, true, sConnStr);
                    }
                    g = g.GetGlobalParamsByName("EventFilePath", sConnStr);
                    if (g == null)
                    {
                        g = new GlobalParams(); g.ParamName = "EventFilePath";
                        g.PValue = "C:\\";
                        g.ParamComments = "EventFilePath";
                        g.Save(g, true, sConnStr);
                    }
                    g = g.GetGlobalParamsByName("LocalFilePath", sConnStr);
                    if (g == null)
                    {
                        g = new GlobalParams(); g.ParamName = "LocalFilePath";
                        g.PValue = "C:\\";
                        g.ParamComments = "LocalFilePath";
                        g.Save(g, true, sConnStr);
                    }

                    ExtTripInfoMapping m = new ExtTripInfoMapping();
                    m = m.GetExtTripInfoMappingById("1", sConnStr);
                    if (m == null)
                    {
                        m = new ExtTripInfoMapping();
                        m.Description = "WEIGHT";
                        m.ExtTripInfoId = 1;
                        m.MappingID = 1;
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = new ExtTripInfoMapping();
                    m = m.GetExtTripInfoMappingById("2", sConnStr);
                    if (m == null)
                    {
                        m = new ExtTripInfoMapping();
                        m.Description = "WEIGHT";
                        m.ExtTripInfoId = 2;
                        m.MappingID = 1;
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = new ExtTripInfoMapping();
                    m = m.GetExtTripInfoMappingById("3", sConnStr);
                    if (m == null)
                    {
                        m = new ExtTripInfoMapping();
                        m.Description = "WEIGHT";
                        m.ExtTripInfoId = 3;
                        m.MappingID = 1;
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }

                    m = new ExtTripInfoMapping();
                    m = m.GetExtTripInfoMappingById("4", sConnStr);
                    if (m == null)
                    {
                        m = new ExtTripInfoMapping();
                        m.Description = "WEIGHT";
                        m.ExtTripInfoId = 4;
                        m.MappingID = 1;
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);
                    }
                    InsertDBUpdate(strUpd, "ExtTripInfoType");
                }
                #endregion
                #region Update 78. Callibration and ExtTripInfoDetail updates.
                strUpd = "Rez000078";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"alter table GFI_FLT_ExtTripInfoDetail drop column TripID";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfoDetail", "TripID")).Rows.Count == 0)
                    {
                        dbExecute("delete from GFI_FLT_ExtTripInfoDetail");
                        dbExecute(sql);
                    }

                    sql = "ALTER TABLE GFI_FLT_ExtTripInfoDetail] add GPSDataId] int] NOT NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfoDetail", "GPSDataId")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_FLT_ExtTripInfoDetail] add DateTimeGPS_UTC] DateTime] NOT NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfoDetail", "DateTimeGPS_UTC")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_SYS_NaaAddress] add Fax] NVARCHAR(15) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_NaaAddress", "Fax")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_FLT_CalibrationChart] (
                                iID] bigint IDENTITY(1, 1) NOT NULL,
                                AssetID] bigint NULL,
                                DeviceID] nvarchar(50) NULL,
                                CalType] nvarchar(20) NULL,
                                SeuqenceNo] int NULL,
                                ReadingUOM] int not NULL,
                                Reading] float NULL,
                                ConvertedValue] float NULL,
                                ConvertedUOM] int not NULL,
                                CreatedBy] nvarchar(30) NULL,
                                CreatedDate] datetime NULL,
                                UpdatedBy] nvarchar(30) NULL,
                                UpdatedDate] datetime NULL,
	                            CONSTRAINT PK_GFI_FLT_CalibrationChart] PRIMARY KEY  ([iID] ASC)
                            )";
                    if (GetDataDT(ExistTableSql("GFI_FLT_CalibrationChart")).Rows.Count == 0)
                        dbExecute(sql);

                    if (GetDataDT(ExistColumnSql("GFI_SYS_UOM", "Description")).Rows.Count == 0)
                        dbExecute(RenameColumn("UOM", "Description", "GFI_SYS_UOM"));

                    sql = @"ALTER TABLE GFI_FLT_CalibrationChart 
                            ADD CONSTRAINT FK_GFI_FLT_CalibrationChart_GFI_SYS_UOM 
                                FOREIGN KEY (ReadingUOM) REFERENCES GFI_SYS_UOM (UID),
                                FOREIGN KEY (ConvertedUOM) REFERENCES GFI_SYS_UOM (UID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_CalibrationChart_GFI_SYS_UOM")).Rows.Count == 0)
                        dbExecute(sql);

                    UOM u = new UOM();
                    u = u.GetUOMByDescription("Kg", sConnStr);
                    if (u == null)
                    {
                        u = new UOM();
                        u.Description = "Kg";
                        u.Save(u, true, sConnStr);
                    }

                    u = u.GetUOMByDescription("%", sConnStr);
                    if (u == null)
                    {
                        u = new UOM();
                        u.Description = "%";
                        u.Save(u, true, sConnStr);
                    }
                    UOM up = u.GetUOMByDescription("%", sConnStr);

                    u = u.GetUOMByDescription("Ton", sConnStr);
                    if (u == null)
                    {
                        u = new UOM();
                        u.Description = "Ton";
                        u.Save(u, true, sConnStr);
                    }

                    u = u.GetUOMByDescription("Degrees C", sConnStr);
                    if (u == null)
                    {
                        u = new UOM();
                        u.Description = "Degrees C";
                        u.Save(u, true, sConnStr);
                    }

                    u = u.GetUOMByDescription("Liters", sConnStr);
                    if (u == null)
                    {
                        u = new UOM();
                        u.Description = "Liters";
                        u.Save(u, true, sConnStr);
                    }
                    u = u.GetUOMByDescription("Liters", sConnStr);

                    sql = @"
                            ;with cte as 
                            (
                                select h.AssetID, d.*, 
                                    row_number() over(partition by h.AssetID order by h.AssetID) as RowNum
	                            from GFI_GPS_GPSDataDetail d
	                            inner join GFI_GPS_GPSData h on h.UID = d.UID
                                WHERE UOM = '%'
                            )
                            INSERT INTO GFI_FLT_CalibrationChart(AssetID, DeviceID, CalType, SeuqenceNo, ReadingUOM, Reading, ConvertedValue, ConvertedUOM, CreatedBy, CreatedDate) 
                            select 
	                            cte.AssetID
	                            ,''
	                            ,'LINEAR'
	                            ,0
	                            ," + up.UID.ToString() + @"
	                            ,100
	                            ,65
	                            ," + u.UID.ToString() + @"
	                            ,'" + usrInst.Username + @"'
	                            , getdate()
                            from cte left outer join GFI_FLT_CalibrationChart c on cte.AssetID = c.AssetID
                                where RowNum = 1 and c.AssetID is null";
                    //dbExecuteWithoutTransaction(sql, 8000);

                    InsertDBUpdate(strUpd, "Callibration and ExtTripInfoDetail updates");
                }
                #endregion
                #region Update 79. AssetExtProVehicles and Team Additional fields
                strUpd = "Rez000079";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_FLT_ZoneHeader] add isMarker] int] NOT NULL DEFAULT ((0))";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ZoneHeader", "isMarker")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_FLT_ZoneHeader] add iBuffer] int] NOT NULL DEFAULT ((0))";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ZoneHeader", "iBuffer")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_FLT_CalibrationChart] add AuxID] int] NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_CalibrationChart", "AuxID")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_CalibrationChart 
                            ADD CONSTRAINT FK_GFI_FLT_CalibrationChart_GFI_FLT_DeviceAuxilliaryMap 
                                FOREIGN KEY(AuxID) 
                                REFERENCES GFI_FLT_DeviceAuxilliaryMap(UID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_CalibrationChart_GFI_FLT_DeviceAuxilliaryMap")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_AMM_AssetExtProVehicles] add Field1Value] nvarchar](50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_AssetExtProVehicles", "Field1Value")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_AMM_AssetExtProVehicles] add Field2Value] nvarchar](50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_AssetExtProVehicles", "Field2Value")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_AMM_AssetExtProVehicles] add Field3Value] int] NULL";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_AssetExtProVehicles", "Field3Value")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_AMM_AssetExtProVehicles] add Field4Value] int] NULL";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_AssetExtProVehicles", "Field4Value")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_AMM_AssetExtProVehicles] add Field5Value] nvarchar](50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_AssetExtProVehicles", "Field5Value")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_AMM_AssetExtProVehicles] add Field6Value] datetime NULL";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_AssetExtProVehicles", "Field6Value")).Rows.Count == 0)
                        dbExecute(sql);

                    //DROP FOREIGN KEY 
                    sql = "ALTER TABLE GFI_HRM_EmpBase] DROP FOREIGN KEY FK_GFI_HRM_EmpBase_GFI_SYS_NaaAddress";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_HRM_EmpBase_GFI_SYS_NaaAddress")).Rows.Count > 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_FLT_ExtTripInfoResources] DROP FOREIGN KEY FK_GFI_FLT_ExtTripInfoResources_GFI_HRM_EmpBase";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfoResources_GFI_HRM_EmpBase")).Rows.Count > 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_HRM_EmpBase] DROP FOREIGN KEY PK_GFI_HRM_EmpBase";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_HRM_EmpBase")).Rows.Count > 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_HRM_EmpBase] DROP FOREIGN KEY FK_GFI_HRM_EmpBase_GFI_HRM_Team";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_HRM_EmpBase_GFI_HRM_Team")).Rows.Count > 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_SYS_NaaAddress] DROP FOREIGN KEY PK_GFI_SYS_NaaAddress";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_SYS_NaaAddress")).Rows.Count > 0)
                        dbExecute(sql);

                    //GFI_SYS_NaaAddress
                    sql = @"ALTER TABLE GFI_SYS_NaaAddress ADD iID int NOT NULL AUTO_INCREMENT";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_NaaAddress", "iID")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "alter table GFI_SYS_NaaAddress add CONSTRAINT PK_GFI_SYS_NaaAddress] PRIMARY KEY ([iID] ASC)";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_SYS_NaaAddress")).Rows.Count == 0)
                        dbExecute(sql);

                    //GFI_HRM_Team
                    sql = "ALTER TABLE GFI_HRM_Team] DROP FOREIGN KEY PK_GFI_HRM_Team";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_HRM_Team")).Rows.Count > 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_HRM_Team ADD iID int NOT NULL AUTO_INCREMENT";
                    if (GetDataDT(ExistColumnSql("GFI_HRM_Team", "iID")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "alter table GFI_HRM_Team add CONSTRAINT PK_GFI_HRM_Team] PRIMARY KEY ([iID] ASC)";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_HRM_Team")).Rows.Count == 0)
                        dbExecute(sql);

                    //Adding TempEmpCodeID
                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfoResources ADD TempEmpCodeID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfoResources", "TempEmpCodeID")).Rows.Count == 0)
                        dbExecute(sql);

                    //GFI_HRM_EmpBase
                    sql = @"ALTER TABLE GFI_HRM_EmpBase ADD iID int NOT NULL AUTO_INCREMENT";
                    if (GetDataDT(ExistColumnSql("GFI_HRM_EmpBase", "iID")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_HRM_EmpBase ADD TID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_HRM_EmpBase", "TID")).Rows.Count == 0)
                        dbExecute(sql);

                    //Adding TempNaaCodeID
                    sql = @"ALTER TABLE GFI_HRM_EmpBase ADD TempNaaCodeID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_HRM_EmpBase", "TempNaaCodeID")).Rows.Count == 0)
                        dbExecute(sql);

                    //Update TempEmpCodeID
                    sql = @"UPDATE GFI_FLT_ExtTripInfoResources 
                                SET TempEmpCodeID = T2.iID
                            FROM        GFI_FLT_ExtTripInfoResources T1
                            INNER JOIN  GFI_HRM_EmpBase T2 ON T2.EmpCode = T1.EmpCode";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfoResources", "TempEmpCodeID")).Rows.Count == 0)
                        dbExecute(sql);

                    //Rename Columns
                    dt = GetDataDT(GetFieldSchema("GFI_FLT_ExtTripInfoResources", "EmpCode"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                    {
                        dbExecute(RenameColumn("EmpCode", "EmpCodeOld", "GFI_FLT_ExtTripInfoResources"));
                        dbExecute(RenameColumn("TempEmpCodeID", "EmpCode", "GFI_FLT_ExtTripInfoResources"));
                    }

                    //Update TempNaaCodeID
                    sql = @"UPDATE GFI_HRM_EmpBase 
                                SET TempNaaCodeID = T2.iID
                            FROM        GFI_HRM_EmpBase T1
                            INNER JOIN  GFI_SYS_NaaAddress T2 ON T2.NaaCode = T1.NaaCode";
                    if (GetDataDT(ExistColumnSql("GFI_HRM_EmpBase", "TempNaaCodeID")).Rows.Count == 0)
                        dbExecute(sql);

                    //Rename Columns
                    dt = GetDataDT(GetFieldSchema("GFI_HRM_EmpBase", "NaaCode"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                    {
                        dbExecute(RenameColumn("NaaCode", "NaaCodeOld", "GFI_HRM_EmpBase"));
                        dbExecute(RenameColumn("TempNaaCodeID", "NaaCode", "GFI_HRM_EmpBase"));
                    }

                    sql = @"ALTER TABLE GFI_HRM_EmpBase]  
	                            ADD  CONSTRAINT FK_GFI_HRM_EmpBase_GFI_SYS_NaaAddress]
	                            FOREIGN KEY([NaaCode])
	                            REFERENCES GFI_SYS_NaaAddress] ([iID])
                                    ON UPDATE NO ACTION 
	                                ON DELETE Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_HRM_EmpBase_GFI_SYS_NaaAddress")).Rows.Count == 0)
                        dbExecute(sql);

                    //Update TempNaaCodeID
                    sql = @"UPDATE GFI_HRM_EmpBase 
                                SET TempNaaCodeID = T2.iID
                            FROM        GFI_HRM_EmpBase T1
                            INNER JOIN  GFI_SYS_NaaAddress T2 ON T2.NaaCode = T1.NaaCode";
                    if (GetDataDT(ExistColumnSql("GFI_HRM_EmpBase", "TempNaaCodeID")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "alter table GFI_HRM_EmpBase add CONSTRAINT PK_GFI_HRM_EmpBase] PRIMARY KEY ([iID] ASC)";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_HRM_EmpBase")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_HRM_EmpBase 
                            ADD CONSTRAINT FK_GFI_HRM_EmpBase_GFI_HRM_Team
                                FOREIGN KEY(TID) 
                                REFERENCES GFI_HRM_Team(iID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_HRM_EmpBase_GFI_HRM_Team")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfoResources]  
	                            ADD  CONSTRAINT FK_GFI_FLT_ExtTripInfoResources_GFI_HRM_EmpBase]
	                            FOREIGN KEY([EmpCode])
	                            REFERENCES GFI_HRM_EmpBase] ([iID])
                                    ON UPDATE NO ACTION 
	                                ON DELETE Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfoResources_GFI_HRM_EmpBase")).Rows.Count == 0)
                        dbExecute(sql);

                    //Drop Old Columns (varchar)
                    sql = @"alter table GFI_FLT_ExtTripInfoResources drop column EmpCodeOld";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfoResources", "EmpCodeOld")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"alter table GFI_HRM_EmpBase drop column NaaCodeOld";
                    if (GetDataDT(ExistColumnSql("GFI_HRM_EmpBase", "NaaCodeOld")).Rows.Count == 0)
                        dbExecute(sql);

                    //GFI_FLT_ExtTripInfoResources
                    //1. DROP FOREIGN KEY
                    sql = "ALTER TABLE GFI_FLT_ExtTripInfoResources] DROP FOREIGN KEY FK_GFI_FLT_ExtTripInfoResources_GFI_FLT_ExtTripInfo";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfoResources_GFI_FLT_ExtTripInfo")).Rows.Count > 0)
                        dbExecute(sql);

                    //2. Create Foreign Key Column in different Name
                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfoResources ADD TempExTripID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfoResources", "TempExTripID")).Rows.Count == 0)
                        dbExecute(sql);

                    //3. Update Foreign Key with right value
                    sql = @"UPDATE GFI_FLT_ExtTripInfoResources 
                                SET TempExTripID = T2.iID
                            FROM        GFI_FLT_ExtTripInfoResources T1
                            INNER JOIN  GFI_FLT_ExtTripInfo T2 ON T2.ExtTripID = T1.ExtTripID";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfoResources", "TempExTripID")).Rows.Count == 0)
                        dbExecute(sql);

                    //4. Rename Columns. Old Foreign becomes old, TempID becomes real ForeignID
                    dt = GetDataDT(GetFieldSchema("GFI_FLT_ExtTripInfoResources", "ExtTripID"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                    {
                        dbExecute(RenameColumn("ExtTripID", "ExtTripIDOld", "GFI_FLT_ExtTripInfoResources"));
                        dbExecute(RenameColumn("TempExTripID", "ExtTripID", "GFI_FLT_ExtTripInfoResources"));
                    }

                    //5. Recreate Constraint
                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfoResources]  
	                            ADD  CONSTRAINT FK_GFI_FLT_ExtTripInfoResources_GFI_FLT_ExtTripInfo]
	                            FOREIGN KEY([ExtTripID])
	                            REFERENCES GFI_FLT_ExtTripInfo] ([iID])
                                    ON UPDATE NO ACTION 
	                                ON DELETE Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfoResources_GFI_FLT_ExtTripInfo")).Rows.Count == 0)
                        dbExecute(sql);

                    //6. Delete Old Column
                    sql = @"alter table GFI_FLT_ExtTripInfoResources drop column ExtTripIDOld";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfoResources", "ExtTripIDOld")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "AssetExtProVehicles and Team Additional fields and Constraints");
                }
                #endregion
                #region Update 80. GFI_FLT_ExtTripInfoDetails Constraints
                strUpd = "Rez000080";
                if (!CheckUpdateExist(strUpd))
                {
                    //1. DROP FOREIGN KEY
                    sql = "ALTER TABLE GFI_FLT_ExtTripInfoDetail] DROP FOREIGN KEY FK_GFI_FLT_ExtTripInfoDetail_GFI_FLT_ExtTripInfo";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfoDetail_GFI_FLT_ExtTripInfo")).Rows.Count > 0)
                        dbExecute(sql);

                    //2. Create Foreign Key Column in different Name
                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfoDetail ADD TempExTripID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfoDetail", "TempExTripID")).Rows.Count == 0)
                        dbExecute(sql);

                    //3. Update Foreign Key with right value
                    sql = @"UPDATE GFI_FLT_ExtTripInfoDetail 
                                SET TempExTripID = T2.iID
                            FROM        GFI_FLT_ExtTripInfoDetail T1
                            INNER JOIN  GFI_FLT_ExtTripInfo T2 ON T2.ExtTripID = T1.ExtTripID";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfoDetail", "TempExTripID")).Rows.Count == 0)
                        dbExecute(sql);

                    //4. Rename Columns. Old Foreign becomes old, TempID becomes real ForeignID
                    dt = GetDataDT(GetFieldSchema("GFI_FLT_ExtTripInfoDetail", "ExtTripID"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                    {
                        dbExecute(RenameColumn("ExtTripID", "ExtTripIDOld", "GFI_FLT_ExtTripInfoDetail"));
                        dbExecute(RenameColumn("TempExTripID", "ExtTripID", "GFI_FLT_ExtTripInfoDetail"));
                    }

                    //5. Recreate Constraint
                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfoDetail]  
	                            ADD  CONSTRAINT FK_GFI_FLT_ExtTripInfoDetail_GFI_FLT_ExtTripInfo]
	                            FOREIGN KEY([ExtTripID])
	                            REFERENCES GFI_FLT_ExtTripInfo] ([iID])
                                    ON UPDATE NO ACTION 
	                                ON DELETE Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfoDetail_GFI_FLT_ExtTripInfo")).Rows.Count == 0)
                        dbExecute(sql);

                    //6. Delete Old Column
                    sql = @"alter table GFI_FLT_ExtTripInfoDetail drop column ExtTripIDOld";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfoDetail", "ExtTripIDOld")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "GFI_FLT_ExtTripInfoDetails Constraints");
                }
                #endregion
                #region Update 81. GFI_FLT_ExtTripInfo Constraints
                strUpd = "Rez000081";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "alter table GFI_FLT_ExtTripInfoType add CONSTRAINT PK_GFI_FLT_ExtTripInfoType] PRIMARY KEY ([iID] ASC)";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_FLT_ExtTripInfoType")).Rows.Count == 0)
                        dbExecute(sql);

                    //GFI_FLT_ExtTripInfoMapping Constraint
                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfoMapping]  
	                            ADD  CONSTRAINT FK_GFI_FLT_ExtTripInfoMapping_GFI_FLT_ExtTripInfoType]
	                            FOREIGN KEY([ExtTripInfoId])
	                            REFERENCES GFI_FLT_ExtTripInfoType] ([iID])
                                    ON UPDATE NO ACTION 
	                                ON DELETE Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfoMapping_GFI_FLT_ExtTripInfoType")).Rows.Count == 0)
                        dbExecute(sql);

                    //GFI_FLT_ExtTripInfo Constraints

                    #region Field1Name
                    //1. DROP FOREIGN KEY
                    sql = "ALTER TABLE GFI_FLT_ExtTripInfo] DROP FOREIGN KEY FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType")).Rows.Count > 0)
                        dbExecute(sql);

                    //2. Create Foreign Key Column in different Name
                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD TempID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "TempID")).Rows.Count == 0)
                        dbExecute(sql);

                    //3. Update Foreign Key with right value
                    sql = @"UPDATE GFI_FLT_ExtTripInfo 
                                SET TempID = T2.iID
                            FROM        GFI_FLT_ExtTripInfo T1
                            INNER JOIN  GFI_FLT_ExtTripInfoType T2 ON T2.Field1Name = T1.Field1Name";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "TempID")).Rows.Count == 0)
                        dbExecute(sql);

                    //4. Rename Columns. Old Foreign becomes old, TempID becomes real ForeignID
                    dt = GetDataDT(GetFieldSchema("GFI_FLT_ExtTripInfo", "Field1Name"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                    {
                        dbExecute(RenameColumn("Field1Name", "Field1NameOld", "GFI_FLT_ExtTripInfo"));
                        dbExecute(RenameColumn("TempID", "Field1Name", "GFI_FLT_ExtTripInfo"));
                    }
                    #endregion
                    #region Field2Name
                    //1. DROP FOREIGN KEY
                    sql = "ALTER TABLE GFI_FLT_ExtTripInfo] DROP FOREIGN KEY FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType")).Rows.Count > 0)
                        dbExecute(sql);

                    //2. Create Foreign Key Column in different Name
                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD TempID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "TempID")).Rows.Count == 0)
                        dbExecute(sql);

                    //3. Update Foreign Key with right value
                    sql = @"UPDATE GFI_FLT_ExtTripInfo 
                                SET TempID = T2.iID
                            FROM        GFI_FLT_ExtTripInfo T1
                            INNER JOIN  GFI_FLT_ExtTripInfoType T2 ON T2.Field1Name = T1.Field2Name";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "TempID")).Rows.Count == 0)
                        dbExecute(sql);

                    //4. Rename Columns. Old Foreign becomes old, TempID becomes real ForeignID
                    dt = GetDataDT(GetFieldSchema("GFI_FLT_ExtTripInfo", "Field2Name"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                    {
                        dbExecute(RenameColumn("Field2Name", "Field2NameOld", "GFI_FLT_ExtTripInfo"));
                        dbExecute(RenameColumn("TempID", "Field2Name", "GFI_FLT_ExtTripInfo"));
                    }
                    #endregion
                    #region Field3Name
                    //1. DROP FOREIGN KEY
                    sql = "ALTER TABLE GFI_FLT_ExtTripInfo] DROP FOREIGN KEY FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType")).Rows.Count > 0)
                        dbExecute(sql);

                    //2. Create Foreign Key Column in different Name
                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD TempID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "TempID")).Rows.Count == 0)
                        dbExecute(sql);

                    //3. Update Foreign Key with right value
                    sql = @"UPDATE GFI_FLT_ExtTripInfo 
                                SET TempID = T2.iID
                            FROM        GFI_FLT_ExtTripInfo T1
                            INNER JOIN  GFI_FLT_ExtTripInfoType T2 ON T2.Field1Name = T1.Field3Name";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "TempID")).Rows.Count == 0)
                        dbExecute(sql);

                    //4. Rename Columns. Old Foreign becomes old, TempID becomes real ForeignID
                    dt = GetDataDT(GetFieldSchema("GFI_FLT_ExtTripInfo", "Field3Name"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                    {
                        dbExecute(RenameColumn("Field3Name", "Field3NameOld", "GFI_FLT_ExtTripInfo"));
                        dbExecute(RenameColumn("TempID", "Field3Name", "GFI_FLT_ExtTripInfo"));
                    }
                    #endregion
                    #region Field4Name
                    //1. DROP FOREIGN KEY
                    sql = "ALTER TABLE GFI_FLT_ExtTripInfo] DROP FOREIGN KEY FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType")).Rows.Count > 0)
                        dbExecute(sql);

                    //2. Create Foreign Key Column in different Name
                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD TempID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "TempID")).Rows.Count == 0)
                        dbExecute(sql);

                    //3. Update Foreign Key with right value
                    sql = @"UPDATE GFI_FLT_ExtTripInfo 
                                SET TempID = T2.iID
                            FROM        GFI_FLT_ExtTripInfo T1
                            INNER JOIN  GFI_FLT_ExtTripInfoType T2 ON T2.Field1Name = T1.Field4Name";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "TempID")).Rows.Count == 0)
                        dbExecute(sql);

                    //4. Rename Columns. Old Foreign becomes old, TempID becomes real ForeignID
                    dt = GetDataDT(GetFieldSchema("GFI_FLT_ExtTripInfo", "Field4Name"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                    {
                        dbExecute(RenameColumn("Field4Name", "Field4NameOld", "GFI_FLT_ExtTripInfo"));
                        dbExecute(RenameColumn("TempID", "Field4Name", "GFI_FLT_ExtTripInfo"));
                    }
                    #endregion
                    #region Field5Name
                    //1. DROP FOREIGN KEY
                    sql = "ALTER TABLE GFI_FLT_ExtTripInfo] DROP FOREIGN KEY FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType")).Rows.Count > 0)
                        dbExecute(sql);

                    //2. Create Foreign Key Column in different Name
                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD TempID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "TempID")).Rows.Count == 0)
                        dbExecute(sql);

                    //3. Update Foreign Key with right value
                    sql = @"UPDATE GFI_FLT_ExtTripInfo 
                                SET TempID = T2.iID
                            FROM        GFI_FLT_ExtTripInfo T1
                            INNER JOIN  GFI_FLT_ExtTripInfoType T2 ON T2.Field1Name = T1.Field5Name";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "TempID")).Rows.Count == 0)
                        dbExecute(sql);

                    //4. Rename Columns. Old Foreign becomes old, TempID becomes real ForeignID
                    dt = GetDataDT(GetFieldSchema("GFI_FLT_ExtTripInfo", "Field5Name"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                    {
                        dbExecute(RenameColumn("Field5Name", "Field5NameOld", "GFI_FLT_ExtTripInfo"));
                        dbExecute(RenameColumn("TempID", "Field5Name", "GFI_FLT_ExtTripInfo"));
                    }
                    #endregion
                    #region Field6Name
                    //1. DROP FOREIGN KEY
                    sql = "ALTER TABLE GFI_FLT_ExtTripInfo] DROP FOREIGN KEY FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType")).Rows.Count > 0)
                        dbExecute(sql);

                    //2. Create Foreign Key Column in different Name
                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD TempID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "TempID")).Rows.Count == 0)
                        dbExecute(sql);

                    //3. Update Foreign Key with right value
                    sql = @"UPDATE GFI_FLT_ExtTripInfo 
                                SET TempID = T2.iID
                            FROM        GFI_FLT_ExtTripInfo T1
                            INNER JOIN  GFI_FLT_ExtTripInfoType T2 ON T2.Field1Name = T1.Field6Name";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "TempID")).Rows.Count == 0)
                        dbExecute(sql);

                    //4. Rename Columns. Old Foreign becomes old, TempID becomes real ForeignID
                    dt = GetDataDT(GetFieldSchema("GFI_FLT_ExtTripInfo", "Field6Name"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                    {
                        dbExecute(RenameColumn("Field6Name", "Field6NameOld", "GFI_FLT_ExtTripInfo"));
                        dbExecute(RenameColumn("TempID", "Field6Name", "GFI_FLT_ExtTripInfo"));
                    }
                    #endregion

                    //5. Recreate Constraint
                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo]  
	                            ADD  CONSTRAINT FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType]
	                            FOREIGN KEY([Field1Name]) REFERENCES GFI_FLT_ExtTripInfoType] ([iID]),
	                            FOREIGN KEY([Field2Name]) REFERENCES GFI_FLT_ExtTripInfoType] ([iID]),
	                            FOREIGN KEY([Field3Name]) REFERENCES GFI_FLT_ExtTripInfoType] ([iID]),
	                            FOREIGN KEY([Field4Name]) REFERENCES GFI_FLT_ExtTripInfoType] ([iID]),
	                            FOREIGN KEY([Field5Name]) REFERENCES GFI_FLT_ExtTripInfoType] ([iID]),
	                            FOREIGN KEY([Field6Name]) REFERENCES GFI_FLT_ExtTripInfoType] ([iID])
                                    ON UPDATE NO ACTION 
	                                ON DELETE No Action ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType")).Rows.Count == 0)
                        dbExecute(sql);

                    //6. Delete Old Column
                    sql = @"alter table GFI_FLT_ExtTripInfo drop column Field1NameOld";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "Field1NameOld")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"alter table GFI_FLT_ExtTripInfo drop column Field2NameOld";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "Field2NameOld")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"alter table GFI_FLT_ExtTripInfo drop column Field3NameOld";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "Field3NameOld")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"alter table GFI_FLT_ExtTripInfo drop column Field4NameOld";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "Field4NameOld")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"alter table GFI_FLT_ExtTripInfo drop column Field5NameOld";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "Field5NameOld")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"alter table GFI_FLT_ExtTripInfo drop column Field6NameOld";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "Field6NameOld")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "GFI_FLT_ExtTripInfo Constraints");
                }
                #endregion
                #region Update 82. GFI_FLT_ExtTripInfoType
                strUpd = "Rez000082";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"alter table GFI_FLT_ExtTripInfoType drop column Field2Name";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfoType", "Field2Name")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"alter table GFI_FLT_ExtTripInfoType drop column Field2Label";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfoType", "Field2Label")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"alter table GFI_FLT_ExtTripInfoType drop column Field2Type";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfoType", "Field2Type")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_FLT_ExtTripInfoType] add sDataSource] NVARCHAR(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfoType", "sDataSource")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_FLT_ExtTripInfoMapping] add Type] NVARCHAR(20) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfoMapping", "Type")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_AMM_AssetExtProVehicles] add Field1Id] int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_AssetExtProVehicles", "Field1Id")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = "ALTER TABLE GFI_AMM_AssetExtProVehicles] add Field2Id] int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_AssetExtProVehicles", "Field2Id")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = "ALTER TABLE GFI_AMM_AssetExtProVehicles] add Field3Id] int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_AssetExtProVehicles", "Field3Id")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = "ALTER TABLE GFI_AMM_AssetExtProVehicles] add Field4Id] int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_AssetExtProVehicles", "Field4Id")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = "ALTER TABLE GFI_AMM_AssetExtProVehicles] add Field5Id] int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_AssetExtProVehicles", "Field5Id")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = "ALTER TABLE GFI_AMM_AssetExtProVehicles] add Field6Id] int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_AssetExtProVehicles", "Field6Id")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_FLT_Asset] DROP FOREIGN KEY FK_GFI_FLT_Asset_GFI_FLT_AssetType";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_Asset_GFI_FLT_AssetType")).Rows.Count > 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "GFI_FLT_ExtTripInfoType");
                }
                #endregion

                #region Update 83. AssetTypeMapping
                strUpd = "Rez000083";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_FLT_AssetTypeMapping](
	                            IID] int] AUTO_INCREMENT NOT NULL,
	                            Description] nvarchar](255) NOT NULL,
	                            ExtTripInfoId] int] NOT NULL,
	                            CreatedBy] nvarchar](50) NULL,
	                            CreatedDate] datetime] NULL,
	                            UpdatedBy] nvarchar](50) NULL,
	                            UpdatedDate] datetime] NULL,
	                            MappingId] int] NOT NULL,
                                 CONSTRAINT PK_GFI_FLT_AssetTypeMapping] PRIMARY KEY  
                                (
	                                IID] ASC
                                )
                            )";
                    if (GetDataDT(ExistTableSql("GFI_FLT_AssetTypeMapping")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_AssetTypeMapping 
                            ADD CONSTRAINT FK_GFI_FLT_AssetTypeMapping_GFI_FLT_ExtTripInfoType 
                                FOREIGN KEY(ExtTripInfoId) 
                                REFERENCES GFI_FLT_ExtTripInfoType(IID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  NO ACTION";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_AssetTypeMapping_GFI_FLT_ExtTripInfoType")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_AMM_AssetExtProVehicles]  
	                            ADD  CONSTRAINT FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_ExtTripInfoType]
	                            FOREIGN KEY([Field1Id]) REFERENCES GFI_FLT_ExtTripInfoType] ([iID]),
	                            FOREIGN KEY([Field2Id]) REFERENCES GFI_FLT_ExtTripInfoType] ([iID]),
	                            FOREIGN KEY([Field3Id]) REFERENCES GFI_FLT_ExtTripInfoType] ([iID]),
	                            FOREIGN KEY([Field4Id]) REFERENCES GFI_FLT_ExtTripInfoType] ([iID]),
	                            FOREIGN KEY([Field5Id]) REFERENCES GFI_FLT_ExtTripInfoType] ([iID]),
	                            FOREIGN KEY([Field6Id]) REFERENCES GFI_FLT_ExtTripInfoType] ([iID])
                                    ON UPDATE NO ACTION 
	                                ON DELETE No Action ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_ExtTripInfoType")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"alter table GFI_FLT_ExtTripInfoMapping drop column Type]";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfoMapping", "Type")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD Consumption float NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "Consumption")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD Timediff float NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "Timediff")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD DateTimeGPSStart_UTC DateTime NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "DateTimeGPSStart_UTC")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD DateTimeGPSStop_UTC DateTime NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "DateTimeGPSStop_UTC")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "AssetTypeMapping");
                }
                #endregion
                #region Update 84. Default Fuel Callibration
                strUpd = "Rez000084";
                if (!CheckUpdateExist(strUpd))
                {
                    AuxilliaryType at = new AuxilliaryType();
                    at = at.GetAuxilliaryTypeByNames("Fuel", sConnStr);
                    if (at == null)
                    {
                        at = new AuxilliaryType();
                        at.AuxilliaryTypeName = "Fuel";
                        at.AuxilliaryTypeDescription = "Fuel";

                        at.Field1 = "Field1";
                        at.Field1Label = String.Empty;
                        at.Field1Type = "VarChar";

                        at.Field2 = "Field2";
                        at.Field2Label = String.Empty;
                        at.Field2Type = "VarChar";

                        at.Field3 = "Field3";
                        at.Field3Label = String.Empty;
                        at.Field3Type = "Integer";

                        at.Field4 = "Field4";
                        at.Field4Label = String.Empty;
                        at.Field4Type = "Integer";

                        at.Field5 = "Field5";
                        at.Field5Label = String.Empty;
                        at.Field5Type = "Boolean";

                        at.Field6 = "Field6";
                        at.Field6Label = String.Empty;
                        at.Field6Type = "DateTime";
                        at.Save(at, true, sConnStr);
                    }
                    at = at.GetAuxilliaryTypeByNames("Fuel", sConnStr);


                    UOM uomFr = new UOM();
                    uomFr = uomFr.GetUOMByDescription("%", sConnStr);

                    UOM uomTo = new UOM();
                    uomTo = uomTo.GetUOMByDescription("Liters", sConnStr);

                    sql = @"
;with cte as 
(
	select h.AssetID, d.*, 
		row_number() over(partition by h.AssetID order by h.AssetID) as RowNum
	from GFI_GPS_GPSDataDetail d
	inner join GFI_GPS_GPSData h on h.UID = d.UID
	WHERE UOM = '%'
)
insert GFI_FLT_DeviceAuxilliaryMap (MapID, Auxilliary, AuxilliaryType, CreatedDate, CreatedBy, Uom, Thresholdhigh, Thresholdlow, Capacity)
    select m.MapID, 'AUX1', " + at.Uid + @", GETUTCDATE(), '" + usrInst.Username + @"', " + uomFr.UID.ToString() + @", 100, 0, 100
    from cte c
	    left outer join GFI_FLT_AssetDeviceMap m on m.AssetID = c.AssetID
	    left outer join GFI_FLT_DeviceAuxilliaryMap a on a.MapID = m.MapID
    where RowNum = 1 and a.MapID is null

";
                    dbExecuteWithoutTransaction(sql, 8000);


                    sql = "delete from GFI_FLT_CalibrationChart where AuxID is null";
                    dbExecute(sql);

                    sql = @";with cte as (select Uid from GFI_FLT_DeviceAuxilliaryMap)
insert GFI_FLT_CalibrationChart	([CalType], SeuqenceNo], ReadingUOM], Reading], ConvertedValue], ConvertedUOM], CreatedBy], CreatedDate], AuxID])
	select 'LINEAR', 0, " + uomFr.UID.ToString() + @", 100, 65, " + uomTo.UID.ToString() + @", '" + usrInst.Username + @"', GETUTCDATE(), c.Uid
from cte c
	left outer join GFI_FLT_CalibrationChart cc on cc.AuxID = c.Uid where cc.AuxID is null";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "Default Fuel Callibration");
                }
                #endregion
                #region Update 85. User Previous Password
                strUpd = "Rez000085";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_SYS_User ADD PreviousPassword nvarchar](50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "PreviousPassword")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD PasswordUpdateddate DateTime NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "PasswordUpdateddate")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "User Previous Password");
                }
                #endregion
                #region Update 86. New Menu items wrt new menu design
                strUpd = "Rez000086";
                if (!CheckUpdateExist(strUpd))
                {
                    //Add New Modules
                    NaveoOneLib.Models.Module m = new NaveoOneLib.Models.Module();
                    NaveoOneLib.Models.AccessTemplate at = new NaveoOneLib.Models.AccessTemplate();
                    NaveoOneLib.Models.AccessProfile ap = new NaveoOneLib.Models.AccessProfile();

                    DataTable dtAccessTemplate = at.GetAccessTemplates(usrInst.lMatrix, myStrConn);
                    DataSet dsResult = new DataSet();
                    m = m.GetModulesByName("MAPS & GIS --> Planning", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module();
                        m.ModuleName = "MAPS & GIS --> Planning";
                        m.Description = "MAPS & GIS --> Planning";
                        m.Command = "cmdPlanning";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);

                        m = m.GetModulesByName("MAPS & GIS --> Planning", sConnStr);
                        foreach (DataRow r in dtAccessTemplate.Rows)
                        {
                            ap = new NaveoOneLib.Models.AccessProfile();
                            ap.UID = Constants.NullInt;
                            ap.TemplateId = Convert.ToInt32(r["UID"].ToString());
                            ap.ModuleId = m.UID;
                            ap.Command = "cmdPlanning";
                            ap.AllowRead = "0";
                            ap.AllowNew = "0";
                            ap.AllowEdit = "0";
                            ap.AllowDelete = "0";
                            ap.CreatedDate = DateTime.Now;
                            ap.CreatedBy = usrInst.Username;
                            ap.Save(ap, out dsResult, null, sConnStr);
                        }
                    }

                    m = m.GetModulesByName("Settings --> Extended Trip History", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Settings --> Extended Trip History";
                        m.Description = "Settings --> Extended Trip History";
                        m.Command = "cmdExtendedTripInfo";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);

                        m = m.GetModulesByName("Settings --> Extended Trip History", sConnStr);
                        foreach (DataRow r in dtAccessTemplate.Rows)
                        {
                            ap = new NaveoOneLib.Models.AccessProfile();
                            ap.UID = Constants.NullInt;
                            ap.TemplateId = Convert.ToInt32(r["UID"].ToString());
                            ap.ModuleId = m.UID;
                            ap.Command = "cmdExtendedTripInfo";
                            ap.AllowRead = "0";
                            ap.AllowNew = "0";
                            ap.AllowEdit = "0";
                            ap.AllowDelete = "0";
                            ap.CreatedDate = DateTime.Now;
                            ap.CreatedBy = usrInst.Username;
                            ap.Save(ap, out dsResult, null, sConnStr);
                        }
                    }

                    m = m.GetModulesByName("Settings --> Extended Trip History Report", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Settings --> Extended Trip History Report";
                        m.Description = "Settings --> Extended Trip History Report";
                        m.Command = "cmdExtTripInfoReport";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);

                        m = m.GetModulesByName("Settings --> Extended Trip History Report", sConnStr);
                        foreach (DataRow r in dtAccessTemplate.Rows)
                        {
                            ap = new NaveoOneLib.Models.AccessProfile();
                            ap.UID = Constants.NullInt;
                            ap.TemplateId = Convert.ToInt32(r["UID"].ToString());
                            ap.ModuleId = m.UID;
                            ap.Command = "cmdExtTripInfoReport";

                            ap.AllowRead = "0";
                            ap.AllowNew = "0";
                            ap.AllowEdit = "0";
                            ap.AllowDelete = "0";
                            ap.CreatedDate = DateTime.Now;
                            ap.CreatedBy = usrInst.Username;
                            ap.Save(ap, out dsResult, null, sConnStr);
                        }
                    }

                    m = m.GetModulesByName("Settings --> Extended Trip Info Type", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Settings --> Extended Trip Info Type";
                        m.Description = "Settings --> Extended Trip Info Type";
                        m.Command = "cmdExtTripInfoType";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);

                        m = m.GetModulesByName("Settings --> Extended Trip Info Type", sConnStr);
                        foreach (DataRow r in dtAccessTemplate.Rows)
                        {
                            ap = new NaveoOneLib.Models.AccessProfile();
                            ap.UID = Constants.NullInt;
                            ap.TemplateId = Convert.ToInt32(r["UID"].ToString());
                            ap.ModuleId = m.UID;
                            ap.Command = "cmdExtTripInfoType";
                            ap.AllowRead = "0";
                            ap.AllowNew = "0";
                            ap.AllowEdit = "0";
                            ap.AllowDelete = "0";
                            ap.CreatedDate = DateTime.Now;
                            ap.CreatedBy = usrInst.Username;
                            ap.Save(ap, out dsResult, null, sConnStr);
                        }
                    }


                    m = m.GetModulesByName("Settings --> Extended Trip Info Mapping", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Settings --> Extended Trip Info Mapping";
                        m.Description = "Settings --> Extended Trip Info Mapping";
                        m.Command = "cmdExtTripInfoTypeMapping";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);

                        m = m.GetModulesByName("Settings --> Extended Trip Info Mapping", sConnStr);
                        foreach (DataRow r in dtAccessTemplate.Rows)
                        {
                            ap = new NaveoOneLib.Models.AccessProfile();
                            ap.UID = Constants.NullInt;
                            ap.TemplateId = Convert.ToInt32(r["UID"].ToString());
                            ap.ModuleId = m.UID;
                            ap.Command = "cmdExtTripInfoTypeMapping";
                            ap.AllowRead = "0";
                            ap.AllowNew = "0";
                            ap.AllowEdit = "0";
                            ap.AllowDelete = "0";
                            ap.CreatedDate = DateTime.Now;
                            ap.CreatedBy = usrInst.Username;
                            ap.Save(ap, out dsResult, null, sConnStr);
                        }
                    }

                    InsertDBUpdate(strUpd, "New Menu items wrt new menu design");
                }
                #endregion
                #region Update 87. GFI_AMM_VehicleMaintenance New Coloumn and View Added.
                strUpd = "Rez000087";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintenance] add AssetStatus] nvarchar(20) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_VehicleMaintenance", "AssetStatus")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintenance] add Comment] nvarchar(12) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_VehicleMaintenance", "Comment")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE VIEW GFI_FLT_VW_VehicleMaintenance 
                                As SELECT AssetId, startdate, AssetStatus, Comment
                                FROM GFI_AMM_VehicleMaintenance vm                         
                                WHERE getdate() > = StartDate and getdate() <= EndDate";
                    if (GetDataDT(ExistViewSql("GFI_FLT_VW_VehicleMaintenance")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "Vehicle Maintenance to LiveMap ");
                }
                #endregion
                #region Update 88. GFI_FLT_AssetDataType
                strUpd = "Rez000088";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_FLT_AssetDataType]
                            (
                                 iID] int] AUTO_INCREMENT NOT NULL,
                                 TypeName] varchar](20) NOT NULL,
                                 Field1Name] varchar](50) NOT NULL,
                                 Field1Label] varchar](50) NOT NULL,
                                 Field1Type] varchar](50) NOT NULL,
                                 CreatedBy] nvarchar](30) NOT NULL,
                                 CreatedDate] datetime] NOT NULL,
                                 UpdatedBy] nvarchar](30) NULL,
                                 UpdatedDate] datetime] NULL,
                                 sDataSource] nvarchar](50) NULL,
                                 CONSTRAINT PK_GFI_FLT_AssetDataType] PRIMARY KEY  ([IID] ASC)
                            ) ";
                    if (GetDataDT(ExistTableSql("GFI_FLT_AssetDataType")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_FLT_AssetTypeMapping] DROP FOREIGN KEY FK_GFI_FLT_AssetTypeMapping_GFI_FLT_ExtTripInfoType";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_AssetTypeMapping_GFI_FLT_ExtTripInfoType")).Rows.Count > 0)
                        dbExecute(sql);

                    sql = @"alter table GFI_FLT_AssetTypeMapping drop column ExtTripInfoId";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetTypeMapping", "ExtTripInfoId")).Rows.Count == 0)
                        dbExecute(sql);

                    AssetDataType assetDataType = new AssetDataType();
                    assetDataType = assetDataType.GetAssetDataTypeByField1Name("**Default**", sConnStr);
                    if (assetDataType == null)
                    {
                        assetDataType = new AssetDataType();
                        assetDataType.CreatedBy = usrInst.UID.ToString();
                        assetDataType.CreatedDate = DateTime.Today;
                        assetDataType.Field1Label = "**Default**";
                        assetDataType.Field1Name = "**Default**";
                        assetDataType.Field1Type = "Text";
                        assetDataType.sDataSource = "";
                        assetDataType.TypeName = "**Default**";
                        assetDataType.UpdatedBy = usrInst.UID.ToString();
                        assetDataType.UpdatedDate = DateTime.Today;
                        assetDataType.Save(assetDataType, true, sConnStr);
                    }
                    assetDataType = assetDataType.GetAssetDataTypeByField1Name("**Default**", sConnStr);

                    sql = @"alter table GFI_FLT_AssetTypeMapping add AssetTypeId int not null default " + assetDataType.IID.ToString();
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetTypeMapping", "AssetTypeId")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_AssetTypeMapping 
                            ADD CONSTRAINT FK_GFI_FLT_AssetTypeMapping_GFI_FLT_AssetDataType 
                                FOREIGN KEY(AssetTypeId) 
                                REFERENCES GFI_FLT_AssetDataType(iID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_AssetTypeMapping_GFI_FLT_AssetDataType")).Rows.Count == 0)
                        dbExecute(sql);

                    dt = dtGetFK("GFI_AMM_AssetExtProVehicles");
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr["fkTbName"].ToString().Trim() == "GFI_FLT_ExtTripInfoType")
                        {
                            sql = "ALTER TABLE GFI_AMM_AssetExtProVehicles] DROP FOREIGN KEY " + dr["fkName"].ToString() + "]";
                            dbExecute(sql);
                        }
                    }

                    sql = @"ALTER TABLE GFI_AMM_AssetExtProVehicles]  
	                            ADD  CONSTRAINT FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_AssetDataType]
	                            FOREIGN KEY([Field1Id]) REFERENCES GFI_FLT_AssetDataType] ([iID]),
	                            FOREIGN KEY([Field2Id]) REFERENCES GFI_FLT_AssetDataType] ([iID]),
	                            FOREIGN KEY([Field3Id]) REFERENCES GFI_FLT_AssetDataType] ([iID]),
	                            FOREIGN KEY([Field4Id]) REFERENCES GFI_FLT_AssetDataType] ([iID]),
	                            FOREIGN KEY([Field5Id]) REFERENCES GFI_FLT_AssetDataType] ([iID]),
	                            FOREIGN KEY([Field6Id]) REFERENCES GFI_FLT_AssetDataType] ([iID])
                                    ON UPDATE NO ACTION 
	                                ON DELETE No Action ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_AssetDataType")).Rows.Count == 0)
                        dbExecute(sql);

                    ///
                    List<AssetTypeMapping> latm = new List<AssetTypeMapping>();
                    latm = new AssetTypeMapping().GetAssetTypeMappingByMappingId(1, sConnStr);
                    if (latm.Count == 0)
                    {
                        AssetTypeMapping atm = new AssetTypeMapping();
                        atm.Description = "**Default**";
                        atm.MappingId = 1;
                        atm.AssetTypeId = assetDataType.IID;
                        atm.CreatedBy = usrInst.Username;
                        atm.CreatedDate = DateTime.Now;
                        atm.Save(atm, true, sConnStr);
                    }

                    sql = "ALTER TABLE GFI_FLT_Asset] DROP FOREIGN KEY FK_GFI_FLT_Asset_GFI_FLT_AssetType";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_Asset_GFI_FLT_AssetType")).Rows.Count > 0)
                        dbExecute(sql);

                    sql = @"SELECT object_name(default_object_id)
                             FROM sys.columns
                             WHERE object_id = object_id('[GFI_FLT_Asset]')
                               AND name = 'AssetType'";
                    dt = GetDataDT(sql);
                    if (dt.Rows.Count > 0)
                    {
                        String strConstrainName = dt.Rows[0][0].ToString();
                        if (strConstrainName.Length > 0)
                        {
                            sql = "ALTER TABLE GFI_FLT_Asset] DROP FOREIGN KEY " + strConstrainName;
                            dbExecute(sql);

                            sql = @"alter table GFI_FLT_Asset drop column AssetType";
                            if (GetDataDT(ExistColumnSql("GFI_FLT_Asset", "AssetType")).Rows.Count == 0)
                                dbExecute(sql);
                        }
                    }

                    sql = @"drop table GFI_FLT_AssetType";
                    if (GetDataDT(ExistTableSql("GFI_FLT_AssetType")).Rows.Count == 0)
                        dbExecute(sql);

                    latm = new AssetTypeMapping().GetAssetTypeMappingByMappingId(1, sConnStr);
                    sql = @"alter table GFI_FLT_Asset Add AssetType int not null Default " + latm[0].MappingId.ToString();
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Asset", "AssetType")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Asset 
                            ADD CONSTRAINT FK_GFI_FLT_Asset_GFI_FLT_AssetTypeMapping
                                FOREIGN KEY(AssetType) 
                                REFERENCES GFI_FLT_AssetTypeMapping(iID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_Asset_GFI_FLT_AssetTypeMapping")).Rows.Count == 0)
                        dbExecute(sql);
                    ///

                    InsertDBUpdate(strUpd, "GFI_FLT_AssetDataType");
                }
                #endregion
                #region Update 89. GFI_AMM_AssetExtProVehicles
                strUpd = "Rez000089";
                if (!CheckUpdateExist(strUpd))
                {
                    dt = GetDataDT(GetFieldSchema("GFI_AMM_AssetExtProVehicles", "Field1Value"));
                    sql = "Alter table GFI_AMM_AssetExtProVehicles alter column Field1Value nvarchar(50) null";
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "nvarchar")
                        dbExecute(sql);

                    dt = GetDataDT(GetFieldSchema("GFI_AMM_AssetExtProVehicles", "Field2Value"));
                    sql = "Alter table GFI_AMM_AssetExtProVehicles alter column Field2Value nvarchar(50) null";
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "nvarchar")
                        dbExecute(sql);

                    dt = GetDataDT(GetFieldSchema("GFI_AMM_AssetExtProVehicles", "Field3Value"));
                    sql = "Alter table GFI_AMM_AssetExtProVehicles alter column Field3Value nvarchar(50) null";
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "nvarchar")
                        dbExecute(sql);

                    dt = GetDataDT(GetFieldSchema("GFI_AMM_AssetExtProVehicles", "Field4Value"));
                    sql = "Alter table GFI_AMM_AssetExtProVehicles alter column Field4Value nvarchar(50) null";
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "nvarchar")
                        dbExecute(sql);

                    dt = GetDataDT(GetFieldSchema("GFI_AMM_AssetExtProVehicles", "Field5Value"));
                    sql = "Alter table GFI_AMM_AssetExtProVehicles alter column Field5Value nvarchar(50) null";
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "nvarchar")
                        dbExecute(sql);

                    dt = GetDataDT(GetFieldSchema("GFI_AMM_AssetExtProVehicles", "Field6Value"));
                    sql = "Alter table GFI_AMM_AssetExtProVehicles alter column Field6Value nvarchar(50) null";
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "nvarchar")
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "GFI_AMM_AssetExtProVehicles");
                }
                #endregion
                #region Update 90. New Menu items wrt new menu design
                strUpd = "Rez000090";
                if (!CheckUpdateExist(strUpd))
                {
                    //Add New Modules
                    NaveoOneLib.Models.Module m = new NaveoOneLib.Models.Module();
                    NaveoOneLib.Models.AccessTemplate at = new NaveoOneLib.Models.AccessTemplate();
                    NaveoOneLib.Models.AccessProfile ap = new NaveoOneLib.Models.AccessProfile();

                    DataTable dtAccessTemplate = at.GetAccessTemplates(usrInst.lMatrix, sConnStr);
                    DataSet dsResult = new DataSet();

                    m = m.GetModulesByName("Vehicles and Assets --> Live Maintenance", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module();
                        m.ModuleName = "Vehicles and Assets --> Live Maintenance";
                        m.Description = "Vehicles and Assets --> Live Maintenance";
                        m.Command = "cmdLiveMaintenance";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);

                        m = m.GetModulesByName("Vehicles and Assets --> Live Maintenance", sConnStr);
                        foreach (DataRow r in dtAccessTemplate.Rows)
                        {
                            ap = new NaveoOneLib.Models.AccessProfile();
                            ap.UID = Constants.NullInt;
                            ap.TemplateId = Convert.ToInt32(r["UID"].ToString());
                            ap.ModuleId = m.UID;
                            ap.Command = "cmdLiveMaintenance";
                            ap.AllowRead = "0";
                            ap.AllowNew = "0";
                            ap.AllowEdit = "0";
                            ap.AllowDelete = "0";
                            ap.CreatedDate = DateTime.Now;
                            ap.CreatedBy = usrInst.Username;
                            ap.Save(ap, out dsResult, null, sConnStr);
                        }
                    }

                    m = m.GetModulesByName("Settings --> Asset Type Mapping", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Settings --> Asset Type Mapping";
                        m.Description = "Settings --> Asset Type Mapping";
                        m.Command = "cmdAsseTypeMapping";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);

                        m = m.GetModulesByName("Settings --> Asset Type Mapping", sConnStr);
                        foreach (DataRow r in dtAccessTemplate.Rows)
                        {
                            ap = new NaveoOneLib.Models.AccessProfile();
                            ap.UID = Constants.NullInt;
                            ap.TemplateId = Convert.ToInt32(r["UID"].ToString());
                            ap.ModuleId = m.UID;
                            ap.Command = "cmdAsseTypeMapping";
                            ap.AllowRead = "0";
                            ap.AllowNew = "0";
                            ap.AllowEdit = "0";
                            ap.AllowDelete = "0";
                            ap.CreatedDate = DateTime.Now;
                            ap.CreatedBy = usrInst.Username;
                            ap.Save(ap, out dsResult, null, sConnStr);
                        }
                    }

                    m = m.GetModulesByName("Settings --> Asset Type", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Settings --> Asset Type";
                        m.Description = "Settings --> Asset Type";
                        m.Command = "cmdAsseType";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);

                        m = m.GetModulesByName("Settings --> Asset Type", sConnStr);
                        foreach (DataRow r in dtAccessTemplate.Rows)
                        {
                            ap = new NaveoOneLib.Models.AccessProfile();
                            ap.UID = Constants.NullInt;
                            ap.TemplateId = Convert.ToInt32(r["UID"].ToString());
                            ap.ModuleId = m.UID;
                            ap.Command = "cmdAsseType";
                            ap.AllowRead = "0";
                            ap.AllowNew = "0";
                            ap.AllowEdit = "0";
                            ap.AllowDelete = "0";
                            ap.CreatedDate = DateTime.Now;
                            ap.CreatedBy = usrInst.Username;
                            ap.Save(ap, out dsResult, null, sConnStr);
                        }
                    }

                    m = m.GetModulesByName("Drivers & Passengers --> Name and Addresses", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Drivers & Passengers --> Name and Addresses";
                        m.Description = "Drivers & Passengers --> Name and Addresses";
                        m.Command = "cmdNaAddress";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);

                        m = m.GetModulesByName("Drivers & Passengers --> Name and Addresses", sConnStr);
                        foreach (DataRow r in dtAccessTemplate.Rows)
                        {
                            ap = new NaveoOneLib.Models.AccessProfile();
                            ap.UID = Constants.NullInt;
                            ap.TemplateId = Convert.ToInt32(r["UID"].ToString());
                            ap.ModuleId = m.UID;
                            ap.Command = "cmdNaAddress";

                            ap.AllowRead = "0";
                            ap.AllowNew = "0";
                            ap.AllowEdit = "0";
                            ap.AllowDelete = "0";
                            ap.CreatedDate = DateTime.Now;
                            ap.CreatedBy = usrInst.Username;
                            ap.Save(ap, out dsResult, null, sConnStr);
                        }
                    }

                    m = m.GetModulesByName("Drivers & Passengers --> Emp Master File", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Drivers & Passengers --> Emp Master File";
                        m.Description = "Drivers & Passengers --> Emp Master File";
                        m.Command = "cmdEmpBas";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);

                        m = m.GetModulesByName("Drivers & Passengers --> Emp Master File", sConnStr);
                        foreach (DataRow r in dtAccessTemplate.Rows)
                        {
                            ap = new NaveoOneLib.Models.AccessProfile();
                            ap.UID = Constants.NullInt;
                            ap.TemplateId = Convert.ToInt32(r["UID"].ToString());
                            ap.ModuleId = m.UID;
                            ap.Command = "cmdEmpBas";
                            ap.AllowRead = "0";
                            ap.AllowNew = "0";
                            ap.AllowEdit = "0";
                            ap.AllowDelete = "0";
                            ap.CreatedDate = DateTime.Now;
                            ap.CreatedBy = usrInst.Username;
                            ap.Save(ap, out dsResult, null, sConnStr);
                        }
                    }


                    m = m.GetModulesByName("Fuel & Telemetry -->Reports --> Fuel Consumption", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Fuel & Telemetry -->Reports --> Fuel Consumption";
                        m.Description = "Fuel & Telemetry -->Reports --> Fuel Consumption";
                        m.Command = "CmdFuelConsumption";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);

                        m = m.GetModulesByName("Fuel & Telemetry -->Reports --> Fuel Consumption", sConnStr);
                        foreach (DataRow r in dtAccessTemplate.Rows)
                        {
                            ap = new NaveoOneLib.Models.AccessProfile();
                            ap.UID = Constants.NullInt;
                            ap.TemplateId = Convert.ToInt32(r["UID"].ToString());
                            ap.ModuleId = m.UID;
                            ap.Command = "CmdFuelConsumption";
                            ap.AllowRead = "0";
                            ap.AllowNew = "0";
                            ap.AllowEdit = "0";
                            ap.AllowDelete = "0";
                            ap.CreatedDate = DateTime.Now;
                            ap.CreatedBy = usrInst.Username;
                            ap.Save(ap, out dsResult, null, sConnStr);
                        }
                    }
                    m = m.GetModulesByName("Fuel & Telemetry -->Reports --> Refuel Report", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Fuel & Telemetry -->Reports --> Refuel Report";
                        m.Description = "Fuel & Telemetry -->Reports --> Refuel Report";
                        m.Command = "CmdRefuel";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);

                        m = m.GetModulesByName("Fuel & Telemetry -->Reports --> Refuel Report", sConnStr);
                        foreach (DataRow r in dtAccessTemplate.Rows)
                        {
                            ap = new NaveoOneLib.Models.AccessProfile();
                            ap.UID = Constants.NullInt;
                            ap.TemplateId = Convert.ToInt32(r["UID"].ToString());
                            ap.ModuleId = m.UID;
                            ap.Command = "CmdRefuel";
                            ap.AllowRead = "0";
                            ap.AllowNew = "0";
                            ap.AllowEdit = "0";
                            ap.AllowDelete = "0";
                            ap.CreatedDate = DateTime.Now;
                            ap.CreatedBy = usrInst.Username;
                            ap.Save(ap, out dsResult, null, sConnStr);
                        }
                    }

                    m = m.GetModulesByName("Geofencing -->Settings -->Zone Import", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module(); m.ModuleName = "Geofencing -->Settings -->Zone Import";
                        m.Description = "Geofencing -->Settings -->Zone Import";
                        m.Command = "cmdZoneImport";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);

                        m = m.GetModulesByName("Geofencing -->Settings -->Zone Import", sConnStr);
                        foreach (DataRow r in dtAccessTemplate.Rows)
                        {
                            ap = new NaveoOneLib.Models.AccessProfile();
                            ap.UID = Constants.NullInt;
                            ap.TemplateId = Convert.ToInt32(r["UID"].ToString());
                            ap.ModuleId = m.UID;
                            ap.Command = "cmdZoneImport";
                            ap.AllowRead = "0";
                            ap.AllowNew = "0";
                            ap.AllowEdit = "0";
                            ap.AllowDelete = "0";
                            ap.CreatedDate = DateTime.Now;
                            ap.CreatedBy = usrInst.Username;
                            ap.Save(ap, out dsResult, null, sConnStr);
                        }
                    }

                    InsertDBUpdate(strUpd, "New Menu items wrt new menu design");
                }
                #endregion
                #region Update 91. Settings --> System Administration
                strUpd = "Rez000091";
                if (!CheckUpdateExist(strUpd))
                {
                    //Add New Modules
                    NaveoOneLib.Models.Module m = new NaveoOneLib.Models.Module();
                    NaveoOneLib.Models.AccessTemplate at = new NaveoOneLib.Models.AccessTemplate();
                    NaveoOneLib.Models.AccessProfile ap = new NaveoOneLib.Models.AccessProfile();

                    DataTable dtAccessTemplate = at.GetAccessTemplates(usrInst.lMatrix, sConnStr);
                    DataSet dsResult = new DataSet();

                    m = m.GetModulesByName("Settings --> System Administration", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Module();
                        m.ModuleName = "Settings --> System Administration";
                        m.Description = "Settings --> System Administration";
                        m.Command = "";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        m.Save(m, true, sConnStr);

                        m = m.GetModulesByName("Settings --> System Administration", sConnStr);
                        foreach (DataRow r in dtAccessTemplate.Rows)
                        {
                            ap = new NaveoOneLib.Models.AccessProfile();
                            ap.UID = Constants.NullInt;
                            ap.TemplateId = Convert.ToInt32(r["UID"].ToString());
                            ap.ModuleId = m.UID;
                            ap.Command = "cmdSysAdmin";
                            ap.AllowRead = "0";
                            ap.AllowNew = "0";
                            ap.AllowEdit = "0";
                            ap.AllowDelete = "0";
                            ap.CreatedDate = DateTime.Now;
                            ap.CreatedBy = usrInst.Username;
                            ap.Save(ap, out dsResult, null, sConnStr);
                        }
                    }
                    InsertDBUpdate(strUpd, "Settings --> System Administration");
                }
                #endregion
                #region Update 92. Access Profiles
                strUpd = "Rez000092";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"delete from GFI_SYS_AccessProfiles where ModuleId in 
	                            (select UID from GFI_SYS_Modules where MenuHeaderId is null)

                            delete from GFI_SYS_Modules where MenuHeaderId is null";
                    dbExecute(sql);

                    sql = @"CREATE TABLE GFI_AMM_MaintenanceTrip](
	                            Uid] bigint] AUTO_INCREMENT NOT NULL,
	                            MaintID] int] NOT NULL,
	                            CustomTripID] int NULL,
	                            CreatedDate] datetime] NULL,
	                            CreatedBy] nvarchar](30) NULL,
	                            UpdatedDate] datetime] NULL,
	                            UpdatedBy] nvarchar](30) NULL,
	                            CONSTRAINT PK_GFI_AMM_MaintenanceTrip] PRIMARY KEY  (UID ASC)
                            )";
                    if (GetDataDT(ExistTableSql("GFI_AMM_MaintenanceTrip")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_AMM_MaintenanceTrip 
                            ADD CONSTRAINT FK_GFI_AMM_MaintenanceTrip_GFI_AMM_VehicleMaintenance 
                                FOREIGN KEY(MaintID) 
                                REFERENCES GFI_AMM_VehicleMaintenance(URI) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  No Action ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_MaintenanceTrip_GFI_AMM_VehicleMaintenance")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_AMM_MaintenanceTrip 
                            ADD CONSTRAINT FK_GFI_AMM_MaintenanceTrip_GFI_FLT_ExtTripInfo
                                FOREIGN KEY(CustomTripID) 
                                REFERENCES GFI_FLT_ExtTripInfo] ([iID])
                         ON UPDATE  NO ACTION 
	                     ON DELETE  No Action ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_MaintenanceTrip_GFI_FLT_ExtTripInfo")).Rows.Count == 0)
                        dbExecute(sql);

                    Globals.ValidateAccessProfilesModules(sConnStr);
                    InsertDBUpdate(strUpd, "Access Profiles");
                }
                #endregion
                #region Update 93. BCM_DECISION
                strUpd = "Rez000093";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_AMM_ExtZonePropDetails ADD BCM_DECISION] nvarchar](20) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_ExtZonePropDetails", "BCM_DECISION")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "update GFI_SYS_User set Status_b = 'A', LoginCount = 1, PasswordUpdateddate = DATEADD(year,100, GETDATE()) where Username = 'Admin'";
                    dbExecute(sql);

                    sql = "update GFI_SYS_User set Status_b = 'A' where Username not in ('Admin', 'Installer')";
                    dbExecute(sql);

                    GlobalParams g = new GlobalParams();
                    g = g.GetGlobalParamsByName("IdleTimeToLock", sConnStr);
                    if (g == null)
                    {
                        g = new GlobalParams();
                        g.ParamName = "IdleTimeToLock";
                        g.PValue = "60";
                        g.ParamComments = "Idle Time in minutes To Log off automatically";
                        g.Save(g, true, sConnStr);
                    }

                    InsertDBUpdate(strUpd, "BCM_DECISION");
                }
                #endregion
                #region Update 97.GFI_FLT_ExtTripInfo Trip Distance.
                strUpd = "Rez000097";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo] add TripDistance] float NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "TripDistance")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "update GFI_SYS_GlobalParams set ParamName = 'PWD_O' where ParamName = 'O_PWD'";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "GFI_FLT_ExtTripInfo Trip Distance.");
                }
                #endregion
                #region Update 98. Security Config
                strUpd = "Rez000098";
                if (!CheckUpdateExist(strUpd))
                {
                    GlobalParams g = new GlobalParams();
                    g = g.GetGlobalParamsByName("SYS_UserSec", sConnStr);
                    if (g == null)
                    {
                        g = new GlobalParams();
                        g.ParamName = "SYS_UserSec";
                        g.PValue = "No";
                        g.ParamComments = "Implement User Security. Yes/No";
                        g.Save(g, true, sConnStr);
                    }

                    Models.Module module = new Models.Module();
                    module = module.GetModulesByName("MAPS --> Planning Report", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Module();
                        module.ModuleName = "MAPS --> Planning Report";
                        module.Description = "MAPS --> Planning Report";
                        module.Command = "CmdPlanningReport";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        module.Save(module, sConnStr);
                    }

                    module = new Models.Module();
                    module = module.GetModulesByName("Drivers & Passengers --> Team", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Module();
                        module.ModuleName = "Drivers & Passengers --> Team";
                        module.Description = "Drivers & Passengers --> Team";
                        module.Command = "CmdTeam";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        module.Save(module, sConnStr);
                    }

                    Globals.ValidateAccessProfilesModules(sConnStr);
                    InsertDBUpdate(strUpd, "Security Config");
                }
                #endregion
                #endregion

                #region Update 113. Heartbeat table
                strUpd = "Rez000113";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"drop table GFI_SYS_HeartBeat";
                    if (GetDataDT(ExistTableSql("GFI_SYS_HeartBeat")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_GPS_HeartBeat]
                        (
                            AssetID] int NOT NULL,
                            DateTimeGPS_UTC] datetime] NULL,
                            DateTimeServer] datetime] NULL,
                            Longitude] float] NULL,
                            Latitude] float] NULL,
                                CONSTRAINT PK_GFI_GPS_HeartBeat] PRIMARY KEY ([AssetID] ASC)
                        )";
                    if (GetDataDT(ExistTableSql("GFI_GPS_HeartBeat")).Rows.Count == 0)
                        dbExecute(sql);

                    GlobalParams param = new GlobalParams();
                    param = param.GetGlobalParamsByName("TickerMessage", sConnStr);
                    if (param == null)
                    {
                        param = new GlobalParams();
                        param.ParamComments = "TickerMessage";
                        param.ParamName = "TickerMessage";
                        param.PValue = "*** Welcome to Naveo ***";
                        param.Save(param, true, sConnStr);
                    }
                    InsertDBUpdate(strUpd, "Heartbeat table");
                }
                #endregion
                #region Update 114. Exceptions DateTimeGPS_UTC
                strUpd = "Rez000114";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"alter table GFI_GPS_Exceptions add DateTimeGPS_UTC datetime default GetUTCDate() not null";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_Exceptions", "DateTimeGPS_UTC")).Rows.Count == 0)
                    {
                        dbExecuteWithoutTransaction(sql, 4000);

                        sql = @"update GFI_GPS_Exceptions 
	                                set DateTimeGPS_UTC = g.DateTimeGPS_UTC
                                from GFI_GPS_Exceptions t1
	                                inner join GFI_GPS_GPSData g on g.UID = t1.GPSDataID";
                        dbExecuteWithoutTransaction(sql, 4000);
                    }

                    sql = @"alter table GFI_ARC_Exceptions add DateTimeGPS_UTC datetime default GetUTCDate() not null";
                    if (GetDataDT(ExistColumnSql("GFI_ARC_Exceptions", "DateTimeGPS_UTC")).Rows.Count == 0)
                    {
                        dbExecuteWithoutTransaction(sql, 4000);

                        sql = @"update GFI_ARC_Exceptions 
	                                set DateTimeGPS_UTC = g.DateTimeGPS_UTC
                                from GFI_ARC_Exceptions t1
	                                inner join GFI_ARC_GPSData g on g.UID = t1.GPSDataID";
                        dbExecuteWithoutTransaction(sql, 4000);
                    }

                    sql = "Alter table GFI_SYS_GlobalParams alter column PValue nvarchar(max)";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "Exceptions DateTimeGPS_UTC");
                }
                #endregion
                #region Update 115. GFI_FLT_ZoneHeader isMarker n iBuffer
                strUpd = "Rez000115";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_FLT_ZoneHeader] add isMarker] int] NOT NULL DEFAULT ((0))";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ZoneHeader", "isMarker")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_FLT_ZoneHeader] add iBuffer] int] NOT NULL DEFAULT ((0))";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ZoneHeader", "iBuffer")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "GFI_FLT_ZoneHeader isMarker n iBuffer");
                }
                #endregion
                #region Update 116. sMisc Col in GFI_FLT_AutoReprotingConfigDetails
                strUpd = "Rez000116";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_FLT_AutoReprotingConfigDetails] add sMisc] nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AutoReprotingConfigDetails", "sMisc")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "sMisc Col in GFI_FLT_AutoReprotingConfigDetails");
                }
                #endregion
                #region Update 117. TimeZone Table
                strUpd = "Rez000117";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"
CREATE TABLE GFI_SYS_TimeZones] 
(
	[TimeZoneID] INT AUTO_INCREMENT NOT NULL,
	[DisplayName] NVARCHAR(100) NOT NULL,
	[StandardName] NVARCHAR (100) NOT NULL,
	[HasDST] INT NOT NULL,
	[UTCOffset] NVARCHAR (10) NOT NULL
	CONSTRAINT PK_GFI_SYS_TimeZones] PRIMARY KEY  ([TimeZoneID] ASC)
);
";
                    if (GetDataDT(ExistTableSql("GFI_SYS_TimeZones")).Rows.Count == 0)
                        dbExecute(sql);

                    foreach (TimeZoneInfo z in TimeZoneInfo.GetSystemTimeZones())
                    {
                        if (z.SupportsDaylightSavingTime)
                            sql = ("INSERT INTO GFI_SYS_TimeZones (DisplayName, StandardName, HasDST, UTCOffset) VALUES ('" + z.DisplayName.Replace("'", "''") + "', '" + z.StandardName.Replace("'", "''") + "', 1, '" + z.BaseUtcOffset + "')");
                        else
                            sql = ("INSERT INTO GFI_SYS_TimeZones (DisplayName, StandardName, HasDST, UTCOffset) VALUES ('" + z.DisplayName.Replace("'", "''") + "', '" + z.StandardName.Replace("'", "''") + "', 0, '" + z.BaseUtcOffset + "')");

                        dbExecute(sql);
                    }

                    sql = @"CREATE UNIQUE  INDEX IX_GFI_SYS_TimeZones ON GFI_SYS_TimeZones (StandardName)";
                    if (GetDataDT(ExistsSQLConstraint("IX_GFI_SYS_TimeZones")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "GFI_SYS_TimeZones");
                }
                #endregion
                #region Update 118. GFI_SYS_Notification Index
                strUpd = "Rez000118";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "drop INDEX MyIdxGet_GFI_SYS_Notification ON GFI_SYS_Notification] ";
                    if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "MyIdxGet_GFI_SYS_Notification")).Rows.Count > 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX MyIdxGet_GFI_SYS_Notification
                            ON GFI_SYS_Notification] ([UID],[Status],[ReportID])
                            INCLUDE ([ExceptionID],[Asset],[RuleName],[DateTimeGPS_UTC])";
                    if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "MyIdxGet_GFI_SYS_Notification")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "GFI_SYS_Notification Index");
                }
                #endregion
                #region Update 119. TimeZoneID.
                strUpd = "Rez000119";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "update GFI_FLT_Asset set TimeZoneID = 'Arabian Standard Time' where TimeZoneID = ''";
                    dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD MaxDayMail int not NULL Default 25";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "MaxDayMail")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "TimeZoneID, MaxDayMail");
                }
                #endregion
                #region Update 120. MOLG BCM_SEQ
                strUpd = "Rez000120";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_AMM_ExtZonePropDetails] add BCM_SEQ] int Not NULL Default -1";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_ExtZonePropDetails", "BCM_SEQ")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_AMM_ExtZonePropAssessment] add ASSESS_SEQ] int Not NULL Default -1";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_ExtZonePropAssessment", "ASSESS_SEQ")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "BCM_SEQ");
                }
                #endregion
                #region Update 121. GFI_AMM_ExtZonePropAssessment ASSESS_Status
                strUpd = "Rez000121";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "alter table GFI_AMM_ExtZonePropAssessment alter column ASSESS_Status] nvarchar(15)";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "GFI_AMM_ExtZonePropAssessmentStatus");
                }
                #endregion

                #region Update 95. GFI_GPS_TripHeader ScoreCard
                strUpd = "Rez000095";
                if (!CheckUpdateExist(strUpd))
                {
                    //GFI_GPS_TripHeader
                    sql = @"ALTER TABLE GFI_GPS_TripHeader ADD OverSpeed1Time float] NOT NULL Default 0";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "OverSpeed1Time")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_GPS_TripHeader ADD OverSpeed2Time float] NOT NULL Default 0";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "OverSpeed2Time")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_GPS_TripHeader ADD Accel int] NOT NULL Default 0";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "Accel")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_GPS_TripHeader ADD Brake int] NOT NULL Default 0";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "Brake")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_GPS_TripHeader ADD Corner int] NOT NULL Default 0";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "Corner")).Rows.Count == 0)
                        dbExecute(sql);

                    //GFI_ARC_TripHeader
                    sql = @"ALTER TABLE GFI_ARC_TripHeader ADD OverSpeed1Time float] NOT NULL Default 0";
                    if (GetDataDT(ExistColumnSql("GFI_ARC_TripHeader", "OverSpeed1Time")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_ARC_TripHeader ADD OverSpeed2Time float] NOT NULL Default 0";
                    if (GetDataDT(ExistColumnSql("GFI_ARC_TripHeader", "OverSpeed2Time")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_ARC_TripHeader ADD Accel int] NOT NULL Default 0";
                    if (GetDataDT(ExistColumnSql("GFI_ARC_TripHeader", "Accel")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_ARC_TripHeader ADD Brake int] NOT NULL Default 0";
                    if (GetDataDT(ExistColumnSql("GFI_ARC_TripHeader", "Brake")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_ARC_TripHeader ADD Corner int] NOT NULL Default 0";
                    if (GetDataDT(ExistColumnSql("GFI_ARC_TripHeader", "Corner")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "TripHeader ScoreCard");
                }
                #endregion
                #region Update 96. GFI_SYS_Variables
                strUpd = "Rez000096";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_SYS_Variables]
                        (
                            iID] int AUTO_INCREMENT NOT NULL,
                            sName] NVARCHAR(50) NOT NULL,
                            sDesc] NVARCHAR(100) NOT NULL,
                            iValue] int] NULL,
                            fValue] float] NULL,
                            sValue] NVARCHAR(100) NULL,
                            dtValue] datetime] NULL,
                                CONSTRAINT PK_GFI_SYS_Variables] PRIMARY KEY ([iID] ASC)
                        )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_Variables")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"insert into GFI_SYS_Variables]  (sName, sDesc, dtValue)
	                            Select 'SendFuelToIBM', 'Last Fuel Sent To IBM', GetUTCDate() where not exists (select sName from GFI_SYS_Variables where sName = 'SendFuelToIBM')";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "GFI_SYS_Variables");
                }
                #endregion

                #region Update 99. Trip Report index
                strUpd = "Rez000099";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "drop index IX_Trip] ON GFI_GPS_TripHeader]";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "IX_Trip")).Rows.Count > 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX IX_Trip] ON GFI_GPS_TripHeader] ([AssetID], dtStart],[dtEnd])
	                        INCLUDE (
				                        iID], DriverID], ExceptionFlag], MaxSpeed]
				                        , IdlingTime], StopTime], TripDistance], TripTime]
				                        , GPSDataStartUID], GPSDataEndUID], fStartLon], fStartLat], fEndLon], fEndtLat]
			                        )";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "IX_Trip")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "Trip Report index");
                }
                #endregion
                #region Update 100. GFI_GPS_Exceptions Archive Index
                strUpd = "Rez000100";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE  INDEX Ix_Excep_Archive ON GFI_GPS_Exceptions] ([AssetID],[InsertedAt]) INCLUDE ([iID],[RuleID],[GPSDataID],[DriverID],[Severity],[GroupID],[Posted],[DateTimeGPS_UTC])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_Exceptions", "Ix_Excep_Archive")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "GFI_GPS_Exceptions Archive Index");
                }
                #endregion

                #region Update 102. Security Monitoring
                strUpd = "Rez000102";
                if (!CheckUpdateExist(strUpd))
                {
                    //Assets
                    sql = @"CREATE TABLE GFI_FLT_SecuMonitorAssets] 
                            (
	                            iID] INT IDENTITY (-1, -1) NOT NULL,
	                            DisplayName] NVARCHAR(100) NOT NULL,
	                            DeviceID] NVARCHAR (100) NOT NULL,
                                TimeZoneID] nvarchar(50) NOT NULL DEFAULT 'Arabian Standard Time',
                                PanicID] nvarchar(5) Not Null Default 'Aux1',
                                PanicValue] nvarchar(5) Not Null Default 'True'
	                            CONSTRAINT PK_GFI_FLT_SecuMonitorAssets] PRIMARY KEY  ([iID] ASC)
                            );";
                    if (GetDataDT(ExistTableSql("GFI_FLT_SecuMonitorAssets")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE UNIQUE  INDEX IX_UK_SecuMonitorAssets_DeviceID ON GFI_FLT_SecuMonitorAssets (DeviceID)";
                    if (GetDataDT(ExistsIndex("GFI_FLT_SecuMonitorAssets", "IX_UK_SecuMonitorAssets_DeviceID")).Rows.Count == 0)
                        dbExecute(sql);

                    //Users
                    sql = @"ALTER TABLE GFI_SYS_User ADD UsrTypeID int] NOT NULL Default 0";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "UsrTypeID")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD UsrTypeDesc nvarchar(50) NOT NULL Default 'Std User'";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "UsrTypeDesc")).Rows.Count == 0)
                        dbExecute(sql);

                    //Live
                    sql = @"CREATE TABLE GFI_GPS_SecuMonitorLiveData](
	                        UID] int] AUTO_INCREMENT NOT NULL,
	                        AssetID] int] NULL,
	                        DateTimeGPS_UTC] datetime] NULL,
	                        DateTimeServer] datetime] NULL,
	                        Longitude] float] NULL,
	                        Latitude] float] NULL,
	                        LongLatValidFlag] int] NULL,
	                        Speed] float] NULL,
	                        EgineOn] int] NULL,
	                        StopFlag] int] NULL,
	                        TripDistance] float] NULL,
	                        TripTime] float] NULL,
	                        WorkHour] int] NULL,
	                        DriverID] int] NOT NULL,
                         CONSTRAINT PK_GFI_GPS_SecuMonitorLiveData] PRIMARY KEY ([UID] ASC)
                        )";
                    if (GetDataDT(ExistTableSql("GFI_GPS_SecuMonitorLiveData")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_GPS_SecuMonitorLiveDataDetail](
	                        GPSDetailID] bigint] AUTO_INCREMENT NOT NULL,
	                        UID] int] NULL,
	                        TypeID] nvarchar](30) NULL,
	                        TypeVaule] nvarchar](30) NULL,
	                        UOM] nvarchar](15) NULL,
	                        Remarks] nvarchar](50) NULL,
                         CONSTRAINT PK_GFI_GPS_SecuMonitorLiveDataDetail] PRIMARY KEY  ([GPSDetailID] ASC)
                        )";
                    if (GetDataDT(ExistTableSql("GFI_GPS_SecuMonitorLiveDataDetail")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_GPS_SecuMonitorLiveDataDetail]
                            ADD  CONSTRAINT FK_GFI_GPS_SecuMonitorLiveDataDetail_GFI_GPS_SecuMonitorLiveData]
                                FOREIGN KEY([UID])
                            REFERENCES GFI_GPS_SecuMonitorLiveData] ([UID])
                            ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_SecuMonitorLiveDataDetail_GFI_GPS_SecuMonitorLiveData")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_GPS_SecuMonitorLiveData 
                            ADD CONSTRAINT FK_GFI_GPS_SecuMonitorLiveData_GFI_FLT_SecuMonitorAssets 
                                FOREIGN KEY(AssetID) 
                                REFERENCES GFI_FLT_SecuMonitorAssets(iID) 
                            ON UPDATE  NO ACTION 
	                        ON DELETE cascade";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_SecuMonitorLiveData_GFI_FLT_SecuMonitorAssets")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "DROP INDEX myIX_GFI_GPS_SecuMonitorLiveData] ON GFI_GPS_SecuMonitorLiveData]";
                    if (GetDataDT(ExistsIndex("GFI_GPS_SecuMonitorLiveData", "myIX_GFI_GPS_SecuMonitorLiveData")).Rows.Count > 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX myIX_GFI_GPS_SecuMonitorLiveData] ON GFI_GPS_SecuMonitorLiveData]
                        (
	                        LongLatValidFlag] ASC,
	                        DateTimeGPS_UTC] ASC
                        )
                        INCLUDE ( 	
                            UID],
	                        AssetID],
	                        DateTimeServer],
	                        Longitude],
	                        Latitude],
	                        Speed],
	                        EgineOn],
	                        StopFlag],
	                        TripDistance],
	                        TripTime],
	                        DriverID]
                                )";
                    if (GetDataDT(ExistsIndex("GFI_GPS_SecuMonitorLiveData", "myIX_GFI_GPS_SecuMonitorLiveData")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "DROP INDEX MyIX_GFI_GPS_SecuMonitorLiveDataDetail] ON GFI_GPS_SecuMonitorLiveDataDetail] ";
                    if (GetDataDT(ExistsIndex("GFI_GPS_SecuMonitorLiveDataDetail", "MyIX_GFI_GPS_SecuMonitorLiveDataDetail")).Rows.Count > 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX MyIX_GFI_GPS_SecuMonitorLiveDataDetail
                            ON GFI_GPS_SecuMonitorLiveDataDetail] ([UID])
                            INCLUDE ([GPSDetailID])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_SecuMonitorLiveDataDetail", "MyIX_GFI_GPS_SecuMonitorLiveDataDetail")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);


                    //LiveToShow
                    sql = @"CREATE TABLE GFI_FLT_SecuMonitorToBSeen] 
                            (
	                            AssetID] int NOT NULL,
	                            CONSTRAINT PK_GFI_FLT_SecuMonitorToBSeen] PRIMARY KEY  ([AssetID] ASC)
                            );";
                    if (GetDataDT(ExistTableSql("GFI_FLT_SecuMonitorToBSeen")).Rows.Count == 0)
                        dbExecute(sql);

                    //Admin User
                    sql = "update GFI_SYS_User set UsrTypeID = 1, UsrTypeDesc = 'Secu Monitor' where UID = 2";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "Security Monitoring");
                }
                #endregion

                #region Update 122. SP Maint updated
                strUpd = "Rez000122";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"
ALTER PROCEDURE sp_AMM_VehicleMaint_ProcessRecords]
AS
BEGIN
    /***********************************************************************************************************************
    Version: 3.1.0.0
    Modifed: 11/ 08/2016.	06/08/2014
    Created By: Perry Ramen
    Modified By: Reza Dowlut
    Description: Process list of Maintenance Types for each vehicle to determine if they are To Schedule] or Overdue]
    ***********************************************************************************************************************/
        BEGIN
     	DECLARE @iRecCountProcessed int
     	
     	DECLARE @iMaintStatusId_Overdue int
		DECLARE @iMaintStatusId_ToSchedule int
		DECLARE @iMaintStatusId_Scheduled int
		DECLARE @iMaintStatusId_InProgress int
		DECLARE @iMaintStatusId_Completed int
		DECLARE @iMaintStatusId_Valid int
		DECLARE @iMaintStatusId_Expired int
		
		DECLARE @URI int
		DECLARE @MaintURI int
		DECLARE @AssetId int
		DECLARE @MaintTypeId int
		DECLARE @OccurrenceType int
     	DECLARE @NextMaintDate DateTime
		DECLARE @NextMaintDateTG DateTime 
		DECLARE @CurrentOdometer int
		DECLARE @NextMaintOdometer int
		DECLARE @NextMaintOdometerTG int
		DECLARE @CurrentEngHrs int
		DECLARE @NextMaintEngHrs int
		DECLARE @NextMaintEngHrsTG int
		DECLARE @MaintStatusId int
		
		DECLARE @MaintURIUpdate int
		
		SET @iMaintStatusId_Overdue = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Overdue')
		SET @iMaintStatusId_ToSchedule = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'To Schedule')
		SET @iMaintStatusId_Scheduled = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Scheduled')
		SET @iMaintStatusId_InProgress = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'In Progress')		
		SET @iMaintStatusId_Completed = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Completed')				
		SET @iMaintStatusId_Valid = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Valid')		
		SET @iMaintStatusId_Expired = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Expired')				

		update GFI_AMM_VehicleMaintTypesLink set CurrentOdometer = GetCalcOdo(AssetId)

		DECLARE csrData CURSOR FOR
		SELECT URI, 
		    vmtl.MaintURI,
		    vmtl.AssetId,
		    vmtl.MaintTypeId,
		    vmt.OccurrenceType,
			vmtl.NextMaintDate, 
			vmtl.NextMaintDateTG, 
			vmtl.CurrentOdometer, 
			vmtl.NextMaintOdometer, 
			vmtl.NextMaintOdometerTG, 
			vmtl.CurrentEngHrs,
			vmtl.NextMaintEngHrs,
			vmtl.NextMaintEngHrsTG,
			vmtl.[Status]
		FROM GFI_AMM_VehicleMaintTypesLink vmtl
		INNER JOIN GFI_AMM_VehicleMaintTypes vmt
		ON vmtl.MaintTypeId = vmt.MaintTypeId
		
		OPEN csrData
		FETCH NEXT FROM csrData INTO @URI, @MaintURI, @AssetId, @MaintTypeId, @OccurrenceType, @NextMaintDate, @NextMaintDateTG, @CurrentOdometer, @NextMaintOdometer, @NextMaintOdometerTG, @CurrentEngHrs, @NextMaintEngHrs, @NextMaintEngHrsTG, @MaintStatusId
		SET @iRecCountProcessed = 0
		
		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF @NextMaintDate IS NULL SET @NextMaintDate = GETDATE()
			                        
			-- Recurring - Flag As: To Schedule
			IF (
				(@OccurrenceType = 2) 
				AND ((@NextMaintDateTG <= GETDATE()) OR (@NextMaintOdometerTG < @CurrentOdometer) OR (@NextMaintEngHrsTG < @CurrentEngHrs)) 
				AND ((@MaintStatusId = @iMaintStatusId_Completed) OR (@MaintStatusId = @iMaintStatusId_Valid)))
			BEGIN
				IF NOT EXISTS(SELECT * FROM GFI_AMM_VehicleMaintTypesLink WHERE AssetId = @AssetId AND MaintTypeId = @MaintTypeId AND URI > @URI)
				BEGIN
					IF (@MaintStatusId = @iMaintStatusId_Completed)
					BEGIN
						INSERT GFI_AMM_VehicleMaintenance (AssetId, MaintTypeId_cbo, MaintStatusId_cbo, StartDate, EndDate, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy) 
						VALUES (@AssetId, @MaintTypeId, @iMaintStatusId_ToSchedule, @NextMaintDate, @NextMaintDate, GETDATE(), CURRENT_USER, GETDATE(), CURRENT_USER)
					
						SET @MaintURIUpdate = (SELECT SCOPE_IDENTITY())
											
						UPDATE GFI_AMM_VehicleMaintTypesLink
						SET MaintURI = @MaintURIUpdate, Status = @iMaintStatusId_ToSchedule, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
						WHERE URI = @URI
					END
					ELSE IF (@MaintStatusId = @iMaintStatusId_Valid)
					BEGIN
						SET @NextMaintDate = DATEADD(DD, 1, @NextMaintDate)
												
						INSERT GFI_AMM_VehicleMaintenance (AssetId, MaintTypeId_cbo, MaintStatusId_cbo, StartDate, EndDate, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy) 
						VALUES (@AssetId, @MaintTypeId, @iMaintStatusId_ToSchedule, @NextMaintDate, @NextMaintDate, GETDATE(), CURRENT_USER, GETDATE(), CURRENT_USER)
					
						SET @MaintURIUpdate = (SELECT SCOPE_IDENTITY())
												
						INSERT GFI_AMM_VehicleMaintTypesLink (MaintURI, AssetId, MaintTypeId, NextMaintDate, NextMaintDateTG, Status, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy)
						VALUES (@MaintURIUpdate, @AssetId, @MaintTypeId, @NextMaintDate, @NextMaintDate, @iMaintStatusId_ToSchedule, GETDATE(), CURRENT_USER, GETDATE(), CURRENT_USER)
					END
				END
			END

			--Flag As: Overdue
			IF (((@NextMaintDate <= GETDATE()) OR (@NextMaintOdometer < @CurrentOdometer) OR (@NextMaintEngHrs < @CurrentEngHrs)) AND (@MaintStatusId = @iMaintStatusId_ToSchedule))
			BEGIN				
				UPDATE GFI_AMM_VehicleMaintTypesLink
				SET Status = @iMaintStatusId_Overdue, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
				WHERE URI = @URI
				
				UPDATE GFI_AMM_VehicleMaintenance 
				SET MaintStatusId_cbo = @iMaintStatusId_Overdue, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
				WHERE URI = @MaintURI
			END
			                        
			--Auto Expiry: Lapsed Insurance
			IF ((@MaintStatusId = @iMaintStatusId_Valid) AND (@NextMaintDate < GETDATE()))
			BEGIN
				UPDATE GFI_AMM_VehicleMaintTypesLink
				SET Status = @iMaintStatusId_Expired , UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
				WHERE URI = @URI
				                        
				UPDATE GFI_AMM_VehicleMaintenance 
				SET MaintStatusId_cbo = @iMaintStatusId_Expired, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
				WHERE URI = @MaintURI
			END
										
			SET @iRecCountProcessed = @iRecCountProcessed + 1
			
			FETCH NEXT FROM csrData INTO @URI, @MaintURI, @AssetId, @MaintTypeId, @OccurrenceType, @NextMaintDate, @NextMaintDateTG, @CurrentOdometer, @NextMaintOdometer, @NextMaintOdometerTG, @CurrentEngHrs, @NextMaintEngHrs, @NextMaintEngHrsTG, @MaintStatusId
		END
		
		CLOSE csrData
		DEALLOCATE csrData
		
		RETURN @iRecCountProcessed
    END
END";
                    dbExecute(sql);
                    InsertDBUpdate(strUpd, "SP Maint updated");
                }
                #endregion
                #region Update 123. Excp Idx and Init Clean Ids after Migration
                strUpd = "Rez000123";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "CREATE  INDEX IX_Posted ON GFI_GPS_Exceptions] ([Posted]) INCLUDE ([iID])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_Exceptions", "IX_Posted")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"
delete from GFI_SYS_INIT where upd_id = 'DIN0000001' and exists (select * from GFI_SYS_INIT where upd_id = 'Rez000001')
delete from GFI_SYS_INIT where upd_id = 'DIN000002'  and exists (select * from GFI_SYS_INIT where upd_id = 'Rez000002') 

delete from GFI_SYS_INIT where upd_id = 'CPR000018'	 and exists (select * from GFI_SYS_INIT where upd_id = 'Rez000018') 
delete from GFI_SYS_INIT where upd_id = 'CPR000019'	 and exists (select * from GFI_SYS_INIT where upd_id = 'Rez000019') 

delete from GFI_SYS_INIT where upd_id = 'DIN000055'	 and exists (select * from GFI_SYS_INIT where upd_id = 'Rez000055') 
delete from GFI_SYS_INIT where upd_id = 'DIN0000101' and exists (select * from GFI_SYS_INIT where upd_id = 'Rez000101') 
delete from GFI_SYS_INIT where upd_id = 'DIN0000103' and exists (select * from GFI_SYS_INIT where upd_id = 'Rez000103') 
delete from GFI_SYS_INIT where upd_id = 'DIN0000104' and exists (select * from GFI_SYS_INIT where upd_id = 'Rez000104') 
delete from GFI_SYS_INIT where upd_id = 'DIN0000105' and exists (select * from GFI_SYS_INIT where upd_id = 'Rez000105') 
delete from GFI_SYS_INIT where upd_id = 'DIN0000106' and exists (select * from GFI_SYS_INIT where upd_id = 'Rez000106') 
delete from GFI_SYS_INIT where upd_id = 'DIN0000107' and exists (select * from GFI_SYS_INIT where upd_id = 'Rez000107') 

delete from GFI_SYS_INIT where upd_id = 'Vis0000086' and exists (select * from GFI_SYS_INIT where upd_id = 'Rez000086') 
delete from GFI_SYS_INIT where upd_id = 'Vis000087'	 and exists (select * from GFI_SYS_INIT where upd_id = 'Rez000087') 
delete from GFI_SYS_INIT where upd_id = 'DIN0000090' and exists (select * from GFI_SYS_INIT where upd_id = 'Rez000090') 
delete from GFI_SYS_INIT where upd_id = 'DIN0000091' and exists (select * from GFI_SYS_INIT where upd_id = 'Rez000091') 
";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "Excp Idx and Init Clean Ids after Migration");
                }
                #endregion
                #region Update 124. Missing Indexes
                strUpd = "Rez000124";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "CREATE INDEX IX_GFI_GPS_ProcessPending_ProcessCode_Processing] ON GFI_GPS_ProcessPending] ([ProcessCode], Processing]) INCLUDE ([AssetID], dtDateFrom])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_ProcessPending", "IX_GFI_GPS_ProcessPending_ProcessCode_Processing")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_GPS_GPSDataDetail_UID_TypeID] ON GFI_GPS_GPSDataDetail] ([UID], TypeID])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSDataDetail", "IX_GFI_GPS_GPSDataDetail_UID_TypeID")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_SYS_GroupMatrixZone_GMID] ON GFI_SYS_GroupMatrixZone] ([GMID]) INCLUDE ([MID], iID])";
                    if (GetDataDT(ExistsIndex("GFI_SYS_GroupMatrixZone", "IX_GFI_SYS_GroupMatrixZone_GMID")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_GPS_Exceptions_RuleID_GPSDataID] ON GFI_GPS_Exceptions] ([RuleID], GPSDataID])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_Exceptions", "IX_GFI_GPS_Exceptions_RuleID_GPSDataID")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_GPS_Exceptions_RuleID_AssetID] ON GFI_GPS_Exceptions] ([RuleID], AssetID]) INCLUDE ([InsertedAt])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_Exceptions", "IX_GFI_GPS_Exceptions_RuleID_AssetID")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_SYS_Notification_UID_Status_ReportID] ON GFI_SYS_Notification] ([UID], Status],[ReportID]) INCLUDE ([SendAt])";
                    if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "IX_GFI_SYS_Notification_UID_Status_ReportID")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_SYS_Notification_RuleName_email] ON GFI_SYS_Notification] ([RuleName], email]) INCLUDE ([NID], ARID], ExceptionID], UID], Status], InsertedAt], SendAt], UIDCategory], ReportID], Lat], Lon], Driver], Asset], DateTimeGPS_UTC], name], ParentRuleID], GPSDataUID], Err])";
                    if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "IX_GFI_SYS_Notification_RuleName_email")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_SYS_Notification_ParentRuleID] ON GFI_SYS_Notification] ([ParentRuleID]) INCLUDE ([NID], ARID], ExceptionID], UID], Status], InsertedAt], SendAt], UIDCategory], ReportID], Lat], Lon], Driver], Asset], RuleName], email], DateTimeGPS_UTC], name], GPSDataUID], Err])";
                    if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "IX_GFI_SYS_Notification_ParentRuleID")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_SYS_Notification_ARID_Status] ON GFI_SYS_Notification] ([ARID], Status]) INCLUDE ([NID], ExceptionID], UID], InsertedAt], SendAt], UIDCategory], ReportID], Lat], Lon], Driver], Asset], RuleName], email], DateTimeGPS_UTC], name], ParentRuleID], GPSDataUID], Err])";
                    if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "IX_GFI_SYS_Notification_ARID_Status")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_GPS_TripHeader_dtStart_dtEnd] ON GFI_GPS_TripHeader] ([dtStart], dtEnd]) INCLUDE ([iID], AssetID], DriverID], ExceptionFlag], MaxSpeed], IdlingTime], StopTime], TripDistance], TripTime], GPSDataStartUID], GPSDataEndUID], fStartLon], fStartLat], fEndLon], fEndtLat], OverSpeed1Time], OverSpeed2Time], Accel], Brake], Corner])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "IX_GFI_GPS_TripHeader_dtStart_dtEnd")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_GPS_LiveData_DateTimeGPS_UTC] ON GFI_GPS_LiveData] ([DateTimeGPS_UTC]) INCLUDE ([UID], AssetID], DateTimeServer], Longitude], Latitude], LongLatValidFlag], Speed], EgineOn], StopFlag], TripDistance], TripTime], DriverID])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_LiveData", "IX_GFI_GPS_LiveData_DateTimeGPS_UTC")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_FLT_ZoneHeadZoneType_ZoneHeadID] ON GFI_FLT_ZoneHeadZoneType] ([ZoneHeadID]) INCLUDE ([ZoneTypeID], iID])";
                    if (GetDataDT(ExistsIndex("GFI_FLT_ZoneHeadZoneType", "IX_GFI_FLT_ZoneHeadZoneType_ZoneHeadID")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_FLT_ZoneHeadZoneType_ZoneTypeID] ON GFI_FLT_ZoneHeadZoneType] ([ZoneTypeID]) INCLUDE ([ZoneHeadID])";
                    if (GetDataDT(ExistsIndex("GFI_FLT_ZoneHeadZoneType", "IX_GFI_FLT_ZoneHeadZoneType_ZoneTypeID")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_FLT_AutoReportingConfig_TargetID_ReportID] ON GFI_FLT_AutoReportingConfig] ([TargetID],[ReportID]) INCLUDE ([ARID], ReportPeriod])";
                    if (GetDataDT(ExistsIndex("GFI_FLT_AutoReportingConfig", "IX_GFI_FLT_AutoReportingConfig_TargetID_ReportID")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_ARC_GPSData_AssetID] ON GFI_ARC_GPSData] ([AssetID]) INCLUDE ([UID])";
                    if (GetDataDT(ExistsIndex("GFI_ARC_GPSData", "IX_GFI_ARC_GPSData_AssetID")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_ARC_GPSData_DateTimeGPS_UTC] ON GFI_ARC_GPSData] ([DateTimeGPS_UTC]) INCLUDE ([UID], AssetID])";
                    if (GetDataDT(ExistsIndex("GFI_ARC_GPSData", "IX_GFI_ARC_GPSData_DateTimeGPS_UTC")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_ARC_Exceptions_RuleID] ON GFI_ARC_Exceptions] ([RuleID]) INCLUDE ([iID])";
                    if (GetDataDT(ExistsIndex("GFI_ARC_Exceptions", "IX_GFI_ARC_Exceptions_RuleID")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "Missing Indexes");
                }
                #endregion
                #region Update 125. Delete wrong rules and auto report
                strUpd = "Rez000125";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"
--delete from GFI_GPS_Rules where ParentRuleID in (select ParentRuleID from GFI_GPS_Rules group by ParentRuleID having count(distinct(Struc)) < 3)
select ParentRuleID, count(distinct(Struc)) CntStruc from GFI_GPS_Rules group by ParentRuleID having count(distinct(Struc)) < 3 order by ParentRuleID

delete from GFI_FLT_AutoReportingConfig where arid in
(select arc.ARID 
from GFI_FLT_AutoReportingConfig  arc
    inner join GFI_FLT_AutoReprotingConfigDetails d on d.ARID = arc.ARID 
where arc.ReportID < 100    
group by arc.ARID having count(distinct(d.UIDType)) < 2
)

select arc.ARID, count(distinct(d.UIDType)) CntStruc 
from GFI_FLT_AutoReportingConfig  arc
    inner join GFI_FLT_AutoReprotingConfigDetails d on d.ARID = arc.ARID 
where arc.ReportID < 100    
group by arc.ARID having count(distinct(d.UIDType)) < 2 order by arc.ARID
";

                    sql = @"delete from GFI_GPS_Rules where ParentRuleID in (select ParentRuleID from GFI_GPS_Rules group by ParentRuleID having count(distinct(Struc)) < 3)";
                    dbExecute(sql, null, 8000);

                    sql = @"
delete from GFI_FLT_AutoReportingConfig where arid in
(select arc.ARID 
from GFI_FLT_AutoReportingConfig  arc
    inner join GFI_FLT_AutoReprotingConfigDetails d on d.ARID = arc.ARID 
where arc.ReportID < 100    
group by arc.ARID having count(distinct(d.UIDType)) < 2
)
";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "Delete wrong rules and auto report");
                }
                #endregion

                #region Update 126. Notification Table
                strUpd = "Rez000126";
                if (!CheckUpdateExist(strUpd))
                {
                    //AssetID
                    sql = @"ALTER TABLE GFI_SYS_Notification ADD AssetID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "AssetID")).Rows.Count == 0)
                    {
                        dbExecute(sql);

                        sql = @"update GFI_SYS_Notification 
	                                set AssetID = a.AssetID
                                from GFI_SYS_Notification n
	                                inner join GFI_FLT_Asset a on a.AssetNumber = n.Asset";
                        dbExecute(sql, null, 8000);

                        sql = "DROP INDEX MyIdxGet_GFI_SYS_Notification] ON GFI_SYS_Notification]";
                        if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "MyIdxGet_GFI_SYS_Notification")).Rows.Count == 1)
                            dbExecute(sql);

                        sql = @"DROP INDEX IX_GFI_SYS_Notification_ARID_Status] ON GFI_SYS_Notification]";
                        if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "IX_GFI_SYS_Notification_ARID_Status")).Rows.Count == 1)
                            dbExecute(sql);

                        sql = "DROP INDEX IX_GFI_SYS_Notification_ParentRuleID] ON GFI_SYS_Notification]";
                        if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "IX_GFI_SYS_Notification_ParentRuleID")).Rows.Count == 1)
                            dbExecute(sql);

                        sql = "DROP INDEX IX_GFI_SYS_Notification_RuleName_email] ON GFI_SYS_Notification]";
                        if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "IX_GFI_SYS_Notification_RuleName_email")).Rows.Count == 1)
                            dbExecute(sql);

                        sql = @"alter table GFI_SYS_Notification drop column Asset";
                        dbExecute(sql);

                        sql = @"ALTER TABLE GFI_SYS_Notification 
                                    ADD CONSTRAINT FK_GFI_SYS_Notification_GFI_FLT_Asset 
                                        FOREIGN KEY(AssetID) 
                                        REFERENCES GFI_FLT_Asset(AssetID) 
                                 ON UPDATE  NO ACTION 
	                             ON DELETE  NO ACTION ";
                        if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_Notification_GFI_FLT_Asset")).Rows.Count == 0)
                            dbExecute(sql);
                    }

                    sql = @"alter table GFI_SYS_Notification drop column RuleName";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "RuleName")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"alter table GFI_SYS_Notification drop column Email";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "Email")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"alter table GFI_SYS_Notification drop column Name";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "Name")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"alter table GFI_SYS_Notification drop column Lat";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "Lat")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"alter table GFI_SYS_Notification drop column Lon";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "Lon")).Rows.Count == 0)
                        dbExecute(sql);

                    //DriverID
                    sql = @"ALTER TABLE GFI_SYS_Notification ADD DriverID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "DriverID")).Rows.Count == 0)
                    {
                        dbExecute(sql);

                        sql = @"update GFI_SYS_Notification 
	                                set DriverID = d.DriverID
                                from GFI_SYS_Notification n
	                                inner join GFI_FLT_Driver d on d.sDriverName = n.Driver";
                        dbExecute(sql, null, 8000);

                        sql = @"alter table GFI_SYS_Notification drop column Driver";
                        dbExecute(sql);

                        sql = @"ALTER TABLE GFI_SYS_Notification 
                                    ADD CONSTRAINT FK_GFI_SYS_Notification_GFI_FLT_Driver 
                                        FOREIGN KEY(DriverID) 
                                        REFERENCES GFI_FLT_Driver(DriverID) 
                                 ON UPDATE  NO ACTION 
	                             ON DELETE  NO ACTION ";
                        if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_Notification_GFI_FLT_Driver")).Rows.Count == 0)
                            dbExecute(sql);
                    }

                    sql = @"ALTER TABLE GFI_SYS_Notification ADD sBody nvarchar(max) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "sBody")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_Notification ADD Type nvarchar(15) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "Type")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "update GFI_SYS_Notification set Type = 'AutoR' where ExceptionID = 0 and Type is null";
                    dbExecute(sql);

                    sql = "update GFI_SYS_Notification set Type = 'Rule' where ExceptionID > 0 and Type is null";
                    dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_Notification ADD Title nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "Title")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "update GFI_SYS_Notification set UID = null where UID = 0";
                    dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_Notification 
                                    ADD CONSTRAINT FK_GFI_SYS_Notification_GFI_SYS_User 
                                        FOREIGN KEY(UID) 
                                        REFERENCES GFI_SYS_User(UID) 
                                 ON UPDATE  NO ACTION 
	                             ON DELETE  Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_Notification_GFI_SYS_User")).Rows.Count == 0)
                        dbExecute(sql);


                    //GPSDataUID
                    sql = @"UPDATE GFI_SYS_Notification 
	                            SET GPSDataUID = null
                            WHERE NOT EXISTS (
					                            SELECT * 
						                            FROM GFI_GPS_GPSData 
					                            WHERE GFI_SYS_Notification.GPSDataUID = GFI_GPS_GPSData.UID
				                            )";
                    dbExecute(sql, null, 8000);

                    sql = @"ALTER TABLE GFI_SYS_Notification 
                                    ADD CONSTRAINT FK_GFI_SYS_Notification_GFI_GPS_GPSData 
                                        FOREIGN KEY(GPSDataUID) 
                                        REFERENCES GFI_GPS_GPSData(UID) 
                                 ON UPDATE  NO ACTION 
	                             ON DELETE  Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_Notification_GFI_GPS_GPSData")).Rows.Count == 0)
                        dbExecute(sql);

                    //ExceptionID
                    sql = @"UPDATE GFI_SYS_Notification 
	                            SET ExceptionID = null
                            WHERE NOT EXISTS (
					                            SELECT * 
						                            FROM GFI_GPS_Exceptions 
					                            WHERE GFI_SYS_Notification.ExceptionID = GFI_GPS_Exceptions.iID
				                            )";
                    dbExecute(sql, null, 8000);

                    sql = "delete from GFI_SYS_Notification where ExceptionID is null and Type = 'Rule'";
                    dbExecute(sql, null, 8000);

                    sql = @"ALTER TABLE GFI_SYS_Notification 
                                    ADD CONSTRAINT FK_GFI_SYS_Notification_GFI_GPS_Exceptions
                                        FOREIGN KEY(ExceptionID) 
                                        REFERENCES GFI_GPS_Exceptions(iID) 
                                 ON UPDATE  NO ACTION 
	                             ON DELETE  NO ACTION  ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_Notification_GFI_GPS_Exceptions")).Rows.Count == 0)
                        dbExecute(sql, null, 8000);

                    sql = @"alter table GFI_SYS_Notification add VirtualEmail nvarchar(100) null";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "VirtualEmail")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "Notification Table");
                }
                #endregion

                #region Update 128. CreatedBy UpdatedBy int for Assets
                strUpd = "Rez000128";
                if (!CheckUpdateExist(strUpd))
                {
                    #region Assets
                    dt = GetDataDT(GetFieldSchema("GFI_FLT_Asset", "CreatedBy"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                    {
                        sql = @"Alter table GFI_FLT_Asset add cb nvarchar(100)";
                        if (GetDataDT(ExistColumnSql("GFI_FLT_Asset", "cb")).Rows.Count == 0)
                        {
                            dbExecute(sql);
                            sql = @"update GFI_FLT_Asset set cb = CreatedBy
                                    update GFI_FLT_Asset set CreatedBy = null
                                    ALTER TABLE GFI_FLT_Asset ALTER COLUMN CreatedBy int
                                    update GFI_FLT_Asset
	                                    set CreatedBy = u.UID
                                    from GFI_FLT_Asset a
	                                    inner join GFI_SYS_User u on u.Email = a.cb

                                    update GFI_FLT_Asset
	                                    set CreatedBy = u.UID
                                    from GFI_FLT_Asset a
	                                    inner join GFI_SYS_User u on u.UserName = a.cb
                                    where a.CreatedBy is null

                                    alter table GFI_FLT_Asset drop column cb";
                            dbExecute(sql);
                        }
                    }

                    dt = GetDataDT(GetFieldSchema("GFI_FLT_Asset", "UpdatedBy"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                    {
                        sql = @"Alter table GFI_FLT_Asset add ub nvarchar(100)";
                        if (GetDataDT(ExistColumnSql("GFI_FLT_Asset", "ub")).Rows.Count == 0)
                        {
                            dbExecute(sql);
                            sql = @"update GFI_FLT_Asset set ub = UpdatedBy
                                    update GFI_FLT_Asset set UpdatedBy = null
                                    ALTER TABLE GFI_FLT_Asset ALTER COLUMN UpdatedBy int
                                    update GFI_FLT_Asset
	                                    set UpdatedBy = u.UID
                                    from GFI_FLT_Asset a
	                                    inner join GFI_SYS_User u on u.Email = a.ub

                                    update GFI_FLT_Asset
	                                    set UpdatedBy = u.UID
                                    from GFI_FLT_Asset a
	                                    inner join GFI_SYS_User u on u.UserName = a.ub
                                    where a.UpdatedBy is null

                                    alter table GFI_FLT_Asset drop column ub";
                            dbExecute(sql);
                        }
                    }

                    sql = @"ALTER TABLE GFI_FLT_Asset 
                            ADD CONSTRAINT FK_GFI_FLT_Asset_GFI_SYS_User_CB 
                                FOREIGN KEY(CreatedBy) 
                                REFERENCES GFI_SYS_User(UID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_Asset_GFI_SYS_User_CB")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Asset 
                            ADD CONSTRAINT FK_GFI_FLT_Asset_GFI_SYS_User_UB 
                                FOREIGN KEY(UpdatedBy) 
                                REFERENCES GFI_SYS_User(UID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_Asset_GFI_SYS_User_UB")).Rows.Count == 0)
                        dbExecute(sql);
                    #endregion

                    #region AssetDeviceMap
                    dt = GetDataDT(GetFieldSchema("GFI_FLT_AssetDeviceMap", "CreatedBy"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                    {
                        sql = @"Alter table GFI_FLT_AssetDeviceMap add cb nvarchar(100)";
                        if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "cb")).Rows.Count == 0)
                        {
                            dbExecute(sql);
                            sql = @"update GFI_FLT_AssetDeviceMap set cb = CreatedBy
                                    update GFI_FLT_AssetDeviceMap set CreatedBy = null
                                    ALTER TABLE GFI_FLT_AssetDeviceMap ALTER COLUMN CreatedBy int
                                    update GFI_FLT_AssetDeviceMap
	                                    set CreatedBy = u.UID
                                    from GFI_FLT_AssetDeviceMap a
	                                    inner join GFI_SYS_User u on u.Email = a.cb

                                    update GFI_FLT_AssetDeviceMap
	                                    set CreatedBy = u.UID
                                    from GFI_FLT_AssetDeviceMap a
	                                    inner join GFI_SYS_User u on u.UserName = a.cb
                                    where a.CreatedBy is null

                                    alter table GFI_FLT_AssetDeviceMap drop column cb";
                            dbExecute(sql);
                        }
                    }

                    dt = GetDataDT(GetFieldSchema("GFI_FLT_AssetDeviceMap", "UpdatedBy"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                    {
                        sql = @"Alter table GFI_FLT_AssetDeviceMap add ub nvarchar(100)";
                        if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "ub")).Rows.Count == 0)
                        {
                            dbExecute(sql);
                            sql = @"update GFI_FLT_AssetDeviceMap set ub = UpdatedBy
                                    update GFI_FLT_AssetDeviceMap set UpdatedBy = null
                                    ALTER TABLE GFI_FLT_AssetDeviceMap ALTER COLUMN UpdatedBy int
                                    update GFI_FLT_AssetDeviceMap
	                                    set UpdatedBy = u.UID
                                    from GFI_FLT_AssetDeviceMap a
	                                    inner join GFI_SYS_User u on u.Email = a.ub

                                    update GFI_FLT_AssetDeviceMap
	                                    set UpdatedBy = u.UID
                                    from GFI_FLT_AssetDeviceMap a
	                                    inner join GFI_SYS_User u on u.UserName = a.ub
                                    where a.UpdatedBy is null

                                    alter table GFI_FLT_AssetDeviceMap drop column ub";
                            dbExecute(sql);
                        }
                    }

                    sql = @"ALTER TABLE GFI_FLT_AssetDeviceMap 
                            ADD CONSTRAINT FK_GFI_FLT_AssetDeviceMap_GFI_SYS_User_CB 
                                FOREIGN KEY(CreatedBy) 
                                REFERENCES GFI_SYS_User(UID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_AssetDeviceMap_GFI_SYS_User_CB")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_AssetDeviceMap 
                            ADD CONSTRAINT FK_GFI_FLT_AssetDeviceMap_GFI_SYS_User_UB 
                                FOREIGN KEY(UpdatedBy) 
                                REFERENCES GFI_SYS_User(UID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_AssetDeviceMap_GFI_SYS_User_UB")).Rows.Count == 0)
                        dbExecute(sql);
                    #endregion

                    #region all others
                    List<String> lTableN = new List<String>();
                    lTableN.Add("GFI_FLT_AssetTypeMapping");
                    lTableN.Add("GFI_FLT_AssetDataType");
                    lTableN.Add("GFI_FLT_ExtTripInfoType");
                    lTableN.Add("GFI_FLT_ExtTripInfoMapping");
                    lTableN.Add("GFI_FLT_ExtTripInfo");
                    lTableN.Add("GFI_SYS_LookUp");
                    lTableN.Add("GFI_AMM_MaintenanceTrip");
                    lTableN.Add("GFI_FLT_Asset");
                    lTableN.Add("GFI_SYS_Ticker");
                    lTableN.Add("GFI_FLT_AssetDeviceMap");
                    lTableN.Add("GFI_AMM_AssetZoneMap");
                    lTableN.Add("GFI_FLT_AuxilliaryType");
                    //lTableN.Add("GFI_SYS_User");
                    lTableN.Add("GFI_HRM_Team");
                    lTableN.Add("GFI_AMM_ExtZonePropAssessment");
                    //lTableN.Add("TempCostItems");
                    //lTableN.Add("ReportTemplates");
                    lTableN.Add("GFI_PLA_DataSnapshot");
                    lTableN.Add("GFI_AMM_VehicleMaintenance");
                    lTableN.Add("GFI_FLT_Driver");
                    lTableN.Add("GFI_SYS_NaaAddress");
                    lTableN.Add("GFI_HRM_EmpBase");
                    lTableN.Add("GFI_FLT_AutoReportingConfig");
                    //lTableN.Add("Contractors");
                    //lTableN.Add("AspNetUsers");
                    //lTableN.Add("ContractorVehicles");
                    //lTableN.Add("Vehicles");
                    //lTableN.Add("Contracts");
                    lTableN.Add("GFI_SYS_Modules");
                    lTableN.Add("GFI_SYS_AccessTemplates");
                    lTableN.Add("GFI_SYS_AccessProfiles");
                    //lTableN.Add("AspNetAccessTemplates");
                    //lTableN.Add("AspNetAccessTemplateDetails");
                    lTableN.Add("GFI_AMM_VehicleMaintCat");
                    lTableN.Add("GFI_AMM_AssetExtProFields");
                    lTableN.Add("GFI_AMM_InsCoverTypes");
                    lTableN.Add("GFI_AMM_VehicleTypes");
                    lTableN.Add("GFI_AMM_VehicleMaintStatus");
                    lTableN.Add("GFI_AMM_VehicleMaintTypes");
                    lTableN.Add("GFI_AMM_AssetExtProXT");
                    lTableN.Add("GFI_AMM_AssetExtProVehicles");
                    lTableN.Add("GFI_FLT_DeviceAuxilliaryMap");
                    lTableN.Add("GFI_AMM_VehicleMaintTypesLink");
                    //lTableN.Add("TransportRequestContracts");
                    //lTableN.Add("DriverVehicleMappings");
                    //lTableN.Add("LookupTypes");
                    //lTableN.Add("RosterDriverMappingHistory");
                    //lTableN.Add("LookupValues");
                    lTableN.Add("GFI_FLT_CalibrationChart");
                    //lTableN.Add("Officers");
                    //lTableN.Add("ResponsibleOfficers");
                    //lTableN.Add("TransportRequestDetails");
                    //lTableN.Add("TransportRequests");
                    lTableN.Add("GFI_AMM_VehicleMaintItems");
                    //lTableN.Add("RosterDriverMappings");
                    //lTableN.Add("Rosters");

                    if (1 == 0)
                        foreach (String s in lTableN)
                            try
                            {
                                dt = GetDataDT(GetFieldSchema(s, "CreatedBy"));
                                if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                                {
                                    sql = @"Alter table " + s + @" add cb nvarchar(100)";
                                    if (GetDataDT(ExistColumnSql(s, "cb")).Rows.Count == 0)
                                    {
                                        dbExecute(sql);

                                        sql = @"update " + s + @" set cb = CreatedBy
                                                Alter table " + s + @" Alter Column CreatedBy nvarchar(100) null
                                                update " + s + @" set CreatedBy = null
                                                ALTER TABLE " + s + @" ALTER COLUMN CreatedBy int
                                                update " + s + @"
	                                                set CreatedBy = u.UID
                                                from " + s + @" a
	                                                inner join GFI_SYS_User u on u.Email = a.cb

                                                update " + s + @"
	                                                set CreatedBy = u.UID
                                                from " + s + @" a
	                                                inner join GFI_SYS_User u on u.UserName = a.cb
                                                where a.CreatedBy is null

                                                alter table " + s + @" drop column cb";
                                        dbExecute(sql);
                                    }
                                }

                                dt = GetDataDT(GetFieldSchema(s, "UpdatedBy"));
                                if (dt.Rows.Count > 0)
                                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                                    {
                                        sql = @"Alter table " + s + @" add ub nvarchar(100)";
                                        if (GetDataDT(ExistColumnSql(s, "ub")).Rows.Count == 0)
                                        {
                                            dbExecute(sql);

                                            sql = @"update " + s + @" set ub = UpdatedBy
                                                    Alter table " + s + @" Alter Column UpdatedBy nvarchar(100) null
                                                    update " + s + @" set UpdatedBy = null
                                                    ALTER TABLE " + s + @" ALTER COLUMN UpdatedBy int
                                                    update " + s + @"
	                                                    set UpdatedBy = u.UID
                                                    from " + s + @" a
	                                                    inner join GFI_SYS_User u on u.Email = a.ub

                                                    update " + s + @"
	                                                    set UpdatedBy = u.UID
                                                    from " + s + @" a
	                                                    inner join GFI_SYS_User u on u.UserName = a.ub
                                                    where a.UpdatedBy is null

                                                    alter table " + s + @" drop column ub";
                                            dbExecute(sql);
                                        }
                                    }

                                sql = @"ALTER TABLE " + s + @" 
                                        ADD CONSTRAINT FK_" + s + @"_GFI_SYS_User_CB 
                                            FOREIGN KEY(CreatedBy) 
                                            REFERENCES GFI_SYS_User(UID) 
                                     ON UPDATE  NO ACTION 
	                                 ON DELETE  NO ACTION ";
                                if (GetDataDT(ExistsSQLConstraint("FK_" + s + @"_GFI_SYS_User_CB")).Rows.Count == 0)
                                    dbExecute(sql);

                                if (dt.Rows.Count > 0)
                                {
                                    sql = @"ALTER TABLE " + s + @" 
                                        ADD CONSTRAINT FK_" + s + @"_GFI_SYS_User_UB 
                                            FOREIGN KEY(UpdatedBy) 
                                            REFERENCES GFI_SYS_User(UID) 
                                     ON UPDATE  NO ACTION 
	                                 ON DELETE  NO ACTION ";
                                    if (GetDataDT(ExistsSQLConstraint("FK_" + s + @"_GFI_SYS_User_UB")).Rows.Count == 0)
                                        dbExecute(sql);
                                }
                            }
                            catch (Exception ex) { }
                    #endregion

                    InsertDBUpdate(strUpd, "CreatedBy UpdatedBy int for Assets");
                }
                #endregion
                #region Update 129. GFI_SYS_SessionNaveo
                strUpd = "Rez000129";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_SYS_SessionNaveo]
                            (
	                            Id] int] AUTO_INCREMENT NOT NULL,
	                            Uid] int] NOT NULL,
	                            IpAddress] nvarchar](50) NULL,
	                            MachineName] nvarchar](100) NULL,
	                            Type] nvarchar](15) NOT NULL,
	                            InsertedDate] datetime] NOT NULL Default GetUTCDate(),
	                            ExpiryDate] datetime] NULL,
	                            Token] nvarchar](100) NOT NULL,
                             CONSTRAINT PK_GFI_SYS_SessionNaveo] PRIMARY KEY  ([Token] ASC)
                            )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_SessionNaveo")).Rows.Count == 0)
                        dbExecute(sql);

                    GlobalParams g = new GlobalParams();
                    g = g.GetGlobalParamsByName("MultiSession", sConnStr);
                    if (g == null)
                    {
                        g = new GlobalParams();
                        g.ParamName = "MultiSession";
                        g.PValue = "Yes";
                        g.ParamComments = "Allow Multiple login for same user";
                        g.Save(g, true, sConnStr);
                    }
                    InsertDBUpdate(strUpd, "GFI_SYS_SessionNaveo");
                }
                #endregion
                #region Update 130. Archived Audit Tables
                strUpd = "Rez000130";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_ARC_Audit]
                            (
	                            auditId] int] AUTO_INCREMENT NOT NULL,
	                            auditUser] nvarchar](100) NULL,
	                            auditDate] datetime] NULL,
	                            auditType] nvarchar](2) NULL,
	                            ReferenceCode] nvarchar](20) NULL,
	                            ReferenceDesc] nvarchar](50) NULL,
	                            ReferenceTable] nvarchar](20) NULL,
	                            CreatedDate] datetime] NULL,
	                            CallerFunction] nvarchar](20) NULL,
	                            SQLRemarks] nvarchar](200) NULL,
	                            NewValue] nvarchar](200) NULL,
	                            OldValue] nvarchar](200) NULL,
	                            PostedDate] datetime] NULL,
	                            PostedFlag] nvarchar](1) NULL,
	                            UserID] int] NOT NULL DEFAULT ((1))
                            )";
                    if (GetDataDT(ExistTableSql("GFI_ARC_Audit")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_ARC_Audit 
                                ADD CONSTRAINT FK_GFI_ARC_Audit_GFI_SYS_User 
                                    FOREIGN KEY(UserID) 
                                    REFERENCES GFI_SYS_User(UID) 
                                ON UPDATE  NO ACTION 
	                            ON DELETE  NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_ARC_Audit_GFI_SYS_User")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "Archived Audit Tables");
                }
                #endregion
                #region Update 131. User Table fields
                strUpd = "Rez000131";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_SYS_User ALTER COLUMN Tel nvarchar(15) null";
                    dbExecute(sql);
                    sql = "ALTER TABLE GFI_SYS_User ALTER COLUMN MobileNo nvarchar(15) null";
                    dbExecute(sql);
                    sql = "ALTER TABLE GFI_SYS_User ALTER COLUMN UserName nvarchar(100) null";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "User Table fields");
                }
                #endregion
                #region Update 132. CreatedBy UpdatedBy int for User and Assets related
                strUpd = "Rez000132";
                if (!CheckUpdateExist(strUpd))
                {
                    List<String> lTableN = new List<String>();
                    lTableN.Add("GFI_AMM_AssetExtProVehicles");
                    lTableN.Add("GFI_FLT_AssetDeviceMap");
                    lTableN.Add("GFI_FLT_DeviceAuxilliaryMap");
                    lTableN.Add("GFI_FLT_CalibrationChart");

                    foreach (String s in lTableN)
                    {
                        dt = GetDataDT(GetFieldSchema(s, "CreatedBy"));
                        if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                        {
                            sql = @"Alter table " + s + @" add cb nvarchar(100)";
                            if (GetDataDT(ExistColumnSql(s, "cb")).Rows.Count == 0)
                            {
                                dbExecute(sql);

                                sql = @"update " + s + @" set cb = CreatedBy
                                                Alter table " + s + @" Alter Column CreatedBy nvarchar(100) null
                                                update " + s + @" set CreatedBy = null
                                                ALTER TABLE " + s + @" ALTER COLUMN CreatedBy int
                                                update " + s + @"
	                                                set CreatedBy = u.UID
                                                from " + s + @" a
	                                                inner join GFI_SYS_User u on u.Email = a.cb

                                                update " + s + @"
	                                                set CreatedBy = u.UID
                                                from " + s + @" a
	                                                inner join GFI_SYS_User u on u.UserName = a.cb
                                                where a.CreatedBy is null

                                                alter table " + s + @" drop column cb";
                                dbExecute(sql);
                            }
                        }

                        dt = GetDataDT(GetFieldSchema(s, "UpdatedBy"));
                        if (dt.Rows.Count > 0)
                            if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                            {
                                sql = @"Alter table " + s + @" add ub nvarchar(100)";
                                if (GetDataDT(ExistColumnSql(s, "ub")).Rows.Count == 0)
                                {
                                    dbExecute(sql);

                                    sql = @"update " + s + @" set ub = UpdatedBy
                                                    Alter table " + s + @" Alter Column UpdatedBy nvarchar(100) null
                                                    update " + s + @" set UpdatedBy = null
                                                    ALTER TABLE " + s + @" ALTER COLUMN UpdatedBy int
                                                    update " + s + @"
	                                                    set UpdatedBy = u.UID
                                                    from " + s + @" a
	                                                    inner join GFI_SYS_User u on u.Email = a.ub

                                                    update " + s + @"
	                                                    set UpdatedBy = u.UID
                                                    from " + s + @" a
	                                                    inner join GFI_SYS_User u on u.UserName = a.ub
                                                    where a.UpdatedBy is null

                                                    alter table " + s + @" drop column ub";
                                    dbExecute(sql);
                                }
                            }

                        sql = @"ALTER TABLE " + s + @" 
                                        ADD CONSTRAINT FK_" + s + @"_GFI_SYS_User_CB 
                                            FOREIGN KEY(CreatedBy) 
                                            REFERENCES GFI_SYS_User(UID) 
                                     ON UPDATE  NO ACTION 
	                                 ON DELETE  NO ACTION ";
                        if (GetDataDT(ExistsSQLConstraint("FK_" + s + @"_GFI_SYS_User_CB")).Rows.Count == 0)
                            dbExecute(sql);

                        if (dt.Rows.Count > 0)
                        {
                            sql = @"ALTER TABLE " + s + @" 
                                        ADD CONSTRAINT FK_" + s + @"_GFI_SYS_User_UB 
                                            FOREIGN KEY(UpdatedBy) 
                                            REFERENCES GFI_SYS_User(UID) 
                                     ON UPDATE  NO ACTION 
	                                 ON DELETE  NO ACTION ";
                            if (GetDataDT(ExistsSQLConstraint("FK_" + s + @"_GFI_SYS_User_UB")).Rows.Count == 0)
                                dbExecute(sql);
                        }
                    }

                    #region GFI_SYS_User
                    dt = GetDataDT(GetFieldSchema("GFI_SYS_User", "CreatedBy"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                    {
                        sql = @"Alter table GFI_SYS_User add cb nvarchar(100)";
                        if (GetDataDT(ExistColumnSql("GFI_SYS_User", "cb")).Rows.Count == 0)
                        {
                            dbExecute(sql);

                            sql = @"update GFI_SYS_User set cb = CreatedBy
                                    Alter table GFI_SYS_User Alter Column CreatedBy nvarchar(100) null
                                    update GFI_SYS_User set CreatedBy = null
                                    ALTER TABLE GFI_SYS_User ALTER COLUMN CreatedBy int
                                    update GFI_SYS_User
	                                    set CreatedBy = u.UID
                                    from GFI_SYS_User a
	                                    inner join (select * from GFI_SYS_User) u on u.Email = a.cb

                                    update GFI_SYS_User
	                                    set CreatedBy = u.UID
                                    from GFI_SYS_User a
	                                    inner join (select * from GFI_SYS_User) u on u.UserName = a.cb
                                    where a.CreatedBy is null

                                    alter table GFI_SYS_User drop column cb";
                            dbExecute(sql);
                        }
                    }

                    dt = GetDataDT(GetFieldSchema("GFI_SYS_User", "UpdatedBy"));
                    if (dt.Rows.Count > 0)
                        if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                        {
                            sql = @"Alter table GFI_SYS_User add ub nvarchar(100)";
                            if (GetDataDT(ExistColumnSql("GFI_SYS_User", "ub")).Rows.Count == 0)
                            {
                                dbExecute(sql);

                                sql = @"update GFI_SYS_User set ub = UpdatedBy
                                        Alter table GFI_SYS_User Alter Column UpdatedBy nvarchar(100) null
                                        update GFI_SYS_User set UpdatedBy = null
                                        ALTER TABLE GFI_SYS_User ALTER COLUMN UpdatedBy int
                                        update GFI_SYS_User
	                                        set UpdatedBy = u.UID
                                        from GFI_SYS_User a
	                                        inner join (select * from GFI_SYS_User) u on u.Email = a.ub

                                        update GFI_SYS_User
	                                        set UpdatedBy = u.UID
                                        from GFI_SYS_User a
	                                        inner join (select * from GFI_SYS_User) u on u.UserName = a.ub
                                        where a.UpdatedBy is null

                                        alter table GFI_SYS_User drop column ub";
                                dbExecute(sql);
                            }
                        }

                    sql = @"ALTER TABLE GFI_SYS_User 
                                        ADD CONSTRAINT FK_GFI_SYS_User_GFI_SYS_User_CB 
                                            FOREIGN KEY(CreatedBy) 
                                            REFERENCES GFI_SYS_User(UID) 
                                     ON UPDATE  NO ACTION 
	                                 ON DELETE  NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_User_GFI_SYS_User_CB")).Rows.Count == 0)
                        dbExecute(sql);

                    if (dt.Rows.Count > 0)
                    {
                        sql = @"ALTER TABLE GFI_SYS_User 
                                        ADD CONSTRAINT FK_GFI_SYS_User_GFI_SYS_User_UB 
                                            FOREIGN KEY(UpdatedBy) 
                                            REFERENCES GFI_SYS_User(UID) 
                                     ON UPDATE  NO ACTION 
	                                 ON DELETE  NO ACTION ";
                        if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_User_GFI_SYS_User_UB")).Rows.Count == 0)
                            dbExecute(sql);
                    }

                    #endregion
                    InsertDBUpdate(strUpd, "CreatedBy UpdatedBy int for User and Assets related");
                }
                #endregion
                #region Update 133. Releasing Web Version
                strUpd = "Rez000133";
                if (!CheckUpdateExist(strUpd))
                {
                    User u = new User();
                    u = u.GetUserByeMail("ADMIN@naveo.mu", "SysInitPass", sConnStr, false, true);
                    u.Password = "WebCore@2016";
                    u.Update(u, sConnStr);

                    u = new User();
                    u = u.GetUserByeMail("Support@naveo.mu", "SysInitPass", sConnStr, false, true);
                    u.Password = "SupportMe@2016";
                    u.Update(u, sConnStr);

                    InsertDBUpdate(strUpd, "Releasing Web Version");
                }
                #endregion
                #region Update 134. GFI_SYS_LoginAudit User
                strUpd = "Rez000134";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_SYS_LoginAudit ALTER COLUMN auditUser nvarchar(50) null";
                    dbExecute(sql);

                    sql = "ALTER TABLE GFI_SYS_Audit ALTER COLUMN auditUser nvarchar(50) null";
                    dbExecute(sql);

                    sql = "ALTER TABLE GFI_SYS_AuditAccess] ALTER COLUMN auditUser] nvarchar(50);";
                    dbExecute(sql);

                    sql = "alter table GFI_FLT_AutoReportingConfig alter column CreatedBy nvarchar(100)";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "Audit User");
                }
                #endregion
                #region Update 135. Notification Title 400
                strUpd = "Rez000135";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_SYS_Notification ALTER COLUMN Title nvarchar(400) NULL";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "Notification Title 400");
                }
                #endregion

                #region Update 136. UOM ADC
                strUpd = "Rez000136";
                if (!CheckUpdateExist(strUpd))
                {
                    UOM u = new UOM();
                    u = u.GetUOMByDescription("ADC", sConnStr);
                    if (u == null)
                    {
                        u = new UOM();
                        u.Description = "ADC";
                        u.Save(u, true, sConnStr);
                    }

                    u = new UOM();
                    u = u.GetUOMByDescription("ADC2", sConnStr);
                    if (u == null)
                    {
                        u = new UOM();
                        u.Description = "ADC2";
                        u.Save(u, true, sConnStr);
                    }

                    sql = "CREATE  INDEX IX_Arc_GFI_SYS_Notif_ExceptionID ON GFI_SYS_Notification] ([ExceptionID])";
                    if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "IX_Arc_GFI_SYS_Notif_ExceptionID")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "UOM ADC n IX_Arc_GFI_SYS_Notif_ExceptionID");
                }
                #endregion
                #region Update 137. Admin Button in template
                strUpd = "Rez000137";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "update GFI_SYS_Modules set command = 'cmdSysAdmin' where ModuleName = 'Settings --> System Administration' and Command = ''";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "Admin Button in template");
                }
                #endregion

                #region Update 138. New GroupMatrixes
                strUpd = "Rez000138";
                if (!CheckUpdateExist(strUpd))
                {
                    //1 row per option. e.g DriverRosterMapping, Contractors
                    sql = @"CREATE TABLE GFI_SYS_GroupMatrixCompany]
                            (
	                              MID] INT AUTO_INCREMENT NOT NULL
	                            , GMID] INT NOT NULL
	                            , OptionsEnabled] nvarchar(25) NOT NULL
                                , CONSTRAINT PK_GFI_SYS_GroupMatrixCompany] PRIMARY KEY  ([MID] ASC)
                             )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrixCompany")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixCompany
                            ADD CONSTRAINT FK_GFI_SYS_GroupMatrixCompany_GFI_SYS_GroupMatrix 
                                FOREIGN KEY (GMID) 
                                REFERENCES GFI_SYS_GroupMatrix (GMID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixCompany_GFI_SYS_GroupMatrix")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "New GroupMatrixes");
                }
                #endregion
                #region Update 139. Driver as Resources
                strUpd = "Rez000139";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Department nvarchar](50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Department")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Email nvarchar](50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Email")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD sLastName nvarchar](50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "sLastName")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD OfficerLevel nvarchar](50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "OfficerLevel")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD OfficerTitle nvarchar](50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "OfficerTitle")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Phone nvarchar](50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Phone")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Title nvarchar](50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Title")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD ASPUser nvarchar](50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "ASPUser")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD UserId int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "UserId")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Photo varBinary] (1024) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Photo")).Rows.Count == 0)
                        dbExecute(sql);

                    dt = GetDataDT(GetFieldSchema("GFI_FLT_Driver", "Photo"));
                    if (dt.Rows[0]["CHARACTER_MAXIMUM_LENGTH"].ToString() == "1024")
                        dbExecute("ALTER TABLE GFI_FLT_Driver Alter Column Photo varBinary] (MAX) NULL");

                    sql = @"ALTER TABLE GFI_FLT_Driver 
                            ADD CONSTRAINT FK_GFI_SYS_User_GFI_FLT_Driver 
                                FOREIGN KEY(UserId) 
                                REFERENCES GFI_SYS_User(UID) 
                         ON UPDATE NO ACTION 
	                     ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_User_GFI_FLT_Driver")).Rows.Count == 0)
                        dbExecute(sql);

                    dt = GetDataDT(GetFieldSchema("GFI_FLT_Driver", "ZoneID"));
                    sql = "Alter table GFI_FLT_Driver alter column ZoneID int null";
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver 
                            ADD CONSTRAINT FK_GFI_FLT_Driver_GFI_FLT_ZoneHeader 
                                FOREIGN KEY(ZoneID) 
                                REFERENCES GFI_FLT_ZoneHeader(ZoneID) 
                         ON UPDATE NO ACTION 
	                     ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_Driver_GFI_FLT_ZoneHeader")).Rows.Count == 0)
                        dbExecute(sql);

                    #region CreatedBy UpdatedBy int for Driver
                    List<String> lTableN = new List<String>();
                    lTableN.Add("GFI_FLT_Driver");
                    foreach (String s in lTableN)
                    {
                        dt = GetDataDT(GetFieldSchema(s, "CreatedBy"));
                        if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                        {
                            sql = @"Alter table " + s + @" add cb nvarchar(100)";
                            if (GetDataDT(ExistColumnSql(s, "cb")).Rows.Count == 0)
                            {
                                dbExecute(sql);

                                sql = @"update " + s + @" set cb = CreatedBy
                                                Alter table " + s + @" Alter Column CreatedBy nvarchar(100) null
                                                update " + s + @" set CreatedBy = null
                                                ALTER TABLE " + s + @" ALTER COLUMN CreatedBy int
                                                update " + s + @"
	                                                set CreatedBy = u.UID
                                                from " + s + @" a
	                                                inner join GFI_SYS_User u on u.Email = a.cb

                                                update " + s + @"
	                                                set CreatedBy = u.UID
                                                from " + s + @" a
	                                                inner join GFI_SYS_User u on u.UserName = a.cb
                                                where a.CreatedBy is null

                                                alter table " + s + @" drop column cb";
                                dbExecute(sql);
                            }
                        }

                        dt = GetDataDT(GetFieldSchema(s, "UpdatedBy"));
                        if (dt.Rows.Count > 0)
                            if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                            {
                                sql = @"Alter table " + s + @" add ub nvarchar(100)";
                                if (GetDataDT(ExistColumnSql(s, "ub")).Rows.Count == 0)
                                {
                                    dbExecute(sql);

                                    sql = @"update " + s + @" set ub = UpdatedBy
                                                    Alter table " + s + @" Alter Column UpdatedBy nvarchar(100) null
                                                    update " + s + @" set UpdatedBy = null
                                                    ALTER TABLE " + s + @" ALTER COLUMN UpdatedBy int
                                                    update " + s + @"
	                                                    set UpdatedBy = u.UID
                                                    from " + s + @" a
	                                                    inner join GFI_SYS_User u on u.Email = a.ub

                                                    update " + s + @"
	                                                    set UpdatedBy = u.UID
                                                    from " + s + @" a
	                                                    inner join GFI_SYS_User u on u.UserName = a.ub
                                                    where a.UpdatedBy is null

                                                    alter table " + s + @" drop column ub";
                                    dbExecute(sql);
                                }
                            }

                        sql = @"ALTER TABLE " + s + @" 
                                        ADD CONSTRAINT FK_" + s + @"_GFI_SYS_User_CB 
                                            FOREIGN KEY(CreatedBy) 
                                            REFERENCES GFI_SYS_User(UID) 
                                     ON UPDATE  NO ACTION 
	                                 ON DELETE  NO ACTION ";
                        if (GetDataDT(ExistsSQLConstraint("FK_" + s + @"_GFI_SYS_User_CB")).Rows.Count == 0)
                            dbExecute(sql);

                        if (dt.Rows.Count > 0)
                        {
                            sql = @"ALTER TABLE " + s + @" 
                                        ADD CONSTRAINT FK_" + s + @"_GFI_SYS_User_UB 
                                            FOREIGN KEY(UpdatedBy) 
                                            REFERENCES GFI_SYS_User(UID) 
                                     ON UPDATE  NO ACTION 
	                                 ON DELETE  NO ACTION ";
                            if (GetDataDT(ExistsSQLConstraint("FK_" + s + @"_GFI_SYS_User_UB")).Rows.Count == 0)
                                dbExecute(sql);
                        }
                    }
                    #endregion

                    //Migrating Officers to Drivers
                    if (GetDataDT(ExistTableSql("Officers")).Rows.Count == 0)
                        if (GetDataDT(ExistColumnSql("Officers", "User")).Rows.Count == 0)
                        {
                            sql = @"insert GFI_FLT_Driver (sDriverName, sFirtName, sLastName, DateOfBirth, Address1, Phone, MobileNumber, CreatedDate, UpdatedDate, Email, Title, Department, OfficerLevel, OfficerTitle, ASPUser, EmployeeType )
	                            select FirstName, FirstName, LastName, DOB, Address, t1.Phone, Mobile, t1.CreatedDate, t1.UpdatedDate, t1.Email, t1.Title, t1.Department, t1.OfficerLevel, t1.OfficerTitle, user], 'Officer' from Officers t1
		                            LEFT JOIN GFI_FLT_Driver t2 ON t2.sFirtName = t1.FirstName
	                            WHERE t2.sFirtName IS NULL";
                            dbExecute(sql);

                            sql = @"update GFI_FLT_Driver
	                            set UpdatedBy = t3.UID
                            from GFI_FLT_Driver t1
                            inner join Officers t2 on t2.FirstName = t1.sFirtName
                            inner join AspNetUsers t3 on t3.Id = t2.UpdatedBy";
                            dbExecute(sql);

                            sql = @"update GFI_FLT_Driver
	                            set CreatedBy = t3.UID
                            from GFI_FLT_Driver t1
                            inner join Officers t2 on t2.FirstName = t1.sFirtName
                            inner join AspNetUsers t3 on t3.Id = t2.CreatedBy";
                            dbExecute(sql);
                        }
                    InsertDBUpdate(strUpd, "Driver as Resources");
                }
                #endregion
                #region Update 140. GFI_SYS_LookUp Types n Values
                strUpd = "Rez000140";
                if (!CheckUpdateExist(strUpd))
                {
                    #region Type
                    sql = @"CREATE TABLE GFI_SYS_LookUpTypes] 
                            (
	                            TID] INT AUTO_INCREMENT NOT NULL,
	                            Code] NVARCHAR(25) NOT NULL,
	                            Description] NVARCHAR (200) NULL,
	                            CreatedDate] DateTime NULL,
	                            UpdatedDate] DateTime NULL,
	                            CreatedBy] int NULL,
	                            UpdatedBy] int NULL
                            	CONSTRAINT PK_GFI_SYS_LookUpTypes] PRIMARY KEY  ([TID] ASC)
                            );";
                    if (GetDataDT(ExistTableSql("GFI_SYS_LookUpTypes")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_LookUpTypes 
                            ADD CONSTRAINT FK_GFI_SYS_LookUpTypes_GFI_SYS_User_C
                                FOREIGN KEY(CreatedBy) 
                                REFERENCES GFI_SYS_User(UID) 
                         ON UPDATE NO ACTION 
	                     ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_LookUpTypes_GFI_SYS_User_C")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_LookUpTypes 
                            ADD CONSTRAINT FK_GFI_SYS_LookUpTypes_GFI_SYS_User_U
                                FOREIGN KEY(UpdatedBy) 
                                REFERENCES GFI_SYS_User(UID) 
                         ON UPDATE NO ACTION 
	                     ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_LookUpTypes_GFI_SYS_User_U")).Rows.Count == 0)
                        dbExecute(sql);
                    #endregion

                    #region Values
                    sql = @"CREATE TABLE GFI_SYS_LookUpValues] 
                            (
	                            VID] INT AUTO_INCREMENT NOT NULL,
	                            TID] INT NOT NULL,
	                            Name] NVARCHAR(250) NOT NULL,
	                            Description] NVARCHAR (250) NULL,
	                            Value] int NULL,
	                            CreatedDate] DateTime NULL,
	                            UpdatedDate] DateTime NULL,
	                            CreatedBy] int NULL,
	                            UpdatedBy] int NULL,
                                RootMatrixID] int null
                            	CONSTRAINT PK_GFI_SYS_LookUpValues] PRIMARY KEY  ([VID] ASC)
                            );";
                    if (GetDataDT(ExistTableSql("GFI_SYS_LookUpValues")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_LookUpValues 
                            ADD CONSTRAINT FK_GFI_SYS_LookUpValues_GFI_SYS_LookUpTypes 
                                FOREIGN KEY(TID) 
                                REFERENCES GFI_SYS_LookUpTypes(TID) 
                         ON UPDATE NO ACTION 
	                     ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_LookUpValues_GFI_SYS_LookUpTypes")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_LookUpValues 
                            ADD CONSTRAINT FK_GFI_SYS_LookUpValues_GFI_SYS_User_C
                                FOREIGN KEY(CreatedBy) 
                                REFERENCES GFI_SYS_User(UID) 
                         ON UPDATE NO ACTION 
	                     ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_LookUpValues_GFI_SYS_User_C")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_LookUpValues 
                            ADD CONSTRAINT FK_GFI_SYS_LookUpValues_GFI_SYS_User_U
                                FOREIGN KEY(UpdatedBy) 
                                REFERENCES GFI_SYS_User(UID) 
                         ON UPDATE NO ACTION 
	                     ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_LookUpValues_GFI_SYS_User_U")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_LookUpValues 
                            ADD CONSTRAINT FK_GFI_SYS_LookUpValues_GFI_SYS_GroupMatrix 
                                FOREIGN KEY (RootMatrixID) 
                                REFERENCES GFI_SYS_GroupMatrix (GMID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_LookUpValues_GFI_SYS_GroupMatrix")).Rows.Count == 0)
                        dbExecute(sql);
                    #endregion

                    #region Type Data
                    if (GetDataDT(ExistTableSql("LookupTypes")).Rows.Count == 0)
                    {
                        sql = @"insert GFI_SYS_LookUpTypes (Code, Description, CreatedDate)
	                            select t1.Code, t1.Description, t1.CreatedDate from LookupTypes t1
		                            LEFT JOIN GFI_SYS_LookUpTypes t2 ON t2.Code = t1.Code
	                            WHERE t2.Code IS NULL";
                        dbExecute(sql);

                        sql = @"update GFI_SYS_LookUpTypes
		                        set UpdatedBy = t3.UID
	                        from GFI_SYS_LookUpTypes t1
	                        inner join LookupTypes t2 on t2.Code = t1.Code
	                        inner join AspNetUsers t3 on t3.Id = t2.UpdatedBy";
                        dbExecute(sql);

                        sql = @"update GFI_SYS_LookUpTypes
		                        set CreatedBy = t3.UID
	                        from GFI_SYS_LookUpTypes t1
	                        inner join LookupTypes t2 on t2.Code = t1.Code
	                        inner join AspNetUsers t3 on t3.Id = t2.CreatedBy";
                        dbExecute(sql);
                    }

                    LookUpTypes lt = new LookUpTypes();
                    lt = lt.GetLookUpTypesByCode("CONTRACTOR_TYPE", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "CONTRACTOR_TYPE";
                        lt.Description = "CONTRACTOR_TYPE";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lt.Save(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lt.GetLookUpTypesByCode("FILLING_TYPE", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "FILLING_TYPE";
                        lt.Description = "FILLING_TYPE";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lt.Save(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lt.GetLookUpTypesByCode("CITY", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "CITY";
                        lt.Description = "CITY";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lt.Save(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lt.GetLookUpTypesByCode("FUEL_TYPE", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "FUEL_TYPE";
                        lt.Description = "FUEL_TYPE";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lt.Save(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lt.GetLookUpTypesByCode("MAKE_TYPE", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "MAKE_TYPE";
                        lt.Description = "MAKE_TYPE";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lt.Save(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lt.GetLookUpTypesByCode("VEHICLE_TYPE", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "VEHICLE_TYPE";
                        lt.Description = "VEHICLE_TYPE";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lt.Save(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lt.GetLookUpTypesByCode("REQUEST_TYPE", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "REQUEST_TYPE";
                        lt.Description = "REQUEST_TYPE";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lt.Save(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lt.GetLookUpTypesByCode("DISTRICT", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "DISTRICT";
                        lt.Description = "DISTRICT";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lt.Save(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lt.GetLookUpTypesByCode("ACCIDENTSTATUS", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "ACCIDENTSTATUS";
                        lt.Description = "ACCIDENTSTATUS";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lt.Save(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lt.GetLookUpTypesByCode("OFFICERTITLE", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "OFFICERTITLE";
                        lt.Description = "OFFICERTITLE";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lt.Save(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lt.GetLookUpTypesByCode("DEPARTMENT", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "DEPARTMENT";
                        lt.Description = "DEPARTMENT";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lt.Save(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lt.GetLookUpTypesByCode("ENGINE_TYPE", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "ENGINE_TYPE";
                        lt.Description = "ENGINE_TYPE";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lt.Save(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lt.GetLookUpTypesByCode("COMPANY_NAMES", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "COMPANY_NAMES";
                        lt.Description = "COMPANY_NAMES";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lt.Save(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lt.GetLookUpTypesByCode("MyType", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "MyType";
                        lt.Description = "MyType";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lt.Save(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lt.GetLookUpTypesByCode("Institution", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "Institution";
                        lt.Description = "Institution";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lt.Save(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lt.GetLookUpTypesByCode("Purpose", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "Purpose";
                        lt.Description = "Purpose";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lt.Save(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lt.GetLookUpTypesByCode("Color", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "Color";
                        lt.Description = "Color";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lt.Save(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lt.GetLookUpTypesByCode("DriverType", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "DriverType";
                        lt.Description = "Driver Type";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lt.Save(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lt.GetLookUpTypesByCode("GradeType", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "GradeType";
                        lt.Description = "Grade Type";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lt.Save(lt, true, sConnStr);
                    }

                    #endregion

                    #region Values Data
                    if (GetDataDT(ExistTableSql("LookupValues")).Rows.Count == 0)
                    {
                        sql = @"insert GFI_SYS_LookUpValues (TID, Name, Description, CreatedDate)
	                            select t3.TID, t1.Name, t1.Description, t1.CreatedDate from LookupValues t1
		                            inner join LookupTypes t0 on t0.id = t1.LookupTypeId
		                            inner join GFI_SYS_LookUpTypes t3 on t0.Code = t3.Code
		                            LEFT JOIN GFI_SYS_LookUpValues t2 ON t2.Name = t1.Name
	                            WHERE t2.Name IS NULL";
                        dbExecute(sql);

                        sql = @"update GFI_SYS_LookUpValues
		                        set UpdatedBy = t3.UID
	                        from GFI_SYS_LookUpValues t1
	                        inner join LookupValues t2 on t2.Name = t1.Name
	                        inner join AspNetUsers t3 on t3.Id = t2.UpdatedBy";
                        dbExecute(sql);

                        sql = @"update GFI_SYS_LookUpValues
		                        set CreatedBy = t3.UID
	                        from GFI_SYS_LookUpValues t1
	                        inner join LookupValues t2 on t2.Name = t1.Name
	                        inner join AspNetUsers t3 on t3.Id = t2.CreatedBy";
                        dbExecute(sql);
                    }

                    LookUpTypes lts = new LookUpTypes();
                    LookUpValues lv = new LookUpValues();

                    lv = lv.GetLookUpValuesByName("COMPANY", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "COMPANY";
                        lv.Description = "COMPANY";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lts.GetLookUpTypesByCode("CONTRACTOR_TYPE", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lv.Save(lv, true, sConnStr);
                        }
                    }

                    lv = lv.GetLookUpValuesByName("Engen Beau Bassin", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "Engen Beau Bassin";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lts.GetLookUpTypesByCode("FILLING_TYPE", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lv.Save(lv, true, sConnStr);
                        }
                    }


                    lv = lv.GetLookUpValuesByName("Beau Bassin", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "Beau Bassin";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lts.GetLookUpTypesByCode("CITY", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lv.Save(lv, true, sConnStr);
                        }
                    }

                    lv = lv.GetLookUpValuesByName("DIESEL", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "DIESEL";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lts.GetLookUpTypesByCode("FUEL_TYPE", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lv.Save(lv, true, sConnStr);
                        }
                    }

                    lv = lv.GetLookUpValuesByName("AUDI", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "AUDI";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lts.GetLookUpTypesByCode("MAKE_TYPE", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lv.Save(lv, true, sConnStr);
                        }
                    }

                    lv = lv.GetLookUpValuesByName("CAR", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "CAR";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lts.GetLookUpTypesByCode("VEHICLE_TYPE", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lv.Save(lv, true, sConnStr);
                        }
                    }

                    lv = lv.GetLookUpValuesByName("ROUTINE", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "ROUTINE";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lts.GetLookUpTypesByCode("REQUEST_TYPE", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lv.Save(lv, true, sConnStr);
                        }
                    }

                    lv = lv.GetLookUpValuesByName("Moka", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "Moka";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lts.GetLookUpTypesByCode("DISTRICT", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lv.Save(lv, true, sConnStr);
                        }
                    }

                    lv = lv.GetLookUpValuesByName("CASE CLOSED", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "CASE CLOSED";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lts.GetLookUpTypesByCode("ACCIDENTSTATUS", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lv.Save(lv, true, sConnStr);
                        }
                    }

                    lv = lv.GetLookUpValuesByName("SECRETARY", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "SECRETARY";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lts.GetLookUpTypesByCode("OFFICERTITLE", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lv.Save(lv, true, sConnStr);
                        }
                    }

                    lv = lv.GetLookUpValuesByName("REGISTRY", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "REGISTRY";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lts.GetLookUpTypesByCode("DEPARTMENT", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lv.Save(lv, true, sConnStr);
                        }
                    }

                    lv = lv.GetLookUpValuesByName("MANUAL", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "MANUAL";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lts.GetLookUpTypesByCode("ENGINE_TYPE", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lv.Save(lv, true, sConnStr);
                        }
                    }

                    lv = lv.GetLookUpValuesByName("Naveo", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "Naveo";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lts.GetLookUpTypesByCode("COMPANY_NAMES", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lv.Save(lv, true, sConnStr);
                        }
                    }

                    lv = lv.GetLookUpValuesByName("Naveo Type", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "Naveo Type";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lts.GetLookUpTypesByCode("MyType", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lv.Save(lv, true, sConnStr);
                        }
                    }

                    lv = lv.GetLookUpValuesByName("Naveo Institution", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "Naveo Institution";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lts.GetLookUpTypesByCode("Institution", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lv.Save(lv, true, sConnStr);
                        }
                    }

                    lv = lv.GetLookUpValuesByName("Naveo Purpose", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "Naveo Purpose";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lts.GetLookUpTypesByCode("Purpose", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lv.Save(lv, true, sConnStr);
                        }
                    }

                    lv = lv.GetLookUpValuesByName("Red", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "Red";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lts.GetLookUpTypesByCode("Color", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lv.Save(lv, true, sConnStr);
                        }
                    }

                    lv = lv.GetLookUpValuesByName("Driver Van", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "Driver Van";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lts.GetLookUpTypesByCode("DriverType", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lv.Save(lv, true, sConnStr);
                        }
                    }

                    lv = lv.GetLookUpValuesByName("Driver Grade", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "Driver Grade";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lts.GetLookUpTypesByCode("GradeType", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lv.Save(lv, true, sConnStr);
                        }
                    }
                    #endregion

                    InsertDBUpdate(strUpd, "GFI_SYS_LookUp Types n Values");
                }
                #endregion
                #region Update 141. Planning
                strUpd = "Rez000141";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_PLN_Planning] 
                            (
	                            PID] INT AUTO_INCREMENT NOT NULL,
                                Approval] int null,
	                            Remarks] NVARCHAR (500) NULL,
	                            CreatedDate] DateTime NULL,
	                            UpdatedDate] DateTime NULL,
	                            CreatedBy] int NULL,
	                            UpdatedBy] int NULL
                            	CONSTRAINT PK_GFI_PLN_Planning] PRIMARY KEY  ([PID] ASC)
                            );";
                    if (GetDataDT(ExistTableSql("GFI_PLN_Planning")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_PLN_Planning 
                            ADD CONSTRAINT FK_GFI_PLN_Planning_GFI_SYS_User_C
                                FOREIGN KEY(CreatedBy) 
                                REFERENCES GFI_SYS_User(UID) 
                         ON UPDATE NO ACTION 
	                     ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_Planning_GFI_SYS_User_C")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_PLN_Planning 
                            ADD CONSTRAINT FK_GFI_PLN_Planning_GFI_SYS_User_U
                                FOREIGN KEY(UpdatedBy) 
                                REFERENCES GFI_SYS_User(UID) 
                         ON UPDATE NO ACTION 
	                     ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_Planning_GFI_SYS_User_U")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_PLN_Planning ADD Urgent int not NULL default 0";
                    if (GetDataDT(ExistColumnSql("GFI_PLN_Planning", "Urgent")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_PLN_Planning ADD RequestCode nvarchar](14) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_PLN_Planning", "RequestCode")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_PLN_Planning] ADD  CONSTRAINT UK_GFI_PLN_Planning_RequestCode] Unique ([RequestCode] ASC)";
                    if (GetDataDT(ExistsSQLConstraint("UK_GFI_PLN_Planning_RequestCode")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_PLN_Planning ADD ParentRequestCode nvarchar](14) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_PLN_Planning", "ParentRequestCode")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_PLN_Planning ADD Type int NULL";    //1 Fetch, 2 Convey
                    if (GetDataDT(ExistColumnSql("GFI_PLN_Planning", "Type")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_PLN_Planning ADD sComments nvarchar(250) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_PLN_Planning", "sComments")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_PLN_Planning ADD ClosureComments nvarchar(250) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_PLN_Planning", "ClosureComments")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_PLN_Planning ADD Ref nvarchar(250) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_PLN_Planning", "Ref")).Rows.Count == 0)
                        dbExecute(sql);


                    #region MOHRequestCode
                    sql = @"CREATE TABLE GFI_PLN_MOHRequestCode] 
                            (
	                            Code] nvarchar(3) NOT NULL,
                                iYear] int not null,
	                            iSeed] int not NULL
                            	CONSTRAINT PK_GFI_PLN_MOHRequestCode] PRIMARY KEY  ([Code], iYear] ASC)
                            );";
                    if (GetDataDT(ExistTableSql("GFI_PLN_MOHRequestCode")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[sp_MOHRequestCode]') AND type in (N'P', N'PC'))
                            DROP PROCEDURE sp_MOHRequestCode]";
                    dbExecute(sql);

                    sql = @"
                            CREATE PROCEDURE sp_MOHRequestCode] @Code nvarchar(3)
                            as Begin
                        
	                            DECLARE @iYear int
	                            DECLARE @sYear nvarchar(2)
	                            DECLARE @iSeed int
	                            DECLARE @sSeed nvarchar(5)
	                            DECLARE @ReqNo nvarchar(14)
	                            DECLARE @idd int
	                            DECLARE @sdd nvarchar(2)
	                            DECLARE @imm int
	                            DECLARE @smm nvarchar(2)
    
	                            set @iYear = 0

	                            ;with cta as
	                            (
		                            select * from GFI_PLN_MOHRequestCode] where Code] = @Code and iYear = DATEPART(yyyy, GETDATE()) 
	                            )
	                            select @iYear = iYear, @iSeed = iSeed from cta;

	                            if (@iYear = 0)
	                            begin
		                            insert GFI_PLN_MOHRequestCode] values (@Code, DATEPART(yyyy, GETDATE()), 0); 

		                            ;with cta as
		                            (
			                            select * from GFI_PLN_MOHRequestCode] where Code] = @Code and iYear = DATEPART(yyyy, GETDATE()) 
		                            )
		                            select @iYear = iYear, @iSeed = iSeed from cta;
	                            end
	
	                            update GFI_PLN_MOHRequestCode] set iSeed = iSeed + 1 where Code] = @Code and iYear = DATEPART(yyyy, GETDATE()) 
	
	                            ;with cta as
	                            (
		                            select * from GFI_PLN_MOHRequestCode] where Code] = @Code and iYear = DATEPART(yyyy, GETDATE()) 
	                            )
	                            select @iYear = iYear, @iSeed = iSeed from cta;

	                            --yy
	                            if(@iYear > 2000)
		                            set @iYear = @iYear - 2000
	                            set @sYear = CAST(@iYear AS nvarchar(2))

	                            --mm
	                            set @imm = DATEPART(mm,GETDATE());
	                            if(@imm < 10)
		                            set @smm = '0' + CAST(@imm AS nvarchar(2))
	                            else
		                            set @smm = CAST(@imm AS nvarchar(2))

	                            --dd
	                            set @idd = DATEPART(dd,GETDATE());
	                            if(@idd < 10)
		                            set @sdd = '0' + CAST(@idd AS nvarchar(2))
	                            else
		                            set @sdd = CAST(@idd AS nvarchar(2))

	                            --Seed
	                            if(@iSeed < 10)	        set @sSeed = '0000' + CAST(@iSeed AS nvarchar(5))
	                            else if(@iSeed < 100)	set @sSeed = '000' + CAST(@iSeed AS nvarchar(5))
	                            else if(@iSeed < 1000)	set @sSeed = '00' + CAST(@iSeed AS nvarchar(5))
	                            else if(@iSeed < 10000)	set @sSeed = '0' + CAST(@iSeed AS nvarchar(5))
	                            else                    set @sSeed = CAST(@iSeed AS nvarchar(5))

	                            set @ReqNo = @Code + @sdd + @smm + @sYear + @sSeed
	                            select @ReqNo
                            end";
                    dbExecute(sql);
                    #endregion

                    #region Matrix
                    sql = @"CREATE TABLE GFI_SYS_GroupMatrixPlanning]
                            (
	                              MID] INT AUTO_INCREMENT NOT NULL
	                            , GMID] INT NOT NULL
	                            , iID] INT NOT NULL
                                , CONSTRAINT PK_GFI_SYS_GroupMatrixPlanning] PRIMARY KEY  ([MID] ASC)
                             )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrixPlanning")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixPlanning 
                            ADD CONSTRAINT FK_GFI_SYS_GroupMatrixPlanning_GFI_SYS_GroupMatrix 
                                FOREIGN KEY (GMID) 
                                REFERENCES GFI_SYS_GroupMatrix (GMID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixPlanning_GFI_SYS_GroupMatrix")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixPlanning 
                            ADD CONSTRAINT FK_GFI_SYS_GroupMatrixPlanning_GFI_PLN_Planning 
                                FOREIGN KEY(iID) 
                                REFERENCES GFI_PLN_Planning(PID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixPlanning_GFI_PLN_Planning")).Rows.Count == 0)
                        dbExecute(sql);
                    #endregion

                    #region GFI_PLN_PlanningLkup. Institution, Purpose
                    sql = @"CREATE TABLE GFI_PLN_PlanningLkup] 
                            (
	                            iID] INT AUTO_INCREMENT NOT NULL,
                                PID] int not null,
                                LookupValueID] int not null,    
                                LookupType] nvarchar(25) null   
                            	CONSTRAINT PK_GFI_GFI_PLN_PlanningLkup] PRIMARY KEY  ([iID] ASC)
                            );";
                    if (GetDataDT(ExistTableSql("GFI_PLN_PlanningLkup")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_PLN_PlanningLkup 
                            ADD CONSTRAINT FK_GFI_PLN_PlanningLkup_GFI_PLN_Planning 
                                FOREIGN KEY (PID) 
                                REFERENCES GFI_PLN_Planning (PID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_PlanningLkup_GFI_PLN_Planning")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_PLN_PlanningLkup 
                            ADD CONSTRAINT FK_GFI_PLN_PlanningLkup_GFI_SYS_LookUpValues 
                                FOREIGN KEY (LookupValueID) 
                                REFERENCES GFI_SYS_LookUpValues (VID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_PlanningLkup_GFI_SYS_LookUpValues")).Rows.Count == 0)
                        dbExecute(sql);
                    #endregion

                    #region GFI_PLN_PlanningResource. RequestOfficer, SentForApproval, Passenger
                    sql = @"CREATE TABLE GFI_PLN_PlanningResource] 
                            (
	                            iID] INT AUTO_INCREMENT NOT NULL,
                                PID] int not null,
                                DriverID] int not null,
                                ResourceType] nvarchar(25) null    --RequestOfficer, SentForApproval, Passenger
                            	CONSTRAINT PK_GFI_PLN_PlanningResource] PRIMARY KEY  ([iID] ASC)
                            );";
                    if (GetDataDT(ExistTableSql("GFI_PLN_PlanningResource")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_PLN_PlanningResource 
                            ADD CONSTRAINT FK_GFI_PLN_PlanningResource_GFI_PLN_Planning 
                                FOREIGN KEY (PID) 
                                REFERENCES GFI_PLN_Planning (PID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_PlanningResource_GFI_PLN_Planning")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_PLN_PlanningResource 
                            ADD CONSTRAINT FK_GFI_PLN_PlanningResource_GFI_FLT_Driver 
                                FOREIGN KEY (DriverID) 
                                REFERENCES GFI_FLT_Driver (DriverID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_PlanningResource_GFI_FLT_Driver")).Rows.Count == 0)
                        dbExecute(sql);

                    #endregion

                    #region GFI_PLN_PlanningViaPointH
                    sql = @"CREATE TABLE GFI_PLN_PlanningViaPointH] 
                            (
	                            iID] INT AUTO_INCREMENT NOT NULL,
                                PID] int not null,
                                SeqNo] int not null,    
                                ZoneID] int null,
                                Lat] float null,
                                Lon] float null,
                                LocalityVCA] int null,
                                Buffer] int null,
                                Remarks] nvarchar(250) null,
                                dtUTC] DateTime null
                            	CONSTRAINT PK_GFI_PLN_PlanningViaPointH] PRIMARY KEY  ([iID] ASC)
                            );";
                    if (GetDataDT(ExistTableSql("GFI_PLN_PlanningViaPointH")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_PLN_PlanningViaPointH 
                            ADD CONSTRAINT FK_GFI_PLN_PlanningViaPointH_GFI_PLN_Planning 
                                FOREIGN KEY (PID) 
                                REFERENCES GFI_PLN_Planning (PID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_PlanningViaPointH_GFI_PLN_Planning")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_PLN_PlanningViaPointH 
                            ADD CONSTRAINT FK_GFI_PLN_PlanningViaPointH_GFI_FLT_ZoneHeader 
                                FOREIGN KEY (ZoneID) 
                                REFERENCES GFI_FLT_ZoneHeader (ZoneID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_PlanningViaPointH_GFI_FLT_ZoneHeader")).Rows.Count == 0)
                        dbExecute(sql);
                    #endregion

                    #region GFI_PLN_PlanningViaPointD
                    sql = @"CREATE TABLE GFI_PLN_PlanningViaPointD] 
                            (
	                            iID] INT AUTO_INCREMENT NOT NULL,
                                HID] int not null,
                                UOM] int null,
                                Capacity] int null,    
                                Remarks] nvarchar(250) null
                            	CONSTRAINT PK_GFI_PLN_PlanningViaPointD] PRIMARY KEY  ([iID] ASC)
                            );";
                    if (GetDataDT(ExistTableSql("GFI_PLN_PlanningViaPointD")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_PLN_PlanningViaPointD 
                            ADD CONSTRAINT FK_GFI_PLN_PlanningViaPointD_GFI_PLN_PlanningViaPointH
                                FOREIGN KEY (HID) 
                                REFERENCES GFI_PLN_PlanningViaPointH (iID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_PlanningViaPointD_GFI_PLN_PlanningViaPointH")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_PLN_PlanningViaPointD 
                            ADD CONSTRAINT FK_GFI_PLN_PlanningViaPointD_GFI_SYS_UOM
                                FOREIGN KEY (UOM) 
                                REFERENCES GFI_SYS_UOM (UID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_PlanningViaPointD_GFI_SYS_UOM")).Rows.Count == 0)
                        dbExecute(sql);
                    #endregion

                    #region GFI_SYS_CustomerMaster
                    sql = @"CREATE TABLE GFI_SYS_CustomerMaster] 
                            (
	                            ClientID] INT AUTO_INCREMENT NOT NULL,
                                Surname] nvarchar(50) null,
                                Name] nvarchar(50) null,
                                Tel1] nvarchar(15) null,
                                Tel2] nvarchar(15) null,
                                Add1] nvarchar(50) null,
                                Add2] nvarchar(50) null,
                                Remarks] nvarchar(250) null
                            	CONSTRAINT PK_GFI_SYS_CustomerMaster] PRIMARY KEY  ([ClientID] ASC)
                            );";
                    if (GetDataDT(ExistTableSql("GFI_SYS_CustomerMaster")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_CustomerMaster ADD HomeNo int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_CustomerMaster", "HomeNo")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_CustomerMaster ADD District nvarchar(25)  NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_CustomerMaster", "District")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_CustomerMaster ADD PostalCode nvarchar(15)  NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_CustomerMaster", "PostalCode")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_CustomerMaster ADD MobileNumber nvarchar(15)  NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_CustomerMaster", "MobileNumber")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_CustomerMaster ADD NIC nvarchar(15)  NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_CustomerMaster", "NIC")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_PLN_PlanningViaPointH ADD ClientID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_PLN_PlanningViaPointH", "ClientID")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_PLN_PlanningViaPointH 
                            ADD CONSTRAINT FK_GFI_PLN_PlanningViaPointH_GFI_SYS_CustomerMaster
                                FOREIGN KEY (ClientID) 
                                REFERENCES GFI_SYS_CustomerMaster (ClientID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_PlanningViaPointH_GFI_SYS_CustomerMaster")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_CustomerMaster ADD ZoneID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_CustomerMaster", "ZoneID")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_CustomerMaster 
                            ADD CONSTRAINT FK_GFI_SYS_CustomerMaster_GFI_FLT_ZoneHeader 
                                FOREIGN KEY(ZoneID) 
                                REFERENCES GFI_FLT_ZoneHeader(ZoneID) 
                         ON UPDATE NO ACTION 
	                     ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_CustomerMaster_GFI_FLT_ZoneHeader")).Rows.Count == 0)
                        dbExecute(sql);
                    #endregion

                    InsertDBUpdate(strUpd, "Planning");
                }
                #endregion
                #region Update 142. Scheduled
                strUpd = "Rez000142";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_PLN_Scheduled] 
                            (
	                            HID] INT AUTO_INCREMENT NOT NULL,
                                AssetSelectionID] int null,
	                            DriverSelectionID] int null,
	                            Status] int null,
	                            Remarks] nvarchar(50) null,
	                            intField] int null,
	                            CreatedDate] DateTime NULL,
	                            UpdatedDate] DateTime NULL,
	                            CreatedBy] int NULL,
	                            UpdatedBy] int NULL
                            	CONSTRAINT PK_GFI_PLN_Scheduled] PRIMARY KEY  ([HID] ASC)
                            );";
                    if (GetDataDT(ExistTableSql("GFI_PLN_Scheduled")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_PLN_Scheduled 
                            ADD CONSTRAINT FK_GFI_PLN_Scheduled_GFI_SYS_User_C
                                FOREIGN KEY(CreatedBy) 
                                REFERENCES GFI_SYS_User(UID) 
                         ON UPDATE NO ACTION 
	                     ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_Scheduled_GFI_SYS_User_C")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_PLN_Scheduled 
                            ADD CONSTRAINT FK_GFI_PLN_Scheduled_GFI_SYS_User_U
                                FOREIGN KEY(UpdatedBy) 
                                REFERENCES GFI_SYS_User(UID) 
                         ON UPDATE NO ACTION 
	                     ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_Scheduled_GFI_SYS_User_U")).Rows.Count == 0)
                        dbExecute(sql);


                    #region Matrix
                    sql = @"CREATE TABLE GFI_SYS_GroupMatrixScheduled]
                            (
	                              MID] INT AUTO_INCREMENT NOT NULL
	                            , GMID] INT NOT NULL
	                            , iID] INT NOT NULL
                                , CONSTRAINT PK_GFI_SYS_GroupMatrixScheduled] PRIMARY KEY  ([MID] ASC)
                             )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrixScheduled")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixScheduled 
                            ADD CONSTRAINT FK_GFI_SYS_GroupMatrixScheduled_GFI_SYS_GroupMatrix 
                                FOREIGN KEY (GMID) 
                                REFERENCES GFI_SYS_GroupMatrix (GMID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixScheduled_GFI_SYS_GroupMatrix")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixScheduled 
                            ADD CONSTRAINT FK_GFI_SYS_GroupMatrixScheduled_GFI_PLN_Scheduled
                                FOREIGN KEY(iID) 
                                REFERENCES GFI_PLN_Scheduled(HID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixScheduled_GFI_PLN_Scheduled")).Rows.Count == 0)
                        dbExecute(sql);
                    #endregion

                    #region GFI_PLN_ScheduledPlan Details1
                    sql = @"CREATE TABLE GFI_PLN_ScheduledPlan] 
                            (
	                            iID] INT AUTO_INCREMENT NOT NULL,
                                HID] int not null,
                                PID] int not null,
                                SeqNo] int null,
                            	CONSTRAINT PK_GFI_PLN_ScheduledPlan] PRIMARY KEY  ([iID] ASC)
                            );";
                    if (GetDataDT(ExistTableSql("GFI_PLN_ScheduledPlan")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_PLN_ScheduledPlan 
                            ADD CONSTRAINT FK_GFI_PLN_ScheduledPlan_GFI_PLN_Scheduled 
                                FOREIGN KEY (HID) 
                                REFERENCES GFI_PLN_Scheduled (HID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_ScheduledPlan_GFI_PLN_Scheduled")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_PLN_ScheduledPlan 
                            ADD CONSTRAINT FK_GFI_PLN_ScheduledPlan_GFI_PLN_Planning 
                                FOREIGN KEY (PID) 
                                REFERENCES GFI_PLN_Planning (PID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_ScheduledPlan_GFI_PLN_Planning")).Rows.Count == 0)
                        dbExecute(sql);
                    #endregion

                    #region GFI_PLN_ScheduledAsset Details2
                    sql = @"CREATE TABLE GFI_PLN_ScheduledAsset] 
                            (
	                            iID] INT AUTO_INCREMENT NOT NULL,
                                HID] int not null,
                                AssetID] int null,    
                                DriverID] int null   
                            	CONSTRAINT PK_GFI_PLN_ScheduledAsset] PRIMARY KEY  ([iID] ASC)
                            );";
                    if (GetDataDT(ExistTableSql("GFI_PLN_ScheduledAsset")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_PLN_ScheduledAsset 
                            ADD CONSTRAINT FK_GFI_PLN_ScheduledAsset_GFI_PLN_Scheduled 
                                FOREIGN KEY (HID) 
                                REFERENCES GFI_PLN_Scheduled (HID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_ScheduledAsset_GFI_PLN_Scheduled")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_PLN_ScheduledAsset 
                            ADD CONSTRAINT FK_GFI_GFI_PLN_ScheduledAsset_GFI_FLT_Asset 
                                FOREIGN KEY (AssetID) 
                                REFERENCES GFI_FLT_Asset (AssetID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GFI_PLN_ScheduledAsset_GFI_FLT_Asset")).Rows.Count == 0)
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_PLN_ScheduledAsset 
                            ADD CONSTRAINT FK_GFI_PLN_ScheduledAsset_GFI_FLT_Driver 
                                FOREIGN KEY (DriverID) 
                                REFERENCES GFI_FLT_Driver (DriverID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_ScheduledAsset_GFI_FLT_Driver")).Rows.Count == 0)
                        dbExecute(sql);
                    #endregion

                    InsertDBUpdate(strUpd, "Scheduled");
                }
                #endregion
                #region Update 143. Driver Lookup
                strUpd = "Rez000143";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Type_Institution int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Type_Institution")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver 
                            ADD CONSTRAINT FK_GFI_FLT_Driver_GFI_SYS_LookUpValues_Inst 
                                FOREIGN KEY(Type_Institution) 
                                REFERENCES GFI_SYS_LookUpValues (VID) 
                         ON UPDATE NO ACTION 
	                     ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_Driver_GFI_SYS_LookUpValues_Inst")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Type_Driver int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Type_Driver")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver 
                            ADD CONSTRAINT FK_GFI_FLT_Driver_GFI_SYS_LookUpValues_Driver 
                                FOREIGN KEY(Type_Driver) 
                                REFERENCES GFI_SYS_LookUpValues (VID) 
                         ON UPDATE NO ACTION 
	                     ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_Driver_GFI_SYS_LookUpValues_Driver")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Type_Grade int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Type_Grade")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver 
                            ADD CONSTRAINT FK_GFI_FLT_Driver_GFI_SYS_LookUpValues_Grade 
                                FOREIGN KEY(Type_Grade) 
                                REFERENCES GFI_SYS_LookUpValues (VID) 
                         ON UPDATE NO ACTION 
	                     ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_Driver_GFI_SYS_LookUpValues_Grade")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD HomeNo int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "HomeNo")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_FLT_ResponsibleDriver] 
                            (
	                            iID] INT AUTO_INCREMENT NOT NULL,
                                DriverID] int not null,
	                            ResponsibleDriverID] int not null
                            	CONSTRAINT PK_GFI_FLT_ResponsibleDriver] PRIMARY KEY  ([iID] ASC)
                            );";
                    if (GetDataDT(ExistTableSql("GFI_FLT_ResponsibleDriver")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_ResponsibleDriver 
                            ADD CONSTRAINT FK_GFI_FLT_ResponsibleDriver_GFI_FLT_Driver_Driver 
                                FOREIGN KEY(DriverID) 
                                REFERENCES GFI_FLT_Driver (DriverID) 
                         ON UPDATE NO ACTION 
	                     ON DELETE cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ResponsibleDriver_GFI_FLT_Driver_Driver")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_ResponsibleDriver 
                            ADD CONSTRAINT FK_GFI_FLT_ResponsibleDriver_GFI_FLT_Driver_Responsible 
                                FOREIGN KEY(ResponsibleDriverID) 
                                REFERENCES GFI_FLT_Driver (DriverID) 
                         ON UPDATE NO ACTION 
	                     ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ResponsibleDriver_GFI_FLT_Driver_Responsible")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "Driver Lookup");
                }
                #endregion
                #region Update 144. Asset updates
                strUpd = "Rez000144";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_FLT_Asset ADD VehicleTypeId int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Asset", "VehicleTypeId")).Rows.Count == 0)
                        dbExecute(sql);

                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_Asset_GFI_AMM_VehicleTypes")).Rows.Count == 0)
                    {
                        sql = @"UPDATE GFI_FLT_Asset 
                                    SET VehicleTypeId = T2.VehicleTypeId_cbo
                                FROM        GFI_FLT_Asset T1
                                INNER JOIN  GFI_AMM_AssetExtProVehicles T2 ON T2.AssetId = T1.AssetID";
                        dbExecute(sql);

                        sql = @"ALTER TABLE GFI_FLT_Asset 
                            ADD CONSTRAINT FK_GFI_FLT_Asset_GFI_AMM_VehicleTypes 
                                FOREIGN KEY(VehicleTypeId) 
                                REFERENCES GFI_AMM_VehicleTypes (VehicleTypeId) 
                         ON UPDATE NO ACTION 
	                     ON DELETE NO ACTION ";
                        dbExecute(sql);
                    }

                    sql = "ALTER TABLE GFI_AMM_AssetExtProVehicles] DROP FOREIGN KEY FK_GFI_AMM_Asset_ExtPro_Vehicles_GFI_AMM_VehicleTypes";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_Asset_ExtPro_Vehicles_GFI_AMM_VehicleTypes")).Rows.Count > 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Asset ADD ColorID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Asset", "ColorID")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_Asset 
                            ADD CONSTRAINT FK_GFI_FLT_Asset_GFI_SYS_LookUpValues_Color 
                                FOREIGN KEY(ColorID) 
                                REFERENCES GFI_SYS_LookUpValues (VID) 
                         ON UPDATE NO ACTION 
	                     ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_Asset_GFI_SYS_LookUpValues_Color")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_FLT_AssetUOM] 
                            (
	                            iID] INT AUTO_INCREMENT NOT NULL,
                                AssetID] int not null,
	                            UOM] int not null,
                                Capacity] int null
                            	CONSTRAINT PK_GFI_FLT_AssetUOM] PRIMARY KEY  ([iID] ASC)
                            );";
                    if (GetDataDT(ExistTableSql("GFI_FLT_AssetUOM")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_AssetUOM 
                            ADD CONSTRAINT FK_GFI_FLT_AssetUOM_GFI_FLT_Asset
                                FOREIGN KEY(AssetID) 
                                REFERENCES GFI_FLT_Asset(AssetID) 
                         ON UPDATE NO ACTION 
	                     ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_AssetUOM_GFI_FLT_Asset")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_AssetUOM 
                            ADD CONSTRAINT FK_GFI_FLT_AssetUOM_GFI_SYS_UOM
                                FOREIGN KEY(UOM) 
                                REFERENCES GFI_SYS_UOM(UID) 
                         ON UPDATE NO ACTION 
	                     ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_AssetUOM_GFI_SYS_UOM")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "Asset updates");
                }
                #endregion
                #region Update 145. Distance SQL functions
                strUpd = "Rez000145";
                if (!CheckUpdateExist(strUpd))
                {
                    #region fnCalcDistanceInKM
                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[fnCalcDistanceInKM]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
                            DROP FUNCTION fnCalcDistanceInKM]";
                    dbExecute(sql);

                    sql = @"CREATE FUNCTION fnCalcDistanceInKM(@lat1 FLOAT, @lat2 FLOAT, @lon1 FLOAT, @lon2 FLOAT)
                            RETURNS FLOAT 
                            AS
                            BEGIN
                                RETURN ACOS(SIN(PI()*@lat1/180.0)*SIN(PI()*@lat2/180.0)+COS(PI()*@lat1/180.0)*COS(PI()*@lat2/180.0)*COS(PI()*@lon2/180.0-PI()*@lon1/180.0))*6371
                            END";
                    dbExecute(sql);
                    #endregion

                    #region spGPSDataDistanceFromXY
                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[spGPSDataDistanceFromXY]') AND type in (N'P', N'PC'))
                            DROP PROCEDURE spGPSDataDistanceFromXY]";
                    dbExecute(sql);

                    sql = @"
                            -- Stored Procedure
                            --    Author     : Reza
                            --    Create date: 15/03/2017
                            --    Description: Return calculated distance from debug data. 
                            --                 If optional parameter @dist is null, return all data, else only data <= @dist    
                            Create PROCEDURE spGPSDataDistanceFromXY
	                            @dtFrom Datetime 
	                            , @dtTo Datetime 
	                            , @AssetID int
	                            , @lat1 FLOAT
                                , @lon1 FLOAT
                                , @dist int = null
                            as Begin
	                            SELECT *, fnCalcDistanceInKM(@lat1, Latitude, @lon1, Longitude) * 1000 DistanceInM into #dtDist from GFI_GPS_GPSData
		                            where DateTimeGPS_UTC >= @dtFrom and DateTimeGPS_UTC <= @dtTo
			                            and AssetID = @AssetID

                                if @dist is null
                                    select * from #dtDist order by AssetID, DateTimeGPS_UTC 
                                else  
                                    select * from #dtDist where DistanceInM <= @dist order by AssetID, DateTimeGPS_UTC                      
                            end";
                    dbExecute(sql);
                    #endregion

                    #region spGPSDataInZone
                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[spGPSDataInZone]') AND type in (N'P', N'PC'))
                            DROP PROCEDURE spGPSDataInZone]";
                    dbExecute(sql);

                    sql = @"
                            -- Stored Procedure
                            --    Author     : Reza
                            --    Create date: 15/03/2017
                            --    Description: Return debug data with additional field isPointInZone. 
                            --                 If optional parameter @ShowAllData is null, return all data, else only data in zone    
                            Create PROCEDURE spGPSDataInZone
	                            @dtFrom Datetime 
	                            , @dtTo Datetime 
	                            , @AssetID int
	                            , @ZoneID int
                                , @ShowAllData int = null
                            as Begin
	                            SELECT *, isPointInZone(Longitude, Latitude, @ZoneID) isPointInZone into #dtDist from GFI_GPS_GPSData
		                            where DateTimeGPS_UTC >= @dtFrom and DateTimeGPS_UTC <= @dtTo
			                            and AssetID = @AssetID

                                if @ShowAllData is null
                                    select * from #dtDist order by AssetID, DateTimeGPS_UTC 
                                else  
                                    select * from #dtDist where isPointInZone = 1 order by AssetID, DateTimeGPS_UTC                      
                            end";
                    dbExecute(sql);
                    #endregion

                    InsertDBUpdate(strUpd, "Distance SQL functions");
                }
                #endregion
                #region Update 146. VCA
                strUpd = "Rez000146";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_PLN_PlanningViaPointH DROP FOREIGN KEY FK_GFI_PLN_PlanningViaPointH_GFI_FLT_VCA";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_PlanningViaPointH_GFI_FLT_VCA")).Rows.Count > 0)
                        dbExecute(sql);

                    sql = @"drop TABLE GFI_FLT_VCA]";
                    if (GetDataDT(ExistTableSql("GFI_FLT_VCA")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_FLT_VCAHeader](
	                            VCAID] int] AUTO_INCREMENT NOT NULL,
	                            Name] nvarchar](255) NULL,
	                            Comments] nvarchar](500) NULL,
	                            Color] int] DEFAULT 1 NOT NULL,
                            CONSTRAINT PK_GFI_FLT_VCAHeader] PRIMARY KEY  
                                ([VCAID] ASC)
                         ) ";
                    if (GetDataDT(ExistTableSql("GFI_FLT_VCAHeader")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_FLT_VCADetail](
	                        iID] int] AUTO_INCREMENT NOT NULL,
	                        Latitude] float] NULL,
	                        Longitude] float] NULL,
	                        VCAID] int] NULL,
	                        CordOrder] int null,
                            CONSTRAINT PK_GFI_FLT_VCADetail] PRIMARY KEY  
                            ([iID] ASC)
                        )";
                    if (GetDataDT(ExistTableSql("GFI_FLT_VCADetail")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_VCADetail] WITH CHECK 
                            ADD CONSTRAINT FK_GFI_FLT_VCADetail_GFI_FLT_VCAHeader] 
                                FOREIGN KEY([VCAID])
                                REFERENCES GFI_FLT_VCAHeader] (VCAID)
                            ON DELETE CASCADE";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_VCADetail_GFI_FLT_VCAHeader")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_PLN_PlanningViaPointH 
                            ADD CONSTRAINT FK_GFI_PLN_PlanningViaPointH_GFI_FLT_VCAHeader 
                                FOREIGN KEY (LocalityVCA) 
                                REFERENCES GFI_FLT_VCAHeader (VCAID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_PlanningViaPointH_GFI_FLT_VCAHeader")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[isPointInVCA]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
                            DROP FUNCTION isPointInVCA]";

                    dbExecute(sql);
                    sql = @"CREATE function isPointInVCA] (@Lon float, @Lat float, @iVCAID int)
                        returns int
                        as Begin
	                        Declare @iResult int
	                        set @iResult = 0

	                        DECLARE	@Longitude float] 
	                        DECLARE	@Latitude float]
	                        DECLARE	@nLongitude float] 
	                        DECLARE	@nLatitude float]
	                        DECLARE	@myLongitude float] 
	                        DECLARE	@myLatitude float]
	                        set	@myLongitude = @Lon
	                        set	@myLatitude = @Lat

	                        declare @c bit
	                        set @c = 0
	                        declare xyCursor cursor  LOCAL SCROLL STATIC FOR  
			                        select d.Latitude, d.Longitude from GFI_FLT_VCAHeader h, GFI_FLT_VCADetail d where h.VCAID = @iVCAID and h.VCAID = d.VCAID order by CordOrder

	                        open xyCursor
		                        fetch next from xyCursor into @Latitude, @Longitude
		                        WHILE @@FETCH_STATUS = 0   
		                        BEGIN 
		                        fetch PRIOR from xyCursor into @Latitude, @Longitude
		                        fetch next from xyCursor into @nLatitude, @nLongitude

					                        if ((((@Latitude <= @myLatitude) and (@myLatitude < @nLatitude))
							                        or ((@nLatitude <= @myLatitude) and (@myLatitude < @Latitude)))
							                        and (@myLongitude < (@nLongitude - @Longitude) * (@myLatitude - @Latitude)
								                        / (@nLatitude - @Latitude) + @Longitude))

						                        if(@c = 0)
							                        set @c = 1
						                        else
							                        set @c = 0
			                        fetch next from xyCursor into @Latitude, @Longitude
		                        END
	                        close xyCursor
	                        deallocate xyCursor

	                        if(@c = 0)
		                        set @iResult = 0
	                        else
		                        set @iResult = 1		

	                        return @iResult
                        end";
                    dbExecute(sql);


                    sql = @"ALTER TABLE GFI_SYS_CustomerMaster ADD VCAID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_CustomerMaster", "VCAID")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_CustomerMaster 
                            ADD CONSTRAINT FK_GFI_SYS_CustomerMaster_GFI_FLT_VCAHeader 
                                FOREIGN KEY (VCAID) 
                                REFERENCES GFI_FLT_VCAHeader (VCAID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_CustomerMaster_GFI_FLT_VCAHeader")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "VCA");
                }
                #endregion
                #region Update 147. Missing indexes during notif service
                strUpd = "Rez000147";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "CREATE INDEX IX_GFI_GPS_Exceptions_Notif] ON GFI_GPS_Exceptions ([RuleID],[AssetID], DateTimeGPS_UTC]) INCLUDE ([GPSDataID], Severity], GroupID], InsertedAt])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_Exceptions", "IX_GFI_GPS_Exceptions_Notif")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"DROP INDEX IX_GFI_SYS_Notification_UID_Status_ReportID] ON GFI_SYS_Notification]";
                    if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "IX_GFI_SYS_Notification_UID_Status_ReportID")).Rows.Count == 1)
                        dbExecute(sql);

                    sql = "CREATE INDEX IX_GFI_SYS_Notification_UID_Status_ReportID] ON GFI_SYS_Notification] ([UID], Status],[ReportID]) INCLUDE ([ExceptionID], DateTimeGPS_UTC], SendAt])";
                    if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "IX_GFI_SYS_Notification_UID_Status_ReportID")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_SYS_Notification_GPSData_UID] ON GFI_SYS_Notification] ([GPSDataUID]) INCLUDE ([NID])";
                    if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "IX_GFI_SYS_Notification_GPSData_UID")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "Missing indexes during notif service");
                }
                #endregion
                #region Update 148. User Additional fields
                strUpd = "Rez000148";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_SYS_User ADD ZoneID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "ZoneID")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User 
                            ADD CONSTRAINT FK_GFI_SYS_User_GFI_FLT_ZoneHeader 
                                FOREIGN KEY(ZoneID) 
                                REFERENCES GFI_FLT_ZoneHeader(ZoneID) 
                         ON UPDATE NO ACTION 
	                     ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_User_GFI_FLT_ZoneHeader")).Rows.Count == 0)
                        dbExecute(sql);


                    sql = @"ALTER TABLE GFI_SYS_User ADD PlnLkpVal int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "PlnLkpVal")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User 
                            ADD CONSTRAINT FK_GFI_SYS_User_GFI_SYS_LookUpValues_PLN 
                                FOREIGN KEY(PlnLkpVal) 
                                REFERENCES GFI_SYS_LookUpValues (VID) 
                         ON UPDATE NO ACTION 
	                     ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_User_GFI_SYS_LookUpValues_PLN")).Rows.Count == 0)
                        dbExecute(sql);


                    InsertDBUpdate(strUpd, "User Additional fields");
                }
                #endregion
                #region Update 149. UOM MOH
                strUpd = "Rez000149";
                if (!CheckUpdateExist(strUpd))
                {
                    UOM u = new UOM();
                    u = u.GetUOMByDescription("Sitting", sConnStr);
                    if (u == null)
                    {
                        u = new UOM();
                        u.Description = "Sitting";
                        u.Save(u, true, sConnStr);
                    }

                    u = new UOM();
                    u = u.GetUOMByDescription("Lying", sConnStr);
                    if (u == null)
                    {
                        u = new UOM();
                        u.Description = "Lying";
                        u.Save(u, true, sConnStr);
                    }

                    VehicleTypes objVehicleTypes = new VehicleTypes();
                    objVehicleTypes = objVehicleTypes.GetVehicleTypesByDescription("Ambulance", sConnStr);
                    if (objVehicleTypes == null)
                    {
                        objVehicleTypes = new VehicleTypes();
                        objVehicleTypes.VehicleTypeId = 7;
                        objVehicleTypes.Description = "Ambulance";
                        objVehicleTypes.CreatedDate = DateTime.Now;
                        objVehicleTypes.CreatedBy = usrInst.Username;
                        objVehicleTypes.UpdatedDate = DateTime.Now;
                        objVehicleTypes.UpdatedBy = usrInst.Username;
                        objVehicleTypes.Save(objVehicleTypes, sConnStr);
                    }

                    InsertDBUpdate(strUpd, "UOM MOH");
                }
                #endregion

                #region Update 150. MOH Roster
                strUpd = "Rez000150";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_RST_Shift](
	                            SID] int] AUTO_INCREMENT NOT NULL,
	                            sName] nvarchar](255) NULL,
	                            sComments] nvarchar](500) NULL,
	                            TimeFr] Datetime] NOT NULL,
	                            TimeTo] Datetime] NOT NULL,
                            CONSTRAINT PK_GFI_RST_Shift] PRIMARY KEY  
                                ([SID] ASC)
                         ) ";
                    if (GetDataDT(ExistTableSql("GFI_RST_Shift")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_RST_Teams](
	                            TID] int] AUTO_INCREMENT NOT NULL,
	                            sName] nvarchar](255) NULL,
	                            sComments] nvarchar](500) NULL,
	                            GUID] nvarchar] (255) NULL,
                            CONSTRAINT PK_GFI_RST_Teams] PRIMARY KEY  
                                ([TID] ASC)
                         ) ";
                    if (GetDataDT(ExistTableSql("GFI_RST_Teams")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_RST_TeamsResource](
	                            iID] int] AUTO_INCREMENT NOT NULL,
	                            TID] int] not NULL,
	                            DriverID] int not NULL,
                            CONSTRAINT PK_GFI_RST_TeamsResource] PRIMARY KEY  
                                ([iID] ASC)
                         ) ";
                    if (GetDataDT(ExistTableSql("GFI_RST_TeamsResource")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_RST_TeamsAsset](
	                            iID] int] AUTO_INCREMENT NOT NULL,
	                            TID] int] not NULL,
	                            AssetID] int not NULL,
                            CONSTRAINT PK_GFI_RST_TeamsAsset] PRIMARY KEY  
                                ([iID] ASC)
                         ) ";
                    if (GetDataDT(ExistTableSql("GFI_RST_TeamsAsset")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_RST_Roster](
	                            RID] int] AUTO_INCREMENT NOT NULL,
	                            GRID] int] not NULL,
	                            SID] int] not NULL,
                                TeamID] int] null,
	                            rDate] Datetime] NOT NULL,
                                isLocked] int,
                                isOriginal] int,
                            CONSTRAINT PK_GFI_RST_Roster] PRIMARY KEY  
                                ([RID] ASC)
                         ) ";
                    if (GetDataDT(ExistTableSql("GFI_RST_Roster")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_RST_Roster ADD DriverID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_RST_Roster", "DriverID")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_RST_Roster ADD AssetID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_RST_Roster", "AssetID")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_RST_Roster alter column TeamID int NULL";
                    dbExecute(sql);

                    sql = @"ALTER TABLE GFI_RST_TeamsResource 
                            ADD CONSTRAINT FK_GFI_RST_TeamsResource_GFI_FLT_Driver 
                                FOREIGN KEY (DriverID) 
                                REFERENCES GFI_FLT_Driver (DriverID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_RST_TeamsResource_GFI_FLT_Driver")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_RST_TeamsResource 
                            ADD CONSTRAINT FK_GFI_RST_TeamsResource_GFI_RST_Teams
                                FOREIGN KEY (TID) 
                                REFERENCES GFI_RST_Teams(TID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_RST_TeamsResource_GFI_RST_Teams")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_RST_TeamsAsset 
                            ADD CONSTRAINT FK_GFI_RST_TeamsAsset_GFI_FLT_Asset 
                                FOREIGN KEY (AssetID) 
                                REFERENCES GFI_FLT_Asset (AssetID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_RST_TeamsAsset_GFI_FLT_Asset")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_RST_TeamsAsset 
                            ADD CONSTRAINT FK_GFI_RST_TeamsAsset_GFI_RST_Teams 
                                FOREIGN KEY (TID) 
                                REFERENCES GFI_RST_Teams (TID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_RST_TeamsAsset_GFI_RST_Teams")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_RST_Roster 
                            ADD CONSTRAINT FK_GFI_RST_Roster_GFI_RST_Teams 
                                FOREIGN KEY (TeamID) 
                                REFERENCES GFI_RST_Teams (TID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_RST_Roster_GFI_RST_Teams")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_RST_Roster 
                            ADD CONSTRAINT FK_GFI_RST_Roster_GFI_FLT_Driver 
                                FOREIGN KEY (DriverID) 
                                REFERENCES GFI_FLT_Driver (DriverID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_RST_Roster_GFI_FLT_Driver")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_RST_Roster 
                            ADD CONSTRAINT FK_GFI_RST_Roster_GFI_FLT_Asset 
                                FOREIGN KEY (AssetID) 
                                REFERENCES GFI_FLT_Asset (AssetID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_RST_Roster_GFI_FLT_Asset")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_RST_Roster 
                            ADD CONSTRAINT FK_GFI_RST_Roster_GFI_RST_Shift 
                                FOREIGN KEY (SID) 
                                REFERENCES GFI_RST_Shift (SID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_RST_Roster_GFI_RST_Shift")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_RST_Roster Add DynTeamID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_RST_Roster", "DynTeamID")).Rows.Count == 0)
                        dbExecute(sql);

                    //GFI_RST_RosterHeader
                    sql = @"CREATE TABLE GFI_RST_RosterHeader](
	                            iID] int] AUTO_INCREMENT NOT NULL,
	                            dtFrom] datetime NULL,
	                            dtTo] datetime NULL,
	                            sRemarks] nvarchar] (255) NULL,
                            CONSTRAINT PK_GFI_RST_RosterHeader] PRIMARY KEY  ([iID] ASC)
                         ) ";
                    if (GetDataDT(ExistTableSql("GFI_RST_RosterHeader")).Rows.Count == 0)
                        dbExecute(sql);

                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_RST_Roster_GFI_RST_RosterHeader")).Rows.Count == 0)
                    {
                        sql = "delete from GFI_RST_Roster";
                        dbExecute(sql);

                        sql = @"ALTER TABLE GFI_RST_Roster 
                            ADD CONSTRAINT FK_GFI_RST_Roster_GFI_RST_RosterHeader 
                                FOREIGN KEY (GRID) 
                                REFERENCES GFI_RST_RosterHeader (iID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE CASCADE ";
                        dbExecute(sql);
                    }

                    sql = "Alter table GFI_RST_RosterHeader add isLocked int null";
                    if (GetDataDT(ExistColumnSql("GFI_RST_RosterHeader", "isLocked")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "Alter table GFI_RST_RosterHeader add isOriginal int null";
                    if (GetDataDT(ExistColumnSql("GFI_RST_RosterHeader", "isOriginal")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "alter table GFI_RST_Roster drop column isLocked";
                    if (GetDataDT(ExistColumnSql("GFI_RST_Roster", "isLocked")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "alter table GFI_RST_Roster drop column isOriginal";
                    if (GetDataDT(ExistColumnSql("GFI_RST_Roster", "isOriginal")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "MOH Roster");
                }
                #endregion
                #region Update 151. Audit keys and VCA index
                strUpd = "Rez000151";
                if (!CheckUpdateExist(strUpd))
                {
                    //GFI_SYS_AuditAccess
                    sql = "ALTER TABLE GFI_SYS_AuditAccess ADD CONSTRAINT PK_GFI_SYS_AuditAccess PRIMARY KEY  (iID) ";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_SYS_AuditAccess")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"SELECT object_name(default_object_id)
                             FROM sys.columns
                             WHERE object_id = object_id('[GFI_SYS_AuditAccess]')
                               AND name = 'UserID'";
                    dt = GetDataDT(sql);
                    if (dt.Rows.Count > 0)
                    {
                        String strConstrainName = dt.Rows[0][0].ToString();
                        if (strConstrainName.Length > 0)
                        {
                            sql = "ALTER TABLE GFI_SYS_AuditAccess] DROP FOREIGN KEY " + strConstrainName;
                            dbExecute(sql);
                        }
                    }

                    //GFI_SYS_LoginAudit
                    sql = "ALTER TABLE GFI_SYS_LoginAudit ADD CONSTRAINT PK_GFI_SYS_LoginAudit PRIMARY KEY  (auditId) ";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_SYS_LoginAudit")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"SELECT object_name(default_object_id)
                             FROM sys.columns
                             WHERE object_id = object_id('[GFI_SYS_LoginAudit]')
                               AND name = 'UserID'";
                    dt = GetDataDT(sql);
                    if (dt.Rows.Count > 0)
                    {
                        String strConstrainName = dt.Rows[0][0].ToString();
                        if (strConstrainName.Length > 0)
                        {
                            sql = "ALTER TABLE GFI_SYS_LoginAudit] DROP FOREIGN KEY " + strConstrainName;
                            dbExecute(sql);
                        }
                    }

                    //GFI_SYS_Audit
                    sql = "ALTER TABLE GFI_SYS_Audit ADD CONSTRAINT PK_GFI_SYS_Audit PRIMARY KEY  (auditId) ";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_SYS_Audit")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"SELECT object_name(default_object_id)
                             FROM sys.columns
                             WHERE object_id = object_id('[GFI_SYS_Audit]')
                               AND name = 'UserID'";
                    dt = GetDataDT(sql);
                    if (dt.Rows.Count > 0)
                    {
                        String strConstrainName = dt.Rows[0][0].ToString();
                        if (strConstrainName.Length > 0)
                        {
                            sql = "ALTER TABLE GFI_SYS_Audit] DROP FOREIGN KEY " + strConstrainName;
                            dbExecute(sql);
                        }
                    }

                    sql = "CREATE  INDEX IX_GFI_FLT_VCADetail_CordOrder] ON GFI_FLT_VCADetail] ([CordOrder])";
                    if (GetDataDT(ExistsIndex("GFI_FLT_VCADetail", "IX_GFI_FLT_VCADetail_CordOrder")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "Alter table GFI_FLT_VCAHeader add ParentVCAID int null";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_VCAHeader", "ParentVCAID")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_VCAHeader 
                            ADD CONSTRAINT FK_GFI_FLT_VCAHeader_Parent 
                                FOREIGN KEY(ParentVCAID) 
                                REFERENCES GFI_FLT_VCAHeader(VCAID) 
                         ON UPDATE NO ACTION 
	                     ON DELETE NO ACTION";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_VCAHeader_Parent")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrix 
                            ADD CONSTRAINT FK_GFI_SYS_GroupMatrix_Parent 
                                FOREIGN KEY(ParentGMID) 
                                REFERENCES GFI_SYS_GroupMatrix(GMID) 
                         ON UPDATE NO ACTION 
	                     ON DELETE NO ACTION";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrix_Parent")).Rows.Count == 0)
                        dbExecute(sql);

                    GlobalParams g = new GlobalParams();
                    g = g.GetGlobalParamsByName("WebLogo", sConnStr);
                    if (g == null)
                    {
                        g = new GlobalParams();
                        g.ParamName = "WebLogo";
                        g.PValue = "~/Resources/Images/Logo/NaveoOne.jpg";
                        g.ParamComments = "Web Logo for login page";
                        g.Save(g, true, sConnStr);
                    }

                    InsertDBUpdate(strUpd, "Audit keys and VCA index");
                }
                #endregion
                #region Update 152. RoadSpeed in debug
                strUpd = "Rez000152";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_GPS_GPSData ADD RoadSpeed int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_GPSData", "RoadSpeed")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_ARC_GPSData ADD RoadSpeed int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_ARC_GPSData", "RoadSpeed")).Rows.Count == 0)
                        dbExecute(sql);

                    GlobalParams g = new GlobalParams();
                    g = g.GetGlobalParamsByName("RoadSpeed", sConnStr);
                    if (g == null)
                    {
                        dt = GetDataDT("select min(uid) from GFI_GPS_GPSData where DateTimeGPS_UTC >= DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()))");
                        int iRoadSpeed = 0;
                        int.TryParse(dt.Rows[0][0].ToString(), out iRoadSpeed);

                        g = new GlobalParams();
                        g.ParamName = "RoadSpeed";
                        g.PValue = iRoadSpeed.ToString();
                        g.ParamComments = "RoadSpeed GPS Data ID, updated by service";
                        g.Save(g, true, sConnStr);
                    }

                    InsertDBUpdate(strUpd, "RoadSpeed in debug");
                }
                #endregion
                #region Update 153. District int in CustomerMaster
                strUpd = "Rez000153";
                if (!CheckUpdateExist(strUpd))
                {
                    dt = GetDataDT(GetFieldSchema("GFI_SYS_CustomerMaster", "District"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                    {
                        sql = @"ALTER TABLE GFI_SYS_CustomerMaster ALTER COLUMN District int null";
                        dbExecute(sql);

                        sql = @"ALTER TABLE GFI_SYS_CustomerMaster 
                                ADD CONSTRAINT FK_GFI_SYS_CustomerMaster_GFI_SYS_LookUpValues 
                                    FOREIGN KEY (District) 
                                    REFERENCES GFI_SYS_LookUpValues (VID) 
                                 ON UPDATE NO ACTION 
	                             ON DELETE NO ACTION ";
                        if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_CustomerMaster_GFI_SYS_LookUpValues")).Rows.Count == 0)
                            dbExecute(sql);
                    }

                    UOM u = new UOM();
                    u = u.GetUOMByDescription("Boxes", sConnStr);
                    if (u == null)
                    {
                        u = new UOM();
                        u.Description = "Boxes";
                        u.Save(u, true, sConnStr);
                    }

                    InsertDBUpdate(strUpd, "District int in CustomerMaster");
                }
                #endregion
                #region Update 154. Telemetry Table updates
                strUpd = "Rez000154";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_TEL_TelDataHeader] alter column MsgHeader] nvarchar](10) NULL";
                    dbExecute(sql);
                    sql = @"ALTER TABLE GFI_TEL_TelDataHeader] alter column Type] nvarchar](20) NULL";
                    dbExecute(sql);
                    sql = @"ALTER TABLE GFI_TEL_TelDataHeader] alter column MsgStatus] nvarchar](10) NULL";
                    dbExecute(sql);
                    sql = @"ALTER TABLE GFI_TEL_TelDataHeader] alter column isMaintenanceMode] INT NULL";
                    dbExecute(sql);
                    sql = @"ALTER TABLE GFI_TEL_TelDataHeader] alter column isFailedCallout] INT NULL";
                    dbExecute(sql);
                    sql = @"ALTER TABLE GFI_TEL_TelDataHeader] alter column Temperature] float NULL";
                    dbExecute(sql);
                    sql = @"ALTER TABLE GFI_TEL_TelDataHeader] alter column isBatteryLow] INT NULL";
                    dbExecute(sql);
                    sql = @"ALTER TABLE GFI_TEL_TelDataHeader] alter column isAutoConfig] INT NULL";
                    dbExecute(sql);
                    sql = @"ALTER TABLE GFI_TEL_TelDataHeader] alter column Carrier] nvarchar](50) NULL";
                    dbExecute(sql);
                    sql = @"ALTER TABLE GFI_TEL_TelDataHeader] alter column SignalStrength] INT NULL";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "Telemetry Table updates");
                }
                #endregion
                #region Update 155. Default MOH Shifts and missing indexes
                strUpd = "Rez000155";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "CREATE INDEX IX_GFI_PLN_PlanningViaPointD_HID] ON GFI_PLN_PlanningViaPointD ([HID]) INCLUDE ([UOM])";
                    if (GetDataDT(ExistsIndex("GFI_PLN_PlanningViaPointD_HID", "IX_GFI_PLN_PlanningViaPointD_HID")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    Shift s = new Shift();
                    s = s.GetShiftByName("D", sConnStr);
                    if (s == null)
                    {
                        s = new Shift();
                        s.sName = "D";
                        s.sComments = "Day shift";
                        s.TimeFr = Convert.ToDateTime("2017-04-08 08:00:00.000");
                        s.TimeTo = Convert.ToDateTime("2017-04-08 16:00:00.000");
                        s.Save(s, true, sConnStr);
                    }

                    s = new Shift();
                    s = s.GetShiftByName("N", sConnStr);
                    if (s == null)
                    {
                        s = new Shift();
                        s.sName = "N";
                        s.sComments = "Night shift";
                        s.TimeFr = Convert.ToDateTime("2017-04-08 16:00:00.001");
                        s.TimeTo = Convert.ToDateTime("2017-04-08 07:00:59.000");
                        s.Save(s, true, sConnStr);
                    }

                    InsertDBUpdate(strUpd, "Default MOH Shifts and missing indexes");
                }
                #endregion
                #region Update 156. sp_AMM_VehicleMaint_ProcessRecords updated
                strUpd = "Rez000156";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[sp_AMM_VehicleMaint_ProcessRecords]') AND type in (N'P', N'PC'))
                            DROP PROCEDURE sp_AMM_VehicleMaint_ProcessRecords]";
                    dbExecute(sql);

                    sql = @"CREATE PROCEDURE sp_AMM_VehicleMaint_ProcessRecords]
AS
BEGIN
    /***********************************************************************************************************************
    Version: 3.1.0.0
    Modifed: 11/ 08/2016.	06/08/2014
    Created By: Perry Ramen
    Modified By: Reza Dowlut
    Description: Process list of Maintenance Types for each vehicle to determine if they are To Schedule] or Overdue]
    ***********************************************************************************************************************/
        BEGIN
     	DECLARE @iRecCountProcessed int
     	
     	DECLARE @iMaintStatusId_Overdue int
		DECLARE @iMaintStatusId_ToSchedule int
		DECLARE @iMaintStatusId_Scheduled int
		DECLARE @iMaintStatusId_InProgress int
		DECLARE @iMaintStatusId_Completed int
		DECLARE @iMaintStatusId_Valid int
		DECLARE @iMaintStatusId_Expired int
		
		DECLARE @URI int
		DECLARE @MaintURI int
		DECLARE @AssetId int
		DECLARE @MaintTypeId int
		DECLARE @OccurrenceType int
     	DECLARE @NextMaintDate DateTime
		DECLARE @NextMaintDateTG DateTime 
		DECLARE @CurrentOdometer int
		DECLARE @NextMaintOdometer int
		DECLARE @NextMaintOdometerTG int
		DECLARE @CurrentEngHrs int
		DECLARE @NextMaintEngHrs int
		DECLARE @NextMaintEngHrsTG int
		DECLARE @MaintStatusId int
		
		DECLARE @MaintURIUpdate int
		
		DECLARE @OccurrenceDuration int
		DECLARE @OccurrenceDurationTh int
		DECLARE @OccurrencePeriod_cbo nvarchar(50)

		SET @iMaintStatusId_Overdue = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Overdue')
		SET @iMaintStatusId_ToSchedule = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'To Schedule')
		SET @iMaintStatusId_Scheduled = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Scheduled')
		SET @iMaintStatusId_InProgress = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'In Progress')		
		SET @iMaintStatusId_Completed = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Completed')				
		SET @iMaintStatusId_Valid = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Valid')		
		SET @iMaintStatusId_Expired = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Expired')				

		update GFI_AMM_VehicleMaintTypesLink set CurrentOdometer = GetCalcOdo(AssetId)

		DECLARE csrData CURSOR FOR
		SELECT URI, 
		    vmtl.MaintURI,
		    vmtl.AssetId,
		    vmtl.MaintTypeId,
		    vmt.OccurrenceType,
			vmtl.NextMaintDate, 
			vmtl.NextMaintDateTG, 
			vmtl.CurrentOdometer, 
			vmtl.NextMaintOdometer, 
			vmtl.NextMaintOdometerTG, 
			vmtl.CurrentEngHrs,
			vmtl.NextMaintEngHrs,
			vmtl.NextMaintEngHrsTG,
			vmtl.[Status]
			, vmt.OccurrenceDuration
			, vmt.OccurrenceDurationTh
			, vmt.OccurrencePeriod_cbo
		FROM GFI_AMM_VehicleMaintTypesLink vmtl
			INNER JOIN GFI_AMM_VehicleMaintTypes vmt ON vmtl.MaintTypeId = vmt.MaintTypeId
		
		OPEN csrData
		FETCH NEXT FROM csrData INTO @URI, @MaintURI, @AssetId, @MaintTypeId, @OccurrenceType, @NextMaintDate, @NextMaintDateTG, @CurrentOdometer, @NextMaintOdometer, @NextMaintOdometerTG, @CurrentEngHrs, @NextMaintEngHrs, @NextMaintEngHrsTG, @MaintStatusId
			, @OccurrenceDuration, @OccurrenceDurationTh, @OccurrencePeriod_cbo
		SET @iRecCountProcessed = 0
		
		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF @NextMaintDate IS NULL SET @NextMaintDate = GETDATE()
			     
			--select @URI, 'OccurrenceType 2', @OccurrenceType
			--	, 'AND'
			--	, '@NextMaintDateTG <= GETDATE()', @NextMaintDateTG, GETDATE()
			--	, '@NextMaintOdometerTG < @CurrentOdometer', @NextMaintOdometerTG, @CurrentOdometer
			--	, '@NextMaintEngHrsTG < @CurrentEngHrs', @NextMaintEngHrsTG, @CurrentEngHrs
			--	, 'AND'
			--	, '@MaintStatusId = @iMaintStatusId_Completed', @MaintStatusId, @iMaintStatusId_Completed
			--	, '@MaintStatusId = @iMaintStatusId_Valid', @MaintStatusId, @iMaintStatusId_Valid

			-- Threhold by date range
			declare @Maintdate DateTime
			declare @Thershold int
			set @Thershold = @OccurrenceDurationTh * -1 -- - @OccurrenceDuration
			set @Maintdate = DateAdd(month, @Thershold, GETDATE());
			if @OccurrencePeriod_cbo = 'DAY(S)' 
				set @Maintdate = DateAdd(day, @Thershold, GETDATE());

			select @Thershold, @NextMaintDate, @Maintdate
			--if(@Thershold != 0 and @NextMaintDate <= @Maintdate)
			--	select '*************************************************'

			-- Recurring - Flag As: To Schedule
			IF (
				(@OccurrenceType = 2) 
				AND ((@NextMaintDateTG <= GETDATE()) OR (@NextMaintOdometerTG < @CurrentOdometer) OR (@NextMaintEngHrsTG < @CurrentEngHrs) OR (@Thershold != 0 and @NextMaintDate <= @Maintdate)) 
				AND ((@MaintStatusId = @iMaintStatusId_Completed) OR (@MaintStatusId = @iMaintStatusId_Valid)))
			BEGIN
				IF NOT EXISTS(SELECT * FROM GFI_AMM_VehicleMaintTypesLink WHERE AssetId = @AssetId AND MaintTypeId = @MaintTypeId AND URI > @URI)
				BEGIN
					IF (@MaintStatusId = @iMaintStatusId_Completed)
					BEGIN
						INSERT GFI_AMM_VehicleMaintenance (AssetId, MaintTypeId_cbo, MaintStatusId_cbo, StartDate, EndDate, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy) 
						VALUES (@AssetId, @MaintTypeId, @iMaintStatusId_ToSchedule, @NextMaintDate, @NextMaintDate, GETDATE(), CURRENT_USER, GETDATE(), CURRENT_USER)
					
						SET @MaintURIUpdate = (SELECT SCOPE_IDENTITY())
											
						UPDATE GFI_AMM_VehicleMaintTypesLink
						SET MaintURI = @MaintURIUpdate, Status = @iMaintStatusId_ToSchedule, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
						WHERE URI = @URI
					END
					ELSE IF (@MaintStatusId = @iMaintStatusId_Valid)
					BEGIN
						SET @NextMaintDate = DATEADD(DD, 1, @NextMaintDate)
												
						INSERT GFI_AMM_VehicleMaintenance (AssetId, MaintTypeId_cbo, MaintStatusId_cbo, StartDate, EndDate, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy) 
						VALUES (@AssetId, @MaintTypeId, @iMaintStatusId_ToSchedule, @NextMaintDate, @NextMaintDate, GETDATE(), CURRENT_USER, GETDATE(), CURRENT_USER)
					
						SET @MaintURIUpdate = (SELECT SCOPE_IDENTITY())
												
						INSERT GFI_AMM_VehicleMaintTypesLink (MaintURI, AssetId, MaintTypeId, NextMaintDate, NextMaintDateTG, Status, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy)
						VALUES (@MaintURIUpdate, @AssetId, @MaintTypeId, @NextMaintDate, @NextMaintDate, @iMaintStatusId_ToSchedule, GETDATE(), CURRENT_USER, GETDATE(), CURRENT_USER)
					END
				END
			END

			--Flag As: Overdue
			IF (((@NextMaintDate <= GETDATE()) OR (@NextMaintOdometer < @CurrentOdometer) OR (@NextMaintEngHrs < @CurrentEngHrs)) AND (@MaintStatusId = @iMaintStatusId_ToSchedule))
			BEGIN				
				UPDATE GFI_AMM_VehicleMaintTypesLink
				SET Status = @iMaintStatusId_Overdue, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
				WHERE URI = @URI
				
				UPDATE GFI_AMM_VehicleMaintenance 
				SET MaintStatusId_cbo = @iMaintStatusId_Overdue, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
				WHERE URI = @MaintURI
			END
			                        
			--Auto Expiry: Lapsed Insurance
			IF ((@MaintStatusId = @iMaintStatusId_Valid) AND (@NextMaintDate < GETDATE()))
			BEGIN
				UPDATE GFI_AMM_VehicleMaintTypesLink
				SET Status = @iMaintStatusId_Expired , UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
				WHERE URI = @URI
				                        
				UPDATE GFI_AMM_VehicleMaintenance 
				SET MaintStatusId_cbo = @iMaintStatusId_Expired, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
				WHERE URI = @MaintURI
			END
										
			SET @iRecCountProcessed = @iRecCountProcessed + 1
			
			FETCH NEXT FROM csrData INTO @URI, @MaintURI, @AssetId, @MaintTypeId, @OccurrenceType, @NextMaintDate, @NextMaintDateTG, @CurrentOdometer, @NextMaintOdometer, @NextMaintOdometerTG, @CurrentEngHrs, @NextMaintEngHrs, @NextMaintEngHrsTG, @MaintStatusId
				, @OccurrenceDuration, @OccurrenceDurationTh, @OccurrencePeriod_cbo
		END
		
		CLOSE csrData
		DEALLOCATE csrData
		
		RETURN @iRecCountProcessed
    END
END
";
                    dbExecute(sql);
                    InsertDBUpdate(strUpd, "sp_AMM_VehicleMaint_ProcessRecords");
                }
                #endregion
                #region Update 157. isPointInVCA optimized
                strUpd = "Rez000157";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[isPointInVCA]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
                            DROP FUNCTION isPointInVCA]";

                    dbExecute(sql);
                    sql = @"
Create function isPointInVCA] (@Lon float, @Lat float, @iVCAID int)
returns int
as Begin
	--Extremity Chk
	Declare @iPossibleChk int
	select  @iPossibleChk = VCAID from 
		(select VCAID, min(Latitude) minLat, min(Longitude) minLon, max(Latitude) maxLat, max(Longitude) maxLon  
			from GFI_FLT_VCADetail where VCAID = @iVCAID
			group by VCAID
		) myView
	where 1 = 1
		and (@Lon between minLon and maxLon)
		and (@Lat between minLat and maxLat)

	if(@iPossibleChk is null)
		return 0
	--
	
	Declare @iResult int
	set @iResult = 0

	DECLARE	@Longitude float] 
	DECLARE	@Latitude float]
	DECLARE	@nLongitude float] 
	DECLARE	@nLatitude float]
	DECLARE	@myLongitude float] 
	DECLARE	@myLatitude float]
	set	@myLongitude = @Lon
	set	@myLatitude = @Lat

	declare @c bit
	set @c = 0

	declare xyCursor cursor  LOCAL SCROLL STATIC FOR  
			select Latitude, Longitude from GFI_FLT_VCADetail
			where VCAID = @iVCAID order by CordOrder

	open xyCursor
		fetch next from xyCursor into @Latitude, @Longitude
		WHILE @@FETCH_STATUS = 0   
		BEGIN 
		fetch PRIOR from xyCursor into @Latitude, @Longitude
		fetch next from xyCursor into @nLatitude, @nLongitude

					if ((((@Latitude <= @myLatitude) and (@myLatitude < @nLatitude))
							or ((@nLatitude <= @myLatitude) and (@myLatitude < @Latitude)))
							and (@myLongitude < (@nLongitude - @Longitude) * (@myLatitude - @Latitude)
								/ (@nLatitude - @Latitude) + @Longitude))

						if(@c = 0)
							set @c = 1
						else
							set @c = 0

						if(@c = 1)
						begin
							Break;
						end

			fetch next from xyCursor into @Latitude, @Longitude
		END
	close xyCursor
	deallocate xyCursor

	if(@c = 0)
		set @iResult = 0
	else
		set @iResult = 1		

	return @iResult
end";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "isPointInVCA optimized");
                }
                #endregion
                #region Update 158. TelDataDetails updated
                strUpd = "Rez000158";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_TEL_TelDataDetail ADD SensorAddress int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_TEL_TelDataDetail", "SensorAddress")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_TEL_TelDataDetail ADD UOM int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_TEL_TelDataDetail", "UOM")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_TEL_TelDataDetail 
                            ADD CONSTRAINT FK_GFI_TEL_TelDataDetail_GFI_SYS_UOM
                                FOREIGN KEY (UOM) 
                                REFERENCES GFI_SYS_UOM (UID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_TEL_TelDataDetail_GFI_SYS_UOM")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "TelDataDetails updated");
                }
                #endregion
                #region Update 159. TwoWayAuthenticationOnSSL
                strUpd = "Rez000159";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_SYS_User ADD CONSTRAINT UK_GFI_SYS_User_UN] Unique (Username)";
                    if (GetDataDT(ExistsSQLConstraint("UK_GFI_SYS_User_UN")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD CONSTRAINT UK_GFI_SYS_User_Email] Unique (Email)";
                    if (GetDataDT(ExistsSQLConstraint("UK_GFI_SYS_User_Email")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD MobileAccess int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "MobileAccess")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD ExternalAccess int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "ExternalAccess")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_GlobalParams DROP FOREIGN KEY U_dbo_GFI_SYS_GlobalParams_1";
                    if (GetDataDT(ExistsSQLConstraint("U_dbo_GFI_SYS_GlobalParams_1")).Rows.Count != 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_GlobalParams alter column ParamName nvarchar(35) NOT NULL";
                    dt = GetDataDT(GetFieldSchema("GFI_SYS_GlobalParams", "ParamName"));
                    if (dt.Rows[0]["CHARACTER_MAXIMUM_LENGTH"].ToString() != "35")
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_GlobalParams ADD CONSTRAINT UK_GFI_SYS_GlobalParams_PN] Unique (ParamName)";
                    if (GetDataDT(ExistsSQLConstraint("UK_GFI_SYS_GlobalParams_PN")).Rows.Count == 0)
                        dbExecute(sql);

                    GlobalParams g = new GlobalParams();
                    g = g.GetGlobalParamsByName("TwoWayAuthenticationEnabledOnSSL", sConnStr);
                    if (g == null)
                    {
                        g = new GlobalParams();
                        g.ParamName = "TwoWayAuthenticationEnabledOnSSL";
                        g.PValue = "1";
                        g.ParamComments = "1 enabled";
                        g.Save(g, true, sConnStr);
                    }

                    InsertDBUpdate(strUpd, "TwoWayAuthenticationOnSSL");
                }
                #endregion
                #region Update 160. Driver Licenses
                strUpd = "Rez000160";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_FLT_ResourceLicenses](
	                            iID] int] AUTO_INCREMENT NOT NULL,
	                            DriverID] int] NOT NULL,
	                            sType] nvarchar](50) NULL,
	                            sCategory] nvarchar](50) NULL,
	                            sNumber] nvarchar](50) NULL,
	                            IssueDate] Datetime] NULL,
	                            ExpiryDate] Datetime] NULL,
                            CONSTRAINT PK_GFI_FLT_ResourceLicenses] PRIMARY KEY  
                                ([iID] ASC)
                         ) ";
                    if (GetDataDT(ExistTableSql("GFI_FLT_ResourceLicenses")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_ResourceLicenses 
                            ADD CONSTRAINT FK_GFI_FLT_ResourceLicenses_GFI_FLT_Driver 
                                FOREIGN KEY(DriverID) 
                                REFERENCES GFI_FLT_Driver(DriverID) 
                         ON UPDATE NO ACTION 
	                     ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ResourceLicenses_GFI_FLT_Driver")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "Vehicle");
                }
                #endregion
                #region Update 161. Pln Live indexes
                strUpd = "Rez000161";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"DROP INDEX IX_Live_GFI_PLN_PlanningViaPointH] ON GFI_PLN_PlanningViaPointH] /*WITH ( ONLINE = OFF )*/";
                    if (GetDataDT(ExistsIndex("GFI_PLN_PlanningViaPointH", "IX_Live_GFI_PLN_PlanningViaPointH")).Rows.Count == 1)
                        dbExecute(sql);

                    sql = @"DROP INDEX IX_Live_GFI_PLN_ScheduledPlan] ON GFI_PLN_ScheduledPlan] /*WITH ( ONLINE = OFF )*/";
                    if (GetDataDT(ExistsIndex("GFI_PLN_ScheduledPlan", "IX_Live_GFI_PLN_ScheduledPlan")).Rows.Count == 1)
                        dbExecute(sql);

                    sql = @"DROP INDEX IX_Live1_GFI_PLN_PlanningViaPointH] ON GFI_PLN_PlanningViaPointH] /*WITH ( ONLINE = OFF )*/";
                    if (GetDataDT(ExistsIndex("GFI_PLN_PlanningViaPointH", "IX_Live1_GFI_PLN_PlanningViaPointH")).Rows.Count == 1)
                        dbExecute(sql);

                    sql = "CREATE  INDEX IX_Live_GFI_PLN_PlanningViaPointH] ON GFI_PLN_PlanningViaPointH] ([PID]) INCLUDE ([iID])";
                    if (GetDataDT(ExistsIndex("GFI_PLN_PlanningViaPointH", "IX_Live_GFI_PLN_PlanningViaPointH")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE  INDEX IX_LiveVCA_GFI_PLN_PlanningViaPointH] ON GFI_PLN_PlanningViaPointH] ([LocalityVCA], dtUTC]) INCLUDE ([iID], PID], Buffer])";
                    if (GetDataDT(ExistsIndex("GFI_PLN_PlanningViaPointH", "IX_LiveVCA_GFI_PLN_PlanningViaPointH")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE  INDEX IX_LiveLonLat_GFI_PLN_PlanningViaPointH] ON GFI_PLN_PlanningViaPointH] ([Lon], dtUTC]) INCLUDE ([iID], PID], Lat], Buffer])";
                    if (GetDataDT(ExistsIndex("GFI_PLN_PlanningViaPointH", "IX_LiveLonLat_GFI_PLN_PlanningViaPointH")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE  INDEX IX_LiveZone_GFI_PLN_PlanningViaPointH] ON GFI_PLN_PlanningViaPointH] ([ZoneID], dtUTC]) INCLUDE ([iID], PID], Buffer])";
                    if (GetDataDT(ExistsIndex("GFI_PLN_PlanningViaPointH", "IX_LiveZone_GFI_PLN_PlanningViaPointH")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE  INDEX IX_Live_GFI_PLN_PlanningResource] ON GFI_PLN_PlanningResource] ([ResourceType]) INCLUDE ([PID], DriverID])";
                    if (GetDataDT(ExistsIndex("GFI_PLN_PlanningResource", "IX_Live_GFI_PLN_PlanningResource")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE  INDEX IX_Live_GFI_PLN_ScheduledPlan] ON GFI_PLN_ScheduledPlan] ([PID]) INCLUDE ([HID])";
                    if (GetDataDT(ExistsIndex("GFI_PLN_ScheduledPlan", "IX_Live_GFI_PLN_ScheduledPlan")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE  INDEX IX_GFI_PLN_ScheduledPlan_HID] ON GFI_PLN_ScheduledPlan] ([HID])";
                    if (GetDataDT(ExistsIndex("GFI_PLN_ScheduledPlan", "IX_GFI_PLN_ScheduledPlan_HID")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE  INDEX IX_Live_GFI_FLT_VCADetail] ON GFI_FLT_VCADetail] ([VCAID]) INCLUDE ([Latitude], Longitude])";
                    if (GetDataDT(ExistsIndex("GFI_FLT_VCADetail", "IX_Live_GFI_FLT_VCADetail")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE  INDEX IX_GFI_SYS_GroupMatrixPlanning] ON GFI_SYS_GroupMatrixPlanning] ([iID])";
                    if (GetDataDT(ExistsIndex("GFI_SYS_GroupMatrixPlanning", "IX_GFI_SYS_GroupMatrixPlanning")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "Pln Live indexes");
                }
                #endregion

                #region Update 162. User Login.
                strUpd = "Rez000162";
                if (!CheckUpdateExist(strUpd))
                {
                    //Exception Indexes
                    sql = "CREATE INDEX IX_GFI_GPS_Exceptions_RuleID_AssetID_GroupID] ON GFI_GPS_Exceptions] ([RuleID], AssetID], GroupID]) INCLUDE ([InsertedAt])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_Exceptions", "IX_GFI_GPS_Exceptions_RuleID_AssetID_GroupID")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "delete from GFI_GPS_ProcessPending";
                    dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD TwoStepVerif nvarchar(10) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "TwoStepVerif")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD TwoStepAttempts int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "TwoStepAttempts")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD UserToken UniqueIdentifier NOT NULL default newid()";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "UserToken")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User] ADD CONSTRAINT UK_Token_GFI_SYS_User] Unique ([UserToken] ASC)";
                    if (GetDataDT(ExistsSQLConstraint("UK_Token_GFI_SYS_User")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD PswdExpiryDate datetime NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "PswdExpiryDate")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD PswdExpiryWarnDate datetime NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "PswdExpiryWarnDate")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "update GFI_SYS_User set LoginCount = 0 where LoginCount is null";
                    dbExecute(sql);
                    sql = @"ALTER TABLE GFI_SYS_User alter column LoginCount int not NULL";
                    dbExecute(sql);
                    sql = "Alter table GFI_SYS_User add Constraint D_LoginCount_GFI_SYS_User Default 0 for LoginCount";
                    if (GetDataDT(ExistsSQLConstraint("D_LoginCount_GFI_SYS_User")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "update GFI_SYS_User set email = 'Installer@naveo.mu' where email = 'INSTALLER'";
                    dbExecute(sql);
                    sql = "update GFI_SYS_User set email = 'Admin@naveo.mu' where email = 'ADMIN'";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "User Login");
                }
                #endregion
                #region Update 163. Modules with Matrix
                strUpd = "Rez000163";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_SYS_NaveoModules]
                            (
	                              iID] INT AUTO_INCREMENT NOT NULL
	                            , Description] nvarchar(50) NOT NULL
                                , CONSTRAINT PK_GFI_SYS_NaveoModules] PRIMARY KEY  ([iID] ASC)
                             )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_NaveoModules")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_SYS_GroupMatrixNaveoModules]
                            (
	                              MID] INT AUTO_INCREMENT NOT NULL
	                            , GMID] INT NOT NULL
	                            , iID] INT NOT NULL
                                , CONSTRAINT PK_GFI_SYS_GroupMatrixNaveoModules] PRIMARY KEY  ([MID] ASC)
                             )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrixNaveoModules")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixNaveoModules
                            ADD CONSTRAINT FK_GFI_SYS_GroupMatrixNaveoModules_GFI_SYS_GroupMatrix 
                                FOREIGN KEY (GMID) 
                                REFERENCES GFI_SYS_GroupMatrix (GMID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixNaveoModules_GFI_SYS_GroupMatrix")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixNaveoModules 
                            ADD CONSTRAINT FK_GFI_SYS_GroupMatrixNaveoModules_GFI_SYS_NaveoModules
                                FOREIGN KEY(iID) 
                                REFERENCES GFI_SYS_NaveoModules(iID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixNaveoModules_GFI_SYS_NaveoModules")).Rows.Count == 0)
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "Naveo Modules");
                }
                #endregion
                #region Update 164. Permissions
                strUpd = "Rez000164";
                if (!CheckUpdateExist(strUpd))
                {
                    #region GFI_SYS_Permissions
                    sql = @"CREATE TABLE GFI_SYS_Permissions]
                            (
	                              iID] INT AUTO_INCREMENT NOT NULL
	                            , Description] nvarchar(50) NOT NULL
                                , ModuleID int not null
                                , CONSTRAINT PK_GFI_SYS_Permissions] PRIMARY KEY  ([iID] ASC)
                             )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_Permissions")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_Permissions 
                            ADD CONSTRAINT FK_GFI_SYS_Permissions_GFI_SYS_NaveoModules
                                FOREIGN KEY(ModuleID) 
                                REFERENCES GFI_SYS_NaveoModules(iID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_Permissions_GFI_SYS_NaveoModules")).Rows.Count == 0)
                        dbExecute(sql);
                    #endregion

                    #region Roles Groups]
                    sql = @"CREATE TABLE GFI_SYS_Roles]
                            (
	                              iID] INT AUTO_INCREMENT NOT NULL
	                            , Description] nvarchar(50) NOT NULL
                                , CONSTRAINT PK_GFI_SYS_Groups] PRIMARY KEY  ([iID] ASC)
                             )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_Roles")).Rows.Count == 0)
                        dbExecute(sql);

                    //GroupMatrix
                    sql = @"CREATE TABLE GFI_SYS_GroupMatrixRoles]
                            (
	                              MID] INT AUTO_INCREMENT NOT NULL
	                            , GMID] INT NOT NULL
	                            , iID] INT NOT NULL
                                , CONSTRAINT PK_GFI_SYS_GroupMatrixRoles] PRIMARY KEY  ([MID] ASC)
                             )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrixRoles")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixRoles
                            ADD CONSTRAINT FK_GFI_SYS_GroupMatrixRoles_GFI_SYS_GroupMatrix 
                                FOREIGN KEY (GMID) 
                                REFERENCES GFI_SYS_GroupMatrix (GMID) 
                             ON UPDATE NO ACTION 
	                         ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixRoles_GFI_SYS_GroupMatrix")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixRoles 
                            ADD CONSTRAINT FK_GFI_SYS_GroupMatrixRoles_GFI_SYS_Roles
                                FOREIGN KEY(iID) 
                                REFERENCES GFI_SYS_Roles(iID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixRoles_GFI_SYS_Roles")).Rows.Count == 0)
                        dbExecute(sql);
                    #endregion

                    #region RolePermissions
                    sql = @"CREATE TABLE GFI_SYS_RolePermissions]
                            (
	                            RoleID int NOT NULL
                                , PermissionID int not null
                                , CONSTRAINT PK_GFI_SYS_RolePermissions] PRIMARY KEY  (RoleID, PermissionID)
                             )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_RolePermissions")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_RolePermissions 
                            ADD CONSTRAINT FK_GFI_SYS_RolePermissions_GFI_SYS_Roles
                                FOREIGN KEY(RoleID) 
                                REFERENCES GFI_SYS_Roles(iID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_RolePermissions_GFI_SYS_Roles")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_RolePermissions 
                            ADD CONSTRAINT FK_GFI_SYS_RolePermissions_GFI_SYS_Permissions
                                FOREIGN KEY(PermissionID) 
                                REFERENCES GFI_SYS_Permissions(iID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_RolePermissions_GFI_SYS_Permissions")).Rows.Count == 0)
                        dbExecute(sql);
                    #endregion

                    #region UserRoles
                    sql = @"CREATE TABLE GFI_SYS_UserRoles]
                            (
                                UserID int not null
	                            , RoleID int NOT NULL
                                , CONSTRAINT PK_GFI_SYS_UserRoles] PRIMARY KEY  (UserID, RoleID)
                             )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_UserRoles")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_UserRoles 
                            ADD CONSTRAINT FK_GFI_SYS_UserRoles_GFI_SYS_User
                                FOREIGN KEY(UserID) 
                                REFERENCES GFI_SYS_User(UID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_UserRoles_GFI_SYS_User")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_UserRoles 
                            ADD CONSTRAINT FK_GFI_SYS_UserRoles_GFI_SYS_Roles
                                FOREIGN KEY(RoleID) 
                                REFERENCES GFI_SYS_Roles(iID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_UserRoles_GFI_SYS_Roles")).Rows.Count == 0)
                        dbExecute(sql);
                    #endregion

                    InsertDBUpdate(strUpd, "Permissions");
                }
                #endregion
                #region Update 165. User Login additional fields, GFI_SYS_Tokens and sDriverName
                strUpd = "Rez000165";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_SYS_User ADD LastName nvarchar(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "LastName")).Rows.Count == 0)
                        dbExecute(sql);

                    if (GetDataDT(ExistTableSql("AspNetUsers")).Rows[0][0].ToString() == "1")
                    {
                        sql = @"update GFI_SYS_User
	                                set LastName = T2.LastName
                                from GFI_SYS_User T1
	                                inner join AspNetUsers T2 on T1.Email = T2.Email";
                        dbExecute(sql);
                    }

                    sql = @"CREATE TABLE GFI_SYS_Tokens]
                            (
	                            TokenID nvarchar(37) NOT NULL
	                            , sqlCmd] nvarchar(max) NOT NULL
                                , ExpiryDate datetime not null
                                , sData nvarchar(150) null
                                , sAdditionalData nvarchar(150) null
                                , CONSTRAINT PK_GFI_SYS_Tokens] PRIMARY KEY  ([TokenID] ASC)
                             )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_Tokens")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_FLT_Driver alter column sDriverName nvarchar(100) not null";
                    dbExecute(sql);

                    sql = "update GFI_FLT_Driver set sDriverName = sLastName + ' ' + sFirtName where sDriverName = sFirtName";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "User Login additional fields, GFI_SYS_Tokens and sDriverName");
                }
                #endregion
                #region Update 166. updated sp_MOHRequestCode
                strUpd = "Rez000166";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_PLN_Planning] DROP FOREIGN KEY UK_GFI_PLN_Planning_RequestCode]";
                    if (GetDataDT(ExistsSQLConstraint("UK_GFI_PLN_Planning_RequestCode")).Rows.Count > 0)
                        dbExecute(sql);

                    sql = "alter table GFI_PLN_Planning alter column RequestCode nvarchar(14) null";
                    dbExecute(sql);
                    sql = "alter table GFI_PLN_Planning alter column ParentRequestCode nvarchar(14) null";
                    dbExecute(sql);

                    sql = @"ALTER TABLE GFI_PLN_Planning] ADD  CONSTRAINT UK_GFI_PLN_Planning_RequestCode] Unique ([RequestCode] ASC)";
                    if (GetDataDT(ExistsSQLConstraint("UK_GFI_PLN_Planning_RequestCode")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_PLN_MOHRequestCode ADD iMonth int NOT NULL Default 0";
                    if (GetDataDT(ExistColumnSql("GFI_PLN_MOHRequestCode", "iMonth")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"Alter Table GFI_PLN_MOHRequestCode DROP FOREIGN KEY PK_GFI_PLN_MOHRequestCode";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_PLN_MOHRequestCode")).Rows.Count > 0)
                        dbExecute(sql);

                    sql = "alter table GFI_PLN_MOHRequestCode add CONSTRAINT PK_GFI_PLN_MOHRequestCode] PRIMARY KEY  ([Code], iYear], iMonth] ASC)";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_PLN_MOHRequestCode")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[sp_MOHRequestCode]') AND type in (N'P', N'PC'))
                            DROP PROCEDURE sp_MOHRequestCode]";
                    dbExecute(sql);

                    sql = @"
Create PROCEDURE sp_MOHRequestCode] @Code nvarchar(3)
as Begin
                        
    DECLARE @iYear int
    DECLARE @iMonth int
    DECLARE @sYear nvarchar(2)
    DECLARE @iSeed int
    DECLARE @sSeed nvarchar(5)
    DECLARE @ReqNo nvarchar(14)
    DECLARE @idd int
    DECLARE @sdd nvarchar(2)
    DECLARE @imm int
    DECLARE @smm nvarchar(2)
    
    set @iYear = 0

    ;with cta as
    (
	    select * from GFI_PLN_MOHRequestCode] where Code] = @Code and iYear = DATEPART(yyyy, GETDATE()) and iMonth = DATEPART(MM, GETDATE())
    )
    select @iYear = iYear, @iSeed = iSeed, @iMonth = iMonth from cta;

    if (@iYear = 0)
    begin
	    insert GFI_PLN_MOHRequestCode] values (@Code, DATEPART(yyyy, GETDATE()), 0, DATEPART(MM, GETDATE()));

	    ;with cta as
	    (
		    select * from GFI_PLN_MOHRequestCode] where Code] = @Code and iYear = DATEPART(yyyy, GETDATE()) and iMonth = DATEPART(MM, GETDATE())
	    )
	    select @iYear = iYear, @iSeed = iSeed, @iMonth = iMonth from cta;
    end

    update GFI_PLN_MOHRequestCode] set iSeed = iSeed + 1 where Code] = @Code and iYear = DATEPART(yyyy, GETDATE()) and iMonth = DATEPART(MM, GETDATE())
	
    ;with cta as
    (
	    select * from GFI_PLN_MOHRequestCode] where Code] = @Code and iYear = DATEPART(yyyy, GETDATE()) 
    )
    select @iYear = iYear, @iSeed = iSeed from cta;

    --yy
    if(@iYear > 2000)
	    set @iYear = @iYear - 2000
    set @sYear = CAST(@iYear AS nvarchar(2))

    --mm
    set @imm = DATEPART(mm,GETDATE());
    if(@imm < 10)
	    set @smm = '0' + CAST(@imm AS nvarchar(2))
    else
	    set @smm = CAST(@imm AS nvarchar(2))

    --dd
    set @idd = DATEPART(dd,GETDATE());
    if(@idd < 10)
	    set @sdd = '0' + CAST(@idd AS nvarchar(2))
    else
	    set @sdd = CAST(@idd AS nvarchar(2))

    --Seed
    if(@iSeed < 10)	        set @sSeed = '0000' + CAST(@iSeed AS nvarchar(5))
    else if(@iSeed < 100)	set @sSeed = '000' + CAST(@iSeed AS nvarchar(5))
    else if(@iSeed < 1000)	set @sSeed = '00' + CAST(@iSeed AS nvarchar(5))
    else if(@iSeed < 10000)	set @sSeed = '0' + CAST(@iSeed AS nvarchar(5))
    else                    set @sSeed = CAST(@iSeed AS nvarchar(5))

    set @ReqNo = @Code + @sdd + @smm + @sYear + @sSeed
    select @ReqNo
end";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "updated sp_MOHRequestCode");
                }
                #endregion

                #region Update 167. User, Modules, Permission
                strUpd = "Rez000167";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_SYS_GroupMatrix ADD CompanyCode nvarchar(15) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_GroupMatrix", "CompanyCode")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_SYS_GroupMatrix ADD LegalName nvarchar(150) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_GroupMatrix", "LegalName")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_SYS_RolePermissions ADD iCreate int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_RolePermissions", "iCreate")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_SYS_RolePermissions ADD iRead int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_RolePermissions", "iRead")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_SYS_RolePermissions ADD iUpdate int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_RolePermissions", "iUpdate")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_SYS_RolePermissions ADD iDelete int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_RolePermissions", "iDelete")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_SYS_RolePermissions ADD ControllerName nvarchar(50) not null";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_RolePermissions", "ControllerName")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_SYS_RolePermissions ADD ModuleID int not NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_RolePermissions", "ModuleID")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_SYS_Permissions] DROP FOREIGN KEY FK_GFI_SYS_Permissions_GFI_SYS_NaveoModules";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_Permissions_GFI_SYS_NaveoModules")).Rows.Count > 0)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_SYS_RolePermissions] DROP FOREIGN KEY FK_GFI_SYS_RolePermissions_GFI_SYS_Permissions";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_RolePermissions_GFI_SYS_Permissions")).Rows.Count > 0)
                        dbExecute(sql);

                    sql = @"drop table GFI_SYS_Permissions";
                    if (GetDataDT(ExistTableSql("GFI_SYS_Permissions")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_RolePermissions 
                            ADD CONSTRAINT FK_GFI_SYS_RolePermissions_GFI_SYS_NaveoModules
                                FOREIGN KEY(ModuleID) 
                                REFERENCES GFI_SYS_NaveoModules(iID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_RolePermissions_GFI_SYS_NaveoModules")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_NaveoModules] ADD CONSTRAINT UK_GFI_SYS_NaveoModules] Unique ([Description] ASC)";
                    if (GetDataDT(ExistsSQLConstraint("UK_GFI_SYS_NaveoModules")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"IF NOT EXISTS (SELECT * FROM GFI_SYS_NaveoModules WHERE Description = 'Admin')
                               BEGIN
                                   INSERT INTO GFI_SYS_NaveoModules (Description) VALUES ('Admin')
                               END";
                    dbExecute(sql);

                    sql = @"IF NOT EXISTS (SELECT * FROM GFI_SYS_NaveoModules WHERE Description = 'Fleet')
                               BEGIN
                                   INSERT INTO GFI_SYS_NaveoModules (Description) VALUES ('Fleet')
                               END";
                    dbExecute(sql);

                    sql = @"IF NOT EXISTS (SELECT * FROM GFI_SYS_NaveoModules WHERE Description = 'Maintenance')
                               BEGIN
                                   INSERT INTO GFI_SYS_NaveoModules (Description) VALUES ('Maintenance')
                               END";
                    dbExecute(sql);

                    sql = "ALTER TABLE GFI_SYS_RolePermissions] DROP FOREIGN KEY FK_GFI_SYS_RolePermissions_GFI_SYS_NaveoModules";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_RolePermissions_GFI_SYS_NaveoModules")).Rows.Count == 1)
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_SYS_RolePermissions drop column ControllerName";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_RolePermissions", "ControllerName")).Rows[0][0].ToString() == "1")
                        dbExecute(sql);

                    sql = "ALTER TABLE GFI_SYS_RolePermissions drop column ModuleID";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_RolePermissions", "ModuleID")).Rows[0][0].ToString() == "1")
                        dbExecute(sql);

                    #region GFI_SYS_Permissions
                    sql = @"CREATE TABLE GFI_SYS_Permissions]
                            (
	                              iID] INT AUTO_INCREMENT NOT NULL
	                            , ControllerName] nvarchar(50) NOT NULL
                                , ModuleID int not null
                                , CONSTRAINT PK_GFI_SYS_Permissions] PRIMARY KEY  ([iID] ASC)
                             )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_Permissions")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_Permissions 
                            ADD CONSTRAINT FK_GFI_SYS_Permissions_GFI_SYS_NaveoModules
                                FOREIGN KEY(ModuleID) 
                                REFERENCES GFI_SYS_NaveoModules(iID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_Permissions_GFI_SYS_NaveoModules")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_RolePermissions 
                            ADD CONSTRAINT FK_GFI_SYS_RolePermissions_GFI_SYS_Permissions
                                FOREIGN KEY(PermissionID) 
                                REFERENCES GFI_SYS_Permissions(iID) 
                         ON UPDATE  NO ACTION 
	                     ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_RolePermissions_GFI_SYS_Permissions")).Rows.Count == 0)
                        dbExecute(sql);
                    #endregion

                    NaveoModules n = new NaveoModules();
                    n = n.GetNaveoModulesByName("ADMIN", sConnStr);
                    if (n != null)
                    {
                        Boolean b = false;
                        foreach (Permissions perm in n.lPermissions)
                            if (perm.ControllerName == "User")
                                b = true;
                        if (!b)
                        {
                            if (n.lMatrix.Count == 0)
                            {
                                GroupMatrix gmRoot = new GroupMatrix();
                                gmRoot = new GroupMatrix().GetRoot(sConnStr);

                                Matrix m = new Matrix();
                                m.GMID = gmRoot.gmID;
                                m.oprType = DataRowState.Added;
                                m.iID = n.iID;
                                n.lMatrix.Add(m);
                            }

                            Permissions p = new Permissions();
                            p.ControllerName = "User";
                            p.ModuleID = n.iID;
                            p.oprType = DataRowState.Added;
                            n.lPermissions.Add(p);
                            n.Save(n, false, sConnStr);
                        }
                    }

                    n = new NaveoModules();
                    n = n.GetNaveoModulesByName("GIS", sConnStr);
                    if (n == null)
                    {
                        n = new NaveoModules();
                        n.description = "GIS";
                        n.lMatrix = new List<Matrix>();
                        n.lPermissions = new List<Permissions>();

                        GroupMatrix gmRoot = new GroupMatrix();
                        gmRoot = new GroupMatrix().GetRoot(sConnStr);

                        Matrix m = new Matrix();
                        m.GMID = gmRoot.gmID;
                        m.oprType = DataRowState.Added;
                        n.lMatrix.Add(m);

                        Permissions p = new Permissions();
                        p.ControllerName = "Map";
                        p.oprType = DataRowState.Added;
                        n.lPermissions.Add(p);

                        n.Save(n, true, sConnStr);
                    }

                    InsertDBUpdate(strUpd, "User, Modules, Permission");
                }
                #endregion
                #region Update 168. GetCalcOdo updated
                strUpd = "Rez000168";
                if (!CheckUpdateExist(strUpd))
                {
                    //TO create FN
                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[GetCalcOdo]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
                            DROP FUNCTION GetCalcOdo]";
                    dbExecute(sql);
                    sql = @"CREATE function GetCalcOdo] (@AssetID int)
                            returns int
                            as Begin
                        
                            DECLARE @fOdo float
                   
	                            ;with cta as
	                            (
		                            select *, 
			                            row_number() over(partition by AssetID order by URI desc) as RowNum
		                            from GFI_AMM_VehicleMaintenance
		                            where AssetId = @AssetID
			                            and ActualOdometer is not null
			                            and ActualOdometer > 0
	                            )
	                            select @fOdo = COALESCE(SUM(h.TripDistance),0) + COALESCE(max(cta.ActualOdometer),0)
	                            from GFI_GPS_TripHeader h
	                            left outer join cta on h.AssetID = cta.AssetId and cta.RowNum = 1
		                            where 
				                            h.dtEnd >= COALESCE(cta.EndDate,DATEADD(YY, -50, GETDATE()))
				                            and h.AssetId = @AssetID

	                            if(@fOdo = 0)
	                            begin
		                            ;with cta as
		                            (
			                            select *, 
				                            row_number() over(partition by AssetID order by URI desc) as RowNum
			                            from GFI_AMM_VehicleMaintenance
			                            where AssetId = @AssetID
				                            and ActualOdometer is not null
				                            and ActualOdometer > 0
		                            )
		                            select @fOdo = COALESCE(max(cta.ActualOdometer),0)
			                            from cta where cta.RowNum = 1
	                            end

	                            if(@fOdo = 0)
	                            begin
		                            select @fOdo = COALESCE(SUM(h.TripDistance),0)
			                            from GFI_GPS_TripHeader h
		                            where  h.AssetId = @AssetID
	                            end

	                            return CAST(@fOdo AS int)
                            end";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "GetCalcOdo updated");
                }
                #endregion
                #region Update 169. GetCalcEngHrs updated
                strUpd = "Rez000169";
                if (!CheckUpdateExist(strUpd))
                {
                    //to create FN
                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[GetCalcEngHrs]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
                            DROP FUNCTION GetCalcEngHrs]";
                    dbExecute(sql);
                    sql = @"CREATE function GetCalcEngHrs] (@AssetID int)
                            returns int
                            as Begin
                        
                            DECLARE @fEng float
                        
                            ;with cta as
                            (
                                select *, 
                                    row_number() over(partition by AssetID order by URI desc) as RowNum
	                            from GFI_AMM_VehicleMaintenance
	                            where AssetId = @AssetID
		                            and ActualEngineHrs is not null
		                            and ActualEngineHrs > 0
                            )
                            select @fEng = COALESCE(SUM(h.TripTime),0) + COALESCE(max(cta.ActualEngineHrs),0)
                            from GFI_GPS_TripHeader h
                            left outer join cta on h.AssetID = cta.AssetId and cta.RowNum = 1
	                            where 
		                             h.dtEnd >= COALESCE(cta.EndDate,DATEADD(YY, -50, GETDATE()))
		                             and h.AssetId = @AssetID


	                        if(@fEng = 0)
	                            begin
		                            ;with cta as
		                            (
			                            select *, 
				                            row_number() over(partition by AssetID order by URI desc) as RowNum
			                            from GFI_AMM_VehicleMaintenance
			                            where AssetId = @AssetID
				                            and ActualEngineHrs is not null
				                            and ActualEngineHrs > 0
		                            )
		                            select @fEng = COALESCE(max(cta.ActualEngineHrs),0)
			                            from cta where cta.RowNum = 1
	                            end

	                            if(@fEng = 0)
	                            begin
		                            select @fEng = COALESCE(SUM(h.TripTime),0)
			                            from GFI_GPS_TripHeader h
		                            where  h.AssetId = @AssetID
	                            end

                            return CAST(@fEng AS int)
                            end";
                    dbExecute(sql);

                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[sp_AMM_VehicleMaint_ProcessRecords]') AND type in (N'P', N'PC'))
                            DROP PROCEDURE sp_AMM_VehicleMaint_ProcessRecords]";
                    dbExecute(sql);

                    sql = @"
CREATE PROCEDURE sp_AMM_VehicleMaint_ProcessRecords]
AS
BEGIN
    /***********************************************************************************************************************
    Version: 3.1.0.0
    Modifed: 11/ 08/2016.	06/08/2014
    Created By: Perry Ramen
    Modified By: Reza Dowlut
    Description: Process list of Maintenance Types for each vehicle to determine if they are To Schedule] or Overdue]
    ***********************************************************************************************************************/
        BEGIN
     	DECLARE @iRecCountProcessed int
     	
     	DECLARE @iMaintStatusId_Overdue int
		DECLARE @iMaintStatusId_ToSchedule int
		DECLARE @iMaintStatusId_Scheduled int
		DECLARE @iMaintStatusId_InProgress int
		DECLARE @iMaintStatusId_Completed int
		DECLARE @iMaintStatusId_Valid int
		DECLARE @iMaintStatusId_Expired int
		
		DECLARE @URI int
		DECLARE @MaintURI int
		DECLARE @AssetId int
		DECLARE @MaintTypeId int
		DECLARE @OccurrenceType int
     	DECLARE @NextMaintDate DateTime
		DECLARE @NextMaintDateTG DateTime 
		DECLARE @CurrentOdometer int
		DECLARE @NextMaintOdometer int
		DECLARE @NextMaintOdometerTG int
		DECLARE @CurrentEngHrs int
		DECLARE @NextMaintEngHrs int
		DECLARE @NextMaintEngHrsTG int
		DECLARE @MaintStatusId int
		
		DECLARE @MaintURIUpdate int

		DECLARE @OccurrenceDuration int
		DECLARE @OccurrenceDurationTh int

		
		DECLARE @OccurrenceKM int
		DECLARE @OccurrenceKMTh int
		DECLARE @OccurrencePeriod_cbo nvarchar(50)

		SET @iMaintStatusId_Overdue = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Overdue')
		SET @iMaintStatusId_ToSchedule = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'To Schedule')
		SET @iMaintStatusId_Scheduled = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Scheduled')
		SET @iMaintStatusId_InProgress = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'In Progress')		
		SET @iMaintStatusId_Completed = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Completed')				
		SET @iMaintStatusId_Valid = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Valid')		
		SET @iMaintStatusId_Expired = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Expired')				

		update GFI_AMM_VehicleMaintTypesLink set CurrentOdometer = GetCalcOdo(AssetId)

		DECLARE csrData CURSOR FOR
		SELECT URI, 
		    vmtl.MaintURI,
		    vmtl.AssetId,
		    vmtl.MaintTypeId,
		    vmt.OccurrenceType,
			vmtl.NextMaintDate, 
			vmtl.NextMaintDateTG, 
			vmtl.CurrentOdometer, 
			vmtl.NextMaintOdometer, 
			vmtl.NextMaintOdometerTG, 
			vmtl.CurrentEngHrs,
			vmtl.NextMaintEngHrs,
			vmtl.NextMaintEngHrsTG,
			vmtl.[Status]
			, vmt.OccurrenceDuration
			, vmt.OccurrenceDurationTh
			, vmt.OccurrencePeriod_cbo
			, vmt.OccurrenceKM
			, vmt.OccurrenceKMTh 
		FROM GFI_AMM_VehicleMaintTypesLink vmtl
			INNER JOIN GFI_AMM_VehicleMaintTypes vmt ON vmtl.MaintTypeId = vmt.MaintTypeId
		
		OPEN csrData
		FETCH NEXT FROM csrData INTO @URI, @MaintURI, @AssetId, @MaintTypeId, @OccurrenceType, @NextMaintDate, @NextMaintDateTG, @CurrentOdometer, @NextMaintOdometer, @NextMaintOdometerTG, @CurrentEngHrs, @NextMaintEngHrs, @NextMaintEngHrsTG, @MaintStatusId
			, @OccurrenceDuration, @OccurrenceDurationTh, @OccurrencePeriod_cbo, @OccurrenceKM, @OccurrenceKMTh
		SET @iRecCountProcessed = 0
		
		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF @NextMaintDate IS NULL SET @NextMaintDate = GETDATE()
			     
			--select @URI, 'OccurrenceType 2', @OccurrenceType
			--	, 'AND'
			--	, '@NextMaintDateTG <= GETDATE()', @NextMaintDateTG, GETDATE()
			--	, '@NextMaintOdometerTG < @CurrentOdometer', @NextMaintOdometerTG, @CurrentOdometer
			--	, '@NextMaintEngHrsTG < @CurrentEngHrs', @NextMaintEngHrsTG, @CurrentEngHrs
			--	, 'AND'
			--	, '@MaintStatusId = @iMaintStatusId_Completed', @MaintStatusId, @iMaintStatusId_Completed
			--	, '@MaintStatusId = @iMaintStatusId_Valid', @MaintStatusId, @iMaintStatusId_Valid

			-- Threhold by date range
			declare @Maintdate DateTime
			declare @Thershold int
			set @Thershold = @OccurrenceDurationTh * -1 -- - @OccurrenceDuration
			set @Maintdate = DateAdd(month, @Thershold, GETDATE());
			if @OccurrencePeriod_cbo = 'DAY(S)' 
				set @Maintdate = DateAdd(day, @Thershold, GETDATE());

			select @Thershold, @NextMaintDate, @Maintdate
			--if(@Thershold != 0 and @NextMaintDate <= @Maintdate)
			--	select '*************************************************'

			-- Recurring - Flag As: To Schedule
			IF (
				(@OccurrenceType = 2) 
				AND ((@NextMaintDateTG <= GETDATE() and @OccurrenceDuration > 0) OR ( @CurrentOdometer >= (@NextMaintOdometer - (@OccurrenceKM - @OccurrenceKMTh)) ) OR (@NextMaintEngHrsTG < @CurrentEngHrs) OR (@Thershold != 0 and @NextMaintDate <= @Maintdate)) 
				AND ((@MaintStatusId = @iMaintStatusId_Completed) OR (@MaintStatusId = @iMaintStatusId_Valid)))
			BEGIN
				IF NOT EXISTS(SELECT * FROM GFI_AMM_VehicleMaintTypesLink WHERE AssetId = @AssetId AND MaintTypeId = @MaintTypeId AND URI > @URI)
				BEGIN
					IF (@MaintStatusId = @iMaintStatusId_Completed)
					BEGIN
						INSERT GFI_AMM_VehicleMaintenance (AssetId, MaintTypeId_cbo, MaintStatusId_cbo, StartDate, EndDate, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy) 
						VALUES (@AssetId, @MaintTypeId, @iMaintStatusId_ToSchedule, @NextMaintDate, @NextMaintDate, GETDATE(), CURRENT_USER, GETDATE(), CURRENT_USER)
					
						SET @MaintURIUpdate = (SELECT SCOPE_IDENTITY())
											
						UPDATE GFI_AMM_VehicleMaintTypesLink
						SET MaintURI = @MaintURIUpdate, Status = @iMaintStatusId_ToSchedule, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER, NextMaintOdometer = @OccurrenceKM + @CurrentOdometer
						WHERE URI = @URI
					END
					ELSE IF (@MaintStatusId = @iMaintStatusId_Valid)
					BEGIN
						SET @NextMaintDate = DATEADD(DD, 1, @NextMaintDate)
												
						INSERT GFI_AMM_VehicleMaintenance (AssetId, MaintTypeId_cbo, MaintStatusId_cbo, StartDate, EndDate, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy) 
						VALUES (@AssetId, @MaintTypeId, @iMaintStatusId_ToSchedule, @NextMaintDate, @NextMaintDate, GETDATE(), CURRENT_USER, GETDATE(), CURRENT_USER)
					
						SET @MaintURIUpdate = (SELECT SCOPE_IDENTITY())
												
						INSERT GFI_AMM_VehicleMaintTypesLink (MaintURI, AssetId, MaintTypeId, NextMaintDate, NextMaintDateTG, Status, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy)
						VALUES (@MaintURIUpdate, @AssetId, @MaintTypeId, @NextMaintDate, @NextMaintDate, @iMaintStatusId_ToSchedule, GETDATE(), CURRENT_USER, GETDATE(), CURRENT_USER)
					END
				END
			END

			--Flag As: Overdue
			IF (
					(
						(@NextMaintDate <= GETDATE() and @OccurrenceDuration > 0) 
							OR (@NextMaintOdometer < @CurrentOdometer) 
							OR (@NextMaintEngHrs < @CurrentEngHrs)
					) 
					AND (@MaintStatusId = @iMaintStatusId_ToSchedule)
				)
			BEGIN				
				UPDATE GFI_AMM_VehicleMaintTypesLink
				SET Status = @iMaintStatusId_Overdue, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
				WHERE URI = @URI
				
				UPDATE GFI_AMM_VehicleMaintenance 
				SET MaintStatusId_cbo = @iMaintStatusId_Overdue, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
				WHERE URI = @MaintURI
			END
			                        
			--Auto Expiry: Lapsed Insurance
			IF ((@MaintStatusId = @iMaintStatusId_Valid) AND (@NextMaintDate < GETDATE()))
			BEGIN
				UPDATE GFI_AMM_VehicleMaintTypesLink
				SET Status = @iMaintStatusId_Expired , UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
				WHERE URI = @URI
				                        
				UPDATE GFI_AMM_VehicleMaintenance 
				SET MaintStatusId_cbo = @iMaintStatusId_Expired, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
				WHERE URI = @MaintURI
			END
										
			SET @iRecCountProcessed = @iRecCountProcessed + 1
			
			FETCH NEXT FROM csrData INTO @URI, @MaintURI, @AssetId, @MaintTypeId, @OccurrenceType, @NextMaintDate, @NextMaintDateTG, @CurrentOdometer, @NextMaintOdometer, @NextMaintOdometerTG, @CurrentEngHrs, @NextMaintEngHrs, @NextMaintEngHrsTG, @MaintStatusId
				, @OccurrenceDuration, @OccurrenceDurationTh, @OccurrencePeriod_cbo, @OccurrenceKM, @OccurrenceKMTh
		END
		
		CLOSE csrData
		DEALLOCATE csrData
		
		RETURN @iRecCountProcessed
    END
END
";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "GetCalcEngHrs updated");
                }
                #endregion
                #region Update 170. SP Maintenance updated
                strUpd = "Rez000170";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[sp_AMM_VehicleMaint_ProcessRecords]') AND type in (N'P', N'PC'))
                            DROP PROCEDURE sp_AMM_VehicleMaint_ProcessRecords]";
                    dbExecute(sql);
                    //MySQL to create SP as per Update 19
                    sql = @"
CREATE PROCEDURE sp_AMM_VehicleMaint_ProcessRecords]
AS
BEGIN
    /***********************************************************************************************************************
    Version: 3.1.0.0
    Modifed: 11/ 08/2016.	06/08/2014
    Created By: Perry Ramen
    Modified By: Reza Dowlut
    Description: Process list of Maintenance Types for each vehicle to determine if they are To Schedule] or Overdue]
    ***********************************************************************************************************************/
        BEGIN
     	DECLARE @iRecCountProcessed int
     	
     	DECLARE @iMaintStatusId_Overdue int
		DECLARE @iMaintStatusId_ToSchedule int
		DECLARE @iMaintStatusId_Scheduled int
		DECLARE @iMaintStatusId_InProgress int
		DECLARE @iMaintStatusId_Completed int
		DECLARE @iMaintStatusId_Valid int
		DECLARE @iMaintStatusId_Expired int
		
		DECLARE @URI int
		DECLARE @MaintURI int
		DECLARE @AssetId int
		DECLARE @MaintTypeId int
		DECLARE @OccurrenceType int
     	DECLARE @NextMaintDate DateTime
		DECLARE @NextMaintDateTG DateTime 
		DECLARE @CurrentOdometer int
		DECLARE @NextMaintOdometer int
		DECLARE @NextMaintOdometerTG int
		DECLARE @CurrentEngHrs int
		DECLARE @NextMaintEngHrs int
		DECLARE @NextMaintEngHrsTG int
		DECLARE @MaintStatusId int
		
		DECLARE @MaintURIUpdate int

		DECLARE @OccurrenceDuration int
		DECLARE @OccurrenceDurationTh int

		
		DECLARE @OccurrenceKM int
		DECLARE @OccurrenceKMTh int
		DECLARE @OccurrencePeriod_cbo nvarchar(50)

		SET @iMaintStatusId_Overdue = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Overdue')
		SET @iMaintStatusId_ToSchedule = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'To Schedule')
		SET @iMaintStatusId_Scheduled = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Scheduled')
		SET @iMaintStatusId_InProgress = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'In Progress')		
		SET @iMaintStatusId_Completed = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Completed')				
		SET @iMaintStatusId_Valid = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Valid')		
		SET @iMaintStatusId_Expired = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Expired')				

		update GFI_AMM_VehicleMaintTypesLink set CurrentOdometer = GetCalcOdo(AssetId)

		DECLARE csrData CURSOR FOR
		SELECT URI, 
		    vmtl.MaintURI,
		    vmtl.AssetId,
		    vmtl.MaintTypeId,
		    vmt.OccurrenceType,
			vmtl.NextMaintDate, 
			vmtl.NextMaintDateTG, 
			vmtl.CurrentOdometer, 
			vmtl.NextMaintOdometer, 
			vmtl.NextMaintOdometerTG, 
			vmtl.CurrentEngHrs,
			vmtl.NextMaintEngHrs,
			vmtl.NextMaintEngHrsTG,
			vmtl.[Status]
			, vmt.OccurrenceDuration
			, vmt.OccurrenceDurationTh
			, vmt.OccurrencePeriod_cbo
			, vmt.OccurrenceKM
			, vmt.OccurrenceKMTh 
		FROM GFI_AMM_VehicleMaintTypesLink vmtl
			INNER JOIN GFI_AMM_VehicleMaintTypes vmt ON vmtl.MaintTypeId = vmt.MaintTypeId
		
		OPEN csrData
		FETCH NEXT FROM csrData INTO @URI, @MaintURI, @AssetId, @MaintTypeId, @OccurrenceType, @NextMaintDate, @NextMaintDateTG, @CurrentOdometer, @NextMaintOdometer, @NextMaintOdometerTG, @CurrentEngHrs, @NextMaintEngHrs, @NextMaintEngHrsTG, @MaintStatusId
			, @OccurrenceDuration, @OccurrenceDurationTh, @OccurrencePeriod_cbo, @OccurrenceKM, @OccurrenceKMTh
		SET @iRecCountProcessed = 0
		
		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF @NextMaintDate IS NULL SET @NextMaintDate = GETDATE()
			     
			-- Threhold by date range
			declare @Maintdate DateTime
			declare @Thershold int
			set @Thershold = @OccurrenceDurationTh * -1 -- - @OccurrenceDuration
			set @Maintdate = DateAdd(month, @Thershold, GETDATE());
			if @OccurrencePeriod_cbo = 'DAY(S)' 
				set @Maintdate = DateAdd(day, @Thershold, GETDATE());

			select @Thershold, @NextMaintDate, @Maintdate
			--if(@Thershold != 0 and @NextMaintDate <= @Maintdate)
			--	select '*************************************************'

			-- Recurring - Flag As: To Schedule
			IF (
				(@OccurrenceType = 2) 
				AND ((@NextMaintDateTG <= GETDATE() and @OccurrenceDuration > 0) OR ( @CurrentOdometer >= (@NextMaintOdometer - (@OccurrenceKM - @OccurrenceKMTh)) ) OR (@NextMaintEngHrsTG < @CurrentEngHrs) OR (@Thershold != 0 and @NextMaintDate <= @Maintdate)) 
				AND ((@MaintStatusId = @iMaintStatusId_Completed) OR (@MaintStatusId = @iMaintStatusId_Valid)))
			BEGIN
				IF NOT EXISTS(SELECT * FROM GFI_AMM_VehicleMaintTypesLink WHERE AssetId = @AssetId AND MaintTypeId = @MaintTypeId AND URI > @URI)
				BEGIN
					IF (@MaintStatusId = @iMaintStatusId_Completed)
					BEGIN
						INSERT GFI_AMM_VehicleMaintenance (AssetId, MaintTypeId_cbo, MaintStatusId_cbo, StartDate, EndDate, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy) 
						VALUES (@AssetId, @MaintTypeId, @iMaintStatusId_ToSchedule, @NextMaintDate, @NextMaintDate, GETDATE(), CURRENT_USER, GETDATE(), CURRENT_USER)
					
						SET @MaintURIUpdate = (SELECT SCOPE_IDENTITY())
								
						--select * from GFI_AMM_VehicleMaintTypes where MaintTypeId = @MaintTypeId and Description = 'ODOMETER/ENGINE HOURS ENTRY'
						--if(@@ROWCOUNT > 0)
						--begin		
							UPDATE GFI_AMM_VehicleMaintTypesLink
							SET MaintURI = @MaintURIUpdate, Status = @iMaintStatusId_ToSchedule, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER --,NextMaintOdometer = @OccurrenceKM + @CurrentOdometer
							WHERE URI = @URI
						--end
					END
					ELSE IF (@MaintStatusId = @iMaintStatusId_Valid)
					BEGIN
						SET @NextMaintDate = DATEADD(DD, 1, @NextMaintDate)
												
						INSERT GFI_AMM_VehicleMaintenance (AssetId, MaintTypeId_cbo, MaintStatusId_cbo, StartDate, EndDate, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy) 
						VALUES (@AssetId, @MaintTypeId, @iMaintStatusId_ToSchedule, @NextMaintDate, @NextMaintDate, GETDATE(), CURRENT_USER, GETDATE(), CURRENT_USER)
					
						SET @MaintURIUpdate = (SELECT SCOPE_IDENTITY())
												
						INSERT GFI_AMM_VehicleMaintTypesLink (MaintURI, AssetId, MaintTypeId, NextMaintDate, NextMaintDateTG, Status, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy)
						VALUES (@MaintURIUpdate, @AssetId, @MaintTypeId, @NextMaintDate, @NextMaintDate, @iMaintStatusId_ToSchedule, GETDATE(), CURRENT_USER, GETDATE(), CURRENT_USER)
					END
				END
			END

			--Flag As: Overdue
			IF (
					(
						(@NextMaintDate <= GETDATE() and @OccurrenceDuration > 0) 
							OR (@NextMaintOdometer < @CurrentOdometer) 
							OR (@NextMaintEngHrs < @CurrentEngHrs)
					) 
					AND (@MaintStatusId = @iMaintStatusId_ToSchedule)
				)
			BEGIN				
				UPDATE GFI_AMM_VehicleMaintTypesLink
				SET Status = @iMaintStatusId_Overdue, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
				WHERE URI = @URI
				
				UPDATE GFI_AMM_VehicleMaintenance 
				SET MaintStatusId_cbo = @iMaintStatusId_Overdue, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
				WHERE URI = @MaintURI
			END
			                        
			--Auto Expiry: Lapsed Insurance
			IF ((@MaintStatusId = @iMaintStatusId_Valid) AND (@NextMaintDate < GETDATE()))
			BEGIN
				UPDATE GFI_AMM_VehicleMaintTypesLink
				SET Status = @iMaintStatusId_Expired , UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
				WHERE URI = @URI
				                        
				UPDATE GFI_AMM_VehicleMaintenance 
				SET MaintStatusId_cbo = @iMaintStatusId_Expired, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
				WHERE URI = @MaintURI
			END
										
			SET @iRecCountProcessed = @iRecCountProcessed + 1
			
			FETCH NEXT FROM csrData INTO @URI, @MaintURI, @AssetId, @MaintTypeId, @OccurrenceType, @NextMaintDate, @NextMaintDateTG, @CurrentOdometer, @NextMaintOdometer, @NextMaintOdometerTG, @CurrentEngHrs, @NextMaintEngHrs, @NextMaintEngHrsTG, @MaintStatusId
				, @OccurrenceDuration, @OccurrenceDurationTh, @OccurrencePeriod_cbo, @OccurrenceKM, @OccurrenceKMTh
		END
		
		CLOSE csrData
		DEALLOCATE csrData
		
		RETURN @iRecCountProcessed
    END
END
";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "SP Maintenance updated");
                }
                #endregion

                #region Update 171. Indexes
                strUpd = "Rez000171";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"drop index IX_GFI_GPS_GPSData_ProcessTrips] on GFI_GPS_GPSData]";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSData", "IX_GFI_GPS_GPSData_ProcessTrips")).Rows.Count == 1)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_GPS_GPSData_ProcessTrips] ON GFI_GPS_GPSData] ([AssetID],[DateTimeGPS_UTC]) INCLUDE ([UID], DateTimeServer], Longitude], Latitude], LongLatValidFlag], Speed], EgineOn], StopFlag], TripDistance], TripTime], WorkHour], DriverID], RoadSpeed])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSData", "IX_GFI_GPS_GPSData_ProcessTrips")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "Indexes");
                }
                #endregion
                #region Update 172. SecurityToken
                strUpd = "Rez000172";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_SYS_SecurityToken]
                            (
	                              OneTimeToken] UniqueIdentifier NOT NULL
	                            , UID] int NOT NULL
                                , UserToken UniqueIdentifier NOT NULL
                                , ExpiryDate datetime not null default getdate() + 1
                                , TimeOut datetime not null default DateAdd(mi, 20, getdate())
                                , CONSTRAINT PK_GFI_SYS_SecurityToken] PRIMARY KEY  ([OneTimeToken] ASC)
                             )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_SecurityToken")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = "update GFI_FLT_Driver set Status = 'Active' where sDriverName = 'Default Driver' and iButton = '00000000' and sComments = 'Driver for vehicles with no Driver' and Status is null";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "SecurityToken");
                }
                #endregion
                #region Update 173. Assets no longer encrypted
                strUpd = "Rez000173";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"select * from GFI_FLT_Asset";
                    DataTable dtA = new Connection(myStrConn).GetDataDT(sql, myStrConn);
                    foreach (DataRow dr in dtA.Rows)
                    {
                        String AN = Services.BaseService.DecryptMe(dr["AssetName"].ToString());
                        String ANum = Services.BaseService.DecryptMe(dr["AssetNumber"].ToString());
                        if (!String.IsNullOrEmpty(AN))
                        {
                            sql = "update GFI_FLT_Asset set AssetName = '" + AN + "', AssetNumber = '" + ANum + "' where AssetID = " + dr["AssetID"].ToString();
                            dbExecute(sql);
                        }
                    }

                    InsertDBUpdate(strUpd, "Assets no longer encrypted");
                }
                #endregion
                #region Update 174. Asset Name n Number no longer encrypted
                strUpd = "Rez000174";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"select * from GFI_FLT_Asset";
                    DataTable dtA = new Connection(myStrConn).GetDataDT(sql, myStrConn);
                    foreach (DataRow dr in dtA.Rows)
                    {
                        String AN = Services.BaseService.DecryptMe(dr["AssetName"].ToString());
                        if (!String.IsNullOrEmpty(AN))
                        {
                            sql = "update GFI_FLT_Asset set AssetName = '" + AN + "' where AssetID = " + dr["AssetID"].ToString();
                            dbExecute(sql);
                        }

                        String ANum = Services.BaseService.DecryptMe(dr["AssetNumber"].ToString());
                        if (!String.IsNullOrEmpty(ANum))
                        {
                            sql = "update GFI_FLT_Asset set AssetNumber = '" + ANum + "' where AssetID = " + dr["AssetID"].ToString();
                            dbExecute(sql);
                        }
                    }

                    InsertDBUpdate(strUpd, "Asset Name n Number no longer encrypted");
                }
                #endregion
                #region Update 175. GlobalParams Company Parameters and TimeZones
                strUpd = "Rez000175";
                if (!CheckUpdateExist(strUpd))
                {
                    GlobalParams g = new GlobalParams();
                    g = g.GetGlobalParamsByName("CompanyParam", sConnStr);
                    if (g == null)
                    {
                        g = new GlobalParams();
                        g.ParamName = "CompanyParam";
                        g.PValue = "none";
                        g.ParamComments = "Company Parameters per database. PValue is folder name";
                        g.Save(g, true, sConnStr);
                    }

                    sql = @"Alter TABLE GFI_SYS_TimeZones] add TotalMinutes float NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_TimeZones", "TotalMinutes")).Rows.Count == 0)
                        dbExecute(sql);

                    dt = GetDataDT("select * from GFI_SYS_TimeZones");
                    foreach (DataRow dr in dt.Rows)
                    {
                        String st = dr["StandardName"].ToString();
                        foreach (TimeZoneInfo z in TimeZoneInfo.GetSystemTimeZones())
                            if (z.StandardName == st)
                            {
                                sql = "update GFI_SYS_TimeZones set TotalMinutes = " + z.BaseUtcOffset.TotalMinutes + " where StandardName = '" + z.StandardName + "'";
                                dbExecute(sql);
                                break;
                            }
                    }

                    sql = "CREATE  INDEX IX_Del_GPS_Exceptions ON GFI_GPS_Exceptions] ([AssetID]) INCLUDE ([iID])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_Exceptions", "IX_Del_GPS_Exceptions")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE  INDEX IX_Del_ARC_Exceptions ON GFI_ARC_Exceptions] ([AssetID]) INCLUDE ([iID])";
                    if (GetDataDT(ExistsIndex("GFI_ARC_Exceptions", "IX_Del_ARC_Exceptions")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE  INDEX IX_Del_Notif ON GFI_SYS_Notification] ([AssetID]) INCLUDE ([NID])";
                    if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "IX_Del_Notif")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "GlobalParams Company Parameters and Timezones");
                }
                #endregion
                #region Update 176. Updated user Support
                strUpd = "Rez000176";
                if (!CheckUpdateExist(strUpd))
                {
                    User u = new User();
                    u = u.GetUserByeMail("Support@naveo.mu", User.EditModePswd, sConnStr, false, true);
                    u.Password = "SupportMe@2018.";
                    u.Update(u, sConnStr);

                    InsertDBUpdate(strUpd, "Updated user Support");
                }
                #endregion
                #endregion

                #region Update 177. Fuel Process
                strUpd = "Rez000177";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_GPS_FuelProcessed](
	                        iID] int] AUTO_INCREMENT NOT NULL,
	                        AssetID] int] NOT NULL,
	                        DateTimeGPS_UTC] DateTime] NOT NULL Default DATEADD(DAY, -31, GETDATE()),
                         CONSTRAINT PK_FuelProcessed] PRIMARY KEY  
	                        ([iID] ASC)
                        )";
                    if (GetDataDT(ExistTableSql("GFI_GPS_FuelProcessed")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_GPS_FuelProcessed] WITH CHECK
	                        ADD CONSTRAINT FK_GFI_GPS_FuelProcessed_GFI_FLT_Asset] FOREIGN KEY([AssetID])
                        REFERENCES GFI_FLT_Asset] ([AssetID])";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_FuelProcessed_GFI_FLT_Asset")).Rows.Count == 0)
                        dbExecute(sql);

                    //InsertDBUpdate(strUpd, "Fuel Process");
                }
                #endregion

                #region Update 127.
                strUpd = "Rez000127";
                if (!CheckUpdateExist(strUpd))
                {
                    //InsertDBUpdate(strUpd, "Vehicle");
                }
                #endregion

                usrInst = null;
            }
            catch (Exception ex)
            {
                throw new Exception(strUpd + "  " + sql + " ] " + ex.ToString());
            }
            finally
            {
                Globals.uLogin = null;
            }
        }

        private DataSet GetTablesFromColumn(String pStrColumn)
        {
            return (new Connection(myStrConn).GetDS("select object_name(id) as Table_Name from syscolumns where name  = '" + pStrColumn + "' and OBJECTPROPERTY(id, N'IsUserTable') = 1", myStrConn));
        }
        private String ExistTableSql(String pStrTable)
        {
            return ("SELECT * FROM information_schema.tables WHERE table_name = '" + pStrTable.ToLower() + "' LIMIT 1");
        }
        private String ExistViewSql(String pStrView)
        {
            return ("select count(*) from sysobjects where id = object_id(N'" + pStrView + "') and OBJECTPROPERTY(id, N'IsView') = 1");
        }
        private String ExistColumnSql(String pStrTable, String pStrColumn)
        {
            //SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = 'db_name' AND TABLE_NAME = 'table_name' AND COLUMN_NAME = 'column_name'
            return ("SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME = '" + pStrTable.ToLower() + "' AND COLUMN_NAME = '" + pStrColumn + "'");
        }
        private String ExistsSQLConstraint(String strConstraintName, String pStrTable="")
        {
            if (strConstraintName.Contains("FK_"))
            {
                return
                @"SELECT * FROM information_schema.TABLE_CONSTRAINTS 
                    WHERE CONSTRAINT_SCHEMA = DATABASE()
                        AND TABLE_NAME = '" + pStrTable + @"'
                        AND CONSTRAINT_NAME = '" + strConstraintName + @"'
                        AND CONSTRAINT_TYPE = 'FOREIGN KEY'";
            }
            return "SHOW KEYS FROM " + pStrTable + " WHERE Key_name = '" + strConstraintName + "'";
        }
        private String ExistsIndex(String pStrTable, String pStrIndex)
        {
            return "show index from " + pStrTable + " where Key_name = '" + pStrIndex + "'";
        }
        private String ExistsFunction(String sFunction)
        {
            return @"SELECT * 
FROM INFORMATION_SCHEMA.ROUTINES
WHERE
    ROUTINE_TYPE = 'FUNCTION'
    AND ROUTINE_NAME = '" + sFunction + @"'
   AND ROUTINE_SCHEMA = '" + strDB + "'";
        }
        private String ExistsSP(String sSP)
        {
            return @"SELECT * 
FROM INFORMATION_SCHEMA.ROUTINES
WHERE
    ROUTINE_TYPE = 'PROCEDURE'
    AND ROUTINE_NAME = '" + sSP + @"'
   AND ROUTINE_SCHEMA = '" + strDB + "'";
        }
        private String GetFieldSchema(String pStrTable, String pStrColumn)
        {
            return "SELECT * FROM INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = '" + pStrTable + "' AND COLUMN_NAME = '" + pStrColumn + "'";
        }
        private String RenameColumn(String sOld, String sNew, String sTable)
        {
            return "EXEC SP_RENAME '" + sTable + "." + sOld + "', '" + sNew + "', 'COLUMN'";
        }
        private Boolean CheckUpdateExist(String UpdateId)
        {
            Boolean bOk = false;
            if (new Connection(myStrConn).GetNumberOfRecords("GFI_SYS_INIT", "*", " upd_id = '" + UpdateId + "'", myStrConn) > 0)
                bOk = true;
            return bOk;
        }
        private Boolean CheckParamExist(String pCode, String sConnStr)
        {
            Boolean bOk = false;
            if (new Connection(myStrConn).GetNumberOfRecords("db_param", "*", " code = '" + pCode + "'", myStrConn) > 0)
                bOk = true;
            return bOk;
        }
        private Boolean ExistTrigger(String strTriggerName)
        {
            Boolean bOk = false;
            if (new Connection(myStrConn).GetNumberOfRecords("sysobjects", "*", " name = '" + strTriggerName + "' and OBJECTPROPERTY(id, 'IsTrigger') = 1", myStrConn) > 0)
                bOk = true;
            return bOk;
        }
        private Boolean hasIdentity(String sTableName)
        {
            String sql = "SELECT * FROM sys.identity_columns WHERE OBJECT_NAME(object_id) = '" + sTableName + "'";

            DataTable dt = new Connection(myStrConn).GetDataDT(sql, myStrConn);
            if (dt.Rows.Count > 0)
                return true;
            return false;
        }

        private DataTable dtGetFK(String sTableName)
        {
            String sql = @"
SELECT
  object_name(parent_object_id) tbName,
  object_name(referenced_object_id) fkTbName,
  name fkName
FROM sys.foreign_keys
WHERE parent_object_id = object_id('" + sTableName + "')";

            DataTable dt = new Connection(myStrConn).GetDataDT(sql, myStrConn);
            return dt;
        }
        private void InsertDBUpdate(String pUpd_Id, String pUpd_Desc)
        {
            new Connection(myStrConn).dbExecute("Insert into GFI_SYS_INIT values( '" + pUpd_Id + "', '" + pUpd_Desc + "', Now())", myStrConn);
        }
        private void SQLCompaibilityLvl2014()
        {
            String sq = @"
USE master
GO
ALTER DATABASE CoreDataDupl
SET COMPATIBILITY_LEVEL = 120;
GO";
        }
        private String GetAllKeys(String pStrTable)
        {
            return "EXEC sp_fkeys '" + pStrTable + "'";
        }
        private void WebConfig()
        {
            //<!-- To put in webconfig to allows creation on asp user --!>
            //    <contexts>
            //      <context type="NaveoOneWeb.Models.ApplicationDbContext, NaveoOneWeb">
            //        <databaseInitializer type="System.Data.Entity.MigrateDatabaseToLatestVersion`2[[NaveoOneWeb.Models.ApplicationDbContext, NaveoOneWeb], NaveoOneWeb.Migrations.Configuration, NaveoOneWeb]], EntityFramework, PublicKeyToken=b77a5c561934e089">
            //          <parameters>
            //            <parameter value="DefaultConnection_DatabasePublish" />
            //          </parameters>
            //        </databaseInitializer>
            //      </context>
            //    </contexts>
        }
    }
}