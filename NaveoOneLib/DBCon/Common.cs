﻿using NaveoOneLib.Common;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Services.Permissions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoOneLib.DBCon
{
    public class Common
    {
        public void AddPermissions(String sConnStr)
        {
            DataTable dtRole = new RolesService().GetRolesByRoleName("### Default_Role ###", sConnStr);

            int roleID = dtRole.Rows[0].Field<int>(0);
            string description = dtRole.Rows[0].Field<string>(1);
            if (dtRole.Rows.Count != 0)
            {
                Role uRoles = new Role();
                uRoles.RoleID = roleID;
                uRoles.Description = description;
                uRoles.oprType = DataRowState.Unchanged;

                #region Matrix
                DataTable dtMatrix = new RolesService().GetRoleMatrix(roleID, sConnStr);

                Matrix m = new Matrix();
                m.MID = dtMatrix.Rows[0].Field<int>(0);
                m.GMID = dtMatrix.Rows[0].Field<int>(1);
                m.oprType = DataRowState.Unchanged;
                m.iID = dtMatrix.Rows[0].Field<int>(2);
                uRoles.lMatrix = new List<Matrix>();
                uRoles.lMatrix.Add(m);
                #endregion

                #region RolePermissions
                RolesService rs = new RolesService();
                PermissionsService ps = new PermissionsService();

                uRoles.lPermissions = new List<RolePermission>();
                DataTable dtPermissions = ps.GetPermission(sConnStr);
                DataTable dtPermissionId = new RolesService().GetPermissionIdByRoleId(roleID, sConnStr);

                foreach (DataRow x in dtPermissions.Rows)
                {
                    int PermissionId = (int)x["iID"];
                    DataRow[] results = dtPermissionId.Select("PermissionID ='" + PermissionId + "'");
                    if (results.Length == 0)
                    {
                        RolePermission rp = new RolePermission();
                        rp.RoleID = roleID;
                        rp.PermissionID = PermissionId;
                        rp.iCreate = 1;
                        rp.iDelete = 1;
                        rp.iRead = 1;
                        rp.iUpdate = 1;
                        rp.Report1 = 1;
                        rp.Report2 = 1;
                        rp.Report3 = 1;
                        rp.Report4 = 1;
                        rp.oprType = DataRowState.Added;

                        uRoles.lPermissions.Add(rp);
                    }
                }
                #endregion

                rs.SaveRoles(uRoles, false, sConnStr);
            }
        }

        public void AssignPermissionsToNaveoModules(String sConnStr)
        {
            Permission p = new Permission();
            PermissionsService ps = new PermissionsService();

            NaveoModule n = new NaveoModule();
            NaveoModulesService ns = new NaveoModulesService();

            N1Controller n1Controller = new N1Controller();
            var listControllerName = n1Controller.listControllerName();
            string controllerName = string.Empty;
            string moduleName = string.Empty;
            foreach (var x in listControllerName)
            {
                switch (x)
                {
                    case "Dashboard":
                        controllerName = x;
                        moduleName = "Admin";
                        break;

                    case "Asset":
                        controllerName = x;
                        moduleName = "Admin";
                        break;

                    case "Exception":
                        controllerName = x;
                        moduleName = "Fleet";
                        break;

                    case "Fuel":
                        controllerName = x;
                        moduleName = "Fleet";
                        break;

                    case "Live":
                        controllerName = x;
                        moduleName = "GIS";
                        break;

                    case "Trip":
                        controllerName = x;
                        moduleName = "GIS";
                        break;

                    case "Rules":
                        controllerName = x;
                        moduleName = "Fleet";
                        break;

                    case "Zones":
                        controllerName = x;
                        moduleName = "GIS";
                        break;

                    case "Resource":
                        controllerName = x;
                        moduleName = "Admin";
                        break;

                    case "Driver":
                        controllerName = x;
                        moduleName = "Admin";
                        break;

                    case "Temperature":
                        controllerName = x;
                        moduleName = "Fleet";
                        break;

                    case "Maintenance":
                        controllerName = x;
                        moduleName = "Fleet";
                        break;

                    case "Role":
                        controllerName = x;
                        moduleName = "Admin";
                        break;

                    case "Planning":
                        controllerName = x;
                        moduleName = "Fleet";
                        break;

                    case "Schedule":
                        controllerName = x;
                        moduleName = "Fleet";
                        break;

                    case "Roster":
                        controllerName = x;
                        moduleName = "Fleet";
                        break;

                    case "User":
                        controllerName = x;
                        moduleName = "Admin";
                        break;

                    case "GPSData":
                        controllerName = x;
                        moduleName = "Fleet";
                        break;

                    case "Audit":
                        controllerName = x;
                        moduleName = "Admin";
                        break;

                    case "GroupMatrix":
                        controllerName = x;
                        moduleName = "Admin";
                        break;

                    case "Blup":
                        controllerName = x;
                        moduleName = "GIS";
                        break;

                    case "FixedAsset":
                        controllerName = x;
                        moduleName = "GIS";
                        break;

                    case "TerraPlanning":
                        controllerName = x;
                        moduleName = "TERRA";
                        break;


                    case "CustomizedTrips":
                        controllerName = x;
                        moduleName = "Fleet";
                        break;

                    case "MOLGExtendedTrips":
                        controllerName = x;
                        moduleName = "MOLG";
                        break;

                    case "Monitoring":
                        controllerName = x;
                        moduleName = "Admin";
                        break;

                    case "AccidentManagement":
                        controllerName = x;
                        moduleName = "Fleet";
                        break;


   
                    case "Shift":
                        controllerName = x;
                        moduleName = "Fleet";
                        break;


                    case "DriverScoreCards":
                        controllerName = x;
                        moduleName = "Fleet";
                        break;



                    case "GlobalParams":
                        controllerName = x;
                        moduleName = "Admin";
                        break;


                    case "Error":
                        controllerName = x;
                        moduleName = "Admin";
                        break;


                    case "ZoneType":
                        controllerName = x;
                        moduleName = "Fleet";
                        break;


                    case "DriverVehicle":
                        controllerName = x;
                        moduleName = "Fleet";
                        break;


                    case "LookupTypeValues":
                        controllerName = x;
                        moduleName = "Admin";
                        break;



                }

                p = ps.GetPermisionsByControllerName(controllerName, sConnStr);
                if (p == null)
                {
                    n = ns.GetNaveoModulesByName(moduleName, sConnStr);
                    p = new Permission();
                    p.ControllerName = controllerName;
                    p.ModuleID = n.iID;
                    p.oprType = DataRowState.Added;
                    ps.SavePermissions(p, true, sConnStr);
                }
            }

            NaveoModulesService naveoModulesService = new NaveoModulesService();
            NaveoModule n1 = naveoModulesService.GetNaveoModulesByName("Fleet", sConnStr);
            if (n1 != null)
            {
                if (n1.lMatrix.Count == 0)
                {
                    GroupMatrix gmRoot = new GroupMatrix();
                    gmRoot = new GroupMatrixService().GetRoot(sConnStr);

                    Matrix m = new Matrix();
                    m.GMID = gmRoot.gmID;
                    m.oprType = DataRowState.Added;
                    m.iID = n1.iID;
                    n1.lMatrix.Add(m);

                    n1.oprType = DataRowState.Modified;
                    naveoModulesService.SaveNaveoModules(n1, false, sConnStr);
                }
            }

            n1 = naveoModulesService.GetNaveoModulesByName("General", sConnStr);
            if (n1 != null)
            {
                if (n1.lMatrix.Count == 0)
                {
                    GroupMatrix gmRoot = new GroupMatrix();
                    gmRoot = new GroupMatrixService().GetRoot(sConnStr);

                    Matrix m = new Matrix();
                    m.GMID = gmRoot.gmID;
                    m.oprType = DataRowState.Added;
                    m.iID = n1.iID;
                    n1.lMatrix.Add(m);

                    n1.oprType = DataRowState.Modified;
                    naveoModulesService.SaveNaveoModules(n1, false, sConnStr);
                }
            }

            n1 = naveoModulesService.GetNaveoModulesByName("Maintenance", sConnStr);
            if (n1 != null)
            {
                if (n1.lMatrix.Count == 0)
                {
                    GroupMatrix gmRoot = new GroupMatrix();
                    gmRoot = new GroupMatrixService().GetRoot(sConnStr);

                    Matrix m = new Matrix();
                    m.GMID = gmRoot.gmID;
                    m.oprType = DataRowState.Added;
                    m.iID = n1.iID;
                    n1.lMatrix.Add(m);

                    n1.oprType = DataRowState.Modified;
                    naveoModulesService.SaveNaveoModules(n1, false, sConnStr);
                }
            }
        }

        public void NaveoModules(String sConnStr)
        {
            NaveoModulesService naveoModulesService = new NaveoModulesService();
            NaveoModule n = naveoModulesService.GetNaveoModulesByName("TERRA", sConnStr);

            //n = naveoModulesService.GetNaveoModulesByName("T", sConnStr);
            if (n == null)
            {
                n = new NaveoModule();
                n.description = "TERRA";
                n.lMatrix = new List<Matrix>();
                n.lPermissions = new List<Permission>();

                GroupMatrix gmRoot = new GroupMatrix();
                gmRoot = new GroupMatrixService().GetRoot(sConnStr);

                Matrix m = new Matrix();
                m.GMID = gmRoot.gmID;
                m.oprType = DataRowState.Added;
                n.lMatrix.Add(m);

                //Permission p = new Permission();
                //p.ControllerName = "Map";
                //p.oprType = DataRowState.Added;
                //n.lPermissions.Add(p);

                naveoModulesService.SaveNaveoModules(n, true, sConnStr);
            }

            n = naveoModulesService.GetNaveoModulesByName("CWA", sConnStr);
            if (n == null)
            {
                n = new NaveoModule();
                n.description = "CWA";
                n.lMatrix = new List<Matrix>();
                n.lPermissions = new List<Permission>();

                GroupMatrix gmRoot = new GroupMatrix();
                gmRoot = new GroupMatrixService().GetRoot(sConnStr);

                Matrix m = new Matrix();
                m.GMID = gmRoot.gmID;
                m.oprType = DataRowState.Added;
                n.lMatrix.Add(m);

                //Permission p = new Permission();
                //p.ControllerName = "Map";
                //p.oprType = DataRowState.Added;
                //n.lPermissions.Add(p);

                naveoModulesService.SaveNaveoModules(n, true, sConnStr);
            }


            n = naveoModulesService.GetNaveoModulesByName("MOLG", sConnStr);
            if (n == null)
            {
                n = new NaveoModule();
                n.description = "MOLG";
                n.lMatrix = new List<Matrix>();
                n.lPermissions = new List<Permission>();

                GroupMatrix gmRoot = new GroupMatrix();
                gmRoot = new GroupMatrixService().GetRoot(sConnStr);

                Matrix m = new Matrix();
                m.GMID = gmRoot.gmID;
                m.oprType = DataRowState.Added;
                n.lMatrix.Add(m);

                //Permission p = new Permission();
                //p.ControllerName = "Map";
                //p.oprType = DataRowState.Added;
                //n.lPermissions.Add(p);

                naveoModulesService.SaveNaveoModules(n, true, sConnStr);
            }
        }


        #region Alert on planning enum
       public enum AlertPlanningConstant
        {
            EarlyArrival = 0,
            EarlyDeparture = 1,
            LateArrival = 2,
            LateDeparture = 3,
            OnTime = 4,
            NotVisited = 5,
       
        }

        #endregion

        public static string GetAPIConnStr(String SvrName)
        {
            String sConnStr = String.Empty;
            NaveoOneLib.Models.Users.User.NaveoDatabases myEnum;
            if (Enum.TryParse(SvrName, true, out myEnum))
            {
                switch (myEnum)
                {
                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Demo:
                        sConnStr = ConfigurationKeys.CoreConnStr;
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Mercury:
                        sConnStr = ConfigurationKeys.CoreConnStrDB2;
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Venus:
                        sConnStr = "DB3";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Earth:
                        sConnStr = "DB4";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Mars:
                        sConnStr = "DB5";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Jupiter:
                        sConnStr = "DB6";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Saturn:
                        sConnStr = "DB7";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Uranus:
                        sConnStr = "DB8";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Neptune:
                        sConnStr = "DB9";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Pluto:
                        sConnStr = "DB10";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Baby:
                        sConnStr = "DB11";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Kids:
                        sConnStr = "DB12";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Sun:
                        sConnStr = "DB13";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Moon:
                        sConnStr = "DB14";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.BlackWidow:
                        sConnStr = "DB15";
                        break;
                }
            }
            if (sConnStr == String.Empty)
                sConnStr = ConfigurationKeys.CoreConnStrDB99;

            return sConnStr;
        }

        public static class ConfigurationKeys
        {
            public const string TicketTimeout = "TicketTimeout";
            public const string CoreConnStr = "DB1";
            public const string CoreConnStrDB2 = "DB2";
            public const string CoreConnStrDB3 = "DB3";
            public const string CoreConnStrDB4 = "DB4";
            public const string CoreConnStrDB5 = "DB5";
            public const string CoreConnStrDB6 = "DB6";
            public const string CoreConnStrDB7 = "DB7";
            public const string CoreConnStrDB8 = "DB8";
            public const string CoreConnStrDB9 = "DB9";
            public const string CoreConnStrDB10 = "DB10";
            public const string CoreConnStrDB11 = "DB11";
            public const string CoreConnStrDB12 = "DB12";
            public const string CoreConnStrDB13 = "DB13";
            public const string CoreConnStrDB14 = "DB14";
            public const string CoreConnStrDB15 = "DB15";
            public const string CoreConnStrDB16 = "DB16";
            public const string CoreConnStrDB17 = "DB17";
            public const string CoreConnStrDB18 = "DB18";
            public const string CoreConnStrDB19 = "DB19";
            public const string CoreConnStrDB20 = "DB20";
            public const string CoreConnStrDB21 = "DB21";
            public const string CoreConnStrDB22 = "DB22";
            public const string CoreConnStrDB23 = "DB23";
            public const string CoreConnStrDB24 = "DB24";
            public const string CoreConnStrDB25 = "DB25";
            public const string CoreConnStrDB26 = "DB26";
            public const string CoreConnStrDB27 = "DB27";
            public const string CoreConnStrDB28 = "DB28";
            public const string CoreConnStrDB29 = "DB29";
            public const string CoreConnStrDB30 = "DB30";
            public const string CoreConnStrDB99 = "DB99";
            public const string ExportLocation = "ExportLocation";
            public const string ImportLocation = "ImportLocation";

            public static int TimeOut = 60;
        }

    }
}
