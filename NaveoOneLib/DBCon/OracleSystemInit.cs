﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using NaveoOneLib.Common;
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.Drivers;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Users;
using NaveoOneLib.Models.Trips;
using NaveoOneLib.Models.Shifts;
using NaveoOneLib.Models.Zones;
using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.Lookups;
using NaveoOneLib.Models;
using NaveoOneLib.Services.GMatrix;
using DBConn.Common;
using NaveoOneLib.Services.Assets;
using NaveoOneLib.Services.Zones;
using NaveoOneLib.Services.Drivers;
using NaveoOneLib.Services.Lookups;
using NaveoOneLib.Services.Permissions;
using NaveoOneLib.Services.Shifts;
using NaveoOneLib.Services.Trips;
using NaveoOneLib.Services.Users;
using NaveoOneLib.Services;
using System.Text.RegularExpressions;
using NaveoOneLib.Services.Rules;
using System.Data.Common;

namespace NaveoOneLib.DBCon
{
    public class OracleSystemInit
    {
        Connection _SqlConn;
        String myStrConn = String.Empty;
        String strDB = String.Empty;
        VehicleMaintStatusService vehicleMaintStatusService = new VehicleMaintStatusService();
        VehicleMaintTypeService vehicleMaintTypeService = new VehicleMaintTypeService();
        VehicleMaintCatService vehicleMaintCatService = new VehicleMaintCatService();
        UOMService uomService = new UOMService();
        LookUpValuesService lookUpValuesService = new LookUpValuesService();
        GlobalParamsService globalParamsService = new GlobalParamsService();
        ModuleService moduleService = new ModuleService();

        int dbExecute(String sql)
        {
            sql = sql.Replace("[dbo].", "");
            sql = sql.Replace("]", "");
            sql = sql.Replace("[", "");
            sql = sql.Replace("varchar", "varchar2");
            sql = sql.Replace("nvarchar", "varchar2");
            sql = sql.Replace("varchar22", "varchar2");
            return _SqlConn.dbExecute(sql, myStrConn);
        }
        int dbExecute(String sql, DbTransaction tran, int i)
        {
            return _SqlConn.dbExecute(sql, tran, i, myStrConn);
        }
        int dbExecuteWithoutTransaction(String sql, int i)
        {
            sql = sql.Replace("CREATE NONCLUSTERED INDEX", "CREATE INDEX");
            sql = sql.Replace(") INCLUDE (", ", ");
            _SqlConn.OracleScriptExecute(sql, myStrConn);
            return 1;
        }
        DataTable GetDataDT(String sql)
        {
            return _SqlConn.GetDataDT(sql, myStrConn);
        }
        DataSet GetDataDS(String sql, int TimeOut, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            dtSql.Rows.Add(drSql);

            return _SqlConn.GetDataDS(dtSql, myStrConn, TimeOut);
        }

        public void UpdateDB(String sConnStr)
        {
            Boolean bIsConnected = DBConn.DbCon.OracleConn.CheckConn(sConnStr);

            myStrConn = sConnStr;
            strDB = ConnName.GetOracleDBName(sConnStr);

            //DBConn.DbCon.OracleConn.CreateDatabaseIfNotExist(sConnStr);

            String sql = String.Empty;
            String strUpd = String.Empty;
            DataTable dt = new DataTable();
            _SqlConn = new Connection(sConnStr);

            if (GetDataDT(ExistTableSql("GFI_SYS_INIT")).Rows[0][0].ToString() == "0")
                dbExecute("create table GFI_SYS_INIT(upd_id varchar2(10) not null, upd_desc varchar2(300) not null, upd_date timestamp(3) not null)");
            try
            {
                //Addinional fields for users should be in update 7 as well
                //Addinional fields for Driver should be update 127 as well
                #region init
                #region Update 94. Init Clean Ids
                strUpd = "Rez000094";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"update GFI_SYS_INIT set upd_id = 'Rez000001' where upd_id = 'DIN0000001'";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "Init Clean Ids");
                }
                #endregion

                #region Update 1 Audit Table
                strUpd = "Rez000001";
                if (!CheckUpdateExist(strUpd))
                {
                    if (GetDataDT(ExistTableSql("SYS_AUDIT")).Rows[0][0].ToString() == "0")
                    {
                        sql = @"
						 CREATE TABLE SYS_Audit
							(
								  AuditId int NOT NULL 
								, AuditTypes NVARCHAR2(6) NOT NULL
								, ReferenceCode NVARCHAR2(20) NOT NULL
								, ReferenceDesc NVARCHAR2(30) NULL
								, CreatedDate TIMESTAMP(3) NULL
								, AuditUser NVARCHAR2(20) NULL
								, ReferenceTable NVARCHAR2(50) NULL
								, CallerFunction NVARCHAR2(50) NULL
								, SQLRemarks NVARCHAR2(1000) NULL
								, CONSTRAINT PK_SYS_Audit PRIMARY KEY (AuditId)
							)
							";
                        CreateTable(sql, "SYS_Audit", "AuditId");
                    }
                    InsertDBUpdate(strUpd, "Audit Table");
                }
                #endregion
                #region Update 2. GFI_SYS_LOOKUP
                strUpd = "Rez000002";
                if (!CheckUpdateExist(strUpd))
                {
                    if (GetDataDT(ExistTableSql("GFI_SYS_LookUp")).Rows[0][0].ToString() == "0")
                    {
                        sql = @"
							begin
							   execute immediate
									'CREATE TABLE GFI_SYS_LOOKUP
									(
										LookUpId int NOT NULL,
										LookUpCode nvarchar2(20) NOT NULL,
										LookUpDesc nvarchar2(50) NOT NULL,
										LookUpParentID int NOT NULL,
										CreatedBy nvarchar2(20) NOT NULL,
										CreatedDate timestamp(3) NOT NULL,
										UpdatedBy nvarchar2(20) NULL,
										UpdatedDate timestamp(3) NULL,
										CONSTRAINT PK_GFI_SYS_LookUp PRIMARY KEY (LookUpId )  
									)';
						end;";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                        //Generate ID using sequence and trigger
                        sql = "CREATE SEQUENCE GFI_SYS_LookUp_seq START WITH 1 INCREMENT BY 1";
                        if (GetDataDT(ExistSequenceSql("GFI_SYS_LookUp_seq")).Rows[0][0].ToString() == "0")
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                        sql = @"
						CREATE OR REPLACE TRIGGER GFI_SYS_LookUp_seq_tr
						BEFORE INSERT ON GFI_SYS_LookUp FOR EACH ROW
						WHEN(NEW.LookUpId IS NULL)
						BEGIN
						SELECT GFI_SYS_LookUp_seq.NEXTVAL INTO :NEW.LookUpId FROM DUAL;
						END;";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    }
                    InsertDBUpdate(strUpd, "GFI_SYS_LookUp");
                }
                #endregion
                #region Update 3. Matrix
                strUpd = "Rez000003";
                if (!CheckUpdateExist(strUpd))
                {
                    #region Matrix
                    if (GetDataDT(ExistTableSql("GFI_SYS_MATRIX")).Rows[0][0].ToString() == "0")
                    {
                        sql = @"
								begin
									execute immediate
										'CREATE TABLE GFI_SYS_MATRIX
										(
											  MID int NOT NULL
											, GMID int NOT NULL
											, SourceTable NVARCHAR2(50) NULL
											, SourceID NVARCHAR2(20) NULL
											, SourceIDField NVARCHAR2(30) NULL
											, CONSTRAINT PK_GFI_SYS_Matrix PRIMARY KEY(MID)
										)';
								end;";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                        //Generate ID using sequence and trigger
                        sql = @"CREATE SEQUENCE GFI_SYS_Matrix_seq START WITH 1 INCREMENT BY 1";
                        if (GetDataDT(ExistSequenceSql("GFI_SYS_Matrix_seq")).Rows[0][0].ToString() == "0")
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                        sql = @"CREATE OR REPLACE TRIGGER GFI_SYS_Matrix_seq_tr
								BEFORE INSERT ON GFI_SYS_Matrix FOR EACH ROW
								WHEN(NEW.MID IS NULL)
								BEGIN
								SELECT GFI_SYS_Matrix_seq.NEXTVAL INTO :NEW.MID FROM DUAL;
								END;";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    }
                    #endregion

                    #region GroupMatrix
                    if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrix")).Rows[0][0].ToString() == "0")
                    {
                        sql = @"
						begin
							execute immediate
								'CREATE TABLE GFI_SYS_GroupMatrix
								(
									  GMID int NOT NULL
									, GMDescription NVARCHAR2(100) NULL
									, ParentGMID NUMBER(10) NULL
									, Remarks NCHAR(100) NULL
									, CONSTRAINT PK_GFI_SYS_GroupMatrix PRIMARY KEY(GMID)
								)';
						end;";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                        //Generate ID using sequence and trigger
                        sql = @"CREATE SEQUENCE GFI_SYS_GroupMatrix_seq START WITH 1 INCREMENT BY 1";
                        if (GetDataDT(ExistSequenceSql("GFI_SYS_GroupMatrix_seq")).Rows[0][0].ToString() == "0")
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                        sql = @"
						CREATE OR REPLACE TRIGGER GFI_SYS_GroupMatrix_seq_tr
						BEFORE INSERT ON GFI_SYS_GroupMatrix FOR EACH ROW
						WHEN(NEW.GMID IS NULL)
						BEGIN
						SELECT GFI_SYS_GroupMatrix_seq.NEXTVAL INTO :NEW.GMID FROM DUAL;
						END;";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    }
                    #endregion

                    sql = "ALTER TABLE GFI_SYS_GroupMatrix ADD IsCompany int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_GroupMatrix", "IsCompany")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_SYS_GroupMatrix ADD CompanyCode nvarchar2(15) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_GroupMatrix", "CompanyCode")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_SYS_GroupMatrix ADD LegalName nvarchar2(150) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_GroupMatrix", "LegalName")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    GroupMatrix gmRoot = new GroupMatrix();
                    gmRoot = new GroupMatrixService().GetRoot(sConnStr);
                    GroupMatrixService groupMatrixService = new GroupMatrixService();
                    if (gmRoot == null)
                    {
                        gmRoot = new GroupMatrix();
                        gmRoot.gmDescription = "ROOT";
                        gmRoot.remarks = "Root";
                        Boolean b = groupMatrixService.SaveGroupMatrix(gmRoot, true, sConnStr).data;
                        gmRoot = groupMatrixService.GetRoot(sConnStr);

                        GroupMatrix gm = new GroupMatrix();
                        gm.gmDescription = "Security";
                        gm.parentGMID = gmRoot.gmID;
                        gm.remarks = "Security Root";
                        groupMatrixService.SaveGroupMatrix(gm, true, sConnStr);

                        gm = new GroupMatrix();
                        gm.gmDescription = "All_Clients";
                        gm.parentGMID = gmRoot.gmID;
                        gm.remarks = "Clients Root";
                        groupMatrixService.SaveGroupMatrix(gm, true, sConnStr);
                    }
                    InsertDBUpdate(strUpd, "Matrix Tables");
                }
                #endregion

                #region Update 4. user
                strUpd = "Rez000004";
                if (!CheckUpdateExist(strUpd))
                {
                    InsertDBUpdate(strUpd, "User Tables");
                }
                #endregion
                #region Update 5. User Table updated
                strUpd = "Rez000005";
                if (!CheckUpdateExist(strUpd))
                {
                    InsertDBUpdate(strUpd, "User Table updated");
                }
                #endregion
                #region Update 6. FLT tables
                strUpd = "Rez000006";
                if (!CheckUpdateExist(strUpd))
                {
                    if (GetDataDT(ExistTableSql("GFI_FLT_Asset")).Rows[0][0].ToString() == "0")
                    {
                        sql = @"
							begin
								execute immediate
									'CREATE TABLE GFI_FLT_Asset
									(
										AssetID number(10) NOT NULL,
										AssetType nvarchar2(20) NULL,
										MatrixID number(10) NULL,
										Make nvarchar2(30) NULL,
										Model nvarchar2(50) NULL,
										Status_b nvarchar2(50) NULL,
										CreatedDate timestamp(3) NULL,
										CreatedBy nvarchar2(30) NULL,
										UpdatedDate timestamp(3) NULL,
										UpdatedBy nvarchar2(30) NULL,
										AssetNumber nvarchar2(30) NULL, CONSTRAINT PK_GFI_FLT_Asset PRIMARY KEY(AssetID)
									)';
							end;";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                        //Generate ID using sequence and trigger
                        sql = @"CREATE SEQUENCE GFI_FLT_Asset_seq START WITH 1 INCREMENT BY 1";
                        if (GetDataDT(ExistSequenceSql("GFI_FLT_Asset_seq")).Rows[0][0].ToString() == "0")
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                        sql = @"CREATE OR REPLACE TRIGGER GFI_FLT_Asset_seq_tr
									BEFORE INSERT ON GFI_FLT_Asset FOR EACH ROW
									WHEN(NEW.AssetID IS NULL)
									BEGIN
										SELECT GFI_FLT_Asset_seq.NEXTVAL INTO :NEW.AssetID FROM DUAL;
									END;";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    }

                    if (GetDataDT(ExistTableSql("GFI_FLT_AssetDeviceMap")).Rows[0][0].ToString() == "0")
                    {
                        sql = @"begin
									execute immediate
										'CREATE TABLE GFI_FLT_AssetDeviceMap(
												MapID number(10) NOT NULL,
												AssetID number(10) NULL,
												DeviceID nvarchar2(50) NULL,
												SerialNumber nvarchar2(50) NULL,
												Status_b nvarchar2(2) NULL,
												CreatedBy nvarchar2(30) NULL,
												CreatedDate timestamp(3) NULL,
												UpdatedBy nvarchar2(30) NULL,
												UpdatedDate timestamp(3) NULL,
											 CONSTRAINT PK_GFI_FLT_AssetDeviceMap PRIMARY KEY(MapID)
											)';
								end;";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                        //Generate ID using sequence and trigger
                        sql = @"CREATE SEQUENCE GFI_FLT_AssetDeviceMap_seq START WITH 1 INCREMENT BY 1";
                        if (GetDataDT(ExistSequenceSql("GFI_FLT_AssetDeviceMap_seq")).Rows[0][0].ToString() == "0")
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                        sql = @"CREATE OR REPLACE TRIGGER GFI_FLT_AssetDeviceMap_seq_tr
									BEFORE INSERT ON GFI_FLT_AssetDeviceMap FOR EACH ROW
									WHEN(NEW.MapID IS NULL)
									BEGIN
										SELECT GFI_FLT_AssetDeviceMap_seq.NEXTVAL INTO :NEW.MapID FROM DUAL;
									END;";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    }

                    if (GetDataDT(ExistTableSql("GFI_GPS_GPSData")).Rows[0][0].ToString() == "0")
                    {
                        sql = @"begin
									execute immediate
										'CREATE TABLE GFI_GPS_GPSData(
												""UID"" number(10) NOT NULL,
												AssetID number(10) NULL,
												DeviceID nvarchar2(50) NOT NULL,
												DirverID nvarchar2(50) NULL,
												DateTimeGPS_UTC timestamp(3) NULL,
												DateTimeServer timestamp(3) NULL,
												Longitude binary_double NULL,
												Latitude binary_double NULL,
												LongLatValidFlag number(3) NULL,
												Speed binary_double NULL,
												EngineOn number(3) NULL,
												StopFlag number(3) NULL,
												TripDistance binary_double NULL,
												TripTime binary_double NULL,
												WorkHour number(3) NULL,
											 CONSTRAINT PK_GFI_GPS_GPSData PRIMARY KEY(""UID"")
											)';
								 end;";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                        //Generate ID using sequence and trigger
                        sql = @"CREATE SEQUENCE GFI_GPS_GPSData_seq START WITH 1 INCREMENT BY 1";
                        if (GetDataDT(ExistSequenceSql("GFI_GPS_GPSData_seq")).Rows[0][0].ToString() == "0")
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                        sql = @"CREATE OR REPLACE TRIGGER GFI_GPS_GPSData_seq_tr
									BEFORE INSERT ON GFI_GPS_GPSData FOR EACH ROW
									WHEN(NEW.""UID"" IS NULL)
									BEGIN
										SELECT GFI_GPS_GPSData_seq.NEXTVAL INTO :NEW.""UID"" FROM DUAL;
									END;";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    }

                    if (GetDataDT(ExistTableSql("GFI_GPS_GPSDataDetail")).Rows[0][0].ToString() == "0")
                    {
                        sql = @"begin
									execute immediate
										'CREATE TABLE GFI_GPS_GPSDataDetail(
												GPSDetailID number(19) NOT NULL,
												""UID"" number(10) NULL,
												TypeID nvarchar2(30) NULL,
												TypeValue nvarchar2(30) NULL,
												UOM nvarchar2(15) NULL,
												Remarks nvarchar2(50) NULL, CONSTRAINT PK_GFI_GPS_GPSDataDetail PRIMARY KEY(GPSDetailID)
											)';
								end;";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                        //Generate ID using sequence and trigger
                        sql = @"CREATE SEQUENCE GFI_GPS_GPSDataDetail_seq START WITH 1 INCREMENT BY 1";
                        if (GetDataDT(ExistSequenceSql("GFI_GPS_GPSDataDetail_seq")).Rows[0][0].ToString() == "0")
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                        sql = @"CREATE OR REPLACE TRIGGER GFI_GPS_GPSDataDetail_seq_tr
									BEFORE INSERT ON GFI_GPS_GPSDataDetail FOR EACH ROW
									WHEN(NEW.GPSDetailID IS NULL)
									BEGIN
										SELECT GFI_GPS_GPSDataDetail_seq.NEXTVAL INTO :NEW.GPSDetailID FROM DUAL;
									END;";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    }

                    if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrix")).Rows[0][0].ToString() == "0")
                    {
                        sql = @"begin
									execute immediate
										'CREATE TABLE GFI_SYS_GroupMatrix(
												GMID number(10) NOT NULL,
												GMDescription nvarchar2(100) NULL,
												ParentGMID number(10) NULL,
												Remarks nchar(100) NULL, CONSTRAINT PK_GFI_SYS_GroupMatrix PRIMARY KEY(GMID)
											)';
								end;";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                        //Generate ID using sequence and trigger
                        sql = @"CREATE SEQUENCE GFI_SYS_GroupMatrix_seq START WITH 1 INCREMENT BY 1";
                        if (GetDataDT(ExistSequenceSql("GFI_SYS_GroupMatrix_seq")).Rows[0][0].ToString() == "0")
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                        sql = @"CREATE OR REPLACE TRIGGER GFI_SYS_GroupMatrix_seq_tr
									BEFORE INSERT ON GFI_SYS_GroupMatrix FOR EACH ROW
									WHEN(NEW.GMID IS NULL)
									BEGIN
										SELECT GFI_SYS_GroupMatrix_seq.NEXTVAL INTO :NEW.GMID FROM DUAL;
									END;";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    }

                    InsertDBUpdate(strUpd, "FLT tables");
                }
                #endregion

                //Addinional fields for users should be here as well
                #region Update 7. TripHistory Tables
                strUpd = "Rez000007";
                UserService userService = new UserService();
                if (!CheckUpdateExist(strUpd))
                {
                    if (GetDataDT(ExistTableSql("GFI_GPS_TripHeader")).Rows[0][0].ToString() == "0")
                    {
                        sql = @"
						begin
							execute immediate
							'CREATE TABLE GFI_GPS_TripHeader
								(
									  iID number(10) NOT NULL
									, StartLon binary_double NOT NULL
									, StartLat binary_double NULL
									, EndLon binary_double NULL
									, EndLat binary_double NULL
									, AssetID number(10) NOT NULL
									, DriverID number(10) NOT NULL
									, ExceptionFlag number(3) NULL
									, CONSTRAINT PK_GFI_GPS_TripHeader PRIMARY KEY(iID)
								)';
						end;";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                        //Generate ID using sequence and trigger
                        sql = @"CREATE SEQUENCE GFI_GPS_TripHeader_seq START WITH 1 INCREMENT BY 1";
                        if (GetDataDT(ExistSequenceSql("GFI_GPS_TripHeader_seq")).Rows[0][0].ToString() == "0")
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                        sql = @"CREATE OR REPLACE TRIGGER GFI_GPS_TripHeader_seq_tr
									BEFORE INSERT ON GFI_GPS_TripHeader FOR EACH ROW
									WHEN(NEW.iID IS NULL)
									BEGIN
										SELECT GFI_GPS_TripHeader_seq.NEXTVAL INTO :NEW.iID FROM DUAL;
									END;";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    }

                    if (GetDataDT(ExistTableSql("GFI_GPS_TripDetail")).Rows[0][0].ToString() == "0")
                    {
                        sql = @"
						begin
						execute immediate
						'CREATE TABLE GFI_GPS_TripDetail
							(
								  iID number(19) NOT NULL
								, HeaderiID number(10) NOT NULL
								, GPSDataUID number(10) NOT NULL
								, CONSTRAINT PK_GFI_GPS_TripDetail PRIMARY KEY(iID)
							)';
						  end;";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                        //Generate ID using sequence and trigger
                        sql = @"CREATE SEQUENCE GFI_GPS_TripDetail_seq START WITH 1 INCREMENT BY 1";
                        if (GetDataDT(ExistSequenceSql("GFI_GPS_TripDetail_seq")).Rows[0][0].ToString() == "0")
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                        sql = @"CREATE OR REPLACE TRIGGER GFI_GPS_TripDetail_seq_tr
									BEFORE INSERT ON GFI_GPS_TripDetail FOR EACH ROW
									WHEN(NEW.iID IS NULL)
									BEGIN
										SELECT GFI_GPS_TripDetail_seq.NEXTVAL INTO :NEW.iID FROM DUAL;
									END;";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    }

                    sql = @"drop table GFI_SYS_Matrix";
                    if (GetDataDT(ExistTableSql("GFI_SYS_Matrix")).Rows[0][0].ToString() != "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"drop table GFI_SYS_GroupMatrixUser";
                    if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrixUser")).Rows[0][0].ToString() != "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"drop table GFI_SYS_User";
                    if (GetDataDT(ExistTableSql("GFI_SYS_User")).Rows[0][0].ToString() != "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    if (GetDataDT(ExistTableSql("GFI_SYS_User")).Rows[0][0].ToString() == "0")
                    {
                        sql = @"begin
								execute immediate
									'CREATE TABLE GFI_SYS_User
										(
											  ""UID"" number(10) NOT NULL
											, Username NVARCHAR2(20) NOT NULL
											, Names NVARCHAR2(50) NOT NULL
											, Email NVARCHAR2(50) NOT NULL
											, Tel NVARCHAR2(10) NULL
											, MobileNo NVARCHAR2(10) NULL
											, DateJoined TIMESTAMP(3) NULL
											, Status_b NVARCHAR2(2) NOT NULL
											, Password NVARCHAR2(50) NULL
											, AccessList NVARCHAR2(10) NULL
											, StoreUnit_b NVARCHAR2(2) NULL
											, UType_cbo NVARCHAR2(20) NULL
											, CreatedBy number(10) null
											, CreatedDate TIMESTAMP(3) NULL
											, UpdatedBy number(10) null
											, UpdatedDate TIMESTAMP(3) NULL
											, Dummy1 NVARCHAR2(50) NULL
											, LoginCount NUMBER(10) NULL
											, CONSTRAINT PK_SYS_BY_User PRIMARY KEY(""UID"")
										)';
									end;";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                        //Generate ID using sequence and trigger
                        sql = @"CREATE SEQUENCE GFI_SYS_User_seq START WITH 1 INCREMENT BY 1";
                        if (GetDataDT(ExistSequenceSql("GFI_SYS_User_seq")).Rows[0][0].ToString() == "0")
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                        sql = @"CREATE OR REPLACE TRIGGER GFI_SYS_User_seq_tr
									BEFORE INSERT ON GFI_SYS_User FOR EACH ROW
									WHEN(NEW.""UID"" IS NULL)
									BEGIN
										SELECT GFI_SYS_User_seq.NEXTVAL INTO :NEW.""UID"" FROM DUAL;
									END;";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    }

                    sql = @"CREATE UNIQUE INDEX IX_GFI_SYS_User ON GFI_SYS_User (Email)";
                    if (GetDataDT(ExistsSQLConstraint("IX_GFI_SYS_User", "GFI_SYS_User")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrixUser")).Rows[0][0].ToString() == "0")
                    {
                        sql = @"begin
								execute immediate
									'CREATE TABLE GFI_SYS_GroupMatrixUser
											(
												  MID NUMBER(10) NOT NULL
												, GMID NUMBER(10) NOT NULL
												, iID NUMBER(10) NOT NULL
												, CONSTRAINT PK_GFI_SYS_GroupMatrixUser PRIMARY KEY(MID)
											 )';
								end;";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                        //Generate ID using sequence and trigger
                        sql = @"CREATE SEQUENCE GFI_SYS_GroupMatrixUser_seq START WITH 1 INCREMENT BY 1";
                        if (GetDataDT(ExistSequenceSql("GFI_SYS_GroupMatrixUser_seq")).Rows[0][0].ToString() == "0")
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                        sql = @"CREATE OR REPLACE TRIGGER GFI_SYS_GroupMatrixUser_seq_tr
									BEFORE INSERT ON GFI_SYS_GroupMatrixUser FOR EACH ROW
									WHEN(NEW.MID IS NULL)
									BEGIN
										SELECT GFI_SYS_GroupMatrixUser_seq.NEXTVAL INTO :NEW.MID FROM DUAL;
									END;";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    }

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixUser 
							ADD CONSTRAINT FK_GFI_SYS_GroupMatrixUser_GFI_SYS_GroupMatrix 
								FOREIGN KEY (GMID) 
								REFERENCES GFI_SYS_GroupMatrix (GMID) ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixUser_GFI_SYS_GroupMatrix", "GFI_SYS_GroupMatrixUser")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixUser 
							ADD CONSTRAINT FK_GFI_SYS_GroupMatrixUser_GFI_SYS_User 
								FOREIGN KEY(iID) 
								REFERENCES GFI_SYS_User(""UID"") 
						 ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixUser_GFI_SYS_User", "GFI_SYS_GroupMatrixUser")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_User ADD AccessTemplateId int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "AccessTemplateId")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_User ADD LoginAttempt int Default 1";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "LoginAttempt")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_User ADD PreviousPassword NVARCHAR2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "PreviousPassword")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_User ADD PasswordUpdateddate TIMESTAMP(3) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "PasswordUpdateddate")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_User ADD MaxDayMail int Default 25";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "MaxDayMail")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_User ADD UsrTypeID int Default 0";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "UsrTypeID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_User ADD UsrTypeDesc NVARCHAR2(50) Default 'Std User'";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "UsrTypeDesc")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_User ADD ZoneID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "ZoneID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_User ADD PlnLkpVal int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "PlnLkpVal")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_User ADD MobileAccess int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "MobileAccess")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_User ADD ExternalAccess int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "ExternalAccess")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_User ADD TwoStepVerif NVARCHAR2(10) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "TwoStepVerif")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_User ADD TwoStepAttempts int Default 0";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "TwoStepAttempts")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "UserToken")).Rows[0][0].ToString() == "0")
                    {
                        sql = @"ALTER TABLE GFI_SYS_User ADD UserToken VARCHAR(64) NULL";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                        sql = @"CREATE OR REPLACE TRIGGER before_insert_GFI_SYS_User
									BEFORE INSERT ON GFI_SYS_User 
										FOR EACH ROW WHEN(NEW.UserToken IS NULL)
										BEGIN
											select SYS_GUID() into :NEW.UserToken from DUAL;
										END;";
                        _SqlConn.OracleScriptExecute(sql, sConnStr);
                    }

                    sql = @"CREATE UNIQUE INDEX IX_UserToken_GFI_SYS_User ON GFI_SYS_User (UserToken)";
                    if (GetDataDT(ExistsSQLConstraint("IX_UserToken_GFI_SYS_User", "GFI_SYS_User")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD PswdExpiryDate TIMESTAMP(3) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "PswdExpiryDate")).Rows[0][0].ToString() == "0")
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD PswdExpiryWarnDate TIMESTAMP(3) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "PswdExpiryWarnDate")).Rows[0][0].ToString() == "0")
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_User ADD LastName NVARCHAR2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "LastName")).Rows[0][0].ToString() == "0")
                        dbExecute(sql);

                    #region GFI_SYS_GlobalParams
                    if (GetDataDT(ExistTableSql("GFI_SYS_GlobalParams")).Rows[0][0].ToString() == "0")
                    {
                        sql = @"
								begin
									execute immediate
										'CREATE TABLE GFI_SYS_GlobalParams 
										(
											iid number(10)  NOT NULL,
											ParamName nvarchar2(20) NOT NULL,
											PValue nvarchar2(100) NOT NULL,
											ParamComments nvarchar2(500) NULL,
											PRIMARY KEY (iid ),
											CONSTRAINT U_dbo_GFI_SYS_GlobalParams_1
												UNIQUE (iid, ParamName )
										)';
								end;";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                        //Generate ID using sequence and trigger
                        sql = @"CREATE SEQUENCE GFI_SYS_GlobalParams_seq START WITH 1 INCREMENT BY 1";
                        if (GetDataDT(ExistSequenceSql("GFI_SYS_GlobalParams_seq")).Rows[0][0].ToString() == "0")
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                        sql = @"CREATE OR REPLACE TRIGGER GFI_SYS_GlobalParams_seq_tr
									BEFORE INSERT ON GFI_SYS_GlobalParams FOR EACH ROW
									WHEN (NEW.iid IS NULL)
								BEGIN
									SELECT GFI_SYS_GlobalParams_seq.NEXTVAL INTO :NEW.iid FROM DUAL;
								END;";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    }
                    #endregion

                    User u = new User();
                    u = userService.GetUserByeMail("Installer@naveo.mu", "SysInitPass", sConnStr, false, false);
                    GroupMatrix gmRoot = new GroupMatrix();
                    gmRoot = new GroupMatrixService().GetRoot(sConnStr);
                    if (u.QryResult == User.UserResult.InvalidCredentials && u.UID == -999)
                    {
                        u = new User();
                        u.Username = "INSTALLER";
                        u.Email = "Installer@naveo.mu";
                        u.Names = "Installer";
                        u.Password = "CoreAdmin@123.";
                        u.Status_b = "AC";

                        Matrix m = new Matrix();
                        m.GMID = gmRoot.gmID;
                        m.oprType = DataRowState.Added;
                        u.lMatrix.Add(m);

                        userService.SaveUser(u, true, sConnStr);
                    }
                    u = new User();
                    u = userService.GetUserByeMail("ADMIN@naveo.mu", "SysInitPass", sConnStr, false, false);
                    if (u.QryResult == User.UserResult.InvalidCredentials && u.UID == -999)
                    {
                        u = new User();
                        u.Username = "ADMIN";
                        u.Email = "ADMIN@naveo.mu";
                        u.Names = "Administrator";
                        u.Password = "Password@123.";
                        u.Status_b = "AC";

                        Matrix m = new Matrix();
                        m.GMID = gmRoot.gmID;
                        m.oprType = DataRowState.Added;
                        u.lMatrix.Add(m);

                        userService.SaveUser(u, true, sConnStr);
                    }

                    InsertDBUpdate(strUpd, "TripHistory Tables and matrix review");
                }
                #endregion
                #region Update 8. Asset Matrix review
                strUpd = "Rez000008";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"alter table GFI_GPS_GPSData drop column DirverID";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_GPSData", "DirverID")).Rows[0][0].ToString() != "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"alter table GFI_FLT_Asset drop column MatrixID";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Asset", "MatrixID")).Rows[0][0].ToString() != "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrixAsset")).Rows[0][0].ToString() == "0")
                    {
                        sql = @"
								begin
									execute immediate
										'CREATE TABLE GFI_SYS_GroupMatrixAsset
												(
													  MID NUMBER(10) NOT NULL
													, GMID NUMBER(10) NOT NULL
													, iID NUMBER(10) NOT NULL
													, CONSTRAINT PK_GFI_SYS_GroupMatrixAsset PRIMARY KEY(MID)
												 )';
								end;";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                        //Generate ID using sequence and trigger
                        sql = @"CREATE SEQUENCE GFI_SYS_GroupMatrixAsset_seq START WITH 1 INCREMENT BY 1";
                        if (GetDataDT(ExistSequenceSql("GFI_SYS_GroupMatrixAsset_seq")).Rows[0][0].ToString() == "0")
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                        sql = @"CREATE OR REPLACE TRIGGER GFI_SYS_GroupMatrixAsset_seq_tr
									BEFORE INSERT ON GFI_SYS_GroupMatrixAsset FOR EACH ROW
									WHEN(NEW.MID IS NULL)
									BEGIN
										SELECT GFI_SYS_GroupMatrixAsset_seq.NEXTVAL INTO :NEW.MID FROM DUAL;
									END;";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    }

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixAsset 
							ADD CONSTRAINT FK_GFI_SYS_GroupMatrixAsset_GFI_SYS_GroupMatrix 
								FOREIGN KEY (GMID) 
								REFERENCES GFI_SYS_GroupMatrix (GMID) ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixAsset_GFI_SYS_GroupMatrix", "GFI_SYS_GroupMatrixAsset")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixAsset 
							ADD CONSTRAINT FK_GFI_SYS_GroupMatrixAsset_GFI_FLT_Asset 
								FOREIGN KEY(iID) 
								REFERENCES GFI_FLT_Asset(AssetID) 
						 ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixAsset_GFI_FLT_Asset", "GFI_SYS_GroupMatrixAsset")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_AssetDeviceMap 
							ADD CONSTRAINT FK_GFI_FLT_AssetDeviceMap_GFI_FLT_Asset 
								FOREIGN KEY(AssetID) 
								REFERENCES GFI_FLT_Asset(AssetID) ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_AssetDeviceMap_GFI_FLT_Asset", "GFI_FLT_AssetDeviceMap")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_GPS_TripDetail 
							ADD CONSTRAINT FK_GFI_GPS_TripDetail_GFI_GPS_TripHeader 
								FOREIGN KEY(HeaderiID) 
								REFERENCES GFI_GPS_TripHeader(iID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_TripDetail_GFI_GPS_TripHeader", "GFI_GPS_TripDetail")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_GPS_GPSDataDetail 
							ADD CONSTRAINT FK_GFI_GPS_GPSDataDetail_GFI_GPS_GPSData 
								FOREIGN KEY(""UID"") 
								REFERENCES GFI_GPS_GPSData(""UID"")";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_GPSDataDetail_GFI_GPS_GPSData", "GFI_GPS_GPSDataDetail")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"
								begin
									execute immediate
										'CREATE TABLE GFI_FLT_Driver(
												DriverID NUMBER(10) NOT NULL,
												sDriverName nvarchar2(50) NOT NULL,
												iButton nvarchar2(50) NULL,
												sEmployeeNo nvarchar2(50) NULL,
												sComments nvarchar2(1024) NULL,
												 CONSTRAINT PK_GFI_FLT_Driver PRIMARY KEY(DriverID)
											)';
								end;";
                    if (GetDataDT(ExistTableSql("GFI_FLT_Driver")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_FLT_Driver_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_FLT_Driver_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_FLT_Driver_seq_tr
									BEFORE INSERT ON GFI_FLT_Driver FOR EACH ROW
									WHEN(NEW.DriverID IS NULL)
									BEGIN
										SELECT GFI_FLT_Driver_seq.NEXTVAL INTO :NEW.DriverID FROM DUAL;
									END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"
							begin
								execute immediate
									'CREATE TABLE GFI_SYS_GroupMatrixDriver
											(
												  MID NUMBER(10) NOT NULL
												, GMID NUMBER(10) NOT NULL
												, iID NUMBER(10) NOT NULL
												, CONSTRAINT PK_GFI_SYS_GroupMatrixDriver PRIMARY KEY(MID)
											 )';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrixDriver")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_SYS_GroupMatrixDriver_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_SYS_GroupMatrixDriver_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_SYS_GroupMatrixDriver_seq_tr
								BEFORE INSERT ON GFI_SYS_GroupMatrixDriver FOR EACH ROW
								WHEN(NEW.MID IS NULL)
								BEGIN
									SELECT GFI_SYS_GroupMatrixDriver_seq.NEXTVAL INTO :NEW.MID FROM DUAL;
								END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixDriver 
							ADD CONSTRAINT FK_GFI_SYS_GroupMatrixDriver_GFI_SYS_GroupMatrix 
								FOREIGN KEY (GMID) 
								REFERENCES GFI_SYS_GroupMatrix (GMID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixDriver_GFI_SYS_GroupMatrix", "GFI_SYS_GroupMatrixDriver")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixDriver 
							ADD CONSTRAINT FK_GFI_SYS_GroupMatrixDriver_GFI_FLT_Driver 
								FOREIGN KEY(iID) 
								REFERENCES GFI_FLT_Driver(DriverID) 
						 ON DELETE  CASCADE";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixDriver_GFI_FLT_Driver", "GFI_SYS_GroupMatrixDriver")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_GPS_TripHeader 
							ADD CONSTRAINT FK_GFI_GPS_TripHeader_GFI_FLT_Asset 
								FOREIGN KEY(AssetID) 
								REFERENCES GFI_FLT_Asset(AssetID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_TripHeader_GFI_FLT_Asset", "GFI_GPS_TripHeader")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_GPS_TripHeader 
							ADD CONSTRAINT FK_GFI_GPS_TripHeader_GFI_FLT_Driver 
								FOREIGN KEY(DriverID) 
								REFERENCES GFI_FLT_Driver(DriverID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_TripHeader_GFI_FLT_Driver", "GFI_GPS_TripHeader")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_GPS_GPSData 
							ADD CONSTRAINT FK_GFI_GPS_GPSData_GFI_FLT_Asset 
								FOREIGN KEY(AssetID) 
								REFERENCES GFI_FLT_Asset(AssetID) ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_GPSData_GFI_FLT_Asset", "GFI_GPS_GPSData")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"alter table GFI_GPS_GPSData drop column DeviceID";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_GPSData", "DeviceID")).Rows[0][0].ToString() != "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_GPS_GPSData add DriverID int DEFAULT 1";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_GPSData", "DriverID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_GPS_GPSData 
							ADD CONSTRAINT FK_GFI_GPS_GPSData_GFI_FLT_Driver 
								FOREIGN KEY(DriverID) 
								REFERENCES GFI_FLT_Driver(DriverID) ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_GPSData_GFI_FLT_Driver", "GFI_GPS_GPSData")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "Asset Matrix review and other Table Links");
                }
                #endregion

                //Addinional fields for Driver should be here as well
                #region Update 127. GFI_FLT_Driver Additional fields
                strUpd = "Rez000127";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_FLT_Driver ADD DateOfBirth TIMESTAMP(3) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "DateOfBirth")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Gender nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Gender")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Address1 nvarchar2(100) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Address1")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Address2 nvarchar2(100) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Address2")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD City nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "City")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Nationality nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Nationality")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD sFirtName nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "sFirtName")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD LicenseNo1 nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "LicenseNo1")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD LicenseNo1Expiry TIMESTAMP(3) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "LicenseNo1Expiry")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD LicenseNo2 nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "LicenseNo2")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Licenseno2Expiry TIMESTAMP(3) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Licenseno2Expiry")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Ref1 nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Ref1")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD ZoneID nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "ZoneID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD CreatedDate TIMESTAMP(3) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "CreatedDate")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD CreatedBy int  NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "CreatedBy")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD UpdatedDate TIMESTAMP(3) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "UpdatedDate")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD UpdatedBy int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "UpdatedBy")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Status nvarchar2(50)  NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Status")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Address3 nvarchar2(50)  NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Address3")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD District nvarchar2(25)  NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "District")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD PostalCode nvarchar2(15)  NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "PostalCode")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD MobileNumber nvarchar2(15)  NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "MobileNumber")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Reason nvarchar2(500)  NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Reason")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Department nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Department")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Email nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Email")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD sLastName nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "sLastName")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD OfficerLevel nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "OfficerLevel")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD OfficerTitle nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "OfficerTitle")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Phone nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Phone")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Title nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Title")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD ASPUser nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "ASPUser")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD UserId int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "UserId")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Photo bfile NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Photo")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Type_Institution int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Type_Institution")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Type_Driver int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Type_Driver")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Type_Grade int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Type_Grade")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD HomeNo int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "HomeNo")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "GFI_FLT_Driver Additional fields");
                }
                #endregion
                #region Update 9. Default Driver
                strUpd = "Rez000009";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_FLT_Driver ADD EmployeeType nvarchar(15) Default 'Driver'";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "EmployeeType")).Rows[0][0].ToString() == "0")
                        dbExecute(sql);

                    DriverService driverService = new DriverService();
                    Driver d = driverService.GetDefaultDriver(sConnStr);
                    GroupMatrix gmRoot = new GroupMatrix();
                    gmRoot = new GroupMatrixService().GetRoot(sConnStr);
                    if (d == null)
                    {
                        d = new Driver();
                        d.sDriverName = "Default Driver";
                        d.sComments = "Driver for vehicles with no Driver";
                        d.sEmployeeNo = "INSTALLER";
                        d.iButton = "00000000";

                        Matrix m = new Matrix();
                        m.GMID = gmRoot.gmID;
                        m.oprType = DataRowState.Added;
                        d.lMatrix.Add(m);

                        driverService.SaveDriver(d, true, sConnStr);
                    }

                    sql = @"ALTER TABLE GFI_FLT_AssetDeviceMap add StartDate TIMESTAMP(3) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "StartDate")).Rows[0][0].ToString() == "0")
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_FLT_AssetDeviceMap add EndDate TIMESTAMP(3) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "EndDate")).Rows[0][0].ToString() == "0")
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_GPS_GPSData modify ( LongLatValidFlag INT )";
                    dbExecute(sql);
                    sql = @"ALTER TABLE GFI_GPS_GPSData modify ( EngineOn INT )";
                    dbExecute(sql);
                    sql = @"ALTER TABLE GFI_GPS_GPSData modify ( StopFlag INT )";
                    dbExecute(sql);
                    sql = @"ALTER TABLE GFI_GPS_GPSData modify ( WorkHour INT )";
                    dbExecute(sql);
                    sql = @"ALTER TABLE GFI_GPS_TripHeader modify ( ExceptionFlag INT )";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "Default Driver");
                }
                #endregion
                #region Update 10. TripHeader
                strUpd = "Rez000010";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_GPS_TripHeader add MaxSpeed int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "MaxSpeed")).Rows[0][0].ToString() == "0")
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_GPS_TripHeader add IdlingTime float NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "IdlingTime")).Rows[0][0].ToString() == "0")
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_GPS_TripHeader add StopTime float NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "StopTime")).Rows[0][0].ToString() == "0")
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_GPS_TripHeader add TripDistance float NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "TripDistance")).Rows[0][0].ToString() == "0")
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_GPS_TripHeader add TripTime float NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "TripTime")).Rows[0][0].ToString() == "0")
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_GPS_TripHeader add GPSDataStartUID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "GPSDataStartUID")).Rows[0][0].ToString() == "0")
                        dbExecute(sql);
                    sql = @"ALTER TABLE GFI_GPS_TripHeader add GPSDataEndUID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "GPSDataEndUID")).Rows[0][0].ToString() == "0")
                        dbExecute(sql);
                    InsertDBUpdate(strUpd, "TripHeader");
                }
                #endregion

                #region Update 11. Zone
                strUpd = "Rez000011";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"
							begin
								execute immediate
									'CREATE TABLE GFI_FLT_ZoneHeader(
												ZoneID number(10) NOT NULL,
												Description nvarchar2(255) NULL,
												Displayed number(10) DEFAULT 1 NOT NULL,
												Comments nvarchar2(1024) NULL,
												Color number(10) DEFAULT 1 NOT NULL,
												 ZoneTypeID number(10) DEFAULT 1 NOT NULL,
											CONSTRAINT PK_GFI_FLT_ZoneHeader PRIMARY KEY
												(ZoneID)
										 )';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_FLT_ZoneHeader")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_FLT_ZoneHeader_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistTableSql("GFI_FLT_ZoneHeader_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_FLT_ZoneHeader_seq_tr
					BEFORE INSERT ON GFI_FLT_ZoneHeader FOR EACH ROW
					WHEN(NEW.ZoneID IS NULL)
					BEGIN
					SELECT GFI_FLT_ZoneHeader_seq.NEXTVAL INTO :NEW.ZoneID FROM DUAL;
					END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);


                    sql = @"
							begin
								execute immediate
									'CREATE TABLE GFI_FLT_ZoneDetail(
											iID number(10) NOT NULL,
											Latitude binary_double NULL,
											Longitude binary_double NULL,
											ZoneID number(10) NULL,
											CordOrder number(10) null,
											CONSTRAINT PK_GFI_FLT_ZoneDetail PRIMARY KEY
											(iID)
										)';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_FLT_ZoneDetail")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_FLT_ZoneDetail_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistTableSql("GFI_FLT_ZoneDetail_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_FLT_ZoneDetail_seq_tr
					BEFORE INSERT ON GFI_FLT_ZoneDetail FOR EACH ROW
					WHEN(NEW.iID IS NULL)
					BEGIN
					SELECT GFI_FLT_ZoneDetail_seq.NEXTVAL INTO :NEW.iID FROM DUAL;
					END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_ZoneDetail 
							ADD CONSTRAINT FK_GFI_FLT_ZoneDetail_GFI_FLT_ZoneHeader 
								FOREIGN KEY(ZoneID)
								REFERENCES GFI_FLT_ZoneHeader (ZoneID)
							ON DELETE CASCADE";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ZoneDetail_GFI_FLT_ZoneHeader", "GFI_FLT_ZoneDetail")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    sql = @"
					begin
					execute immediate
					'CREATE TABLE GFI_SYS_GroupMatrixZone
						(
							MID NUMBER(10) NOT NULL
							, GMID NUMBER(10) NOT NULL
							, iID NUMBER(10) NOT NULL
							, CONSTRAINT PK_GFI_SYS_GroupMatrixZone PRIMARY KEY(MID)
						)';
					end;";
                    if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrixZone")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_SYS_GroupMatrixZone_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrixZone_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_SYS_GroupMatrixZone_seq_tr
					BEFORE INSERT ON GFI_SYS_GroupMatrixZone FOR EACH ROW
					WHEN(NEW.MID IS NULL)
					BEGIN
					SELECT GFI_SYS_GroupMatrixZone_seq.NEXTVAL INTO :NEW.MID FROM DUAL;
					END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixZone 
							ADD CONSTRAINT FK_GFI_SYS_GroupMatrixZone_GFI_SYS_GroupMatrix 
								FOREIGN KEY (GMID) 
								REFERENCES GFI_SYS_GroupMatrix (GMID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixZone_GFI_SYS_GroupMatrix", "GFI_SYS_GroupMatrixZone")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixZone 
							ADD CONSTRAINT FK_GFI_SYS_GroupMatrixZone_GFI_FLT_ZoneHeader 
								FOREIGN KEY(iID) 
								REFERENCES GFI_FLT_ZoneHeader(ZoneID) 
						 ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixZone_GFI_FLT_ZoneHeader", "GFI_SYS_GroupMatrixZone")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    InsertDBUpdate(strUpd, "Zone");
                }
                #endregion
                #region Update 12. Timezone
                strUpd = "Rez000012";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_FLT_Asset add TimeZoneID nvarchar2(50) DEFAULT 'Arabian Standard Time'";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Asset", "TimeZoneID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    sql = "ALTER TABLE GFI_FLT_Asset add Odometer int DEFAULT 0";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Asset", "Odometer")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    sql = "ALTER TABLE GFI_FLT_AssetDeviceMap add MaxNoLogDays int DEFAULT 1";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "MaxNoLogDays")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "TimeZone");
                }
                #endregion
                #region Update 13. Asset Fields
                strUpd = "Rez000013";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"alter table GFI_FLT_Asset drop column Make";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Asset", "Make")).Rows[0][0].ToString() != "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"alter table GFI_FLT_Asset drop column Model";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Asset", "Model")).Rows[0][0].ToString() != "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_FLT_Asset add AssetName NVARCHAR2(120) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Asset", "AssetName")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"alter table GFI_GPS_TripHeader drop column StartLon";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "StartLon")).Rows[0][0].ToString() != "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"alter table GFI_GPS_TripHeader drop column StartLat";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "StartLat")).Rows[0][0].ToString() != "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"alter table GFI_GPS_TripHeader drop column EndLon";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "EndLon")).Rows[0][0].ToString() != "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"alter table GFI_GPS_TripHeader drop column EndLat";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "EndLat")).Rows[0][0].ToString() != "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_GPS_TripDetail DROP CONSTRAINT FK_GFI_GPS_TripDetail_GFI_GPS_TripHeader";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_TripDetail_GFI_GPS_TripHeader", "GFI_GPS_TripDetail")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_GPS_TripDetail
							ADD CONSTRAINT FK_GFI_GPS_TripDetail_GFI_GPS_TripHeader
								FOREIGN KEY(HeaderiID)
								REFERENCES GFI_GPS_TripHeader(iID)
							ON DELETE CASCADE";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_TripDetail_GFI_GPS_TripHeader", "GFI_GPS_TripDetail")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_GPS_GPSDataDetail DROP CONSTRAINT FK_GFI_GPS_GPSDataDetail_GFI_GPS_GPSData";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_GPSDataDetail_GFI_GPS_GPSData", "GFI_GPS_GPSDataDetail")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_GPS_GPSDataDetail 
							ADD CONSTRAINT FK_GFI_GPS_GPSDataDetail_GFI_GPS_GPSData 
								FOREIGN KEY(""UID"") 
								REFERENCES GFI_GPS_GPSData(""UID"")
							ON DELETE CASCADE";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_GPSDataDetail_GFI_GPS_GPSData", "GFI_GPS_GPSDataDetail")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "Asset Fields");
                }
                #endregion
                #region Update 14. Indexes
                strUpd = "Rez000014";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "DROP INDEX my_IX_GFI_GPS_GPSData";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSData", "my_IX_GFI_GPS_GPSData")).Rows[0][0].ToString() != "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE UNIQUE INDEX my_IX_GFI_GPS_GPSData ON GFI_GPS_GPSData
							(
								""UID"" ASC,
								AssetID ASC,
								DateTimeGPS_UTC ASC,
								DriverID ASC
							)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSData", "my_IX_GFI_GPS_GPSData")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE INDEX my_IX_GFI_GPS_GPSDataDetail ON GFI_GPS_GPSDataDetail 
							(
								UID ASC
							)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSDataDetail", "my_IX_GFI_GPS_GPSDataDetail")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE INDEX my_IX_GFI_GPS_TripDetail ON GFI_GPS_TripDetail 
							(
								HeaderiID ASC
							)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripDetail", "my_IX_GFI_GPS_TripDetail")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE UNIQUE INDEX my_IX_GFI_GPS_TripHeader ON GFI_GPS_TripHeader
							(
								iID ASC,
								AssetID ASC,
								DriverID ASC,
								ExceptionFlag ASC,
								GPSDataStartUID ASC,
								GPSDataEndUID ASC
							)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "my_IX_GFI_GPS_TripHeader")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "Indexes");
                }
                #endregion
                #region Update 15. CM_TH Extended
                strUpd = "Rez000015";
                if (!CheckUpdateExist(strUpd))
                {
                    InsertDBUpdate(strUpd, "CM_TH Extended");
                }
                #endregion

                #region Update 16. Rules & Exceptions
                strUpd = "Rez000016";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE [dbo].[GFI_GPS_Rules](
                             [RuleID] [int] IDENTITY(1,1) NOT NULL,
                             [ParentRuleID] [int] NULL,
                             [RuleType] [int] NULL,
                             [MinTriggerValue] [int] NULL,
                             [MaxTriggerValue] [int] NULL,
                             [Struc] [nvarchar](20) NOT NULL,
                             [StrucValue] [nvarchar](20) NOT NULL,
                             [StrucCondition] [nvarchar](100) NULL,
                             [RuleName] [nvarchar](300) NULL,
                             [StrucType] [nvarchar](20) NULL,
                             CONSTRAINT [PK_GFI_GPS_Rules] PRIMARY KEY CLUSTERED ([RuleID] ASC)
                            )";
                    if (GetDataDT(ExistTableSql("GFI_GPS_Rules")).Rows[0][0].ToString() == "0")
                        CreateTable(sql, "GFI_GPS_Rules", "RuleID");

                    sql = @"CREATE TABLE GFI_GPS_Exceptions(
											 iID number(10) NOT NULL,
											 RuleID number(10) NULL,
											 GPSDataID number(19) NULL,
											 AssetID number(10) NULL,
											 DriverID number(10) NULL,
											 Severity nvarchar2(100) NULL,
											 CONSTRAINT PK_GFI_GPS_Exceptions PRIMARY KEY (iID)
									)";
                    if (GetDataDT(ExistTableSql("GFI_GPS_Exceptions")).Rows[0][0].ToString() == "0")
                        CreateTable(sql, "GFI_GPS_Exceptions", "iID");

                    InsertDBUpdate(strUpd, "Rules and Exceptions");
                }
                #endregion
                #region Update 17. CMTrips
                strUpd = "Rez000017";
                if (!CheckUpdateExist(strUpd))
                {
                    InsertDBUpdate(strUpd, "CMTrips");
                }
                #endregion

                User usrInst = new User();
                usrInst = userService.GetUserInstaller(sConnStr);
                Globals.uLogin = usrInst;

                #region Update 18. Assets Management Module
                strUpd = "Rez000018";
                if (!CheckUpdateExist(strUpd))
                {
                    #region Tables

                    // -- Table: GFI_AMM_VehicleMaintCat
                    sql = @"
							begin
								execute immediate
									'CREATE TABLE GFI_AMM_VehicleMaintCat
									(
									 MaintCatId number(10) NOT NULL,
									 Description nvarchar2(120) NULL,
									 CreatedDate timestamp(3) NULL,
									 CreatedBy nvarchar2(30) NULL,
									 UpdatedDate timestamp(3) NULL,
									 UpdatedBy nvarchar2(30) NULL,
									 CONSTRAINT PK_GFI_AMM_VehicleMaintCat PRIMARY KEY  
										(
										 MaintCatId 
										)
									)';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_AMM_VehicleMaintCat")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    // -- Table: GFI_AMM_AssetExtProFields
                    sql = @"
							begin
								execute immediate
										'CREATE TABLE GFI_AMM_AssetExtProFields(
										FieldId number(10) NOT NULL,
										AssetType nvarchar2(120) NULL,
										Category nvarchar2(120) NULL,
										FieldName nvarchar2(120) NULL,
										Label nvarchar2(120) NULL,
										Description nvarchar2(255) NULL,
										Type nvarchar2(120) NULL,
										UnitOfMeasure nvarchar2(120) NULL,
										KeepHistory char(1) NULL,
										DataRetentionDays number(10) NULL,
										THWarning_Value number(10) NULL,
										THWarning_TimeBased number(10) NULL,
										THCritical_Value number(10) NULL,
										THCritical_TimeBased number(10) NULL,
										DisplayOrder number(10) NULL,
										CreatedDate timestamp(3) NULL,
										CreatedBy nvarchar2(30) NULL,
										UpdatedDate timestamp(3) NULL,
										UpdatedBy nvarchar2(30) NULL,
									 CONSTRAINT PK_GFI_AMM_ExtP_Fields PRIMARY KEY  
									(FieldId )
									)';
						end;";
                    if (GetDataDT(ExistTableSql("GFI_AMM_AssetExtProFields")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE  INDEX IX_AssetCategory ON GFI_AMM_AssetExtProFields 
						(
							Category ASC
						)";

                    if (GetDataDT(ExistsIndex("GFI_AMM_AssetExtProFields", "IX_AssetCategory")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE  INDEX IX_AssetType ON GFI_AMM_AssetExtProFields 
						(
						   AssetType ASC
						)";

                    if (GetDataDT(ExistsIndex("GFI_AMM_AssetExtProFields", "IX_AssetType")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE  INDEX IX_FieldName ON GFI_AMM_AssetExtProFields
						(
							FieldName ASC
						)";

                    if (GetDataDT(ExistsIndex("GFI_AMM_AssetExtProFields", "IX_FieldName")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    // -- Table: GFI_AMM_InsCoverTypes
                    sql = @"
							begin
								execute immediate
									'CREATE TABLE GFI_AMM_InsCoverTypes(
									CoverTypeId number(10) NOT NULL,
									Description nvarchar2(120) NULL,
									CreatedDate timestamp(3) NULL,
									CreatedBy nvarchar2(30) NULL,
									UpdatedDate timestamp(3) NULL,
									UpdatedBy nvarchar2(30) NULL,
								 CONSTRAINT PK_GFI_AMM_InsCoverTypes PRIMARY KEY  
								(CoverTypeId ) 
								)';
							end;";

                    if (GetDataDT(ExistTableSql("GFI_AMM_InsCoverTypes")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE INDEX IX_CoverTypeDescription ON GFI_AMM_InsCoverTypes
						(
							Description ASC
						)";

                    if (GetDataDT(ExistsIndex("GFI_AMM_InsCoverTypes", "IX_CoverTypeDescription")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    // -- Table: GFI_AMM_VehicleTypes
                    sql = @"
							begin
								execute immediate
									'CREATE TABLE GFI_AMM_VehicleTypes(
									VehicleTypeId number(10) NOT NULL,
									Description nvarchar2(120) NULL,
									CreatedDate timestamp(3) NULL,
									CreatedBy nvarchar2(30) NULL,
									UpdatedDate timestamp(3) NULL,
									UpdatedBy nvarchar2(30) NULL,
									CONSTRAINT PK_GFI_AMM_VehicleTypes PRIMARY KEY  
									(VehicleTypeId )
									)';
							end;";

                    if (GetDataDT(ExistTableSql("GFI_AMM_VehicleTypes")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE  INDEX IX_Description ON GFI_AMM_VehicleTypes 
						(
							Description ASC
						)";

                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleTypes", "IX_Description")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    // -- Table: GFI_AMM_VehicleMaintStatus
                    sql = @"
							begin
								execute immediate
									'CREATE TABLE GFI_AMM_VehicleMaintStatus(
									MaintStatusId number(10) NOT NULL,
									Description nvarchar2(120) NULL,
									SortOrder number(10) NULL,
									CreatedDate timestamp(3) NULL,
									CreatedBy nvarchar2(30) NULL,
									UpdatedDate timestamp(3) NULL,
									UpdatedBy nvarchar2(30) NULL,
									CONSTRAINT PK_GFI_AMM_VehicleMaintStatus PRIMARY KEY  
									(MaintStatusId )
									)';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_AMM_VehicleMaintStatus")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE  INDEX IX_GFI_AMM_VehicleMaintStatus_Description ON GFI_AMM_VehicleMaintStatus(Description ASC)";
                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintStatus", "IX_GFI_AMM_VehicleMaintStatus_Description")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    // -- Table: GFI_AMM_VehicleMaintType
                    sql = @"
							begin
								execute immediate
										'CREATE TABLE GFI_AMM_VehicleMaintTypes(
										MaintTypeId number(10) NOT NULL,
										MaintCatId_cbo number(10) NULL,
										Description nvarchar2(120) NULL,
										OccurrenceType number(10) NULL,
										OccurrenceFixedDate timestamp(3) NULL,
										OccurrenceFixedDateTh number(10) NULL,
										OccurrenceDuration number(10) NULL,
										OccurrenceDurationTh number(10) NULL,
										OccurrencePeriod_cbo nvarchar2(30) NULL,
										OccurrenceKM number(10) NULL,
										OccurrenceKMTh number(10) NULL,
										OccurrenceEngineHrs number(10) NULL,	                       
										OccurrenceEngineHrsTh number(10) NULL,
										CreatedDate timestamp(3) NULL,
										CreatedBy nvarchar2(30) NULL,
										UpdatedDate timestamp(3) NULL,
										UpdatedBy nvarchar2(30) NULL,
									 CONSTRAINT PK_GFI_AMM_VehicleMaintTypes PRIMARY KEY  
									(MaintTypeId )
									)';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_AMM_VehicleMaintTypes")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_AMM_VehicleMaintTypes_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_AMM_VehicleMaintTypes_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_AMM_VehicleMaintTypes_seq_tr
								BEFORE INSERT ON GFI_AMM_VehicleMaintTypes FOR EACH ROW
								WHEN(NEW.MaintTypeId IS NULL)
								BEGIN
									SELECT GFI_AMM_VehicleMaintTypes_seq.NEXTVAL INTO :NEW.MaintTypeId FROM DUAL;
								END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE  INDEX IX_GFI_AMM_VehicleMaintTypes_Description ON GFI_AMM_VehicleMaintTypes(Description ASC)";
                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintTypes", "IX_GFI_AMM_VehicleMaintTypes_Description")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE  INDEX IX_GFI_AMM_VehicleMaintTypes_MaintCatId ON GFI_AMM_VehicleMaintTypes(MaintCatId_cbo ASC)";
                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintTypes", "IX_GFI_AMM_VehicleMaintTypes_MaintCatId")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    // -- Table: GFI_AMM_AssetExtProXT
                    sql = @"
							begin
								execute immediate
									'CREATE TABLE GFI_AMM_AssetExtProXT(
									 URI number(19) NOT NULL,
									 AssetId number(10) NULL,
									 FieldId number(10) NULL,
									 XTValue nvarchar2(255) NULL,
									 CreatedDate timestamp(3) NULL,
									 CreatedBy nvarchar2(30) NULL,
									 UpdatedDate timestamp(3) NULL,
									 UpdatedBy nvarchar2(30) NULL,
									 CONSTRAINT PK_GFI_AMM_AssetExtProXT PRIMARY KEY  
									 (URI )
									 )';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_AMM_AssetExtProXT")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_AMM_AssetExtProXT_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_AMM_AssetExtProXT_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_AMM_AssetExtProXT_seq_tr
					BEFORE INSERT ON GFI_AMM_AssetExtProXT FOR EACH ROW
					WHEN(NEW.URI IS NULL)
					BEGIN
					SELECT GFI_AMM_AssetExtProXT_seq.NEXTVAL INTO :NEW.URI FROM DUAL;
					END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE  INDEX IX_GFI_AMM_AssetExtProXT_AssetId ON GFI_AMM_AssetExtProXT(AssetId ASC)";
                    if (GetDataDT(ExistsIndex("GFI_AMM_AssetExtProXT", "IX_GFI_AMM_AssetExtProXT_AssetId")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE  INDEX IX_GFI_AMM_AssetExtProXT_FieldId ON GFI_AMM_AssetExtProXT(FieldId ASC)";
                    if (GetDataDT(ExistsIndex("GFI_AMM_AssetExtProXT", "IX_GFI_AMM_AssetExtProXT_FieldId")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE  INDEX IX_GFI_AMM_AssetExtProXT_XTValue ON GFI_AMM_AssetExtProXT(XTValue ASC)";
                    if (GetDataDT(ExistsIndex("GFI_AMM_AssetExtProXT", "IX_GFI_AMM_AssetExtProXT_XTValue")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    // -- Table: GFI_AMM_AssetExtProVehicles
                    sql = @"CREATE TABLE GFI_AMM_AssetExtProVehicles(
							AssetId number(10) NOT NULL,
							VIN nvarchar2(120) NULL,
							Description nvarchar2(120) NULL,
							Make nvarchar2(120) NULL,
							Model nvarchar2(120) NULL,
							VehicleTypeId_cbo number(10) NULL,
							Manufacturer nvarchar2(120) NULL,
							YearManufactured number(10) NULL,
							PurchasedDate timestamp(3) NULL,
							PurchasedValue number(18, 2) NULL,
							PORef nvarchar2(120) NULL,
							ExpectedLifetime number(10) NULL,
							ResidualValue number(18, 2) NULL,
							EngineSerialNumber nvarchar2(120) NULL,
							EngineType nvarchar2(120) NULL,
							EngineCapacity number(10) NULL,
							EnginePower number(10) NULL,
							FuelType nvarchar2(120) NULL,
							StdConsumption number(18, 2) NULL,
							ExternalReference nvarchar2(120) NULL,
							InService_b char(1) NULL,
							AdditionalInfo nvarchar2(1000) NULL,
							CreatedDate timestamp(3) NOT NULL,
							CreatedBy nvarchar2(30) NOT NULL,
							UpdatedDate timestamp(3) NULL,
							UpdatedBy nvarchar2(30) NULL,
						 CONSTRAINT PK_GFI_AMM_Asset_ExtPro PRIMARY KEY (AssetId)
						)";
                    if (GetDataDT(ExistTableSql("GFI_AMM_AssetExtProVehicles")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE  INDEX IX_GFI_AMM_AssetExtProVehicles_Make ON GFI_AMM_AssetExtProVehicles(Make ASC)";
                    if (GetDataDT(ExistsIndex("GFI_AMM_AssetExtProVehicles", "IX_GFI_AMM_AssetExtProVehicles_Make")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE  INDEX IX_GFI_AMM_AssetExtProVehicles_Manuafacturer ON GFI_AMM_AssetExtProVehicles(Manufacturer ASC)";
                    if (GetDataDT(ExistsIndex("GFI_AMM_AssetExtProVehicles", "IX_GFI_AMM_AssetExtProVehicles_Manuafacturer")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE  INDEX IX_GFI_AMM_AssetExtProVehicles_Model ON GFI_AMM_AssetExtProVehicles(Model ASC)";
                    if (GetDataDT(ExistsIndex("GFI_AMM_AssetExtProVehicles", "IX_GFI_AMM_AssetExtProVehicles_Model")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE  INDEX IX_GFI_AMM_AssetExtProVehicles_VehicleTypeId ON GFI_AMM_AssetExtProVehicles(VehicleTypeId_cbo ASC)";
                    if (GetDataDT(ExistsIndex("GFI_AMM_AssetExtProVehicles", "IX_GFI_AMM_AssetExtProVehicles_VehicleTypeId")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    // -- Table: GFI_AMM_AssetExtProVehicles
                    sql = @"
							begin
								execute immediate
									'CREATE TABLE GFI_AMM_VehicleMaintenance(
									URI number(10) NOT NULL,
									AssetId number(10) NULL,
									MaintTypeId_cbo number(10) NULL,
									CompanyName nvarchar2(255) NULL,
									PhoneNumber nvarchar2(120) NULL,
									CompanyRef nvarchar2(120) NULL,
									MaintDescription nvarchar2(512) NULL,
									StartDate timestamp(3) NULL,
									EndDate timestamp(3) NULL,
									MaintStatusId_cbo number(10) NULL,
									EstimatedValue number(18, 2) NULL,
									TotalCost number(18, 2) NULL,
									CoverTypeId_cbo number(10) NULL,
									CalculatedOdometer number(10) NULL,
									ActualOdometer number(10) NULL,
									CalculatedEngineHrs number(10) NULL,
									ActualEngineHrs number(10) NULL,
									AdditionalInfo nvarchar2(1000) NULL,
									CreatedDate timestamp(3) NULL,
									CreatedBy nvarchar2(30) NULL,
									UpdatedDate timestamp(3) NULL,
									UpdatedBy nvarchar2(30) NULL,
								 CONSTRAINT PK_GFI_AMM_VehicleMaint PRIMARY KEY  
								(URI)
								)';
						end;";
                    if (GetDataDT(ExistTableSql("GFI_AMM_VehicleMaintenance")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_AMM_VehicleMaintenance_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_AMM_VehicleMaintenance_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_AMM_VehicleMaintenance_seq_tr
								BEFORE INSERT ON GFI_AMM_VehicleMaintenance FOR EACH ROW
								WHEN(NEW.URI IS NULL)
								BEGIN
									SELECT GFI_AMM_VehicleMaintenance_seq.NEXTVAL INTO :NEW.URI FROM DUAL;
								END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //--Indexes
                    sql = @"CREATE  INDEX IX_GFI_AMM_VehicleMaintenance_ActualCompletionDate ON GFI_AMM_VehicleMaintenance(EndDate ASC)";
                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintenance", "IX_GFI_AMM_VehicleMaintenance_ActualCompletionDate")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //--
                    sql = @"CREATE  INDEX IX_GFI_AMM_VehicleMaintenance_ActualStartDate ON GFI_AMM_VehicleMaintenance(StartDate ASC)";
                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintenance", "IX_GFI_AMM_VehicleMaintenance_ActualStartDate")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //--
                    sql = @"CREATE  INDEX IX_GFI_AMM_VehicleMaintenance_AssetId ON GFI_AMM_VehicleMaintenance(AssetId ASC)";
                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintenance", "IX_GFI_AMM_VehicleMaintenance_AssetId")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //--
                    sql = @"CREATE  INDEX IX_GFI_AMM_VehicleMaintenance_CompanyName ON GFI_AMM_VehicleMaintenance(CompanyName ASC)";
                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintenance", "IX_GFI_AMM_VehicleMaintenance_CompanyName")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //--
                    sql = @"CREATE  INDEX IX_GFI_AMM_VehicleMaintenance_CoverTypeId ON GFI_AMM_VehicleMaintenance(CoverTypeId_cbo ASC)";
                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintenance", "IX_GFI_AMM_VehicleMaintenance_CoverTypeId")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //--
                    sql = @"CREATE  INDEX IX_GFI_AMM_VehicleMaintenance_MaintStatusId ON GFI_AMM_VehicleMaintenance(MaintStatusId_cbo ASC)";
                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintenance", "IX_GFI_AMM_VehicleMaintenance_MaintStatusId")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //--
                    sql = @"CREATE  INDEX IX_GFI_AMM_VehicleMaintenance_MaintTypeId ON GFI_AMM_VehicleMaintenance(MaintTypeId_cbo ASC)";
                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintenance", "IX_GFI_AMM_VehicleMaintenance_MaintTypeId")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    // -- Table: GFI_AMM_AssetExtProVehicles
                    sql = @"
							begin
								execute immediate
										'CREATE TABLE GFI_AMM_VehicleMaintTypesLink(
										URI number(10) NOT NULL,
										MaintURI number(10) NULL,
										AssetId number(10) NULL,
										MaintTypeId number(10) NULL,
										NextMaintDate timestamp(3) NULL,
										NextMaintDateTG timestamp(3) NULL,
										CurrentOdometer number(10) NULL,
										NextMaintOdometer number(10) NULL,
										NextMaintOdometerTG number(10) NULL,
										CurrentEngHrs number(10) NULL,
										NextMaintEngHrs number(10) NULL,
										NextMaintEngHrsTG number(10) NULL,
										Status number(10) NULL,
										CreatedDate timestamp(3) NULL,
										CreatedBy nvarchar2(30) NULL,
										UpdatedDate timestamp(3) NULL,
										UpdatedBy nvarchar2(30) NULL,
									 CONSTRAINT PK_GFI_AMM_VehicleMaintTypesLink PRIMARY KEY  
									(
										URI 
									)
									)';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_AMM_VehicleMaintTypesLink")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_AMM_VehicleMaintTypesLink_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_AMM_VehicleMaintTypesLink_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_AMM_VehicleMaintTypesLink_seq_tr
					BEFORE INSERT ON GFI_AMM_VehicleMaintTypesLink FOR EACH ROW
					WHEN(NEW.URI IS NULL)
					BEGIN
					SELECT GFI_AMM_VehicleMaintTypesLink_seq.NEXTVAL INTO :NEW.URI FROM DUAL;
					END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //--
                    sql = @"CREATE  INDEX IX_GFI_AMM_VehicleMaintTypesLink_AssetId ON GFI_AMM_VehicleMaintTypesLink(AssetId ASC)";
                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintTypesLink", "IX_GFI_AMM_VehicleMaintTypesLink_AssetId")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //--
                    sql = @"CREATE  INDEX IX_GFI_AMM_VehicleMaintTypesLink_MaintTypeId ON GFI_AMM_VehicleMaintTypesLink(MaintTypeId ASC)";
                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintTypesLink", "IX_GFI_AMM_VehicleMaintTypesLink_MaintTypeId")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    // -- Table: GFI_AMM_VehicleMaintItems
                    sql = @"
							begin
								execute immediate
								   'CREATE TABLE GFI_AMM_VehicleMaintItems(
									URI number(10) NOT NULL,
									MaintURI number(10) NOT NULL,
									ItemCode nvarchar2(50) NULL,
									Description nvarchar2(120) NULL,
									Quantity number(10) NULL,
									UnitCost number(18, 2) NULL,
									CreatedDate timestamp(3) NULL,
									CreatedBy nvarchar2(30) NULL,
									UpdatedDate timestamp(3) NULL,
									UpdatedBy nvarchar2(30) NULL,
								   CONSTRAINT PK_GFI_AMM_VehicleMaintItems PRIMARY KEY  
								  (URI )
								  )';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_AMM_VehicleMaintItems")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_AMM_VehicleMaintItems_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_AMM_VehicleMaintItems_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_AMM_VehicleMaintItems_seq_tr
					BEFORE INSERT ON GFI_AMM_VehicleMaintItems FOR EACH ROW
					WHEN(NEW.URI IS NULL)
					BEGIN
					SELECT GFI_AMM_VehicleMaintItems_seq.NEXTVAL INTO :NEW.URI FROM DUAL;
					END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //--
                    sql = @"CREATE  INDEX IX_GFI_AMM_VehicleMaintItems_ItemCode ON GFI_AMM_VehicleMaintItems(ItemCode ASC)";
                    //--
                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintItems", "IX_GFI_AMM_VehicleMaintItems_ItemCode")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //--
                    sql = @"CREATE  INDEX IX_GFI_AMM_VehicleMaintItems_MaintURI ON GFI_AMM_VehicleMaintItems(MaintURI ASC)";
                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintItems", "IX_GFI_AMM_VehicleMaintItems_MaintURI")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    //--
                    sql = @"ALTER TABLE GFI_AMM_AssetExtProVehicles  ADD  CONSTRAINT FK_GFI_AMM_Asset_ExtPro_Vehicles_GFI_AMM_VehicleTypes FOREIGN KEY(VehicleTypeId_cbo)
							REFERENCES GFI_AMM_VehicleTypes (VehicleTypeId)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_Asset_ExtPro_Vehicles_GFI_AMM_VehicleTypes", "GFI_AMM_AssetExtProVehicles")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_AssetExtProVehicles CHECK CONSTRAINT FK_GFI_AMM_Asset_ExtPro_Vehicles_GFI_AMM_VehicleTypes";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_Asset_ExtPro_Vehicles_GFI_AMM_VehicleTypes", "GFI_AMM_AssetExtProVehicles")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_AssetExtProXT ADD  CONSTRAINT FK_GFI_AMM_AssetExtProXT_GFI_AMM_AssetExtProFields FOREIGN KEY(FieldId)
							REFERENCES GFI_AMM_AssetExtProFields (FieldId)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_AssetExtProXT_GFI_AMM_AssetExtProFields", "GFI_AMM_AssetExtProXT")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_AssetExtProXT CHECK CONSTRAINT FK_GFI_AMM_AssetExtProXT_GFI_AMM_AssetExtProFields";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_AssetExtProXT_GFI_AMM_AssetExtProFields", "GFI_AMM_AssetExtProXT")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintenance  ADD  CONSTRAINT FK_GFI_AMM_VehicleMaint_GFI_AMM_VehicleMaintStatus FOREIGN KEY(MaintStatusId_cbo)
							REFERENCES GFI_AMM_VehicleMaintStatus (MaintStatusId)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaint_GFI_AMM_VehicleMaintStatus", "GFI_AMM_VehicleMaintenance")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintenance CHECK CONSTRAINT FK_GFI_AMM_VehicleMaint_GFI_AMM_VehicleMaintStatus";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaint_GFI_AMM_VehicleMaintStatus", "GFI_AMM_VehicleMaintenance")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintenance  ADD  CONSTRAINT FK_GFI_AMM_VehicleMaintenance_GFI_AMM_Asset_ExtPro_Vehicles FOREIGN KEY(AssetId)
							REFERENCES GFI_AMM_AssetExtProVehicles (AssetId)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintenance_GFI_AMM_Asset_ExtPro_Vehicles", "GFI_AMM_VehicleMaintenance")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintenance CHECK CONSTRAINT FK_GFI_AMM_VehicleMaintenance_GFI_AMM_Asset_ExtPro_Vehicles";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintenance_GFI_AMM_Asset_ExtPro_Vehicles", "GFI_AMM_VehicleMaintenance")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintenance  ADD  CONSTRAINT FK_GFI_AMM_VehicleMaintenance_GFI_AMM_VehicleMaintTypes FOREIGN KEY(MaintTypeId_cbo)
							REFERENCES GFI_AMM_VehicleMaintTypes (MaintTypeId)";

                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintenance_GFI_AMM_VehicleMaintTypes", "GFI_AMM_VehicleMaintenance")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintenance CHECK CONSTRAINT FK_GFI_AMM_VehicleMaintenance_GFI_AMM_VehicleMaintTypes";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintenance_GFI_AMM_VehicleMaintTypes", "GFI_AMM_VehicleMaintenance")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintItems  ADD  CONSTRAINT FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance FOREIGN KEY (MaintURI)
							REFERENCES GFI_AMM_VehicleMaintenance (URI)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance", "GFI_AMM_VehicleMaintItems")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintItems CHECK CONSTRAINT FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance", "GFI_AMM_VehicleMaintItems")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintTypes  ADD  CONSTRAINT FK_GFI_AMM_VehicleMaintTypes_GFI_AMM_VehicleMaintCat FOREIGN KEY(MaintCatId_cbo)
							REFERENCES GFI_AMM_VehicleMaintCat (MaintCatId)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintTypes_GFI_AMM_VehicleMaintCat", "GFI_AMM_VehicleMaintTypes")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintTypes CHECK CONSTRAINT FK_GFI_AMM_VehicleMaintTypes_GFI_AMM_VehicleMaintCat";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintTypes_GFI_AMM_VehicleMaintCat", "GFI_AMM_VehicleMaintTypes")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintTypesLink  ADD  CONSTRAINT FK_GFI_AMM_VehicleMaintTypesLink_GFI_AMM_AssetExtProVehicles FOREIGN KEY(AssetId)
							REFERENCES GFI_AMM_AssetExtProVehicles (AssetId)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintTypesLink_GFI_AMM_AssetExtProVehicles", "GFI_AMM_VehicleMaintTypesLink")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintTypesLink CHECK CONSTRAINT FK_GFI_AMM_VehicleMaintTypesLink_GFI_AMM_AssetExtProVehicles";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintTypesLink_GFI_AMM_AssetExtProVehicles", "GFI_AMM_VehicleMaintTypesLink")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintTypesLink  ADD  CONSTRAINT FK_GFI_AMM_VehicleMaintTypesLink_GFI_AMM_VehicleMaintTypes FOREIGN KEY(MaintTypeId)
							REFERENCES GFI_AMM_VehicleMaintTypes (MaintTypeId)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintTypesLink_GFI_AMM_VehicleMaintTypes", "GFI_AMM_VehicleMaintTypesLink")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //--
                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintTypesLink CHECK CONSTRAINT FK_GFI_AMM_VehicleMaintTypesLink_GFI_AMM_VehicleMaintTypes";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintTypesLink_GFI_AMM_VehicleMaintTypes", "GFI_AMM_VehicleMaintTypesLink")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    #endregion

                    #region indexes
                    //Index 2
                    sql = @"DROP INDEX IX_GFI_AMM_VehicleMaintCat_MaintCatDescription";
                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintCat", "IX_GFI_AMM_VehicleMaintCat_MaintCatDescription")).Rows[0][0].ToString() != "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE UNIQUE  INDEX IX_GFI_AMM_VehicleMaintCat_MaintCatDescription ON GFI_AMM_VehicleMaintCat(Description ASC)";
                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintCat", "IX_GFI_AMM_VehicleMaintCat_MaintCatDescription")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Index 3
                    sql = @"DROP INDEX IX_GFI_AMM_VehicleMaintStatus_Description";
                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintStatus", "IX_GFI_AMM_VehicleMaintStatus_Description")).Rows[0][0].ToString() != "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE UNIQUE  INDEX IX_GFI_AMM_VehicleMaintStatus_Description ON GFI_AMM_VehicleMaintStatus(Description ASC)";
                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintStatus", "IX_GFI_AMM_VehicleMaintStatus_Description")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Index 4
                    sql = @"DROP INDEX IX_GFI_AMM_VehicleMaintType_Description";
                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintType", "IX_GFI_AMM_VehicleMaintType_Description")).Rows[0][0].ToString() != "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE UNIQUE INDEX IX_GFI_AMM_VehicleMaintType_Description ON GFI_AMM_VehicleMaintTypes(Description ASC)";
                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintTypes", "IX_GFI_AMM_VehicleMaintTypes_Description")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    #endregion

                    #region inserts
                    //--------------- Insert Default Values -------------//
                    //
                    // 1. GFI_AMM_AssetExtProFields
                    // 2. GFI_AMM_InsCoverTypes
                    // 3. GFI_AMM_VehicleMaintCat
                    // 4. GFI_AMM_VehicleMaintStatus
                    // 5. GFI_AMM_VehicleMaintType
                    // 6. GFI_AMM_VehicleTypes
                    //
                    //---------------------------------------------------//

                    AssetExtProFieldService assetExtProFieldService = new AssetExtProFieldService();
                    AssetExtProField objAssetExtProFields = assetExtProFieldService.GetAssetExtProFieldByFieldName("FUEL", sConnStr);
                    if (objAssetExtProFields == null)
                    {
                        objAssetExtProFields = new AssetExtProField();
                        objAssetExtProFields.FieldId = 1;
                        objAssetExtProFields.AssetType = "VEHICLE";
                        objAssetExtProFields.Category = "COUNTERS";
                        objAssetExtProFields.FieldName = "FUEL";
                        objAssetExtProFields.Description = "FUEL";
                        objAssetExtProFields.Type = "STRING";
                        objAssetExtProFields.UnitOfMeasure = "LITRES";
                        objAssetExtProFields.KeepHistory = "Y";
                        objAssetExtProFields.DataRetentionDays = 99999999;
                        objAssetExtProFields.THWarning_Value = 99999999;
                        objAssetExtProFields.THWarning_TimeBased = 99999999;
                        objAssetExtProFields.THCritical_Value = 99999999;
                        objAssetExtProFields.THCritical_TimeBased = 99999999;
                        objAssetExtProFields.DisplayOrder = 1;
                        objAssetExtProFields.CreatedDate = DateTime.Now;
                        objAssetExtProFields.CreatedBy = usrInst.Username;
                        objAssetExtProFields.UpdatedDate = DateTime.Now;
                        objAssetExtProFields.UpdatedBy = usrInst.Username;
                        assetExtProFieldService.SaveAssetExtProField(objAssetExtProFields, null, sConnStr);
                    }

                    objAssetExtProFields = new AssetExtProField();
                    objAssetExtProFields = assetExtProFieldService.GetAssetExtProFieldByFieldName("CARGOTEMP", sConnStr);
                    if (objAssetExtProFields == null)
                    {
                        objAssetExtProFields = new AssetExtProField();
                        objAssetExtProFields.FieldId = 2;
                        objAssetExtProFields.AssetType = "VEHICLE";
                        objAssetExtProFields.Category = "COUNTERS";
                        objAssetExtProFields.FieldName = "CARGOTEMP";
                        objAssetExtProFields.Description = "CARGOTEMP";
                        objAssetExtProFields.Type = "STRING";
                        objAssetExtProFields.UnitOfMeasure = "LITRES";
                        objAssetExtProFields.KeepHistory = "Y";
                        objAssetExtProFields.DataRetentionDays = 99999999;
                        objAssetExtProFields.THWarning_Value = 99999999;
                        objAssetExtProFields.THWarning_TimeBased = 99999999;
                        objAssetExtProFields.THCritical_Value = 99999999;
                        objAssetExtProFields.THCritical_TimeBased = 99999999;
                        objAssetExtProFields.DisplayOrder = 2;
                        objAssetExtProFields.CreatedDate = DateTime.Now;
                        objAssetExtProFields.CreatedBy = usrInst.Username;
                        objAssetExtProFields.UpdatedDate = DateTime.Now;
                        objAssetExtProFields.UpdatedBy = usrInst.Username;
                        assetExtProFieldService.SaveAssetExtProField(objAssetExtProFields, null, sConnStr);
                    }

                    //---------------------------------------------------//

                    InsCoverType objInsCoverTypes = new InsCoverType();
                    InsCoverTypesService insCoverTypesService = new InsCoverTypesService();
                    objInsCoverTypes = insCoverTypesService.GetInsCoverTypeByDescription("COMPREHENSIVE", sConnStr);
                    if (objInsCoverTypes == null)
                    {
                        objInsCoverTypes = new InsCoverType();
                        objInsCoverTypes.CoverTypeId = 1;
                        objInsCoverTypes.Description = "COMPREHENSIVE";
                        objInsCoverTypes.CreatedDate = DateTime.Now;
                        objInsCoverTypes.CreatedBy = usrInst.Username;
                        objInsCoverTypes.UpdatedDate = DateTime.Now;
                        objInsCoverTypes.UpdatedBy = usrInst.Username;
                        insCoverTypesService.SaveInsCoverType(objInsCoverTypes, null, sConnStr);
                    }

                    objInsCoverTypes = new InsCoverType();
                    objInsCoverTypes = insCoverTypesService.GetInsCoverTypeByDescription("THIRD PARTY", sConnStr);
                    if (objInsCoverTypes == null)
                    {
                        objInsCoverTypes = new InsCoverType();
                        objInsCoverTypes.CoverTypeId = 2;
                        objInsCoverTypes.Description = "THIRD PARTY";
                        objInsCoverTypes.CreatedDate = DateTime.Now;
                        objInsCoverTypes.CreatedBy = usrInst.Username;
                        objInsCoverTypes.UpdatedDate = DateTime.Now;
                        objInsCoverTypes.UpdatedBy = usrInst.Username;
                        insCoverTypesService.SaveInsCoverType(objInsCoverTypes, null, sConnStr);
                    }

                    //---------------------------------------------------//

                    VehicleMaintCat objVehicleMaintCat = new VehicleMaintCat();
                    objVehicleMaintCat = vehicleMaintCatService.GetVehicleMaintCatByDescription("Maintenance", sConnStr);
                    if (objVehicleMaintCat == null)
                    {
                        objVehicleMaintCat = new VehicleMaintCat();
                        objVehicleMaintCat.MaintCatId = 1;
                        objVehicleMaintCat.Description = "MAINTENANCE";
                        objVehicleMaintCat.CreatedDate = DateTime.Now;
                        objVehicleMaintCat.CreatedBy = usrInst.Username;
                        objVehicleMaintCat.UpdatedDate = DateTime.Now;
                        objVehicleMaintCat.UpdatedBy = usrInst.Username;
                        vehicleMaintCatService.SaveVehicleMaintCat(objVehicleMaintCat, null, sConnStr);
                    }

                    objVehicleMaintCat = new VehicleMaintCat();
                    objVehicleMaintCat = vehicleMaintCatService.GetVehicleMaintCatByDescription("Inspection", sConnStr);
                    if (objVehicleMaintCat == null)
                    {
                        objVehicleMaintCat = new VehicleMaintCat();
                        objVehicleMaintCat.MaintCatId = 2;
                        objVehicleMaintCat.Description = "INSPECTION";
                        objVehicleMaintCat.CreatedDate = DateTime.Now;
                        objVehicleMaintCat.CreatedBy = usrInst.Username;
                        objVehicleMaintCat.UpdatedDate = DateTime.Now;
                        objVehicleMaintCat.UpdatedBy = usrInst.Username;
                        vehicleMaintCatService.SaveVehicleMaintCat(objVehicleMaintCat, null, sConnStr);
                    }

                    objVehicleMaintCat = new VehicleMaintCat();
                    objVehicleMaintCat = vehicleMaintCatService.GetVehicleMaintCatByDescription("Registration", sConnStr);
                    if (objVehicleMaintCat == null)
                    {
                        objVehicleMaintCat = new VehicleMaintCat();
                        objVehicleMaintCat.MaintCatId = 3;
                        objVehicleMaintCat.Description = "REGISTRATION";
                        objVehicleMaintCat.CreatedDate = DateTime.Now;
                        objVehicleMaintCat.CreatedBy = usrInst.Username;
                        objVehicleMaintCat.UpdatedDate = DateTime.Now;
                        objVehicleMaintCat.UpdatedBy = usrInst.Username;
                        vehicleMaintCatService.SaveVehicleMaintCat(objVehicleMaintCat, null, sConnStr);
                    }

                    objVehicleMaintCat = new VehicleMaintCat();
                    objVehicleMaintCat = vehicleMaintCatService.GetVehicleMaintCatByDescription("Insurance", sConnStr);
                    if (objVehicleMaintCat == null)
                    {
                        objVehicleMaintCat = new VehicleMaintCat();
                        objVehicleMaintCat.MaintCatId = 4;
                        objVehicleMaintCat.Description = "INSURANCE";
                        objVehicleMaintCat.CreatedDate = DateTime.Now;
                        objVehicleMaintCat.CreatedBy = usrInst.Username;
                        objVehicleMaintCat.UpdatedDate = DateTime.Now;
                        objVehicleMaintCat.UpdatedBy = usrInst.Username;
                        vehicleMaintCatService.SaveVehicleMaintCat(objVehicleMaintCat, null, sConnStr);
                    }

                    //---------------------------------------------------//

                    VehicleMaintType objVehicleMaintType = new VehicleMaintType();
                    objVehicleMaintType = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("SERVICING - 5000 KM", sConnStr);
                    if (objVehicleMaintType == null)
                    {
                        objVehicleMaintType = new VehicleMaintType();
                        objVehicleMaintType.MaintCatId_cbo = 1;
                        objVehicleMaintType.Description = "SERVICING - 5000 KM";
                        objVehicleMaintType.OccurrenceType = 2;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceDuration = 0;
                        objVehicleMaintType.OccurrenceDurationTh = 0;
                        objVehicleMaintType.OccurrenceKM = 0;
                        objVehicleMaintType.OccurrenceEngineHrs = 0;
                        objVehicleMaintType.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceKMTh = 0;
                        objVehicleMaintType.CreatedDate = DateTime.Now;
                        objVehicleMaintType.CreatedBy = usrInst.Username;
                        objVehicleMaintType.UpdatedDate = DateTime.Now;
                        objVehicleMaintType.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVehicleMaintType, true, sConnStr);
                    }

                    objVehicleMaintType = new VehicleMaintType();
                    objVehicleMaintType = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("SERVICING - 10000 KM OR 6 MONTHS", sConnStr);
                    if (objVehicleMaintType == null)
                    {
                        objVehicleMaintType = new VehicleMaintType();
                        objVehicleMaintType.MaintCatId_cbo = 1;
                        objVehicleMaintType.Description = "SERVICING - 10000 KM OR 6 MONTHS";
                        objVehicleMaintType.OccurrenceType = 2;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceDuration = 0;
                        objVehicleMaintType.OccurrenceDurationTh = 0;
                        objVehicleMaintType.OccurrenceKM = 0;
                        objVehicleMaintType.OccurrenceEngineHrs = 0;
                        objVehicleMaintType.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceKMTh = 0;
                        objVehicleMaintType.CreatedDate = DateTime.Now;
                        objVehicleMaintType.CreatedBy = usrInst.Username;
                        objVehicleMaintType.UpdatedDate = DateTime.Now;
                        objVehicleMaintType.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVehicleMaintType, true, sConnStr);
                    }

                    objVehicleMaintType = new VehicleMaintType();
                    objVehicleMaintType = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("VEHICLE INSPECTION", sConnStr);
                    if (objVehicleMaintType == null)
                    {
                        objVehicleMaintType = new VehicleMaintType();
                        objVehicleMaintType.MaintCatId_cbo = 2;
                        objVehicleMaintType.Description = "VEHICLE INSPECTION";
                        objVehicleMaintType.OccurrenceType = 2;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceDuration = 0;
                        objVehicleMaintType.OccurrenceDurationTh = 0;
                        objVehicleMaintType.OccurrenceKM = 0;
                        objVehicleMaintType.OccurrenceEngineHrs = 0;
                        objVehicleMaintType.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceKMTh = 0;
                        objVehicleMaintType.CreatedDate = DateTime.Now;
                        objVehicleMaintType.CreatedBy = usrInst.Username;
                        objVehicleMaintType.UpdatedDate = DateTime.Now;
                        objVehicleMaintType.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVehicleMaintType, true, sConnStr);
                    }

                    objVehicleMaintType = new VehicleMaintType();
                    objVehicleMaintType = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("INSURANCE - 6 MONTHS", sConnStr);
                    if (objVehicleMaintType == null)
                    {
                        objVehicleMaintType = new VehicleMaintType();
                        objVehicleMaintType.MaintCatId_cbo = 4;
                        objVehicleMaintType.Description = "INSURANCE - 6 MONTHS";
                        objVehicleMaintType.OccurrenceType = 2;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceDuration = 0;
                        objVehicleMaintType.OccurrenceDurationTh = 0;
                        objVehicleMaintType.OccurrenceKM = 0;
                        objVehicleMaintType.OccurrenceEngineHrs = 0;
                        objVehicleMaintType.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceKMTh = 0;
                        objVehicleMaintType.CreatedDate = DateTime.Now;
                        objVehicleMaintType.CreatedBy = usrInst.Username;
                        objVehicleMaintType.UpdatedDate = DateTime.Now;
                        objVehicleMaintType.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVehicleMaintType, true, sConnStr);
                    }

                    objVehicleMaintType = new VehicleMaintType();
                    objVehicleMaintType = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("INSURANCE - 12 MONTHS", sConnStr);
                    if (objVehicleMaintType == null)
                    {
                        objVehicleMaintType = new VehicleMaintType();
                        objVehicleMaintType.MaintCatId_cbo = 4;
                        objVehicleMaintType.Description = "INSURANCE - 12 MONTHS";
                        objVehicleMaintType.OccurrenceType = 2;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceDuration = 0;
                        objVehicleMaintType.OccurrenceDurationTh = 0;
                        objVehicleMaintType.OccurrenceKM = 0;
                        objVehicleMaintType.OccurrenceEngineHrs = 0;
                        objVehicleMaintType.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceKMTh = 0;
                        objVehicleMaintType.CreatedDate = DateTime.Now;
                        objVehicleMaintType.CreatedBy = usrInst.Username;
                        objVehicleMaintType.UpdatedDate = DateTime.Now;
                        objVehicleMaintType.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVehicleMaintType, true, sConnStr);
                    }

                    objVehicleMaintType = new VehicleMaintType();
                    objVehicleMaintType = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("TYRE CHANGE - 12 MONTHS OR 50,000 KM", sConnStr);
                    if (objVehicleMaintType == null)
                    {
                        objVehicleMaintType = new VehicleMaintType();
                        objVehicleMaintType.MaintCatId_cbo = 1;
                        objVehicleMaintType.Description = "TYRE CHANGE - 12 MONTHS OR 50,000 KM";
                        objVehicleMaintType.OccurrenceType = 2;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceDuration = 0;
                        objVehicleMaintType.OccurrenceDurationTh = 0;
                        objVehicleMaintType.OccurrenceKM = 0;
                        objVehicleMaintType.OccurrenceEngineHrs = 0;
                        objVehicleMaintType.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceKMTh = 0;
                        objVehicleMaintType.CreatedDate = DateTime.Now;
                        objVehicleMaintType.CreatedBy = usrInst.Username;
                        objVehicleMaintType.UpdatedDate = DateTime.Now;
                        objVehicleMaintType.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVehicleMaintType, true, sConnStr);
                    }

                    objVehicleMaintType = new VehicleMaintType();
                    objVehicleMaintType = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("VEHICLE REGISTRATION - 12 MONTHS", sConnStr);
                    if (objVehicleMaintType == null)
                    {
                        objVehicleMaintType = new VehicleMaintType();
                        objVehicleMaintType.MaintCatId_cbo = 3;
                        objVehicleMaintType.Description = "VEHICLE REGISTRATION - 12 MONTHS";
                        objVehicleMaintType.OccurrenceType = 2;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceDuration = 0;
                        objVehicleMaintType.OccurrenceDurationTh = 0;
                        objVehicleMaintType.OccurrenceKM = 0;
                        objVehicleMaintType.OccurrenceEngineHrs = 0;
                        objVehicleMaintType.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceKMTh = 0;
                        objVehicleMaintType.CreatedDate = DateTime.Now;
                        objVehicleMaintType.CreatedBy = usrInst.Username;
                        objVehicleMaintType.UpdatedDate = DateTime.Now;
                        objVehicleMaintType.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVehicleMaintType, true, sConnStr);
                    }

                    objVehicleMaintType = new VehicleMaintType();
                    objVehicleMaintType = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("VEHICLE WASH", sConnStr);
                    if (objVehicleMaintType == null)
                    {
                        objVehicleMaintType = new VehicleMaintType();
                        objVehicleMaintType.MaintCatId_cbo = 1;
                        objVehicleMaintType.Description = "VEHICLE WASH";
                        objVehicleMaintType.OccurrenceType = 2;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceDuration = 0;
                        objVehicleMaintType.OccurrenceDurationTh = 0;
                        objVehicleMaintType.OccurrenceKM = 0;
                        objVehicleMaintType.OccurrenceEngineHrs = 0;
                        objVehicleMaintType.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceKMTh = 0;
                        objVehicleMaintType.CreatedDate = DateTime.Now;
                        objVehicleMaintType.CreatedBy = usrInst.Username;
                        objVehicleMaintType.UpdatedDate = DateTime.Now;
                        objVehicleMaintType.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVehicleMaintType, true, sConnStr);
                    }

                    //---------------------------------------------------//

                    VehicleMaintStatus objVehicleMaintStatus = new VehicleMaintStatus();
                    objVehicleMaintStatus = vehicleMaintStatusService.GetVehicleMaintStatusByDescription("Overdue", sConnStr);
                    if (objVehicleMaintStatus == null)
                    {
                        objVehicleMaintStatus = new VehicleMaintStatus();
                        objVehicleMaintStatus.MaintStatusId = 1;
                        objVehicleMaintStatus.Description = "OVERDUE";
                        objVehicleMaintStatus.SortOrder = 1;
                        objVehicleMaintStatus.CreatedDate = DateTime.Now;
                        objVehicleMaintStatus.CreatedBy = usrInst.Username;
                        objVehicleMaintStatus.UpdatedDate = DateTime.Now;
                        objVehicleMaintStatus.UpdatedBy = usrInst.Username;
                        vehicleMaintStatusService.SaveVehicleMaintStatus(objVehicleMaintStatus, null, sConnStr);
                    }

                    objVehicleMaintStatus = new VehicleMaintStatus();
                    objVehicleMaintStatus = vehicleMaintStatusService.GetVehicleMaintStatusByDescription("To Schedule", sConnStr);
                    if (objVehicleMaintStatus == null)
                    {
                        objVehicleMaintStatus = new VehicleMaintStatus();
                        objVehicleMaintStatus.MaintStatusId = 2;
                        objVehicleMaintStatus.Description = "TO SCHEDULE";
                        objVehicleMaintStatus.SortOrder = 2;
                        objVehicleMaintStatus.CreatedDate = DateTime.Now;
                        objVehicleMaintStatus.CreatedBy = usrInst.Username;
                        objVehicleMaintStatus.UpdatedDate = DateTime.Now;
                        objVehicleMaintStatus.UpdatedBy = usrInst.Username;
                        vehicleMaintStatusService.SaveVehicleMaintStatus(objVehicleMaintStatus, null, sConnStr);
                    }

                    objVehicleMaintStatus = new VehicleMaintStatus();
                    objVehicleMaintStatus = vehicleMaintStatusService.GetVehicleMaintStatusByDescription("Scheduled", sConnStr);
                    if (objVehicleMaintStatus == null)
                    {
                        objVehicleMaintStatus = new VehicleMaintStatus();
                        objVehicleMaintStatus.MaintStatusId = 3;
                        objVehicleMaintStatus.Description = "SCHEDULED";
                        objVehicleMaintStatus.SortOrder = 3;
                        objVehicleMaintStatus.CreatedDate = DateTime.Now;
                        objVehicleMaintStatus.CreatedBy = usrInst.Username;
                        objVehicleMaintStatus.UpdatedDate = DateTime.Now;
                        objVehicleMaintStatus.UpdatedBy = usrInst.Username;
                        vehicleMaintStatusService.SaveVehicleMaintStatus(objVehicleMaintStatus, null, sConnStr);
                    }

                    objVehicleMaintStatus = new VehicleMaintStatus();
                    objVehicleMaintStatus = vehicleMaintStatusService.GetVehicleMaintStatusByDescription("In Progress", sConnStr);
                    if (objVehicleMaintStatus == null)
                    {
                        objVehicleMaintStatus = new VehicleMaintStatus();
                        objVehicleMaintStatus.MaintStatusId = 4;
                        objVehicleMaintStatus.Description = "IN PROGRESS";
                        objVehicleMaintStatus.SortOrder = 4;
                        objVehicleMaintStatus.CreatedDate = DateTime.Now;
                        objVehicleMaintStatus.CreatedBy = usrInst.Username;
                        objVehicleMaintStatus.UpdatedDate = DateTime.Now;
                        objVehicleMaintStatus.UpdatedBy = usrInst.Username;
                        vehicleMaintStatusService.SaveVehicleMaintStatus(objVehicleMaintStatus, null, sConnStr);
                    }

                    objVehicleMaintStatus = new VehicleMaintStatus();
                    objVehicleMaintStatus = vehicleMaintStatusService.GetVehicleMaintStatusByDescription("Completed", sConnStr);
                    if (objVehicleMaintStatus == null)
                    {
                        objVehicleMaintStatus = new VehicleMaintStatus();
                        objVehicleMaintStatus.MaintStatusId = 5;
                        objVehicleMaintStatus.Description = "COMPLETED";
                        objVehicleMaintStatus.SortOrder = 5;
                        objVehicleMaintStatus.CreatedDate = DateTime.Now;
                        objVehicleMaintStatus.CreatedBy = usrInst.Username;
                        objVehicleMaintStatus.UpdatedDate = DateTime.Now;
                        objVehicleMaintStatus.UpdatedBy = usrInst.Username;
                        vehicleMaintStatusService.SaveVehicleMaintStatus(objVehicleMaintStatus, null, sConnStr);
                    }

                    objVehicleMaintStatus = new VehicleMaintStatus();
                    objVehicleMaintStatus = vehicleMaintStatusService.GetVehicleMaintStatusByDescription("Valid", sConnStr);
                    if (objVehicleMaintStatus == null)
                    {
                        objVehicleMaintStatus = new VehicleMaintStatus();
                        objVehicleMaintStatus.MaintStatusId = 6;
                        objVehicleMaintStatus.Description = "VALID";
                        objVehicleMaintStatus.SortOrder = 6;
                        objVehicleMaintStatus.CreatedDate = DateTime.Now;
                        objVehicleMaintStatus.CreatedBy = usrInst.Username;
                        objVehicleMaintStatus.UpdatedDate = DateTime.Now;
                        objVehicleMaintStatus.UpdatedBy = usrInst.Username;
                        vehicleMaintStatusService.SaveVehicleMaintStatus(objVehicleMaintStatus, null, sConnStr);
                    }

                    objVehicleMaintStatus = new VehicleMaintStatus();
                    objVehicleMaintStatus = vehicleMaintStatusService.GetVehicleMaintStatusByDescription("Expired", sConnStr);
                    if (objVehicleMaintStatus == null)
                    {
                        objVehicleMaintStatus = new VehicleMaintStatus();
                        objVehicleMaintStatus.MaintStatusId = 7;
                        objVehicleMaintStatus.Description = "EXPIRED";
                        objVehicleMaintStatus.SortOrder = 7;
                        objVehicleMaintStatus.CreatedDate = DateTime.Now;
                        objVehicleMaintStatus.CreatedBy = usrInst.Username;
                        objVehicleMaintStatus.UpdatedDate = DateTime.Now;
                        objVehicleMaintStatus.UpdatedBy = usrInst.Username;
                        vehicleMaintStatusService.SaveVehicleMaintStatus(objVehicleMaintStatus, null, sConnStr);
                    }

                    //---------------------------------------------------//

                    VehicleType objVehicleTypes = new VehicleType();
                    VehicleTypesService vehicleTypesService = new VehicleTypesService();
                    objVehicleTypes = vehicleTypesService.GetVehicleTypesByDescription("Car", sConnStr);
                    if (objVehicleTypes == null)
                    {
                        objVehicleTypes = new VehicleType();
                        objVehicleTypes.VehicleTypeId = 1;
                        objVehicleTypes.Description = "CAR";
                        objVehicleTypes.CreatedDate = DateTime.Now;
                        objVehicleTypes.CreatedBy = usrInst.Username;
                        objVehicleTypes.UpdatedDate = DateTime.Now;
                        objVehicleTypes.UpdatedBy = usrInst.Username;
                        vehicleTypesService.SaveVehicleTypes(objVehicleTypes, null, sConnStr);
                    }

                    objVehicleTypes = new VehicleType();
                    objVehicleTypes = vehicleTypesService.GetVehicleTypesByDescription("Van", sConnStr);
                    if (objVehicleTypes == null)
                    {
                        objVehicleTypes = new VehicleType();
                        objVehicleTypes.VehicleTypeId = 2;
                        objVehicleTypes.Description = "VAN";
                        objVehicleTypes.CreatedDate = DateTime.Now;
                        objVehicleTypes.CreatedBy = usrInst.Username;
                        objVehicleTypes.UpdatedDate = DateTime.Now;
                        objVehicleTypes.UpdatedBy = usrInst.Username;
                        vehicleTypesService.SaveVehicleTypes(objVehicleTypes, null, sConnStr);
                    }

                    objVehicleTypes = new VehicleType();
                    objVehicleTypes = vehicleTypesService.GetVehicleTypesByDescription("Truck", sConnStr);
                    if (objVehicleTypes == null)
                    {
                        objVehicleTypes = new VehicleType();
                        objVehicleTypes.VehicleTypeId = 3;
                        objVehicleTypes.Description = "TRUCK";
                        objVehicleTypes.CreatedDate = DateTime.Now;
                        objVehicleTypes.CreatedBy = usrInst.Username;
                        objVehicleTypes.UpdatedDate = DateTime.Now;
                        objVehicleTypes.UpdatedBy = usrInst.Username;
                        vehicleTypesService.SaveVehicleTypes(objVehicleTypes, null, sConnStr);
                    }

                    objVehicleTypes = new VehicleType();
                    objVehicleTypes = vehicleTypesService.GetVehicleTypesByDescription("Bus", sConnStr);
                    if (objVehicleTypes == null)
                    {
                        objVehicleTypes = new VehicleType();
                        objVehicleTypes.VehicleTypeId = 4;
                        objVehicleTypes.Description = "BUS";
                        objVehicleTypes.CreatedDate = DateTime.Now;
                        objVehicleTypes.CreatedBy = usrInst.Username;
                        objVehicleTypes.UpdatedDate = DateTime.Now;
                        objVehicleTypes.UpdatedBy = usrInst.Username;
                        vehicleTypesService.SaveVehicleTypes(objVehicleTypes, null, sConnStr);
                    }

                    objVehicleTypes = new VehicleType();
                    objVehicleTypes = vehicleTypesService.GetVehicleTypesByDescription("Trailer", sConnStr);
                    if (objVehicleTypes == null)
                    {
                        objVehicleTypes = new VehicleType();
                        objVehicleTypes.VehicleTypeId = 5;
                        objVehicleTypes.Description = "TRAILER";
                        objVehicleTypes.CreatedDate = DateTime.Now;
                        objVehicleTypes.CreatedBy = usrInst.Username;
                        objVehicleTypes.UpdatedDate = DateTime.Now;
                        objVehicleTypes.UpdatedBy = usrInst.Username;
                        vehicleTypesService.SaveVehicleTypes(objVehicleTypes, null, sConnStr);
                    }

                    objVehicleTypes = new VehicleType();
                    objVehicleTypes = vehicleTypesService.GetVehicleTypesByDescription("Motor Cycle", sConnStr);
                    if (objVehicleTypes == null)
                    {
                        objVehicleTypes = new VehicleType();
                        objVehicleTypes.VehicleTypeId = 6;
                        objVehicleTypes.Description = "MOTOR CYCLE";
                        objVehicleTypes.CreatedDate = DateTime.Now;
                        objVehicleTypes.CreatedBy = usrInst.Username;
                        objVehicleTypes.UpdatedDate = DateTime.Now;
                        objVehicleTypes.UpdatedBy = usrInst.Username;
                        vehicleTypesService.SaveVehicleTypes(objVehicleTypes, null, sConnStr);
                    }
                    #endregion

                    InsertDBUpdate(strUpd, "Asset Management");
                }

                #endregion
                #region Update 19. Assets Management Stored Procedures
                strUpd = "Rez000019";
                if (!CheckUpdateExist(strUpd))
                {
                    InsertDBUpdate(strUpd, "Asset Management Stored Procedures");
                }
                #endregion
                #region Update 20. Rules and Exceptions
                strUpd = "Rez000020";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_FLT_AssetDeviceMap add Aux1Name_cbo int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "Aux1Name_cbo")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_FLT_AssetDeviceMap add Aux2Name_cbo int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "Aux2Name_cbo")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_FLT_AssetDeviceMap add Aux3Name_cbo int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "Aux3Name_cbo")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_FLT_AssetDeviceMap add Aux4Name_cbo int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "Aux4Name_cbo")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_FLT_AssetDeviceMap add Aux5Name_cbo int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "Aux5Name_cbo")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_FLT_AssetDeviceMap add Aux6Name_cbo int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "Aux6Name_cbo")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "Rules and Exceptions updated");
                }
                #endregion

                #region Update 21. Assets Encrypted
                strUpd = "Rez000021";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "Alter table GFI_FLT_Asset modify (AssetNumber nvarchar2(50))";
                    dt = GetDataDT(GetFieldSchema("GFI_FLT_Asset", "AssetNumber"));
                    if (dt.Rows[0]["DATA_LENGTH"].ToString() != "50")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_AMM_VehicleMaintItems drop constraint FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance", "GFI_AMM_VehicleMaintItems")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE Tmp_GFI_AMM_VehicleMaintItems drop constraint PK_Tmp_GFI_AMM_VehicleMaintItems ";
                    if (GetDataDT(ExistsSQLConstraint("PK_Tmp_GFI_AMM_VehicleMaintItems", "Tmp_GFI_AMM_VehicleMaintItems")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_AMM_VehicleMaintItems drop constraint PK_Tmp_GFI_AMM_VehicleMaintItems";
                    if (GetDataDT(ExistsSQLConstraint("PK_Tmp_GFI_AMM_VehicleMaintItems", "GFI_AMM_VehicleMaintItems")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"
							begin
								execute immediate
									'CREATE TABLE Tmp_GFI_AMM_VehicleMaintItems
									(
										URI number(10) NOT NULL,
										MaintURI number(10) NOT NULL,
										ItemCode nvarchar2(50) NULL,
										Description nvarchar2(120) NULL,
										Quantity number(18, 2) NULL,
										UnitCost number(18, 2) NULL,
										CreatedDate timestamp(3) NULL,
										CreatedBy nvarchar2(30) NULL,
										UpdatedDate timestamp(3) NULL,
										UpdatedBy nvarchar2(30) NULL,
										CONSTRAINT PK_Tmp_GFI_AMM_VehicleMaintItems PRIMARY KEY (URI)
									)';
							end;";
                    if (GetDataDT(ExistTableSql("Tmp_GFI_AMM_VehicleMaintItems")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE Tmp_GFI_AMM_VehicleMaintItems_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("Tmp_GFI_AMM_VehicleMaintItems_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "DROP TRIGGER Tmp_GFI_AMM_VehicleMaintItems_seq_tr";
                    if (bExistTrigger("Tmp_GFI_AMM_VehicleMaintItems_seq_tr"))
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER Tmp_GFI_AMM_VehicleMaintItems_seq_tr
					            BEFORE INSERT ON Tmp_GFI_AMM_VehicleMaintItems FOR EACH ROW
					            WHEN(NEW.URI IS NULL)
					            BEGIN
					                SELECT Tmp_GFI_AMM_VehicleMaintItems_seq.NEXTVAL INTO :NEW.URI FROM DUAL;
					            END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"INSERT INTO Tmp_GFI_AMM_VehicleMaintItems(URI, MaintURI, ItemCode, Description, Quantity, UnitCost, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy)
								SELECT URI, MaintURI, ItemCode, Description, Quantity, UnitCost, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy FROM Tmp_GFI_AMM_VehicleMaintItems";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"DROP TABLE GFI_AMM_VehicleMaintItems";
                    if (GetDataDT(ExistTableSql("GFI_AMM_VehicleMaintItems")).Rows[0][0].ToString() != "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"alter table Tmp_GFI_AMM_VehicleMaintItems rename to GFI_AMM_VehicleMaintItems";
                    if (GetDataDT(ExistTableSql("GFI_AMM_VehicleMaintItems")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE INDEX IX_ItemCode ON GFI_AMM_VehicleMaintItems (ItemCode)";
                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintItems", "IX_ItemCode")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE  INDEX IX_MaintURI ON GFI_AMM_VehicleMaintItems (MaintURI)";
                    if (GetDataDT(ExistsIndex("GFI_AMM_VehicleMaintItems", "IX_MaintURI")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintItems 
							ADD CONSTRAINT FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance 
								FOREIGN KEY (MaintURI) 
								REFERENCES GFI_AMM_VehicleMaintenance(URI)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance", "GFI_AMM_VehicleMaintItems")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "Assets Encrypted");
                }
                #endregion
                #region Update 22. Group Matrix updated
                strUpd = "Rez000022";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"update GFI_SYS_GroupMatrix set GMDescription = '### ROOT ###' where GMID = 1";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"update GFI_SYS_GroupMatrix set GMDescription = '### Security ###' where GMID = 2";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"update GFI_SYS_GroupMatrix set GMDescription = '### All Clients ###' where GMID = 3";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"update GFI_SYS_GroupMatrix set ParentGMID = 3 where ParentGMID = 1 and GMID > 3";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "Group Matrix updated");
                }
                #endregion
                #region Update 23. Rule Group Matrix
                strUpd = "Rez000023";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"
						begin
							execute immediate
								'CREATE TABLE GFI_SYS_GroupMatrixRule
								(
									MID NUMBER(10) NOT NULL
									, GMID NUMBER(10) NOT NULL
									, iID NUMBER(10) NOT NULL
									, CONSTRAINT PK_GFI_SYS_GroupMatrixRule PRIMARY KEY  (MID)
								)';
						end;";
                    if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrixRule")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_SYS_GroupMatrixRule_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_SYS_GroupMatrixRule_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    sql = @"CREATE OR REPLACE TRIGGER GFI_SYS_GroupMatrixRule_seq_tr
								BEFORE INSERT ON GFI_SYS_GroupMatrixRule FOR EACH ROW
								WHEN(NEW.MID IS NULL)
								BEGIN
									SELECT GFI_SYS_GroupMatrixRule_seq.NEXTVAL INTO :NEW.MID FROM DUAL;
								END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixRule 
							ADD CONSTRAINT FK_GFI_SYS_GroupMatrixRule_GFI_SYS_GroupMatrix 
								FOREIGN KEY (GMID) 
								REFERENCES GFI_SYS_GroupMatrix (GMID) ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixRule_GFI_SYS_GroupMatrix", "GFI_SYS_GroupMatrixRule")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixRule 
							ADD CONSTRAINT FK_GFI_SYS_GroupMatrixRule_GFI_GPS_Rules 
								FOREIGN KEY(iID) 
								REFERENCES GFI_GPS_Rules(RuleID) 
						 ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixRule_GFI_GPS_Rules", "GFI_SYS_GroupMatrixRule")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "Rule Group Matrix");
                }
                #endregion
                #region Update 24. Admin User
                strUpd = "Rez000024";
                if (!CheckUpdateExist(strUpd))
                {
                    User u = new User();
                    u = userService.GetUserByeMail("ADMIN@naveo.mu", "SysInitPass", sConnStr, false, true);
                    u.Password = "Core4dmin@123";
                    userService.SaveUser(u, false, sConnStr);

                    InsertDBUpdate(strUpd, "Admin User");
                }
                #endregion
                #region Update 25. Zone Types
                strUpd = "Rez000025";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_FLT_ZoneType
                        (
	                        ZoneTypeID number(10)  NOT NULL,
	                        sDescription varchar(50) NULL,
	                        sComments nvarchar(1024) NULL,
                            isSystem nvarchar (1) DEFAULT 0 NOT NULL,
                            CONSTRAINT PK_GFI_FLT_ZoneType PRIMARY KEY (ZoneTypeID)
                        )";
                    CreateTable(sql, "GFI_FLT_ZoneType", "ZoneTypeID");

                    sql = @"CREATE TABLE GFI_SYS_GroupMatrixZoneType
                        (
	                        MID NUMBER(10)  NOT NULL
	                        , GMID NUMBER(10) NOT NULL
	                        , iID NUMBER(10) NOT NULL
                            , CONSTRAINT PK_GFI_SYS_GroupMatrixZoneType PRIMARY KEY (MID)
                        )";
                    CreateTable(sql, "GFI_SYS_GroupMatrixZoneType", "MID");

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixZoneType 
                            ADD CONSTRAINT FK_GFI_SYS_GroupMatrixZoneType_GFI_SYS_GroupMatrix 
                                FOREIGN KEY (GMID) 
                                REFERENCES GFI_SYS_GroupMatrix (GMID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixZoneType_GFI_SYS_GroupMatrix", "GFI_SYS_GroupMatrixZoneType")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixZoneType 
                            ADD CONSTRAINT FK_GFI_SYS_GroupMatrixZoneType_GFI_FLT_ZoneType 
                            FOREIGN KEY(iID) 
                            REFERENCES GFI_FLT_ZoneType(ZoneTypeID) 
	                     ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixZoneType_GFI_FLT_ZoneType", "GFI_SYS_GroupMatrixZoneType")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE TABLE GFI_FLT_ZoneHeadZoneType
                        (
	                        ZoneHeadID number(10) NOT NULL,
	                        ZoneTypeID number(10) NOT NULL,
	                        iID number(10)  NOT NULL,
                            CONSTRAINT PK_GFI_FLT_ZoneHeadZoneType PRIMARY KEY (iID)
                        )";
                    CreateTable(sql, "GFI_FLT_ZoneHeadZoneType", "iID");

                    sql = @"ALTER TABLE GFI_FLT_ZoneHeadZoneType 
                            ADD CONSTRAINT FK_GFI_FLT_ZoneHeadZoneType_GFI_FLT_ZoneHeader
                                FOREIGN KEY(ZoneHeadID)
                                REFERENCES GFI_FLT_ZoneHeader (ZoneID)
                            ON DELETE CASCADE";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ZoneHeadZoneType_GFI_FLT_ZoneHeader", "GFI_FLT_ZoneHeadZoneType")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_ZoneHeadZoneType   
                            ADD CONSTRAINT FK_GFI_FLT_ZoneHeadZoneType_GFI_FLT_ZoneType
                                FOREIGN KEY(ZoneTypeID)
                                REFERENCES GFI_FLT_ZoneType (ZoneTypeID)
                            ON DELETE CASCADE";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ZoneHeadZoneType_GFI_FLT_ZoneType", "GFI_FLT_ZoneHeadZoneType")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    GroupMatrix gmRoot = new GroupMatrix();
                    gmRoot = new GroupMatrixService().GetRoot(sConnStr);
                    ZoneTypeService zoneTypeService = new ZoneTypeService();
                    ZoneType zt = zoneTypeService.GetZoneTypeByComments("1", sConnStr);
                    if (zt == null)
                    {
                        zt = new ZoneType();
                        zt.sDescription = "Address Lookup Zone";
                        zt.sComments = "1";
                        zt.isSystem = "1";

                        Matrix m = new Matrix();
                        m.GMID = gmRoot.gmID;
                        m.oprType = DataRowState.Added;
                        zt.lMatrix.Add(m);

                        zoneTypeService.SaveZoneType(zt, true, sConnStr);
                    }

                    zt = zoneTypeService.GetZoneTypeByComments("2", sConnStr);
                    if (zt == null)
                    {
                        zt = new ZoneType();
                        zt.sDescription = "Customer Zone";
                        zt.sComments = "2";
                        zt.isSystem = "1";

                        Matrix m = new Matrix();
                        m.GMID = gmRoot.gmID;
                        m.oprType = DataRowState.Added;
                        zt.lMatrix.Add(m);

                        zoneTypeService.SaveZoneType(zt, true, sConnStr);
                    }

                    zt = zoneTypeService.GetZoneTypeByComments("3", sConnStr);
                    if (zt == null)
                    {
                        zt = new ZoneType();
                        zt.sDescription = "Office/Depot Zone";
                        zt.sComments = "3";
                        zt.isSystem = "1";

                        Matrix m = new Matrix();
                        m.GMID = gmRoot.gmID;
                        m.oprType = DataRowState.Added;
                        zt.lMatrix.Add(m);

                        zoneTypeService.SaveZoneType(zt, true, sConnStr);
                    }

                    zt = zoneTypeService.GetZoneTypeByComments("4", sConnStr);
                    if (zt == null)
                    {
                        zt = new ZoneType();
                        zt.sDescription = "Home Zone";
                        zt.sComments = "4";
                        zt.isSystem = "1";

                        Matrix m = new Matrix();
                        m.GMID = gmRoot.gmID;
                        m.oprType = DataRowState.Added;
                        zt.lMatrix.Add(m);

                        zoneTypeService.SaveZoneType(zt, true, sConnStr);
                    }

                    if (GetDataDT(ExistColumnSql("GFI_FLT_ZoneHeader", "ZoneTypeID")).Rows[0][0].ToString() == "1")
                    {
                        sql = "select * from GFI_FLT_ZoneHeader";
                        DataTable dtz = GetDataDT(sql);
                        Boolean b = true;
                        foreach (DataRow drz in dtz.Rows)
                        {
                            int cmID = (int)drz["ZoneTypeID"];
                            zt = zoneTypeService.GetZoneTypeByComments(cmID.ToString(), sConnStr);
                            if (zt == null)
                                zt = zoneTypeService.GetZoneTypeByComments("1", sConnStr);

                            ZoneHeadZoneType zhzt = new ZoneHeadZoneType();
                            zhzt.ZoneHeadID = (int)(drz["ZoneID"]);
                            zhzt.ZoneTypeID = zt.ZoneTypeID;
                            b = b & new ZoneHeadZoneTypeService().SaveZoneHeadZoneType(zhzt, null, sConnStr);
                        }
                    }
                    InsertDBUpdate(strUpd, "Zone Types");
                }
                #endregion

                #region Update 26. Vehicle Maintenance Stored Proc
                strUpd = "Rez000026";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE  INDEX IX_GFI_GPS_GPSData_AssetID_DateTimeGPS ON GFI_GPS_GPSData 
						(
							AssetID ASC,
							DateTimeGPS_UTC ASC
						)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSData", "IX_GFI_GPS_GPSData_AssetID_DateTimeGPS")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE  INDEX IX_GFI_FLT_AssetDeviceMap_GFI_FLT_AssetDeviceMap_DeviceId_Status ON GFI_FLT_AssetDeviceMap (DeviceId ASC,Status_b ASC)";
                    if (GetDataDT(ExistsIndex("GFI_FLT_AssetDeviceMap", "IX_GFI_FLT_AssetDeviceMap_GFI_FLT_AssetDeviceMap_DeviceId_Status")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE  INDEX IX_GFI_FLT_Driver_GFI_FLT_Driver_iButton ON GFI_FLT_Driver (iButton ASC)";
                    if (GetDataDT(ExistsIndex("GFI_FLT_Driver", "IX_GFI_FLT_Driver_GFI_FLT_Driver_iButton")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    Globals.uLogin = usrInst;
                    #region Inserts
                    //----------------------- GFI_AMM_VehicleMaintCat ----------------------------//
                    VehicleType objVehicleTypes = new VehicleType();
                    VehicleTypesService vehicleTypesService = new VehicleTypesService();
                    objVehicleTypes = vehicleTypesService.GetVehicleTypesByDescription("VAN", sConnStr);
                    if (objVehicleTypes == null)
                    {
                        objVehicleTypes = new VehicleType();
                        objVehicleTypes.VehicleTypeId = 2;
                        objVehicleTypes.Description = "VAN";
                        objVehicleTypes.CreatedDate = DateTime.Now;
                        objVehicleTypes.CreatedBy = usrInst.Username;
                        objVehicleTypes.UpdatedDate = DateTime.Now;
                        objVehicleTypes.UpdatedBy = usrInst.Username;
                        vehicleTypesService.SaveVehicleTypes(objVehicleTypes, null, sConnStr);
                    }

                    VehicleMaintCat objVehicleMaintCat = new VehicleMaintCat();
                    objVehicleMaintCat = vehicleMaintCatService.GetVehicleMaintCatByDescription("MAINTENANCE", sConnStr);
                    if (objVehicleMaintCat == null)
                    {
                        objVehicleMaintCat = new VehicleMaintCat();
                        objVehicleMaintCat.MaintCatId = 1;
                        objVehicleMaintCat.Description = "MAINTENANCE";
                        objVehicleMaintCat.CreatedDate = DateTime.Now;
                        objVehicleMaintCat.CreatedBy = usrInst.Username;
                        objVehicleMaintCat.UpdatedDate = DateTime.Now;
                        objVehicleMaintCat.UpdatedBy = usrInst.Username;
                        vehicleMaintCatService.SaveVehicleMaintCat(objVehicleMaintCat, null, sConnStr);
                    }

                    objVehicleMaintCat = new VehicleMaintCat();
                    objVehicleMaintCat = vehicleMaintCatService.GetVehicleMaintCatByDescription("UNPLANNED MAINTENANCE", sConnStr);
                    if (objVehicleMaintCat == null)
                    {
                        objVehicleMaintCat = new VehicleMaintCat();
                        objVehicleMaintCat.MaintCatId = 5;
                        objVehicleMaintCat.Description = "UNPLANNED MAINTENANCE";
                        objVehicleMaintCat.CreatedDate = DateTime.Now;
                        objVehicleMaintCat.CreatedBy = usrInst.Username;
                        objVehicleMaintCat.UpdatedDate = DateTime.Now;
                        objVehicleMaintCat.UpdatedBy = usrInst.Username;
                        vehicleMaintCatService.SaveVehicleMaintCat(objVehicleMaintCat, null, sConnStr);
                    }

                    objVehicleMaintCat = new VehicleMaintCat();
                    objVehicleMaintCat = vehicleMaintCatService.GetVehicleMaintCatByDescription("FUEL MANAGEMENT", sConnStr);
                    if (objVehicleMaintCat == null)
                    {
                        objVehicleMaintCat = new VehicleMaintCat();
                        objVehicleMaintCat.MaintCatId = 6;
                        objVehicleMaintCat.Description = "FUEL MANAGEMENT";
                        objVehicleMaintCat.CreatedDate = DateTime.Now;
                        objVehicleMaintCat.CreatedBy = usrInst.Username;
                        objVehicleMaintCat.UpdatedDate = DateTime.Now;
                        objVehicleMaintCat.UpdatedBy = usrInst.Username;
                        vehicleMaintCatService.SaveVehicleMaintCat(objVehicleMaintCat, null, sConnStr);
                    }

                    objVehicleMaintCat = new VehicleMaintCat();
                    objVehicleMaintCat = vehicleMaintCatService.GetVehicleMaintCatByDescription("ACCIDENT MANAGEMENT", sConnStr);
                    if (objVehicleMaintCat == null)
                    {
                        objVehicleMaintCat = new VehicleMaintCat();
                        objVehicleMaintCat.MaintCatId = 7;
                        objVehicleMaintCat.Description = "ACCIDENT MANAGEMENT";
                        objVehicleMaintCat.CreatedDate = DateTime.Now;
                        objVehicleMaintCat.CreatedBy = usrInst.Username;
                        objVehicleMaintCat.UpdatedDate = DateTime.Now;
                        objVehicleMaintCat.UpdatedBy = usrInst.Username;
                        vehicleMaintCatService.SaveVehicleMaintCat(objVehicleMaintCat, null, sConnStr);
                    }

                    objVehicleMaintCat = new VehicleMaintCat();
                    objVehicleMaintCat = vehicleMaintCatService.GetVehicleMaintCatByDescription("GPS UNIT MANAGEMENT", sConnStr);
                    if (objVehicleMaintCat == null)
                    {
                        objVehicleMaintCat = new VehicleMaintCat();
                        objVehicleMaintCat.MaintCatId = 8;
                        objVehicleMaintCat.Description = "GPS UNIT MANAGEMENT";
                        objVehicleMaintCat.CreatedDate = DateTime.Now;
                        objVehicleMaintCat.CreatedBy = usrInst.Username;
                        objVehicleMaintCat.UpdatedDate = DateTime.Now;
                        objVehicleMaintCat.UpdatedBy = usrInst.Username;
                        vehicleMaintCatService.SaveVehicleMaintCat(objVehicleMaintCat, null, sConnStr);
                    }

                    objVehicleMaintCat = new VehicleMaintCat();
                    objVehicleMaintCat = vehicleMaintCatService.GetVehicleMaintCatByDescription("FOLLOWUP", sConnStr);
                    if (objVehicleMaintCat == null)
                    {
                        objVehicleMaintCat = new VehicleMaintCat();
                        objVehicleMaintCat.MaintCatId = 9;
                        objVehicleMaintCat.Description = "FOLLOWUP";
                        objVehicleMaintCat.CreatedDate = DateTime.Now;
                        objVehicleMaintCat.CreatedBy = usrInst.Username;
                        objVehicleMaintCat.UpdatedDate = DateTime.Now;
                        objVehicleMaintCat.UpdatedBy = usrInst.Username;
                        vehicleMaintCatService.SaveVehicleMaintCat(objVehicleMaintCat, null, sConnStr);
                    }

                    //----------------------- GFI_AMM_VehicleMaintType ----------------------------//

                    VehicleMaintType objVehicleMaintType = new VehicleMaintType();
                    objVehicleMaintType = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("REPAIRS", sConnStr);
                    if (objVehicleMaintType == null)
                    {
                        objVehicleMaintType = new VehicleMaintType();
                        objVehicleMaintType.MaintCatId_cbo = 5;
                        objVehicleMaintType.Description = "REPAIRS";
                        objVehicleMaintType.OccurrenceType = 0;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceDuration = 0;
                        objVehicleMaintType.OccurrenceDurationTh = 0;
                        objVehicleMaintType.OccurrenceKM = 0;
                        objVehicleMaintType.OccurrenceEngineHrs = 0;
                        objVehicleMaintType.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceKMTh = 0;
                        objVehicleMaintType.CreatedDate = DateTime.Now;
                        objVehicleMaintType.CreatedBy = usrInst.Username;
                        objVehicleMaintType.UpdatedDate = DateTime.Now;
                        objVehicleMaintType.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVehicleMaintType, true, sConnStr);
                    }

                    objVehicleMaintType = new VehicleMaintType();
                    objVehicleMaintType = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("VEHICLE CHECK", sConnStr);
                    if (objVehicleMaintType == null)
                    {
                        objVehicleMaintType = new VehicleMaintType();
                        objVehicleMaintType.MaintCatId_cbo = 5;
                        objVehicleMaintType.Description = "VEHICLE CHECK";
                        objVehicleMaintType.OccurrenceType = 0;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceDuration = 0;
                        objVehicleMaintType.OccurrenceDurationTh = 0;
                        objVehicleMaintType.OccurrenceKM = 0;
                        objVehicleMaintType.OccurrenceEngineHrs = 0;
                        objVehicleMaintType.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceKMTh = 0;
                        objVehicleMaintType.CreatedDate = DateTime.Now;
                        objVehicleMaintType.CreatedBy = usrInst.Username;
                        objVehicleMaintType.UpdatedDate = DateTime.Now;
                        objVehicleMaintType.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVehicleMaintType, true, sConnStr);
                    }

                    objVehicleMaintType = new VehicleMaintType();
                    objVehicleMaintType = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("PETROL", sConnStr);
                    if (objVehicleMaintType == null)
                    {
                        objVehicleMaintType = new VehicleMaintType();
                        objVehicleMaintType.MaintCatId_cbo = 6;
                        objVehicleMaintType.Description = "FUEL TYPE - PETROL";
                        objVehicleMaintType.OccurrenceType = 0;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceDuration = 0;
                        objVehicleMaintType.OccurrenceDurationTh = 0;
                        objVehicleMaintType.OccurrenceKM = 0;
                        objVehicleMaintType.OccurrenceEngineHrs = 0;
                        objVehicleMaintType.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceKMTh = 0;
                        objVehicleMaintType.CreatedDate = DateTime.Now;
                        objVehicleMaintType.CreatedBy = usrInst.Username;
                        objVehicleMaintType.UpdatedDate = DateTime.Now;
                        objVehicleMaintType.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVehicleMaintType, true, sConnStr);
                    }

                    objVehicleMaintType = new VehicleMaintType();
                    objVehicleMaintType = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("DIESEL", sConnStr);
                    if (objVehicleMaintType == null)
                    {
                        objVehicleMaintType = new VehicleMaintType();
                        objVehicleMaintType.MaintCatId_cbo = 6;
                        objVehicleMaintType.Description = "FUEL TYPE - DIESEL";
                        objVehicleMaintType.OccurrenceType = 0;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceDuration = 0;
                        objVehicleMaintType.OccurrenceDurationTh = 0;
                        objVehicleMaintType.OccurrenceKM = 0;
                        objVehicleMaintType.OccurrenceEngineHrs = 0;
                        objVehicleMaintType.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceKMTh = 0;
                        objVehicleMaintType.CreatedDate = DateTime.Now;
                        objVehicleMaintType.CreatedBy = usrInst.Username;
                        objVehicleMaintType.UpdatedDate = DateTime.Now;
                        objVehicleMaintType.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVehicleMaintType, true, sConnStr);
                    }

                    objVehicleMaintType = new VehicleMaintType();
                    objVehicleMaintType = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("GAS", sConnStr);
                    if (objVehicleMaintType == null)
                    {
                        objVehicleMaintType = new VehicleMaintType();
                        objVehicleMaintType.MaintCatId_cbo = 6;
                        objVehicleMaintType.Description = "FUEL TYPE - GAS";
                        objVehicleMaintType.OccurrenceType = 0;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceDuration = 0;
                        objVehicleMaintType.OccurrenceDurationTh = 0;
                        objVehicleMaintType.OccurrenceKM = 0;
                        objVehicleMaintType.OccurrenceEngineHrs = 0;
                        objVehicleMaintType.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceKMTh = 0;
                        objVehicleMaintType.CreatedDate = DateTime.Now;
                        objVehicleMaintType.CreatedBy = usrInst.Username;
                        objVehicleMaintType.UpdatedDate = DateTime.Now;
                        objVehicleMaintType.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVehicleMaintType, true, sConnStr);
                    }

                    objVehicleMaintType = new VehicleMaintType();
                    objVehicleMaintType = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("ELECTRIC POWER", sConnStr);
                    if (objVehicleMaintType == null)
                    {
                        objVehicleMaintType = new VehicleMaintType();
                        objVehicleMaintType.MaintCatId_cbo = 6;
                        objVehicleMaintType.Description = "FUEL TYPE - ELECTRIC POWER";
                        objVehicleMaintType.OccurrenceType = 0;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceDuration = 0;
                        objVehicleMaintType.OccurrenceDurationTh = 0;
                        objVehicleMaintType.OccurrenceKM = 0;
                        objVehicleMaintType.OccurrenceEngineHrs = 0;
                        objVehicleMaintType.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceKMTh = 0;
                        objVehicleMaintType.CreatedDate = DateTime.Now;
                        objVehicleMaintType.CreatedBy = usrInst.Username;
                        objVehicleMaintType.UpdatedDate = DateTime.Now;
                        objVehicleMaintType.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVehicleMaintType, true, sConnStr);
                    }

                    objVehicleMaintType = new VehicleMaintType();
                    objVehicleMaintType = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("ACCIDENT", sConnStr);
                    if (objVehicleMaintType == null)
                    {
                        objVehicleMaintType = new VehicleMaintType();
                        objVehicleMaintType.MaintCatId_cbo = 7;
                        objVehicleMaintType.Description = "ACCIDENT";
                        objVehicleMaintType.OccurrenceType = 0;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceDuration = 0;
                        objVehicleMaintType.OccurrenceDurationTh = 0;
                        objVehicleMaintType.OccurrenceKM = 0;
                        objVehicleMaintType.OccurrenceEngineHrs = 0;
                        objVehicleMaintType.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceKMTh = 0;
                        objVehicleMaintType.CreatedDate = DateTime.Now;
                        objVehicleMaintType.CreatedBy = usrInst.Username;
                        objVehicleMaintType.UpdatedDate = DateTime.Now;
                        objVehicleMaintType.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVehicleMaintType, true, sConnStr);
                    }

                    objVehicleMaintType = new VehicleMaintType();
                    objVehicleMaintType = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("GPS UNIT INSTALLATION", sConnStr);
                    if (objVehicleMaintType == null)
                    {
                        objVehicleMaintType = new VehicleMaintType();
                        objVehicleMaintType.MaintCatId_cbo = 8;
                        objVehicleMaintType.Description = "GPS UNIT INSTALLATION";
                        objVehicleMaintType.OccurrenceType = 0;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceDuration = 0;
                        objVehicleMaintType.OccurrenceDurationTh = 0;
                        objVehicleMaintType.OccurrenceKM = 0;
                        objVehicleMaintType.OccurrenceEngineHrs = 0;
                        objVehicleMaintType.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceKMTh = 0;
                        objVehicleMaintType.CreatedDate = DateTime.Now;
                        objVehicleMaintType.CreatedBy = usrInst.Username;
                        objVehicleMaintType.UpdatedDate = DateTime.Now;
                        objVehicleMaintType.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVehicleMaintType, true, sConnStr);
                    }

                    objVehicleMaintType = new VehicleMaintType();
                    objVehicleMaintType = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("GPS UNIT REPLACEMENT", sConnStr);
                    if (objVehicleMaintType == null)
                    {
                        objVehicleMaintType = new VehicleMaintType();
                        objVehicleMaintType.MaintCatId_cbo = 8;
                        objVehicleMaintType.Description = "GPS UNIT REPLACEMENT";
                        objVehicleMaintType.OccurrenceType = 0;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceDuration = 0;
                        objVehicleMaintType.OccurrenceDurationTh = 0;
                        objVehicleMaintType.OccurrenceKM = 0;
                        objVehicleMaintType.OccurrenceEngineHrs = 0;
                        objVehicleMaintType.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceKMTh = 0;
                        objVehicleMaintType.CreatedDate = DateTime.Now;
                        objVehicleMaintType.CreatedBy = usrInst.Username;
                        objVehicleMaintType.UpdatedDate = DateTime.Now;
                        objVehicleMaintType.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVehicleMaintType, true, sConnStr);
                    }

                    objVehicleMaintType = new VehicleMaintType();
                    objVehicleMaintType = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("REMINDER", sConnStr);
                    if (objVehicleMaintType == null)
                    {
                        objVehicleMaintType = new VehicleMaintType();
                        objVehicleMaintType.MaintCatId_cbo = 9;
                        objVehicleMaintType.Description = "REMINDER";
                        objVehicleMaintType.OccurrenceType = 0;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceDuration = 0;
                        objVehicleMaintType.OccurrenceDurationTh = 0;
                        objVehicleMaintType.OccurrenceKM = 0;
                        objVehicleMaintType.OccurrenceEngineHrs = 0;
                        objVehicleMaintType.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceKMTh = 0;
                        objVehicleMaintType.CreatedDate = DateTime.Now;
                        objVehicleMaintType.CreatedBy = usrInst.Username;
                        objVehicleMaintType.UpdatedDate = DateTime.Now;
                        objVehicleMaintType.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVehicleMaintType, true, sConnStr);
                    }
                    #endregion
                    Globals.uLogin = null;

                    InsertDBUpdate(strUpd, "Vehicle Maintenance SP and indexes");
                }
                #endregion
                #region Update 27. Assets Management Stored Proc (Insurance Valid/To Schedule)
                strUpd = "Rez000027";
                if (!CheckUpdateExist(strUpd))
                {
                    InsertDBUpdate(strUpd, "Assets Management Stored Proc (Insurance Valid/To Schedule)");
                }
                #endregion

                #region Update 28. Odo and Hr Meter
                strUpd = "Rez000028";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"
							begin
								execute immediate
									'CREATE TABLE GFI_GPS_DriversDriving
									(
										unitID varchar2(20) NOT NULL,
										DriverID varchar2(20) NULL,
										dtDateTime timestamp(3) NULL,
										CONSTRAINT PK_GFI_GPS_DriversDriving PRIMARY KEY (unitID)
									)';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_GPS_DriversDriving")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, sConnStr);

                    VehicleMaintCat objVehicleMaintCat = new VehicleMaintCat();
                    objVehicleMaintCat = vehicleMaintCatService.GetVehicleMaintCatByDescription("ADMIN FUNCTIONS", sConnStr);
                    if (objVehicleMaintCat == null)
                    {
                        objVehicleMaintCat = new VehicleMaintCat();
                        objVehicleMaintCat.MaintCatId = 10;
                        objVehicleMaintCat.Description = "ADMIN FUNCTIONS";
                        objVehicleMaintCat.CreatedDate = DateTime.Now;
                        objVehicleMaintCat.CreatedBy = usrInst.Username;
                        objVehicleMaintCat.UpdatedDate = DateTime.Now;
                        objVehicleMaintCat.UpdatedBy = usrInst.Username;
                        vehicleMaintCatService.SaveVehicleMaintCat(objVehicleMaintCat, null, sConnStr);
                    }
                    VehicleMaintType objVehicleMaintType = new VehicleMaintType();
                    objVehicleMaintType = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("ODOMETER/ENGINE HOURS ENTRY", sConnStr);
                    if (objVehicleMaintType == null)
                    {
                        objVehicleMaintType = new VehicleMaintType();
                        objVehicleMaintType.MaintCatId_cbo = 10;
                        objVehicleMaintType.Description = "ODOMETER/ENGINE HOURS ENTRY";
                        objVehicleMaintType.OccurrenceType = 0;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceDuration = 0;
                        objVehicleMaintType.OccurrenceDurationTh = 0;
                        objVehicleMaintType.OccurrenceKM = 0;
                        objVehicleMaintType.OccurrenceEngineHrs = 0;
                        objVehicleMaintType.OccurrenceEngineHrsTh = 0;
                        objVehicleMaintType.OccurrenceFixedDateTh = 0;
                        objVehicleMaintType.OccurrenceKMTh = 0;
                        objVehicleMaintType.CreatedDate = DateTime.Now;
                        objVehicleMaintType.CreatedBy = usrInst.Username;
                        objVehicleMaintType.UpdatedDate = DateTime.Now;
                        objVehicleMaintType.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVehicleMaintType, true, sConnStr);
                    }
                    InsertDBUpdate(strUpd, "Odo and Hr Meter and GFI_GPS_DriversDriving");
                }
                #endregion
                #region Update 29. Vehicle Maint SP & Process Table
                strUpd = "Rez000029";
                if (!CheckUpdateExist(strUpd))
                {
                    #region NEW MAINTENANCE TYPES

                    //VEHICLE INSPECTION (FITNESS) 6 MONTHS
                    VehicleMaintType objVMT = new VehicleMaintType();
                    objVMT = new VehicleMaintType();
                    objVMT = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("VEHICLE INSPECTION", sConnStr);
                    if (objVMT == null)
                    {
                        objVMT = new VehicleMaintType();
                        objVMT.MaintCatId_cbo = 2;
                        objVMT.Description = "VEHICLE INSPECTION - 6 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 6;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVMT, true, sConnStr);
                    }
                    else
                    {
                        objVMT.MaintCatId_cbo = 2;
                        objVMT.Description = "VEHICLE INSPECTION - 6 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 6;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVMT, false, sConnStr);
                    }

                    //VEHICLE INSPECTION (FITNESS) 12 MONTHS
                    objVMT = new VehicleMaintType();
                    objVMT = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("VEHICLE INSPECTION - 12 MONTHS", sConnStr);
                    if (objVMT == null)
                    {
                        objVMT = new VehicleMaintType();
                        objVMT.MaintCatId_cbo = 2;
                        objVMT.Description = "VEHICLE INSPECTION - 12 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 12;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVMT, true, sConnStr);
                    }

                    //VEHICLE INSPECTION (FITNESS) 24 MONTHS
                    objVMT = new VehicleMaintType();
                    objVMT = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("VEHICLE INSPECTION - 24 MONTHS", sConnStr);
                    if (objVMT == null)
                    {
                        objVMT = new VehicleMaintType();
                        objVMT.MaintCatId_cbo = 2;
                        objVMT.Description = "VEHICLE INSPECTION - 24 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 24;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVMT, true, sConnStr);
                    }

                    //VEHICLE REGISTRATION (ROAD TAX)  3 MONTHS
                    objVMT = new VehicleMaintType();
                    objVMT = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("VEHICLE REGISTRATION - 3 MONTHS", sConnStr);
                    if (objVMT == null)
                    {
                        objVMT = new VehicleMaintType();
                        objVMT.MaintCatId_cbo = 3;
                        objVMT.Description = "VEHICLE REGISTRATION - 3 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 3;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVMT, true, sConnStr);
                    }

                    //VEHICLE REGISTRATION (ROAD TAX)  6 MONTHS
                    objVMT = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("VEHICLE REGISTRATION - 6 MONTHS", sConnStr);
                    if (objVMT == null)
                    {
                        objVMT = new VehicleMaintType();
                        objVMT.MaintCatId_cbo = 3;
                        objVMT.Description = "VEHICLE REGISTRATION - 6 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 6;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVMT, true, sConnStr);
                    }

                    //VEHICLE REGISTRATION (ROAD TAX)  12 MONTHS
                    objVMT = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("VEHICLE REGISTRATION - 12 MONTHS", sConnStr);
                    if (objVMT == null)
                    {
                        objVMT = new VehicleMaintType();
                        objVMT.MaintCatId_cbo = 3;
                        objVMT.Description = "VEHICLE REGISTRATION - 12 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 12;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVMT, true, sConnStr);
                    }
                    else
                    {
                        objVMT.MaintCatId_cbo = 3;
                        objVMT.Description = "VEHICLE REGISTRATION - 12 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 12;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVMT, false, sConnStr);
                    }

                    //SERVICING - 5000 KM
                    objVMT = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("SERVICING - 5000 KM", sConnStr);
                    if (objVMT != null)
                    {
                        objVMT.OccurrenceKM = 5000;
                        objVMT.OccurrenceKMTh = 500;
                        objVMT.UpdatedDate = DateTime.Now; ;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVMT, false, sConnStr);
                    }

                    //SERVICING - 10000 KM OR 6 MONTHS
                    objVMT = new VehicleMaintType();
                    objVMT = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("SERVICING - 10000 KM OR 6 MONTHS", sConnStr);
                    if (objVMT == null)
                    {
                        objVMT = new VehicleMaintType();
                        objVMT.MaintCatId_cbo = 1;
                        objVMT.Description = "SERVICING - 10000 KM OR 6 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 6;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 10000;
                        objVMT.OccurrenceKMTh = 9500;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVMT, true, sConnStr);
                    }
                    else
                    {
                        objVMT.MaintCatId_cbo = 1;
                        objVMT.Description = "SERVICING - 10000 KM OR 6 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 6;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 10000;
                        objVMT.OccurrenceKMTh = 9500;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVMT, false, sConnStr);
                    }

                    //INSURANCE - 6 MONTHS
                    objVMT = new VehicleMaintType();
                    objVMT = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("INSURANCE - 6 MONTHS", sConnStr);
                    if (objVMT == null)
                    {
                        objVMT = new VehicleMaintType();
                        objVMT.MaintCatId_cbo = 4;
                        objVMT.Description = "INSURANCE - 6 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 6;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVMT, true, sConnStr);
                    }
                    else
                    {
                        objVMT.MaintCatId_cbo = 4;
                        objVMT.Description = "INSURANCE - 6 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 6;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVMT, false, sConnStr);
                    }

                    //INSURANCE - 12 MONTHS
                    objVMT = new VehicleMaintType();
                    objVMT = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("INSURANCE - 12 MONTHS", sConnStr);
                    if (objVMT == null)
                    {
                        objVMT = new VehicleMaintType();
                        objVMT.MaintCatId_cbo = 4;
                        objVMT.Description = "INSURANCE - 12 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 12;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVMT, true, sConnStr);
                    }
                    else
                    {
                        objVMT.MaintCatId_cbo = 4;
                        objVMT.Description = "INSURANCE - 12 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 12;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVMT, false, sConnStr);
                    }
                    #endregion

                    sql = @"CREATE TABLE GFI_GPS_ProcessPending
                            (
								iID number(10) NOT NULL,
								AssetID number(10) NOT NULL,
								dtDateFrom timestamp(3) DEFAULT SYSTIMESTAMP NOT NULL ,
								ProcessCode nvarchar2 (50) NULL,
								Processing number(10) DEFAULT 0  NOT NULL,
								CONSTRAINT PK_ProcessPending PRIMARY KEY  
								(iID )
							)";
                        CreateTable(sql, "GFI_GPS_ProcessPending", "iID");

                    sql = @"ALTER TABLE GFI_GPS_ProcessPending  
							ADD CONSTRAINT FK_GFI_GPS_ProcessPending_GFI_FLT_Asset  FOREIGN KEY(AssetID )
						REFERENCES GFI_FLT_Asset (AssetID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_ProcessPending_GFI_FLT_Asset", "GFI_GPS_ProcessPending")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"
						begin
							execute immediate
								'CREATE TABLE GFI_SYS_Ticker(
								 Id number(10) NOT NULL,
								 TickDate timestamp(3) NULL,
								 Message nvarchar2(500) NULL,
								 CreatedBy nvarchar2(255) NULL,
								 AuthorizedBy nvarchar2(255) NULL,
								 CONSTRAINT PK_GFI_SYS_Ticker PRIMARY KEY (Id)
								)';
						end;";
                    if (GetDataDT(ExistTableSql("GFI_SYS_Ticker")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, sConnStr);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_SYS_Ticker_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_SYS_Ticker_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_SYS_Ticker_seq_tr
					BEFORE INSERT ON GFI_SYS_Ticker FOR EACH ROW
					WHEN(NEW.Id IS NULL)
					BEGIN
						SELECT GFI_SYS_Ticker_seq.NEXTVAL INTO :NEW.Id FROM DUAL;
					END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "Vehicle Maintenance Bulk Assign SP and Process Table");
                }
                #endregion

                #region Update 30. Maint Ref and Vat
                strUpd = "Rez000030";
                if (!CheckUpdateExist(strUpd))
                {
                    /*
					sql = "ALTER TABLE GFI_AMM_VehicleMaintenance DROP CONSTRAINT FK_GFI_AMM_VehicleMaintenance_GFI_AMM_Asset_ExtPro_Vehicles";
					if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintenance_GFI_AMM_Asset_ExtPro_Vehicles", "GFI_AMM_VehicleMaintenance")).Rows.Count > 0)
						_SqlConn.ScriptExecute(sql, myStrConn);
					sql = "ALTER TABLE GFI_AMM_VehicleMaintenance DROP CONSTRAINT FK_GFI_AMM_VehicleMaint_GFI_AMM_VehicleMaintStatus";
					if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaint_GFI_AMM_VehicleMaintStatus", "GFI_AMM_VehicleMaintenance")).Rows.Count > 0)
						_SqlConn.ScriptExecute(sql, myStrConn);
					sql = "ALTER TABLE GFI_AMM_VehicleMaintenance DROP CONSTRAINT FK_GFI_AMM_VehicleMaintenance_GFI_AMM_VehicleMaintTypes";
					if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintenance_GFI_AMM_VehicleMaintTypes", "GFI_AMM_VehicleMaintenance")).Rows.Count > 0)
						_SqlConn.ScriptExecute(sql, myStrConn);
					sql = "ALTER TABLE GFI_AMM_VehicleMaintenance DROP CONSTRAINT PK_GFI_AMM_VehicleMaint";
					if (GetDataDT(ExistsSQLConstraint("PK_GFI_AMM_VehicleMaint", "GFI_AMM_VehicleMaintenance")).Rows.Count > 0)
						_SqlConn.ScriptExecute(sql, myStrConn);

					sql = @"
					begin
						execute immediate
							'CREATE TABLE Tmp_GFI_AMM_VehicleMaintenance
								(
								URI number(10) NOT NULL,
								AssetId number(10) NULL,
								MaintTypeId_cbo number(10) NULL,
								CompanyName nvarchar2(255) NULL,
								PhoneNumber nvarchar2(120) NULL,
								CompanyRef nvarchar2(120) NULL,
								CompanyRef2 nvarchar2(120) NULL,
								MaintDescription nvarchar2(512) NULL,
								StartDate timestamp(3) NULL,
								EndDate timestamp(3) NULL,
								MaintStatusId_cbo number(10) NULL,
								EstimatedValue number(18, 2) NULL,
								VATInclInItemsAmt nchar(1) NULL,
								VATAmount number(18, 2) NULL,
								TotalCost number(18, 2) NULL,
								CoverTypeId_cbo number(10) NULL,
								CalculatedOdometer number(10) NULL,
								ActualOdometer number(10) NULL,
								CalculatedEngineHrs number(10) NULL,
								ActualEngineHrs number(10) NULL,
								AdditionalInfo nvarchar2(500) NULL,
								CreatedDate timestamp(3) NULL,
								CreatedBy nvarchar2(30) NULL,
								UpdatedDate timestamp(3) NULL,
								UpdatedBy nvarchar2(30) NULL,
								CONSTRAINT PK_GFI_AMM_VehicleMaint PRIMARY KEY (URI)
								)'; 
					end;";
					if (GetDataDT(ExistTableSql("Tmp_GFI_AMM_VehicleMaintenance")).Rows[0][0].ToString() == "0")
						_SqlConn.ScriptExecute(sql, myStrConn);

					//Generate ID using sequence and trigger
					sql = @"CREATE SEQUENCE Tmp_GFI_AMM_VehicleMaintenance_seq START WITH 1 INCREMENT BY 1";
					if (GetDataDT(ExistSequenceSql("Tmp_GFI_AMM_VehicleMaintenance_seq")).Rows[0][0].ToString() == "0")
						_SqlConn.ScriptExecute(sql, myStrConn);


					sql = @"CREATE OR REPLACE TRIGGER Tmp_GFI_AMM_VehicleMaintenance_seq_tr
								BEFORE INSERT ON Tmp_GFI_AMM_VehicleMaintenance FOR EACH ROW
								WHEN(NEW.URI IS NULL)
								BEGIN
									SELECT Tmp_GFI_AMM_VehicleMaintenance_seq.NEXTVAL INTO :NEW.URI FROM DUAL;
								END;";
					_SqlConn.ScriptExecute(sql, myStrConn);

					
					INSERT INTO Tmp_GFI_AMM_VehicleMaintenance (URI, AssetId, MaintTypeId_cbo, CompanyName, PhoneNumber, CompanyRef, MaintDescription, StartDate, EndDate, MaintStatusId_cbo, EstimatedValue, TotalCost, CoverTypeId_cbo, CalculatedOdometer, ActualOdometer, CalculatedEngineHrs, ActualEngineHrs, AdditionalInfo, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy)
							SELECT URI, AssetId, MaintTypeId_cbo, CompanyName, PhoneNumber, CompanyRef, MaintDescription, StartDate, EndDate, MaintStatusId_cbo, EstimatedValue, TotalCost, CoverTypeId_cbo, CalculatedOdometer, ActualOdometer, CalculatedEngineHrs, ActualEngineHrs, AdditionalInfo, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy FROM GFI_AMM_VehicleMaintenance;

					ALTER TABLE GFI_AMM_VehicleMaintItems
						DROP FOREIGN KEY FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance;

					DROP TABLE GFI_AMM_VehicleMaintenance;

					RENAME TABLE Tmp_GFI_AMM_VehicleMaintenance To GFI_AMM_VehicleMaintenance;

					CREATE  INDEX IX_ActualCompletionDate ON GFI_AMM_VehicleMaintenance(EndDate);

					CREATE  INDEX IX_ActualStartDate ON GFI_AMM_VehicleMaintenance(StartDate);

					CREATE  INDEX IX_AssetId ON GFI_AMM_VehicleMaintenance(AssetId);

					CREATE  INDEX IX_CompanyName ON GFI_AMM_VehicleMaintenance(CompanyName);

					CREATE  INDEX IX_CoverTypeId ON GFI_AMM_VehicleMaintenance(CoverTypeId_cbo);
					CREATE  INDEX IX_EndDate ON GFI_AMM_VehicleMaintenance(EndDate);

					CREATE  INDEX IX_MaintStatusId ON GFI_AMM_VehicleMaintenance(MaintStatusId_cbo);

					CREATE  INDEX IX_MaintTypeId ON GFI_AMM_VehicleMaintenance(MaintTypeId_cbo);

					CREATE  INDEX IX_StartDate ON GFI_AMM_VehicleMaintenance(StartDate);

					ALTER TABLE GFI_AMM_VehicleMaintenance ADD CONSTRAINT
						FK_GFI_AMM_VehicleMaintenance_GFI_AMM_VehicleMaintTypes FOREIGN KEY
						(
						MaintTypeId_cbo
						) REFERENCES GFI_AMM_VehicleMaintTypes
						(
						MaintTypeId
						) ON UPDATE  NO ACTION 
						 ON DELETE  NO ACTION ;
	
					ALTER TABLE GFI_AMM_VehicleMaintenance ADD CONSTRAINT
						FK_GFI_AMM_VehicleMaint_GFI_AMM_VehicleMaintStatus FOREIGN KEY
						(
						MaintStatusId_cbo
						) REFERENCES GFI_AMM_VehicleMaintStatus
						(
						MaintStatusId
						) ON UPDATE  NO ACTION 
						 ON DELETE  NO ACTION ;

					ALTER TABLE GFI_AMM_VehicleMaintenance ADD CONSTRAINT
						FK_GFI_AMM_VehicleMaintenance_GFI_AMM_Asset_ExtPro_Vehicles FOREIGN KEY
						(
						AssetId
						) REFERENCES GFI_AMM_AssetExtProVehicles
						(
						AssetId
						) ON UPDATE  NO ACTION 
						 ON DELETE  NO ACTION ;
	

					ALTER TABLE GFI_AMM_VehicleMaintItems ADD CONSTRAINT
						FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance FOREIGN KEY
						(
						MaintURI
						) REFERENCES GFI_AMM_VehicleMaintenance
						(
						URI
						) ON UPDATE  NO ACTION 
						 ON DELETE  NO ACTION ;
						";
					dbExecute(sql);

					sql = @"
DELIMITER //
	DROP PROCEDURE IF EXISTS sp_AMM_VehicleMaint_CreateChildRecords;
	CREATE PROCEDURE sp_AMM_VehicleMaint_CreateChildRecords
	(
	   iAssetId int,
	   iMaintTypeId int
	)
	BEGIN
	  select @VMTLURI := (SELECT MAX(URI) FROM GFI_AMM_VehicleMaintTypesLink  WHERE AssetId = @iAssetId AND MaintTypeId = iMaintTypeId);
	  select @iMaintStatusId := (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'To Schedule');
	  select @NextMaintDate :=  (SELECT NextMaintDate FROM GFI_AMM_VehicleMaintTypesLink WHERE URI = @VMTLURI);

	  IF (@NextMaintDate IS NULL) then
			set @NextMaintDate = NOW() + INTERVAL 1 DAY;
		end if;

	  INSERT GFI_AMM_VehicleMaintenance (AssetId, MaintTypeId_cbo, MaintStatusId_cbo, StartDate, EndDate, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy) 
	  VALUES (@iAssetId, @iMaintTypeId, @iMaintStatusId, @NextMaintDate, @NextMaintDate, NOW(), CURRENT_USER, NOW(), CURRENT_USER);

	  select @iMaintURI = (SELECT LAST_INSERT_ID());

	  UPDATE GFI_AMM_VehicleMaintTypesLink
		SET NextMaintDate = @NextMaintDate, Status = @iMaintStatusId, MaintURI = @iMaintURI
	  WHERE URI = @VMTLURI;
	END //
DELIMITER ;";
					*/
                    _SqlConn.OracleScriptExecute(sql, sConnStr);

                    InsertDBUpdate(strUpd, "Maint Ref and Vat");
                }
                #endregion
                #region Update 31. Time zone
                strUpd = "Rez000031";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "update GFI_FLT_Asset set TimeZoneID = 'Arabian Standard Time' where TimeZoneID = ''";
                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "Time zone updated");
                }
                #endregion
                #region Update 32. MaintType Group Matrix
                strUpd = "Rez000032";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"
						begin
							execute immediate
								'CREATE TABLE GFI_SYS_GroupMatrixMaintType
								(
									MID NUMBER(10) NOT NULL
									, GMID NUMBER(10) NOT NULL
									, iID NUMBER(10) NOT NULL
									, CONSTRAINT PK_GFI_SYS_GroupMatrixMaintType PRIMARY KEY  (MID)
								)';
						end;";
                    if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrixMaintType")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_SYS_GroupMatrixMaintType_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_SYS_GroupMatrixMaintType_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_SYS_GroupMatrixMaintType_seq_tr
								BEFORE INSERT ON GFI_SYS_GroupMatrixMaintType FOR EACH ROW
								WHEN(NEW.MID IS NULL)
								BEGIN
									SELECT GFI_SYS_GroupMatrixMaintType_seq.NEXTVAL INTO :NEW.MID FROM DUAL;
								END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixMaintType 
							ADD CONSTRAINT FK_GFI_SYS_GroupMatrixMaintType_GFI_SYS_GroupMatrix 
								FOREIGN KEY (GMID) 
								REFERENCES GFI_SYS_GroupMatrix (GMID) ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixMaintType_GFI_SYS_GroupMatrix", "GFI_SYS_GroupMatrixMaintType")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixMaintType 
							ADD CONSTRAINT FK_GFI_SYS_GroupMatrixRule_GFI_AMM_VehicleMaintTypes 
								FOREIGN KEY(iID) 
								REFERENCES GFI_AMM_VehicleMaintTypes(MaintTypeId) 
						 ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixMaintType_GFI_SYS_GroupMatrix", "GFI_SYS_GroupMatrixMaintType")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "MaintType Group Matrix");
                }
                #endregion
                #region Update 33. Trip Process index
                strUpd = "Rez000033";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE INDEX IX_GFI_GPS_GPSData_ProcessTrips ON GFI_GPS_GPSData 
							(
								AssetID ASC,
								DateTimeGPS_UTC ASC,
								LongLatValidFlag ASC,
								StopFlag ASC
							)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSData", "IX_GFI_GPS_GPSData_ProcessTrips")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "Trip Process index");
                }
                #endregion
                #region Update 34. Reconfigure Vehicle Maint Cat
                strUpd = "Rez000034";
                if (!CheckUpdateExist(strUpd))
                {
                    //Maint Cat - Insurance renamed to Renewals
                    VehicleMaintCat objVehicleMaintCat = new VehicleMaintCat();
                    VehicleMaintCatService vehicleMaintCatService = new VehicleMaintCatService();
                    objVehicleMaintCat = vehicleMaintCatService.GetVehicleMaintCatByDescription("INSURANCE", sConnStr);
                    if (objVehicleMaintCat != null)
                    {
                        objVehicleMaintCat = new VehicleMaintCat();
                        objVehicleMaintCat.MaintCatId = 4;
                        objVehicleMaintCat.Description = "RENEWALS";
                        objVehicleMaintCat.UpdatedDate = DateTime.Now;
                        objVehicleMaintCat.UpdatedBy = usrInst.Username;
                        vehicleMaintCatService.UpdateVehicleMaintCat(objVehicleMaintCat, null, sConnStr);
                    }

                    InsertDBUpdate(strUpd, "Reconfigure Vehicle Maint Cat");
                }
                #endregion

                #region Update 35. Reconfigure Vehicle Maint Types
                strUpd = "Rez000035";
                if (!CheckUpdateExist(strUpd))
                {
                    //VEHICLE INSPECTION (FITNESS) 6 MONTHS
                    VehicleMaintType objVMT = new VehicleMaintType();
                    objVMT = new VehicleMaintType();
                    objVMT = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("VEHICLE INSPECTION - 6 MONTHS", sConnStr);
                    if (objVMT != null)
                    {
                        objVMT.MaintCatId_cbo = 4;
                        objVMT.Description = "FITNESS - 6 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 6;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVMT, false, sConnStr);
                    }

                    //VEHICLE INSPECTION (FITNESS) 12 MONTHS
                    objVMT = new VehicleMaintType();
                    objVMT = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("VEHICLE INSPECTION - 12 MONTHS", sConnStr);
                    if (objVMT != null)
                    {
                        objVMT.MaintCatId_cbo = 4;
                        objVMT.Description = "FITNESS - 12 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 12;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVMT, false, sConnStr);
                    }

                    //VEHICLE INSPECTION (FITNESS) 24 MONTHS
                    objVMT = new VehicleMaintType();
                    objVMT = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("VEHICLE INSPECTION - 24 MONTHS", sConnStr);
                    if (objVMT != null)
                    {
                        objVMT.MaintCatId_cbo = 4;
                        objVMT.Description = "FITNESS - 24 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 24;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVMT, false, sConnStr);
                    }

                    //VEHICLE REGISTRATION (ROAD TAX)  3 MONTHS
                    objVMT = new VehicleMaintType();
                    objVMT = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("VEHICLE REGISTRATION - 3 MONTHS", sConnStr);
                    if (objVMT != null)
                    {
                        objVMT.MaintCatId_cbo = 4;
                        objVMT.Description = "VEHICLE REGISTRATION - 3 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 3;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVMT, false, sConnStr);
                    }

                    //VEHICLE REGISTRATION (ROAD TAX)  6 MONTHS
                    objVMT = new VehicleMaintType();
                    objVMT = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("VEHICLE REGISTRATION - 6 MONTHS", sConnStr);
                    if (objVMT != null)
                    {
                        objVMT.MaintCatId_cbo = 4;
                        objVMT.Description = "VEHICLE REGISTRATION - 6 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 6;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVMT, false, sConnStr);
                    }

                    //VEHICLE REGISTRATION (ROAD TAX)  12 MONTHS
                    objVMT = new VehicleMaintType();
                    objVMT = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("VEHICLE REGISTRATION - 12 MONTHS", sConnStr);
                    if (objVMT != null)
                    {
                        objVMT.MaintCatId_cbo = 4;
                        objVMT.Description = "VEHICLE REGISTRATION - 12 MONTHS";
                        objVMT.OccurrenceType = 2;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceDuration = 12;
                        objVMT.OccurrenceDurationTh = 1;
                        objVMT.OccurrencePeriod_cbo = "MONTH(S)";
                        objVMT.OccurrenceKM = 0;
                        objVMT.OccurrenceEngineHrs = 0;
                        objVMT.OccurrenceEngineHrsTh = 0;
                        objVMT.OccurrenceFixedDateTh = 0;
                        objVMT.OccurrenceKMTh = 0;
                        objVMT.CreatedDate = DateTime.Now;
                        objVMT.CreatedBy = usrInst.Username;
                        objVMT.UpdatedDate = DateTime.Now;
                        objVMT.UpdatedBy = usrInst.Username;
                        vehicleMaintTypeService.SaveVehicleMaintType(objVMT, false, sConnStr);
                    }

                    //Re-categorize VEHICLE WASH as UNPLANNED MAINTENANCE
                    objVMT = new VehicleMaintType();
                    objVMT = vehicleMaintTypeService.GetVehicleMaintTypeByDescription("VEHICLE WASH", sConnStr);
                    if (objVMT != null)
                    {
                        VehicleMaintCat objMaintCat = new VehicleMaintCat();
                        objMaintCat = vehicleMaintCatService.GetVehicleMaintCatByDescription("UNPLANNED MAINTENANCE", sConnStr);
                        if (objMaintCat != null)
                        {
                            objVMT.MaintCatId_cbo = objMaintCat.MaintCatId;
                            vehicleMaintTypeService.SaveVehicleMaintType(objVMT, false, sConnStr);
                        }
                    }

                    //Delete Maint Cat INSPECTION]
                    VehicleMaintCat objVehicleMaintCat = new VehicleMaintCat();
                    objVehicleMaintCat = vehicleMaintCatService.GetVehicleMaintCatByDescription("INSPECTION", sConnStr);
                    if (objVehicleMaintCat != null)
                    {
                        vehicleMaintCatService.DeleteVehicleMaintCat(objVehicleMaintCat, sConnStr);
                    }

                    //Delete Maint Cat REGISTRATION]
                    objVehicleMaintCat = new VehicleMaintCat();
                    objVehicleMaintCat = vehicleMaintCatService.GetVehicleMaintCatByDescription("REGISTRATION", sConnStr);
                    if (objVehicleMaintCat != null)
                    {
                        vehicleMaintCatService.DeleteVehicleMaintCat(objVehicleMaintCat, sConnStr);
                    }

                    InsertDBUpdate(strUpd, "Reconfigure Vehicle Maint Types");
                }
                #endregion
                #region Update 36. Asset Constraints
                strUpd = "Rez000036";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_FLT_AssetDeviceMap DROP CONSTRAINT FK_GFI_FLT_AssetDeviceMap_GFI_FLT_Asset";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_AssetDeviceMap_GFI_FLT_Asset", "GFI_FLT_AssetDeviceMap")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_AssetDeviceMap 
							ADD CONSTRAINT FK_GFI_FLT_AssetDeviceMap_GFI_FLT_Asset 
								FOREIGN KEY(AssetID) 
								REFERENCES GFI_FLT_Asset(AssetID) 
							 ON DELETE  Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_AssetDeviceMap_GFI_FLT_Asset", "GFI_FLT_AssetDeviceMap")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_GPS_TripHeader DROP CONSTRAINT FK_GFI_GPS_TripHeader_GFI_FLT_Asset";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_TripHeader_GFI_FLT_Asset", "GFI_GPS_TripHeader")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_GPS_TripHeader 
							ADD CONSTRAINT FK_GFI_GPS_TripHeader_GFI_FLT_Asset 
								FOREIGN KEY(AssetID) 
								REFERENCES GFI_FLT_Asset(AssetID) 
							ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_TripHeader_GFI_FLT_Asset", "GFI_GPS_TripHeader")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_GPS_GPSData DROP CONSTRAINT FK_GFI_GPS_GPSData_GFI_FLT_Asset";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_GPSData_GFI_FLT_Asset", "GFI_GPS_GPSData")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_GPS_GPSData 
							ADD CONSTRAINT FK_GFI_GPS_GPSData_GFI_FLT_Asset 
								FOREIGN KEY(AssetID) 
								REFERENCES GFI_FLT_Asset(AssetID) 
							ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_GPSData_GFI_FLT_Asset", "GFI_GPS_GPSData")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_GPS_Exceptions DROP CONSTRAINT FK_GFI_GPS_Exceptions_GFI_FLT_Asset";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_Exceptions_GFI_FLT_Asset", "GFI_GPS_Exceptions")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_GPS_Exceptions 
							ADD CONSTRAINT FK_GFI_GPS_Exceptions_GFI_FLT_Asset 
								FOREIGN KEY(AssetID) 
								REFERENCES GFI_FLT_Asset(AssetID) 
							ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_Exceptions_GFI_FLT_Asset", "GFI_GPS_Exceptions")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintenance
							DROP CONSTRAINT FK_GFI_AMM_VehicleMaintenance_GFI_AMM_Asset_ExtPro_Vehicles";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintenance_GFI_AMM_Asset_ExtPro_Vehicles", "GFI_AMM_VehicleMaintenance")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintTypesLink
							DROP CONSTRAINT FK_GFI_AMM_VehicleMaintTypesLink_GFI_AMM_AssetExtProVehicles";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintTypesLink_GFI_AMM_AssetExtProVehicles", "GFI_AMM_VehicleMaintTypesLink")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintItems
							DROP CONSTRAINT FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance", "GFI_AMM_VehicleMaintItems")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintenance ADD CONSTRAINT
							FK_GFI_AMM_VehicleMaintenance_GFI_AMM_Asset_ExtPro_Vehicles FOREIGN KEY
							(AssetId) REFERENCES GFI_AMM_AssetExtProVehicles(AssetId) 
							 ON DELETE  CASCADE";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintenance_GFI_AMM_Asset_ExtPro_Vehicles", "GFI_AMM_VehicleMaintenance")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintItems ADD CONSTRAINT
							FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance FOREIGN KEY
							(MaintURI) REFERENCES GFI_AMM_VehicleMaintenance(URI) 
							 ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance", "GFI_AMM_VehicleMaintItems")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintTypesLink ADD CONSTRAINT
							FK_GFI_AMM_VehicleMaintTypesLink_GFI_AMM_AssetExtProVehicles FOREIGN KEY
							(AssetId) REFERENCES GFI_AMM_AssetExtProVehicles(AssetId) 
							 ON DELETE  CASCADE";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintTypesLink_GFI_AMM_AssetExtProVehicles", "GFI_AMM_VehicleMaintTypesLink")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_AMM_AssetExtProVehicles
							DROP CONSTRAINT FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_Asset";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_Asset", "GFI_AMM_AssetExtProVehicles")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_AMM_AssetExtProVehicles ADD CONSTRAINT
							FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_Asset FOREIGN KEY
							(AssetId) REFERENCES GFI_FLT_Asset(AssetID) 
								ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_Asset", "GFI_AMM_AssetExtProVehicles")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "update GFI_FLT_Asset set Status_b = 'AC' where Status_b = ''";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "Asset Constraints");
                }
                #endregion
                #region Update 37. Odo and Hr Meter
                strUpd = "Rez000037";
                if (!CheckUpdateExist(strUpd))
                {
                    InsertDBUpdate(strUpd, "Get Odometer & Engine Hours functions");
                }
                #endregion
                #region Update 38. Add field ContactDetails to table GFI_AMM_VehicleMaintenance
                strUpd = "Rez000038";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_AMM_VehicleMaintenance DROP CONSTRAINT FK_GFI_AMM_VehicleMaintenance_GFI_AMM_Asset_ExtPro_Vehicles";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintenance_GFI_AMM_Asset_ExtPro_Vehicles", "GFI_AMM_VehicleMaintenance")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_AMM_VehicleMaintenance DROP CONSTRAINT FK_GFI_AMM_VehicleMaint_GFI_AMM_VehicleMaintStatus";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaint_GFI_AMM_VehicleMaintStatus", "GFI_AMM_VehicleMaintenance")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_AMM_VehicleMaintenance DROP CONSTRAINT FK_GFI_AMM_VehicleMaintenance_GFI_AMM_VehicleMaintTypes";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintenance_GFI_AMM_VehicleMaintTypes", "GFI_AMM_VehicleMaintenance")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_AMM_VehicleMaintItems DROP CONSTRAINT FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance", "GFI_AMM_VehicleMaintItems")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"
					begin
						execute immediate
					'CREATE TABLE Tmp_GFI_AMM_VehicleMaintenance
						(
						URI number(10) NOT NULL,
						AssetId number(10) NULL,
						MaintTypeId_cbo number(10) NULL,
						ContactDetails nvarchar2(120) NULL,
						CompanyName nvarchar2(255) NULL,
						PhoneNumber nvarchar2(120) NULL,
						CompanyRef nvarchar2(120) NULL,
						CompanyRef2 nvarchar2(120) NULL,
						MaintDescription nvarchar2(512) NULL,
						StartDate timestamp(3) NULL,
						EndDate timestamp(3) NULL,
						MaintStatusId_cbo number(10) NULL,
						EstimatedValue number(18, 2) NULL,
						VATInclInItemsAmt nchar(1) NULL,
						VATAmount number(18, 2) NULL,
						TotalCost number(18, 2) NULL,
						CoverTypeId_cbo number(10) NULL,
						CalculatedOdometer number(10) NULL,
						ActualOdometer number(10) NULL,
						CalculatedEngineHrs number(10) NULL,
						ActualEngineHrs number(10) NULL,
						AdditionalInfo nvarchar2(500) NULL,
						CreatedDate timestamp(3) NULL,
						CreatedBy nvarchar2(30) NULL,
						UpdatedDate timestamp(3) NULL,
						UpdatedBy nvarchar2(30) NULL,
						CONSTRAINT PK_Tmp_GFI_AMM_VehicleMaintenance PRIMARY KEY (URI)
						)';
					end;";
                    if (GetDataDT(ExistTableSql("Tmp_GFI_AMM_VehicleMaintenance")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE Tmp_GFI_AMM_VehicleMaintenance_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("Tmp_GFI_AMM_VehicleMaintenance_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER Tmp_GFI_AMM_VehicleMaintenance_seq_tr
					BEFORE INSERT ON Tmp_GFI_AMM_VehicleMaintenance FOR EACH ROW
					WHEN(NEW.URI IS NULL)
					BEGIN
						SELECT Tmp_GFI_AMM_VehicleMaintenance_seq.NEXTVAL INTO :NEW.URI FROM DUAL;
					END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    /*
					INSERT INTO Tmp_GFI_AMM_VehicleMaintenance (URI, AssetId, MaintTypeId_cbo, CompanyName, PhoneNumber, CompanyRef, CompanyRef2, MaintDescription, StartDate, EndDate, MaintStatusId_cbo, EstimatedValue, VATInclInItemsAmt, VATAmount, TotalCost, CoverTypeId_cbo, CalculatedOdometer, ActualOdometer, CalculatedEngineHrs, ActualEngineHrs, AdditionalInfo, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy)
							SELECT URI, AssetId, MaintTypeId_cbo, CompanyName, PhoneNumber, CompanyRef, CompanyRef2, MaintDescription, StartDate, EndDate, MaintStatusId_cbo, EstimatedValue, VATInclInItemsAmt, VATAmount, TotalCost, CoverTypeId_cbo, CalculatedOdometer, ActualOdometer, CalculatedEngineHrs, ActualEngineHrs, AdditionalInfo, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy FROM GFI_AMM_VehicleMaintenance ;

					DROP TABLE GFI_AMM_VehicleMaintenance;

					RENAME Table Tmp_GFI_AMM_VehicleMaintenance TO GFI_AMM_VehicleMaintenance;

					CREATE  INDEX IX_ActualCompletionDate ON GFI_AMM_VehicleMaintenance
						(
						EndDate
						);

					CREATE  INDEX IX_ActualStartDate ON GFI_AMM_VehicleMaintenance
						(
						StartDate
						);

					CREATE  INDEX IX_AssetId ON GFI_AMM_VehicleMaintenance
						(
						AssetId
						);

					CREATE  INDEX IX_CompanyName ON GFI_AMM_VehicleMaintenance
						(
						CompanyName
						);

					CREATE  INDEX IX_CoverTypeId ON GFI_AMM_VehicleMaintenance
						(
						CoverTypeId_cbo
						);

					CREATE  INDEX IX_EndDate ON GFI_AMM_VehicleMaintenance
						(
						EndDate
						);

					CREATE  INDEX IX_MaintStatusId ON GFI_AMM_VehicleMaintenance
						(
						MaintStatusId_cbo
						);

					CREATE  INDEX IX_MaintTypeId ON GFI_AMM_VehicleMaintenance
						(
						MaintTypeId_cbo
						);

					CREATE  INDEX IX_StartDate ON GFI_AMM_VehicleMaintenance
						(
						StartDate
						);

					ALTER TABLE GFI_AMM_VehicleMaintenance ADD CONSTRAINT
						FK_GFI_AMM_VehicleMaintenance_GFI_AMM_VehicleMaintTypes FOREIGN KEY
						(
						MaintTypeId_cbo
						) REFERENCES GFI_AMM_VehicleMaintTypes
						(
						MaintTypeId
						) ON UPDATE  NO ACTION 
						 ON DELETE  NO ACTION ;

					ALTER TABLE GFI_AMM_VehicleMaintenance ADD CONSTRAINT
						FK_GFI_AMM_VehicleMaint_GFI_AMM_VehicleMaintStatus FOREIGN KEY
						(
						MaintStatusId_cbo
						) REFERENCES GFI_AMM_VehicleMaintStatus
						(
						MaintStatusId
						) ON UPDATE  NO ACTION 
						 ON DELETE  NO ACTION ;

					ALTER TABLE GFI_AMM_VehicleMaintenance ADD CONSTRAINT
						FK_GFI_AMM_VehicleMaintenance_GFI_AMM_Asset_ExtPro_Vehicles FOREIGN KEY
						(
						AssetId
						) REFERENCES GFI_AMM_AssetExtProVehicles
						(
						AssetId
						) ON UPDATE  NO ACTION 
						 ON DELETE  CASCADE; 
	
					ALTER TABLE GFI_AMM_VehicleMaintItems ADD CONSTRAINT
						FK_GFI_AMM_VehicleMaintItems_GFI_AMM_VehicleMaintenance FOREIGN KEY
						(
						MaintURI
						) REFERENCES GFI_AMM_VehicleMaintenance
						(
						URI
						) ON UPDATE  NO ACTION 
						 ON DELETE  CASCADE ;";

					dbExecute(sql);

					*/

                    InsertDBUpdate(strUpd, "Add field ContactDetails to table GFI_AMM_VehicleMaintenance");
                }
                #endregion
                #region Update 39. GFI_FLT_AutoReportingConfig
                strUpd = "Rez000039";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"
						begin
							execute immediate
								'CREATE TABLE GFI_FLT_AutoReportingConfig(
									ARID number(10) NOT NULL,
									TargetID number(10) NULL,
									TargetType nvarchar2(20) NULL,
									ReportID number(10) NULL,
									ReportPeriod number(10) NULL,
									TriggerTime nvarchar2(50) NULL,
									TriggerInterval nvarchar2(50) NULL,
									CreatedBy nchar(10) NOT NULL,
									CONSTRAINT PK_GFI_FLT_AutoReportingConfig PRIMARY KEY  
								(ARID)
								)';
						end;";
                    if (GetDataDT(ExistTableSql("GFI_FLT_AutoReportingConfig")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_FLT_AutoReportingConfig_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_FLT_AutoReportingConfig_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_FLT_AutoReportingConfig_seq_tr
								BEFORE INSERT ON GFI_FLT_AutoReportingConfig FOR EACH ROW
								WHEN(NEW.ARID IS NULL)
								BEGIN
									SELECT GFI_FLT_AutoReportingConfig_seq.NEXTVAL INTO :NEW.ARID FROM DUAL;
								END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"
							begin
								execute immediate
									'CREATE TABLE GFI_FLT_AutoReportingConfigDetails(
									 ARDID number(10) NOT NULL,
									 ARID number(10) NOT NULL,
									 UID number(10) NOT NULL,
									 UIDType nvarchar2(50) NULL,
									 UIDCategory nvarchar2(50) NULL,
									 CONSTRAINT PK_GFI_FLT_AutoReportingConfigDetails_1 PRIMARY KEY  
									(ARDID)
									)';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_FLT_AutoReportingConfigDetails")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_FLT_AutoReportingConfigDetails_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_FLT_AutoReportingConfigDetails_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_FLT_AutoReportingConfigDetails_seq_tr
								BEFORE INSERT ON GFI_FLT_AutoReportingConfigDetails FOR EACH ROW
								WHEN(NEW.ARDID IS NULL)
								BEGIN
									SELECT GFI_FLT_AutoReportingConfigDetails_seq.NEXTVAL INTO :NEW.ARDID FROM DUAL;
								END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE  INDEX IX_GFI_FLT_AutoReportingConfigDetails ON GFI_FLT_AutoReportingConfigDetails (ARID ASC)";
                    if (GetDataDT(ExistsIndex("GFI_FLT_AutoReportingConfigDetails", "IX_GFI_FLT_AutoReportingConfigDetails")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_AutoReportingConfigDetails  ADD  CONSTRAINT FK_GFI_FLT_ARCD_GFI_FLT_ARC FOREIGN KEY(ARID)
						REFERENCES GFI_FLT_AutoReportingConfig (ARID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ARCD_GFI_FLT_ARC", "GFI_FLT_AutoReportingConfigDetails")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "GFI_FLT_AutoReportingConfig");
                }
                #endregion

                #region Update 40. Rules and Exceptions
                strUpd = "Rez000040";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "DROP TRIGGER trgAfterInsert_GFI_AMM_VehicleMaintTypesLink]";
                    if (bExistTrigger("trgAfterInsert_GFI_AMM_VehicleMaintTypesLink"))
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE  INDEX myIX_GFI_GPS_TripDetail_GPSDataUID
							ON GFI_GPS_TripDetail (GPSDataUID, iID, HeaderiID)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripDetail", "myIX_GFI_GPS_TripDetail_GPSDataUID")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "Rules and Exceptions");
                }
                #endregion
                #region Update 41. Access Profiles
                strUpd = "Rez000041";
                if (!CheckUpdateExist(strUpd))
                {
                    #region Tables

                    // -- Table: GFI_SYS_Modules
                    sql = @"
						begin
							execute immediate
								'CREATE TABLE GFI_SYS_Modules(
									""UID"" number(10) NOT NULL,
									ModuleName nvarchar2(255) NULL,
									Description nvarchar2(255) NULL,
									Command nvarchar2(255) NULL,
									CreatedDate timestamp(3) NULL,
									CreatedBy nvarchar2(30) NULL,
									UpdatedDate timestamp(3) NULL,
									UpdatedBy nvarchar2(30) NULL,
								 CONSTRAINT PK_GFI_SYS_Modules PRIMARY KEY  
								(""UID"")
								)';
						end;";
                    if (GetDataDT(ExistTableSql("GFI_SYS_Modules")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_SYS_Modules_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_SYS_Modules_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_SYS_Modules_seq_tr
								BEFORE INSERT ON GFI_SYS_Modules FOR EACH ROW
								WHEN(NEW.UID IS NULL)
								BEGIN
									SELECT GFI_SYS_Modules_seq.NEXTVAL INTO :NEW.UID FROM DUAL;
								END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    // -- Table: GFI_SYS_AccessTemplates
                    sql = @"
						begin
							execute immediate
								'CREATE TABLE GFI_SYS_AccessTemplates(
									""UID"" number(10) NOT NULL,
									TemplateName nvarchar2(120) NULL,
									Description nvarchar2(255) NULL,
									CreatedDate timestamp(3) NULL,
									CreatedBy nvarchar2(30) NULL,
									UpdatedDate timestamp(3) NULL,
									UpdatedBy nvarchar2(30) NULL,
								 CONSTRAINT PK_GFI_SYS_AccessTemplates PRIMARY KEY  
								(""UID"")
								)';
						end;";
                    if (GetDataDT(ExistTableSql("GFI_SYS_AccessTemplates")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_SYS_AccessTemplates_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_SYS_AccessTemplates_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    sql = @"CREATE OR REPLACE TRIGGER GFI_SYS_AccessTemplates_seq_tr
								BEFORE INSERT ON GFI_SYS_AccessTemplates FOR EACH ROW
								WHEN(NEW.""UID"" IS NULL)
								BEGIN
									SELECT GFI_SYS_AccessTemplates_seq.NEXTVAL INTO :NEW.""UID"" FROM DUAL;
								END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    // -- Table: GFI_SYS_AccessProfiles
                    sql = @"
						begin
							execute immediate
								 'CREATE TABLE GFI_SYS_AccessProfiles(
									""UID"" number(10) NOT NULL,
									TemplateId int NULL,
									ModuleId number(10) NULL,
									Command nvarchar2(255) NULL,
									AllowRead number(10) NULL,
									AllowNew number(10) NULL,
									AllowEdit number(10) NULL,
									AllowDelete number(10) NULL,
									CreatedDate timestamp(3) NULL,
									CreatedBy nvarchar2(30) NULL,
									UpdatedDate timestamp(3) NULL,
									UpdatedBy nvarchar2(30) NULL,
								 CONSTRAINT PK_GFI_SYS_AccessProfiles PRIMARY KEY  
								(""UID"")
								)';
					end;";
                    if (GetDataDT(ExistTableSql("GFI_SYS_AccessProfiles")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_SYS_AccessProfiles_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_SYS_AccessProfiles_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_SYS_AccessProfiles_seq_tr
								BEFORE INSERT ON GFI_SYS_AccessProfiles FOR EACH ROW
								WHEN(NEW.UID IS NULL)
								BEGIN
									SELECT GFI_SYS_AccessProfiles_seq.NEXTVAL INTO :NEW.UID FROM DUAL;
								END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    #endregion

                    #region Indexes
                    sql = @"CREATE  INDEX IX_GFI_SYS_AccessProfiles_ModuleId ON GFI_SYS_AccessProfiles(ModuleId ASC)";
                    if (GetDataDT(ExistsIndex("GFI_SYS_AccessProfiles", "IX_GFI_SYS_AccessProfiles_ModuleId")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE  INDEX IX_GFI_SYS_AccessProfiles_TemplateId ON GFI_SYS_AccessProfiles( TemplateId ASC)";
                    if (GetDataDT(ExistsIndex("GFI_SYS_AccessProfiles", "IX_GFI_SYS_AccessProfiles_TemplateId")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    #endregion

                    #region Constraints
                    sql = @"ALTER TABLE GFI_SYS_AccessProfiles  ADD  CONSTRAINT FK_GFI_SYS_AccessProfiles_GFI_SYS_AccessTemplates FOREIGN KEY(TemplateId)
						REFERENCES GFI_SYS_AccessTemplates (""UID"")";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_AccessProfiles_GFI_SYS_AccessTemplates", "GFI_SYS_AccessProfiles")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_AccessProfiles  ADD  CONSTRAINT FK_GFI_SYS_AccessProfiles_GFI_SYS_Modules FOREIGN KEY(ModuleId)
						REFERENCES GFI_SYS_Modules (""UID"")";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_AccessProfiles_GFI_SYS_Modules", "GFI_SYS_AccessProfiles")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_AccessProfiles CHECK CONSTRAINT FK_GFI_SYS_AccessProfiles_GFI_SYS_AccessTemplates";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_AccessProfiles_GFI_SYS_AccessTemplates", "GFI_SYS_AccessProfiles")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_AccessProfiles CHECK CONSTRAINT FK_GFI_SYS_AccessProfiles_GFI_SYS_Modules";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_AccessProfiles_GFI_SYS_Modules", "GFI_SYS_AccessProfiles")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    #endregion

                    InsertDBUpdate(strUpd, "Access Profiles");
                }
                #endregion
                #region Update 42. Reconfigure Vehicle Maint Status.
                strUpd = "Rez000042";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_GPS_Exceptions add GroupID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_Exceptions", "GroupID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE  INDEX IX_GroupID ON GFI_GPS_Exceptions
					(
						GroupID ASC
					)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_Exceptions", "IX_GroupID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    //COMPLETED
                    VehicleMaintStatus objVMS = new VehicleMaintStatus();
                    objVMS = vehicleMaintStatusService.GetVehicleMaintStatusByDescription("COMPLETED", sConnStr);
                    if (objVMS != null)
                    {
                        objVMS.SortOrder = 6;
                        objVMS.UpdatedDate = DateTime.Now;
                        objVMS.UpdatedBy = usrInst.Username;
                        vehicleMaintStatusService.UpdateVehicleMaintStatus(objVMS, null, sConnStr);
                    }

                    objVMS = vehicleMaintStatusService.GetVehicleMaintStatusByDescription("VALID", sConnStr);
                    if (objVMS != null)
                    {
                        objVMS.SortOrder = 5;
                        objVMS.UpdatedDate = DateTime.Now;
                        objVMS.UpdatedBy = usrInst.Username;
                        vehicleMaintStatusService.UpdateVehicleMaintStatus(objVMS, null, sConnStr);
                    }

                    InsertDBUpdate(strUpd, "Reconfigure Vehicle Maint Status N Exception GroupID");
                }
                #endregion
                #region Update 43. Add field AccessTemplateId] to table GFI_SYS_User]
                strUpd = "Rez000043";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_SYS_User ADD AccessTemplateId int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "AccessTemplateId")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    InsertDBUpdate(strUpd, "Add field AccessTemplateId] to table GFI_SYS_User]");
                }
                #endregion
                #region Update 44. Add Application Modules to the Modules Tables
                strUpd = "Rez000044";
                if (!CheckUpdateExist(strUpd))
                {
                    NaveoOneLib.Models.Permissions.Module module = new NaveoOneLib.Models.Permissions.Module();
                    module = moduleService.GetModulesByName("MAPS --> LIVE MAP", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Permissions.Module();
                        module.ModuleName = "MAPS --> LIVE MAP";
                        module.Description = "MAPS --> LIVE MAP";
                        module.Command = "CmdLiveMap";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        moduleService.SaveModules(module, null, sConnStr);
                    }

                    module = moduleService.GetModulesByName("MAPS --> TRIP HISTORY", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Permissions.Module();
                        module.ModuleName = "MAPS --> TRIP HISTORY";
                        module.Description = "MAPS --> TRIP HISTORY";
                        module.Command = "CmdTripHistory";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        moduleService.SaveModules(module, null, sConnStr);
                    }

                    module = moduleService.GetModulesByName("MAPS --> Zone VisitedNotVisited", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Permissions.Module();
                        module.ModuleName = "MAPS --> Zone VisitedNotVisited";
                        module.Description = "MAPS --> Zone VisitedNotVisited";
                        module.Command = "CmdZoneVisitedNotVisited";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        moduleService.SaveModules(module, null, sConnStr);
                    }

                    module = moduleService.GetModulesByName("MAPS --> Test Trip History", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Permissions.Module();
                        module.ModuleName = "MAPS --> Test Trip History";
                        module.Description = "MAPS --> Test Trip History";
                        module.Command = "CmdTestTripHistory";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        moduleService.SaveModules(module, null, sConnStr);
                    }

                    module = moduleService.GetModulesByName("Vehicles & Assets --> Current Status", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Permissions.Module();
                        module.ModuleName = "Vehicles & Assets --> Current Status";
                        module.Description = "Vehicles & Assets --> Current Status";
                        module.Command = "CmdCurrentStatus";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        moduleService.SaveModules(module, null, sConnStr);
                    }

                    module = moduleService.GetModulesByName("Vehicles & Assets --> Asset List", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Permissions.Module();
                        module.ModuleName = "Vehicles & Assets --> Asset List";
                        module.Description = "Vehicles & Assets --> Asset List";
                        module.Command = "cmdAssetVehList";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        moduleService.SaveModules(module, null, sConnStr);
                    }

                    module = moduleService.GetModulesByName("Vehicles & Assets --> Asset Maint. Dashboard", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Permissions.Module();
                        module.ModuleName = "Vehicles & Assets --> Asset Maint. Dashboard";
                        module.Description = "Vehicles & Assets --> Asset Maint. Dashboard";
                        module.Command = "cmdAssetMainStatusRep";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        moduleService.SaveModules(module, null, sConnStr);
                    }

                    module = moduleService.GetModulesByName("Vehicles & Assets --> Assign Maint. Types", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Permissions.Module();
                        module.ModuleName = "Vehicles & Assets --> Assign Maint. Types";
                        module.Description = "Vehicles & Assets --> Assign Maint. Types";
                        module.Command = "cmdBulkAssignMaintTypes";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        moduleService.SaveModules(module, null, sConnStr);
                    }

                    module = moduleService.GetModulesByName("Vehicles & Assets --> Assets Management", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Permissions.Module();
                        module.ModuleName = "Vehicles & Assets --> Assets Management";
                        module.Description = "Vehicles & Assets --> Assets Management";
                        module.Command = "CmdAssetsMgmt";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        moduleService.SaveModules(module, null, sConnStr);
                    }

                    module = moduleService.GetModulesByName("Vehicles & Assets --> Drivers Management", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Permissions.Module();
                        module.ModuleName = "Vehicles & Assets --> Drivers Management";
                        module.Description = "Vehicles & Assets --> Drivers Management";
                        module.Command = "cmdDriversMgmt";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        moduleService.SaveModules(module, null, sConnStr);
                    }

                    module = moduleService.GetModulesByName("Rules & Messaging --> Exceptions", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Permissions.Module();
                        module.ModuleName = "Rules & Messaging --> Exceptions";
                        module.Description = "Rules & Messaging --> Exceptions";
                        module.Command = "CmdExceptions";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        moduleService.SaveModules(module, null, sConnStr);
                    }

                    module = moduleService.GetModulesByName("Rules & Messaging --> Manage Exceptions", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Permissions.Module();
                        module.ModuleName = "Rules & Messaging --> Manage Exceptions";
                        module.Description = "Rules & Messaging --> Manage Exceptions";
                        module.Command = "CmdManageExceptions";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        moduleService.SaveModules(module, null, sConnStr);
                    }

                    module = moduleService.GetModulesByName("Rules & Messaging --> Messaging", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Permissions.Module();
                        module.ModuleName = "Rules & Messaging --> Messaging";
                        module.Description = "Rules & Messaging --> Messaging";
                        module.Command = "CmdMessaging";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        moduleService.SaveModules(module, null, sConnStr);
                    }

                    module = moduleService.GetModulesByName("Rules & Messaging --> Rules", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Permissions.Module();
                        module.ModuleName = "Rules & Messaging --> Rules";
                        module.Description = "Rules & Messaging --> Rules";
                        module.Command = "CmdRules";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        moduleService.SaveModules(module, null, sConnStr);
                    }

                    module = moduleService.GetModulesByName("Rules & Messaging --> Reprocess Rules", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Permissions.Module();
                        module.ModuleName = "Rules & Messaging --> Reprocess Rules";
                        module.Description = "Rules & Messaging --> Reprocess Rules";
                        module.Command = "cmdReprocessRules";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        moduleService.SaveModules(module, null, sConnStr);
                    }

                    module = moduleService.GetModulesByName("Zone Management --> Zone Types", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Permissions.Module();
                        module.ModuleName = "Zone Management --> Zone Types";
                        module.Description = "Zone Management --> Zone Types";
                        module.Command = "CmdZoneTypes";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        moduleService.SaveModules(module, null, sConnStr);
                    }

                    module = moduleService.GetModulesByName("Zone Management --> Zone Management", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Permissions.Module();
                        module.ModuleName = "Zone Management --> Zone Management";
                        module.Description = "Zone Management --> Zone Management";
                        module.Command = "CmdZoneManagement";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        moduleService.SaveModules(module, null, sConnStr);
                    }

                    module = moduleService.GetModulesByName("Reports --> Zones Visited", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Permissions.Module();
                        module.ModuleName = "Reports --> Zones Visited";
                        module.Description = "Reports --> Zones Visited";
                        module.Command = "CmdZonesVisited";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        moduleService.SaveModules(module, null, sConnStr);
                    }

                    module = moduleService.GetModulesByName("Reports --> Fuel Consumption", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Permissions.Module();
                        module.ModuleName = "Reports --> Fuel Consumption";
                        module.Description = "Reports --> Fuel Consumption";
                        module.Command = "CmdFuelConsumption";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        moduleService.SaveModules(module, null, sConnStr);
                    }

                    module = moduleService.GetModulesByName("Reports --> Exception Report", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Permissions.Module();
                        module.ModuleName = "Reports --> Exception Report";
                        module.Description = "Reports --> Exception Report";
                        module.Command = "CmdExceptionReport";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        moduleService.SaveModules(module, null, sConnStr);
                    }

                    module = moduleService.GetModulesByName("Reports --> Asset Summary", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Permissions.Module();
                        module.ModuleName = "Reports --> Asset Summary";
                        module.Description = "Reports --> Asset Summary";
                        module.Command = "CmdAssetSummary";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        moduleService.SaveModules(module, null, sConnStr);
                    }

                    module = moduleService.GetModulesByName("Reports --> Daily Summary", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Permissions.Module();
                        module.ModuleName = "Reports --> Daily Summary";
                        module.Description = "Reports --> Daily Summary";
                        module.Command = "CmdDailySummary";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        moduleService.SaveModules(module, null, sConnStr);
                    }

                    module = moduleService.GetModulesByName("Reports --> Auxilliary Report", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Permissions.Module();
                        module.ModuleName = "Reports --> Auxilliary Report";
                        module.Description = "Reports --> Auxilliary Report";
                        module.Command = "CmdAuxilliaryReport";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        moduleService.SaveModules(module, null, sConnStr);
                    }

                    module = moduleService.GetModulesByName("Reports --> Automatic Reporting", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Permissions.Module();
                        module.ModuleName = "Reports --> Automatic Reporting";
                        module.Description = "Reports --> Automatic Reporting";
                        module.Command = "cmdAutoReportingConfig";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        moduleService.SaveModules(module, null, sConnStr);
                    }

                    module = moduleService.GetModulesByName("Reports --> Maint. Costs Report", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Permissions.Module();
                        module.ModuleName = "Reports --> Maint. Costs Report";
                        module.Description = "Reports --> Maint. Costs Report";
                        module.Command = "cmdMaintCostReport";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        moduleService.SaveModules(module, null, sConnStr);
                    }

                    module = moduleService.GetModulesByName("Advanced --> Asset Maintenance", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Permissions.Module();
                        module.ModuleName = "Advanced --> Asset Maintenance";
                        module.Description = "Advanced --> Asset Maintenance";
                        module.Command = "CmdAdvAssetMaintenance";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        moduleService.SaveModules(module, null, sConnStr);
                    }

                    module = moduleService.GetModulesByName("Advanced --> Change Password", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Permissions.Module();
                        module.ModuleName = "Advanced --> Change Password";
                        module.Description = "Advanced --> Change Password";
                        module.Command = "CmdChangePassword";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        moduleService.SaveModules(module, null, sConnStr);
                    }

                    module = moduleService.GetModulesByName("Advanced --> Users Management", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Permissions.Module();
                        module.ModuleName = "Advanced --> Users Management";
                        module.Description = "Advanced --> Users Management";
                        module.Command = "CmdUsersMgmt";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        moduleService.SaveModules(module, null, sConnStr);
                    }

                    module = moduleService.GetModulesByName("Advanced --> User Access Templates", sConnStr);
                    if (module == null)
                    {
                        module = new NaveoOneLib.Models.Permissions.Module();
                        module.ModuleName = "Advanced --> User Access Templates";
                        module.Description = "Advanced --> User Access Templates";
                        module.Command = "cmdUserAccessTemplate";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        moduleService.SaveModules(module, null, sConnStr);
                    }

                    InsertDBUpdate(strUpd, "Add Application Modules to the Modules Tables");
                }
                #endregion
                #region Update 45. Zone refs
                strUpd = "Rez000045";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_FLT_ZoneHeader ADD ExternalRef nvarchar2(30) Default ''";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ZoneHeader", "ExternalRef")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_ZoneHeader ADD Ref2 nvarchar2(50) Default ''";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ZoneHeader", "Ref2")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE  INDEX my_IX_GFI_GPS_TripDetail ON GFI_GPS_TripDetail
							(
								HeaderiID ASC
							)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripDetail", "my_IX_GFI_GPS_TripDetail")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "Zone refs");
                }
                #endregion
                #region Update 46. User Settings
                strUpd = "Rez000046";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"
						begin
							execute immediate
								'CREATE TABLE GFI_SYS_UserSettings(
									iID number(10) NOT NULL,
									UserID number(10) NOT NULL,
									ModuleID nvarchar2(50) NOT NULL,
									ParamType nvarchar2(50) NOT NULL,
									Parameter nvarchar2(100) NOT NULL,
									ParamValue1 nvarchar2(100) NULL,
									ParamValue2 nvarchar2(100) NULL,
									ParamValue3 nvarchar2(100) NULL,
									ParamDate1 timestamp(3) NULL,
									ParamDate2 timestamp(3) NULL,
								 CONSTRAINT PK_GFI_SYS_UserSettings PRIMARY KEY  
								(iID)
								)';
						end;";
                    if (GetDataDT(ExistTableSql("GFI_SYS_UserSettings")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_SYS_UserSettings_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_SYS_UserSettings_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_SYS_UserSettings_seq_tr
								BEFORE INSERT ON GFI_SYS_UserSettings FOR EACH ROW
								WHEN(NEW.iID IS NULL)
								BEGIN
									SELECT GFI_SYS_UserSettings_seq.NEXTVAL INTO :NEW.iID FROM DUAL;
								END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_UserSettings 
							ADD CONSTRAINT FK_GFI_SYS_UserSettings_GFI_SYS_User 
								FOREIGN KEY(UserID) 
								REFERENCES GFI_SYS_User(""UID"") 
						 ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_UserSettings_GFI_SYS_User", "GFI_SYS_UserSettings")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE  INDEX IX_UserSettings ON GFI_SYS_UserSettings
						(UserID ASC)";
                    if (GetDataDT(ExistsIndex("GFI_SYS_UserSettings", "IX_ModuleId")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    sql = @"ALTER TABLE GFI_FLT_Driver ADD EmployeeType nvarchar(15) NOT NULL Default 'Driver'";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "EmployeeType")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    sql = @"CREATE  INDEX my_IX_EmployeeType ON GFI_FLT_Driver
							(
								EmployeeType ASC
							)";
                    if (GetDataDT(ExistsIndex("GFI_FLT_Driver", "my_IX_EmployeeType")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "User Settings and EmployeeType");
                }
                #endregion
                #region Update 47. TripHeader and GPSData indexes
                strUpd = "Rez000047";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE  INDEX my_IX_AssetID_GFI_GPS_TripHeader ON GFI_GPS_TripHeader (AssetID)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "my_IX_AssetID_GFI_GPS_TripHeader")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"DROP INDEX my_IX_GFI_GPS_TripHeader";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "my_IX_GFI_GPS_TripHeader")).Rows[0][0].ToString() != "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX my_IX_GFI_GPS_TripHeader ON GFI_GPS_TripHeader
						(
							AssetID ASC,
							DriverID ASC,
							GPSDataStartUID ASC,
							GPSDataEndUID ASC
						)/*WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)*/";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "my_IX_GFI_GPS_TripHeader")).Rows.Count > 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX my_IX_EndUID_GFI_GPS_TripHeader ON GFI_GPS_TripHeader (GPSDataEndUID)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "my_IX_EndUID_GFI_GPS_TripHeader")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"ALTER TABLE GFI_GPS_Exceptions ADD Posted int Default 0";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_Exceptions", "Posted")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "TH indexes and Exception fields");
                }
                #endregion
                #region Update 48. Notification Table
                strUpd = "Rez000048";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"
						begin
							execute immediate
								'CREATE TABLE GFI_SYS_Notification(
									 NID number(10) NOT NULL,
									 ARID number(10) NULL,
									 ExceptionID number(10) NULL,
									 ""UID"" number(10) null,
									 Status number(3) Default (0) NULL ,
									 InsertedAt timestamp(3) DEFAULT (systimestamp) NULL ,
									 SendAt timestamp(3) NULL,
									 CONSTRAINT PK_Notification PRIMARY KEY  
										(NID )
							
								)';
						end;";
                    if (GetDataDT(ExistTableSql("GFI_SYS_Notification")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_SYS_Notification_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_SYS_Notification_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_SYS_Notification_seq_tr
								BEFORE INSERT ON GFI_SYS_Notification FOR EACH ROW
								WHEN(NEW.NID IS NULL)
								BEGIN
									SELECT GFI_SYS_Notification_seq.NEXTVAL INTO :NEW.NID FROM DUAL;
								END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE  INDEX ARID_ExID ON GFI_SYS_Notification (ARID ASC, ExceptionID ASC)";
                    if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "ARID_ExID")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "Notification Table");
                }
                #endregion
                #region Update 49. RegisterDevicesForProcessing 5 days max
                strUpd = "Rez000049";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "delete from GFI_GPS_ProcessPending where dtDateFrom < INTERVAL '-5' day(5) + systimestamp";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"
CREATE OR REPLACE PROCEDURE RegisterDevicesForProcessing
(
    p_AssetID NUMBER,
    p_dtDateFrom timestamp,
    p_ProcessCode NVARCHAR2--, outcount OUT NUMBER
)
AS
    v_ValidDateTime timestamp(3);
    v_temp NUMBER(13, 0) := 0;
BEGIN
    v_ValidDateTime := systimestamp + INTERVAL '-5' day; 
    IF ( p_dtDateFrom > v_ValidDateTime ) THEN
        BEGIN
            SELECT 1 INTO v_temp
                FROM DUAL
                WHERE EXISTS (SELECT T1.* FROM GFI_GPS_ProcessPending T1
                                WHERE  AssetID = p_AssetID
                                   AND ProcessCode = p_ProcessCode
                                   AND Processing = 0 
                             );                
            IF v_temp = 1 THEN
                BEGIN
                    BEGIN
                        SELECT 2 INTO v_temp FROM DUAL 
                            WHERE EXISTS -- (p_dtDateFrom < 
                                (
                                    SELECT T1.* FROM GFI_GPS_ProcessPending T1
                                        WHERE  AssetID = p_AssetID
                                            AND ProcessCode = p_ProcessCode
                                            AND Processing = 0 
                                            and p_dtDateFrom < dtDateFrom
                                ); 
                        --);
                    EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                            SELECT 0 INTO v_temp from dual;
                        when others then select -1 INTO v_temp from dual;
                    END;
                
                    IF v_temp = 2 THEN
                        UPDATE GFI_GPS_ProcessPending
                            SET dtDateFrom = p_dtDateFrom
                        WHERE AssetID = p_AssetID
                            AND ProcessCode = p_ProcessCode
                            AND Processing = 0;
                    END IF;
                END;
            END IF;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                INSERT INTO GFI_GPS_ProcessPending (AssetID, dtDateFrom, ProcessCode) VALUES ( p_AssetID, p_dtDateFrom, p_ProcessCode );  
            when others then select 7 into v_temp from dual;            
        END;
    ELSE    --ignoring
        --BEGIN
            --p_ProcessCode := p_ProcessCode || 'Ignored';
            BEGIN
                SELECT 10 INTO v_temp FROM DUAL
                    WHERE EXISTS (SELECT T1.* FROM GFI_GPS_ProcessPending T1
                                    WHERE  AssetID = p_AssetID
                                        AND ProcessCode = p_ProcessCode || 'Ignored'
                                        AND Processing = 0 
                                 );                                 
                IF v_temp = 10 THEN      
                    BEGIN
                        BEGIN
                            SELECT 20 INTO v_temp FROM DUAL
                                WHERE (p_dtDateFrom < 
                                    (
                                        SELECT dtDateFrom FROM GFI_GPS_ProcessPending 
                                            WHERE AssetID = p_AssetID
                                                AND ProcessCode = p_ProcessCode || 'Ignored'
                                                AND Processing = 0
                                    )
                            );
                        EXCEPTION
                            WHEN NO_DATA_FOUND THEN SELECT 0 INTO v_temp from dual;
                        END;
                                 
                        IF v_temp = 20 THEN
                            UPDATE GFI_GPS_ProcessPending
                                SET dtDateFrom = p_dtDateFrom
                            WHERE  AssetID = p_AssetID
                                AND ProcessCode = p_ProcessCode || 'Ignored'
                                AND Processing = 0;
                        END IF;
                    END;
                END IF; 
            EXCEPTION
                WHEN NO_DATA_FOUND THEN 
                    INSERT INTO GFI_GPS_ProcessPending (AssetID, dtDateFrom, ProcessCode) VALUES(p_AssetID, p_dtDateFrom, p_ProcessCode || 'Ignored');                
            END; 
        --END;  
    END IF;
    --outcount:=v_temp;
END;
--AlreadyOracleStatement";
                    _SqlConn.OracleScriptExecute(sql, sConnStr);

                    #region Transit tables. Not in SQL
                    sql = "CREATE GLOBAL TEMPORARY TABLE GlobalTmpProcessTable (AssetID int, dt timestamp(3)) ON COMMIT DELETE ROWS";
                    CreateTable(sql, "GlobalTmpProcessTable", String.Empty);

                    sql = @"CREATE GLOBAL TEMPORARY TABLE GlobalTmp_GFI_GPS_GPSData
                            (
                                iid int,
                                DeviceID varchar(50),
                                iButton varchar(50),
                                DateTimeGPS_UTC timestamp(3),
                                Longitude float,
                                Latitude float,
                                LongLatValidFlag int,
                                Speed float,
                                EngineOn int,
                                StopFlag int,
                                TripDistance int,
                                TripTime int,
                                WorkHour int,
                                AssetID int, DriverID int, GPSDataID int
                            ) ON COMMIT DELETE ROWS";
                    CreateTable(sql, "GlobalTmp_GFI_GPS_GPSData", String.Empty);

                    sql = @"CREATE GLOBAL TEMPORARY TABLE GlobalTmp_GFI_GPS_GPSDataDetail
                    (
                        UID int NULL,
                        TypeID nvarchar2(30) NULL,
                        TypeValue nvarchar2(30) NULL,
                        UOM nvarchar2(15) NULL,
                        Remarks nvarchar2(50) NULL
                    ) ON COMMIT DELETE ROWS";
                    CreateTable(sql, "GlobalTmp_GFI_GPS_GPSDataDetail", String.Empty);

                    sql = @"CREATE GLOBAL TEMPORARY TABLE GlobalTmpProcessRoadSpeed
                    (
                        UID int NOT NULL,
                        AssetID int null,
                        RoadSpeed int NULL,
                        RoadType nvarchar2(25) NULL,
                        FinalRoadSpeed int NULL,
                        TypeRoadSpeed int NULL
                    ) ON COMMIT DELETE ROWS";
                    CreateTable(sql, "GlobalTmpProcessRoadSpeed", String.Empty);

                    sql = @"CREATE GLOBAL TEMPORARY TABLE GlobalTmpLiveAssets
                            (
	                            [RowNumber] [int] IDENTITY(1,1) NOT NULL,
	                            [iAssetID] [int] PRIMARY KEY CLUSTERED,
                                Asset nvarchar(120),
                                TimeZone int Default 0
                            ) ON COMMIT PRESERVE ROWS";
                    CreateTable(sql, "GlobalTmpLiveAssets", "RowNumber");

                    sql = @"CREATE GLOBAL TEMPORARY TABLE GlobalTmpLiveData
                            (
                                uid int null
                                , AssetID int null
                                , Asset nvarchar2(225) NULL
                                , DriverID int null
                                , DateTimeGPS_UTC timestamp(3)
                                , dtLocalTime timestamp(3)
                                , DateTimeServer  timestamp(3)
                                , Longitude float
                                , Latitude float
                                , LongLatValidFlag int null
                                , Speed int null 
                                , EngineOn int null
                                , StopFlag int null
                                , TripDistance int null
                                , TripTime int null 
                                , Row_Num int null
                                , AssetStatus nvarchar2(225) NULL
                                , Comment nvarchar2(225) NULL
                            ) ON COMMIT PRESERVE ROWS";
                    CreateTable(sql, "GlobalTmpLiveData", String.Empty);

                    sql = "CREATE GLOBAL TEMPORARY TABLE GlobalTmpZonesIDs (i int) ON COMMIT DELETE ROWS";
                    CreateTable(sql, "GlobalTmpZonesIDs", String.Empty);

                    sql = "CREATE GLOBAL TEMPORARY TABLE GlobalTmpfTmpZone (i int) ON COMMIT DELETE ROWS";
                    CreateTable(sql, "GlobalTmpfTmpZone", String.Empty);

                    sql = "CREATE GLOBAL TEMPORARY TABLE GlobalTmptTmpDistinctZone (i int) ON COMMIT PRESERVE ROWS";
                    CreateTable(sql, "GlobalTmptTmpDistinctZone", String.Empty);

                    sql = "CREATE GLOBAL TEMPORARY TABLE GlobalTmpZones (ZoneID int, minLat float, minLon float, maxLat float, maxLon float) ON COMMIT PRESERVE ROWS";
                    CreateTable(sql, "GlobalTmpZones", String.Empty);
                    #endregion
                    InsertDBUpdate(strUpd, "RegisterDevicesForProcessing 5 days max");
                }
                #endregion
                #region Update 50. GFI_AMM_VehicleMaintTypes GM assignment
                strUpd = "Rez000050";
                if (!CheckUpdateExist(strUpd))
                {
                    DataTable dtMainType = GetDataDT("SELECT * FROM GFI_AMM_VehicleMaintTypes");
                    foreach (DataRow r in dtMainType.Rows)
                    {
                        VehicleMaintType obj = vehicleMaintTypeService.GetVehicleMaintTypeById(r["MaintTypeId"].ToString(), sConnStr);
                        if (obj.lMatrix.Count == 0)
                        {
                            obj.lMatrix = usrInst.lMatrix;
                            foreach (Matrix m in obj.lMatrix)
                            {
                                m.iID = obj.MaintTypeId;
                                m.oprType = DataRowState.Added;
                            }
                            vehicleMaintTypeService.SaveVehicleMaintType(obj, false, sConnStr);
                        }
                    }
                    foreach (DataRow r in dtMainType.Rows)
                    {
                        VehicleMaintType obj = vehicleMaintTypeService.GetVehicleMaintTypeById(r["MaintTypeId"].ToString(), sConnStr);
                        if (obj.CreatedBy == String.Empty)
                        {
                            obj.CreatedBy = usrInst.Username;
                            vehicleMaintTypeService.SaveVehicleMaintType(obj, false, sConnStr);
                        }
                    }
                    InsertDBUpdate(strUpd, "GFI_AMM_VehicleMaintTypes GM assignment");
                }
                #endregion
                #region Update 51. AccessTemplate Group Matrix.
                strUpd = "Rez000051";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"
					  begin
						execute immediate
							  'CREATE TABLE GFI_SYS_GroupMatrixAccessTemplate
								(
									MID NUMBER(10) NOT NULL
									, GMID NUMBER(10) NOT NULL
									, iID NUMBER(10) NOT NULL
									, CONSTRAINT PK_GFI_SYS_GroupMatrixAccessTemplate PRIMARY KEY  (MID)
								)';
					  end;";
                    if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrixAccessTemplate")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_SYS_GroupMatrixAccessTemplate_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_SYS_GroupMatrixAccessTemplate_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_SYS_GroupMatrixAccessTemplate_seq_tr
								BEFORE INSERT ON GFI_SYS_GroupMatrixAccessTemplate FOR EACH ROW
								WHEN(NEW.MID IS NULL)
								BEGIN
									SELECT GFI_SYS_GroupMatrixAccessTemplate_seq.NEXTVAL INTO :NEW.MID FROM DUAL;
								END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixAccessTemplate 
							ADD CONSTRAINT FK_GFI_SYS_GroupMatrixAccessTemplate_GFI_SYS_GroupMatrix 
								FOREIGN KEY (GMID) 
								REFERENCES GFI_SYS_GroupMatrix (GMID)  ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixAccessTemplate_GFI_SYS_GroupMatrix", "GFI_SYS_GroupMatrixAccessTemplate")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixAccessTemplate 
							ADD CONSTRAINT FK_GFI_SYS_GroupMatrixAccessTemplate_GFI_SYS_AccessTemplates 
								FOREIGN KEY(iID) 
								REFERENCES GFI_SYS_AccessTemplates(UID) 
						 ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixAccessTemplate_GFI_SYS_AccessTemplates", "GFI_SYS_GroupMatrixAccessTemplate")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "AccessTemplate Group Matrix");
                }
                #endregion
                #region Update 52. GFI_SYS_AccessTemplates GM assignment
                strUpd = "Rez000052";
                if (!CheckUpdateExist(strUpd))
                {
                    DataTable dtAT = GetDataDT("SELECT * FROM GFI_SYS_AccessTemplates");
                    foreach (DataRow r in dtAT.Rows)
                    {
                        AccessTemplate obj = new AccessTemplate().GetAccessTemplatesById(r["UID"].ToString(), sConnStr);
                        if (obj.lMatrix.Count == 0)
                        {
                            obj.lMatrix = usrInst.lMatrix;
                            obj.CreatedBy = usrInst.Username;
                            obj.UpdatedBy = usrInst.Username;
                            foreach (Matrix m in obj.lMatrix)
                            {
                                m.iID = obj.UID;
                                m.oprType = DataRowState.Added;
                            }
                            obj.Save(obj, false, new DataTable(), sConnStr);
                        }
                    }
                    InsertDBUpdate(strUpd, "GFI_SYS_AccessTemplates GM assignment");
                }
                #endregion
                #region Update 53. TripDetail UID now BigInt.
                strUpd = "Rez000053";
                if (!CheckUpdateExist(strUpd))
                {
                    InsertDBUpdate(strUpd, "TripDetail UID now BigInt");
                }
                #endregion
                #region Update 54. Group Matrix IsCompany field
                strUpd = "Rez000054";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_SYS_GroupMatrix ADD IsCompany int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_GroupMatrix", "IsCompany")).Rows[0][0].ToString() == "0")
                    {
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                        //sql = @"
                        //--ALTER TABLE GFI_SYS_GroupMatrix SET (LOCK_ESCALATION = TABLE)

                        //Update GFI_SYS_GroupMatrix set IsCompany = 1 where ParentGMID = 3

                        //DECLARE @GMID int
                        //DECLARE @GMDESCRITION nvarchar(100)
                        //Declare @ParentGMID int
                        //DECLARE @Remarks nchar(100)
                        //DECLARE @IsCompany int

                        //DECLARE curgm CURSOR FOR SELECT GMID
                        //      ,GMDescription
                        //      ,ParentGMID
                        //      ,Remarks
                        //      ,IsCompany
                        //  FROM GFI_SYS_GroupMatrix WHERE ISCompany = 1 and GMDescription <>''

                        //OPEN curgm
                        //--IF @@CURSOR_ROWS > 0
                        // --BEGIN
                        // FETCH NEXT FROM curgm INTO @GMID,@GMDESCRITION,@ParentGMID,@Remarks,@IsCompany
                        //  WHILE @@Fetch_status = 0
                        //  BEGIN
                        //  --SELECT @GMID = GMID ,@GMDESCRITION =  GMDESCRITION, 
                        //  --INSERT INTO GFI_SYS_GroupMatrix (GMDescription,ParentGMID,Remarks,IsCompany) Values(CONCAT('### DEFAULT_',@GMDESCRITION,' ###' ), @GMID,'Default Content Group',0 )
                        //  INSERT INTO GFI_SYS_GroupMatrix (GMDescription,ParentGMID,Remarks,IsCompany) Values('### DEFAULT_'+ @GMDESCRITION + ' ###', @GMID,'Default Content Group',0 )

                        //  FETCH NEXT FROM curgm INTO @GMID,@GMDESCRITION,@ParentGMID,@Remarks,@IsCompany
                        //  END

                        // --END

                        //CLOSE curgm
                        //DEALLOCATE curgm";
                        //dbExecute(sql);
                    }
                    InsertDBUpdate(strUpd, "Group Matrix IsCompany field");
                }
                #endregion
                #region Update 55. GFI_SYS_PARAMETERSTable with
                strUpd = "Rez000055";
                if (!CheckUpdateExist(strUpd))
                {
                    InsertDBUpdate(strUpd, "PARAMETER TABLE WITH DEFAULT Value");
                }
                #endregion
                #region Update 56. my_IX_MaxGrpID on GFI_GPS_Exceptions
                strUpd = "Rez000056";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE  INDEX my_IX_MaxGrpID ON GFI_GPS_Exceptions (AssetID)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_Exceptions", "my_IX_MaxGrpID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "my_IX_MaxGrpID on GFI_GPS_Exceptions");
                }
                #endregion
                #region Update 57. myIdxDT on GFI_GPS_GPSData
                strUpd = "Rez000057";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE  INDEX myIdxDT ON GFI_GPS_GPSData (DateTimeGPS_UTC)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSData", "myIdxDT")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "myIdxDT on GFI_GPS_GPSData");
                }
                #endregion
                #region Update 58. GFI_FLT_AutoReportingConfig columns
                strUpd = "Rez000058";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_FLT_AutoReportingConfig ADD ReportFormat NVARCHAR2(100) Default ''";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AutoReportingConfig", "ReportFormat")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "GFI_FLT_AutoReportingConfig columns");
                }
                #endregion
                #region Update 59. Indexes
                strUpd = "Rez000059";
                if (!CheckUpdateExist(strUpd))
                {
                    //GPSData
                    sql = "DROP INDEX IX_GFI_GPS_GPSData_AssetID_DateTimeGPS";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSData", "IX_GFI_GPS_GPSData_AssetID_DateTimeGPS")).Rows[0][0].ToString() != "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "DROP INDEX IX_GFI_GPS_GPSData_ProcessTrips";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSData", "IX_GFI_GPS_GPSData_ProcessTrips")).Rows[0][0].ToString() != "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "DROP INDEX MyIdxAssetID";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSData", "MyIdxAssetID")).Rows[0][0].ToString() != "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "DROP INDEX my_IX_GFI_GPS_GPSData";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSData", "my_IX_GFI_GPS_GPSData")).Rows[0][0].ToString() != "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "DROP INDEX MyIdxTripProcessor";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSData", "MyIdxTripProcessor")).Rows[0][0].ToString() != "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX IX_GFI_GPS_GPSData_ProcessTrips
							ON GFI_GPS_GPSData] (AssetID, DateTimeGPS_UTC)
							INCLUDE ([UID],[DateTimeServer],[Longitude],[Latitude],[LongLatValidFlag],[Speed],[EngineOn],[StopFlag],[TripDistance],[TripTime],[WorkHour],[DriverID])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSData", "IX_GFI_GPS_GPSData_ProcessTrips")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    //GFI_GPS_GPSDataDetail
                    sql = "DROP INDEX MyIdxDTripProcessor";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSDataDetail", "MyIdxDTripProcessor")).Rows[0][0].ToString() != "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE INDEX MyIdxDTripProcessor
							ON GFI_GPS_GPSDataDetail (TypeID)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSDataDetail", "MyIdxDTripProcessor")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    //GFI_FLT_ZoneDetail
                    sql = "DROP INDEX myIdxGFI_FLT_ZoneDetailZoneID";
                    if (GetDataDT(ExistsIndex("GFI_FLT_ZoneDetail", "myIdxGFI_FLT_ZoneDetailZoneID")).Rows[0][0].ToString() != "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX myIdxGFI_FLT_ZoneDetailZoneID
							ON GFI_FLT_ZoneDetail (ZoneID)";
                    if (GetDataDT(ExistsIndex("GFI_FLT_ZoneDetail", "myIdxGFI_FLT_ZoneDetailZoneID")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    //GFI_GPS_Exceptions
                    sql = "DROP INDEX MyIxExceptionProcessor";
                    if (GetDataDT(ExistsIndex("GFI_GPS_Exceptions", "MyIxExceptionProcessor")).Rows[0][0].ToString() != "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX MyIxExceptionProcessor
							ON GFI_GPS_Exceptions (AssetID, GPSDataID,iID)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_Exceptions", "MyIxExceptionProcessor")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    //GFI_GPS_TripHeader
                    sql = "DROP INDEX my_IX_AssetID_GFI_GPS_TripHeader";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "my_IX_AssetID_GFI_GPS_TripHeader")).Rows[0][0].ToString() != "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "DROP INDEX my_IX_EndUID_GFI_GPS_TripHeader";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "my_IX_EndUID_GFI_GPS_TripHeader")).Rows[0][0].ToString() != "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "DROP INDEX MyIXAssetDriver";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "MyIXAssetDriver")).Rows[0][0].ToString() != "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX MyIXAsset
							ON GFI_GPS_TripHeader (AssetID)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "MyIXAsset")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE INDEX MyIXDriver
							ON GFI_GPS_TripHeader (DriverID)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "MyIXDriver")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "Indexes");
                }
                #endregion
                #region Update 60. TripHeader Additional Fields.
                strUpd = "Rez000060";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_GPS_TripHeader add dtStart TIMESTAMP NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "dtStart")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_GPS_TripHeader add fStartLon float NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "fStartLon")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_GPS_TripHeader add fStartLat float NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "fStartLat")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_GPS_TripHeader add dtEnd TIMESTAMP NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "dtEnd")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_GPS_TripHeader add fEndLon float NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "fEndLon")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_GPS_TripHeader add fEndLat float NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "fEndLat")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE  INDEX IX_Trip ON GFI_GPS_TripHeader 
					(
						dtStart, dtEnd
					)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "IX_Trip")).Rows[0][0].ToString() == "0")
                        dbExecute(sql, null, 8000);

                    InsertDBUpdate(strUpd, "TripHeader Additional Fields");
                }
                #endregion
                #region Update 61. GFI_GPS_TripDetail Index
                strUpd = "Rez000061";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "DROP INDEX my_IX_GFI_GPS_TripDetail";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripDetail", "my_IX_GFI_GPS_TripDetail")).Rows[0][0].ToString() != "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX my_IX_GFI_GPS_TripDetail ON GFI_GPS_TripDetail
							(
								HeaderiID ASC
							)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripDetail", "my_IX_GFI_GPS_TripDetail")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "GFI_GPS_TripDetail Indexes");
                }
                #endregion
                #region Update 62. GPSData Live.
                strUpd = "Rez000062";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"
						begin
							execute immediate
								'CREATE TABLE GFI_GPS_LiveData(
									UID number(10) NOT NULL,
									AssetID number(10) NULL,
									DateTimeGPS_UTC timestamp(3) NULL,
									DateTimeServer timestamp(3) NULL,
									Longitude binary_double NULL,
									Latitude binary_double NULL,
									LongLatValidFlag number(10) NULL,
									Speed binary_double NULL,
									EngineOn number(10) NULL,
									StopFlag number(10) NULL,
									TripDistance binary_double NULL,
									TripTime binary_double NULL,
									WorkHour number(10) NULL,
									DriverID number(10) NOT NULL,
								 CONSTRAINT PK_GFI_GPS_LiveData PRIMARY KEY (UID)
								)';
						end;";
                    if (GetDataDT(ExistTableSql("GFI_GPS_LiveData")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"
						begin
							execute immediate
								'CREATE TABLE GFI_GPS_LiveDataDetail(
									GPSDetailID number(19) NOT NULL,
									UID number(10) NULL,
									TypeID nvarchar2(30) NULL,
									TypeValue nvarchar2(30) NULL,
									UOM nvarchar2(15) NULL,
									Remarks nvarchar2(50) NULL,
								 CONSTRAINT PK_GFI_GPS_LiveDataDetail PRIMARY KEY  (GPSDetailID)
								)';
						end;";
                    if (GetDataDT(ExistTableSql("GFI_GPS_LiveDataDetail")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_GPS_LiveDataDetail_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_GPS_LiveDataDetail_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_GPS_LiveDataDetail_seq_tr
								BEFORE INSERT ON GFI_GPS_LiveDataDetail FOR EACH ROW
								WHEN(NEW.GPSDetailID IS NULL)
								BEGIN
									SELECT GFI_GPS_LiveDataDetail_seq.NEXTVAL INTO :NEW.GPSDetailID FROM DUAL;
								END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_GPS_LiveDataDetail
							ADD  CONSTRAINT FK_GFI_GPS_LiveDataDetail_GFI_GPS_LiveData
								FOREIGN KEY(UID)
							REFERENCES GFI_GPS_LiveData (UID)
							ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_LiveDataDetail_GFI_GPS_LiveData", "GFI_GPS_LiveDataDetail")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);




                    InsertDBUpdate(strUpd, "GPSData Live");
                }
                #endregion
                #region Update 63. Referential Constraints
                strUpd = "Rez000063";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_GPS_ProcessPending DROP CONSTRAINT FK_GFI_GPS_ProcessPending_GFI_FLT_Asset";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_ProcessPending_GFI_FLT_Asset", "GFI_GPS_ProcessPending")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_GPS_ProcessPending 
							ADD CONSTRAINT FK_GFI_GPS_ProcessPending_GFI_FLT_Asset 
								FOREIGN KEY(AssetID) 
								REFERENCES GFI_FLT_Asset(AssetID) 
						 ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_ProcessPending_GFI_FLT_Asset", "GFI_GPS_ProcessPending")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_GPS_Exceptions 
							ADD CONSTRAINT FK_GFI_GPS_Exceptions_GFI_FLT_Asset 
								FOREIGN KEY(AssetID) 
								REFERENCES GFI_FLT_Asset(AssetID) 
						 ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_Exceptions_GFI_FLT_Asset", "GFI_GPS_Exceptions")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_GPS_Exceptions 
							ADD CONSTRAINT FK_GFI_GPS_Exceptions_GFI_FLT_Driver 
								FOREIGN KEY(DriverID) 
								REFERENCES GFI_FLT_Driver(DriverID) 
						 ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_Exceptions_GFI_FLT_Driver", "GFI_GPS_Exceptions")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_GPS_Exceptions 
							ADD CONSTRAINT FK_GFI_GPS_Exceptions_GFI_GPS_Rules 
								FOREIGN KEY(RuleID) 
								REFERENCES GFI_GPS_Rules(RuleID) 
						 ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_Exceptions_GFI_GPS_Rules", "GFI_GPS_Exceptions")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_GPS_LiveData 
							ADD CONSTRAINT FK_GFI_GPS_LiveData_GFI_FLT_Asset 
								FOREIGN KEY(AssetID) 
								REFERENCES GFI_FLT_Asset(AssetID) 
						 ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_LiveData_GFI_FLT_Asset", "GFI_GPS_LiveData")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_GPS_LiveData 
							ADD CONSTRAINT FK_GFI_GPS_LiveData_GFI_FLT_Driver 
								FOREIGN KEY(DriverID) 
								REFERENCES GFI_FLT_Driver(DriverID) 
						 ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_LiveData_GFI_FLT_Driver", "GFI_GPS_LiveData")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_AutoReportingConfigDetails DROP CONSTRAINT FK_GFI_FLT_ARCD_GFI_FLT_ARC";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ARCD_GFI_FLT_ARC", "GFI_FLT_AutoReportingConfigDetails")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_AutoReportingConfigDetails 
							ADD CONSTRAINT FK_ARCD_ARC 
								FOREIGN KEY(ARID) 
								REFERENCES GFI_FLT_AutoReportingConfig(ARID) 
						 ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_ARCD_ARC", "GFI_FLT_AutoReportingConfigDetails")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "Referential Constraints");
                }
                #endregion
                #region Update 64. Odo and Hr Meter
                strUpd = "Rez000064";
                if (!CheckUpdateExist(strUpd))
                {
                    InsertDBUpdate(strUpd, "Odo and Hr Meter optimized");
                }
                #endregion
                #region Update 65. GFI_SYS_Notification updated
                strUpd = "Rez000065";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"alter table GFI_SYS_Notification add UIDCategory nvarchar2(10) not null";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "UIDCategory")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"alter table GFI_SYS_Notification add ReportID int null";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "ReportID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"alter table GFI_SYS_Notification add Lat float null";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "Lat")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"alter table GFI_SYS_Notification add Lon float null";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "Lat")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"alter table GFI_SYS_Notification add Driver nvarchar2(100) null";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "Driver")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"alter table GFI_SYS_Notification add Asset nvarchar2(100) null";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "Asset")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"alter table GFI_SYS_Notification add RuleName nvarchar2(100) null";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "RuleName")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"alter table GFI_SYS_Notification add email nvarchar2(50) null";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "email")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"alter table GFI_SYS_Notification add DataTimeGPS timestamp(3) null";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "DataTimeGPS")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"alter table GFI_SYS_Notification add name nvarchar2(100) null";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "name")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_AutoReportingConfig 
							DROP CONSTRAINT FK_GFI_FLT_ARC_GFI_GPS_Rules";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ARC_GFI_GPS_Rules", "GFI_FLT_AutoReportingConfig")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "GFI_SYS_Notification updated");
                }
                #endregion

                #region Update 66. Odo and LiveMap enhanced
                strUpd = "Rez000066";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "DROP INDEX myIX_LiveData";
                    if (GetDataDT(ExistsIndex("GFI_GPS_LiveData", "myIX_LiveData")).Rows[0][0].ToString() != "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX myIX_LiveData ON GFI_GPS_LiveData
						(
							LongLatValidFlag ASC,
							DateTimeGPS_UTC ASC
						)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_LiveData", "myIX_LiveData")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "DROP INDEX MyIXCalOdoEng";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "MyIXCalOdoEng")).Rows[0][0].ToString() != "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "Odo and LiveMap enhanced");
                }
                #endregion
                #region Update 67. ARC Additional Fields and LiveMap index
                strUpd = "Rez000067";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "DROP INDEX myIX_LiveData";
                    if (GetDataDT(ExistsIndex("GFI_GPS_LiveData", "myIX_LiveData")).Rows[0][0].ToString() != "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX myIX_LiveData ON GFI_GPS_LiveData
						(
							AssetID ASC,
							DateTimeGPS_UTC ASC
						)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_LiveData", "myIX_LiveData")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "DROP INDEX MyIX_LiveDataDetail";
                    if (GetDataDT(ExistsIndex("GFI_GPS_LiveDataDetail", "MyIX_LiveDataDetail")).Rows[0][0].ToString() != "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX MyIX_LiveDataDetail
							ON GFI_GPS_LiveDataDetail (UID)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_LiveDataDetail", "MyIX_LiveDataDetail")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);


                    sql = "ALTER TABLE GFI_FLT_AutoReportingConfig add ReportType nvarchar2(25) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AutoReportingConfig", "ReportType")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_FLT_AutoReportingConfig add SaveToDisk number (1)";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AutoReportingConfig", "SaveToDisk")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_FLT_AutoReportingConfig add ReportPath nvarchar2(250) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AutoReportingConfig", "ReportPath")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_AutoReportingConfig ADD GridXML nvarchar2(250) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AutoReportingConfig", "GridXML")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "ARC Additional Fields and LiveMap index");
                }
                #endregion
                #region Update 68. Trip and Exception enhanced.
                strUpd = "Rez000068";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "DROP INDEX MyIXTripProcess";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "MyIXTripProcess")).Rows[0][0].ToString() != "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX MyIXTripProcess
							ON GFI_GPS_TripHeader (AssetID, StopTime)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "MyIXTripProcess")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    User u = new User();
                    u = userService.GetUserByeMail("ADMIN@naveo.mu", "SysInitPass", sConnStr, false, true);
                    u.Password = "NewCore4dmin@123";
                    userService.SaveUser(u, false, sConnStr);

                    InsertDBUpdate(strUpd, "Trip and Exception enhanced");
                }
                #endregion
                #region Update 69. ARC data initialized. SMPT settings added
                strUpd = "Rez000069";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "update GFI_FLT_AutoReportingConfig set ReportType = 'EXCEL' where ReportType = ''";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "update GFI_FLT_AutoReportingConfig set ReportType = 'EXCEL' where ReportType is null";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    GlobalParam g = new GlobalParam();
                    g.ParamName = "SmtpClientHost";
                    g.PValue = "smtp.gmail.com";
                    g.ParamComments = "Name or IP of host used for SMTP Transactions";
                    globalParamsService.SaveGlobalParams(g, true, sConnStr);

                    g = new GlobalParam();
                    g.ParamName = "SmtpClientPort";
                    g.PValue = "587";
                    g.ParamComments = "Port used for SMTP Transactions";
                    globalParamsService.SaveGlobalParams(g, true, sConnStr);

                    g = new GlobalParam();
                    g.ParamName = "SmtpClientEnableSsl";
                    g.PValue = "1";
                    g.ParamComments = "1 True, 0 False";
                    globalParamsService.SaveGlobalParams(g, true, sConnStr);

                    g = new GlobalParam();
                    g.ParamName = "SmtpClientUsrName";
                    g.PValue = "NaveoOne@gmail.com";
                    g.ParamComments = "UserName used for SMTP Transactions";
                    globalParamsService.SaveGlobalParams(g, true, sConnStr);

                    g = new GlobalParam();
                    g.ParamName = "PwdSmtpClientUsr";
                    g.PValue = "ebs4dmin";
                    g.ParamComments = "Corresponding password used for SMTP Transactions";
                    globalParamsService.SaveGlobalParams(g, true, sConnStr);

                    g = new GlobalParam();
                    g.ParamName = "SmtpClientSenderMail";
                    g.PValue = "naveo.mu1@gmail.com";
                    g.ParamComments = "Serder appearing on mail";
                    globalParamsService.SaveGlobalParams(g, true, sConnStr);

                    InsertDBUpdate(strUpd, "ARC data initialized. SMPT settings added");
                }
                #endregion
                #region Update 70. GFI_GPS_ProcessErrors and Notif updated
                strUpd = "Rez000070";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "DROP INDEX MyIdx_TD_Del";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripDetail", "MyIdx_TD_Del")).Rows[0][0].ToString() != "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"ALTER TABLE GFI_SYS_User ADD AccessTemplateId int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "AccessTemplateId")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_User ADD LoginAttempt int Default 1";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "LoginAttempt")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_User ADD PreviousPassword nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "PreviousPassword")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_User ADD PasswordUpdateddate DateTime NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "PasswordUpdateddate")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    User u = new User();
                    u = userService.GetUserByeMail("Support@naveo.mu", "SysInitPass", sConnStr, false, false);
                    GroupMatrix gmRoot = new GroupMatrix();
                    if (u.QryResult == User.UserResult.InvalidCredentials && u.UID == -999)
                    {
                        gmRoot = new NaveoOneLib.Services.GMatrix.GroupMatrixService().GetRoot(sConnStr);

                        u = new User();
                        u.Username = "NaveoSupport";
                        u.Email = "Support@naveo.mu";
                        u.Names = "NaveoSupport";
                        u.Password = "ebs@3Admin";
                        u.Status_b = "AC";

                        Matrix m = new Matrix();
                        m.GMID = gmRoot.gmID;
                        m.oprType = DataRowState.Added;
                        u.lMatrix.Add(m);

                        userService.SaveUser(u, true, sConnStr);
                    }

                    sql = @"CREATE TABLE GFI_GPS_ProcessErrors
                            (
                                  iID NUMBER(10) NOT NULL,
                                  sError NVARCHAR2(100) NULL,
                                  sOrigine NVARCHAR2(50) NOT NULL,
                                  sFieldName NVARCHAR2(50) NULL,
                                  sFieldValue NVARCHAR2(50) NULL,
                                  AssetID int null,
                                  ServerDate timestamp(3) DEFAULT SYSTIMESTAMP,
                                  dtUTC timestamp(3) null,
                                  CONSTRAINT PK_GFI_GPS_ProcessErrors PRIMARY KEY (iID)
                            )";
                    CreateTable(sql, "GFI_GPS_ProcessErrors", "iID");

                    sql = @"ALTER TABLE dbo.GFI_SYS_Notification ADD ParentRuleID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "ParentRuleID")).Rows[0][0].ToString() == "0")
                        dbExecute(sql);

                    sql = @"ALTER TABLE dbo.GFI_SYS_Notification ADD GPSDataUID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "GPSDataUID")).Rows[0][0].ToString() == "0")
                        dbExecute(sql);

                    InsertDBUpdate(strUpd, "GFI_GPS_ProcessErrors and Notif updated");
                }
                #endregion
                #region Update 71. Notif updated. New Menu
                strUpd = "Rez000071";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_SYS_Notification ADD Err nvarchar2(25)";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "Err")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"
							begin
								execute immediate
								   'CREATE TABLE GFI_SYS_MenuGroup
									(
										MenuId number(10) NOT NULL,
										MenuName nvarchar2(50) NULL,
										CONSTRAINT PK_GFI_SYS_MenuGroup PRIMARY KEY  (MenuId)
									)
									';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_SYS_MenuGroup")).Rows[0][0].ToString() == "0")
                    {
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                        sql = "insert into GFI_SYS_MenuGroup (MenuID, MenuName) values (1, 'Functions')";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                        sql = "insert into GFI_SYS_MenuGroup (MenuID, MenuName) values (2, 'Reports')";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                        sql = "insert into GFI_SYS_MenuGroup (MenuID, MenuName) values (3, 'Settings')";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    }

                    sql = @"
							begin
								execute immediate
									'CREATE TABLE GFI_SYS_MenuHeader
									(
										MenuHeaderId number(10) NOT NULL,
										MenuHeader nvarchar2(50) NULL,
										CONSTRAINT PK_GFI_SYS_MenuHeader PRIMARY KEY  
										(
											MenuHeaderId 
										)
									)
									';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_SYS_MenuHeader")).Rows[0][0].ToString() == "0")
                    {
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                        sql = "insert into GFI_SYS_MenuHeader (MenuHeaderId, MenuHeader) values (1, 'Maps & GIS')";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                        sql = "insert into GFI_SYS_MenuHeader (MenuHeaderId, MenuHeader) values (2, 'Vehicles & Assets')";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                        sql = "insert into GFI_SYS_MenuHeader (MenuHeaderId, MenuHeader) values (3, 'Fuel & Telemetry')";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                        sql = "insert into GFI_SYS_MenuHeader (MenuHeaderId, MenuHeader) values (4, 'Geofencing')";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                        sql = "insert into GFI_SYS_MenuHeader (MenuHeaderId, MenuHeader) values (5, 'Rules & Alerts')";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                        sql = "insert into GFI_SYS_MenuHeader (MenuHeaderId, MenuHeader) values (6, 'Settings')";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    }

                    sql = @"ALTER TABLE GFI_SYS_Modules ADD MenuHeaderId int";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Modules", "MenuHeaderId")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    sql = @"ALTER TABLE GFI_SYS_Modules ADD MenuGroupId int";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Modules", "MenuGroupId")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    sql = @"ALTER TABLE GFI_SYS_Modules ADD OrderIndex int";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Modules", "OrderIndex")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    sql = @"ALTER TABLE GFI_SYS_Modules ADD Title nvarchar2(200)";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Modules", "Title")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    sql = @"ALTER TABLE GFI_SYS_Modules ADD Summary nvarchar2(100)";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Modules", "Summary")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    #region Module
                    NaveoOneLib.Models.Permissions.Module m = new NaveoOneLib.Models.Permissions.Module();
                    m = moduleService.GetModulesByName("Maps & GIS -->Function -->Live Map", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Maps & GIS -->Function -->Live Map";
                        m.Description = "Maps & GIS -->Function -->Live Map";
                        m.Command = "CmdLiveMap";
                        m.MenuHeaderId = 1;
                        m.MenuGroupId = 1;
                        m.OrderIndex = 1;
                        m.Title = "Live Map";
                        m.Summary = "A live geographical view of all vehicles and other connected assets";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Maps & GIS -->Function -->Trip History", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Maps & GIS -->Function -->Trip History";
                        m.Description = "Maps & GIS -->Function -->Trip History";
                        m.Command = "CmdTripHistory";
                        m.MenuHeaderId = 1;
                        m.MenuGroupId = 1;
                        m.OrderIndex = 2;
                        m.Title = "Trip History";
                        m.Summary = "Detailed vehicle/asset activity over a period of time";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Maps & GIS -->Reports -->Trip Summary", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Maps & GIS -->Reports -->Trip Summary";
                        m.Description = "Maps & GIS -->Reports -->Trip Summary";
                        m.Command = "CmdTripSummary";
                        m.MenuHeaderId = 1;
                        m.MenuGroupId = 2;
                        m.OrderIndex = 1;
                        m.Title = "Trip Summary";
                        m.Summary = "Summarized activity data for moving assets";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Vehicles & Assets -->Functions -->Maintenance Dashboard", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Vehicles & Assets -->Functions -->Maintenance Dashboard";
                        m.Description = "Vehicles & Assets -->Functions -->Maintenance Dashboard";
                        m.Command = "CmdMaintenance";
                        m.MenuHeaderId = 2;
                        m.MenuGroupId = 1;
                        m.OrderIndex = 1;
                        m.Title = "Maintenance Dashboard";
                        m.Summary = "Status of ongoing maintenance jobs";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Vehicles & Assets -->Functions -->Bulk Maintenance Assignment", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module();
                        m.ModuleName = "Vehicles & Assets -->Functions -->Bulk Maintenance Assignment";
                        m.Description = "Vehicles & Assets -->Functions -->Bulk Maintenance Assignment";
                        m.Command = "CmdBulkMaintenance";
                        m.MenuHeaderId = 2;
                        m.MenuGroupId = 1;
                        m.OrderIndex = 2;
                        m.Title = "Bulk Maintenance Assignment";
                        m.Summary = "An easy tool to assign maintenancce type to multiple assets";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Vehicles & Assets -->Functions -->Asset Master Details", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module();
                        m.ModuleName = "Vehicles & Assets -->Functions -->Asset Master Details";
                        m.Description = "Vehicles & Assets -->Functions -->Asset Master Details";
                        m.Command = "CmdAssetMasterDetails";
                        m.MenuHeaderId = 2;
                        m.MenuGroupId = 1;
                        m.OrderIndex = 3;
                        m.Title = "Asset Master Details";
                        m.Summary = "Vehicle/asset masterfile details";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Vehicles & Assets -->Reports -->Maintenance Cost", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module();
                        m.ModuleName = "Vehicles & Assets -->Reports -->Maintenance Cost";
                        m.Description = "Vehicles & Assets -->Reports -->Maintenance Cost";
                        m.Command = "CmdMaintenanceCost";
                        m.MenuHeaderId = 2;
                        m.MenuGroupId = 2;
                        m.OrderIndex = 1;
                        m.Title = "Maintenance Cost";
                        m.Summary = "Detailed vehicle/asset maintenance expenses over a period of time";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Vehicles & Assets -->Reports -->Maintenance Cost", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module();
                        m.ModuleName = "Vehicles & Assets -->Reports -->Maintenance Cost";
                        m.Description = "Vehicles & Assets -->Reports -->Maintenance Cost";
                        m.Command = "CmdMaintenanceCost";
                        m.MenuHeaderId = 2;
                        m.MenuGroupId = 2;
                        m.OrderIndex = 1;
                        m.Title = "Maintenance Cost";
                        m.Summary = "Detailed vehicle/asset maintenance expenses over a period of time";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Vehicles & Assets -->Reports -->Asset Summary", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module();
                        m.ModuleName = "Vehicles & Assets -->Reports -->Asset Summary";
                        m.Description = "Vehicles & Assets -->Reports -->Asset Summary";
                        m.Command = "CmdAssetSummary";
                        m.MenuHeaderId = 2;
                        m.MenuGroupId = 2;
                        m.OrderIndex = 2;
                        m.Title = "Asset Summary";
                        m.Summary = "Summarized asset activity data over a period of time";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Vehicles & Assets -->Reports -->Daily Summary", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module();
                        m.ModuleName = "Vehicles & Assets -->Reports -->Daily Summary";
                        m.Description = "Vehicles & Assets -->Reports -->Daily Summary";
                        m.Command = "CmdDailySummary";
                        m.MenuHeaderId = 2;
                        m.MenuGroupId = 2;
                        m.OrderIndex = 3;
                        m.Title = "Daily Summary";
                        m.Summary = "Summarized daily asset activity data";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Vehicles & Assets -->Settings -->Maintenance Types", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Vehicles & Assets -->Settings -->Maintenance Types";
                        m.Description = "Vehicles & Assets -->Settings -->Maintenance Types";
                        m.Command = "CmdMaintenanceTypes";
                        m.MenuHeaderId = 2;
                        m.MenuGroupId = 3;
                        m.OrderIndex = 1;
                        m.Title = "Maintenance Types";
                        m.Summary = "Add/maintain maintenance types";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Vehicles & Assets -->Settings -->Maintenance Status", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Vehicles & Assets -->Settings -->Maintenance Status";
                        m.Description = "Vehicles & Assets -->Settings -->Maintenance Status";
                        m.Command = "CmdMaintenanceStatus";
                        m.MenuHeaderId = 2;
                        m.MenuGroupId = 3;
                        m.OrderIndex = 2;
                        m.Title = "Maintenance Status";
                        m.Summary = "Add/maintain maintenance status workflow";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Vehicles & Assets -->Settings -->Vehicle Types", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Vehicles & Assets -->Settings -->Vehicle Types";
                        m.Description = "Vehicles & Assets -->Settings -->Vehicle Types";
                        m.Command = "CmdVehicleTypes";
                        m.MenuHeaderId = 2;
                        m.MenuGroupId = 3;
                        m.OrderIndex = 3;
                        m.Title = "Vehicle Types";
                        m.Summary = "Add/maintain vehicle types";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Vehicles & Assets -->Settings -->Insurance Cover Types", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Vehicles & Assets -->Settings -->Insurance Cover Types";
                        m.Description = "Vehicles & Assets -->Settings -->Insurance Cover Types";
                        m.Command = "CmdInsuranceCoverTypes";
                        m.MenuHeaderId = 2;
                        m.MenuGroupId = 3;
                        m.OrderIndex = 4;
                        m.Title = "Insurance Cover Types";
                        m.Summary = "Add/maintain insurance cover types";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Vehicles & Assets -->Settings -->Assets Extended Properties", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Vehicles & Assets -->Settings -->Assets Extended Properties";
                        m.Description = "Vehicles & Assets -->Settings -->Assets Extended Properties";
                        m.Command = "CmdAssetsExtendedProperties";
                        m.MenuHeaderId = 2;
                        m.MenuGroupId = 3;
                        m.OrderIndex = 5;
                        m.Title = "Assets Extended Properties";
                        m.Summary = "Assets Extended Properties";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Fuel & Telemetry -->Functions -->Tank Levels", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Fuel & Telemetry -->Functions -->Tank Levels";
                        m.Description = "Fuel & Telemetry -->Functions -->Tank Levels";
                        m.Command = "CmdTankLevels";
                        m.MenuHeaderId = 3;
                        m.MenuGroupId = 1;
                        m.OrderIndex = 1;
                        m.Title = "Tank Levels";
                        m.Summary = "Online status of connected fixed tanks";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Fuel & Telemetry -->Functions -->Fuel Data Entry", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Fuel & Telemetry -->Functions -->Fuel Data Entry";
                        m.Description = "Fuel & Telemetry -->Functions -->Fuel Data Entry";
                        m.Command = "CmdFuelDataEntry";
                        m.MenuHeaderId = 3;
                        m.MenuGroupId = 1;
                        m.OrderIndex = 2;
                        m.Title = "Fuel Data Entry";
                        m.Summary = "Fuel Data Entry";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Fuel & Telemetry -->Reports -->Fuel Graph", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Fuel & Telemetry -->Reports -->Fuel Graph";
                        m.Description = "Fuel & Telemetry -->Reports -->Fuel Graph";
                        m.Command = "CmdFuelGraph";
                        m.MenuHeaderId = 3;
                        m.MenuGroupId = 2;
                        m.OrderIndex = 1;
                        m.Title = "Fuel Graph";
                        m.Summary = "Graphical historic data for connected fuel sensors (moving assets)";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Fuel & Telemetry -->Reports -->Fuel Consumption", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Fuel & Telemetry -->Reports -->Fuel Consumption";
                        m.Description = "Fuel & Telemetry -->Reports -->Fuel Consumption";
                        m.Command = "CmdFuelConsumption";
                        m.MenuHeaderId = 3;
                        m.MenuGroupId = 2;
                        m.OrderIndex = 2;
                        m.Title = "Fuel Consumption";
                        m.Summary = "Vehicle/asset fuel consumption data over a period of time";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Fuel & Telemetry -->Reports -->Temperature Graph", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Fuel & Telemetry -->Reports -->Temperature Graph";
                        m.Description = "Fuel & Telemetry -->Reports -->Temperature Graph";
                        m.Command = "CmdTemperatureGraph";
                        m.MenuHeaderId = 3;
                        m.MenuGroupId = 2;
                        m.OrderIndex = 3;
                        m.Title = "Temperature Graph";
                        m.Summary = "Graphical historic data for connected temperature sensors";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Fuel & Telemetry -->Reports -->Auxilliary Report", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Fuel & Telemetry -->Reports -->Auxilliary Report";
                        m.Description = "Fuel & Telemetry -->Reports -->Auxilliary Report";
                        m.Command = "CmdAuxilliaryReport";
                        m.MenuHeaderId = 3;
                        m.MenuGroupId = 2;
                        m.OrderIndex = 4;
                        m.Title = "Auxilliary Report";
                        m.Summary = "Output data received from connected auxilliaries/sensors";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Geofencing -->Functions -->Create new zone", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Geofencing -->Functions -->Create new zone";
                        m.Description = "Geofencing -->Functions -->Create new zone";
                        m.Command = "CmdCreatenewzone";
                        m.MenuHeaderId = 4;
                        m.MenuGroupId = 1;
                        m.OrderIndex = 1;
                        m.Title = "Create new zone";
                        m.Summary = "Add/maintain geofences on the map";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Geofencing -->Reports -->Zones Visited", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Geofencing -->Reports -->Zones Visited";
                        m.Description = "Geofencing -->Reports -->Zones Visited";
                        m.Command = "CmdZonesVisited";
                        m.MenuHeaderId = 4;
                        m.MenuGroupId = 2;
                        m.OrderIndex = 1;
                        m.Title = "Zones Visited";
                        m.Summary = "List of zones visited over a period of time";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Geofencing -->Reports -->Selective Zones Visited", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Geofencing -->Reports -->Selective Zones Visited";
                        m.Description = "Geofencing -->Reports -->Selective Zones Visited";
                        m.Command = "CmdSelectiveZonesVisited";
                        m.MenuHeaderId = 4;
                        m.MenuGroupId = 2;
                        m.OrderIndex = 2;
                        m.Title = "Selective Zones Visited";
                        m.Summary = "List of zones visited over a period of time based on user selection";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Geofencing -->Reports -->Zones Visited Summarized", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Geofencing -->Reports -->Zones Visited Summarized";
                        m.Description = "Geofencing -->Reports -->Zones Visited Summarized";
                        m.Command = "CmdZonesVisitedSummarized";
                        m.MenuHeaderId = 4;
                        m.MenuGroupId = 2;
                        m.OrderIndex = 3;
                        m.Title = "Zones Visited Summarized";
                        m.Summary = "Summary of zones visited (count)";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Geofencing -->Settings -->Zone Types", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Geofencing -->Settings -->Zone Types";
                        m.Description = "Geofencing -->Settings -->Zone Types";
                        m.Command = "CmdZoneTypes";
                        m.MenuHeaderId = 4;
                        m.MenuGroupId = 3;
                        m.OrderIndex = 1;
                        m.Title = "Zone Types";
                        m.Summary = "Add/maintain groups of zones";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Rules & Alerts -->Functions -->Reprocess Exception Rules", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Rules & Alerts -->Functions -->Reprocess Exception Rules";
                        m.Description = "Rules & Alerts -->Functions -->Reprocess Exception Rules";
                        m.Command = "CmdReprocessExceptionRules";
                        m.MenuHeaderId = 5;
                        m.MenuGroupId = 1;
                        m.OrderIndex = 1;
                        m.Title = "Reprocess Exception Rules";
                        m.Summary = "A tool to apply a rule to historic data";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Rules & Alerts -->Functions -->Test Automatic Reporting", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Rules & Alerts -->Functions -->Test Automatic Reporting";
                        m.Description = "Rules & Alerts -->Functions -->Test Automatic Reporting";
                        m.Command = "CmdTestAutomaticReporting";
                        m.MenuHeaderId = 5;
                        m.MenuGroupId = 1;
                        m.OrderIndex = 2;
                        m.Title = "Test Automatic Reporting";
                        m.Summary = "A tool to test the proper functioning of automatic reports";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Rules & Alerts -->Reports -->Exceptions Broken", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Rules & Alerts -->Reports -->Exceptions Broken";
                        m.Description = "Rules & Alerts -->Reports -->Exceptions Broken";
                        m.Command = "CmdExceptionsBroken";
                        m.MenuHeaderId = 5;
                        m.MenuGroupId = 2;
                        m.OrderIndex = 1;
                        m.Title = "Exceptions Broken";
                        m.Summary = "List of rules broken over a period of time";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Rules & Alerts -->Settings -->Setup New Rules", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Rules & Alerts -->Settings -->Setup New Rules";
                        m.Description = "Rules & Alerts -->Settings -->Setup New Rules";
                        m.Command = "CmdSetupNewRules";
                        m.MenuHeaderId = 5;
                        m.MenuGroupId = 3;
                        m.OrderIndex = 1;
                        m.Title = "Setup New Rules";
                        m.Summary = "Add/maintain rules";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Rules & Alerts -->Settings -->Setup Automatic Reporting", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Rules & Alerts -->Settings -->Setup Automatic Reporting";
                        m.Description = "Rules & Alerts -->Settings -->Setup Automatic Reporting";
                        m.Command = "CmdSetupAutomaticReporting";
                        m.MenuHeaderId = 5;
                        m.MenuGroupId = 3;
                        m.OrderIndex = 2;
                        m.Title = "Setup Automatic Reporting";
                        m.Summary = "Add/maintain automatic reports";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Settings -->Settings -->User Management", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Settings -->Settings -->User Management";
                        m.Description = "Settings -->Settings -->User Management";
                        m.Command = "CmdUserManagement";
                        m.MenuHeaderId = 6;
                        m.MenuGroupId = 3;
                        m.OrderIndex = 1;
                        m.Title = "User Management";
                        m.Summary = "Add/maintain users";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Settings -->Settings -->User Access Templates", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Settings -->Settings -->User Access Templates";
                        m.Description = "Settings -->Settings -->User Access Templates";
                        m.Command = "CmdUserAccessTemplates";
                        m.MenuHeaderId = 6;
                        m.MenuGroupId = 3;
                        m.OrderIndex = 2;
                        m.Title = "User Access Templates";
                        m.Summary = "Add/maintain permissions for users";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Settings -->Settings -->Change Password", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Settings -->Settings -->Change Password";
                        m.Description = "Settings -->Settings -->Change Password";
                        m.Command = "CmdChangePassword";
                        m.MenuHeaderId = 6;
                        m.MenuGroupId = 3;
                        m.OrderIndex = 3;
                        m.Title = "Change Password";
                        m.Summary = "User-specific function for changing password";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Settings -->Settings -->Setup Drivers", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Settings -->Settings -->Setup Drivers";
                        m.Description = "Settings -->Settings -->Setup Drivers";
                        m.Command = "CmdSetupDrivers";
                        m.MenuHeaderId = 6;
                        m.MenuGroupId = 3;
                        m.OrderIndex = 4;
                        m.Title = "Setup Drivers";
                        m.Summary = "Add/maintain drivers";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Settings -->Settings -->Setup Assets", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Settings -->Settings -->Setup Assets";
                        m.Description = "Settings -->Settings -->Setup Assets";
                        m.Command = "CmdSetupAssets";
                        m.MenuHeaderId = 6;
                        m.MenuGroupId = 3;
                        m.OrderIndex = 5;
                        m.Title = "Setup Assets";
                        m.Summary = "Add/maintain assets";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }
                    #endregion
                    #region Access Templates
                    m = new NaveoOneLib.Models.Permissions.Module();
                    DataTable dtAccessModules = moduleService.GetModules(sConnStr);
                    DataTable dtAccessProfiles = AccessProfile.initAccessProfile();
                    foreach (DataRow dr in dtAccessModules.Rows)
                    {
                        DataRow drA = dtAccessProfiles.Rows.Add();
                        drA["ModuleId"] = dr["UID"];
                        drA["AllowRead"] = 1;
                        drA["AllowNew"] = 1;
                        drA["AllowEdit"] = 1;
                        drA["AllowDelete"] = 1;
                    }

                    AccessTemplate ac = new AccessTemplate();
                    ac.TemplateName = "System Default";
                    ac.Description = "System Default";
                    ac.CreatedDate = DateTime.Now;
                    ac.CreatedBy = usrInst.Username;
                    ac.UpdatedDate = Constants.NullDateTime;
                    ac.UpdatedBy = String.Empty;
                    ac.lMatrix = usrInst.lMatrix;

                    DataSet dsResult = new DataSet();
                    Boolean b = ac.Save(ac, true, dtAccessProfiles, out dsResult, sConnStr);
                    #endregion

                    InsertDBUpdate(strUpd, "Notif updated, New Menu");
                }

                #endregion
                #region Update 72. Telemetry.
                strUpd = "Rez000072";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"
							begin
								execute immediate
									'CREATE TABLE GFI_TEL_TelDataHeader(
										iID number(10) NOT NULL,
										MsgHeader nvarchar2(10) NOT NULL,
										Type nvarchar2(20) NOT NULL,
										AssetID number(10) NOT NULL,
										UTCdt timestamp(3) NOT NULL,
										MsgStatus nvarchar2(10) NOT NULL,
										isMaintenanceMode number(10) NOT NULL,
										isFailedCallout number(10) NOT NULL,
										Temperature binary_double NOT NULL,
										isBatteryLow number(10) NOT NULL,
										isAutoConfig number(10) NOT NULL,
										Carrier nvarchar2(50) NOT NULL,
										SignalStrength number(10) NOT NULL,
										CONSTRAINT PK_GFI_TEL_TelDataHeader PRIMARY KEY  (iID)
									)';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_TEL_TelDataHeader")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_TEL_TelDataHeader_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_TEL_TelDataHeader_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_TEL_TelDataHeader_seq_tr
					BEFORE INSERT ON GFI_TEL_TelDataHeader FOR EACH ROW
					WHEN(NEW.iID IS NULL)
					BEGIN
						SELECT GFI_TEL_TelDataHeader_seq.NEXTVAL INTO :NEW.iID FROM DUAL;
					END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"
							begin
								execute immediate
									'CREATE TABLE GFI_TEL_TelDataDetail(
										iID number(10) NOT NULL,
										HeaderiID number(10) NOT NULL,
										UTCdt timestamp(3) NULL,
										Sensor number(10) NULL,
										SensorReading binary_double NULL,
										CONSTRAINT PK_GFI_TEL_TelDataDetail PRIMARY KEY  (iID)
									)';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_TEL_TelDataDetail")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_TEL_TelDataDetail_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_TEL_TelDataDetail_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_TEL_TelDataDetail_seq_tr
					BEFORE INSERT ON GFI_TEL_TelDataDetail FOR EACH ROW
					WHEN(NEW.iID IS NULL)
					BEGIN
						SELECT GFI_TEL_TelDataDetail_seq.NEXTVAL INTO :NEW.iID FROM DUAL;
					END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);


                    sql = @"ALTER TABLE GFI_TEL_TelDataDetail
							ADD CONSTRAINT FK_GFI_TEL_TelDataDetail_GFI_TEL_TelDataHeader 
								FOREIGN KEY(HeaderiID)
								REFERENCES GFI_TEL_TelDataHeader (iID)
							ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_TEL_TelDataDetail_GFI_TEL_TelDataHeader", "GFI_TEL_TelDataDetail")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_TEL_TelDataHeader 
							ADD CONSTRAINT FK_GFI_TEL_TelDataHeader_GFI_FLT_Asset 
								FOREIGN KEY(AssetID) 
								REFERENCES GFI_FLT_Asset(AssetID) 
							ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_TEL_TelDataHeader_GFI_FLT_Asset", "GFI_TEL_TelDataHeader")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "Telemetry");
                }
                #endregion
                #region Update 73. Menu updated
                strUpd = "Rez000073";
                if (!CheckUpdateExist(strUpd))
                {
                    NaveoOneLib.Models.Permissions.Module m = new NaveoOneLib.Models.Permissions.Module();
                    m = moduleService.GetModulesByName("Drivers & Passengers -->Reports -->Driver Activity", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Drivers & Passengers -->Reports -->Driver Activity";
                        m.Description = "Drivers & Passengers -->Reports -->Driver Activity";
                        m.Command = "cmdDriverActivity";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Drivers & Passengers -->Reports -->Passenger Activity", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Drivers & Passengers -->Reports -->Passenger Activity";
                        m.Description = "Drivers & Passengers -->Reports -->Passenger Activity";
                        m.Command = "cmdPassengerActivity";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Drivers & Passengers -->Reports -->Passenger Occupancy", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Drivers & Passengers -->Reports -->Passenger Occupancy";
                        m.Description = "Drivers & Passengers -->Reports -->Passenger Activity";
                        m.Command = "cmdPassengerOccupancy";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Drivers & Passengers -->Reports -->Driver Master Details", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Drivers & Passengers -->Reports -->Driver Master Details";
                        m.Description = "Drivers & Passengers -->Reports -->Driver Master Details";
                        m.Command = "cmdDriverMasterDetails";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Drivers & Passengers -->Reports -->Passenger Master Details", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Drivers & Passengers -->Reports -->Passenger Master Details";
                        m.Description = "Drivers & Passengers -->Reports -->Passenger Master Details";
                        m.Command = "cmdPassengerMasterDetail";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    m = moduleService.GetModulesByName("Geofencing -->Reports -->Zones - Visited v/s Not Visited", sConnStr);
                    if (m == null)
                    {
                        m = new NaveoOneLib.Models.Permissions.Module(); m.ModuleName = "Geofencing -->Reports -->Zones - Visited v/s Not Visited";
                        m.Description = "Geofencing -->Reports -->Zones - Visited v/s Not Visited";
                        m.Command = "cmdZonesVisitedNotVisited";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);
                    }

                    InsertDBUpdate(strUpd, "Menu updated");
                }
                #endregion
                #region Update 74. isPointInCircle and Archiving tables.
                strUpd = "Rez000074";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"
CREATE OR REPLACE function isPointInCircle (p_CentreLon binary_double, p_CentreLat binary_double, p_Radius binary_double, p_toTestLg binary_double, p_toTestLt binary_double)
return number
as v_iResult number(10);
    v_dX binary_double;
    v_dY binary_double;
    v_sumOfSquares binary_double;
    v_distance binary_double;
Begin
    v_iResult := 0;
    v_dX := Abs(p_toTestLg - p_CentreLon);
    v_dY := Abs(p_toTestLt - p_CentreLat);
    v_sumOfSquares := v_dX * v_dX + v_dY * v_dY;
    v_distance := Sqrt(v_sumOfSquares);
    
    if (p_Radius >= v_distance) then
        v_iResult := 1;
    else
        v_iResult := 0;
    end if;		
    
    return v_iResult;
end;";
                    _SqlConn.OracleScriptExecute(sql, sConnStr);

                    sql = @"CREATE TABLE GFI_ARC_GPSData
									(
										AssetID number(10) NULL,
										DateTimeGPS_UTC timestamp(3) NULL,
										DateTimeServer timestamp(3) NULL,
										Longitude binary_double NULL,
										Latitude binary_double NULL,
										LongLatValidFlag number(10) NULL,
										Speed binary_double NULL,
										EngineOn number(10) NULL,
										StopFlag number(10) NULL,
										TripDistance binary_double NULL,
										TripTime binary_double NULL,
										WorkHour number(10) NULL,
										DriverID number(10) DEFAULT ((1)) NOT NULL ,
										RoadSpeed int NULL,
                                        UID number(10) NOT NULL,
									 CONSTRAINT PK_GFI_ARC_GPSData PRIMARY KEY (UID)
									)";
                    if (GetDataDT(ExistTableSql("GFI_ARC_GPSData")).Rows[0][0].ToString() == "0")
                        CreateTable(sql, "GFI_ARC_GPSData", String.Empty);

                    sql = @"
							ALTER TABLE GFI_ARC_GPSData  
								ADD CONSTRAINT FK_GFI_ARC_GPSData_GFI_FLT_Asset 
									FOREIGN KEY(AssetID) REFERENCES GFI_FLT_Asset (AssetID)
								ON DELETE CASCADE
							";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_ARC_GPSData_GFI_FLT_Asset", "GFI_ARC_GPSData")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"
							ALTER TABLE GFI_ARC_GPSData  
								ADD CONSTRAINT FK_GFI_ARC_GPSData_GFI_FLT_Driver 
									FOREIGN KEY(DriverID) REFERENCES GFI_FLT_Driver (DriverID)
								ON DELETE CASCADE
							";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_ARC_GPSData_GFI_FLT_Driver", "GFI_ARC_GPSData")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE TABLE GFI_ARC_GPSDataDetail
									(
										UID number(10) NULL,
										TypeID nvarchar2(30) NULL,
										TypeValue nvarchar2(30) NULL,
										UOM nvarchar2(15) NULL,
										Remarks nvarchar2(50) NULL,
										GPSDetailID number(19) NOT NULL,
									 CONSTRAINT PK_GFI_ARC_GPSDataDetail PRIMARY KEY (GPSDetailID)
									)";
                    if (GetDataDT(ExistTableSql("GFI_ARC_GPSDataDetail")).Rows[0][0].ToString() == "0")
                        CreateTable(sql, "GFI_ARC_GPSDataDetail", String.Empty);

                    sql = @"
							ALTER TABLE GFI_ARC_GPSDataDetail
								ADD  CONSTRAINT FK_GFI_ARC_GPSDataDetail_GFI_ARC_GPSData
									FOREIGN KEY(UID)
									REFERENCES GFI_ARC_GPSData (UID)
								ON DELETE CASCADE
							";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_ARC_GPSDataDetail_GFI_ARC_GPSData", "GFI_ARC_GPSDataDetail")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE  INDEX myARCIdxDT ON GFI_ARC_GPSData (DateTimeGPS_UTC ASC)";
                    if (GetDataDT(ExistsIndex("GFI_ARC_GPSData", "myARCIdxDT")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX my_IX_GFI_ARC_GPSDataDetail ON GFI_ARC_GPSDataDetail (UID ASC)";
                    if (GetDataDT(ExistsIndex("GFI_ARC_GPSDataDetail", "my_IX_GFI_ARC_GPSDataDetail")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE TABLE GFI_ARC_TripHeader
										(
											AssetID number(10) NOT NULL,
											DriverID number(10) NOT NULL,
											ExceptionFlag number(10) NULL,
											MaxSpeed number(10) NULL,
											IdlingTime binary_double NULL,
											StopTime binary_double NULL,
											TripDistance binary_double NULL,
											TripTime binary_double NULL,
											GPSDataStartUID number(10) NULL,
											GPSDataEndUID number(10) NULL,
											dtStart timestamp(3) NULL,
											fStartLon binary_double NULL,
											fStartLat binary_double NULL,
											dtEnd timestamp(3) NULL,
											fEndLon binary_double NULL,
											fEndLat binary_double NULL,
                                            OverSpeed1Time float Default 0,
                                            OverSpeed2Time float Default 0,
                                            Accel int Default 0,
                                            Brake int Default 0,
                                            Corner int Default 0,
											iID number(10) NOT NULL,
										 CONSTRAINT PK_GFI_ARC_TripHeader PRIMARY KEY  (iID)
										)";
                    if (GetDataDT(ExistTableSql("GFI_ARC_TripHeader")).Rows[0][0].ToString() == "0")
                        CreateTable(sql, "GFI_ARC_TripHeader", String.Empty);

                    sql = @"
							ALTER TABLE GFI_ARC_TripHeader
								ADD CONSTRAINT FK_GFI_ARC_TripHeader_GFI_FLT_Asset 
									FOREIGN KEY(AssetID)
									REFERENCES GFI_FLT_Asset (AssetID)
								ON DELETE CASCADE";

                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_ARC_TripHeader_GFI_FLT_Asset", "GFI_ARC_TripHeader")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE TABLE GFI_ARC_TripDetail
									(
										HeaderiID number(10) NOT NULL,
										GPSDataUID number(10) NOT NULL,
										iID number(19) NOT NULL,
									 CONSTRAINT PK_GFI_ARC_TripDetail PRIMARY KEY  (iID)
									)";
                    if (GetDataDT(ExistTableSql("GFI_ARC_TripDetail")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"
							ALTER TABLE GFI_ARC_TripDetail 
								ADD CONSTRAINT FK_GFI_ARC_TripDetail_GFI_ARC_TripHeader 
									FOREIGN KEY(HeaderiID)
									REFERENCES GFI_ARC_TripHeader (iID)
								ON DELETE CASCADE
							";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_ARC_TripDetail_GFI_ARC_TripHeader", "GFI_ARC_TripDetail")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE  INDEX myIX_GFI_ARC_TripDetail_GPSDataUID ON GFI_ARC_TripDetail (GPSDataUID ASC) ";
                    if (GetDataDT(ExistsIndex("GFI_ARC_TripDetail", "myIX_GFI_ARC_TripDetail_GPSDataUID")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);
                    sql = @"CREATE  INDEX my_IX_GFI_ARC_TripDetail ON GFI_ARC_TripDetail (HeaderiID ASC)";
                    if (GetDataDT(ExistsIndex("GFI_ARC_TripDetail", "my_IX_GFI_ARC_TripDetail")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);
                    sql = @"CREATE  INDEX MyIXARCDriver ON GFI_ARC_TripHeader (DriverID ASC)";
                    if (GetDataDT(ExistsIndex("GFI_ARC_TripHeader", "MyIXARCDriver")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);
                    sql = @"CREATE  INDEX MyIXCalOdoEng ON GFI_ARC_TripHeader	(AssetID ASC)";
                    if (GetDataDT(ExistsIndex("GFI_ARC_TripHeader", "MyIXCalOdoEng")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);
                    sql = @"CREATE  INDEX IXARC_Trip ON GFI_ARC_TripHeader (dtStart ASC, dtEnd ASC)";
                    if (GetDataDT(ExistsIndex("GFI_ARC_TripHeader", "IX_Trip")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);
                    InsertDBUpdate(strUpd, "isPointInCircle and Archieving tables");
                }
                #endregion
                #region Update 75. Exceptions Table updated. Exception Archiving
                strUpd = "Rez000075";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"alter table GFI_GPS_Exceptions add InsertedAt timestamp(3) default SYSTIMESTAMP";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_Exceptions", "InsertedAt")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE TABLE GFI_ARC_Exceptions
									(
										RuleID number(10) NULL,
										GPSDataID number(19) NULL,
										AssetID number(10) NULL,
										DriverID number(10) NULL,
										Severity nvarchar2(100) NULL,
										GroupID number(10) NULL,
										Posted number(10) DEFAULT ((0)) NOT NULL ,
										InsertedAt timestamp(3) DEFAULT (sys_extract_utc(systimestamp)) NOT NULL ,
										iID number(10) NOT NULL,
									 CONSTRAINT PK_GFI_ARC_Exceptions PRIMARY KEY  (iID)
									)";
                    if (GetDataDT(ExistTableSql("GFI_ARC_Exceptions")).Rows[0][0].ToString() == "0")
                        CreateTable(sql, "GFI_ARC_Exceptions", String.Empty);

                    sql = @"
							ALTER TABLE GFI_ARC_Exceptions 
								ADD CONSTRAINT FK_GFI_ARC_Exceptions_GFI_FLT_Asset 
									FOREIGN KEY(AssetID) REFERENCES GFI_FLT_Asset (AssetID)
								ON DELETE CASCADE
							";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_ARC_Exceptions_GFI_FLT_Asset", "GFI_ARC_Exceptions")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"
							ALTER TABLE GFI_ARC_Exceptions 
								ADD CONSTRAINT FK_GFI_ARC_Exceptions_GFI_GPS_Rules 
									FOREIGN KEY(RuleID) REFERENCES GFI_GPS_Rules (RuleID)
								ON DELETE CASCADE
							";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_ARC_Exceptions_GFI_GPS_Rules", "GFI_ARC_Exceptions")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "Exceptions Table updated. Exception Archiving");
                }
                #endregion

                #region Update 101 . ZoneXtPro Tables
                strUpd = "Rez000101";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"
						begin
							execute immediate
								'CREATE TABLE GFI_SYS_LoginAudit(
									auditId number(10) NOT NULL,
									auditUser nvarchar2(20) NULL,
									auditDate timestamp(3) NULL,
									auditType nvarchar2(2) NULL,
									machineName nvarchar2(50) NULL,
									clientGroupMachine nvarchar2(50) NULL,
									PostedDate timestamp(3) NULL,
									PostedFlag nvarchar2(1) NULL
								)';
						end;";
                    if (GetDataDT(ExistTableSql("GFI_SYS_LoginAudit")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_SYS_LoginAudit_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_SYS_LoginAudit_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_SYS_LoginAudit_seq_tr
					            BEFORE INSERT ON GFI_SYS_LoginAudit FOR EACH ROW
					            WHEN(NEW.auditId IS NULL)
					            BEGIN
						            SELECT GFI_SYS_LoginAudit_seq.NEXTVAL INTO :NEW.auditId FROM DUAL;
					            END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"
							begin
								execute immediate
								   'CREATE TABLE GFI_SYS_Audit (
									auditId number(10) NOT NULL,
									auditUser nvarchar2(20) NULL,
									auditDate timestamp(3) NULL,
									auditType nvarchar2(2) NULL,
									ReferenceCode	 nvarchar2(20) ,
									ReferenceDesc	 nvarchar2(50) ,
									ReferenceTable	 nvarchar2(20) ,
									CreatedDate	 timestamp(3) NULL,
									CallerFunction	 nvarchar2(20) ,
									SQLRemarks	 nvarchar2(200) ,
									NewValue	 nvarchar2(200) ,
									OldValue	 nvarchar2(200) ,
									PostedDate	 timestamp(3) NULL,
									PostedFlag	nvarchar2(1) 
								)';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_SYS_Audit")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_SYS_Audit_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_SYS_Audit_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_SYS_Audit_seq_tr
					            BEFORE INSERT ON GFI_SYS_Audit FOR EACH ROW
					            WHEN(NEW.auditId IS NULL)
					            BEGIN
						            SELECT GFI_SYS_Audit_seq.NEXTVAL INTO :NEW.auditId FROM DUAL;
					            END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"
						begin
							execute immediate
								'CREATE TABLE GFI_SYS_AuditAccess(
								 IId number(10) NOT NULL,
								 auditUser nvarchar2(20) NOT NULL,
								 Control nvarchar2(50) NOT NULL,
								 AuditDate timestamp(3) NOT NULL,
								  PostedDate	 timestamp(3) NULL,
								  PostedFlag	nvarchar2(1) 
								)';
						end;";
                    if (GetDataDT(ExistTableSql("GFI_SYS_AuditAccess")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_SYS_AuditAccess_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_SYS_AuditAccess_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_SYS_AuditAccess_seq_tr
					            BEFORE INSERT ON GFI_SYS_AuditAccess FOR EACH ROW
					            WHEN(NEW.IId IS NULL)
					            BEGIN
						            SELECT GFI_SYS_AuditAccess_seq.NEXTVAL INTO :NEW.IId FROM DUAL;
					            END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "Audit Tables updated");
                }
                #endregion
                #region Update 103 . Active Users
                strUpd = "Rez000103";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"
						 begin
							execute immediate
								 'CREATE TABLE GFI_SYS_ActiveSession(
								 UID number(10) NOT NULL,
								 Username nvarchar2(20) NOT NULL,
								 LoginDate timestamp(3) NOT NULL,
								 MachineName nvarchar2(50) NOT NULL,
								 UserProfile nvarchar2(50) NOT NULL,
								 Company nvarchar2(50) NOT NULL,
								 AppVersion nvarchar2(50) NOT NULL,
								 PRIMARY KEY  (UID)
								   )';
						 end;";

                    if (GetDataDT(ExistTableSql("GFI_SYS_ActiveSession")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    InsertDBUpdate(strUpd, "Active User");
                }

                #endregion
                #region Update 104 . Extended trip info type
                strUpd = "Rez000104";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"
							begin
								execute immediate
									'CREATE TABLE GFI_FLT_ExtTripInfoType(
										IID number(10) NOT NULL,
										TypeName nvarchar2(20) NOT NULL,
										Field1Name nvarchar2(50) NOT NULL,
										Field1Label nvarchar2(50) NOT NULL,
										Field1Type nvarchar2(50) NOT NULL,
										Field2Name nvarchar2(50) NULL,
										Field2Label nvarchar2(50) NULL,
										Field2Type nvarchar2(50) NULL,
										CreatedBy nvarchar2(30) NOT NULL,
										CreatedDate timestamp(3) NOT NULL,
										UpdatedBy nvarchar2(30) NULL,
										UpdatedDate timestamp(3) NULL
									   )';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_FLT_ExtTripInfoType")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_FLT_ExtTripInfoType_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_FLT_ExtTripInfoType_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_FLT_ExtTripInfoType_seq_tr
					            BEFORE INSERT ON GFI_FLT_ExtTripInfoType FOR EACH ROW
					            WHEN(NEW.IID IS NULL)
					            BEGIN
						            SELECT GFI_FLT_ExtTripInfoType_seq.NEXTVAL INTO :NEW.IID FROM DUAL;
					            END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "Extended trip info type");
                }

                #endregion

                #region Update 105 . Extended trip info type mapping
                strUpd = "Rez000105";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"
							begin
								execute immediate
									'CREATE TABLE GFI_FLT_ExtTripInfoMapping(
										 IID number(10) NOT NULL,
										 Description nvarchar2(255) NOT NULL,
										 ExtTripInfoId number(10) NOT NULL,
										 CreatedBy nvarchar2(50) NULL,
										 CreatedDate timestamp(3) NULL,
										 UpdatedBy nvarchar2(50) NULL,
										 UpdatedDate timestamp(3) NULL,
										 MappingId number(10) NOT NULL,
                                        CONSTRAINT PK_GFI_FLT_ExtTripInfoMapping PRIMARY KEY(ExtTripInfoId)
										)';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_FLT_ExtTripInfoMapping")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_FLT_ExtTripInfoMapping_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_FLT_ExtTripInfoMapping_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_FLT_ExtTripInfoMapping_seq_tr
					BEFORE INSERT ON GFI_FLT_ExtTripInfoMapping FOR EACH ROW
					WHEN(NEW.IID IS NULL)
					BEGIN
						SELECT GFI_FLT_ExtTripInfoMapping_seq.NEXTVAL INTO :NEW.IID FROM DUAL;
					END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "Extended trip info type mapping");
                }

                #endregion
                #region Update 106 . Extended trip info Table
                strUpd = "Rez000106";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"
							begin
								execute immediate
									  'CREATE TABLE GFI_FLT_ExtTripInfo(
									   IID number(10) NOT NULL,
									   ExtTripID number(38) NULL,
									   Field1Name nvarchar2(50) NULL,
									   Field1Value nvarchar2(50) NULL,
									   Field2Name nvarchar2(50) NULL,
									   Field2Value nvarchar2(50) NULL,
									   Field3Name nvarchar2(50) NULL,
									   Field3Value nvarchar2(50) NULL,
									   CreatedBy nvarchar2(50) NULL,
									   CreatedDate date NULL,
									   UpdatedBy nvarchar2(50) NULL,
									   UpdatedDate date NULL,
									   AssetName nvarchar2(50) NULL,
									   CONSTRAINT PK_GFI_FLT_ExtTripInfo PRIMARY KEY  (IID)
									  )';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_FLT_ExtTripInfo")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_FLT_ExtTripInfo_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_FLT_ExtTripInfo_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_FLT_ExtTripInfo_seq_tr
					            BEFORE INSERT ON GFI_FLT_ExtTripInfo FOR EACH ROW
					            WHEN(NEW.IID IS NULL)
					            BEGIN
						            SELECT GFI_FLT_ExtTripInfo_seq.NEXTVAL INTO :NEW.IID FROM DUAL;
					            END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "Extended trip info Table");
                }
                #endregion
                #region Update 107 . Extended trip detail info Table
                strUpd = "Rez000107";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"
						begin
							execute immediate
								 'CREATE TABLE GFI_FLT_ExtTripInfoDetail(
								  IID number(10) NOT NULL,
								  ExtTripId number(38) NOT NULL,
								  CONSTRAINT PK_GFI_FLT_ExtTripInfoDetail PRIMARY KEY  (IID)
								 )';
						end;";
                    if (GetDataDT(ExistTableSql("GFI_FLT_ExtTripInfoDetail")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_FLT_ExtTripInfoDetail_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_FLT_ExtTripInfoDetail_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_FLT_ExtTripInfoDetail_seq_tr
					            BEFORE INSERT ON GFI_FLT_ExtTripInfoDetail FOR EACH ROW
					            WHEN(NEW.IID IS NULL)
					            BEGIN
						            SELECT GFI_FLT_ExtTripInfoDetail_seq.NEXTVAL INTO :NEW.IID FROM DUAL;
					            END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "Extended trip detail info Table");
                }
                #endregion

                #region Update 108. GFI_SYS_LoginAudit
                strUpd = "Rez000108";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_SYS_Audit add UserID int Default 1";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Audit", "UserID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_Audit 
								ADD CONSTRAINT FK_GFI_SYS_Audit_GFI_SYS_User 
									FOREIGN KEY(UserID) 
									REFERENCES GFI_SYS_User(UID) ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_Audit_GFI_SYS_User", "GFI_SYS_Audit")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_AuditAccess add UserID int Default 1";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_AuditAccess", "UserID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_AuditAccess 
								ADD CONSTRAINT FK_GFI_SYS_AuditAccess_GFI_SYS_User 
									FOREIGN KEY(UserID) 
									REFERENCES GFI_SYS_User(UID)  ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_AuditAccess_GFI_SYS_User", "GFI_SYS_AuditAccess")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_ActiveSession add UserID int Default 1";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_ActiveSession", "UserID")).Rows[0][0].ToString() == "0")
                    {
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                        sql = @"delete from GFI_SYS_ActiveSession";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    }

                    sql = @"ALTER TABLE GFI_SYS_ActiveSession 
								ADD CONSTRAINT FK_GFI_SYS_ActiveSession_GFI_SYS_User 
									FOREIGN KEY(UserID) 
									REFERENCES GFI_SYS_User(UID) 
								    ON DELETE  Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_ActiveSession_GFI_SYS_User", "GFI_SYS_ActiveSession")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "User Constraints");
                }
                #endregion
                #region Update 109. GFI_FLT_ExtTripInfo
                strUpd = "Rez000109";
                if (!CheckUpdateExist(strUpd))
                {
                    //sql = @"ALTER TABLE GFI_FLT_ExtTripInfoMapping ADD  CONSTRAINT PK_GFI_FLT_ExtTripInfoMapping PRIMARY KEY  (iID )";
                    //if (GetDataDT(ExistsSQLConstraint("PK_GFI_FLT_ExtTripInfoMapping", "GFI_FLT_ExtTripInfoMapping")).Rows.Count == 0)
                    //    _SqlConn.ScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD  CONSTRAINT UK_GFI_FLT_ExtTripInfo Unique (ExtTripID )";
                    if (GetDataDT(ExistsSQLConstraint("UK_GFI_FLT_ExtTripInfo", "GFI_FLT_ExtTripInfo")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfoDetail 
							ADD CONSTRAINT FK_GFI_FLT_ExtTripInfoDetail_GFI_FLT_ExtTripInfo
								FOREIGN KEY(ExtTripID) 
								REFERENCES GFI_FLT_ExtTripInfo(ExtTripID) 
							    ON DELETE  Cascade";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfoDetail_GFI_FLT_ExtTripInfo", "GFI_FLT_ExtTripInfoDetail")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_User ADD LoginAttempt int Default 1";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "LoginAttempt")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD Field4Name nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "Field4Name")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD Field4Value nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "Field4Value")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD Field5Name nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "Field5Name")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD Field5Value nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "Field5Value")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD Field6Name nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "Field6Name")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD Field6Value nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "Field6Value")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD AdditionalField1 nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "AdditionalField1")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD AdditionalField2 nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "AdditionalField2")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD AdditionalValue1 nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "AdditionalValue1")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD AdditionalValue2 nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "AdditionalValue2")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "GFI_FLT_ExtTripInfo");
                }
                #endregion
                #region Update 110. GFI_AMM_AssetZoneMap
                strUpd = "Rez000110";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"
							begin
								execute immediate
									'CREATE TABLE GFI_AMM_AssetZoneMap 
									(
										Id number(10)  NOT NULL,
										AssetID number(10) NOT NULL,
										ZoneID number(10) NOT NULL,
										Param1 nvarchar2(10) NULL,
										Param2 nvarchar2(10) NULL,
										Param3 nvarchar2(10) NULL,
										CreatedBy nvarchar2(50) NULL,
										CreatedDate timestamp(3) NULL,
										ModifiedBy nvarchar2(50) NULL,
										ModifiedDate timestamp(3) NULL
									)';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_AMM_AssetZoneMap")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_AMM_AssetZoneMap_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_AMM_AssetZoneMap_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_AMM_AssetZoneMap_seq_tr
					        BEFORE INSERT ON GFI_AMM_AssetZoneMap FOR EACH ROW
					        WHEN(NEW.Id IS NULL)
					        BEGIN
						        SELECT GFI_AMM_AssetZoneMap_seq.NEXTVAL INTO :NEW.Id FROM DUAL;
					        END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_AMM_AssetZoneMap ADD  CONSTRAINT PK_GFI_AMM_AssetZoneMap PRIMARY KEY  (ID )";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_FLT_ExtTripInfoMapping", "GFI_AMM_AssetZoneMap")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_AMM_AssetZoneMap 
									ADD CONSTRAINT FK_GFI_AMM_AssetZoneMap_GFI_FLT_Asset 
										FOREIGN KEY(AssetID) 
										REFERENCES GFI_FLT_Asset(AssetID) 
									ON DELETE  Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_AssetZoneMap_GFI_FLT_Asset", "GFI_AMM_AssetZoneMap")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_AMM_AssetZoneMap 
									ADD CONSTRAINT FK_GGFI_AMM_AssetZoneMap_GFI_FLT_ZoneHeader
										FOREIGN KEY(ZoneID) 
										REFERENCES GFI_FLT_ZoneHeader(ZoneID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GGFI_AMM_AssetZoneMap_GFI_FLT_ZoneHeader", "GFI_AMM_AssetZoneMap")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "GFI_AMM_AssetZoneMap");
                }
                #endregion
                #region Update 111 . GFI_AMM_ExtZoneProp Tables
                strUpd = "Rez000111";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"
							begin
								execute immediate
									'CREATE TABLE GFI_FLT_AuxilliaryType 
									(
										""UID"" number(10) NOT NULL,
										AuxilliaryTypeName nvarchar2(30) NULL,
										AuxilliaryTypeDescription nvarchar2(30) NULL,
										Field1 nvarchar2(50) NULL,
										Field1Label nvarchar2(30) NULL,
										Field1Type nvarchar2(30) NULL,
										Field2 nvarchar2(50) NULL,
										Field2Label nvarchar2(30) NULL,
										Field2Type nvarchar2(30) NULL,
										Field3 nvarchar2(50) NULL,
										Field3Label nvarchar2(30) NULL,
										Field3Type nvarchar2(30) NULL,
										Field4 nvarchar2(50) NULL,
										Field4Label nvarchar2(30) NULL,
										Field4Type nvarchar2(30) NULL,
										Field5 nvarchar2(50) NULL,
										Field5Label nvarchar2(30) NULL,
										Field5Type nvarchar2(30) NULL,
										Field6 nvarchar2(50) NULL,
										Field6Label nvarchar2(30) NULL,
										Field6Type nvarchar2(30) NULL,
										Field7 nvarchar2(50) NULL,
										Field7Label nvarchar2(30) NULL,
										Field7Type nvarchar2(30) NULL,
										CreatedDate timestamp(3) NULL,
										CreatedBy nvarchar2(30) NULL,
										UpdatedDate timestamp(3) NULL,
										UpdatedBy nvarchar2(30) NULL,
                                        CONSTRAINT PK_GFI_FLT_AuxilliaryType PRIMARY KEY  (""UID"")
									)';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_FLT_AuxilliaryType")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_FLT_AuxilliaryType_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_FLT_AuxilliaryType_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_FLT_AuxilliaryType_seq_tr
								BEFORE INSERT ON GFI_FLT_AuxilliaryType FOR EACH ROW
								WHEN(NEW.Uid IS NULL)
								BEGIN
									SELECT GFI_FLT_AuxilliaryType_seq.NEXTVAL INTO :NEW.Uid FROM DUAL;
								END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"
							begin
								execute immediate
									'CREATE TABLE GFI_AMM_ExtZoneProps 
									(
										UID number(19)  NOT NULL,
										ZoneId number(10) NOT NULL,
										ZoneType nvarchar2(50) NULL,
										ApplicationNum nvarchar2(50) NULL,
										ApplicationType nvarchar2(50) NULL,
										ApplicationDate timestamp(3) NULL,
										ONames nvarchar2(100) NULL,
										Surname nvarchar2(50) NULL,
										AddressL1 nvarchar2(50) NULL,
										TVNumber nvarchar2(50) NULL,
										LinkPDF nvarchar2(100) NULL,
										LinkIMG nvarchar2(100) NULL,
                                        CONSTRAINT PK_GFI_AMM_ExtZoneProps PRIMARY KEY  (UID)
									)';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_AMM_ExtZoneProps")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_AMM_ExtZoneProps_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_AMM_ExtZoneProps_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_AMM_ExtZoneProps_seq_tr
					        BEFORE INSERT ON GFI_AMM_ExtZoneProps FOR EACH ROW
					        WHEN(NEW.Uid IS NULL)
					        BEGIN
						        SELECT GFI_AMM_ExtZoneProps_seq.NEXTVAL INTO :NEW.Uid FROM DUAL;
					        END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //sql = @"ALTER TABLE GFI_AMM_ExtZoneProps ADD  CONSTRAINT PK_GFI_AMM_ExtZoneProps PRIMARY KEY  (UID)";
                    //if (GetDataDT(ExistsSQLConstraint("PK_GFI_AMM_ExtZoneProps")).Rows.Count == 0)
                    //    _SqlConn.ScriptExecute(sql, myStrConn);

                    sql = @"
							begin
								execute immediate
									'CREATE TABLE GFI_HRM_Team 
									(
                                        iID number(19)  NOT NULL,
										TeamCode nvarchar2(50) NOT NULL,
										TeamSize nvarchar2(50) NULL,
										TeamLeader nvarchar2(200) NULL,
										MainRole nvarchar2(200) NULL,
										CreatedBy nvarchar2(200) NULL,
										CreatedDate timestamp(3) NULL,
										UpdatedBy nvarchar2(200) NULL,
										UpdatedDate timestamp(3) NULL,
                                        CONSTRAINT PK_GFI_HRM_Team PRIMARY KEY (iID )
									)';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_HRM_Team")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"
							begin
								execute immediate
									'CREATE TABLE GFI_AMM_ExtZoneProp 
									(
										APL_ID nvarchar2(50) NOT NULL,				
										APL_NAME nvarchar2(255) NULL,
										APL_FNAME nvarchar2(255) NULL,
										APL_DATE timestamp(3) NULL,
										APL_EFF_DATE timestamp(3) NULL,
										APL_ADDR1 nvarchar2(255) NULL,
										APL_ADDR2 nvarchar2(255) NULL,
										APL_ADDR3 nvarchar2(255) NULL,
										APL_LOC_PLAN nvarchar2(255) NULL,
										APL_TV_NO nvarchar2(255) NULL,
										APL_PROP_DEVP nvarchar2(255) NULL,
										APL_EXTENT binary_double NULL,
										Link_Pdf nvarchar2(255) NULL,
										TYPE nvarchar2(255) NULL,
										BUFFERSIZE binary_double NULL,
										POINT_X binary_double NULL,
										POINT_Y binary_double NULL,
										IID number(10)  NOT NULL,
										ZoneID number(10) NULL,
										Status nvarchar2(50) NULL
									)';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_AMM_ExtZoneProp")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_AMM_ExtZoneProp_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_AMM_ExtZoneProp_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_AMM_ExtZoneProp_seq_tr
					        BEFORE INSERT ON GFI_AMM_ExtZoneProp FOR EACH ROW
					        WHEN(NEW.IID IS NULL)
					        BEGIN
						        SELECT GFI_AMM_ExtZoneProp_seq.NEXTVAL INTO :NEW.IID FROM DUAL;
					        END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_AMM_ExtZoneProp ADD  CONSTRAINT PK_GFI_AMM_ExtZoneProp PRIMARY KEY  (IID )";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_AMM_ExtZoneProp", "GFI_AMM_ExtZoneProp")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"
							begin
								execute immediate
									'CREATE TABLE GFI_AMM_ExtZonePropAssessment 
									(
										UID number(10)  NOT NULL,
										APL_Id nvarchar2(50) NULL,
										ASSESS_Code nvarchar2(50) NULL,
										ASSESS_Date timestamp(3) NULL,
										ASSESS_Detail nvarchar2(400) NULL,
										ASSESS_Recommend nvarchar2(400) NULL,
										ASSESS_Status nvarchar2(5) NULL,
										CreatedBy nvarchar2(50) NULL,
										CreatedDate timestamp(3) NULL,
										UpdatedBy nvarchar2(50) NULL,
										UpdatedDate timestamp(3) NULL,
										ASSESS_By nvarchar2(50) NULL
									)';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_AMM_ExtZonePropAssessment")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_AMM_ExtZonePropAssessment_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_AMM_ExtZonePropAssessment_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_AMM_ExtZonePropAssessment_seq_tr
					        BEFORE INSERT ON GFI_AMM_ExtZonePropAssessment FOR EACH ROW
					        WHEN(NEW.UID IS NULL)
					        BEGIN
						        SELECT GFI_AMM_ExtZonePropAssessment_seq.NEXTVAL INTO :NEW.UID FROM DUAL;
					        END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_AMM_ExtZonePropAssessment ADD  CONSTRAINT PK_GFI_AMM_ExtZonePropAssessment PRIMARY KEY  (UID )";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_AMM_ExtZonePropAssessment", "GFI_AMM_ExtZonePropAssessment")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_AMM_ExtZoneProp ADD  CONSTRAINT UK_GFI_AMM_ExtZoneProp Unique (APL_Id )";
                    if (GetDataDT(ExistsSQLConstraint("UK_GFI_AMM_ExtZoneProp", "GFI_AMM_ExtZoneProp")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_AMM_ExtZonePropAssessment 
									ADD CONSTRAINT FK_GFI_AMM_ExtZonePropAssessment_GFI_AMM_ExtZoneProp 
										FOREIGN KEY(APL_Id) 
										REFERENCES GFI_AMM_ExtZoneProp(APL_Id) 
									ON DELETE  Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_ExtZonePropAssessment_GFI_AMM_ExtZoneProp", "GFI_AMM_ExtZonePropAssessment")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"
							begin
								execute immediate
									'CREATE TABLE GFI_AMM_ExtZonePropDetails(
										APL_ID nvarchar2(50) NOT NULL, 
										BCM_CDATE timestamp(3) NULL,
										BCM_CMT nvarchar2(255) NULL,
										REF_DESC nvarchar2(50) NULL,
										UID number(10)  NOT NULL,
										BCM_STATUS nvarchar2(10) NULL
									)';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_AMM_ExtZonePropDetails")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_AMM_ExtZonePropDetails_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_AMM_ExtZonePropDetails_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_AMM_ExtZonePropDetails_seq_tr
					        BEFORE INSERT ON GFI_AMM_ExtZonePropDetails FOR EACH ROW
					        WHEN(NEW.UID IS NULL)
					        BEGIN
						        SELECT GFI_AMM_ExtZonePropDetails_seq.NEXTVAL INTO :NEW.UID FROM DUAL;
					        END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_AMM_ExtZonePropDetails ADD  CONSTRAINT PK_GFI_AMM_ExtZonePropDetails PRIMARY KEY  (UID)";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_AMM_ExtZonePropDetails", "GFI_AMM_ExtZonePropDetails")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_AMM_ExtZonePropDetails 
									ADD CONSTRAINT FK_GFI_AMM_ExtZonePropDetails_GFI_AMM_ExtZoneProp 
										FOREIGN KEY(APL_Id) 
										REFERENCES GFI_AMM_ExtZoneProp(APL_Id) 
									ON DELETE  Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_ExtZonePropDetails_GFI_AMM_ExtZoneProp", "GFI_AMM_ExtZonePropDetails")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"
							begin
								execute immediate
									'CREATE TABLE GFI_PLA_DataSnapshot(
									 UId number(10) NOT NULL,
									 GroupingId number(10) NULL,
									 FieldName nvarchar2(50) NULL,
									 FieldValue nvarchar2(50) NULL,
									 CreatedBy nvarchar2(50) NULL,
									 CreatedDate timestamp(3) NULL,
									 UpdatedBy nvarchar2(50) NULL,
									 UpdatedDate timestamp(3) NULL,
									 CONSTRAINT PK_GFI_PLA_DataSnapshot PRIMARY KEY  (UId)
									)';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_PLA_DataSnapshot")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_PLA_DataSnapshot_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_PLA_DataSnapshot_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_PLA_DataSnapshot_seq_tr
					        BEFORE INSERT ON GFI_PLA_DataSnapshot FOR EACH ROW
					        WHEN(NEW.UId IS NULL)
					        BEGIN
						        SELECT GFI_PLA_DataSnapshot_seq.NEXTVAL INTO :NEW.UId FROM DUAL;
					        END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_PLA_DataSnapshot  
								ADD  CONSTRAINT FK_GFI_PLA_DataSnapshot_GFI_AMM_ExtZonePropDetails 
								FOREIGN KEY(GroupingId)
								REFERENCES GFI_AMM_ExtZonePropDetails (UID)
									ON DELETE  Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLA_DataSnapshot_GFI_AMM_ExtZonePropDetails", "GFI_PLA_DataSnapshot")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "GFI_AMM_ExtZoneProp Tables");
                }
                #endregion
                #region Update 112. GFI_SYS_NaaAddress
                strUpd = "Rez000112";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"
							begin
								execute immediate
									'CREATE TABLE GFI_SYS_NaaAddress 
									(
                                        iID number(19)  NOT NULL,
										NaaCode nvarchar2(50) NOT NULL,
										LastName nvarchar2(200) NULL,
										OtherName nvarchar2(200) NULL,
										Street1 nvarchar2(400) NULL,
										Street2 nvarchar2(400) NULL,
										Locality nvarchar2(400) NULL,
										City nvarchar2(200) NULL,
										Tel nvarchar2(50) NULL,
										Email nvarchar2(200) NULL,
										Mobile nvarchar2(50) NULL,
										CreatedBy nvarchar2(50) NULL,
										CreatedDate timestamp(3) NULL,
										UpdatedBy nvarchar2(50) NULL,
										UpdatedDate timestamp(3) NULL,
                                        CONSTRAINT PK_GFI_SYS_NaaAddress PRIMARY KEY (iID)
									)';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_SYS_NaaAddress")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"
							begin
								execute immediate
									'CREATE TABLE GFI_HRM_EmpBase 
									(
										EmpCode number(38) NOT NULL,
										NaaCode number(19) NULL,
										EmpType nvarchar2(50) NULL,
										EmpCategory nvarchar2(50) NULL,
										DateJoined timestamp(3) NULL,
										CreatedBy nvarchar2(200) NULL,
										CreatedDate timestamp(3) NULL,
										UpdatedBy nvarchar2(200) NULL,
										UpdatedDate timestamp(3) NULL,
                                        CONSTRAINT PK_GFI_HRM_EmpBase PRIMARY KEY (EmpCode)
									)';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_HRM_EmpBase")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_HRM_EmpBase ADD  CONSTRAINT PK_GFI_HRM_EmpBase PRIMARY KEY  (EmpCode )";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_HRM_EmpBase", "GFI_HRM_EmpBase")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_HRM_EmpBase  
								ADD  CONSTRAINT FK_GFI_HRM_EmpBase_GFI_SYS_NaaAddress
								FOREIGN KEY(NaaCode)
								REFERENCES GFI_SYS_NaaAddress (iID)
									ON DELETE Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_HRM_EmpBase_GFI_SYS_NaaAddress", "GFI_HRM_EmpBase")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"
							begin
							execute immediate
							    'CREATE TABLE GFI_FLT_ExtTripInfoResources 
							    (
                                    IID number(10) NOT NULL,
								    EmpCode number(38) NULL,
								    NaaCodeValue nvarchar2(50) NULL,						
								    ExtTripID number(38) NULL,
								    CONSTRAINT PK_GFI_FLT_ExtTripInfoResources PRIMARY KEY (iID)                                        
							    )';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_FLT_ExtTripInfoResources")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_FLT_ExtTripInfoResources_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_FLT_ExtTripInfoResources_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_FLT_ExtTripInfoResources_seq_tr
					            BEFORE INSERT ON GFI_FLT_ExtTripInfoResources FOR EACH ROW
					            WHEN(NEW.IID IS NULL)
					            BEGIN
						            SELECT GFI_FLT_ExtTripInfoResources_seq.NEXTVAL INTO :NEW.IID FROM DUAL;
					            END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfoResources ADD CONSTRAINT PK_GFI_FLT_ExtTripInfoResources PRIMARY KEY  (IID )";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_FLT_ExtTripInfoResources", "GFI_FLT_ExtTripInfoResources")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfoResources  
								ADD  CONSTRAINT FK_GFI_FLT_ExtTripInfoResources_GFI_HRM_EmpBase
								FOREIGN KEY(EmpCode)
								REFERENCES GFI_HRM_EmpBase (EmpCode)
									ON DELETE Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfoResources_GFI_HRM_EmpBase", "GFI_FLT_ExtTripInfoResources")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfoResources  
								ADD  CONSTRAINT FK_GFI_FLT_ExtTripInfoResources_GFI_FLT_ExtTripInfo
								FOREIGN KEY(ExtTripID)
								REFERENCES GFI_FLT_ExtTripInfo (ExtTripID)
									ON DELETE Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfoResources_GFI_FLT_ExtTripInfo", "GFI_FLT_ExtTripInfoResources")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "GFI_SYS_NaaAddress");
                }
                #endregion

                #region MOLG n Security
                #region Update 76. Asset Wizard.
                strUpd = "Rez000076";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"alter table GFI_FLT_AssetDeviceMap drop column Aux1Name_cbo";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "Aux1Name_cbo")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"alter table GFI_FLT_AssetDeviceMap drop column Aux2Name_cbo";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "Aux2Name_cbo")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"alter table GFI_FLT_AssetDeviceMap drop column Aux3Name_cbo";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "Aux3Name_cbo")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"alter table GFI_FLT_AssetDeviceMap drop column Aux4Name_cbo";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "Aux4Name_cbo")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"alter table GFI_FLT_AssetDeviceMap drop column Aux5Name_cbo";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "Aux5Name_cbo")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"alter table GFI_FLT_AssetDeviceMap drop column Aux6Name_cbo";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "Aux6Name_cbo")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"
							begin
								execute immediate
									'CREATE TABLE GFI_SYS_UOM
									(
										UID number(10) NOT NULL,
										UOM nvarchar2(20) NOT NULL,
										CONSTRAINT PK_SYS_BY_UOM PRIMARY KEY (UID)
									)';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_SYS_UOM")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_SYS_UOM_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_SYS_UOM_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_SYS_UOM_seq_tr
					            BEFORE INSERT ON GFI_SYS_UOM FOR EACH ROW
					            WHEN(NEW.UID IS NULL)
					            BEGIN
						            SELECT GFI_SYS_UOM_seq.NEXTVAL INTO :NEW.UID FROM DUAL;
					            END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"
							begin
								execute immediate
									'CREATE TABLE GFI_FLT_DeviceAuxilliaryMap(
										Uid number(10) NOT NULL,
										MapID number(10) NULL,
										Auxilliary nvarchar2(30) NULL,
										AuxilliaryType number(10) NULL,
										Field1 nvarchar2(200) NULL,
										Field2 nvarchar2(200) NULL,
										Field3 number(10) NULL,
										Field4 number(10) NULL,
										Field5 nvarchar2(20) NULL,
										Field6 timestamp(3) NULL,
										CreatedDate timestamp(3) NULL,
										CreatedBy nvarchar2(30) NULL,
										UpdatedDate timestamp(3) NULL,
										UpdatedBy nvarchar2(30) NULL,
										Uom number(10) NULL,
										Thresholdhigh number(10) NULL,
										Thresholdlow number(10) NULL,
										Capacity number(10) NULL,
										CONSTRAINT PK_GFI_FLT_DeviceAuxilliaryMap PRIMARY KEY  (Uid)
									)';
						end;";
                    if (GetDataDT(ExistTableSql("GFI_FLT_DeviceAuxilliaryMap")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_FLT_DeviceAuxilliaryMap_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_FLT_DeviceAuxilliaryMap_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_FLT_DeviceAuxilliaryMap_seq_tr
					            BEFORE INSERT ON GFI_FLT_DeviceAuxilliaryMap FOR EACH ROW
					            WHEN(NEW.Uid IS NULL)
					            BEGIN
						            SELECT GFI_FLT_DeviceAuxilliaryMap_seq.NEXTVAL INTO :NEW.Uid FROM DUAL;
					            END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_DeviceAuxilliaryMap 
							ADD CONSTRAINT FK_GFI_FLT_DeviceAuxilliaryMap_GFI_FLT_AssetDeviceMap 
								FOREIGN KEY(MapID) 
								REFERENCES GFI_FLT_AssetDeviceMap(MapID) 
						 ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_DeviceAuxilliaryMap_GFI_FLT_AssetDeviceMap", "GFI_FLT_DeviceAuxilliaryMap")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_DeviceAuxilliaryMap 
							ADD CONSTRAINT FK_GFI_FLT_DeviceAuxilliaryMap_GFI_FLT_AuxilliaryType 
								FOREIGN KEY(AuxilliaryType) 
								REFERENCES GFI_FLT_AuxilliaryType(UID)  ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_DeviceAuxilliaryMap_GFI_FLT_AuxilliaryType", "GFI_FLT_DeviceAuxilliaryMap")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_DeviceAuxilliaryMap 
							ADD CONSTRAINT FK_GFI_FLT_DeviceAuxilliaryMap_GFI_SYS_UOM 
								FOREIGN KEY(UOM) 
								REFERENCES GFI_SYS_UOM(UID)  ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_DeviceAuxilliaryMap_GFI_SYS_UOM", "GFI_FLT_DeviceAuxilliaryMap")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"
							begin
								execute immediate
									'CREATE TABLE GFI_FLT_AssetType(
										 Uid NUMBER(10) NOT NULL,
										 AssetTypeName nvarchar2(30) NULL,
										 AssetTypeDescription nvarchar2(30) NULL,
										 Field1 nvarchar2(50) NULL,
										 Field1Label nvarchar2(30) NULL,
										 Field1Type nvarchar2(30) NULL,
										 Field2 nvarchar2(50) NULL,
										 Field2Label nvarchar2(30) NULL,
										 Field2Type nvarchar2(30) NULL,
										 Field3 nvarchar2(50) NULL,
										 Field3Label nvarchar2(30) NULL,
										 Field3Type nvarchar2(30) NULL,
										 Field4 nvarchar2(50) NULL,
										 Field4Label nvarchar2(30) NULL,
										 Field4Type nvarchar2(30) NULL,
										 Field5 nvarchar2(50) NULL,
										 Field5Label nvarchar2(30) NULL,
										 Field5Type nvarchar2(30) NULL,
										 Field6 nvarchar2(50) NULL,
										 Field6Label nvarchar2(30) NULL,
										 Field6Type nvarchar2(30) NULL,
										CONSTRAINT PK_GFI_FLT_AssetType PRIMARY KEY (UID)
									)';
							end;";
                    if (GetDataDT(ExistTableSql("GFI_FLT_AssetType")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_FLT_AssetType_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_FLT_AssetType_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_FLT_AssetType_seq_tr
					            BEFORE INSERT ON GFI_FLT_AssetType FOR EACH ROW
					            WHEN(NEW.Uid IS NULL)
					            BEGIN
						            SELECT GFI_FLT_AssetType_seq.NEXTVAL INTO :NEW.Uid FROM DUAL;
					            END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //AssetType at = new AssetType();
                    //at = at.GetAssetTypeByName("*** Default ***");
                    //if (at == null)
                    //{
                    //    at = new AssetType();
                    //    at.AssetTypeName = "*** Default ***";
                    //    at.AssetTypeDescription = "*** Default ***";

                    //    at.Save(at, true);
                    //}
                    //at = at.GetAssetTypeByName("*** Default ***");

                    sql = @"alter table GFI_FLT_Asset Add AssetType int Default " + 1;
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Asset", "AssetType")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    dt = GetDataDT(GetFieldSchema("GFI_FLT_Asset", "AssetType"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                    {
                        sql = @"alter table GFI_FLT_Asset drop column AssetType";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                        //sql = @"alter table GFI_FLT_Asset Add AssetType int not null Default " + at.Uid.ToString();
                        sql = @"alter table GFI_FLT_Asset Add AssetType int Default " + 1;
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    }

                    InsertDBUpdate(strUpd, "Asset Wizard");
                }
                #endregion

                #region Update 77. ExtTripInfoType.
                strUpd = "Rez000077";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_FLT_ExtTripInfoType add sDataSource NVARCHAR2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfoType", "sDataSource")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    ExtTripInfoTypeService extTripInfoTypeService = new ExtTripInfoTypeService();
                    ExtTripInfoType et = extTripInfoTypeService.GetExtTripInfoTypeByTypeName("NETWEIGHT", sConnStr);
                    if (et == null)
                    {
                        et = new ExtTripInfoType();
                        et.TypeName = "NETWEIGHT";
                        et.Field1Name = "NETWEIGHT";
                        et.Field1Label = "NETWEIGHT";
                        et.Field1Type = "Numeric";
                        et.CreatedBy = usrInst.Username;
                        et.CreatedDate = DateTime.Now;
                        extTripInfoTypeService.SaveExtTripInfoType(et, true, sConnStr);
                    }

                    et = new ExtTripInfoType();
                    et = extTripInfoTypeService.GetExtTripInfoTypeByTypeName("VOUCHER_NO", sConnStr);
                    if (et == null)
                    {
                        et = new ExtTripInfoType();
                        et.TypeName = "VOUCHER_NO";
                        et.Field1Name = "VOUCHER_NO";
                        et.Field1Label = "VOUCHER_NO";
                        et.Field1Type = "Text";
                        et.CreatedBy = usrInst.Username;
                        et.CreatedDate = DateTime.Now;
                        extTripInfoTypeService.SaveExtTripInfoType(et, true, sConnStr);
                    }

                    et = new ExtTripInfoType();
                    et = extTripInfoTypeService.GetExtTripInfoTypeByTypeName("GROSSWGT", sConnStr);
                    if (et == null)
                    {
                        et = new ExtTripInfoType();
                        et.TypeName = "GROSSWGT";
                        et.Field1Name = "GROSSWGT";
                        et.Field1Label = "GROSSWGT";
                        et.Field1Type = "Numeric";
                        et.CreatedBy = usrInst.Username;
                        et.CreatedDate = DateTime.Now;
                        extTripInfoTypeService.SaveExtTripInfoType(et, true, sConnStr);
                    }

                    et = new ExtTripInfoType();
                    et = extTripInfoTypeService.GetExtTripInfoTypeByTypeName("TAREWGT", sConnStr);
                    if (et == null)
                    {
                        et = new ExtTripInfoType();
                        et.TypeName = "TAREWGT";
                        et.Field1Name = "TAREWGT";
                        et.Field1Label = "TAREWGT";
                        et.Field1Type = "Numeric";
                        et.CreatedBy = usrInst.Username;
                        et.CreatedDate = DateTime.Now;
                        extTripInfoTypeService.SaveExtTripInfoType(et, true, sConnStr);
                    }

                    GlobalParam g = new GlobalParam();
                    g = globalParamsService.GetGlobalParamsByName("O_IMP_TABLES", sConnStr);
                    if (g == null)
                    {
                        g = new GlobalParam();
                        g.ParamName = "O_IMP_TABLES";
                        g.PValue = "applications,assessments";
                        g.ParamComments = "Tables to be imported from Oracle";
                        globalParamsService.SaveGlobalParams(g, true, sConnStr);
                    }

                    g = globalParamsService.GetGlobalParamsByName("O_IMP_FL_PATH", sConnStr);
                    if (g == null)
                    {
                        g = new GlobalParam(); g.ParamName = "O_IMP_FL_PATH";
                        g.PValue = "S:\\IMPORT_REPOSITORY";
                        g.ParamComments = "Oracle Files Import Path";
                        globalParamsService.SaveGlobalParams(g, true, sConnStr);
                    }
                    g = globalParamsService.GetGlobalParamsByName("FILES_REP", sConnStr);
                    if (g == null)
                    {
                        g = new GlobalParam(); g.ParamName = "FILES_REP";
                        g.PValue = "S:\\REPOSITORY";
                        g.ParamComments = "Files Respository";
                        globalParamsService.SaveGlobalParams(g, true, sConnStr);
                    }
                    g = globalParamsService.GetGlobalParamsByName("O_CONN", sConnStr);
                    if (g == null)
                    {
                        g = new GlobalParam(); g.ParamName = "O_CONN";
                        g.PValue = "127.0.0.1,XE,1521,naveo";
                        g.ParamComments = "Oracle Connection String  Servername,SID,PORT,schema";
                        globalParamsService.SaveGlobalParams(g, true, sConnStr);
                    }
                    g = globalParamsService.GetGlobalParamsByName("PWD_O", sConnStr);
                    if (g == null)
                    {
                        g = new GlobalParam();
                        g.ParamName = "PWD_O";
                        g.PValue = "KZkSzmSyhh1Ug8IPw0C8nA==";
                        g.ParamComments = "Oracle Password";
                        globalParamsService.SaveGlobalParams(g, true, sConnStr);
                    }
                    g = globalParamsService.GetGlobalParamsByName("IDLEWAIT", sConnStr);
                    if (g == null)
                    {
                        g = new GlobalParam(); g.ParamName = "IDLEWAIT";
                        g.PValue = "30";
                        g.ParamComments = "IDLE WAIT TIME IN MINUTES";
                        globalParamsService.SaveGlobalParams(g, true, sConnStr);
                    }
                    g = globalParamsService.GetGlobalParamsByName("EventFilePath", sConnStr);
                    if (g == null)
                    {
                        g = new GlobalParam(); g.ParamName = "EventFilePath";
                        g.PValue = "C:\\";
                        g.ParamComments = "EventFilePath";
                        globalParamsService.SaveGlobalParams(g, true, sConnStr);
                    }
                    g = globalParamsService.GetGlobalParamsByName("LocalFilePath", sConnStr);
                    if (g == null)
                    {
                        g = new GlobalParam(); g.ParamName = "LocalFilePath";
                        g.PValue = "C:\\";
                        g.ParamComments = "LocalFilePath";
                        globalParamsService.SaveGlobalParams(g, true, sConnStr);
                    }

                    ExtTripInfoMappingService extTripInfoMappingService = new ExtTripInfoMappingService();
                    ExtTripInfoMapping m = extTripInfoMappingService.GetExtTripInfoMappingById("1", sConnStr);
                    if (m == null)
                    {
                        m = new ExtTripInfoMapping();
                        m.Description = "WEIGHT";
                        m.ExtTripInfoId = 1;
                        m.MappingID = 1;
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        extTripInfoMappingService.SaveExtTripInfoMapping(m, true, sConnStr);
                    }

                    m = new ExtTripInfoMapping();
                    m = extTripInfoMappingService.GetExtTripInfoMappingById("2", sConnStr);
                    if (m == null)
                    {
                        m = new ExtTripInfoMapping();
                        m.Description = "WEIGHT";
                        m.ExtTripInfoId = 2;
                        m.MappingID = 1;
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        extTripInfoMappingService.SaveExtTripInfoMapping(m, true, sConnStr);
                    }

                    m = new ExtTripInfoMapping();
                    m = extTripInfoMappingService.GetExtTripInfoMappingById("3", sConnStr);
                    if (m == null)
                    {
                        m = new ExtTripInfoMapping();
                        m.Description = "WEIGHT";
                        m.ExtTripInfoId = 3;
                        m.MappingID = 1;
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        extTripInfoMappingService.SaveExtTripInfoMapping(m, true, sConnStr);
                    }

                    m = new ExtTripInfoMapping();
                    m = extTripInfoMappingService.GetExtTripInfoMappingById("4", sConnStr);
                    if (m == null)
                    {
                        m = new ExtTripInfoMapping();
                        m.Description = "WEIGHT";
                        m.ExtTripInfoId = 4;
                        m.MappingID = 1;
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        extTripInfoMappingService.SaveExtTripInfoMapping(m, true, sConnStr);
                    }
                    InsertDBUpdate(strUpd, "ExtTripInfoType");
                }
                #endregion
                #region Update 78. Callibration and ExtTripInfoDetail updates.
                strUpd = "Rez000078";
                if (!CheckUpdateExist(strUpd))
                {
                    //sql = @"alter table GFI_FLT_ExtTripInfoDetail drop column ExtTripID";
                    //if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfoDetail", "ExtTripID")).Rows[0][0].ToString() != "0")
                    //{
                    //    dbExecute("delete from GFI_FLT_ExtTripInfoDetail");
                    //    _SqlConn.ScriptExecute(sql, myStrConn);
                    //}

                    sql = "ALTER TABLE GFI_FLT_ExtTripInfoDetail add GPSDataId int";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfoDetail", "GPSDataId")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_FLT_ExtTripInfoDetail add DateTimeGPS_UTC TIMESTAMP";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfoDetail", "DateTimeGPS_UTC")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_SYS_NaaAddress add Fax NVARCHAR2(15)";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_NaaAddress", "Fax")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"
                            begin
                                execute immediate
                                    'CREATE TABLE GFI_FLT_CalibrationChart (
								        iID number(19)  NOT NULL,
								        AssetID number(19) NULL,
								        DeviceID nvarchar2(50) NULL,
								        CalType nvarchar2(20) NULL,
								        SeuqenceNo number(10) NULL,
								        ReadingUOM number(10) not NULL,
								        Reading binary_double NULL,
								        ConvertedValue binary_double NULL,
								        ConvertedUOM number(10) not NULL,
								        CreatedBy nvarchar2(30) NULL,
								        CreatedDate timestamp(3) NULL,
								        UpdatedBy nvarchar2(30) NULL,
								        UpdatedDate timestamp(3) NULL,
								        CONSTRAINT PK_GFI_FLT_CalibrationChart PRIMARY KEY  (iID)
							        )';
                            end;";
                    if (GetDataDT(ExistTableSql("GFI_FLT_CalibrationChart")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_FLT_CalibrationChart_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_FLT_CalibrationChart_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_FLT_CalibrationChart_seq_tr
                                BEFORE INSERT ON GFI_FLT_CalibrationChart FOR EACH ROW
                                WHEN(NEW.iID IS NULL)
                                BEGIN
                                    SELECT GFI_FLT_CalibrationChart_seq.NEXTVAL INTO :NEW.iID FROM DUAL;
                                END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    if (GetDataDT(ExistColumnSql("GFI_SYS_UOM", "Description")).Rows[0][0].ToString() == "0")
                        dbExecute(RenameColumn("UOM", "Description", "GFI_SYS_UOM"));

                    sql = @"ALTER TABLE GFI_FLT_CalibrationChart 
							ADD CONSTRAINT FK_GFI_FLT_CalibrationChart_GFI_SYS_UOM_C 
								FOREIGN KEY (ConvertedUOM) REFERENCES GFI_SYS_UOM (""UID"")";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_CalibrationChart_GFI_SYS_UOM_C", "GFI_FLT_CalibrationChart")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_CalibrationChart
                            ADD CONSTRAINT FK_GFI_FLT_CalibrationChart_GFI_SYS_UOM_R
                                FOREIGN KEY (ReadingUOM)REFERENCES GFI_SYS_UOM(""UID"")";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_CalibrationChart_GFI_SYS_UOM_R", "GFI_FLT_CalibrationChart")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    UOM u = new UOM();
                    u = uomService.GetUOMByDescription("Kg", sConnStr);
                    if (u == null)
                    {
                        u = new UOM();
                        u.Description = "Kg";
                        uomService.SaveUOM(u, true, sConnStr);
                    }

                    u = uomService.GetUOMByDescription("%", sConnStr);
                    if (u == null)
                    {
                        u = new UOM();
                        u.Description = "%";
                        uomService.SaveUOM(u, true, sConnStr);
                    }
                    UOM up = uomService.GetUOMByDescription("%", sConnStr);

                    u = uomService.GetUOMByDescription("Ton", sConnStr);
                    if (u == null)
                    {
                        u = new UOM();
                        u.Description = "Ton";
                        uomService.SaveUOM(u, true, sConnStr);
                    }

                    u = uomService.GetUOMByDescription("Degrees C", sConnStr);
                    if (u == null)
                    {
                        u = new UOM();
                        u.Description = "Degrees C";
                        uomService.SaveUOM(u, true, sConnStr);
                    }

                    u = uomService.GetUOMByDescription("Liters", sConnStr);
                    if (u == null)
                    {
                        u = new UOM();
                        u.Description = "Liters";
                        uomService.SaveUOM(u, true, sConnStr);
                    }
                    u = uomService.GetUOMByDescription("Liters", sConnStr);

                    InsertDBUpdate(strUpd, "Callibration and ExtTripInfoDetail updates");
                }
                #endregion
                #region Update 79. AssetExtProVehicles and Team Additional fields
                strUpd = "Rez000079";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_FLT_ZoneHeader add isMarker int DEFAULT ((0))";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ZoneHeader", "isMarker")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_FLT_ZoneHeader add iBuffer int DEFAULT ((0))";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ZoneHeader", "iBuffer")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_FLT_CalibrationChart add AuxID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_CalibrationChart", "AuxID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_CalibrationChart 
							ADD CONSTRAINT FK_GFI_FLT_CalibrationChart_GFI_FLT_DeviceAuxilliaryMap 
								FOREIGN KEY(AuxID) 
								REFERENCES GFI_FLT_DeviceAuxilliaryMap(UID) 
						 ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_CalibrationChart_GFI_FLT_DeviceAuxilliaryMap", "GFI_FLT_CalibrationChart")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_AMM_AssetExtProVehicles add Field1Value nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_AssetExtProVehicles", "Field1Value")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_AMM_AssetExtProVehicles add Field2Value nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_AssetExtProVehicles", "Field2Value")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_AMM_AssetExtProVehicles add Field3Value int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_AssetExtProVehicles", "Field3Value")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_AMM_AssetExtProVehicles add Field4Value int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_AssetExtProVehicles", "Field4Value")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_AMM_AssetExtProVehicles add Field5Value nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_AssetExtProVehicles", "Field5Value")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_AMM_AssetExtProVehicles add Field6Value timestamp NULL";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_AssetExtProVehicles", "Field6Value")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //DROP FOREIGN KEY 
                    sql = "ALTER TABLE GFI_HRM_EmpBase DROP CONSTRAINT FK_GFI_HRM_EmpBase_GFI_SYS_NaaAddress";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_HRM_EmpBase_GFI_SYS_NaaAddress", "GFI_HRM_EmpBase")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_FLT_ExtTripInfoResources DROP CONSTRAINT FK_GFI_FLT_ExtTripInfoResources_GFI_HRM_EmpBase";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfoResources_GFI_HRM_EmpBase", "GFI_FLT_ExtTripInfoResources")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_HRM_EmpBase DROP CONSTRAINT PK_GFI_HRM_EmpBase";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_HRM_EmpBase", "GFI_HRM_EmpBase")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_HRM_EmpBase DROP CONSTRAINT FK_GFI_HRM_EmpBase_GFI_HRM_Team";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_HRM_EmpBase_GFI_HRM_Team", "GFI_HRM_EmpBase")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_SYS_NaaAddress DROP CONSTRAINT PK_GFI_SYS_NaaAddress";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_SYS_NaaAddress", "GFI_SYS_NaaAddress")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //GFI_SYS_NaaAddress
                    sql = @"ALTER TABLE GFI_SYS_NaaAddress ADD iID int NOT NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_NaaAddress", "iID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Adding TempEmpCodeID
                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfoResources ADD TempEmpCodeID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfoResources", "TempEmpCodeID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //GFI_HRM_EmpBase
                    sql = @"ALTER TABLE GFI_HRM_EmpBase ADD iID int";
                    if (GetDataDT(ExistColumnSql("GFI_HRM_EmpBase", "iID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_HRM_EmpBase ADD TID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_HRM_EmpBase", "TID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "alter table GFI_HRM_EmpBase add CONSTRAINT PK_GFI_HRM_EmpBase PRIMARY KEY (iID )";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_HRM_EmpBase", "PK_GFI_HRM_EmpBase")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_HRM_EmpBase 
							ADD CONSTRAINT FK_GFI_HRM_EmpBase_GFI_HRM_Team
								FOREIGN KEY(TID) 
								REFERENCES GFI_HRM_Team(iID) 
						 ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_HRM_EmpBase_GFI_HRM_Team", "GFI_HRM_EmpBase")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfoResources  
								ADD  CONSTRAINT FK_GFI_FLT_ExtTripInfoResources_GFI_HRM_EmpBase
								FOREIGN KEY(EmpCode)
								REFERENCES GFI_HRM_EmpBase (iID)
									ON DELETE Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfoResources_GFI_HRM_EmpBase", "GFI_FLT_ExtTripInfoResources")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Drop Old Columns (varchar)
                    sql = @"alter table GFI_FLT_ExtTripInfoResources drop column EmpCodeOld";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfoResources", "EmpCodeOld")).Rows[0][0].ToString() != "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"alter table GFI_HRM_EmpBase drop column NaaCodeOld";
                    if (GetDataDT(ExistColumnSql("GFI_HRM_EmpBase", "NaaCodeOld")).Rows[0][0].ToString() != "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //GFI_FLT_ExtTripInfoResources
                    //1. DROP FOREIGN KEY
                    sql = "ALTER TABLE GFI_FLT_ExtTripInfoResources DROP CONSTRAINT FK_GFI_FLT_ExtTripInfoResources_GFI_FLT_ExtTripInfo";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfoResources_GFI_FLT_ExtTripInfo", "GFI_FLT_ExtTripInfoResources")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //5. Recreate Constraint
                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfoResources  
								ADD  CONSTRAINT FK_GFI_FLT_ExtTripInfoResources_GFI_FLT_ExtTripInfo
								FOREIGN KEY(ExtTripID)
								REFERENCES GFI_FLT_ExtTripInfo (iID)
									ON DELETE Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfoResources_GFI_FLT_ExtTripInfo", "GFI_FLT_ExtTripInfoResources")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "AssetExtProVehicles and Team Additional fields and Constraints");
                }
                #endregion
                #region Update 80. GFI_FLT_ExtTripInfoDetails Constraints
                strUpd = "Rez000080";
                if (!CheckUpdateExist(strUpd))
                {
                    //1. DROP FOREIGN KEY
                    sql = "ALTER TABLE GFI_FLT_ExtTripInfoDetail DROP CONSTRAINT FK_GFI_FLT_ExtTripInfoDetail_GFI_FLT_ExtTripInfo";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfoDetail_GFI_FLT_ExtTripInfo", "GFI_FLT_ExtTripInfoDetail")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //5. Recreate Constraint
                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfoDetail  
								ADD  CONSTRAINT FK_GFI_FLT_ExtTripInfoDetail_GFI_FLT_ExtTripInfo
								FOREIGN KEY(ExtTripID)
								REFERENCES GFI_FLT_ExtTripInfo (iID)
									ON DELETE Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfoDetail_GFI_FLT_ExtTripInfo", "GFI_FLT_ExtTripInfoDetail")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "GFI_FLT_ExtTripInfoDetails Constraints");
                }
                #endregion
                #region Update 81. GFI_FLT_ExtTripInfo Constraints
                strUpd = "Rez000081";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "alter table GFI_FLT_ExtTripInfoType add CONSTRAINT PK_GFI_FLT_ExtTripInfoType PRIMARY KEY (iID )";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_FLT_ExtTripInfoType", "GFI_FLT_ExtTripInfoType")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //GFI_FLT_ExtTripInfoMapping Constraint
                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfoMapping
								ADD  CONSTRAINT FK_GFI_FLT_ExtTripInfoMapping_GFI_FLT_ExtTripInfoType
								FOREIGN KEY(ExtTripInfoId)
								REFERENCES GFI_FLT_ExtTripInfoType (iID)
									ON DELETE Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfoMapping_GFI_FLT_ExtTripInfoType", "GFI_FLT_ExtTripInfoMapping")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //GFI_FLT_ExtTripInfo Constraints

                    #region Field1Name
                    //1. DROP FOREIGN KEY
                    sql = "ALTER TABLE GFI_FLT_ExtTripInfo DROP CONSTRAINT FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType", "GFI_FLT_ExtTripInfo")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //2. Create Foreign Key Column in different Name
                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD TempID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "TempID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //3. Update Foreign Key with right value
                    sql = @"UPDATE GFI_FLT_ExtTripInfo 
								SET TempID = T2.iID
							FROM        GFI_FLT_ExtTripInfo T1
							INNER JOIN  GFI_FLT_ExtTripInfoType T2 ON T2.Field1Name = T1.Field1Name";
                    sql = @"UPDATE 
                            (
                                SELECT t1.TempID as OLD, T2.iID as NEW
                                FROM GFI_FLT_ExtTripInfo t1
                                     INNER JOIN GFI_FLT_ExtTripInfoType t2 ON T2.Field1Name = T1.Field1Name
                                --WHERE GFI_FLT_ExtTripInfo.UPDATETYPE='blah'
                            ) t
                            SET t.OLD = t.NEW";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "TempID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //4. Rename Columns. Old Foreign becomes old, TempID becomes real ForeignID
                    dt = GetDataDT(GetFieldSchema("GFI_FLT_ExtTripInfo", "Field1Name"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                    {
                        dbExecute(RenameColumn("Field1Name", "Field1NameOld", "GFI_FLT_ExtTripInfo"));
                        dbExecute(RenameColumn("TempID", "Field1Name", "GFI_FLT_ExtTripInfo"));
                    }
                    #endregion
                    #region Field2Name
                    //1. DROP FOREIGN KEY
                    sql = "ALTER TABLE GFI_FLT_ExtTripInfo DROP CONSTRAINT FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType", "GFI_FLT_ExtTripInfo")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //2. Create Foreign Key Column in different Name
                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD TempID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "TempID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //3. Update Foreign Key with right value
                    sql = @"UPDATE GFI_FLT_ExtTripInfo 
								SET TempID = T2.iID
							FROM        GFI_FLT_ExtTripInfo T1
							INNER JOIN  GFI_FLT_ExtTripInfoType T2 ON T2.Field1Name = T1.Field2Name";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "TempID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //4. Rename Columns. Old Foreign becomes old, TempID becomes real ForeignID
                    dt = GetDataDT(GetFieldSchema("GFI_FLT_ExtTripInfo", "Field2Name"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                    {
                        dbExecute(RenameColumn("Field2Name", "Field2NameOld", "GFI_FLT_ExtTripInfo"));
                        dbExecute(RenameColumn("TempID", "Field2Name", "GFI_FLT_ExtTripInfo"));
                    }
                    #endregion
                    #region Field3Name
                    //1. DROP FOREIGN KEY
                    sql = "ALTER TABLE GFI_FLT_ExtTripInfo DROP CONSTRAINT FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType", "GFI_FLT_ExtTripInfo")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //2. Create Foreign Key Column in different Name
                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD TempID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "TempID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //3. Update Foreign Key with right value
                    sql = @"UPDATE GFI_FLT_ExtTripInfo 
								SET TempID = T2.iID
							FROM        GFI_FLT_ExtTripInfo T1
							INNER JOIN  GFI_FLT_ExtTripInfoType T2 ON T2.Field1Name = T1.Field3Name";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "TempID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //4. Rename Columns. Old Foreign becomes old, TempID becomes real ForeignID
                    dt = GetDataDT(GetFieldSchema("GFI_FLT_ExtTripInfo", "Field3Name"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                    {
                        dbExecute(RenameColumn("Field3Name", "Field3NameOld", "GFI_FLT_ExtTripInfo"));
                        dbExecute(RenameColumn("TempID", "Field3Name", "GFI_FLT_ExtTripInfo"));
                    }
                    #endregion
                    #region Field4Name
                    //1. DROP FOREIGN KEY
                    sql = "ALTER TABLE GFI_FLT_ExtTripInfo DROP CONSTRAINT FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType", "GFI_FLT_ExtTripInfo")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //2. Create Foreign Key Column in different Name
                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD TempID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "TempID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //3. Update Foreign Key with right value
                    sql = @"UPDATE GFI_FLT_ExtTripInfo 
								SET TempID = T2.iID
							FROM        GFI_FLT_ExtTripInfo T1
							INNER JOIN  GFI_FLT_ExtTripInfoType T2 ON T2.Field1Name = T1.Field4Name";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "TempID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //4. Rename Columns. Old Foreign becomes old, TempID becomes real ForeignID
                    dt = GetDataDT(GetFieldSchema("GFI_FLT_ExtTripInfo", "Field4Name"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                    {
                        dbExecute(RenameColumn("Field4Name", "Field4NameOld", "GFI_FLT_ExtTripInfo"));
                        dbExecute(RenameColumn("TempID", "Field4Name", "GFI_FLT_ExtTripInfo"));
                    }
                    #endregion
                    #region Field5Name
                    //1. DROP FOREIGN KEY
                    sql = "ALTER TABLE GFI_FLT_ExtTripInfo DROP CONSTRAINT FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType", "GFI_FLT_ExtTripInfo")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //2. Create Foreign Key Column in different Name
                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD TempID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "TempID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //3. Update Foreign Key with right value
                    sql = @"UPDATE GFI_FLT_ExtTripInfo 
								SET TempID = T2.iID
							FROM        GFI_FLT_ExtTripInfo T1
							INNER JOIN  GFI_FLT_ExtTripInfoType T2 ON T2.Field1Name = T1.Field5Name";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "TempID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //4. Rename Columns. Old Foreign becomes old, TempID becomes real ForeignID
                    dt = GetDataDT(GetFieldSchema("GFI_FLT_ExtTripInfo", "Field5Name"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                    {
                        dbExecute(RenameColumn("Field5Name", "Field5NameOld", "GFI_FLT_ExtTripInfo"));
                        dbExecute(RenameColumn("TempID", "Field5Name", "GFI_FLT_ExtTripInfo"));
                    }
                    #endregion
                    #region Field6Name
                    //1. DROP FOREIGN KEY
                    sql = "ALTER TABLE GFI_FLT_ExtTripInfo DROP CONSTRAINT FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType", "GFI_FLT_ExtTripInfo")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //2. Create Foreign Key Column in different Name
                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD TempID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "TempID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //3. Update Foreign Key with right value
                    sql = @"UPDATE GFI_FLT_ExtTripInfo 
								SET TempID = T2.iID
							FROM        GFI_FLT_ExtTripInfo T1
							INNER JOIN  GFI_FLT_ExtTripInfoType T2 ON T2.Field1Name = T1.Field6Name";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "TempID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //4. Rename Columns. Old Foreign becomes old, TempID becomes real ForeignID
                    dt = GetDataDT(GetFieldSchema("GFI_FLT_ExtTripInfo", "Field6Name"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                    {
                        dbExecute(RenameColumn("Field6Name", "Field6NameOld", "GFI_FLT_ExtTripInfo"));
                        dbExecute(RenameColumn("TempID", "Field6Name", "GFI_FLT_ExtTripInfo"));
                    }
                    #endregion

                    //5. Recreate Constraint
                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo  
								ADD  CONSTRAINT FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType
								FOREIGN KEY(Field1Name) REFERENCES GFI_FLT_ExtTripInfoType (iID) ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType", "GFI_FLT_ExtTripInfo")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo  
								ADD  CONSTRAINT FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType
                                FOREIGN KEY(Field2Name) REFERENCES GFI_FLT_ExtTripInfoType (iID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType", "GFI_FLT_ExtTripInfo")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo  
								ADD  CONSTRAINT FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType
                                FOREIGN KEY(Field3Name) REFERENCES GFI_FLT_ExtTripInfoType (iID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType", "GFI_FLT_ExtTripInfo")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo  
								ADD  CONSTRAINT FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType
                                FOREIGN KEY(Field4Name) REFERENCES GFI_FLT_ExtTripInfoType (iID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType", "GFI_FLT_ExtTripInfo")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo  
								ADD  CONSTRAINT FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType
                                FOREIGN KEY(Field6Name) REFERENCES GFI_FLT_ExtTripInfoType (iID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ExtTripInfo_GFI_FLT_ExtTripInfoType", "GFI_FLT_ExtTripInfo")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    //------

                    //6. Delete Old Column
                    sql = @"alter table GFI_FLT_ExtTripInfo drop column Field1NameOld";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "Field1NameOld")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    sql = @"alter table GFI_FLT_ExtTripInfo drop column Field2NameOld";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "Field2NameOld")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    sql = @"alter table GFI_FLT_ExtTripInfo drop column Field3NameOld";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "Field3NameOld")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    sql = @"alter table GFI_FLT_ExtTripInfo drop column Field4NameOld";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "Field4NameOld")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    sql = @"alter table GFI_FLT_ExtTripInfo drop column Field5NameOld";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "Field5NameOld")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    sql = @"alter table GFI_FLT_ExtTripInfo drop column Field6NameOld";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "Field6NameOld")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "GFI_FLT_ExtTripInfo Constraints");
                }
                #endregion
                #region Update 82. GFI_FLT_ExtTripInfoType
                strUpd = "Rez000082";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"alter table GFI_FLT_ExtTripInfoType drop column Field2Name";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfoType", "Field2Name")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"alter table GFI_FLT_ExtTripInfoType drop column Field2Label";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfoType", "Field2Label")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"alter table GFI_FLT_ExtTripInfoType drop column Field2Type";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfoType", "Field2Type")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_FLT_ExtTripInfoType add sDataSource NVARCHAR2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfoType", "sDataSource")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_FLT_ExtTripInfoMapping add Type NVARCHAR2(20) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfoMapping", "Type")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_AMM_AssetExtProVehicles add Field1Id int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_AssetExtProVehicles", "Field1Id")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    sql = "ALTER TABLE GFI_AMM_AssetExtProVehicles add Field2Id int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_AssetExtProVehicles", "Field2Id")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    sql = "ALTER TABLE GFI_AMM_AssetExtProVehicles add Field3Id int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_AssetExtProVehicles", "Field3Id")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    sql = "ALTER TABLE GFI_AMM_AssetExtProVehicles add Field4Id int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_AssetExtProVehicles", "Field4Id")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    sql = "ALTER TABLE GFI_AMM_AssetExtProVehicles add Field5Id int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_AssetExtProVehicles", "Field5Id")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    sql = "ALTER TABLE GFI_AMM_AssetExtProVehicles add Field6Id int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_AssetExtProVehicles", "Field6Id")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_FLT_Asset DROP CONSTRAINT FK_GFI_FLT_Asset_GFI_FLT_AssetType";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_Asset_GFI_FLT_AssetType", "GFI_FLT_Asset")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "GFI_FLT_ExtTripInfoType");
                }
                #endregion

                #region Update 83. AssetTypeMapping
                strUpd = "Rez000083";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_FLT_AssetTypeMapping(
								IID number(10) NOT NULL,
								Description nvarchar2(255) NOT NULL,
								CreatedBy nvarchar2(50) NULL,
								CreatedDate timestamp(3) NULL,
								UpdatedBy nvarchar2(50) NULL,
								UpdatedDate timestamp(3) NULL,
								MappingId number(10) NOT NULL,
                                AssetTypeId number(10) NOT NULL,
								 CONSTRAINT PK_GFI_FLT_AssetTypeMapping PRIMARY KEY (IID)
							)";
                    if (GetDataDT(ExistTableSql("GFI_FLT_AssetTypeMapping")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_FLT_AssetTypeMapping_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_FLT_AssetTypeMapping_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_FLT_AssetTypeMapping_seq_tr
                    BEFORE INSERT ON GFI_FLT_AssetTypeMapping FOR EACH ROW
                    WHEN(NEW.IID IS NULL)
                    BEGIN
                        SELECT GFI_FLT_AssetTypeMapping_seq.NEXTVAL INTO :NEW.IID FROM DUAL;
                    END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_AMM_AssetExtProVehicles  
                            ADD  CONSTRAINT FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_ExtTripInfoType
                            FOREIGN KEY(Field1Id) REFERENCES GFI_FLT_ExtTripInfoType (iID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_ExtTripInfoType", "GFI_AMM_AssetExtProVehicles")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_AMM_AssetExtProVehicles  
                            ADD  CONSTRAINT FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_ExtTripInfoType
                            FOREIGN KEY(Field2Id) REFERENCES GFI_FLT_ExtTripInfoType (iID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_ExtTripInfoType", "GFI_AMM_AssetExtProVehicles")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_AMM_AssetExtProVehicles  
                            ADD  CONSTRAINT FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_ExtTripInfoType
                            FOREIGN KEY(Field3Id) REFERENCES GFI_FLT_ExtTripInfoType (iID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_ExtTripInfoType", "GFI_AMM_AssetExtProVehicles")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    sql = @"ALTER TABLE GFI_AMM_AssetExtProVehicles  
                            ADD  CONSTRAINT FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_ExtTripInfoType
                            FOREIGN KEY(Field4Id) REFERENCES GFI_FLT_ExtTripInfoType (iID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_ExtTripInfoType", "GFI_AMM_AssetExtProVehicles")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_AMM_AssetExtProVehicles  
                            ADD  CONSTRAINT FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_ExtTripInfoType
                            FOREIGN KEY(Field5Id) REFERENCES GFI_FLT_ExtTripInfoType (iID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_ExtTripInfoType", "GFI_AMM_AssetExtProVehicles")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_AMM_AssetExtProVehicles  
                            ADD  CONSTRAINT FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_ExtTripInfoType
                            FOREIGN KEY(Field6Id) REFERENCES GFI_FLT_ExtTripInfoType (iID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_ExtTripInfoType", "GFI_AMM_AssetExtProVehicles")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    //-------

                    sql = @"alter table GFI_FLT_ExtTripInfoMapping drop column Type";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfoMapping", "Type")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD Consumption float NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "Consumption")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD Timediff float NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "Timediff")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD DateTimeGPSStart_UTC timestamp NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "DateTimeGPSStart_UTC")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo ADD DateTimeGPSStop_UTC timestamp NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "DateTimeGPSStop_UTC")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "AssetTypeMapping");
                }
                #endregion
                #region Update 84. Default Fuel Callibration
                strUpd = "Rez000084";
                if (!CheckUpdateExist(strUpd))
                {
                    AuxilliaryType at = new AuxilliaryType();
                    AuxilliaryTypeService auxilliaryTypeService = new AuxilliaryTypeService();
                    at = auxilliaryTypeService.GetAuxilliaryTypeByNames("Fuel", sConnStr);
                    if (at == null)
                    {
                        at = new AuxilliaryType();
                        at.AuxilliaryTypeName = "Fuel";
                        at.AuxilliaryTypeDescription = "Fuel";

                        at.Field1 = "Field1";
                        at.Field1Label = String.Empty;
                        at.Field1Type = "VarChar";

                        at.Field2 = "Field2";
                        at.Field2Label = String.Empty;
                        at.Field2Type = "VarChar";

                        at.Field3 = "Field3";
                        at.Field3Label = String.Empty;
                        at.Field3Type = "Integer";

                        at.Field4 = "Field4";
                        at.Field4Label = String.Empty;
                        at.Field4Type = "Integer";

                        at.Field5 = "Field5";
                        at.Field5Label = String.Empty;
                        at.Field5Type = "Boolean";

                        at.Field6 = "Field6";
                        at.Field6Label = String.Empty;
                        at.Field6Type = "DateTime";
                        auxilliaryTypeService.SaveAuxilliaryType(at, true, sConnStr);
                    }
                    at = auxilliaryTypeService.GetAuxilliaryTypeByNames("Fuel", sConnStr);

                    InsertDBUpdate(strUpd, "Default Fuel Callibration");
                }
                #endregion
                #region Update 85. User Previous Password
                strUpd = "Rez000085";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_SYS_User ADD PreviousPassword nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "PreviousPassword")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_User ADD PasswordUpdateddate timestamp NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "PasswordUpdateddate")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "User Previous Password");
                }
                #endregion
                #region Update 86. New Menu items wrt new menu design
                strUpd = "Rez000086";
                if (!CheckUpdateExist(strUpd))
                {
                    InsertDBUpdate(strUpd, "New Menu items wrt new menu design");
                }
                #endregion
                #region Update 87. GFI_AMM_VehicleMaintenance New Coloumn and View Added.
                strUpd = "Rez000087";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintenance add AssetStatus nvarchar2(20) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_VehicleMaintenance", "AssetStatus")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    sql = @"ALTER TABLE GFI_AMM_VehicleMaintenance add Comment nvarchar2(12) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_VehicleMaintenance", "Comment")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    sql = @"CREATE VIEW GFI_FLT_VW_VehicleMaintenance 
								As SELECT AssetId, startdate, AssetStatus, Comment
								FROM GFI_AMM_VehicleMaintenance vm                         
								WHERE systimestamp > = StartDate and systimestamp <= EndDate";
                    if (GetDataDT(ExistViewSql("GFI_FLT_VW_VehicleMaintenance")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    InsertDBUpdate(strUpd, "Vehicle Maintenance to LiveMap ");
                }
                #endregion
                #region Update 88. GFI_FLT_AssetDataType
                strUpd = "Rez000088";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_FLT_AssetDataType
							(
								 iID number(10) NOT NULL,
								 TypeName nvarchar2(20) NOT NULL,
								 Field1Name nvarchar2(50) NOT NULL,
								 Field1Label nvarchar2(50) NOT NULL,
								 Field1Type nvarchar2(50) NOT NULL,
								 CreatedBy nvarchar2(30) NOT NULL,
								 CreatedDate timestamp(3) NOT NULL,
								 UpdatedBy nvarchar2(30) NULL,
								 UpdatedDate timestamp(3) NULL,
								 sDataSource nvarchar2(50) NULL,
								 CONSTRAINT PK_GFI_FLT_AssetDataType PRIMARY KEY (IID)
							) ";
                    if (GetDataDT(ExistTableSql("GFI_FLT_AssetDataType")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE GFI_FLT_AssetDataType_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql("GFI_FLT_AssetDataType_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER GFI_FLT_AssetDataType_seq_tr
                                BEFORE INSERT ON GFI_FLT_AssetDataType FOR EACH ROW
                                WHEN(NEW.iID IS NULL)
                                BEGIN
                                    SELECT GFI_FLT_AssetDataType_seq.NEXTVAL INTO :NEW.iID FROM DUAL;
                                END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    AssetDataTypeService assetDataTypeService = new AssetDataTypeService();
                    AssetDataType assetDataType = assetDataTypeService.GetAssetDataTypeByField1Name("**Default**", sConnStr);
                    if (assetDataType == null)
                    {
                        assetDataType = new AssetDataType();
                        assetDataType.CreatedBy = usrInst.UID.ToString();
                        assetDataType.CreatedDate = DateTime.Today;
                        assetDataType.Field1Label = "**Default**";
                        assetDataType.Field1Name = "**Default**";
                        assetDataType.Field1Type = "Text";
                        assetDataType.sDataSource = "";
                        assetDataType.TypeName = "**Default**";
                        assetDataType.UpdatedBy = usrInst.UID.ToString();
                        assetDataType.UpdatedDate = DateTime.Today;
                        assetDataTypeService.SaveAssetDataType(assetDataType, true, sConnStr);
                    }
                    assetDataType = assetDataTypeService.GetAssetDataTypeByField1Name("**Default**", sConnStr);

                    sql = @"alter table GFI_FLT_AssetTypeMapping add AssetTypeId int default " + assetDataType.IID.ToString();
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AssetTypeMapping", "AssetTypeId")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_AssetTypeMapping 
							ADD CONSTRAINT FK_GFI_FLT_AssetTypeMapping_GFI_FLT_AssetDataType 
								FOREIGN KEY(AssetTypeId) 
								REFERENCES GFI_FLT_AssetDataType(iID) 
						 ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_AssetTypeMapping_GFI_FLT_AssetDataType", "GFI_FLT_AssetTypeMapping")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //----
                    sql = @"ALTER TABLE GFI_AMM_AssetExtProVehicles  
								ADD  CONSTRAINT FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_AssetDataType
								FOREIGN KEY(Field1Id) REFERENCES GFI_FLT_AssetDataType (iID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_AssetDataType", "GFI_AMM_AssetExtProVehicles")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_AMM_AssetExtProVehicles  
								ADD  CONSTRAINT FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_AssetDataType
								FOREIGN KEY(Field2Id) REFERENCES GFI_FLT_AssetDataType (iID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_AssetDataType", "GFI_AMM_AssetExtProVehicles")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_AMM_AssetExtProVehicles  
								ADD  CONSTRAINT FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_AssetDataType
								FOREIGN KEY(Field3Id) REFERENCES GFI_FLT_AssetDataType (iID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_AssetDataType", "GFI_AMM_AssetExtProVehicles")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_AMM_AssetExtProVehicles  
								ADD  CONSTRAINT FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_AssetDataType
								FOREIGN KEY(Field4Id) REFERENCES GFI_FLT_AssetDataType (iID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_AssetDataType", "GFI_AMM_AssetExtProVehicles")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_AMM_AssetExtProVehicles  
								ADD  CONSTRAINT FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_AssetDataType
								FOREIGN KEY(Field5Id) REFERENCES GFI_FLT_AssetDataType (iID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_AssetDataType", "GFI_AMM_AssetExtProVehicles")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_AMM_AssetExtProVehicles  
								ADD  CONSTRAINT FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_AssetDataType
								FOREIGN KEY(Field6Id) REFERENCES GFI_FLT_AssetDataType (iID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_AssetExtProVehicles_GFI_FLT_AssetDataType", "GFI_AMM_AssetExtProVehicles")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    List<AssetTypeMapping> latm = new List<AssetTypeMapping>();
                    AssetTypeMappingService assetTypeMappingService = new AssetTypeMappingService();
                    latm = assetTypeMappingService.GetAssetTypeMappingByMappingId(1, sConnStr);
                    if (latm.Count == 0)
                    {
                        AssetTypeMapping atm = new AssetTypeMapping();
                        atm.Description = " * *Default**";
                        atm.MappingId = 1;
                        atm.AssetTypeId = assetDataType.IID;
                        atm.CreatedBy = usrInst.Username;
                        atm.CreatedDate = DateTime.Now;
                        assetTypeMappingService.SaveAssetTypeMapping(atm, true, sConnStr);
                    }

                    sql = "ALTER TABLE GFI_FLT_Asset DROP CONSTRAINT FK_GFI_FLT_Asset_GFI_FLT_AssetType";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_Asset_GFI_FLT_AssetType", "GFI_FLT_Asset")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    latm = new AssetTypeMappingService().GetAssetTypeMappingByMappingId(1, sConnStr);
                    sql = @"alter table GFI_FLT_Asset Add AssetType int default " + latm[0].MappingId.ToString();
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Asset", "AssetType")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Asset 
							ADD CONSTRAINT FK_GFI_FLT_Asset_GFI_FLT_AssetTypeMapping
								FOREIGN KEY(AssetType) 
								REFERENCES GFI_FLT_AssetTypeMapping(iID) ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_Asset_GFI_FLT_AssetTypeMapping", "GFI_FLT_Asset")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    ///

                    InsertDBUpdate(strUpd, "GFI_FLT_AssetDataType");
                }
                #endregion
                #region Update 89. GFI_AMM_AssetExtProVehicles
                strUpd = "Rez000089";
                if (!CheckUpdateExist(strUpd))
                {
                    InsertDBUpdate(strUpd, "GFI_AMM_AssetExtProVehicles");
                }
                #endregion
                #region Update 90. New Menu items wrt new menu design
                strUpd = "Rez000090";
                if (!CheckUpdateExist(strUpd))
                {
                    //Add New Modules
                    Module m = new Module();
                    AccessTemplate at = new AccessTemplate();
                    AccessProfile ap = new AccessProfile();

                    DataTable dtAccessTemplate = at.GetAccessTemplates(usrInst.lMatrix, sConnStr);
                    DataSet dsResult = new DataSet();

                    m = moduleService.GetModulesByName("Vehicles and Assets --> Live Maintenance", sConnStr);
                    if (m == null)
                    {
                        m = new Module();
                        m.ModuleName = "Vehicles and Assets --> Live Maintenance";
                        m.Description = "Vehicles and Assets --> Live Maintenance";
                        m.Command = "cmdLiveMaintenance";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);

                        m = moduleService.GetModulesByName("Vehicles and Assets --> Live Maintenance", sConnStr);
                        foreach (DataRow r in dtAccessTemplate.Rows)
                        {
                            ap = new NaveoOneLib.Models.AccessProfile();
                            ap.UID = Constants.NullInt;
                            ap.TemplateId = Convert.ToInt32(r["UID"].ToString());
                            ap.ModuleId = m.UID;
                            ap.Command = "cmdLiveMaintenance";
                            ap.AllowRead = "0";
                            ap.AllowNew = "0";
                            ap.AllowEdit = "0";
                            ap.AllowDelete = "0";
                            ap.CreatedDate = DateTime.Now;
                            ap.CreatedBy = usrInst.Username;
                            ap.Save(ap, out dsResult, null, sConnStr);
                        }
                    }

                    m = moduleService.GetModulesByName("Settings --> Asset Type Mapping", sConnStr);
                    if (m == null)
                    {
                        m = new Module(); m.ModuleName = "Settings --> Asset Type Mapping";
                        m.Description = "Settings --> Asset Type Mapping";
                        m.Command = "cmdAsseTypeMapping";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);

                        m = moduleService.GetModulesByName("Settings --> Asset Type Mapping", sConnStr);
                        foreach (DataRow r in dtAccessTemplate.Rows)
                        {
                            ap = new NaveoOneLib.Models.AccessProfile();
                            ap.UID = Constants.NullInt;
                            ap.TemplateId = Convert.ToInt32(r["UID"].ToString());
                            ap.ModuleId = m.UID;
                            ap.Command = "cmdAsseTypeMapping";
                            ap.AllowRead = "0";
                            ap.AllowNew = "0";
                            ap.AllowEdit = "0";
                            ap.AllowDelete = "0";
                            ap.CreatedDate = DateTime.Now;
                            ap.CreatedBy = usrInst.Username;
                            ap.Save(ap, out dsResult, null, sConnStr);
                        }
                    }

                    m = moduleService.GetModulesByName("Settings --> Asset Type", sConnStr);
                    if (m == null)
                    {
                        m = new Module(); m.ModuleName = "Settings --> Asset Type";
                        m.Description = "Settings --> Asset Type";
                        m.Command = "cmdAsseType";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);

                        m = moduleService.GetModulesByName("Settings --> Asset Type", sConnStr);
                        foreach (DataRow r in dtAccessTemplate.Rows)
                        {
                            ap = new NaveoOneLib.Models.AccessProfile();
                            ap.UID = Constants.NullInt;
                            ap.TemplateId = Convert.ToInt32(r["UID"].ToString());
                            ap.ModuleId = m.UID;
                            ap.Command = "cmdAsseType";
                            ap.AllowRead = "0";
                            ap.AllowNew = "0";
                            ap.AllowEdit = "0";
                            ap.AllowDelete = "0";
                            ap.CreatedDate = DateTime.Now;
                            ap.CreatedBy = usrInst.Username;
                            ap.Save(ap, out dsResult, null, sConnStr);
                        }
                    }

                    m = moduleService.GetModulesByName("Drivers & Passengers --> Name and Addresses", sConnStr);
                    if (m == null)
                    {
                        m = new Module(); m.ModuleName = "Drivers & Passengers --> Name and Addresses";
                        m.Description = "Drivers & Passengers --> Name and Addresses";
                        m.Command = "cmdNaAddress";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);

                        m = moduleService.GetModulesByName("Drivers & Passengers --> Name and Addresses", sConnStr);
                        foreach (DataRow r in dtAccessTemplate.Rows)
                        {
                            ap = new NaveoOneLib.Models.AccessProfile();
                            ap.UID = Constants.NullInt;
                            ap.TemplateId = Convert.ToInt32(r["UID"].ToString());
                            ap.ModuleId = m.UID;
                            ap.Command = "cmdNaAddress";

                            ap.AllowRead = "0";
                            ap.AllowNew = "0";
                            ap.AllowEdit = "0";
                            ap.AllowDelete = "0";
                            ap.CreatedDate = DateTime.Now;
                            ap.CreatedBy = usrInst.Username;
                            ap.Save(ap, out dsResult, null, sConnStr);
                        }
                    }

                    m = moduleService.GetModulesByName("Drivers & Passengers --> Emp Master File", sConnStr);
                    if (m == null)
                    {
                        m = new Module(); m.ModuleName = "Drivers & Passengers --> Emp Master File";
                        m.Description = "Drivers & Passengers --> Emp Master File";
                        m.Command = "cmdEmpBas";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);

                        m = moduleService.GetModulesByName("Drivers & Passengers --> Emp Master File", sConnStr);
                        foreach (DataRow r in dtAccessTemplate.Rows)
                        {
                            ap = new NaveoOneLib.Models.AccessProfile();
                            ap.UID = Constants.NullInt;
                            ap.TemplateId = Convert.ToInt32(r["UID"].ToString());
                            ap.ModuleId = m.UID;
                            ap.Command = "cmdEmpBas";
                            ap.AllowRead = "0";
                            ap.AllowNew = "0";
                            ap.AllowEdit = "0";
                            ap.AllowDelete = "0";
                            ap.CreatedDate = DateTime.Now;
                            ap.CreatedBy = usrInst.Username;
                            ap.Save(ap, out dsResult, null, sConnStr);
                        }
                    }


                    m = moduleService.GetModulesByName("Fuel & Telemetry -->Reports --> Fuel Consumption", sConnStr);
                    if (m == null)
                    {
                        m = new Module(); m.ModuleName = "Fuel & Telemetry -->Reports --> Fuel Consumption";
                        m.Description = "Fuel & Telemetry -->Reports --> Fuel Consumption";
                        m.Command = "CmdFuelConsumption";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);

                        m = moduleService.GetModulesByName("Fuel & Telemetry -->Reports --> Fuel Consumption", sConnStr);
                        foreach (DataRow r in dtAccessTemplate.Rows)
                        {
                            ap = new NaveoOneLib.Models.AccessProfile();
                            ap.UID = Constants.NullInt;
                            ap.TemplateId = Convert.ToInt32(r["UID"].ToString());
                            ap.ModuleId = m.UID;
                            ap.Command = "CmdFuelConsumption";
                            ap.AllowRead = "0";
                            ap.AllowNew = "0";
                            ap.AllowEdit = "0";
                            ap.AllowDelete = "0";
                            ap.CreatedDate = DateTime.Now;
                            ap.CreatedBy = usrInst.Username;
                            ap.Save(ap, out dsResult, null, sConnStr);
                        }
                    }
                    m = moduleService.GetModulesByName("Fuel & Telemetry -->Reports --> Refuel Report", sConnStr);
                    if (m == null)
                    {
                        m = new Module(); m.ModuleName = "Fuel & Telemetry -->Reports --> Refuel Report";
                        m.Description = "Fuel & Telemetry -->Reports --> Refuel Report";
                        m.Command = "CmdRefuel";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);

                        m = moduleService.GetModulesByName("Fuel & Telemetry -->Reports --> Refuel Report", sConnStr);
                        foreach (DataRow r in dtAccessTemplate.Rows)
                        {
                            ap = new NaveoOneLib.Models.AccessProfile();
                            ap.UID = Constants.NullInt;
                            ap.TemplateId = Convert.ToInt32(r["UID"].ToString());
                            ap.ModuleId = m.UID;
                            ap.Command = "CmdRefuel";
                            ap.AllowRead = "0";
                            ap.AllowNew = "0";
                            ap.AllowEdit = "0";
                            ap.AllowDelete = "0";
                            ap.CreatedDate = DateTime.Now;
                            ap.CreatedBy = usrInst.Username;
                            ap.Save(ap, out dsResult, null, sConnStr);
                        }
                    }

                    m = moduleService.GetModulesByName("Geofencing -->Settings -->Zone Import", sConnStr);
                    if (m == null)
                    {
                        m = new Module(); m.ModuleName = "Geofencing -->Settings -->Zone Import";
                        m.Description = "Geofencing -->Settings -->Zone Import";
                        m.Command = "cmdZoneImport";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);

                        m = moduleService.GetModulesByName("Geofencing -->Settings -->Zone Import", sConnStr);
                        foreach (DataRow r in dtAccessTemplate.Rows)
                        {
                            ap = new NaveoOneLib.Models.AccessProfile();
                            ap.UID = Constants.NullInt;
                            ap.TemplateId = Convert.ToInt32(r["UID"].ToString());
                            ap.ModuleId = m.UID;
                            ap.Command = "cmdZoneImport";
                            ap.AllowRead = "0";
                            ap.AllowNew = "0";
                            ap.AllowEdit = "0";
                            ap.AllowDelete = "0";
                            ap.CreatedDate = DateTime.Now;
                            ap.CreatedBy = usrInst.Username;
                            ap.Save(ap, out dsResult, null, sConnStr);
                        }
                    }

                    InsertDBUpdate(strUpd, "New Menu items wrt new menu design");
                }
                #endregion
                #region Update 91. Settings --> System Administration
                strUpd = "Rez000091";
                if (!CheckUpdateExist(strUpd))
                {
                    //Add New Modules
                    Module m = new Module();
                    AccessTemplate at = new AccessTemplate();
                    AccessProfile ap = new AccessProfile();

                    DataTable dtAccessTemplate = at.GetAccessTemplates(usrInst.lMatrix, sConnStr);
                    DataSet dsResult = new DataSet();

                    m = moduleService.GetModulesByName("Settings --> System Administration", sConnStr);
                    if (m == null)
                    {
                        m = new Module();
                        m.ModuleName = "Settings --> System Administration";
                        m.Description = "Settings --> System Administration";
                        m.Command = "";
                        m.MenuHeaderId = 0;
                        m.MenuGroupId = 0;
                        m.OrderIndex = 0;
                        m.Title = "";
                        m.Summary = "";
                        m.CreatedBy = usrInst.Username;
                        m.CreatedDate = DateTime.Now;
                        moduleService.SaveModules(m, true, sConnStr);

                        m = moduleService.GetModulesByName("Settings --> System Administration", sConnStr);
                        foreach (DataRow r in dtAccessTemplate.Rows)
                        {
                            ap = new NaveoOneLib.Models.AccessProfile();
                            ap.UID = Constants.NullInt;
                            ap.TemplateId = Convert.ToInt32(r["UID"].ToString());
                            ap.ModuleId = m.UID;
                            ap.Command = "cmdSysAdmin";
                            ap.AllowRead = "0";
                            ap.AllowNew = "0";
                            ap.AllowEdit = "0";
                            ap.AllowDelete = "0";
                            ap.CreatedDate = DateTime.Now;
                            ap.CreatedBy = usrInst.Username;
                            ap.Save(ap, out dsResult, null, sConnStr);
                        }
                    }
                    InsertDBUpdate(strUpd, "Settings --> System Administration");
                }
                #endregion
                #region Update 92. Access Profiles
                strUpd = "Rez000092";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"delete from GFI_SYS_AccessProfiles where ModuleId in 
								(select UID from GFI_SYS_Modules where MenuHeaderId is null)";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"delete from GFI_SYS_Modules where MenuHeaderId is null";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @" CREATE TABLE GFI_AMM_MaintenanceTrip (
								Uid number(10) NOT NULL,
								MaintID number(10) NOT NULL,
								CustomTripID int NULL,
								CreatedDate timestamp(3) NULL,
								CreatedBy nvarchar2(30) NULL,
								UpdatedDate timestamp(3) NULL,
								UpdatedBy nvarchar2(30) NULL,
								CONSTRAINT PK_GFI_AMM_MaintenanceTrip PRIMARY KEY  (UID)
							)";
                    //if (GetDataDT(ExistTableSql("GFI_AMM_MaintenanceTrip")).Rows[0][0].ToString() == "0")
                    //    _SqlConn.ScriptExecute(sql, myStrConn);

                    CreateTable(sql, "GFI_AMM_MaintenanceTrip", "UID");

                    sql = @"ALTER TABLE GFI_AMM_MaintenanceTrip 
							ADD CONSTRAINT FK_GFI_AMM_MaintenanceTrip_GFI_AMM_VehicleMaintenance 
								FOREIGN KEY(MaintID) 
								REFERENCES GFI_AMM_VehicleMaintenance(URI)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_MaintenanceTrip_GFI_AMM_VehicleMaintenance", "GFI_AMM_MaintenanceTrip")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_AMM_MaintenanceTrip 
							ADD CONSTRAINT FK_GFI_AMM_MaintenanceTrip_GFI_FLT_ExtTripInfo
								FOREIGN KEY(CustomTripID) 
								REFERENCES GFI_FLT_ExtTripInfo (iID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_MaintenanceTrip_GFI_FLT_ExtTripInfo", "GFI_AMM_MaintenanceTrip")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    Globals.ValidateAccessProfilesModules(sConnStr);
                    InsertDBUpdate(strUpd, "Access Profiles");
                }
                #endregion
                #region Update 93. BCM_DECISION
                strUpd = "Rez000093";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_AMM_ExtZonePropDetails ADD BCM_DECISION nvarchar2(20) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_ExtZonePropDetails", "BCM_DECISION")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "update GFI_SYS_User set Status_b = 'AC', LoginCount = 1, PasswordUpdateddate = add_months(SYSTIMESTAMP, 120) where Username = 'Admin'";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "update GFI_SYS_User set Status_b = 'A' where Username not in ('Admin', 'Installer')";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    GlobalParam g = new GlobalParam();
                    g = globalParamsService.GetGlobalParamsByName("IdleTimeToLock", sConnStr);
                    if (g == null)
                    {
                        g = new GlobalParam();
                        g.ParamName = "IdleTimeToLock";
                        g.PValue = "60";
                        g.ParamComments = "Idle Time in minutes To Log off automatically";
                        globalParamsService.SaveGlobalParams(g, true, sConnStr);
                    }

                    InsertDBUpdate(strUpd, "BCM_DECISION");
                }
                #endregion
                #region Update 97.GFI_FLT_ExtTripInfo Trip Distance.
                strUpd = "Rez000097";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_FLT_ExtTripInfo add TripDistance float NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ExtTripInfo", "TripDistance")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "update GFI_SYS_GlobalParams set ParamName = 'PWD_O' where ParamName = 'O_PWD'";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "GFI_FLT_ExtTripInfo Trip Distance.");
                }
                #endregion
                #region Update 98. Security Config
                strUpd = "Rez000098";
                if (!CheckUpdateExist(strUpd))
                {
                    GlobalParam g = new GlobalParam();
                    g = globalParamsService.GetGlobalParamsByName("SYS_UserSec", sConnStr);
                    if (g == null)
                    {
                        g = new GlobalParam();
                        g.ParamName = "SYS_UserSec";
                        g.PValue = "No";
                        g.ParamComments = "Implement User Security. Yes/No";
                        globalParamsService.SaveGlobalParams(g, true, sConnStr);
                    }

                    Module module = new Module();
                    module = moduleService.GetModulesByName("MAPS --> Planning Report", sConnStr);
                    if (module == null)
                    {
                        module = new Module();
                        module.ModuleName = "MAPS --> Planning Report";
                        module.Description = "MAPS --> Planning Report";
                        module.Command = "CmdPlanningReport";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        moduleService.SaveModules(module, null, sConnStr);
                    }

                    module = new Module();
                    module = moduleService.GetModulesByName("Drivers & Passengers --> Team", sConnStr);
                    if (module == null)
                    {
                        module = new Module();
                        module.ModuleName = "Drivers & Passengers --> Team";
                        module.Description = "Drivers & Passengers --> Team";
                        module.Command = "CmdTeam";
                        module.CreatedDate = DateTime.Now;
                        module.CreatedBy = usrInst.Username;
                        module.UpdatedDate = DateTime.Now;
                        module.UpdatedBy = usrInst.Username;
                        moduleService.SaveModules(module, null, sConnStr);
                    }

                    Globals.ValidateAccessProfilesModules(sConnStr);
                    InsertDBUpdate(strUpd, "Security Config");
                }
                #endregion
                #endregion

                #region Update 113. Heartbeat table
                strUpd = "Rez000113";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"drop table GFI_SYS_HeartBeat";
                    if (GetDataDT(ExistTableSql("GFI_SYS_HeartBeat")).Rows[0][0].ToString() != "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE TABLE GFI_GPS_HeartBeat
						(
							AssetID int NOT NULL,
							DateTimeGPS_UTC Datetime NULL,
							DateTimeServer Datetime NULL,
							Longitude float NULL,
							Latitude float NULL,
							CONSTRAINT PK_GFI_GPS_HeartBeat PRIMARY KEY (AssetID)
						)";
                    CreateTable(sql, "GFI_GPS_HeartBeat", "AssetID");

                    GlobalParam param = new GlobalParam();
                    param = globalParamsService.GetGlobalParamsByName("TickerMessage", sConnStr);
                    if (param == null)
                    {
                        param = new GlobalParam();
                        param.ParamComments = "TickerMessage";
                        param.ParamName = "TickerMessage";
                        param.PValue = "*** Welcome to Naveo ***";
                        globalParamsService.SaveGlobalParams(param, true, sConnStr);
                    }
                    InsertDBUpdate(strUpd, "Heartbeat table");
                }
                #endregion
                #region Update 114. Exceptions DateTimeGPS_UTC
                strUpd = "Rez000114";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"alter table GFI_GPS_Exceptions add DateTimeGPS_UTC timestamp(3) default SYS_EXTRACT_UTC(SYSTIMESTAMP) not null";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_Exceptions", "DateTimeGPS_UTC")).Rows[0][0].ToString() == "0")
                    {
                        dbExecuteWithoutTransaction(sql, 4000);

                        sql = @"update GFI_GPS_Exceptions 
									set DateTimeGPS_UTC = ( select g.DateTimeGPS_UTC
								from GFI_GPS_Exceptions t1
									inner join GFI_GPS_GPSData g on g.UID = t1.GPSDataID)";
                        dbExecuteWithoutTransaction(sql, 4000);
                    }

                    sql = @"alter table GFI_ARC_Exceptions add DateTimeGPS_UTC timestamp(3) default SYS_EXTRACT_UTC(SYSTIMESTAMP) not null";
                    if (GetDataDT(ExistColumnSql("GFI_ARC_Exceptions", "DateTimeGPS_UTC")).Rows[0][0].ToString() == "0")
                    {
                        dbExecuteWithoutTransaction(sql, 4000);

                        sql = @"update GFI_ARC_Exceptions 
									set DateTimeGPS_UTC = ( select g.DateTimeGPS_UTC
								from GFI_ARC_Exceptions t1
									inner join GFI_ARC_GPSData g on g.UID = t1.GPSDataID)";
                        dbExecuteWithoutTransaction(sql, 4000);
                    }

                    //sql = "Alter table GFI_SYS_GlobalParam alter column PValue nvarchar2(100)";
                    //_SqlConn.ScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "Exceptions DateTimeGPS_UTC");
                }
                #endregion
                #region Update 115. GFI_FLT_ZoneHeader isMarker n iBuffer
                strUpd = "Rez000115";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_FLT_ZoneHeader add isMarker int DEFAULT ((0))";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ZoneHeader", "isMarker")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_FLT_ZoneHeader add iBuffer int DEFAULT ((0))";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ZoneHeader", "iBuffer")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "GFI_FLT_ZoneHeader isMarker n iBuffer");
                }
                #endregion
                #region Update 116. sMisc Col in GFI_FLT_AutoReportingConfigDetails
                strUpd = "Rez000116";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_FLT_AutoReportingConfigDetails add sMisc nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_AutoReportingConfigDetails", "sMisc")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "sMisc Col in GFI_FLT_AutoReportingConfigDetails");
                }
                #endregion
                #region Update 117. TimeZone Table
                strUpd = "Rez000117";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"
                            CREATE TABLE GFI_SYS_TimeZones 
                            (
	                            TimeZoneID number(10) NOT NULL,
	                            DisplayName NVARCHAR2(100) NOT NULL,
	                            StandardName NVARCHAR2(100) NOT NULL,
	                            HasDST number(10) NOT NULL,
	                            UTCOffset NVARCHAR2(100) NOT NULL,
	                            CONSTRAINT PK_GFI_SYS_TimeZones PRIMARY KEY (TimeZoneID)
                            )";
                    //if (GetDataDT(ExistTableSql("GFI_SYS_TimeZones")).Rows.Count == 0)
                    //    _SqlConn.ScriptExecute(sql, myStrConn);

                    CreateTable(sql, "GFI_SYS_TimeZones", "TimeZoneID");

                    foreach (TimeZoneInfo z in TimeZoneInfo.GetSystemTimeZones())
                    {
                        if (z.SupportsDaylightSavingTime)
                            sql = ("INSERT INTO GFI_SYS_TimeZones (DisplayName, StandardName, HasDST, UTCOffset) VALUES ('" + z.DisplayName.Replace("'", "''") + "', '" + z.StandardName.Replace("'", "''") + "', 1, '" + z.BaseUtcOffset + "')");
                        else
                            sql = ("INSERT INTO GFI_SYS_TimeZones (DisplayName, StandardName, HasDST, UTCOffset) VALUES ('" + z.DisplayName.Replace("'", "''") + "', '" + z.StandardName.Replace("'", "''") + "', 0, '" + z.BaseUtcOffset + "')");

                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    }

                    sql = @"CREATE UNIQUE  INDEX IX_GFI_SYS_TimeZones ON GFI_SYS_TimeZones (StandardName)";
                    if (GetDataDT(ExistsSQLConstraint("IX_GFI_SYS_TimeZones", "GFI_SYS_TimeZones")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "GFI_SYS_TimeZones");
                }
                #endregion
                #region Update 118. GFI_SYS_Notification Index
                strUpd = "Rez000118";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "drop INDEX MyIdxGet_GFI_SYS_Notification";
                    if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "MyIdxGet_GFI_SYS_Notification")).Rows[0][0].ToString() != "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX MyIdxGet_GFI_SYS_Notification
							ON GFI_SYS_Notification (UID,Status,ReportID)";
                    if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "MyIdxGet_GFI_SYS_Notification")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "GFI_SYS_Notification Index");
                }
                #endregion
                #region Update 119. TimeZoneID.
                strUpd = "Rez000119";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "update GFI_FLT_Asset set TimeZoneID = 'Arabian Standard Time' where TimeZoneID = ''";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_User ADD MaxDayMail int Default 25";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "MaxDayMail")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "TimeZoneID, MaxDayMail");
                }
                #endregion
                #region Update 120. MOLG BCM_SEQ
                strUpd = "Rez000120";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_AMM_ExtZonePropDetails add BCM_SEQ int Default -1";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_ExtZonePropDetails", "BCM_SEQ")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_AMM_ExtZonePropAssessment add ASSESS_SEQ int Default -1";
                    if (GetDataDT(ExistColumnSql("GFI_AMM_ExtZonePropAssessment", "ASSESS_SEQ")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "BCM_SEQ");
                }
                #endregion
                #region Update 121. GFI_AMM_ExtZonePropAssessment ASSESS_Status
                strUpd = "Rez000121";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "alter table GFI_AMM_ExtZonePropAssessment modify ASSESS_Status nvarchar2(15)";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "GFI_AMM_ExtZonePropAssessmentStatus");
                }
                #endregion

                #region Update 95. GFI_GPS_TripHeader ScoreCard
                strUpd = "Rez000095";
                if (!CheckUpdateExist(strUpd))
                {
                    //GFI_GPS_TripHeader
                    sql = @"ALTER TABLE GFI_GPS_TripHeader ADD OverSpeed1Time float Default 0";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "OverSpeed1Time")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_GPS_TripHeader ADD OverSpeed2Time float Default 0";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "OverSpeed2Time")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_GPS_TripHeader ADD Accel int Default 0";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "Accel")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_GPS_TripHeader ADD Brake int Default 0";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "Brake")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_GPS_TripHeader ADD Corner int Default 0";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "Corner")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "TripHeader ScoreCard");
                }
                #endregion
                #region Update 96. GFI_SYS_Variables
                strUpd = "Rez000096";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"
                        CREATE TABLE GFI_SYS_Variables
						(
							iID number(10) NOT NULL,
							sName nvarchar(50) NOT NULL,
							sDesc nvarchar(100) NOT NULL,
							iValue number(10) NULL,
							fValue float NULL,
							sValue nvarchar(100) NULL,
							dtValue Datetime NULL,
							CONSTRAINT PK_GFI_SYS_Variables PRIMARY KEY (iID)
						)
                        ";
                    //if (GetDataDT(ExistTableSql("GFI_SYS_Variables")).Rows[0][0].ToString() == "0")
                    //    _SqlConn.ScriptExecute(sql, myStrConn);

                    CreateTable(sql, "GFI_SYS_Variables", "iID");

                    sql = @"insert into GFI_SYS_Variables  (sName, sDesc, dtValue)
								Select 'SendFuelToIBM', 'Last Fuel Sent To IBM', GetUTCDate() from dual where not exists (select sName from GFI_SYS_Variables where sName = 'SendFuelToIBM')";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "GFI_SYS_Variables");
                }
                #endregion

                #region Update 99. Trip Report index
                strUpd = "Rez000099";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "drop index IX_Trip";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "IX_Trip")).Rows.Count > 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX IX_Trip ON GFI_GPS_TripHeader (AssetID, dtStart,dtEnd)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "IX_Trip")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "Trip Report index");
                }
                #endregion
                #region Update 100. GFI_GPS_Exceptions Archive Index
                strUpd = "Rez000100";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE  INDEX Ix_Excep_Archive ON GFI_GPS_Exceptions (AssetID,InsertedAt)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_Exceptions", "Ix_Excep_Archive")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "GFI_GPS_Exceptions Archive Index");
                }
                #endregion

                #region Update 102. Security Monitoring
                strUpd = "Rez000102";
                if (!CheckUpdateExist(strUpd))
                {
                    //Assets
                    sql = @"CREATE TABLE GFI_FLT_SecuMonitorAssets
							(
								iID NUMBER(10) NOT NULL,
								DisplayName NVARCHAR2(100) NOT NULL,
								DeviceID NVARCHAR2 (100) NOT NULL,
								TimeZoneID nvarchar(50) NOT NULL,
								PanicID nvarchar(5) Not Null,
								PanicValue nvarchar(5) Not Null,
								CONSTRAINT PK_GFI_FLT_SecuMonitorAssets PRIMARY KEY (iID)
							)";
                    //if (GetDataDT(ExistTableSql("GFI_FLT_SecuMonitorAssets")).Rows[0][0].ToString() == "0")
                    //    dbExecute(sql);

                    CreateTable(sql, "GFI_FLT_SecuMonitorAssets", "iID");

                    sql = @"CREATE UNIQUE  INDEX IX_UK_SecuMonitorAssets_DeviceID ON GFI_FLT_SecuMonitorAssets (DeviceID)";
                    if (GetDataDT(ExistsIndex("GFI_FLT_SecuMonitorAssets", "IX_UK_SecuMonitorAssets_DeviceID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    //Users
                    sql = @"ALTER TABLE GFI_SYS_User ADD UsrTypeID int NOT NULL Default 0";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "UsrTypeID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    sql = @"ALTER TABLE GFI_SYS_User ADD UsrTypeDesc nvarchar2(50) NOT NULL Default 'Std User'";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "UsrTypeDesc")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    //Live
                    sql = @"CREATE TABLE GFI_GPS_SecuMonitorLiveData(
							UID number(10) NOT NULL,
							AssetID number(10) NULL,
							DateTimeGPS_UTC timestamp(3) NULL,
							DateTimeServer timestamp(3) NULL,
							Longitude binary_double NULL,
							Latitude binary_double NULL,
							LongLatValidFlag number(10) NULL,
							Speed binary_double NULL,
							EngineOn number(10) NULL,
							StopFlag number(10) NULL,
							TripDistance binary_double NULL,
							TripTime binary_double NULL,
							WorkHour number(10) NULL,
							DriverID number(10) NOT NULL,
						 CONSTRAINT PK_GFI_GPS_SecuMonitorLiveData PRIMARY KEY (UID)
						)";
                    //if (GetDataDT(ExistTableSql("GFI_GPS_SecuMonitorLiveData")).Rows[0][0].ToString() == "0")
                    //    dbExecute(sql);
                    CreateTable(sql, "GFI_GPS_SecuMonitorLiveData", "UID");


                    sql = @"CREATE TABLE GFI_GPS_SecuMonitorLiveDataDetail(
							GPSDetailID number(19) NOT NULL,
							UID number(10) NULL,
							TypeID nvarchar(30) NULL,
							TypeVaule nvarchar(30) NULL,
							UOM nvarchar(15) NULL,
							Remarks nvarchar(50) NULL,
						 CONSTRAINT PK_GFI_GPS_SecuMonitorLiveDataDetail PRIMARY KEY (GPSDetailID)
						)";
                    //if (GetDataDT(ExistTableSql("GFI_GPS_SecuMonitorLiveDataDetail")).Rows[0][0].ToString() == "0")
                    //    _SqlConn.ScriptExecute(sql, myStrConn);
                    CreateTable(sql, "GFI_GPS_SecuMonitorLiveDataDetail", "GPSDetailID");


                    sql = @"ALTER TABLE GFI_GPS_SecuMonitorLiveDataDetail
							ADD  CONSTRAINT FK_GFI_GPS_SecuMonitorLiveDataDetail_GFI_GPS_SecuMonitorLiveData
								FOREIGN KEY(UID)
							REFERENCES GFI_GPS_SecuMonitorLiveData (UID)
							ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_SecuMonitorLiveDataDetail_GFI_GPS_SecuMonitorLiveData", "GFI_GPS_SecuMonitorLiveDataDetail")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    sql = @"ALTER TABLE GFI_GPS_SecuMonitorLiveData 
							ADD CONSTRAINT FK_GFI_GPS_SecuMonitorLiveData_GFI_FLT_SecuMonitorAssets 
								FOREIGN KEY(AssetID) 
								REFERENCES GFI_FLT_SecuMonitorAssets(iID) ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_SecuMonitorLiveData_GFI_FLT_SecuMonitorAssets", "GFI_GPS_SecuMonitorLiveData")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    sql = "DROP INDEX myIX_GFI_GPS_SecuMonitorLiveData";
                    if (GetDataDT(ExistsIndex("GFI_GPS_SecuMonitorLiveData", "myIX_GFI_GPS_SecuMonitorLiveData")).Rows[0][0].ToString() != "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX myIX_GFI_GPS_SecuMonitorLiveData ON GFI_GPS_SecuMonitorLiveData
						(
							LongLatValidFlag ASC,
							DateTimeGPS_UTC ASC
						)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_SecuMonitorLiveData", "myIX_GFI_GPS_SecuMonitorLiveData")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "DROP INDEX MyIX_GFI_GPS_SecuMonitorLiveDataDetail";
                    if (GetDataDT(ExistsIndex("GFI_GPS_SecuMonitorLiveDataDetail", "MyIX_GFI_GPS_SecuMonitorLiveDataDetail")).Rows[0][0].ToString() != "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE  INDEX MyIX_GFI_GPS_SecuMonitorLiveDataDetail
							ON GFI_GPS_SecuMonitorLiveDataDetail (UID)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_SecuMonitorLiveDataDetail", "MyIX_GFI_GPS_SecuMonitorLiveDataDetail")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);


                    //LiveToShow
                    sql = @"CREATE TABLE GFI_FLT_SecuMonitorToBSeen 
							(
								AssetID int NOT NULL,
								CONSTRAINT PK_GFI_FLT_SecuMonitorToBSeen PRIMARY KEY  (AssetID)
							)";
                    //if (GetDataDT(ExistTableSql("GFI_FLT_SecuMonitorToBSeen")).Rows[0][0].ToString() == "0")
                    //    dbExecute(sql);
                    CreateTable(sql, "GFI_FLT_SecuMonitorToBSeen", "AssetID");

                    //Admin User
                    sql = "update GFI_SYS_User set UsrTypeID = 1, UsrTypeDesc = 'Secu Monitor' where UID = 2";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);


                    InsertDBUpdate(strUpd, "Security Monitoring");
                }
                #endregion

                #region Update 122. SP Maint updated
                strUpd = "Rez000122";
                if (!CheckUpdateExist(strUpd))

                {
                    //sp_AMM_VehicleMaint_ProcessRecords deleted
                    InsertDBUpdate(strUpd, "SP Maint updated");
                }
                #endregion
                #region Update 123. Excp Idx and Init Clean Ids after Migration
                strUpd = "Rez000123";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "CREATE  INDEX IX_Posted ON GFI_GPS_Exceptions (Posted)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_Exceptions", "IX_Posted")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"delete from GFI_SYS_INIT where upd_id = 'DIN0000001' and exists (select * from GFI_SYS_INIT where upd_id = 'Rez000001')";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"delete from GFI_SYS_INIT where upd_id = 'DIN000002'  and exists (select * from GFI_SYS_INIT where upd_id = 'Rez000002')";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"delete from GFI_SYS_INIT where upd_id = 'CPR000018'  and exists(select * from GFI_SYS_INIT where upd_id = 'Rez000018')";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"delete from GFI_SYS_INIT where upd_id = 'CPR000019'  and exists(select * from GFI_SYS_INIT where upd_id = 'Rez000019')";

                    sql = @"delete from GFI_SYS_INIT where upd_id = 'DIN000055'  and exists(select * from GFI_SYS_INIT where upd_id = 'Rez000055')";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"delete from GFI_SYS_INIT where upd_id = 'DIN0000101' and exists(select * from GFI_SYS_INIT where upd_id = 'Rez000101')";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"delete from GFI_SYS_INIT where upd_id = 'DIN0000103' and exists(select * from GFI_SYS_INIT where upd_id = 'Rez000103')";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"delete from GFI_SYS_INIT where upd_id = 'DIN0000104' and exists(select * from GFI_SYS_INIT where upd_id = 'Rez000104')";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"delete from GFI_SYS_INIT where upd_id = 'DIN0000105' and exists(select * from GFI_SYS_INIT where upd_id = 'Rez000105')";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"delete from GFI_SYS_INIT where upd_id = 'DIN0000106' and exists(select * from GFI_SYS_INIT where upd_id = 'Rez000106')";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"delete from GFI_SYS_INIT where upd_id = 'DIN0000107' and exists(select * from GFI_SYS_INIT where upd_id = 'Rez000107')";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"delete from GFI_SYS_INIT where upd_id = 'Vis0000086' and exists(select * from GFI_SYS_INIT where upd_id = 'Rez000086')";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"delete from GFI_SYS_INIT where upd_id = 'Vis000087'  and exists(select * from GFI_SYS_INIT where upd_id = 'Rez000087')";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"delete from GFI_SYS_INIT where upd_id = 'DIN0000090' and exists(select * from GFI_SYS_INIT where upd_id = 'Rez000090')";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"delete from GFI_SYS_INIT where upd_id = 'DIN0000091' and exists(select * from GFI_SYS_INIT where upd_id = 'Rez000091')";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "Excp Idx and Init Clean Ids after Migration");
                }
                #endregion
                #region Update 124. Missing Indexes
                strUpd = "Rez000124";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "CREATE INDEX IX_GFI_GPS_ProcessPending_ProcessCode_Processing ON GFI_GPS_ProcessPending (ProcessCode, Processing)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_ProcessPending", "IX_GFI_GPS_ProcessPending_ProcessCode_Processing")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_GPS_GPSDataDetail_UID_TypeID ON GFI_GPS_GPSDataDetail (UID, TypeID)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSDataDetail", "IX_GFI_GPS_GPSDataDetail_UID_TypeID")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_SYS_GroupMatrixZone_GMID ON GFI_SYS_GroupMatrixZone (GMID)";
                    if (GetDataDT(ExistsIndex("GFI_SYS_GroupMatrixZone", "IX_GFI_SYS_GroupMatrixZone_GMID")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_GPS_Exceptions_RuleID_GPSDataID ON GFI_GPS_Exceptions (RuleID, GPSDataID)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_Exceptions", "IX_GFI_GPS_Exceptions_RuleID_GPSDataID")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_GPS_Exceptions_RuleID_AssetID ON GFI_GPS_Exceptions (RuleID, AssetID)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_Exceptions", "IX_GFI_GPS_Exceptions_RuleID_AssetID")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    //sql = "CREATE INDEX IX_GFI_SYS_Notification_UID_Status_ReportID ON GFI_SYS_Notification (UID, Status,ReportID)";
                    //if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "IX_GFI_SYS_Notification_UID_Status_ReportID")).Rows[0][0].ToString() == "0")
                    //    dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_SYS_Notification_RuleName_email ON GFI_SYS_Notification (RuleName, email)";
                    if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "IX_GFI_SYS_Notification_RuleName_email")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_SYS_Notification_ParentRuleID ON GFI_SYS_Notification (ParentRuleID)";
                    if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "IX_GFI_SYS_Notification_ParentRuleID")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_SYS_Notification_ARID_Status ON GFI_SYS_Notification (ARID, Status)";
                    if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "IX_GFI_SYS_Notification_ARID_Status")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_GPS_TripHeader_dtStart_dtEnd ON GFI_GPS_TripHeader (dtStart, dtEnd)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "IX_GFI_GPS_TripHeader_dtStart_dtEnd")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_GPS_LiveData_DateTimeGPS_UTC ON GFI_GPS_LiveData (DateTimeGPS_UTC)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_LiveData", "IX_GFI_GPS_LiveData_DateTimeGPS_UTC")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_FLT_ZoneHeadZoneType_ZoneHeadID ON GFI_FLT_ZoneHeadZoneType (ZoneHeadID)";
                    if (GetDataDT(ExistsIndex("GFI_FLT_ZoneHeadZoneType", "IX_GFI_FLT_ZoneHeadZoneType_ZoneHeadID")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_FLT_ZoneHeadZoneType_ZoneTypeID ON GFI_FLT_ZoneHeadZoneType (ZoneTypeID)";
                    if (GetDataDT(ExistsIndex("GFI_FLT_ZoneHeadZoneType", "IX_GFI_FLT_ZoneHeadZoneType_ZoneTypeID")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_FLT_AutoReportingConfig_TargetID_ReportID ON GFI_FLT_AutoReportingConfig (TargetID,ReportID)";
                    if (GetDataDT(ExistsIndex("GFI_FLT_AutoReportingConfig", "IX_GFI_FLT_AutoReportingConfig_TargetID_ReportID")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_ARC_GPSData_AssetID ON GFI_ARC_GPSData (AssetID)";
                    if (GetDataDT(ExistsIndex("GFI_ARC_GPSData", "IX_GFI_ARC_GPSData_AssetID")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    //sql = "CREATE INDEX IX_GFI_ARC_GPSData_DateTimeGPS_UTC ON GFI_ARC_GPSData (DateTimeGPS_UTC)";
                    //if (GetDataDT(ExistsIndex("GFI_ARC_GPSData", "IX_GFI_ARC_GPSData_DateTimeGPS_UTC")).Rows[0][0].ToString() == "0")
                    //    dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_ARC_Exceptions_RuleID ON GFI_ARC_Exceptions (RuleID)";
                    if (GetDataDT(ExistsIndex("GFI_ARC_Exceptions", "IX_GFI_ARC_Exceptions_RuleID")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "Missing Indexes");
                }
                #endregion
                #region Update 125. Delete wrong rules and auto report
                strUpd = "Rez000125";
                if (!CheckUpdateExist(strUpd))
                {
                    InsertDBUpdate(strUpd, "Delete wrong rules and auto report");
                }
                #endregion

                #region Update 126. Notification Table
                strUpd = "Rez000126";
                if (!CheckUpdateExist(strUpd))
                {
                    //AssetID
                    sql = @"ALTER TABLE GFI_SYS_Notification ADD AssetID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "AssetID")).Rows[0][0].ToString() == "0")
                    {
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                        sql = "DROP INDEX MyIdxGet_GFI_SYS_Notification";
                        if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "MyIdxGet_GFI_SYS_Notification")).Rows[0][0].ToString() == "1")
                            _SqlConn.OracleScriptExecute(sql, myStrConn);


                        sql = @"DROP INDEX IX_GFI_SYS_Notification_ARID_Status";
                        if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "IX_GFI_SYS_Notification_ARID_Status")).Rows[0][0].ToString() == "1")
                            _SqlConn.OracleScriptExecute(sql, myStrConn);


                        sql = "DROP INDEX IX_GFI_SYS_Notification_ParentRuleID";
                        if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "IX_GFI_SYS_Notification_ParentRuleID")).Rows[0][0].ToString() == "1")
                            _SqlConn.OracleScriptExecute(sql, myStrConn);


                        sql = "DROP INDEX IX_GFI_SYS_Notification_RuleName_email";
                        if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "IX_GFI_SYS_Notification_RuleName_email")).Rows[0][0].ToString() == "1")
                            _SqlConn.OracleScriptExecute(sql, myStrConn);


                        //sql = @"alter table GFI_SYS_Notification drop column Asset";
                        //_SqlConn.ScriptExecute(sql, myStrConn);


                        sql = @"ALTER TABLE GFI_SYS_Notification 
									ADD CONSTRAINT FK_GFI_SYS_Notification_GFI_FLT_Asset 
										FOREIGN KEY(AssetID) 
										REFERENCES GFI_FLT_Asset(AssetID)";
                        if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_Notification_GFI_FLT_Asset", "GFI_SYS_Notification")).Rows.Count == 0)
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                    }

                    sql = @"alter table GFI_SYS_Notification drop column RuleName";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "RuleName")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    sql = @"alter table GFI_SYS_Notification drop column Email";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "Email")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    sql = @"alter table GFI_SYS_Notification drop column Name";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "Name")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    sql = @"alter table GFI_SYS_Notification drop column Lat";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "Lat")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    //sql = @"alter table GFI_SYS_Notification drop column Lon";
                    //if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "Lon")).Rows[0][0].ToString() == "0")
                    //    _SqlConn.ScriptExecute(sql, myStrConn);

                    //DriverID
                    sql = @"ALTER TABLE GFI_SYS_Notification ADD DriverID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "DriverID")).Rows[0][0].ToString() == "0")
                    {
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                        sql = @"ALTER TABLE GFI_SYS_Notification 
									ADD CONSTRAINT FK_GFI_SYS_Notification_GFI_FLT_Driver 
										FOREIGN KEY(DriverID) 
										REFERENCES GFI_FLT_Driver(DriverID)";
                        if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_Notification_GFI_FLT_Driver", "GFI_SYS_Notification")).Rows.Count == 0)
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                    }

                    sql = @"ALTER TABLE GFI_SYS_Notification ADD sBody nvarchar2(100) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "sBody")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    sql = @"ALTER TABLE GFI_SYS_Notification ADD Type nvarchar2(15) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "Type")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    sql = "update GFI_SYS_Notification set Type = 'AutoR' where ExceptionID = 0 and Type is null";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);


                    sql = "update GFI_SYS_Notification set Type = 'Rule' where ExceptionID > 0 and Type is null";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);


                    sql = @"ALTER TABLE GFI_SYS_Notification ADD Title nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "Title")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    sql = "update GFI_SYS_Notification set UID = null where UID = 0";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);


                    sql = @"ALTER TABLE GFI_SYS_Notification 
									ADD CONSTRAINT FK_GFI_SYS_Notification_GFI_SYS_User 
										FOREIGN KEY(UID) 
										REFERENCES GFI_SYS_User(UID)  
								 ON DELETE  Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_Notification_GFI_SYS_User", "GFI_SYS_Notification")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    sql = @"ALTER TABLE GFI_SYS_Notification 
									ADD CONSTRAINT FK_GFI_SYS_Notification_GFI_GPS_GPSData 
										FOREIGN KEY(GPSDataUID) 
										REFERENCES GFI_GPS_GPSData(UID)  
								 ON DELETE  Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_Notification_GFI_GPS_GPSData", "GFI_SYS_Notification")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    sql = @"ALTER TABLE GFI_SYS_Notification 
									ADD CONSTRAINT FK_GFI_SYS_Notification_GFI_GPS_Exceptions
										FOREIGN KEY(ExceptionID) 
										REFERENCES GFI_GPS_Exceptions(iID) ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_Notification_GFI_GPS_Exceptions", "GFI_SYS_Notification")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"alter table GFI_SYS_Notification add VirtualEmail nvarchar2(100) null";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_Notification", "VirtualEmail")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    InsertDBUpdate(strUpd, "Notification Table");
                }
                #endregion

                #region Update 128. CreatedBy UpdatedBy int for Assets
                strUpd = "Rez000128";
                if (!CheckUpdateExist(strUpd))
                {
                    #region Assets
                    dt = GetDataDT(GetFieldSchema("GFI_FLT_Asset", "CreatedBy"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                    {
                        sql = @"Alter table GFI_FLT_Asset add cb nvarchar2(100)";
                        if (GetDataDT(ExistColumnSql("GFI_FLT_Asset", "cb")).Rows[0][0].ToString() == "0")
                        {
                            _SqlConn.OracleScriptExecute(sql, myStrConn);


                            sql = @"update GFI_FLT_Asset set cb = CreatedBy";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                            sql = @"update GFI_FLT_Asset set CreatedBy = NULL";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                            sql = @"ALTER TABLE GFI_FLT_Asset MODIFY (CreatedBy INT)";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                            sql = @"update GFI_FLT_Asset a
                                    set CreatedBy = (select UID FROM GFI_SYS_User u WHERE u.Email = a.cb)";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                            sql = @"update GFI_FLT_Asset a 
                                    set CreatedBy = (select UID FROM GFI_SYS_User u WHERE u.UserName = a.cb)
                                    where a.CreatedBy = NULL";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                            sql = @" ALTER TABLE GFI_FLT_Asset DROP COLUMN cb";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                        }
                    }

                    dt = GetDataDT(GetFieldSchema("GFI_FLT_Asset", "UpdatedBy"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                    {
                        sql = @"Alter table GFI_FLT_Asset add ub nvarchar2(100)";
                        if (GetDataDT(ExistColumnSql("GFI_FLT_Asset", "ub")).Rows[0][0].ToString() == "0")
                        {
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                            sql = @"update GFI_FLT_Asset set ub = UpdatedBy";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                            sql = @"update GFI_FLT_Asset set UpdatedBy = null";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                            sql = @"ALTER TABLE GFI_FLT_Asset MODIFY (UpdatedBy INT)";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                            sql = @"update GFI_FLT_Asset a
                                    set UpdatedBy = (select UID FROM GFI_SYS_User u WHERE u.Email = a.ub)";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                            sql = @"update GFI_FLT_Asset a 
                                    set UpdatedBy = (select UID FROM GFI_SYS_User u WHERE u.UserName = a.ub)
                                    where a.UpdatedBy = NULL";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                            sql = @" ALTER TABLE GFI_FLT_Asset DROP COLUMN ub";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                        }
                    }

                    sql = @"ALTER TABLE GFI_FLT_Asset 
							ADD CONSTRAINT FK_GFI_FLT_Asset_GFI_SYS_User_CB 
								FOREIGN KEY(CreatedBy) 
								REFERENCES GFI_SYS_User(UID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_Asset_GFI_SYS_User_CB", "GFI_FLT_Asset")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);



                    sql = @"Alter table GFI_FLT_Asset MODIFY UPDATEDBY NUMBER(38,0)";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);


                    sql = @"ALTER TABLE GFI_FLT_Asset 
							ADD CONSTRAINT FK_GFI_FLT_Asset_GFI_SYS_User_UB 
								FOREIGN KEY(UpdatedBy) 
								REFERENCES GFI_SYS_User(UID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_Asset_GFI_SYS_User_UB", "GFI_FLT_Asset")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    #endregion

                    #region AssetDeviceMap
                    dt = GetDataDT(GetFieldSchema("GFI_FLT_AssetDeviceMap", "CreatedBy"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                    {
                        sql = @"Alter table GFI_FLT_AssetDeviceMap add cb nvarchar2(100)";
                        if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "cb")).Rows[0][0].ToString() == "0")
                        {
                            _SqlConn.OracleScriptExecute(sql, myStrConn);


                            sql = @"update GFI_FLT_AssetDeviceMap set cb = CreatedBy";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                            sql = @"update GFI_FLT_AssetDeviceMap set CreatedBy = null";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                            sql = @"ALTER TABLE GFI_FLT_AssetDeviceMap MODIFY (CreatedBy INT)";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                            sql = @"update GFI_FLT_AssetDeviceMap a
                                    set CreatedBy = (select UID FROM GFI_SYS_User u WHERE u.Email = a.cb)";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                            sql = @"update GFI_FLT_AssetDeviceMap a 
                                    set CreatedBy = (select UID FROM GFI_SYS_User u WHERE u.UserName = a.cb)
                                    where a.CreatedBy = NULL";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                            sql = @" ALTER TABLE GFI_FLT_AssetDeviceMap DROP COLUMN cb";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                        }
                    }

                    dt = GetDataDT(GetFieldSchema("GFI_FLT_AssetDeviceMap", "UpdatedBy"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                    {
                        sql = @"Alter table GFI_FLT_AssetDeviceMap add ub nvarchar2(100)";
                        if (GetDataDT(ExistColumnSql("GFI_FLT_AssetDeviceMap", "ub")).Rows[0][0].ToString() == "0")
                        {
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                            sql = @"update GFI_FLT_AssetDeviceMap set ub = UpdatedBy";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                            sql = @"update GFI_FLT_AssetDeviceMap set UpdatedBy = null";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                            sql = @"ALTER TABLE GFI_FLT_AssetDeviceMap MODIFY (UpdatedBy INT)";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                            sql = @"update GFI_FLT_AssetDeviceMap a
                                    set UpdatedBy = (select UID FROM GFI_SYS_User u WHERE u.Email = a.ub)";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                            sql = @"update GFI_FLT_AssetDeviceMap a 
                                    set UpdatedBy = (select UID FROM GFI_SYS_User u WHERE u.UserName = a.ub)
                                    where a.UpdatedBy = NULL";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                            sql = @" ALTER TABLE GFI_FLT_AssetDeviceMap DROP COLUMN ub";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);
                        }
                    }


                    sql = @"Alter table GFI_FLT_AssetDeviceMap MODIFY CreatedBy NUMBER(38,0)";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_AssetDeviceMap 
							ADD CONSTRAINT FK_GFI_FLT_AssetDeviceMap_GFI_SYS_User_CB 
								FOREIGN KEY(CreatedBy) 
								REFERENCES GFI_SYS_User(UID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_AssetDeviceMap_GFI_SYS_User_CB", "GFI_FLT_AssetDeviceMap")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    sql = @"Alter table GFI_FLT_AssetDeviceMap MODIFY UpdatedBy NUMBER(38,0)";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_AssetDeviceMap 
							ADD CONSTRAINT FK_GFI_FLT_AssetDeviceMap_GFI_SYS_User_UB 
								FOREIGN KEY(UpdatedBy) 
								REFERENCES GFI_SYS_User(UID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_AssetDeviceMap_GFI_SYS_User_UB", "GFI_FLT_AssetDeviceMap")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    #endregion

                    #region all others
                    List<String> lTableN = new List<String>();
                    lTableN.Add("GFI_FLT_AssetTypeMapping");
                    lTableN.Add("GFI_FLT_AssetDataType");
                    lTableN.Add("GFI_FLT_ExtTripInfoType");
                    lTableN.Add("GFI_FLT_ExtTripInfoMapping");
                    lTableN.Add("GFI_FLT_ExtTripInfo");
                    lTableN.Add("GFI_SYS_LookUp");
                    lTableN.Add("GFI_AMM_MaintenanceTrip");
                    lTableN.Add("GFI_FLT_Asset");
                    lTableN.Add("GFI_SYS_Ticker");
                    lTableN.Add("GFI_FLT_AssetDeviceMap");
                    lTableN.Add("GFI_AMM_AssetZoneMap");
                    lTableN.Add("GFI_FLT_AuxilliaryType");
                    //lTableN.Add("GFI_SYS_User");
                    lTableN.Add("GFI_HRM_Team");
                    lTableN.Add("GFI_AMM_ExtZonePropAssessment");
                    //lTableN.Add("TempCostItems");
                    //lTableN.Add("ReportTemplates");
                    lTableN.Add("GFI_PLA_DataSnapshot");
                    lTableN.Add("GFI_AMM_VehicleMaintenance");
                    lTableN.Add("GFI_FLT_Driver");
                    lTableN.Add("GFI_SYS_NaaAddress");
                    lTableN.Add("GFI_HRM_EmpBase");
                    lTableN.Add("GFI_FLT_AutoReportingConfig");
                    //lTableN.Add("Contractors");
                    //lTableN.Add("AspNetUsers");
                    //lTableN.Add("ContractorVehicles");
                    //lTableN.Add("Vehicles");
                    //lTableN.Add("Contracts");
                    lTableN.Add("GFI_SYS_Modules");
                    lTableN.Add("GFI_SYS_AccessTemplates");
                    lTableN.Add("GFI_SYS_AccessProfiles");
                    //lTableN.Add("AspNetAccessTemplates");
                    //lTableN.Add("AspNetAccessTemplateDetails");
                    lTableN.Add("GFI_AMM_VehicleMaintCat");
                    lTableN.Add("GFI_AMM_AssetExtProFields");
                    lTableN.Add("GFI_AMM_InsCoverTypes");
                    lTableN.Add("GFI_AMM_VehicleTypes");
                    lTableN.Add("GFI_AMM_VehicleMaintStatus");
                    lTableN.Add("GFI_AMM_VehicleMaintTypes");
                    lTableN.Add("GFI_AMM_AssetExtProXT");
                    lTableN.Add("GFI_AMM_AssetExtProVehicles");
                    lTableN.Add("GFI_FLT_DeviceAuxilliaryMap");
                    lTableN.Add("GFI_AMM_VehicleMaintTypesLink");
                    //lTableN.Add("TransportRequestContracts");
                    //lTableN.Add("DriverVehicleMappings");
                    //lTableN.Add("LookupTypes");
                    //lTableN.Add("RosterDriverMappingHistory");
                    //lTableN.Add("LookupValues");
                    lTableN.Add("GFI_FLT_CalibrationChart");
                    //lTableN.Add("Officers");
                    //lTableN.Add("ResponsibleOfficers");
                    //lTableN.Add("TransportRequestDetails");
                    //lTableN.Add("TransportRequests");
                    lTableN.Add("GFI_AMM_VehicleMaintItems");
                    //lTableN.Add("RosterDriverMappings");
                    //lTableN.Add("Rosters");

                    if (String.IsNullOrEmpty("ABC"))
                        foreach (String s in lTableN)
                            try
                            {
                                dt = GetDataDT(GetFieldSchema(s, "CreatedBy"));
                                if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                                {
                                    sql = @"Alter table " + s + @" add cb nvarchar(100)";
                                    if (GetDataDT(ExistColumnSql(s, "cb")).Rows[0][0].ToString() == "0")
                                    {
                                        dbExecute(sql);

                                        sql = @"update " + s + @" set cb = CreatedBy
												Alter table " + s + @" Alter Column CreatedBy nvarchar(100) null
												update " + s + @" set CreatedBy = null
												ALTER TABLE " + s + @" ALTER COLUMN CreatedBy int
												update " + s + @"
													set CreatedBy = u.UID
												from " + s + @" a
													inner join GFI_SYS_User u on u.Email = a.cb

												update " + s + @"
													set CreatedBy = u.UID
												from " + s + @" a
													inner join GFI_SYS_User u on u.UserName = a.cb
												where a.CreatedBy is null

												alter table " + s + @" drop column cb";
                                        dbExecute(sql);
                                    }
                                }

                                dt = GetDataDT(GetFieldSchema(s, "UpdatedBy"));
                                if (dt.Rows.Count > 0)
                                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                                    {
                                        sql = @"Alter table " + s + @" add ub nvarchar(100)";
                                        if (GetDataDT(ExistColumnSql(s, "ub")).Rows[0][0].ToString() == "0")
                                        {
                                            dbExecute(sql);

                                            sql = @"update " + s + @" set ub = UpdatedBy
													Alter table " + s + @" Alter Column UpdatedBy nvarchar(100) null
													update " + s + @" set UpdatedBy = null
													ALTER TABLE " + s + @" ALTER COLUMN UpdatedBy int
													update " + s + @"
														set UpdatedBy = u.UID
													from " + s + @" a
														inner join GFI_SYS_User u on u.Email = a.ub

													update " + s + @"
														set UpdatedBy = u.UID
													from " + s + @" a
														inner join GFI_SYS_User u on u.UserName = a.ub
													where a.UpdatedBy is null

													alter table " + s + @" drop column ub";
                                            dbExecute(sql);
                                        }
                                    }

                                sql = @"ALTER TABLE " + s + @" 
										ADD CONSTRAINT FK_" + s + @"_GFI_SYS_User_CB 
											FOREIGN KEY(CreatedBy) 
											REFERENCES GFI_SYS_User(UID) 
									 ON UPDATE  NO ACTION 
									 ON DELETE  NO ACTION ";
                                if (GetDataDT(ExistsSQLConstraint("FK_" + s + @"_GFI_SYS_User_CB", s)).Rows.Count == 0)
                                    dbExecute(sql);

                                if (dt.Rows.Count > 0)
                                {
                                    sql = @"ALTER TABLE " + s + @" 
										ADD CONSTRAINT FK_" + s + @"_GFI_SYS_User_UB 
											FOREIGN KEY(UpdatedBy) 
											REFERENCES GFI_SYS_User(UID) 
									 ON UPDATE  NO ACTION 
									 ON DELETE  NO ACTION ";
                                    if (GetDataDT(ExistsSQLConstraint("FK_" + s + @"_GFI_SYS_User_UB", s)).Rows.Count == 0)
                                        dbExecute(sql);
                                }
                            }
                            catch { }
                    #endregion

                    InsertDBUpdate(strUpd, "CreatedBy UpdatedBy int for Assets");
                }
                #endregion
                #region Update 129. GFI_SYS_SessionNaveo
                strUpd = "Rez000129";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_SYS_SessionNaveo
							(
								Id number(10) NOT NULL,
								Uid number(10) NOT NULL,
								IpAddress nvarchar2(50) NULL,
								MachineName nvarchar2(100) NULL,
								Type nvarchar2(15) NOT NULL,
								InsertedDate timestamp(3) Default SYS_EXTRACT_UTC(SYSTIMESTAMP) NOT NULL ,
								ExpiryDate timestamp(3) NULL,
								Token nvarchar2(100) NOT NULL,
							 CONSTRAINT PK_GFI_SYS_SessionNaveo PRIMARY KEY  (Token)
							)";
                    if (GetDataDT(ExistTableSql("GFI_SYS_SessionNaveo")).Rows[0][0].ToString() == "0")
                        CreateTable(sql, "GFI_SYS_SessionNaveo", "Token");



                    GlobalParam g = new GlobalParam();
                    g = globalParamsService.GetGlobalParamsByName("MultiSession", sConnStr);
                    if (g == null)
                    {
                        g = new GlobalParam();
                        g.ParamName = "MultiSession";
                        g.PValue = "Yes";
                        g.ParamComments = "Allow Multiple login for same user";
                        globalParamsService.SaveGlobalParams(g, true, sConnStr);
                    }
                    InsertDBUpdate(strUpd, "GFI_SYS_SessionNaveo");
                }
                #endregion
                #region Update 130. Archived Audit Tables
                strUpd = "Rez000130";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_ARC_Audit
							(
								auditUser nvarchar2(100) NULL,
								auditDate timestamp(3) NULL,
								auditType nvarchar2(2) NULL,
								ReferenceCode nvarchar2(20) NULL,
								ReferenceDesc nvarchar2(50) NULL,
								ReferenceTable nvarchar2(20) NULL,
								CreatedDate timestamp(3) NULL,
								CallerFunction nvarchar2(20) NULL,
								SQLRemarks nvarchar2(200) NULL,
								NewValue nvarchar2(200) NULL,
								OldValue nvarchar2(200) NULL,
								PostedDate timestamp(3) NULL,
								PostedFlag nvarchar2(1) NULL,
								UserID number(10) DEFAULT ((1)) NOT NULL, 
								AuditId number(10) NOT NULL
								, CONSTRAINT PK_GFI_ARC_Audit PRIMARY KEY (AuditId)
                            )";
                    if (GetDataDT(ExistTableSql("GFI_ARC_Audit")).Rows[0][0].ToString() == "0")
                        CreateTable(sql, "GFI_ARC_Audit", String.Empty);

                    sql = @"ALTER TABLE GFI_ARC_Audit 
								ADD CONSTRAINT FK_GFI_ARC_Audit_GFI_SYS_User 
									FOREIGN KEY(UserID) 
									REFERENCES GFI_SYS_User(UID) ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_ARC_Audit_GFI_SYS_User", "GFI_ARC_Audit")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "Archived Audit Tables");
                }
                #endregion
                #region Update 131. User Table fields
                strUpd = "Rez000131";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_SYS_User MODIFY Tel nvarchar2(15)";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);
                    sql = "ALTER TABLE GFI_SYS_User MODIFY MobileNo nvarchar2(15)";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);
                    sql = "ALTER TABLE GFI_SYS_User MODIFY UserName nvarchar2(100)";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "User Table fields");
                }
                #endregion
                #region Update 132. CreatedBy UpdatedBy int for User and Assets related
                strUpd = "Rez000132";
                if (!CheckUpdateExist(strUpd))
                {
                    List<String> lTableN = new List<String>();
                    lTableN.Add("GFI_AMM_AssetExtProVehicles");
                    lTableN.Add("GFI_FLT_AssetDeviceMap");
                    lTableN.Add("GFI_FLT_DeviceAuxilliaryMap");
                    lTableN.Add("GFI_FLT_CalibrationChart");

                    foreach (String s in lTableN)
                    {
                        dt = GetDataDT(GetFieldSchema(s, "CreatedBy"));
                        if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                        {
                            sql = @"Alter table " + s + @" add cb nvarchar2(100)";
                            if (GetDataDT(ExistColumnSql(s, "cb")).Rows[0][0].ToString() == "0")
                            {
                                _SqlConn.OracleScriptExecute(sql, myStrConn);

                                //alter table " + s + @" drop column cb";
                                sql = @"update " + s + @" set cb = CreatedBy";
                                _SqlConn.OracleScriptExecute(sql, myStrConn);
                                //sql = @"Alter table " + s + @" MODIFY (CreatedBy nvarchar2(100))";
                                //_SqlConn.ScriptExecute(sql, myStrConn);
                                sql = @"update " + s + @" set CreatedBy = null";
                                _SqlConn.OracleScriptExecute(sql, myStrConn);
                                sql = @"ALTER TABLE " + s + @" MODIFY ( CreatedBy int )";
                                _SqlConn.OracleScriptExecute(sql, myStrConn);

                                sql = @"update " + s + @" a
                                	  set CreatedBy = (select UID from GFI_SYS_User u where  u.Email = a.cb)";
                                _SqlConn.OracleScriptExecute(sql, myStrConn);

                                sql = @"update " + s + @" a
                                      set CreatedBy = (select UID from GFI_SYS_User u where  u.UserName = a.cb)
                                      where a.CreatedBy is null";
                                _SqlConn.OracleScriptExecute(sql, myStrConn);

                                sql = @"alter table " + s + @" drop column cb";
                                _SqlConn.OracleScriptExecute(sql, myStrConn);



                            }
                        }

                        dt = GetDataDT(GetFieldSchema(s, "UpdatedBy"));
                        if (dt.Rows.Count > 0)
                            if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                            {
                                sql = @"Alter table " + s + @" add ub nvarchar2(100)";
                                if (GetDataDT(ExistColumnSql(s, "ub")).Rows[0][0].ToString() == "0")
                                {
                                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                                    sql = @"update " + s + @" set ub = UpdatedBy";
                                    _SqlConn.OracleScriptExecute(sql, myStrConn);
                                    //sql = @"Alter table " + s + @" MODIFY (UpdatedBy nvarchar2(100))";
                                    //_SqlConn.ScriptExecute(sql, myStrConn);
                                    sql = @"update " + s + @" set UpdatedBy = null";
                                    _SqlConn.OracleScriptExecute(sql, myStrConn);
                                    sql = @"ALTER TABLE " + s + @" MODIFY(UpdatedBy int)";
                                    _SqlConn.OracleScriptExecute(sql, myStrConn);
                                    sql = @"update " + s + @" a
                                	  set UpdatedBy = (select UID from GFI_SYS_User u where  u.Email = a.ub)";
                                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                                    sql = @"update " + s + @" a
                                      set UpdatedBy = (select UID from GFI_SYS_User u where  u.UserName = a.ub)
                                      where a.UpdatedBy is null";
                                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                                    sql = @"alter table " + s + @" drop column ub";
                                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                                }
                            }

                        sql = @"ALTER TABLE " + s + @" 
										ADD CONSTRAINT FK_" + s + @"_GFI_SYS_User_CB 
											FOREIGN KEY(CreatedBy) 
											REFERENCES GFI_SYS_User(UID)";
                        if (GetDataDT(ExistsSQLConstraint("FK_" + s + @"_GFI_SYS_User_CB", s)).Rows.Count == 0)
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                        if (dt.Rows.Count > 0)
                        {
                            sql = @"ALTER TABLE " + s + @" 
										ADD CONSTRAINT FK_" + s + @"_GFI_SYS_User_UB 
											FOREIGN KEY(UpdatedBy) 
											REFERENCES GFI_SYS_User(UID)";
                            if (GetDataDT(ExistsSQLConstraint("FK_" + s + @"_GFI_SYS_User_UB", s)).Rows.Count == 0)
                                _SqlConn.OracleScriptExecute(sql, myStrConn);
                        }
                    }

                    #region GFI_SYS_User
                    dt = GetDataDT(GetFieldSchema("GFI_SYS_User", "CreatedBy"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                    {
                        sql = @"Alter table GFI_SYS_User add cb nvarchar2(100)";
                        if (GetDataDT(ExistColumnSql("GFI_SYS_User", "cb")).Rows[0][0].ToString() == "0")
                        {
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                            sql = @"update GFI_SYS_User set cb = CreatedBy";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);
                            //sql = @"Alter table GFI_SYS_User MODIFY (CreatedBy nvarchar2(100))";
                            //_SqlConn.ScriptExecute(sql, myStrConn);
                            sql = @"update GFI_SYS_User set CreatedBy = null";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);
                            sql = @"ALTER TABLE GFI_SYS_User MODIFY(CreatedBy int)";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);
                            sql = @"update GFI_SYS_User a
										set CreatedBy = (select UID from GFI_SYS_User u where u.Email = a.cb)";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);
                            sql = @"update GFI_SYS_User a
								    set CreatedBy = (select UID from GFI_SYS_User u where u.UserName = a.cb)
									where a.CreatedBy is null";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);
                            sql = @"alter table GFI_SYS_User drop column cb";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);
                        }
                    }

                    dt = GetDataDT(GetFieldSchema("GFI_SYS_User", "UpdatedBy"));
                    if (dt.Rows.Count > 0)
                        if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                        {
                            sql = @"Alter table GFI_SYS_User add ub nvarchar2(100)";
                            if (GetDataDT(ExistColumnSql("GFI_SYS_User", "ub")).Rows[0][0].ToString() == "0")
                            {
                                _SqlConn.OracleScriptExecute(sql, myStrConn);

                                sql = @"update GFI_SYS_User set ub = UpdatedBy";
                                _SqlConn.OracleScriptExecute(sql, myStrConn);
                                //sql = @"Alter table GFI_SYS_User MODIFY (UpdatedBy nvarchar2(100))";
                                //_SqlConn.ScriptExecute(sql, myStrConn);
                                sql = @"update GFI_SYS_User set UpdatedBy = null";
                                _SqlConn.OracleScriptExecute(sql, myStrConn);
                                sql = @"ALTER TABLE GFI_SYS_User MODIFY(UpdatedBy int)";
                                _SqlConn.OracleScriptExecute(sql, myStrConn);
                                sql = @"update GFI_SYS_User a
										set UpdatedBy = (select UID from GFI_SYS_User u where u.Email = a.ub)";
                                _SqlConn.OracleScriptExecute(sql, myStrConn); //select all there 
                                sql = @"update GFI_SYS_User a
										set UpdatedBy = (select UID from GFI_SYS_User u where u.UserName = a.ub)  
									where a.UpdatedBy is null";
                                _SqlConn.OracleScriptExecute(sql, myStrConn);
                                sql = @"alter table GFI_SYS_User drop column ub";
                                _SqlConn.OracleScriptExecute(sql, myStrConn);
                            }
                        }

                    sql = @"ALTER TABLE GFI_SYS_User 
										ADD CONSTRAINT FK_GFI_SYS_User_GFI_SYS_User_CB 
											FOREIGN KEY(CreatedBy) 
											REFERENCES GFI_SYS_User(UID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_User_GFI_SYS_User_CB", "GFI_SYS_User")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    if (dt.Rows.Count > 0)
                    {
                        sql = @"ALTER TABLE GFI_SYS_User 
										ADD CONSTRAINT FK_GFI_SYS_User_GFI_SYS_User_UB 
											FOREIGN KEY(UpdatedBy) 
											REFERENCES GFI_SYS_User(UID) ";
                        if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_User_GFI_SYS_User_UB", "GFI_SYS_User")).Rows.Count == 0)
                            _SqlConn.OracleScriptExecute(sql, myStrConn);
                    }

                    #endregion
                    InsertDBUpdate(strUpd, "CreatedBy UpdatedBy int for User and Assets related");
                }
                #endregion
                #region Update 133. Releasing Web Version
                strUpd = "Rez000133";
                if (!CheckUpdateExist(strUpd))
                {
                    User u = new User();
                    u = userService.GetUserByeMail("ADMIN@naveo.mu", "SysInitPass", sConnStr, false, true);
                    u.Password = "WebCore@2016";
                    userService.SaveUser(u, false, sConnStr);

                    u = new User();
                    u = userService.GetUserByeMail("Support@naveo.mu", "SysInitPass", sConnStr, false, true);
                    u.Password = "SupportMe@2016";
                    userService.SaveUser(u, false, sConnStr);

                    InsertDBUpdate(strUpd, "Releasing Web Version");
                }
                #endregion
                #region Update 134. GFI_SYS_LoginAudit User
                strUpd = "Rez000134";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_SYS_LoginAudit MODIFY auditUser nvarchar2(50) ";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_SYS_Audit MODIFY auditUser nvarchar2(50) ";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_SYS_AuditAccess MODIFY auditUser nvarchar2(50)";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "alter table GFI_FLT_AutoReportingConfig MODIFY CreatedBy nvarchar2(100) null";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "Audit User");
                }
                #endregion
                #region Update 135. Notification Title 400
                strUpd = "Rez000135";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_SYS_Notification MODIFY Title nvarchar2(400)";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "Notification Title 400");
                }
                #endregion

                #region Update 136. UOM ADC
                strUpd = "Rez000136";
                if (!CheckUpdateExist(strUpd))
                {
                    UOM u = new UOM();
                    u = uomService.GetUOMByDescription("ADC", sConnStr);
                    if (u == null)
                    {
                        u = new UOM();
                        u.Description = "ADC";
                        uomService.SaveUOM(u, true, sConnStr);
                    }

                    u = new UOM();
                    u = uomService.GetUOMByDescription("ADC2", sConnStr);
                    if (u == null)
                    {
                        u = new UOM();
                        u.Description = "ADC2";
                        uomService.SaveUOM(u, true, sConnStr);
                    }

                    sql = "CREATE INDEX IX_Arc_GFI_SYS_Notif_ExceptionID ON GFI_SYS_Notification (ExceptionID)";
                    if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "IX_Arc_GFI_SYS_Notif_ExceptionID")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "UOM ADC n IX_Arc_GFI_SYS_Notif_ExceptionID");
                }
                #endregion
                #region Update 137. Admin Button in template
                strUpd = "Rez000137";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "update GFI_SYS_Modules set command = 'cmdSysAdmin' where ModuleName = 'Settings --> System Administration' and Command = ''";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "Admin Button in template");
                }
                #endregion

                #region Update 138. New GroupMatrixes
                strUpd = "Rez000138";
                if (!CheckUpdateExist(strUpd))
                {
                    //1 row per option. e.g DriverRosterMapping, Contractors
                    sql = @"CREATE TABLE GFI_SYS_GroupMatrixCompany
							(
								  MID NUMBER(10) NOT NULL
								, GMID NUMBER(10) NOT NULL
								, OptionsEnabled nvarchar2(25) NOT NULL
								, CONSTRAINT PK_GFI_SYS_GroupMatrixCompany PRIMARY KEY  (MID)
							 )";
                    //if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrixCompany")).Rows[0][0].ToString() == "0")
                    //    _SqlConn.ScriptExecute(sql, myStrConn);
                    CreateTable(sql, "GFI_SYS_GroupMatrixCompany", "MID");

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixCompany
							ADD CONSTRAINT FK_GFI_SYS_GroupMatrixCompany_GFI_SYS_GroupMatrix 
								FOREIGN KEY (GMID) 
								REFERENCES GFI_SYS_GroupMatrix (GMID) 
							 ON DELETE Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixCompany_GFI_SYS_GroupMatrix", "GFI_SYS_GroupMatrixCompany")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "New GroupMatrixes");
                }
                #endregion
                #region Update 139. Driver as Resources
                strUpd = "Rez000139";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Department nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Department")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Email nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Email")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD sLastName nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "sLastName")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD OfficerLevel nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "OfficerLevel")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD OfficerTitle nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "OfficerTitle")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Phone nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Phone")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Title nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Title")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD ASPUser nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "ASPUser")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD UserId int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "UserId")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Photo varBinary (1024) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Photo")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //dt = GetDataDT(GetFieldSchema("GFI_FLT_Driver", "Photo"));
                    //if (dt.Rows[0]["CHARACTER_MAXIMUM_LENGTH"].ToString() == "1024")
                    //    dbExecute("ALTER TABLE GFI_FLT_Driver Alter Column Photo varBinary (MAX) NULL");

                    sql = @"ALTER TABLE GFI_FLT_Driver 
							ADD CONSTRAINT FK_GFI_SYS_User_GFI_FLT_Driver 
								FOREIGN KEY(UserId) 
								REFERENCES GFI_SYS_User(UID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_User_GFI_FLT_Driver", "GFI_FLT_Driver")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    dt = GetDataDT(GetFieldSchema("GFI_FLT_Driver", "ZoneID"));
                    sql = "Alter table GFI_FLT_Driver MODIFY ZoneID number(10)";
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver 
							ADD CONSTRAINT FK_GFI_FLT_Driver_GFI_FLT_ZoneHeader 
								FOREIGN KEY(ZoneID) 
								REFERENCES GFI_FLT_ZoneHeader(ZoneID) ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_Driver_GFI_FLT_ZoneHeader", "GFI_FLT_Driver")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    #region CreatedBy UpdatedBy int for Driver
                    List<String> lTableN = new List<String>();
                    lTableN.Add("GFI_FLT_Driver");
                    foreach (String s in lTableN)
                    {
                        //            dt = GetDataDT(GetFieldSchema(s, "CreatedBy"));
                        //            if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                        //            {
                        //                sql = @"Alter table " + s + @" add cb nvarchar(100)";
                        //                if (GetDataDT(ExistColumnSql(s, "cb")).Rows[0][0].ToString() == "0")
                        //                {
                        //                    _SqlConn.ScriptExecute(sql, myStrConn);

                        //                    sql = @"update " + s + @" set cb = CreatedBy
                        //Alter table " + s + @" Alter Column CreatedBy nvarchar(100) null
                        //update " + s + @" set CreatedBy = null
                        //ALTER TABLE " + s + @" ALTER COLUMN CreatedBy int
                        //update " + s + @"
                        //	set CreatedBy = u.UID
                        //from " + s + @" a
                        //	inner join GFI_SYS_User u on u.Email = a.cb

                        //update " + s + @"
                        //	set CreatedBy = u.UID
                        //from " + s + @" a
                        //	inner join GFI_SYS_User u on u.UserName = a.cb
                        //where a.CreatedBy is null

                        //alter table " + s + @" drop column cb";
                        //                    _SqlConn.ScriptExecute(sql, myStrConn);
                        //                }
                        //            }

                        //            dt = GetDataDT(GetFieldSchema(s, "UpdatedBy"));
                        //            if (dt.Rows.Count > 0)
                        //                if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                        //                {
                        //                    sql = @"Alter table " + s + @" add ub nvarchar(100)";
                        //                    if (GetDataDT(ExistColumnSql(s, "ub")).Rows[0][0].ToString() == "0")
                        //                    {
                        //                        _SqlConn.ScriptExecute(sql, myStrConn);

                        //                        sql = @"update " + s + @" set ub = UpdatedBy
                        //	Alter table " + s + @" Alter Column UpdatedBy nvarchar(100) null
                        //	update " + s + @" set UpdatedBy = null
                        //	ALTER TABLE " + s + @" ALTER COLUMN UpdatedBy int
                        //	update " + s + @"
                        //		set UpdatedBy = u.UID
                        //	from " + s + @" a
                        //		inner join GFI_SYS_User u on u.Email = a.ub

                        //	update " + s + @"
                        //		set UpdatedBy = u.UID
                        //	from " + s + @" a
                        //		inner join GFI_SYS_User u on u.UserName = a.ub
                        //	where a.UpdatedBy is null

                        //	alter table " + s + @" drop column ub";
                        //                        _SqlConn.ScriptExecute(sql, myStrConn);
                        //                    }
                        //                }

                        sql = @"ALTER TABLE " + s + @" 
										ADD CONSTRAINT FK_" + s + @"_GFI_SYS_User_CB 
											FOREIGN KEY(CreatedBy) 
											REFERENCES GFI_SYS_User(UID)  ";
                        if (GetDataDT(ExistsSQLConstraint("FK_" + s + @"_GFI_SYS_User_CB", s)).Rows.Count == 0)
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                        if (dt.Rows.Count > 0)
                        {
                            sql = @"ALTER TABLE " + s + @" 
										ADD CONSTRAINT FK_" + s + @"_GFI_SYS_User_UB 
											FOREIGN KEY(UpdatedBy) 
											REFERENCES GFI_SYS_User(UID)";
                            if (GetDataDT(ExistsSQLConstraint("FK_" + s + @"_GFI_SYS_User_UB", s)).Rows.Count == 0)
                                _SqlConn.OracleScriptExecute(sql, myStrConn);
                        }
                    }
                    #endregion

                    //Migrating Officers to Drivers
                    if (GetDataDT(ExistTableSql("Officers")).Rows[0][0].ToString() != "0")
                        if (GetDataDT(ExistColumnSql("Officers", "User")).Rows[0][0].ToString() != "0")
                        {
                            sql = @"insert INTO GFI_FLT_Driver (sDriverName, sFirtName, sLastName, DateOfBirth, Address1, Phone, MobileNumber, CreatedDate, UpdatedDate, Email, Title, Department, OfficerLevel, OfficerTitle, ASPUser, EmployeeType )
								select FirstName, FirstName, LastName, DOB, Address, t1.Phone, Mobile, t1.CreatedDate, t1.UpdatedDate, t1.Email, t1.Title, t1.Department, t1.OfficerLevel, t1.OfficerTitle, user, 'Officer' from Officers t1
									LEFT JOIN GFI_FLT_Driver t2 ON t2.sFirtName = t1.FirstName
								WHERE t2.sFirtName IS NULL";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                            sql = @"update GFI_FLT_Driver
								set UpdatedBy = t3.UID
							from GFI_FLT_Driver t1
							inner join Officers t2 on t2.FirstName = t1.sFirtName
							inner join AspNetUsers t3 on t3.Id = t2.UpdatedBy";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);

                            sql = @"update GFI_FLT_Driver
								set CreatedBy = t3.UID
							from GFI_FLT_Driver t1
							inner join Officers t2 on t2.FirstName = t1.sFirtName
							inner join AspNetUsers t3 on t3.Id = t2.CreatedBy";
                            _SqlConn.OracleScriptExecute(sql, myStrConn);
                        }
                    InsertDBUpdate(strUpd, "Driver as Resources");
                }
                #endregion
                #region Update 140. GFI_SYS_LookUp Types n Values
                strUpd = "Rez000140";
                if (!CheckUpdateExist(strUpd))
                {
                    #region Type
                    sql = @"CREATE TABLE GFI_SYS_LookUpTypes 
							(
								TID NUMBER(10) NOT NULL,
								Code NVARCHAR2(25) NOT NULL,
								Description NVARCHAR2 (200) NULL,
								CreatedDate TIMESTAMP(3) NULL,
								UpdatedDate TIMESTAMP(3) NULL,
								CreatedBy number(10) NULL,
								UpdatedBy number(10) NULL,
								CONSTRAINT PK_GFI_SYS_LookUpTypes PRIMARY KEY (TID)
							)";
                    //if (GetDataDT(ExistTableSql("GFI_SYS_LookUpTypes")).Rows.Count == 0)
                    //    dbExecute(sql);

                    CreateTable(sql, "GFI_SYS_LookUpTypes", "TID");

                    sql = @"ALTER TABLE GFI_SYS_LookUpTypes 
							ADD CONSTRAINT FK_GFI_SYS_LookUpTypes_GFI_SYS_User_C
								FOREIGN KEY(CreatedBy) 
								REFERENCES GFI_SYS_User(UID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_LookUpTypes_GFI_SYS_User_C", "GFI_SYS_LookUpTypes")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_LookUpTypes 
							ADD CONSTRAINT FK_GFI_SYS_LookUpTypes_GFI_SYS_User_U
								FOREIGN KEY(UpdatedBy) 
								REFERENCES GFI_SYS_User(UID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_LookUpTypes_GFI_SYS_User_U", "GFI_SYS_LookUpTypes")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    #endregion

                    #region Values
                    sql = @"CREATE TABLE GFI_SYS_LookUpValues 
							(
								VID NUMBER(10) NOT NULL,
								TID NUMBER(10) NOT NULL,
								Name NVARCHAR2(250) NOT NULL,
								Description NVARCHAR2 (250) NULL,
								Value number(10) NULL,
								CreatedDate TIMESTAMP(3) NULL,
								UpdatedDate TIMESTAMP(3) NULL,
								CreatedBy number(10) NULL,
								UpdatedBy number(10) NULL,
								RootMatrixID number(10) NULL,
								CONSTRAINT PK_GFI_SYS_LookUpValues PRIMARY KEY  (VID)
							)";
                    //if (GetDataDT(ExistTableSql("GFI_SYS_LookUpValues")).Rows[0][0].ToString() == "0")
                    //    dbExecute(sql);
                    CreateTable(sql, "GFI_SYS_LookUpValues", "VID");

                    sql = @"ALTER TABLE GFI_SYS_LookUpValues 
							ADD CONSTRAINT FK_GFI_SYS_LookUpValues_GFI_SYS_LookUpTypes 
								FOREIGN KEY(TID) 
								REFERENCES GFI_SYS_LookUpTypes(TID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_LookUpValues_GFI_SYS_LookUpTypes", "GFI_SYS_LookUpValues")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    sql = @"ALTER TABLE GFI_SYS_LookUpValues 
							ADD CONSTRAINT FK_GFI_SYS_LookUpValues_GFI_SYS_User_C
								FOREIGN KEY(CreatedBy) 
								REFERENCES GFI_SYS_User(UID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_LookUpValues_GFI_SYS_User_C", "GFI_SYS_LookUpValues")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    sql = @"ALTER TABLE GFI_SYS_LookUpValues 
							ADD CONSTRAINT FK_GFI_SYS_LookUpValues_GFI_SYS_User_U
								FOREIGN KEY(UpdatedBy) 
								REFERENCES GFI_SYS_User(UID) ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_LookUpValues_GFI_SYS_User_U", "GFI_SYS_LookUpValues")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    sql = @"ALTER TABLE GFI_SYS_LookUpValues 
							ADD CONSTRAINT FK_GFI_SYS_LookUpValues_GFI_SYS_GroupMatrix 
								FOREIGN KEY (RootMatrixID) 
								REFERENCES GFI_SYS_GroupMatrix (GMID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_LookUpValues_GFI_SYS_GroupMatrix", "GFI_SYS_LookUpValues")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    #endregion

                    #region Type Data
                    if (GetDataDT(ExistTableSql("LookupTypes")).Rows[0][0].ToString() != "0")
                    {
                        sql = @"insert GFI_SYS_LookUpTypes (Code, Description, CreatedDate)
								select t1.Code, t1.Description, t1.CreatedDate from LookupTypes t1
									LEFT JOIN GFI_SYS_LookUpTypes t2 ON t2.Code = t1.Code
								WHERE t2.Code IS NULL";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                        sql = @"update GFI_SYS_LookUpTypes
								set UpdatedBy = t3.UID
							from GFI_SYS_LookUpTypes t1
							inner join LookupTypes t2 on t2.Code = t1.Code
							inner join AspNetUsers t3 on t3.Id = t2.UpdatedBy";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                        sql = @"update GFI_SYS_LookUpTypes
								set CreatedBy = t3.UID
							from GFI_SYS_LookUpTypes t1
							inner join LookupTypes t2 on t2.Code = t1.Code
							inner join AspNetUsers t3 on t3.Id = t2.CreatedBy";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    }

                    LookUpTypes lt = new LookUpTypes();
                    LookUpTypesService lookUpTypesService = new LookUpTypesService();
                    lt = lookUpTypesService.GetLookUpTypesByCode("CONTRACTOR_TYPE", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "CONTRACTOR_TYPE";
                        lt.Description = "CONTRACTOR_TYPE";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lookUpTypesService.SaveLookUpTypes(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lookUpTypesService.GetLookUpTypesByCode("FILLING_TYPE", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "FILLING_TYPE";
                        lt.Description = "FILLING_TYPE";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lookUpTypesService.SaveLookUpTypes(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lookUpTypesService.GetLookUpTypesByCode("CITY", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "CITY";
                        lt.Description = "CITY";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lookUpTypesService.SaveLookUpTypes(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lookUpTypesService.GetLookUpTypesByCode("FUEL_TYPE", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "FUEL_TYPE";
                        lt.Description = "FUEL_TYPE";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lookUpTypesService.SaveLookUpTypes(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lookUpTypesService.GetLookUpTypesByCode("MAKE_TYPE", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "MAKE_TYPE";
                        lt.Description = "MAKE_TYPE";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lookUpTypesService.SaveLookUpTypes(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lookUpTypesService.GetLookUpTypesByCode("VEHICLE_TYPE", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "VEHICLE_TYPE";
                        lt.Description = "VEHICLE_TYPE";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lookUpTypesService.SaveLookUpTypes(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lookUpTypesService.GetLookUpTypesByCode("REQUEST_TYPE", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "REQUEST_TYPE";
                        lt.Description = "REQUEST_TYPE";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lookUpTypesService.SaveLookUpTypes(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lookUpTypesService.GetLookUpTypesByCode("DISTRICT", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "DISTRICT";
                        lt.Description = "DISTRICT";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lookUpTypesService.SaveLookUpTypes(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lookUpTypesService.GetLookUpTypesByCode("ACCIDENTSTATUS", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "ACCIDENTSTATUS";
                        lt.Description = "ACCIDENTSTATUS";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lookUpTypesService.SaveLookUpTypes(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lookUpTypesService.GetLookUpTypesByCode("OFFICERTITLE", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "OFFICERTITLE";
                        lt.Description = "OFFICERTITLE";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lookUpTypesService.SaveLookUpTypes(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lookUpTypesService.GetLookUpTypesByCode("DEPARTMENT", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "DEPARTMENT";
                        lt.Description = "DEPARTMENT";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lookUpTypesService.SaveLookUpTypes(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lookUpTypesService.GetLookUpTypesByCode("ENGINE_TYPE", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "ENGINE_TYPE";
                        lt.Description = "ENGINE_TYPE";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lookUpTypesService.SaveLookUpTypes(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lookUpTypesService.GetLookUpTypesByCode("COMPANY_NAMES", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "COMPANY_NAMES";
                        lt.Description = "COMPANY_NAMES";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lookUpTypesService.SaveLookUpTypes(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lookUpTypesService.GetLookUpTypesByCode("MyType", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "MyType";
                        lt.Description = "MyType";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lookUpTypesService.SaveLookUpTypes(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lookUpTypesService.GetLookUpTypesByCode("Institution", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "Institution";
                        lt.Description = "Institution";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lookUpTypesService.SaveLookUpTypes(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lookUpTypesService.GetLookUpTypesByCode("Purpose", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "Purpose";
                        lt.Description = "Purpose";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lookUpTypesService.SaveLookUpTypes(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lookUpTypesService.GetLookUpTypesByCode("Color", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "Color";
                        lt.Description = "Color";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lookUpTypesService.SaveLookUpTypes(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lookUpTypesService.GetLookUpTypesByCode("DriverType", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "DriverType";
                        lt.Description = "Driver Type";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lookUpTypesService.SaveLookUpTypes(lt, true, sConnStr);
                    }

                    lt = new LookUpTypes();
                    lt = lookUpTypesService.GetLookUpTypesByCode("GradeType", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "GradeType";
                        lt.Description = "Grade Type";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lookUpTypesService.SaveLookUpTypes(lt, true, sConnStr);
                    }

                    #endregion

                    #region Values Data
                    if (GetDataDT(ExistTableSql("LookupValues")).Rows[0][0].ToString() != "0")
                    {
                        sql = @"insert into GFI_SYS_LookUpValues (TID, Name, Description, CreatedDate)
								select t3.TID, t1.Name, t1.Description, t1.CreatedDate from LookupValues t1
									inner join LookupTypes t0 on t0.id = t1.LookupTypeId
									inner join GFI_SYS_LookUpTypes t3 on t0.Code = t3.Code
									LEFT JOIN GFI_SYS_LookUpValues t2 ON t2.Name = t1.Name
								WHERE t2.Name IS NULL";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                        sql = @"update GFI_SYS_LookUpValues
								set UpdatedBy = t3.UID
							from GFI_SYS_LookUpValues t1
							inner join LookupValues t2 on t2.Name = t1.Name
							inner join AspNetUsers t3 on t3.Id = t2.UpdatedBy";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                        sql = @"update GFI_SYS_LookUpValues
								set CreatedBy = t3.UID
							from GFI_SYS_LookUpValues t1
							inner join LookupValues t2 on t2.Name = t1.Name
							inner join AspNetUsers t3 on t3.Id = t2.CreatedBy";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    }

                    LookUpTypes lts = new LookUpTypes();
                    LookUpValues lv = new LookUpValues();

                    lv = lookUpValuesService.GetLookUpValuesByName("COMPANY", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "COMPANY";
                        lv.Description = "COMPANY";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lookUpTypesService.GetLookUpTypesByCode("CONTRACTOR_TYPE", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lookUpValuesService.SaveLookUpValues(lv, true, sConnStr);
                        }
                    }

                    lv = lookUpValuesService.GetLookUpValuesByName("Engen Beau Bassin", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "Engen Beau Bassin";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lookUpTypesService.GetLookUpTypesByCode("FILLING_TYPE", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lookUpValuesService.SaveLookUpValues(lv, true, sConnStr);
                        }
                    }


                    lv = lookUpValuesService.GetLookUpValuesByName("Beau Bassin", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "Beau Bassin";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lookUpTypesService.GetLookUpTypesByCode("CITY", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lookUpValuesService.SaveLookUpValues(lv, true, sConnStr);
                        }
                    }

                    lv = lookUpValuesService.GetLookUpValuesByName("DIESEL", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "DIESEL";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lookUpTypesService.GetLookUpTypesByCode("FUEL_TYPE", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lookUpValuesService.SaveLookUpValues(lv, true, sConnStr);
                        }
                    }

                    lv = lookUpValuesService.GetLookUpValuesByName("AUDI", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "AUDI";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lookUpTypesService.GetLookUpTypesByCode("MAKE_TYPE", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lookUpValuesService.SaveLookUpValues(lv, true, sConnStr);
                        }
                    }

                    lv = lookUpValuesService.GetLookUpValuesByName("CAR", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "CAR";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lookUpTypesService.GetLookUpTypesByCode("VEHICLE_TYPE", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lookUpValuesService.SaveLookUpValues(lv, true, sConnStr);
                        }
                    }

                    lv = lookUpValuesService.GetLookUpValuesByName("ROUTINE", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "ROUTINE";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lookUpTypesService.GetLookUpTypesByCode("REQUEST_TYPE", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lookUpValuesService.SaveLookUpValues(lv, true, sConnStr);
                        }
                    }

                    lv = lookUpValuesService.GetLookUpValuesByName("Moka", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "Moka";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lookUpTypesService.GetLookUpTypesByCode("DISTRICT", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lookUpValuesService.SaveLookUpValues(lv, true, sConnStr);
                        }
                    }

                    lv = lookUpValuesService.GetLookUpValuesByName("CASE CLOSED", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "CASE CLOSED";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lookUpTypesService.GetLookUpTypesByCode("ACCIDENTSTATUS", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lookUpValuesService.SaveLookUpValues(lv, true, sConnStr);
                        }
                    }

                    lv = lookUpValuesService.GetLookUpValuesByName("SECRETARY", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "SECRETARY";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lookUpTypesService.GetLookUpTypesByCode("OFFICERTITLE", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lookUpValuesService.SaveLookUpValues(lv, true, sConnStr);
                        }
                    }

                    lv = lookUpValuesService.GetLookUpValuesByName("REGISTRY", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "REGISTRY";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lookUpTypesService.GetLookUpTypesByCode("DEPARTMENT", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lookUpValuesService.SaveLookUpValues(lv, true, sConnStr);
                        }
                    }

                    lv = lookUpValuesService.GetLookUpValuesByName("MANUAL", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "MANUAL";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lookUpTypesService.GetLookUpTypesByCode("ENGINE_TYPE", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lookUpValuesService.SaveLookUpValues(lv, true, sConnStr);
                        }
                    }

                    lv = lookUpValuesService.GetLookUpValuesByName("Naveo", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "Naveo";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lookUpTypesService.GetLookUpTypesByCode("COMPANY_NAMES", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lookUpValuesService.SaveLookUpValues(lv, true, sConnStr);
                        }
                    }

                    lv = lookUpValuesService.GetLookUpValuesByName("Naveo Type", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "Naveo Type";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lookUpTypesService.GetLookUpTypesByCode("MyType", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lookUpValuesService.SaveLookUpValues(lv, true, sConnStr);
                        }
                    }

                    lv = lookUpValuesService.GetLookUpValuesByName("Naveo Institution", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "Naveo Institution";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lookUpTypesService.GetLookUpTypesByCode("Institution", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lookUpValuesService.SaveLookUpValues(lv, true, sConnStr);
                        }
                    }

                    lv = lookUpValuesService.GetLookUpValuesByName("Naveo Purpose", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "Naveo Purpose";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lookUpTypesService.GetLookUpTypesByCode("Purpose", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lookUpValuesService.SaveLookUpValues(lv, true, sConnStr);
                        }
                    }

                    lv = lookUpValuesService.GetLookUpValuesByName("Red", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "Red";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lookUpTypesService.GetLookUpTypesByCode("Color", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lookUpValuesService.SaveLookUpValues(lv, true, sConnStr);
                        }
                    }

                    lv = lookUpValuesService.GetLookUpValuesByName("Driver Van", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "Driver Van";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lookUpTypesService.GetLookUpTypesByCode("DriverType", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lookUpValuesService.SaveLookUpValues(lv, true, sConnStr);
                        }
                    }

                    lv = lookUpValuesService.GetLookUpValuesByName("Driver Grade", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "Driver Grade";
                        lv.Description = "Default";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;

                        lts = new LookUpTypes();
                        lts = lookUpTypesService.GetLookUpTypesByCode("GradeType", sConnStr);
                        if (lts != null)
                        {
                            lv.TID = lts.TID;
                            lookUpValuesService.SaveLookUpValues(lv, true, sConnStr);
                        }
                    }
                    #endregion

                    InsertDBUpdate(strUpd, "GFI_SYS_LookUp Types n Values");
                }
                #endregion
                #region Update 141. Planning
                strUpd = "Rez000141";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_PLN_Planning 
							(
								PID NUMBER(10) NOT NULL,
								Approval number(10) null,
								Remarks NVARCHAR2 (500) NULL,
								CreatedDate TIMESTAMP(3) NULL,
								UpdatedDate TIMESTAMP(3) NULL,
								CreatedBy number(10) NULL,
								UpdatedBy number(10) NULL,
								CONSTRAINT PK_GFI_PLN_Planning PRIMARY KEY  (PID)
                    )";
                    //if (GetDataDT(ExistTableSql("GFI_PLN_Planning")).Rows[0][0].ToString() == "0")
                    //    dbExecute(sql);
                    CreateTable(sql, "GFI_PLN_Planning", "PID");

                    sql = @"ALTER TABLE GFI_PLN_Planning 
							ADD CONSTRAINT FK_GFI_PLN_Planning_GFI_SYS_User_C
								FOREIGN KEY(CreatedBy) 
								REFERENCES GFI_SYS_User(UID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_Planning_GFI_SYS_User_C", "GFI_PLN_Planning")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_PLN_Planning 
							ADD CONSTRAINT FK_GFI_PLN_Planning_GFI_SYS_User_U
								FOREIGN KEY(UpdatedBy) 
								REFERENCES GFI_SYS_User(UID) ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_Planning_GFI_SYS_User_U", "GFI_PLN_Planning")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_PLN_Planning ADD Urgent int default 0";
                    if (GetDataDT(ExistColumnSql("GFI_PLN_Planning", "Urgent")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_PLN_Planning ADD RequestCode nvarchar2(14) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_PLN_Planning", "RequestCode")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_PLN_Planning ADD  CONSTRAINT UK_GFI_PLN_Planning_RequestCode Unique (RequestCode)";
                    if (GetDataDT(ExistsSQLConstraint("UK_GFI_PLN_Planning_RequestCode", "GFI_PLN_Planning")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_PLN_Planning ADD ParentRequestCode nvarchar2(14) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_PLN_Planning", "ParentRequestCode")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_PLN_Planning ADD Type int NULL";    //1 Fetch, 2 Convey
                    if (GetDataDT(ExistColumnSql("GFI_PLN_Planning", "Type")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_PLN_Planning ADD sComments nvarchar2(250) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_PLN_Planning", "sComments")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_PLN_Planning ADD ClosureComments nvarchar2(250) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_PLN_Planning", "ClosureComments")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_PLN_Planning ADD Ref nvarchar2(250) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_PLN_Planning", "Ref")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    #region MOHRequestCode
                    sql = @"CREATE TABLE GFI_PLN_MOHRequestCode 
							(
								Code nvarchar2(3) NOT NULL,
								iYear number(10)  NOT NULL,
								iSeed number(10)  NOT NULL,
								CONSTRAINT PK_GFI_PLN_MOHRequestCode PRIMARY KEY (Code, iYear)
							) ";
                    //if (GetDataDT(ExistTableSql("GFI_PLN_MOHRequestCode")).Rows[0][0].ToString() == "0")
                    //    dbExecute(sql);
                    CreateTable(sql, "GFI_PLN_MOHRequestCode", "Code");

                    //             sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('sp_MOHRequestCode') AND type in (N'P', N'PC'))
                    //DROP PROCEDURE sp_MOHRequestCode";
                    //             _SqlConn.ScriptExecute(sql, myStrConn);

                    //             sql = @"
                    //CREATE PROCEDURE sp_MOHRequestCode] @Code nvarchar(3)
                    //as Begin

                    //	DECLARE @iYear int
                    //	DECLARE @sYear nvarchar(2)
                    //	DECLARE @iSeed int
                    //	DECLARE @sSeed nvarchar(5)
                    //	DECLARE @ReqNo nvarchar(14)
                    //	DECLARE @idd int
                    //	DECLARE @sdd nvarchar(2)
                    //	DECLARE @imm int
                    //	DECLARE @smm nvarchar(2)

                    //	set @iYear = 0

                    //	;with cta as
                    //	(
                    //		select * from GFI_PLN_MOHRequestCode] where Code] = @Code and iYear = DATEPART(yyyy, GETDATE()) 
                    //	)
                    //	select @iYear = iYear, @iSeed = iSeed from cta;

                    //	if (@iYear = 0)
                    //	begin
                    //		insert GFI_PLN_MOHRequestCode] values (@Code, DATEPART(yyyy, GETDATE()), 0); 

                    //		;with cta as
                    //		(
                    //			select * from GFI_PLN_MOHRequestCode] where Code] = @Code and iYear = DATEPART(yyyy, GETDATE()) 
                    //		)
                    //		select @iYear = iYear, @iSeed = iSeed from cta;
                    //	end

                    //	update GFI_PLN_MOHRequestCode] set iSeed = iSeed + 1 where Code] = @Code and iYear = DATEPART(yyyy, GETDATE()) 

                    //	;with cta as
                    //	(
                    //		select * from GFI_PLN_MOHRequestCode] where Code] = @Code and iYear = DATEPART(yyyy, GETDATE()) 
                    //	)
                    //	select @iYear = iYear, @iSeed = iSeed from cta;

                    //	--yy
                    //	if(@iYear > 2000)
                    //		set @iYear = @iYear - 2000
                    //	set @sYear = CAST(@iYear AS nvarchar(2))

                    //	--mm
                    //	set @imm = DATEPART(mm,GETDATE());
                    //	if(@imm < 10)
                    //		set @smm = '0' + CAST(@imm AS nvarchar(2))
                    //	else
                    //		set @smm = CAST(@imm AS nvarchar(2))

                    //	--dd
                    //	set @idd = DATEPART(dd,GETDATE());
                    //	if(@idd < 10)
                    //		set @sdd = '0' + CAST(@idd AS nvarchar(2))
                    //	else
                    //		set @sdd = CAST(@idd AS nvarchar(2))

                    //	--Seed
                    //	if(@iSeed < 10)	        set @sSeed = '0000' + CAST(@iSeed AS nvarchar(5))
                    //	else if(@iSeed < 100)	set @sSeed = '000' + CAST(@iSeed AS nvarchar(5))
                    //	else if(@iSeed < 1000)	set @sSeed = '00' + CAST(@iSeed AS nvarchar(5))
                    //	else if(@iSeed < 10000)	set @sSeed = '0' + CAST(@iSeed AS nvarchar(5))
                    //	else                    set @sSeed = CAST(@iSeed AS nvarchar(5))

                    //	set @ReqNo = @Code + @sdd + @smm + @sYear + @sSeed
                    //	select @ReqNo
                    //end";
                    //             _SqlConn.ScriptExecute(sql, myStrConn);
                    #endregion

                    #region Matrix
                    sql = @"CREATE TABLE GFI_SYS_GroupMatrixPlanning
							(
								  MID NUMBER(10) NOT NULL
								, GMID NUMBER(10) NOT NULL
								, iID NUMBER(10) NOT NULL
								, CONSTRAINT PK_GFI_SYS_GroupMatrixPlanning PRIMARY KEY  (MID)
							 )";
                    //if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrixPlanning")).Rows[0][0].ToString() == "0")
                    //    dbExecute(sql);
                    CreateTable(sql, "GFI_SYS_GroupMatrixPlanning", "MID");

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixPlanning 
							ADD CONSTRAINT FK_GFI_SYS_GroupMatrixPlanning_GFI_SYS_GroupMatrix 
								FOREIGN KEY (GMID) 
								REFERENCES GFI_SYS_GroupMatrix (GMID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixPlanning_GFI_SYS_GroupMatrix", "GFI_SYS_GroupMatrixPlanning")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixPlanning 
							ADD CONSTRAINT FK_GFI_SYS_GroupMatrixPlanning_GFI_PLN_Planning 
								FOREIGN KEY(iID) 
								REFERENCES GFI_PLN_Planning(PID)  
						 ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixPlanning_GFI_PLN_Planning", "GFI_SYS_GroupMatrixPlanning")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    #endregion

                    #region GFI_PLN_PlanningLkup. Institution, Purpose
                    sql = @"CREATE TABLE GFI_PLN_PlanningLkup 
							(
								iID NUMBER(10) NOT NULL,
								PID number(10) not null,
								LookupValueID number(10) not null,    
								LookupType nvarchar2(25) null,   
								CONSTRAINT PK_GFI_GFI_PLN_PlanningLkup PRIMARY KEY  (iID)
							)";
                    //if (GetDataDT(ExistTableSql("GFI_PLN_PlanningLkup")).Rows[0][0].ToString() == "0")
                    //    dbExecute(sql);
                    CreateTable(sql, "GFI_PLN_PlanningLkup", "iID");

                    sql = @"ALTER TABLE GFI_PLN_PlanningLkup 
							ADD CONSTRAINT FK_GFI_PLN_PlanningLkup_GFI_PLN_Planning 
								FOREIGN KEY (PID) 
								REFERENCES GFI_PLN_Planning (PID) 
							 ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_PlanningLkup_GFI_PLN_Planning", "GFI_PLN_PlanningLkup")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_PLN_PlanningLkup 
							ADD CONSTRAINT FK_GFI_PLN_PlanningLkup_GFI_SYS_LookUpValues 
								FOREIGN KEY (LookupValueID) 
								REFERENCES GFI_SYS_LookUpValues (VID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_PlanningLkup_GFI_SYS_LookUpValues", "GFI_PLN_PlanningLkup")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    #endregion

                    #region GFI_PLN_PlanningResource. RequestOfficer, SentForApproval, Passenger
                    sql = @"CREATE TABLE GFI_PLN_PlanningResource 
							(
								iID NUMBER(10) NOT NULL,
								PID number(10) not null,
								DriverID number(10) not null,
								ResourceType nvarchar2(25) null,
								CONSTRAINT PK_GFI_PLN_PlanningResource PRIMARY KEY (iID)
							)";
                    //if (GetDataDT(ExistTableSql("GFI_PLN_PlanningResource")).Rows[0][0].ToString() == "0")
                    //    dbExecute(sql);
                    CreateTable(sql, "GFI_PLN_PlanningResource", "iID");

                    sql = @"ALTER TABLE GFI_PLN_PlanningResource 
							ADD CONSTRAINT FK_GFI_PLN_PlanningResource_GFI_PLN_Planning 
								FOREIGN KEY (PID) 
								REFERENCES GFI_PLN_Planning (PID)  
							 ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_PlanningResource_GFI_PLN_Planning", "GFI_PLN_PlanningResource")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_PLN_PlanningResource 
							ADD CONSTRAINT FK_GFI_PLN_PlanningResource_GFI_FLT_Driver 
								FOREIGN KEY (DriverID) 
								REFERENCES GFI_FLT_Driver (DriverID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_PlanningResource_GFI_FLT_Driver", "GFI_PLN_PlanningResource")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    #endregion

                    #region GFI_PLN_PlanningViaPointH
                    sql = @"CREATE TABLE GFI_PLN_PlanningViaPointH 
							(
								iID NUMBER(10) NOT NULL,
								PID number(10) not null,
								SeqNo number(10) not null,    
								ZoneID number(10) null,
								Lat binary_double null,
								Lon binary_double null,
								LocalityVCA number(10) null,
								Buffer number(10) null,
								Remarks nvarchar2(250) null,
								dtUTC TIMESTAMP(3) null,
								CONSTRAINT PK_GFI_PLN_PlanningViaPointH PRIMARY KEY (iID)
							)";
                    //if (GetDataDT(ExistTableSql("GFI_PLN_PlanningViaPointH")).Rows[0][0].ToString() == "0")
                    //    dbExecute(sql);
                    CreateTable(sql, "GFI_PLN_PlanningViaPointH", "iID");

                    sql = @"ALTER TABLE GFI_PLN_PlanningViaPointH 
							ADD CONSTRAINT FK_GFI_PLN_PlanningViaPointH_GFI_PLN_Planning 
								FOREIGN KEY (PID) 
								REFERENCES GFI_PLN_Planning (PID) 
							 ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_PlanningViaPointH_GFI_PLN_Planning", "GFI_PLN_PlanningViaPointH")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_PLN_PlanningViaPointH 
							ADD CONSTRAINT FK_GFI_PLN_PlanningViaPointH_GFI_FLT_ZoneHeader 
								FOREIGN KEY (ZoneID) 
								REFERENCES GFI_FLT_ZoneHeader (ZoneID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_PlanningViaPointH_GFI_FLT_ZoneHeader", "GFI_PLN_PlanningViaPointH")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    #endregion

                    #region GFI_PLN_PlanningViaPointD
                    sql = @"CREATE TABLE GFI_PLN_PlanningViaPointD 
							(
								iID NUMBER(10) NOT NULL,
								HID number(10) not null,
								UOM number(10) null,
								Capacity number(10) null,    
								Remarks nvarchar2(250) null,
								CONSTRAINT PK_GFI_PLN_PlanningViaPointD PRIMARY KEY (iID)
							)";
                    //if (GetDataDT(ExistTableSql("GFI_PLN_PlanningViaPointD")).Rows[0][0].ToString() == "0")
                    //    dbExecute(sql);
                    CreateTable(sql, "GFI_PLN_PlanningViaPointD", "iID");

                    sql = @"ALTER TABLE GFI_PLN_PlanningViaPointD 
							ADD CONSTRAINT FK_GFI_PLN_PlanningViaPointD_GFI_PLN_PlanningViaPointH
								FOREIGN KEY (HID) 
								REFERENCES GFI_PLN_PlanningViaPointH (iID) 
							 ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_PlanningViaPointD_GFI_PLN_PlanningViaPointH", "GFI_PLN_PlanningViaPointD")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_PLN_PlanningViaPointD 
							ADD CONSTRAINT FK_GFI_PLN_PlanningViaPointD_GFI_SYS_UOM
								FOREIGN KEY (UOM) 
								REFERENCES GFI_SYS_UOM (UID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_PlanningViaPointD_GFI_SYS_UOM", "GFI_PLN_PlanningViaPointD")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    #endregion

                    #region GFI_SYS_CustomerMaster
                    sql = @"CREATE TABLE GFI_SYS_CustomerMaster 
							(
								ClientID NUMBER(10) NOT NULL,
								Surname nvarchar2(50) null,
								Name nvarchar2(50) null,
								Tel1 nvarchar2(15) null,
								Tel2 nvarchar2(15) null,
								Add1 nvarchar2(50) null,
								Add2 nvarchar2(50) null,
								Remarks nvarchar2(250) null,
								CONSTRAINT PK_GFI_SYS_CustomerMaster PRIMARY KEY (ClientID)
							)";
                    //if (GetDataDT(ExistTableSql("GFI_SYS_CustomerMaster")).Rows[0][0].ToString() == "0")
                    //    dbExecute(sql);
                    CreateTable(sql, "GFI_SYS_CustomerMaster", "ClientID");

                    sql = @"ALTER TABLE GFI_SYS_CustomerMaster ADD HomeNo number(10) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_CustomerMaster", "HomeNo")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_CustomerMaster ADD District nvarchar2(25)  NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_CustomerMaster", "District")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_CustomerMaster ADD PostalCode nvarchar2(15)  NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_CustomerMaster", "PostalCode")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_CustomerMaster ADD MobileNumber nvarchar2(15)  NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_CustomerMaster", "MobileNumber")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_CustomerMaster ADD NIC nvarchar2(15)  NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_CustomerMaster", "NIC")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_PLN_PlanningViaPointH ADD ClientID number(10) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_PLN_PlanningViaPointH", "ClientID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_PLN_PlanningViaPointH 
							ADD CONSTRAINT FK_GFI_PLN_PlanningViaPointH_GFI_SYS_CustomerMaster
								FOREIGN KEY (ClientID) 
								REFERENCES GFI_SYS_CustomerMaster (ClientID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_PlanningViaPointH_GFI_SYS_CustomerMaster", "GFI_PLN_PlanningViaPointH")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_CustomerMaster ADD ZoneID number(10) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_CustomerMaster", "ZoneID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_CustomerMaster 
							ADD CONSTRAINT FK_GFI_SYS_CustomerMaster_GFI_FLT_ZoneHeader 
								FOREIGN KEY(ZoneID) 
								REFERENCES GFI_FLT_ZoneHeader(ZoneID) ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_CustomerMaster_GFI_FLT_ZoneHeader", "GFI_SYS_CustomerMaster")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    #endregion

                    InsertDBUpdate(strUpd, "Planning");
                }
                #endregion
                #region Update 142. Scheduled
                strUpd = "Rez000142";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_PLN_Scheduled 
							(
								HID NUMBER(10) NOT NULL,
								AssetSelectionID number(10) null,
								DriverSelectionID number(10) null,
								Status number(10) null,
								Remarks nvarchar2(50) null,
								intField number(10) null,
								CreatedDate TIMESTAMP(3) NULL,
								UpdatedDate TIMESTAMP(3) NULL,
								CreatedBy number(10) NULL,
								UpdatedBy number(10) NULL,
								CONSTRAINT PK_GFI_PLN_Scheduled PRIMARY KEY (HID)
							)";
                    //if (GetDataDT(ExistTableSql("GFI_PLN_Scheduled")).Rows[0][0].ToString() == "0")
                    //    dbExecute(sql);
                    CreateTable(sql, "GFI_PLN_Scheduled", "HID");

                    sql = @"ALTER TABLE GFI_PLN_Scheduled 
							ADD CONSTRAINT FK_GFI_PLN_Scheduled_GFI_SYS_User_C
								FOREIGN KEY(CreatedBy) 
								REFERENCES GFI_SYS_User(UID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_Scheduled_GFI_SYS_User_C", "GFI_PLN_Scheduled")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_PLN_Scheduled 
							ADD CONSTRAINT FK_GFI_PLN_Scheduled_GFI_SYS_User_U
								FOREIGN KEY(UpdatedBy) 
								REFERENCES GFI_SYS_User(UID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_Scheduled_GFI_SYS_User_U", "GFI_PLN_Scheduled")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    #region Matrix
                    sql = @"CREATE TABLE GFI_SYS_GroupMatrixScheduled
							(
								  MID NUMBER(10) NOT NULL
								, GMID NUMBER(10) NOT NULL
								, iID NUMBER(10) NOT NULL
								, CONSTRAINT PK_GFI_SYS_GroupMatrixScheduled PRIMARY KEY  (MID)
							 )";
                    //if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrixScheduled")).Rows[0][0].ToString() == "0")
                    //    dbExecute(sql);
                    CreateTable(sql, "GFI_SYS_GroupMatrixScheduled", "MID");

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixScheduled 
							ADD CONSTRAINT FK_GFI_SYS_GroupMatrixScheduled_GFI_SYS_GroupMatrix 
								FOREIGN KEY (GMID) 
								REFERENCES GFI_SYS_GroupMatrix (GMID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixScheduled_GFI_SYS_GroupMatrix", "GFI_SYS_GroupMatrixScheduled")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixScheduled 
							ADD CONSTRAINT FK_GFI_SYS_GroupMatrixScheduled_GFI_PLN_Scheduled
								FOREIGN KEY(iID) 
								REFERENCES GFI_PLN_Scheduled(HID) 
						 ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixScheduled_GFI_PLN_Scheduled", "GFI_SYS_GroupMatrixScheduled")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    #endregion

                    #region GFI_PLN_ScheduledPlan Details1
                    sql = @"CREATE TABLE GFI_PLN_ScheduledPlan 
							(
								iID NUMBER(10) NOT NULL,
								HID number(10) not null,
								PID number(10) not null,
								SeqNo number(10) null,
								CONSTRAINT PK_GFI_PLN_ScheduledPlan PRIMARY KEY  (iID)
							)";
                    //if (GetDataDT(ExistTableSql("GFI_PLN_ScheduledPlan")).Rows[0][0].ToString() == "0")
                    //    dbExecute(sql);
                    CreateTable(sql, "GFI_PLN_ScheduledPlan", "iID");

                    sql = @"ALTER TABLE GFI_PLN_ScheduledPlan 
							ADD CONSTRAINT FK_GFI_PLN_ScheduledPlan_GFI_PLN_Scheduled 
								FOREIGN KEY (HID) 
								REFERENCES GFI_PLN_Scheduled (HID)
							 ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_ScheduledPlan_GFI_PLN_Scheduled", "GFI_PLN_ScheduledPlan")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_PLN_ScheduledPlan 
							ADD CONSTRAINT FK_GFI_PLN_ScheduledPlan_GFI_PLN_Planning 
								FOREIGN KEY (PID) 
								REFERENCES GFI_PLN_Planning (PID) ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_ScheduledPlan_GFI_PLN_Planning", "GFI_PLN_ScheduledPlan")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    #endregion

                    #region GFI_PLN_ScheduledAsset Details2
                    sql = @"CREATE TABLE GFI_PLN_ScheduledAsset 
							(
								iID NUMBER(10) NOT NULL,
								HID number(10) not null,
								AssetID number(10) null,    
								DriverID number(10) null,
								CONSTRAINT PK_GFI_PLN_ScheduledAsset PRIMARY KEY (iID)
							)";
                    //if (GetDataDT(ExistTableSql("GFI_PLN_ScheduledAsset")).Rows[0][0].ToString() == "0")
                    //    dbExecute(sql);
                    CreateTable(sql, "GFI_PLN_ScheduledAsset", "iID");

                    sql = @"ALTER TABLE GFI_PLN_ScheduledAsset 
							ADD CONSTRAINT FK_GFI_PLN_ScheduledAsset_GFI_PLN_Scheduled 
								FOREIGN KEY (HID) 
								REFERENCES GFI_PLN_Scheduled (HID)
							 ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_ScheduledAsset_GFI_PLN_Scheduled", "GFI_PLN_ScheduledAsset")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_PLN_ScheduledAsset 
							ADD CONSTRAINT FK_GFI_GFI_PLN_ScheduledAsset_GFI_FLT_Asset 
								FOREIGN KEY (AssetID) 
								REFERENCES GFI_FLT_Asset (AssetID) 
							 ON DELETE Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GFI_PLN_ScheduledAsset_GFI_FLT_Asset", "GFI_PLN_ScheduledAsset")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_PLN_ScheduledAsset 
							ADD CONSTRAINT FK_GFI_PLN_ScheduledAsset_GFI_FLT_Driver 
								FOREIGN KEY (DriverID) 
								REFERENCES GFI_FLT_Driver (DriverID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_ScheduledAsset_GFI_FLT_Driver", "GFI_PLN_ScheduledAsset")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    #endregion

                    InsertDBUpdate(strUpd, "Scheduled");
                }
                #endregion
                #region Update 143. Driver Lookup
                strUpd = "Rez000143";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Type_Institution number(10) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Type_Institution")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver 
							ADD CONSTRAINT FK_GFI_FLT_Driver_GFI_SYS_LookUpValues_Inst 
								FOREIGN KEY(Type_Institution) 
								REFERENCES GFI_SYS_LookUpValues (VID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_Driver_GFI_SYS_LookUpValues_Inst", "GFI_FLT_Driver")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Type_Driver number(10) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Type_Driver")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver 
							ADD CONSTRAINT FK_GFI_FLT_Driver_GFI_SYS_LookUpValues_Driver 
								FOREIGN KEY(Type_Driver) 
								REFERENCES GFI_SYS_LookUpValues (VID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_Driver_GFI_SYS_LookUpValues_Driver", "GFI_FLT_Driver")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD Type_Grade number(10) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "Type_Grade")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver 
							ADD CONSTRAINT FK_GFI_FLT_Driver_GFI_SYS_LookUpValues_Grade 
								FOREIGN KEY(Type_Grade) 
								REFERENCES GFI_SYS_LookUpValues (VID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_Driver_GFI_SYS_LookUpValues_Grade", "GFI_FLT_Driver")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Driver ADD HomeNo number(10) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Driver", "HomeNo")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE TABLE GFI_FLT_ResponsibleDriver 
							(
								iID NUMBER(10) NOT NULL,
								DriverID number(10) not null,
								ResponsibleDriverID number(10) not null,
								CONSTRAINT PK_GFI_FLT_ResponsibleDriver PRIMARY KEY (iID)
							)";
                    //if (GetDataDT(ExistTableSql("GFI_FLT_ResponsibleDriver")).Rows[0][0].ToString() == "0")
                    //    _SqlConn.ScriptExecute(sql, myStrConn);
                    CreateTable(sql, "GFI_FLT_ResponsibleDriver", "iID");

                    sql = @"ALTER TABLE GFI_FLT_ResponsibleDriver 
							ADD CONSTRAINT FK_GFI_FLT_ResponsibleDriver_GFI_FLT_Driver_Driver 
								FOREIGN KEY(DriverID) 
								REFERENCES GFI_FLT_Driver (DriverID) 
						 ON DELETE cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ResponsibleDriver_GFI_FLT_Driver_Driver", "GFI_FLT_ResponsibleDriver")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_ResponsibleDriver 
							ADD CONSTRAINT FK_GFI_FLT_ResponsibleDriver_GFI_FLT_Driver_Responsible 
								FOREIGN KEY(ResponsibleDriverID) 
								REFERENCES GFI_FLT_Driver (DriverID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ResponsibleDriver_GFI_FLT_Driver_Responsible", "GFI_FLT_ResponsibleDriver")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "Driver Lookup");
                }
                #endregion
                #region Update 144. Asset updates
                strUpd = "Rez000144";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_FLT_Asset ADD VehicleTypeId number(10) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Asset", "VehicleTypeId")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_Asset_GFI_AMM_VehicleTypes", "GFI_FLT_Asset")).Rows.Count == 0)
                    {
                        sql = @"UPDATE GFI_FLT_Asset 
									SET VehicleTypeId = (select T2.VehicleTypeId_cbo
								FROM GFI_FLT_Asset T1
								INNER JOIN GFI_AMM_AssetExtProVehicles T2 ON T2.AssetId = T1.AssetID)";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                        sql = @"ALTER TABLE GFI_FLT_Asset 
							ADD CONSTRAINT FK_GFI_FLT_Asset_GFI_AMM_VehicleTypes 
								FOREIGN KEY(VehicleTypeId) 
								REFERENCES GFI_AMM_VehicleTypes (VehicleTypeId)";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    }

                    //sql = @"ALTER TABLE GFI_AMM_AssetExtProVehicles DROP FOREIGN KEY FK_GFI_AMM_Asset_ExtPro_Vehicles_GFI_AMM_VehicleTypes";
                    sql = @"ALTER TABLE GFI_AMM_AssetExtProVehicles DROP CONSTRAINT  FK_GFI_AMM_Asset_ExtPro_Vehicles_GFI_AMM_VehicleTypes";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_AMM_Asset_ExtPro_Vehicles_GFI_AMM_VehicleTypes", "GFI_AMM_AssetExtProVehicles")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Asset ADD ColorID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Asset", "ColorID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_Asset 
							ADD CONSTRAINT FK_GFI_FLT_Asset_GFI_SYS_LookUpValues_Color 
								FOREIGN KEY(ColorID) 
								REFERENCES GFI_SYS_LookUpValues (VID) ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_Asset_GFI_SYS_LookUpValues_Color", "GFI_FLT_Asset")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE TABLE GFI_FLT_AssetUOM 
							(
								iID NUMBER(10) NOT NULL,
								AssetID number(10) not null,
								UOM number(10) not null,
								Capacity number(10) null,
								CONSTRAINT PK_GFI_FLT_AssetUOM PRIMARY KEY (iID)
							)";
                    //if (GetDataDT(ExistTableSql("GFI_FLT_AssetUOM")).Rows[0][0].ToString() == "0")
                    //    _SqlConn.ScriptExecute(sql, myStrConn);
                    CreateTable(sql, "GFI_FLT_AssetUOM", "iID");

                    sql = @"ALTER TABLE GFI_FLT_AssetUOM 
							ADD CONSTRAINT FK_GFI_FLT_AssetUOM_GFI_FLT_Asset
								FOREIGN KEY(AssetID) 
								REFERENCES GFI_FLT_Asset(AssetID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_AssetUOM_GFI_FLT_Asset", "GFI_FLT_AssetUOM")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_FLT_AssetUOM 
							ADD CONSTRAINT FK_GFI_FLT_AssetUOM_GFI_SYS_UOM
								FOREIGN KEY(UOM) 
								REFERENCES GFI_SYS_UOM(UID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_AssetUOM_GFI_SYS_UOM", "GFI_FLT_AssetUOM")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "Asset updates");
                }
                #endregion
                #region Update 145. Distance SQL functions
                strUpd = "Rez000145";
                if (!CheckUpdateExist(strUpd))
                {
                    //             #region fnCalcDistanceInKM
                    //             sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnCalcDistanceInKM') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
                    //DROP FUNCTION fnCalcDistanceInKM";
                    //             _SqlConn.ScriptExecute(sql, myStrConn);

                    //             sql = @"CREATE FUNCTION fnCalcDistanceInKM(@lat1 FLOAT, @lat2 FLOAT, @lon1 FLOAT, @lon2 FLOAT)
                    //RETURNS FLOAT 
                    //AS
                    //BEGIN
                    //	RETURN ACOS(SIN(PI()*@lat1/180.0)*SIN(PI()*@lat2/180.0)+COS(PI()*@lat1/180.0)*COS(PI()*@lat2/180.0)*COS(PI()*@lon2/180.0-PI()*@lon1/180.0))*6371
                    //END";
                    //             _SqlConn.ScriptExecute(sql, myStrConn);
                    //             #endregion

                    //             #region spGPSDataDistanceFromXY
                    //             sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'spGPSDataDistanceFromXY') AND type in (N'P', N'PC'))
                    //DROP PROCEDURE spGPSDataDistanceFromXY";
                    //             _SqlConn.ScriptExecute(sql, myStrConn);

                    //             sql = @"
                    //-- Stored Procedure
                    //--    Author     : Reza
                    //--    Create date: 15/03/2017
                    //--    Description: Return calculated distance from debug data. 
                    //--                 If optional parameter @dist is null, return all data, else only data <= @dist    
                    //Create PROCEDURE spGPSDataDistanceFromXY
                    //	@dtFrom Datetime 
                    //	, @dtTo Datetime 
                    //	, @AssetID int
                    //	, @lat1 FLOAT
                    //	, @lon1 FLOAT
                    //	, @dist int = null
                    //as Begin
                    //	SELECT *, fnCalcDistanceInKM(@lat1, Latitude, @lon1, Longitude) * 1000 DistanceInM into #dtDist from GFI_GPS_GPSData
                    //		where DateTimeGPS_UTC >= @dtFrom and DateTimeGPS_UTC <= @dtTo
                    //			and AssetID = @AssetID

                    //	if @dist is null
                    //		select * from #dtDist order by AssetID, DateTimeGPS_UTC 
                    //	else  
                    //		select * from #dtDist where DistanceInM <= @dist order by AssetID, DateTimeGPS_UTC                      
                    //end";
                    //             _SqlConn.ScriptExecute(sql, myStrConn);
                    //             #endregion

                    //             #region spGPSDataInZone
                    //             sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'spGPSDataInZone') AND type in (N'P', N'PC'))
                    //DROP PROCEDURE spGPSDataInZone";
                    //             _SqlConn.ScriptExecute(sql, myStrConn);

                    //             sql = @"
                    //-- Stored Procedure
                    //--    Author     : Reza
                    //--    Create date: 15/03/2017
                    //--    Description: Return debug data with additional field isPointInZone. 
                    //--                 If optional parameter @ShowAllData is null, return all data, else only data in zone    
                    //Create PROCEDURE spGPSDataInZone
                    //	@dtFrom Datetime 
                    //	, @dtTo Datetime 
                    //	, @AssetID int
                    //	, @ZoneID int
                    //	, @ShowAllData int = null
                    //as Begin
                    //	SELECT *, isPointInZone(Longitude, Latitude, @ZoneID) isPointInZone into #dtDist from GFI_GPS_GPSData
                    //		where DateTimeGPS_UTC >= @dtFrom and DateTimeGPS_UTC <= @dtTo
                    //			and AssetID = @AssetID

                    //	if @ShowAllData is null
                    //		select * from #dtDist order by AssetID, DateTimeGPS_UTC 
                    //	else  
                    //		select * from #dtDist where isPointInZone = 1 order by AssetID, DateTimeGPS_UTC                      
                    //end";
                    //             _SqlConn.ScriptExecute(sql, myStrConn);
                    //             #endregion

                    InsertDBUpdate(strUpd, "Distance SQL functions");
                }
                #endregion
                #region Update 146. VCA
                strUpd = "Rez000146";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_PLN_PlanningViaPointH DROP FOREIGN KEY FK_GFI_PLN_PlanningViaPointH_GFI_FLT_VCA";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_PlanningViaPointH_GFI_FLT_VCA", "GFI_PLN_PlanningViaPointH")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"drop TABLE GFI_FLT_VCA";
                    if (GetDataDT(ExistTableSql("GFI_FLT_VCA")).Rows[0][0].ToString() != "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE TABLE GFI_FLT_VCAHeader(
								VCAID number(10) NOT NULL,
								Name nvarchar2(255) NULL,
								Comments nvarchar2(500) NULL,
								Color number(10) DEFAULT 1 NOT NULL,
							CONSTRAINT PK_GFI_FLT_VCAHeader PRIMARY KEY (VCAID)
						 ) ";
                    //if (GetDataDT(ExistTableSql("GFI_FLT_VCAHeader")).Rows[0][0].ToString() == "0")
                    //    _SqlConn.ScriptExecute(sql, myStrConn);
                    CreateTable(sql, "GFI_FLT_VCAHeader", "VCAID");

                    sql = @"CREATE TABLE GFI_FLT_VCADetail(
							iID number(10) NOT NULL,
							Latitude binary_double NULL,
							Longitude binary_double NULL,
							VCAID number(10) NULL,
							CordOrder number(10) null,
							CONSTRAINT PK_GFI_FLT_VCADetail PRIMARY KEY  
							(iID)
						)";
                    //if (GetDataDT(ExistTableSql("GFI_FLT_VCADetail")).Rows[0][0].ToString() == "0")
                    //    _SqlConn.ScriptExecute(sql, myStrConn);
                    CreateTable(sql, "GFI_FLT_VCADetail", "iID");

                    sql = @"ALTER TABLE GFI_FLT_VCADetail 
							ADD CONSTRAINT FK_GFI_FLT_VCADetail_GFI_FLT_VCAHeader 
								FOREIGN KEY(VCAID)
								REFERENCES GFI_FLT_VCAHeader (VCAID)
							ON DELETE CASCADE";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_VCADetail_GFI_FLT_VCAHeader", "GFI_FLT_VCADetail")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_PLN_PlanningViaPointH 
							ADD CONSTRAINT FK_GFI_PLN_PlanningViaPointH_GFI_FLT_VCAHeader 
								FOREIGN KEY (LocalityVCA) 
								REFERENCES GFI_FLT_VCAHeader (VCAID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_PLN_PlanningViaPointH_GFI_FLT_VCAHeader", "GFI_PLN_PlanningViaPointH")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //             sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'isPointInVCA') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
                    //DROP FUNCTION isPointInVCA";
                    //             _SqlConn.ScriptExecute(sql, myStrConn);

                    //              sql = @"CREATE function isPointInVCA (@Lon float, @Lat float, @iVCAID int)
                    //returns int
                    //as Begin
                    //	Declare @iResult int
                    //	set @iResult = 0

                    //	DECLARE	@Longitude float 
                    //	DECLARE	@Latitude float
                    //	DECLARE	@nLongitude float 
                    //	DECLARE	@nLatitude float
                    //	DECLARE	@myLongitude float 
                    //	DECLARE	@myLatitude float
                    //	set	@myLongitude = @Lon
                    //	set	@myLatitude = @Lat

                    //	declare @c bit
                    //	set @c = 0
                    //	declare xyCursor cursor  LOCAL SCROLL STATIC FOR  
                    //			select d.Latitude, d.Longitude from GFI_FLT_VCAHeader h, GFI_FLT_VCADetail d where h.VCAID = @iVCAID and h.VCAID = d.VCAID order by CordOrder

                    //	open xyCursor
                    //		fetch next from xyCursor into @Latitude, @Longitude
                    //		WHILE @@FETCH_STATUS = 0   
                    //		BEGIN 
                    //		fetch PRIOR from xyCursor into @Latitude, @Longitude
                    //		fetch next from xyCursor into @nLatitude, @nLongitude

                    //					if ((((@Latitude <= @myLatitude) and (@myLatitude < @nLatitude))
                    //							or ((@nLatitude <= @myLatitude) and (@myLatitude < @Latitude)))
                    //							and (@myLongitude < (@nLongitude - @Longitude) * (@myLatitude - @Latitude)
                    //								/ (@nLatitude - @Latitude) + @Longitude))

                    //						if(@c = 0)
                    //							set @c = 1
                    //						else
                    //							set @c = 0
                    //			fetch next from xyCursor into @Latitude, @Longitude
                    //		END
                    //	close xyCursor
                    //	deallocate xyCursor

                    //	if(@c = 0)
                    //		set @iResult = 0
                    //	else
                    //		set @iResult = 1		

                    //	return @iResult
                    //end";
                    //              _SqlConn.ScriptExecute(sql, myStrConn);


                    sql = @"ALTER TABLE GFI_SYS_CustomerMaster ADD VCAID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_CustomerMaster", "VCAID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_CustomerMaster 
							ADD CONSTRAINT FK_GFI_SYS_CustomerMaster_GFI_FLT_VCAHeader 
								FOREIGN KEY (VCAID) 
								REFERENCES GFI_FLT_VCAHeader (VCAID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_CustomerMaster_GFI_FLT_VCAHeader", "GFI_SYS_CustomerMaster")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "VCA");
                }
                #endregion
                #region Update 147. Missing indexes during notif service
                strUpd = "Rez000147";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "CREATE INDEX IX_GFI_GPS_Exceptions_Notif ON GFI_GPS_Exceptions (RuleID,AssetID, DateTimeGPS_UTC)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_Exceptions", "IX_GFI_GPS_Exceptions_Notif")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"DROP INDEX IX_GFI_SYS_Notification_UID_Status_ReportID ON GFI_SYS_Notification";
                    if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "IX_GFI_SYS_Notification_UID_Status_ReportID")).Rows[0][0].ToString() == "1")
                        dbExecute(sql);

                    sql = "CREATE INDEX IX_GFI_SYS_Notification_UID_Status_ReportID ON GFI_SYS_Notification (UID, Status,ReportID)";
                    if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "IX_GFI_SYS_Notification_UID_Status_ReportID")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_SYS_Notification_GPSData_UID ON GFI_SYS_Notification (GPSDataUID)";
                    if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "IX_GFI_SYS_Notification_GPSData_UID")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "Missing indexes during notif service");
                }
                #endregion
                #region Update 148. User Additional fields
                strUpd = "Rez000148";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_SYS_User ADD ZoneID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "ZoneID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_User 
							ADD CONSTRAINT FK_GFI_SYS_User_GFI_FLT_ZoneHeader 
								FOREIGN KEY(ZoneID) 
								REFERENCES GFI_FLT_ZoneHeader(ZoneID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_User_GFI_FLT_ZoneHeader", "GFI_SYS_User")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    sql = @"ALTER TABLE GFI_SYS_User ADD PlnLkpVal int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "PlnLkpVal")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_User 
							ADD CONSTRAINT FK_GFI_SYS_User_GFI_SYS_LookUpValues_PLN 
								FOREIGN KEY(PlnLkpVal) 
								REFERENCES GFI_SYS_LookUpValues (VID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_User_GFI_SYS_LookUpValues_PLN", "GFI_SYS_User")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);


                    InsertDBUpdate(strUpd, "User Additional fields");
                }
                #endregion
                #region Update 149. UOM MOH
                strUpd = "Rez000149";
                if (!CheckUpdateExist(strUpd))
                {
                    UOM u = new UOM();
                    u = uomService.GetUOMByDescription("Sitting", sConnStr);
                    if (u == null)
                    {
                        u = new UOM();
                        u.Description = "Sitting";
                        uomService.SaveUOM(u, true, sConnStr);
                    }

                    u = new UOM();
                    u = uomService.GetUOMByDescription("Lying", sConnStr);
                    if (u == null)
                    {
                        u = new UOM();
                        u.Description = "Lying";
                        uomService.SaveUOM(u, true, sConnStr);
                    }

                    VehicleType objVehicleTypes = new VehicleType();
                    VehicleTypesService vehicleTypesService = new VehicleTypesService();
                    objVehicleTypes = vehicleTypesService.GetVehicleTypesByDescription("Ambulance", sConnStr);
                    if (objVehicleTypes == null)
                    {
                        objVehicleTypes = new VehicleType();
                        objVehicleTypes.VehicleTypeId = 7;
                        objVehicleTypes.Description = "Ambulance";
                        objVehicleTypes.CreatedDate = DateTime.Now;
                        objVehicleTypes.CreatedBy = usrInst.Username;
                        objVehicleTypes.UpdatedDate = DateTime.Now;
                        objVehicleTypes.UpdatedBy = usrInst.Username;
                        vehicleTypesService.SaveVehicleTypes(objVehicleTypes, null, sConnStr);
                    }

                    InsertDBUpdate(strUpd, "UOM MOH");
                }
                #endregion

                #region Update 150. MOH Roster
                strUpd = "Rez000150";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_RST_Shift(
								SID number(10) NOT NULL,
								sName nvarchar2(255) NULL,
								sComments nvarchar2(500) NULL,
								TimeFr Timestamp(3) NOT NULL,
								TimeTo Timestamp(3) NOT NULL,
							CONSTRAINT PK_GFI_RST_Shift PRIMARY KEY  
								(SID)
						 )";
                    //if (GetDataDT(ExistTableSql("GFI_RST_Shift")).Rows[0][0].ToString() == "0")
                    //    _SqlConn.ScriptExecute(sql, myStrConn);
                    CreateTable(sql, "GFI_RST_Shift", "SID");

                    sql = @"CREATE TABLE GFI_RST_Teams(
								TID number(10) NOT NULL,
								sName nvarchar2(255) NULL,
								sComments nvarchar2(500) NULL,
								GUID nvarchar2 (255) NULL,
							CONSTRAINT PK_GFI_RST_Teams PRIMARY KEY  
								(TID)
						 )";
                    //if (GetDataDT(ExistTableSql("GFI_RST_Teams")).Rows[0][0].ToString() == "0")
                    //    dbExecute(sql);
                    CreateTable(sql, "GFI_RST_Teams", "TID");

                    sql = @"CREATE TABLE GFI_RST_TeamsResource(
								iID number(10) NOT NULL,
								TID number(10) not NULL,
								DriverID number(10) not NULL,
							CONSTRAINT PK_GFI_RST_TeamsResource PRIMARY KEY  
								(iID)
						 )";
                    //if (GetDataDT(ExistTableSql("GFI_RST_TeamsResource")).Rows[0][0].ToString() == "0")
                    //    dbExecute(sql);
                    CreateTable(sql, "GFI_RST_TeamsResource", "iID");

                    sql = @"CREATE TABLE GFI_RST_TeamsAsset(
								iID number(10) NOT NULL,
								TID number(10) not NULL,
								AssetID number(10) not NULL,
							CONSTRAINT PK_GFI_RST_TeamsAsset PRIMARY KEY  
								(iID)
						 )";
                    //if (GetDataDT(ExistTableSql("GFI_RST_TeamsAsset")).Rows[0][0].ToString() == "0")
                    //    dbExecute(sql);
                    CreateTable(sql, "GFI_RST_TeamsAsset", "iID");

                    sql = @"CREATE TABLE GFI_RST_Roster(
								RID number(10) NOT NULL,
								GRID number(10) not NULL,
								SID number(10) not NULL,
								TeamID number(10) null,
								rDate Timestamp(3) NOT NULL,
								isLocked number(10),
								isOriginal number(10),
							CONSTRAINT PK_GFI_RST_Roster PRIMARY KEY  
								(RID)
						 ) ";
                    //if (GetDataDT(ExistTableSql("GFI_RST_Roster")).Rows[0][0].ToString() == "0")
                    //    dbExecute(sql);
                    CreateTable(sql, "GFI_RST_Roster", "RID");

                    sql = @"ALTER TABLE GFI_RST_Roster ADD DriverID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_RST_Roster", "DriverID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_RST_Roster ADD AssetID int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_RST_Roster", "AssetID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_RST_Roster MODIFY TeamID int";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_RST_TeamsResource 
							ADD CONSTRAINT FK_GFI_RST_TeamsResource_GFI_FLT_Driver 
								FOREIGN KEY (DriverID) 
								REFERENCES GFI_FLT_Driver (DriverID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_RST_TeamsResource_GFI_FLT_Driver", "GFI_RST_TeamsResource")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_RST_TeamsResource 
							ADD CONSTRAINT FK_GFI_RST_TeamsResource_GFI_RST_Teams
								FOREIGN KEY (TID) 
								REFERENCES GFI_RST_Teams(TID)
							 ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_RST_TeamsResource_GFI_RST_Teams", "GFI_RST_TeamsResource")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_RST_TeamsAsset 
							ADD CONSTRAINT FK_GFI_RST_TeamsAsset_GFI_FLT_Asset 
								FOREIGN KEY (AssetID) 
								REFERENCES GFI_FLT_Asset (AssetID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_RST_TeamsAsset_GFI_FLT_Asset", "GFI_RST_TeamsAsset")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_RST_TeamsAsset 
							ADD CONSTRAINT FK_GFI_RST_TeamsAsset_GFI_RST_Teams 
								FOREIGN KEY (TID) 
								REFERENCES GFI_RST_Teams (TID) 
							 ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_RST_TeamsAsset_GFI_RST_Teams", "GFI_RST_TeamsAsset")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_RST_Roster 
							ADD CONSTRAINT FK_GFI_RST_Roster_GFI_RST_Teams 
								FOREIGN KEY (TeamID) 
								REFERENCES GFI_RST_Teams (TID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_RST_Roster_GFI_RST_Teams", "GFI_RST_Roster")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_RST_Roster 
							ADD CONSTRAINT FK_GFI_RST_Roster_GFI_FLT_Driver 
								FOREIGN KEY (DriverID) 
								REFERENCES GFI_FLT_Driver (DriverID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_RST_Roster_GFI_FLT_Driver", "GFI_RST_Roster")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_RST_Roster 
							ADD CONSTRAINT FK_GFI_RST_Roster_GFI_FLT_Asset 
								FOREIGN KEY (AssetID) 
								REFERENCES GFI_FLT_Asset (AssetID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_RST_Roster_GFI_FLT_Asset", "GFI_RST_Roster")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_RST_Roster 
							ADD CONSTRAINT FK_GFI_RST_Roster_GFI_RST_Shift 
								FOREIGN KEY (SID) 
								REFERENCES GFI_RST_Shift (SID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_RST_Roster_GFI_RST_Shift", "GFI_RST_Roster")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_RST_Roster Add DynTeamID int";
                    if (GetDataDT(ExistColumnSql("GFI_RST_Roster", "DynTeamID")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //GFI_RST_RosterHeader
                    sql = @"CREATE TABLE GFI_RST_RosterHeader(
								iID number(10) NOT NULL,
								dtFrom timestamp(3) NULL,
								dtTo timestamp(3) NULL,
								sRemarks nvarchar2 (255) NULL,
							CONSTRAINT PK_GFI_RST_RosterHeader PRIMARY KEY  (iID)
						 )";
                    //if (GetDataDT(ExistTableSql("GFI_RST_RosterHeader")).Rows[0][0].ToString() == "0")
                    //    _SqlConn.ScriptExecute(sql, myStrConn);
                    CreateTable(sql, "GFI_RST_RosterHeader", "iID");

                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_RST_Roster_GFI_RST_RosterHeader", "GFI_RST_RosterHeader")).Rows.Count == 0)
                    {
                        sql = "delete from GFI_RST_Roster";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                        sql = @"ALTER TABLE GFI_RST_Roster 
							ADD CONSTRAINT FK_GFI_RST_Roster_GFI_RST_RosterHeader 
								FOREIGN KEY (GRID) 
								REFERENCES GFI_RST_RosterHeader (iID) 
							 ON DELETE CASCADE ";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    }

                    sql = "Alter table GFI_RST_RosterHeader add isLocked int ";
                    if (GetDataDT(ExistColumnSql("GFI_RST_RosterHeader", "isLocked")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "Alter table GFI_RST_RosterHeader add isOriginal int";
                    if (GetDataDT(ExistColumnSql("GFI_RST_RosterHeader", "isOriginal")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "alter table GFI_RST_Roster drop column isLocked";
                    if (GetDataDT(ExistColumnSql("GFI_RST_Roster", "isLocked")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "alter table GFI_RST_Roster drop column isOriginal";
                    if (GetDataDT(ExistColumnSql("GFI_RST_Roster", "isOriginal")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "MOH Roster");
                }
                #endregion
                #region Update 151. Audit keys and VCA index
                strUpd = "Rez000151";
                if (!CheckUpdateExist(strUpd))
                {
                    //            //GFI_SYS_AuditAccess
                    //            sql = "ALTER TABLE GFI_SYS_AuditAccess ADD CONSTRAINT PK_GFI_SYS_AuditAccess PRIMARY KEY  (iID) ";
                    //            if (GetDataDT(ExistsSQLConstraint("PK_GFI_SYS_AuditAccess", "GFI_SYS_AuditAccess")).Rows.Count == 0)
                    //                dbExecute(sql);

                    //            sql = @"SELECT object_name(default_object_id)
                    //FROM sys.columns
                    //WHERE object_id = object_id('[GFI_SYS_AuditAccess]')
                    //  AND name = 'UserID'";
                    //            dt = GetDataDT(sql);
                    //            if (dt.Rows.Count > 0)
                    //            {
                    //                String strConstrainName = dt.Rows[0][0].ToString();
                    //                if (strConstrainName.Length > 0)
                    //                {
                    //                    sql = "ALTER TABLE GFI_SYS_AuditAccess] DROP FOREIGN KEY " + strConstrainName;
                    //                    dbExecute(sql);
                    //                }
                    //            }

                    //            //GFI_SYS_LoginAudit
                    //            sql = "ALTER TABLE GFI_SYS_LoginAudit ADD CONSTRAINT PK_GFI_SYS_LoginAudit PRIMARY KEY  (auditId) ";
                    //            if (GetDataDT(ExistsSQLConstraint("PK_GFI_SYS_LoginAudit", "GFI_SYS_LoginAudit")).Rows.Count == 0)
                    //                dbExecute(sql);

                    //            sql = @"SELECT object_name(default_object_id)
                    //FROM sys.columns
                    //WHERE object_id = object_id('GFI_SYS_LoginAudit')
                    //  AND name = 'UserID'";
                    //            dt = GetDataDT(sql);
                    //            if (dt.Rows.Count > 0)
                    //            {
                    //                String strConstrainName = dt.Rows[0][0].ToString();
                    //                if (strConstrainName.Length > 0)
                    //                {
                    //                    sql = "ALTER TABLE GFI_SYS_LoginAudit DROP FOREIGN KEY " + strConstrainName;
                    //                    dbExecute(sql);
                    //                }
                    //            }

                    //            //GFI_SYS_Audit
                    //            sql = "ALTER TABLE GFI_SYS_Audit ADD CONSTRAINT PK_GFI_SYS_Audit PRIMARY KEY  (auditId) ";
                    //            if (GetDataDT(ExistsSQLConstraint("PK_GFI_SYS_Audit", "GFI_SYS_Audit")).Rows.Count == 0)
                    //                dbExecute(sql);

                    //            sql = @"SELECT object_name(default_object_id)
                    //FROM sys.columns
                    //WHERE object_id = object_id('GFI_SYS_Audit')
                    //  AND name = 'UserID'";
                    //            dt = GetDataDT(sql);
                    //            if (dt.Rows.Count > 0)
                    //            {
                    //                String strConstrainName = dt.Rows[0][0].ToString();
                    //                if (strConstrainName.Length > 0)
                    //                {
                    //                    sql = "ALTER TABLE GFI_SYS_Audit] DROP FOREIGN KEY " + strConstrainName;
                    //                    dbExecute(sql);
                    //                }
                    //            }

                    sql = "CREATE  INDEX IX_GFI_FLT_VCADetail_CordOrder ON GFI_FLT_VCADetail (CordOrder)";
                    if (GetDataDT(ExistsIndex("GFI_FLT_VCADetail", "IX_GFI_FLT_VCADetail_CordOrder")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "Alter table GFI_FLT_VCAHeader add ParentVCAID int null";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_VCAHeader", "ParentVCAID")).Rows[0][0].ToString() == "0")
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_FLT_VCAHeader 
							ADD CONSTRAINT FK_GFI_FLT_VCAHeader_Parent 
								FOREIGN KEY(ParentVCAID) 
								REFERENCES GFI_FLT_VCAHeader(VCAID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_VCAHeader_Parent", "GFI_FLT_VCAHeader")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrix 
							ADD CONSTRAINT FK_GFI_SYS_GroupMatrix_Parent 
								FOREIGN KEY(ParentGMID) 
								REFERENCES GFI_SYS_GroupMatrix(GMID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrix_Parent", "GFI_SYS_GroupMatrix")).Rows.Count == 0)
                        dbExecute(sql);

                    GlobalParam g = new GlobalParam();
                    g = globalParamsService.GetGlobalParamsByName("WebLogo", sConnStr);
                    if (g == null)
                    {
                        g = new GlobalParam();
                        g.ParamName = "WebLogo";
                        g.PValue = "~/Resources/Images/Logo/NaveoOne.jpg";
                        g.ParamComments = "Web Logo for login page";
                        globalParamsService.SaveGlobalParams(g, true, sConnStr);
                    }

                    InsertDBUpdate(strUpd, "Audit keys and VCA index");
                }
                #endregion
                #region Update 152. RoadSpeed in debug
                strUpd = "Rez000152";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_GPS_GPSData ADD RoadSpeed int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_GPSData", "RoadSpeed")).Rows[0][0].ToString() == "0")
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_ARC_GPSData ADD RoadSpeed int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_ARC_GPSData", "RoadSpeed")).Rows[0][0].ToString() == "0")
                        dbExecute(sql);

                    GlobalParam g = new GlobalParam();
                    g = globalParamsService.GetGlobalParamsByName("RoadSpeed", sConnStr);
                    if (g == null)
                    {
                        g = new GlobalParam();
                        g.ParamName = "RoadSpeed";
                        g.PValue = "0";
                        g.ParamComments = "RoadSpeed GPS Data ID, updated by service";
                        globalParamsService.SaveGlobalParams(g, true, sConnStr);
                    }

                    InsertDBUpdate(strUpd, "RoadSpeed in debug");
                }
                #endregion
                #region Update 153. District int in CustomerMaster
                strUpd = "Rez000153";
                if (!CheckUpdateExist(strUpd))
                {
                    dt = GetDataDT(GetFieldSchema("GFI_SYS_CustomerMaster", "District"));
                    if (dt.Rows[0]["DATA_TYPE"].ToString() != "int")
                    {
                        sql = @"ALTER TABLE GFI_SYS_CustomerMaster MODIFY District int";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                        sql = @"ALTER TABLE GFI_SYS_CustomerMaster 
								ADD CONSTRAINT FK_GFI_SYS_CustomerMaster_GFI_SYS_LookUpValues 
									FOREIGN KEY (District) 
									REFERENCES GFI_SYS_LookUpValues (VID)";
                        if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_CustomerMaster_GFI_SYS_LookUpValues", "GFI_SYS_CustomerMaster")).Rows.Count == 0)
                            _SqlConn.OracleScriptExecute(sql, myStrConn);
                    }

                    UOM u = new UOM();
                    u = uomService.GetUOMByDescription("Boxes", sConnStr);
                    if (u == null)
                    {
                        u = new UOM();
                        u.Description = "Boxes";
                        uomService.SaveUOM(u, true, sConnStr);
                    }

                    InsertDBUpdate(strUpd, "District int in CustomerMaster");
                }
                #endregion
                #region Update 154. Telemetry Table updates
                strUpd = "Rez000154";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_TEL_TelDataHeader MODIFY MsgHeader nvarchar2(10) NULL";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);
                    sql = @"ALTER TABLE GFI_TEL_TelDataHeader MODIFY Type nvarchar2(20) NULL";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);
                    sql = @"ALTER TABLE GFI_TEL_TelDataHeader MODIFY MsgStatus nvarchar2(10) NULL";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);
                    sql = @"ALTER TABLE GFI_TEL_TelDataHeader MODIFY isMaintenanceMode number(10) NULL";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);
                    sql = @"ALTER TABLE GFI_TEL_TelDataHeader MODIFY isFailedCallout number(10) NULL";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);
                    sql = @"ALTER TABLE GFI_TEL_TelDataHeader MODIFY Temperature float NULL";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);
                    sql = @"ALTER TABLE GFI_TEL_TelDataHeader MODIFY isBatteryLow number(10) NULL";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);
                    sql = @"ALTER TABLE GFI_TEL_TelDataHeader MODIFY isAutoConfig INT NULL";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);
                    sql = @"ALTER TABLE GFI_TEL_TelDataHeader MODIFY Carrier nvarchar2(50) NULL";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);
                    sql = @"ALTER TABLE GFI_TEL_TelDataHeader MODIFY SignalStrength number(10) NULL";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "Telemetry Table updates");
                }
                #endregion
                #region Update 155. Default MOH Shifts and missing indexes
                strUpd = "Rez000155";
                ShiftService shiftService = new ShiftService();
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "CREATE INDEX IX_GFI_PLN_PlanningViaPointD_HID ON GFI_PLN_PlanningViaPointD (HID)";
                    if (GetDataDT(ExistsIndex("GFI_PLN_PlanningViaPointD_HID", "IX_GFI_PLN_PlanningViaPointD_HID")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    Shift s = shiftService.GetShiftByName("D", sConnStr);
                    if (s == null)
                    {
                        s = new Shift();
                        s.sName = "D";
                        s.sComments = "Day shift";
                        s.TimeFr = Convert.ToDateTime("2017-04-08 08:00:00.000");
                        s.TimeTo = Convert.ToDateTime("2017-04-08 16:00:00.000");
                        shiftService.SaveShift(s, true, sConnStr);
                    }

                    s = new Shift();
                    s = shiftService.GetShiftByName("N", sConnStr);
                    if (s == null)
                    {
                        s = new Shift();
                        s.sName = "N";
                        s.sComments = "Night shift";
                        s.TimeFr = Convert.ToDateTime("2017-04-08 16:00:00.001");
                        s.TimeTo = Convert.ToDateTime("2017-04-08 07:00:59.000");
                        shiftService.SaveShift(s, true, sConnStr);
                    }

                    InsertDBUpdate(strUpd, "Default MOH Shifts and missing indexes");
                }
                #endregion
                #region Update 156. sp_AMM_VehicleMaint_ProcessRecords updated
                strUpd = "Rez000156";
                if (!CheckUpdateExist(strUpd))
                {
                    //                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'sp_AMM_VehicleMaint_ProcessRecords') AND type in (N'P', N'PC'))
                    //							DROP PROCEDURE sp_AMM_VehicleMaint_ProcessRecords";
                    //                    dbExecute(sql);

                    //                    sql = @"CREATE PROCEDURE sp_AMM_VehicleMaint_ProcessRecords]
                    //AS
                    //BEGIN
                    //	/***********************************************************************************************************************
                    //	Version: 3.1.0.0
                    //	Modifed: 11/ 08/2016.	06/08/2014
                    //	Created By: Perry Ramen
                    //	Modified By: Reza Dowlut
                    //	Description: Process list of Maintenance Types for each vehicle to determine if they are To Schedule] or Overdue]
                    //	***********************************************************************************************************************/
                    //		BEGIN
                    //		DECLARE @iRecCountProcessed int

                    //		DECLARE @iMaintStatusId_Overdue int
                    //		DECLARE @iMaintStatusId_ToSchedule int
                    //		DECLARE @iMaintStatusId_Scheduled int
                    //		DECLARE @iMaintStatusId_InProgress int
                    //		DECLARE @iMaintStatusId_Completed int
                    //		DECLARE @iMaintStatusId_Valid int
                    //		DECLARE @iMaintStatusId_Expired int

                    //		DECLARE @URI int
                    //		DECLARE @MaintURI int
                    //		DECLARE @AssetId int
                    //		DECLARE @MaintTypeId int
                    //		DECLARE @OccurrenceType int
                    //		DECLARE @NextMaintDate DateTime
                    //		DECLARE @NextMaintDateTG DateTime 
                    //		DECLARE @CurrentOdometer int
                    //		DECLARE @NextMaintOdometer int
                    //		DECLARE @NextMaintOdometerTG int
                    //		DECLARE @CurrentEngHrs int
                    //		DECLARE @NextMaintEngHrs int
                    //		DECLARE @NextMaintEngHrsTG int
                    //		DECLARE @MaintStatusId int

                    //		DECLARE @MaintURIUpdate int

                    //		DECLARE @OccurrenceDuration int
                    //		DECLARE @OccurrenceDurationTh int
                    //		DECLARE @OccurrencePeriod_cbo nvarchar(50)

                    //		SET @iMaintStatusId_Overdue = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Overdue')
                    //		SET @iMaintStatusId_ToSchedule = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'To Schedule')
                    //		SET @iMaintStatusId_Scheduled = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Scheduled')
                    //		SET @iMaintStatusId_InProgress = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'In Progress')		
                    //		SET @iMaintStatusId_Completed = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Completed')				
                    //		SET @iMaintStatusId_Valid = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Valid')		
                    //		SET @iMaintStatusId_Expired = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Expired')				

                    //		update GFI_AMM_VehicleMaintTypesLink set CurrentOdometer = GetCalcOdo(AssetId)

                    //		DECLARE csrData CURSOR FOR
                    //		SELECT URI, 
                    //			vmtl.MaintURI,
                    //			vmtl.AssetId,
                    //			vmtl.MaintTypeId,
                    //			vmt.OccurrenceType,
                    //			vmtl.NextMaintDate, 
                    //			vmtl.NextMaintDateTG, 
                    //			vmtl.CurrentOdometer, 
                    //			vmtl.NextMaintOdometer, 
                    //			vmtl.NextMaintOdometerTG, 
                    //			vmtl.CurrentEngHrs,
                    //			vmtl.NextMaintEngHrs,
                    //			vmtl.NextMaintEngHrsTG,
                    //			vmtl.[Status]
                    //			, vmt.OccurrenceDuration
                    //			, vmt.OccurrenceDurationTh
                    //			, vmt.OccurrencePeriod_cbo
                    //		FROM GFI_AMM_VehicleMaintTypesLink vmtl
                    //			INNER JOIN GFI_AMM_VehicleMaintTypes vmt ON vmtl.MaintTypeId = vmt.MaintTypeId

                    //		OPEN csrData
                    //		FETCH NEXT FROM csrData INTO @URI, @MaintURI, @AssetId, @MaintTypeId, @OccurrenceType, @NextMaintDate, @NextMaintDateTG, @CurrentOdometer, @NextMaintOdometer, @NextMaintOdometerTG, @CurrentEngHrs, @NextMaintEngHrs, @NextMaintEngHrsTG, @MaintStatusId
                    //			, @OccurrenceDuration, @OccurrenceDurationTh, @OccurrencePeriod_cbo
                    //		SET @iRecCountProcessed = 0

                    //		WHILE @@FETCH_STATUS = 0
                    //		BEGIN
                    //			IF @NextMaintDate IS NULL SET @NextMaintDate = GETDATE()

                    //			--select @URI, 'OccurrenceType 2', @OccurrenceType
                    //			--	, 'AND'
                    //			--	, '@NextMaintDateTG <= GETDATE()', @NextMaintDateTG, GETDATE()
                    //			--	, '@NextMaintOdometerTG < @CurrentOdometer', @NextMaintOdometerTG, @CurrentOdometer
                    //			--	, '@NextMaintEngHrsTG < @CurrentEngHrs', @NextMaintEngHrsTG, @CurrentEngHrs
                    //			--	, 'AND'
                    //			--	, '@MaintStatusId = @iMaintStatusId_Completed', @MaintStatusId, @iMaintStatusId_Completed
                    //			--	, '@MaintStatusId = @iMaintStatusId_Valid', @MaintStatusId, @iMaintStatusId_Valid

                    //			-- Threhold by date range
                    //			declare @Maintdate DateTime
                    //			declare @Thershold int
                    //			set @Thershold = @OccurrenceDurationTh * -1 -- - @OccurrenceDuration
                    //			set @Maintdate = DateAdd(month, @Thershold, GETDATE());
                    //			if @OccurrencePeriod_cbo = 'DAY(S)' 
                    //				set @Maintdate = DateAdd(day, @Thershold, GETDATE());

                    //			select @Thershold, @NextMaintDate, @Maintdate
                    //			--if(@Thershold != 0 and @NextMaintDate <= @Maintdate)
                    //			--	select '*************************************************'

                    //			-- Recurring - Flag As: To Schedule
                    //			IF (
                    //				(@OccurrenceType = 2) 
                    //				AND ((@NextMaintDateTG <= GETDATE()) OR (@NextMaintOdometerTG < @CurrentOdometer) OR (@NextMaintEngHrsTG < @CurrentEngHrs) OR (@Thershold != 0 and @NextMaintDate <= @Maintdate)) 
                    //				AND ((@MaintStatusId = @iMaintStatusId_Completed) OR (@MaintStatusId = @iMaintStatusId_Valid)))
                    //			BEGIN
                    //				IF NOT EXISTS(SELECT * FROM GFI_AMM_VehicleMaintTypesLink WHERE AssetId = @AssetId AND MaintTypeId = @MaintTypeId AND URI > @URI)
                    //				BEGIN
                    //					IF (@MaintStatusId = @iMaintStatusId_Completed)
                    //					BEGIN
                    //						INSERT GFI_AMM_VehicleMaintenance (AssetId, MaintTypeId_cbo, MaintStatusId_cbo, StartDate, EndDate, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy) 
                    //						VALUES (@AssetId, @MaintTypeId, @iMaintStatusId_ToSchedule, @NextMaintDate, @NextMaintDate, GETDATE(), CURRENT_USER, GETDATE(), CURRENT_USER)

                    //						SET @MaintURIUpdate = (SELECT SCOPE_IDENTITY())

                    //						UPDATE GFI_AMM_VehicleMaintTypesLink
                    //						SET MaintURI = @MaintURIUpdate, Status = @iMaintStatusId_ToSchedule, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
                    //						WHERE URI = @URI
                    //					END
                    //					ELSE IF (@MaintStatusId = @iMaintStatusId_Valid)
                    //					BEGIN
                    //						SET @NextMaintDate = DATEADD(DD, 1, @NextMaintDate)

                    //						INSERT GFI_AMM_VehicleMaintenance (AssetId, MaintTypeId_cbo, MaintStatusId_cbo, StartDate, EndDate, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy) 
                    //						VALUES (@AssetId, @MaintTypeId, @iMaintStatusId_ToSchedule, @NextMaintDate, @NextMaintDate, GETDATE(), CURRENT_USER, GETDATE(), CURRENT_USER)

                    //						SET @MaintURIUpdate = (SELECT SCOPE_IDENTITY())

                    //						INSERT GFI_AMM_VehicleMaintTypesLink (MaintURI, AssetId, MaintTypeId, NextMaintDate, NextMaintDateTG, Status, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy)
                    //						VALUES (@MaintURIUpdate, @AssetId, @MaintTypeId, @NextMaintDate, @NextMaintDate, @iMaintStatusId_ToSchedule, GETDATE(), CURRENT_USER, GETDATE(), CURRENT_USER)
                    //					END
                    //				END
                    //			END

                    //			--Flag As: Overdue
                    //			IF (((@NextMaintDate <= GETDATE()) OR (@NextMaintOdometer < @CurrentOdometer) OR (@NextMaintEngHrs < @CurrentEngHrs)) AND (@MaintStatusId = @iMaintStatusId_ToSchedule))
                    //			BEGIN				
                    //				UPDATE GFI_AMM_VehicleMaintTypesLink
                    //				SET Status = @iMaintStatusId_Overdue, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
                    //				WHERE URI = @URI

                    //				UPDATE GFI_AMM_VehicleMaintenance 
                    //				SET MaintStatusId_cbo = @iMaintStatusId_Overdue, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
                    //				WHERE URI = @MaintURI
                    //			END

                    //			--Auto Expiry: Lapsed Insurance
                    //			IF ((@MaintStatusId = @iMaintStatusId_Valid) AND (@NextMaintDate < GETDATE()))
                    //			BEGIN
                    //				UPDATE GFI_AMM_VehicleMaintTypesLink
                    //				SET Status = @iMaintStatusId_Expired , UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
                    //				WHERE URI = @URI

                    //				UPDATE GFI_AMM_VehicleMaintenance 
                    //				SET MaintStatusId_cbo = @iMaintStatusId_Expired, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
                    //				WHERE URI = @MaintURI
                    //			END

                    //			SET @iRecCountProcessed = @iRecCountProcessed + 1

                    //			FETCH NEXT FROM csrData INTO @URI, @MaintURI, @AssetId, @MaintTypeId, @OccurrenceType, @NextMaintDate, @NextMaintDateTG, @CurrentOdometer, @NextMaintOdometer, @NextMaintOdometerTG, @CurrentEngHrs, @NextMaintEngHrs, @NextMaintEngHrsTG, @MaintStatusId
                    //				, @OccurrenceDuration, @OccurrenceDurationTh, @OccurrencePeriod_cbo
                    //		END

                    //		CLOSE csrData
                    //		DEALLOCATE csrData

                    //		RETURN @iRecCountProcessed
                    //	END
                    //END
                    //";
                    //                    dbExecute(sql);
                    InsertDBUpdate(strUpd, "sp_AMM_VehicleMaint_ProcessRecords");
                }
                #endregion
                #region Update 157. isPointInVCA optimized
                strUpd = "Rez000157";
                if (!CheckUpdateExist(strUpd))
                {
                    #region isPointInVCA optimized
                    //                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[isPointInVCA]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
                    //							DROP FUNCTION isPointInVCA]";

                    //                    dbExecute(sql);
                    //                    sql = @"
                    //Create function isPointInVCA] (@Lon float, @Lat float, @iVCAID int)
                    //returns int
                    //as Begin
                    //	--Extremity Chk
                    //	Declare @iPossibleChk int
                    //	select  @iPossibleChk = VCAID from 
                    //		(select VCAID, min(Latitude) minLat, min(Longitude) minLon, max(Latitude) maxLat, max(Longitude) maxLon  
                    //			from GFI_FLT_VCADetail where VCAID = @iVCAID
                    //			group by VCAID
                    //		) myView
                    //	where 1 = 1
                    //		and (@Lon between minLon and maxLon)
                    //		and (@Lat between minLat and maxLat)

                    //	if(@iPossibleChk is null)
                    //		return 0
                    //	--

                    //	Declare @iResult int
                    //	set @iResult = 0

                    //	DECLARE	@Longitude float] 
                    //	DECLARE	@Latitude float]
                    //	DECLARE	@nLongitude float] 
                    //	DECLARE	@nLatitude float]
                    //	DECLARE	@myLongitude float] 
                    //	DECLARE	@myLatitude float]
                    //	set	@myLongitude = @Lon
                    //	set	@myLatitude = @Lat

                    //	declare @c bit
                    //	set @c = 0

                    //	declare xyCursor cursor  LOCAL SCROLL STATIC FOR  
                    //			select Latitude, Longitude from GFI_FLT_VCADetail
                    //			where VCAID = @iVCAID order by CordOrder

                    //	open xyCursor
                    //		fetch next from xyCursor into @Latitude, @Longitude
                    //		WHILE @@FETCH_STATUS = 0   
                    //		BEGIN 
                    //		fetch PRIOR from xyCursor into @Latitude, @Longitude
                    //		fetch next from xyCursor into @nLatitude, @nLongitude

                    //					if ((((@Latitude <= @myLatitude) and (@myLatitude < @nLatitude))
                    //							or ((@nLatitude <= @myLatitude) and (@myLatitude < @Latitude)))
                    //							and (@myLongitude < (@nLongitude - @Longitude) * (@myLatitude - @Latitude)
                    //								/ (@nLatitude - @Latitude) + @Longitude))

                    //						if(@c = 0)
                    //							set @c = 1
                    //						else
                    //							set @c = 0

                    //						if(@c = 1)
                    //						begin
                    //							Break;
                    //						end

                    //			fetch next from xyCursor into @Latitude, @Longitude
                    //		END
                    //	close xyCursor
                    //	deallocate xyCursor

                    //	if(@c = 0)
                    //		set @iResult = 0
                    //	else
                    //		set @iResult = 1		

                    //	return @iResult
                    //end";
                    //                    _SqlConn.ScriptExecute(sql, myStrConn);
                    #endregion


                    InsertDBUpdate(strUpd, "isPointInVCA optimized");
                }
                #endregion
                #region Update 158. TelDataDetails updated
                strUpd = "Rez000158";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_TEL_TelDataDetail ADD SensorAddress number(10) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_TEL_TelDataDetail", "SensorAddress")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_TEL_TelDataDetail ADD UOM number(10) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_TEL_TelDataDetail", "UOM")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_TEL_TelDataDetail 
							ADD CONSTRAINT FK_GFI_TEL_TelDataDetail_GFI_SYS_UOM
								FOREIGN KEY (UOM) 
								REFERENCES GFI_SYS_UOM (UID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_TEL_TelDataDetail_GFI_SYS_UOM", "GFI_TEL_TelDataDetail")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "TelDataDetails updated");
                }
                #endregion
                #region Update 159. TwoWayAuthenticationOnSSL
                strUpd = "Rez000159";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_SYS_User ADD CONSTRAINT UK_GFI_SYS_User_UN Unique (Username)";
                    if (GetDataDT(ExistsSQLConstraint("UK_GFI_SYS_User_UN", "GFI_SYS_User")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_User ADD CONSTRAINT UK_GFI_SYS_User_Email Unique (Email)";
                    if (GetDataDT(ExistsSQLConstraint("UK_GFI_SYS_User_Email", "GFI_SYS_User")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_User ADD MobileAccess int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "MobileAccess")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_User ADD ExternalAccess int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "ExternalAccess")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_GlobalParam DROP FOREIGN KEY U_dbo_GFI_SYS_GlobalParam_1";
                    if (GetDataDT(ExistsSQLConstraint("U_dbo_GFI_SYS_GlobalParam_1", "GFI_SYS_GlobalParam")).Rows.Count != 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //sql = @"ALTER TABLE GFI_SYS_GlobalParam alter column ParamName nvarchar2(35) NOT NULL";
                    //dt = GetDataDT(GetFieldSchema("GFI_SYS_GlobalParam", "ParamName"));
                    //if (dt.Rows[0]["CHARACTER_MAXIMUM_LENGTH"].ToString() != "35")
                    //    _SqlConn.ScriptExecute(sql, myStrConn);


                    sql = @"ALTER TABLE GFI_SYS_GlobalParam ADD CONSTRAINT UK_GFI_SYS_GlobalParam_PN Unique (ParamName)";
                    if (GetDataDT(ExistsSQLConstraint("UK_GFI_SYS_GlobalParam_PN", "GFI_SYS_GlobalParam")).Rows.Count != 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    GlobalParam g = new GlobalParam();
                    g = globalParamsService.GetGlobalParamsByName("TwoWayAuthenticationEnabledOnSSL", sConnStr);
                    if (g == null)
                    {
                        g = new GlobalParam();
                        g.ParamName = "TwoWayAuthenticationEnabledOnSSL";
                        g.PValue = "1";
                        g.ParamComments = "1 enabled";
                        globalParamsService.SaveGlobalParams(g, true, sConnStr);
                    }

                    InsertDBUpdate(strUpd, "TwoWayAuthenticationOnSSL");
                }
                #endregion
                #region Update 160. Driver Licenses
                strUpd = "Rez000160";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_FLT_ResourceLicenses(
								iID number(10) NOT NULL,
								DriverID number(10) NOT NULL,
								sType nvarchar2(50) NULL,
								sCategory nvarchar2(50) NULL,
								sNumber nvarchar2(50) NULL,
								IssueDate Timestamp(3) NULL,
								ExpiryDate Timestamp(3) NULL,
							CONSTRAINT PK_GFI_FLT_ResourceLicenses PRIMARY KEY  
								(iID)
						 )";
                    //if (GetDataDT(ExistTableSql("GFI_FLT_ResourceLicenses")).Rows[0][0].ToString() == "0")
                    //    dbExecute(sql);
                    CreateTable(sql, "GFI_FLT_ResourceLicenses", "iID");

                    sql = @"ALTER TABLE GFI_FLT_ResourceLicenses 
							ADD CONSTRAINT FK_GFI_FLT_ResourceLicenses_GFI_FLT_Driver 
								FOREIGN KEY(DriverID) 
								REFERENCES GFI_FLT_Driver(DriverID)
						 ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_ResourceLicenses_GFI_FLT_Driver", "GFI_FLT_ResourceLicenses")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "Vehicle");
                }
                #endregion
                #region Update 161. Pln Live indexes
                strUpd = "Rez000161";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"DROP INDEX IX_Live_GFI_PLN_PlanningViaPointH";
                    if (GetDataDT(ExistsIndex("GFI_PLN_PlanningViaPointH", "IX_Live_GFI_PLN_PlanningViaPointH")).Rows[0][0].ToString() == "1")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"DROP INDEX IX_Live_GFI_PLN_ScheduledPlan";
                    if (GetDataDT(ExistsIndex("GFI_PLN_ScheduledPlan", "IX_Live_GFI_PLN_ScheduledPlan")).Rows[0][0].ToString() == "1")
                        _SqlConn.OracleScriptExecute(sql, myStrConn); ;

                    sql = @"DROP INDEX IX_Live1_GFI_PLN_PlanningViaPointH";
                    if (GetDataDT(ExistsIndex("GFI_PLN_PlanningViaPointH", "IX_Live1_GFI_PLN_PlanningViaPointH")).Rows[0][0].ToString() == "1")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "CREATE  INDEX IX_Live_GFI_PLN_PlanningViaPointH ON GFI_PLN_PlanningViaPointH (PID)";
                    if (GetDataDT(ExistsIndex("GFI_PLN_PlanningViaPointH", "IX_Live_GFI_PLN_PlanningViaPointH")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE  INDEX IX_LiveVCA_GFI_PLN_PlanningViaPointH ON GFI_PLN_PlanningViaPointH (LocalityVCA, dtUTC)";
                    if (GetDataDT(ExistsIndex("GFI_PLN_PlanningViaPointH", "IX_LiveVCA_GFI_PLN_PlanningViaPointH")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE  INDEX IX_LiveLonLat_GFI_PLN_PlanningViaPointH ON GFI_PLN_PlanningViaPointH (Lon, dtUTC)";
                    if (GetDataDT(ExistsIndex("GFI_PLN_PlanningViaPointH", "IX_LiveLonLat_GFI_PLN_PlanningViaPointH")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE  INDEX IX_LiveZone_GFI_PLN_PlanningViaPointH ON GFI_PLN_PlanningViaPointH (ZoneID, dtUTC)";
                    if (GetDataDT(ExistsIndex("GFI_PLN_PlanningViaPointH", "IX_LiveZone_GFI_PLN_PlanningViaPointH")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE  INDEX IX_Live_GFI_PLN_PlanningResource ON GFI_PLN_PlanningResource (ResourceType)";
                    if (GetDataDT(ExistsIndex("GFI_PLN_PlanningResource", "IX_Live_GFI_PLN_PlanningResource")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE  INDEX IX_Live_GFI_PLN_ScheduledPlan ON GFI_PLN_ScheduledPlan (PID)";
                    if (GetDataDT(ExistsIndex("GFI_PLN_ScheduledPlan", "IX_Live_GFI_PLN_ScheduledPlan")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE  INDEX IX_GFI_PLN_ScheduledPlan_HID ON GFI_PLN_ScheduledPlan (HID)";
                    if (GetDataDT(ExistsIndex("GFI_PLN_ScheduledPlan", "IX_GFI_PLN_ScheduledPlan_HID")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE  INDEX IX_Live_GFI_FLT_VCADetail ON GFI_FLT_VCADetail (VCAID)";
                    if (GetDataDT(ExistsIndex("GFI_FLT_VCADetail", "IX_Live_GFI_FLT_VCADetail")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE  INDEX IX_GFI_SYS_GroupMatrixPlanning ON GFI_SYS_GroupMatrixPlanning (iID)";
                    if (GetDataDT(ExistsIndex("GFI_SYS_GroupMatrixPlanning", "IX_GFI_SYS_GroupMatrixPlanning")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "Pln Live indexes");
                }
                #endregion

                #region Update 162. User Login.
                strUpd = "Rez000162";
                if (!CheckUpdateExist(strUpd))
                {
                    //Exception Indexes
                    sql = "CREATE INDEX IX_GFI_GPS_Exceptions_RuleID_AssetID_GroupID ON GFI_GPS_Exceptions (RuleID, AssetID, GroupID)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_Exceptions", "IX_GFI_GPS_Exceptions_RuleID_AssetID_GroupID")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "delete from GFI_GPS_ProcessPending";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_User ADD TwoStepVerif nvarchar2(10) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "TwoStepVerif")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_User ADD TwoStepAttempts number(10) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "TwoStepAttempts")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_User ADD UserToken UniqueIdentifier default newid()";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "UserToken")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_User ADD CONSTRAINT UK_Token_GFI_SYS_User Unique (UserToken)";
                    if (GetDataDT(ExistsSQLConstraint("UK_Token_GFI_SYS_User", "GFI_SYS_User")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_User ADD PswdExpiryDate timestamp(3) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "PswdExpiryDate")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_User ADD PswdExpiryWarnDate timestamp(3) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "PswdExpiryWarnDate")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "update GFI_SYS_User set LoginCount = 0 where LoginCount is null";
                    _SqlConn.OracleScriptExecute(sql, myStrConn); ;
                    sql = @"ALTER TABLE GFI_SYS_User modify LoginCount int";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //sql = "Alter table GFI_SYS_User add Constraint D_LoginCount_GFI_SYS_User Default 0 for LoginCount";
                    //if (GetDataDT(ExistsSQLConstraint("D_LoginCount_GFI_SYS_User", "GFI_SYS_User")).Rows.Count == 0)
                    //    _SqlConn.ScriptExecute(sql, myStrConn);

                    sql = "update GFI_SYS_User set email = 'Installer@naveo.mu' where email = 'INSTALLER'";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);
                    sql = "update GFI_SYS_User set email = 'Admin@naveo.mu' where email = 'ADMIN'";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "User Login");
                }
                #endregion
                #region Update 163. Modules with Matrix
                strUpd = "Rez000163";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_SYS_NaveoModules
							(
								  iID NUMBER(10) NOT NULL
								, Description nvarchar2(50) NOT NULL
								, CONSTRAINT PK_GFI_SYS_NaveoModules PRIMARY KEY  (iID)
							 )";
                    //if (GetDataDT(ExistTableSql("GFI_SYS_NaveoModules")).Rows[0][0].ToString() == "0")
                    //    dbExecute(sql);
                    CreateTable(sql, "GFI_SYS_NaveoModules", "iID");

                    sql = @"CREATE TABLE GFI_SYS_GroupMatrixNaveoModules
							(
								  MID NUMBER(10) NOT NULL
								, GMID NUMBER(10) NOT NULL
								, iID NUMBER(10) NOT NULL
								, CONSTRAINT PK_GFI_SYS_GroupMatrixNaveoModules PRIMARY KEY  (MID)
							 )";
                    //if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrixNaveoModules")).Rows[0][0].ToString() == "0")
                    //    dbExecute(sql);
                    CreateTable(sql, "GFI_SYS_GroupMatrixNaveoModules", "MID");

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixNaveoModules
							ADD CONSTRAINT FK_GFI_SYS_GroupMatrixNaveoModules_GFI_SYS_GroupMatrix 
								FOREIGN KEY (GMID) 
								REFERENCES GFI_SYS_GroupMatrix (GMID)";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixNaveoModules_GFI_SYS_GroupMatrix", "GFI_SYS_GroupMatrixNaveoModules")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixNaveoModules 
							ADD CONSTRAINT FK_GFI_SYS_GroupMatrixNaveoModules_GFI_SYS_NaveoModules
								FOREIGN KEY(iID) 
								REFERENCES GFI_SYS_NaveoModules(iID) 
						 ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixNaveoModules_GFI_SYS_NaveoModules", "GFI_SYS_GroupMatrixNaveoModules")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "Naveo Modules");
                }
                #endregion
                #region Update 164. Permissions
                strUpd = "Rez000164";
                if (!CheckUpdateExist(strUpd))
                {
                    #region GFI_SYS_Permissions
                    sql = @"CREATE TABLE GFI_SYS_Permissions
							(
								  iID NUMBER(10) NOT NULL
								, Description nvarchar2(50) NOT NULL
								, ModuleID number(10) not null
								, CONSTRAINT PK_GFI_SYS_Permissions PRIMARY KEY  (iID)
							 )";
                    //if (GetDataDT(ExistTableSql("GFI_SYS_Permissions")).Rows[0][0].ToString() == "0")
                    //    dbExecute(sql);
                    CreateTable(sql, "GFI_SYS_Permissions", "iID");

                    sql = @"ALTER TABLE GFI_SYS_Permissions 
							ADD CONSTRAINT FK_GFI_SYS_Permissions_GFI_SYS_NaveoModules
								FOREIGN KEY(ModuleID) 
								REFERENCES GFI_SYS_NaveoModules(iID) 
						 ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_Permissions_GFI_SYS_NaveoModules", "GFI_SYS_Permissions")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    #endregion

                    #region Roles Groups]
                    sql = @"CREATE TABLE GFI_SYS_Roles
							(
								  iID NUMBER(10) NOT NULL
								, Description nvarchar2(50) NOT NULL
								, CONSTRAINT PK_GFI_SYS_Groups PRIMARY KEY  (iID)
							 )";
                    //if (GetDataDT(ExistTableSql("GFI_SYS_Roles")).Rows[0][0].ToString() == "0")
                    //    dbExecute(sql);
                    CreateTable(sql, "GFI_SYS_Roles", "iID");

                    //GroupMatrix
                    sql = @"CREATE TABLE GFI_SYS_GroupMatrixRoles
							(
								  MID NUMBER(10) NOT NULL
								, GMID NUMBER(10) NOT NULL
								, iID NUMBER(10) NOT NULL
								, CONSTRAINT PK_GFI_SYS_GroupMatrixRoles PRIMARY KEY  (MID)
							 )";
                    //if (GetDataDT(ExistTableSql("GFI_SYS_GroupMatrixRoles")).Rows[0][0].ToString() == "0")
                    //    dbExecute(sql);
                    CreateTable(sql, "GFI_SYS_GroupMatrixRoles", "MID");

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixRoles
							ADD CONSTRAINT FK_GFI_SYS_GroupMatrixRoles_GFI_SYS_GroupMatrix 
								FOREIGN KEY (GMID) 
								REFERENCES GFI_SYS_GroupMatrix (GMID) ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixRoles_GFI_SYS_GroupMatrix", "GFI_SYS_GroupMatrixRoles")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_GroupMatrixRoles 
							ADD CONSTRAINT FK_GFI_SYS_GroupMatrixRoles_GFI_SYS_Roles
								FOREIGN KEY(iID) 
								REFERENCES GFI_SYS_Roles(iID) 
						 ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_GroupMatrixRoles_GFI_SYS_Roles", "GFI_SYS_GroupMatrixRoles")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    #endregion

                    #region RolePermissions
                    sql = @"CREATE TABLE GFI_SYS_RolePermissions
							(
								RoleID number(10) NOT NULL
								, PermissionID number(10) not null
								, CONSTRAINT PK_GFI_SYS_RolePermissions PRIMARY KEY  (RoleID, PermissionID)
							 )";
                    //if (GetDataDT(ExistTableSql("GFI_SYS_RolePermissions")).Rows[0][0].ToString() == "0")
                    //    dbExecute(sql);
                    CreateTable(sql, "GFI_SYS_RolePermissions", "RoleID");

                    sql = @"ALTER TABLE GFI_SYS_RolePermissions 
							ADD CONSTRAINT FK_GFI_SYS_RolePermissions_GFI_SYS_Roles
								FOREIGN KEY(RoleID) 
								REFERENCES GFI_SYS_Roles(iID) 
						 ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_RolePermissions_GFI_SYS_Roles", "GFI_SYS_RolePermissions")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_RolePermissions 
							ADD CONSTRAINT FK_GFI_SYS_RolePermissions_GFI_SYS_Permissions
								FOREIGN KEY(PermissionID) 
								REFERENCES GFI_SYS_Permissions(iID) 
						 ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_RolePermissions_GFI_SYS_Permissions", "GFI_SYS_RolePermissions")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    #endregion

                    #region UserRoles
                    sql = @"CREATE TABLE GFI_SYS_UserRoles
							(
								UserID number(10) not null
								, RoleID number(10) NOT NULL
								, CONSTRAINT PK_GFI_SYS_UserRoles PRIMARY KEY  (UserID, RoleID)
							 )";
                    //if (GetDataDT(ExistTableSql("GFI_SYS_UserRoles")).Rows[0][0].ToString() == "0")
                    //    dbExecute(sql);
                    CreateTable(sql, "GFI_SYS_UserRoles", "UserID");

                    sql = @"ALTER TABLE GFI_SYS_UserRoles 
							ADD CONSTRAINT FK_GFI_SYS_UserRoles_GFI_SYS_User
								FOREIGN KEY(UserID) 
								REFERENCES GFI_SYS_User(UID) 
						 ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_UserRoles_GFI_SYS_User", "GFI_SYS_UserRoles")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_UserRoles 
							ADD CONSTRAINT FK_GFI_SYS_UserRoles_GFI_SYS_Roles
								FOREIGN KEY(RoleID) 
								REFERENCES GFI_SYS_Roles(iID) 
						 ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_UserRoles_GFI_SYS_Roles", "GFI_SYS_UserRoles")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    #endregion

                    InsertDBUpdate(strUpd, "Permissions");
                }
                #endregion
                #region Update 165. User Login additional fields, GFI_SYS_Tokens and sDriverName
                strUpd = "Rez000165";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_SYS_User ADD LastName nvarchar2(50) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_User", "LastName")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    if (GetDataDT(ExistTableSql("AspNetUsers")).Rows[0][0].ToString() == "1")
                    {
                        sql = @"update GFI_SYS_User
									set LastName = T2.LastName
								from GFI_SYS_User T1
									inner join AspNetUsers T2 on T1.Email = T2.Email";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    }

                    sql = @"CREATE TABLE GFI_SYS_Tokens
							(
								TokenID nvarchar2(37) NOT NULL
								, sqlCmd nvarchar(100) NOT NULL
								, ExpiryDate timestamp(3) not null
								, sData nvarchar2(150) null
								, sAdditionalData nvarchar2(150) null
								, CONSTRAINT PK_GFI_SYS_Tokens PRIMARY KEY  (TokenID)
							 )";
                    //if (GetDataDT(ExistTableSql("GFI_SYS_Tokens")).Rows[0][0].ToString() == "0")
                    //    _SqlConn.ScriptExecute(sql, myStrConn);
                    CreateTable(sql, "GFI_SYS_Tokens", "TokenID");

                    sql = "ALTER TABLE GFI_FLT_Driver modify sDriverName nvarchar2(100)";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "update GFI_FLT_Driver set sDriverName = sLastName + ' ' + sFirtName where sDriverName = sFirtName";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "User Login additional fields, GFI_SYS_Tokens and sDriverName");
                }
                #endregion
                #region Update 166. updated sp_MOHRequestCode
                strUpd = "Rez000166";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"ALTER TABLE GFI_PLN_Planning DROP CONSTRAINT UK_GFI_PLN_Planning_RequestCode";
                    if (GetDataDT(ExistsSQLConstraint("UK_GFI_PLN_Planning_RequestCode", "GFI_PLN_Planning")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "alter table GFI_PLN_Planning modify RequestCode nvarchar2(14)";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);
                    sql = "alter table GFI_PLN_Planning modify ParentRequestCode nvarchar2(14)";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_PLN_Planning ADD  CONSTRAINT UK_GFI_PLN_Planning_RequestCode Unique (RequestCode)";
                    if (GetDataDT(ExistsSQLConstraint("UK_GFI_PLN_Planning_RequestCode", "GFI_PLN_Planning")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_PLN_MOHRequestCode ADD iMonth number(10) Default 0";
                    if (GetDataDT(ExistColumnSql("GFI_PLN_MOHRequestCode", "iMonth")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"Alter Table GFI_PLN_MOHRequestCode DROP CONSTRAINT PK_GFI_PLN_MOHRequestCode";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_PLN_MOHRequestCode", "GFI_PLN_MOHRequestCode")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "alter table GFI_PLN_MOHRequestCode ADD CONSTRAINT PK_GFI_PLN_MOHRequestCode PRIMARY KEY  (Code, iYear, iMonth)";
                    if (GetDataDT(ExistsSQLConstraint("PK_GFI_PLN_MOHRequestCode", "GFI_PLN_MOHRequestCode")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[sp_MOHRequestCode]') AND type in (N'P', N'PC'))
                    //							DROP PROCEDURE sp_MOHRequestCode]";
                    //                    _SqlConn.ScriptExecute(sql, myStrConn);

                    //                    sql = @"
                    //Create PROCEDURE sp_MOHRequestCode] @Code nvarchar(3)
                    //as Begin

                    //	DECLARE @iYear int
                    //	DECLARE @iMonth int
                    //	DECLARE @sYear nvarchar(2)
                    //	DECLARE @iSeed int
                    //	DECLARE @sSeed nvarchar(5)
                    //	DECLARE @ReqNo nvarchar(14)
                    //	DECLARE @idd int
                    //	DECLARE @sdd nvarchar(2)
                    //	DECLARE @imm int
                    //	DECLARE @smm nvarchar(2)

                    //	set @iYear = 0

                    //	;with cta as
                    //	(
                    //		select * from GFI_PLN_MOHRequestCode] where Code] = @Code and iYear = DATEPART(yyyy, GETDATE()) and iMonth = DATEPART(MM, GETDATE())
                    //	)
                    //	select @iYear = iYear, @iSeed = iSeed, @iMonth = iMonth from cta;

                    //	if (@iYear = 0)
                    //	begin
                    //		insert GFI_PLN_MOHRequestCode] values (@Code, DATEPART(yyyy, GETDATE()), 0, DATEPART(MM, GETDATE()));

                    //		;with cta as
                    //		(
                    //			select * from GFI_PLN_MOHRequestCode] where Code] = @Code and iYear = DATEPART(yyyy, GETDATE()) and iMonth = DATEPART(MM, GETDATE())
                    //		)
                    //		select @iYear = iYear, @iSeed = iSeed, @iMonth = iMonth from cta;
                    //	end

                    //	update GFI_PLN_MOHRequestCode] set iSeed = iSeed + 1 where Code] = @Code and iYear = DATEPART(yyyy, GETDATE()) and iMonth = DATEPART(MM, GETDATE())

                    //	;with cta as
                    //	(
                    //		select * from GFI_PLN_MOHRequestCode] where Code] = @Code and iYear = DATEPART(yyyy, GETDATE()) 
                    //	)
                    //	select @iYear = iYear, @iSeed = iSeed from cta;

                    //	--yy
                    //	if(@iYear > 2000)
                    //		set @iYear = @iYear - 2000
                    //	set @sYear = CAST(@iYear AS nvarchar(2))

                    //	--mm
                    //	set @imm = DATEPART(mm,GETDATE());
                    //	if(@imm < 10)
                    //		set @smm = '0' + CAST(@imm AS nvarchar(2))
                    //	else
                    //		set @smm = CAST(@imm AS nvarchar(2))

                    //	--dd
                    //	set @idd = DATEPART(dd,GETDATE());
                    //	if(@idd < 10)
                    //		set @sdd = '0' + CAST(@idd AS nvarchar(2))
                    //	else
                    //		set @sdd = CAST(@idd AS nvarchar(2))

                    //	--Seed
                    //	if(@iSeed < 10)	        set @sSeed = '0000' + CAST(@iSeed AS nvarchar(5))
                    //	else if(@iSeed < 100)	set @sSeed = '000' + CAST(@iSeed AS nvarchar(5))
                    //	else if(@iSeed < 1000)	set @sSeed = '00' + CAST(@iSeed AS nvarchar(5))
                    //	else if(@iSeed < 10000)	set @sSeed = '0' + CAST(@iSeed AS nvarchar(5))
                    //	else                    set @sSeed = CAST(@iSeed AS nvarchar(5))

                    //	set @ReqNo = @Code + @sdd + @smm + @sYear + @sSeed
                    //	select @ReqNo
                    //end";
                    //                    _SqlConn.ScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "updated sp_MOHRequestCode");
                }
                #endregion

                #region Update 167. User, Modules, Permission
                strUpd = "Rez000167";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_SYS_GroupMatrix ADD CompanyCode nvarchar2(15)";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_GroupMatrix", "CompanyCode")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_SYS_GroupMatrix ADD LegalName nvarchar2(150) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_GroupMatrix", "LegalName")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_SYS_RolePermissions ADD iCreate number(10) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_RolePermissions", "iCreate")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_SYS_RolePermissions ADD iRead number(10) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_RolePermissions", "iRead")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_SYS_RolePermissions ADD iUpdate number(10) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_RolePermissions", "iUpdate")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_SYS_RolePermissions ADD iDelete number(10) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_RolePermissions", "iDelete")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_SYS_RolePermissions ADD ControllerName nvarchar2(50) not null";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_RolePermissions", "ControllerName")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_SYS_RolePermissions ADD ModuleID number(10) not NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_RolePermissions", "ModuleID")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_SYS_Permissions DROP CONSTRAINT FK_GFI_SYS_Permissions_GFI_SYS_NaveoModules";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_Permissions_GFI_SYS_NaveoModules", "GFI_SYS_Permissions")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_SYS_RolePermissions DROP CONSTRAINT FK_GFI_SYS_RolePermissions_GFI_SYS_Permissions";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_RolePermissions_GFI_SYS_Permissions", "GFI_SYS_Permissions")).Rows.Count > 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"drop table GFI_SYS_Permissions";
                    if (GetDataDT(ExistTableSql("GFI_SYS_Permissions")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_RolePermissions 
							ADD CONSTRAINT FK_GFI_SYS_RolePermissions_GFI_SYS_NaveoModules
								FOREIGN KEY(ModuleID) 
								REFERENCES GFI_SYS_NaveoModules(iID) 
						 ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_RolePermissions_GFI_SYS_NaveoModules", "GFI_SYS_RolePermissions")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_NaveoModules ADD CONSTRAINT UK_GFI_SYS_NaveoModules Unique (Description)";
                    if (GetDataDT(ExistsSQLConstraint("UK_GFI_SYS_NaveoModules", "GFI_SYS_NaveoModules")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    //sql = @"INSERT INTO GFI_SYS_NaveoModules(Description)
                    //       SELECT 'Admin'
                    //       FROM DUAL
                    //       WHERE NOT EXISTS(SELECT * FROM GFI_SYS_NaveoModules WHERE Description='Admin')";
                    //_SqlConn.ScriptExecute(sql, myStrConn); 

                    //sql = @"INSERT INTO GFI_SYS_NaveoModules(Description)
                    //       SELECT 'Fleet'
                    //       FROM DUAL
                    //       WHERE NOT EXISTS(SELECT * FROM GFI_SYS_NaveoModules WHERE Description='Fleet')";
                    //_SqlConn.ScriptExecute(sql, myStrConn); 

                    //sql = @"INSERT INTO GFI_SYS_NaveoModules(Description)
                    //       SELECT 'Maintenance'
                    //       FROM DUAL
                    //       WHERE NOT EXISTS(SELECT * FROM GFI_SYS_NaveoModules WHERE Description='Maintenance')";
                    //_SqlConn.ScriptExecute(sql, myStrConn);





                    sql = "ALTER TABLE GFI_SYS_RolePermissions DROP CONSTRAINT FK_GFI_SYS_RolePermissions_GFI_SYS_NaveoModules";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_RolePermissions_GFI_SYS_NaveoModules", "GFI_SYS_RolePermissions")).Rows.Count == 1)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_SYS_RolePermissions drop column ControllerName";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_RolePermissions", "ControllerName")).Rows[0][0].ToString() == "1")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = "ALTER TABLE GFI_SYS_RolePermissions drop column ModuleID";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_RolePermissions", "ModuleID")).Rows[0][0].ToString() == "1")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    #region GFI_SYS_Permissions
                    sql = @"CREATE TABLE GFI_SYS_Permissions
							(
								  iID NUMBER(10) NOT NULL
								, ControllerName nvarchar2(50) NOT NULL
								, ModuleID number(10) not null
								, CONSTRAINT PK_GFI_SYS_Permissions PRIMARY KEY  (iID)
							 )";
                    //if (GetDataDT(ExistTableSql("GFI_SYS_Permissions")).Rows[0][0].ToString() == "0")
                    //    dbExecute(sql);
                    CreateTable(sql, "GFI_SYS_Permissions", "iID");

                    sql = @"ALTER TABLE GFI_SYS_Permissions 
							ADD CONSTRAINT FK_GFI_SYS_Permissions_GFI_SYS_NaveoModules
								FOREIGN KEY(ModuleID) 
								REFERENCES GFI_SYS_NaveoModules(iID) 
						 ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_Permissions_GFI_SYS_NaveoModules", "GFI_SYS_Permissions")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE GFI_SYS_RolePermissions 
							ADD CONSTRAINT FK_GFI_SYS_RolePermissions_GFI_SYS_Permissions
								FOREIGN KEY(PermissionID) 
								REFERENCES GFI_SYS_Permissions(iID) 
						 ON DELETE  CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_SYS_RolePermissions_GFI_SYS_Permissions", "GFI_SYS_RolePermissions")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    #endregion

                    NaveoModule n = new NaveoModule();
                    NaveoModulesService naveoModulesService = new NaveoModulesService();

                    n = naveoModulesService.GetNaveoModulesByName("ADMIN", sConnStr);
                    if (n == null)
                    {
                        n = new NaveoModule();
                        n.description = "Admin";
                        n.lMatrix = new List<Matrix>();
                        n.lPermissions = new List<Permission>();

                        naveoModulesService.SaveNaveoModules(n, true, sConnStr);
                    }


                    n = naveoModulesService.GetNaveoModulesByName("Fleet", sConnStr);
                    if (n == null)
                    {
                        n = new NaveoModule();
                        n.description = "Fleet";
                        n.lMatrix = new List<Matrix>();
                        n.lPermissions = new List<Permission>();

                        naveoModulesService.SaveNaveoModules(n, true, sConnStr);
                    }


                    n = naveoModulesService.GetNaveoModulesByName("Maintenance", sConnStr);
                    if (n == null)
                    {
                        n = new NaveoModule();
                        n.description = "Maintenance";
                        n.lMatrix = new List<Matrix>();
                        n.lPermissions = new List<Permission>();

                        naveoModulesService.SaveNaveoModules(n, true, sConnStr);
                    }

                    if (n != null)
                    {
                        Boolean b = false;
                        foreach (Permission permission in n.lPermissions)
                            if (permission.ControllerName == "User")
                                b = true;
                        if (!b)
                        {
                            if (n.lMatrix.Count == 0)
                            {
                                GroupMatrix gmRoot = new GroupMatrix();
                                gmRoot = new GroupMatrixService().GetRoot(sConnStr);

                                Matrix m = new Matrix();
                                m.GMID = gmRoot.gmID;
                                m.oprType = DataRowState.Added;
                                m.iID = n.iID;
                                n.lMatrix.Add(m);
                            }

                            Permission p = new Permission();
                            p.ControllerName = "User";
                            p.ModuleID = n.iID;
                            p.oprType = DataRowState.Added;
                            n.lPermissions.Add(p);
                            naveoModulesService.SaveNaveoModules(n, false, sConnStr);
                        }
                    }

                    n = new NaveoModule();
                    n = naveoModulesService.GetNaveoModulesByName("GIS", sConnStr);
                    if (n == null)
                    {
                        n = new NaveoModule();
                        n.description = "GIS";
                        n.lMatrix = new List<Matrix>();
                        n.lPermissions = new List<Permission>();

                        GroupMatrix gmRoot = new GroupMatrix();
                        gmRoot = new GroupMatrixService().GetRoot(sConnStr);

                        Matrix m = new Matrix();
                        m.GMID = gmRoot.gmID;
                        m.oprType = DataRowState.Added;
                        n.lMatrix.Add(m);

                        Permission p = new Permission();
                        p.ControllerName = "Map";
                        p.oprType = DataRowState.Added;
                        n.lPermissions.Add(p);

                        naveoModulesService.SaveNaveoModules(n, true, sConnStr);
                    }

                    InsertDBUpdate(strUpd, "User, Modules, Permission");
                }
                #endregion
                #region Update 168. GetCalcOdo updated
                strUpd = "Rez000168";
                if (!CheckUpdateExist(strUpd))
                {
                    //TO create FN
                    //             sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[GetCalcOdo]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
                    //DROP FUNCTION GetCalcOdo";
                    //             dbExecute(sql);
                    //             sql = @"CREATE function GetCalcOdo] (@AssetID int)
                    //returns int
                    //as Begin

                    //DECLARE @fOdo float

                    //	;with cta as
                    //	(
                    //		select *, 
                    //			row_number() over(partition by AssetID order by URI desc) as RowNum
                    //		from GFI_AMM_VehicleMaintenance
                    //		where AssetId = @AssetID
                    //			and ActualOdometer is not null
                    //			and ActualOdometer > 0
                    //	)
                    //	select @fOdo = COALESCE(SUM(h.TripDistance),0) + COALESCE(max(cta.ActualOdometer),0)
                    //	from GFI_GPS_TripHeader h
                    //	left outer join cta on h.AssetID = cta.AssetId and cta.RowNum = 1
                    //		where 
                    //				h.dtEnd >= COALESCE(cta.EndDate,DATEADD(YY, -50, GETDATE()))
                    //				and h.AssetId = @AssetID

                    //	if(@fOdo = 0)
                    //	begin
                    //		;with cta as
                    //		(
                    //			select *, 
                    //				row_number() over(partition by AssetID order by URI desc) as RowNum
                    //			from GFI_AMM_VehicleMaintenance
                    //			where AssetId = @AssetID
                    //				and ActualOdometer is not null
                    //				and ActualOdometer > 0
                    //		)
                    //		select @fOdo = COALESCE(max(cta.ActualOdometer),0)
                    //			from cta where cta.RowNum = 1
                    //	end

                    //	if(@fOdo = 0)
                    //	begin
                    //		select @fOdo = COALESCE(SUM(h.TripDistance),0)
                    //			from GFI_GPS_TripHeader h
                    //		where  h.AssetId = @AssetID
                    //	end

                    //	return CAST(@fOdo AS int)
                    //end";
                    //             dbExecute(sql);

                    InsertDBUpdate(strUpd, "GetCalcOdo updated");
                }
                #endregion
                #region Update 169. GetCalcEngHrs updated
                strUpd = "Rez000169";
                if (!CheckUpdateExist(strUpd))
                {
                    //to create FN
                    //                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[GetCalcEngHrs]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
                    //							DROP FUNCTION GetCalcEngHrs]";
                    //                    dbExecute(sql);
                    //                    sql = @"CREATE function GetCalcEngHrs] (@AssetID int)
                    //							returns int
                    //							as Begin

                    //							DECLARE @fEng float

                    //							;with cta as
                    //							(
                    //								select *, 
                    //									row_number() over(partition by AssetID order by URI desc) as RowNum
                    //								from GFI_AMM_VehicleMaintenance
                    //								where AssetId = @AssetID
                    //									and ActualEngineHrs is not null
                    //									and ActualEngineHrs > 0
                    //							)
                    //							select @fEng = COALESCE(SUM(h.TripTime),0) + COALESCE(max(cta.ActualEngineHrs),0)
                    //							from GFI_GPS_TripHeader h
                    //							left outer join cta on h.AssetID = cta.AssetId and cta.RowNum = 1
                    //								where 
                    //									 h.dtEnd >= COALESCE(cta.EndDate,DATEADD(YY, -50, GETDATE()))
                    //									 and h.AssetId = @AssetID


                    //							if(@fEng = 0)
                    //								begin
                    //									;with cta as
                    //									(
                    //										select *, 
                    //											row_number() over(partition by AssetID order by URI desc) as RowNum
                    //										from GFI_AMM_VehicleMaintenance
                    //										where AssetId = @AssetID
                    //											and ActualEngineHrs is not null
                    //											and ActualEngineHrs > 0
                    //									)
                    //									select @fEng = COALESCE(max(cta.ActualEngineHrs),0)
                    //										from cta where cta.RowNum = 1
                    //								end

                    //								if(@fEng = 0)
                    //								begin
                    //									select @fEng = COALESCE(SUM(h.TripTime),0)
                    //										from GFI_GPS_TripHeader h
                    //									where  h.AssetId = @AssetID
                    //								end

                    //							return CAST(@fEng AS int)
                    //							end";
                    //                    dbExecute(sql);

                    //                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[sp_AMM_VehicleMaint_ProcessRecords]') AND type in (N'P', N'PC'))
                    //							DROP PROCEDURE sp_AMM_VehicleMaint_ProcessRecords]";
                    //                    dbExecute(sql);

                    //                    sql = @"
                    //CREATE PROCEDURE sp_AMM_VehicleMaint_ProcessRecords]
                    //AS
                    //BEGIN
                    //	/***********************************************************************************************************************
                    //	Version: 3.1.0.0
                    //	Modifed: 11/ 08/2016.	06/08/2014
                    //	Created By: Perry Ramen
                    //	Modified By: Reza Dowlut
                    //	Description: Process list of Maintenance Types for each vehicle to determine if they are To Schedule] or Overdue]
                    //	***********************************************************************************************************************/
                    //		BEGIN
                    //		DECLARE @iRecCountProcessed int

                    //		DECLARE @iMaintStatusId_Overdue int
                    //		DECLARE @iMaintStatusId_ToSchedule int
                    //		DECLARE @iMaintStatusId_Scheduled int
                    //		DECLARE @iMaintStatusId_InProgress int
                    //		DECLARE @iMaintStatusId_Completed int
                    //		DECLARE @iMaintStatusId_Valid int
                    //		DECLARE @iMaintStatusId_Expired int

                    //		DECLARE @URI int
                    //		DECLARE @MaintURI int
                    //		DECLARE @AssetId int
                    //		DECLARE @MaintTypeId int
                    //		DECLARE @OccurrenceType int
                    //		DECLARE @NextMaintDate DateTime
                    //		DECLARE @NextMaintDateTG DateTime 
                    //		DECLARE @CurrentOdometer int
                    //		DECLARE @NextMaintOdometer int
                    //		DECLARE @NextMaintOdometerTG int
                    //		DECLARE @CurrentEngHrs int
                    //		DECLARE @NextMaintEngHrs int
                    //		DECLARE @NextMaintEngHrsTG int
                    //		DECLARE @MaintStatusId int

                    //		DECLARE @MaintURIUpdate int

                    //		DECLARE @OccurrenceDuration int
                    //		DECLARE @OccurrenceDurationTh int


                    //		DECLARE @OccurrenceKM int
                    //		DECLARE @OccurrenceKMTh int
                    //		DECLARE @OccurrencePeriod_cbo nvarchar(50)

                    //		SET @iMaintStatusId_Overdue = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Overdue')
                    //		SET @iMaintStatusId_ToSchedule = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'To Schedule')
                    //		SET @iMaintStatusId_Scheduled = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Scheduled')
                    //		SET @iMaintStatusId_InProgress = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'In Progress')		
                    //		SET @iMaintStatusId_Completed = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Completed')				
                    //		SET @iMaintStatusId_Valid = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Valid')		
                    //		SET @iMaintStatusId_Expired = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Expired')				

                    //		update GFI_AMM_VehicleMaintTypesLink set CurrentOdometer = GetCalcOdo(AssetId)

                    //		DECLARE csrData CURSOR FOR
                    //		SELECT URI, 
                    //			vmtl.MaintURI,
                    //			vmtl.AssetId,
                    //			vmtl.MaintTypeId,
                    //			vmt.OccurrenceType,
                    //			vmtl.NextMaintDate, 
                    //			vmtl.NextMaintDateTG, 
                    //			vmtl.CurrentOdometer, 
                    //			vmtl.NextMaintOdometer, 
                    //			vmtl.NextMaintOdometerTG, 
                    //			vmtl.CurrentEngHrs,
                    //			vmtl.NextMaintEngHrs,
                    //			vmtl.NextMaintEngHrsTG,
                    //			vmtl.[Status]
                    //			, vmt.OccurrenceDuration
                    //			, vmt.OccurrenceDurationTh
                    //			, vmt.OccurrencePeriod_cbo
                    //			, vmt.OccurrenceKM
                    //			, vmt.OccurrenceKMTh 
                    //		FROM GFI_AMM_VehicleMaintTypesLink vmtl
                    //			INNER JOIN GFI_AMM_VehicleMaintTypes vmt ON vmtl.MaintTypeId = vmt.MaintTypeId

                    //		OPEN csrData
                    //		FETCH NEXT FROM csrData INTO @URI, @MaintURI, @AssetId, @MaintTypeId, @OccurrenceType, @NextMaintDate, @NextMaintDateTG, @CurrentOdometer, @NextMaintOdometer, @NextMaintOdometerTG, @CurrentEngHrs, @NextMaintEngHrs, @NextMaintEngHrsTG, @MaintStatusId
                    //			, @OccurrenceDuration, @OccurrenceDurationTh, @OccurrencePeriod_cbo, @OccurrenceKM, @OccurrenceKMTh
                    //		SET @iRecCountProcessed = 0

                    //		WHILE @@FETCH_STATUS = 0
                    //		BEGIN
                    //			IF @NextMaintDate IS NULL SET @NextMaintDate = GETDATE()

                    //			--select @URI, 'OccurrenceType 2', @OccurrenceType
                    //			--	, 'AND'
                    //			--	, '@NextMaintDateTG <= GETDATE()', @NextMaintDateTG, GETDATE()
                    //			--	, '@NextMaintOdometerTG < @CurrentOdometer', @NextMaintOdometerTG, @CurrentOdometer
                    //			--	, '@NextMaintEngHrsTG < @CurrentEngHrs', @NextMaintEngHrsTG, @CurrentEngHrs
                    //			--	, 'AND'
                    //			--	, '@MaintStatusId = @iMaintStatusId_Completed', @MaintStatusId, @iMaintStatusId_Completed
                    //			--	, '@MaintStatusId = @iMaintStatusId_Valid', @MaintStatusId, @iMaintStatusId_Valid

                    //			-- Threhold by date range
                    //			declare @Maintdate DateTime
                    //			declare @Thershold int
                    //			set @Thershold = @OccurrenceDurationTh * -1 -- - @OccurrenceDuration
                    //			set @Maintdate = DateAdd(month, @Thershold, GETDATE());
                    //			if @OccurrencePeriod_cbo = 'DAY(S)' 
                    //				set @Maintdate = DateAdd(day, @Thershold, GETDATE());

                    //			select @Thershold, @NextMaintDate, @Maintdate
                    //			--if(@Thershold != 0 and @NextMaintDate <= @Maintdate)
                    //			--	select '*************************************************'

                    //			-- Recurring - Flag As: To Schedule
                    //			IF (
                    //				(@OccurrenceType = 2) 
                    //				AND ((@NextMaintDateTG <= GETDATE() and @OccurrenceDuration > 0) OR ( @CurrentOdometer >= (@NextMaintOdometer - (@OccurrenceKM - @OccurrenceKMTh)) ) OR (@NextMaintEngHrsTG < @CurrentEngHrs) OR (@Thershold != 0 and @NextMaintDate <= @Maintdate)) 
                    //				AND ((@MaintStatusId = @iMaintStatusId_Completed) OR (@MaintStatusId = @iMaintStatusId_Valid)))
                    //			BEGIN
                    //				IF NOT EXISTS(SELECT * FROM GFI_AMM_VehicleMaintTypesLink WHERE AssetId = @AssetId AND MaintTypeId = @MaintTypeId AND URI > @URI)
                    //				BEGIN
                    //					IF (@MaintStatusId = @iMaintStatusId_Completed)
                    //					BEGIN
                    //						INSERT GFI_AMM_VehicleMaintenance (AssetId, MaintTypeId_cbo, MaintStatusId_cbo, StartDate, EndDate, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy) 
                    //						VALUES (@AssetId, @MaintTypeId, @iMaintStatusId_ToSchedule, @NextMaintDate, @NextMaintDate, GETDATE(), CURRENT_USER, GETDATE(), CURRENT_USER)

                    //						SET @MaintURIUpdate = (SELECT SCOPE_IDENTITY())

                    //						UPDATE GFI_AMM_VehicleMaintTypesLink
                    //						SET MaintURI = @MaintURIUpdate, Status = @iMaintStatusId_ToSchedule, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER, NextMaintOdometer = @OccurrenceKM + @CurrentOdometer
                    //						WHERE URI = @URI
                    //					END
                    //					ELSE IF (@MaintStatusId = @iMaintStatusId_Valid)
                    //					BEGIN
                    //						SET @NextMaintDate = DATEADD(DD, 1, @NextMaintDate)

                    //						INSERT GFI_AMM_VehicleMaintenance (AssetId, MaintTypeId_cbo, MaintStatusId_cbo, StartDate, EndDate, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy) 
                    //						VALUES (@AssetId, @MaintTypeId, @iMaintStatusId_ToSchedule, @NextMaintDate, @NextMaintDate, GETDATE(), CURRENT_USER, GETDATE(), CURRENT_USER)

                    //						SET @MaintURIUpdate = (SELECT SCOPE_IDENTITY())

                    //						INSERT GFI_AMM_VehicleMaintTypesLink (MaintURI, AssetId, MaintTypeId, NextMaintDate, NextMaintDateTG, Status, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy)
                    //						VALUES (@MaintURIUpdate, @AssetId, @MaintTypeId, @NextMaintDate, @NextMaintDate, @iMaintStatusId_ToSchedule, GETDATE(), CURRENT_USER, GETDATE(), CURRENT_USER)
                    //					END
                    //				END
                    //			END

                    //			--Flag As: Overdue
                    //			IF (
                    //					(
                    //						(@NextMaintDate <= GETDATE() and @OccurrenceDuration > 0) 
                    //							OR (@NextMaintOdometer < @CurrentOdometer) 
                    //							OR (@NextMaintEngHrs < @CurrentEngHrs)
                    //					) 
                    //					AND (@MaintStatusId = @iMaintStatusId_ToSchedule)
                    //				)
                    //			BEGIN				
                    //				UPDATE GFI_AMM_VehicleMaintTypesLink
                    //				SET Status = @iMaintStatusId_Overdue, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
                    //				WHERE URI = @URI

                    //				UPDATE GFI_AMM_VehicleMaintenance 
                    //				SET MaintStatusId_cbo = @iMaintStatusId_Overdue, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
                    //				WHERE URI = @MaintURI
                    //			END

                    //			--Auto Expiry: Lapsed Insurance
                    //			IF ((@MaintStatusId = @iMaintStatusId_Valid) AND (@NextMaintDate < GETDATE()))
                    //			BEGIN
                    //				UPDATE GFI_AMM_VehicleMaintTypesLink
                    //				SET Status = @iMaintStatusId_Expired , UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
                    //				WHERE URI = @URI

                    //				UPDATE GFI_AMM_VehicleMaintenance 
                    //				SET MaintStatusId_cbo = @iMaintStatusId_Expired, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
                    //				WHERE URI = @MaintURI
                    //			END

                    //			SET @iRecCountProcessed = @iRecCountProcessed + 1

                    //			FETCH NEXT FROM csrData INTO @URI, @MaintURI, @AssetId, @MaintTypeId, @OccurrenceType, @NextMaintDate, @NextMaintDateTG, @CurrentOdometer, @NextMaintOdometer, @NextMaintOdometerTG, @CurrentEngHrs, @NextMaintEngHrs, @NextMaintEngHrsTG, @MaintStatusId
                    //				, @OccurrenceDuration, @OccurrenceDurationTh, @OccurrencePeriod_cbo, @OccurrenceKM, @OccurrenceKMTh
                    //		END

                    //		CLOSE csrData
                    //		DEALLOCATE csrData

                    //		RETURN @iRecCountProcessed
                    //	END
                    //END
                    //";
                    //                    dbExecute(sql);

                    InsertDBUpdate(strUpd, "GetCalcEngHrs updated");
                }
                #endregion
                #region Update 170. SP Maintenance updated
                strUpd = "Rez000170";
                if (!CheckUpdateExist(strUpd))
                {

                    //                    #region SP Maintenance updated
                    //                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[sp_AMM_VehicleMaint_ProcessRecords]') AND type in (N'P', N'PC'))
                    //							DROP PROCEDURE sp_AMM_VehicleMaint_ProcessRecords]";
                    //                    dbExecute(sql);
                    //                    //Oracle to create SP as per Update 19
                    //                    sql = @"
                    //CREATE PROCEDURE sp_AMM_VehicleMaint_ProcessRecords]
                    //AS
                    //BEGIN
                    //	/***********************************************************************************************************************
                    //	Version: 3.1.0.0
                    //	Modifed: 11/ 08/2016.	06/08/2014
                    //	Created By: Perry Ramen
                    //	Modified By: Reza Dowlut
                    //	Description: Process list of Maintenance Types for each vehicle to determine if they are To Schedule] or Overdue]
                    //	***********************************************************************************************************************/
                    //		BEGIN
                    //		DECLARE @iRecCountProcessed int

                    //		DECLARE @iMaintStatusId_Overdue int
                    //		DECLARE @iMaintStatusId_ToSchedule int
                    //		DECLARE @iMaintStatusId_Scheduled int
                    //		DECLARE @iMaintStatusId_InProgress int
                    //		DECLARE @iMaintStatusId_Completed int
                    //		DECLARE @iMaintStatusId_Valid int
                    //		DECLARE @iMaintStatusId_Expired int

                    //		DECLARE @URI int
                    //		DECLARE @MaintURI int
                    //		DECLARE @AssetId int
                    //		DECLARE @MaintTypeId int
                    //		DECLARE @OccurrenceType int
                    //		DECLARE @NextMaintDate DateTime
                    //		DECLARE @NextMaintDateTG DateTime 
                    //		DECLARE @CurrentOdometer int
                    //		DECLARE @NextMaintOdometer int
                    //		DECLARE @NextMaintOdometerTG int
                    //		DECLARE @CurrentEngHrs int
                    //		DECLARE @NextMaintEngHrs int
                    //		DECLARE @NextMaintEngHrsTG int
                    //		DECLARE @MaintStatusId int

                    //		DECLARE @MaintURIUpdate int

                    //		DECLARE @OccurrenceDuration int
                    //		DECLARE @OccurrenceDurationTh int


                    //		DECLARE @OccurrenceKM int
                    //		DECLARE @OccurrenceKMTh int
                    //		DECLARE @OccurrencePeriod_cbo nvarchar(50)

                    //		SET @iMaintStatusId_Overdue = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Overdue')
                    //		SET @iMaintStatusId_ToSchedule = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'To Schedule')
                    //		SET @iMaintStatusId_Scheduled = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Scheduled')
                    //		SET @iMaintStatusId_InProgress = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'In Progress')		
                    //		SET @iMaintStatusId_Completed = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Completed')				
                    //		SET @iMaintStatusId_Valid = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Valid')		
                    //		SET @iMaintStatusId_Expired = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Expired')				

                    //		update GFI_AMM_VehicleMaintTypesLink set CurrentOdometer = GetCalcOdo(AssetId)

                    //		DECLARE csrData CURSOR FOR
                    //		SELECT URI, 
                    //			vmtl.MaintURI,
                    //			vmtl.AssetId,
                    //			vmtl.MaintTypeId,
                    //			vmt.OccurrenceType,
                    //			vmtl.NextMaintDate, 
                    //			vmtl.NextMaintDateTG, 
                    //			vmtl.CurrentOdometer, 
                    //			vmtl.NextMaintOdometer, 
                    //			vmtl.NextMaintOdometerTG, 
                    //			vmtl.CurrentEngHrs,
                    //			vmtl.NextMaintEngHrs,
                    //			vmtl.NextMaintEngHrsTG,
                    //			vmtl.[Status]
                    //			, vmt.OccurrenceDuration
                    //			, vmt.OccurrenceDurationTh
                    //			, vmt.OccurrencePeriod_cbo
                    //			, vmt.OccurrenceKM
                    //			, vmt.OccurrenceKMTh 
                    //		FROM GFI_AMM_VehicleMaintTypesLink vmtl
                    //			INNER JOIN GFI_AMM_VehicleMaintTypes vmt ON vmtl.MaintTypeId = vmt.MaintTypeId

                    //		OPEN csrData
                    //		FETCH NEXT FROM csrData INTO @URI, @MaintURI, @AssetId, @MaintTypeId, @OccurrenceType, @NextMaintDate, @NextMaintDateTG, @CurrentOdometer, @NextMaintOdometer, @NextMaintOdometerTG, @CurrentEngHrs, @NextMaintEngHrs, @NextMaintEngHrsTG, @MaintStatusId
                    //			, @OccurrenceDuration, @OccurrenceDurationTh, @OccurrencePeriod_cbo, @OccurrenceKM, @OccurrenceKMTh
                    //		SET @iRecCountProcessed = 0

                    //		WHILE @@FETCH_STATUS = 0
                    //		BEGIN
                    //			IF @NextMaintDate IS NULL SET @NextMaintDate = GETDATE()

                    //			-- Threhold by date range
                    //			declare @Maintdate DateTime
                    //			declare @Thershold int
                    //			set @Thershold = @OccurrenceDurationTh * -1 -- - @OccurrenceDuration
                    //			set @Maintdate = DateAdd(month, @Thershold, GETDATE());
                    //			if @OccurrencePeriod_cbo = 'DAY(S)' 
                    //				set @Maintdate = DateAdd(day, @Thershold, GETDATE());

                    //			select @Thershold, @NextMaintDate, @Maintdate
                    //			--if(@Thershold != 0 and @NextMaintDate <= @Maintdate)
                    //			--	select '*************************************************'

                    //			-- Recurring - Flag As: To Schedule
                    //			IF (
                    //				(@OccurrenceType = 2) 
                    //				AND ((@NextMaintDateTG <= GETDATE() and @OccurrenceDuration > 0) OR ( @CurrentOdometer >= (@NextMaintOdometer - (@OccurrenceKM - @OccurrenceKMTh)) ) OR (@NextMaintEngHrsTG < @CurrentEngHrs) OR (@Thershold != 0 and @NextMaintDate <= @Maintdate)) 
                    //				AND ((@MaintStatusId = @iMaintStatusId_Completed) OR (@MaintStatusId = @iMaintStatusId_Valid)))
                    //			BEGIN
                    //				IF NOT EXISTS(SELECT * FROM GFI_AMM_VehicleMaintTypesLink WHERE AssetId = @AssetId AND MaintTypeId = @MaintTypeId AND URI > @URI)
                    //				BEGIN
                    //					IF (@MaintStatusId = @iMaintStatusId_Completed)
                    //					BEGIN
                    //						INSERT GFI_AMM_VehicleMaintenance (AssetId, MaintTypeId_cbo, MaintStatusId_cbo, StartDate, EndDate, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy) 
                    //						VALUES (@AssetId, @MaintTypeId, @iMaintStatusId_ToSchedule, @NextMaintDate, @NextMaintDate, GETDATE(), CURRENT_USER, GETDATE(), CURRENT_USER)

                    //						SET @MaintURIUpdate = (SELECT SCOPE_IDENTITY())

                    //						--select * from GFI_AMM_VehicleMaintTypes where MaintTypeId = @MaintTypeId and Description = 'ODOMETER/ENGINE HOURS ENTRY'
                    //						--if(@@ROWCOUNT > 0)
                    //						--begin		
                    //							UPDATE GFI_AMM_VehicleMaintTypesLink
                    //							SET MaintURI = @MaintURIUpdate, Status = @iMaintStatusId_ToSchedule, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER --,NextMaintOdometer = @OccurrenceKM + @CurrentOdometer
                    //							WHERE URI = @URI
                    //						--end
                    //					END
                    //					ELSE IF (@MaintStatusId = @iMaintStatusId_Valid)
                    //					BEGIN
                    //						SET @NextMaintDate = DATEADD(DD, 1, @NextMaintDate)

                    //						INSERT GFI_AMM_VehicleMaintenance (AssetId, MaintTypeId_cbo, MaintStatusId_cbo, StartDate, EndDate, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy) 
                    //						VALUES (@AssetId, @MaintTypeId, @iMaintStatusId_ToSchedule, @NextMaintDate, @NextMaintDate, GETDATE(), CURRENT_USER, GETDATE(), CURRENT_USER)

                    //						SET @MaintURIUpdate = (SELECT SCOPE_IDENTITY())

                    //						INSERT GFI_AMM_VehicleMaintTypesLink (MaintURI, AssetId, MaintTypeId, NextMaintDate, NextMaintDateTG, Status, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy)
                    //						VALUES (@MaintURIUpdate, @AssetId, @MaintTypeId, @NextMaintDate, @NextMaintDate, @iMaintStatusId_ToSchedule, GETDATE(), CURRENT_USER, GETDATE(), CURRENT_USER)
                    //					END
                    //				END
                    //			END

                    //			--Flag As: Overdue
                    //			IF (
                    //					(
                    //						(@NextMaintDate <= GETDATE() and @OccurrenceDuration > 0) 
                    //							OR (@NextMaintOdometer < @CurrentOdometer) 
                    //							OR (@NextMaintEngHrs < @CurrentEngHrs)
                    //					) 
                    //					AND (@MaintStatusId = @iMaintStatusId_ToSchedule)
                    //				)
                    //			BEGIN				
                    //				UPDATE GFI_AMM_VehicleMaintTypesLink
                    //				SET Status = @iMaintStatusId_Overdue, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
                    //				WHERE URI = @URI

                    //				UPDATE GFI_AMM_VehicleMaintenance 
                    //				SET MaintStatusId_cbo = @iMaintStatusId_Overdue, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
                    //				WHERE URI = @MaintURI
                    //			END

                    //			--Auto Expiry: Lapsed Insurance
                    //			IF ((@MaintStatusId = @iMaintStatusId_Valid) AND (@NextMaintDate < GETDATE()))
                    //			BEGIN
                    //				UPDATE GFI_AMM_VehicleMaintTypesLink
                    //				SET Status = @iMaintStatusId_Expired , UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
                    //				WHERE URI = @URI

                    //				UPDATE GFI_AMM_VehicleMaintenance 
                    //				SET MaintStatusId_cbo = @iMaintStatusId_Expired, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
                    //				WHERE URI = @MaintURI
                    //			END

                    //			SET @iRecCountProcessed = @iRecCountProcessed + 1

                    //			FETCH NEXT FROM csrData INTO @URI, @MaintURI, @AssetId, @MaintTypeId, @OccurrenceType, @NextMaintDate, @NextMaintDateTG, @CurrentOdometer, @NextMaintOdometer, @NextMaintOdometerTG, @CurrentEngHrs, @NextMaintEngHrs, @NextMaintEngHrsTG, @MaintStatusId
                    //				, @OccurrenceDuration, @OccurrenceDurationTh, @OccurrencePeriod_cbo, @OccurrenceKM, @OccurrenceKMTh
                    //		END

                    //		CLOSE csrData
                    //		DEALLOCATE csrData

                    //		RETURN @iRecCountProcessed
                    //	END
                    //END
                    //";
                    //                    dbExecute(sql);

                    //                    #endregion


                    //                    #region sp_AMM_VehicleMaint_ProcessRecords updated
                    //                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'sp_AMM_VehicleMaint_ProcessRecords') AND type in (N'P', N'PC'))
                    //							DROP PROCEDURE sp_AMM_VehicleMaint_ProcessRecords";
                    //                    dbExecute(sql);

                    //                    sql = @"CREATE PROCEDURE sp_AMM_VehicleMaint_ProcessRecords]
                    //AS
                    //BEGIN
                    //	/***********************************************************************************************************************
                    //	Version: 3.1.0.0
                    //	Modifed: 11/ 08/2016.	06/08/2014
                    //	Created By: Perry Ramen
                    //	Modified By: Reza Dowlut
                    //	Description: Process list of Maintenance Types for each vehicle to determine if they are To Schedule] or Overdue]
                    //	***********************************************************************************************************************/
                    //		BEGIN
                    //		DECLARE @iRecCountProcessed int

                    //		DECLARE @iMaintStatusId_Overdue int
                    //		DECLARE @iMaintStatusId_ToSchedule int
                    //		DECLARE @iMaintStatusId_Scheduled int
                    //		DECLARE @iMaintStatusId_InProgress int
                    //		DECLARE @iMaintStatusId_Completed int
                    //		DECLARE @iMaintStatusId_Valid int
                    //		DECLARE @iMaintStatusId_Expired int

                    //		DECLARE @URI int
                    //		DECLARE @MaintURI int
                    //		DECLARE @AssetId int
                    //		DECLARE @MaintTypeId int
                    //		DECLARE @OccurrenceType int
                    //		DECLARE @NextMaintDate DateTime
                    //		DECLARE @NextMaintDateTG DateTime 
                    //		DECLARE @CurrentOdometer int
                    //		DECLARE @NextMaintOdometer int
                    //		DECLARE @NextMaintOdometerTG int
                    //		DECLARE @CurrentEngHrs int
                    //		DECLARE @NextMaintEngHrs int
                    //		DECLARE @NextMaintEngHrsTG int
                    //		DECLARE @MaintStatusId int

                    //		DECLARE @MaintURIUpdate int

                    //		DECLARE @OccurrenceDuration int
                    //		DECLARE @OccurrenceDurationTh int
                    //		DECLARE @OccurrencePeriod_cbo nvarchar(50)

                    //		SET @iMaintStatusId_Overdue = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Overdue')
                    //		SET @iMaintStatusId_ToSchedule = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'To Schedule')
                    //		SET @iMaintStatusId_Scheduled = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Scheduled')
                    //		SET @iMaintStatusId_InProgress = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'In Progress')		
                    //		SET @iMaintStatusId_Completed = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Completed')				
                    //		SET @iMaintStatusId_Valid = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Valid')		
                    //		SET @iMaintStatusId_Expired = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'Expired')				

                    //		update GFI_AMM_VehicleMaintTypesLink set CurrentOdometer = GetCalcOdo(AssetId)

                    //		DECLARE csrData CURSOR FOR
                    //		SELECT URI, 
                    //			vmtl.MaintURI,
                    //			vmtl.AssetId,
                    //			vmtl.MaintTypeId,
                    //			vmt.OccurrenceType,
                    //			vmtl.NextMaintDate, 
                    //			vmtl.NextMaintDateTG, 
                    //			vmtl.CurrentOdometer, 
                    //			vmtl.NextMaintOdometer, 
                    //			vmtl.NextMaintOdometerTG, 
                    //			vmtl.CurrentEngHrs,
                    //			vmtl.NextMaintEngHrs,
                    //			vmtl.NextMaintEngHrsTG,
                    //			vmtl.[Status]
                    //			, vmt.OccurrenceDuration
                    //			, vmt.OccurrenceDurationTh
                    //			, vmt.OccurrencePeriod_cbo
                    //		FROM GFI_AMM_VehicleMaintTypesLink vmtl
                    //			INNER JOIN GFI_AMM_VehicleMaintTypes vmt ON vmtl.MaintTypeId = vmt.MaintTypeId

                    //		OPEN csrData
                    //		FETCH NEXT FROM csrData INTO @URI, @MaintURI, @AssetId, @MaintTypeId, @OccurrenceType, @NextMaintDate, @NextMaintDateTG, @CurrentOdometer, @NextMaintOdometer, @NextMaintOdometerTG, @CurrentEngHrs, @NextMaintEngHrs, @NextMaintEngHrsTG, @MaintStatusId
                    //			, @OccurrenceDuration, @OccurrenceDurationTh, @OccurrencePeriod_cbo
                    //		SET @iRecCountProcessed = 0

                    //		WHILE @@FETCH_STATUS = 0
                    //		BEGIN
                    //			IF @NextMaintDate IS NULL SET @NextMaintDate = GETDATE()

                    //			--select @URI, 'OccurrenceType 2', @OccurrenceType
                    //			--	, 'AND'
                    //			--	, '@NextMaintDateTG <= GETDATE()', @NextMaintDateTG, GETDATE()
                    //			--	, '@NextMaintOdometerTG < @CurrentOdometer', @NextMaintOdometerTG, @CurrentOdometer
                    //			--	, '@NextMaintEngHrsTG < @CurrentEngHrs', @NextMaintEngHrsTG, @CurrentEngHrs
                    //			--	, 'AND'
                    //			--	, '@MaintStatusId = @iMaintStatusId_Completed', @MaintStatusId, @iMaintStatusId_Completed
                    //			--	, '@MaintStatusId = @iMaintStatusId_Valid', @MaintStatusId, @iMaintStatusId_Valid

                    //			-- Threhold by date range
                    //			declare @Maintdate DateTime
                    //			declare @Thershold int
                    //			set @Thershold = @OccurrenceDurationTh * -1 -- - @OccurrenceDuration
                    //			set @Maintdate = DateAdd(month, @Thershold, GETDATE());
                    //			if @OccurrencePeriod_cbo = 'DAY(S)' 
                    //				set @Maintdate = DateAdd(day, @Thershold, GETDATE());

                    //			select @Thershold, @NextMaintDate, @Maintdate
                    //			--if(@Thershold != 0 and @NextMaintDate <= @Maintdate)
                    //			--	select '*************************************************'

                    //			-- Recurring - Flag As: To Schedule
                    //			IF (
                    //				(@OccurrenceType = 2) 
                    //				AND ((@NextMaintDateTG <= GETDATE()) OR (@NextMaintOdometerTG < @CurrentOdometer) OR (@NextMaintEngHrsTG < @CurrentEngHrs) OR (@Thershold != 0 and @NextMaintDate <= @Maintdate)) 
                    //				AND ((@MaintStatusId = @iMaintStatusId_Completed) OR (@MaintStatusId = @iMaintStatusId_Valid)))
                    //			BEGIN
                    //				IF NOT EXISTS(SELECT * FROM GFI_AMM_VehicleMaintTypesLink WHERE AssetId = @AssetId AND MaintTypeId = @MaintTypeId AND URI > @URI)
                    //				BEGIN
                    //					IF (@MaintStatusId = @iMaintStatusId_Completed)
                    //					BEGIN
                    //						INSERT GFI_AMM_VehicleMaintenance (AssetId, MaintTypeId_cbo, MaintStatusId_cbo, StartDate, EndDate, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy) 
                    //						VALUES (@AssetId, @MaintTypeId, @iMaintStatusId_ToSchedule, @NextMaintDate, @NextMaintDate, GETDATE(), CURRENT_USER, GETDATE(), CURRENT_USER)

                    //						SET @MaintURIUpdate = (SELECT SCOPE_IDENTITY())

                    //						UPDATE GFI_AMM_VehicleMaintTypesLink
                    //						SET MaintURI = @MaintURIUpdate, Status = @iMaintStatusId_ToSchedule, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
                    //						WHERE URI = @URI
                    //					END
                    //					ELSE IF (@MaintStatusId = @iMaintStatusId_Valid)
                    //					BEGIN
                    //						SET @NextMaintDate = DATEADD(DD, 1, @NextMaintDate)

                    //						INSERT GFI_AMM_VehicleMaintenance (AssetId, MaintTypeId_cbo, MaintStatusId_cbo, StartDate, EndDate, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy) 
                    //						VALUES (@AssetId, @MaintTypeId, @iMaintStatusId_ToSchedule, @NextMaintDate, @NextMaintDate, GETDATE(), CURRENT_USER, GETDATE(), CURRENT_USER)

                    //						SET @MaintURIUpdate = (SELECT SCOPE_IDENTITY())

                    //						INSERT GFI_AMM_VehicleMaintTypesLink (MaintURI, AssetId, MaintTypeId, NextMaintDate, NextMaintDateTG, Status, CreatedDate, CreatedBy, UpdatedDate, UpdatedBy)
                    //						VALUES (@MaintURIUpdate, @AssetId, @MaintTypeId, @NextMaintDate, @NextMaintDate, @iMaintStatusId_ToSchedule, GETDATE(), CURRENT_USER, GETDATE(), CURRENT_USER)
                    //					END
                    //				END
                    //			END

                    //			--Flag As: Overdue
                    //			IF (((@NextMaintDate <= GETDATE()) OR (@NextMaintOdometer < @CurrentOdometer) OR (@NextMaintEngHrs < @CurrentEngHrs)) AND (@MaintStatusId = @iMaintStatusId_ToSchedule))
                    //			BEGIN				
                    //				UPDATE GFI_AMM_VehicleMaintTypesLink
                    //				SET Status = @iMaintStatusId_Overdue, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
                    //				WHERE URI = @URI

                    //				UPDATE GFI_AMM_VehicleMaintenance 
                    //				SET MaintStatusId_cbo = @iMaintStatusId_Overdue, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
                    //				WHERE URI = @MaintURI
                    //			END

                    //			--Auto Expiry: Lapsed Insurance
                    //			IF ((@MaintStatusId = @iMaintStatusId_Valid) AND (@NextMaintDate < GETDATE()))
                    //			BEGIN
                    //				UPDATE GFI_AMM_VehicleMaintTypesLink
                    //				SET Status = @iMaintStatusId_Expired , UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
                    //				WHERE URI = @URI

                    //				UPDATE GFI_AMM_VehicleMaintenance 
                    //				SET MaintStatusId_cbo = @iMaintStatusId_Expired, UpdatedDate = GETDATE(), UpdatedBy = CURRENT_USER
                    //				WHERE URI = @MaintURI
                    //			END

                    //			SET @iRecCountProcessed = @iRecCountProcessed + 1

                    //			FETCH NEXT FROM csrData INTO @URI, @MaintURI, @AssetId, @MaintTypeId, @OccurrenceType, @NextMaintDate, @NextMaintDateTG, @CurrentOdometer, @NextMaintOdometer, @NextMaintOdometerTG, @CurrentEngHrs, @NextMaintEngHrs, @NextMaintEngHrsTG, @MaintStatusId
                    //				, @OccurrenceDuration, @OccurrenceDurationTh, @OccurrencePeriod_cbo
                    //		END

                    //		CLOSE csrData
                    //		DEALLOCATE csrData

                    //		RETURN @iRecCountProcessed
                    //	END
                    //END
                    //";
                    //                    dbExecute(sql);
                    //                    #endregion

                    //                    // start Distance SQL functions
                    //                    #region fnCalcDistanceInKM
                    //                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnCalcDistanceInKM') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
                    //							DROP FUNCTION fnCalcDistanceInKM";
                    //                    _SqlConn.ScriptExecute(sql, myStrConn);

                    //                    sql = @"CREATE FUNCTION fnCalcDistanceInKM(@lat1 FLOAT, @lat2 FLOAT, @lon1 FLOAT, @lon2 FLOAT)
                    //							RETURNS FLOAT 
                    //							AS
                    //							BEGIN
                    //								RETURN ACOS(SIN(PI()*@lat1/180.0)*SIN(PI()*@lat2/180.0)+COS(PI()*@lat1/180.0)*COS(PI()*@lat2/180.0)*COS(PI()*@lon2/180.0-PI()*@lon1/180.0))*6371
                    //							END";
                    //                    _SqlConn.ScriptExecute(sql, myStrConn);
                    //                    #endregion

                    //                    #region spGPSDataDistanceFromXY
                    //                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'spGPSDataDistanceFromXY') AND type in (N'P', N'PC'))
                    //							DROP PROCEDURE spGPSDataDistanceFromXY";
                    //                    _SqlConn.ScriptExecute(sql, myStrConn);

                    //                    sql = @"
                    //							-- Stored Procedure
                    //							--    Author     : Reza
                    //							--    Create date: 15/03/2017
                    //							--    Description: Return calculated distance from debug data. 
                    //							--                 If optional parameter @dist is null, return all data, else only data <= @dist    
                    //							Create PROCEDURE spGPSDataDistanceFromXY
                    //								@dtFrom Datetime 
                    //								, @dtTo Datetime 
                    //								, @AssetID int
                    //								, @lat1 FLOAT
                    //								, @lon1 FLOAT
                    //								, @dist int = null
                    //							as Begin
                    //								SELECT *, fnCalcDistanceInKM(@lat1, Latitude, @lon1, Longitude) * 1000 DistanceInM into #dtDist from GFI_GPS_GPSData
                    //									where DateTimeGPS_UTC >= @dtFrom and DateTimeGPS_UTC <= @dtTo
                    //										and AssetID = @AssetID

                    //								if @dist is null
                    //									select * from #dtDist order by AssetID, DateTimeGPS_UTC 
                    //								else  
                    //									select * from #dtDist where DistanceInM <= @dist order by AssetID, DateTimeGPS_UTC                      
                    //							end";
                    //                    _SqlConn.ScriptExecute(sql, myStrConn);
                    //                    #endregion

                    //                    #region spGPSDataInZone
                    //                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'spGPSDataInZone') AND type in (N'P', N'PC'))
                    //							DROP PROCEDURE spGPSDataInZone";
                    //                    _SqlConn.ScriptExecute(sql, myStrConn);

                    //                    sql = @"
                    //							-- Stored Procedure
                    //							--    Author     : Reza
                    //							--    Create date: 15/03/2017
                    //							--    Description: Return debug data with additional field isPointInZone. 
                    //							--                 If optional parameter @ShowAllData is null, return all data, else only data in zone    
                    //							Create PROCEDURE spGPSDataInZone
                    //								@dtFrom Datetime 
                    //								, @dtTo Datetime 
                    //								, @AssetID int
                    //								, @ZoneID int
                    //								, @ShowAllData int = null
                    //							as Begin
                    //								SELECT *, isPointInZone(Longitude, Latitude, @ZoneID) isPointInZone into #dtDist from GFI_GPS_GPSData
                    //									where DateTimeGPS_UTC >= @dtFrom and DateTimeGPS_UTC <= @dtTo
                    //										and AssetID = @AssetID

                    //								if @ShowAllData is null
                    //									select * from #dtDist order by AssetID, DateTimeGPS_UTC 
                    //								else  
                    //									select * from #dtDist where isPointInZone = 1 order by AssetID, DateTimeGPS_UTC                      
                    //							end";
                    //                    _SqlConn.ScriptExecute(sql, myStrConn);
                    //                    #endregion
                    //                    //end

                    //                    #region isPointInVCA optimized
                    //                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[isPointInVCA]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
                    //							DROP FUNCTION isPointInVCA]";

                    //                    dbExecute(sql);
                    //                    sql = @"
                    //Create function isPointInVCA] (@Lon float, @Lat float, @iVCAID int)
                    //returns int
                    //as Begin
                    //	--Extremity Chk
                    //	Declare @iPossibleChk int
                    //	select  @iPossibleChk = VCAID from 
                    //		(select VCAID, min(Latitude) minLat, min(Longitude) minLon, max(Latitude) maxLat, max(Longitude) maxLon  
                    //			from GFI_FLT_VCADetail where VCAID = @iVCAID
                    //			group by VCAID
                    //		) myView
                    //	where 1 = 1
                    //		and (@Lon between minLon and maxLon)
                    //		and (@Lat between minLat and maxLat)

                    //	if(@iPossibleChk is null)
                    //		return 0
                    //	--

                    //	Declare @iResult int
                    //	set @iResult = 0

                    //	DECLARE	@Longitude float] 
                    //	DECLARE	@Latitude float]
                    //	DECLARE	@nLongitude float] 
                    //	DECLARE	@nLatitude float]
                    //	DECLARE	@myLongitude float] 
                    //	DECLARE	@myLatitude float]
                    //	set	@myLongitude = @Lon
                    //	set	@myLatitude = @Lat

                    //	declare @c bit
                    //	set @c = 0

                    //	declare xyCursor cursor  LOCAL SCROLL STATIC FOR  
                    //			select Latitude, Longitude from GFI_FLT_VCADetail
                    //			where VCAID = @iVCAID order by CordOrder

                    //	open xyCursor
                    //		fetch next from xyCursor into @Latitude, @Longitude
                    //		WHILE @@FETCH_STATUS = 0   
                    //		BEGIN 
                    //		fetch PRIOR from xyCursor into @Latitude, @Longitude
                    //		fetch next from xyCursor into @nLatitude, @nLongitude

                    //					if ((((@Latitude <= @myLatitude) and (@myLatitude < @nLatitude))
                    //							or ((@nLatitude <= @myLatitude) and (@myLatitude < @Latitude)))
                    //							and (@myLongitude < (@nLongitude - @Longitude) * (@myLatitude - @Latitude)
                    //								/ (@nLatitude - @Latitude) + @Longitude))

                    //						if(@c = 0)
                    //							set @c = 1
                    //						else
                    //							set @c = 0

                    //						if(@c = 1)
                    //						begin
                    //							Break;
                    //						end

                    //			fetch next from xyCursor into @Latitude, @Longitude
                    //		END
                    //	close xyCursor
                    //	deallocate xyCursor

                    //	if(@c = 0)
                    //		set @iResult = 0
                    //	else
                    //		set @iResult = 1		

                    //	return @iResult
                    //end";
                    //                    _SqlConn.ScriptExecute(sql, myStrConn);
                    //                    #endregion

                    //                    #region updated sp_MOHRequestCode
                    //                    //                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[sp_MOHRequestCode]') AND type in (N'P', N'PC'))
                    //                    //							DROP PROCEDURE sp_MOHRequestCode]";
                    //                    //                    _SqlConn.ScriptExecute(sql, myStrConn);

                    //                    //                    sql = @"
                    //                    //Create PROCEDURE sp_MOHRequestCode] @Code nvarchar(3)
                    //                    //as Begin

                    //                    //	DECLARE @iYear int
                    //                    //	DECLARE @iMonth int
                    //                    //	DECLARE @sYear nvarchar(2)
                    //                    //	DECLARE @iSeed int
                    //                    //	DECLARE @sSeed nvarchar(5)
                    //                    //	DECLARE @ReqNo nvarchar(14)
                    //                    //	DECLARE @idd int
                    //                    //	DECLARE @sdd nvarchar(2)
                    //                    //	DECLARE @imm int
                    //                    //	DECLARE @smm nvarchar(2)

                    //                    //	set @iYear = 0

                    //                    //	;with cta as
                    //                    //	(
                    //                    //		select * from GFI_PLN_MOHRequestCode] where Code] = @Code and iYear = DATEPART(yyyy, GETDATE()) and iMonth = DATEPART(MM, GETDATE())
                    //                    //	)
                    //                    //	select @iYear = iYear, @iSeed = iSeed, @iMonth = iMonth from cta;

                    //                    //	if (@iYear = 0)
                    //                    //	begin
                    //                    //		insert GFI_PLN_MOHRequestCode] values (@Code, DATEPART(yyyy, GETDATE()), 0, DATEPART(MM, GETDATE()));

                    //                    //		;with cta as
                    //                    //		(
                    //                    //			select * from GFI_PLN_MOHRequestCode] where Code] = @Code and iYear = DATEPART(yyyy, GETDATE()) and iMonth = DATEPART(MM, GETDATE())
                    //                    //		)
                    //                    //		select @iYear = iYear, @iSeed = iSeed, @iMonth = iMonth from cta;
                    //                    //	end

                    //                    //	update GFI_PLN_MOHRequestCode] set iSeed = iSeed + 1 where Code] = @Code and iYear = DATEPART(yyyy, GETDATE()) and iMonth = DATEPART(MM, GETDATE())

                    //                    //	;with cta as
                    //                    //	(
                    //                    //		select * from GFI_PLN_MOHRequestCode] where Code] = @Code and iYear = DATEPART(yyyy, GETDATE()) 
                    //                    //	)
                    //                    //	select @iYear = iYear, @iSeed = iSeed from cta;

                    //                    //	--yy
                    //                    //	if(@iYear > 2000)
                    //                    //		set @iYear = @iYear - 2000
                    //                    //	set @sYear = CAST(@iYear AS nvarchar(2))

                    //                    //	--mm
                    //                    //	set @imm = DATEPART(mm,GETDATE());
                    //                    //	if(@imm < 10)
                    //                    //		set @smm = '0' + CAST(@imm AS nvarchar(2))
                    //                    //	else
                    //                    //		set @smm = CAST(@imm AS nvarchar(2))

                    //                    //	--dd
                    //                    //	set @idd = DATEPART(dd,GETDATE());
                    //                    //	if(@idd < 10)
                    //                    //		set @sdd = '0' + CAST(@idd AS nvarchar(2))
                    //                    //	else
                    //                    //		set @sdd = CAST(@idd AS nvarchar(2))

                    //                    //	--Seed
                    //                    //	if(@iSeed < 10)	        set @sSeed = '0000' + CAST(@iSeed AS nvarchar(5))
                    //                    //	else if(@iSeed < 100)	set @sSeed = '000' + CAST(@iSeed AS nvarchar(5))
                    //                    //	else if(@iSeed < 1000)	set @sSeed = '00' + CAST(@iSeed AS nvarchar(5))
                    //                    //	else if(@iSeed < 10000)	set @sSeed = '0' + CAST(@iSeed AS nvarchar(5))
                    //                    //	else                    set @sSeed = CAST(@iSeed AS nvarchar(5))

                    //                    //	set @ReqNo = @Code + @sdd + @smm + @sYear + @sSeed
                    //                    //	select @ReqNo
                    //                    //end";
                    //                    //                    _SqlConn.ScriptExecute(sql, myStrConn);
                    //                    #endregion

                    InsertDBUpdate(strUpd, "SP Maintenance updated");
                }
                #endregion

                #region Update 171. Indexes
                strUpd = "Rez000171";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"drop index IX_GFI_GPS_GPSData_ProcessTrips";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSData", "IX_GFI_GPS_GPSData_ProcessTrips")).Rows[0][0].ToString() == "1")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX IX_GFI_GPS_GPSData_ProcessTrips ON GFI_GPS_GPSData (AssetID,DateTimeGPS_UTC)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_GPSData", "IX_GFI_GPS_GPSData_ProcessTrips")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "Indexes");
                }
                #endregion
                #region Update 172. SecurityToken
                strUpd = "Rez000172";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_SYS_SecurityToken
							(
								  OneTimeToken CHAR(36) NOT NULL
								, UID number(10) NOT NULL
								, UserToken CHAR(36) NOT NULL
								, ExpiryDate timestamp(3) default systimestamp + 1 not null 
								,TimeOut timestamp(3) NOT NULL  
								, CONSTRAINT PK_GFI_SYS_SecurityToken PRIMARY KEY  (OneTimeToken)
							 )";
                    //TimeOut timestamp(3) default INTERVAL '20' mi + systimestamp not null
                    //if (GetDataDT(ExistTableSql("GFI_SYS_SecurityToken")).Rows[0][0].ToString() == "0")
                    //    dbExecute(sql);
                    CreateTable(sql, "GFI_SYS_SecurityToken", "OneTimeToken");

                    sql = "update GFI_FLT_Driver set Status = 'Active' where sDriverName = 'Default Driver' and iButton = '00000000' and sComments = 'Driver for vehicles with no Driver' and Status is null";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "SecurityToken");
                }
                #endregion
                #region Update 173. Assets no longer encrypted
                strUpd = "Rez000173";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"select * from GFI_FLT_Asset";
                    DataTable dtA = new Connection(myStrConn).GetDataDT(sql, myStrConn);
                    foreach (DataRow dr in dtA.Rows)
                    {
                        String AN = Services.BaseService.DecryptMe(dr["AssetName"].ToString());
                        String ANum = Services.BaseService.DecryptMe(dr["AssetNumber"].ToString());
                        if (!String.IsNullOrEmpty(AN))
                        {
                            sql = "update GFI_FLT_Asset set AssetName = '" + AN + "', AssetNumber = '" + ANum + "' where AssetID = " + dr["AssetID"].ToString();
                            _SqlConn.OracleScriptExecute(sql, myStrConn);
                        }
                    }

                    InsertDBUpdate(strUpd, "Assets no longer encrypted");
                }
                #endregion
                #region Update 174. Asset Name n Number no longer encrypted
                strUpd = "Rez000174";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"select * from GFI_FLT_Asset";
                    DataTable dtA = new Connection(myStrConn).GetDataDT(sql, myStrConn);
                    foreach (DataRow dr in dtA.Rows)
                    {
                        String AN = Services.BaseService.DecryptMe(dr["AssetName"].ToString());
                        if (!String.IsNullOrEmpty(AN))
                        {
                            sql = "update GFI_FLT_Asset set AssetName = '" + AN + "' where AssetID = " + dr["AssetID"].ToString();
                            _SqlConn.OracleScriptExecute(sql, myStrConn);
                        }

                        String ANum = Services.BaseService.DecryptMe(dr["AssetNumber"].ToString());
                        if (!String.IsNullOrEmpty(ANum))
                        {
                            sql = "update GFI_FLT_Asset set AssetNumber = '" + ANum + "' where AssetID = " + dr["AssetID"].ToString();
                            _SqlConn.OracleScriptExecute(sql, myStrConn);
                        }
                    }

                    InsertDBUpdate(strUpd, "Asset Name n Number no longer encrypted");
                }
                #endregion
                #region Update 175. GlobalParam Company Parameters and TimeZones
                strUpd = "Rez000175";
                if (!CheckUpdateExist(strUpd))
                {
                    GlobalParam g = new GlobalParam();
                    g = globalParamsService.GetGlobalParamsByName("CompanyParam", sConnStr);
                    if (g == null)
                    {
                        g = new GlobalParam();
                        g.ParamName = "CompanyParam";
                        g.PValue = "none";
                        g.ParamComments = "Company Parameters per database. PValue is folder name";
                        globalParamsService.SaveGlobalParams(g, true, sConnStr);
                    }

                    sql = @"Alter TABLE GFI_SYS_TimeZones] add TotalMinutes float NULL";
                    if (GetDataDT(ExistColumnSql("GFI_SYS_TimeZones", "TotalMinutes")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    dt = GetDataDT("select * from GFI_SYS_TimeZones");
                    foreach (DataRow dr in dt.Rows)
                    {
                        String st = dr["StandardName"].ToString();
                        foreach (TimeZoneInfo z in TimeZoneInfo.GetSystemTimeZones())
                            if (z.StandardName == st)
                            {
                                sql = "update GFI_SYS_TimeZones set TotalMinutes = " + z.BaseUtcOffset.TotalMinutes + " where StandardName = '" + z.StandardName + "'";
                                _SqlConn.OracleScriptExecute(sql, myStrConn);
                                break;
                            }
                    }

                    //sql = "CREATE  INDEX IX_Del_GPS_Exceptions ON GFI_GPS_Exceptions ([AssetID)";
                    //if (GetDataDT(ExistsIndex("GFI_GPS_Exceptions", "IX_Del_GPS_Exceptions")).Rows[0][0].ToString() == "0")
                    //    dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE  INDEX IX_Del_ARC_Exceptions ON GFI_ARC_Exceptions ([AssetID)";
                    if (GetDataDT(ExistsIndex("GFI_ARC_Exceptions", "IX_Del_ARC_Exceptions")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE  INDEX IX_Del_Notif ON GFI_SYS_Notification ([AssetID)";
                    if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "IX_Del_Notif")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "GlobalParam Company Parameters and Timezones");
                }
                #endregion
                #region Update 176. Updated user Support
                strUpd = "Rez000176";
                if (!CheckUpdateExist(strUpd))
                {
                    User u = new User();
                    u = userService.GetUserByeMail("Support@naveo.mu", User.EditModePswd, sConnStr, false, true);
                    u.Password = "SupportMe@2018.";
                    userService.SaveUser(u, false, sConnStr);

                    InsertDBUpdate(strUpd, "Updated user Support");
                }
                #endregion
                #endregion

                #region Update 177. Removing Seed from ARC tables
                strUpd = "Rez000177";
                if (!CheckUpdateExist(strUpd))
                {
                    InsertDBUpdate(strUpd, "Removing Seed from ARC tables");
                }
                #endregion
                #region Update 178. Fuel Process
                strUpd = "Rez000178";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE [dbo].[GFI_GPS_FuelProcessed](
	                        [iID] [int] IDENTITY(1,1) NOT NULL,
	                        [AssetID] [int] NOT NULL,
	                        [DateTimeGPS_UTC] [DateTime] NOT NULL Default systimestamp(3) - 7,
                         CONSTRAINT [PK_FuelProcessed] PRIMARY KEY ([iID] ASC)
                        )";
                    if (GetDataDT(ExistTableSql("GFI_GPS_FuelProcessed")).Rows[0][0].ToString() == "0")
                        CreateTable(sql, "GFI_GPS_FuelProcessed", "iID");

                    sql = @"ALTER TABLE [dbo].[GFI_GPS_FuelProcessed] 
	                        ADD CONSTRAINT [FK_GFI_GPS_FuelProcessed_GFI_FLT_Asset] FOREIGN KEY([AssetID])
                        REFERENCES [dbo].[GFI_FLT_Asset] ([AssetID])";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_FuelProcessed_GFI_FLT_Asset", "GFI_GPS_FuelProcessed")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"CREATE TABLE GFI_GPS_FuelData
                            (
	                            UID int NOT NULL,
	                            AssetID int NOT NULL,
	                            DateTimeGPS_UTC datetime NOT NULL,
                                ConvertedValue float null,
                                UOM int null,
                                fType nvarchar(15),
                                CONSTRAINT [PK_GFI_GPS_FuelData] PRIMARY KEY CLUSTERED ( [UID] ASC)
                        )";
                    if (GetDataDT(ExistTableSql("GFI_GPS_FuelData")).Rows[0][0].ToString() == "0")
                        CreateTable(sql, "GFI_GPS_FuelData", "UID");

                    sql = @"ALTER TABLE [dbo].[GFI_GPS_FuelData] 
	                            ADD CONSTRAINT [FK_GFI_GPS_FuelData_GFI_GPS_GPSData] FOREIGN KEY([UID])
                                REFERENCES [dbo].[GFI_GPS_GPSData] ([UID])
                                ON UPDATE NO ACTION 
	                            ON DELETE CASCADE ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_FuelData_GFI_GPS_GPSData", "GFI_GPS_FuelData")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE [dbo].[GFI_GPS_FuelData]
	                        ADD CONSTRAINT [FK_GFI_GPS_FuelData_GFI_FLT_Asset] FOREIGN KEY([AssetID])
                            REFERENCES [dbo].[GFI_FLT_Asset] ([AssetID])
                                ON UPDATE NO ACTION 
	                            ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_FuelData_GFI_FLT_Asset", "GFI_GPS_FuelData")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE dbo.GFI_GPS_FuelData 
                            ADD CONSTRAINT FK_GFI_GPS_FuelData_GFI_SYS_UOM 
                                FOREIGN KEY(UOM) 
                                REFERENCES dbo.GFI_SYS_UOM(UID) 
                         ON UPDATE NO ACTION 
	                     ON DELETE Cascade ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_GPS_FuelData_GFI_SYS_UOM", "GFI_GPS_FuelData")).Rows.Count == 0)
                        dbExecute(sql);

                    if (GetDataDT(ExistColumnSql("GFI_GPS_FuelData", "iID")).Rows[0][0].ToString() != "0")
                    {
                        sql = "delete from GFI_GPS_FuelProcessed";
                        dbExecute(sql);

                        sql = "delete from GFI_GPS_FuelData";
                        dbExecute(sql);

                        sql = "ALTER TABLE GFI_GPS_FuelData DROP CONSTRAINT[PK_GFI_GPS_FuelData]";
                        dbExecute(sql);

                        sql = @"ALTER TABLE GFI_GPS_FuelData drop column iID";
                        dbExecute(sql);

                        sql = "ALTER TABLE GFI_GPS_FuelData ADD CONSTRAINT PK_GFI_GPS_FuelData PRIMARY KEY CLUSTERED ([UID] ASC)";
                        dbExecute(sql);
                    }

                    RulesService ruleService = new RulesService();
                    Models.Rules.Rule fuelRule = ruleService.GetFuelRule("Fill", sConnStr);
                    if (fuelRule == null)
                    {
                        fuelRule = new Models.Rules.Rule();
                        fuelRule.Struc = "Fuel";
                        fuelRule.StrucValue = "Fill";
                        fuelRule.RuleName = "Fuel Fill Detected";
                        fuelRule.RuleType = 9;
                        fuelRule.MinTriggerValue = 0;
                        fuelRule.MaxTriggerValue = 0;
                        ruleService.SaveFuelRule(fuelRule, sConnStr);
                    }

                    ruleService = new RulesService();
                    fuelRule = ruleService.GetFuelRule("Drop", sConnStr);
                    if (fuelRule == null)
                    {
                        fuelRule = new Models.Rules.Rule();
                        fuelRule.Struc = "Fuel";
                        fuelRule.StrucValue = "Drop";
                        fuelRule.RuleName = "Fuel Drop Suspected";
                        fuelRule.RuleType = 9;
                        fuelRule.MinTriggerValue = 0;
                        fuelRule.MaxTriggerValue = 0;
                        ruleService.SaveFuelRule(fuelRule, sConnStr);
                    }

                    InsertDBUpdate(strUpd, "Fuel Process");
                }
                #endregion
                #region Update 179. IX_TripExceptions
                strUpd = "Rez000179";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "CREATE NONCLUSTERED INDEX [IX_TripExceptions] ON [GFI_GPS_Exceptions] ([GPSDataID]) INCLUDE ([RuleID],[GroupID])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_Exceptions", "IX_TripExceptions")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "IX_TripExceptions");
                }
                #endregion
                #region Update 180. Indexes
                strUpd = "Rez000180";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"DROP INDEX [IX_GFI_ARC_GPSData_AssetID]";
                    if (GetDataDT(ExistsIndex("GFI_ARC_GPSData", "IX_GFI_ARC_GPSData_AssetID")).Rows[0][0].ToString() == "1")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"DROP INDEX [IX_GFI_ARC_GPSData_DateTimeGPS_UTC]";
                    if (GetDataDT(ExistsIndex("GFI_ARC_GPSData", "IX_GFI_ARC_GPSData_DateTimeGPS_UTC")).Rows[0][0].ToString() == "1")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"DROP INDEX [myARCIdxDT]";
                    if (GetDataDT(ExistsIndex("GFI_ARC_GPSData", "myARCIdxDT")).Rows[0][0].ToString() == "1")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX [IX_ARC_GPSData] ON [GFI_ARC_GPSData] ([AssetID],[DateTimeGPS_UTC], [UID])";
                    if (GetDataDT(ExistsIndex("GFI_ARC_GPSData", "IX_ARC_GPSData")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX [IX_GFI_SYS_GroupMatrixZone_iID_GMID] ON [GFI_SYS_GroupMatrixZone] ([iID],[GMID])";
                    if (GetDataDT(ExistsIndex("GFI_SYS_GroupMatrixZone", "IX_GFI_SYS_GroupMatrixZone_iID_GMID")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "DROP INDEX [IX_Del_Exceptions]";
                    if (GetDataDT(ExistsIndex("GFI_GPS_Exceptions", "IX_Del_Exceptions")).Rows[0][0].ToString() == "1")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "DROP INDEX [IX_GFI_GPS_Exceptions_RuleID_AssetID_DateTimeGPS]";
                    if (GetDataDT(ExistsIndex("GFI_GPS_Exceptions", "IX_GFI_GPS_Exceptions_RuleID_AssetID_DateTimeGPS")).Rows[0][0].ToString() == "1")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"DROP INDEX [Missing_IXNC_GFI_GPS_Exceptions_AssetID_DateTimeGPS_UTC_CFE72]";
                    if (GetDataDT(ExistsIndex("GFI_GPS_Exceptions", "Missing_IXNC_GFI_GPS_Exceptions_AssetID_DateTimeGPS_UTC_CFE72")).Rows[0][0].ToString() == "1")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE NONCLUSTERED INDEX [IX_GFI_GPS_Exceptions_AssetID_DateTimeGPS_GrpID] ON [dbo].[GFI_GPS_Exceptions] ([AssetID], [DateTimeGPS_UTC], GroupID)";
                    if (GetDataDT(ExistsIndex("GFI_GPS_Exceptions", "IX_GFI_GPS_Exceptions_AssetID_DateTimeGPS_GrpID")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"DROP INDEX [IX_GFI_GPS_TripHeader_AssetID_dtStart_dtEnd]";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "IX_GFI_GPS_TripHeader_AssetID_dtStart_dtEnd")).Rows[0][0].ToString() == "1")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE NONCLUSTERED INDEX [IX_GFI_GPS_TripHeader_AssetID_dtStart_dtEnd] ON [dbo].[GFI_GPS_TripHeader] ([AssetID], [dtStart], [dtEnd],
                                [iID], [DriverID], [ExceptionFlag], [MaxSpeed], [IdlingTime], [StopTime], [TripDistance], [TripTime], [GPSDataStartUID], [GPSDataEndUID], [fStartLon], [fStartLat], [fEndLon], [fEndLat], [OverSpeed1Time], [OverSpeed2Time], [Accel], [Brake], [Corner])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripHeader", "IX_GFI_GPS_TripHeader_AssetID_dtStart_dtEnd")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    InsertDBUpdate(strUpd, "Indexes");
                }
                #endregion
                #region Update 181. GFI_GPS_FuelData Lon Lat
                strUpd = "Rez000181";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "CREATE INDEX [IX_Notification_AssetID_InsertedAt] ON GFI_SYS_Notification ([AssetID],[InsertedAt], [NID])";
                    if (GetDataDT(ExistsIndex("GFI_SYS_Notification", "IX_Notification_AssetID_InsertedAt")).Rows.Count == 0)
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "alter table GFI_GPS_FuelData add [Longitude] [float] NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_FuelData", "Longitude")).Rows[0][0].ToString() == "0")
                        dbExecute(sql);

                    sql = "alter table GFI_GPS_FuelData add [Latitude] [float] NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_FuelData", "Latitude")).Rows[0][0].ToString() == "0")
                        dbExecute(sql);

                    sql = @"UPDATE GFI_GPS_FuelData 
                                SET Longitude = T2.Longitude, Latitude = T2.Latitude, DateTimeGPS_UTC = T2.DateTimeGPS_UTC
                            FROM        GFI_GPS_FuelData T1
                            INNER JOIN  GFI_GPS_GPSData T2 ON T2.UID = T1.UID
                            where T1.Longitude is null";
                    //dbExecute(sql);

                    InsertDBUpdate(strUpd, "GFI_GPS_FuelData Lon Lat");
                }
                #endregion
                #region Update 182. AssetTypeMapping
                strUpd = "Rez000182";
                if (!CheckUpdateExist(strUpd))
                {
                    AssetTypeMappingService s = new AssetTypeMappingService();
                    AssetDataType assetDataType = new AssetDataTypeService().GetAssetDataTypeByField1Name("**Default**", sConnStr);
                    AssetTypeMapping atm = s.GetAssetTypeMappingByDescription("Light Vehicle", sConnStr);
                    if (atm == null)
                    {
                        atm = new AssetTypeMapping();
                        atm.Description = "Light Vehicle";
                        atm.MappingId = 1;
                        atm.AssetTypeId = assetDataType.IID;
                        atm.CreatedBy = usrInst.Username;
                        atm.CreatedDate = DateTime.Now;
                        s.SaveAssetTypeMapping(atm, true, sConnStr);
                    }

                    atm = s.GetAssetTypeMappingByDescription("Truck", sConnStr);
                    if (atm == null)
                    {
                        atm = new AssetTypeMapping();
                        atm.Description = "Truck";
                        atm.MappingId = 1;
                        atm.AssetTypeId = assetDataType.IID;
                        atm.CreatedBy = usrInst.Username;
                        atm.CreatedDate = DateTime.Now;
                        s.SaveAssetTypeMapping(atm, true, sConnStr);
                    }

                    atm = s.GetAssetTypeMappingByDescription("Heavy Truck", sConnStr);
                    if (atm == null)
                    {
                        atm = new AssetTypeMapping();
                        atm.Description = "Heavy Truck";
                        atm.MappingId = 1;
                        atm.AssetTypeId = assetDataType.IID;
                        atm.CreatedBy = usrInst.Username;
                        atm.CreatedDate = DateTime.Now;
                        s.SaveAssetTypeMapping(atm, true, sConnStr);
                    }

                    atm = s.GetAssetTypeMappingByDescription("Off Road", sConnStr);
                    if (atm == null)
                    {
                        atm = new AssetTypeMapping();
                        atm.Description = "Off Road";
                        atm.MappingId = 1;
                        atm.AssetTypeId = assetDataType.IID;
                        atm.CreatedBy = usrInst.Username;
                        atm.CreatedDate = DateTime.Now;
                        s.SaveAssetTypeMapping(atm, true, sConnStr);
                    }

                    atm = s.GetAssetTypeMappingByDescription("Motorcycle", sConnStr);
                    if (atm == null)
                    {
                        atm = new AssetTypeMapping();
                        atm.Description = "Motorcycle";
                        atm.MappingId = 1;
                        atm.AssetTypeId = assetDataType.IID;
                        atm.CreatedBy = usrInst.Username;
                        atm.CreatedDate = DateTime.Now;
                        s.SaveAssetTypeMapping(atm, true, sConnStr);
                    }

                    InsertDBUpdate(strUpd, "AssetTypeMapping");
                }
                #endregion
                #region Update 183. GFI_FLT_AssetExt
                strUpd = "Rez000183";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_FLT_AssetExt
                            (
	                            AssetId int NOT NULL,
	                            MakeID int NULL,
	                            ModelID int NULL,
	                            ColorID int NULL,
	                            YearManufactured int NULL,
	                            PurchasedDate datetime NULL,
	                            PurchasedValue numeric(18, 2) NULL,
	                            RefA nvarchar(120) NULL,
	                            RefB nvarchar(120) NULL,
	                            RefC nvarchar(120) NULL,
	                            RefD nvarchar(120) NULL,
	                            ExternalRef nvarchar(120) NULL,
	                            EngineSerialNumber nvarchar(120) NULL,
	                            EngineCapacity int NULL,
	                            EnginePower int NULL,
                                    CONSTRAINT PK_GFI_FLT_AssetExt PRIMARY KEY CLUSTERED (AssetId ASC)
                            )";
                    if (GetDataDT(ExistTableSql("GFI_FLT_AssetExt")).Rows[0][0].ToString() == "0")
                        CreateTable(sql, "GFI_FLT_AssetExt", String.Empty);

                    sql = @"ALTER TABLE dbo.GFI_FLT_AssetExt 
                            ADD CONSTRAINT FK_GFI_FLT_AssetExt_GFI_FLT_Asset
                                FOREIGN KEY(AssetID) 
                                REFERENCES dbo.GFI_FLT_Asset (AssetID) 
                         ON UPDATE NO ACTION 
	                     ON DELETE Cascade";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_AssetExt_GFI_FLT_Asset", "GFI_FLT_AssetExt")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE dbo.GFI_FLT_AssetExt 
                            ADD CONSTRAINT FK_GFI_FLT_AssetExt_GFI_SYS_LookUpValues_MakeID 
                                FOREIGN KEY(MakeID) 
                                REFERENCES dbo.GFI_SYS_LookUpValues (VID) 
                         ON UPDATE NO ACTION 
	                     ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_AssetExt_GFI_SYS_LookUpValues_MakeID", "GFI_FLT_AssetExt")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE dbo.GFI_FLT_AssetExt 
                            ADD CONSTRAINT FK_GFI_FLT_AssetExt_GFI_SYS_LookUpValues_ModelID 
                                FOREIGN KEY(ModelID) 
                                REFERENCES dbo.GFI_SYS_LookUpValues (VID) 
                         ON UPDATE NO ACTION 
	                     ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_AssetExt_GFI_SYS_LookUpValues_ModelID", "GFI_FLT_AssetExt")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"ALTER TABLE dbo.GFI_FLT_AssetExt 
                            ADD CONSTRAINT FK_GFI_FLT_AssetExt_GFI_SYS_LookUpValues_ColorID 
                                FOREIGN KEY(ColorID) 
                                REFERENCES dbo.GFI_SYS_LookUpValues (VID) 
                         ON UPDATE NO ACTION 
	                     ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_AssetExt_GFI_SYS_LookUpValues_ColorID", "GFI_FLT_AssetExt")).Rows.Count == 0)
                        dbExecute(sql);

                    sql = @"insert GFI_FLT_AssetExt (AssetID)
	                            select AssetID from GFI_FLT_Asset where AssetID not in (select AssetID from GFI_FLT_AssetExt)";
                    dbExecute(sql);

                    //GFI_FLT_AssetExt fields
                    sql = @"UPDATE GFI_FLT_AssetExt 
                                    SET YearManufactured = T2.YearManufactured
                                        , PurchasedDate = T2.PurchasedDate
                                        , PurchasedValue = T2.PurchasedValue
                                        , EngineSerialNumber = T2.EngineSerialNumber
                                        , EngineCapacity = T2.EngineCapacity
                                        , EnginePower = T2.EnginePower
                                        , ExternalRef = T2.ExternalReference
                                FROM        GFI_FLT_AssetExt T1
                                INNER JOIN  GFI_AMM_AssetExtProVehicles T2 ON T2.AssetID = T1.AssetID";
                    //dbExecute(sql);

                    //GFI_FLT_AssetExt Make
                    sql = @"UPDATE GFI_FLT_AssetExt 
                                    SET MakeID = T2.VID
                                FROM   GFI_AMM_AssetExtProVehicles T1
	                                INNER JOIN  GFI_SYS_LookUpValues T2 ON T2.Description = T1.Make
	                                inner join GFI_SYS_LookUpTypes T3 on T3.TID = T2.TID 
	                                inner join GFI_FLT_AssetExt T4 on T4.AssetID = T1.AssetId
                                where T3.Code = 'MAKE_TYPE'";
                    //dbExecute(sql);

                    //GFI_FLT_AssetExt Model
                    LookUpTypes lt = new LookUpTypes();
                    LookUpTypesService lts = new LookUpTypesService();
                    lt = lts.GetLookUpTypesByCode("Model", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "Model";
                        lt.Description = "Model Type";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lts.SaveLookUpTypes(lt, true, sConnStr);
                    }
                    lt = lts.GetLookUpTypesByCode("Model", sConnStr);

                    LookUpValuesService lvs = new LookUpValuesService();
                    LookUpValues lv = lookUpValuesService.GetLookUpValuesByName("A3", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "A3";
                        lv.Description = "A3";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;
                        lv.TID = lt.TID;
                        lvs.SaveLookUpValues(lv, true, sConnStr);
                    }

                    sql = @"UPDATE GFI_FLT_AssetExt 
                                    SET ModelID = T2.VID
                                FROM   GFI_AMM_AssetExtProVehicles T1
	                                INNER JOIN  GFI_SYS_LookUpValues T2 ON T2.Description = T1.Make
	                                inner join GFI_SYS_LookUpTypes T3 on T3.TID = T2.TID 
	                                inner join GFI_FLT_AssetExt T4 on T4.AssetID = T1.AssetId
                                where T3.Code = 'Model'";
                    //dbExecute(sql);

                    //GFI_FLT_AssetExt Color
                    sql = @"UPDATE GFI_FLT_AssetExt 
                                    SET ColorID = T2.ColorID
                                FROM        GFI_FLT_AssetExt T1
                                INNER JOIN  GFI_FLT_Asset T2 ON T2.AssetID = T1.AssetID";
                    //dbExecute(sql);

                    //GFI_FLT_DeviceAuxilliaryMap
                    sql = "update GFI_FLT_DeviceAuxilliaryMap set field3 = null";
                    dbExecute(sql);

                    sql = @"ALTER TABLE dbo.GFI_FLT_DeviceAuxilliaryMap 
                            ADD CONSTRAINT FK_GFI_FLT_DeviceAuxilliaryMap_GFI_SYS_LookUpValues_Field3 
                                FOREIGN KEY(Field3) 
                                REFERENCES dbo.GFI_SYS_LookUpValues (VID) 
                         ON UPDATE NO ACTION 
	                     ON DELETE NO ACTION ";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_DeviceAuxilliaryMap_GFI_SYS_LookUpValues_Field3", "GFI_FLT_DeviceAuxilliaryMap")).Rows.Count == 0)
                        dbExecute(sql);

                    lt = lts.GetLookUpTypesByCode("CallibrationProduct", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "CallibrationProduct";
                        lt.Description = "Callibration Product";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lts.SaveLookUpTypes(lt, true, sConnStr);
                    }
                    lt = lts.GetLookUpTypesByCode("CallibrationProduct", sConnStr);

                    lvs = new LookUpValuesService();
                    lv = lookUpValuesService.GetLookUpValuesByName("Gasoline", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "Gasoline";
                        lv.Description = "Gasoline";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;
                        lv.TID = lt.TID;
                        lvs.SaveLookUpValues(lv, true, sConnStr);
                    }

                    lv = lookUpValuesService.GetLookUpValuesByName("LPG", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "LPG";
                        lv.Description = "LPG";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;
                        lv.TID = lt.TID;
                        lvs.SaveLookUpValues(lv, true, sConnStr);
                    }

                    lv = lookUpValuesService.GetLookUpValuesByName("Diesel", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "Diesel";
                        lv.Description = "Diesel";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;
                        lv.TID = lt.TID;
                        lvs.SaveLookUpValues(lv, true, sConnStr);
                    }

                    lv = lookUpValuesService.GetLookUpValuesByName("Heavy oil", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "Heavy oil";
                        lv.Description = "Heavy oil";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;
                        lv.TID = lt.TID;
                        lvs.SaveLookUpValues(lv, true, sConnStr);
                    }

                    lv = lookUpValuesService.GetLookUpValuesByName("Water", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "Water";
                        lv.Description = "Water";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;
                        lv.TID = lt.TID;
                        lvs.SaveLookUpValues(lv, true, sConnStr);
                    }

                    //Keeping for V1
                    //sql = @"ALTER TABLE dbo.GFI_FLT_Asset Drop column ColorID int NULL";
                    //dbExecute(sql);

                    InsertDBUpdate(strUpd, "GFI_FLT_AssetExt");
                }
                #endregion
                #region Update 184. Automatic Reporting
                strUpd = "Rez000184";
                if (!CheckUpdateExist(strUpd))
                {
                    InsertDBUpdate(strUpd, "Automatic Reporting");
                }
                #endregion
                #region Update 185. SYS_AuditAccess
                strUpd = "Rez000185";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE GFI_SYS_AuditTrace
                            (
                                  AuditTraceId bigint NOT NULL IDENTITY(1,1)
                                , UserToken VARCHAR(64) NOT NULL
                                , Controller NVARCHAR(25) NULL
                                , Action NVARCHAR(25) NULL
                                , IsAccessGranted INT NULL
                                , AccessedOn DATETIME NULL
                                , CONSTRAINT PK_GFI_SYS_AuditTrace PRIMARY KEY CLUSTERED(AuditTraceId ASC)
                            )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_AuditTrace")).Rows[0][0].ToString() == "0")
                        CreateTable(sql, "GFI_SYS_AuditTrace", "AuditTraceId");

                    InsertDBUpdate(strUpd, "SYS_AuditAccess");
                }
                #endregion
                #region Update 188. Audit login.
                strUpd = "Rez000188";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = @"CREATE TABLE [dbo].[GFI_SYS_AuditTraceLogin]
                            (
                                [AuditTraceLoginId] number(19) NOT NULL IDENTITY(1,1)
                                , [Email] NVARCHAR(50) NULL
                                , [sAction] NVARCHAR(6) NULL
                                , [accessedOn] DATETIME NOT NULL Default GetDate()
                                , [IsAccessGranted] INT NULL
                                , CONSTRAINT [PK_GFI_SYS_AuditTraceLogin] PRIMARY KEY CLUSTERED([AuditTraceLoginId] ASC)
                            )";
                    if (GetDataDT(ExistTableSql("GFI_SYS_AuditTraceLogin")).Rows[0][0].ToString() == "0")
                        CreateTable(sql, "GFI_SYS_AuditTraceLogin", "AuditTraceLoginId");

                    InsertDBUpdate(strUpd, "SYS_AuditAccess");
                }
                #endregion
                #region Update 189. Missing indexes.
                strUpd = "Rez000189";
                if (!CheckUpdateExist(strUpd))
                {
                    LookUpTypes lt = new LookUpTypes();
                    LookUpTypesService lts = new LookUpTypesService();
                    lt = lts.GetLookUpTypesByCode("Model", sConnStr);
                    if (lt == null)
                    {
                        lt = new LookUpTypes();
                        lt.Code = "Model";
                        lt.Description = "Model Type";
                        lt.CreatedBy = usrInst.UID;
                        lt.CreatedDate = DateTime.Now;

                        lts.SaveLookUpTypes(lt, true, sConnStr);
                    }
                    lt = lts.GetLookUpTypesByCode("Model", sConnStr);

                    LookUpValuesService lvs = new LookUpValuesService();
                    LookUpValues lv = lookUpValuesService.GetLookUpValuesByName("A3", sConnStr);
                    if (lv == null)
                    {
                        lv = new LookUpValues();
                        lv.Name = "A3";
                        lv.Description = "A3";
                        lv.CreatedBy = usrInst.UID;
                        lv.CreatedDate = DateTime.Now;
                        lv.TID = lt.TID;
                        lvs.SaveLookUpValues(lv, true, sConnStr);
                    }

                    sql = "CREATE INDEX IX_Exceptions_Trips ON GFI_GPS_Exceptions ([DateTimeGPS_UTC], [RuleID], [GPSDataID], [GroupID])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_Exceptions", "IX_Exceptions_Trips")).Rows.Count == 0)
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    InsertDBUpdate(strUpd, "Missing indexes");
                }
                #endregion

                #region Update 190. TypeVaule renamed. WIP
                strUpd = "Rez000190";
                if (!CheckUpdateExist(strUpd))
                {
                    #region Transit tables. Not in SQL
                    sql = "CREATE GLOBAL TEMPORARY TABLE GlobalTmpAssetStatusAssets (iAssetID int) ON COMMIT PRESERVE ROWS";
                    CreateTable(sql, "GlobalTmpAssetStatusAssets", String.Empty);

                    sql = @"
CREATE GLOBAL TEMPORARY TABLE GlobalTmpAssetStatusTable 
(
    	assetID INT,
    	deviceID NVARCHAR(20),
    	gmDescription NVARCHAR(50),
    	gmID INT,
		dtLastRptd DateTime,
    	speed INT,
    	engineOn INT,
		details nvarchar(500),
		daysNoReport int,
        color nvarchar(15)
)  ON COMMIT PRESERVE ROWS
";
                    CreateTable(sql, "GlobalTmpAssetStatusTable", String.Empty);

                    sql = @"	
CREATE GLOBAL TEMPORARY TABLE GlobalTmpTripProcessor_GFI_GPS_GPSData 
	(
		[RowNumber] [int] IDENTITY(1,1) NOT NULL,
		[UID] [int] PRIMARY KEY CLUSTERED,
		[AssetID] [int] NULL,
		[DateTimeGPS_UTC] [datetime] NULL,
		[DateTimeServer] [datetime] NULL,
		[Longitude] [float] NULL,
		[Latitude] [float] NULL,
		[LongLatValidFlag] [int] NULL,
		[Speed] [float] NULL,
		[EngineOn] [int] NULL,
		[StopFlag] [int] NULL,
		[TripDistance] [float] NULL,
		[TripTime] [float] NULL,
		[WorkHour] [int] NULL,
		[DriverID] [int] NOT NULL,
        Misc1 nvarchar(100) null,
		Misc2 nvarchar(100) null,
		Misc3 nvarchar(100) null
	)
 ON COMMIT DELETE ROWS";
                    CreateTable(sql, "GlobalTmpTripProcessor_GFI_GPS_GPSData", "Row_Number");

                    sql = @"
CREATE INDEX myGlobalTmpTripProcessorIndex ON GlobalTmpTripProcessor_GFI_GPS_GPSData 
(
	[AssetID] ASC,
	[DateTimeGPS_UTC] ASC,
	[LongLatValidFlag] ASC,
	[StopFlag] ASC,
	[Speed] ASC
)
";
                    if (GetDataDT(ExistsIndex("GlobalTmpTripProcessor_GFI_GPS_GPSData", "myGlobalTmpTripProcessorIndex")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"
CREATE GLOBAL TEMPORARY TABLE GlobalTmpTripProcessorGFI_GPS_TripHeader
(
	[iID] [int]  NOT NULL PRIMARY KEY CLUSTERED,
	[AssetID] [int] NOT NULL,
	[DriverID] [int] NOT NULL,
	[ExceptionFlag] [int] NULL,
	[MaxSpeed] [int] NULL,
	[IdlingTime] [float] NULL,
	[StopTime] [float] NULL,
	[TripDistance] [float] NULL,
	[TripTime] [float] NULL,
	[GPSDataStartUID] [int] NULL,
	[GPSDataEndUID] [int] NULL,
	[dtStart] [datetime] NULL,
	[fStartLon] [float] NULL,
	[fStartLat] [float] NULL,
	[dtEnd] [datetime] NULL,
	[fEndLon] [float] NULL,
	[fEndLat] [float] NULL,
	[OverSpeed1Time] [float] Default 0,
	[OverSpeed2Time] [float] Default 0,
	[Accel] [int] Default 0,
	[Brake] [int] Default 0,
	[Corner] [int] Default 0
	) ON COMMIT DELETE ROWS 
";
                    CreateTable(sql, "GlobalTmpTripProcessorGFI_GPS_TripHeader", String.Empty);

                    sql = @"
CREATE GLOBAL TEMPORARY TABLE GlobalTmpTripProcessor_trip
(
    UID int null
    , DateTimeGPS_UTC datetime null
    , Speed float null
    , Row_Num int null
) ON COMMIT DELETE ROWS ";
                    CreateTable(sql, "GlobalTmpTripProcessor_trip", String.Empty);

                    sql = @"
CREATE GLOBAL TEMPORARY TABLE GlobalTmpTripProcessor_idles
(
    UID int null
    , DateTimeGPS_UTC datetime null
    , Speed float null
    , Row_Num int null
    , PreviousIdleValue int null
    , NextIdleValue int null
    , GrpID int default 0
) ON COMMIT DELETE ROWS ";
                    CreateTable(sql, "GlobalTmpTripProcessor_idles", String.Empty);


                    sql = @"
CREATE GLOBAL TEMPORARY TABLE GlobalTmpTripProcessorGFI_GPS_TripDetail
(
	[RowNumber] [int] IDENTITY(1,1) PRIMARY KEY CLUSTERED NOT NULL,
	HeaderiID int,
	GPSDataUID int
)
";
                    CreateTable(sql, "GlobalTmpTripProcessorGFI_GPS_TripDetail", "RowNumber");

                    sql = @"
create or replace function PI
  RETURN NUMBER as
  
  pi number;
  begin 
   pi :=3.14159265358979;
   return pi;
 end;
";
                    _SqlConn.OracleScriptExecute(sql, sConnStr);

                    sql = @"
create or replace function DATEDIFF(TimeType nvarchar2, dtFr timestamp, dtTo timestamp)
RETURN NUMBER as
    myResult number;
begin 
    if(LOWER(TimeType) = 'second') then
        myResult := round((cast(dtTo as date) - date '1970-01-01')*24*60*60 - (cast(dtFr as date) - date '1970-01-01')*24*60*60);
    end if;
    
    if(LOWER(TimeType) = 'minute') then
        myResult := round((cast(dtTo as date) - date '1970-01-01')*24*60*60 - (cast(dtFr as date) - date '1970-01-01')*24*60);
    end if;
    
    if(LOWER(TimeType) = 'hour') then
        myResult := round((cast(dtTo as date) - date '1970-01-01')*24*60*60 - (cast(dtFr as date) - date '1970-01-01')*24);
    end if;    
    
    return myResult;
end;";
                    _SqlConn.OracleScriptExecute(sql, sConnStr);

                    sql = @"	
CREATE GLOBAL TEMPORARY TABLE GlobalTmpTripProcessor_OuterT 
	(
		[RowNumber] [int] NULL,
		[UID] [int] PRIMARY KEY CLUSTERED,
		[AssetID] [int] NULL,
		[DateTimeGPS_UTC] [datetime] NULL,
		[DateTimeServer] [datetime] NULL,
		[Longitude] [float] NULL,
		[Latitude] [float] NULL,
		[LongLatValidFlag] [int] NULL,
		[Speed] [float] NULL,
		[EngineOn] [int] NULL,
		[StopFlag] [int] NULL,
		[TripDistance] [float] NULL,
		[TripTime] [float] NULL,
		[WorkHour] [int] NULL,
		[DriverID] [int] NOT NULL,
        Misc1 nvarchar(100) null,
		Misc2 nvarchar(100) null,
		Misc3 nvarchar(100) null
		, PreviousOuterValue int null
		, NextOuterValue int null
		, GrpID int default 0
	)
 ON COMMIT DELETE ROWS";
                    CreateTable(sql, "GlobalTmpTripProcessor_OuterT", String.Empty);

                    sql = @"	
CREATE GLOBAL TEMPORARY TABLE GlobalTmpTripProcessor_AllRows 
	(
		[RowNumber] [int] NULL,
		[UID] [int] PRIMARY KEY CLUSTERED,
		[AssetID] [int] NULL,
		[DateTimeGPS_UTC] [datetime] NULL,
		[DateTimeServer] [datetime] NULL,
		[Longitude] [float] NULL,
		[Latitude] [float] NULL,
		[LongLatValidFlag] [int] NULL,
		[Speed] [float] NULL,
		[EngineOn] [int] NULL,
		[StopFlag] [int] NULL,
		[TripDistance] [float] NULL,
		[TripTime] [float] NULL,
		[WorkHour] [int] NULL,
		[DriverID] [int] NOT NULL,
        Misc1 nvarchar(100) null,
		Misc2 nvarchar(100) null,
		Misc3 nvarchar(100) null
		, PreviousOuterValue int null
		, NextOuterValue int null
		, GrpID int default 0
		, InnerRow_Num int null
	)
 ON COMMIT DELETE ROWS";
                    CreateTable(sql, "GlobalTmpTripProcessor_AllRows", String.Empty);

                    sql = @"	
CREATE GLOBAL TEMPORARY TABLE GlobalTmpTripProcessor_AllRowsPrepared 
	(
		[RowNumber] [int] NULL,
		[UID] [int] PRIMARY KEY CLUSTERED,
		[AssetID] [int] NULL,
		[DateTimeGPS_UTC] [datetime] NULL,
		[DateTimeServer] [datetime] NULL,
		[Longitude] [float] NULL,
		[Latitude] [float] NULL,
		[LongLatValidFlag] [int] NULL,
		[Speed] [float] NULL,
		[EngineOn] [int] NULL,
		[StopFlag] [int] NULL,
		[TripDistance] [float] NULL,
		[TripTime] [float] NULL,
		[WorkHour] [int] NULL,
		[DriverID] [int] NOT NULL,
        Misc1 nvarchar(100) null,
		Misc2 nvarchar(100) null,
		Misc3 nvarchar(100) null
		, PreviousOuterValue int null
		, NextOuterValue int null
		, GrpID int default 0
		, InnerRow_Num int null
		, PreviousInnerValue int null
		, NextInnerValue int null
	)
 ON COMMIT DELETE ROWS";
                    CreateTable(sql, "GlobalTmpTripProcessor_AllRowsPrepared", String.Empty);

                    sql = @"	
CREATE GLOBAL TEMPORARY TABLE GlobalTmpTripProcessor_Speed 
	(
		[GrpID] [int] NULL,
		[dtFr] [datetime] NULL,
		[dtTo] [datetime] NULL,
		[Durations] [int] Default 0,
		[TID] [int] Default 0,
		[SumDuration] [int] Default 0,
		[Accel] [int] Default 0,
		[Brake] [int] Default 0,
		[Corner] [int] Default 0
	)
 ON COMMIT DELETE ROWS";
                    CreateTable(sql, "GlobalTmpTripProcessor_Speed", String.Empty);

                    sql = @"
CREATE GLOBAL TEMPORARY TABLE GlobalTmpTrips_GFI_GPS_TripHeader
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL,
	[iID] [int] PRIMARY KEY CLUSTERED,
	[GroupDate] [datetime] NULL,

	[AssetID] [int] NOT NULL,
    Asset nvarchar(120),
    TimeZone int,

	[DriverID] [int] NOT NULL,
    Driver nvarchar(100),

	[ExceptionFlag] [int] NULL,
	[MaxSpeed] [int] NULL,

	[IdlingTime] [float] NULL,
	[StopTime] [float] NULL,
	[TripDistance] [float] NULL,

    tsIdlingTime timestamp(0) null,
    tsStopTime timestamp(0) null,
    tsTripTime timestamp(0) null,

    [TripTime] [float] NULL,
	[GPSDataStartUID] [int] NULL,
	[GPSDataEndUID] [int] NULL,
	[dtStart] [datetime] NULL,
	[fStartLon] [float] NULL,
	[fStartLat] [float] NULL,
	[dtEnd] [datetime] NULL,
	[fEndLon] [float] NULL,
	[fEndLat] [float] NULL,

	[OverSpeed1Time] [float] NOT NULL DEFAULT 0,
	[OverSpeed2Time] [float] NOT NULL DEFAULT 0,
    [tsOverSpeed1Time] timestamp(0) null,
    [tsOverSpeed2Time] timestamp(0) null,

	[Accel] [int] NOT NULL DEFAULT 0,
	[Brake] [int] NOT NULL DEFAULT 0,
	[Corner] [int] NOT NULL DEFAULT 0,

    [DepartureAddress] nvarchar(150),
    [ArrivalAddress] nvarchar(150),

    [iDepartureZone] int null,
    [sDepartureZone] nvarchar(500) null,
    [iDepartureZoneColor] int null,
                            
    [iArrivalZone] int null,
    [sArrivalZone] nvarchar(500) null,
    [iArrivalZoneColor] int null,

    [iArrivalZones] nvarchar(2000) null,
    [sArrivalZones] nvarchar(2000) null,
    [sArrivalZonesColor] nvarchar(2000) null,

    [CalculatedOdometer] int null,
    [exceptionsCnt] int default 0,

	[dtUTCStart] [datetime] NULL,
	[dtUTCEnd] [datetime] NULL
)  ON COMMIT PRESERVE ROWS";
                    CreateTable(sql, "GlobalTmpTrips_GFI_GPS_TripHeader", "RowNumber");

                    sql = "CREATE NONCLUSTERED INDEX  myIdxGblTripsH ON GlobalTmpTrips_GFI_GPS_TripHeader ([AssetID], [iID],[GPSDataStartUID],[GPSDataEndUID])";
                    if (GetDataDT(ExistsIndex("GlobalTmpTrips_GFI_GPS_TripHeader", "myIdxGblTripsH")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"
CREATE GLOBAL TEMPORARY TABLE GlobalTmpTrips_GFI_GPS_GPSData 
(
	[RowNumber] [int] PRIMARY KEY CLUSTERED NOT NULL,
	[UID] [int],
	[AssetID] [int] NULL,
	[DateTimeGPS_UTC] [datetime] NULL,
	[DateTimeServer] [datetime] NULL,
	[Longitude] [float] NULL,
	[Latitude] [float] NULL,
	[LongLatValidFlag] [int] NULL,
	[Speed] [float] NULL,
	[EngineOn] [int] NULL,
	[StopFlag] [int] NULL,
	[TripDistance] [float] NULL,
	[TripTime] [float] NULL,
	[WorkHour] [int] NULL,
	[DriverID] [int] NOT NULL,
    [RoadSpeed] [int] NULL,
	Misc1 nvarchar(100) null,
	Misc2 nvarchar(100) null,
	Misc3 nvarchar(100) null
)  ON COMMIT PRESERVE ROWS";
                    CreateTable(sql, "GlobalTmpTrips_GFI_GPS_GPSData", "RowNumber");

                    sql = @"CREATE NONCLUSTERED INDEX myGblTripGPSIdx ON GlobalTmpTrips_GFI_GPS_GPSData 
(
	[AssetID] ASC,
	[DateTimeGPS_UTC] ASC,
	[LongLatValidFlag] ASC,
	[StopFlag] ASC
)";
                    if (GetDataDT(ExistsIndex("GlobalTmpTrips_GFI_GPS_GPSData", "myGblTripGPSIdx")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE NONCLUSTERED INDEX myIdxGblTripGPS ON GlobalTmpTrips_GFI_GPS_GPSData ([UID], [RowNumber],[AssetID],[DateTimeGPS_UTC],[DateTimeServer],[Longitude],[Latitude],[LongLatValidFlag],[Speed],[EngineOn],[StopFlag],[TripDistance],[TripTime],[WorkHour],[DriverID],[Misc1],[Misc2],[Misc3])";
                    if (GetDataDT(ExistsIndex("GlobalTmpTrips_GFI_GPS_GPSData", "myIdxGblTripGPS")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"
CREATE GLOBAL TEMPORARY TABLE GlobalTmpTrips_Assets 
(
	[RowNumber] [int] NOT NULL PRIMARY KEY,
	[iAssetID] [int],
    Asset nvarchar(120),
    TimeZone int,
	[iDriverID] [int]
) ON COMMIT PRESERVE ROWS";
                    CreateTable(sql, "GlobalTmpTrips_Assets", "RowNumber");

                    sql = @"
CREATE GLOBAL TEMPORARY TABLE GlobalTmpTrips_FltAssets 
(
     AssetID int null
	, AssetNumber nvarchar(120) null
    , AssetName nvarchar(120) null
	, AssetType int null
	, Status_b nvarchar(50) null
    , TimeZoneID nvarchar(50) null
    , Odometer int null
)  ON COMMIT PRESERVE ROWS";
                    CreateTable(sql, "GlobalTmpTrips_FltAssets", String.Empty);

                    sql = @"
CREATE GLOBAL TEMPORARY TABLE GlobalTmpTrips_ArrivalZoneTrips
(
	[RowNumber] [int] NOT NULL,
	[iID] [int] ,
	[GroupDate] [datetime] NULL,

	[AssetID] [int] NOT NULL,
    Asset nvarchar(120),
    TimeZone int,

	[DriverID] [int] NOT NULL,
    Driver nvarchar(100),

	[ExceptionFlag] [int] NULL,
	[MaxSpeed] [int] NULL,

	[IdlingTime] [float] NULL,
	[StopTime] [float] NULL,
	[TripDistance] [float] NULL,

    tsIdlingTime timestamp(0) null,
    tsStopTime timestamp(0) null,
    tsTripTime timestamp(0) null,

    [TripTime] [float] NULL,
	[GPSDataStartUID] [int] NULL,
	[GPSDataEndUID] [int] NULL,
	[dtStart] [datetime] NULL,
	[fStartLon] [float] NULL,
	[fStartLat] [float] NULL,
	[dtEnd] [datetime] NULL,
	[fEndLon] [float] NULL,
	[fEndLat] [float] NULL,

	[OverSpeed1Time] [float] NOT NULL DEFAULT 0,
	[OverSpeed2Time] [float] NOT NULL DEFAULT 0,
    [tsOverSpeed1Time] timestamp(0) null,
    [tsOverSpeed2Time] timestamp(0) null,

	[Accel] [int] NOT NULL DEFAULT 0,
	[Brake] [int] NOT NULL DEFAULT 0,
	[Corner] [int] NOT NULL DEFAULT 0,

    [DepartureAddress] nvarchar(150),
    [ArrivalAddress] nvarchar(150),

    [iDepartureZone] int null,
    [sDepartureZone] nvarchar(500) null,
    [iDepartureZoneColor] int null,
                            
    [iArrivalZone] int null,
    [sArrivalZone] nvarchar(500) null,
    [iArrivalZoneColor] int null,

    [iArrivalZones] nvarchar(2000) null,
    [sArrivalZones] nvarchar(2000) null,
    [sArrivalZonesColor] nvarchar(2000) null,

    [CalculatedOdometer] int null,
    [exceptionsCnt] int default 0,

	[dtUTCStart] [datetime] NULL,
	[dtUTCEnd] [datetime] NULL,

	[ZoneID] [int] NOT NULL,
	[Description] [nvarchar](255) NOT NULL,
	[Displayed] [int] NOT NULL,
	[Comments] [nvarchar](1024) NULL,
	[Color] [int] NOT NULL,
	[ExternalRef] [nvarchar](30) NULL,
	[Ref2] [nvarchar](50) NULL,
	[isMarker] [int] NOT NULL,
	[iBuffer] [int] NOT NULL,
	[GeomData] [SDO_GEOMETRY] NULL,
	[ZoneExpiry] [datetime] NULL,

    inZone int null

)  ON COMMIT PRESERVE ROWS";
                    CreateTable(sql, "GlobalTmpTrips_ArrivalZoneTrips", String.Empty);

                    sql = @"
CREATE GLOBAL TEMPORARY TABLE GlobalTmpTrips_DepartureZoneTrips
(
	[RowNumber] [int] NOT NULL,
	[iID] [int] ,
	[GroupDate] [datetime] NULL,

	[AssetID] [int] NOT NULL,
    Asset nvarchar(120),
    TimeZone int,

	[DriverID] [int] NOT NULL,
    Driver nvarchar(100),

	[ExceptionFlag] [int] NULL,
	[MaxSpeed] [int] NULL,

	[IdlingTime] [float] NULL,
	[StopTime] [float] NULL,
	[TripDistance] [float] NULL,

    tsIdlingTime timestamp(0) null,
    tsStopTime timestamp(0) null,
    tsTripTime timestamp(0) null,

    [TripTime] [float] NULL,
	[GPSDataStartUID] [int] NULL,
	[GPSDataEndUID] [int] NULL,
	[dtStart] [datetime] NULL,
	[fStartLon] [float] NULL,
	[fStartLat] [float] NULL,
	[dtEnd] [datetime] NULL,
	[fEndLon] [float] NULL,
	[fEndLat] [float] NULL,

	[OverSpeed1Time] [float] NOT NULL DEFAULT 0,
	[OverSpeed2Time] [float] NOT NULL DEFAULT 0,
    [tsOverSpeed1Time] timestamp(0) null,
    [tsOverSpeed2Time] timestamp(0) null,

	[Accel] [int] NOT NULL DEFAULT 0,
	[Brake] [int] NOT NULL DEFAULT 0,
	[Corner] [int] NOT NULL DEFAULT 0,

    [DepartureAddress] nvarchar(150),
    [ArrivalAddress] nvarchar(150),

    [iDepartureZone] int null,
    [sDepartureZone] nvarchar(500) null,
    [iDepartureZoneColor] int null,
                            
    [iArrivalZone] int null,
    [sArrivalZone] nvarchar(500) null,
    [iArrivalZoneColor] int null,

    [iArrivalZones] nvarchar(2000) null,
    [sArrivalZones] nvarchar(2000) null,
    [sArrivalZonesColor] nvarchar(2000) null,

    [CalculatedOdometer] int null,
    [exceptionsCnt] int default 0,

	[dtUTCStart] [datetime] NULL,
	[dtUTCEnd] [datetime] NULL,

	[ZoneID] [int] NOT NULL,
	[Description] [nvarchar](255) NOT NULL,
	[Displayed] [int] NOT NULL,
	[Comments] [nvarchar](1024) NULL,
	[Color] [int] NOT NULL,
	[ExternalRef] [nvarchar](30) NULL,
	[Ref2] [nvarchar](50) NULL,
	[isMarker] [int] NOT NULL,
	[iBuffer] [int] NOT NULL,
	[GeomData] [SDO_GEOMETRY] NULL,
	[ZoneExpiry] [datetime] NULL,

    inZone int null

)  ON COMMIT PRESERVE ROWS";
                    CreateTable(sql, "GlobalTmpTrips_DepartureZoneTrips", String.Empty);

                    sql = @"
CREATE GLOBAL TEMPORARY TABLE GlobalTmpTrips_FltDrivers 
 (
    DRIVERID NUMBER(10,0) NOT NULL ENABLE, 
	SDRIVERNAME NVARCHAR2(100) NOT NULL ENABLE, 
	IBUTTON NVARCHAR2(50), 
	SEMPLOYEENO NVARCHAR2(50), 
	SCOMMENTS NVARCHAR2(1024), 
	DATEOFBIRTH TIMESTAMP (3), 
	GENDER NVARCHAR2(50), 
	ADDRESS1 NVARCHAR2(100), 
	ADDRESS2 NVARCHAR2(100), 
	CITY NVARCHAR2(50), 
	NATIONALITY NVARCHAR2(50), 
	SFIRTNAME NVARCHAR2(50), 
	LICENSENO1 NVARCHAR2(50), 
	LICENSENO1EXPIRY TIMESTAMP (3), 
	LICENSENO2 NVARCHAR2(50), 
	LICENSENO2EXPIRY TIMESTAMP (3), 
	REF1 NVARCHAR2(50), 
	ZONEID NUMBER(10,0), 
	CREATEDDATE TIMESTAMP (3), 
	CREATEDBY NUMBER(*,0), 
	UPDATEDDATE TIMESTAMP (3), 
	UPDATEDBY NUMBER(13), 
	STATUS NVARCHAR2(50), 
	ADDRESS3 NVARCHAR2(50), 
	DISTRICT NVARCHAR2(25), 
	POSTALCODE NVARCHAR2(15), 
	MOBILENUMBER NVARCHAR2(15), 
	REASON NVARCHAR2(500), 
	DEPARTMENT NVARCHAR2(50), 
	EMAIL NVARCHAR2(50), 
	SLASTNAME NVARCHAR2(50), 
	OFFICERLEVEL NVARCHAR2(50), 
	OFFICERTITLE NVARCHAR2(50), 
	PHONE NVARCHAR2(50), 
	TITLE NVARCHAR2(50), 
	ASPUSER NVARCHAR2(50), 
	USERID NUMBER(13), 
	PHOTO BFILE, 
	TYPE_INSTITUTION NUMBER(13), 
	TYPE_DRIVER NUMBER(13), 
	TYPE_GRADE NUMBER(13), 
	HOMENO NUMBER(13), 
	EMPLOYEETYPE VARCHAR2(15)
)   ON COMMIT PRESERVE ROWS";
                    CreateTable(sql, "GlobalTmpTrips_FltDrivers", String.Empty);

                    sql = "ALTER TABLE GFI_FLT_Driver MODIFY EMPLOYEETYPE varchar2(15)";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"
CREATE GLOBAL TEMPORARY TABLE GlobalTmp_Exceptions
(
    assetID int null
    , assetName nvarchar(120) null
	, ruleID int null
    , ruleName nvarchar(120) null
	, dtFrom datetime null
    , groupID int null
    , duration int default 0
	, dateTimeToLocal datetime
	, gpsDataID int Default -1
    , lon float default 0.0
    , lat float default 0.0
    , sZones nvarchar(1200) null
    , driverID int default 1
    , sDriverName nvarchar(250) null
    , maxSpeed int default 0
    , roadSpeed int null
)   ON COMMIT PRESERVE ROWS";
                    CreateTable(sql, "GlobalTmp_Exceptions", String.Empty);

                    sql = @"
CREATE GLOBAL TEMPORARY TABLE GlobalTmp_ExceptionDetails
(
    uid int null
    , fLongitude float null
    , fLatitude float null
    , dateTimeGPS_UTC datetime null
	, dateTimeToLocal datetime null
    , speed int null, roadSpeed int null
	, ruleName nvarchar(120) null, parentRuleID int null
	, assetID int null, severity int null, groupID int null, ruleID int null
	, assetName nvarchar(120) null
)   ON COMMIT PRESERVE ROWS";
                    CreateTable(sql, "GlobalTmp_ExceptionDetails", String.Empty);

                    sql = @"
----------------------------FUNCTION DateADD--------------------------------------------
Create or replace  Function DateADD(vchDatePart  varchar , intOP   in   int, dt date)
/***********************************************************************************
--GEO--C--  GAjoseph
--GEO--E--  Select DateADD('dd' , -10, sysdate) from dual
--GEO--E--  Select DateADD('dd' , 30, to_date('31-FEB-2003')) from dual
--GEO--E--  Select dateadd('m', 2 , sysdate)
                , to_char(DateADD ( 'h' , 3 , to_date('01-JAN-2003 10:33:44' , 'dd-MON-YYYY HH:MI:SS') )  , 'dd-MON-YYYY HH:MI:SS' )
                , DateADD ( 'h' , 3 , to_date('01-JAN-2003 10:33:44' , 'dd-MON-YYYY HH:MI:SS') ) 
            From dual;
--GEO--C--  Returns a new datetime value based on adding an interval to the specified date.
--GEO--Ex--
            Is the parameter that specifies on which part of the date to 
return a new value. The table lists the dateparts and abbreviations 
recognized by Microsoft® SQL Server™.
            Datepart            Abbreviations
                Year                yy, yyyy
                Month               mm, m
                Day                 dd, d
                Hour                hh
                minute              mi, n
                second              ss, s

************************************************************************************/
return date
as
    dd      int;
    mm      int;
    yyyy    int;
    hh      int;
    NN      int;
    SS      int;
    v       date;
    lintOP  int;
Begin
    lintOP := intOP;
--GEO--C--  INcreament  Days
    if upper(vchDatePart) like 'D%'  then
        return dt + intOP;
    end if;
    dd  := to_number(to_Char(dt,'dd'));
    mm  := to_number(to_Char(dt,'MM'));
    yyyy:= to_number(to_Char(dt,'yyyy'));
    HH  := to_number(to_Char(dt, 'HH'));
    NN  := to_number(to_Char(dt, 'MI'));
    SS  := to_number(to_Char(dt, 'SS'));
--GEO--C--  INcreament  Year
    if upper(vchDatePart) like 'Y%' then
        yyyy:= yyyy+ lintOP;
    end if;
--GEO--C--  INcreament  Month.
    if upper(vchDatePart) like 'MM%' then
        yyyy:= yyyy+round(lintOP/12);
        mm  := mm+mod(lintOP,12);
    end if;-->MM
    if upper(vchDatePart) like 'H%' then
        dd  :=  dd  +   round(lintOP/24);
        hh  :=  hh  +   mod(lintOP,24);
    end if;--> hh
    if upper(vchDatePart) like 'MI%' then
        dd  :=  dd  +   round(lintOP/(24*60));
        hh  :=  hh  +   round(lintOP/60);
        NN  :=  NN  +   mod(lintOP , 60);
    end if;--> MInutes
    if upper(vchDatePart) like 'S%' then
        dd  :=  dd  +   round(lintOP/(24*60*60));
        hh  :=  hh  +   round(lintOP/60*60);
        NN  :=  NN  +   round(lintOP/60);
        NN  :=  NN  +   MOD(lintOP,60);
    end if;--> SS
        v   := LAST_DAY(to_date('01/'||to_char(mm,'09')||'/'|| to_char(yyyy, '0009'),'dd/mm/yyyy'));
        if dd > to_number(to_Char(v,'DD'))  then
            dd := to_number(to_Char(v,'DD'));
        end if;
    return to_date(lpad(dd,2,'0')||to_char(mm,'09')||'/'|| to_char(yyyy, '0009')||' '||lpad(hh,2,'0')||':'||lpad(NN,2,'0')||':'||lpad(SS,2,'0'), 'dd/mm/yyyy HH24:MI:SS') ;
    exception  when others then return null ;
End;

--AlreadyOracleStatement";
                    _SqlConn.OracleScriptExecute(sql, sConnStr);

                    sql = @"
CREATE GLOBAL TEMPORARY TABLE GlobalTmp_ZoneExceptions
(
    assetID int null
    , assetName nvarchar(120) null
	, ruleID int null
    , ruleName nvarchar(120) null
	, dtFrom datetime null
    , groupID int null
    , duration int default 0
	, dateTimeToLocal datetime
	, gpsDataID int Default -1
    , lon float default 0.0
    , lat float default 0.0
    , sZones nvarchar(1200) null
    , driverID int default 1
    , sDriverName nvarchar(250) null
    , maxSpeed int default 0
    , roadSpeed int null

	, ZoneID] [int] NOT NULL
	, Description] [nvarchar](255) NOT NULL
	, Displayed] [int] NOT NULL
	, Comments] [nvarchar](1024) NULL
	, Color] [int] NOT NULL
	, ExternalRef] [nvarchar](30) NULL
	, Ref2] [nvarchar](50) NULL
	, isMarker] [int] NOT NULL
	, iBuffer] [int] NOT NULL
	, GeomData] [SDO_GEOMETRY] NULL
	, ZoneExpiry] [datetime] NULL

    , inZone int null

)   ON COMMIT PRESERVE ROWS";
                    CreateTable(sql, "GlobalTmp_ZoneExceptions", String.Empty);

                    sql = @"
CREATE GLOBAL TEMPORARY TABLE GlobalTmpTripDetails_TripIDs
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL,
	[TripID] [int] PRIMARY KEY CLUSTERED
)  ON COMMIT PRESERVE ROWS
";
                    CreateTable(sql, "GlobalTmpTripDetails_TripIDs", "Row_Number");


                    sql = "update GFI_GPS_TripHeader set Accel = 0 where Accel is null";
                    _SqlConn.OracleScriptExecute(sql, sConnStr);
                    sql = "update GFI_GPS_TripHeader set Brake = 0 where Brake is null";
                    _SqlConn.OracleScriptExecute(sql, sConnStr);
                    sql = "update GFI_GPS_TripHeader set Corner = 0 where Corner is null";
                    _SqlConn.OracleScriptExecute(sql, sConnStr);

                    sql = @"
BEGIN
   execute immediate 'ALTER TABLE GFI_GPS_TripHeader modify Accel int Default 0 not null';
EXCEPTION
   WHEN others THEN null; -- handle the error
END;";
                    _SqlConn.OracleScriptExecute(sql, sConnStr);

                    sql = @"
BEGIN
   execute immediate 'ALTER TABLE GFI_GPS_TripHeader modify Brake int Default 0 not null';
EXCEPTION
   WHEN others THEN null; -- handle the error
END;";
                    _SqlConn.OracleScriptExecute(sql, sConnStr);

                    sql = @"
BEGIN
   execute immediate 'ALTER TABLE GFI_GPS_TripHeader modify Corner int Default 0 not null';
EXCEPTION
   WHEN others THEN null; -- handle the error
END;";
                    _SqlConn.OracleScriptExecute(sql, sConnStr);

                    sql = @"
CREATE GLOBAL TEMPORARY TABLE GlobalTmpTripDetails_td
(
    headerID int null
    ,  gpsDataUID int null
    ,  localTime datetime null
    ,  dateTimeServer datetime null
    ,  lat float null
    ,  lon float null
    ,  lonLatValidFlag int null
    , speed int null
    ,  roadSpeed int null
    ,  engineOn int null
    , ""UID"" int null
    , details nvarchar(500) null
    ,  ruleName nvarchar(500) null
    ,  ruleID int null
    , GPSDataStartUID int null
    , GPSDataEndUID int null
)  ON COMMIT PRESERVE ROWS
";
                    CreateTable(sql, "GlobalTmpTripDetails_td", String.Empty);

                    sql = "alter table GlobalTmpTrips_GFI_GPS_TripHeader modify tsStopTime nvarchar2(20);";
                    _SqlConn.OracleScriptExecute(sql, sConnStr);
                    sql = "alter table GlobalTmpTrips_GFI_GPS_TripHeader modify tsTripTime nvarchar2(20);";
                    _SqlConn.OracleScriptExecute(sql, sConnStr);
                    sql = "alter table GlobalTmpTrips_GFI_GPS_TripHeader modify tsIdlingTime nvarchar2(20);";
                    _SqlConn.OracleScriptExecute(sql, sConnStr);
                    sql = "alter table GlobalTmpTrips_GFI_GPS_TripHeader modify tsOverSpeed1Time nvarchar2(20);";
                    _SqlConn.OracleScriptExecute(sql, sConnStr);
                    sql = "alter table GlobalTmpTrips_GFI_GPS_TripHeader modify tsOverSpeed2Time nvarchar2(20);";
                    _SqlConn.OracleScriptExecute(sql, sConnStr);
                    sql = "alter table GlobalTmpTrips_GFI_GPS_TripHeader modify GroupDate nvarchar2(20);";
                    _SqlConn.OracleScriptExecute(sql, sConnStr);
                    sql = "alter table GlobalTmpTrips_ArrivalZoneTrips modify GroupDate nvarchar2(20);";
                    _SqlConn.OracleScriptExecute(sql, sConnStr);
                    sql = "alter table GlobalTmpTrips_DepartureZoneTrips modify GroupDate nvarchar2(20);";
                    _SqlConn.OracleScriptExecute(sql, sConnStr);

                    sql = @"
CREATE GLOBAL TEMPORARY TABLE GlobalTmpTrips_DailySummary
(
	GroupDate  nvarchar2(20)
	, registrationNo nvarchar2(120)
	, driver nvarchar2(1200)
	, trips int
	, startTime TIMESTAMP(3)
	, endTime TIMESTAMP(3)
	, kmTravelled int
	, idlingTime nvarchar2(20)
	, drivingTime nvarchar2(20)
	, inZoneStopTime nvarchar2(20)
	, outZoneStopTime nvarchar2(20)
) ON COMMIT PRESERVE ROWS";
                    CreateTable(sql, "GlobalTmpTrips_DailySummary", String.Empty);

                    sql = @"create global temporary table GlobalTmpAddressTrips
                            (
                                iID int NOT NULL,
                                DepartureAddress nvarchar2(250) NULL,
                                ArrivalAddress nvarchar2(250) NULL
                            ) on commit delete rows";
                    CreateTable(sql, "GlobalTmpAddressTrips", String.Empty);

                    sql = @"create global temporary table GlobalTmpAddressTripWizNoAddress
                            (
                                iID int NOT NULL,
                                DepartureAddress nvarchar2(250) NULL,
                                ArrivalAddress nvarchar2(250) NULL
                            ) on commit delete rows";
                    CreateTable(sql, "GlobalTmpAddressTripWizNoAddress", String.Empty);

                    sql = "create global temporary table  GlobalTmpExceptionsProcessor_TempRle(ParentRuleID int null) on commit delete rows";
                    CreateTable(sql, "GlobalTmpExceptionsProcessor_TempRle", String.Empty);

                    sql = "create global temporary table  GlobalTmpExceptionsProcessor_RulesCursor(ParentRuleID int null) on commit delete rows";
                    CreateTable(sql, "GlobalTmpExceptionsProcessor_RulesCursor", String.Empty);

                    sql = "create global temporary table GlobalTmpExceptionsProcessor_AssetFrmGMID (GMID int, GMDescription nvarchar(1000), ParentGMID int, StrucID int)  on commit delete rows";
                    CreateTable(sql, "GlobalTmpExceptionsProcessor_AssetFrmGMID", String.Empty);

                    sql = @"create global temporary table GlobalTmpExceptionsProcessor_ExTemp 
	                    (
		                    [RowNum] [int],
		                    [UID] [int] ,
		                    [ParentRuleID] [int] NULL,
		                    [RuleID] [int] NULL,
		                    [RuleType] int null,
		                    [StrucID] nvarchar(50),
		                    [Others] nvarchar(100)
	                    )  on commit delete rows";
                    CreateTable(sql, "GlobalTmpExceptionsProcessor_ExTemp", String.Empty);

                    sql = @"create global temporary table GlobalTmpExceptionsProcessor_ResultTable
	                        (
		                        ParentRuleIDTriggered [int],
		                        [RowNumber] [int] NOT NULL,
		                        [UID] [int], 
		                        [AssetID] [int] NULL,
		                        [DateTimeGPS_UTC] [datetime] NULL,
		                        [DateTimeServer] [datetime] NULL,
		                        [Longitude] [float] NULL,
		                        [Latitude] [float] NULL,
		                        [LongLatValidFlag] [int] NULL,
		                        [Speed] [float] NULL,
		                        [EngineOn] [int] NULL,
		                        [StopFlag] [int] NULL,
		                        [TripDistance] [float] NULL,
		                        [TripTime] [float] NULL,
		                        [WorkHour] [int] NULL,
		                        [DriverID] [int] NOT NULL,
		                        Misc1 nvarchar(100) null,
		                        Misc2 nvarchar(100) null,
		                        Misc3 nvarchar(100) null,
		                        RoadSpeed [float] NULL,
		                        Misc4 int null
    	                    )  on commit delete rows";
                    CreateTable(sql, "GlobalTmpExceptionsProcessor_ResultTable", String.Empty);

                    sql = @"create global temporary table GlobalTmpExceptionsProcessor_GFI_GPS_GPSData 
				            (
					            [Row_Number] [int] IDENTITY(1,1) NOT NULL,
					            [UID] [int] PRIMARY KEY CLUSTERED,
					            [AssetID] [int] NULL,
					            [DateTimeGPS_UTC] [datetime] NULL,
					            [DateTimeServer] [datetime] NULL,
					            [Longitude] [float] NULL,
					            [Latitude] [float] NULL,
					            [LongLatValidFlag] [int] NULL,
					            [Speed] [float] NULL,
					            [EngineOn] [int] NULL,
					            [StopFlag] [int] NULL,
					            [TripDistance] [float] NULL,
					            [TripTime] [float] NULL,
					            [WorkHour] [int] NULL,
					            [DriverID] [int] NOT NULL,
					            Misc1 nvarchar(100) null,
					            Misc2 int null,
					            Misc3 nvarchar(100) null,
					            RoadSpeed [float] NULL
    	                    )  on commit delete rows";

                    CreateTable(sql, "GlobalTmpExceptionsProcessor_GFI_GPS_GPSData", "Row_Number");

                    sql = @"create global temporary table GlobalTmpExceptionsProcessor_GFI_GPS_GPSDataDetail
				            (
					            [GPSDetailID] number(19) NOT NULL,
					            [UID] [int] NULL,
					            [TypeID] [nvarchar](30) NULL,
					            [TypeValue] [nvarchar](30) NULL,
					            [UOM] [nvarchar](15) NULL,
					            [Remarks] [nvarchar](50) NULL
    	                    )  on commit delete rows";
                    CreateTable(sql, "GlobalTmpExceptionsProcessor_GFI_GPS_GPSDataDetail", "UID");

                    sql = "CREATE index GlobalTmpExceptionsProcessor_ResultTable_ix1 on GlobalTmpExceptionsProcessor_ResultTable (Misc1, Misc2)";
                    if (GetDataDT(ExistsIndex("GlobalTmpExceptionsProcessor_ResultTable", "GlobalTmpExceptionsProcessor_ResultTable_ix1")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE INDEX GlobalmyIndex ON GlobalTmpExceptionsProcessor_GFI_GPS_GPSData 
				            (
					            [AssetID] ASC,
					            [DateTimeGPS_UTC] ASC,
					            [LongLatValidFlag] ASC,
					            [StopFlag] ASC,
					            [Speed] ASC
				            )";
                    if (GetDataDT(ExistsIndex("GlobalTmpExceptionsProcessor_GFI_GPS_GPSData", "GlobalmyIndex")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE INDEX Globalxxx ON GlobalTmpExceptionsProcessor_GFI_GPS_GPSData ([LongLatValidFlag], [DateTimeGPS_UTC], [DriverID], [RowNumber], [UID])";
                    if (GetDataDT(ExistsIndex("GlobalTmpExceptionsProcessor_GFI_GPS_GPSData", "Globalxxx")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE NONCLUSTERED INDEX Globalyyy ON GlobalTmpExceptionsProcessor_GFI_GPS_GPSData ([AssetID], [LongLatValidFlag], [DateTimeGPS_UTC], [RowNumber], [UID])";
                    if (GetDataDT(ExistsIndex("GlobalTmpExceptionsProcessor_GFI_GPS_GPSData", "Globalyyy")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    //not supported in oracle
                    //sql = @"ALTER TABLE GlobalTmpExceptionsProcessor_GFI_GPS_GPSDataDetail ADD CONSTRAINT FK_GlobalTmpExceptionsProcessor_GFI_GPS_GPSDataDetail FOREIGN KEY([UID])
					//            REFERENCES GlobalTmpExceptionsProcessor_GFI_GPS_GPSData ([UID])
					//        ON DELETE CASCADE";
                    //if (GetDataDT(ExistsSQLConstraint("FK_GlobalTmpExceptionsProcessor_GFI_GPS_GPSDataDetail", "GlobalTmpExceptionsProcessor_GFI_GPS_GPSDataDetail")).Rows.Count == 0)
                    //    _SqlConn.ScriptExecute(sql, myStrConn);

                    sql = "CREATE NONCLUSTERED INDEX GlobalMyIdxDTripProcessor ON GlobalTmpExceptionsProcessor_GFI_GPS_GPSDataDetail([TypeID], [UID], [TypeValue], [UOM])";
                    if (GetDataDT(ExistsIndex("GlobalTmpExceptionsProcessor_GFI_GPS_GPSDataDetail", "GlobalMyIdxDTripProcessor")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"create global temporary table GlobalTmpExceptionsProcessor_GFI_GPS_GPSDataWithDetails 
				            (
					            [Row_Number] [int] IDENTITY(1,1) NOT NULL,
					            [UID] [int], --PRIMARY KEY CLUSTERED,
					            [AssetID] [int] NULL,
					            [DateTimeGPS_UTC] [datetime] NULL,
					            [DateTimeServer] [datetime] NULL,
					            [Longitude] [float] NULL,
					            [Latitude] [float] NULL,
					            [LongLatValidFlag] [int] NULL,
					            [Speed] [float] NULL,
					            [EngineOn] [int] NULL,
					            [StopFlag] [int] NULL,
					            [TripDistance] [float] NULL,
					            [TripTime] [float] NULL,
					            [WorkHour] [int] NULL,
					            [DriverID] [int] NOT NULL,
					            TypeID nvarchar(100) null,
					            TypeValue nvarchar(100) null,
					            UOM nvarchar(100) null
    	                    )  on commit delete rows";
                    CreateTable(sql, "GlobalTmpExceptionsProcessor_GFI_GPS_GPSDataWithDetails", "Row_Number");
                    #endregion

                    //InsertDBUpdate(strUpd, "TypeVaule renamed");
                }
                #endregion

                #region Update 192. TripHistory Address and zone in db.
                strUpd = "Rez000192";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_FLT_ZoneHeader drop column ZoneTypeID";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ZoneHeader", "ZoneTypeID")).Rows[0][0].ToString() == "1")
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_GPS_TripHeader drop column sAddress";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "sAddress")).Rows[0][0].ToString() == "1")
                        dbExecute(sql);

                    sql = @"ALTER TABLE GFI_ARC_TripHeader drop column sAddress";
                    if (GetDataDT(ExistColumnSql("GFI_ARC_TripHeader", "sAddress")).Rows[0][0].ToString() == "1")
                        dbExecute(sql);

                    sql = @"ALTER TABLE dbo.GFI_GPS_TripHeader Drop column sDepartureAddress";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "sDepartureAddress")).Rows[0][0].ToString() == "1")
                        dbExecute(sql);

                    sql = @"ALTER TABLE dbo.GFI_GPS_TripHeader drop column sArrivalAddress";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "sArrivalAddress")).Rows[0][0].ToString() == "1")
                        dbExecute(sql);

                    sql = @"ALTER TABLE dbo.GFI_ARC_TripHeader drop column sDepartureAddress";
                    if (GetDataDT(ExistColumnSql("GFI_ARC_TripHeader", "sDepartureAddress")).Rows[0][0].ToString() == "1")
                        dbExecute(sql);

                    sql = @"ALTER TABLE dbo.GFI_ARC_TripHeader drop column sArrivalAddress";
                    if (GetDataDT(ExistColumnSql("GFI_ARC_TripHeader", "sArrivalAddress")).Rows[0][0].ToString() == "1")
                        dbExecute(sql);

                    sql = @"ALTER TABLE dbo.GFI_GPS_TripHeader ADD DepartureAddress nvarchar(250) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "DepartureAddress")).Rows[0][0].ToString() == "0")
                        dbExecute(sql);

                    sql = @"ALTER TABLE dbo.GFI_GPS_TripHeader ADD ArrivalAddress nvarchar(250) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_GPS_TripHeader", "ArrivalAddress")).Rows[0][0].ToString() == "0")
                        dbExecute(sql);

                    sql = @"ALTER TABLE dbo.GFI_ARC_TripHeader ADD DepartureAddress nvarchar(250) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_ARC_TripHeader", "DepartureAddress")).Rows[0][0].ToString() == "0")
                        dbExecute(sql);

                    sql = @"ALTER TABLE dbo.GFI_ARC_TripHeader ADD ArrivalAddress nvarchar(250) NULL";
                    if (GetDataDT(ExistColumnSql("GFI_ARC_TripHeader", "ArrivalAddress")).Rows[0][0].ToString() == "0")
                        dbExecute(sql);

                    sql = "CREATE NONCLUSTERED INDEX IX_GFI_GPS_TripDetail_Exceptions ON [dbo].[GFI_GPS_TripDetail] ([HeaderiID], [GPSDataUID])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_TripDetail", "IX_GFI_GPS_TripDetail_Exceptions")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "CREATE NONCLUSTERED INDEX IX_GFI_GPS_Exceptions_TripDetail ON [dbo].[GFI_GPS_Exceptions] ([GPSDataID],[AssetID]) INCLUDE ([RuleID], [GroupID])";
                    if (GetDataDT(ExistsIndex("GFI_GPS_Exceptions", "IX_GFI_GPS_Exceptions_TripDetail")).Rows[0][0].ToString() == "0")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = "DROP INDEX IX_Exceptions_Trips ON [dbo].[GFI_GPS_Exceptions]";
                    if (GetDataDT(ExistsIndex("GFI_GPS_Exceptions", "IX_Exceptions_Trips")).Rows[0][0].ToString() == "1")
                        dbExecuteWithoutTransaction(sql, 4000);

                    sql = @"CREATE TABLE [dbo].[GFI_GPS_TripWizNoAddress]
                            (
                                [iID] int NOT NULL IDENTITY(1,1)
                                , [HeaderID] int not NULL
                                , [fStartLon] [float] NULL
                                , [fStartLat] [float] NULL
                                , [fEndLon] [float] NULL
                                , [fEndLat] [float] NULL
                                , CONSTRAINT [PK_GFI_GPS_TripWizNoAddress] PRIMARY KEY CLUSTERED([iID] ASC)
                            )";
                    if (GetDataDT(ExistTableSql("GFI_GPS_TripWizNoAddress")).Rows[0][0].ToString() == "0")
                        CreateTable(sql, "GFI_GPS_TripWizNoAddress", "iID");

                    GlobalParam g = new GlobalParam();
                    g = globalParamsService.GetGlobalParamsByName("TripAddress", sConnStr);
                    if (g == null)
                    {
                        g = new GlobalParam();
                        g.ParamName = "TripAddress";
                        g.PValue = "1";
                        g.ParamComments = "TripAddress GPS TripHeader ID, updated by service";
                        globalParamsService.SaveGlobalParams(g, true, sConnStr);
                    }

                    sql = @"
CREATE OR REPLACE FUNCTION isPointInZone
(
  v_Lon IN FLOAT,
  v_Lat IN FLOAT,
  v_iZoneID IN NUMBER
)
RETURN NUMBER
AS
   v_iResult NUMBER(10,0);
   v_Longitude FLOAT(53);
   v_Latitude FLOAT(53);
   v_nLongitude FLOAT(53);
   v_nLatitude FLOAT(53);
   v_myLongitude FLOAT(53);
   v_myLatitude FLOAT(53);
   v_c NUMBER(1,0);
   CURSOR xyCursor
     IS SELECT Latitude, Longitude FROM GFI_FLT_ZoneDetail WHERE  ZoneID = v_iZoneID ORDER BY CordOrder;

BEGIN
   v_iResult := 0 ;
   v_myLongitude := v_Lon ;
   v_myLatitude := v_Lat ;
   v_c := 0 ;
   OPEN xyCursor;
   FETCH xyCursor INTO v_Latitude,v_Longitude;
   WHILE xyCursor%FOUND 
   LOOP 
      
      BEGIN
        FETCH xyCursor INTO v_Latitude,v_Longitude;
         IF ( ( ( ( v_Latitude <= v_myLatitude )
           AND ( v_myLatitude < v_nLatitude ) )
           OR ( ( v_nLatitude <= v_myLatitude )
           AND ( v_myLatitude < v_Latitude ) ) )
           AND ( v_myLongitude < (v_nLongitude - v_Longitude) * (v_myLatitude - v_Latitude) / (v_nLatitude - v_Latitude) + v_Longitude ) ) THEN
          IF ( v_c = 0 ) THEN
          v_c := 1 ;
         ELSE
            v_c := 0 ;
         END IF;
         END IF;
        FETCH xyCursor INTO v_nLatitude,v_nLongitude;
      
      END;
   END LOOP;
   CLOSE xyCursor;
   IF ( v_c = 0 ) THEN
    v_iResult := 0 ;
   ELSE
      v_iResult := 1 ;
   END IF;
   RETURN v_iResult;

EXCEPTION WHEN OTHERS THEN return 2; --utils.handleerror(SQLCODE,SQLERRM);
END;
--AlreadyOracleStatement";
                    _SqlConn.OracleScriptExecute(sql, sConnStr);

                    InsertDBUpdate(strUpd, "TripHistory Address and zone in db");
                }
                #endregion

                #region Update 196. GFI_FLT_RoadSpeedType.
                strUpd = "Rez000196";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE [dbo].[GFI_FLT_Asset] DROP CONSTRAINT FK_GFI_FLT_Asset_GFI_FLT_RoadSpeedType";
                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_Asset_GFI_FLT_RoadSpeedType", "GFI_FLT_RoadSpeedType")).Rows.Count > 0)
                    {
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                        sql = "update GFI_FLT_Asset set RoadSpeedType = null";
                        _SqlConn.OracleScriptExecute(sql, myStrConn);
                    }

                    sql = @"drop table GFI_FLT_RoadSpeedType";
                    if (GetDataDT(ExistTableSql("GFI_FLT_RoadSpeedType")).Rows[0][0].ToString() != "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE TABLE [dbo].[GFI_FLT_RoadSpeedType]
                            (
                                [iID] int NOT NULL IDENTITY(1,1)
                                , [VehicleType] nvarchar(100) NOT NULL
                                , [MotorWay] [int] NULL
                                , [RoadA] [int] NULL
                                , [RoadB] [int] NULL
                                , [RoadC] [int] NULL
                                , [RoadD] [int] NULL
                                , [RoadE] [int] NULL
                                , [RoadF] [int] NULL
                                , [RoadG] [int] NULL
                                , [RoadH] [int] NULL
                                , CONSTRAINT [PK_GFI_FLT_RoadSpeedType] PRIMARY KEY CLUSTERED([iID] ASC)
                            )";
                    if (GetDataDT(ExistTableSql("GFI_FLT_RoadSpeedType")).Rows[0][0].ToString() == "0")
                        CreateTable(sql, "GFI_FLT_RoadSpeedType", "iID");

                    sql = @"insert into GFI_FLT_RoadSpeedType (VehicleType, MotorWay, RoadA, RoadB, RoadC) 
                                	Select 'Motor car', 110, 90, 80, 60 from dual where not exists (select iID from GFI_FLT_RoadSpeedType where VehicleType = 'Motor car')";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"insert into GFI_FLT_RoadSpeedType (VehicleType, MotorWay, RoadA, RoadB, RoadC) 
                                	Select 'Bus with M.G.W less than 3.5 tons', 80, 80, 60, 40 from dual where not exists (select iID from GFI_FLT_RoadSpeedType where VehicleType = 'Bus with M.G.W less than 3.5 tons')";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"insert into GFI_FLT_RoadSpeedType (VehicleType, MotorWay, RoadA, RoadB, RoadC) 
                                	Select 'Bus with M.G.W more than 3.5 tons', 70, 60, 50, 40 from dual where not exists (select iID from GFI_FLT_RoadSpeedType where VehicleType = 'Bus with M.G.W more than 3.5 tons')";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"insert into GFI_FLT_RoadSpeedType (VehicleType, MotorWay, RoadA, RoadB, RoadC) 
                                	Select 'Goods vehicles or Articulated vehicles with M.G.W less than 3.5 tons', 80, 80, 60, 40 from dual where not exists (select iID from GFI_FLT_RoadSpeedType where VehicleType = 'Goods vehicles or Articulated vehicles with M.G.W less than 3.5 tons')";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"insert into GFI_FLT_RoadSpeedType (VehicleType, MotorWay, RoadA, RoadB, RoadC) 
                                	Select 'Goods vehicles or Articulated vehicles with M.G.W more than 3.5 tons', 70, 60, 50, 40 from dual where not exists (select iID from GFI_FLT_RoadSpeedType where VehicleType = 'Goods vehicles or Articulated vehicles with M.G.W more than 3.5 tons')";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"insert into GFI_FLT_RoadSpeedType (VehicleType, MotorWay, RoadA, RoadB, RoadC) 
                                	Select 'Motor vehicles drawing one trailer with M.G.W less than 3.5 tons', 55, 60, 50, 40 from dual where not exists (select iID from GFI_FLT_RoadSpeedType where VehicleType = 'Motor vehicles drawing one trailer with M.G.W less than 3.5 tons')";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"insert into GFI_FLT_RoadSpeedType (VehicleType, MotorWay, RoadA, RoadB, RoadC) 
                                	Select 'Motor vehicles drawing one trailer with M.G.W more than 3.5 tons', 55, 40, 40, 40 from dual where not exists (select iID from GFI_FLT_RoadSpeedType where VehicleType = 'Motor vehicles drawing one trailer with M.G.W more than 3.5 tons')";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"insert into GFI_FLT_RoadSpeedType (VehicleType, MotorWay, RoadA, RoadB, RoadC) 
                                	Select 'Motor vehicles drawing more than one trailer', 40, 10, 40, 40 from dual where not exists (select iID from GFI_FLT_RoadSpeedType where VehicleType = 'Motor vehicles drawing more than one trailer')";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE dbo.GFI_FLT_Asset ADD RoadSpeedType int NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_Asset", "RoadSpeedType")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    if (GetDataDT(ExistsSQLConstraint("FK_GFI_FLT_Asset_GFI_FLT_RoadSpeedType", "GFI_FLT_ASSET")).Rows.Count == 0)
                    {
                        sql = @"ALTER TABLE dbo.GFI_FLT_Asset 
                            ADD CONSTRAINT FK_GFI_FLT_Asset_GFI_FLT_RoadSpeedType 
                                FOREIGN KEY(RoadSpeedType) 
                                REFERENCES dbo.GFI_FLT_RoadSpeedType (iID) 
	                     ON DELETE Cascade ";
                        dbExecute(sql);
                    }

                    sql = "update GFI_FLT_Asset set RoadSpeedType = (select iID from GFI_FLT_RoadSpeedType where ROOWNUM = 1) where RoadSpeedType is null";
                    dbExecute(sql);

                    sql = "update GFI_GPS_Rules set RuleType = 9 where Struc = 'Fuel' and RuleType = 0";
                    dbExecute(sql);

                    //Struc = 'RoadSpeed', RuleType = 10
                    //Create an speed rule, then run the following script to simulate RoadSpeed rule
                    //update GFI_GPS_Rules set RuleType = 10, Struc = 'RoadSpeed', StrucValue = 0, StrucCondition = '', StrucType = '' where Struc = 'Speed' and ParentRuleID = 1

                    InsertDBUpdate(strUpd, "GFI_FLT_RoadSpeedType");
                }
                #endregion
                #region Update 197. Zone Spatial.
                strUpd = "Rez000197";
                if (!CheckUpdateExist(strUpd))
                {
                    #region Zone
                    sql = @"ALTER TABLE GFI_FLT_ZoneHeader add GeomData SDO_GEOMETRY";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ZoneHeader", "GeomData")).Rows[0][0].ToString() == "0")
                        dbExecute(sql);

                    sql = @"ALTER TABLE [dbo].[GFI_FLT_ZoneHeader] add [ZoneExpiry] DateTime NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ZoneHeader", "ZoneExpiry")).Rows[0][0].ToString() == "0")
                        dbExecute(sql);

                    #endregion

                    #region VCA
                    sql = @"ALTER TABLE GFI_FLT_VCAHeader add GeomData SDO_GEOMETRY";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_VCAHeader", "GeomData")).Rows[0][0].ToString() == "0")
                        dbExecute(sql);
                    #endregion
                    InsertDBUpdate(strUpd, "Zone Spatial");
                }
                #endregion

                #region Update 200. Planning and Zones. WIP
                strUpd = "Rez000200";
                if (!CheckUpdateExist(strUpd))
                {
                    sql = "ALTER TABLE GFI_FLT_ZoneType MODIFY sDescription nvarchar2(50)";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"ALTER TABLE [dbo].[GFI_PLN_PlanningResource] add [iSource] INT NULL";
                    if (GetDataDT(ExistColumnSql("GFI_PLN_PlanningResource", "iSource")).Rows[0][0].ToString() == "0")
                        dbExecute(sql);

                    sql = @"ALTER TABLE [dbo].[GFI_FLT_ZoneHeader] add [ZoneExpiry] DateTime NULL";
                    if (GetDataDT(ExistColumnSql("GFI_FLT_ZoneHeader", "ZoneExpiry")).Rows[0][0].ToString() == "0")
                        dbExecute(sql);

                    sql = @"--DoNotReplace[]
create or replace function Geometry2Json (p_geo SDO_GEOMETRY)
return nvarchar2
as 
    Begin
        return (
                     '{' ||
                         (CASE p_geo.Get_GType()
                             WHEN 1 THEN '""oprType"": 0,""type"": ""Point"",""coordinates"":' || REPLACE(REPLACE(REPLACE(REPLACE(p_geo.Get_WKT(),'POINT ',''),'(',	'['),')',']'),' ',',')
                             WHEN 3 THEN '""oprType"": 0,""type"": ""Polygon"",""coordinates"":' || '[' || REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(p_geo.Get_WKT(), 'POLYGON ', ''), '(', '['), ')', ']'), '], ', ']],	['), ', ', '],['), ' ', ',') || ']'
                             WHEN 7 THEN '""oprType"": 0,""type"": ""MultiPolygon"",""coordinates"":' || '[' || REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(p_geo.Get_WKT(), 'MULTIPOLYGON ', ''), '(', '['), ')', ']'), '], ', ']],['), ', ', '],['), ' ', ',') || ']'
                             WHEN 5 THEN '""oprType"": 0,""type"": ""MultiPoint"",""coordinates"":' || '[' || REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(p_geo.Get_WKT(), 'MULTIPOINT ', ''), '(', '['), ')', ']'), '], ', ']],['), ', ', '],['), ' ', ',') || ']'
                             WHEN 2 THEN '""oprType"": 0,""type"": ""LineString"",""coordinates"":' || '[' || REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(p_geo.Get_WKT(), 'LINESTRING ', ''), '(', '['), ')', ']'), '], ', ']],['), ', ', '],['), ' ', ',') || ']'
                             ELSE NULL
                         END)
                    || '}'
                );
                    end; ";
                    _SqlConn.OracleScriptExecute(sql, sConnStr);

                    sql = "select z.GeomData.Get_WKT(), z.GeomData.Get_GType(), Geometry2Json(z.GeomData) from GFI_FLT_ZoneHeader z";

                    //InsertDBUpdate(strUpd, "Planning and Zones");
                }
                #endregion

                usrInst = null;
            }
            catch (Exception ex)
            {
                throw new Exception(strUpd + "  " + sql + " ] " + ex.ToString());
            }
            finally
            {
                Globals.uLogin = null;
            }
        }

        private DataSet GetTablesFromColumn(String pStrColumn)
        {
            return (new Connection(myStrConn).GetDS("select object_name(id) as Table_Name from syscolumns where name  = '" + pStrColumn + "' and OBJECTPROPERTY(id, N'IsUserTable') = 1", myStrConn));
        }
        private String ExistTableSql(String pStrTable)
        {
            string sql = "SELECT COUNT(*) cnt FROM DUAL WHERE EXISTS(SELECT * FROM ALL_OBJECTS WHERE OBJECT_TYPE = 'TABLE' AND OBJECT_NAME = '" + pStrTable.ToUpper() + "' and OWNER = '" + strDB.ToUpper() + "')";
            return sql;

        }
        private String ExistViewSql(String pStrView)
        {
            return ("SELECT COUNT(*) cnt FROM DUAL WHERE EXISTS (SELECT * FROM ALL_OBJECTS WHERE OBJECT_TYPE = 'VIEW' AND OBJECT_NAME = '" + pStrView.ToUpper() + "' and OWNER = '" + strDB.ToUpper() + "')");
        }
        private String ExistColumnSql(String pStrTable, String pStrColumn)
        {
            string sql = "SELECT COUNT(*) cnt FROM ALL_TAB_COLUMNS WHERE TABLE_NAME = '" + pStrTable.ToUpper() + "' AND COLUMN_NAME = '" + pStrColumn.ToUpper() + "' and OWNER = '" + strDB.ToUpper() + "'";
            return sql;
        }
        private String ExistSequenceSql(String pStrSequence)
        {
            String sql = "SELECT COUNT(*) cnt FROM all_sequences WHERE sequence_name = '" + pStrSequence.ToUpper() + "' and SEQUENCE_OWNER = '" + strDB.ToUpper() + "'";
            return sql;
        }

        private String ExistsSQLConstraint(String strConstraintName, String pStrTable)
        {
            /*
			THE CONSTRAINT_TYPE will tell you what type of contraint it is
			R - Referential key(foreign key)
			U - Unique key
			P - Primary key
			C - Check constraint
			*/
            String sql = "SELECT * FROM USER_CONSTRAINTS WHERE CONSTRAINT_NAME = '" + strConstraintName.ToUpper() + "' AND TABLE_NAME = '" + pStrTable.ToUpper() + "' and OWNER = '" + strDB.ToUpper() + "'";
            return sql;
        }

        private String GetPrimaryKey(String pk, String sTable)
        {
            String sql = "SELECT* FROM all_constraints WHERE TABLE_NAME = UPPER('" + sTable + "')";
            return sql;
        }
        private Boolean bExistTrigger(String Trigger)
        {
            Boolean bOk = false;
            String sql = "SELECT * FROM user_triggers WHERE trigger_name = '" + Trigger.ToUpper() + "'";
            if (GetDataDT(sql).Rows.Count > 0)
                bOk = true;
            return bOk;
        }
        private String ExistsIndex(String pStrTable, String pStrIndex)
        {
            String sql = "SELECT COUNT(*) cnt FROM user_indexes WHERE index_name = '" + pStrIndex.ToUpper() + "' AND TABLE_NAME = '" + pStrTable.ToUpper() + "' and TABLE_OWNER = '" + strDB.ToUpper() + "'";
            return sql;
        }
        private String ExistsFunction(String sFunction)
        {
            return @"SELECT * 
FROM INFORMATION_SCHEMA.ROUTINES
WHERE
	ROUTINE_TYPE = 'FUNCTION'
	AND ROUTINE_NAME = '" + sFunction + @"'
   AND ROUTINE_SCHEMA = '" + strDB + "'";
        }
        private String ExistsSP(String sSP)
        {
            return @"SELECT * 
FROM INFORMATION_SCHEMA.ROUTINES
WHERE
	ROUTINE_TYPE = 'PROCEDURE'
	AND ROUTINE_NAME = '" + sSP + @"'
   AND ROUTINE_SCHEMA = '" + strDB + "'";
        }
        private String GetFieldSchema(String pStrTable, String pStrColumn)
        {
            String sql = "SELECT * FROM ALL_TAB_COLUMNS WHERE TABLE_NAME = '" + pStrTable.ToUpper() + "' AND COLUMN_NAME = '" + pStrColumn.ToUpper() + "' and OWNER = '" + strDB.ToUpper() + "'";
            return sql;
        }
        private String RenameColumn(String sOld, String sNew, String sTable)
        {
            String sql = "alter table " + sTable.ToUpper() + " rename column " + sOld.ToUpper() + " TO " + sNew.ToUpper() + "";
            return sql;
        }
        private Boolean CheckUpdateExist(String UpdateId)
        {
            Boolean bOk = false;
            if (new Connection(myStrConn).GetNumberOfRecords("GFI_SYS_INIT", "*", " upd_id = '" + UpdateId + "'", myStrConn) > 0)
                bOk = true;
            return bOk;
        }
        private Boolean CheckParamExist(String pCode, String sConnStr)
        {
            Boolean bOk = false;
            if (new Connection(myStrConn).GetNumberOfRecords("db_param", "*", " code = '" + pCode + "'", myStrConn) > 0)
                bOk = true;
            return bOk;
        }
        private Boolean hasIdentity(String sTableName)
        {
            String sql = "SELECT * FROM sys.identity_columns WHERE OBJECT_NAME(object_id) = '" + sTableName + "'";

            DataTable dt = new Connection(myStrConn).GetDataDT(sql, myStrConn);
            if (dt.Rows.Count > 0)
                return true;
            return false;
        }

        private DataTable dtGetFK(String sTableName)
        {
            String sql = @"
SELECT
  object_name(parent_object_id) tbName,
  object_name(referenced_object_id) fkTbName,
  name fkName
FROM sys.foreign_keys
WHERE parent_object_id = object_id('" + sTableName + "')";

            DataTable dt = new Connection(myStrConn).GetDataDT(sql, myStrConn);
            return dt;
        }
        private void InsertDBUpdate(String pUpd_Id, String pUpd_Desc)
        {
            new Connection(myStrConn).dbExecute("Insert into GFI_SYS_INIT values( '" + pUpd_Id + "', '" + pUpd_Desc + "', systimestamp)", myStrConn);
        }
        private String SQLCompaibilityLvl2014()
        {
            String sql = @"
USE master
GO
ALTER DATABASE CoreDataDupl
SET COMPATIBILITY_LEVEL = 120;
GO";
            return sql;
        }
        private String GetAllKeys(String pStrTable)
        {
            return "EXEC sp_fkeys '" + pStrTable + "'";
        }
        private void WebConfig()
        {
            //<!-- To put in webconfig to allows creation on asp user --!>
            //    <contexts>
            //      <context type="NaveoOneWeb.Models.ApplicationDbContext, NaveoOneWeb">
            //        <databaseInitializer type="System.Data.Entity.MigrateDatabaseToLatestVersion`2[[NaveoOneWeb.Models.ApplicationDbContext, NaveoOneWeb], NaveoOneWeb.Migrations.Configuration, NaveoOneWeb]], EntityFramework, PublicKeyToken=b77a5c561934e089">
            //          <parameters>
            //            <parameter value="DefaultConnection_DatabasePublish" />
            //          </parameters>
            //        </databaseInitializer>
            //      </context>
            //    </contexts>
        }

        void CreateTable(String sql, String Tablename, String identity)
        {
            String oracleSQL = String.Empty;
            if (sql.Trim().ToUpper().StartsWith("CREATE TABLE") || sql.Trim().ToUpper().StartsWith("CREATE GLOBAL TEMPORARY TABLE"))
            {
                sql = Regex.Replace(sql, @"\bint\b", "number", RegexOptions.IgnoreCase);
                sql = Regex.Replace(sql, @"\bbigint\b", "number(19)", RegexOptions.IgnoreCase);

                oracleSQL = "begin execute immediate'";
                oracleSQL += sql;
                oracleSQL = oracleSQL.Replace("IDENTITY(1,1)", String.Empty);
                oracleSQL += "'; end;";
                oracleSQL = oracleSQL.Replace("AUTO_INCREMENT", String.Empty);
                oracleSQL = Regex.Replace(oracleSQL, @"\bCLUSTERED\b", String.Empty, RegexOptions.IgnoreCase);
                oracleSQL = Regex.Replace(oracleSQL, @"\bdbo.\b", String.Empty, RegexOptions.IgnoreCase);
                oracleSQL = Regex.Replace(oracleSQL, @"\bASC\b", String.Empty, RegexOptions.IgnoreCase);
                oracleSQL = Regex.Replace(oracleSQL, @"\bNOT NULL Default\b", "Default", RegexOptions.IgnoreCase);
                oracleSQL = Regex.Replace(oracleSQL, @"\bvarchar\b", "nvarchar", RegexOptions.IgnoreCase);
                oracleSQL = Regex.Replace(oracleSQL, @"\bnvarchar2\b", "nvarchar", RegexOptions.IgnoreCase);
                oracleSQL = Regex.Replace(oracleSQL, @"\bnvarchar\b", "nvarchar2", RegexOptions.IgnoreCase);
                oracleSQL = Regex.Replace(oracleSQL, @"\bDatetime\b", "timestamp(3)", RegexOptions.IgnoreCase);

                if (GetDataDT(ExistTableSql(Tablename)).Rows[0][0].ToString() == "0")
                    _SqlConn.OracleScriptExecute(oracleSQL, myStrConn);

                if (!String.IsNullOrEmpty(identity))
                {
                    //Generate ID using sequence and trigger
                    sql = @"CREATE SEQUENCE " + Tablename + "_seq START WITH 1 INCREMENT BY 1";
                    if (GetDataDT(ExistSequenceSql(Tablename + "_seq")).Rows[0][0].ToString() == "0")
                        _SqlConn.OracleScriptExecute(sql, myStrConn);

                    sql = @"CREATE OR REPLACE TRIGGER " + Tablename + "_SEQ_TR BEFORE INSERT ON " + Tablename.ToUpper() + " FOR EACH ROW WHEN (NEW." + identity + " IS NULL) BEGIN SELECT " + Tablename.ToUpper() + "_SEQ.NEXTVAL INTO :NEW." + identity + " FROM DUAL; END;";
                    _SqlConn.OracleScriptExecute(sql, myStrConn);
                }
            }
        }
    }
}