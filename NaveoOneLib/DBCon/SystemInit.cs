﻿using NaveoOneLib.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using NaveoOneLib.Models;
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.Drivers;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Users;
using NaveoOneLib.Models.Trips;
using NaveoOneLib.Models.Shifts;
using NaveoOneLib.Models.Zones;
using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.Lookups;
using NaveoOneLib.Services;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Services.Rules;
using static NaveoOneLib.Common.Globals;

namespace NaveoOneLib.DBCon
{
    public class SystemInit
    {
        public void UpdateDB(String sConnStr)
        {
            String providerName = ConfigurationManager.ConnectionStrings[sConnStr].ProviderName;
            if (providerName.Contains("PostgreSQL"))
                dbProvider = Globals.DatabaseProvider.PostgreSQL;
            switch (dbProvider)
            {
                case DatabaseProvider.MicrosoftSQLServer:
                    new SQLSystemInit().UpdateDB(sConnStr);
                    break;
                case DatabaseProvider.Oracle:
                    new OracleSystemInit().UpdateDB(sConnStr);
                    break;
                case DatabaseProvider.PostgreSQL:
                    new PostGreSQLSystemInit().UpdateDB(sConnStr);
                    break;
                default:
                    break;
            }
        }
    }
}
