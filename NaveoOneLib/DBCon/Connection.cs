﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

using System.Data.SqlClient;
//using MySql.Data.MySqlClient;
//using System.Data.OleDb;

using System.Windows.Forms;
using System.Data;
using System.Data.Common;

using NaveoOneLib.Common;
using NaveoOneLib.Models;
using DBConn.DbCon;
using DBConn.Common;
using static NaveoOneLib.Common.Globals;
using Npgsql;

namespace NaveoOneLib.DBCon
{
    public class Connection
    {
        #region Variables
        public static String UsingWebService;
        public static String WebURL = ConfigurationManager.AppSettings["WebUrl"].ToString();

        Errors er = new Errors();
        public static String strLog = String.Empty;

        NaveoEng.EngService ws;
        #endregion

        public Connection(String sConnStr)
        {
            String providerName = ConfigurationManager.ConnectionStrings[sConnStr].ProviderName;
            if (providerName.Contains("Oracle"))
                dbProvider = Globals.DatabaseProvider.Oracle;
            else if (providerName.Contains("PostgreSQL"))
                dbProvider = Globals.DatabaseProvider.PostgreSQL;
            else
                dbProvider = Globals.DatabaseProvider.MicrosoftSQLServer;
        }

        private void WebServiceConn()
        {
            ws = new NaveoEng.EngService();
            ws.Url = WebURL;

            System.Threading.Thread testThread = new System.Threading.Thread(new System.Threading.ThreadStart(ChkWebConn));
        }
        void ChkWebConn()
        {
            if (!DataTransport.IsAddressAvailable(ws.Url))
                new Errors().ShowMsg("Fail to connect to server. Please check your internet connection");
        }
        public Boolean ChkWebServiceConn()
        {
            if (UsingWebService == "Y")
            {
                if (!DataTransport.IsAddressAvailable(WebURL))
                    return false;
            }

            return true;
        }

        public DataSet GetDataSet(String sql, String sConnStr)
        {
            switch (dbProvider)
            {
                case DatabaseProvider.MicrosoftSQLServer:
                    return new SQLConn().GetDataSet(sql, sConnStr);
                case DatabaseProvider.Oracle:
                    return new OracleConn().GetDataSet(sql, sConnStr);
                case DatabaseProvider.PostgreSQL:
                    return new PostGreSQLConn().GetDataSet(sql, sConnStr);
                default:
                    return null;
            }
        }

        #region WS specifics
        public DataSet GetLatestGPSData(List<int> lAssetID, String strDB)
        {
            if (UsingWebService == "Y")
            {
                WebServiceConn();

                DataTable dt = new DataTable();
                dt.Columns.Add("MyInt", typeof(int));
                foreach (int i in lAssetID)
                    dt.Rows.Add(i);
                Byte[] b = DataTransport.CompressDT(dt);
                return DataTransport.DeCompressByteToDataSet(ws.GetLatestGPSData(b, strDB));
            }

            switch (dbProvider)
            {
                case DatabaseProvider.MicrosoftSQLServer:
                    return new SQLConn().GetLatestGPSData(lAssetID, strDB);
                case DatabaseProvider.Oracle:
                    return new OracleConn().GetLatestGPSData(lAssetID, strDB);
                case DatabaseProvider.PostgreSQL:
                    return new PostGreSQLConn().GetLatestGPSData(lAssetID, strDB);
                default:
                    return null;
            }
        }
        public DataTable GetExceptions(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            if (UsingWebService == "Y")
            {
                WebServiceConn();
                //ws = new NaveoEng.EngService();
                DataTable dt = new DataTable();
                dt.Columns.Add("MyInt", typeof(int));
                foreach (int i in lAssetID)
                    dt.Rows.Add(i);
                Byte[] b = DataTransport.CompressDT(dt);
                return DataTransport.DeCompressByteToDT(ws.GetExceptions(b, dtFrom, dtTo, sConnStr));
            }

            switch (dbProvider)
            {
                case DatabaseProvider.MicrosoftSQLServer:
                    return new SQLConn().GetExceptions(lAssetID, dtFrom, dtTo, sConnStr);
                case DatabaseProvider.Oracle:
                    return new OracleConn().GetExceptions(lAssetID, dtFrom, dtTo, sConnStr);
                case DatabaseProvider.PostgreSQL:
                    return new PostGreSQLConn().GetExceptions(lAssetID, dtFrom, dtTo, sConnStr);
                default:
                    return null;
            }
        }
        #endregion
        public DataTable dtSql()
        {
            switch (dbProvider)
            {
                case DatabaseProvider.MicrosoftSQLServer:
                    return new SQLConn().dtSql();
                case DatabaseProvider.Oracle:
                    return new SQLConn().dtSql();
                case DatabaseProvider.PostgreSQL:
                    return new PostGreSQLConn().dtSql();
                default:
                    return null;
            }
        }
        public DataSet GetDataDS(DataTable dtSQL, String sConnStr, int iTimeOut)
        {
            return GetDataDS(dtSQL, sConnStr, null, iTimeOut);
        }
        public DataSet GetDataDS(DataTable dtSQL, String sConnStr)
        {
            return GetDataDS(dtSQL, sConnStr, null, 90);
        }
        DataSet GetDataDS(DataTable dtSQL, String sConnStr, DbTransaction tran, int iTimeOut)
        {
            //UsingWebService = "Y";
            if (UsingWebService == "Y")
            {
                if (Globals.bDebugMode)
                {
                    DataRow drSql = dtSQL.NewRow();
                    drSql["sSQL"] = "select getdate()";
                    drSql["sTableName"] = "dtSQLF";
                    dtSQL.Rows.InsertAt(drSql, 0);

                    drSql = dtSQL.NewRow();
                    drSql["sSQL"] = "select getdate()";
                    drSql["sTableName"] = "dtSQLT";
                    dtSQL.Rows.Add(drSql);
                }

                WebServiceConn();

                byte[] x = new byte[1];
                DateTime dtTime = DateTime.Now;
                Byte[] b = DataTransport.CompressDT(dtSQL);
                try
                {
                    x = ws.GetDataDS(b, sConnStr);
                }
                catch
                {
                    //String s = ws.ShowCache();
                    //DataTable dt = ws.dt(b, strDB);
                    //DataSet ds = ws.ds(b, strDB);
                }
                try
                {
                    String g = DataTransport.GetString(x);
                }
                catch { }
                DataSet dsResult = DataTransport.DeCompressByteToDataSet(x);

                if (Globals.bDebugMode)
                {
                    String s = "GetDataDS \r\n";
                    s += "Connection Started at Local Time " + dtTime.ToString();
                    s += "\r\n SQL query started at Server time " + dsResult.Tables["dtSQLF"].Rows[0][0];
                    s += "\r\n SQL query ended at Server Time " + dsResult.Tables["dtSQLT"].Rows[0][0];
                    s += "\r\n Unprocessed data received at Local Time " + DateTime.Now;
                    MessageBox.Show(s);
                }
                return dsResult;
            }

            String providerName = ConfigurationManager.ConnectionStrings[sConnStr].ProviderName;
            if (providerName.Contains("Oracle"))
                dbProvider = Globals.DatabaseProvider.Oracle;
            else if (providerName.Contains("PostgreSQL"))
                dbProvider = Globals.DatabaseProvider.PostgreSQL;
            else
                dbProvider = Globals.DatabaseProvider.MicrosoftSQLServer;

            switch (dbProvider)
            {
                case DatabaseProvider.MicrosoftSQLServer:
                    return new SQLConn().GetDataDS(dtSQL, sConnStr, tran, iTimeOut);
                case DatabaseProvider.Oracle:
                    return new OracleConn().GetDataDS(dtSQL, sConnStr, tran, iTimeOut);
                case DatabaseProvider.PostgreSQL:
                    return new PostGreSQLConn().GetDataDS(dtSQL, sConnStr, tran, iTimeOut);
                default:
                    return null;
            }
        }

        public void clearAllConnection(String sConnStr)
        {
            switch (dbProvider)
            {
                case DatabaseProvider.PostgreSQL:
                    new PostGreSQLConn().clearAllConnections(sConnStr);
                    break;
                default:
                    break;
            }
        }

        public DataTable GetDataDT(String mySQL, String sConnStr)
        {
            switch (dbProvider)
            {
                case DatabaseProvider.MicrosoftSQLServer:
                    return GetDataDT(mySQL, String.Empty, (SqlTransaction)null, sConnStr);
                case DatabaseProvider.Oracle:
                    return new OracleConn().GetDataDT(mySQL, String.Empty, null, sConnStr);
                case DatabaseProvider.PostgreSQL:
                    return new PostGreSQLConn().GetDataDT(mySQL, String.Empty, null, sConnStr);
                default:
                    return null;
            }
        }
        public DataTable GetDataDT(String mySQL, DbTransaction transaction, String sConnStr)
        {
            switch (dbProvider)
            {
                case DatabaseProvider.MicrosoftSQLServer:
                    return GetDataDT(mySQL, String.Empty, (SqlTransaction)transaction, sConnStr);
                case DatabaseProvider.Oracle:
                    return GetDataDT(mySQL, String.Empty, (SqlTransaction)transaction, sConnStr);
                case DatabaseProvider.PostgreSQL:
                    return GetDataDT(mySQL, String.Empty, (NpgsqlTransaction)transaction, sConnStr);
                default:
                    return null;
            }
        }
        DataTable GetDataDT(String mySQL, String strParam, SqlTransaction transaction, String sConnStr)
        {
            if (UsingWebService == "Y")
            {
                WebServiceConn();
                return DataTransport.DeCompressByteToDT(ws.GetDataTable(Crypt.Encrypt(mySQL), strParam, sConnStr));
            }

            return new SQLConn().GetDataDT(mySQL, strParam, transaction, sConnStr);
        }
        public DataTable GetDataDT(String mySQL, String strParams, DbTransaction transaction, String sConnStr)
        {
            switch (dbProvider)
            {
                case DatabaseProvider.MicrosoftSQLServer:
                    return GetDataDT(mySQL, strParams, (SqlTransaction)transaction, sConnStr);
                case DatabaseProvider.Oracle:
                    return new OracleConn().GetDataDT(mySQL, strParams, transaction, sConnStr);
                case DatabaseProvider.PostgreSQL:
                    return new PostGreSQLConn().GetDataDT(mySQL, strParams, (NpgsqlTransaction)transaction, sConnStr);
                default:
                    return null;
            }
        }

        public static DataTable GetDataTable(String sql, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtData";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }

        public DataTable GetDataTableBy(String mySQL, String id, String sConnStr)
        {
            if (UsingWebService == "Y")
            {
                WebServiceConn();
                return DataTransport.DeCompressByteToDT(ws.GetDataTableBy(Crypt.Encrypt(mySQL), id, sConnStr));
            }

            switch (dbProvider)
            {
                case DatabaseProvider.MicrosoftSQLServer:
                    return new SQLConn().GetDataTableBy(mySQL, id, sConnStr);
                case DatabaseProvider.Oracle:
                    return new OracleConn().GetDataTableBy(mySQL, id, sConnStr);
                case DatabaseProvider.PostgreSQL:
                    return new PostGreSQLConn().GetDataTableBy(mySQL, id, sConnStr);
                default:
                    return null;
            }
        }

        public int dbExecute(String strSQl, String sConnStr)
        {
            switch (dbProvider)
            {
                case DatabaseProvider.MicrosoftSQLServer:
                    return new SQLConn().dbExecute(strSQl, null, 0, sConnStr);
                case DatabaseProvider.Oracle:
                    return new OracleConn().dbExecute(strSQl, null, 0, sConnStr);
                case DatabaseProvider.PostgreSQL:
                    return new PostGreSQLConn().dbExecute(strSQl, null, 0, sConnStr);
                default:
                    return -1;
            }
        }
        public int dbExecute(String strSQl, DbTransaction transaction, String sConnStr)
        {
            switch (dbProvider)
            {
                case DatabaseProvider.MicrosoftSQLServer:
                    return dbExecute(strSQl, (SqlTransaction)transaction, 0, sConnStr);
                case DatabaseProvider.Oracle:
                    return new OracleConn().dbExecute(strSQl, transaction, 0, sConnStr);
                case DatabaseProvider.PostgreSQL:
                    return dbExecute(strSQl, transaction, 0, sConnStr);
                default:
                    return -1;
            }
        }
        public int dbExecute(String strSQl, DbTransaction transaction, int timeout, String sConnStr)
        {
            switch (dbProvider)
            {
                case DatabaseProvider.MicrosoftSQLServer:
                    return new SQLConn().dbExecute(strSQl, (SqlTransaction)transaction, timeout, sConnStr);
                case DatabaseProvider.Oracle:
                    return new OracleConn().dbExecute(strSQl, transaction, timeout, sConnStr);
                case DatabaseProvider.PostgreSQL:
                    return new PostGreSQLConn().dbExecute(strSQl, (NpgsqlTransaction)transaction, timeout, sConnStr);
                default:
                    return -1;
            }
        }
        public DataSet dbExecuteNSelect(String strSQl, DbTransaction transaction, int timeout, String sConnStr)
        {
            switch (dbProvider)
            {
                case DatabaseProvider.MicrosoftSQLServer:
                    return new SQLConn().dbExecuteNSelect(strSQl, (SqlTransaction)transaction, timeout, sConnStr);
                case DatabaseProvider.Oracle:
                    return new OracleConn().dbExecuteNSelect(strSQl, transaction, timeout, sConnStr);
                case DatabaseProvider.PostgreSQL:
                    return new PostGreSQLConn().dbExecuteNSelect(strSQl, (NpgsqlTransaction)transaction, timeout, sConnStr);
                default:
                    return null;
            }
        }

        public int dbExecuteWithoutTransaction(String strSQl, int iTimeOut, String sConnStr)
        {
            switch (dbProvider)
            {
                case DatabaseProvider.MicrosoftSQLServer:
                    return new SQLConn().dbExecuteWithoutTransaction(strSQl, iTimeOut, sConnStr);

                case DatabaseProvider.Oracle:
                    return new OracleConn().dbExecuteWithoutTransaction(strSQl, iTimeOut, sConnStr);

                case DatabaseProvider.PostgreSQL:
                    return new PostGreSQLConn().dbExecuteWithoutTransaction(strSQl, iTimeOut, sConnStr);
            }
            return -1;
        }

        public Boolean GenInsert(DataTable DTInsert, String sConnStr)
        {
            return GenInsert(DTInsert, (DbTransaction)null, sConnStr);
        }
        public Boolean GenInsert(DataTable DTInsert, SqlTransaction dbTran, String sConnStr)
        {
            return new SQLConn().GenInsert(DTInsert, dbTran, sConnStr);
        }
        public Boolean GenInsert(DataTable DTInsert, DbTransaction dbTran, String sConnStr)
        {
            switch (dbProvider)
            {
                case DatabaseProvider.MicrosoftSQLServer:
                    return new SQLConn().GenInsert(DTInsert, dbTran, sConnStr);
                case DatabaseProvider.Oracle:
                    return new OracleConn().GenInsert(DTInsert, dbTran, sConnStr);
                case DatabaseProvider.PostgreSQL:
                    return new PostGreSQLConn().GenInsert(DTInsert, dbTran, sConnStr);
                default:
                    return false;
            }
        }

        public Boolean InsertGPSData(DataTable DTInsert, Int32 UID, int AssetID, DbTransaction dbTran, String sConnStr)
        {
            switch (dbProvider)
            {
                case DatabaseProvider.MicrosoftSQLServer:
                    return new SQLConn().InsertGPSData(DTInsert, UID, AssetID, dbTran, sConnStr);
                case DatabaseProvider.Oracle:
                    return new OracleConn().InsertGPSData(DTInsert, UID, AssetID, dbTran, sConnStr);
                case DatabaseProvider.PostgreSQL:
                    return new PostGreSQLConn().InsertGPSData(DTInsert, UID, AssetID, dbTran, sConnStr);
                default:
                    return false;
            }
        }
        public Boolean InsertGPSDataDetail(DataTable DTInsert, Int32 UID, DbTransaction dbTran, String sConnStr)
        {
            switch (dbProvider)
            {
                case DatabaseProvider.MicrosoftSQLServer:
                    return new SQLConn().InsertGPSDataDetail(DTInsert, UID, dbTran, sConnStr);
                case DatabaseProvider.Oracle:
                    return new OracleConn().InsertGPSDataDetail(DTInsert, UID, dbTran, sConnStr);
                case DatabaseProvider.PostgreSQL:
                    return new PostGreSQLConn().InsertGPSDataDetail(DTInsert, UID, dbTran, sConnStr);
                default:
                    return false;
            }
        }
        public Boolean DeleteOldLive(int AssetID, DbTransaction dbTran)
        {
            switch (dbProvider)
            {
                case DatabaseProvider.MicrosoftSQLServer:
                    return new SQLConn().DeleteOldLive(AssetID, dbTran);
                case DatabaseProvider.Oracle:
                    return new OracleConn().DeleteOldLive(AssetID, dbTran);
                case DatabaseProvider.PostgreSQL:
                    return new PostGreSQLConn().DeleteOldLive(AssetID, dbTran);
                default:
                    return false;
            }
        }

        public DataTable CTRLTBL()
        {
            return new SQLConn().CTRLTBL();
        }
        public DataSet ProcessData(DataSet DSProc, String strDB, Boolean bUseTransaction = true, String uid = "")
        {
            DataSet ds = new DataSet();

            if (UsingWebService == "Y")
            {
                WebServiceConn();
                Byte[] b = DataTransport.CompressDS(DSProc);

                ds = DataTransport.DeCompressByteToDataSet(ws.ProcessData(b, strDB));
            }
            else
            {
                switch (dbProvider)
                {
                    case DatabaseProvider.MicrosoftSQLServer:
                        ds = new SQLConn().ProcessData(DSProc, strDB, bUseTransaction, uid);
                        break;
                    case DatabaseProvider.Oracle:
                        ds = new OracleConn().ProcessData(DSProc, strDB, bUseTransaction);
                        break;
                    case DatabaseProvider.PostgreSQL:
                        ds = new PostGreSQLConn().ProcessData(DSProc, strDB, bUseTransaction);
                        break;
                }
            }

            try
            {
                if (ds != null)
                    if (ds.Tables.Contains("CTRLTBL"))
                    {
                        DataRow[] dRow = ds.Tables["CTRLTBL"].Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");
                        if (dRow.Length > 0)
                            foreach (DataRow dr in ds.Tables["CTRLTBL"].Rows)
                            {
                                foreach (DataColumn dc in ds.Tables["CTRLTBL"].Columns)
                                    Errors.WriteToLog(dc.ToString() + " > " + dr[dc].ToString());

                                Errors.WriteToLog("\r\n");
                            }
                    }
            }
            catch { }

            return ds;
        }

        public Boolean GenUpdate(DataTable DT, String strWhereClause, String sConnStr)
        {
            return GenUpdate(DT, strWhereClause, (DbTransaction)null, sConnStr);
        }
        Boolean GenUpdate(DataTable DT, String strWhereClause, SqlTransaction transaction, String sConnStr)
        {
            return new SQLConn().GenUpdate(DT, strWhereClause, transaction, sConnStr);
        }
        public Boolean GenUpdate(DataTable DT, String strWhereClause, DbTransaction dbTran, String sConnStr)
        {
            switch (dbProvider)
            {
                case DatabaseProvider.MicrosoftSQLServer:
                    return GenUpdate(DT, strWhereClause, (SqlTransaction)dbTran, sConnStr);
                case DatabaseProvider.Oracle:
                    return new OracleConn().GenUpdate(DT, strWhereClause, dbTran, sConnStr);
                case DatabaseProvider.PostgreSQL:
                    return new PostGreSQLConn().GenUpdate(DT, strWhereClause, (NpgsqlTransaction)dbTran, sConnStr);
                default:
                    return false;
            }
        }

        public Boolean GenDelete(String strDelete, String strWhereClause, String sConnStr)
        {
            return GenDelete(strDelete, strWhereClause, (DbTransaction)null, sConnStr);
        }
        Boolean GenDelete(String strDelete, String strWhereClause, SqlTransaction transaction, String sConnStr)
        {
            return new SQLConn().GenDelete(strDelete, strWhereClause, transaction, sConnStr);
        }
        public Boolean GenDelete(String strDelete, String strWhereClause, DbTransaction dbTran, String sConnStr)
        {
            switch (dbProvider)
            {
                case DatabaseProvider.MicrosoftSQLServer:
                    return GenDelete(strDelete, strWhereClause, (SqlTransaction)dbTran, sConnStr);
                case DatabaseProvider.Oracle:
                    return new OracleConn().GenDelete(strDelete, strWhereClause, dbTran, sConnStr);
                case DatabaseProvider.PostgreSQL:
                    return new PostGreSQLConn().GenDelete(strDelete, strWhereClause, (NpgsqlTransaction)dbTran, sConnStr);
                default:
                    return false;
            }
        }

        public static DbConnection GetConnection(String sConnStr)
        {
            switch (dbProvider)
            {
                case DatabaseProvider.MicrosoftSQLServer:
                    return SQLConn.GetConnection(sConnStr);
                case DatabaseProvider.Oracle:
                    return SQLConn.GetConnection(sConnStr);
                case DatabaseProvider.PostgreSQL:
                    return PostGreSQLConn.GetConnection(sConnStr);
                default:
                    return SQLConn.GetConnection(sConnStr);
            }
        }
        public int GetNextID(String strTableName, DbTransaction transaction, String sConnStr)
        {
            switch (dbProvider)
            {
                case DatabaseProvider.MicrosoftSQLServer:
                    return new SQLConn().GetNextID(strTableName, transaction, sConnStr);
                case DatabaseProvider.Oracle:
                    return new OracleConn().GetNextID(strTableName, transaction, sConnStr);
                case DatabaseProvider.PostgreSQL:
                    return new PostGreSQLConn().GetNextID(strTableName, transaction, sConnStr);
                default:
                    return -1;
            }
        }
        public int GetNumberOfRecords(String pTable, String pColumn, String pWhere, String sConnStr)
        {
            switch (dbProvider)
            {
                case DatabaseProvider.MicrosoftSQLServer:
                    return new SQLConn().GetNumberOfRecords(pTable, pColumn, pWhere, sConnStr);
                case DatabaseProvider.Oracle:
                    return new OracleConn().GetNumberOfRecords(pTable, pColumn, pWhere, sConnStr);
                case DatabaseProvider.PostgreSQL:
                    return new PostGreSQLConn().GetNumberOfRecords(pTable, pColumn, pWhere, sConnStr);
                default:
                    return -1;
            }
        }
        public DataSet GetDS(String mySQL, String sConnStr)
        {
            switch (dbProvider)
            {
                case DatabaseProvider.MicrosoftSQLServer:
                    return new SQLConn().GetDS(mySQL, sConnStr);
                case DatabaseProvider.Oracle:
                    return new OracleConn().GetDS(mySQL, sConnStr);
                case DatabaseProvider.PostgreSQL:
                    return new PostGreSQLConn().GetDS(mySQL, sConnStr);
                default:
                    return null;
            }
        }

        public static String GetDBName(String sConnStr)
        {
            return ConnName.GetDBName(sConnStr);
        }

        #region MySQL
        //public Boolean ScriptExecute(String strSQl, String sConnStr)
        //{
        //    return new MySQLConn().ScriptExecute(strSQl, sConnStr);
        //}
        //public int dbExecute(String strSQl, MySqlTransaction transaction, int timeout, String sConnStr)
        //{
        //    return new MySQLConn().dbExecute(strSQl, transaction, timeout, sConnStr);
        //}
        //public DataSet dbExecuteNSelect(String strSQl, MySqlTransaction transaction, int timeout, String sConnStr)
        //{
        //    return new MySQLConn().dbExecuteNSelect(strSQl, transaction, timeout, sConnStr);
        //}
        //DataTable GetDataDT(String mySQL, String strParam, MySqlTransaction transaction, String sConnStr)
        //{
        //    return new MySQLConn().GetDataDT(mySQL, strParam, transaction, sConnStr);
        //}
        //public Boolean GenInsert(DataTable DTInsert, MySqlTransaction dbTran, String sConnStr)
        //{
        //    return new MySQLConn().GenInsert(DTInsert, dbTran, sConnStr);
        //}
        //public Boolean GenUpdate(DataTable DTInsert, String strWhereClause, MySqlTransaction dbTran, String sConnStr)
        //{
        //    return new MySQLConn().GenUpdate(DTInsert, strWhereClause, dbTran, sConnStr);
        //}
        //Boolean GenDelete(String strDelete, String strWhereClause, MySqlTransaction transaction, String sConnStr)
        //{
        //    return new MySQLConn().GenDelete(strDelete, strWhereClause, transaction, sConnStr);
        //}
        #endregion

        #region Oracle
        public Boolean OracleScriptExecute(String strSQl, String sConnStr)
        {
            return new OracleConn().ScriptExecute(strSQl, sConnStr);
        }
        #endregion

    }
}