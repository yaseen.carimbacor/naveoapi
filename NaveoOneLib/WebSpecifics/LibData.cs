﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using NaveoOneLib.DBCon;
using NaveoOneLib.Services;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Services.Assets;
using NaveoOneLib.Services.Users;
//using NaveoOneLib.Services.Common;
using NaveoOneLib.Common;
using System.Threading.Tasks;
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.Zones;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Drivers;
using NaveoOneLib.Models.Users;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Services.GPS;
using NaveoOneLib.Services.Drivers;
using NaveoOneLib.Services.Reportings;
using NaveoOneLib.Services.Zones;

namespace NaveoOneLib.WebSpecifics
{
    public class LibData
    {
        public List<int> GetAllAssets(int iLogin, String sConnStr)
        {
            User u = new UserService().GetUserById(iLogin, User.EditModePswd, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in u.lMatrix)
                iParentIDs.Add(m.GMID);

            return GetAllAssets(iParentIDs, sConnStr);
        }
        public List<int> GetAllAssets(List<Matrix> lMatrix, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            return GetAllAssets(iParentIDs, sConnStr);
        }
        public List<int> GetAllAssets(List<int> iParentIDs, String sConnStr)
        {
            Globals.dtMatrix = new DataTable();
            Globals.dtAsset = new DataTable();
            Globals.dtDriver = new DataTable();
            Globals.dtRules = new DataTable();

            List<int> lAssetID = new List<int>();

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new AssetService().sGetAsset(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtAsset";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            DataTable dtAsset = ds.Tables["dtAsset"];
            foreach (DataRow dr in dtAsset.Rows)
                lAssetID.Add((int)dr["AssetID"]);

            return lAssetID;
        }

        //Matrix
        public DataTable dtGMZone(int uLogin, String sConnStr)
        {
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Zones);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMZone";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }
        public DataTable dtGMZoneType(int uLogin, String sConnStr)
        {
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.ZoneTypes);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMZoneType";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }

        public DataTable dtGMDriver(int uLogin, String sConnStr)
        {
            List<Driver.ResourceType> lrt = new List<Driver.ResourceType>();
            lrt.Add(Driver.ResourceType.Driver);
            return dtGMDriver(uLogin, lrt, true, sConnStr);
        }
        public DataTable dtGMDriver(int uLogin, Driver.ResourceType rt, String sConnStr)
        {
            List<Driver.ResourceType> lrt = new List<Driver.ResourceType>();
            lrt.Add(rt);
            return dtGMDriver(uLogin, lrt, true, sConnStr);
        }
        public DataTable dtGMDriver(int uLogin, List<Driver.ResourceType> lrt, Boolean bShowActiveOnly, String sConnStr)
        {
            if (lrt == null)
            {
                lrt = new List<Driver.ResourceType>();
                lrt.Add(Driver.ResourceType.Driver);
            }

            String srt = String.Empty;
            foreach (Driver.ResourceType rt in lrt)
                srt += "'" + rt.ToString() + "', ";
            if (srt.Length > 3)
                srt = srt.Substring(0, srt.Length - 2);

            List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Drivers);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMDriver";
            dtSql.Rows.Add(drSql);

            sql = "select * from GFI_FLT_Driver where EmployeeType in (" + srt + ")";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtResources";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            DataTable dtGMDriver = ds.Tables["dtGMDriver"];
            DataTable dtResources = ds.Tables["dtResources"];

            DataTable dt = dtGMDriver.Clone();
            foreach (DataRow dr in dtGMDriver.Rows)
            {
                if (dr["StrucID"].ToString().Contains("-"))
                {
                    dt.Rows.Add(dr.ItemArray);
                    continue;
                }

                String sCondition = "DriverID = " + dr["StrucID"];
                if (bShowActiveOnly)
                    sCondition += " and Status = 'Active'";
                DataRow[] result = dtResources.Select(sCondition);
                if (result.Length > 0)
                {
                    String s = String.Empty;

                    if (!String.IsNullOrEmpty(result[0]["sDriverName"].ToString()))
                        s = result[0]["sDriverName"].ToString();

                    dr["GMDescription"] = s;
                    dt.Rows.Add(dr.ItemArray);
                }
            }
            return dt;
        }

        public DataTable dtGMUser(int uLogin, Boolean bIncludeEmail, String sConnStr)
        {
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Users);
            if (bIncludeEmail)
                sql = sql.Replace("a.Names", "a.Names + '(' + a.Email + ')'");

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMUser";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }
        public DataTable dtGMRule(int uLogin, String sConnStr)
        {
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Rules);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMRule";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }
        public DataTable dtGMAsset(int uLogin, String sConnStr)
        {
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Assets);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMAsset";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }
        public DataTable dtGMMatrix(int uLogin, String sConnStr)
        {
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Groups);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMNone";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }
        public DataTable dtGMPlanningRequest(int uLogin, String sConnStr)
        {
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.PlanningRequest);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMPlanningRequest";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }
        public DataTable dtGMBlup(int uLogin, String sConnStr)
        {
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.BLUP);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMBlup";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }

        public static DataTable dtGMData(int uLogin, NaveoModels nm, String sConnStr)
        {
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, nm);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMData";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }
        public static List<int> lValidateData(Guid sUserToken, NaveoModels nm, String sConnStr, out int myUID)
        {
            int UID = new UserService().GetUserID(sUserToken, sConnStr);
            DataTable dt = dtGMData(UID, nm, sConnStr);

            List<int> lIDs = new List<int>();
            foreach (DataRow dr in dt.Rows)
            {
                int i = 0;
                if (int.TryParse(dr["StrucID"].ToString(), out i))
                    if (i > 0)
                        lIDs.Add(i);
            }

            myUID = UID;
            return lIDs.Distinct().ToList();
        }

        public String sqlValidatedData(Guid sUserToken, NaveoModels nm, String sConnStr)
        {
            int UID = new NaveoOneLib.Services.Users.UserService().GetUserID(sUserToken, sConnStr);
            return sqlValidatedData(UID, nm, sConnStr);
        }
        public String sqlValidatedData(int UID, NaveoModels nm, String sConnStr)
        {
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            String sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, nm);
            sql = sql.Replace("\r\n order by GMID --Remove from Oracle", "\r\n\r\n");
            sql = sql.Replace("SELECT Distinct GMID, GMDescription, ParentGMID, -2 StrucID, null myStatus, -9 mID, -20 matrixiID FROM tree"
                , "	insert #ValidatedData SELECT Distinct GMID, GMDescription, ParentGMID, -2 StrucID, null myStatus, -9 mID, -20 matrixiID FROM tree");

            sql = @"drop table if exists #ValidatedData

            create table #ValidatedData (GMID int, GMDescription nvarchar(500), ParentGMID int, StrucID int, myStatus nvarchar(500), mID int, matrixID int)
            create index vidx on #ValidatedData (GMID) 
            " + sql;

            #region Previous codes
            //String sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, nm);
            //sql = sql.Replace("\r\n order by GMID --Remove from Oracle", "\r\n\r\n");
            //sql = sql.Replace("FROM tree", "into #ValidatedData FROM tree");
            #endregion

            return sql;
        }
        public DataTable SearchAsset(int UID, NaveoModels nm, String SearchValue, String sConnStr)
        {
            String sql = sqlValidatedData(UID, nm, sConnStr);
            String search = "'%" + SearchValue + "%'";
            switch (nm)
            {
                case NaveoModels.Assets:
                    sql += @"select b.AssetID, b.AssetNumber, a.GMID ParentGMID
	                            from GFI_FLT_Asset b
	                            inner join #ValidatedData a on a.StrucID = b.AssetID
                            where AssetNumber like " + search;
                    break;

                case NaveoModels.BLUP:
                    sql += @"select b.iID, b.APL_ID, b.APL_FNAME, b.APL_FNAME, b.cat ParentGMID
	                            from GFI_GIS_BLUP b
	                            inner join #ValidatedData a on a.StrucID = b.iID
                            where APL_ID like " + search + " or APL_NAME like " + search + " or APL_FNAME like " + search + "";
                    break;
            }

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtSearch";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }

        public DataTable dtGMRoles(int uLogin, String sConnStr)
        {
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Roles);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMRoles";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }


        public DataTable dtGMScheduledPlanning(int uLogin, String sConnStr)
        {
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.ScheduledPlanning);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMScheduledPlanning";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }
        public DataTable dtGMVCA(String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new VCAHeaderService().sGetVCAInTree();
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMVCA";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }

        //Pagination
        public dtData dtGroups(Guid sUserToken, String sConnStr, int? Page, int? LimitPerPage)
        {
            int UID = new UserService().GetUserID(sUserToken, sConnStr);
            if (UID == -1)
                return new dtData();

            DataTable dtGroup = dtGMMatrix(UID, sConnStr);
            dtGroup.Columns.Remove("myStatus");
            dtGroup.Columns.Remove("MID");
            dtGroup.Columns.Remove("MatrixiID");
            dtGroup.Columns.Remove("StrucID");
            dtGroup.Columns["GMID"].ColumnName = "id";
            dtGroup.Columns["GMDescription"].ColumnName = "desc";
            dtGroup.Columns["ParentGMID"].ColumnName = "parent";

            DataTable ResultTable = new DataTable();
            ResultTable.Columns.Add("RowNum", typeof(int));
            ResultTable.Columns["RowNum"].AutoIncrement = true;
            ResultTable.Columns["RowNum"].AutoIncrementSeed = 1;
            ResultTable.Columns["RowNum"].AutoIncrementStep = 1;
            ResultTable.Merge(dtGroup);

            Pagination p = Pagination.GetRowNums(Page, LimitPerPage);
            DataTable dt = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo).CopyToDataTable();
            dt.TableName = "dtGroup";

            dtData dtD = new dtData();
            dtD.Data = dt;
            dtD.Rowcnt = ResultTable.Rows.Count;

            return dtD;
        }

        public dtData NaveoMasterData(Guid sUserToken, String sConnStr, NaveoModels nm, int? Page, int? LimitPerPage)
        {
            int UID = new UserService().GetUserID(sUserToken, sConnStr);
            if (UID == -1)
                return new dtData();

            List<Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);

            return NaveoMasterData(UID, lm, sConnStr, nm, Page, LimitPerPage);
        }

        public dtData NaveoMasterData(int UID, List<Matrix> lm, String sConnStr, NaveoModels nm, int? Page, int? LimitPerPage)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, nm);
            if (nm == NaveoModels.Users)
                sql = sql.Replace("a.Names", "a.Names + '(' + a.Email + ')'");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dt";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            DataTable dtGM = ds.Tables[0];

            //dtGM.Columns.Remove("myStatus");
            dtGM.Columns.Remove("MID");
            dtGM.Columns.Remove("MatrixiID");
            dtGM.Columns.Add("type");
            dtGM.Columns.Add("id", typeof(Int32));

            DataTable myData = dtGM.Clone();
            DataTable myGrp = dtGM.Clone();

            if (nm == NaveoModels.Groups)
                foreach (DataRow dr in dtGM.Rows)
                {
                    int i = -1;
                    int.TryParse(dr["GMID"].ToString(), out i);
                    if (i > 0)
                    {
                        dr["Type"] = "G";
                        dr["id"] = dr["GMID"];
                        myData.Rows.Add(dr.ItemArray);
                    }
                }
            else
                foreach (DataRow dr in dtGM.Rows)
                {
                    int i = -1;
                    int.TryParse(dr["GMID"].ToString(), out i);
                    if (i < 0)
                    {
                        if (dr["GMDescription"].ToString() == "Default Driver")
                            if (dr["ParentGMID"].ToString() == "1")
                            {
                                DataRow[] drd = dtGM.Select("GMID = 1");
                                if (drd.Length == 0)
                                    dr["ParentGMID"] = "-1";
                            }
                        dr["Type"] = "O";
                        dr["id"] = dr["StrucID"];
                        myData.Rows.Add(dr.ItemArray);
                    }
                    else
                    {
                        dr["Type"] = "G";
                        dr["id"] = dr["GMID"];
                        myGrp.Rows.Add(dr.ItemArray);
                    }
                }
            myData.Columns.Remove("GMID");
            myData.Columns.Remove("StrucID");
            myGrp.Columns.Remove("GMID");
            myGrp.Columns.Remove("StrucID");

            if (nm == NaveoModels.Assets)
            {
                myData.Columns.Add("TimeZone");

                List<Asset> la = new NaveoOneLib.WebSpecifics.LibData().lAsset(UID, sConnStr);
                foreach (Asset a in la)
                {
                    DataRow[] dr = myData.Select("id = '" + a.AssetID + "'");
                    if (dr.Length > 0)
                        foreach (DataRow d in dr)
                            d["TimeZone"] = a.TimeZoneTS;
                }
            }

            DataTable ResultTable = new DataTable();
            ResultTable.Columns.Add("rowNum", typeof(int));
            ResultTable.Columns["rowNum"].AutoIncrement = true;
            ResultTable.Columns["rowNum"].AutoIncrementSeed = 1;
            ResultTable.Columns["rowNum"].AutoIncrementStep = 1;
            ResultTable.Merge(myData);

            Pagination p = Pagination.GetRowNums(Page, LimitPerPage);
            DataTable dt = new DataTable();
            DataRow[] drChk = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo);
            if (drChk != null && drChk.Length > 0)
                dt = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo).CopyToDataTable();

            dt.TableName = "dt" + nm.ToString();
            dt.Merge(myGrp);
            dt.Columns["GMDescription"].ColumnName = "description";
            dt.Columns["ParentGMID"].ColumnName = "parentID";

            dtData dtD = new dtData();
            dtD.Data = dt;
            dtD.Rowcnt = myData.Rows.Count;

            return dtD;
        }

        public dtData NaveoMaintenanceData(Guid sUserToken, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = "select GFI_AMM_VehicleMaintStatus.Description, GFI_AMM_VehicleMaintStatus.MaintStatusId As id, F.Cnt, 'STATUS' AS TEXT from GFI_AMM_VehicleMaintStatus, (select count(MaintStatusId_cbo) as Cnt, MaintStatusId_cbo from GFI_AMM_VehicleMaintenance group by  MaintStatusId_cbo) F where GFI_AMM_VehicleMaintStatus.MaintStatusId = F.MaintStatusId_cbo and GFI_AMM_VehicleMaintStatus.Description NOT IN ('EXPIRED','VALID')";
            sql += "\r\n UNION ";
            sql += "\r\n select GFI_AMM_VehicleMaintTypes.Description, GFI_AMM_VehicleMaintTypes.MaintTypeId As id, F.Cnt, 'TYPE' AS TEXT from GFI_AMM_VehicleMaintTypes, (select count(MaintTypeId_cbo) as Cnt, MaintTypeId_cbo from GFI_AMM_VehicleMaintenance group by  MaintTypeId_cbo) F where GFI_AMM_VehicleMaintTypes.MaintTypeId = F.MaintTypeId_cbo ";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dt";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            DataTable dtGM = ds.Tables[0];

            dtGM.Columns.Add("type");
            //dtGM.Columns.Add("id", typeof(Int32));

            dtGM.Columns.Add("ParentId", typeof(Int32));

            DataTable myDataStatus = dtGM.Clone(); //All data here
            DataTable myDataType = dtGM.Clone();
            //DataTable myGrp = dtGM.Clone();

            // Zone Status

            DataRow drRootStatus = myDataStatus.NewRow();

            drRootStatus["Type"] = "G";
            drRootStatus["id"] = "999";
            drRootStatus["Description"] = "Maintenance Status";

            myDataStatus.Rows.Add(drRootStatus);

            //End Zone Status


            DataRow drRootStatusNone = myDataStatus.NewRow();

            drRootStatusNone["Type"] = "O";
            drRootStatusNone["id"] = "9999";
            drRootStatusNone["Description"] = "None";

            drRootStatusNone["ParentId"] = "999";
            drRootStatusNone["TEXT"] = "STATUS";


            myDataStatus.Rows.Add(drRootStatusNone);

            DataRow drRootType = myDataType.NewRow();

            drRootType["Type"] = "G";
            drRootType["id"] = "998";
            drRootType["Description"] = "Maintenance Type";

            myDataType.Rows.Add(drRootType);

            //First List status

            //ParentId hardcore

            //Send list Type
            foreach (DataRow dr in dtGM.Rows)
            {
                if (dr["TEXT"].ToString() == "STATUS")
                {
                    dr["Type"] = "O";
                    dr["ParentId"] = "999";
                    //Add value here
                    myDataStatus.Rows.Add(dr.ItemArray);
                }
                else if (dr["TEXT"].ToString() == "TYPE")
                {
                    dr["Type"] = "O";
                    dr["ParentId"] = "998";
                    //Add value here
                    myDataType.Rows.Add(dr.ItemArray);
                }

                //int i = -1;
                //int.TryParse(dr["GMID"].ToString(), out i);
                //if (i < 0)
                //{
                //        if (dr["ParentGMID"].ToString() == "1")
                //        {
                //            DataRow[] drd = dtGM.Select("GMID = 1");
                //            if (drd.Length == 0)
                //                dr["ParentGMID"] = "-1";
                //        }
                //    dr["Type"] = "O";
                //    dr["id"] = dr["StrucID"];
                //    myData.Rows.Add(dr.ItemArray);
                //}
                //else
                //{
                //    dr["Type"] = "G";
                //    dr["id"] = dr["GMID"];
                //    myGrp.Rows.Add(dr.ItemArray);
                //}
            }

            //dr["Type"] = "G";
            //    dr["id"] = dr["GMID"];
            //    myGrp.Rows.Add(dr.ItemArray);


            DataTable ResultTable = new DataTable();
            ResultTable.Columns.Add("rowNum", typeof(int));
            ResultTable.Columns["rowNum"].AutoIncrement = true;
            ResultTable.Columns["rowNum"].AutoIncrementSeed = 1;
            ResultTable.Columns["rowNum"].AutoIncrementStep = 1;
            ResultTable.Merge(myDataStatus);

            ResultTable.Merge(myDataType);

            //Pagination p = Pagination.GetRowNums(Page, LimitPerPage);


            dtData dtD = new dtData();
            dtD.Data = ResultTable;
            dtD.Rowcnt = ResultTable.Rows.Count;

            return dtD;
        }

        public dtData NaveoMaintenanceDataFAOnly(Guid sUserToken, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = "select GFI_AMM_VehicleMaintStatus.Description, GFI_AMM_VehicleMaintStatus.MaintStatusId As id, F.Cnt, 'STATUS' AS TEXT from GFI_AMM_VehicleMaintStatus, (select count(MaintStatusId_cbo) as Cnt, MaintStatusId_cbo from GFI_AMM_VehicleMaintenance group by  MaintStatusId_cbo) F where GFI_AMM_VehicleMaintStatus.MaintStatusId = F.MaintStatusId_cbo and GFI_AMM_VehicleMaintStatus.Description NOT IN ('EXPIRED','VALID')";
            sql += "\r\n UNION ";
            sql += "\r\n select GFI_AMM_VehicleMaintTypes.Description, GFI_AMM_VehicleMaintTypes.MaintTypeId As id, F.Cnt, 'TYPE' AS TEXT from GFI_AMM_VehicleMaintTypes, (select count(MaintTypeId_cbo) as Cnt, MaintTypeId_cbo from GFI_AMM_VehicleMaintenance group by  MaintTypeId_cbo) F where GFI_AMM_VehicleMaintTypes.MaintTypeId = F.MaintTypeId_cbo and GFI_AMM_VehicleMaintTypes.MaintCatId_cbo = 11";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dt";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            DataTable dtGM = ds.Tables[0];

            dtGM.Columns.Add("type");
            //dtGM.Columns.Add("id", typeof(Int32));

            dtGM.Columns.Add("ParentId", typeof(Int32));

            DataTable myDataStatus = dtGM.Clone(); //All data here
            DataTable myDataType = dtGM.Clone();
            //DataTable myGrp = dtGM.Clone();

            // Zone Status

            DataRow drRootStatus = myDataStatus.NewRow();

            drRootStatus["Type"] = "G";
            drRootStatus["id"] = "999";
            drRootStatus["Description"] = "Maintenance Status";

            myDataStatus.Rows.Add(drRootStatus);

            //End Zone Status


            DataRow drRootStatusNone = myDataStatus.NewRow();

            drRootStatusNone["Type"] = "O";
            drRootStatusNone["id"] = "9999";
            drRootStatusNone["Description"] = "None";

            drRootStatusNone["ParentId"] = "999";
            drRootStatusNone["TEXT"] = "STATUS";


            myDataStatus.Rows.Add(drRootStatusNone);

            DataRow drRootType = myDataType.NewRow();

            drRootType["Type"] = "G";
            drRootType["id"] = "998";
            drRootType["Description"] = "Maintenance Type";

            myDataType.Rows.Add(drRootType);

            //First List status

            //ParentId hardcore

            //Send list Type
            foreach (DataRow dr in dtGM.Rows)
            {
                if (dr["TEXT"].ToString() == "STATUS")
                {
                    dr["Type"] = "O";
                    dr["ParentId"] = "999";
                    //Add value here
                    myDataStatus.Rows.Add(dr.ItemArray);
                }
                else if (dr["TEXT"].ToString() == "TYPE")
                {
                    dr["Type"] = "O";
                    dr["ParentId"] = "998";
                    //Add value here
                    myDataType.Rows.Add(dr.ItemArray);
                }

                //int i = -1;
                //int.TryParse(dr["GMID"].ToString(), out i);
                //if (i < 0)
                //{
                //        if (dr["ParentGMID"].ToString() == "1")
                //        {
                //            DataRow[] drd = dtGM.Select("GMID = 1");
                //            if (drd.Length == 0)
                //                dr["ParentGMID"] = "-1";
                //        }
                //    dr["Type"] = "O";
                //    dr["id"] = dr["StrucID"];
                //    myData.Rows.Add(dr.ItemArray);
                //}
                //else
                //{
                //    dr["Type"] = "G";
                //    dr["id"] = dr["GMID"];
                //    myGrp.Rows.Add(dr.ItemArray);
                //}
            }

            //dr["Type"] = "G";
            //    dr["id"] = dr["GMID"];
            //    myGrp.Rows.Add(dr.ItemArray);


            DataTable ResultTable = new DataTable();
            ResultTable.Columns.Add("rowNum", typeof(int));
            ResultTable.Columns["rowNum"].AutoIncrement = true;
            ResultTable.Columns["rowNum"].AutoIncrementSeed = 1;
            ResultTable.Columns["rowNum"].AutoIncrementStep = 1;
            ResultTable.Merge(myDataStatus);

            ResultTable.Merge(myDataType);

            //Pagination p = Pagination.GetRowNums(Page, LimitPerPage);


            dtData dtD = new dtData();
            dtD.Data = ResultTable;
            dtD.Rowcnt = ResultTable.Rows.Count;

            return dtD;
        }

        public dtData treeResources(Guid sUserToken, String sConnStr, Driver.ResourceType rt, int? Page, int? LimitPerPage)
        {
            int UID = new UserService().GetUserID(sUserToken, sConnStr);
            DataTable dtGM = new LibData().dtGMDriver(UID, rt, sConnStr);
            dtGM.Columns.Remove("myStatus");
            dtGM.Columns.Remove("MID");
            dtGM.Columns.Remove("MatrixiID");
            dtGM.Columns.Add("type");
            dtGM.Columns.Add("id", typeof(Int32));

            DataTable myData = dtGM.Clone();
            DataTable myGrp = dtGM.Clone();
            foreach (DataRow dr in dtGM.Rows)
            {
                int i = -1;
                int.TryParse(dr["GMID"].ToString(), out i);
                if (i < 0)
                {
                    if (dr["GMDescription"].ToString() == "Default Driver")
                        if (dr["ParentGMID"].ToString() == "1")
                        {
                            DataRow[] drd = dtGM.Select("GMID = 1");
                            if (drd.Length == 0)
                                dr["ParentGMID"] = "-1";
                        }
                    dr["Type"] = "O";
                    dr["id"] = dr["StrucID"];
                    myData.Rows.Add(dr.ItemArray);
                }
                else
                {
                    dr["Type"] = "G";
                    dr["id"] = dr["GMID"];
                    myGrp.Rows.Add(dr.ItemArray);
                }
            }

            myData.Columns.Remove("GMID");
            myData.Columns.Remove("StrucID");
            myGrp.Columns.Remove("GMID");
            myGrp.Columns.Remove("StrucID");

            DataTable ResultTable = new DataTable();
            ResultTable.Columns.Add("rowNum", typeof(int));
            ResultTable.Columns["rowNum"].AutoIncrement = true;
            ResultTable.Columns["rowNum"].AutoIncrementSeed = 1;
            ResultTable.Columns["rowNum"].AutoIncrementStep = 1;
            ResultTable.Merge(myData);

            Pagination p = Pagination.GetRowNums(Page, LimitPerPage);
            DataTable dt = new DataTable();
            DataRow[] drChk = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo);
            if (drChk != null && drChk.Length > 0)
                dt = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo).CopyToDataTable();

            dt.TableName = "dtResources";
            dt.Merge(myGrp);
            dt.Columns["GMDescription"].ColumnName = "description";
            dt.Columns["ParentGMID"].ColumnName = "parentID";
            dtData dtD = new dtData();
            dtD.Data = dt;
            dtD.Rowcnt = myData.Rows.Count;

            return dtD;
        }
        public DataTable dtVehicleMaintTypeAssign(int uLogin, String sConnStr)
        {
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new VehicleMaintTypeService().sGetVehicleMaintTypeForMaintAssign(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtVehicleMaintTypeAssign";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }
        public DataTable dtVehicleTypes(int uLogin, String sConnStr)
        {
            //List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin);
            //List<int> iParentIDs = new List<int>();
            //foreach (Matrix m in lm)
            //    iParentIDs.Add(m.GMID);

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new VehicleTypesService().sGetVehicleTypes();
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtVehicleTypes";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }
        public DataTable dtVehicleMaintCat(int uLogin, String sConnStr)
        {
            //List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin);
            //List<int> iParentIDs = new List<int>();
            //foreach (Matrix m in lm)
            //    iParentIDs.Add(m.GMID);

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new VehicleMaintCatService().sGetVehicleMaintCat();
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtVehicleMaintCat";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }
        public DataTable dtVehicleMaintStatus(int uLogin, String sConnStr)
        {
            //List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin);
            //List<int> iParentIDs = new List<int>();
            //foreach (Matrix m in lm)
            //    iParentIDs.Add(m.GMID);

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new VehicleMaintStatusService().sGetVehicleMaintStatus();
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtVehicleMaintStatus";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }

        public DataTable dtLookupValues(int uLogin, int LkpType, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String s = new UserService().sGetMatrixUserByUserID(uLogin);
            s = s.Replace("SELECT m.MID, m.iID, m.GMID, gm.GMDescription", "SELECT m.GMID");
            s = s.Replace("order by gm.GMID", " ");

            String sql = @"select * from GFI_SYS_LookUpValues where TID = " + LkpType + @" and RootMatrixID is null
                            union 
                            select * from GFI_SYS_LookUpValues where TID = " + LkpType + @" and RootMatrixID in (" + s + ")";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtLookupValues";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }
        public DataTable dtLookupValues(int uLogin, String sLkpType, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String s = new UserService().sGetMatrixUserByUserID(uLogin);
            s = s.Replace("SELECT m.MID, m.iID, m.GMID, gm.GMDescription", "SELECT m.GMID");
            s = s.Replace("order by gm.GMID", " ");

            String sql = @"
select v.* from GFI_SYS_LookUpValues v 
    inner join GFI_SYS_LookUpTypes t on t.TID = v.TID
where t.code = '" + sLkpType + @"' and v.RootMatrixID is null
union 
select v.* from GFI_SYS_LookUpValues v 
    inner join GFI_SYS_LookUpTypes t on t.TID = v.TID
where t.code = '" + sLkpType + @"' and v.RootMatrixID in (" + s + ")";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtLookupValues";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }

        public int GetMasterParentfromGMID(int iGMID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new GroupMatrixService().sGetAllParentsfromGMID(iGMID) + "  where ParentGMID = (select GMID from GFI_SYS_GroupMatrix where GMDescription = '### All Clients ###')";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dt";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return (int)ds.Tables[0].Rows[0][0];
        }

        public DataTable GetAutoReportingConfig(int uLogin, String sConnStr)
        {
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new AutoReportingConfigService().sGetAutoReportingConfig(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtVehicleMaintStatus";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }

        public DataSet dsZones(List<int> iParentIDs, String sConnStr, int? Page, int? LimitPerPage)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String rep = @"z.*
	, (select max(Longitude) from GFI_FLT_ZoneDetail where ZoneID = z.ZoneID) MaxLon
	, (select min(Longitude) from GFI_FLT_ZoneDetail where ZoneID = z.ZoneID) MinLon
	, (select min(Latitude) from GFI_FLT_ZoneDetail where ZoneID = z.ZoneID) MinLat
	, (select max(Latitude) from GFI_FLT_ZoneDetail where ZoneID = z.ZoneID) MaxLat";
            String sql = new ZoneService().sGetZoneHeader(iParentIDs, sConnStr, Page, LimitPerPage);
            sql = sql.Replace("z.*", rep);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZoneH";
            dtSql.Rows.Add(drSql);

            sql = new ZoneService().sGetZoneDetail(iParentIDs, sConnStr, Page, LimitPerPage);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZoneD";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            String[] s = new String[ds.Tables["dtZoneH"].Columns.Count];
            int i = 0;
            foreach (DataColumn dc in ds.Tables["dtZoneH"].Columns)
            {
                s[i] = dc.ColumnName;
                i++;
            }
            DataTable dtZoneH = ds.Tables["dtZoneH"].DefaultView.ToTable(true, s);
            ds.Tables.Remove("dtZoneH");
            ds.Tables.Add(dtZoneH);

            return ds;
        }
        public DataSet dsZones(int uLogin, String sConnStr, Boolean bOnlyHeaderDetails, int? Page, int? LimitPerPage)
        {
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String rep = @"z.*
	, (select max(Longitude) from GFI_FLT_ZoneDetail where ZoneID = z.ZoneID) MaxLon
	, (select min(Longitude) from GFI_FLT_ZoneDetail where ZoneID = z.ZoneID) MinLon
	, (select min(Latitude) from GFI_FLT_ZoneDetail where ZoneID = z.ZoneID) MinLat
	, (select max(Latitude) from GFI_FLT_ZoneDetail where ZoneID = z.ZoneID) MaxLat";
            String sql = new ZoneService().sGetZoneHeader(iParentIDs, sConnStr, Page, LimitPerPage);
            sql = sql.Replace("z.*", rep);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZoneH";
            dtSql.Rows.Add(drSql);

            sql = new ZoneService().sGetZoneDetail(iParentIDs, sConnStr, Page, LimitPerPage);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZoneD";
            dtSql.Rows.Add(drSql);

            if (!bOnlyHeaderDetails)
            {
                sql = new ZoneTypeService().sGetZoneTypes(iParentIDs, sConnStr);
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtZoneTypes";
                dtSql.Rows.Add(drSql);

                sql = new ZoneHeadZoneTypeService().sGetZHZTs(iParentIDs, sConnStr);
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtZHZT";
                dtSql.Rows.Add(drSql);

                sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Zones);
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtGMZone";
                dtSql.Rows.Add(drSql);

                sql = new MatrixService().sGetMatrixByGMID(iParentIDs, "GFI_SYS_GroupMatrixZone");
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtGMLZone";
                dtSql.Rows.Add(drSql);

                sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.ZoneTypes);
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtGMZoneType";
                dtSql.Rows.Add(drSql);
            }

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            String[] s = new String[ds.Tables["dtZoneH"].Columns.Count];
            int i = 0;
            foreach (DataColumn dc in ds.Tables["dtZoneH"].Columns)
            {
                s[i] = dc.ColumnName;
                i++;
            }
            DataTable dtZoneH = ds.Tables["dtZoneH"].DefaultView.ToTable(true, s);
            ds.Tables.Remove("dtZoneH");
            ds.Tables.Add(dtZoneH);

            return ds;
        }
        public List<Zone> lZone(int uLogin, String sConnStr, int? Page, int? LimitPerPage, Boolean bOnlyHeaderDetails = false, List<int> lZoneIDs = null)
        {
            DataSet ds = dsZones(uLogin, sConnStr, bOnlyHeaderDetails, Page, LimitPerPage);

            //--- Zone ---
            DataTable dtZoneD = ds.Tables["dtZoneD"];
            DataTable dtZoneH = ds.Tables["dtZoneH"];
            if (lZoneIDs != null)
            {
                String s = String.Empty;
                foreach (int i in lZoneIDs)
                    s += "zoneID = " + i + " or ";
                if (s.Length > 4)
                    s = s.Substring(0, s.Length - 3);

                DataTable dt = dtZoneH.Select(s).CopyToDataTable();
                dtZoneH = dt;
            }

            List<Zone> lz = new myConverter().ConvertToList<Zone>(dtZoneH).Distinct().ToList();
            List<ZoneDetail> ld = new myConverter().ConvertToList<ZoneDetail>(dtZoneD);

            DataTable dtZHZT = new DataTable();
            DataTable dtZoneTypes = new DataTable();
            List<ZoneHeadZoneType> lzhzt = new List<ZoneHeadZoneType>();
            if (!bOnlyHeaderDetails)
            {
                dtZHZT = ds.Tables["dtZHZT"];
                dtZoneTypes = ds.Tables["dtZoneTypes"];

                lzhzt = new myConverter().ConvertToList<ZoneHeadZoneType>(dtZHZT);
            }

            ParallelOptions p = new ParallelOptions();
            p.MaxDegreeOfParallelism = -1;
            Parallel.ForEach(lz, p, z =>
            //foreach(Zone z in lz)
            {
                z.zoneDetails = ld.FindAll(o => o.zoneID == z.zoneID);

                if (!bOnlyHeaderDetails)
                {
                    z.lZoneType = new ZoneTypeService().getMyZoneTypes(lzhzt.FindAll(o => o.ZoneHeadID == z.zoneID), dtZoneTypes, true);

                    DataRow[] dRows = ds.Tables["dtGMZone"].Select("StrucID = " + z.zoneID + "");
                    foreach (DataRow dr in dRows)
                    {
                        Matrix mx = new Matrix();
                        mx.MID = (int)dr["MID"];
                        mx.iID = (int)dr["StrucID"];
                        mx.GMID = (int)dr["ParentGMID"];

                        String strDesc = String.Empty;
                        DataRow[] drGMDesc = ds.Tables["dtGMZone"].Select("GMID = " + dr["ParentGMID"] + "");
                        if (drGMDesc.Length > 0)
                            strDesc = drGMDesc[0]["GMDescription"].ToString();

                        mx.GMDesc = strDesc;
                        mx.oprType = DataRowState.Unchanged;
                        z.lMatrix.Add(mx);
                    }
                }
            });
            return lz;
        }
        public DataTable dtZoneHeader(int uLogin, String sConnStr, int? Page, int? LimitPerPage)
        {
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new ZoneService().sGetZoneHeader(iParentIDs, sConnStr, Page, LimitPerPage);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZoneH";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            DataTable dtZoneH = ds.Tables["dtZoneH"];

            return dtZoneH;
        }

        public List<Asset> lAsset(int uLogin, String sConnStr)
        {
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new AssetService().sGetAsset(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtAsset";
            dtSql.Rows.Add(drSql);

            sql = new AssetDeviceMapService().sGetAssetDeviceMap(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtAssetMap";
            dtSql.Rows.Add(drSql);

            sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Assets);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMAsset";
            dtSql.Rows.Add(drSql);

            sql = new MatrixService().sGetMatrixByGMID(iParentIDs, "GFI_SYS_GroupMatrixAsset");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMLAsset";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            //--- Asset ---
            DataTable dtAsset = ds.Tables["dtAsset"];
            //foreach (DataRow dr in dtAsset.Rows)
            //    dr["AssetNumber"] = BaseService.Decrypt(dr["AssetNumber"].ToString());
            List<Asset> la = new myConverter().ConvertToList<Asset>(dtAsset);
            DataTable dtDeviceMap = ds.Tables["dtAssetMap"];
            List<AssetDeviceMap> ldm = new myConverter().ConvertToList<AssetDeviceMap>(dtDeviceMap);
            foreach (Asset a in la)
                a.assetDeviceMap = ldm.FindAll(o => o.AssetID == a.AssetID).FirstOrDefault();

            foreach (Asset a in la)
            {
                String tzName = a.TimeZoneID;
                if (tzName == String.Empty)
                    tzName = TimeZone.CurrentTimeZone.StandardName;
                a.TimeZoneTS = new Utils().GetTimeSpan(tzName);

                DataRow[] dRows = ds.Tables["dtGMLAsset"].Select("iID = " + a.AssetID + "");

                foreach (DataRow dr in dRows)
                {
                    Matrix mx = new Matrix();
                    mx.MID = (int)dr["MID"];
                    mx.iID = (int)dr["iID"];
                    mx.GMID = (int)dr["GMID"];
                    mx.GMDesc = dr["GMDescription"].ToString();
                    mx.oprType = DataRowState.Unchanged;
                    a.lMatrix.Add(mx);
                }
            }

            return la;
        }
        public List<Driver> lDriver(int uLogin, String sConnStr)
        {
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new DriverService().sGetDriver(iParentIDs, String.Empty, false, Driver.ResourceType.Driver, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtDriver";
            dtSql.Rows.Add(drSql);

            sql = new MatrixService().sGetMatrixByGMID(iParentIDs, "GFI_SYS_GroupMatrixDriver");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMLDriver";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            //--- Driver ---
            DataTable dtDriver = ds.Tables["dtDriver"];
            List<Driver> ldr = new myConverter().ConvertToList<Driver>(dtDriver);

            foreach (Driver d in ldr)
            {
                DataRow[] dRows = ds.Tables["dtGMLDriver"].Select("iID = " + d.DriverID + "");

                foreach (DataRow dr in dRows)
                {
                    Matrix mx = new Matrix();
                    mx.MID = (int)dr["MID"];
                    mx.iID = (int)dr["iID"];
                    mx.GMID = (int)dr["GMID"];
                    mx.GMDesc = dr["GMDescription"].ToString();
                    mx.oprType = DataRowState.Unchanged;
                    d.lMatrix.Add(mx);
                }
            }
            return ldr;
        }
        public List<Matrix> lMatrix(int uLogin, String sConnStr)
        {
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin, sConnStr);
            return lm;
        }
        DataSet QryNaveoOne(String sql, String sWebUrl, String strDB)
        {
            Connection c = new Connection(strDB);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            Connection.UsingWebService = "Y";
            Connection.WebURL = sWebUrl;

            dtSql.Rows.Clear();
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            dtSql.Rows.Add(drSql);

            DataSet dsResult = c.GetDataDS(dtSql, strDB);
            return dsResult;
        }
        public DataSet QryNaveoOne()
        {
            String sql = new GPSDataService().sGetLatestGPSDataForAllAssets();
            DataSet ds = QryNaveoOne(sql, "http://NaveoCore.gets-it.net:83/EngService.asmx", "DB1");
            DataSet dsFromNaveoOne = new DataSet();
            DataTable dt = new NaveoOneLib.Maps.ESRIMapService().dtLive(ds.Tables[0], "");
            dsFromNaveoOne.Merge(dt);

            sql = sql.Replace("select 'CoreDB1' SvrDBName,", "select 'CoreDB2' SvrDBName,");
            ds = QryNaveoOne(sql, "http://NaveoCore.gets-it.net:83/EngService.asmx", "DB2");
            dt = new Maps.ESRIMapService().dtLive(ds.Tables[0], "");
            dsFromNaveoOne.Merge(dt);

            sql = sql.Replace("select 'CoreDB2' SvrDBName,", "select 'CoreDB4' SvrDBName,");
            ds = QryNaveoOne(sql, "http://NaveoCore.gets-it.net:83/EngService.asmx", "DB4");
            dt = new Maps.ESRIMapService().dtLive(ds.Tables[0], "");
            dsFromNaveoOne.Merge(dt);

            sql = sql.Replace("select 'CoreDB4' SvrDBName,", "select 'SvrDB1' SvrDBName,");
            ds = QryNaveoOne(sql, "http://NaveoSVR.gets-it.net:83/EngService.asmx", "DB1");
            dt = new Maps.ESRIMapService().dtLive(ds.Tables[0], "");
            dsFromNaveoOne.Merge(dt);

            sql = sql.Replace("select 'SvrDB1' SvrDBName,", "select 'SvrDB2' SvrDBName,");
            ds = QryNaveoOne(sql, "http://NaveoSVR.gets-it.net:83/EngService.asmx", "DB2");
            dt = new Maps.ESRIMapService().dtLive(ds.Tables[0], "");
            dsFromNaveoOne.Merge(dt);

            return dsFromNaveoOne;
        }
    }
}
