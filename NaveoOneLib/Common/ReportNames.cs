﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NaveoOneLib.Common
{
    public class ReportNames
    {
        public enum Report
        {
            Exception = 1,
            TripHistory = 2,
            Auxillaries = 3,
            ZoneVisitedNotVisited = 4,

            EmailToSMSNotification = 10,
            PopUpNotification = 11,
            Email_Notification = 12,
            PopUpWithSoundNotification = 13
        }
    }
}
