﻿//using NaveoOneLib.Models.ImportTemplates;
//using OfficeOpenXml;
//using System;
//using System.Collections.Generic;
//using System.Data;
//using System.IO;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using NaveoOneLib.Constants;
//using System.Web;
//using System.Drawing;
//using NaveoOneLib.Models.Common;
//using NaveoOneLib.Models.Permissions;
//using OfficeOpenXml.Style;
//using NaveoOneLib.Services.Trips;

//namespace NaveoOneLib.Common
//{
//    //UserTemplateRow E:\Reza\Geo\SDK\NaveoOneWebV2\NaveoWebApi\Models\ImportTemplates\*.* to delete
//    public static class ExportData
//    {
//        /// <summary>
//        /// Generate excel stream based on entity
//        /// </summary>
//        /// <param name="entity"></param>
//        /// <returns></returns>
//        public static MemoryStream GenerateExcelStreamToExport(Entity entity, SecurityToken securityToken)  //MemoryStream FileStream
//        {
//            string templateDocument = string.Empty;
//            int i = 0;
//            int row = 2; // First excel row

//            // Results Output
//            MemoryStream output = new MemoryStream();
//            //String sFN = HttpContext.Current.Server.MapPath("~/Resources/Template/WorkingFolder/TMP") + Guid.NewGuid().ToString();
//            //FileStream output = new FileStream(sFN, FileMode.OpenOrCreate);
//            try
//            {
//                switch (entity.entityName)
//                {
//                    case NaveoEntity.USERS:
//                        #region User excel export test
//                        templateDocument = HttpContext.Current.Server.MapPath("~/Resources/Template/Export/Excel/User_Export_Template.xlsx");
//                        // Open Template
//                        using (FileStream excelTemplate = File.OpenRead(templateDocument))
//                        {
//                            // Results Output
//                            //MemoryStream output = new MemoryStream();
//                            // Create Excel EPPlus Package based on template stream
//                            using (ExcelPackage package = new ExcelPackage(excelTemplate))
//                            {
//                                // Grab the 1st sheet with the template.
//                                ExcelWorksheet sheet = package.Workbook.Worksheets.First();

//                                #region Data to export to excel
//                                List<UserTemplateRow> userRows = new List<UserTemplateRow>
//                       {
//                            new UserTemplateRow { UID = 1, Names = "John", Email = "john@naveo.mu" },
//                            new UserTemplateRow { UID = 2, Names = "James", Email = "james@naveo.mu" },
//                            new UserTemplateRow { UID = 3, Names = "Mark", Email = "mark@naveo.mu" }
//                        };
//                                #endregion
//                                // Insert data into template
//                                if (sheet != null)
//                                {
//                                    int listIndex = 0;
//                                    int listCount = userRows.Count;
//                                    for (int column = 1; column <= listCount; column++)
//                                    {
//                                        sheet.Cells[row, column].Value = userRows[listIndex].UID;
//                                        sheet.Cells[row, column].Value = userRows[listIndex].Names;
//                                        sheet.Cells[row, column].Value = userRows[listIndex].Email;
//                                        row++; listIndex++;
//                                    }
//                                }
//                                package.SaveAs(output);
//                            }
//                            return output;
//                        }
//                    #endregion

//                    case NaveoEntity.TRIPS:
//                        #region Trip excel export
//                        //var trips = new TripController().Trips(entity.trip, Request.Headers);
//                        templateDocument = HttpContext.Current.Server.MapPath("~/Resources/Template/Export/Excel/Trip_Export_Template.xlsx");
//                        NaveoService.Models.TripHeader tripHeader = new TripService().GetTripHeaders(securityToken.UserToken, entity.trip.assetIds, entity.trip.dateFrom, entity.trip.dateTo, entity.trip.byDriver, entity.trip.iMinTripDistance, securityToken.sConnStr, securityToken.Page, securityToken.LimitPerPage);

//                        //var obj = tripHeader.ToTripDTO(securityToken);
//                        //var test = obj.data.trips;
//                        var trips = tripHeader.dtTrips;

//                        using (FileStream excelTemplate = File.OpenRead(templateDocument))
//                        {
//                            /// TODO: [Aboo] Remove bellow comment concerning test
//                            // Use for excel filling test 
//                            // using (ExcelPackage package = new ExcelPackage(fileinfo))

//                            // Create Excel EPPlus Package based on template stream
//                            using (ExcelPackage package = new ExcelPackage(excelTemplate))
//                            {
//                                // Grab the 1st sheet with the template.
//                                ExcelWorksheet sheet = package.Workbook.Worksheets.First();
//                                // Insert data into template
//                                if (sheet != null)
//                                {
//                                    int listCount = trips.Rows.Count;
//                                    TimeSpan totalTripTime = new TimeSpan();
//                                    Double totalTripDistance = 0;
//                                    TimeSpan totalStopTime = new TimeSpan();
//                                    TimeSpan totalIdleTime = new TimeSpan();
//                                    TimeSpan totaloverSpeed1Time = new TimeSpan();
//                                    TimeSpan totaloverSpeed2Time = new TimeSpan();
//                                    Double totalMaxSpeed = 0;
//                                    Double totalAcceleration = 0;
//                                    Double totalBrake = 0;
//                                    Double totalCorner = 0;
//                                    string tripTime = "";
//                                    string tripDistance = "";
//                                    string stopTime = "";
//                                    string idleTime = "";
//                                    string maxSpeed = "";
//                                    string acceleration = "";
//                                    string brake = "";
//                                    string corner = "";
//                                    string overSpeed1Time = "";
//                                    string overSpeed2Time = "";

//                                    foreach (DataRow dataRow in trips.Rows)
//                                    {
//                                        try
//                                        {
//                                            sheet.Cells[row, (int)TripsXlsxColumns.TripId].Value = dataRow["iID"];
//                                            sheet.Cells[row, (int)TripsXlsxColumns.VehicleId].Value = dataRow["AssetID"];
//                                            sheet.Cells[row, (int)TripsXlsxColumns.VehicleName].Value = dataRow["Vehicle"];
//                                            sheet.Cells[row, (int)TripsXlsxColumns.driverId].Value = dataRow["DriverID"];

//                                            if (!dataRow["Driver"].ToString().Equals("Default Driver"))
//                                            {
//                                                sheet.Column((int)TripsXlsxColumns.DriverName).Hidden = false;
//                                                sheet.Cells[row, (int)TripsXlsxColumns.DriverName].Value = dataRow["Driver"].ToString();
//                                            }

//                                            tripTime = dataRow["TripTime"].ToString();
//                                            tripDistance = dataRow["TripDistance"].ToString();
//                                            stopTime = dataRow["StopTime"].ToString();
//                                            sheet.Cells[row, (int)TripsXlsxColumns.StartDate].Value = dataRow["From"].ToString();
//                                            sheet.Cells[row, (int)TripsXlsxColumns.TripTime].Value = tripTime;
//                                            sheet.Cells[row, (int)TripsXlsxColumns.EndDate].Value = dataRow["To"].ToString();
//                                            sheet.Cells[row, (int)TripsXlsxColumns.TripDistance].Value = tripDistance;
//                                            sheet.Cells[row, (int)TripsXlsxColumns.StopTime].Value = stopTime;
//                                            sheet.Cells[row, (int)TripsXlsxColumns.DepartureAddress].Value = dataRow["DepartureAddress"];
//                                            sheet.Cells[row, (int)TripsXlsxColumns.ArrivalAddress].Value = dataRow["ArrivalAddress"];

//                                            if (int.TryParse(dataRow["iColorDZ"].ToString(), out i))
//                                            {
//                                                sheet.Cells[row, (int)TripsXlsxColumns.DepartureZone].Style.Fill.PatternType = ExcelFillStyle.Solid;
//                                                sheet.Cells[row, (int)TripsXlsxColumns.DepartureZone].Style.Fill.BackgroundColor.SetColor(ConvertToColor(i));
//                                                sheet.Cells[row, (int)TripsXlsxColumns.DepartureZone].Value = dataRow["DepartureZone"];
//                                            }

//                                            if (int.TryParse(dataRow["iColor"].ToString(), out i))
//                                            {
//                                                sheet.Cells[row, (int)TripsXlsxColumns.ArrivalZone].Style.Fill.PatternType = ExcelFillStyle.Solid;
//                                                sheet.Cells[row, (int)TripsXlsxColumns.ArrivalZone].Style.Fill.BackgroundColor.SetColor(ConvertToColor(i));
//                                                sheet.Cells[row, (int)TripsXlsxColumns.ArrivalZone].Value = dataRow["ArrivalZone"];
//                                            }

//                                            idleTime = dataRow["IdlingTime"].ToString();
//                                            maxSpeed = dataRow["MaxSpeed"].ToString();
//                                            acceleration = dataRow["Accel"].ToString();
//                                            brake = dataRow["Brake"].ToString();
//                                            corner = dataRow["Corner"].ToString();
//                                            overSpeed1Time = dataRow["OverSpeed1Time"].ToString();
//                                            overSpeed2Time = dataRow["OverSpeed1Time"].ToString();
//                                            sheet.Cells[row, (int)TripsXlsxColumns.IdleTime].Value = idleTime;
//                                            sheet.Cells[row, (int)TripsXlsxColumns.MaximumSpeed].Value = maxSpeed;
//                                            sheet.Cells[row, (int)TripsXlsxColumns.Acceleration].Value = acceleration;
//                                            sheet.Cells[row, (int)TripsXlsxColumns.Brake].Value = brake;
//                                            sheet.Cells[row, (int)TripsXlsxColumns.Corner].Value = corner;
//                                            sheet.Cells[row, (int)TripsXlsxColumns.SpeedingTime1].Value = overSpeed1Time;
//                                            sheet.Cells[row, (int)TripsXlsxColumns.SpeedingTime2].Value = overSpeed2Time;

//                                            totalTripDistance = totalTripDistance + Convert.ToDouble(tripDistance);
//                                            TimeSpan ts = new TimeSpan();
//                                            Double d = 0.0;
//                                            if (TimeSpan.TryParse(tripTime, out ts))
//                                                totalTripTime = totalTripTime.Add(ts);
//                                            if (TimeSpan.TryParse(stopTime, out ts))
//                                                totalStopTime = totalStopTime.Add(ts);
//                                            if (TimeSpan.TryParse(idleTime, out ts))
//                                                totalIdleTime = totalIdleTime.Add(ts);
//                                            if (Double.TryParse(maxSpeed, out d))
//                                                if (d > totalMaxSpeed)
//                                                    totalMaxSpeed = d;
//                                            if (Double.TryParse(acceleration, out d))
//                                                totalAcceleration += d;
//                                            if (Double.TryParse(brake, out d))
//                                                totalBrake += d;
//                                            if (Double.TryParse(corner, out d))
//                                                totalCorner += d;
//                                            if (TimeSpan.TryParse(overSpeed1Time, out ts))
//                                                totaloverSpeed1Time = totaloverSpeed1Time.Add(ts);
//                                            if (TimeSpan.TryParse(overSpeed2Time, out ts))
//                                                totaloverSpeed2Time = totaloverSpeed1Time.Add(ts);
//                                            row++;
//                                        }
//                                        catch { }
//                                    }

//                                    //DataRow drTotal = tripHeader.dtTotals.Rows[0];
//                                    // Adding style to border for sum value
//                                    var borderStyle = ExcelBorderStyle.Thick;
//                                    sheet.Cells[row, (int)TripsXlsxColumns.TripTime].Style.Border.Top.Style = borderStyle;
//                                    sheet.Cells[row, (int)TripsXlsxColumns.TripTime].Style.Border.Bottom.Style = borderStyle;
//                                    sheet.Cells[row, (int)TripsXlsxColumns.TripDistance].Style.Border.Top.Style = borderStyle;
//                                    sheet.Cells[row, (int)TripsXlsxColumns.TripDistance].Style.Border.Bottom.Style = borderStyle;
//                                    sheet.Cells[row, (int)TripsXlsxColumns.StopTime].Style.Border.Top.Style = borderStyle;
//                                    sheet.Cells[row, (int)TripsXlsxColumns.StopTime].Style.Border.Bottom.Style = borderStyle;
//                                    sheet.Cells[row, (int)TripsXlsxColumns.IdleTime].Style.Border.Top.Style = borderStyle;
//                                    sheet.Cells[row, (int)TripsXlsxColumns.IdleTime].Style.Border.Bottom.Style = borderStyle;
//                                    sheet.Cells[row, (int)TripsXlsxColumns.MaximumSpeed].Style.Border.Top.Style = borderStyle;
//                                    sheet.Cells[row, (int)TripsXlsxColumns.MaximumSpeed].Style.Border.Bottom.Style = borderStyle;
//                                    sheet.Cells[row, (int)TripsXlsxColumns.Acceleration].Style.Border.Top.Style = borderStyle;
//                                    sheet.Cells[row, (int)TripsXlsxColumns.Acceleration].Style.Border.Bottom.Style = borderStyle;
//                                    sheet.Cells[row, (int)TripsXlsxColumns.Brake].Style.Border.Top.Style = borderStyle;
//                                    sheet.Cells[row, (int)TripsXlsxColumns.Brake].Style.Border.Bottom.Style = borderStyle;
//                                    sheet.Cells[row, (int)TripsXlsxColumns.Corner].Style.Border.Top.Style = borderStyle;
//                                    sheet.Cells[row, (int)TripsXlsxColumns.Corner].Style.Border.Bottom.Style = borderStyle;
//                                    sheet.Cells[row, (int)TripsXlsxColumns.SpeedingTime1].Style.Border.Top.Style = borderStyle;
//                                    sheet.Cells[row, (int)TripsXlsxColumns.SpeedingTime1].Style.Border.Bottom.Style = borderStyle;
//                                    sheet.Cells[row, (int)TripsXlsxColumns.SpeedingTime2].Style.Border.Top.Style = borderStyle;
//                                    sheet.Cells[row, (int)TripsXlsxColumns.SpeedingTime2].Style.Border.Bottom.Style = borderStyle;

//                                    sheet.Cells[row, (int)TripsXlsxColumns.TripTime].Value = totalTripTime.ToString();
//                                    sheet.Cells[row, (int)TripsXlsxColumns.TripDistance].Value = totalTripDistance.ToString();
//                                    sheet.Cells[row, (int)TripsXlsxColumns.StopTime].Value = totalStopTime.ToString();
//                                    sheet.Cells[row, (int)TripsXlsxColumns.IdleTime].Value = totalIdleTime.ToString();
//                                    sheet.Cells[row, (int)TripsXlsxColumns.MaximumSpeed].Value = totalMaxSpeed.ToString();
//                                    sheet.Cells[row, (int)TripsXlsxColumns.Acceleration].Value = totalAcceleration.ToString();
//                                    sheet.Cells[row, (int)TripsXlsxColumns.Brake].Value = totalBrake.ToString();
//                                    sheet.Cells[row, (int)TripsXlsxColumns.Corner].Value = totalCorner.ToString();
//                                    sheet.Cells[row, (int)TripsXlsxColumns.SpeedingTime1].Value = totaloverSpeed1Time.ToString();
//                                    sheet.Cells[row, (int)TripsXlsxColumns.SpeedingTime2].Value = totaloverSpeed2Time.ToString();

//                                    //sheet.Cells[row+2, (int)TripsXlsxColumns.TripTime].Value = drTotal["totalTripTime"].ToString();
//                                    //sheet.Cells[row+2, (int)TripsXlsxColumns.MaximumSpeed].Value = drTotal["maxSpeed"].ToString();
//                                    //sheet.Cells[row+2, (int)TripsXlsxColumns.TripDistance].Value = drTotal["totalTripDistance"].ToString();
//                                    //sheet.Cells[row+2, (int)TripsXlsxColumns.StopTime].Value = drTotal["totalStopTime"].ToString();
//                                    //sheet.Cells[row+2, (int)TripsXlsxColumns.IdleTime].Value = drTotal["totalIdlingTime"].ToString();
//                                }

//                                // Used for test 
//                                //TODO:Remove test codes
//                                if (String.IsNullOrEmpty("DDD"))
//                                {
//                                    //FileInfo fileinfo = new FileInfo(@"C:\Users\Reza\Desktop\Trip_Export_Template.xlsx");
//                                    FileInfo fileinfo = new FileInfo(@"C:\Users\Saahir\Desktop\Trip_Export_Template.xlsx");
//                                    package.SaveAs(fileinfo);
//                                }
//                                else
//                                    package.SaveAs(output);
//                            }
//                            return output;
//                        }
//                    #endregion

//                    case NaveoEntity.LIVE:
//                        #region Live excel export
//                        //var trips = new TripController().Trips(entity.trip, Request.Headers);
//                        try
//                        {
//                            templateDocument = HttpContext.Current.Server.MapPath("~/Resources/Template/Export/Excel/Live_Export_Template.xlsx");
//                            List<GPSData> liveData = new LiveService().GetLive(securityToken.UserToken, entity.trip.assetIds, securityToken.sConnStr);

//                            using (FileStream excelTemplate = File.OpenRead(templateDocument))
//                            {
//                                // Results Output
//                                //MemoryStream output = new MemoryStream();

//                                // Create Excel EPPlus Package based on template stream
//                                using (ExcelPackage package = new ExcelPackage(excelTemplate))
//                                {
//                                    // Grab the 1st sheet with the template.
//                                    ExcelWorksheet sheet = package.Workbook.Worksheets.First();
//                                    // Insert data into template
//                                    if (sheet != null)
//                                    {
//                                        //for (int column = 0; column < listCount; column++)
//                                        foreach (GPSData item in liveData)
//                                        {
//                                            sheet.Cells[row, (int)LiveDataXlsxColumns.UID].Value = item.UID;
//                                            sheet.Cells[row, (int)LiveDataXlsxColumns.AssetID].Value = item.AssetID;
//                                            sheet.Cells[row, (int)LiveDataXlsxColumns.AssetNum].Value = item.AssetNum;
//                                            sheet.Cells[row, (int)LiveDataXlsxColumns.DriverId].Value = item.DriverID;
//                                            //if (trips.AsEnumerable().Select(r => r["Driver"]).Distinct().Contains("Default"))
//                                            //{
//                                            //    sheet.Row((int)LiveDataXlsxColumns.DriverName).Hidden = true;
//                                            //};
//                                            //if (!item["Driver"].ToString().Equals("Default Driver"))
//                                            //{
//                                            //    sheet.Column((int)LiveDataXlsxColumns.DriverName).Hidden = false;
//                                            //    sheet.Cells[row, (int)LiveDataXlsxColumns.DriverName].Value = item["Driver"].ToString();
//                                            //}

//                                            sheet.Cells[row, (int)LiveDataXlsxColumns.Availability].Value = item.Availability;
//                                            sheet.Cells[row, (int)LiveDataXlsxColumns.Latitude].Value = item.Latitude;
//                                            sheet.Cells[row, (int)LiveDataXlsxColumns.Longitude].Value = item.Longitude;
//                                            sheet.Cells[row, (int)LiveDataXlsxColumns.RoadSpeed].Value = item.RoadSpeed;
//                                            sheet.Cells[row, (int)LiveDataXlsxColumns.Speed].Value = item.Speed;
//                                            sheet.Cells[row, (int)LiveDataXlsxColumns.TripDistance].Value = item.TripDistance;
//                                            sheet.Cells[row, (int)LiveDataXlsxColumns.TripTime].Value = item.TripTime;
//                                            // TODO: [Aboo] Confirm with Reza Requirements
//                                            //sheet.Cells[row, (int)LiveDataXlsxColumns.GPSDetailID].Value = item.GPSDataDetails.gps;
//                                            //sheet.Cells[row, (int)LiveDataXlsxColumns.Remarks].Value = item;
//                                            //sheet.Cells[row, (int)LiveDataXlsxColumns.TypeID].Value = item;
//                                            //sheet.Cells[row, (int)LiveDataXlsxColumns.TypeValue].Value = item;
//                                            //sheet.Cells[row, (int)LiveDataXlsxColumns.UOM].Value = item;
//                                            sheet.Cells[row, (int)LiveDataXlsxColumns.WorkHour].Value = item.WorkHour;
//                                            sheet.Cells[row, (int)LiveDataXlsxColumns.StopFlag].Value = item.StopFlag;
//                                            sheet.Cells[row, (int)LiveDataXlsxColumns.DeviceID].Value = item.DeviceID;
//                                            sheet.Cells[row, (int)LiveDataXlsxColumns.DirectionImage].Value = item.DirectionImage;
//                                            sheet.Cells[row, (int)LiveDataXlsxColumns.EngineOn].Value = item.EngineOn;
//                                            sheet.Cells[row, (int)LiveDataXlsxColumns.iButton].Value = item.iButton;
//                                            sheet.Cells[row, (int)LiveDataXlsxColumns.LastReportedInLocalTime].Value = item.LastReportedInLocalTime;

//                                            //if (int.TryParse(item["iColorDZ"].ToString(), out i))
//                                            //{
//                                            //    sheet.Cells[row, (int)LiveDataXlsxColumns.DepartureZone].Style.Fill.PatternType = ExcelFillStyle.Solid;
//                                            //    sheet.Cells[row, (int)LiveDataXlsxColumns.DepartureZone].Style.Fill.BackgroundColor.SetColor(ConvertToColor(i));
//                                            //    sheet.Cells[row, (int)LiveDataXlsxColumns.DepartureZone].Value = item["DepartureZone"];
//                                            //}

//                                            //if (int.TryParse(item["iColor"].ToString(), out i))
//                                            //{
//                                            //    sheet.Cells[row, (int)LiveDataXlsxColumns.ArrivalZone].Style.Fill.PatternType = ExcelFillStyle.Solid;
//                                            //    sheet.Cells[row, (int)LiveDataXlsxColumns.ArrivalZone].Style.Fill.BackgroundColor.SetColor(ConvertToColor(i));
//                                            //    sheet.Cells[row, (int)LiveDataXlsxColumns.ArrivalZone].Value = item["ArrivalZone"];
//                                            //}
//                                            row++;
//                                        }
//                                    }

//                                    // Used for test 
//                                    //TODO:Remove test codes
//                                    if (String.IsNullOrEmpty("DDD"))
//                                    {
//                                        //FileInfo fileinfo = new FileInfo(@"C:\Users\Reza\Desktop\Live_Export_Template.xlsx");
//                                        FileInfo fileinfo = new FileInfo(@"C:\Users\Saahir\Desktop\Live_Export_Template.xlsx");
//                                        package.SaveAs(fileinfo);
//                                    }
//                                    else
//                                        package.SaveAs(output);
//                                }
//                                return output;
//                            }
//                        }
//                        catch (Exception e)
//                        {
//                            return null;
//                        }
//                    #endregion

//                    case NaveoEntity.DAILY_SUMMARIZED_TRIPS:
//                        #region DAILYSUMMARIZEDTRIPS

//                        try
//                        {
//                            templateDocument = HttpContext.Current.Server.MapPath("~/Resources/Template/Export/Excel/Daily_Summarized_Trips.xlsx");

//                            dtData dtGetSummarizedTrips = new TripService().GetDailySummarizedTrips(securityToken.UserToken, entity.trip.assetIds, entity.trip.dateFrom, entity.trip.dateTo, securityToken.sConnStr, securityToken.Page, securityToken.LimitPerPage);

//                            using (FileStream excelTemplate = File.OpenRead(templateDocument))
//                            {
//                                // Results Output
//                                //MemoryStream output = new MemoryStream();

//                                // Create Excel EPPlus Package based on template stream
//                                using (ExcelPackage package = new ExcelPackage(excelTemplate))
//                                {
//                                    // Grab the 1st sheet with the template.
//                                    ExcelWorksheet sheet = package.Workbook.Worksheets.First();
//                                    // Insert data into template
//                                    if (sheet != null)
//                                    {
//                                        //for (int column = 0; column < listCount; column++)
//                                        foreach (DataRow dr in dtGetSummarizedTrips.Data.Rows)
//                                        {
//                                            sheet.Cells[row, (int)GetDailySummarizedTripsXlsxColumns.date].Value = dr["date"].ToString();
//                                            sheet.Cells[row, (int)GetDailySummarizedTripsXlsxColumns.registrationNo].Value = dr["registrationNo"].ToString();
//                                            sheet.Cells[row, (int)GetDailySummarizedTripsXlsxColumns.driver].Value = dr["driver"].ToString();
//                                            sheet.Cells[row, (int)GetDailySummarizedTripsXlsxColumns.trips].Value = dr["trips"].ToString();
//                                            sheet.Cells[row, (int)GetDailySummarizedTripsXlsxColumns.startTime].Value = dr["startTime"].ToString();
//                                            sheet.Cells[row, (int)GetDailySummarizedTripsXlsxColumns.endTime].Value = dr["endTime"].ToString();
//                                            sheet.Cells[row, (int)GetDailySummarizedTripsXlsxColumns.kmTravelled].Value = dr["kmTravelled"].ToString();
//                                            sheet.Cells[row, (int)GetDailySummarizedTripsXlsxColumns.idlingTime].Value = dr["idlingTime"].ToString();
//                                            sheet.Cells[row, (int)GetDailySummarizedTripsXlsxColumns.drivingTime].Value = dr["drivingTime"].ToString();
//                                            sheet.Cells[row, (int)GetDailySummarizedTripsXlsxColumns.inZoneStopTime].Value = dr["inZoneStopTime"].ToString();
//                                            sheet.Cells[row, (int)GetDailySummarizedTripsXlsxColumns.outZoneStopTime].Value = dr["outZoneStopTime"].ToString();

//                                        }

//                                    }

//                                    // Used for test 
//                                    //TODO:Remove test codes
//                                    if (String.IsNullOrEmpty("DDD"))
//                                    {
//                                        FileInfo fileinfo = new FileInfo(@"C:\Users\Dell\Desktop\Daily_Summarized_Trips.xlsx");
//                                        //FileInfo fileinfo = new FileInfo(@"C:\Users\Saahir\Desktop\Live_Export_Template.xlsx");
//                                        package.SaveAs(fileinfo);
//                                    }
//                                    else
//                                        package.SaveAs(output);
//                                }
//                                return output;
//                            }
//                        }
//                        catch (Exception e)
//                        {
//                            return null;
//                        }
//                    #endregion

//                    case NaveoEntity.SUMMARIZED_TRIPS:
//                        #region SUMMARIZEDTRIPS

//                        try
//                        {
//                            templateDocument = HttpContext.Current.Server.MapPath("~/Resources/Template/Export/Excel/Summarized_Trips.xlsx");

//                            dtData dtGetSummarizedTrips = new TripService().GetSummarizedTrips(securityToken.UserToken, entity.trip.assetIds, entity.trip.dateFrom, entity.trip.dateTo, securityToken.sConnStr, securityToken.Page, securityToken.LimitPerPage);

//                            using (FileStream excelTemplate = File.OpenRead(templateDocument))
//                            {
//                                // Results Output
//                                //MemoryStream output = new MemoryStream();

//                                // Create Excel EPPlus Package based on template stream
//                                using (ExcelPackage package = new ExcelPackage(excelTemplate))
//                                {
//                                    // Grab the 1st sheet with the template.
//                                    ExcelWorksheet sheet = package.Workbook.Worksheets.First();
//                                    // Insert data into template
//                                    if (sheet != null)
//                                    {
//                                        //for (int column = 0; column < listCount; column++)
//                                        foreach (DataRow dr in dtGetSummarizedTrips.Data.Rows)
//                                        {
//                                            sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.registrationNo].Value = dr["registrationNo"].ToString();
//                                            sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.driver].Value = dr["driver"].ToString();
//                                            sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.kmTravelled].Value = dr["kmTravelled"].ToString();
//                                            sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.stopLess15min].Value = dr["stopLess15min"].ToString();
//                                            sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.stopLess30min].Value = dr["stopLess30min"].ToString();
//                                            sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.stopLess60min].Value = dr["stopLess60min"].ToString();
//                                            sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.stopLess5Hrs].Value = dr["stopLess5Hrs"].ToString();
//                                            sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.stopMore5Hrs].Value = dr["stopMore5Hrs"].ToString();
//                                            sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.trips].Value = dr["trips"].ToString();
//                                            sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.drivingTime].Value = dr["drivingTime"].ToString();
//                                            sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.idlingTime].Value = dr["idlingTime"].ToString();
//                                            sheet.Cells[row, (int)GetSummarizedTripsXlsxColumns.stopMore70Kms].Value = dr["stopMore70Kms"].ToString();

//                                            row++;

//                                        }

//                                    }

//                                    // Used for test 
//                                    //TODO:Remove test codes
//                                    if (String.IsNullOrEmpty("DDD"))
//                                    {
//                                        FileInfo fileinfo = new FileInfo(@"C:\Users\Dell\Desktop\Summarized_Trips.xlsx");
//                                        //FileInfo fileinfo = new FileInfo(@"C:\Users\Saahir\Desktop\Live_Export_Template.xlsx");
//                                        package.SaveAs(fileinfo);
//                                    }
//                                    else
//                                        package.SaveAs(output);
//                                }
//                                return output;
//                            }
//                        }
//                        catch (Exception e)
//                        {
//                            return null;
//                        }
//                    #endregion

//                    case NaveoEntity.DAILY_TRIP_TIME:
//                        #region DAILYTRIPTIME
//                        try
//                        {
//                            templateDocument = HttpContext.Current.Server.MapPath("~/Resources/Template/Export/Excel/Daily_Trip_Time.xlsx");

//                            dtData dtGetSummarizedTrips = new TripService().GetDailyTripTime(securityToken.UserToken, entity.trip.assetIds, entity.trip.dateFrom, entity.trip.dateTo, entity.trip.byDriver, securityToken.sConnStr, securityToken.Page, securityToken.LimitPerPage);

//                            using (FileStream excelTemplate = File.OpenRead(templateDocument))
//                            {

//                                int countColumn = 0;
//                                // Create Excel EPPlus Package based on template stream
//                                using (ExcelPackage package = new ExcelPackage(excelTemplate))
//                                {
//                                    // Grab the 1st sheet with the template.
//                                    ExcelWorksheet sheet = package.Workbook.Worksheets.First();
//                                    // Insert data into template
//                                    if (sheet != null)
//                                    {


//                                        foreach (DataRow dr in dtGetSummarizedTrips.Data.Rows)
//                                        {

//                                            sheet.Cells[row, (int)GetDailyTripTimeXlsxColumns.sDesc].Value = dr["sDesc"].ToString();

//                                            foreach (DataColumn dc in dtGetSummarizedTrips.Data.Columns)
//                                            {
//                                                countColumn++;
//                                                if (countColumn >= 3)
//                                                {
//                                                    if (dc.ColumnName.Trim().ToLower() != "rownum")
//                                                    {
//                                                        if (dc.ColumnName.Trim().ToLower() != "sdesc")
//                                                        {
//                                                            string colData = dc.ColumnName.ToString();
//                                                            sheet.Cells[1, countColumn].Value = colData;
//                                                            sheet.Cells[row, countColumn].Value = dr[colData].ToString();
//                                                        }
//                                                    }
//                                                }
//                                            }
//                                            row++;
//                                        }
//                                    }

//                                    // Used for test 
//                                    //TODO:Remove test codes
//                                    if (String.IsNullOrEmpty("DDD"))
//                                    {
//                                        FileInfo fileinfo = new FileInfo(@"C:\Users\Dell\Desktop\Daily_Trip_Times.xlsx");
//                                        package.SaveAs(fileinfo);
//                                    }
//                                    else
//                                        package.SaveAs(output);
//                                }
//                                return output;
//                            }
//                        }
//                        catch (Exception e)
//                        {
//                            return null;
//                        }
//                    #endregion

//                    default:
//                        return null;
//                }
//            }
//            catch (Exception ex)
//            {
//                System.IO.File.AppendAllText(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "\\Registers\\err.dat", "\r\n Export \r\n" + ex.Message + "\r\n" + ex.InnerException);
//                return null;
//            }
//            finally
//            {
//                //File.Delete(sFN);
//            }

//        }

//        private static Color ConvertToColor(int i)
//        {
//            color = CommonService.HexConverter(Color.FromArgb(i));
//            return ColorTranslator.FromHtml(color);
//        }

//    }
//}
