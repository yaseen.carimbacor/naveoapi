﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoOneLib.Common
{
    public class N1Controller
    {
        public List<string> ListModuleName()
        {
            List<string> lModuleName = new List<string>();
            ModuleName mN = new ModuleName();
            lModuleName.Add(mN.Admin);
            lModuleName.Add(mN.Fleet);
            lModuleName.Add(mN.GIS);
            lModuleName.Add(mN.Maintenance);

            return lModuleName;
        }

        public List<string> listControllerName()
        {
            List<string> lControllerName = new List<string>();
            ControllerName cN = new ControllerName();
            lControllerName.Add(cN.AssetController);
            lControllerName.Add(cN.ExceptionController);
            lControllerName.Add(cN.DashBoardController);
            lControllerName.Add(cN.LiveController);
            lControllerName.Add(cN.FuelController);
            lControllerName.Add(cN.TripController);
            lControllerName.Add(cN.PlanningController);
            lControllerName.Add(cN.ScheduleController);
            lControllerName.Add(cN.ZoneController);
            lControllerName.Add(cN.RulesController);
            lControllerName.Add(cN.UserController);
            lControllerName.Add(cN.ResourceController);
            lControllerName.Add(cN.DriverController);
            lControllerName.Add(cN.DashBoardController);
            lControllerName.Add(cN.TemperatureController);
            lControllerName.Add(cN.MaintenanceController);
            lControllerName.Add(cN.RoleController);
            lControllerName.Add(cN.GroupMatrixController);
            lControllerName.Add(cN.GPSDataController);
            lControllerName.Add(cN.AuditController);
            lControllerName.Add(cN.BlupController);
            lControllerName.Add(cN.FixedAssetController);
            lControllerName.Add(cN.TerraPlanningController);
            lControllerName.Add(cN.CustomizedTripsController);
            lControllerName.Add(cN.MokaExtendedTrips);
            lControllerName.Add(cN.Monitoring);
            lControllerName.Add(cN.RossterController);
            lControllerName.Add(cN.ShiftController);
            lControllerName.Add(cN.AccidentManagementController);
            lControllerName.Add(cN.GlobalParams);
            lControllerName.Add(cN.DriverScoreCards);
            lControllerName.Add(cN.Error);
            lControllerName.Add(cN.ZoneType);
            lControllerName.Add(cN.DriverVehicle);
            lControllerName.Add(cN.LookupTypeValues);

            return lControllerName;
        }

        public List<Naveo_Report> lNaveoReport = new List<Naveo_Report>();
    }

    public class ControllerName
    {
        public string AssetController = "Asset";
        public string ExceptionController = "Exception";
        public string DashBoardController = "Dashboard";
        public string LiveController = "Live";
        public string FuelController = "Fuel";
        public string TripController = "Trip";
        public string PlanningController = "Planning";
        public string ScheduleController = "Schedule";
        public string ZoneController = "Zones";
        public string RulesController = "Rules";
        public string UserController = "User";
        public string ResourceController = "Resource";
        public string DriverController = "Driver";
        public string TemperatureController = "Temperature";
        public string MaintenanceController = "Maintenance";
        public string RoleController = "Role";
        public string GroupMatrixController = "GroupMatrix";
        public string GPSDataController = "GPSData";
        public string RossterController = "Roster";
        public string AuditController = "Audit";
        public string BlupController = "Blup";
        public string FixedAssetController = "FixedAsset";
        public string TerraPlanningController = "TerraPlanning";
        public string CustomizedTripsController = "CustomizedTrips";
        public string MokaExtendedTrips = "MOLGExtendedTrips";
        public string Monitoring = "Monitoring";
        public string AccidentManagementController = "AccidentManagement";
        public string ShiftController = "Shift";
        public string GlobalParams = "GlobalParams";
        public string DriverScoreCards = "DriverScoreCards";
        public string Error = "Error";
        public string ZoneType = "ZoneType";
        public string DriverVehicle = "DriverVehicle";
        public string LookupTypeValues = "LookupTypeValues";

    }


    public class ModuleName
    {
        public string Admin = "Admin";
        public string Fleet = "Fleet";
        public string GIS = "GIS";
        public string Maintenance = "Maintenance";
    }

    public class Naveo_Report
    {

        public string ControllerName { get; set; }

        public report Report1 { get; set; }
        public report Report2 { get; set; }
        public report Report3 { get; set; } 
        public report Report4 { get; set; } 


    }

    public class report
    {
        public string DisplayName { get; set; }

        public string ReportName { get; set; }


    }




    #region Alert on planning enum
    public enum AlertPlanningConstant
    {
        EarlyArrival = 0,
        EarlyDeparture = 1,
        LateArrival = 2,
        LateDeparture = 3,
        OnTime = 4,
        NotVisited = 5,

    }

    #endregion

}
