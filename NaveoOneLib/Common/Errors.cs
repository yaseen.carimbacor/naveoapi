﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

namespace NaveoOneLib.Common
{
    public class Errors
    {

        public void ShowExceptions(Exception str)
        {
            MessageBox.Show(str.ToString());
        }

        public void ShowMsg(String str)
        {
            MessageBox.Show(str.ToString());
        }

        public bool ShowConfimation(String str)
        {
            if (MessageBox.Show(str, str, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                return true;
            else
                return false;
        }

        //*************************************************************

        //NAME:          WriteToEventLog

        //PURPOSE:       Write to Event Log

        //PARAMETERS:    Entry - Value to Write

        //               AppName - Name of Client Application. Needed

        //               because before writing to event log, you must

        //               have a named EventLog source.

        //               EventType - Entry Type, from EventLogEntryType

        //               Structure e.g., EventLogEntryType.Warning,

        //               EventLogEntryType.Error

        //               LogName: Name of Log (System, Application;

        //               Security is read-only) If you

        //               specify a non-existent log, the log will be

        //               created

        //RETURNS:       True if successful

        //*************************************************************
        public static bool WriteToEventLog(string entry, string appName, EventLogEntryType eventType, string logName)
        {
            EventLog objEventLog = new EventLog();
            try
            {
                if (!(EventLog.SourceExists(appName)))
                {
                    EventLog.CreateEventSource(appName, logName);
                }
                objEventLog.Source = appName;
                objEventLog.ModifyOverflowPolicy(OverflowAction.OverwriteAsNeeded, 0);
                objEventLog.WriteEntry(entry, eventType);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        // *************************************************************

        //NAME:          WriteToErrorLog

        //PURPOSE:       Open or create an error log and submit error message

        //PARAMETERS:    msg - message to be written to error file

        //               stkTrace - stack trace from error message

        //               title - title of the error file entry

        //RETURNS:       Nothing

        //*************************************************************
        public static void WriteToErrorLog(string msg, string stkTrace, string title, string app_path)
        {
            if (!(System.IO.Directory.Exists(app_path + "\\Errors\\")))
                System.IO.Directory.CreateDirectory(app_path + "\\Errors\\");

            FileStream fs = new FileStream(app_path + "\\Errors\\errlog.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite);
            StreamWriter s = new StreamWriter(fs);
            s.Close();
            fs.Close();

            try
            {
                FileInfo f = new FileInfo(app_path + "\\Errors\\errlog.txt");
                if (f.Length > 10000)
                    File.Delete(app_path + "\\Errors\\errlog.txt");
            }
            catch { }

            FileStream fs1 = new FileStream(app_path + "\\Errors\\errlog.txt", FileMode.Append, FileAccess.Write);
            StreamWriter s1 = new StreamWriter(fs1);
            s1.Write("Title: " + title + "\r\n");
            s1.Write("Message: " + msg + "\r\n");
            s1.Write("StackTrace: " + stkTrace + "\r\n");
            s1.Write("Date/Time: " + DateTime.Now.ToString() + "\r\n");
            s1.Write("===========================================================================================" + "\r\n");
            s1.Close();
            fs1.Close();

            ni.Visible = true;
            ni.Icon = new System.Drawing.Icon(Globals.DeviceDirectionPath() + "messagebox_warning.ico");
            ni.ShowBalloonTip(0, "Naveo", "Please verify error log for more details. Tips " + app_path + "\\Errors\\errlog.txt", ToolTipIcon.Error);
            ni.DoubleClick += new EventHandler(ni_DoubleClick);
        }
        public static void WriteToErrorLog(Exception ex)
        {
            try
            {
                Errors.WriteToErrorLog(ex.Message, ex.StackTrace, ex.Source, Application.StartupPath.ToString());
            }
            catch { }
        }
        public static void WriteToErrorLog(Exception ex, String strOtherMessage)
        {
            try
            {
                Errors.WriteToErrorLog(ex.Message + " >>> " + strOtherMessage, ex.StackTrace, ex.Source, Application.StartupPath.ToString());
            }
            catch { }
        }

        static NotifyIcon ni = new NotifyIcon();
        static void ni_DoubleClick(object sender, EventArgs e)
        {
            NotifyIcon i = (NotifyIcon)sender;
            i.Visible = false;
            //Process proc = new Process();
            //proc.StartInfo.FileName = Application.StartupPath.ToString() + "\\Errors\\errlog.txt";
            //proc.Start();
        }

        public static void WriteToLog(String strMessage)
        {
            try
            {
                FileInfo f = new FileInfo(Globals.LogPath() + "log.txt");
                if (f.Length > 1000000)
                    File.Delete(Globals.LogPath() + "log.txt");
            }
            catch { }

            strMessage = DateTime.Now.ToString() + " > " + strMessage;

            try
            {
                File.AppendAllText(Globals.LogPath() + "log.txt", strMessage + "\r\n");
            }
            catch { }
        }
        public static void WriteToErrorLog(String strMessage)
        {
            try
            {
                FileInfo f = new FileInfo(Globals.LogPath() + "errlog.txt");
                if (f.Length > 10000)
                    File.Delete(Globals.LogPath() + "errlog.txt");
            }
            catch { }

            strMessage = DateTime.Now.ToString() + " > " + strMessage;

            try
            {
                File.AppendAllText(Globals.LogPath() + "errlog.txt", strMessage + "\r\n");
            }
            catch { }
        }

        public static void WriteToLogDesktop(Exception ex)
        {
            WriteToErrorLog(ex.Message, ex.StackTrace, ex.Source, Globals.DesktopPath());
        }

        public static void ShowPopup(String str)
        {
            if (str == String.Empty)
                return;
            ni.Visible = true;
            ni.Icon = new System.Drawing.Icon(Globals.DeviceDirectionPath() + "messagebox_warning.ico");
            ni.ShowBalloonTip(0, "Naveo", str, ToolTipIcon.Info);
            ni.DoubleClick += new EventHandler(ni_DoubleClick);
        }
    }
}
