﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NaveoOneLib.Common
{
    //http://zybermark.blogspot.com/2011/04/simple-fast-and-accurate-running.html
    public class SimpleRunningAverage
    {
        int _size;
        int[] _values = null;
        int _valuesIndex = 0;
        int _valueCount = 0;
        int _sum = 0;
        int _PreviousValue = 0;
        Boolean _DropOrFillInProgress = false;

        public SimpleRunningAverage(int size)
        {
            System.Diagnostics.Debug.Assert(size > 0);
            _size = Math.Max(size, 1);
            _values = new int[_size];
        }

        //NewValue is the value to be compared
        //NextValues are next values to compare if newValue is a single peak
        //DropOrFillValue: Minimum value that detects a DropOrFillValue
        public int Add(int newValue, int[] NextValues, int DropOrFillValue)
        {
            Boolean DropOrFill = false;
            if (Math.Abs(_PreviousValue - newValue) > DropOrFillValue)
            {
                Boolean bTobeRejected = false;

                for (int i = 0; i < NextValues.Length; i++)
                    if (Math.Abs(newValue - Math.Abs(NextValues[i])) > DropOrFillValue)
                    {
                        bTobeRejected = true;
                        break;
                    }

                if (bTobeRejected)
                {
                    Console.WriteLine("Drop or fill rejected " + newValue.ToString());
                    return 0;
                }
                else
                {
                    DropOrFill = true;
                    _DropOrFillInProgress = true;

                    for (int i = _valuesIndex; i >= 0; i--)
                        _values[i] = newValue;

                    for (int i = _valuesIndex; i < _values.Length; i++)
                        _values[i] = 0;

                    _sum = _valueCount * newValue;

                    Console.WriteLine("Drop or fill");
                }
            }

            if (_DropOrFillInProgress)
            {
                //When All values in Array has been checked
                if (_valuesIndex == 0)
                    _DropOrFillInProgress = false;
            }

            _PreviousValue = newValue;

            // calculate new value to add to sum by subtracting the 
            // value that is replaced from the new value; 
            int temp = newValue - _values[_valuesIndex];
            _values[_valuesIndex] = newValue;

            if (!_DropOrFillInProgress)
                _sum += temp;

            _valuesIndex++;
            _valuesIndex %= _size;

            if (_valueCount < _size)
                _valueCount++;

            int x = _sum / _valueCount;

            if (DropOrFill)
                return _PreviousValue;

            return _sum / _valueCount;
        }

        static void Main(string[] args)
        {
            SimpleRunningAverage avgg = new SimpleRunningAverage(9);
            int[] iValues = new int[] { 40, 39, 40, 40, 41, 41, 100, 100, 99, 98, 98, 20, 21, 22, 20, 19, 50, 20, 21, 22, 35, 30, 20, 21, 22 };
            for (int i = 0; i < iValues.Length; i++)
            {
                int[] NextValues = new int[3];
                for (int j = 0; j < NextValues.Length; j++)
                {
                    if (i + j + 1 < iValues.Length)
                        NextValues[j] = iValues[i + j + 1];
                    else
                        NextValues[j] = -999;
                }

                Console.WriteLine(avgg.Add(iValues[i], NextValues, 10));
            }
        }
    }
}
