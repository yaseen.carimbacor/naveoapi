﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Data;

using NaveoOneLib.Common;
using NaveoOneLib.Models;
using NaveoOneLib.DBCon;
using System.IO;
using NaveoOneLib.Services;
//using Ionic.Zip;
using DBConn.DbCon;
using System.Configuration;
//using System.Windows.Forms.DataVisualization.Charting;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using NaveoOneLib.Maps;
using NaveoOneLib.Models.Assets;

//Dinesh 20170228

//Naveo One graphing system with sensor value normalisation


namespace NaveoOneLib.Common
{
    public partial class N1Graph
    {
        // Global Variables with inital values

        public int iSr = 6;
        public int iACVR = 1;
        public int iPOITh = 20;

        public DataTable dtAuxData = new DataTable();

        public DataTable NGraphProcessed(DataTable Sn, String sConnStr)
        {
            int i;
            Double Testval;
            Double Testval2;
            Double Testval3;

            Double uTestval;
            Double uTestval2;
            Double uTestval3;

            int counter = 0;

            //Defining Data Table for processing
            int iAssetId = 0;

            DataTable dtProcess = new DataTable();
            dtProcess.Columns.Add("TimeStamp", typeof(DateTime));
            dtProcess.Columns.Add("AbsValue", typeof(Double));
            dtProcess.Columns.Add("AssetId", typeof(int));
            dtProcess.Columns.Add("TypeID", typeof(String));
            dtProcess.Columns.Add("UOM", typeof(String));
            dtProcess.Columns.Add("ConvertedUOMDesc", typeof(String));
            dtProcess.Columns.Add("TypeValue", typeof(Double));
            dtProcess.Columns.Add("ConvertedValue", typeof(Double));
            dtProcess.Columns.Add("DateTimeGPS_UTC", typeof(DateTime));
            dtProcess.Columns.Add("IgnInfo", typeof(String));
            dtProcess.Columns.Add("MHarmon1", typeof(Double));
            dtProcess.Columns.Add("MHarmon2", typeof(Double));
            dtProcess.Columns.Add("CompMag", typeof(Double));
            dtProcess.Columns.Add("Validpt", typeof(Boolean));
            dtProcess.Columns.Add("DetectedPOI", typeof(int));
            dtProcess.Columns.Add("Remarks2", typeof(String));
            dtProcess.Columns.Add("Address", typeof(String));

            for (i = 0; i <= Sn.Rows.Count - 1; i++)
            {
                DataRow dr = dtProcess.NewRow();
                dr["AssetId"] = Sn.Rows[i]["AssetId"];
                iAssetId = Convert.ToInt16(Sn.Rows[i]["AssetId"].ToString());
                dr["TypeID"] = Sn.Rows[i]["TypeID"];
                dr["UOM"] = Sn.Rows[i]["UOM"];
                dr["ConvertedUOMDesc"] = Sn.Rows[i]["ConvertedUOMDesc"];
                dr["TypeValue"] = Sn.Rows[i]["TypeValue"];
                dr["ConvertedValue"] = Sn.Rows[i]["ConvertedValue"];
                dr[0] = Sn.Rows[i]["DateTimeGPS_UTC"];
                dr[1] = Sn.Rows[i]["TypeValue"];
                dr["DateTimeGPS_UTC"] = Sn.Rows[i]["DateTimeGPS_UTC"];
                dr["IgnInfo"] = Sn.Rows[i]["IngnitionInfo"];

                dtProcess.Rows.Add(dr);
            }

            //Dinesh Variance to be properly implemented when doin for calibration chart
            Double minVal = 0;
            Double maxVal = 0;
            foreach (DataRow dr in dtProcess.Rows)
            {
                if (minVal > Convert.ToDouble(dr["ConvertedValue"]))
                    minVal = Convert.ToDouble(dr["ConvertedValue"]);

                if (maxVal < Convert.ToDouble(dr["ConvertedValue"]))
                    maxVal = Convert.ToDouble(dr["ConvertedValue"]);
            }

            CalibrationChart cal = new CalibrationChart();
            List<CalibrationChart> lcal = new Services.Assets.CalibrationChartService().GetCalibrationChartLByAssetId(iAssetId, sConnStr);
            Double VR = maxVal - minVal;
            if (lcal == null)
                VR = maxVal - minVal;
            else
            {
                VR = 0;
                foreach (CalibrationChart cc in lcal)
                {
                    if (cc.ConvertedValue > VR)
                        VR = cc.ConvertedValue;
                }
            }

            Double TempMHarmon1 = 0;
            int j = 0, k = 0;
            for (j = 0; j <= dtProcess.Rows.Count - (1 + iSr); j++)
            {
                TempMHarmon1 = 0;
                for (k = 0; k <= iSr - 1; k++)
                    TempMHarmon1 = TempMHarmon1 + Convert.ToDouble(dtProcess.Rows[j + k]["ConvertedValue"]);
                TempMHarmon1 = TempMHarmon1 / iSr;

                dtProcess.Rows[j]["MHarmon1"] = Math.Round(TempMHarmon1, 2);
                if (j > 1 && j <= dtProcess.Rows.Count - (1 + iSr))
                {
                    Testval = (Convert.ToDouble(dtProcess.Rows[j]["ConvertedValue"])) - (Convert.ToDouble(dtProcess.Rows[j - 2]["ConvertedValue"]));
                    uTestval = (Convert.ToDouble(dtProcess.Rows[j]["ConvertedValue"])) - (Convert.ToDouble(dtProcess.Rows[j - 1]["ConvertedValue"]));
                    if (Testval < 0)
                        Testval = Testval * -1;

                    Testval2 = (Convert.ToDouble(dtProcess.Rows[j]["MHarmon1"])) - (Convert.ToDouble(dtProcess.Rows[j - 2]["MHarmon1"]));
                    uTestval2 = (Convert.ToDouble(dtProcess.Rows[j]["MHarmon1"])) - (Convert.ToDouble(dtProcess.Rows[j - 2]["MHarmon1"]));
                    if (Testval2 < 0)
                        Testval2 = Testval2 * -1;

                    Testval3 = Testval2 - Testval;
                    uTestval3 = uTestval2 - uTestval;
                    if (Testval3 < 0)
                        Testval3 = Testval3 * -1;

                    dtProcess.Rows[j]["CompMag"] = Testval3;

                    Double TestPerc = ((Testval3 / VR) * 100);
                    if (TestPerc > iACVR)
                        dtProcess.Rows[j]["MHarmon2"] = Convert.ToDouble(dtProcess.Rows[j - 2]["MHarmon1"]);
                    else
                        dtProcess.Rows[j]["MHarmon2"] = Convert.ToDouble(dtProcess.Rows[j]["ConvertedValue"]);

                    Double uTestPerc = ((uTestval3 / VR) * 100);
                    if (uTestPerc >= iPOITh)
                    {
                        dtProcess.Rows[j]["DetectedPOI"] = 1;
                        dtProcess.Rows[j]["Remarks2"] = dtProcess.Rows[j]["TimeStamp"].ToString() + "---> FILL UP" + "\r\n";
                        counter = counter + 1;
                    }
                    if (uTestPerc <= (-1 * iPOITh))
                    {
                        dtProcess.Rows[j]["DetectedPOI"] = -1;
                        dtProcess.Rows[j]["Remarks2"] = dtProcess.Rows[j]["TimeStamp"].ToString() + "---> LEVEL DROP" + "\r\n";
                        counter = counter + 1;
                    }
                }
            }

            foreach (DataRow dril in dtProcess.Rows)
            {
                if (dril["MHarmon1"].ToString() == "")
                    dril["MHarmon1"] = dril["ConvertedValue"];
            }

            int l = 0;

            for (l = 0; l <= dtProcess.Rows.Count - 1; l++)
            {
                Double TTemp = 0;
                if (dtProcess.Rows[l]["IgnInfo"].ToString() != "")
                {
                    if (l > 1)
                    {
                        int n = 3, count = 0;
                        for (n = 3; n < 12; n++)
                        {
                            if (l - n > 0)
                            {
                                count = count + 1;
                                TTemp = TTemp + Convert.ToDouble(dtProcess.Rows[l - n]["MHarmon1"]);
                            }
                        }
                        TTemp = Math.Round(TTemp / count, 2);

                        int m = 0;
                        for (m = 0; m <= 14; m++)
                        {
                            if ((l + m) < dtProcess.Rows.Count)
                            {
                                dtProcess.Rows[l + m]["MHarmon1"] = TTemp;
                            }
                            int p = 0;
                            for (p = 0; p <= 5; p++)
                            {
                                if (l - p > 0)
                                {
                                    dtProcess.Rows[l - p]["MHarmon1"] = TTemp;
                                }
                            }
                        }
                    }
                }
            }

            // MessageBox.Show("Detected=" + counter.ToString());
            return dtProcess;
        }
    }
}