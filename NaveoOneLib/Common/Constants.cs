﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace NaveoOneLib.Common
{
    public class Constants
    {
        public static Boolean bIsDevEnvironment = false;
        /// <summary>
        /// The value used to represent a null DateTime value
        /// </summary>
        public static DateTime NullDateTime = new DateTime(1900, 01, 01, 00, 00, 00);//DateTime.MinValue;
        public static DateTime MaxDateTime = new DateTime(3999, 01, 01, 00, 00, 00);//DateTime.MaxValue;

        /// <summary>
        /// The value used to represent a null decimal value
        /// </summary>
        public static decimal NullDecimal = decimal.MinValue;

        /// <summary>
        /// The value used to represent a null double value
        /// </summary>
        public static double NullDouble = double.MinValue;

        /// <summary>
        /// The value used to represent a null Guid value
        /// </summary>
        public static Guid NullGuid = Guid.Empty;

        /// <summary>
        /// The value used to represent a null int value
        /// </summary>
        public static int NullInt = int.MinValue;

        /// <summary>
        /// The value used to represent a null long value
        /// </summary>
        public static long NullLong = long.MinValue;

        /// <summary>
        /// The value used to represent a null float value
        /// </summary>
        public static float NullFloat = float.MinValue;

        /// <summary>
        /// The value used to represent a null string value
        /// </summary>
        public static string NullString = string.Empty;


        public static string strCustomer = "C";

        public static string strActive = "AC";

        public static string strNotActive = "NA";

        public static string strDraft = "DRAFT";

        public static string strPosted = "POSTED";

        public static String DebugMode = "N";

        //public static String strInterventionSoftware = "SOFTWARE";
        //Status of SIM Cards
        public static String strSimStatusActive ="AC";

        public static String strSimStatusNotActive ="NA";

        public static String strSimStatusSusValidity ="SV";

        public static String dbLogin()
        {
            return "NaveoOnePrimary";
        }
        public static String dbPass()
        {
            //"PRdrRD$0987";
            String s = "rjAai+rrro36UmOe8fKSf0SYF+KGP8qO";
            return NaveoOneLib.Services.BaseService.Decrypt(s);
        }
        public static String sConnStrCred()
        {
            String s = "User ID = ";
            s += dbLogin();
            s += "; Pwd = ";
            s += dbPass();
            s += ";";
            return s;
        }

        public static CultureInfo myCulture()
        {            
            //CultureInfo culture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            CultureInfo culture = new CultureInfo("en-GB"); 
            culture.DateTimeFormat.ShortDatePattern = "dd-MMM-yyyy HH:mm:ss";
            culture.DateTimeFormat.LongTimePattern = "";

            System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";
            culture.NumberFormat = nfi;
            return culture;
        }
    }
}
