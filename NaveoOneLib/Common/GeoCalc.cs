﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NaveoOneLib.Common
{
    public class GeoCalc
    {
        public class CDistanceBetweenLocations
        {
            public static double Calc(double Lat1,
                          double Long1, double Lat2, double Long2)
            {
                /*
                    The Haversine formula according to Dr. Math.
                    http://mathforum.org/library/drmath/view/51879.html
                
                    dlon = lon2 - lon1
                    dlat = lat2 - lat1
                    a = (sin(dlat/2))^2 + cos(lat1) * cos(lat2) * (sin(dlon/2))^2
                    c = 2 * atan2(sqrt(a), sqrt(1-a)) 
                    d = R * c
                
                    Where
                        * dlon is the change in longitude
                        * dlat is the change in latitude
                        * c is the great circle distance in Radians.
                        * R is the radius of a spherical Earth.
                        * The locations of the two points in 
                            spherical coordinates (longitude and 
                            latitude) are lon1,lat1 and lon2, lat2.
                */
                double dDistance = Double.MinValue;
                double dLat1InRad = Lat1 * (Math.PI / 180.0);
                double dLong1InRad = Long1 * (Math.PI / 180.0);
                double dLat2InRad = Lat2 * (Math.PI / 180.0);
                double dLong2InRad = Long2 * (Math.PI / 180.0);

                double dLongitude = dLong2InRad - dLong1InRad;
                double dLatitude = dLat2InRad - dLat1InRad;

                // Intermediate result a.
                double a = Math.Pow(Math.Sin(dLatitude / 2.0), 2.0) +
                           Math.Cos(dLat1InRad) * Math.Cos(dLat2InRad) *
                           Math.Pow(Math.Sin(dLongitude / 2.0), 2.0);

                // Intermediate result c (great circle distance in Radians).
                double c = 2.0 * Math.Asin(Math.Sqrt(a));

                // Distance.
                // const Double kEarthRadiusMiles = 3956.0;
                const Double kEarthRadiusKms = 6376.5;
                dDistance = kEarthRadiusKms * c;

                return dDistance;
            }

            public static double Calc(string NS1, double Lat1, double Lat1Min,
                   string EW1, double Long1, double Long1Min, string NS2,
                   double Lat2, double Lat2Min, string EW2,
                   double Long2, double Long2Min)
            {
                double NS1Sign = NS1.ToUpper() == "N" ? 1.0 : -1.0;
                double EW1Sign = EW1.ToUpper() == "E" ? 1.0 : -1.0;
                double NS2Sign = NS2.ToUpper() == "N" ? 1.0 : -1.0;
                double EW2Sign = EW2.ToUpper() == "E" ? 1.0 : -1.0;
                return (Calc(
                    (Lat1 + (Lat1Min / 60)) * NS1Sign,
                    (Long1 + (Long1Min / 60)) * EW1Sign,
                    (Lat2 + (Lat2Min / 60)) * NS2Sign,
                    (Long2 + (Long2Min / 60)) * EW2Sign
                    ));
            }

            public static void Main(string[] args)
            {
                if (args.Length < 12)
                {
                    System.Console.WriteLine("usage: DistanceBetweenLocations" +
                            " N 43 35.500 W 80 27.800 N 43 35.925 W 80 28.318");
                    return;
                }
                System.Console.WriteLine(Calc(
                    args[0],
                    System.Double.Parse(args[1]),
                    System.Double.Parse(args[2]),
                    args[3],
                    System.Double.Parse(args[4]),
                    System.Double.Parse(args[5]),
                    args[6],
                    System.Double.Parse(args[7]),
                    System.Double.Parse(args[8]),
                    args[9],
                    System.Double.Parse(args[10]),
                    System.Double.Parse(args[11])));

            }
        }

        public int Distance2D(int x1, int y1, int x2, int y2)
        {

            int result = 0;
            double part1 = Math.Pow((x2 - x1), 2);

            double part2 = Math.Pow((y2 - y1), 2);
            double underRadical = part1 + part2;
            result = (int)Math.Sqrt(underRadical);

            return result;
        }
        public Double DistanceESRI(Double Lat1, Double Lon1, Double Lat2, Double Lon2)
        {
            Double dist = EGIS.ShapeFileLib.ConversionFunctions.DistanceBetweenLatLongPoints(EGIS.ShapeFileLib.ConversionFunctions.RefEllipse, Lat1, Lon1, Lat2, Lon2);
            return dist;
        }

        public static Double[] GetSquareCoord(Double Lat, Double Lon, int DistanceInMeters)
        {
            Double[] dResult = new Double[8];

            //Earth’s radius, sphere
            Double R = 6378137;

            //offsets in meters
            int dn = DistanceInMeters;
            int de = DistanceInMeters;

            //Coordinate offsets in radians
            Double dLat = dn / R;
            Double dLon = de / (R * Math.Cos(Math.PI * Lat / 180));

            //OffsetPosition, decimal degrees
            Double latO = Lat + dLat * 180 / Math.PI;
            Double lonO = Lon + dLon * 180 / Math.PI;

            dResult[0] = latO;
            dResult[1] = lonO;
            return dResult;
        }
        public static Double MetersToDecimalDegrees(Double meters, Double latitude)
        {
            return meters / (111.32 * 1000 * Math.Cos(latitude * (Math.PI / 180)));
        }

        public static Double[] AddMetersToCoord(int distance, Double Lat, Double Lng, Double angle)
        {
            //Angle
            //  0   > N
            //  -45 > NW
            //  45  > NE
            //  90  > SE
            //  -90 > SW

            Double distanceNorth = Math.Sin(angle) * distance;
            Double distanceEast = Math.Cos(angle) * distance;
            int earthRadius = 6371000;
            Double newLat = Lat + (distanceNorth / earthRadius) * 180 / Math.PI;
            Double newLon = Lng + (distanceEast / (earthRadius * Math.Cos(newLat * 180 / Math.PI))) * 180 / Math.PI;

            Double[] dResult = new Double[2];
            dResult[0] = newLat;
            dResult[1] = newLon;
            return dResult;
        }
        public static Double[] getBoundingBox(Double pLatitude, Double pLongitude, int pDistanceInMeters)
        {
            //pDistanceInMeters = pDistanceInMeters / 2;
            Double[] boundingBox = new Double[4];

            Double latRadian = (Math.PI / 180) * pLatitude;
            Double degLatKm = 110.574235;
            Double degLongKm = 110.572833 * Math.Cos(latRadian);
            Double deltaLat = pDistanceInMeters / 1000.0 / degLatKm;
            Double deltaLong = pDistanceInMeters / 1000.0 / degLongKm;

            Double minLat = pLatitude - deltaLat;
            Double minLong = pLongitude - deltaLong;
            Double maxLat = pLatitude + deltaLat;
            Double maxLong = pLongitude + deltaLong;

            boundingBox[0] = minLat;
            boundingBox[1] = minLong;
            boundingBox[2] = maxLat;
            boundingBox[3] = maxLong;

            return boundingBox;
        }

        public static Boolean isCoordinateInCircle(Coordinate centre, int RadiusInMeters, Coordinate toTes)
        {
            int pDistanceInMeters = RadiusInMeters;// / 2;

            Double latRadian = (Math.PI / 180) * centre.Lt;
            Double degLatKm = 110.574235;
            Double degLongKm = 110.572833 * Math.Cos(latRadian);
            Double deltaLat = pDistanceInMeters / 1000.0 / degLatKm;
            Double deltaLong = pDistanceInMeters / 1000.0 / degLongKm;

            Coordinate c = new Coordinate(centre.Lt, centre.Lg);
            Coordinate m = new Coordinate(toTes.Lt, toTes.Lg);

            Boolean b = GeoCalc.isPointInCircle(c, deltaLat, m);
            return b;

            //Working loging from maps mouse move
            //PointD d = new PointD(centrePointD.Value.X, centrePointD.Value.Y);
            //int pDistanceInMeters = 100 / 2;
            //Double latRadian = (Math.PI / 180) * d.Y;
            //Double degLatKm = 110.574235;
            //Double degLongKm = 110.572833 * Math.Cos(latRadian);
            //Double deltaLat = pDistanceInMeters / 1000.0 / degLatKm;
            //Double deltaLong = pDistanceInMeters / 1000.0 / degLongKm;
            //Coordinate c = new Coordinate(centrePointD.Value.Y, centrePointD.Value.X);
            //Coordinate m = new Coordinate(pt.Y, pt.X);
            //Boolean b = GeoCalc.isPointInCircle(c, deltaLat, m);
            //myToolTip.Show(b.ToString(), this, e.X, e.Y);
        }
        public static Boolean isPointInCircle(Coordinate centre, Double radius, Coordinate toTest)
        {
            // Is the point inside the circle? Sum the squares of the x-difference and
            // y-difference from the centre, square-root it, and compare with the radius.
            // (This is Pythagoras' theorem.)

            Double dX = Math.Abs(toTest.Lg - centre.Lg);
            Double dY = Math.Abs(toTest.Lt - centre.Lt);
            Double sumOfSquares = dX * dX + dY * dY;
            Double distance = Math.Sqrt(sumOfSquares);
            return (radius >= distance);
        }
        public static Boolean IsPointInside(List<Coordinate> poly, Coordinate point)
        {
            int i, j;
            bool c = false;
            for (i = 0, j = poly.Count - 1; i < poly.Count; j = i++)
            {
                if ((((poly[i].Lt <= point.Lt) && (point.Lt < poly[j].Lt))
                        || ((poly[j].Lt <= point.Lt) && (point.Lt < poly[i].Lt)))
                        && (point.Lg < (poly[j].Lg - poly[i].Lg) * (point.Lt - poly[i].Lt)
                            / (poly[j].Lt - poly[i].Lt) + poly[i].Lg))

                    c = !c;
            }
            return c;
        }

        public static Double CalcDistanceInKM(Double lat1, Double lat2, Double lon1, Double lon2)
        {
            return Math.Acos(Math.Sin(Math.PI * lat1 / 180.0) * Math.Sin(Math.PI * lat2 / 180.0) + Math.Cos(Math.PI * lat1 / 180.0) * Math.Cos(Math.PI * lat2 / 180.0) * Math.Cos(Math.PI * lon2 / 180.0 - Math.PI * @lon1 / 180.0)) * 6371;
        }
        public static Double CalcDistanceInM(Double lat1, Double lat2, Double lon1, Double lon2)
        {
            return CalcDistanceInKM(lat1, lat2, lon1, lon2) * 1000;
        }

        public static Double calculateBearingTo(Double lat1, Double lng1, Double lat2, Double lng2)
        {
            //calc bearing from prev point to this one
            Double rLat1 = degreesToRadians(lat1);
            Double rLat2 = degreesToRadians(lat2);
            Double rDeltaLng = degreesToRadians(lng2 - lng1);

            Double y = Math.Sin(rDeltaLng) * Math.Cos(rLat2);
            Double x = Math.Cos(rLat1) * Math.Sin(rLat2) -
                    Math.Sin(rLat1) * Math.Cos(rLat2) * Math.Cos(rDeltaLng);
            Double brng = Math.Atan2(y, x);

            return (radiansToDegrees(brng) + 360) % 360;
        }
        public static Double degreesToRadians(Double degrees)
        { return degrees * Math.PI / 180; }
        public static Double radiansToDegrees(Double radians)
        { return radians * 180 / Math.PI; }
        public static String getPointImage(Double bearing, Boolean isSuspect, Boolean isStop, Boolean isException)
        {
            if (isSuspect)
                return "Suspect.png";
            if (isStop)
                return "Stop.png";
            if (bearing == -999999.999)
                return "Start.png";
            if (bearing == -888888.88)
                return "Idle.png";

            //Format is M[00]Direction[00]iVehicleCount[0]Exception
            String strFilename = String.Empty;

            if (isException)
                strFilename += "1.png;";
            else
                strFilename += "0.png;";

            Double imagesCount = 16;
            Double degreesPerImage = 360 / imagesCount;
            String s = String.Empty;
            for (int i = 0; i < imagesCount; i++)
                if (i <= 9)
                    s += ("M0" + i.ToString() + strFilename);
                else
                    s += ("M" + i.ToString() + strFilename);

            String[] arrImage = s.Split(';');

            Double imageIndex = Math.Floor(bearing / degreesPerImage),
                midlineDistance = bearing % degreesPerImage;
            if (midlineDistance > degreesPerImage / 2)
            {
                imageIndex++;
            }
            imageIndex = imageIndex % arrImage.Length;

            if (imageIndex >= 16)
                imageIndex = 0;

            return arrImage[Convert.ToInt16(imageIndex)];
        }
    }
}
