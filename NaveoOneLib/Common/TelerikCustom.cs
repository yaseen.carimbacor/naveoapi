﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Telerik.WinControls.UI;

namespace NaveoOneLib.Common
{
    public class TelerikCustom
    {
    }

    public class CustomSummaryItemIsNoGood : Telerik.WinControls.UI.GridViewSummaryItem
    {
        public CustomSummaryItemIsNoGood(string name, string formatString, Telerik.WinControls.UI.GridAggregateFunction aggregate)
            : base(name, formatString, aggregate)
        {
        }

        public override object Evaluate(Telerik.WinControls.UI.IHierarchicalRow row)
        {
            System.TimeSpan value = new System.TimeSpan();
            foreach (Telerik.WinControls.UI.GridViewRowInfo childRow in row.ChildRows)
            {
                if (childRow.Cells[this.Name].Value != null)
                {
                    if ((childRow.Cells[this.Name].Value.ToString().Contains("-") ? false : !string.IsNullOrEmpty(childRow.Cells[this.Name].Value.ToString())))
                    {
                        DateTime dateTime = DateTime.Parse(childRow.Cells[this.Name].Value.ToString());
                        value = value + (dateTime - DateTime.Now);
                    }
                }
            }
            return value;
        }
    }

    public class CustomSummaryItem : GridViewSummaryItem
    {
        public CustomSummaryItem() { }
        public CustomSummaryItem(string name, string formatString, GridAggregateFunction aggregate)
            : base(name, formatString, aggregate)
        { }

        public override object Evaluate(IHierarchicalRow row)
        {
            TimeSpan timeSpanSum = new TimeSpan();
            foreach (GridViewRowInfo childRow in row.ChildRows)
            {
                if (childRow is GridViewGroupRowInfo)
                {
                    foreach (GridViewRowInfo r in childRow.ChildRows)
                    {
                        if (r.Cells[this.Name].Value != null)
                            if (!r.Cells[this.Name].Value.ToString().Contains("-") &&
                                !String.IsNullOrEmpty(r.Cells[this.Name].Value.ToString()))
                                timeSpanSum += (TimeSpan)r.Cells[this.Name].Value;
                    }
                }
                else
                {
                    if (childRow.Cells[this.Name].Value != null)
                        if (!childRow.Cells[this.Name].Value.ToString().Contains("-") &&
                            !String.IsNullOrEmpty(childRow.Cells[this.Name].Value.ToString()))
                            timeSpanSum += (TimeSpan)childRow.Cells[this.Name].Value;
                }
            }
            return timeSpanSum;
            //TimeSpan timeSpanSum = new TimeSpan();
            //foreach (GridViewRowInfo childRow in row.ChildRows)
            //{
            //    if (childRow.Cells[this.Name].Value != null)
            //        if (!childRow.Cells[this.Name].Value.ToString().Contains("-") && !String.IsNullOrEmpty(childRow.Cells[this.Name].Value.ToString()))
            //            timeSpanSum += (TimeSpan)childRow.Cells[this.Name].Value;
            //}
            //return timeSpanSum;
        }
    }
}
