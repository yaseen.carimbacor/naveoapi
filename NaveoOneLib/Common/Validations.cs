﻿using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.GMatrix;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace NaveoOneLib.Common
{
    public static class Validations
    {
        public static List<int> lValidateAssets(int UID, List<int> lassetIds, String sConnStr)
        {
            DataTable dtGMAsset = new NaveoOneLib.WebSpecifics.LibData().dtGMAsset(UID, sConnStr);

            //chk assetIds with dtGMAsset assetIds
            List<int> lIDs = new List<int>();
            foreach (int i in lassetIds)
            {
                DataRow[] dr = dtGMAsset.Select("StrucID = " + i);
                if (dr.Length > 0)
                    lIDs.Add(i);
            }

            return lIDs.Distinct().ToList();
        }
        public static List<Matrix> lValidateMatrix(int UID, List<Matrix> lMatrixIDs, String sConnStr)
        {
            DataTable dtGM = new NaveoOneLib.WebSpecifics.LibData().dtGMMatrix(UID, sConnStr);

            List<Matrix> lm = new List<Matrix>();
            foreach (Matrix m in lMatrixIDs)
            {
                DataRow[] dr = dtGM.Select("GMID = " + m.GMID);
                if (dr.Length > 0)
                    lm.Add(m);
            }

            return lm.Distinct().ToList();
        }

        public static List<int> lValidateAssets(int UID, NaveoModels nv, List<int> lassetIds, String sConnStr)
        {
            DataTable dtGMAsset = new DataTable();

            switch (nv)
            {
                case NaveoModels.Assets:
                    dtGMAsset = new NaveoOneLib.WebSpecifics.LibData().dtGMAsset(UID, sConnStr);
                    break;

                case NaveoModels.BLUP:
                    dtGMAsset = new NaveoOneLib.WebSpecifics.LibData().dtGMAsset(UID, sConnStr);
                    break;
            }

            //chk assetIds with dtGMAsset assetIds
            List<int> lIDs = new List<int>();
            foreach (int i in lassetIds)
            {
                DataRow[] dr = dtGMAsset.Select("StrucID = " + i);
                if (dr.Length > 0)
                    lIDs.Add(i);
            }

            return lIDs.Distinct().ToList();
        }

        public static List<int> lValidateDrivers(int UID, List<int> lassetIds, String sConnStr)
        {
            DataTable dtGMAsset = new NaveoOneLib.WebSpecifics.LibData().dtGMDriver(UID, sConnStr);

            //chk assetIds with dtGMAsset assetIds
            List<int> lIDs = new List<int>();
            foreach (int i in lassetIds)
            {
                DataRow[] dr = dtGMAsset.Select("StrucID = " + i);
                if (dr.Length > 0)
                    lIDs.Add(i);
            }

            return lIDs.Distinct().ToList();
        }

        public static class EmailValidation
        {
            /// <summary>
            /// Regular expression, which is used to validate an E-Mail address.
            /// </summary>
            public const string MatchEmailPattern =
                      @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
               + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
    [0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
               + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
    [0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
               + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

            /// <summary>
            /// Checks whether the given Email-Parameter is a valid E-Mail address.
            /// </summary>
            /// <param name="email">Parameter-string that contains an E-Mail address.</param>
            /// <returns>True, when Parameter-string is not null and 
            /// contains a valid E-Mail address;
            /// otherwise false.</returns>
            public static bool IsEmail(string email)
            {
                if(String.IsNullOrEmpty(email))
                    return false;

                if (email != null) 
                    return Regex.IsMatch(email, MatchEmailPattern);
                
                return false;
            }
        }
    }
}
