﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Windows.Forms;
using System.Data;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.IO;
using System.Linq;
using Telerik.WinControls.UI;
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.Drivers;
using NaveoOneLib.Models.Zones;
using NaveoOneLib.Models.Users;
using NaveoOneLib.Models.Audits;
using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Services.Permissions;
using NaveoOneLib.Services.Users;

namespace NaveoOneLib.Common
{
    public class Globals
    {
        public enum DatabaseProvider
        {
            MicrosoftSQLServer,
            MySQL,
            Oracle,
            PostgreSQL
        }
        public static DatabaseProvider dbProvider = DatabaseProvider.MicrosoftSQLServer;

        #region Variables
        public static User uLogin = new User();

        public static ActiveUsers ActiveUser { get; set; }

        public static DataSet dsZones = new DataSet();
        public static List<Zone> lZone = new List<Zone>();
        public static List<Asset> lAsset = new List<Asset>();
        public static List<Asset> lAssetWithZone = new List<Asset>();
        public static List<Driver> lDriver = new List<Driver>();

        public static DataTable cboAutoFill = new DataTable();
        public static DataTable LogReasonDT = new DataTable();
        public static DataTable dtGMNone = new DataTable();
        public static DataTable dtGMAsset = new DataTable();
        public static DataTable dtGMUser = new DataTable();
        public static DataTable dtGMZone = new DataTable();
        public static DataTable dtGMZoneType = new DataTable();
        public static DataTable dtGMDriver = new DataTable();
        public static DataTable dtGMRule = new DataTable();
        public static DataTable dtZoneTypes = new DataTable();
        public static DataTable dtApplications = new DataTable();
        public static DataTable dtUserSettings = new DataTable();

        public static DataTable dtGMLZone = new DataTable();
        public static DataTable dtGMLAsset = new DataTable();
        public static DataTable dtGMLDriver = new DataTable();
        public static DataTable dtGMLUser = new DataTable();
        public static DataTable dtGMLMaintType = new DataTable();
        public static DataTable dtGMTreeCategory = new DataTable();

        //sGet???(lMatrix)
        public static DataTable dtAsset = new DataTable();
        public static DataTable dtDriver = new DataTable();
        public static DataTable dtMatrix = new DataTable();
        public static DataTable dtRules = new DataTable();

        public static DataTable dtAssetExtProFields = new DataTable();
        public static DataTable dtVehicleMaintCat = new DataTable();
        public static DataTable dtInsCoverTypes = new DataTable();
        public static DataTable dtVehicleMaintStatus = new DataTable();
        public static DataTable dtVehicleMaintType = new DataTable();
        public static DataTable dtVehicleMaintTypeAssign = new DataTable();
        public static DataTable dtVehicleTypes = new DataTable();

        public static DataTable dtAccessTemplatesForUserAssign = new DataTable();
        public static DataTable dtAdditionalMapLayers = new DataTable();

        //For debugging purposes
        public static Boolean bDebugMode = false;
        public static DataTable dtDevCTRL = new DataTable();
        public static DataSet dsDev = new DataSet();

        public static Boolean Maploaded = false;
        //public static DateTime? dtArchive;

        public static String ServerName = "";
        public static String ServerDeployUrl = "";
        public static String ExeSource = "";

        public static int ucSelPeriod = 999;
        public static String ucSelFrom = String.Empty;
        public static String ucSelTo = String.Empty;
        public static String sLoginMessage = String.Empty;

        //Proxy
        public static Boolean bUseProxy = false;
        public static String pDomain = String.Empty;
        public static String pUserName = String.Empty;
        public static String pPassword = String.Empty;
        public static String pAddress = String.Empty;
        public static int pPort = 0;

        public enum MapType
        {
            None,
            ESRI,
            HereSatellite,
            HereHybrid,
            HereStreet,
            HereMaps,
            BingMaps,
            OpenStreet,
            Mireo
        };
        public static MapType eMapType = MapType.ESRI;
        public static Double myLat = -20.150000055272;
        public static Double myLon = 57.6004997231693;

        public static String SmtpClientHost = String.Empty;
        public static int SmtpClientPort = -1;
        public static Boolean SmtpClientEnableSsl = true;
        public static String SmtpClientUsrName = String.Empty;
        public static String SmtpClientUsrPwd = String.Empty;
        public static String SmtpClientSenderMail = String.Empty;

        public static string strDisplayer = "";

        public static Boolean bUserSec = true;

        public static String sWebSitePath = String.Empty;
        #endregion

        #region Save and retrieve String to files
        public static String UserDataPath()
        {
            String strP = String.Empty;
            if (String.IsNullOrEmpty(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath))
                strP = Application.StartupPath.ToString() + "\\UserData\\";
            else
                strP = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath.ToString() + "\\UserData\\";
            if (!(System.IO.Directory.Exists(strP)))
                System.IO.Directory.CreateDirectory(strP);
            return strP;
        }
        public static String SaveFile(String sData)
        {
            String strP = UserDataPath();
            string[] filePaths = Directory.GetFiles(strP, "*.*");
            foreach (String file in filePaths)
            {
                try
                {
                    FileInfo fi = new FileInfo(file);
                    if (fi.LastAccessTime < DateTime.Now.AddDays(-5))
                        fi.Delete();
                }
                catch { }
            }
            Guid guid = Guid.NewGuid();
            File.WriteAllText(strP + guid + ".dat", sData);
            return guid.ToString(); ;
        }
        public static String getFile(String sFN)
        {
            String strP = UserDataPath() + sFN;
            String sResult = File.ReadAllText(strP);
            return sResult;
        }
        #endregion

        public static String GetNaveoOnePath()
        {
            String strP = Application.StartupPath.ToString();
            return strP;
        }
        public static String ePath()
        {
            String strP = Application.StartupPath.ToString() + "\\Samplecode\\";
            if (!(System.IO.Directory.Exists(strP)))
                System.IO.Directory.CreateDirectory(strP);

            return strP;
        }
        public static String LogPath()
        {
            String strP = String.Empty;

            if (String.IsNullOrEmpty(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath))
                strP = Application.StartupPath.ToString() + "\\Log\\";
            else
                strP = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath.ToString() + "\\Log\\";

            if (!(System.IO.Directory.Exists(strP)))
                System.IO.Directory.CreateDirectory(strP);

            return strP;
        }
        public static String ErrorsPath()
        {
            String strP = String.Empty;

            if (String.IsNullOrEmpty(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath))
                strP = Application.StartupPath.ToString() + "\\Errors\\";
            else
                strP = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath.ToString() + "\\Errors\\";

            if (!(System.IO.Directory.Exists(strP)))
                System.IO.Directory.CreateDirectory(strP);

            return strP;
        }
        public static String DeviceDirectionPath()
        {
            String strP = Application.StartupPath.ToString() + "\\DeviceDirections\\";
            if (!(System.IO.Directory.Exists(strP)))
                System.IO.Directory.CreateDirectory(strP);

            return strP;
        }
        public static String GetAppFolder()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
        }
        public static String GetNaveoAppFolder(String sAdditionalDir)
        {
            if (sAdditionalDir != String.Empty)
                sAdditionalDir = sAdditionalDir + "\\";
            String strP = GetAppFolder() + "\\Naveo\\" + sAdditionalDir;
            if (!(System.IO.Directory.Exists(strP)))
                System.IO.Directory.CreateDirectory(strP);

            return strP;
        }
        public static String ExportPath(String fn)
        {
            String strP;
            if (String.IsNullOrEmpty(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath))
                strP = Application.StartupPath.ToString() + "\\Resources\\Template\\Export\\Excel\\";
            else
                strP = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath.ToString() + "bin\\Resources\\Template\\Export\\Excel\\";

            if (!(System.IO.Directory.Exists(strP)))
                System.IO.Directory.CreateDirectory(strP);

            return strP + fn;
        }

        public static String PdfExportPath(String fn)
        {
            String strP;
            if (String.IsNullOrEmpty(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath))
                strP = Application.StartupPath.ToString() + "\\Resources\\Template\\Export\\PDF\\";
            else
                strP = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath.ToString() + "bin\\Resources\\Template\\Export\\PDF\\";

            if (!(System.IO.Directory.Exists(strP)))
                System.IO.Directory.CreateDirectory(strP);

            return strP + fn;
        }

        public static String ExportTempPath(String sAdditionalDir)
        {
            String strP = String.Empty;
            if (sAdditionalDir != String.Empty)
                sAdditionalDir = sAdditionalDir + "\\";

            if (String.IsNullOrEmpty(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath))
                strP = Application.StartupPath.ToString() + "\\Resources\\Template\\Export\\Temp\\";
            else
                strP = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath.ToString() + "bin\\\\Resources\\Template\\Export\\Temp\\";

            strP += sAdditionalDir;
            if (!(System.IO.Directory.Exists(strP)))
                System.IO.Directory.CreateDirectory(strP);

            return strP;
        }
        public static String DocsPath(String sAdditionalDir)
        {
            String strP = String.Empty;
            if (sAdditionalDir != String.Empty)
                sAdditionalDir = sAdditionalDir + "\\";

            if (String.IsNullOrEmpty(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath))
                strP = Application.StartupPath.ToString() + "\\Docs\\";
            else
                strP = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath.ToString() + "Docs\\";

            strP += sAdditionalDir;
            if (!(System.IO.Directory.Exists(strP)))
                System.IO.Directory.CreateDirectory(strP);

            return strP;
        }

        public static String RawDataPath()
        {
            String strP = Application.StartupPath.ToString() + "\\RawData\\";
            if (!(System.IO.Directory.Exists(strP)))
                System.IO.Directory.CreateDirectory(strP);


            return strP;
        }
        public static String AppPath()
        {
            String strP = Application.StartupPath.ToString();
            return strP;
        }
        public static String TempPath(String sAdditionalDir)
        {
            if (sAdditionalDir != String.Empty)
                sAdditionalDir = sAdditionalDir + "\\";
            String strP = System.IO.Path.GetTempPath() + "Naveo\\" + sAdditionalDir;
            if (!(System.IO.Directory.Exists(strP)))
                System.IO.Directory.CreateDirectory(strP);

            return strP;
        }
        public static String xmlPath()
        {
            //String strP = Application.StartupPath.ToString() + "\\XML\\";
            String strP = TempPath("XML");
            if (!(System.IO.Directory.Exists(strP)))
                System.IO.Directory.CreateDirectory(strP);

            return strP;
        }

        public static String GetTempFileName()
        {
            return Path.GetRandomFileName();// String.Empty + Guid.NewGuid();
        }
        public static String DesktopPath()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        }
        public static String GetTempFolder()
        {
            String folder = System.IO.Path.Combine(System.IO.Path.GetTempPath(), System.IO.Path.GetRandomFileName());
            while (System.IO.Directory.Exists(folder) || System.IO.File.Exists(folder))
                folder = System.IO.Path.Combine(System.IO.Path.GetTempPath(), System.IO.Path.GetRandomFileName());

            return folder;
        }

        public static Boolean bFileExists(String strFullPath)
        {
            if (File.Exists(strFullPath))
                return true;
            else
                return false;
        }
        public static Boolean IsFileAvailable(String sFileFullPath)
        {
            try
            {
                using (Stream stream = new FileStream(sFileFullPath, FileMode.Open, FileAccess.ReadWrite, FileShare.None))
                {
                }
                return true; //file is available for write
            }
            catch { }

            return false;
        }
        public static Boolean DeleteFile(String strFullPath)
        {
            File.Delete(strFullPath);

            Boolean b = true;
            if (File.Exists(strFullPath))
                b = false;

            return b;
        }

        public static void PrepareDirectory(String strFullPath)
        {
            String[] fileNames = Directory.GetFiles(strFullPath);
            foreach (String fileName in fileNames)
                if (IsFileAvailable(fileName))
                    File.Delete(fileName);
        }
        public static void DeleteDirectory(string path)
        {
            try
            {
                foreach (string directory in Directory.GetDirectories(path))
                {
                    DeleteDirectory(directory);
                }

                try
                {
                    Directory.Delete(path, true);
                }
                catch (IOException)
                {
                    Directory.Delete(path, true);
                }
                catch (UnauthorizedAccessException)
                {
                    Directory.Delete(path, true);
                }
                catch { }
            }
            catch { }
        }

        public static Boolean areNewZoneFilesRqd()
        {
            Boolean b = false;
            int iZoneCount = 0;
            foreach (Zone z in lZone)
                iZoneCount += z.zoneID;

            String strPath = TempPath("zzz") + "id.txt";
            if (bFileExists(strPath))
            {
                StreamReader streamReader = new StreamReader(strPath);
                String inFile = streamReader.ReadToEnd();
                streamReader.Close();
                if (iZoneCount.ToString() != inFile.Trim())
                    b = true;
            }
            else
                b = true;

            if (b)
            {
                String tmpPathOnly = TempPath("zzz");
                String[] fileNames = Directory.GetFiles(tmpPathOnly);
                foreach (String fileName in fileNames)
                    if (IsFileAvailable(fileName))
                        File.Delete(fileName);

                File.WriteAllText(strPath, iZoneCount.ToString());
            }
            return b;
        }

        public static DateTime GetServerDT(String sConnStr)
        {
            String s = "select getdate()";
            DataTable dt = new Connection(sConnStr).GetDataDT(s, sConnStr);
            return (DateTime)dt.Rows[0][0];
        }

        public static String GetCreateTableScript(String strTableName)
        {
            String strSql = @"DECLARE @table_name SYSNAME
                SELECT @table_name = 'dbo." + strTableName + @"'

                DECLARE 
                      @object_name SYSNAME
                    , @object_id INT

                SELECT 
                      @object_name = '[' + s.name + '].[' + o.name + ']'
                    , @object_id = o.[object_id]
                FROM sys.objects o WITH (NOWAIT)
                JOIN sys.schemas s WITH (NOWAIT) ON o.[schema_id] = s.[schema_id]
                WHERE s.name + '.' + o.name = @table_name
                    AND o.[type] = 'U'
                    AND o.is_ms_shipped = 0

                DECLARE @SQL NVARCHAR(MAX) = ''

                ;WITH index_column AS 
                (
                    SELECT 
                          ic.[object_id]
                        , ic.index_id
                        , ic.is_descending_key
                        , ic.is_included_column
                        , c.name
                    FROM sys.index_columns ic WITH (NOWAIT)
                    JOIN sys.columns c WITH (NOWAIT) ON ic.[object_id] = c.[object_id] AND ic.column_id = c.column_id
                    WHERE ic.[object_id] = @object_id
                ),
                fk_columns AS 
                (
                     SELECT 
                          k.constraint_object_id
                        , cname = c.name
                        , rcname = rc.name
                    FROM sys.foreign_key_columns k WITH (NOWAIT)
                    JOIN sys.columns rc WITH (NOWAIT) ON rc.[object_id] = k.referenced_object_id AND rc.column_id = k.referenced_column_id 
                    JOIN sys.columns c WITH (NOWAIT) ON c.[object_id] = k.parent_object_id AND c.column_id = k.parent_column_id
                    WHERE k.parent_object_id = @object_id
                )
                SELECT @SQL = 'CREATE TABLE ' + @object_name + CHAR(13) + '(' + CHAR(13) + STUFF((
                    SELECT CHAR(9) + ', [' + c.name + '] ' + 
                        CASE WHEN c.is_computed = 1
                            THEN 'AS ' + cc.[definition] 
                            ELSE UPPER(tp.name) + 
                                CASE WHEN tp.name IN ('varchar', 'char', 'varbinary', 'binary', 'text')
                                       THEN '(' + CASE WHEN c.max_length = -1 THEN 'MAX' ELSE CAST(c.max_length AS VARCHAR(5)) END + ')'
                                     WHEN tp.name IN ('nvarchar', 'nchar', 'ntext')
                                       THEN '(' + CASE WHEN c.max_length = -1 THEN 'MAX' ELSE CAST(c.max_length / 2 AS VARCHAR(5)) END + ')'
                                     WHEN tp.name IN ('datetime2', 'time2', 'datetimeoffset') 
                                       THEN '(' + CAST(c.scale AS VARCHAR(5)) + ')'
                                     WHEN tp.name = 'decimal' 
                                       THEN '(' + CAST(c.[precision] AS VARCHAR(5)) + ',' + CAST(c.scale AS VARCHAR(5)) + ')'
                                    ELSE ''
                                END +
                                --Reza CASE WHEN c.collation_name IS NOT NULL THEN ' COLLATE ' + c.collation_name ELSE '' END +
                                CASE WHEN c.is_nullable = 1 THEN ' NULL' ELSE ' NOT NULL' END +
                                CASE WHEN dc.[definition] IS NOT NULL THEN ' DEFAULT' + dc.[definition] ELSE '' END + 
                                CASE WHEN ic.is_identity = 1 THEN ' IDENTITY(' + CAST(ISNULL(ic.seed_value, '0') AS CHAR(1)) + ',' + CAST(ISNULL(ic.increment_value, '1') AS CHAR(1)) + ')' ELSE '' END 
                        END + CHAR(13)
                    FROM sys.columns c WITH (NOWAIT)
                    JOIN sys.types tp WITH (NOWAIT) ON c.user_type_id = tp.user_type_id
                    LEFT JOIN sys.computed_columns cc WITH (NOWAIT) ON c.[object_id] = cc.[object_id] AND c.column_id = cc.column_id
                    LEFT JOIN sys.default_constraints dc WITH (NOWAIT) ON c.default_object_id != 0 AND c.[object_id] = dc.parent_object_id AND c.column_id = dc.parent_column_id
                    LEFT JOIN sys.identity_columns ic WITH (NOWAIT) ON c.is_identity = 1 AND c.[object_id] = ic.[object_id] AND c.column_id = ic.column_id
                    WHERE c.[object_id] = @object_id
                    ORDER BY c.column_id
                    FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, CHAR(9) + ' ')
                    + ISNULL((SELECT CHAR(9) + ', CONSTRAINT [' + k.name + '] PRIMARY KEY CLUSTERED(' + 
                                    (SELECT STUFF((
                                         SELECT ', [' + c.name + '] ' + CASE WHEN ic.is_descending_key = 1 THEN 'DESC' ELSE 'ASC' END
                                         FROM sys.index_columns ic WITH (NOWAIT)
                                         JOIN sys.columns c WITH (NOWAIT) ON c.[object_id] = ic.[object_id] AND c.column_id = ic.column_id
                                         WHERE ic.is_included_column = 0
                                             AND ic.[object_id] = k.parent_object_id 
                                             AND ic.index_id = k.unique_index_id     
                                         FOR XML PATH(N''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, ''))
                            + ')' + CHAR(13)
                            FROM sys.key_constraints k WITH (NOWAIT)
                            WHERE k.parent_object_id = @object_id 
                                AND k.[type] = 'PK'), '') + ')'  + CHAR(13)
                    + ISNULL((SELECT (
                        SELECT CHAR(13) +
                             'ALTER TABLE ' + @object_name + ' WITH' 
                            + CASE WHEN fk.is_not_trusted = 1 
                                THEN ' NOCHECK' 
                                ELSE ' CHECK' 
                              END + 
                              ' ADD CONSTRAINT [' + fk.name  + '] FOREIGN KEY(' 
                              + STUFF((
                                SELECT ', [' + k.cname + ']'
                                FROM fk_columns k
                                WHERE k.constraint_object_id = fk.[object_id]
                                FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, '')
                               + ')' +
                              ' REFERENCES [' + SCHEMA_NAME(ro.[schema_id]) + '].[' + ro.name + '] ('
                              + STUFF((
                                SELECT ', [' + k.rcname + ']'
                                FROM fk_columns k
                                WHERE k.constraint_object_id = fk.[object_id]
                                FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, '')
                               + ')'
                            + CASE 
                                WHEN fk.delete_referential_action = 1 THEN ' ON DELETE CASCADE' 
                                WHEN fk.delete_referential_action = 2 THEN ' ON DELETE SET NULL'
                                WHEN fk.delete_referential_action = 3 THEN ' ON DELETE SET DEFAULT' 
                                ELSE '' 
                              END
                            + CASE 
                                WHEN fk.update_referential_action = 1 THEN ' ON UPDATE CASCADE'
                                WHEN fk.update_referential_action = 2 THEN ' ON UPDATE SET NULL'
                                WHEN fk.update_referential_action = 3 THEN ' ON UPDATE SET DEFAULT'  
                                ELSE '' 
                              END 
                            + CHAR(13) + 'ALTER TABLE ' + @object_name + ' CHECK CONSTRAINT [' + fk.name  + ']' + CHAR(13)
                        FROM sys.foreign_keys fk WITH (NOWAIT)
                        JOIN sys.objects ro WITH (NOWAIT) ON ro.[object_id] = fk.referenced_object_id
                        WHERE fk.parent_object_id = @object_id
                        FOR XML PATH(N''), TYPE).value('.', 'NVARCHAR(MAX)')), '')
                    + ISNULL(((SELECT
                         CHAR(13) + 'CREATE' + CASE WHEN i.is_unique = 1 THEN ' UNIQUE' ELSE '' END 
                                + ' NONCLUSTERED INDEX [' + i.name + '] ON ' + @object_name + ' (' +
                                STUFF((
                                SELECT ', [' + c.name + ']' + CASE WHEN c.is_descending_key = 1 THEN ' DESC' ELSE ' ASC' END
                                FROM index_column c
                                WHERE c.is_included_column = 0
                                    AND c.index_id = i.index_id
                                FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, '') + ')'  
                                + ISNULL(CHAR(13) + 'INCLUDE (' + 
                                    STUFF((
                                    SELECT ', [' + c.name + ']'
                                    FROM index_column c
                                    WHERE c.is_included_column = 1
                                        AND c.index_id = i.index_id
                                    FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, '') + ')', '')  + CHAR(13)
                        FROM sys.indexes i WITH (NOWAIT)
                        WHERE i.[object_id] = @object_id
                            AND i.is_primary_key = 0
                            AND i.[type] = 2
                        FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)')
                    ), '')

                select @SQL as strQry
                --EXEC sys.sp_executesql @SQL";

            return strSql;
        }

        public static void SaveSettings(int UserID, String ucName, String objName, Stream sXML)
        {
            String filename = xmlPath() + UserID.ToString() + "_" + ucName + "_" + objName;
            Stream str = File.OpenWrite(filename);
        }

        public static Boolean ValidateLicence()
        {
            return DBConn.Common.DBGlobals.ValidateLicence();
        }

        public static int IdleTime = 0; //Time user has been connected-> initialy zero
        public static int IdleTimeToLock = 5; //Time to lock the screen in min
        public static int WaitLockTime = 1; //time before unlocking user
        public static String Active = "A";
        public static String InActive = "I";
        public static int SyncCoreTimer = 1; //Sync from local to core in mins
        public static int SyncDelLocal = 1; //Sync to delete local data
        public static bool IsSyncToCore = false;

        public static Boolean bNeedToQryArc(String sConnStr, DateTime dtFrom)
        {
            Boolean b = false;
            if (dtFrom < DateTime.Now.AddMonths(-6))
            {
                DataTable dt = new Connection(sConnStr).GetDataDT("select top 1 DateTimeGPS_UTC from GFI_ARC_GPSData order by UID desc", sConnStr);
                if (dt.Rows.Count > 0)
                {
                    DateTime d = DateTime.Now;
                    if (DateTime.TryParse(dt.Rows[0][0].ToString(), out d))
                        if (dtFrom.AddDays(-30) < d)
                            b = true;
                }
            }
            return b;
        }
        //Aanish-04 March 2014
        public static string lengthMessenger = "";

        public static DataTable GetTimerFromDB(String sConnStr)
        {
            String sqlString = @"SELECT ParamName,PValue
                                from GFI_SYS_GlobalParams
                                where ParamName IN ('IdleTime','IdleTimeToLock','WaitToLock','SyncCoreTime','SyncDelLocal')";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }


        //Aanish-02 March 2015
        #region Methods
        public static void Alphabet(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsLetter(e.KeyChar) && !char.IsWhiteSpace(e.KeyChar))
                e.Handled = true;
        }

        public static void DigitOnly(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }

        public static bool IsMandatory(RadDropDownList radDropDownList)
        {

            if (radDropDownList.SelectedIndex == -1)
                return false;
            return true;
        }

        public static bool IsMandatory(RadTextBox radTextBox)
        {
            if (radTextBox.Text.Trim().Length == 0)
                return false;
            return true;
        }

        public static bool CheckLength(RadTextBox radTextBox, int length)
        {
            if (radTextBox.Text.Length == length)
            {
                lengthMessenger = "Max length for " + radTextBox.Name.Substring(3) + " is " + length;
                return false;
            }
            return true;
        }

        public static bool IsMandatory(TextBox textBox)
        {
            if (textBox.Text.Trim().Length == 0)
                return false;
            return true;
        }

        public static bool CheckLength(TextBox textBox, int length)
        {
            if (textBox.Text.Length == length)
            {
                lengthMessenger = "Max length for " + textBox.Name.Substring(3) + " is " + length;
                return false;
            }
            return true;
        }

        public static bool IsMandatory(ComboBox comboBox)
        {
            if (comboBox.SelectedIndex == -1)
                return false;
            return true;
        }

        public static Boolean AddModule(Module lModule, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_Modules";
            if (!dt.Columns.Contains("oprType"))
                dt.Columns.Add("oprType", typeof(DataRowState));

            dt.TableName = "GFI_SYS_Modules";
            dt.Columns.Add("ModuleName", lModule.ModuleName.GetType());
            dt.Columns.Add("Description", lModule.Description.GetType());
            dt.Columns.Add("Command", lModule.Description.GetType());
            dt.Columns.Add("CreatedDate", lModule.CreatedDate.GetType());
            dt.Columns.Add("CreatedBy", lModule.CreatedBy.GetType());
            dt.Columns.Add("UpdatedDate", lModule.UpdatedDate.GetType());
            dt.Columns.Add("UpdatedBy", lModule.UpdatedBy.GetType());

            DataRow dr = dt.NewRow();
            dr["ModuleName"] = lModule.ModuleName;
            dr["Description"] = lModule.Description;
            dr["Command"] = lModule.Command;
            dr["CreatedDate"] = lModule.CreatedDate;
            dr["CreatedBy"] = lModule.CreatedBy;
            dr["UpdatedDate"] = lModule.UpdatedDate;
            dr["UpdatedBy"] = lModule.UpdatedBy;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr);
            DataSet dsProcess = new DataSet();
            DataTable CTRLTBL = _connection.CTRLTBL();
            dsProcess.Merge(dt);

            DataRow drCtrl = CTRLTBL.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "UID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "GET";
            CTRLTBL.Rows.Add(drCtrl);

            DataTable dtAccessTemplates = new NaveoOneLib.Models.AccessTemplate().GetAccessTemplates(Globals.uLogin.lMatrix, sConnStr);
            AccessProfile lAccessProfile = new AccessProfile();

            DataTable dt2 = new DataTable();
            dt2.TableName = "GFI_SYS_AccessProfiles";
            if (!dt2.Columns.Contains("oprType"))
                dt2.Columns.Add("oprType", typeof(DataRowState));

            dt2.Columns.Add("UID", lAccessProfile.UID.GetType());
            dt2.Columns.Add("TemplateId", lAccessProfile.TemplateId.GetType());
            dt2.Columns.Add("ModuleId", lAccessProfile.ModuleId.GetType());
            dt2.Columns.Add("Command", lAccessProfile.Command.GetType());
            dt2.Columns.Add("AllowRead", lAccessProfile.AllowRead.GetType());
            dt2.Columns.Add("AllowNew", lAccessProfile.AllowNew.GetType());
            dt2.Columns.Add("AllowEdit", lAccessProfile.AllowEdit.GetType());
            dt2.Columns.Add("AllowDelete", lAccessProfile.AllowDelete.GetType());
            dt2.Columns.Add("CreatedDate", lAccessProfile.CreatedDate.GetType());
            dt2.Columns.Add("CreatedBy", lAccessProfile.CreatedBy.GetType());
            dt2.Columns.Add("UpdatedDate", lAccessProfile.UpdatedDate.GetType());
            dt2.Columns.Add("UpdatedBy", lAccessProfile.UpdatedBy.GetType());

            foreach (DataRow drAccessTemplate in dtAccessTemplates.Rows)
            {
                DataRow drr = dt2.NewRow();
                drr["oprType"] = DataRowState.Added;
                //drr["UID"] = lAccessProfile.UID;
                drr["TemplateId"] = drAccessTemplate["UID"]; ;
                // drr["ModuleId"] = drAccessTemplate["UID"]; ;
                drr["Command"] = lModule.Command;
                drr["AllowRead"] = 0;
                drr["AllowNew"] = 0;
                drr["AllowEdit"] = 0;
                drr["AllowDelete"] = 0;
                drr["CreatedDate"] = Globals.GetServerDT(sConnStr);
                drr["CreatedBy"] = Globals.uLogin.Username;
                drr["UpdatedDate"] = Globals.GetServerDT(sConnStr);
                drr["UpdatedBy"] = Globals.uLogin.Username;
                dt2.Rows.Add(drr);
            }
            dsProcess.Merge(dt2);

            DataRow drCtrl2 = CTRLTBL.NewRow();
            drCtrl2["SeqNo"] = 1;
            drCtrl2["TblName"] = dt2.TableName;
            drCtrl2["CmdType"] = "TABLE";
            drCtrl2["KeyFieldName"] = "UID";
            drCtrl2["KeyIsIdentity"] = "TRUE";
            drCtrl2["NextIdAction"] = "SET";
            drCtrl2["NextIdFieldName"] = "ModuleId";
            drCtrl2["ParentTblName"] = "GFI_SYS_Modules";
            CTRLTBL.Rows.Add(drCtrl2);

            dsProcess.Merge(CTRLTBL);

            Boolean bInsert = true;

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0 || dri["oprType"].ToString() == "0")
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            DataSet dsResults = _connection.ProcessData(dsProcess, sConnStr);

            CTRLTBL = dsResults.Tables["CTRLTBL"];

            if (CTRLTBL != null)
            {
                DataRow[] dRow = CTRLTBL.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }


        public static AuditAccess SetAuditAccess(String control)
        {
            AuditAccess auditAccess = new AuditAccess();

            auditAccess.AuditUser = Globals.uLogin.Username;
            auditAccess.Control = control;
            auditAccess.AuditDate = DateTime.UtcNow;

            return auditAccess;
        }
        #endregion

        //12.09.2015-Aanish
        public static void ValidateAccessProfilesModules(String sConnStr)
        {
            bool exist;
            DataTable dtModules = new ModuleService().GetModules(sConnStr);
            DataTable dtAccessProfile = new DataTable();

            User usrInst = new User();
            usrInst = new UserService().GetUserInstaller(sConnStr);

            DataTable dtAccessTemplate = new AccessTemplate().GetAccessTemplates(usrInst.lMatrix, sConnStr);

            foreach (DataRow drAccessTemplate in dtAccessTemplate.Rows)
            {
                dtAccessProfile = new AccessProfile().GetAccessProfilesTemplateId(drAccessTemplate["UID"].ToString(), sConnStr);

                foreach (DataRow drModule in dtModules.Rows)
                {
                    exist = false;


                    //Returns true if module exist in AccessProfile
                    //try { exist = dtAccessProfile.AsEnumerable().Where(c => c.Field<int>(Convert.ToInt16("ModuleID")).Equals(Convert.ToInt32(drModule["UID"]))).Count() > 0; }
                    //catch (Exception ex)
                    //{

                    //}

                    DataRow[] dr = dtAccessProfile.Select("ModuleID = " + drModule["UID"]);
                    if (dr.Length > 0)
                    {
                        exist = true;
                    }

                    if (exist == false)
                    {
                        AccessProfile acInsert = new AccessProfile();
                        int i = 0;
                        if (int.TryParse(drAccessTemplate["UID"].ToString(), out i))
                            acInsert.TemplateId = i;
                        if (int.TryParse(drModule["UID"].ToString(), out i))
                            acInsert.ModuleId = i;
                        acInsert.Command = drModule["Command"].ToString();
                        acInsert.AllowRead = "0";
                        acInsert.AllowNew = "0";
                        acInsert.AllowEdit = "0";
                        acInsert.AllowDelete = "0";

                        DataSet ds = new DataSet();
                        acInsert.Save(acInsert, out ds, null, sConnStr);
                    }
                }
            }
        }

        #region Telerik
        //public static bool IsMandatory(RadDropDownList radDropDownList)
        //{

        //    if (radDropDownList.SelectedIndex == -1)
        //        return false;
        //    return true;
        //}
        //public static bool IsMandatory(RadTextBox radTextBox)
        //{
        //    if (radTextBox.Text.Trim().Length == 0)
        //        return false;
        //    return true;
        //}
        //public static bool CheckLength(RadTextBox radTextBox, int length)
        //{
        //    if (radTextBox.Text.Length == length)
        //    {
        //        lengthMessenger = "Max length for " + radTextBox.Name.Substring(3) + " is " + length;
        //        return false;
        //    }
        //    return true;
        //}
        #endregion
    }
}