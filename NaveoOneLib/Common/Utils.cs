﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.OleDb;
using System.Data;
using System.IO;
using System.Data.Odbc;
using Microsoft.VisualBasic;
using System.Windows.Forms;

using System.Xml;
using System.IO.Compression;
using NaveoOneLib.Models;
using System.Configuration;
using NaveoOneLib.Models.GMatrix;
using System.Drawing;

namespace NaveoOneLib.Common
{
    public class Utils
    {
        public class OptionalOut<Type>
        {
            public Type Result { get; set; }
        }
        public DataSet GenerateExcelData(String strFilePath, Boolean FirstRowHeader)
        {
            OleDbConnection oledbConn = new OleDbConnection();
            OleDbCommand cmd = new OleDbCommand(); ;
            OleDbDataAdapter oleda = new OleDbDataAdapter();
            DataSet ds = new DataSet("xlsFile");
            String strFirstRowHeader = "Yes";
            if (!FirstRowHeader)
                strFirstRowHeader = "No";
            try
            {
                /* connection string  to work with excel file. HDR=Yes - indicates 
                   that the first row contains columnnames, not data. HDR=No - indicates 
                   the opposite. "IMEX=1;" tells the driver to always read "intermixed" 
                   (numbers, dates, strings etc) data columns as text. 
                Note that this option might affect excel sheet write access negative. */
                if (Path.GetExtension(strFilePath) == ".xls")
                    oledbConn = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strFilePath + ";Extended Properties=\"Excel 8.0;HDR=" + strFirstRowHeader + ";IMEX=1\"");
                else if (Path.GetExtension(strFilePath) == ".xlsx")
                    oledbConn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + strFilePath + ";Extended Properties='Excel 12.0;HDR=" + strFirstRowHeader + ";IMEX=1;';");
                else if (Path.GetExtension(strFilePath) == ".csv")
                    oledbConn = new OleDbConnection("Provider=Microsoft.Jet.OleDb.4.0; Data Source = " + Path.GetDirectoryName(strFilePath) + ";Extended Properties=\"Text;HDR=" + strFirstRowHeader + ";FMT=Delimited\"");

                oledbConn.Open();

                cmd.Connection = oledbConn;
                cmd.CommandType = CommandType.Text;
                oleda = new OleDbDataAdapter(cmd);

                DataTable dt = oledbConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                foreach (DataRow dr in dt.Rows)
                {
                    cmd.CommandText = "SELECT * FROM [" + dr["TABLE_NAME"] + "]";
                    if (!dr["TABLE_NAME"].ToString().Contains("$_") && !dr["TABLE_NAME"].ToString().Contains("$'_"))
                        oleda.Fill(ds, dr["TABLE_NAME"].ToString());
                }
            }
            finally
            {
                oledbConn.Close();
            }
            return ds;
        }
        public void WriteExcelFile(String strFilePath, DataTable dt)
        {
            OleDbConnection oledbConn = new OleDbConnection();
            if (Path.GetExtension(strFilePath) == ".xls")
                oledbConn = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strFilePath + ";Extended Properties=\"Excel 8.0;\"");
            else if (Path.GetExtension(strFilePath) == ".xlsx")
                oledbConn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + strFilePath + ";Extended Properties='Excel 12.0;IMEX=1;';");

            oledbConn.Open();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = oledbConn;

            DataTable dtChk = oledbConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            Boolean bFound = false;
            foreach (DataRow dr in dtChk.Rows)
                if (dr["TABLE_NAME"].ToString() == dt.TableName)
                    bFound = true;

            if (dt.TableName.Trim() == String.Empty)
                dt.TableName = "DataTable";
            String sql = "CREATE TABLE [" + dt.TableName + "] (";
            if (!bFound)
            {
                //sql = "CREATE TABLE [table1] (";
                foreach (DataColumn dc in dt.Columns)
                    sql += dc.ColumnName + " VARCHAR ,";
                sql = sql.Substring(0, sql.Length - 1) + ");";
                cmd.CommandText = "CREATE TABLE [table1] (id INT, name VARCHAR, datecol DATE );";
                cmd.CommandText = sql;
                cmd.ExecuteNonQuery();
            }

            cmd.CommandText = "UPDATE [table1] SET name = 'DDDD' WHERE id = 3;";
            cmd.CommandText = "INSERT INTO [table1](id,name,datecol) VALUES(1,'AAAA','2014-01-01');";
            String s = "INSERT INTO [" + dt.TableName + "] (";
            foreach (DataColumn dc in dt.Columns)
                s += dc.ColumnName + ",";
            s = s.Substring(0, s.Length - 1) + ") VALUES(";

            //Insert a blank line
            sql = s;
            foreach (DataColumn dc in dt.Columns)
                sql += "' * ',";
            sql = sql.Substring(0, sql.Length - 1) + ");";
            cmd.CommandText = sql;
            cmd.ExecuteNonQuery();

            foreach (DataRow dr in dt.Rows)
            {
                sql = s;
                foreach (DataColumn dc in dt.Columns)
                    sql += "'" + dr[dc].ToString().Replace("'", "") + "',";
                sql = sql.Substring(0, sql.Length - 1) + ");";
                cmd.CommandText = sql;
                cmd.ExecuteNonQuery();
            }

            oledbConn.Close();
        }

        //public DataSet GetDataSetFromCSVFile(String csv_file_path)
        //{
        //    DataTable csvData = new DataTable();
        //    try
        //    {
        //        using (TextFieldParser csvReader = new TextFieldParser(csv_file_path))
        //        {
        //            csvReader.SetDelimiters(new string[] { "," });
        //            csvReader.HasFieldsEnclosedInQuotes = true;
        //            string[] colFields = csvReader.ReadFields();
        //            foreach (string column in colFields)
        //            {
        //                DataColumn datecolumn = new DataColumn(column);
        //                datecolumn.AllowDBNull = true;
        //                csvData.Columns.Add(datecolumn);
        //            }
        //            while (!csvReader.EndOfData)
        //            {
        //                string[] fieldData = csvReader.ReadFields();
        //                //Making empty value as null
        //                for (int i = 0; i < fieldData.Length; i++)
        //                {
        //                    if (fieldData[i] == "")
        //                    {
        //                        fieldData[i] = null;
        //                    }
        //                }
        //                csvData.Rows.Add(fieldData);
        //            }
        //        }
        //    }
        //    catch { }
        //    DataSet ds = new DataSet();
        //    ds.Tables.Add(csvData);
        //    return ds;
        //}
        public void WriteDTtoTxtFile(String strFilePath, DataTable dt)
        {
            File.AppendAllText(strFilePath, "******************\r\n");
            File.AppendAllText(strFilePath, "*** New Import ***\r\n");
            File.AppendAllText(strFilePath, "******************\r\n");

            foreach (DataColumn dc in dt.Columns)
                File.AppendAllText(strFilePath, dc.ColumnName + ", ");
            File.AppendAllText(strFilePath, "\r\n");

            foreach (DataRow dr in dt.Rows)
            {
                foreach (DataColumn dc in dt.Columns)
                    File.AppendAllText(strFilePath, dr[dc].ToString() + ", ");
                File.AppendAllText(strFilePath, "\r\n");
            }
        }

        public String RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            return builder.ToString();
        }

        public DataSet GenerateExcelDataOdbc(String FileName)
        {
            DataSet ds = new DataSet();
            OdbcConnection odbcConn = new OdbcConnection();
            try
            {
                String path = System.IO.Path.GetFullPath(FileName);
                odbcConn = new OdbcConnection(@" Driver={Microsoft Excel Driver (*.xls)};Dbq=" + path + ";ReadOnly=0;");
                odbcConn.Open();
                OdbcCommand cmd = new OdbcCommand(); ;
                OdbcDataAdapter oleda = new OdbcDataAdapter();
                DataTable dt = odbcConn.GetSchema("Tables");
                cmd.Connection = odbcConn;
                cmd.CommandType = CommandType.Text;
                oleda = new OdbcDataAdapter(cmd);
                foreach (DataRow dr in dt.Rows)
                {
                    cmd.CommandText = "SELECT * FROM [" + dr["TABLE_NAME"] + "]";

                    oleda.Fill(ds, "excelData");
                }
            }
            finally
            {
                odbcConn.Close();
            }
            return ds;
        }

        public void SearchDataGrid(DataGridView pDataObject, String pSearchString)
        {
            Boolean SearchUp = false;
            Boolean CaseSensitive = false;
            int col = -1;
            int maxSearches = pDataObject.Rows.Count * pDataObject.Columns.Count + 1;
            int idx = 1;
            int _Column = 0;
            int _Row = 0;
            int[] InvCols = new int[pDataObject.Columns.Count];
            for (int i = 0; i < InvCols.Length; i++)
                InvCols[i] = -1;

            int FirstVisibleCol = 0;
            for (int i = 0; i < pDataObject.Columns.Count; i++)
                if (pDataObject.Columns[i].Visible == true)
                {
                    FirstVisibleCol = i;
                    i = pDataObject.Columns.Count + 1;
                }

            if (!SearchUp)
            { if (pDataObject.CurrentRow.Index >= 0) _Row = pDataObject.CurrentRow.Index + 1; }
            else//Search up
            {
                _Row = pDataObject.CurrentRow.Index - 1;
                if (pDataObject.CurrentRow.Index == 0 || _Row <= 0) _Row = pDataObject.Rows.Count - 1;
            }

            bool isFound = false;
            bool SpecificCol = false;   // If need to search in only 1 column
            //bool ChangeColToVisible = false; // status to check if col.visible has changed to true
            bool AllCols = false;
            if (pDataObject.Visible == false) return;

            string searchValue = pSearchString.ToUpper();
            if (CaseSensitive)
                searchValue = pSearchString;
            string strDGValue;

            if (Convert.ToBoolean(pSearchString.Length))
            {
                // If the item is not found and you haven't looked at every cell, keep searching
                while ((!isFound) & (idx < maxSearches))
                {
                    // Only search visible cells
                    if (col >= 0) { _Column = col; SpecificCol = true; }
                    if (col == -1) // All cols including invisible ones
                        AllCols = true;
                    else
                        AllCols = pDataObject.Columns[_Column].Visible;

                    if (AllCols)
                    {
                        // Do all comparing in UpperCase so it is case insensitive
                        if (_Row < pDataObject.Rows.Count - 1)
                        {
                            strDGValue = pDataObject[_Column, _Row].Value.ToString().ToUpper();
                            if (CaseSensitive)
                                strDGValue = pDataObject[_Column, _Row].Value.ToString();

                            if (strDGValue.Contains(searchValue))
                            {
                                // If found position on the item
                                pDataObject.FirstDisplayedScrollingRowIndex = _Row;
                                pDataObject[_Column, _Row].Selected = true;
                                pDataObject.CurrentCell = pDataObject[_Column, _Row];
                                isFound = true;
                            }
                        }
                    }

                    // Increment the column.
                    _Column++;

                    // If it exceeds the column count
                    if (_Column == pDataObject.Columns.Count || SpecificCol == true)
                    {
                        _Column = 0; //Go to 0 column
                        if (SearchUp)// either go to last row or previous row
                            if (_Row == 0)
                                _Row = pDataObject.Rows.Count - 1;
                            else
                                --_Row;
                        else
                            _Row++;      //Go to the next row

                        // If it exceeds the row count
                        if (_Row == pDataObject.Rows.Count)
                        {
                            _Row = 0; //Start over at the top
                        }
                        SpecificCol = false;
                    }
                    idx++;
                }
            }
        }

        public DataTable dtGroupBy(string i_sGroupByColumn, string i_sAggregateColumn, DataTable i_dSourceTable)
        {

            DataView dv = new DataView(i_dSourceTable);

            //getting distinct values for group column
            DataTable dtGroup = dv.ToTable(true, new string[] { i_sGroupByColumn });

            //adding column for the row count
            dtGroup.Columns.Add("Count", typeof(int));
            dtGroup.Columns.Add("Sum", typeof(int));

            //looping thru distinct values for the group, counting
            foreach (DataRow dr in dtGroup.Rows)
            {
                dr["Count"] = i_dSourceTable.Compute("Count(" + i_sAggregateColumn + ")", i_sGroupByColumn + " = '" + dr[i_sGroupByColumn] + "'");
                dr["Sum"] = i_dSourceTable.Compute("Sum(" + i_sAggregateColumn + ")", i_sGroupByColumn + " = '" + dr[i_sGroupByColumn] + "'");
            }

            //returning grouped/counted result
            return dtGroup;
        }

        public TimeSpan GetTimeSpan(String strTimeZoneID)
        {
            TimeSpan ts = new TimeSpan(0);
            foreach (TimeZoneInfo z in TimeZoneInfo.GetSystemTimeZones())
                if (z.Id.ToUpper().Trim() == strTimeZoneID.ToUpper().Trim())
                    return z.BaseUtcOffset;
            return ts;
        }

        public static DateTime GetStartOfLastWeek()
        {
            int DaysToSubtract = (int)DateTime.Now.DayOfWeek + 7;
            DateTime dt = DateTime.Now.Subtract(TimeSpan.FromDays(DaysToSubtract));
            return new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0, 0);
        }
        public static DateTime GetEndOfLastWeek()
        {
            DateTime dt = GetStartOfLastWeek().AddDays(6);
            return new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 59, 999);
        }
        public static DateTime GetStartOfCurrentWeek()
        {
            int DaysToSubtract = (int)DateTime.Now.DayOfWeek;
            DateTime dt = DateTime.Now.Subtract(TimeSpan.FromDays(DaysToSubtract));
            return new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0, 0);
        }
        public static DateTime GetEndOfCurrentWeek()
        {
            DateTime dt = GetStartOfCurrentWeek().AddDays(6);
            return new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 59, 999);
        }

        public static DateTime FirstDateInWeek(DateTime dt, DayOfWeek weekStartDay)
        {
            while (dt.DayOfWeek != weekStartDay)
                dt = dt.AddDays(-1);
            return new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0, 0);
        }
        public static DateTime LastDateInWeek(DateTime dt, DayOfWeek weekStartDay)
        {
            DateTime dtr = FirstDateInWeek(dt, weekStartDay).AddDays(6);
            return new DateTime(dtr.Year, dtr.Month, dtr.Day, 23, 59, 59, 999);
        }
        public static DateTime FirstDateInLastWeek(DateTime dt, DayOfWeek weekStartDay)
        {
            return FirstDateInWeek(DateTime.Now, DayOfWeek.Monday).AddDays(-7);
        }
        public static DateTime LastDateInLastWeek(DateTime dt, DayOfWeek weekStartDay)
        {
            DateTime dtr = FirstDateInLastWeek(dt, weekStartDay).AddDays(6);
            return new DateTime(dtr.Year, dtr.Month, dtr.Day, 23, 59, 59, 999);
        }
        public static DateTime FirstDayOfMonth(DateTime dt)
        {
            return new DateTime(dt.Year, dt.Month, 1);
        }

        public static Boolean ChkConfigFile(String sConfig)
        {
            Configuration c = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);//System.Windows.Forms.Application.ExecutablePath);
            foreach (KeyValueConfigurationElement s in c.AppSettings.Settings)
                if (s.Key == sConfig)
                    return true;

            return false;
        }
        public static void UpdateConfigFile()
        {
            Configuration c = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);//System.Windows.Forms.Application.ExecutablePath);
            Boolean bFound = false;
            Boolean bSave = false;
            foreach (KeyValueConfigurationElement s in c.AppSettings.Settings)
                if (s.Key == "UseWebService")
                {
                    bFound = true;
                    break;
                }
            if (!bFound)
            {
                c.AppSettings.Settings.Add("UseWebService", "Y");
                bSave = true;
                bFound = false;
            }
            c.Save(ConfigurationSaveMode.Minimal, false);
            if (bSave)
                ConfigurationManager.RefreshSection("appSettings");

            #region WebURL
            c = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            bFound = false;
            bSave = false;
            foreach (KeyValueConfigurationElement s in c.AppSettings.Settings)
                if (s.Key == "WebURL")
                {
                    bFound = true;
                    break;
                }
            if (!bFound)
            {
                c.AppSettings.Settings.Add("WebURL", "http://localhost:59831/EngService.asmx");
                bSave = true;
                bFound = false;
            }
            c.Save(ConfigurationSaveMode.Minimal, false);
            if (bSave)
                ConfigurationManager.RefreshSection("appSettings");
            #endregion

            #region DB2
            c = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            bFound = false;
            bSave = false;
            foreach (ConnectionStringSettings s in c.ConnectionStrings.ConnectionStrings)
                if (s.Name == "DB2")
                {
                    bFound = true;
                    break;
                }
            if (!bFound)
            {
                c.ConnectionStrings.ConnectionStrings.Add(new ConnectionStringSettings("DB2", "Data Source=RezaHP\\SQLExpress;Initial Catalog=cCoreTs;User ID=sa;Pwd=pass;Connect Timeout=200;", "System.Data.SqlClient"));
                bSave = true;
                bFound = false;
            }
            c.Save(ConfigurationSaveMode.Minimal, false);
            if (bSave)
                ConfigurationManager.RefreshSection("ConnectionStrings");
            #endregion

            #region DB3
            c = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            bFound = false;
            bSave = false;
            foreach (ConnectionStringSettings s in c.ConnectionStrings.ConnectionStrings)
                if (s.Name == "DB3")
                {
                    bFound = true;
                    break;
                }
            if (!bFound)
            {
                c.ConnectionStrings.ConnectionStrings.Add(new ConnectionStringSettings("DB3", "Data Source=RezaHP\\SQLExpress;Initial Catalog=cCoreTs;User ID=sa;Pwd=pass;Connect Timeout=200;", "System.Data.SqlClient"));
                bSave = true;
                bFound = false;
            }
            c.Save(ConfigurationSaveMode.Minimal, false);
            if (bSave)
                ConfigurationManager.RefreshSection("ConnectionStrings");
            #endregion

            #region DB4
            c = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            bFound = false;
            bSave = false;
            foreach (ConnectionStringSettings s in c.ConnectionStrings.ConnectionStrings)
                if (s.Name == "DB4")
                {
                    bFound = true;
                    break;
                }
            if (!bFound)
            {
                c.ConnectionStrings.ConnectionStrings.Add(new ConnectionStringSettings("DB4", "Data Source=RezaHP\\SQLExpress;Initial Catalog=cCoreTs;User ID=sa;Pwd=pass;Connect Timeout=200;", "System.Data.SqlClient"));
                bSave = true;
                bFound = false;
            }
            c.Save(ConfigurationSaveMode.Minimal, false);
            if (bSave)
                ConfigurationManager.RefreshSection("ConnectionStrings");
            #endregion

            #region DB5
            c = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            bFound = false;
            bSave = false;
            foreach (ConnectionStringSettings s in c.ConnectionStrings.ConnectionStrings)
                if (s.Name == "DB5")
                {
                    bFound = true;
                    break;
                }
            if (!bFound)
            {
                c.ConnectionStrings.ConnectionStrings.Add(new ConnectionStringSettings("DB5", "Data Source=RezaHP\\SQLExpress;Initial Catalog=cCoreTs;User ID=sa;Pwd=pass;Connect Timeout=200;", "System.Data.SqlClient"));
                bSave = true;
                bFound = false;
            }
            c.Save(ConfigurationSaveMode.Minimal, false);
            if (bSave)
                ConfigurationManager.RefreshSection("ConnectionStrings");
            #endregion

            #region WebExcep
            c = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            bFound = false;
            bSave = false;
            foreach (KeyValueConfigurationElement s in c.AppSettings.Settings)
                if (s.Key == "Excep")
                {
                    bFound = true;
                    break;
                }
            if (!bFound)
            {
                c.AppSettings.Settings.Add("Excep", "SYSTEM");
                bSave = true;
                bFound = false;
            }
            c.Save(ConfigurationSaveMode.Minimal, false);
            if (bSave)
                ConfigurationManager.RefreshSection("appSettings");
            #endregion

            #region EsriMapFile
            c = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            bFound = false;
            bSave = false;
            foreach (KeyValueConfigurationElement s in c.AppSettings.Settings)
                if (s.Key == "EsriMapFile")
                {
                    bFound = true;
                    break;
                }
            if (!bFound)
            {
                c.AppSettings.Settings.Add("EsriMapFile", "webMarch2014.egp");
                bSave = true;
                bFound = false;
            }
            c.Save(ConfigurationSaveMode.Minimal, false);
            if (bSave)
                ConfigurationManager.RefreshSection("appSettings");
            #endregion

            #region Proxy
            //UseProxy
            c = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            bFound = false;
            bSave = false;
            foreach (KeyValueConfigurationElement s in c.AppSettings.Settings)
                if (s.Key == "UseProxy")
                {
                    bFound = true;
                    break;
                }
            if (!bFound)
            {
                c.AppSettings.Settings.Add("UseProxy", "No");
                bSave = true;
                bFound = false;
            }
            c.Save(ConfigurationSaveMode.Minimal, false);
            if (bSave)
                ConfigurationManager.RefreshSection("appSettings");

            //ProxyDomain
            c = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            bFound = false;
            bSave = false;
            foreach (KeyValueConfigurationElement s in c.AppSettings.Settings)
                if (s.Key == "ProxyDomain")
                {
                    bFound = true;
                    break;
                }
            if (!bFound)
            {
                c.AppSettings.Settings.Add("ProxyDomain", "");
                bSave = true;
                bFound = false;
            }
            c.Save(ConfigurationSaveMode.Minimal, false);
            if (bSave)
                ConfigurationManager.RefreshSection("appSettings");

            //ProxyUserName
            c = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            bFound = false;
            bSave = false;
            foreach (KeyValueConfigurationElement s in c.AppSettings.Settings)
                if (s.Key == "ProxyUserName")
                {
                    bFound = true;
                    break;
                }
            if (!bFound)
            {
                c.AppSettings.Settings.Add("ProxyUserName", "");
                bSave = true;
                bFound = false;
            }
            c.Save(ConfigurationSaveMode.Minimal, false);
            if (bSave)
                ConfigurationManager.RefreshSection("appSettings");

            //ProxyPassword
            c = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            bFound = false;
            bSave = false;
            foreach (KeyValueConfigurationElement s in c.AppSettings.Settings)
                if (s.Key == "ProxyPassword")
                {
                    bFound = true;
                    break;
                }
            if (!bFound)
            {
                c.AppSettings.Settings.Add("ProxyPassword", "EncryptedPassword");
                bSave = true;
                bFound = false;
            }
            c.Save(ConfigurationSaveMode.Minimal, false);
            if (bSave)
                ConfigurationManager.RefreshSection("appSettings");

            //ProxyAddress
            c = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            bFound = false;
            bSave = false;
            foreach (KeyValueConfigurationElement s in c.AppSettings.Settings)
                if (s.Key == "ProxyAddress")
                {
                    bFound = true;
                    break;
                }
            if (!bFound)
            {
                c.AppSettings.Settings.Add("ProxyAddress", "192.168.66.1");
                bSave = true;
                bFound = false;
            }
            c.Save(ConfigurationSaveMode.Minimal, false);
            if (bSave)
                ConfigurationManager.RefreshSection("appSettings");

            //ProxyPort
            c = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            bFound = false;
            bSave = false;
            foreach (KeyValueConfigurationElement s in c.AppSettings.Settings)
                if (s.Key == "ProxyPort")
                {
                    bFound = true;
                    break;
                }
            if (!bFound)
            {
                c.AppSettings.Settings.Add("ProxyPort", "8783");
                bSave = true;
                bFound = false;
            }
            c.Save(ConfigurationSaveMode.Minimal, false);
            if (bSave)
                ConfigurationManager.RefreshSection("appSettings");
            #endregion
        }
        public static Boolean DBCreationNeeded()
        {
            Boolean b = false;
            String sCoreConnStr = ConfigurationManager.ConnectionStrings["DB1"].ToString();
            if (sCoreConnStr.ToUpper().Trim() == "DATA SOURCE=NEEDED")
                b = true;
            return b;
        }

        public static DataTable ReadTxtFile(String sPath, Boolean bHasHeaders)
        {
            String[] fileContents = File.ReadAllLines(sPath);
            DataTable table = new DataTable();
            if (fileContents.Length == 0)
                return table;

            String[] Header = fileContents[0].Split(',');
            foreach (String s in Header)
                table.Columns.Add();

            if (bHasHeaders)
                for (int i = 0; i < table.Columns.Count; i++)
                    table.Columns[i].ColumnName = Header[i];

            int j = 0;
            foreach (String s in fileContents)
            {
                if (bHasHeaders)
                    if (j == 0)
                    {
                        j++;
                        continue;
                    }
                DataRow dr = table.NewRow();
                String[] columns = s.Split(',');
                dr.ItemArray = (object[])columns;
                table.Rows.Add(dr);
            }
            return table;
        }
        public static DataTable GetDataTableFromCsv(string path, bool isFirstRowHeader)
        {
            string header = isFirstRowHeader ? "Yes" : "No";

            string pathOnly = Path.GetDirectoryName(path);
            string fileName = Path.GetFileName(path);

            string sql = @"SELECT * FROM [" + fileName + "]";

            using (OleDbConnection connection = new OleDbConnection(
                      @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + pathOnly +
                      ";Extended Properties=\"Text;HDR=" + header + "\""))
            using (OleDbCommand command = new OleDbCommand(sql, connection))
            using (OleDbDataAdapter adapter = new OleDbDataAdapter(command))
            {
                DataTable dataTable = new DataTable();
                try
                {
                    dataTable.Locale = System.Globalization.CultureInfo.CurrentCulture;
                    adapter.Fill(dataTable);
                }
                catch { }
                return dataTable;
            }
        }

        public static void ExportDataTableToExcel(DataTable dt, String sFN)
        {
            StringBuilder sb = new StringBuilder();

            IEnumerable<string> columnNames = dt.Columns.Cast<DataColumn>().
                                              Select(column => column.ColumnName);
            sb.AppendLine(string.Join(",", columnNames));

            foreach (DataRow row in dt.Rows)
            {
                IEnumerable<string> fields = row.ItemArray.Select(field => field.ToString());
                sb.AppendLine(string.Join(",", fields));
            }

            File.WriteAllText(sFN, sb.ToString());
        }
        public static String MCBToExcel(String sPath)
        {
            //Open MCB PDF
            //Copy all lines and paste in txt
            //Move Opening and closing balances to new line
            //indent them with 6 tabs and format as follows
            //						Opening Balance 8,014.71

            String[] fileContents = System.IO.File.ReadAllLines(sPath);
            String x = String.Empty;
            Double Tot = 0;

            foreach (String lc in fileContents)
            {
                String[] g = lc.Split(' ');
                if (g.Length > 0)
                    if (g[0].Length > 0)
                    {
                        if (g[0].Substring(0, 1) == "\t")
                        {
                            x += "01/07/2000; 01/07/2000; 0; " + g[2] + "; " + g[0] + g[1] + "\r\n";
                            Double.TryParse(g[2], out Tot);
                        }
                        else
                        {
                            String s = String.Empty;
                            s += g[0] + "; " + g[1];
                            Double amt = 0;
                            Double.TryParse(g[2], out amt);

                            Double LineTotal = 0;
                            Double.TryParse(g[3], out LineTotal);

                            if (Math.Round(Tot - amt, 2) == LineTotal)
                            {
                                s += g[0] + "; " + g[1] + "; ";
                                for (int i = 4; i < g.Length; i++)
                                {
                                    if (g[i].StartsWith("FT"))
                                        x += ";";
                                    if (g[i].StartsWith("MG"))
                                        x += ";";
                                    if (g[i].StartsWith("Consolidated"))
                                        x += ";";

                                    x += g[i] + " ";
                                }
                                s += amt + "; 0; " + LineTotal + "\r\n";
                            }
                            else
                            {
                                s += g[0] + "; " + g[1] + "; ";
                                for (int i = 4; i < g.Length; i++)
                                {
                                    if (g[i].StartsWith("FT"))
                                        x += ";";
                                    if (g[i].StartsWith("MG"))
                                        x += ";";
                                    if (g[i].StartsWith("Consolidated"))
                                        x += ";";

                                    x += g[i] + " ";
                                }
                                s += " 0; " + amt + "; " + LineTotal + "\r\n";
                            }
                            Tot = LineTotal;

                            x += s;
                        }
                    }
            }

            return x;
        }

        public static string GetXMLFromObject(object o)
        {
            StringWriter sw = new StringWriter();
            XmlTextWriter tw = null;
            try
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(o.GetType());
                tw = new XmlTextWriter(sw);
                serializer.Serialize(tw, o);
            }
            catch
            {
                //Handle Exception Code
            }
            finally
            {
                sw.Close();
                if (tw != null)
                {
                    tw.Close();
                }
            }
            return sw.ToString();
        }
        public static Object GetObjectFromXML(string xml, Type objectType)
        {
            StringReader strReader = null;
            System.Xml.Serialization.XmlSerializer serializer = null;
            XmlTextReader xmlReader = null;
            Object obj = null;
            try
            {
                strReader = new StringReader(xml);
                serializer = new System.Xml.Serialization.XmlSerializer(objectType);
                xmlReader = new XmlTextReader(strReader);
                obj = serializer.Deserialize(xmlReader);
            }
            catch
            {
                //Handle Exception Code
            }
            finally
            {
                if (xmlReader != null)
                {
                    xmlReader.Close();
                }
                if (strReader != null)
                {
                    strReader.Close();
                }
            }
            return obj;
        }

        public static Color GeneratePastelColors()
        {
            Random _random = new Random();
            int r = _random.Next(0, 128);
            int g = _random.Next(0, 128);
            int b = _random.Next(0, 128);

            // to create lighter colours:
            // take a random integer between 0 & 128 (rather than between 0 and 255)
            // and then add 127 to make the colour lighter
            byte[] colorBytes = new byte[3];
            colorBytes[0] = (byte)(r + 127);
            colorBytes[1] = (byte)(g + 127);
            colorBytes[2] = (byte)(b + 127);

            Color color = new Color();
            color = Color.FromArgb(255, colorBytes[0], colorBytes[1], colorBytes[2]);

            return color;
        }
    }
    public static class DataTableExtensions
    {
        /// <summary>
        /// SetOrdinal of DataTable columns based on the index of the columnNames array. Removes invalid column names first.
        /// </summary>
        /// <param name="table"></param>
        /// <param name="columnNames"></param>
        /// <remarks> http://stackoverflow.com/questions/3757997/how-to-change-datatable-colums-order</remarks>
        public static void SetColumnsOrder(this DataTable table, params String[] columnNames)
        {
            //for (int columnIndex = 0; columnIndex < columnNames.Length; columnIndex++)
            //{
            //    table.Columns[columnNames[columnIndex]].SetOrdinal(columnIndex);
            //}

            List<string> listColNames = columnNames.ToList();

            //Remove invalid column names.
            foreach (string colName in columnNames)
            {
                if (!table.Columns.Contains(colName))
                {
                    listColNames.Remove(colName);
                }
            }

            foreach (string colName in listColNames)
            {
                table.Columns[colName].SetOrdinal(listColNames.IndexOf(colName));
            }
        }
    }

    public class myConverter
    {
        //DataTable TriList = new myConverter().ListToDataTable<TripHeader>(lTrip);

        public DataTable ListToDataTable<T>(IList<T> varlist)
        {
            DataTable dt = new DataTable();

            //special handling for value types and string
            //In value type, the DataTable is expected to contain the values of all the variables (items) present in List.
            //Hence I create only one column in the DataTable named “Values”,
            //Though String is a reference type, due to its behavior 
            //I treat it as a special case and handle it as value type only.
            if (typeof(T).IsValueType || typeof(T).Equals(typeof(string)))
            {
                DataColumn dc = new DataColumn("Values");

                dt.Columns.Add(dc);

                foreach (T item in varlist)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = item;
                    dt.Rows.Add(dr);
                }
            }
            //for reference types other than  string
            // Used PropertyInfo class of System.Reflection
            else
            {

                //find all the public properties of this Type using reflection
                System.Reflection.PropertyInfo[] propT = typeof(T).GetProperties();

                foreach (System.Reflection.PropertyInfo pi in propT)
                {
                    //create a datacolumn for each property
                    Type t = pi.PropertyType;
                    if (t.Name.Contains("Nullable"))
                    {
                        if (t.UnderlyingSystemType.FullName.Contains("System.Int32"))
                            t = typeof(int);
                        else if (t.UnderlyingSystemType.FullName.Contains("System.DateTime"))
                            t = typeof(DateTime);
                        else if (t.UnderlyingSystemType.FullName.Contains("System.Single"))
                            t = typeof(Double);
                        else if (t.UnderlyingSystemType.FullName.Contains("System.Double"))
                            t = typeof(Double);
                    }

                    DataColumn dc = new DataColumn(pi.Name, t);
                    dt.Columns.Add(dc);
                }

                //now we iterate through all the items , take the corresponding values and add a new row in dt
                //for (int item = 0; item < varlist.Count(); item++)
                for (int item = 0; item < varlist.Count; item++)
                {
                    DataRow dr = dt.NewRow();

                    for (int property = 0; property < propT.Length; property++)
                    {
                        if (propT[property].GetValue(varlist[item], null) == null)
                            dr[property] = DBNull.Value;
                        else
                            dr[property] = propT[property].GetValue(varlist[item], null);
                    }

                    dt.Rows.Add(dr);
                }
            }

            if (typeof(T) == typeof(Matrix))
                dt.Columns.Remove("GMDesc");
            dt.AcceptChanges();
            return dt;
        }
        /// <summary>
        /// Converts DatATable into List
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="table"></param>
        /// <returns></returns>
        public List<T> ConvertToList<T>(DataTable table)
        {
            if (table == null)
                return null;
            if (table.Rows.Count == 0)
                return new List<T>();
            List<DataRow> rows = new List<DataRow>();
            foreach (DataRow row in table.Rows)
                rows.Add(row);
            return ConvertTo<T>(rows);
        }
        /// <summary>
        /// Converts DataRows in T type list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="rows"></param>
        /// <returns></returns>
        public List<T> ConvertTo<T>(List<DataRow> rows)
        {
            List<T> list = null;
            if (rows != null)
            {
                list = new List<T>();
                foreach (DataRow row in rows)
                {
                    T item = CreateItem<T>(row);
                    list.Add(item);
                }
            }
            return list;
        }
        /// <summary>
        /// Convert DataRow into T Object
        /// </summary>   
        public T CreateItem<T>(DataRow row)
        {
            string columnName;
            T obj = default(T);
            if (row != null)
            {
                //Create the instance of type T
                obj = Activator.CreateInstance<T>();
                foreach (DataColumn column in row.Table.Columns)
                {
                    columnName = column.ColumnName;
                    //Get property with same columnName
                    System.Reflection.PropertyInfo prop = obj.GetType().GetProperty(columnName);
                    try
                    {
                        //Getting null when using a temporary #Table in sql for [RowNumber] [int] IDENTITY(1,1) NOT NULL
                        if (prop == null)
                        {
                            prop = obj.GetType().GetProperty(columnName,
                                        System.Reflection.BindingFlags.SetProperty |
                                        System.Reflection.BindingFlags.IgnoreCase |
                                        System.Reflection.BindingFlags.Public |
                                        System.Reflection.BindingFlags.Instance);

                            if (prop == null)
                                continue;
                        }

                        if (prop.PropertyType.FullName == "System.TimeSpan")
                        {
                            if (row[columnName] != DBNull.Value)
                            {
                                //Get value for the column
                                object value = TimeSpan.FromSeconds(Convert.ToDouble(row[columnName]));
                                //Set property value
                                prop.SetValue(obj, value, null);
                            }
                            else
                                prop.SetValue(obj, null, null);
                        }
                        else if (prop.PropertyType.FullName == "System.Single")
                        {
                            if (row[columnName] != DBNull.Value)
                            {
                                //Get value for the column
                                object value = (float)(Convert.ToDouble(row[columnName]));
                                //Set property value
                                prop.SetValue(obj, value, null);
                            }
                            else
                                prop.SetValue(obj, null, null);
                        }
                        else
                        {
                            if (row[columnName] != DBNull.Value)
                            {
                                //Get value for the column
                                object value = (row[columnName].GetType() == typeof(DBNull)) ? null : row[columnName];
                                //Set property value
                                prop.SetValue(obj, value, null);
                            }
                            else
                                prop.SetValue(obj, null, null);
                        }
                    }
                    catch
                    {
                        prop.SetValue(obj, null, null);
                        //throw new Exception(ex.Message);
                    }
                }
            }
            return obj;
        }
    }

    public class StringToStream
    {
        /// <summary>
        /// Get Byte[] from String
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
        /// <summary>
        /// Get Stream from String
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static Stream GetStream(string str)
        {
            return new MemoryStream(GetBytes(str));
        }
    }
}
