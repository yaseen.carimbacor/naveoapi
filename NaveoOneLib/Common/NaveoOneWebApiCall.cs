﻿using NaveoOneLib.Models.Users;
using NaveoOneLib.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace NaveoOneLib.Common
{
    public static class NaveoOneWebApiCall
    {
        static String Post = "POST";
        static String Get = "GET";
        static HttpWebRequest httpWebRequest(String Method, String url)
        {
            Ssl.EnableTrustedHosts();

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = Method;
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:2.0) Gecko/20100101 Firefox/4.0";
            request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            request.ContentType = "application/json";
            //request.Timeout = 5000;

            //request.Headers.Add("X-APPLICATION-CODE", "NAVEO");

            return request;
        }

        class siteLogin
        {
            public User user { get; set; }
            public String companyToken { get; set; }
        }
        static String Login(String Site)
        {
            String responseString = String.Empty;

            try
            {
                //Body
                siteLogin st = new siteLogin();
                User user = new User();
                user.Email = "Support@naveo.mu";

                String jsData = JsonConvert.SerializeObject(st);
                byte[] body = Encoding.ASCII.GetBytes(jsData);

                HttpWebRequest request = httpWebRequest(Post, Site + "2.0/Login");
                request.ContentLength = body.Length;

                //Sending
                using (System.IO.Stream stream = request.GetRequestStream())
                {
                    stream.Write(body, 0, body.Length);
                }

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                responseString = response.StatusDescription;
            }
            catch (Exception ex)
            {
                responseString = ex.ToString();
            }
            return responseString;
        }
    }
}
