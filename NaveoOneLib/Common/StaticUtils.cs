﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace NaveoOneLib.Common
{
    public static class StaticUtils
    {
        public static List<DateTime> GetDatesBetween(DateTime dtFr, DateTime dtTo, List<DayOfWeek> days)
        {
            var dates = new List<DateTime>();

            for (var dt = dtFr; dt <= dtTo; dt = dt.AddDays(1))
            {
                foreach (DayOfWeek d in days)
                    if (dt.DayOfWeek == d)
                        dates.Add(dt);
            }

            return dates;
        }

        public static String GetWorkHrs(String sWorkHrs)
        {
            //weeksdays, Starting >= time, Ending <= time
            String sDays = String.Empty;

            String[] sSplit = sWorkHrs.Split(',');
            foreach(String s in sSplit)
            {
                switch (s.Trim().ToLower())
                {
                    case "sunday":
                        sDays += " or DATEPART(dw, h.dtStart) = 1 ";
                        break;

                    case "monday":
                        sDays += " or DATEPART(dw, h.dtStart) = 2 ";
                        break;

                    case "tuesday":
                        sDays += " or DATEPART(dw, h.dtStart) = 3 ";
                        break;

                    case "wednesday":
                        sDays += " or DATEPART(dw, h.dtStart) = 4 ";
                        break;

                    case "thursday":
                        sDays += " or DATEPART(dw, h.dtStart) = 5 ";
                        break;

                    case "friday":
                        sDays += " or DATEPART(dw, h.dtStart) = 6 ";
                        break;

                    case "saturday":
                        sDays += " or DATEPART(dw, h.dtStart) = 7 ";
                        break;
                }
            }

            if (sDays != String.Empty)
            {
                sDays = sDays.Remove(0, 3);
                sDays = " and (" + sDays + ")";
            }

            String sTime = String.Empty;
            foreach (String s in sSplit)
            {
                if(s.ToLower().Contains("starting"))
                {
                    String g = s.Replace("starting", String.Empty).Trim();
                    String[] h = g.Split(' ');

                    sTime += " and CAST(h.dtStart as time) " + h[0] + " '" + h[1] + "'";
                }
                else if (s.ToLower().Contains("ending"))
                {
                    String g = s.Replace("ending", String.Empty).Trim();
                    String[] h = g.Split(' ');

                    sTime += " and CAST(h.dtStart as time) " + h[0] + " '" + h[1] + "'";
                }

            }

            if (sTime != String.Empty)
            {
                sTime = sTime.Remove(0, 4);
                sTime = " and (" + sTime + ")";
            }

            return sDays + " " + sTime;
        }

        public static Bitmap ChangeColor(Bitmap scrBitmap, Color newColor)
        {
            Color actualColor;
            //make an empty bitmap the same size as scrBitmap. 
            //197:105   197:222
            //325:105   327:222
            Bitmap newBitmap = new Bitmap(scrBitmap.Width, scrBitmap.Height);
            for (int i = 0; i < scrBitmap.Width; i++)
            {
                for (int j = 0; j < scrBitmap.Height; j++)
                {
                    //get the pixel from the scrBitmap image
                    actualColor = scrBitmap.GetPixel(i, j);
                    // > 150 because.. Images edges can be of low pixel colr. if we set all pixel color to new then there will be no smoothness left.
                    if (actualColor.A > 150)
                        newBitmap.SetPixel(i, j, newColor);
                    else
                        newBitmap.SetPixel(i, j, actualColor);
                }
            }
            return newBitmap;
        }

        public static bool TryParseJson<T>(this string obj, out T result)
        {
            try
            {
                // Validate missing fields of object
                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.MissingMemberHandling = MissingMemberHandling.Error;

                result = JsonConvert.DeserializeObject<T>(obj, settings);
                return true;
            }
            catch (Exception)
            {
                result = default(T);
                return false;
            }
        }

    }
}
