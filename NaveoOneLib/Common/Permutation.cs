﻿// C# program to print all 
// permutations of a given string. 
using System;
using System.Collections.Generic;
using System.Linq;

public class Permutation
{
    /** 
	* permutation function 
	* @param str string to 
	calculate permutation for 
	* @param l starting index 
	* @param r end index 
	*/
    List<String> lResult = new List<string>();

    public List<String> permute(List<String> str, int l, int r)
    {
        if (l == r)
        {
            String innerList = String.Empty;
            foreach (String s in str)
                innerList += s + " ";

            lResult.Add(innerList);
        }
        else
        {
            for (int i = l; i <= r; i++)
            {
                str = swap(str, l, i);
                permute(str, l + 1, r);
                str = swap(str, l, i);
            }
        }

        return lResult;
    }

    /** 
	* Swap Characters at position 
	* @param a string value 
	* @param i position 1 
	* @param j position 2 
	* @return swapped string 
	*/
    public static List<String> swap(List<String> a, int i, int j)
    {
        String temp;
        String[] charArray = a.ToArray();
        temp = charArray[i];
        charArray[i] = charArray[j];
        charArray[j] = temp;
        return charArray.ToList();
    }

    // Driver Code 
    public static void Main()
    {
        List<String> str = new List<string> { "A", "B", "C" };
        List<String> ls = new Permutation().permute(str, 0, str.Count - 1);
    }
}

// This code is contributed by mits 
