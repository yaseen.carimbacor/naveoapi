﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Documents.Spreadsheet.Formatting.FormatStrings.Infos;

namespace NaveoOneLib.Models.Maintenance
{
    public class AssetMaintenanceDetailDto
    {
        public int URI { get; set; }
        public string MaintenanceType { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string CreatedBy { get; set; }
        public string Comment { get; set; }
        public Nullable<int> AssetId { get; set; }
        public List<lstPartDetails> lstParts { get; set; }
        public Boolean Action { get; set; }
        public List<String> PicNames { get; set; }

    }
    public class lstPartDetails
    {
        public string ItemCode { get; set; }
        public Nullable<decimal> Quantity { get; set; }
        public Nullable<decimal> UnitCost { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }





    public class CPMmaintenance
    {


        public string AssetCode { get; set; }

        public string MaintenanceType { get; set; }

        public string Status { get; set; }

        public DateTime DateFrom { get; set; }

        public DateTime DateTo { get; set; }

        public int Percentage { get; set; }

       public String Field1 { get; set; }

       public String Field2 { get; set; }

       public String Field3 { get; set; }

    }
}
