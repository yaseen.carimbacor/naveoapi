
using System.Data;

namespace NaveoOneLib.Models.Plannings
{
    public class ScheduledAsset
    {
        public int iID { get; set; }
        public int HID { get; set; }
        public int? AssetID { get; set; }
        public int? DriverID { get; set; }
        public DataRowState OprType { get; set; }
    }
}