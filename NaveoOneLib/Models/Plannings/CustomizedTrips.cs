﻿using NaveoOneLib.Models.GMatrix;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoOneLib.Models.Plannings
{
    public class CustomizedTrips
    {
        public int iID { get; set; }
        public int? AssetID { get; set; }
        public int? DriverID { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo{ get; set; }
        public String Description { get; set; }
        public int? ZoneID { get; set; }
        public int? LookUpValue { get; set; }
        public int? IsStopped { get; set; }
        public int? CountAsWorkHour { get; set; }
        public int? Duration { get; set; } //InMinutes
        public String Field1 { get; set; }
        public String Field2 { get; set; }
        public String Field3 { get; set; }
        public String Field4 { get; set; }
        public String Field5 { get; set; }
        public String Field6 { get; set; }

        public int? PID { get; set; }
        public DataRowState OprType { get; set; }
        //public String Source { get; set; }
        //public int? SourceID { get; set; }

       
    }
}

