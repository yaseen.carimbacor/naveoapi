using System;
using System.Data;

namespace NaveoOneLib.Models.Plannings
{
    public class PlanningViaPointD
    {
        public int iID { get; set; }
        public int HID { get; set; }
        public int? UOM { get; set; }
        public string uomDesc { get; set; }
        public int? Capacity { get; set; }
        public String Remarks { get; set; }
        public DataRowState oprType { get; set; }
    }
}