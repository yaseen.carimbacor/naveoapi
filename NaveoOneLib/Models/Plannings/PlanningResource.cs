using System;
using System.Data;

namespace NaveoOneLib.Models.Plannings
{
    public class PlanningResource
    {
        public int iID { get; set; }
        public int PID { get; set; }
        public int? iSource { get; set; }
        public int DriverID { get; set; }
        public string DriverName { get; set; }
        public String ResourceType { get; set; }
        public int? CustomType { get; set; }
        public int? MileageAllowance { get; set; }
        public int? TravelGrant { get; set; }
        public string Remarks { get; set; }
        public DataRowState oprType { get; set; }
    }
}