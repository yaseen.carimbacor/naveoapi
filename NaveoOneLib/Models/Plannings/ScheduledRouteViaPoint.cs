﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoOneLib.Models.Plannings
{
    public class ScheduledRouteViaPoint
    {
        public int iID { get; set; }
        public int HID { get; set; }
        public int ViaPointHId { get; set; }
        public string ViaPointName { get; set; }
        public int SeqNo { get; set; }
        public DateTime  dtUTC { get; set; }
        public DataRowState OprType { get; set; }
    }
}
