using System;
using System.Data;

namespace NaveoOneLib.Models.Plannings
{
    public class PlanningLkup
    {
        public int iID { get; set; }
        public int PID { get; set; }
        public int LookupValueID { get; set; }
        public string LookupValueDesc { get; set; }
        public String LookupType { get; set; }
        public DataRowState oprType { get; set; }
    }
}