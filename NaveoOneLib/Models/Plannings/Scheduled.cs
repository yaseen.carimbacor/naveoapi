using System;
using System.Collections.Generic;
using NaveoOneLib.Models.GMatrix;
using System.Data;

namespace NaveoOneLib.Models.Plannings
{
    public class Scheduled
    {
        public int HID { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public String Remarks { get; set; }

        public List<ScheduledAsset> lSchAsset { get; set; }
        public List<ScheduledPlan> lSchPlan { get; set; }

        public List<PlanningResource> lAdditionalResources = new List<PlanningResource>();

        public List<ScheduledRouteViaPoint> lSchRouteViaPoints = new List<ScheduledRouteViaPoint>();
        public List<Matrix> lMatrix { get; set; }

        public int? AssetSelectionID { get; set; }
        public int? DriverSelectionID { get; set; }
        public int? Status { get; set; }
        public int? intField { get; set; }

        public DriverNotification driverNotification { get; set; }

        public DataRowState OprType { get; set; }
    }

    public class DriverNotification
    {
        public String sEmail { get; set; }

        public String sMobile { get; set; }
        public String sCountryCode { get; set; }

        public List<String> lCoordinates { get; set; }
        public List<coordinatesSummary> lSummary { get; set; }
        public DateTime currentDate { get; set; }
    }

    public class rebrandlyBaseDTO
    {
        public String destination { get; set; }

        public String shortUrl { get; set; }
    }

    public class coordinatesSummary
    {
        public String description { get; set; }
        public DateTime time { get; set; }
    }
}