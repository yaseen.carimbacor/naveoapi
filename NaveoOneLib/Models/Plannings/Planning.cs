using System;
using System.Collections.Generic;
using System.Data;
using NaveoOneLib.Models.GMatrix;


namespace NaveoOneLib.Models.Plannings
{
    public class Planning
    {
        public int PID { get; set; }
        public int? Approval { get; set; }
        public String Remarks { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }

        public int urgent { get; set; }
        public String requestCode { get; set; }
        public String parentRequestCode { get; set; }
        public String type { get; set; }
        public String sComments { get; set; }
        public String closureComments { get; set; }
        public String Ref { get; set; }
        public int ApprovalID { get; set; }
        public DataRowState OprType { get; set; }

        public List<Matrix> lMatrix { get; set; } = new List<Matrix>();
        public List<PlanningLkup> lPlanningLkup { get; set; } = new List<PlanningLkup>();
        public List<PlanningResource> lPlanningResource { get; set; } = new List<PlanningResource>();
        public List<PlanningViaPointH> lPlanningViaPointH { get; set; } = new List<PlanningViaPointH>();
    }
}