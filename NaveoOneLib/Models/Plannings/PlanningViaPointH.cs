using System;
using System.Collections.Generic;
using System.Data;

namespace NaveoOneLib.Models.Plannings
{
    public class PlanningViaPointH
    {
        public int iID { get; set; }
        public int PID { get; set; }
        public int SeqNo { get; set; }
        public int? ZoneID { get; set; }
        public Double? Lat { get; set; }
        public Double? Lon { get; set; }
        public int? LocalityVCA { get; set; }
        public int? Buffer { get; set; }
        public String Remarks { get; set; }
        public DateTime? dtUTC { get; set; }
        public int? minDuration { get; set; }
        public int? maxDuration { get; set; }

        public string ZoneDesc { get; set; }
        public string LocalityVCADesc { get; set; }
        public DataRowState oprType { get; set; }
        public List<PlanningViaPointD> lDetail { get; set; } = new List<PlanningViaPointD>();
    }
}