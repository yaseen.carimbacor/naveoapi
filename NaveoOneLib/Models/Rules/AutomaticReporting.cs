﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoOneLib.Models.Rules
{
    public class AutomaticReporting
    {


        public List<AutomaticReportingConfig> automaticReporting { get; set; } = new List<AutomaticReportingConfig>();

    }



    public class AutomaticReportingConfig
    {
        public List<Tree> tree { get; set; } = new List<Tree>();

        public int ARID { get; set; }
        public Recepients recepients { get; set; }

        public List<Recepients> lrecepients { get; set; }
        public List<OtherValues> other { get; set; }
        public int? targetId { get; set; }
        public String targetType { get; set; }
        public int? reportId { get; set; }
        public int? reportPeriod { get; set; }
        public String triggerTime { get; set; }
        public String triggerInterval { get; set; }
        public String reportFormat { get; set; }
        public String reportType { get; set; }
        public String reportPath { get; set; }
        public int saveToDisk { get; set; }
        public DataRowState oprType { get; set; }

    }
    public class TreeSelection
    {
        public int ruleId { get; set; }
        public string type { get; set; }
        public List<int> lIds { get; set; } = new List<int>();
        public int id { get; set; }
        public DataRowState oprType { get; set; }

    }
    public class Group
    {

        public string group { get; set; }
        public List<TreeSelection> lGroup { get; set; } = new List<TreeSelection>();
    }
    public class Tree
    {

        public List<Group> lTreeSelections { get; set; } = new List<Group>();

    }
    public class Recepients
    {
        public int ARDID { get; set; }
        public int ARID { get; set; }
        public String type { get; set; }
        public List<int> lIds { get; set; } = new List<int>();

        public DataRowState oprType { get; set; }

    }
    public class OtherValues
    {
        public string id { get; set; }
        public string value { get; set; }


    }

}
