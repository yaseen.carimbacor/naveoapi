using System;
using System.Collections.Generic;
using System.Data;
using NaveoOneLib.Common;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Reportings;


namespace NaveoOneLib.Models.Rules
{
    public class Rule
    {
        public int RuleID { get; set; }
        public int ParentRuleID { get; set; }
        public int RuleType { get; set; }
        public int MinTriggerValue { get; set; }
        public int MaxTriggerValue { get; set; }
        public String Struc { get; set; }
        public String StrucValue { get; set; }
        public String StrucCondition { get; set; }
        public String RuleName { get; set; }
        public String StrucType { get; set; }

        public List<AutoReportingConfig> NotifyList { get; set; } = new List<AutoReportingConfig>(); // has data when parentruleid = ruleid
        public List<Matrix> lMatrix { get; set; } = new List<Matrix>();
        public List<Rule> lRules { get; set; } = new List<Rule>();

        public DataRowState oprType { get; set; }
    }



    public class notifyList
    {
        public int userId { get; set; }

        public int notificationType { get; set; }

        public String category { get; set; }
    }





    public class AlertCount
    {
        public int totalRuleCount {get;set;}
        public int drivingRuleCount { get; set; }
        public int engineRuleCount { get; set; }
        public int planningRuleCount { get; set; }
        public int fuelRuleCount { get; set; }
        public int maintenanceRuleCount { get; set; }


    }
}