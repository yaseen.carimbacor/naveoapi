﻿using System;
using System.Collections.Generic;
namespace NaveoOneLib.Models.Rules
{
    public partial class Notification
    {
        public int NID { get; set; }
        public int ARID { get; set; }
        public int ExceptionID { get; set; }
        public int Status { get; set; }
        public DateTime InsertedAt { get; set; }
        public DateTime SendAt { get; set; }

        public List<Notification> lNotification { get; set; }
    }
}
