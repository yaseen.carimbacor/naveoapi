using System;
using System.Collections.Generic;
using System.Data;
using NaveoOneLib.Common;
using Org.BouncyCastle.Math;

namespace NaveoOneLib.Models.Rules
{
    public class RuleException
    {
        private int iid = Constants.NullInt;
        private int ruleid = Constants.NullInt;
        private Int64 gpsdataid = Constants.NullInt;
        private int assetid = Constants.NullInt;
        private int driverid = Constants.NullInt;
        private String severity = Constants.NullString;

        public int iID { get { return iid; } set { iid = value; } }
        public int RuleID { get { return ruleid; } set { ruleid = value; } }
        public Int64 GPSDataID { get { return gpsdataid; } set { gpsdataid = value; } }
        public int AssetID { get { return assetid; } set { assetid = value; } }
        public int DriverID { get { return driverid; } set { driverid = value; } }
        public String Severity { get { return severity; } set { severity = value; } }

    }




    public class PlanningExceptions
    {

        public int iID { get; set; }
        public int RuleID { get; set; }
        public Int64 GPSDataID { get; set; }
        public int DriverID { get; set; }
        public int AssetID { get; set; }
        public BigInteger GroupID { get; set; }
        public int Posted { get; set; }
        public DateTime InsertedAt { get; set; }
        public DateTime DateTimeGPS_UTC { get; set; }
        public String Severity { get; set; }

        public DataRowState oprtype { get; set; }

        public String ExceptionType { get; set; }
        public DateTime ExpectedTime { get; set; }

        public DateTime AcutalTime { get; set; }

        public string Durations { get; set; }

        public int Buffer { get; set; }

        public string viapoint { get; set; }

        public string AlertStatus { get; set; }

    }


}

