﻿using System;
using System.Data;

namespace NaveoOneLib.Models.Rules
{
    public class RuleExceptionReport
    {
        private int parentruleid;
        private String rulename;
        private int assetid;
        private String assetname;
        private Double maxspeed;
        private TimeSpan duration;
        private DateTime dtexception;
        private String address;
        private String zone;
        private DataTable dtdetails;

        public int ParentRuleID { get { return parentruleid; } set { parentruleid = value; } }
        public String RuleName { get { return rulename; } set { rulename = value; } }
        public int AssetID { get { return assetid; } set { assetid = value; } }
        public String AssetName { get { return assetname; } set { assetname = value; } }
        public Double MaxSpeed { get { return maxspeed; } set { maxspeed = value; } }
        public TimeSpan Duration { get { return duration; } set { duration = value; } }
        public DateTime dtException { get { return dtexception; } set { dtexception = value; } }
        public String Address { get { return address; } set { address = value; } }
        public String Zone { get { return zone; } set { zone = value; } }
        public DataTable dtDetails { get { return dtdetails; } set { dtdetails = value; } }
    }
}
