﻿using System;
using System.Collections.Generic;
using System.Data;
using NaveoOneLib.Models.GMatrix;


namespace NaveoOneLib.Models.Drivers
{
    public class Driver
    {
        public enum ResourceType
        {
              Driver
            , Officer
            , Attendant
            , Loader
            , Other
            , All
            , TransportAgent
        };

        public enum LicenseType
        {
            Private, Service
        }

        public enum LicenseCategory
        { 
            Car ,Van, Ambulance, Lorry, Bus, Heavy ,Others
        }

        public int DriverID { get; set; }

        public String sDriverName { get; set; }
        public String iButton { get; set; }
        public String sEmployeeNo { get; set; }
        public String sComments { get; set; }
        public String EmployeeType { get; set; }

        public String Gender { get; set; }
        public String Address1 { get; set; }
        public String Address2 { get; set; }
        public String City { get; set; }
        public String Nationality { get; set; }
        public String sFirtName { get; set; }
        public String LicenseNo1 { get; set; }
        public String LicenseNo2 { get; set; }
        public String Ref1 { get; set; }
        public String Status { get; set; }
        public String Address3 { get; set; }
        public String District { get; set; }
        public String PostalCode { get; set; }
        public String MobileNumber { get; set; }
        public String Reason { get; set; }
        public String Department { get; set; }
        public String Email { get; set; }
        public String sLastName { get; set; }
        public String OfficerLevel { get; set; }
        public String OfficerTitle { get; set; }
        public String Phone { get; set; }
        public String CountryCode { get; set; }
        public String Title { get; set; }
        public String ASPUser { get; set; }

        public DateTime? DateOfBirth { get; set; }
        public DateTime? LicenseNo1Expiry { get; set; }
        public DateTime? Licenseno2Expiry { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }

        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public int? UserId { get; set; }
        public int? ZoneID { get; set; }

        public int? Type_Institution { get; set; }
        public int? Type_Driver { get; set; }
        public int? Type_Grade { get; set; }
        public int? HomeNo { get; set; }
        public byte[] Photo { get; set; }

        public string Type_InstitutionDesc { get; set; }
        public string Type_DriverDesc { get; set; }
        public string Type_GradeDesc { get; set; }
        public string UserDesc { get; set; }
        public string CityDesc { get; set; }
        public string  DistrictDesc { get; set; }
        public string ZoneDesc { get; set; }

        public DataRowState oprType { get; set; }

        public List<ResourceLicense> Licenses { get; set; } = new List<ResourceLicense>();

        public List<Matrix> lMatrix { get; set; } = new List<Matrix> { };
    }
}
