using System;
using System.Data;


namespace NaveoOneLib.Models.Drivers
{
    public class ResourceLicense
    {
        public int iID { get; set; }
        public int DriverID { get; set; }
        public String sType { get; set; }
        public String sCategory { get; set; }
        public String sNumber { get; set; }
        public DateTime? IssueDate { get; set; }
        public DateTime? ExpiryDate { get; set; }

        public DataRowState oprType { get; set; }
    }
}