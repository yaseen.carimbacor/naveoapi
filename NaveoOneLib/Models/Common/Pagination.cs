﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoOneLib.Models.Common
{
    public class Pagination
    {
        public int RowFrom { get; set; }
        public int RowTo { get; set; }

        public static Pagination GetRowNums(int? Page, int? LimitPerPage)
        {
            if (!Page.HasValue)
                return new Pagination { RowFrom = 1, RowTo = int.MaxValue };
            else
                if (!LimitPerPage.HasValue)
                    LimitPerPage = 50;

            int to = Page.Value * LimitPerPage.Value;
            int fr = to - LimitPerPage.Value + 1;

            Pagination p = new Pagination { RowFrom = fr, RowTo = to };
            return p;
        }
    }
}
