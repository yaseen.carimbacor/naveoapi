using System;
using NaveoOneLib.Common;


namespace NaveoOneLib.Models.Common
{
    public class ProcessPending
    {
        private int iid = Constants.NullInt;
        private int assetid = Constants.NullInt;
        private DateTime dtdatefrom = Constants.NullDateTime;
        private String processcode = Constants.NullString;
        private int processing = Constants.NullInt;


        public int iID { get { return iid; } set { iid = value; } }
        public int AssetID { get { return assetid; } set { assetid = value; } }
        public DateTime dtDateFrom { get { return dtdatefrom; } set { dtdatefrom = value; } }
        public String ProcessCode { get { return processcode; } set { processcode = value; } }
        public int Processing { get { return processing; } set { processing = value; } }
    }
}