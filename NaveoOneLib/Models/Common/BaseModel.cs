﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoOneLib.Models.Common
{
    public class BaseModel
    {
        public dynamic data { get; set; }
        public dynamic additionalData { get; set; }
        public String errorMessage { get; set; }
        public DataSet dbResult { get; set; }

        public int total { get; set; }
        public String AdditionalInfo { get; set; }
    }

    //public class GisData
    //{
    //    public String gisType { get; set; }
    //    public List<int> gisIds { get; set; }
    //}
}