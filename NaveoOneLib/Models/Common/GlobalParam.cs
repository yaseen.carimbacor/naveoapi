using System;
using NaveoOneLib.Common;


namespace NaveoOneLib.Models.Common
{
     public class GlobalParam
     {
          private int iid = Constants.NullInt;
          private String paramname = Constants.NullString;
          private String pvalue = Constants.NullString;
          private String paramcomments = Constants.NullString;

          public int Iid { get { return iid; } set { iid = value; } }
          public String ParamName { get { return paramname; } set { paramname = value; } }
          public String PValue { get { return pvalue; } set { pvalue = value; } }
          public String ParamComments { get { return paramcomments; } set { paramcomments = value; } }
     }

    public class UpdateGlobalParam
    {
        private String paramname = Constants.NullString;
        private String pvalue = Constants.NullString;
        private String paramcomments = Constants.NullString;

        public String ParamName { get { return paramname; } set { paramname = value; } }
        public String PValue { get { return pvalue; } set { pvalue = value; } }
        public String ParamComments { get { return paramcomments; } set { paramcomments = value; } }
    }
}