﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NaveoOneLib.Constant;
using NaveoOneLib.Models.Permissions;

namespace NaveoOneLib.Models.Common
{
    class Common
    {
    }

    public enum NaveoModels
    {
        Groups,
        Assets,
        Drivers,
        Zones,
        Rules,
        Users,
        ZoneTypes,
        PlanningRequest,
        ScheduledPlanning,
        BLUP,
        ResourceDriver,
        Roles,
        FixedAssets,
        Districts,
        PlanningRule,
        MaintenanceRule,
        FuelRule
    }

    public class dtData
    {
        public DataTable Data { get; set; }

        public DataSet dsData = null;
        public int Rowcnt { get; set; }

    }


}
