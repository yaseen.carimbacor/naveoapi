using System;
using NaveoOneLib.Common;


namespace NaveoOneLib.Models.Common
{
    public class UOM
    {
        private int uid = Constants.NullInt;
        private String description = Constants.NullString;

        public int UID { get { return uid; } set { uid = value; } }
        public String Description { get { return description; } set { description = value; } }
    }
}