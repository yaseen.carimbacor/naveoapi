﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoOneLib.Models.Maps
{
    public class RoutingData
    {
        public Guid UserToken { get; set; }
        public List<lonLatCoordinate> lPoints { get; set; } = new List<lonLatCoordinate>();
        public double searchRadiusInMeters { get; set; } = 150;
    }

    public class lonLatCoordinate
    {
        public Double lon { get; set; }
        public Double lat { get; set; }
    }
}
