using System;
using System.Collections.Generic;
using NaveoOneLib.Common;


namespace NaveoOneLib.Models.GPS
{
    public class GPSData
    {
        public DateTime dtLocalTime { get; set; }
        public DateTime DateTimeGPS_UTC { get; set; }
        public DateTime LastReportedInLocalTime { get; set; }
        public DateTime DateTimeServer { get; set; }

        public List<GPSDataDetail> GPSDataDetails { get; set; }

        public String iButton { get; set; }

        public int UID { get; set; }
        public int AssetID { get; set; } = -1;
        public int DriverID { get; set; } = 1;
        public int LongLatValidFlag { get; set; } = 1;

        public String DeviceID { get; set; }
        public Double Longitude { get; set; }
        public Double Latitude { get; set; }
        public Double Speed { get; set; }
        public int EngineOn { get; set; }
        public int StopFlag { get; set; }
        public Double TripDistance { get; set; }
        public Double TripTime { get; set; }
        public int WorkHour { get; set; }
        public String AssetNum { get; set; }
        public String DirectionImage { get; set; }
        public String Availability { get; set; }

        public int? RoadSpeed { get; set; }
        public Fuels.Fuel lastFuel { get; set; }

        public String Zone { get; set; }

        public int MaintenanceStatus { get; set; }

        public decimal LastOdometer { get; set; }
    }
}

