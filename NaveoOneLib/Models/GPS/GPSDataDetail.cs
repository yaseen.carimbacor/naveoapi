using System;
using NaveoOneLib.Common;


namespace NaveoOneLib.Models.GPS
{
    public class GPSDataDetail
    {

        private Int64 gpsdetailid;
        private int uid = Constants.NullInt;
        private String typeid = Constants.NullString;
        private String typevalue = Constants.NullString;
        private String uom = Constants.NullString;
        private String remarks = Constants.NullString;


        public Int64 GPSDetailID { get { return gpsdetailid; } set { gpsdetailid = value; } }
        public int UID { get { return uid; } set { uid = value; } }
        public String TypeID { get { return typeid; } set { typeid = value; } }
        public String TypeValue { get { return typevalue; } set { typevalue = value; } }
        public String UOM { get { return uom; } set { uom = value; } }
        public String Remarks { get { return remarks; } set { remarks = value; } }
    }
}
