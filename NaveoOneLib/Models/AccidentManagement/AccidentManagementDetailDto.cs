﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoOneLib.Models.AccidentManagement
{
    public class AccidentManagementDetailDto
    {
        public int Acc_id { get; set; }
        public int AssetID { get; set; }
        public Nullable<System.DateTime> AccidentDate { get; set; }
        public int DriverID { get; set; }
        public string DriverContactNo { get; set; }
        public string SectionDepartment { get; set; }
        public string AccidentSiteLocation { get; set; }
        public string GPSLocation { get; set; }
        public Boolean ReportedToASF { get; set; }
        public Boolean ReportedToPoliceStation { get; set; }
        public Boolean MemoFromHOD { get; set; }
        public string OB { get; set; }
        public string OtherPartyInvolved { get; set; }
        public Boolean TransportRequest { get; set; }
        public string RequestStatus { get; set; }
        public Boolean CustomTripDetails { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public Boolean AuthorisedTrip { get; set; }
        public Boolean AuthorisedOfficer { get; set; }
        public double AssessmentOfDamageCost { get; set; }
        public double AssessorTravelingCost { get; set; }
        public DateTime DateOfEstimatesSent { get; set; }
        public DateTime DateOfSurvey { get; set; }
        public string SurveyorName { get; set; }
        public string SurveyorContactNo { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
        public Boolean flagDelete { get; set; }
        public List<lstOtherPartyInvolved> lstPartyInvolved { get; set; }
        public List<lstNameOfPersonAccompanying> lstNameOfPerson { get; set; }
        public List<String> PicNames { get; set; }
    }


    public class lstAccidentManagementDto
    {

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

    }

    public class getAccidentMngDto
    {
        public int Acc_id { get; set; }
    }

    public class getAssetIdDto
    {
        public int AssetId { get; set; }
    }

    public class lstOtherPartyInvolved
    {
        public int VID { get; set; }
        public string Others { get; set; }
        public string Registration { get; set; }

    }

    public class lstNameOfPersonAccompanying
    {
        public int DriverID { get; set; }
        public string Authorised { get; set; }
        public string Status { get; set; }
        public string AuthorisingOfficer { get; set; }
        public string Action { get; set; }
    }
}
