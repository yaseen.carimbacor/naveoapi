using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using NaveoOneLib.Common;
using NaveoOneLib.Services;
using System.Data.Common;

namespace NaveoOneLib.Models
{
    public class SecuMonitorToBSeen
    {
        private int assetid = Constants.NullInt;
        private string displayname = string.Empty;

        public int AssetID { get { return assetid; } set { assetid = value; } }
        public String DisplayName { get { return displayname; } set { displayname = value; } }

        public DataTable GetSecuMonitorToBSeen(String sConnStr)
        {
            return new SecuMonitorToBSeenService().GetSecuMonitorToBSeen(sConnStr);
        }
        public SecuMonitorToBSeen GetSecuMonitorToBSeenById(String ID, String sConnStr)
        {
            return new SecuMonitorToBSeenService().GetSecuMonitorToBSeenById(ID, sConnStr);
        }

        public Boolean Save(SecuMonitorToBSeen SSecuMonitorToBSeen, Boolean bInsert, String sConnStr)
        {
            return new SecuMonitorToBSeenService().SaveSecuMonitorToBSeen(SSecuMonitorToBSeen, bInsert, sConnStr);
        }
        public Boolean Update(SecuMonitorToBSeen SSecuMonitorToBSeen, String sConnStr)
        {
            return new SecuMonitorToBSeenService().SaveSecuMonitorToBSeen(SSecuMonitorToBSeen, false, sConnStr);
        }
        public Boolean Delete(SecuMonitorToBSeen SSecuMonitorToBSeen, String sConnStr)
        {
            return new SecuMonitorToBSeenService().DeleteSecuMonitorToBSeen(SSecuMonitorToBSeen, sConnStr);
        }
    }
}