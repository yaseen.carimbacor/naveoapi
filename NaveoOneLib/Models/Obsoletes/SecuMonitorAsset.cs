using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using NaveoOneLib.Common;
using NaveoOneLib.Services;
using System.Data.Common;

namespace NaveoOneLib.Models
{
    public class SecuMonitorAsset
    {
        private int iid = Constants.NullInt;
        private String displayname = Constants.NullString;
        private String deviceid = Constants.NullString;
        private String timezoneid = Constants.NullString;
        private String panicid = Constants.NullString;
        private String panicvalue = Constants.NullString;

        public int iID { get { return iid; } set { iid = value; } }
        public String DisplayName { get { return displayname; } set { displayname = value; } }
        public String DeviceID { get { return deviceid; } set { deviceid = value; } }
        public String TimeZoneID { get { return timezoneid; } set { timezoneid = value; } }
        public String PanicID { get { return panicid; } set { panicid = value; } }
        public String PanicValue { get { return panicvalue; } set { panicvalue = value; } }

        public String GetAllSecuMonitorDevices(String sConnStr)
        {
            return new SecuMonitorAssetsService().GetAllSecuMonitorDevices(sConnStr);
        }
        public DataTable GetSecuMonitorAssets(String sConnStr)
        {
            return new SecuMonitorAssetsService().GetSecuMonitorAssets(sConnStr);
        }
        public SecuMonitorAsset GetSecuMonitorAssetsById(String ID, String sConnStr)
        {
            return new SecuMonitorAssetsService().GetSecuMonitorAssetsById(ID, sConnStr);
        }

        public Boolean Save(SecuMonitorAsset SSecuMonitorAssets, Boolean bInsert, String sConnStr)
        {
            return new SecuMonitorAssetsService().SaveSecuMonitorAssets(SSecuMonitorAssets, bInsert, sConnStr);
        }
        public Boolean Update(SecuMonitorAsset SSecuMonitorAssets, String sConnStr)
        {
            return new SecuMonitorAssetsService().SaveSecuMonitorAssets(SSecuMonitorAssets, false, sConnStr);
        }
        public Boolean Delete(SecuMonitorAsset SSecuMonitorAssets, String sConnStr)
        {
            return new SecuMonitorAssetsService().DeleteSecuMonitorAssets(SSecuMonitorAssets, sConnStr);
        }
    }
}