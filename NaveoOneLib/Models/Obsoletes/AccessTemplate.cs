using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Common;
using NaveoOneLib.Services;
using NaveoOneLib.Models.GMatrix;



namespace NaveoOneLib.Models
{
    public class AccessTemplate
    {

        private int uid = Constants.NullInt;
        private String templatename = Constants.NullString;
        private String description = Constants.NullString;
        private DateTime createddate = Constants.NullDateTime;
        private String createdby = Constants.NullString;
        private DateTime updateddate = Constants.NullDateTime;
        private String updatedby = Constants.NullString;

        public List<Matrix> lMatrix { get; set; }

        public int UID { get { return uid; } set { uid = value; } }
        public String TemplateName { get { return templatename; } set { templatename = value; } }
        public String Description { get { return description; } set { description = value; } }
        public DateTime CreatedDate { get { return createddate; } set { createddate = value; } }
        public String CreatedBy { get { return createdby; } set { createdby = value; } }
        public DateTime UpdatedDate { get { return updateddate; } set { updateddate = value; } }
        public String UpdatedBy { get { return updatedby; } set { updatedby = value; } }


        public AccessTemplate ATemplate(DataRow dr)
        {
            return new AccessTemplateService().ATemplate(dr);
        }
        public DataSet GetDetails(int iUID, String sConnStr)
        {
            return new AccessTemplateService().GetDetails(iUID, sConnStr);
        }
        public DataTable GetAccessTemplates(List<Matrix> lMatrix, String sConnStr)
        {
            return new AccessTemplateService().GetAccessTemplates(lMatrix, sConnStr);
        }
        public AccessTemplate GetAccessTemplatesById(String ID, String sConnStr)
        {
            return new AccessTemplateService().GetAccessTemplatesById(ID, sConnStr);
        }

        public Boolean Save(AccessTemplate SAccessTemplates, Boolean bInsert, DataTable dtAccessProfiles, out DataSet dsResults, String sConnStr)
        {
            return new AccessTemplateService().SaveAccessTemplates(SAccessTemplates, bInsert, dtAccessProfiles, out dsResults, sConnStr);
        }
        public Boolean Save(AccessTemplate SAccessTemplates, Boolean bInsert, DataTable dtAccessProfiles, String sConnStr)
        {
            DataSet dsResults = new DataSet(); 
            return new AccessTemplateService().SaveAccessTemplates(SAccessTemplates, bInsert, dtAccessProfiles, out dsResults, sConnStr);
        }

        public bool Delete(AccessTemplate SAccessTemplates, DataTable dtAccessProfiles, out DataSet dsResults, String sConnStr)
        {
            return new AccessTemplateService().DeleteAccessTemplates(SAccessTemplates, dtAccessProfiles, out dsResults, sConnStr);
        }
    }
}