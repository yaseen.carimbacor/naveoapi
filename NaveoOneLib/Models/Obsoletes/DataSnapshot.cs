using System;
using System.Collections.Generic;
using System.Data;
using NaveoOneLib.Common;
using NaveoOneLib.Services;
using System.Data.Common;

namespace NaveoOneLib.Models
{
    public partial class DataSnapshot
    {
        private int uid = Constants.NullInt;
        private int groupingid = Constants.NullInt;
        private String fieldname = Constants.NullString;
        private String fieldvalue = Constants.NullString;
        private String createdby = Constants.NullString;
        private DateTime createddate = Constants.NullDateTime;
        private String updatedby = Constants.NullString;
        private DateTime updateddate = Constants.NullDateTime;

        public int UId { get { return uid; } set { uid = value; } }
        public int GroupingId { get { return groupingid; } set { groupingid = value; } }
        public String FieldName { get { return fieldname; } set { fieldname = value; } }
        public String FieldValue { get { return fieldvalue; } set { fieldvalue = value; } }
        public String CreatedBy { get { return createdby; } set { createdby = value; } }
        public DateTime CreatedDate { get { return createddate; } set { createddate = value; } }
        public String UpdatedBy { get { return updatedby; } set { updatedby = value; } }
        public DateTime UpdatedDate { get { return updateddate; } set { updateddate = value; } }

        public DataTable GetDataSnapshort(String sConnStr)
        {
            return new DataSnapshotService().GetDataSnapshot(sConnStr);
        }
        public DataSnapshot GetDataSnapshortById(String ID, String sConnStr)
        {
            return new DataSnapshotService().GetDataSnapshotById(ID, sConnStr);
        }
        public DataTable GetDataSnapshortByGroupId(String ID, String sConnStr)
        {
            return new DataSnapshotService().GetDataSnapshotByGroupId(ID, sConnStr);
        }

        public bool Save(DataSnapshot SDataSnapshort, Boolean bInsert, String sConnStr)
        {
            return new DataSnapshotService().SaveDataSnapshot(SDataSnapshort, bInsert, sConnStr);
        }

        public bool Save(List<DataSnapshot> lDataSnapshort, Boolean bInsert, String sConnStr)
        {
            return new DataSnapshotService().SaveDataSnapshot(lDataSnapshort, bInsert, sConnStr);
        }

        public bool Update(DataSnapshot SDataSnapshort, String sConnStr)
        {
            return new DataSnapshotService().UpdateDataSnapshot(SDataSnapshort, null, sConnStr);
        }
        public bool Update(DataSnapshot SDataSnapshort, DbTransaction transaction, String sConnStr)
        {
            return new DataSnapshotService().UpdateDataSnapshot(SDataSnapshort, transaction, sConnStr);
        }
        public bool Delete(DataSnapshot SDataSnapshort, String sConnStr)
        {
            return new DataSnapshotService().DeleteDataSnapshot(SDataSnapshort, sConnStr);
        }
    }
}
