using System;
using System.Data;
using NaveoOneLib.Common;
using NaveoOneLib.Services;
using System.Data.Common;

namespace NaveoOneLib.Models
{
    public class AccessProfile
    {

        private int uid = Constants.NullInt;
        private int templateid = Constants.NullInt;
        private int moduleid = Constants.NullInt;
        private String command = Constants.NullString;
        private String allowread = Constants.NullString;
        private String allownew = Constants.NullString;
        private String allowedit = Constants.NullString;
        private String allowdelete = Constants.NullString;
        private DateTime createddate = Constants.NullDateTime;
        private String createdby = Constants.NullString;
        private DateTime updateddate = Constants.NullDateTime;
        private String updatedby = Constants.NullString;

        public int UID { get { return uid; } set { uid = value; } }
        public int TemplateId { get { return templateid; } set { templateid = value; } }
        public int ModuleId { get { return moduleid; } set { moduleid = value; } }
        public String Command { get { return command; } set { command = value; } }
        public String AllowRead { get { return allowread; } set { allowread = value; } }
        public String AllowNew { get { return allownew; } set { allownew = value; } }
        public String AllowEdit { get { return allowedit; } set { allowedit = value; } }
        public String AllowDelete { get { return allowdelete; } set { allowdelete = value; } }
        public DateTime CreatedDate { get { return createddate; } set { createddate = value; } }
        public String CreatedBy { get { return createdby; } set { createdby = value; } }
        public DateTime UpdatedDate { get { return updateddate; } set { updateddate = value; } }
        public String UpdatedBy { get { return updatedby; } set { updatedby = value; } }

        public static DataTable initAccessProfile()
        {
            DataTable dtAccessProfiles = new DataTable();
            dtAccessProfiles.Columns.Add("UID", typeof(int));
            dtAccessProfiles.Columns.Add("TemplateId", typeof(int));
            dtAccessProfiles.Columns.Add("ModuleId", typeof(int));
            dtAccessProfiles.Columns.Add("Command", typeof(String));
            dtAccessProfiles.Columns.Add("AllowRead", typeof(int));
            dtAccessProfiles.Columns.Add("AllowNew", typeof(int));
            dtAccessProfiles.Columns.Add("AllowEdit", typeof(int));
            dtAccessProfiles.Columns.Add("AllowDelete", typeof(int));
            dtAccessProfiles.Columns.Add("CreatedDate", typeof(DateTime));
            dtAccessProfiles.Columns.Add("CreatedBy", typeof(String));
            dtAccessProfiles.Columns.Add("UpdatedDate", typeof(DateTime));
            dtAccessProfiles.Columns.Add("UpdatedBy", typeof(String));

            return dtAccessProfiles;
        }

        public DataTable GetAccessProfiles(String sConnStr)
        {
            return new AccessProfileService().GetAccessProfiles(sConnStr);
        }
        public AccessProfile GetAccessProfilesById(String ID, String sConnStr)
        {
            return new AccessProfileService().GetAccessProfilesById(ID, sConnStr);
        }
        public DataTable GetAccessProfilesTemplateId(String sTemplateId, String sConnStr)
        {
            return new AccessProfileService().GetAccessProfilesByTemplateId(sTemplateId, sConnStr);
        }
        public bool Save(AccessProfile SAccessProfiles,  out DataSet dsResults, DbTransaction transaction, String sConnStr)
        {
            return new AccessProfileService().SaveAccessProfiles(SAccessProfiles, out dsResults, transaction, sConnStr);
        }
        public bool Update(AccessProfile SAccessProfiles, out DataSet dsResults, String sConnStr)
        {
            return new AccessProfileService().UpdateAccessProfiles(SAccessProfiles, out dsResults, null, sConnStr);
        }
        public bool Update(AccessProfile SAccessProfiles, out DataSet dsResults, DbTransaction transaction, String sConnStr)
        {
            return new AccessProfileService().UpdateAccessProfiles(SAccessProfiles, out dsResults, transaction, sConnStr);
        }
        public bool Delete(AccessProfile SAccessProfiles, out DataSet dsResults, String sConnStr)
        {
            return new AccessProfileService().DeleteAccessProfiles(SAccessProfiles, out dsResults, sConnStr);
        }
    }
}