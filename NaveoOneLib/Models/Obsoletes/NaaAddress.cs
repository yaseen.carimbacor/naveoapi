
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using NaveoOneLib.Common;
using NaveoOneLib.Services;
using System.Data.Common;

namespace NaveoOneLib.Models
{
    public partial class NaaAddress
    {

        private int iid = Constants.NullInt;
        private String naacode = Constants.NullString;
        private String lastname = Constants.NullString;
        private String othername = Constants.NullString;
        private String street1 = Constants.NullString;
        private String street2 = Constants.NullString;
        private String locality = Constants.NullString;
        private String city = Constants.NullString;
        private String tel = Constants.NullString;
        private String email = Constants.NullString;
        private String mobile = Constants.NullString;
        private String fax = Constants.NullString;
        private String createdby = Constants.NullString;
        private DateTime createddate = Constants.NullDateTime;
        private String updatedby = Constants.NullString;
        private DateTime updateddate = Constants.NullDateTime;


        public int iID { get { return iid; } set { iid = value; } }
        public String NaaCode { get { return naacode; } set { naacode = value; } }
        public String LastName { get { return lastname; } set { lastname = value; } }
        public String OtherName { get { return othername; } set { othername = value; } }
        public String Street1 { get { return street1; } set { street1 = value; } }
        public String Street2 { get { return street2; } set { street2 = value; } }
        public String Locality { get { return locality; } set { locality = value; } }
        public String City { get { return city; } set { city = value; } }
        public String Tel { get { return tel; } set { tel = value; } }
        public String Fax { get { return fax; } set { fax = value; } }
        public String Email { get { return email; } set { email = value; } }
        public String Mobile { get { return mobile; } set { mobile = value; } }
        public String CreatedBy { get { return createdby; } set { createdby = value; } }
        public DateTime CreatedDate { get { return createddate; } set { createddate = value; } }
        public String UpdatedBy { get { return updatedby; } set { updatedby = value; } }
        public DateTime UpdatedDate { get { return updateddate; } set { updateddate = value; } }


        public DataTable GetNaaAddress(String sConnStr)
        {
            return new NaaAddressService().GetNaaAddress(sConnStr);
        }
        public NaaAddress GetNaaAddressById(String ID,String sConnStr)
        {
            return new NaaAddressService().GetNaaAddressById(ID,sConnStr);
        }

        public bool Save(NaaAddress SNaaAddress, Boolean bInsert, String sConnStr)
        {
            return new NaaAddressService().SaveNaaAddress(SNaaAddress, bInsert,sConnStr);
        }
        public bool Update(NaaAddress SNaaAddress, String sConnStr)
        {
            return new NaaAddressService().UpdateNaaAddress(SNaaAddress, null,sConnStr);
        }
        public bool Update(NaaAddress SNaaAddress, DbTransaction transaction, String sConnStr)
        {
            return new NaaAddressService().UpdateNaaAddress(SNaaAddress, transaction,sConnStr);
        }
        public bool Delete(NaaAddress SNaaAddress, String sConnStr)
        {
            return new NaaAddressService().DeleteNaaAddress(SNaaAddress, sConnStr);
        }
        public NaaAddress GetClone()
        {
             return (NaaAddress)this.MemberwiseClone();
        }

    }
}
