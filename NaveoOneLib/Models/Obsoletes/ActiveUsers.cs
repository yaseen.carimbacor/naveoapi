﻿using System;
using NaveoOneLib.Common;
using NaveoOneLib.Services;
using System.Data;
using NaveoOneLib.Models.Audits;
using NaveoOneLib.Models.Users;
using NaveoOneLib.Services.Audits;

namespace NaveoOneLib.Models
{
    public class ActiveUsers
    {

        private int uid = Constants.NullInt;
        private String username = Constants.NullString;
        private DateTime loginDate = Constants.NullDateTime;
        private String machineName = Constants.NullString;
        private String userProfile = Constants.NullString;
        private String company = Constants.NullString;
        private String appVersion = Constants.NullString;
        private int userid = Constants.NullInt;


        public int UID { get { return uid; } set { uid = value; } }
        public String Username { get { return username; } set { username = value; } }
        public DateTime LoginDate { get { return loginDate; } set { loginDate = value; } }
        public String MachineName { get { return machineName; } set { machineName = value; } }
        public String UserProfile { get { return userProfile; } set { userProfile = value; } }
        public String Company { get { return company; } set { company = value; } }
        public String AppVersion { get { return appVersion; } set { appVersion = value; } }
        public int UserID { get { return uid; } set { uid = value; } }
        
         public ActiveUsers SetActiveSession(String sConnStr)
        {
            return new ActiveUsersService().SetActiveSession(sConnStr);
        }

        public bool SaveActiveSession(ActiveUsers aUser, String sConnStr)
        {
            return new ActiveUsersService().SaveActiveSession(aUser, sConnStr);
        }


        public bool DeleteActiveSession(string UID, String sConnStr)
        {
            return new ActiveUsersService().DeleteActiveSession(UID, sConnStr);
        }

        /*
        public void SaveTobeDeletedSession(ActiveUsers aUser)
        {
            new ActiveUsersService().SaveTobeDeletedSession(aUser);
        }
        */


        public DataTable GetUserSession(int UID, String sConnStr)
        {
            return new ActiveUsersService().GetUserSession(UID, sConnStr);
        }

        /****put in NaveoOneLib.Model******/
        public void AuditResetSession(string userReset, User user)
        {
            //new NaveoOneLib.Services.AuditTableService().AuditResetSession(aUser, user);

            AuditTable auditTable = new AuditTable();
            auditTable.AuditId = user.UID;
            auditTable.AuditRefCode = userReset;
            auditTable.AuditRefDesc = "Reset Session";
            auditTable.AuditRefTable = "GFI_SYS_ActiveSession";
            auditTable.AuditType = "";
            auditTable.CallerFunction = "ResetSession";

            new AuditTableService().SaveAuditTableService(auditTable);
        }
    }
}
