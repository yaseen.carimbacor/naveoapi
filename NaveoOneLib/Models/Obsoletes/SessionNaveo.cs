using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using NaveoOneLib.Common;
using NaveoOneLib.Services;
using System.Data.Common;

namespace NaveoOneLib.Models
{
    public class SessionNaveo
    {
        private int id = Constants.NullInt;
        private int uid = Constants.NullInt;
        private String ipaddress = Constants.NullString;
        private String machinename = Constants.NullString;
        private String type = Constants.NullString;
        private DateTime inserteddate = Constants.NullDateTime;
        private DateTime expirydate = Constants.NullDateTime;
        private String token = Constants.NullString;

        public int Id { get { return id; } set { id = value; } }
        public int Uid { get { return uid; } set { uid = value; } }
        public String IpAddress { get { return ipaddress; } set { ipaddress = value; } }
        public String MachineName { get { return machinename; } set { machinename = value; } }
        public String Type { get { return type; } set { type = value; } }
        public DateTime InsertedDate { get { return inserteddate; } set { inserteddate = value; } }
        public DateTime ExpiryDate { get { return expirydate; } set { expirydate = value; } }
        public String Token { get { return token; } set { token = value; } }

        public DataTable GetSessionNaveo(String sConnStr)
        {
            return new SessionNaveoService().GetSessionNaveo(sConnStr);
        }
        public SessionNaveo GetSessionNaveoByToken(String Token, String sConnStr)
        {
            return new SessionNaveoService().GetSessionNaveoByToken(Token, sConnStr);
        }

        public Boolean Save(SessionNaveo SSessionNaveo, Boolean bInsert, String sConnStr)
        {
            return new SessionNaveoService().SaveSessionNaveo(SSessionNaveo, bInsert, sConnStr);
        }
        public Boolean Update(SessionNaveo SSessionNaveo, String sConnStr)
        {
            return new SessionNaveoService().SaveSessionNaveo(SSessionNaveo, false, sConnStr);
        }
        public Boolean Delete(SessionNaveo SSessionNaveo, String sConnStr)
        {
            return new SessionNaveoService().DeleteSessionNaveo(SSessionNaveo, sConnStr);
        }
    }
}