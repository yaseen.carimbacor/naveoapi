using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using NaveoOneLib.Common;
using NaveoOneLib.Services;
using System.Data.Common;

namespace NaveoOneLib.Models
{
    public class Token {
        public String TokenID { get; set; }
        public String sqlCmd { get; set; }
        public DateTime ExpiryDate { get; set; }
        public String sData { get; set; }
        public String sAdditionalData { get; set; }

        public Token GetTokensById(String sTokenID, String sConnStr)
        {
            return new TokensService().GetTokensById(sTokenID, sConnStr);
        }
        public String Save(Token STokens, Boolean bInsert, String sConnStr)
        {
            return new TokensService().SaveTokens(STokens, bInsert, sConnStr);
        }
        public Boolean Delete(Token STokens, String sConnStr)
        {
            return new TokensService().DeleteTokens(STokens, sConnStr);
        }

        public Boolean ExecuteTokens(String sTokenID, String sConnStr)
        {
            return new TokensService().ExecuteTokens(sTokenID, sConnStr);
        }
    }
}