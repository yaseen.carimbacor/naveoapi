
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using NaveoOneLib.Common;
using NaveoOneLib.Services;
using System.Data.Common;

namespace NaveoOneLib.Models
{
    public class EmpBase
    {

        private int iid = Constants.NullInt;
        private String empcode = Constants.NullString;
        private int naacode = Constants.NullInt;
        private int tid = Constants.NullInt;
        private String emptype = Constants.NullString;
        private String empcategory = Constants.NullString;
        private DateTime datejoined = Constants.NullDateTime;
        private String createdby = Constants.NullString;
        private DateTime createddate = Constants.NullDateTime;
        private String updatedby = Constants.NullString;
        private DateTime updateddate = Constants.NullDateTime;


        public int iID { get { return iid; } set { iid = value; } }
        public String EmpCode { get { return empcode; } set { empcode = value; } }
        public int NaaCode { get { return naacode; } set { naacode = value; } }
        public int TID { get { return tid; } set { tid = value; } }
        public String EmpType { get { return emptype; } set { emptype = value; } }
        public String EmpCategory { get { return empcategory; } set { empcategory = value; } }
        public DateTime DateJoined { get { return datejoined; } set { datejoined = value; } }
        public String CreatedBy { get { return createdby; } set { createdby = value; } }
        public DateTime CreatedDate { get { return createddate; } set { createddate = value; } }
        public String UpdatedBy { get { return updatedby; } set { updatedby = value; } }
        public DateTime UpdatedDate { get { return updateddate; } set { updateddate = value; } }


        public DataTable GetEmpBase(String sConnStr)
        {
            return new EmpBaseService().GetEmpBase(sConnStr);
        }
        public DataTable GetEmpBaseByTeamId(string teamId, String sConnStr)
        {
            return new EmpBaseService().GetExtCodeByTeamId(teamId, sConnStr);
        }
        public EmpBase GetEmpBaseById(String ID, String sConnStr)
        {
            return new EmpBaseService().GetEmpBaseById(ID, sConnStr);
        }
        public bool Save(EmpBase SEmpBase, Boolean bInsert, String sConnStr)
        {
            return new EmpBaseService().SaveEmpBase(SEmpBase, bInsert, sConnStr);
        }

        public bool Update(EmpBase SEmpBase, String sConnStr)
        {
            return new EmpBaseService().UpdateEmpBase(SEmpBase, null, sConnStr);
        }
        public bool Update(EmpBase SEmpBase, DbTransaction transaction, String sConnStr)
        {
            return new EmpBaseService().UpdateEmpBase(SEmpBase, transaction, sConnStr);
        }
        public bool Delete(EmpBase SEmpBase, String sConnStr)
        {
            return new EmpBaseService().DeleteEmpBase(SEmpBase, sConnStr);
        }

        public EmpBase GetClone()
        {
             return (EmpBase)this.MemberwiseClone();
        }

    }
}
