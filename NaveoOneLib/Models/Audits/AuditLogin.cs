﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using NaveoOneLib.Common;

namespace NaveoOneLib.Models.Audits
{
    public  class AuditLogin
    {

        private int  auditId = Constants.NullInt;
        private String auditUser = Constants.NullString;
        private DateTime auditDate = Constants.NullDateTime;
        private String auditType = Constants.NullString;
        private String machineName = Constants.NullString;
        private String clientGroupName = Constants.NullString;
        private DateTime postedDate = Constants.NullDateTime;
        private String postedFlag = Constants.NullString;

        public int AuditId { get { return auditId; } set { auditId = value; } }
        public String AuditUser { get { return auditUser; } set { auditUser = value; } }
        public DateTime AuditDate { get { return auditDate; } set { auditDate = value; } }
        public String AuditType { get { return auditType; } set { auditType = value; } }
        public String MachineName { get { return machineName; } set { machineName = value; } }
        public String ClientGroupName { get { return clientGroupName; } set { clientGroupName = value; } }
        public DateTime PostedDate { get { return postedDate; } set { postedDate = value; } }
        public String PostedFlag { get { return postedFlag; } set { postedFlag = value; } }

    }
}
