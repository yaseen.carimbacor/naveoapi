﻿using System;
using System.Collections.Generic;
using NaveoOneLib.Models.Audits;
using System.Text;
using System.Data;
using NaveoOneLib.Common;
using NaveoOneLib.Services;
using System.Data.Common;

namespace NaveoOneLib.Models.Audits
{
    public class AuditTable
    {
        private int auditId = Constants.NullInt;
        private String auditUser = Constants.NullString;
        private String auditType = Constants.NullString;
        private String auditRefCode = Constants.NullString;
        private String auditRefDesc = Constants.NullString;
        private String auditRefTable = Constants.NullString;
        private String callerFunction = Constants.NullString;
        private String sqlRemarks = Constants.NullString;
        private DateTime auditDate = Constants.NullDateTime;
        private String oldValue = Constants.NullString;
        private String newValue = Constants.NullString;
        private DateTime postedDate = Constants.NullDateTime;
        private String postedFlag = Constants.NullString;

        public int AuditId { get { return auditId; } set { auditId = value; } }
        public String AuditUser { get { return auditUser; } set { auditUser = value; } }
        public DateTime AuditDate { get { return auditDate; } set { auditDate = value; } }
        public String AuditType { get { return auditType; } set { auditType = value; } }
        public String AuditRefCode { get { return auditRefCode; } set { auditRefCode = value; } }
        public String AuditRefDesc { get { return auditRefDesc; } set { auditRefDesc = value; } }
        public String AuditRefTable { get { return auditRefTable; } set { auditRefTable = value; } }
        public String CallerFunction { get { return callerFunction; } set { callerFunction = value; } }
        public String SqlRemarks { get { return sqlRemarks; } set { sqlRemarks = value; } }
        public String OldValue { get { return oldValue; } set { oldValue = value; } }
        public String NewValue { get { return newValue; } set { newValue = value; } }
        public DateTime PostedDate { get { return postedDate; } set { postedDate = value; } }
        public String PostedFlag { get { return postedFlag; } set { postedFlag = value; } }
    }
}
