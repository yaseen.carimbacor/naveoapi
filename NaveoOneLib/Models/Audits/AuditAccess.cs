﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using NaveoOneLib.Common;

namespace NaveoOneLib.Models.Audits
{
     public class AuditAccess
     {

          private int iId = Constants.NullInt;
          private String audituser = Constants.NullString;
          private String control = Constants.NullString;
          private DateTime auditDate = Constants.NullDateTime;
          private DateTime postedDate = Constants.NullDateTime;
          private String postedFlag = Constants.NullString;

          public int IId { get { return iId; } set { iId = value; } }
          public String AuditUser { get { return audituser; } set { audituser = value; } }
          public String Control { get { return control; } set { control = value; } }
          public DateTime AuditDate { get { return auditDate; } set { auditDate = value; } }
          public DateTime PostedDate { get { return postedDate; } set { postedDate = value; } }
          public String PostedFlag { get { return postedFlag; } set { postedFlag = value; } }
     }
}
