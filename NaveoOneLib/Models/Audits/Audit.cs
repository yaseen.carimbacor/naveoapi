﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using NaveoOneLib.Common;

namespace NaveoOneLib.Models.Audits
{
    public class Audit
    {
        public int AuditId { get; set; }
        public String AuditTypes { get; set; }
        public String ReferenceCode { get; set; }
        public String ReferenceDesc { get; set; }
        public DateTime CreatedDate { get; set; }
        public String AuditUser { get; set; }
        public String ReferenceTable { get; set; }
        public String CallerFunction { get; set; }
        public String SQLRemarks { get; set; }

        public String email { get; set; }
        public DateTime dtDate { get; set; }
    }

    public class AuditData 
    { 
        public int AuditTraceId { get; set; }
        public Guid UserToken { get; set; }
        public String Controller { get; set; }
        public String Action { get; set; }
        public int IsAccessGranted { get; set; }
        public DateTime AccessedOn { get; set; }
        public String RawRequest { get; set; }
        public String ReferenceCode { get; set; }
        public int sID { get; set; }
    }
}
