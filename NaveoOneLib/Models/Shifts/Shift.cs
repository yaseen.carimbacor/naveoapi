using NaveoOneLib.Models.GMatrix;
using System;
using System.Collections.Generic;
using System.Data;

namespace NaveoOneLib.Models.Shifts
{
    public class Shift
    {
        public int SID { get; set; }
        public String sName { get; set; }
        public String sComments { get; set; }
        public DateTime TimeFr { get; set; }
        public DateTime TimeTo { get; set; }
        public DataRowState oprType { get; set; }

        public List<Matrix> lMatrix { get; set; } = new List<Matrix>();
    }
}