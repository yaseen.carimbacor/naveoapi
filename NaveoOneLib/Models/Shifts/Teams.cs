using System;
using System.Collections.Generic;
using System.Data;


namespace NaveoOneLib.Models.Shifts
{
    public class Teams
    {
        public int TID { get; set; }
        public String sName { get; set; }
        public String sComments { get; set; }
        public String GUID { get; set; }

        public DataRowState oprType { get; set; }
        public List<TeamsAsset> lAssets { get; set; }
        public List<TeamsResource> lResources { get; set; }
    }
}