using System.Data;


namespace NaveoOneLib.Models.Shifts
{
    public class TeamsAsset
    {
        public int iID { get; set; }
        public int TID { get; set; }
        public int AssetID { get; set; }

        public DataRowState oprType { get; set; }
    }
}