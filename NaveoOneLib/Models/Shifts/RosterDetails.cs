﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoOneLib.Models.Shifts
{
    public class RosterDetails
    {

        public int RID { get; set; }
        public int GRID { get; set; }
        public int SID { get; set; }
        public string Shiftdesc { get; set; }
        public int TeamID { get; set; }

        public int ShiftTemplateId { get; set; }
        public string ShiftTemplateDesc { get; set; }
        public DateTime rDate { get; set; }
        public int AssetID { get; set; }
        public int ResourceID { get; set; }
        public DataRowState oprType { get; set; }
    }
}
