﻿using NaveoOneLib.Models.GMatrix;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoOneLib.Models.Shifts
{
    public class ShiftTemplate
    {
        public int iID { get; set; }
        public int GroupID { get; set; }
        public int? SID { get; set; }
        public int OrderNo { get; set; }
        public String Description { get; set; }

        public List<ShiftTemplate> lShiftTemplates { get; set; }  = new List<ShiftTemplate>();
        public List<Matrix> lMatrix { get; set; } = new List<Matrix>();
        public DataRowState oprType { get; set; }
    }
}
