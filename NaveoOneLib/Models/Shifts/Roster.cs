using NaveoOneLib.Models.GMatrix;
using System;
using System.Collections.Generic;
using System.Data;


namespace NaveoOneLib.Models.Shifts
{
    public class Roster
    {
       
        public int? iID { get; set; }
        public String RosterName { get; set; }
        public DateTime dtFrom { get; set; }
        public DateTime dtTo { get; set; }
        public String sRemarks { get; set; }
        public int? isLocked { get; set; }
        public int? isOriginal { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DataRowState oprType { get; set; }
        public List<RosterDetails> lRosterDetails { get; set; } = new List<RosterDetails>();
        public List<Matrix> lMatrix { get; set; } = new List<Matrix>();
        public List<RosterResources> lResources { get; set; } = new List<RosterResources>();
        public RosterAssets rosterAsset { get; set; } = new RosterAssets();

        //public Shift rShift { get; set; }
        //public Teams rTeam { get; set; }
    }
}