﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoOneLib.Models.Shifts
{
    public class RosterAssets
    {
        public int? iID { get; set; }
        public int? RID { get; set; }
        public int? AssetID { get; set; }
        public string AssetName { get; set; }
        public DataRowState oprType { get; set; }
    }
}
