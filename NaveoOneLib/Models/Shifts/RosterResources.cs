﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoOneLib.Models.Shifts
{
   public  class RosterResources
    {
        public int? iID{ get; set; }
        public int? RID { get; set; }
        public int? DriverID { get; set; }
        public string DriverName { get; set; }
        public DataRowState oprType { get; set; }
    }
}
