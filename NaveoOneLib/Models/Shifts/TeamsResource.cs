using System.Data;


namespace NaveoOneLib.Models.Shifts
{
    public class TeamsResource
    {
        public int iID { get; set; }
        public int TID { get; set; }
        public int DriverID { get; set; }

        public DataRowState oprType { get; set; }
    }
}