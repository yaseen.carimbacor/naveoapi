using System;
using NaveoOneLib.Common;


namespace NaveoOneLib.Models.Shifts
{
    public class Team
    {

        private int iid = Constants.NullInt;
        private String teamcode = Constants.NullString;
        private String teamsize = Constants.NullString;
        private String teamleader = Constants.NullString;
        private String mainrole = Constants.NullString;
        private String createdby = Constants.NullString;
        private DateTime createddate = Constants.NullDateTime;
        private String updatedby = Constants.NullString;
        private DateTime updateddate = Constants.NullDateTime;


        public int iID { get { return iid; } set { iid = value; } }
        public String TeamCode { get { return teamcode; } set { teamcode = value; } }
        public String TeamSize { get { return teamsize; } set { teamsize = value; } }
        public String TeamLeader { get { return teamleader; } set { teamleader = value; } }
        public String MainRole { get { return mainrole; } set { mainrole = value; } }
        public String CreatedBy { get { return createdby; } set { createdby = value; } }
        public DateTime CreatedDate { get { return createddate; } set { createddate = value; } }
        public String UpdatedBy { get { return updatedby; } set { updatedby = value; } }
        public DateTime UpdatedDate { get { return updateddate; } set { updateddate = value; } }
    }
}
