﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoOneLib.Models.DriverAsset
{
    public class DriverAssetDto
    {

        public int drvAsset_id { get; set; }
        public int AssetID { get; set; }
        public int DriverID { get; set; }
        public System.DateTime drvAsset_DateFrom { get; set; }

        public System.DateTime drvAsset_DateTo { get; set; }

        public string drvAsset_Remarks { get; set; }

        public Nullable<System.DateTime> drvAsset_CreatedDate { get; set; }

        public Nullable<System.DateTime> drvAsset_UpdatedDate { get; set; }
        public int drvAsset_CreatedBy { get; set; }
        public int drvAsset_UpdatedBy { get; set; }
        public Boolean drvAsset_flagDelete { get; set; }

    }

    public class lstDriverAssetDto
    {

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

    }
}
