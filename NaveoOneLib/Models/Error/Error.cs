﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using NaveoOneLib.Common;

namespace NaveoOneLib.Models.ErrorData
{ 

    public class ErrorData
    {
        public DateTime dateFrom { get; set; }
        public DateTime dateTo { get; set; }

        public List<int> userIds { get; set; }

        public int Status { get; set; }

        public List<String> sortColumns { get; set; } = new List<String>();
        public Boolean sortOrderAsc { get; set; } = false;
    }

    public class UpdateError 
    {
        public int ErrorID { get; set; }
        public int Status { get; set; }
    }

    public class BaseError
    {
        public int ErrorID { get; set; }
        public String RequestType { get; set; }
        public String UserToken { get; set; }
        public String Description { get; set; }
        public String Source { get; set; }
        public String StackTrace { get; set; }
        public DateTime Time { get; set; }
        public int Status { get; set; }
    }
}
