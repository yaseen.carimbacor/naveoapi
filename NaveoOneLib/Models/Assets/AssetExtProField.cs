using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using NaveoOneLib.Common;

namespace NaveoOneLib.Models.Assets
{
    public class AssetExtProField
    {

        private int fieldid = Constants.NullInt;
        private String assettype = Constants.NullString;
        private String category = Constants.NullString;
        private String fieldname = Constants.NullString;
        private String label = Constants.NullString;
        private String description = Constants.NullString;
        private String type = Constants.NullString;
        private String unitofmeasure = Constants.NullString;
        private String Keephistory = Constants.NullString;
        private int dataretentiondays = Constants.NullInt;
        private int thwarning_value = Constants.NullInt;
        private int thwarning_timebased = Constants.NullInt;
        private int thcritical_value = Constants.NullInt;
        private int thcritical_timebased = Constants.NullInt;
        private int displayorder = Constants.NullInt;
        private DateTime createddate = Constants.NullDateTime;
        private String createdby = Constants.NullString;
        private DateTime updateddate = Constants.NullDateTime;
        private String updatedby = Constants.NullString;


        public int FieldId { get { return fieldid; } set { fieldid = value; } }
        public String AssetType { get { return assettype; } set { assettype = value; } }
        public String Category { get { return category; } set { category = value; } }
        public String FieldName { get { return fieldname; } set { fieldname = value; } }
        public String Label { get { return label; } set { label = value; } }
        public String Description { get { return description; } set { description = value; } }
        public String Type { get { return type; } set { type = value; } }
        public String UnitOfMeasure { get { return unitofmeasure; } set { unitofmeasure = value; } }
        public String KeepHistory { get { return Keephistory; } set { Keephistory = value; } }
        public int DataRetentionDays { get { return dataretentiondays; } set { dataretentiondays = value; } }
        public int THWarning_Value { get { return thwarning_value; } set { thwarning_value = value; } }
        public int THWarning_TimeBased { get { return thwarning_timebased; } set { thwarning_timebased = value; } }
        public int THCritical_Value { get { return thcritical_value; } set { thcritical_value = value; } }
        public int THCritical_TimeBased { get { return thcritical_timebased; } set { thcritical_timebased = value; } }
        public int DisplayOrder { get { return displayorder; } set { displayorder = value; } }
        public DateTime CreatedDate { get { return createddate; } set { createddate = value; } }
        public String CreatedBy { get { return createdby; } set { createdby = value; } }
        public DateTime UpdatedDate { get { return updateddate; } set { updateddate = value; } }
        public String UpdatedBy { get { return updatedby; } set { updatedby = value; } }
    }
}