﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using NaveoOneLib.Common;

namespace NaveoOneLib.Models.Assets
{
    public class VehicleMaintTypeLink
    {
        private int uri = Constants.NullInt;
        private int mainturi = Constants.NullInt;
        private int assetid = Constants.NullInt;
        private int mainttypeid = Constants.NullInt;
        private String description = Constants.NullString;
        private DateTime nextmaintdate = Constants.NullDateTime;
        private DateTime nextmaintdatetg = Constants.NullDateTime;
        private int currentodometer = Constants.NullInt;
	    private int nextmaintodometer = Constants.NullInt;
	    private int nextmaintodometertg = Constants.NullInt;
        private int currentenghrs = Constants.NullInt;	    
        private int nextmaintenghrs = Constants.NullInt;
        private int nextmaintenghrstg = Constants.NullInt;
        private int status = Constants.NullInt;
        private DateTime createddate = Constants.NullDateTime;
        private String createdby = Constants.NullString;
        private DateTime updateddate = Constants.NullDateTime;
        private String updatedby = Constants.NullString;

        public int URI { get { return uri; } set { uri = value; } }
        public int MaintURI { get { return mainturi; } set { mainturi = value; } }
        public int AsssetId { get { return assetid; } set { assetid = value; } }
        public int MaintTypeId { get { return mainttypeid; } set { mainttypeid = value; } }
        public String Description { get { return description; } set { description = value; } }
        public DateTime NextMaintDate { get { return nextmaintdate; } set { nextmaintdate = value; } }
        public DateTime NextMaintDateTG { get { return nextmaintdatetg; } set { nextmaintdatetg = value; } }
        public int CurrentOdometer { get { return currentodometer; } set { currentodometer = value; } }
        public int NextMaintOdometer { get { return nextmaintodometer; } set { nextmaintodometer = value; } }
        public int NextMaintOdometerTG { get { return nextmaintodometertg; } set { nextmaintodometertg = value; } }
        public int CurrentEngHrs { get { return currentenghrs; } set { currentenghrs = value; } }
        public int NextMaintEngHrs { get { return nextmaintenghrs; } set { nextmaintenghrs = value; } }
        public int NextMaintEngHrsTG { get { return nextmaintenghrstg; } set { nextmaintenghrstg = value; } }
        public int Status { get { return status; } set { status = value; } }
        public DateTime CreatedDate { get { return createddate; } set { createddate = value; } }
        public String CreatedBy { get { return createdby; } set { createdby = value; } }
        public DateTime UpdatedDate { get { return updateddate; } set { updateddate = value; } }
        public String UpdatedBy { get { return updatedby; } set { updatedby = value; } }
    }
}
