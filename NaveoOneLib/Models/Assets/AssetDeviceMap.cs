using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using NaveoOneLib.Common;

namespace NaveoOneLib.Models.Assets
{
    public class AssetDeviceMap
    {
        public DeviceAuxilliaryMap deviceAuxilliaryMap { get; set; } = new DeviceAuxilliaryMap();

        public int MapID { get; set; }
        public int AssetID { get; set; }
        public String DeviceID { get; set; }
        public String SerialNumber { get; set; }
        public String Status_b { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int MaxNoLogDays { get; set; }

        public DataRowState oprType { get; set; }
    }
}