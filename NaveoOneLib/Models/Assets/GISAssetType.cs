﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoOneLib.Models.Assets
{
    public class GISAssetType
    {
        public int iID { get; set; }
        public String AssetType { get; set; }
        public DataRowState oprType { get; set; }
    }

    public class GISAssetCategory
    {
        public int iID { get; set; }
        public int AssetTypeID { get; set; }
        public String AssetCategory { get; set; }
        public DataRowState oprType { get; set; }
    }
}
