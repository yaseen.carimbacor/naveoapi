using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using NaveoOneLib.Common;

namespace NaveoOneLib.Models.Assets
{
    public class CalibrationChart
    {

        public Int64 IID { get; set; }
        public int? AssetID { get; set; }
        public String DeviceID { get; set; }
        public int? AuxID { get; set; }
        public String CalType { get; set; }
        public int? SeuqenceNo { get; set; }
        public int ReadingUOM { get; set; }
        public float Reading { get; set; }
        public float ConvertedValue { get; set; }
        public int ConvertedUOM { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }

        public String ReadingUOMDesc { get; set; }
        public String ConvertedUOMDesc { get; set; }
        public DataRowState oprType {get;set;}
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }

        public CalibrationChart GetClone()
        {
            return (CalibrationChart)this.MemberwiseClone();
        }
    }
}