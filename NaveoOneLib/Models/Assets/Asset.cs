using System;
using System.Collections.Generic;
using System.Data;
using NaveoOneLib.Models.GMatrix;

namespace NaveoOneLib.Models.Assets
{
    public partial class Asset
    {
        public int AssetID { get; set; }
        public String AssetNumber { get; set; }
        public String AssetName { get; set; }
        public int AssetType { get; set; }
        public String Status_b { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int? UpdatedBy { get; set; }
        public String TimeZoneID { get; set; }
        public TimeSpan TimeZoneTS { get; set; }
        public int Odometer { get; set; }

        public String eCreatedBy { get; set; }
        public String eUpdatedBy { get; set; }

        public DataRowState oprType { get; set; }

        public int? VehicleTypeId { get; set; }
        public int? MakeID { get; set; }
        public int? ModelID { get; set; }
        public int? ColorID { get; set; }

        public int? RoadSpeedTypeId { get; set; }

        public String sMake { get; set; }
        public String sModel { get; set; }
        public String sColor { get; set; }

        public string sRoadSpeedType { get; set; }

        public int? YearManufactured { get; set; }
        public DateTime? PurchasedDate { get; set; }
        public Double? PurchasedValue { get; set; }
        public String RefA { get; set; }
        public String RefB { get; set; }
        public String RefC { get; set; }
        public String RefD { get; set; }
        public String ExternalRef { get; set; }
        public String EngineSerialNumber { get; set; }
        public int? EngineCapacity { get; set; }
        public int? EnginePower { get; set; }

        public String VehicleType { get; set; }
        public AssetDeviceMap assetDeviceMap { get; set; } = new AssetDeviceMap();
        public List<Matrix> lMatrix { get; set; } = new List<Matrix>();
        public List<AssetZoneMap> lAssetZoneMap { get; set; }
        public AssetExtProVehicle assetExtProVehicles { get; set; }
        public List<AssetUOM> lUOM { get; set; }
        public AssetTypeMapping AssetTypeMappingObject { get; set; }

        public DataTable dtUsers { get; set; }
        public DataTable dtAllUOM { get; set; }
      
        public Asset GetClone()
        {
            return (Asset)this.MemberwiseClone();
        }
    }
}
