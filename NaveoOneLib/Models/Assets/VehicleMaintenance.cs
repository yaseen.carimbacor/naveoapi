using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using NaveoOneLib.Common;

namespace NaveoOneLib.Models.Assets
{
    public class VehicleMaintenance
    {

        private int uri = Constants.NullInt;
        private int vmtluri = Constants.NullInt;
        private int assetid = Constants.NullInt;
        private String assetnumber = Constants.NullString;
        private int maintcatid_cbo = Constants.NullInt;
        private String maintcatdesc = Constants.NullString;
        private int mainttypeid_cbo = Constants.NullInt;
        private String mainttypedesc = Constants.NullString;
        private String contactdetails = Constants.NullString;
        private String companyname = Constants.NullString;
        private String phonenumber = Constants.NullString;
        private String companyref = Constants.NullString;
        private String companyref2 = Constants.NullString;
        private String maintdescription = Constants.NullString;
        private DateTime startdate = Constants.NullDateTime;
        private DateTime enddate = Constants.NullDateTime;
        private int maintstatusid_cbo = Constants.NullInt;
        private String maintstatusdesc = Constants.NullString;
        private Decimal estimatedvalue = Constants.NullDecimal;
        private String vatinclinitemsamt = Constants.NullString;
        private Decimal vatamount = Constants.NullDecimal;
        private Decimal totalcost = Constants.NullDecimal;
        private int covertypeid_cbo = Constants.NullInt;
        private String covertypedesc = Constants.NullString;
        private int calculatedodometer = Constants.NullInt;
        private int actualodometer = Constants.NullInt;
        private int calculatedengineHrs = Constants.NullInt;
        private int actualengineHrs = Constants.NullInt;
        private String additionalinfo = Constants.NullString;
        private DateTime createddate = Constants.NullDateTime;
        private String createdby = Constants.NullString;
        private DateTime updateddate = Constants.NullDateTime;
        private String updatedby = Constants.NullString;
        private String assetstatus = Constants.NullString;
        private String comment = Constants.NullString;

        public int URI { get { return uri; } set { uri = value; } }
        public int VMTLURI { get { return vmtluri; } set { vmtluri = value; } }
        public int AssetId { get { return assetid; } set { assetid = value; } }
        public String AssetNumber { get { return assetnumber; } set { assetnumber = value; } }
        public int MaintCatId_cbo { get { return maintcatid_cbo; } set { maintcatid_cbo = value; } }
        public String MaintCatDesc { get { return maintcatdesc; } set { maintcatdesc = value; } }
        public int MaintTypeId_cbo { get { return mainttypeid_cbo; } set { mainttypeid_cbo = value; } }
        public String MaintTypeDesc { get { return mainttypedesc; } set { mainttypedesc = value; } }
        public String ContactDetails { get { return contactdetails; } set { contactdetails = value; } }
        public String CompanyName { get { return companyname; } set { companyname = value; } }
        public String PhoneNumber { get { return phonenumber; } set { phonenumber = value; } }
        public String CompanyRef { get { return companyref; } set { companyref = value; } }
        public String CompanyRef2 { get { return companyref2; } set { companyref2 = value; } }
        public String MaintDescription { get { return maintdescription; } set { maintdescription = value; } }        
        public DateTime StartDate { get { return startdate; } set { startdate = value; } }
        public DateTime EndDate { get { return enddate; } set { enddate = value; } }
        public int MaintStatusId_cbo { get { return maintstatusid_cbo; } set { maintstatusid_cbo = value; } }
        public String MaintStatusDesc { get { return maintstatusdesc; } set { maintstatusdesc = value; } }
        public Decimal EstimatedValue { get { return estimatedvalue; } set { estimatedvalue = value; } }
        public String VATInclInItemsAmt { get { return vatinclinitemsamt; } set { vatinclinitemsamt = value; } }
        public Decimal VATAmount { get { return vatamount; } set { vatamount = value; } }
        public Decimal TotalCost { get { return totalcost; } set { totalcost = value; } }
        public int CoverTypeId_cbo { get { return covertypeid_cbo; } set { covertypeid_cbo = value; } }
        public String CoverTypeDesc { get { return covertypedesc; } set { covertypedesc = value; } }
        public int CalculatedOdometer { get { return calculatedodometer; } set { calculatedodometer = value; } }
        public int ActualOdometer { get { return actualodometer; } set { actualodometer = value; } }
        public int CalculatedEngineHrs { get { return calculatedengineHrs; } set { calculatedengineHrs = value; } }
        public int ActualEngineHrs { get { return actualengineHrs; } set { actualengineHrs = value; } }
        public String AdditionalInfo { get { return additionalinfo; } set { additionalinfo = value; } }
        public DateTime CreatedDate { get { return createddate; } set { createddate = value; } }
        public String CreatedBy { get { return createdby; } set { createdby = value; } }
        public DateTime UpdatedDate { get { return updateddate; } set { updateddate = value; } }
        public String UpdatedBy { get { return updatedby; } set { updatedby = value; } }
        public String AssetStatus { get { return assetstatus; } set { assetstatus = value; } }
        public String Comment { get { return comment; } set { comment = value; } }
    }
}
