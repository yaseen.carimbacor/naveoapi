using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using NaveoOneLib.Common;

namespace NaveoOneLib.Models.Assets
{
    public class AssetExtProXT
    {

        private int uri = Constants.NullInt;
        private int assetid = Constants.NullInt;
        private int fieldid = Constants.NullInt;
        private String xtvalue = Constants.NullString;
        private DateTime createddate = Constants.NullDateTime;
        private String createdby = Constants.NullString;
        private DateTime updateddate = Constants.NullDateTime;
        private String updatedby = Constants.NullString;


        public int URI { get { return uri; } set { uri = value; } }
        public int AssetId { get { return assetid; } set { assetid = value; } }
        public int FieldId { get { return fieldid; } set { fieldid = value; } }
        public String XTValue { get { return xtvalue; } set { xtvalue = value; } }
        public DateTime CreatedDate { get { return createddate; } set { createddate = value; } }
        public String CreatedBy { get { return createdby; } set { createdby = value; } }
        public DateTime UpdatedDate { get { return updateddate; } set { updateddate = value; } }
        public String UpdatedBy { get { return updatedby; } set { updatedby = value; } }
    }
}
