using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using NaveoOneLib.Common;
using NaveoOneLib.Models.GMatrix;

namespace NaveoOneLib.Models.Assets
{
    public class VehicleMaintType
    {

        private int mainttypeid = Constants.NullInt;
        private int maintcatid = Constants.NullInt;
        private String maintcatdesc = Constants.NullString;
        private String description = Constants.NullString;

        private int occurrencetype = Constants.NullInt;

        private DateTime occurrencefixeddate = Constants.NullDateTime;
        private int occurrenceduration = Constants.NullInt;
        private String occurrenceperiod_cbo = Constants.NullString;
        private int occurrencekm = Constants.NullInt;
        private int occurrenceenginehrs = Constants.NullInt;

        private int occurrencefixeddateth = Constants.NullInt;
        private int occurrencedurationth = Constants.NullInt;
        private int occurrencekmth = Constants.NullInt;
        private int occurrenceenginehrsth = Constants.NullInt;

        private DateTime createddate = Constants.NullDateTime;
        private String createdby = Constants.NullString;
        private DateTime updateddate = Constants.NullDateTime;
        private String updatedby = Constants.NullString;

        private List<Matrix> lmatrix = new List<Matrix>();
        public List<Matrix> lMatrix { get { return lmatrix; } set { lmatrix = value; } }

        public int MaintTypeId { get { return mainttypeid; } set { mainttypeid = value; } }
        public int MaintCatId_cbo { get { return maintcatid; } set { maintcatid = value; } }
        public String MaintCatDesc { get { return maintcatdesc; } set { maintcatdesc = value; } }
        public String Description { get { return description; } set { description = value; } }

        public int OccurrenceType { get { return occurrencetype; } set { occurrencetype = value; } }

        public DateTime OccurrenceFixedDate { get { return occurrencefixeddate; } set { occurrencefixeddate = value; } }
        public int OccurrenceDuration { get { return occurrenceduration; } set { occurrenceduration = value; } }
        public String OccurrencePeriod_cbo { get { return occurrenceperiod_cbo; } set { occurrenceperiod_cbo = value; } }
        public int OccurrenceKM { get { return occurrencekm; } set { occurrencekm = value; } }
        public int OccurrenceEngineHrs { get { return occurrenceenginehrs; } set { occurrenceenginehrs = value; } }

        public int OccurrenceFixedDateTh { get { return occurrencefixeddateth; } set { occurrencefixeddateth = value; } }
        public int OccurrenceDurationTh { get { return occurrencedurationth; } set { occurrencedurationth = value; } }
        public int OccurrenceKMTh { get { return occurrencekmth; } set { occurrencekmth = value; } }
        public int OccurrenceEngineHrsTh { get { return occurrenceenginehrsth; } set { occurrenceenginehrsth = value; } }

        public DateTime CreatedDate { get { return createddate; } set { createddate = value; } }
        public String CreatedBy { get { return createdby; } set { createdby = value; } }
        public DateTime UpdatedDate { get { return updateddate; } set { updateddate = value; } }
        public String UpdatedBy { get { return updatedby; } set { updatedby = value; } }
    }
}
