using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using NaveoOneLib.Common;

namespace NaveoOneLib.Models.Assets
{
    public class AssetDataType
    {
        private int iid = Constants.NullInt;
        private String typename = Constants.NullString;
        private String field1name = Constants.NullString;
        private String field1label = Constants.NullString;
        private String field1type = Constants.NullString;
        private String createdby = Constants.NullString;
        private DateTime createddate = Constants.NullDateTime;
        private String updatedby = Constants.NullString;
        private DateTime updateddate = Constants.NullDateTime;
        private String sdatasource = Constants.NullString;

        public int IID { get { return iid; } set { iid = value; } }
        public String TypeName { get { return typename; } set { typename = value; } }
        public String Field1Name { get { return field1name; } set { field1name = value; } }
        public String Field1Label { get { return field1label; } set { field1label = value; } }
        public String Field1Type { get { return field1type; } set { field1type = value; } }
        public String CreatedBy { get { return createdby; } set { createdby = value; } }
        public DateTime CreatedDate { get { return createddate; } set { createddate = value; } }
        public String UpdatedBy { get { return updatedby; } set { updatedby = value; } }
        public DateTime UpdatedDate { get { return updateddate; } set { updateddate = value; } }
        public String sDataSource { get { return sdatasource; } set { sdatasource = value; } }
    }
}