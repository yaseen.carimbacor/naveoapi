using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using NaveoOneLib.Common;

namespace NaveoOneLib.Models.Assets
{
    public class AssetExtProVehicle
    {
        private int assetid = Constants.NullInt;
        private String registrationno = Constants.NullString;
        private String vin = Constants.NullString;
        private String description = Constants.NullString;
        private String make = Constants.NullString;
        private String model = Constants.NullString;
        private int vehicletypeid_cbo = Constants.NullInt;
        private String vehicletypedesc = Constants.NullString;
        private String manufacturer = Constants.NullString;
        private int yearmanufactured = Constants.NullInt;
        private DateTime purchaseddate = Constants.NullDateTime;
        private Decimal purchasedvalue = Constants.NullDecimal;
        private String poref = Constants.NullString;
        private int expectedlifetime = Constants.NullInt;
        private Decimal residualvalue = Constants.NullDecimal;
        private String engineserialnumber = Constants.NullString;
        private String enginetype = Constants.NullString;
        private int enginecapacity = Constants.NullInt;
        private int enginepower = Constants.NullInt;
        private String fueltype = Constants.NullString;
        private Decimal stdconsumption = Constants.NullDecimal;
        private String externalreference = Constants.NullString;
        private String inservice_b = Constants.NullString;
        private String additionalinfo = Constants.NullString;
        private DateTime createddate = Constants.NullDateTime;
        private DateTime updateddate = Constants.NullDateTime;

        

        public DataRowState oprType {get;set;}



        private int? field1id = Constants.NullInt;
        private String field1 = Constants.NullString;
        private int? field2id = Constants.NullInt;
        private String field2 = Constants.NullString;
        private int? field3id = Constants.NullInt;
        private String field3 = Constants.NullString;
        private int? field4id = Constants.NullInt;
        private String field4 = Constants.NullString;
        private int? field5id = Constants.NullInt;
        private String field5 = Constants.NullString;
        private int? field6id = Constants.NullInt;
        private String field6 = Constants.NullString;

        public string Field1Value { get { return field1; } set { field1 = value; } }
        public string Field2Value { get { return field2; } set { field2 = value; } }
        public string Field3Value { get { return field3; } set { field3 = value; } }
        public string Field4Value { get { return field4; } set { field4 = value; } }
        public string Field5Value { get { return field5; } set { field5 = value; } }
        public string Field6Value { get { return field6; } set { field6 = value; } }

        public int? Field1Id { get { return field1id; } set { field1id = value; } }       
        public int? Field2Id { get { return field2id; } set { field2id = value; } }
        public int? Field3Id { get { return field3id; } set { field3id = value; } }
        public int? Field4Id { get { return field4id; } set { field4id = value; } }
        public int? Field5Id { get { return field5id; } set { field5id = value; } }
        public int? Field6Id { get { return field6id; } set { field6id = value; } }

        public int AssetId { get { return assetid; } set { assetid = value; } }
        public String RegistrationNo { get { return registrationno; } set { registrationno = value; } }
        public String VIN { get { return vin; } set { vin = value; } }
        public String Description { get { return description; } set { description = value; } }
        public String Make { get { return make; } set { make = value; } }
        public String Model { get { return model; } set { model = value; } }
        public int VehicleTypeId_cbo { get { return vehicletypeid_cbo; } set { vehicletypeid_cbo = value; } }
        public String VehicleTypeDesc { get { return vehicletypedesc; } set { vehicletypedesc = value; } }
        public String Manufacturer { get { return manufacturer; } set { manufacturer = value; } }
        public int YearManufactured { get { return yearmanufactured; } set { yearmanufactured = value; } }
        public DateTime PurchasedDate { get { return purchaseddate; } set { purchaseddate = value; } }
        public Decimal PurchasedValue { get { return purchasedvalue; } set { purchasedvalue = value; } }
        public String PORef { get { return poref; } set { poref = value; } }
        public int ExpectedLifetime { get { return expectedlifetime; } set { expectedlifetime = value; } }
        public Decimal ResidualValue { get { return residualvalue; } set { residualvalue = value; } }
        public String EngineSerialNumber { get { return engineserialnumber; } set { engineserialnumber = value; } }
        public String EngineType { get { return enginetype; } set { enginetype = value; } }
        public int EngineCapacity { get { return enginecapacity; } set { enginecapacity = value; } }
        public int EnginePower { get { return enginepower; } set { enginepower = value; } }
        public String FuelType { get { return fueltype; } set { fueltype = value; } }
        public Decimal StdConsumption { get { return stdconsumption; } set { stdconsumption = value; } }
        public String ExternalReference { get { return externalreference; } set { externalreference = value; } }
        public String InService_b { get { return inservice_b; } set { inservice_b = value; } }
        public String AdditionalInfo { get { return additionalinfo; } set { additionalinfo = value; } }
        public DateTime CreatedDate { get { return createddate; } set { createddate = value; } }
        public DateTime UpdatedDate { get { return updateddate; } set { updateddate = value; } }

        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }

        public bool Delete(AssetExtProVehicle uAssetExtProVehicles)
        {
            //Not Implemented By Design

            return false;
        }
    }
}
