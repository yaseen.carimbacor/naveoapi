﻿using NaveoOneLib.Models.GMatrix;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoOneLib.Models.Assets
{
    public class RoadNetwork
    {
        public int RoadID { get; set; }
        public int VCAID { get; set; }
        public String RoadName { get; set; }
        public Double? LengthM { get; set; }
        public Double? WidthM { get; set; }
        public  int? SpeedLimit { get; set; }
        public String RoadClass { get; set; }
        public String RoadType { get; set; }
        public String GeomData { get; set; }
        public DataRowState oprType { get; set; }

        public List<int> vca { get; set; } = new List<int>();
        public List<int> district { get; set; } = new List<int>();
        public List<int> road { get; set; } = new List<int>();
    }

    public class Country
    {
        public int CountryID { get; set; }
        public String CountryName { get; set; }
        public int iColor { get; set; }
        public String GeomData { get; set; }
        public DataRowState oprType { get; set; }
    }

    public class District
    {
        public int DistrictID { get; set; }
        public String DistrictName { get; set; }
        public int iColor { get; set; }
        public String GeomData { get; set; }
        public int? CountryID { get; set; }
        public DataRowState oprType { get; set; }

        public List<Matrix> lMatrix { get; set; } = new List<Matrix>();
    }
    public class VCA
    {
        public int VCAID { get; set; }
        public int DistrictID { get; set; }
        public String VCAName { get; set; }
        public int iColor { get; set; }
        public String GeomData { get; set; }
        public DataRowState oprType { get; set; }
    }

    public class TreeIDs
    {
        public int id { get; set; }
        public String type { get; set; }
    }
}