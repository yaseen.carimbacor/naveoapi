using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using NaveoOneLib.Common;

namespace NaveoOneLib.Models.Assets
{
    public class VehicleMaintStatus
    {

        private int maintstatusid = Constants.NullInt;
        private String description = Constants.NullString;
        private int sortorder = Constants.NullInt;
        private DateTime createddate = Constants.NullDateTime;
        private String createdby = Constants.NullString;
        private DateTime updateddate = Constants.NullDateTime;
        private String updatedby = Constants.NullString;


        public int MaintStatusId { get { return maintstatusid; } set { maintstatusid = value; } }
        public String Description { get { return description; } set { description = value; } }
        public int SortOrder { get { return sortorder; } set { sortorder = value; } }
        public DateTime CreatedDate { get { return createddate; } set { createddate = value; } }
        public String CreatedBy { get { return createdby; } set { createdby = value; } }
        public DateTime UpdatedDate { get { return updateddate; } set { updateddate = value; } }
        public String UpdatedBy { get { return updatedby; } set { updatedby = value; } }
    }
}
