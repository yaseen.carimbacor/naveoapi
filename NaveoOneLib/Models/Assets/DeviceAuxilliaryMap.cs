using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using NaveoOneLib.Common;

namespace NaveoOneLib.Models.Assets
{
    public class DeviceAuxilliaryMap
    {
        public List<CalibrationChart> lCalibration { get; set; } = new List<CalibrationChart>();


        public DataRowState oprType {get;set;}

        public int Uid { get; set; }
        public int? MapId { get; set; }
        public String DeviceId { get; set; }
        public String Auxilliary { get; set; }
        public int? AuxilliaryType { get; set; }
        public String Field1 { get; set; }
        public String Field2 { get; set; }

        public int? Field3 { get; set; }
        public String Field3Desc { get; set; }

        public int? Field4 { get; set; }
        public String Field5 { get; set; }
        public DateTime? Field6 { get; set; }

        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }

        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }

        public int? Uom { get; set; }
        public int? Thresholdhigh { get; set; }
        public int? Thresholdlow { get; set; }
        public int? Capacity { get; set; }
    }
}