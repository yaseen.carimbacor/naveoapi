using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using NaveoOneLib.Common;

namespace NaveoOneLib.Models.Assets
{
    public class AssetTypeMapping
    {
        private int iid = Constants.NullInt;
        private String description = Constants.NullString;
        private int assettypeid = Constants.NullInt;
        private String createdby = Constants.NullString;
        private DateTime createddate = Constants.NullDateTime;
        private String updatedby = Constants.NullString;
        private DateTime updateddate = Constants.NullDateTime;
        private int mappingid = Constants.NullInt;

        public int IID { get { return iid; } set { iid = value; } }
        public String Description { get { return description; } set { description = value; } }
        public int AssetTypeId { get { return assettypeid; } set { assettypeid = value; } }
        public String CreatedBy { get { return createdby; } set { createdby = value; } }
        public DateTime CreatedDate { get { return createddate; } set { createddate = value; } }
        public String UpdatedBy { get { return updatedby; } set { updatedby = value; } }
        public DateTime UpdatedDate { get { return updateddate; } set { updateddate = value; } }
        public int MappingId { get { return mappingid; } set { mappingid = value; } }
    }
}