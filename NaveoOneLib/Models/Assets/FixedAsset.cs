﻿using NaveoOneLib.Models.GMatrix;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoOneLib.Models.Assets
{
    public class FixedAsset
    {
        public int AssetID { get; set; }
        public String AssetCode { get; set; }
        public int AssetType { get; set; }
        public DateTime dtCreated { get; set; }

        public String AssetName { get; set; }
        public String Descript { get; set; }

        public int? RoadID { get; set; }
        public int? LocalityID { get; set; }
        public int? VillageID { get; set; }

        public int? AssetCategory { get; set; }
        public String AssetFamily { get; set; }
        public String Owners { get; set; }
        public String Notes { get; set; }
        public String PictureName { get; set; }
        public String MaintLink { get; set; }
        public String sValue { get; set; }
        public String TitleDeed { get; set; }
        public String DistanceUnit { get; set; }
        public String Pin { get; set; }
        public String GeomData { get; set; }

        public Double? Height { get; set; }
        public Double? Wattage { get; set; }
        public Double? Price_m2 { get; set; }
        public Double? SurfaceArea { get; set; }
        public Double? Distance { get; set; }
        public DateTime? ExpDate { get; set; }
        public int? MarketValue { get; set; }
        public int? NumberOfP { get; set; }

        public DataRowState oprType { get; set; }

        public List<Matrix> lMatrix { get; set; } = new List<Matrix>();
        public GISAssetType gisAssetType { get; set; } = new GISAssetType();
        public GISAssetCategory gisAssetCategory { get; set; } = new GISAssetCategory();

        public List<String> PicNames { get; set; }
        public String Geom2Json { get; set; }
    }
}

