using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;

namespace NaveoOneLib.Models.Assets
{
    public class AssetUOM
    {
        public int iID { get; set; }
        public int AssetID { get; set; }
        public int UOM { get; set; }
        public int? Capacity { get; set; }
        public DataRowState oprType { get; set; }
    }
}