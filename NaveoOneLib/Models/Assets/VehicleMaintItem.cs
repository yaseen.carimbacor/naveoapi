﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using NaveoOneLib.Common;

namespace NaveoOneLib.Models.Assets
{
    public class VehicleMaintItem
    {
        private int uri = Constants.NullInt;
        private int mainturi = Constants.NullInt;
        private String itemcode = Constants.NullString;
        private String description = Constants.NullString;
        private int quantity = Constants.NullInt;
        private Decimal unitcost = Constants.NullDecimal;
        private DateTime createddate = Constants.NullDateTime;
        private String createdby = Constants.NullString;
        private DateTime updateddate = Constants.NullDateTime;
        private String updatedby = Constants.NullString;

        public int URI { get { return uri; } set { uri = value; } }
        public int MaintURI { get { return mainturi; } set { mainturi = value; } }
        public String ItemCode { get { return itemcode; } set { itemcode = value; } }
        public String Description { get { return description; } set { description = value; } }
        public int Quantity { get { return quantity; } set { quantity = value; } }
        public Decimal UnitCost { get { return unitcost; } set { unitcost = value; } }
        public DateTime CreatedDate { get { return createddate; } set { createddate = value; } }
        public String CreatedBy { get { return createdby; } set { createdby = value; } }
        public DateTime UpdatedDate { get { return updateddate; } set { updateddate = value; } }
        public String UpdatedBy { get { return updatedby; } set { updatedby = value; } }
    }
}
