
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using NaveoOneLib.Common;

namespace NaveoOneLib.Models.Assets
{
    public class AuxilliaryType
    {

        private int uid = Constants.NullInt;
        private String auxilliarytypename = Constants.NullString;
        private String auxilliarytypedescription = Constants.NullString;
        private String field1 = Constants.NullString;
        private String field1label = Constants.NullString;
        private String field1type = Constants.NullString;
        private String field2 = Constants.NullString;
        private String field2label = Constants.NullString;
        private String field2type = Constants.NullString;
        private String field3 = Constants.NullString;
        private String field3label = Constants.NullString;
        private String field3type = Constants.NullString;
        private String field4 = Constants.NullString;
        private String field4label = Constants.NullString;
        private String field4type = Constants.NullString;
        private String field5 = Constants.NullString;
        private String field5label = Constants.NullString;
        private String field5type = Constants.NullString;
        private String field6 = Constants.NullString;
        private String field6label = Constants.NullString;
        private String field6type = Constants.NullString;
        private String field7 = Constants.NullString;
        private String field7label = Constants.NullString;
        private String field7type = Constants.NullString;
        private DateTime createddate = Constants.NullDateTime;
        private String createdby = Constants.NullString;
        private DateTime updateddate = Constants.NullDateTime;
        private String updatedby = Constants.NullString;


        public int Uid { get { return uid; } set { uid = value; } }
        public String AuxilliaryTypeName { get { return auxilliarytypename; } set { auxilliarytypename = value; } }
        public String AuxilliaryTypeDescription { get { return auxilliarytypedescription; } set { auxilliarytypedescription = value; } }
        public String Field1 { get { return field1; } set { field1 = value; } }
        public String Field1Label { get { return field1label; } set { field1label = value; } }
        public String Field1Type { get { return field1type; } set { field1type = value; } }
        public String Field2 { get { return field2; } set { field2 = value; } }
        public String Field2Label { get { return field2label; } set { field2label = value; } }
        public String Field2Type { get { return field2type; } set { field2type = value; } }
        public String Field3 { get { return field3; } set { field3 = value; } }
        public String Field3Label { get { return field3label; } set { field3label = value; } }
        public String Field3Type { get { return field3type; } set { field3type = value; } }
        public String Field4 { get { return field4; } set { field4 = value; } }
        public String Field4Label { get { return field4label; } set { field4label = value; } }
        public String Field4Type { get { return field4type; } set { field4type = value; } }
        public String Field5 { get { return field5; } set { field5 = value; } }
        public String Field5Label { get { return field5label; } set { field5label = value; } }
        public String Field5Type { get { return field5type; } set { field5type = value; } }
        public String Field6 { get { return field6; } set { field6 = value; } }
        public String Field6Label { get { return field6label; } set { field6label = value; } }
        public String Field6Type { get { return field6type; } set { field6type = value; } }
        public String Field7 { get { return field7; } set { field7 = value; } }
        public String Field7Label { get { return field7label; } set { field7label = value; } }
        public String Field7Type { get { return field7type; } set { field7type = value; } }
        public DateTime CreatedDate { get { return createddate; } set { createddate = value; } }
        public String CreatedBy { get { return createdby; } set { createdby = value; } }
        public DateTime UpdatedDate { get { return updateddate; } set { updateddate = value; } }
        public String UpdatedBy { get { return updatedby; } set { updatedby = value; } }
    }
}
