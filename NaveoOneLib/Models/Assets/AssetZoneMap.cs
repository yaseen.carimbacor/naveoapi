
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using NaveoOneLib.Common;

namespace NaveoOneLib.Models.Assets
{
    public class AssetZoneMap
    {

        private int id = Constants.NullInt;
        private int assetid = Constants.NullInt;
        private int zoneid = Constants.NullInt;
        private String param1 = Constants.NullString;
        private String param2 = Constants.NullString;
        private String param3 = Constants.NullString;

        public DataRowState oprType { get; set; }
        private String createdby = Constants.NullString;
        private DateTime createddate = Constants.NullDateTime;
        private String modifiedby = Constants.NullString;
        private DateTime modifieddate = Constants.NullDateTime;


        public int Id { get { return id; } set { id = value; } }
        public int AssetID { get { return assetid; } set { assetid = value; } }
        public int ZoneID { get { return zoneid; } set { zoneid = value; } }
        public String Param1 { get { return param1; } set { param1 = value; } }
        public String Param2 { get { return param2; } set { param2 = value; } }
        public String Param3 { get { return param3; } set { param3 = value; } }
        public String CreatedBy { get { return createdby; } set { createdby = value; } }
        public DateTime CreatedDate { get { return createddate; } set { createddate = value; } }
        public String ModifiedBy { get { return modifiedby; } set { modifiedby = value; } }
        public DateTime ModifiedDate { get { return modifieddate; } set { modifieddate = value; } }
    }
}
