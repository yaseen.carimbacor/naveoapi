using System;
using NaveoOneLib.Common;


namespace NaveoOneLib.Models.Trips
{
    public class ExtTripInfoDetail
    {

        private int iid = Constants.NullInt;
        public int IID { get { return iid; } set { iid = value; } }

        private int exttripid = Constants.NullInt;
        public int ExtTripId
        {
            get { return exttripid; }
            set { exttripid = value; }
        }

        private int GPSDataId = Constants.NullInt;
        public int GPSDataID { get { return GPSDataId; } set { GPSDataId = value; } }

        private DateTime datetimeGPS_UTC = Constants.NullDateTime;
        public DateTime DateTimeGPS_UTC { get { return datetimeGPS_UTC; } set { datetimeGPS_UTC = value; } }


        //public DataTable GetExtTripInfoDetail()
        //{
        //    return new ExtTripInfoDetailService().GetExtTripInfoDetail();
        //}
        //public ExtTripInfoDetail GetExtTripInfoDetailById(String ID)
        //{
        //    return new ExtTripInfoDetailService().GetExtTripInfoDetailById(ID);
        //}
        //public bool Save(ExtTripInfoDetail SExtTripInfoDetail)
        //{
        //    return new ExtTripInfoDetailService().SaveExtTripInfoDetail(SExtTripInfoDetail, null);
        //}
        //public bool Save(ExtTripInfoDetail SExtTripInfoDetail, DbTransaction transaction)
        //{
        //    return new ExtTripInfoDetailService().SaveExtTripInfoDetail(SExtTripInfoDetail, transaction);
        //}
        //public bool Update(ExtTripInfoDetail SExtTripInfoDetail)
        //{
        //    return new ExtTripInfoDetailService().UpdateExtTripInfoDetail(SExtTripInfoDetail, null);
        //}
        //public bool Update(ExtTripInfoDetail SExtTripInfoDetail, DbTransaction transaction)
        //{
        //    return new ExtTripInfoDetailService().UpdateExtTripInfoDetail(SExtTripInfoDetail, transaction);
        //}
        //public bool Delete(ExtTripInfoDetail SExtTripInfoDetail)
        //{
        //    return new ExtTripInfoDetailService().DeleteExtTripInfoDetail(SExtTripInfoDetail);
        //}

    }
}
