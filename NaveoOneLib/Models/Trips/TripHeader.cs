﻿using System;
using System.Collections.Generic;
using NaveoOneLib.Common;
using NaveoOneLib.Models.GPS;
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.Drivers;

namespace NaveoOneLib.Models.Trips
{
    public class TripHeader
    {
        private int iid = Constants.NullInt;
        private int assetid = Constants.NullInt;
        private int driverid = Constants.NullInt;
        private int exceptionflag = Constants.NullInt;
        private int maxspeed = Constants.NullInt;
        private TimeSpan idlingtime = TimeSpan.MinValue;
        private TimeSpan stoptime = TimeSpan.MinValue;
        private TimeSpan triptime = TimeSpan.MinValue;
        private float tripdistance = Constants.NullFloat;
        private int gpsdataenduid = Constants.NullInt;
        private int gpsdatastartuid = Constants.NullInt;
        private GPSData gpsdatastart;
        private GPSData gpsdataend;
        private String departureaddress;
        private String arrivaladdress;
        private String departurezone;
        private String arrivalzone;
        private int departurezoneid;
        private int arrivalzoneid;
        private int arrivalzonecolor;
        private int calculatedodometer;
        private TimeSpan overspeed1time = TimeSpan.FromSeconds(0);
        private TimeSpan overspeed2time = TimeSpan.FromSeconds(0);
        private int accel = 0;
        private int brake = 0;
        private int corner = 0;

        private List<TripDetail> tripdetails;
        public List<TripDetail> TDetails { get { return tripdetails; } set { tripdetails = value; } }

        private Asset myasset;
        public Asset myAsset { get { return myasset; } set { myasset = value; } }
        private Driver mydriver;
        public Driver myDriver { get { return mydriver; } set { mydriver = value; } }

        public int iID { get { return iid; } set { iid = value; } }
        public int AssetID { get { return assetid; } set { assetid = value; } }
        public int DriverID { get { return driverid; } set { driverid = value; } }
        public int ExceptionFlag { get { return exceptionflag; } set { exceptionflag = value; } }
        public int MaxSpeed { get { return maxspeed; } set { maxspeed = value; } }
        public TimeSpan IdlingTime { get { return idlingtime; } set { idlingtime = value; } }
        public TimeSpan StopTime { get { return stoptime; } set { stoptime = value; } }
        public TimeSpan TripTime { get { return triptime; } set { triptime = value; } }
        public float TripDistance { get { return tripdistance; } set { tripdistance = value; } }
        public int GPSDataStartUID { get { return gpsdatastartuid; } set { gpsdatastartuid = value; } }
        public int GPSDataEndUID { get { return gpsdataenduid; } set { gpsdataenduid = value; } }
        public GPSData GPSDataEnd { get { return gpsdataend; } set { gpsdataend = value; } }
        public GPSData GPSDataStart { get { return gpsdatastart; } set { gpsdatastart = value; } }
        public String DepartureAddress { get { return departureaddress; } set { departureaddress = value; } }
        public String ArrivalAddress { get { return arrivaladdress; } set { arrivaladdress = value; } }
        public String DepartureZone { get { return departurezone; } set { departurezone = value; } }
        public String ArrivalZone { get { return arrivalzone; } set { arrivalzone = value; } }
        public int DepartureZoneID { get { return departurezoneid; } set { departurezoneid = value; } }
        public int ArrivalZoneID { get { return arrivalzoneid; } set { arrivalzoneid = value; } }
        public int CalculatedOdometer { get { return calculatedodometer; } set { calculatedodometer = value; } }
        public TimeSpan OverSpeed1Time { get { return overspeed1time; } set { overspeed1time = value; } }
        public TimeSpan OverSpeed2Time { get { return overspeed2time; } set { overspeed2time = value; } }
        public int Accel { get { return accel; } set { accel = value; } }
        public int Brake { get { return brake; } set { brake = value; } }
        public int Corner { get { return corner; } set { corner = value; } }
        public int ArrivalZoneColor { get { return arrivalzonecolor; } set { arrivalzonecolor = value; } }

        //Checkmate specific. need to remove afterwards
        private String geo_deviceid = String.Empty;
        public String Geo_DeviceID { get { return geo_deviceid; } set { geo_deviceid = value; } }
        private String geo_ibutton = String.Empty;
        public String Geo_iButton { get { return geo_ibutton; } set { geo_ibutton = value; } }
        private DateTime geo_starttriptime = Constants.NullDateTime;
        public DateTime Geo_StartTripTime { get { return geo_starttriptime; } set { geo_starttriptime = value; } }
        private DateTime geo_endtriptime = Constants.NullDateTime;
        public DateTime Geo_EndTripTime { get { return geo_endtriptime; } set { geo_endtriptime = value; } }
        private int cmth = 0;
        public int CMth { get { return cmth; } set { cmth = value; } }
    }
}