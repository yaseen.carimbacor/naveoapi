﻿using System;
using NaveoOneLib.Common;
using NaveoOneLib.Models.GPS;

namespace NaveoOneLib.Models.Trips
{
    public class TripDetail
    {

        private Int64 iid = Constants.NullInt;
        private int headeriid = Constants.NullInt;
        private int gpsdatauid = Constants.NullInt;

        private GPSData gpsdata;
        public GPSData GpsData { get { return gpsdata; } set { gpsdata = value; } }

        public Int64 iID { get { return iid; } set { iid = value; } }
        public int HeaderiID { get { return headeriid; } set { headeriid = value; } }
        public int GPSDataUID { get { return gpsdatauid; } set { gpsdatauid = value; } }
    }
}