using System;
using NaveoOneLib.Common;


namespace NaveoOneLib.Models.Trips
{
    public class ExtTripInfoMapping
    {

        private int iid = Constants.NullInt;
        private int mappingid = Constants.NullInt;
        private String description = Constants.NullString;
        
        private int exttripinfoid = Constants.NullInt;
        private String createdby = Constants.NullString;
        private DateTime createddate = Constants.NullDateTime;
        private String updatedby = Constants.NullString;
        private DateTime updateddate = Constants.NullDateTime;


        public int IID { get { return iid; } set { iid = value; } }
        public int MappingID { get { return mappingid; } set { mappingid = value; } }
        public String Description { get { return description; } set { description = value; } }
        
        public int ExtTripInfoId { get { return exttripinfoid; } set { exttripinfoid = value; } }
        public String CreatedBy { get { return createdby; } set { createdby = value; } }
        public DateTime CreatedDate { get { return createddate; } set { createddate = value; } }
        public String UpdatedBy { get { return updatedby; } set { updatedby = value; } }
        public DateTime UpdatedDate { get { return updateddate; } set { updateddate = value; } }
    }
}