using System;
using System.Collections.Generic;

using NaveoOneLib.Common;


namespace NaveoOneLib.Models.Trips
{
    public class ExtTripInfo
    {

        private int iid = Constants.NullInt;
        private string exttripid = Constants.NullString;

        private DateTime datetimeGPSStart_UTC = Constants.NullDateTime;
        public DateTime DateTimeGPSStart_UTC { get { return datetimeGPSStart_UTC; } set { datetimeGPSStart_UTC = value; } }

        private DateTime datetimeGPSStop_UTC = Constants.NullDateTime;
        public DateTime DateTimeGPSStop_UTC { get { return datetimeGPSStop_UTC; } set { datetimeGPSStop_UTC = value; } }

        private float consumption = Constants.NullFloat;
        public float Consumption
        {
            get { return consumption; }
            set { consumption = value; }
        }

        private float timediff = Constants.NullInt;
        public float Timediff
        {
            get { return timediff; }
            set { timediff = value; }
        }

        public string ExtTripID
        {
            get { return exttripid; }
            set { exttripid = value; }
        }
        private int? field1name = Constants.NullInt;
        private String field1value = Constants.NullString;
        private int? field2name = Constants.NullInt;
        private String field2value = Constants.NullString;
        private int? field3name = Constants.NullInt;
        private String field3value = Constants.NullString;
        private int? field4name = Constants.NullInt;
        private String field4value = Constants.NullString;
        private int? field5name = Constants.NullInt;
        private String field5value = Constants.NullString;
        private int? field6name = Constants.NullInt;
        private String field6value = Constants.NullString;

        private String Additionalfield1Value = Constants.NullString;
        public String AdditionalField1Value
        {
            get { return Additionalfield1Value; }
            set { Additionalfield1Value = value; }
        }
        private String Additionalfield1Label = Constants.NullString;
        public String AdditionalField1Label
        {
            get { return Additionalfield1Label; }
            set { Additionalfield1Label = value; }
        }

        private String Additionalfield2Value = Constants.NullString;
        public String AdditionalField2Value
        {
            get { return Additionalfield2Value; }
            set { Additionalfield2Value = value; }
        }
        private String Additionalfield2Label = Constants.NullString;
        public String AdditionalField2Label
        {
            get { return Additionalfield2Label; }
            set { Additionalfield2Label = value; }
        }

        private String createdby = Constants.NullString;
        private DateTime createddate = Constants.NullDateTime;
        private String updatedby = Constants.NullString;
        private DateTime updateddate = Constants.NullDateTime;
        private String assetname = Constants.NullString;
        private List<ExtTripInfoDetail> extDetail;

        private List<ExtTripInfoResource> lEmpbase;

        public List<ExtTripInfoResource> lEmpBase
        {
            get { return lEmpbase; }
            set { lEmpbase = value; }
        }

        private double tripdistance = Constants.NullDouble;      




        public int IID { get { return iid; } set { iid = value; } }
        public int? Field1Name { get { return field1name; } set { field1name = value; } }
        public String Field1Value { get { return field1value; } set { field1value = value; } }
        public int? Field2Name { get { return field2name; } set { field2name = value; } }
        public String Field2Value { get { return field2value; } set { field2value = value; } }
        public int? Field3Name { get { return field3name; } set { field3name = value; } }
        public String Field3Value { get { return field3value; } set { field3value = value; } }

        public int? Field4Name { get { return field4name; } set { field4name = value; } }
        public String Field4Value { get { return field4value; } set { field4value = value; } }

        public int? Field5Name { get { return field5name; } set { field5name = value; } }
        public String Field5Value { get { return field5value; } set { field5value = value; } }

        public int? Field6Name { get { return field6name; } set { field6name = value; } }
        public String Field6Value { get { return field6value; } set { field6value = value; } }


        public String CreatedBy { get { return createdby; } set { createdby = value; } }
        public DateTime CreatedDate { get { return createddate; } set { createddate = value; } }
        public String UpdatedBy { get { return updatedby; } set { updatedby = value; } }
        public DateTime UpdatedDate { get { return updateddate; } set { updateddate = value; } }
        public String AssetName { get { return assetname; } set { assetname = value; } }
        public List<ExtTripInfoDetail> ExtDetail
        {
            get { return extDetail; }
            set { extDetail = value; }
        }
        public double TripDistance { get { return tripdistance; } set { tripdistance = value; } }
    }
}
