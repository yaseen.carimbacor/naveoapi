using System;
using NaveoOneLib.Common;


namespace NaveoOneLib.Models.Trips
{
    public class ExtTripInfoResource
    {
        private int empcode = Constants.NullInt;
        private String naacodevalue = Constants.NullString;
        private int exttripid = Constants.NullInt;

        public int EmpCode { get { return empcode; } set { empcode = value; } }
        public String NaaCodeValue { get { return naacodevalue; } set { naacodevalue = value; } }
        public int ExtTripID { get { return exttripid; } set { exttripid = value; } }
    }
}
