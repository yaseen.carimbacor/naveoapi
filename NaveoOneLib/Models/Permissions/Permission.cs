﻿using System;
using System.Data;

namespace NaveoOneLib.Models.Permissions
{
    public class Permission
    {
        public int iID { get; set; }
        public String ControllerName { get; set; }
        public int ModuleID { get; set; }
        public DataRowState oprType { get; set; }
    }
}
