﻿using System;

namespace NaveoOneLib.Models.Permissions
{
    public class SecurityToken
    {
        public Guid OneTimeToken { get; set; }
        public int UID { get; set; }
        public Guid UserToken { get; set; }
        public Boolean IsAuthorized { get; set; }
        public String sConnStr { get; set; }
        public String DebugMessage { get; set; }

        public DateTime expiryDate { get; set; }
        public DateTime timeOut { get; set; }

        public int? Page { get; set; }
        public int? LimitPerPage { get; set; }
        public Boolean? IsExpired { get; set; }
        public Boolean HasTimedOut { get; set; }

        public static Guid DEFAULT_GUID = new Guid("00000000-0000-0000-0000-000000000000");      
    }
}
