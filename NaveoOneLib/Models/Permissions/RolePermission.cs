using System.Collections.Generic;
using System.Data;
using NaveoOneLib.Models.GMatrix;


namespace NaveoOneLib.Models.Permissions
{
    public class RolePermission
    {
        public int RoleID { get; set; }
        public int PermissionID { get; set; }
        public int? iCreate { get; set; }
        public int? iRead { get; set; }
        public int? iUpdate { get; set; }
        public int? iDelete { get; set; }
        public int? Report1 { get; set; }
        public int? Report2 { get; set; }
        public int? Report3 { get; set; }
        public int? Report4 { get; set; }
        public DataRowState oprType { get; set; }
        public List<Matrix> lMatrix { get; set; } 
    }
}