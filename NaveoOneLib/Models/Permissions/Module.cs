using System;
using NaveoOneLib.Common;


namespace NaveoOneLib.Models.Permissions
{
    public class Module
    {

        private int uid = Constants.NullInt;
        private String modulename = Constants.NullString;
        private string command = Constants.NullString;
        private String description = Constants.NullString;
        private String title = Constants.NullString;
        private String summary = Constants.NullString;
        private int menuHeaderId = Constants.NullInt;
        private int menuGroupId = Constants.NullInt;
        private int orderIndex = Constants.NullInt;
        private DateTime createddate = Constants.NullDateTime;
        private String createdby = Constants.NullString;
        private DateTime updateddate = Constants.NullDateTime;
        private String updatedby = Constants.NullString;


        public int UID { get { return uid; } set { uid = value; } }
        public int MenuHeaderId { get { return menuHeaderId; } set { menuHeaderId = value; } }
        public int MenuGroupId { get { return menuGroupId; } set { menuGroupId = value; } }
        public int OrderIndex { get { return orderIndex; } set { orderIndex = value; } }
        public String ModuleName { get { return modulename; } set { modulename = value; } }
        public String Description { get { return description; } set { description = value; } }
        public String Title { get { return title; } set { title = value; } }
        public String Summary { get { return summary; } set { summary = value; } }
        public String Command { get { return command; } set { command = value; } }
        public DateTime CreatedDate { get { return createddate; } set { createddate = value; } }
        public String CreatedBy { get { return createdby; } set { createdby = value; } }
        public DateTime UpdatedDate { get { return updateddate; } set { updateddate = value; } }
        public String UpdatedBy { get { return updatedby; } set { updatedby = value; } }
    }
}