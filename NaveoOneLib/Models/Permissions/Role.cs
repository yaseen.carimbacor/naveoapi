using System;
using System.Collections.Generic;
using System.Data;
using NaveoOneLib.Models.GMatrix;


namespace NaveoOneLib.Models.Permissions
{
    public class Role
    {
        public List<Matrix> lMatrix { get; set; }
        public List<RolePermission> lPermissions { get; set; }

        public int RoleID { get; set; }
        public String Description { get; set; }
        public int UserID { get; set; }
        public DataRowState oprType { get; set; }
    }
}