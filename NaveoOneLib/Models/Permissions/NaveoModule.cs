using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NaveoOneLib.Common;
using NaveoOneLib.Services;
using System.Data.Common;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Services.Permissions;

namespace NaveoOneLib.Models.Permissions
{
    public class NaveoModule
    {
        public int iID { get; set; }
        public String description { get; set; }

        //PK of GFI_SYS_GroupMatrixNaveoModule
        public int mID { get; set; }

        public DataRowState oprType { get; set; }
        public List<Permission> lPermissions { get; set; }
        public List<Matrix> lMatrix { get; set; }
    }
}