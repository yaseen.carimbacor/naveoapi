using System;


namespace NaveoOneLib.Models.Zones
{
    public class VCADetail
    {
        public int iID { get; set; }
        public Double Latitude { get; set; }
        public Double Longitude { get; set; }
        public int VCAID { get; set; }
        public int CordOrder { get; set; }
    }
}