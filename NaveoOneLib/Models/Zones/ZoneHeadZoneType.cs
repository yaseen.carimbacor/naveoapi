using NaveoOneLib.Common;


namespace NaveoOneLib.Models.Zones
{
    public class ZoneHeadZoneType
    {

        private int zoneheadid = Constants.NullInt;
        private int zonetypeid = Constants.NullInt;
        private int iid = Constants.NullInt;


        public int ZoneHeadID { get { return zoneheadid; } set { zoneheadid = value; } }
        public int ZoneTypeID { get { return zonetypeid; } set { zonetypeid = value; } }
        public int iID { get { return iid; } set { iid = value; } }
    }
}