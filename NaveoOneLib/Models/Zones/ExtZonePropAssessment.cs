
using System;
using NaveoOneLib.Common;


namespace NaveoOneLib.Models.Zones
{
    public class ExtZonePropAssessment
    {

        private int uid = Constants.NullInt;
        private String apl_id = Constants.NullString;
        private String assess_code = Constants.NullString;
        private DateTime assess_date = Constants.NullDateTime;
        private String assess_detail = Constants.NullString;
        private String assess_recommend = Constants.NullString;
        private String assess_status = Constants.NullString;
        private String assess_by = Constants.NullString;
        private String createdby = Constants.NullString;
        private DateTime createddate = Constants.NullDateTime;
        private String updatedby = Constants.NullString;
        private DateTime updateddate = Constants.NullDateTime;
        private int assess_seq = Constants.NullInt;


        public int UID { get { return uid; } set { uid = value; } }
        public String APL_ID { get { return apl_id; } set { apl_id = value; } }
        public String ASSESS_Code { get { return assess_code; } set { assess_code = value; } }
        public DateTime ASSESS_Date { get { return assess_date; } set { assess_date = value; } }
        public String ASSESS_Detail { get { return assess_detail; } set { assess_detail = value; } }
        public String ASSESS_Recommend { get { return assess_recommend; } set { assess_recommend = value; } }
        public String ASSESS_Status { get { return assess_status; } set { assess_status = value; } }
        public String ASSESS_By { get { return assess_by; } set { assess_by = value; } }
        public String CreatedBy { get { return createdby; } set { createdby = value; } }
        public DateTime CreatedDate { get { return createddate; } set { createddate = value; } }
        public String UpdatedBy { get { return updatedby; } set { updatedby = value; } }
        public DateTime UpdatedDate { get { return updateddate; } set { updateddate = value; } }
        public int ASSESS_SEQ { get { return assess_seq; } set { assess_seq = value; } }
    }
}
