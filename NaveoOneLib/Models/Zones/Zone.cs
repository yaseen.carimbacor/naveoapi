﻿using System;
using System.Collections.Generic;
using NaveoOneLib.Common;
using NaveoOneLib.Models.GMatrix;
using System.Data;

namespace NaveoOneLib.Models.Zones
{
    public class Zone
    {
        public List<Matrix> lMatrix { get; set; } = new List<Matrix>();
        public List<ZoneDetail> zoneDetails { get; set; } = new List<ZoneDetail>();
        public List<ZoneType> lZoneType { get; set; } = new List<ZoneType>();

        public int zoneID { get; set; }
        public String description { get; set; }
        public int displayed { get; set; }
        public String comments { get; set; }
        public int color { get; set; }
        public String externalRef { get; set; }
        public String ref2 { get; set; }
        public int isMarker { get; set; }
        public int iBuffer { get; set; }
        public int LookUpValue { get; set; }
        public int NoOfHouseHold { get; set; }
        public String GeomData { get; set; }
        public DateTime? ZoneExpiry {get;set;}
        public DataRowState oprType { get; set; }

        public Zone GetZone(int iZoneID, List<Zone> lz)
        {
            Zone myZone = new Zone();
            foreach (Zone z in lz)
                if (z.zoneID == iZoneID)
                {
                    myZone = z;
                    break;
                }

            return myZone;
        }
        public Zone GetClone()
        {
            return (Zone)this.MemberwiseClone();
        }
        public Boolean IsPointInside(Zone z, Coordinate point)
        {
            List<Coordinate> poly = new List<Coordinate>();
            foreach (ZoneDetail zd in z.zoneDetails)
                poly.Add(new Coordinate(zd.latitude, zd.longitude));

            return GeoCalc.IsPointInside(poly, point);
        }
    }
}
public class Coordinate
{
    private double lt;
    private double lg;
    public double Lg
    {
        get { return lg; }
        set { lg = value; }
    }
    public double Lt
    {
        get { return lt; }
        set { lt = value; }
    }
    public Coordinate(double lt, double lg)
    {
        this.lt = lt;
        this.lg = lg;
    }
}