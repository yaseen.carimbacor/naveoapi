using System;
using NaveoOneLib.Common;


namespace NaveoOneLib.Models.Zones
{
     public class ExtZoneProps
     {

          private int uid;
          private int zoneid;
          private String zonetype = Constants.NullString;
          private String applicationnum = Constants.NullString;
          private String applicationtype = Constants.NullString;
          private DateTime applicationdate = Constants.NullDateTime;
          private String onames = Constants.NullString;
          private String surname = Constants.NullString;
          private String addressl1 = Constants.NullString;
          private String tvnumber = Constants.NullString;
          private String linkpdf = Constants.NullString;
          private String linkimg = Constants.NullString;


          public int Uid { get { return uid; } set { uid = value; } }
          public int ZoneId { get { return zoneid; } set { zoneid = value; } } 
          public String ZoneType { get { return zonetype; } set { zonetype = value; } }
          public String ApplicationNum { get { return applicationnum; } set { applicationnum = value; } }
          public String ApplicationType { get { return applicationtype; } set { applicationtype = value; } }
          public DateTime ApplicationDate { get { return applicationdate; } set { applicationdate = value; } }
          public String ONames { get { return onames; } set { onames = value; } }
          public String Surname { get { return surname; } set { surname = value; } }
          public String AddressL1 { get { return addressl1; } set { addressl1 = value; } }
          public String TVNumber { get { return tvnumber; } set { tvnumber = value; } }
          public String LinkPDF { get { return linkpdf; } set { linkpdf = value; } }
          public String LinkIMG { get { return linkimg; } set { linkimg = value; } }
     }
}
