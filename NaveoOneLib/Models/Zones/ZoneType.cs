using System;
using System.Collections.Generic;
using System.Data;
using NaveoOneLib.Common;
using NaveoOneLib.Models.GMatrix;


namespace NaveoOneLib.Models.Zones
{
    public class ZoneType
    {
        //From ZHZT Table
        public int ZHZTiID { get; set; }

        public int ZoneTypeID { get; set; }
        public String sDescription { get; set; }
        public String sComments { get; set; }
        public String isSystem { get; set; }

        public List<Matrix> lMatrix { get; set; } = new List<Matrix>();
        public DataRowState oprType { get; set; }

    }
}