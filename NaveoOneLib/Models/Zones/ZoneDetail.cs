﻿using System;
using System.Data;

namespace NaveoOneLib.Models.Zones
{
    public class ZoneDetail
    {
        public int iID { get; set; }
        public Double latitude { get; set; }
        public Double longitude { get; set; }
        public int zoneID { get; set; }
        public int cordOrder { get; set; }
        public DataRowState oprType { get; set; }

    }
}