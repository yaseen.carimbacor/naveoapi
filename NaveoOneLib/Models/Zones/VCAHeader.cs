using System;
using System.Collections.Generic;
using System.Data;

namespace NaveoOneLib.Models.Zones
{
    public class VCAHeader
    {
        public int VCAID { get; set; }
        public String Name { get; set; }
        public String Comments { get; set; }
        public int? Color { get; set; }
        public int? ParentVCAID { get; set; }
        public String GeomData { get; set; }
        public DataRowState oprType { get; set; }

        public List<VCADetail> VCADetail{ get; set; }
    }
}