using System;
using System.Collections.Generic;
using NaveoOneLib.Common;

namespace NaveoOneLib.Models.Zones
{
    public class ExtZonePropDetailsOra
    {
        private int uid;
        private String apl_id = Constants.NullString;
        private DateTime bcm_cdate = Constants.NullDateTime;
        private String bcm_cmt = Constants.NullString;
        private String ref_desc = Constants.NullString;
        private String bcm_status = Constants.NullString;
        private int bcm_seq = Constants.NullInt;



        public int UID { get { return uid; } set { uid = value; } }
        public String APL_ID { get { return apl_id; } set { apl_id = value; } }
        public DateTime BCM_CDATE { get { return bcm_cdate; } set { bcm_cdate = value; } }
        public String BCM_CMT { get { return bcm_cmt; } set { bcm_cmt = value; } }
        public String REF_DESC { get { return ref_desc; } set { ref_desc = value; } }
        public String BCM_STATUS { get { return bcm_status; } set { bcm_status = value; } }
        public int BCM_SEQ { get { return bcm_seq; } set { bcm_seq = value; } }


    }

    public class ExtZonePropDetail
    {
        private int uid;
        private String apl_id = Constants.NullString;
        private DateTime bcm_cdate = Constants.NullDateTime;
        private String bcm_cmt = Constants.NullString;
        private String ref_desc = Constants.NullString;
        private String bcm_status = Constants.NullString;
        private String bcm_decision = Constants.NullString;
        private int bcm_seq = Constants.NullInt;

        private List<DataSnapshot> lDatasnap;
        public List<DataSnapshot> lDataSnap
        {
            get { return lDatasnap; }
            set { lDatasnap = value; }
        }

        public int UID { get { return uid; } set { uid = value; } }
        public String APL_ID { get { return apl_id; } set { apl_id = value; } }
        public DateTime BCM_CDATE { get { return bcm_cdate; } set { bcm_cdate = value; } }
        public String BCM_CMT { get { return bcm_cmt; } set { bcm_cmt = value; } }
        public String REF_DESC { get { return ref_desc; } set { ref_desc = value; } }
        public String BCM_STATUS { get { return bcm_status; } set { bcm_status = value; } }
        public String BCM_DECISION { get { return bcm_decision; } set { bcm_decision = value; } }
        public int BCM_SEQ { get { return bcm_seq; } set { bcm_seq = value; } }
    }
}
