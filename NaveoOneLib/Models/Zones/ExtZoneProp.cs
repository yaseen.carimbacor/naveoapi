using System;
using NaveoOneLib.Common;


namespace NaveoOneLib.Models.Zones
{
    public class ExtZoneProp
    {
        private int iid;
        private String apl_id = Constants.NullString;
        private String apl_name = Constants.NullString;
        private String apl_fname = Constants.NullString;
        private DateTime apl_date = Constants.NullDateTime;
        private DateTime apl_eff_date = Constants.NullDateTime;
        private String apl_addr1 = Constants.NullString;
        private String apl_addr2 = Constants.NullString;
        private String apl_addr3 = Constants.NullString;
        private String apl_loc_plan = Constants.NullString;
        private String apl_tv_no = Constants.NullString;
        private String apl_prop_devp = Constants.NullString;
        private Double apl_extent = Constants.NullFloat;
        private String link_pdf = Constants.NullString;
        private String type = Constants.NullString;
        private Double buffersize = Constants.NullFloat;
        private Double point_x = Constants.NullFloat;
        private Double point_y = Constants.NullFloat;
        private string status = Constants.NullString;
        //private DataRowState oprType = Constants.;

        //private List<ExtZonePropAssessment> lextZonePropAssessment = new List<ExtZonePropAssessment>();
        //public List<ExtZonePropAssessment> lExtZonePropAssessment
        //{
        //    get { return lextZonePropAssessment; }
        //    set { lextZonePropAssessment = value; }
        //}

        //private List<ExtZonePropDetails> lextZonePropDetail = new List<ExtZonePropDetails>();
        //public List<ExtZonePropDetails> lExtZonePropDetail
        //{
        //    get { return lextZonePropDetail; }
        //    set { lextZonePropDetail = value; }
        //}

        public int IID { get { return iid; } set { iid = value; } }
        public String APL_ID { get { return apl_id; } set { apl_id = value; } }
        public String APL_NAME { get { return apl_name; } set { apl_name = value; } }
        public String APL_FNAME { get { return apl_fname; } set { apl_fname = value; } }
        public DateTime APL_DATE { get { return apl_date; } set { apl_date = value; } }
        public DateTime APL_EFF_DATE { get { return apl_eff_date; } set { apl_eff_date = value; } }
        public String APL_ADDR1 { get { return apl_addr1; } set { apl_addr1 = value; } }
        public String APL_ADDR2 { get { return apl_addr2; } set { apl_addr2 = value; } }
        public String APL_ADDR3 { get { return apl_addr3; } set { apl_addr3 = value; } }
        public String APL_LOC_PLAN { get { return apl_loc_plan; } set { apl_loc_plan = value; } }
        public String APL_TV_NO { get { return apl_tv_no; } set { apl_tv_no = value; } }
        public String APL_PROP_DEVP { get { return apl_prop_devp; } set { apl_prop_devp = value; } }
        public Double APL_EXTENT { get { return apl_extent; } set { apl_extent = value; } }
        public String Link_Pdf { get { return link_pdf; } set { link_pdf = value; } }
        public String TYPE { get { return type; } set { type = value; } }
        public Double BUFFERSIZE { get { return buffersize; } set { buffersize = value; } }
        public Double POINT_X { get { return point_x; } set { point_x = value; } }
        public Double POINT_Y { get { return point_y; } set { point_y = value; } }
        public String STATUS { get { return status; } set { status = value; } }
        //        public DataRowState oprType {get;set;}
    }
}