using System;
using System.Collections.Generic;

namespace NaveoOneLib.Models.Telemetry
{
    public class TelDataHeader
    {
        public int iID { get; set; }
        public String MsgHeader { get; set; }
        public String Type { get; set; }
        public int AssetID { get; set; }
        public DateTime UTCdt { get; set; }
        public String MsgStatus { get; set; }
        public int? isMaintenanceMode { get; set; }
        public int? isFailedCallout { get; set; }
        public float? Temperature { get; set; }
        public int? isBatteryLow { get; set; }
        public int? isAutoConfig { get; set; }
        public String Carrier { get; set; }
        public int? SignalStrength { get; set; }
        public String DeviceID { get; set; }

        private List<TelDataDetail> lteldatadetail = new List<TelDataDetail>();
        public List<TelDataDetail> lTelDataDetail { get { return lteldatadetail; } set { lteldatadetail = value; } }
    }
}