using System;
using NaveoOneLib.Common;

namespace NaveoOneLib.Models.Telemetry
{
    public class TelDataDetail
    {
        private int iid = Constants.NullInt;
        private int headeriid = Constants.NullInt;
        private DateTime utcdt = Constants.NullDateTime;
        private int sensor = Constants.NullInt;
        private float sensorreading = Constants.NullInt;

        public int iID { get { return iid; } set { iid = value; } }
        public int HeaderiID { get { return headeriid; } set { headeriid = value; } }
        public DateTime UTCdt { get { return utcdt; } set { utcdt = value; } }
        public int Sensor { get { return sensor; } set { sensor = value; } }
        public float SensorReading { get { return sensorreading; } set { sensorreading = value; } }
        public int? SensorAddress { get; set; }
        public int? UOM { get; set; }
    }
}