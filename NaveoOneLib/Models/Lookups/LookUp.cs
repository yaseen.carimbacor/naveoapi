using System;
using NaveoOneLib.Common;


namespace NaveoOneLib.Models.Lookups
{
    public class LookUp
    {

        private int lookupid = Constants.NullInt;
        private String lookupcode = Constants.NullString;
        private String lookupdesc = Constants.NullString;
        private int lookupparentid = Constants.NullInt;
        private String createdby = Constants.NullString;
        private DateTime createddate = Constants.NullDateTime;
        private String updatedby = Constants.NullString;
        private DateTime updateddate = Constants.NullDateTime;


        public int LookUpId { get { return lookupid; } set { lookupid = value; } }
        public String LookUpCode { get { return lookupcode; } set { lookupcode = value; } }
        public String LookUpDesc { get { return lookupdesc; } set { lookupdesc = value; } }
        public int LookUpParentID { get { return lookupparentid; } set { lookupparentid = value; } }
        public String CreatedBy { get { return createdby; } set { createdby = value; } }
        public DateTime CreatedDate { get { return createddate; } set { createddate = value; } }
        public String UpdatedBy { get { return updatedby; } set { updatedby = value; } }
        public DateTime UpdatedDate { get { return updateddate; } set { updateddate = value; } }
    }
}