using System;

namespace NaveoOneLib.Models.Lookups
{
    public class LookUpValues
    {
        public int VID { get; set; }
        public int TID { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public Double Value { get; set; }
        public Double Value2 { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public int? RootMatrixID { get; set; }
    }
}