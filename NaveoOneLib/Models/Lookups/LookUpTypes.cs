using System;


namespace NaveoOneLib.Models.Lookups
{
    public class LookUpTypes
    {
        public int TID { get; set; }
        public String Code { get; set; }
        public String Description { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
    }
}