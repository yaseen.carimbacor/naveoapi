﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoOneLib.Models.BLUP
{
    public class BLUPAssessment
    {

        public int? UID { get; set; }
        public int? BLUPiID { get; set; }
        public string ASSESS_Code { get; set; }
        public DateTime ASSESS_Date { get; set; }
        public string ASSESS_Detail { get; set; }
        public string ASSESS_Recommend { get; set; }
        public string ASSESS_Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ASSESS_SEQ { get; set; }
        public DataRowState OprType { get; set; }

    }
}
