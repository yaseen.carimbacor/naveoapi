﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoOneLib.Models.BLUP
{
   public class BLUPDetails
    {

        public int? UID { get; set; }
        public int? BLUPiID { get; set; }
        public DateTime BCM_CDATE { get; set; }
        public string BCM_CMT { get; set; }
        public string REF_DESC { get; set; }
        public string BCM_STATUS { get; set; }
        public string BCM_DECISION { get; set; }
        public int? BCM_SEQ { get; set; }

        public DataRowState OprType { get; set; }
    }
}
