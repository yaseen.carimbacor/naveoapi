﻿using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.GMatrix;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoOneLib.Models.BLUP
{
   public class BLUP
    {
        public int? iID { get; set; }
        public string APL_ID { get; set; }
        public string APL_NAME { get; set; }
        public string APL_FNAME { get; set; }
        public DateTime APL_DATE { get; set; }
        public DateTime APL_EFF_DATE { get; set; }
        public string APL_ADDR1 { get; set; }
        public string APL_ADDR2 { get; set; }
        public string APL_ADDR3 { get; set; }
        public string APL_LOC_PLAN { get; set; }
        public string APL_TV_NO { get; set; }
        public string APL_PROP_DEVP { get; set; }
        public float APL_EXTENT { get; set; }
        public int TYPE { get; set; } //null nels, 1 blup
        public Double POINT_X { get; set; }
        public Double POINT_Y { get; set; }
        public string Status { get; set; }
        public int Cat { get; set; } = 1;
        public String GeomData { get; set; }

        public DataRowState OprType { get; set; }

        public List<Matrix> lMatrix { get; set; } = new List<Matrix>();
        public List<BLUPAssessment> lBlupAssessment { get; set; } = new List<BLUPAssessment>();
        public List<BLUPDetails> lBLUPDetails { get; set; } = new List<BLUPDetails>();
    }

    public class Blup
    {
        public List<int> lids { get; set; }
        public Double lon { get; set; }
        public Double lat { get; set; }
        public Boolean bShowBuffers { get; set; }
        public Boolean ShowDataOnly { get; set; }
        public Double dDistance { get; set; } = 0.1;
        public List<TreeIDs> treeIDs { get; set; }
        public List<int> MaintStatusId { get; set; }
        public List<int> MaintTypeId { get; set; }
        //aDD mAINTENANCE sTATUS AND mAINTENENCE TYPE
        public RoadNetwork roadnetwork { get; set; }
    }
}
