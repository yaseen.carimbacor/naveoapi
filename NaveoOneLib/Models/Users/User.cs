﻿using System;
using System.Collections.Generic;
using System.Data;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Permissions;


namespace NaveoOneLib.Models.Users
{
    public class User
    {
        public const String EditModePswd = "EditMode#123.jxxx:)";
        public const String BypassOldPswd = "OldPswdBypassing#123-*.jxxx:)";

        public const String SysIntWrongPassword = "SysInitPass";


        public enum UserResult
        {
            InvalidCredentials
            , InvalidToken
            , TokenExpected
            , PasswordExpired
            , Locked
            , Inactive
            , CouldNotMailToken
            , Succeeded
            , SucceededButPswdToExpire
        }

        public enum NaveoDatabases
        {
            Demo, Mercury, Venus, Earth, Mars, Jupiter, Saturn, Uranus, Neptune, Pluto, Baby, Kids,BlackWidow,Sun,Moon, Sirius,Vega , Pegasus ,Andromeda , Centaurus , Cygnus , Cosmos ,Epsilon 
        }

        private List<Matrix> lmatrix = new List<Matrix>();
        public List<Matrix> lMatrix { get { return lmatrix; } set { lmatrix = value; } }

        private List<Role> lroles = new List<Role>();
        public List<Role> lRoles { get { return lroles; } set { lroles = value; } }

        public UserExtension userExtension { get; set; }

        public int UID { get; set; }
        public String Username { get; set; }
        public String Names { get; set; }
        public String LastName { get; set; }
        public String Email { get; set; }
        public String Tel { get; set; }
        public String MobileNo { get; set; }
        public DateTime DateJoined { get; set; }
        public String Status_b { get; set; }
        public String Password { get; set; }
        public String MenuLevel_cbo { get; set; }
        public String AccessList { get; set; }
        public String StoreUnit_b { get; set; }
        public String UType_cbo { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public String Dummy1 { get; set; }
        public int AccessTemplateId { get; set; }
        public int LoginCount { get; set; }
        public int LoginAttempt { get; set; }
        public String PreviousPassword { get; set; }
        public DateTime PasswordUpdateddate { get; set; }
        public int MaxDayMail { get; set; }

        public int? UsrTypeID { get; set; }
        public String UsrTypeDesc { get; set; }

        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }

        public int? ZoneID { get; set; }

        public string zoneDesc { get; set; }
        public int? PlnLkpVal { get; set; }

        public string PlnKpValDesc { get;set; }

        public int? MobileAccess { get; set; }
        public int? ExternalAccess { get; set; }

        public String TwoStepVerif { get; set; }
        public int TwoStepAttempts { get; set; }
        public Guid UserToken { get; set; }

        public DateTime? PswdExpiryDate { get; set; }
        public DateTime? PswdExpiryWarnDate { get; set; }

        public UserResult QryResult { get; set; }
        public String sQryResult { get; set; }
        public Boolean bTwoWayAuthEnabled { get; set; }
        public DataRowState oprType { get; set; }
        public DateTime lastLogin { get; set; }
        public DateTime lastLogout { get; set; }

        public String SystemAdminEmail { get; set; }
        public String AccessedOn { get; set; }

        public int SystemAdminPhone { get; set; }
    }



    public class UserExpiryDetails { 
        public int ExpiryDay;
        public int ExpiryWarnDay;
        public Boolean WillExpire;
    }


    public class LiveConnections
    {
        public int iID;
        public Guid UserToken;
        public int FleetLogin;
        public int MaintenanceLogin;
        public DataRowState oprType;

    }
}
