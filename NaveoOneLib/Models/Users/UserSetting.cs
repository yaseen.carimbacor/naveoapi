using System;
using NaveoOneLib.Common;


namespace NaveoOneLib.Models.Users
{
    public class UserSetting
    {
        private int iid = Constants.NullInt;
        private int userid = Constants.NullInt;
        private String moduleid = Constants.NullString;
        private String paramtype = Constants.NullString;
        private String parameter = Constants.NullString;
        private String paramvalue1 = Constants.NullString;
        private String paramvalue2 = Constants.NullString;
        private String paramvalue3 = Constants.NullString;
        private DateTime paramdate1 = Constants.NullDateTime;
        private DateTime paramdate2 = Constants.NullDateTime;

        public int iID { get { return iid; } set { iid = value; } }
        public int UserID { get { return userid; } set { userid = value; } }
        public String ModuleID { get { return moduleid; } set { moduleid = value; } }
        public String ParamType { get { return paramtype; } set { paramtype = value; } }
        public String Parameter { get { return parameter; } set { parameter = value; } }
        public String ParamValue1 { get { return paramvalue1; } set { paramvalue1 = value; } }
        public String ParamValue2 { get { return paramvalue2; } set { paramvalue2 = value; } }
        public String ParamValue3 { get { return paramvalue3; } set { paramvalue3 = value; } }
        public DateTime ParamDate1 { get { return paramdate1; } set { paramdate1 = value; } }
        public DateTime ParamDate2 { get { return paramdate2; } set { paramdate2 = value; } }
    }
}