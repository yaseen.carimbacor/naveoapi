using System;
using NaveoOneLib.Common;


namespace NaveoOneLib
{
    public class UserProfile
    {

        private int uid = Constants.NullInt;
        private int userid = Constants.NullInt;
        private String command = Constants.NullString;
        private String allowread = Constants.NullString;
        private String allownew = Constants.NullString;
        private String allowedit = Constants.NullString;
        private String allowdelete = Constants.NullString;



        public int UID { get { return uid; } set { uid = value; } }
        public int UserId { get { return userid; } set { userid = value; } }
        public String Command { get { return command; } set { command = value; } }
        public String AllowRead { get { return allowread; } set { allowread = value; } }
        public String AllowNew { get { return allownew; } set { allownew = value; } }
        public String AllowEdit { get { return allowedit; } set { allowedit = value; } }
        public String AllowDelete { get { return allowdelete; } set { allowdelete = value; } }
    }
}