﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoOneLib.Models.Users
{
    public class UserExtension
    {
        public int?  UserID { get; set; }
        public int? GMApprovalID { get; set; }
        public DataRowState oprType { get; set; }
    }
}
