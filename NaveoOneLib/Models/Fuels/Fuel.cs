﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoOneLib.Models.Fuels
{
    public partial class Fuel
    {
        public int AssetID { get; set; }
        public int ConvertedValue { get; set; }
        public String fType { get; set; }
        public String UOM { get; set; }
        public int GPSDataId { get; set; }
        public DateTime dtLocalTime { get; set; }
        public Double Latitude { get; set; }
        public Double Longitude { get; set; }
        public String Description { get; set; }
    }
}
