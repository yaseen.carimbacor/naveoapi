﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoOneLib.Models.GMatrix
{
    public class GroupMatrixApprovalProcess
    {

        public int iID { get; set; }
        public int gmID { get; set; }
        public string source { get; set; }
        public string approvalName { get; set; }
        public int approvalValue { get; set; }
        public DataRowState oprType { get; set; }

    }
}
