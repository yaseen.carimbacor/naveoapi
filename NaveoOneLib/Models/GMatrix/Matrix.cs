using System;
using System.Data;


namespace NaveoOneLib.Models.GMatrix
{
    public class Matrix
    {
        public enum Objtype
        {
            Assets
            , Drivers
            , Rules
            , Matrix
            , None
        };

        public DataRowState oprType { get; set; }

        public int MID { get; set; }
        public int iID { get; set; }
        public int GMID { get; set; }
        public String GMDesc { get; set; }

    }
}
