using System;
using System.Collections.Generic;
using System.Data;
using NaveoOneLib.Common;
using System.Windows.Forms;
using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Services.GMatrix;

namespace NaveoOneLib.Models.GMatrix
{
    public class GroupMatrix
    {
        public int? isCompany { get; set; }
        public int gmID { get; set; }
        public int? parentGMID { get; set; }

        public String gmDescription { get; set; }
        public String remarks { get; set; }
        public String companyCode { get; set; }
        public String legalName { get; set; }

        public List<NaveoModule> lNaveoModules { get; set; }
        public List<GroupMatrixApprovalProcess> lGroupMatrixApprovalProcess { get; set; }
        public DataRowState oprType { get; set; }

       
        public void PopulateTreeCategories(TreeView tvNode, List<Matrix> lMatrix, NaveoModels strStruc, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in Globals.uLogin.lMatrix)
                iParentIDs.Add(m.GMID);

            new GroupMatrixService().PopulateTreeCategories(tvNode, iParentIDs, strStruc, sConnStr);
        }
    }
}
