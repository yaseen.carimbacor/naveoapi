using System;
using System.Data;
using NaveoOneLib.Common;


namespace NaveoOneLib.Models.Reportings
{
    public class AutoReportingConfigDetail
    {
        public int ARDID { get; set; }
        public int ARID { get; set; }
        public int UID { get; set; }
        public String UIDType { get; set; }
        public String UIDCategory { get; set; }
        public String sMisc { get; set; }


        public DataRowState oprType {get;set;}
    }
}
