using System;
using System.Collections.Generic;
using System.Data;
using NaveoOneLib.Common;


namespace NaveoOneLib.Models.Reportings
{
    public class AutoReportingConfig
    {
        /* ReportID
            110 SMS Notification
            111
            112 Email Notification
            113 Push Notification 
        */

        public enum ReportingType
        {
            SMS = 110,
            Popup = 111,
            email = 112,
            Push = 113
        }
        public int ARID { get; set; }
        public int? TargetID { get; set; }
        public String TargetType { get; set; }
        public int? ReportID { get; set; }
        public int? ReportPeriod { get; set; }
        public String TriggerTime { get; set; }
        public String TriggerInterval { get; set; }
        public String CreatedBy { get; set; }
        public String ReportFormat { get; set; }
        public String ReportType { get; set; }
        public String ReportPath { get; set; }
        public int SaveToDisk { get; set; }
        public String GridXML { get; set; }

        public DataRowState oprType { get; set; }
        public List<AutoReportingConfigDetail> lDetails { get; set; } = new List<AutoReportingConfigDetail>();
    }
}
