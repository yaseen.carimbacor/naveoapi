﻿using System;
using System.Collections.Generic;
using System.Text;
using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using NaveoOneLib.Models.Audits;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.ErrorData;
using System.Linq;

namespace NaveoOneLib.Services.Error
{
    public class ErrorService
    {
        Errors er = new Errors();

        public static DataTable GetFilteredErrorData(String sConnStr, ErrorData eData)
        {
            String userFilter = eData.userIds.Count() > 0 ? " error.UserToken in ('" + string.Join("', '", new Users.UserService().GetUserTokensById(eData.userIds, sConnStr)) + "')" : "";
            String dateFilter = eData.userIds.Count() > 0 ? " and " : "";
            dateFilter += eData.dateFrom == DateTime.MinValue && eData.dateTo == DateTime.MinValue ? "" : " error.time >= '" + eData.dateFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "' and error.time <= '" + eData.dateTo.ToString("dd-MMM-yyyy HH:mm:ss.fff") + @"'";

            String typeFilter = eData.dateFrom == DateTime.MinValue && eData.dateTo == DateTime.MinValue ? "" : " and ";
            if (eData.Status == 10) typeFilter = "";
            else typeFilter += "status " + (eData.Status == 0 ? "IS NULL OR Status = 0" : " = 1") + "";

            String sql = @"
       select errorid,
       requesttype,
        description,
        source,
        stacktrace,
        time,
        status,
       u.names from GFI_SYS_Error error
       inner join GFI_SYS_USER u on error.UserToken = u.UserToken
       WHERE " + userFilter + @"
       " + dateFilter + @" 
       " + typeFilter;

            DataTable dt = new Connection(sConnStr).GetDataDT( sql, sConnStr );
            return dt;
        }

        public Boolean UpdateErrorStatus(BaseError eData, String sConnStr) {
            DataTable dt = this.ErrorDt();
            DataRow dr = dt.NewRow();
            //dr["ErrorID"] = eData.ErrorID;
            dr["RequestType"] = eData.RequestType;
            dr["UserToken"] = eData.UserToken;
            dr["Description"] = eData.Description;
            dr["StackTrace"] = eData.StackTrace;
            dr["Time"] = eData.Time;
            dr["Status"] = eData.Status;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr);
            if (_connection.GenUpdate(dt, "ErrorID = " + eData.ErrorID, null, sConnStr))
                return true;
            else
                return false;
        }

        public BaseError GetErrorById(int iID, String sConnStr)
        {
            String sqlString = @"SELECT * from GFI_SYS_Error
                                        WHERE errorid = " + iID + "";
            DataTable dt = new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetMe(dt.Rows[0]);
        }

        public BaseError GetMe(DataRow dr)
        {
            BaseError err = new BaseError();
            err.ErrorID = Convert.ToInt32(dr["ErrorID"]);
            err.RequestType = dr["RequestType"].ToString();
            err.UserToken = dr["UserToken"].ToString();
            err.Description = dr["Description"].ToString();
            err.Source = dr["Source"].ToString();
            err.StackTrace = dr["StackTrace"].ToString();
            err.Time = (DateTime) dr["Time"];
            err.Status = Convert.ToInt32(dr["Status"]);
            return err;
        }

        public DataTable ErrorDt()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_Error";
            dt.Columns.Add("RequestType", typeof(String));
            dt.Columns.Add("UserToken", typeof(String));
            dt.Columns.Add("Description", typeof(String));
            dt.Columns.Add("Source", typeof(String));
            dt.Columns.Add("StackTrace", typeof(String));
            dt.Columns.Add("Time", typeof(DateTime));
            dt.Columns.Add("Status", typeof(int));

            return dt;
        }
    }
}

