﻿using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Services.Assets;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NaveoOneLib.Utilities;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Services.Zones;
using NaveoOneLib.Maps;

namespace NaveoOneLib.Services.Fuels
{
    public class FuelService
    {
        class DataTableColumn
        {
            public const string TOANALYSE = "ToAnalyse";
            public const string TAG = "Tag";
            public const string CONVERTED_VALUE = "ConvertedValue";
            public const string TYPE_VALUE = "TypeValue";
            public const string FINAL_VALUE = "FinalValue";
            public const string UID = "UID";
        }
        class Fuel
        {
            public const string FILL = "Fill";
            public const string DROP = "Drop";
            public const string NONE = "None";
            public const string REAL = "Real";
            public const string TEMP_DROP = "TempDrop";
            public const string TEMP_FILL = "TempFill";
            public const string FALSE_FILL = "False Fill";
            public const string FALSE_DROP = "False Drop";
        }

        String GetLastProcessedFuel(int AssetID, String sConnStr)
        {
            String sql = @"
--GetLastProcessedFuel
select * from [GFI_GPS_FuelProcessed] where AssetID = " + AssetID + @"--Oracle Add SemiColumn
if(@@ROWCOUNT = 0)
begin
	insert GFI_GPS_FuelProcessed (AssetID) values (" + AssetID + @")--Oracle Add SemiColumn
end

select 'FuelProcessed' as TableName;
select * from [GFI_GPS_FuelProcessed] where AssetID = " + AssetID + @"--Oracle Add SemiColumn

select 'LiveData' as TableName;
select min(dtDatefrom) dtDate from GFI_GPS_ProcessPending where ProcessCode = 'Trips' and AssetID = " + AssetID + @"";
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);
            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            String dt = ds.Tables["FuelProcessed"].Rows[0]["DateTimeGPS_UTC"].ToString();

            DateTime dateProcessed = DateTime.Now;
            DateTime dateLive = DateTime.Now;
            if (DateTime.TryParse(dt, out dateProcessed))
                if (DateTime.TryParse(ds.Tables["LiveData"].Rows[0]["dtDate"].ToString(), out dateLive))
                    if (dateLive < dateProcessed)
                        dt = ds.Tables["LiveData"].Rows[0]["dtDate"].ToString();

            return dt;
        }
        BaseModel GetFuelDataToProcess(String sConnStr)
        {
            BaseModel baseModel = new BaseModel();
            DataTable dt = new DataTable();
            DataTable dtTankCapacity = new DataTable();

            int skipBy = 25;
            DataTable dtAssetIDs = new CalibrationChartService().GetAllAssetsWithFuel(sConnStr);
            foreach (DataRow dr in dtAssetIDs.Rows)
            {
                int iAssetID = Convert.ToInt32(dr["AssetID"]);
                String dtGPS = GetLastProcessedFuel(iAssetID, sConnStr);
                String sql = @"
--GetFuelDataToProcess

with cte as
(
	SELECT h.UID, h.AssetID, h.DateTimeGPS_UTC, h.Longitude, h.Latitude, d.TypeValue, d.UOM
		, row_number() over(partition by h.AssetID order by h.DateTimeGPS_UTC) as RowNum
		, 'OnRoad' sType
		from GFI_GPS_GPSData h
			inner join GFI_GPS_GPSDataDetail d on h.uid = d.uid
            inner join GFI_FLT_Asset a on h.AssetID = a.AssetID and a.AssetType != 5    --Off Road from GFI_FLT_AssetTypeMapping
		where h.TripTime > 300  --h.TripDistance > 1.5 
            --and h.Speed > 5 
            and h.DateTimeGPS_UTC >= '" + dtGPS + @"' and h.AssetID = " + iAssetID + @" and d.TypeID = 'Fuel'
)
, ctf as
(
	SELECT h.UID, h.AssetID, h.DateTimeGPS_UTC, h.Longitude, h.Latitude, d.TypeValue, d.UOM
		, row_number() over(partition by h.AssetID order by h.DateTimeGPS_UTC) as RowNum
		, 'OffRoad' sType
		from GFI_GPS_GPSData h
			inner join GFI_GPS_GPSDataDetail d on h.uid = d.uid
            inner join GFI_FLT_Asset a on h.AssetID = a.AssetID and a.AssetType = 5     --Off Road from GFI_FLT_AssetTypeMapping
		where h.DateTimeGPS_UTC >= '" + dtGPS + @"' and h.AssetID = " + iAssetID + @" and d.TypeID = 'Fuel'
)
select * into #data from cte where RowNum <= " + ((skipBy * 25) + 0) + @"
union
select * from ctf where RowNum <= " + ((skipBy * 25) + 0) + @";

--FinalResult

--aaa

select UID from #data where RowNum = " + ((skipBy * 20) + 0) + @"--@@

if(@@RowCount > 0)
 begin

    update GFI_GPS_FuelProcessed set DateTimeGPS_UTC = (select DateTimeGPS_UTC from #data where RowNum = " + ((skipBy * 20) + 0) + @") where AssetID = " + iAssetID + @";

 end--p

    select 'FuelData' as TableName;

    select * from #data order by DateTimeGPS_UTC;

    select 'TankCapacity' as TableName;

    select a.AssetID, min(c.ConvertedValue) minValue, max(c.ConvertedValue) maxValue from GFI_FLT_CalibrationChart c
	    inner join GFI_FLT_DeviceAuxilliaryMap d on c.AuxID = d.Uid
	    inner join GFI_FLT_AssetDeviceMap a on a.MapID = d.MapID
    where a.AssetID = " + iAssetID + @"
	    group by a.AssetID

";
                Connection c = new Connection(sConnStr);
                DataTable dtSql = c.dtSql();
                DataRow drSql = dtSql.NewRow();

                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "MultipleDTfromQuery";
                dtSql.Rows.Add(drSql);
                DataSet ds = c.GetDataDS(dtSql, sConnStr);

                if (ds.Tables.Contains("FuelData"))
                {
                    if (ds.Tables["FuelData"].Columns.Contains("DateTimeGPS_UTC"))
                        ds.Tables["FuelData"].Columns["DateTimeGPS_UTC"].ColumnName = "dtUTCTemp";

                    DataTable dtFuelCallibrated = GetCallibratedData(ds.Tables["FuelData"], new List<int>() { iAssetID }, "TypeValue", sConnStr);

                    if (dtFuelCallibrated.Columns.Contains("dtUTCTemp"))
                        dtFuelCallibrated.Columns["dtUTCTemp"].ColumnName = "DateTimeGPS_UTC";

                    dt.Merge(dtFuelCallibrated);
                    dtTankCapacity.Merge(ds.Tables["TankCapacity"]);
                }
            }

            dt.TableName = "FuelData";
            dtTankCapacity.TableName = "TankCapacity";

            DataSet dsResult = new DataSet();
            dsResult.Merge(dt);
            dsResult.Merge(dtTankCapacity);
            baseModel.data = dsResult;
            return baseModel;
        }
        public BaseModel ProcessFuelData(String sConnStr)
        {
            #region Variables
            int skipBy = 25;
            double currentReading = 0;
            Double fillThreshold = 5;
            Double dropThreshold = -5;
            int numberOfPointsToAnalyse = 4;
            int rowsToRemove = 4;
            int numOfDuplicate = skipBy * 1;
            List<dynamic> pointsBeforeAfterTemporaryFill;
            List<dynamic> pointsBeforeAfterTemporaryDrop;
            double meanBefore;
            double meanAfter;
            double meanDiff;
            List<int> beforeAfterList = new List<int>();
            DataTable finalResult = new DataTable();
            #endregion

            BaseModel baseModel = GetFuelDataToProcess(sConnStr);
            DataSet ds = baseModel.data;
            // 1. Get Raw data from Sensor
            DataTable dtAllFuelCallibrated = ds.Tables["FuelData"];

            #region Cater for case when no raw data is available

            if (dtAllFuelCallibrated.Rows.Count == 0)
            {
                baseModel.data = false;
                baseModel.errorMessage = "No fuel data found for process";
                return baseModel;
            }

            #endregion

            dtAllFuelCallibrated.Columns.Add(DataTableColumn.TOANALYSE, typeof(bool));
            dtAllFuelCallibrated.Columns.Add(DataTableColumn.TAG);
            dtAllFuelCallibrated.Columns.Add(DataTableColumn.FINAL_VALUE);

            DataView view = new DataView(dtAllFuelCallibrated);
            DataTable assetTableList = view.ToTable(true, "AssetID");
            foreach (DataRow drAsset in assetTableList.Rows)
            {
                try
                {
                    DataRow drTankCapacity = ds.Tables["TankCapacity"].Select("AssetID = " + drAsset["AssetID"].ToString()).FirstOrDefault();
                    int iTankCapacity = Convert.ToInt16(drTankCapacity["maxValue"]) - Convert.ToInt16(drTankCapacity["minValue"]);
                    if (iTankCapacity == 0)
                        iTankCapacity = Convert.ToInt16(drTankCapacity["maxValue"]);

                    DataTable dtFuelCallibrated = dtAllFuelCallibrated.Select("AssetID = " + drAsset["AssetID"].ToString()).CopyToDataTable();
                    //List<CalibrationChart> lCalChart = new CalibrationChartService().GetCalibrationChartLByAssetId((int)drAsset["AssetID"], sConnStr);
                    //Double dTankCapacity = lCalChart.Max(x => x.ConvertedValue) - lCalChart.Min(x => x.ConvertedValue);

                    // 2. Clean data and take median instead of taking mod
                    //int iRowCnt = dtFuelCallibrated.Rows.Count;
                    //for (int i = 0; i < iRowCnt; i += skipBy)
                    //{

                    //}

                    #region  Skip rows. Taking Mode or Median from range
                    int iRowCnt = dtFuelCallibrated.Rows.Count;
                    if (String.IsNullOrEmpty("Mode"))
                        for (int i = 0; i < iRowCnt; i += skipBy)
                        {
                            Double[] x = new Double[skipBy];
                            for (int j = 0; j < skipBy; j++)
                                if (j + i < iRowCnt)
                                    x[j] = Convert.ToDouble(dtFuelCallibrated.Rows[j + i][DataTableColumn.CONVERTED_VALUE]);

                            //Double high = x.OrderByDescending(n => n).First();
                            Double mode = x.GroupBy(n => n).OrderByDescending(g => g.Count()).Select(g => g.Key).FirstOrDefault();

                            if (i + skipBy <= iRowCnt)
                            {
                                dtFuelCallibrated.Rows[i + skipBy - 1][DataTableColumn.TOANALYSE] = true;
                                dtFuelCallibrated.Rows[i + skipBy - 1][DataTableColumn.FINAL_VALUE] = mode;
                            }
                        }
                    else
                        //median
                        for (int i = 0; i < iRowCnt; i += skipBy)
                        {
                            // Here put into list of 25 to find median to represent set of 25 
                            int[] setof25 = new int[skipBy];
                            for (int j = 0; j < skipBy; j++)
                                if (j + i < iRowCnt)
                                    setof25[j] = Convert.ToInt32(dtFuelCallibrated.Rows[j + i][DataTableColumn.CONVERTED_VALUE]);

                            //Here Find Median
                            int median = setof25.ToList().Median();

                            if (i + skipBy <= iRowCnt)
                            {
                                dtFuelCallibrated.Rows[i + skipBy - 1][DataTableColumn.TOANALYSE] = true;
                                dtFuelCallibrated.Rows[i + skipBy - 1][DataTableColumn.FINAL_VALUE] = median;
                            }
                        }
                    #endregion

                    #region Analysing
                    fillThreshold = ((Double)10 / (Double)100) * (Double)iTankCapacity;
                    dropThreshold = (((Double)10 / (Double)100) * (Double)iTankCapacity) * -1;
                    dtFuelCallibrated = Analyse1stLevel(dtFuelCallibrated, fillThreshold, dropThreshold, 0);
                    #endregion

                    #region Determine fills and drops
                    var count = dtFuelCallibrated.Rows.Count - 1;

                    #region Determining final fills and drops
                    for (int i = 0; i < count; i++)
                    {
                        try
                        {
                            if (dtFuelCallibrated.Rows[i][DataTableColumn.TOANALYSE].ToString() == "True")
                            {
                                currentReading = Convert.ToDouble(dtFuelCallibrated.Rows[i][DataTableColumn.FINAL_VALUE]);
                                #region Analyse TEMP_FILL
                                if (dtFuelCallibrated.Rows[i][DataTableColumn.TAG].ToString() == Fuel.TEMP_FILL)
                                {
                                    #region Fill
                                    pointsBeforeAfterTemporaryFill = (dtFuelCallibrated.AsEnumerable()
                                        .Where(y => y[DataTableColumn.TOANALYSE].ToString() == "True")
                                        .Select(x => x.Field<dynamic>(DataTableColumn.FINAL_VALUE))
                                        .Skip((i / skipBy) - numberOfPointsToAnalyse - 1)
                                        .Take((numberOfPointsToAnalyse * 2) + 1))
                                        .ToList();

                                    if (pointsBeforeAfterTemporaryFill.Count < 5)
                                        continue;

                                    pointsBeforeAfterTemporaryFill.RemoveAt(rowsToRemove);

                                    // Converting dynamic to list of type int to ease manipulation in the process
                                    beforeAfterList.Clear();
                                    for (int val = 0; val < pointsBeforeAfterTemporaryFill.Count; val++)
                                        beforeAfterList.Add(Convert.ToInt32(pointsBeforeAfterTemporaryFill[val]));

                                    meanBefore = beforeAfterList.Take(4).Min();
                                    meanAfter = beforeAfterList.Skip(4).Take(4).Max();
                                    meanDiff = Math.Abs(meanBefore - meanAfter);

                                    if (meanDiff >= fillThreshold)
                                    {
                                        dtFuelCallibrated.Rows[i][DataTableColumn.TAG] = Fuel.FILL;
                                    }
                                    else
                                    {
                                        dtFuelCallibrated.Rows[i][DataTableColumn.TAG] = Fuel.FALSE_FILL;
                                        dtFuelCallibrated.Rows[i][DataTableColumn.TOANALYSE] = false;

                                        dtFuelCallibrated = Analyse1stLevel(dtFuelCallibrated, fillThreshold, dropThreshold, i);
                                        dtFuelCallibrated.Rows[i][DataTableColumn.TOANALYSE] = true;
                                        //i = 0;
                                    }
                                    #endregion
                                }
                                #endregion
                                #region Analyse TEMP_DROP
                                else if (dtFuelCallibrated.Rows[i][DataTableColumn.TAG].ToString() == Fuel.TEMP_DROP)
                                {
                                    #region Drop
                                    pointsBeforeAfterTemporaryDrop = (dtFuelCallibrated.AsEnumerable()
                                     .Where(y => y[DataTableColumn.TOANALYSE].ToString() == "True")
                                     .Select(x => x.Field<dynamic>(DataTableColumn.FINAL_VALUE))
                                     .Skip((i / skipBy) - numberOfPointsToAnalyse - 1)
                                     .Take((numberOfPointsToAnalyse * 2) + 1))
                                     .ToList();
                                    pointsBeforeAfterTemporaryDrop.RemoveAt(rowsToRemove);

                                    if (pointsBeforeAfterTemporaryDrop.Count < 5)
                                        continue;

                                    // Converting dynamic to list of type int to ease manipulation further below in the process
                                    beforeAfterList.Clear();
                                    for (int val = 0; val < pointsBeforeAfterTemporaryDrop.Count; val++)
                                        beforeAfterList.Add(Convert.ToInt32(pointsBeforeAfterTemporaryDrop[val]));

                                    meanBefore = beforeAfterList.Take(4).Max();
                                    meanAfter = beforeAfterList.Skip(4).Take(4).Min();
                                    meanDiff = Math.Abs(meanBefore - meanAfter);

                                    if (meanDiff >= Math.Abs(fillThreshold))
                                    {
                                        dtFuelCallibrated.Rows[i][DataTableColumn.TAG] = Fuel.DROP;
                                    }
                                    else
                                    {
                                        dtFuelCallibrated.Rows[i][DataTableColumn.TAG] = Fuel.FALSE_DROP;
                                        dtFuelCallibrated.Rows[i][DataTableColumn.TOANALYSE] = false;

                                        dtFuelCallibrated = Analyse1stLevel(dtFuelCallibrated, fillThreshold, dropThreshold, i - skipBy);

                                        dtFuelCallibrated.Rows[i][DataTableColumn.TOANALYSE] = true;
                                        //i = 0;
                                    }
                                    #endregion
                                }
                                #endregion
                            }
                        }
                        catch { }
                    }
                    #endregion

                    #endregion

                    //Skipping last 2 Analysed data.
                    //int cnt = dtFuelCallibrated.Rows.Count - 1;
                    //for (int i = cnt; i >= cnt - numOfDuplicate; --i)
                    //    if (dtFuelCallibrated.Rows[i][DataTableColumn.TOANALYSE].ToString() == "True")
                    //        dtFuelCallibrated.Rows[i][DataTableColumn.TOANALYSE] = false;

                    finalResult.Merge(dtFuelCallibrated);
                }
                catch (Exception ex) 
                { }
            }

            if (finalResult.Rows.Count == 0)
                return new BaseModel();

            String sql = @"
--FuelLocation

CREATE TABLE #FuelLocation
(
	UID int NOT NULL,
	AssetID int NOT NULL,
    GPSDataStartUID int, 
    fStartLon float, 
    fStartLat float,
	DateTimeGPS_UTC datetime NULL
);
";
            foreach (DataRow dr in finalResult.Rows)
            {
                if (dr["Tag"].ToString().Trim() != "Fill" && dr["Tag"].ToString().Trim() != "Drop")
                    continue;

                if (dr["ToAnalyse"].ToString().Trim() == "False")
                    continue;

                sql += "insert #FuelLocation select top 1 "
                    + dr["UID"].ToString() + ", "
                    + dr["AssetID"].ToString()
                    + ", GPSDataStartUID, fStartLon, fStartLat "
                    + ", dtStart "
                    + "from GFI_GPS_TripHeader where AssetID = " + dr["AssetID"].ToString()
                    + " and dtStart <= '" + dr["DateTimeGPS_UTC"].ToString()
                    + "' order by dtStart desc; \r\n";
            }

            sql += "select * from #FuelLocation";
            DataTable dtFuelLoc = Connection.GetDataTable(sql, sConnStr);
            foreach (DataRow dr in dtFuelLoc.Rows)
            {
                //If multiple detection on same trip, ignore exact location
                DataRow[] drTripStart = dtFuelLoc.Select("GPSDataStartUID = " + dr["GPSDataStartUID"]);
                if (drTripStart.Length > 1)
                    continue;

                DataRow[] drFinal = finalResult.Select("UID = " + dr["UID"]);
                foreach (DataRow r in drFinal)
                {
                    r["UID"] = dr["GPSDataStartUID"];
                    r["Longitude"] = dr["fStartLon"];
                    r["Latitude"] = dr["fStartLat"];
                    r["DateTimeGPS_UTC"] = dr["DateTimeGPS_UTC"];
                }
            }

            BaseModel bmResult = SaveFuel(finalResult, sConnStr);
            return bmResult;
        }

        DataTable Analyse1stLevel(DataTable dtFuelCallibrated, Double fillThreshold, Double dropThreshold, int iStartRow)
        {
            DataTable dt = dtFuelCallibrated.Select(DataTableColumn.TOANALYSE + " = 'True'").CopyToDataTable();
            int iRowCnt = dt.Rows.Count - 1;

            for (int i = 0; i < iRowCnt; i++)
            {
                Double diff = Convert.ToDouble(dt.Rows[i + 1][DataTableColumn.FINAL_VALUE]) - Convert.ToDouble(dt.Rows[i][DataTableColumn.FINAL_VALUE]);
                if (diff >= fillThreshold)
                    dt.Rows[i + 1][DataTableColumn.TAG] = Fuel.TEMP_FILL;
                else if (diff <= dropThreshold)
                    dt.Rows[i + 1][DataTableColumn.TAG] = Fuel.TEMP_DROP;
                else
                    dt.Rows[i + 1][DataTableColumn.TAG] = Fuel.REAL;

                foreach (DataRow r in dtFuelCallibrated.Rows)
                    if (Convert.ToInt32(r["rowNum"]) >= iStartRow)
                        if (r["rowNum"].ToString() == dt.Rows[i + 1]["rowNum"].ToString())
                        {
                            r[DataTableColumn.TAG] = dt.Rows[i + 1][DataTableColumn.TAG];
                            break;
                        }
            }

            return dtFuelCallibrated;
        }

        public static DataTable GetCallibratedData(DataTable dtResult, List<int> lAssetID, String columnValue, String sConnStr, String columnUOM = "UOM")
        {
            /*
             * Callibration Logic as follows
             * Callibration Reading     Converted
             *              1572        10
             *              382         75
             * 1572 - 382 + 1 = 1191 ReadingGap
             * 75 - 10 + 1 = 66 ConvertedGap
             * Suppose reading value is 871
             * calculation
             * ReadingGap ------ ConvertedGap
             * 1191       ------ 66
             * 871        ------ x
             * 871 must be substracted from Reading
             * x = ((382 - 871)/1191) * 66 = 48.26
            */
            if (!dtResult.Columns.Contains("ConvertedValue")) dtResult.Columns.Add("ConvertedValue");
            if (!dtResult.Columns.Contains("ConvertedUOMDesc")) dtResult.Columns.Add("ConvertedUOMDesc");
            if (!dtResult.Columns.Contains(columnUOM))
            {
                dtResult.Columns.Add(columnUOM);
                foreach (DataRow dr in dtResult.Rows)
                    dr[columnUOM] = "ADC";
            }

            DataTable dt = dtResult.Clone();
            foreach (int iAssetID in lAssetID)
            {
                Boolean b = false;
                TimeSpan ts = new TimeSpan();
                if (dtResult.Columns.Contains("DateTimeGPS_UTC"))
                {
                    Asset a = new Asset();
                    b = true;
                    a = new AssetService().GetAssetById(iAssetID, 0, false, sConnStr);
                    ts = a.TimeZoneTS;
                }

                DataRow[] result = dtResult.Select("AssetID = " + iAssetID.ToString());
                CalibrationChart CalChart = new CalibrationChart();
                try
                {
                    List<CalibrationChart> lCalChart = new CalibrationChartService().GetCalibrationChartLByAssetId(iAssetID, sConnStr);

                    if (lCalChart == null)
                    {
                        // no propoer calibration found
                        foreach (DataRow dri in result)
                        {
                            dri["ConvertedValue"] = dri[columnValue];
                            dri["ConvertedUOMDesc"] = dri[columnUOM];
                            if (b)
                                dri["DateTimeGPS_UTC"] = (DateTime)dri["DateTimeGPS_UTC"] + ts;
                        }
                    }
                    else if (lCalChart.Count == 1)
                    {
                        //Single Line Callibration
                        //Adding minimum in callibation
                        CalibrationChart c = lCalChart[0].GetClone();
                        c.Reading = 0;
                        c.ConvertedValue = 0;
                        lCalChart.Insert(0, (c));
                    }

                    //Logic as follows:
                    //suppose Callibration chart is 
                    //100% ---- 50L
                    //10%  ---- 15L
                    //1%   ---- 1L
                    //Suppose Reading value is 12% > gap between 10% - 100%
                    //1st Gap is 90% --- 35L
                    //1 unit > 1/90*35
                    //ReadingIndent > 2% > 2*1 unit > 2*(1/90*35)
                    //Final Result = LowValue + ReadingIndent > 15L + ReadingIndent
                    if (lCalChart.Count > 1)
                    {
                        foreach (DataRow dri in result)
                        {
                            Double LowKey, LowValue, HighKey, HighValue;
                            for (int i = 0; i < lCalChart.Count - 1; i++)
                            {
                                Double dReadingFuel = Convert.ToDouble(dri[columnValue]);
                                if (
                                        (dReadingFuel >= lCalChart[i].Reading && dReadingFuel <= lCalChart[i + 1].Reading)  // normal calibration
                                        ||
                                        (dReadingFuel <= lCalChart[i].Reading && dReadingFuel >= lCalChart[i + 1].Reading)  // reverse calibration
                                    )
                                {
                                    LowKey = lCalChart[i].Reading;
                                    LowValue = lCalChart[i].ConvertedValue;
                                    HighKey = lCalChart[i + 1].Reading;
                                    HighValue = lCalChart[i + 1].ConvertedValue;

                                    // reverse calibration. Still testing
                                    if (LowKey > HighKey)
                                    {
                                        LowKey = lCalChart[i + 1].Reading;
                                        LowValue = lCalChart[i + 1].ConvertedValue;
                                        HighKey = lCalChart[i].Reading;
                                        HighValue = lCalChart[i].ConvertedValue;
                                    }

                                    Double dReadingGap = Math.Abs(LowKey - HighKey);
                                    Double dConvertedGap = Math.Abs(LowValue - HighValue);
                                    Double dReadingValue = Convert.ToDouble(dri[columnValue]);
                                    Double SingleReadingUnitConvertion = (1.0 / dReadingGap) * dConvertedGap;
                                    Double dIndent = Math.Abs(dReadingValue - LowKey) * SingleReadingUnitConvertion;
                                    Double dConvertedValue = LowValue + dIndent;

                                    if ((dReadingFuel <= lCalChart[i].Reading && dReadingFuel >= lCalChart[i + 1].Reading))  // reverse calibration
                                        dConvertedValue = LowValue - dIndent;

                                    dri["ConvertedValue"] = dConvertedValue.ToString("0");
                                    dri["ConvertedUOMDesc"] = lCalChart[i].ConvertedUOMDesc;
                                    if (b)
                                        dri["DateTimeGPS_UTC"] = (DateTime)dri["DateTimeGPS_UTC"] + ts;

                                    i = lCalChart.Count + 5;
                                }
                                else
                                {
                                    //if Reading outside callibration range
                                    List<CalibrationChart> lCalibSorted = lCalChart;
                                    lCalibSorted = lCalibSorted.OrderBy(p => p.Reading).ToList();
                                    int iCnt = lCalibSorted.Count;

                                    if (dReadingFuel < lCalibSorted[0].Reading)
                                        dri["ConvertedValue"] = lCalChart[0].ConvertedValue;
                                    else if (dReadingFuel > lCalibSorted[iCnt - 1].Reading)
                                        dri["ConvertedValue"] = lCalibSorted[iCnt - 1].ConvertedValue;

                                    dri["ConvertedUOMDesc"] = lCalChart[i].ConvertedUOMDesc;
                                    if (b)
                                        dri["DateTimeGPS_UTC"] = (DateTime)dri["DateTimeGPS_UTC"] + ts;
                                }
                            }
                        }
                    }
                }
                catch { }

                if (result.Length > 0)
                    dt = result.CopyToDataTable<DataRow>();
            }

            try
            {
                DataRow[] matches = dt.Select("ConvertedValue is null ");
                foreach (DataRow row in matches)
                    dt.Rows.Remove(row);

                foreach (DataRow dr in dt.Rows)
                    if (dr.RowState == DataRowState.Deleted)
                        dr.AcceptChanges();
                dt.AcceptChanges();
            }
            catch { };

            return dt;
        }

        public BaseModel GetFuelData(int UID, int AssetID, DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            BaseModel baseModel = new BaseModel();
            DataTable dt = new DataTable();

            DataTable dtGMAsset = new NaveoOneLib.WebSpecifics.LibData().dtGMAsset(UID, sConnStr);
            //DataTable dtGMAsset = NaveoOneLib.WebSpecifics.LibData.dtGMData(UID, NaveoModels.Assets, sConnStr);//Try this, but if should change
            DataRow[] drAssets = dtGMAsset.Select("StrucID = " + AssetID);
            if (drAssets.Length == 0)
            {
                baseModel.errorMessage = "User does not have access to Asset";
                return baseModel;
            }

            String sql = @"
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 

;WITH CTE AS 
(SELECT rownum = ROW_NUMBER() OVER (ORDER BY DateTimeGPS_UTC)
        , DATEADD(MINUTE, t.TotalMinutes, f.DateTimeGPS_UTC) localTime, f.* ,a.AssetName
    FROM GFI_GPS_FuelData f
        inner join GFI_FLT_Asset a on f.AssetID = a.AssetID
        inner join GFI_SYS_TimeZones t on a.TimeZoneID = t.StandardName
    where f.AssetID = " + AssetID + @" 
        and f.DateTimeGPS_UTC >= ? 
        and f.DateTimeGPS_UTC <= ?
        and f.ftype in ('Real', 'Fill', 'Drop')
)
SELECT cte.UID gpsUID, cte.AssetID assetID,cte.AssetName ,cte.localTime dateTimeGPS_UTC, cte.ConvertedValue convertedValue, u.Description uom, cte.fType, cte.ConvertedValue - prev.ConvertedValue diff, cte.Longitude lon, cte.Latitude lat
	FROM CTE
	    inner join GFI_SYS_UOM u on u.UID = cte.UOM
        LEFT JOIN CTE prev ON prev.rownum = CTE.rownum - 1
order by dateTimeGPS_UTC
                    ";
            String strParam = dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = strParam;
            drSql["sTableName"] = "fuelData";
            dtSql.Rows.Add(drSql);
            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            baseModel.data = ds.Tables[0];
            return baseModel;
        }
        public BaseModel GetFuelConsumption(int UID, DateTime? dtFrom, DateTime? dtTo, int AssetID, String sConnStr, int? Page, int? LimitPerPage)
        {
            String sTop = String.Empty;
            if (LimitPerPage.HasValue)
            {
                sTop = " Top " + LimitPerPage + " ";
                LimitPerPage += 1;
            }
            Pagination p = Pagination.GetRowNums(Page, LimitPerPage);

            BaseModel baseModel = new BaseModel();
            DataTable dt = new DataTable();

            DataTable dtGMAsset = new NaveoOneLib.WebSpecifics.LibData().dtGMAsset(UID, sConnStr);
            DataRow[] drAssets = dtGMAsset.Select("StrucID = " + AssetID);
            if (drAssets.Length == 0)
            {
                baseModel.errorMessage = "User does not have access to Asset";
                return baseModel;
            }

            String sql = @"
--GetFuelConsumptionReport
drop table if exists _fill;
;WITH CTE AS 
(SELECT rownum = ROW_NUMBER() OVER (ORDER BY DateTimeGPS_UTC desc)
        , DATEADD(MINUTE, t.TotalMinutes, f.DateTimeGPS_UTC) localTime, f.* 
    FROM GFI_GPS_FuelData f
        inner join GFI_FLT_Asset a on f.AssetID = a.AssetID
        inner join GFI_SYS_TimeZones t on a.TimeZoneID = t.StandardName
    where f.AssetID = " + AssetID + @"
        and f.ftype in ('Fill')
)
	SELECT cte.UID gpsUID, cte.AssetID assetID, cte.localTime dateTimeGPS_UTC, cte.ConvertedValue convertedValue, u.Description uom, cte.fType, cte.ConvertedValue - prev.ConvertedValue diff, cte.Longitude lon, cte.Latitude lat, cte.rownum
		into #Fill FROM CTE
			inner join GFI_SYS_UOM u on u.UID = cte.UOM
			LEFT JOIN CTE prev ON prev.rownum = CTE.rownum - 1
		where cte.rownum <= " + p.RowTo + @"
	order by dateTimeGPS_UTC desc;

select 'FuelData' as TableName

select " + sTop + @"c.FuelDataUID fuelDataUID, f.ConvertedValue lastFill, DATEADD(mi, T.TotalMinutes, f.DateTimeGPS_UTC) dtLocal, fill.rownum 
	, f.ConvertedValue - c.valueBeforeFill litersFill
	, c.ValueAfterFill fuelLevel
	, null percentageLevel, 'Green' percentageColor
	, ROUND(c.Consumption, 1) averageFuelConsumed
	, c.PreviousFill - c.valueBeforeFill fuelConsumed
	, ROUND(c.Distance, 1) distanceInKMs
	, c.IdlingTime idlingTime
	, c.HarshAcc harshAccl
	, c.PossibleDrops possibleDrops
	, f.Latitude lat
	, f.Longitude lon
	, '' fillAddress
    , a.assetNumber
    , a.AssetID
	, vech.Description as vechType
	, vMake.Description  as make  
	, vModel.Description as model
from GFI_GPS_FuelConsumption c
	inner join GFI_GPS_FuelData f on c.FuelDataUID = f.UID
	inner join #Fill fill on c.FuelDataUID = fill.gpsUID
	inner join GFI_FLT_Asset a on a.AssetID = f.AssetID
    left join GFI_AMM_VehicleTypes vech on vech.VehicleTypeId = a.VehicleTypeId
	left join GFI_FLT_AssetEXT assetExt  on assetExt.AssetId = a.AssetID
    left join GFI_SYS_LookUpValues vMake on vMake.VID =assetExt.MakeID
    left join GFI_SYS_LookUpValues vModel on vModel.VID =assetExt.ModelID
    left join GFI_SYS_LookUpTypes tMake on vMake.TID =tMake.TID
    left join GFI_SYS_LookUpTypes tModel on vModel.TID =tMake.TID
	INNER JOIN GFI_SYS_TimeZones t on a.TimeZoneID = T.StandardName";


            if (dtFrom.HasValue && dtTo.HasValue)
                sql += @"  where f.DateTimeGPS_UTC >= '" + dtFrom.Value.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "' and f.DateTimeGPS_UTC <= '" + dtTo.Value.ToString("dd-MMM-yyyy HH:mm:ss.fff") + @"'";

            sql += @" order by f.dateTimeGPS_UTC desc;

select 'FuelEconomy' as TableName

select ROUND(sum(Consumption) / count(1), 1) allTimeAverage, 'Yellow' color
from GFI_GPS_FuelConsumption c
	inner join GFI_GPS_FuelData f on c.FuelDataUID = f.UID
where AssetID = " + AssetID + @" and Consumption is not null group by AssetID
      
select 'ActualFuelLevel' as TableName

select top 1 
	DATEADD(mi, T.TotalMinutes, f.DateTimeGPS_UTC) dtLocal
	, f.fType
	, f.ConvertedValue fuelLevel
	, null PercentageLevel
    , 'Green' percentageColor
from GFI_GPS_FuelData f
	inner join GFI_FLT_Asset a on a.AssetID = f.AssetID
	INNER JOIN GFI_SYS_TimeZones t on a.TimeZoneID = T.StandardName
where f.AssetID = " + AssetID + @"
	order by f.DateTimeGPS_UTC desc --secondOrder
                    ";
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);
            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            List<CalibrationChart> lCalChart = new CalibrationChartService().GetCalibrationChartLByAssetId(AssetID, sConnStr);
            foreach (DataRow dr in ds.Tables["FuelData"].Rows)
            {
                int iLiterToPercentage = LiterToPercentage(Convert.ToDouble(dr["FuelLevel"]), lCalChart);
                if (iLiterToPercentage <= 20)
                    dr["percentageColor"] = "Red";

                dr["PercentageLevel"] = iLiterToPercentage;
                dr["FillAddress"] = SimpleAddress.GetFillingStation(Convert.ToDouble(dr["Lon"]), Convert.ToDouble(dr["Lat"])).Replace("'", " ");
            }

            foreach (DataRow dr in ds.Tables["ActualFuelLevel"].Rows)
            {
                int iLiterToPercentage = LiterToPercentage(Convert.ToDouble(dr["FuelLevel"]), lCalChart);
                if (iLiterToPercentage <= 20)
                    dr["percentageColor"] = "Red";
                dr["PercentageLevel"] = iLiterToPercentage;
            }

            Double dAllTime = 0;
            if (ds.Tables["FuelEconomy"].Rows.Count > 0)
                if (Double.TryParse(ds.Tables["FuelEconomy"].Rows[0]["AllTimeAverage"].ToString(), out dAllTime))
                {
                    Double LastAverage = 0;

                    if (ds.Tables["FuelData"].Rows.Count != 0)
                    {
                        if (Double.TryParse(ds.Tables["FuelData"].Rows[0]["AverageFuelConsumed"].ToString(), out LastAverage))
                        {
                            String myColor = "Yellow";
                            if (dAllTime - LastAverage > 0)
                                myColor = "Green";
                            else
                            {
                                Double dPercentage = Math.Abs(dAllTime - LastAverage);
                                dPercentage = dPercentage / dAllTime;
                                dPercentage = dPercentage * 100;
                                if (dPercentage <= 10)
                                    myColor = "Green";
                                else if (dPercentage <= 20)
                                    myColor = "Yellow";
                                else
                                    myColor = "Red";
                            }

                            ds.Tables["FuelEconomy"].Rows[0]["Color"] = myColor;
                        }
                    }
                }

            baseModel.data = ds;
            baseModel.total = ds.Tables["FuelData"].Rows.Count;
            return baseModel;
        }
        public BaseModel GetPossibleFuelDrops(int UID, int fuelDataUID, String sConnStr)
        {
            BaseModel baseModel = new BaseModel();
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"select * from GFI_GPS_FuelConsumption c
	                            inner join GFI_GPS_FuelData f on c.FuelDataUID = f.UID
                            where c.FuelDataUID = " + fuelDataUID;
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtConsumption";
            dtSql.Rows.Add(drSql);
            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            int AssetID = -1;
            if (ds.Tables.Contains("dtConsumption"))
                if (ds.Tables["dtConsumption"].Rows.Count > 0)
                {
                    if (int.TryParse(ds.Tables["dtConsumption"].Rows[0]["AssetID"].ToString(), out AssetID))
                    {
                        DataTable dtGMAsset = new NaveoOneLib.WebSpecifics.LibData().dtGMAsset(UID, sConnStr);
                        DataRow[] drAssets = dtGMAsset.Select("StrucID = " + AssetID);
                        if (drAssets.Length == 0)
                        {
                            baseModel.errorMessage = "User does not have access to Asset";
                            return baseModel;
                        }

                        sql = @"select top " + ds.Tables["dtConsumption"].Rows[0]["PossibleDrops"].ToString() + @"* from GFI_GPS_FuelData
	                        where AssetID = " + AssetID + @"
		                        and DateTimeGPS_UTC < '" + ds.Tables["dtConsumption"].Rows[0]["DateTimeGPS_UTC"].ToString() + @"'
		                        and fType = 'Drop'
	                        order by DateTimeGPS_UTC desc

";

                        sql = @"select top " + ds.Tables["dtConsumption"].Rows[0]["PossibleDrops"].ToString() + @" f.*, DATEADD(mi, T.TotalMinutes, f.DateTimeGPS_UTC) dtLocal, '' dropAddress
                                from GFI_GPS_FuelData f
	                                inner join GFI_FLT_Asset a on a.AssetID = f.AssetID
	                                INNER JOIN GFI_SYS_TimeZones t on a.TimeZoneID = T.StandardName
                                where f.AssetID = " + AssetID + @"
	                                and DateTimeGPS_UTC < '" + ds.Tables["dtConsumption"].Rows[0]["DateTimeGPS_UTC"].ToString() + @"'
	                                and fType = 'Drop'
                                order by DateTimeGPS_UTC desc";

                        c = new Connection(sConnStr);
                        dtSql = c.dtSql();
                        drSql = dtSql.NewRow();
                        drSql["sSQL"] = sql;
                        drSql["sTableName"] = "dtPossibleDrops";
                        dtSql.Rows.Add(drSql);
                        ds = c.GetDataDS(dtSql, sConnStr);

                        foreach (DataRow dr in ds.Tables["dtPossibleDrops"].Rows)
                            dr["dropAddress"] = SimpleAddress.GetAddresses(Convert.ToDouble(dr["Longitude"]), Convert.ToDouble(dr["Latitude"])).Replace("'", " ");

                        baseModel.data = ds;
                    }
                }
            return baseModel;
        }
        int LiterToPercentage(Double dReadingFuel, List<CalibrationChart> lCalChart)
        {
            int iResult = 0;
            if (lCalChart.Count > 1)
            {
                Double LowKey, LowValue, HighKey, HighValue;
                for (int i = 0; i < lCalChart.Count - 1; i++)
                {
                    if (
                            (dReadingFuel >= lCalChart[i].ConvertedValue && dReadingFuel <= lCalChart[i + 1].ConvertedValue)  // normal calibration
                            ||
                            (dReadingFuel <= lCalChart[i].ConvertedValue && dReadingFuel >= lCalChart[i + 1].ConvertedValue)  // reverse calibration
                        )
                    {
                        LowKey = lCalChart[i].Reading;
                        LowValue = lCalChart[i].ConvertedValue;
                        HighKey = lCalChart[i + 1].Reading;
                        HighValue = lCalChart[i + 1].ConvertedValue;

                        // reverse calibration. Still testing
                        if (LowKey > HighKey)
                        {
                            LowKey = lCalChart[i + 1].Reading;
                            LowValue = lCalChart[i + 1].ConvertedValue;
                            HighKey = lCalChart[i].Reading;
                            HighValue = lCalChart[i].ConvertedValue;
                        }

                        Double dReadingGap = Math.Abs(LowKey - HighKey);
                        Double dConvertedGap = Math.Abs(LowValue - HighValue);
                        Double dReadingValue = dReadingFuel;// Convert.ToDouble(dri[columnValue]);
                        Double SingleReadingUnitConvertion = (1.0 / dReadingGap) * dConvertedGap;   //Should not b 1.0, rather LowKey or LowValue
                        Double dIndent = Math.Abs(dReadingValue - LowValue) / SingleReadingUnitConvertion;
                        Double dConvertedValue = LowKey + dIndent;

                        if ((dReadingFuel <= lCalChart[i].Reading && dReadingFuel >= lCalChart[i + 1].Reading))  // reverse calibration
                            dConvertedValue = LowValue - dIndent;

                        iResult = Convert.ToInt32(dConvertedValue);

                        //dri["ConvertedValue"] = dConvertedValue.ToString("0");
                        //dri["ConvertedUOMDesc"] = lCalChart[i].ConvertedUOMDesc;
                        //if (b)
                        //    dri["DateTimeGPS_UTC"] = (DateTime)dri["DateTimeGPS_UTC"] + ts;

                        i = lCalChart.Count + 5;
                    }
                    else
                    {
                        //if Reading outside callibration range
                        List<CalibrationChart> lCalibSorted = lCalChart;
                        lCalibSorted = lCalibSorted.OrderBy(x => x.Reading).ToList();
                        int iCnt = lCalibSorted.Count;

                        //if (dReadingFuel < lCalibSorted[0].Reading)
                        //    dri["ConvertedValue"] = lCalChart[0].ConvertedValue;
                        //else if (dReadingFuel > lCalibSorted[iCnt - 1].Reading)
                        //    dri["ConvertedValue"] = lCalibSorted[iCnt - 1].ConvertedValue;

                        //dri["ConvertedUOMDesc"] = lCalChart[i].ConvertedUOMDesc;
                        //if (b)
                        //    dri["DateTimeGPS_UTC"] = (DateTime)dri["DateTimeGPS_UTC"] + ts;
                    }
                }
            }
            return iResult;
        }

        public BaseModel SaveFuel(DataTable dtFuelProcessed, String sConnStr)
        {
            BaseModel baseModel = new BaseModel();
            Boolean bResult = false;

            String sql = @"
--SaveFuelToProcess

CREATE TABLE #GFI_GPS_FuelData
                            (
	                            UID int NOT NULL,
	                            AssetID int NOT NULL,
	                            DateTimeGPS_UTC datetime NOT NULL,
                                ConvertedValue float null,
                                UOM int null,
                                UOMDesc nvarchar(20),
                                fType nvarchar(15),
                                Longitude float,
                                Latitude float,
                                RuleID int
                            )

                        ";

            DataRow[] drTrue = dtFuelProcessed.Select("ToAnalyse = 'True'");
            if (drTrue.Length == 0)
            {
                baseModel.errorMessage = "no fuel data to save";
                bResult = false;
                return baseModel;
            }

            DataTable dtFuel = drTrue.CopyToDataTable();
            foreach (DataRow dr in dtFuel.Rows)
            {
                if (dr["Tag"].ToString().Trim().Length == 0)
                    continue;

                int i = 0;
                if (int.TryParse(dr["FinalValue"].ToString(), out i))
                {
                    sql += "insert #GFI_GPS_FuelData (UID, AssetID, DateTimeGPS_UTC, ConvertedValue, UOMDesc, fType, Longitude, Latitude) values (";
                    sql += dr["UID"] + ", ";
                    sql += dr["AssetID"] + ", ";
                    sql += "'" + dr["DateTimeGPS_UTC"] + "', ";
                    sql += dr["FinalValue"] + ", ";
                    sql += "'" + dr["ConvertedUOMDesc"] + "', ";
                    sql += "'" + dr["Tag"] + "', ";
                    sql += dr["Longitude"] + ", ";
                    sql += dr["Latitude"] + ") ";
                    sql += "\r\n";
                    sql += "\r\n";
                }
            }
            sql += @"
UPDATE #GFI_GPS_FuelData 
    SET UOM = T2.UID
FROM #GFI_GPS_FuelData T1
INNER JOIN  GFI_SYS_UOM T2 ON T2.Description COLLATE DATABASE_DEFAULT = T1.UOMDesc COLLATE DATABASE_DEFAULT
                    
UPDATE GFI_GPS_FuelData 
    SET fType = T2.fType, ConvertedValue = T2.ConvertedValue
FROM GFI_GPS_FuelData T1
INNER JOIN  #GFI_GPS_FuelData T2 ON T2.UID = T1.UID
                    
--exceptions
update #GFI_GPS_FuelData 
	set RuleID = t2.ParentRuleID
from #GFI_GPS_FuelData t1
	inner join GFI_GPS_Rules t2 on t2.StrucValue COLLATE DATABASE_DEFAULT = t1.fType COLLATE DATABASE_DEFAULT
where t2.Struc = 'Fuel'
 
--FuelConsumption
CREATE TABLE #FuelConsumption
(
	UID int NOT NULL,
	AssetID int NOT NULL,
	DateTimeGPS_UTC datetime NOT NULL,
	valueBeforeforFill float null,
    valueAfterFill float null,
	Distance float null,
	Origine int,
    dtPreviousFill datetime,
    IdlingTime float, HarshAcc int, PossibleDrops int
)

--insert from #GFI_GPS_FuelData. All Fill
;WITH CTE AS 
(
	SELECT rownum = ROW_NUMBER() OVER (ORDER BY AssetID, DateTimeGPS_UTC)
        , f.* 
    FROM #GFI_GPS_FuelData f
)
insert #FuelConsumption (UID, AssetID, DateTimeGPS_UTC, valueBeforeforFill, valueAfterFill, Origine)
	SELECT cte.UID, cte.AssetID, cte.DateTimeGPS_UTC, prev.ConvertedValue Prev, cte.ConvertedValue actual, 2 --cte.*
		FROM CTE
			LEFT JOIN CTE prev ON prev.rownum = CTE.rownum - 1
		where cte.AssetID = prev.AssetID and cte.ftype = 'Fill'

--insert from GFI_GPS_FuelData. Last Fill
insert #FuelConsumption (UID, AssetID, DateTimeGPS_UTC, valueAfterFill, Origine)
	SELECT distinct t.UID, t.AssetID, t.DateTimeGPS_UTC, t.ConvertedValue, 1 FROM
	    (
		    SELECT ROW_NUMBER() OVER(PARTITION BY AssetID ORDER BY DateTimeGPS_UTC DESC) AS Seq, * FROM GFI_GPS_FuelData where ftype = 'Fill'
	    )t
		    inner join #FuelConsumption f on f.AssetID = t.AssetID
    	WHERE t.Seq = 1

--update dtPreviousFill in #GFI_GPS_FuelData.
;WITH CTE AS 
(
	SELECT rownum = ROW_NUMBER() OVER (ORDER BY AssetID, Origine, DateTimeGPS_UTC)
        , c.* 
    FROM #FuelConsumption c
)
update #FuelConsumption 
    set dtPreviousFill = prev.DateTimeGPS_UTC
		FROM #FuelConsumption f 
			inner join CTE on cte.UID = f.UID
			LEFT JOIN CTE prev ON prev.rownum = CTE.rownum - 1
		where cte.AssetID = prev.AssetID

update #FuelConsumption 
    set Distance = (select sum(TripDistance) from GFI_GPS_TripHeader where AssetID = f.AssetID and dtStart >= f.dtPreviousFill and dtEnd <= f.DateTimeGPS_UTC)
	    , IdlingTime = (select sum(IdlingTime) from GFI_GPS_TripHeader where AssetID = f.AssetID and dtStart >= f.dtPreviousFill and dtEnd <= f.DateTimeGPS_UTC)
	    , HarshAcc = (select sum(Accel) from GFI_GPS_TripHeader where AssetID = f.AssetID and dtStart >= f.dtPreviousFill and dtEnd <= f.DateTimeGPS_UTC)
from #FuelConsumption f
	where Origine = 2
		and dtPreviousFill < DateTimeGPS_UTC

--insert in db
insert into GFI_GPS_FuelData(UID, AssetID, DateTimeGPS_UTC, ConvertedValue, UOM, fType, Longitude, Latitude)
	SELECT t2.UID, t2.AssetID, t2.DateTimeGPS_UTC, t2.ConvertedValue, t2.UOM, t2.fType, t2.Longitude, t2.Latitude
		FROM #GFI_GPS_FuelData t2
	LEFT JOIN GFI_GPS_FuelData t1 ON t1.UID = t2.UID
		WHERE t1.UID IS NULL

update #FuelConsumption 
    set PossibleDrops = (select count(1) from GFI_GPS_FuelData where AssetID = f.AssetID and DateTimeGPS_UTC >= f.dtPreviousFill and DateTimeGPS_UTC <= f.DateTimeGPS_UTC and fType = 'Drop')
from #FuelConsumption f
	where Origine = 2
		and dtPreviousFill < DateTimeGPS_UTC


insert [GFI_GPS_FuelConsumption] (FuelDataUID, valueAfterFill, valueBeforeFill, Distance, IdlingTime, HarshAcc, PossibleDrops)
	select UID, valueAfterFill, valueBeforeforFill, Distance, IdlingTime, HarshAcc, PossibleDrops
		from #FuelConsumption f
				where Origine = 2

;WITH CTE AS 
(
	SELECT rownum = ROW_NUMBER() OVER (ORDER BY AssetID, DateTimeGPS_UTC), c.*
		from GFI_GPS_FuelConsumption c
			inner join GFI_GPS_FuelData f on f.UID = c.FuelDataUID
)
update GFI_GPS_FuelConsumption 
    set PreviousFill = prev.valueAfterFill -- prev.valueBeforeFill 
FROM GFI_GPS_FuelConsumption f
		inner join #FuelConsumption c on c.UID = f.FuelDataUID
		inner join CTE on cte.FuelDataUID = f.FuelDataUID
		LEFT JOIN CTE prev ON prev.rownum = CTE.rownum - 1

update GFI_GPS_FuelConsumption 
    set Consumption = 100 * (f.PreviousFill - f.valueBeforeFill) / f.Distance
FROM GFI_GPS_FuelConsumption f
		inner join #FuelConsumption c on c.UID = f.FuelDataUID
	where f.Distance > 0

select RuleID, UID, AssetID, CHECKSUM(NEWID()) GroupID, DateTimeGPS_UTC into #FuelExceptions
	from #GFI_GPS_FuelData f
		where f.RuleID is not null
			and NOT EXISTS
					(
						select * from GFI_GPS_Exceptions e where f.RuleID = e.RuleID and f.UID = e.GPSDataID
					)

insert GFI_GPS_Exceptions(RuleID, GPSDataID, AssetID, GroupID, DateTimeGPS_UTC)
    select * from #FuelExceptions

insert GFI_GPS_ExceptionsHeader(RuleID, AssetID, GroupID, dateFrom, dateTo)
    select RuleID, AssetID, GroupID, DateTimeGPS_UTC, DateTimeGPS_UTC from #FuelExceptions
                    ";

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                {
                    Common.Errors.WriteToLog(sql);
                    baseModel.dbResult = dsResult;
                    baseModel.dbResult.Tables["CTRLTBL"].TableName = "ResultCtrlTbl";
                    baseModel.dbResult.Merge(dsProcess);
                    baseModel.errorMessage = "error saving fuel data";
                    bResult = false;
                }
                else
                    bResult = true;
            }
            else
                bResult = false;

            baseModel.data = bResult;
            return baseModel;
        }

        public String sGetLatestFill(List<int> lAssetID, int uLogin, Boolean bGetZones, String sConnStr)
        {
            String sql = @"
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 

CREATE TABLE #Assets
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL,
	[iAssetID] [int] PRIMARY KEY CLUSTERED,
    Asset nvarchar(120),
    TimeZone int Default 0
)
";
            String sAsset = String.Empty;
            foreach (int i in lAssetID)
                sAsset += "insert #Assets (iAssetID) values (" + i.ToString() + ")\r\n";

            sql += sAsset + @"

UPDATE #Assets
    SET Asset = T2.AssetNumber, TimeZone = T3.TotalMinutes
FROM #Assets T1
    INNER JOIN GFI_FLT_Asset T2 ON T2.AssetID = T1.iAssetID
    INNER JOIN GFI_SYS_TimeZones T3 on T2.TimeZoneID = T3.StandardName

;with cte as (
    select a.iAssetID, a.Asset, f.UID, f.DateTimeGPS_UTC, f.ConvertedValue, f.UOM, f.Longitude, f.Latitude
        , DATEADD(mi, a.TimeZone, f.DateTimeGPS_UTC) dtLocalTime
        , row_number() over(partition by f.AssetID order by f.DateTimeGPS_UTC desc) as RowNum
   		, CAST('' AS nvarchar(250)) zone, null zoneColor
	from GFI_GPS_FuelData f
		inner join #Assets a on a.iAssetID = f.AssetID
	where fType = 'Fill'
	    )
select cte.* 
into #FuelData 
from cte
    where RowNum <= 3
";

            #region Zones Related
            if (bGetZones)
            {
                List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin, sConnStr);
                sql += ZoneService.sGetZoneIDs(lm, sConnStr);
                sql += @"
declare @Zones table (ZoneID int, minLat float, minLon float, maxLat float, maxLon float, uid int)

;with cte as
(
    select d.ZoneID, min(d.Latitude) minLat, min(d.Longitude) minLon, max(d.Latitude) maxLat, max(d.Longitude) maxLon  
	    from GFI_FLT_ZoneDetail d
            inner join @tTmpDistinctZone t on d.ZoneID = t.i
	    group by  d.ZoneID
)
insert @Zones (ZoneID, minLat, minLon, maxLat, maxLon, uid)
	select distinct cte.*, f.UID from cte
        inner join #FuelData f on 
			(
				f.Longitude <= maxLon
				and f.Longitude >= minLon
				and f.Latitude >= minLat
				and f.Latitude <= maxLat 
			)

update #FuelData
    set zone = h.Description, zoneColor = h.Color
from #FuelData f
    inner join @Zones z on z.UID = f.UID
    inner join GFI_FLT_ZoneHeader h on h.ZoneID = z.ZoneID
";
            }
            #endregion

            sql += @"
select 'FuelConsumption' as TableName
select *, valueBeforeFill fuelLevel, '%' fuelLevel, Consumption AverageFuelConsumed, PreviousFill - valueBeforeFill FuelConsumed, 'NaveoFilling shp' Naveo 
from #FuelData f
	inner join GFI_GPS_FuelConsumption c on f.UID = c.FuelDataUID

select 'LastReal' as TableName
;with cte as (
    select a.iAssetID, a.Asset, f.UID, f.DateTimeGPS_UTC, f.ConvertedValue, f.UOM, f.Longitude, f.Latitude
        , DATEADD(mi, a.TimeZone, f.DateTimeGPS_UTC) dtLocalTime
        , row_number() over(partition by f.AssetID order by f.DateTimeGPS_UTC desc) as RowNum
   		, CAST('' AS nvarchar(250)) zone, null zoneColor
	from GFI_GPS_FuelData f
		inner join #Assets a on a.iAssetID = f.AssetID
	where fType = 'Real'
	    )
select cte.* 
from cte
    where RowNum = 1
";
            return sql;
        }
        public BaseModel GetLatestFill(List<int> lAssetID, int uLogin, Boolean bGetZones, String sConnStr)
        {
            BaseModel baseModel = new BaseModel();

            String sql = sGetLatestFill(lAssetID, uLogin, bGetZones, sConnStr);
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);
            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            baseModel.data = ds.Tables[0];
            return baseModel;
        }

        public BaseModel GetFuelConsumptionReport(int UID, DateTime? dtFrom, DateTime? dtTo, List<int> lAssetID, String sConnStr, int? Page, int? LimitPerPage, List<String> sortColumns, Boolean sortOrderAsc)
        {
            BaseModel bm = new BaseModel();
            DataSet ds = new DataSet();

            DataTable dtFinal = new DataTable();
            dtFinal.Columns.Add("AssetID");
            dtFinal.Columns.Add("Asset");
            dtFinal.Columns.Add("Date Last Fill", typeof(DateTime));
            //dtFinal.Columns.Add("Date Previous Fill");
            dtFinal.Columns.Add("No of Litres(LastFill)", typeof(Int32));
            //dtFinal.Columns.Add("No of Litres(PreviousFill)");
            dtFinal.Columns.Add("KM Travelled", typeof(Double));
            dtFinal.Columns.Add("Avg Consumption Last Fill (L/100 KM)", typeof(Double));
            dtFinal.Columns.Add("Avg Consumption(All Time)", typeof(Double));
            dtFinal.Columns.Add("Vehicle Type");
            dtFinal.Columns.Add("Make");
            dtFinal.Columns.Add("Model");
            dtFinal.Columns.Add("No of Possible Drops", typeof(Int32));
            dtFinal.Columns.Add("No of Hash Acceleration", typeof(Int32));
            foreach (int i in lAssetID)
            {
                BaseModel fuemBM = GetFuelConsumption(UID, dtFrom, dtTo, i, sConnStr, Page, LimitPerPage);
                if (fuemBM.data.Tables["FuelData"].Rows.Count != 0)
                {
                    DataTable dtFuelEco = fuemBM.data.Tables["FuelEconomy"];
                    foreach (DataRow dr in fuemBM.data.Tables["FuelData"].Rows)
                    {
                        DataRow row = dtFinal.NewRow();
                        row["AssetID"] = Convert.ToInt32(dr["AssetID"]);
                        row["Asset"] = dr["assetNumber"].ToString();
                        row["Date Last Fill"] = Convert.ToDateTime(dr["dtLocal"]);
                        row["No of Litres(LastFill)"] = Convert.ToInt32(dr["lastFill"]);
                        row["KM Travelled"] = String.IsNullOrEmpty(dr["distanceInKms"].ToString()) ? (Double)0.0 : Convert.ToDouble(dr["distanceInKms"]);
                        row["Avg Consumption Last Fill (L/100 KM)"] = String.IsNullOrEmpty(dr["averageFuelConsumed"].ToString()) ? (Double)0.0 : Convert.ToDouble(dr["averageFuelConsumed"]);

                        if(dtFuelEco.Rows.Count > 0)
                                row["Avg Consumption(All Time)"] = Convert.ToDouble(dtFuelEco.Rows[0][0].ToString());

                        row["Vehicle Type"] = dr["vechType"].ToString();
                        row["Make"] = dr["make"].ToString();
                        row["Model"] = dr["model"].ToString();
                        row["No of Possible Drops"] = String.IsNullOrEmpty(dr["possibleDrops"].ToString()) ? (int)0 : Convert.ToInt32(dr["possibleDrops"]);
                        row["No of Hash Acceleration"] = String.IsNullOrEmpty(dr["harshAccl"].ToString()) ? (int)0 : Convert.ToInt32(dr["harshAccl"]);

                        dtFinal.Rows.Add(row);
                    }
                }
            }
            #region sort 
            String sSort = String.Empty;
            if (sortColumns != null && sortColumns.Count > 0)
            {
                List<String> lCols = new List<string>() { "AssetID", "Asset", "Date Last Fill", "No of Litres(LastFill)", "KM Travelled", "Avg Consumption Last Fill (L/100 KM)", "Vehicle Type", "Make", "Model", "No of Possible Drops", "No of Hash Acceleration" };

                String ss = String.Join(",", sortColumns
                    .Where(s => !String.IsNullOrWhiteSpace(s))
                    .Where(s => lCols.Contains(s))
                    .Select(s => s));

                if (!String.IsNullOrEmpty(ss))
                    sSort = ss;


                if (sortOrderAsc)
                    sSort += " ASC";
                else
                    sSort += " DESC";
            }



            String dtSort = sSort;
            dtFinal.DefaultView.Sort = sSort;
            dtFinal = dtFinal.DefaultView.ToTable();
            #endregion

            bm.data = dtFinal;
            bm.total = dtFinal.Rows.Count;
            return bm;
        }
    }
}