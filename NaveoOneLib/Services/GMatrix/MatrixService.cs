using System;
using System.Collections.Generic;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Services.Users;

namespace NaveoOneLib.Services.GMatrix
{
    public class MatrixService
    {
        Errors er = new Errors();
        Boolean Vallic = Globals.ValidateLicence();

        public String sGetMatrixIdByField(String SourceTable, String SourceFieldName, String SourceFieldValue, String SourcePrimaryField, String SourceMatrixTable)
        {
            String sql = "SELECT m.MID, m.iID, m.GMID, gm.GMDescription ";
            sql += "from " + SourceTable + " p, " + SourceMatrixTable + " m, GFI_SYS_GroupMatrix gm ";
            sql += "WHERE m.GMID = gm.GMID and p." + SourcePrimaryField + " = m.iID and p." + SourceFieldName + " = '" + SourceFieldValue + "'";
            sql += " order by gm.GMID";
            return sql;
        }

        public String sGetMatrixByGMID(List<int> iParentIDs, String SourceTable)
        {
            String s = String.Empty;
            foreach (int i in iParentIDs)
                s += i.ToString() + ", ";
            s = s.Substring(0, s.Length - 2);
            s = "(" + s + ") ";

            String sql = "SELECT m.MID, m.iID, m.GMID, gm.GMDescription ";
            sql += "from " + SourceTable + " m, GFI_SYS_GroupMatrix gm ";
            sql += "WHERE m.GMID = gm.GMID and m.GMID in " + s;
            sql += " order by gm.GMID";
            return sql;
        }
        public String sGetMatrixId(int iID, String SourceTable)
        {
            String sql = "SELECT m.MID, m.iID, m.GMID, gm.GMDescription ";
            sql += "from " + SourceTable + " m, GFI_SYS_GroupMatrix gm ";
            sql += "WHERE m.GMID = gm.GMID and iID = " + iID.ToString();
            sql += " order by gm.GMID";
            return sql;
        }
        public DataTable GetMatrix(String sConnStr)
        {
            String sqlString = @"SELECT MID, 
                                        iID, 
                                        GMID, 
                                        from GFI_SYS_Matrix order by MID";
            if (Vallic)
                return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
            else
                return null;
        }
        public Matrix GetMatrixById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT * from GFI_SYS_GroupMatrix WHERE GMID = ?";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID,sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return AssignMatrix(dt.Rows[0]);
        }
        
        public List<Matrix> lGetMatrixId(int iID, String SourceTable, String sConnStr)
        {
            String sql = sGetMatrixId(iID, SourceTable);
            List<Matrix> l = new List<Matrix>();
            DataTable dt = new Connection(sConnStr).GetDataDT(sql,sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                foreach (DataRow dr in dt.Rows)
                    l.Add(AssignMatrix(dr));

                if (Vallic)
                    return l;
                else
                    return null;
            }
        }
        public Matrix AssignMatrix(DataRow dr)
        {
            Matrix retMatrix = new Matrix();
            retMatrix.GMDesc = dr["GMDescription"].ToString();
            retMatrix.MID = Convert.ToInt32(dr["MID"]);
            retMatrix.GMID = Convert.ToInt32(dr["GMID"]);
            retMatrix.iID = Convert.ToInt32(dr["iID"]);
            return retMatrix;
        }

        public List<Matrix> GetMatrixByUserID(int UID, String sConnStr)
        {
            String sql = new UserService().sGetMatrixUserByUserID(UID);
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);
            DataSet ds = c.GetDataDS(dtSql,sConnStr);
            List<Matrix> lm = new List<Matrix>();
            foreach (DataRow dr in ds.Tables["dtMatrix"].Rows)
                lm.Add(new MatrixService().AssignMatrix(dr));

            return lm;
        }
        public List<Matrix> GetMatrixByUserToken(Guid sUserToken, String sConnStr)
        {
            String sql = new UserService().sGetMatrixUserByUserToken();
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = sUserToken;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);
            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            List<Matrix> lm = new List<Matrix>();
            foreach (DataRow dr in ds.Tables["dtMatrix"].Rows)
                lm.Add(new MatrixService().AssignMatrix(dr));

            return lm;
        }

        public Boolean SaveMatrix(Matrix uMatrix, String SourceTable, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = SourceTable;
            dt.Columns.Add("GMID", uMatrix.GMID.GetType());
            dt.Columns.Add("iID", uMatrix.iID.GetType());
            DataRow dr = dt.NewRow();

            dr["GMID"] = uMatrix.GMID;
            dr["iID"] = uMatrix.iID;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenInsert(dt, transaction,sConnStr))
                return true;
            else
                return false;
        }
        public String sGetUpdateStatement(int GMID, int iID)
        {
            return "GMID = " + GMID.ToString() + " and iID = " + iID.ToString();
        }
        public bool UpdateMatrix(Matrix uMatrix, String SourceTable, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = SourceTable;
            dt.Columns.Add("MID", uMatrix.MID.GetType());
            dt.Columns.Add("GMID", uMatrix.GMID.GetType());
            dt.Columns.Add("iID", uMatrix.iID.GetType());
            DataRow dr = dt.NewRow();
            dr["MID"] = uMatrix.MID;
            dr["GMID"] = uMatrix.GMID;
            dr["iID"] = uMatrix.iID;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "MID = '" + uMatrix.MID + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool DeleteMatrix(Matrix uMatrix, String SourceTable, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete(SourceTable, "MID = '" + uMatrix.MID + "'",sConnStr))
                return true;
            else
                return false;
        }

        public Matrix GetMatrixId(int iID, String SourceTable, String sConnStr)
        {
            String sql = "SELECT MID, iID, GMID from " + SourceTable + " WHERE iID = " + iID.ToString();
            DataTable dt = new Connection(sConnStr).GetDataDT(sql,sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                Matrix retMatrix = new Matrix();
                retMatrix.MID = Convert.ToInt16(dt.Rows[0]["MID"]);
                retMatrix.GMID = Convert.ToInt16(dt.Rows[0]["GMID"]);
                retMatrix.iID = Convert.ToInt32(dt.Rows[0]["iID"]);
                if (Vallic)
                    return retMatrix;
                else
                    return null;
            }
        }
    }
}


