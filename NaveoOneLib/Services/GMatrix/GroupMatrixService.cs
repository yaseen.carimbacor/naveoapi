using System;
using System.Collections.Generic;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using System.Windows.Forms;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.Zones;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Services.Zones;
using NaveoOneLib.Constant;

namespace NaveoOneLib.Services.GMatrix
{
    public class GroupMatrixService
    {
        Errors er = new Errors();
        public DataTable GetGroupMatrix(String sConnStr)
        {
            String sqlString = @"SELECT * from GFI_SYS_GroupMatrix order by GMID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public DataTable GetGMValues(String sConnStr)
        {
            String sql = "select GMID, GMDescription, coalesce(ParentGMID,'-1') as ParentGMID from GFI_SYS_GroupMatrix order by GMID";
            return new Connection(sConnStr).GetDataDT(sql, sConnStr);
        }
        public String sGetAllGMValues(List<int> iParentIDs, NaveoModels strStruc)
        {
            String s = String.Empty;
            foreach (int i in iParentIDs)
                s += i.ToString() + ", ";
            if (String.IsNullOrEmpty(s))
                return "select -90 GMID, '' GMDescription, -90 ParentGMID, -2 StrucID, '' myStatus, -9 mID, -20 matrixiID";
            s = s.Substring(0, s.Length - 2);
            s = "(" + s + ") ";

            String sDefaultGrp = "-1";

            String sql = @";WITH --postGreSQL RECURSIVE
                                parent AS 
                                (
                                    SELECT distinct ParentGMID
                                    FROM GFI_SYS_GroupMatrix
                                    WHERE ParentGMID in " + s + @"
                                ), 
                                tree (GMID, GMDescription, ParentGMID) AS 
                                (
                                    SELECT 
                                        x.GMID
                                        , x.GMDescription
                                        , x.ParentGMID
                                    FROM GFI_SYS_GroupMatrix x
                                    INNER JOIN parent ON x.ParentGMID = parent.ParentGMID
                                    UNION ALL
                                    SELECT 
                                        y.GMID
                                        , y.GMDescription
                                        , y.ParentGMID
                                    FROM GFI_SYS_GroupMatrix y
                                    INNER JOIN tree t ON y.ParentGMID = t.GMID
                                )
                            , DefaultTree (PGMID, PGMDescription, PParentGMID) AS ( 
                                    SELECT GMID PGMID, 
                                            GMDescription PGMDescription, 
                                            ParentGMID PParentGMID
                                    FROM GFI_SYS_GroupMatrix
                                    WHERE GMID in " + s + @"
                                    UNION ALL 
                                    SELECT p.GMID PGMID,
                                            p.GMDescription PGMDescription,
                                            p.ParentGMID PParentGMID 
                                    FROM GFI_SYS_GroupMatrix p
                                        JOIN DefaultTree t ON t.PParentGMID = p.GMID
                                )
                            SELECT Distinct GMID, GMDescription, ParentGMID, -2 StrucID, null myStatus, -9 mID, -20 matrixiID FROM tree
                            union all
                                SELECT z.GMID, z.GMDescription, -1, -3 Struc, null myStatus, -8 MID, -20 MatrixiID
                                FROM GFI_SYS_GroupMatrix z where z.GMID in " + s + @"
                            --union all
	                        --    SELECT m.GMID, m.GMDescription, " + sDefaultGrp + @", -4, '', -5, -20 MatrixiID
		                    --         FROM DefaultTree t, GFI_SYS_GroupMatrix m 
		                    --         where m.isCompany = 0 and m.ParentGMID = t.PGMID ";
            sql += "\r\n";

            switch (strStruc)
            {
                case NaveoModels.Assets:
                    sql += @" union all
								select -4, a.AssetNumber, tree.GMID as ParentGMID, a.AssetID, a.Status_b, m.MID, m.iID MatrixiID 
									from GFI_FLT_Asset a, GFI_SYS_GroupMatrixAsset m, tree
								where tree.GMID = m.GMID and a.AssetID = m.iID
                            union all
								select -5, a.AssetNumber, m.GMID as ParentGMID, a.AssetID, a.Status_b, m.MID, m.iID MatrixiID 
									from GFI_FLT_Asset a, GFI_SYS_GroupMatrixAsset m
								where m.GMID in " + s + @" and a.AssetID = m.iID";
                    break;

                case NaveoModels.Users:
                    sql += @" union all
								select -4, a.Names, tree.GMID as ParentGMID, a.UID, a.Status_b, m.MID, m.iID MatrixiID 
									from GFI_SYS_User a, GFI_SYS_GroupMatrixUser m, tree
								where tree.GMID = m.GMID and a.UID = m.iID
                            union all
								select -5, a.Names, m.GMID as ParentGMID, a.UID, a.Status_b, m.MID, m.iID MatrixiID 
									from GFI_SYS_User a, GFI_SYS_GroupMatrixUser m
								where m.GMID in " + s + @" and a.UID = m.iID";
                    break;

                case NaveoModels.Zones:
                    sql += @" union all
								select -5, a.Description, tree.GMID as ParentGMID, a.ZoneID, null, m.MID, m.iID MatrixiID  
									from GFI_FLT_ZoneHeader a, GFI_SYS_GroupMatrixZone m, tree
								where tree.GMID = m.GMID and a.ZoneID = m.iID
                            union 
								select -5, a.Description, m.GMID as ParentGMID, a.ZoneID, null, m.MID, m.iID MatrixiID  
									from GFI_FLT_ZoneHeader a, GFI_SYS_GroupMatrixZone m
								where m.GMID in " + s + @" and a.ZoneID = m.iID
                            union 								 
								select -5, a.Description, m.GMID as ParentGMID, a.ZoneID, null, m.MID, m.iID MatrixiID 
									from GFI_FLT_ZoneHeader a, GFI_SYS_GroupMatrixZone m
								where m.GMID in 
									(
										SELECT m.GMID
												 FROM DefaultTree t, GFI_SYS_GroupMatrix m 
											 where m.isCompany = 0 and m.ParentGMID = t.PGMID 
									 )  
									 and a.ZoneID = m.iID ";
                    break;

                case NaveoModels.Drivers:
                    sql += @" union all
								select -4, a.sDriverName, tree.GMID as ParentGMID, a.DriverID, null, m.MID, m.iID MatrixiID  
									from GFI_FLT_Driver a, GFI_SYS_GroupMatrixDriver m, tree
								where tree.GMID = m.GMID and a.DriverID = m.iID
                            union all
								select -5, a.sDriverName, m.GMID as ParentGMID, a.DriverID, null, m.MID, m.iID MatrixiID  
									from GFI_FLT_Driver a, GFI_SYS_GroupMatrixDriver m
								where m.GMID in " + s + @" and a.DriverID = m.iID
                            union 
								select -5, a.sDriverName, m.GMID as ParentGMID, a.DriverID, null, m.MID, m.iID MatrixiID  
									from GFI_FLT_Driver a, GFI_SYS_GroupMatrixDriver m
								where iButton = '00000000' and a.DriverID = m.iID ";
                    break;

                case NaveoModels.Rules:
                    sql += @" union all
								select -4, a.RuleName, tree.GMID as ParentGMID, a.RuleID, null, m.MID, m.iID MatrixiID  
									from GFI_GPS_Rules a, GFI_SYS_GroupMatrixRule m, tree
								where tree.GMID = m.GMID and a.RuleID = m.iID
                            union all
								select -5, a.RuleName, m.GMID as ParentGMID, a.RuleID, null, m.MID, m.iID MatrixiID  
									from GFI_GPS_Rules a, GFI_SYS_GroupMatrixRule m
								where m.GMID in " + s + @" and a.RuleID = m.iID";
                    break;

                case NaveoModels.ZoneTypes:
                    sql += @" union all
								select -4, a.sDescription, tree.GMID as ParentGMID, a.ZoneTypeID, null, m.MID, m.iID MatrixiID  
									from GFI_FLT_ZoneType a, GFI_SYS_GroupMatrixZoneType m, tree
								where tree.GMID = m.GMID and a.ZoneTypeID = m.iID
                            union all
								select -5, a.sDescription, m.GMID as ParentGMID, a.ZoneTypeID, null, m.MID, m.iID MatrixiID  
									from GFI_FLT_ZoneType a, GFI_SYS_GroupMatrixZoneType m
								where m.GMID in " + s + @" and a.ZoneTypeID = m.iID";
                    sql += @" union all
                                select -6, sDescription, -1, ZoneTypeID, null, -7, -20 MatrixiID from GFI_FLT_ZoneType where isSystem = '1'";
                    break;

                case NaveoModels.PlanningRequest:
                    sql += @" union all
								select -4, 'Nono', tree.GMID as ParentGMID, a.PID, null, m.MID, m.iID MatrixiID  
									from GFI_PLN_Planning a, GFI_SYS_GroupMatrixPlanning m, tree
								where tree.GMID = m.GMID and a.PID = m.iID
                            union all
								select -5, 'Nono', m.GMID as ParentGMID, a.PID, null, m.MID, m.iID MatrixiID  
									from GFI_PLN_Planning a, GFI_SYS_GroupMatrixPlanning m
								where m.GMID in " + s + @" and a.PID = m.iID";
                    break;

                case NaveoModels.ScheduledPlanning:
                    sql += @" union all
								select -4, 'Nono', tree.GMID as ParentGMID, a.HID, null, m.MID, m.iID MatrixiID  
									from GFI_PLN_ScheduledPlan a, GFI_SYS_GroupMatrixScheduled m, tree
								where tree.GMID = m.GMID and a.HID = m.iID
                            union all
								select -5, 'Nono', m.GMID as ParentGMID, a.HID, null, m.MID, m.iID MatrixiID  
									from GFI_PLN_ScheduledPlan a, GFI_SYS_GroupMatrixScheduled m
								where m.GMID in " + s + @" and a.HID = m.iID";
                    break;

                case NaveoModels.ResourceDriver:
                    sql += @" union all
								select -4, a.sDriverName, tree.GMID as ParentGMID, a.DriverID, null, m.MID, m.iID MatrixiID  
									from GFI_FLT_Driver a, GFI_SYS_GroupMatrixDriver m, tree
								where tree.GMID = m.GMID and a.DriverID = m.iID and a.EmployeeType = 'Driver'
                            union all
								select -5, a.sDriverName, m.GMID as ParentGMID, a.DriverID, null, m.MID, m.iID MatrixiID  
									from GFI_FLT_Driver a, GFI_SYS_GroupMatrixDriver m
								where m.GMID in " + s + @" and a.DriverID = m.iID and a.EmployeeType = 'Driver'
                            union 
								select -5, a.sDriverName, m.GMID as ParentGMID, a.DriverID, null, m.MID, m.iID MatrixiID  
									from GFI_FLT_Driver a, GFI_SYS_GroupMatrixDriver m
								where iButton = '00000000' and a.DriverID = m.iID  and a.EmployeeType = 'Driver'";
                    break;

                case NaveoModels.Roles:
                    sql += @" union all
								select -4, r.Description, tree.GMID as ParentGMID, r.iID, null, m.MID, m.iID MatrixiID  
									from GFI_SYS_Roles r, GFI_SYS_GroupMatrixRoles m, tree
								where tree.GMID = m.GMID and r.iID = m.iID
                            union all
								select -5, r.Description, m.GMID as ParentGMID, r.iID, null, m.MID, m.iID MatrixiID  
									from GFI_SYS_Roles r, GFI_SYS_GroupMatrixRoles m
								where m.GMID in " + s + @" and r.iID = m.iID";
                    break;

                case NaveoModels.BLUP:
                    sql = sql.Replace("SELECT Distinct GMID, GMDescription, ParentGMID, -2 StrucID, null myStatus, -9 mID, -20 matrixiID FROM tree", sBlupParentMatrix());
                    sql += @"
                                union all
                                    select -4, CONCAT(b.APL_ID, ' - ', b.APL_NAME, ' - ', b.APL_FNAME) as GMDescription, 2147410000 + b.cat as ParentGMID, b.iID, null, m.MID, m.iID MatrixiID  
		                                from GFI_GIS_BLUP b
	                                inner join GFI_SYS_GroupMatrixBLUP m on b.iID = m.iID
	                                inner join tree on m.GMID = tree.GMID
                                union all
	                                select -5, CONCAT(b.APL_ID, ' - ', b.APL_NAME, ' - ', b.APL_FNAME) as GMDescription, 2147410000 + b.cat as ParentGMID, b.iID, null, m.MID, m.iID MatrixiID  
		                                from GFI_GIS_BLUP b
	                                inner join GFI_SYS_GroupMatrixBLUP m on b.iID = m.iID
		                                where m.GMID in " + s;
                    break;

                case NaveoModels.FixedAssets:
                    sql = sql.Replace("SELECT Distinct GMID, GMDescription, ParentGMID, -2 StrucID, null myStatus, -9 mID, -20 matrixiID FROM tree", sFixedAssetsParentMatrix());
                    sql = sql.Replace("--union all", "and GMID < -9");
                    sql += @" union all
								select -4, fa.AssetCode, "+ NaveoEntity.FixedAssetGroup + @" + fa.AssetCategory as ParentGMID, fa.AssetID, null, m.MID, m.iID MatrixiID  
									from GFI_GIS_FixedAsset fa
										inner join GFI_SYS_GroupMatrixFixedAsset m on fa.AssetID = m.iID
										inner join tree on tree.GMID = m.GMID
                            union all
								select -5, fa.AssetCode, " + NaveoEntity.FixedAssetGroup + @" + fa.AssetCategory as ParentGMID, fa.AssetID, null, m.MID, m.iID MatrixiID
									from GFI_GIS_FixedAsset fa
                                        inner join GFI_SYS_GroupMatrixFixedAsset m on fa.AssetID = m.iID
								where m.GMID in " + s;
                    break;

                case NaveoModels.Districts:
                    sql += @"union all
							select distinct c.CountryID, c.CountryName, m.GMID as ParentGMID, c.CountryID, 'Country', 0, -21 MatrixiID  
									from GFI_GIS_District a
                                        inner join GFI_SYS_GroupMatrixDistrict m on a.DistrictID = m.iID 
                                        --inner join tree on tree.ParentGMID = m.GMID
										inner join GFI_GIS_Country c on c.CountryID = a.CountryID
                            union all
								select distinct a.DistrictID, a.DistrictName, a.CountryID as ParentGMID, a.DistrictID, 'District', m.MID, m.iID MatrixiID  
									from GFI_GIS_District a
                                        inner join GFI_SYS_GroupMatrixDistrict m on a.DistrictID = m.iID
                                        --inner join tree on tree.ParentGMID = m.GMID
										inner join GFI_GIS_Country c on c.CountryID = a.CountryID
                            union all
								select  distinct V.VCAID, v.VCAName, d.DistrictID as ParentGMID, V.VCAID, 'VCA', v.VCAID, m.iID MatrixiID
									from GFI_GIS_District d
										inner join GFI_SYS_GroupMatrixDistrict m on d.DistrictID = m.iID
										inner join GFI_GIS_VCA v on d.DistrictID = v.DistrictID
										--inner join tree on tree.ParentGMID = m.GMID
                            union all   --We are using min(r.RoadID) to group roads. sames needs to be ferrered in further queries
								select distinct -7, R.RoadName, v.VCAID as ParentGMID, min(r.RoadID) StructID, 'Road', v.VCAID, m.iID MatrixiID  
									from GFI_GIS_District d
										inner join GFI_SYS_GroupMatrixDistrict m on d.DistrictID = m.iID
										inner join GFI_GIS_VCA v on d.DistrictID = v.DistrictID
										inner join GFI_GIS_RoadNetwork r on r.VCAID = v.VCAID
                                    group by R.RoadName, v.VCAID, m.iID
";
                    break;

                case NaveoModels.PlanningRule:
                    sql += @" union all
								select -4, a.RuleName, tree.GMID as ParentGMID, a.RuleID, null, m.MID, m.iID MatrixiID  
									from GFI_GPS_Rules a, GFI_SYS_GroupMatrixRule m, tree
								where tree.GMID = m.GMID and a.RuleID = m.iID and   a.StrucValue = '2' and a.Struc = 'Exception Type'
                            union all
								select -5, a.RuleName, m.GMID as ParentGMID, a.RuleID, null, m.MID, m.iID MatrixiID  
									from GFI_GPS_Rules a, GFI_SYS_GroupMatrixRule m
								where m.GMID in " + s + @" and a.RuleID = m.iID and a.StrucValue = '2' and a.Struc = 'Exception Type'";
                    break;

                case NaveoModels.MaintenanceRule:
                    sql += @" union all
								select -4, a.RuleName, tree.GMID as ParentGMID, a.RuleID, null, m.MID, m.iID MatrixiID  
									from GFI_GPS_Rules a, GFI_SYS_GroupMatrixRule m, tree
								where tree.GMID = m.GMID and a.RuleID = m.iID and   a.StrucValue = '3' and a.Struc = 'Exception Type'
                            union all
								select -5, a.RuleName, m.GMID as ParentGMID, a.RuleID, null, m.MID, m.iID MatrixiID  
									from GFI_GPS_Rules a, GFI_SYS_GroupMatrixRule m
								where m.GMID in " + s + @" and a.RuleID = m.iID and a.StrucValue = '3' and a.Struc = 'Exception Type'";
                    break;

                case NaveoModels.FuelRule:
                    sql = @"select -4 as GMID, a.RuleName as GMDescription, 0 as ParentGMID, a.RuleID as StrucID, 0 as myStatus, 0 as MID,0 as MatrixiID  
									from GFI_GPS_Rules a where a.Struc = 'Fuel'";
                    break;
            }

            sql += "\r\n";
            sql += "\r\n";
            //sql += "\r\n order by GMID --Remove from Oracle";
            return sql;
        }
        String sFixedAssetsParentMatrix()
        {
            String sql = @"
WITH 
tree (GMID, GMDescription, ParentGMID) AS 
(
        Select 2147480000 as GMID, 'FixedAssets' As GMDescription, -1 as ParentGMID
        union select iID as GMID, AssetType As GMDescription, 2147480000 as ParentGMID from GFI_GIS_AssetType
)
    SELECT Distinct GMID, GMDescription, ParentGMID, -2 StrucID, null myStatus, -9 mID, -20 matrixiID FROM tree
";

            sql = @"
, FAtree (GMID, GMDescription, ParentGMID) AS 
    (
		    Select " + NaveoEntity.FixedAssetGroup + @" as GMID, 'FixedAssets' As GMDescription, -1 as ParentGMID
		    union select iID as GMID, AssetType As GMDescription, " + NaveoEntity.FixedAssetGroup + @" as ParentGMID from GFI_GIS_AssetType
            union select " + NaveoEntity.FixedAssetGroup + @" + iID as GMID, AssetCategory as GMDescription, AssetTypeID as ParentGMID from GFI_GIS_AssetCategory 
            --union select distinct " + NaveoEntity.FixedAssetGroup + @" + f.AssetCategory as GMID, c.AssetCategory as GMDescription, f.AssetType as ParentGMID from GFI_GIS_FixedAsset f inner join GFI_GIS_AssetCategory c on c.iID = f.AssetCategory
    )
    SELECT Distinct GMID, GMDescription, ParentGMID, -2 StrucID, null myStatus, -9 mID, -20 matrixiID FROM tree where GMID < -9
    	union all SELECT Distinct GMID, GMDescription, ParentGMID, -2 StrucID, null myStatus, -9 mID, -20 matrixiID FROM FAtree
";
            return sql;
        }
        public DataTable GetAllGMValues(List<int> iParentIDs, NaveoModels strStruc, String sConnStr)
        {
            String sql = sGetAllGMValues(iParentIDs, strStruc);
            DataTable dt = new Connection(sConnStr).GetDataDT(sql, sConnStr);

            return dt;
        }
        public String GetAllGMValuesWhereClause(List<int> iParentIDs, String sConnStr)
        {
            DataTable dt = GetAllGMValues(iParentIDs, NaveoModels.Groups, sConnStr);
            List<String> ParentList = new List<String>();
            foreach (DataRow dr in dt.Rows)
                ParentList.Add(dr["GMID"].ToString());

            String strResult = String.Empty;
            foreach (String g in ParentList)
                strResult += g + ",";
            strResult += "-1";

            return strResult;
        }
        private void BuildTree(DataTable dt, String strParentCatCode, TreeNode strParentNode, TreeView tvNode)
        {
            if (!dt.Columns.Contains("ParentGMID"))
                return;

            DataRow[] drFiltered = dt.Select("ParentGMID = " + strParentCatCode);
            foreach (DataRow dr in drFiltered)
            {
                String Ref = dr["GMDescription"].ToString();
                String strMyParentCode = dr["GMID"].ToString();
                String strStruc = dr["StrucID"].ToString();
                TreeNode trnode;

                if (strParentNode != null)
                    trnode = strParentNode.Nodes.Add(strMyParentCode, Ref, dr["ParentGMID"].ToString(), strStruc);
                else
                    trnode = tvNode.Nodes.Add(strMyParentCode, Ref, dr["ParentGMID"].ToString(), strStruc);

                int i = 0;
                if (trnode.SelectedImageKey.Trim().Length > 0)
                    i = Convert.ToInt32(trnode.SelectedImageKey);
                if (i > 0)
                    trnode.ImageIndex = 1;
                else
                    trnode.ImageIndex = 0;

                BuildTree(dt, strMyParentCode, trnode, tvNode);
            }
        }
        public void PopulateTreeCategories(TreeView tvNode, List<int> iParentIDs, NaveoModels strStruc, String sConnStr)
        {
            DataTable dtTreeCategory = new DataTable();

            switch (strStruc)
            {
                case NaveoModels.Groups:
                    if (Globals.dtGMNone.Rows.Count == 0)
                        Globals.dtGMNone = GetAllGMValues(iParentIDs, strStruc, sConnStr);
                    dtTreeCategory = Globals.dtGMNone;
                    break;

                case NaveoModels.Assets:
                    if (Globals.dtGMAsset.Rows.Count == 0)
                        Globals.dtGMAsset = GetAllGMValues(iParentIDs, strStruc, sConnStr);
                    dtTreeCategory = Globals.dtGMAsset;
                    break;

                case NaveoModels.Users:
                    if (Globals.dtGMUser.Rows.Count == 0)
                        Globals.dtGMUser = GetAllGMValues(iParentIDs, strStruc, sConnStr);
                    dtTreeCategory = Globals.dtGMUser;
                    break;

                case NaveoModels.Zones:
                    if (Globals.dtGMZone.Rows.Count == 0)
                        Globals.dtGMZone = GetAllGMValues(iParentIDs, strStruc, sConnStr);
                    dtTreeCategory = Globals.dtGMZone;
                    break;

                case NaveoModels.Drivers:
                    if (Globals.dtGMDriver.Rows.Count == 0)
                        Globals.dtGMDriver = GetAllGMValues(iParentIDs, strStruc, sConnStr);
                    dtTreeCategory = Globals.dtGMDriver;
                    break;

                case NaveoModels.Rules:
                    if (Globals.dtGMRule.Rows.Count == 0)
                        Globals.dtGMRule = GetAllGMValues(iParentIDs, strStruc, sConnStr);
                    dtTreeCategory = Globals.dtGMRule;
                    break;

                case NaveoModels.ZoneTypes:
                    if (Globals.dtGMZoneType.Rows.Count == 0)
                        Globals.dtGMZoneType = GetAllGMValues(iParentIDs, strStruc, sConnStr);
                    dtTreeCategory = Globals.dtGMZoneType;
                    break;

                case NaveoModels.BLUP:
                    if (Globals.dtApplications.Rows.Count == 0)
                        Globals.dtApplications = new ExtZonePropService().GetAllInTree(sConnStr);
                    dtTreeCategory = Globals.dtApplications;
                    break;

                default:
                    dtTreeCategory = GetAllGMValues(iParentIDs, strStruc, sConnStr);
                    break;
            }
            tvNode.Nodes.Clear();

            if (dtTreeCategory.Columns.Contains("StrucID"))
            {
                DataView dv = dtTreeCategory.DefaultView;
                dv.Sort = "StrucID";
                dtTreeCategory = dv.ToTable();
            }
            Globals.dtGMTreeCategory = dtTreeCategory;

            BuildTree(dtTreeCategory, "-1", null, tvNode);
        }
        public void PopulateTreeCategories(List<Matrix> lMatrix, NaveoModels strStruc, String sConnStr)
        {
            DataTable dtTreeCategory = new DataTable();

            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in Globals.uLogin.lMatrix)
                iParentIDs.Add(m.GMID);

            switch (strStruc)
            {
                case NaveoModels.Groups:
                    if (Globals.dtGMNone.Rows.Count == 0)
                        Globals.dtGMNone = GetAllGMValues(iParentIDs, strStruc, sConnStr);
                    dtTreeCategory = Globals.dtGMNone;
                    break;

                case NaveoModels.Assets:
                    if (Globals.dtGMAsset.Rows.Count == 0)
                        Globals.dtGMAsset = GetAllGMValues(iParentIDs, strStruc, sConnStr);
                    dtTreeCategory = Globals.dtGMAsset;
                    break;

                case NaveoModels.Users:
                    if (Globals.dtGMUser.Rows.Count == 0)
                        Globals.dtGMUser = GetAllGMValues(iParentIDs, strStruc, sConnStr);
                    dtTreeCategory = Globals.dtGMUser;
                    break;

                case NaveoModels.Zones:
                    if (Globals.dtGMZone.Rows.Count == 0)
                        Globals.dtGMZone = GetAllGMValues(iParentIDs, strStruc, sConnStr);
                    dtTreeCategory = Globals.dtGMZone;
                    break;

                case NaveoModels.Drivers:
                    if (Globals.dtGMDriver.Rows.Count == 0)
                        Globals.dtGMDriver = GetAllGMValues(iParentIDs, strStruc, sConnStr);
                    dtTreeCategory = Globals.dtGMDriver;
                    break;

                case NaveoModels.Rules:
                    if (Globals.dtGMRule.Rows.Count == 0)
                        Globals.dtGMRule = GetAllGMValues(iParentIDs, strStruc, sConnStr);
                    dtTreeCategory = Globals.dtGMRule;
                    break;

                case NaveoModels.ZoneTypes:
                    if (Globals.dtGMZoneType.Rows.Count == 0)
                        Globals.dtGMZoneType = GetAllGMValues(iParentIDs, strStruc, sConnStr);
                    dtTreeCategory = Globals.dtGMZoneType;
                    break;

                case NaveoModels.BLUP:
                    if (Globals.dtApplications.Rows.Count == 0)
                        Globals.dtApplications = new ExtZonePropService().GetAllInTree(sConnStr);
                    dtTreeCategory = Globals.dtApplications;
                    break;

                default:
                    dtTreeCategory = GetAllGMValues(iParentIDs, strStruc, sConnStr);
                    break;
            }

            DataView dv = dtTreeCategory.DefaultView;
            dv.Sort = "StrucID";
            dtTreeCategory = dv.ToTable();
            Globals.dtGMTreeCategory = dtTreeCategory;
        }

        String sBlupParentMatrix()
        {
            String sql = @"
, BlupTree (GMID, GMDescription, ParentGMID) AS 
(
    Select 2147410000 + 1 as GMID, 'BLUP' As GMDescription, -1 as ParentGMID
    UNION Select 2147410000 + 2 as GMID,'COMMERCIAL' As GMDescription, 2147410000 + 1 as ParentGMID
    UNION Select 2147410000 + 3 as GMID,'RESIDENTIAL' As GMDescription, 2147410000 + 1 as ParentGMID
    UNION Select 2147410000 + 4 as GMID,'MORCELLEMNT' As GMDescription, 2147410000 + 1 as ParentGMID
    UNION Select 2147410000 + 5 as GMID,'OPP' As GMDescription, 2147410000 + 1 as ParentGMID
    UNION Select 2147410000 + 6 as GMID,'SERV' As GMDescription, 2147410000 + 1 as ParentGMID
    UNION Select 2147410000 + 7 as GMID,'SG' As GMDescription, 2147410000 + 1 as ParentGMID
    UNION Select 2147410000 + 8 as GMID,'WALL' As GMDescription, 2147410000 + 1 as ParentGMID
    UNION Select 2147410000 + 9 as GMID,'INDUSTRIAL' As GMDescription, 2147410000 + 1 as ParentGMID
    UNION Select 2147410000 + 10 as GMID,'EIA/PER' As GMDescription, 2147410000 + 1 as ParentGMID
    UNION Select 2147410000 + 11 as GMID,'Morcellement Board' As GMDescription, 2147410000 + 1 as ParentGMID
    UNION Select 2147410000 + 12 as GMID,'Land Conversion' As GMDescription, 2147410000 + 1 as ParentGMID
)
SELECT Distinct GMID, GMDescription, ParentGMID, -2 StrucID, null myStatus, -9 mID, -20 matrixiID FROM tree
	union all SELECT Distinct GMID, GMDescription, ParentGMID, -2 StrucID, null myStatus, -9 mID, -20 matrixiID FROM BlupTree
";
            return sql;
        }
        public String sGetAllGMValuesForBLUP()
        {
            String sql = @"
;WITH 
    parent AS 
    (
        SELECT distinct ParentGMID
        FROM GFI_SYS_GroupMatrix
        WHERE ParentGMID in (5) 
    ), 
    tree (GMID, GMDescription, ParentGMID) AS 
    (
        SELECT 
            x.GMID
            , x.GMDescription
            , x.ParentGMID
        FROM GFI_SYS_GroupMatrix x
        INNER JOIN parent ON x.ParentGMID = parent.ParentGMID
        UNION ALL
        SELECT 
            y.GMID
            , y.GMDescription
            , y.ParentGMID
        FROM GFI_SYS_GroupMatrix y
        INNER JOIN tree t ON y.ParentGMID = t.GMID
    )
	
    , DefaultTree (PGMID, PGMDescription, PParentGMID) AS ( 
        SELECT GMID PGMID, 
                GMDescription PGMDescription, 
                ParentGMID PParentGMID
        FROM GFI_SYS_GroupMatrix
        WHERE GMID in (5) 

        UNION ALL 

        SELECT p.GMID PGMID,
                p.GMDescription PGMDescription,
                p.ParentGMID PParentGMID 
        FROM GFI_SYS_GroupMatrix p
            JOIN DefaultTree t ON t.PParentGMID = p.GMID
    )

    SELECT -1 as GMID, GMDescription, ParentGMID, -2 StrucID, null myStatus, -9 mID, -20 matrixiID FROM tree where GMID = -9
                            
    union all
    select -4, CONCAT(b.APL_ID, ' - ', b.APL_NAME, ' - ', b.APL_FNAME) as GMDescription, b.cat as ParentGMID, b.iID as StrucID, null, m.MID, m.iID MatrixiID  
	    from GFI_GIS_BLUP b, GFI_SYS_GroupMatrixBLUP m, tree
    where tree.GMID = m.GMID and b.iID = m.iID
    union all
    select -5, CONCAT(b.APL_ID, ' - ', b.APL_NAME, ' - ', b.APL_FNAME) as GMDescription, b.cat as ParentGMID, b.iID as StrucID, null, m.MID, m.iID MatrixiID  
	    from GFI_GIS_BLUP b, GFI_SYS_GroupMatrixBLUP m
    where m.GMID in (5)  and b.iID = m.iID
    UNION 
        Select 1 as GMID,'ALL PLANNINGS' As GMDescription,-1 as ParentGMID,
                    0 as StrucID, 
                    'AC' as MyStatus, 
                    0 as MID,
                    0 as MatriXID
        UNION
            Select 2 as GMID,'COMMERCIAL' As GMDescription,1 as ParentGMID,
                    0 as StrucID, 
                    'AC' as MyStatus, 
                    0 as MID,
                    0 as MatriXID
        UNION
            Select 3 as GMID,'RESIDENTIAL' As GMDescription,1 as ParentGMID,
                    0 as StrucID, 
                    'AC' as MyStatus, 
                    0 as MID,
                    0 as MatriXID
        UNION
            Select 4 as GMID,'MORCELLEMNT' As GMDescription,1 as ParentGMID,
                    0 as StrucID, 
                    'AC' as MyStatus, 
                    0 as MID,
                    0 as MatriXID
        UNION
                    Select 5 as GMID,'OPP' As GMDescription,1 as ParentGMID,
                    0 as StrucID, 
                    'AC' as MyStatus, 
                    0 as MID,
                    0 as MatriXID
        UNION
            Select 6 as GMID,'SERV' As GMDescription,1 as ParentGMID,
                    0 as StrucID, 
                    'AC' as MyStatus, 
                    0 as MID,
                    0 as MatriXID
        UNION
            Select 7 as GMID,'SG' As GMDescription,1 as ParentGMID,
                    0 as StrucID, 
                    'AC' as MyStatus, 
                    0 as MID,
                    0 as MatriXID
        UNION
            Select 8 as GMID,'WALL' As GMDescription,1 as ParentGMID,
                    0 as StrucID, 
                    'AC' as MyStatus, 
                    0 as MID,
                    0 as MatriXID
        UNION
            Select 9 as GMID,'INDUSTRIAL' As GMDescription,1 as ParentGMID,
                    0 as StrucID, 
                    'AC' as MyStatus, 
                    0 as MID,
                    0 as MatriXID
 order by GMID --Remove from Oracle";
            return sql;
        }
        public String sGetAllGMParentsForBLUP()
        {
            String sql = @"
Select 1 as GMID,'ALL PLANNINGS' As GMDescription,-1 as ParentGMID,
        0 as StrucID, 
        'AC' as MyStatus, 
        0 as MID,
        0 as MatriXID
    UNION
        Select 2 as GMID,'COMMERCIAL' As GMDescription,1 as ParentGMID,
            0 as StrucID, 
            'AC' as MyStatus, 
            0 as MID,
            0 as MatriXID
    UNION
        Select 3 as GMID,'RESIDENTIAL' As GMDescription,1 as ParentGMID,
            0 as StrucID, 
            'AC' as MyStatus, 
            0 as MID,
            0 as MatriXID
    UNION
        Select 4 as GMID,'MORCELLEMNT' As GMDescription,1 as ParentGMID,
            0 as StrucID, 
            'AC' as MyStatus, 
            0 as MID,
            0 as MatriXID
    UNION
        Select 5 as GMID,'OPP' As GMDescription,1 as ParentGMID,
            0 as StrucID, 
            'AC' as MyStatus, 
            0 as MID,
            0 as MatriXID
    UNION
        Select 6 as GMID,'SERV' As GMDescription,1 as ParentGMID,
            0 as StrucID, 
            'AC' as MyStatus, 
            0 as MID,
            0 as MatriXID
    UNION
        Select 7 as GMID,'SG' As GMDescription,1 as ParentGMID,
            0 as StrucID, 
            'AC' as MyStatus, 
            0 as MID,
            0 as MatriXID
    UNION
        Select 8 as GMID,'WALL' As GMDescription,1 as ParentGMID,
            0 as StrucID, 
            'AC' as MyStatus, 
            0 as MID,
            0 as MatriXID
    UNION
        Select 9 as GMID,'INDUSTRIAL' As GMDescription,1 as ParentGMID,
            0 as StrucID, 
            'AC' as MyStatus, 
            0 as MID,
            0 as MatriXID
order by GMID";
            return sql;
        }

        public List<int> GetAllParentsfromGMID(int iGMID, String sConnStr)
        {
            String sql = @"WITH tree AS ( 
                               SELECT GMID, 
                                      GMDescription, 
                                      ParentGMID,
                                      1 as level 
                               FROM GFI_SYS_GroupMatrix
                               WHERE GMID = " + iGMID.ToString() + @"

                               UNION ALL 

                               SELECT p.GMID,
                                      p.GMDescription,
                                      p.ParentGMID, 
                                      t.level + 1
                               FROM GFI_SYS_GroupMatrix p
                                 JOIN tree t ON t.ParentGMID = p.GMID
                            )
                            SELECT *
                            FROM tree";
            sql = sGetAllParentsfromGMID(iGMID);    //same above is now in sGetAllParentsfromGMID. above not deleted as not yet tested
            DataTable dt = new Connection(sConnStr).GetDataDT(sql, sConnStr);
            List<int> myList = new List<int>();
            foreach (DataRow dr in dt.Rows)
                myList.Add((int)dr[0]);

            return myList;
        }
        public String sGetAllParentsfromGMID(int iGMID)
        {
            String sql = @"WITH --postGreSQL RECURSIVE 
tree AS ( 
                               SELECT GMID, 
                                      GMDescription, 
                                      ParentGMID,
                                      1 as level 
                               FROM GFI_SYS_GroupMatrix
                               WHERE GMID = " + iGMID.ToString() + @"

                               UNION ALL 

                               SELECT p.GMID,
                                      p.GMDescription,
                                      p.ParentGMID, 
                                      t.level + 1
                               FROM GFI_SYS_GroupMatrix p
                                 JOIN tree t ON t.ParentGMID = p.GMID
                            )
                            SELECT * FROM tree";
            return sql;
        }
        public String sGetAllParentsDescfromGMID(int iGMID)
        {
            String sql = @"declare @lvl int
;WITH --postGreSQL RECURSIVE 
tree AS ( 
    SELECT GMID, 
            GMDescription, 
            ParentGMID,
            1 as level 
    FROM GFI_SYS_GroupMatrix
    WHERE GMID = " + iGMID.ToString() + @"

    UNION ALL 

    SELECT p.GMID,
            p.GMDescription,
            p.ParentGMID, 
            t.level + 1
    FROM GFI_SYS_GroupMatrix p
        JOIN tree t ON t.ParentGMID = p.GMID
)
select @lvl =  max(level) from tree

;WITH --postGreSQL RECURSIVE 
tree AS ( 
    SELECT GMID, 
            GMDescription, 
            ParentGMID,
            @lvl as level 
    FROM GFI_SYS_GroupMatrix
    WHERE GMID = " + iGMID.ToString() + @"

    UNION ALL 

    SELECT p.GMID,
            p.GMDescription,
            p.ParentGMID, 
            t.level - 1
    FROM GFI_SYS_GroupMatrix p
        JOIN tree t ON t.ParentGMID = p.GMID
)
select * from tree
";
            return sql;
        }

        public String sGetAllParentsGMIDfromGMID(int iGMID)
        {
            String sql = sGetAllParentsfromGMID(iGMID);
            sql = sql.Replace("SELECT * FROM tree", "SELECT GMID FROM tree");
            return sql;
        }
        public String sGetCompanyGMIDfromGMID(int iGMID)
        {
            String sql = sGetAllParentsfromGMID(iGMID);
            sql = sql.Replace(" GMID,", " GMID, IsCompany,");
            sql = sql.Replace("p.GMID,", "p.GMID, p.IsCompany,");
            sql = sql.Replace("SELECT * FROM tree", "SELECT * FROM tree where IsCompany = 1");
            sql += " union select GMID, IsCompany, GMDescription, ParentGMID, 1 as level FROM GFI_SYS_GroupMatrix WHERE ParentGMID = " + iGMID.ToString() + " and IsCompany = 1";
            return sql;
        }

        public int GetMasterParentfromGMID(int iGMID, String sConnStr)
        {
            String sql = sGetAllParentsfromGMID(iGMID) + "  where ParentGMID = (select GMID from GFI_SYS_GroupMatrix where GMDescription = '### All Clients ###')";
            DataTable dt = new Connection(sConnStr).GetDataDT(sql, sConnStr);
            return Convert.ToInt32(dt.Rows[0][0]);
        }

        public List<int> GetAllDefaultsfromGMID(int iGMID, String sConnStr)
        {
            String sql = sGetAllDefaultFromGMID(iGMID);
            DataTable dt = new Connection(sConnStr).GetDataDT(sql, sConnStr);
            List<int> myList = new List<int>();
            foreach (DataRow dr in dt.Rows)
                myList.Add((int)dr["GMID"]);

            return myList;
        }
        public String sGetAllDefaultFromGMID(int iGMID)
        {
            String sql = @"WITH tree AS ( 
                                SELECT GMID PGMID, 
                                        GMDescription PGMDescription, 
                                        ParentGMID PParentGMID,
				                        isCompany PisCompany, 
                                        1 as Plevel 
                                FROM GFI_SYS_GroupMatrix
                                WHERE GMID = " + iGMID.ToString() + @"

                                UNION ALL 

                                SELECT p.GMID PGMID,
                                        p.GMDescription PGMDescription,
                                        p.ParentGMID PParentGMID, 
				                        p.isCompany PisCompany, 
                                        t.Plevel + 1 PLevel
                                FROM GFI_SYS_GroupMatrix p
                                    JOIN tree t ON t.PParentGMID = p.GMID
                            )
                        SELECT * FROM tree t, GFI_SYS_GroupMatrix m where m.isCompany = 0 and m.ParentGMID = t.PGMID";
            return sql;
        }

        public GroupMatrix GetGroupMatrixByName(String strName, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = "SELECT * from GFI_SYS_GroupMatrix WHERE GMDescription = '" + strName + "'";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGroupMatrix";
            dtSql.Rows.Add(drSql);

            //Matrix
            sql = @"select n.*, m.MID from GFI_SYS_NaveoModules n
	                    inner join GFI_SYS_GroupMatrixNaveoModules m on m.iID = n.iID
                        inner join GFI_SYS_GroupMatrix g on n.iID = g.GMID
                    where g.GMDescription = '" + strName + "'";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtModules";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables["dtGroupMatrix"].Rows.Count == 0)
                return null;
            else
                return myGroupMatrix(ds);
        }
        public GroupMatrix GetGroupMatrixById(int GMID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = "SELECT * from GFI_SYS_GroupMatrix WHERE GMID = " + GMID;
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGroupMatrix";
            dtSql.Rows.Add(drSql);

            //Matrix
            sql = @"select n.*, m.MID from GFI_SYS_NaveoModules n
	                    inner join GFI_SYS_GroupMatrixNaveoModules m on m.iID = n.iID
                    where m.GMID = " + GMID;
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtModules";
            dtSql.Rows.Add(drSql);

            sql = @"select approvalProcess.*, m.GMDescription,m.IsCompany from GFI_SYS_GroupMatrixApprovalProcess approvalProcess
	                    inner join GFI_SYS_GroupMatrix m on m.GMID = approvalProcess.GMID
                    where m.GMID = " + GMID;
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtApprovalProcess";
            dtSql.Rows.Add(drSql);
            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables["dtGroupMatrix"].Rows.Count == 0)
                return null;
            else
                return myGroupMatrix(ds);
        }
        GroupMatrix myGroupMatrix(DataSet ds)
        {
            DataRow dr = ds.Tables["dtGroupMatrix"].Rows[0];
            GroupMatrix retGroupMatrix = new GroupMatrix();
            retGroupMatrix.gmID = Convert.ToInt32(dr["GMID"]);
            retGroupMatrix.gmDescription = dr["GMDescription"].ToString();
            if (dr["ParentGMID"].ToString().Trim().Length > 0)
                retGroupMatrix.parentGMID = Convert.ToInt32(dr["ParentGMID"]);
            retGroupMatrix.remarks = dr["Remarks"].ToString();

            if (dr.Table.Columns.Contains("LegalName"))
                retGroupMatrix.legalName = dr["LegalName"].ToString();
            if (dr.Table.Columns.Contains("CompanyCode"))
                retGroupMatrix.companyCode = dr["CompanyCode"].ToString();

            if (dr.Table.Columns.Contains("isCompany"))
                if (dr["isCompany"].ToString().Trim().Length > 0)
                    retGroupMatrix.isCompany = Convert.ToInt32(dr["isCompany"]);

            retGroupMatrix.lNaveoModules = new List<NaveoModule>();
            if (ds.Tables.Contains("dtModules"))
            {
                foreach (DataRow r in ds.Tables["dtModules"].Rows)
                {
                    NaveoModule m = new NaveoModule();
                    m.iID = Convert.ToInt32(r["iID"]);
                    m.description = r["Description"].ToString();

                    if (ds.Tables["dtModules"].Columns.Contains("MID"))
                        m.mID = Convert.ToInt32(r["MID"]);

                    retGroupMatrix.lNaveoModules.Add(m);
                }
            }


            retGroupMatrix.lGroupMatrixApprovalProcess = new List<GroupMatrixApprovalProcess>();
            if(ds.Tables.Contains("dtApprovalProcess"))
            {
                foreach (DataRow r in ds.Tables["dtApprovalProcess"].Rows)
                {
                    GroupMatrixApprovalProcess groupMatrixApprovalProcess = new GroupMatrixApprovalProcess();
                    groupMatrixApprovalProcess.iID = Convert.ToInt32(r["iID"]);
                    groupMatrixApprovalProcess.gmID = Convert.ToInt32(r["GMID"]);
                    groupMatrixApprovalProcess.source = r["Source"].ToString();
                    groupMatrixApprovalProcess.approvalName = r["ApprovalName"].ToString();
                    groupMatrixApprovalProcess.approvalValue = Convert.ToInt32(r["ApprovalValue"]);

                    retGroupMatrix.lGroupMatrixApprovalProcess.Add(groupMatrixApprovalProcess);
                }
            }

            return retGroupMatrix;
        }

        public GroupMatrix GetAllClients(String sConnStr)
        {
            String sqlString = @"SELECT * from GFI_SYS_GroupMatrix WHERE GMDescription = '### All Clients ###'";
            DataTable dt = new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
            if (dt.Rows.Count == 0)
                return new GroupMatrix();

            dt.TableName = "dtGroupMatrix";
            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            return myGroupMatrix(ds);
        }
        public GroupMatrix GetRoot(String sConnStr)
        {
            String sqlString = @"SELECT * from GFI_SYS_GroupMatrix WHERE ParentGMID is NULL";
            DataTable dt = new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
            if (dt.Rows.Count == 0)
                return null;

            dt.TableName = "dtGroupMatrix";
            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            return myGroupMatrix(ds);
        }
        public BaseModel SaveGroupMatrix(GroupMatrix uGroupMatrix, Boolean bInsert, String sConnStr)
        {
            BaseModel baseModel = new BaseModel();

            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_GroupMatrix";
            dt.Columns.Add("GMID", uGroupMatrix.gmID.GetType());
            dt.Columns.Add("GMDescription", uGroupMatrix.gmDescription.GetType());

            if (uGroupMatrix.parentGMID.HasValue)
            {
                dt.Columns.Add("ParentGMID", uGroupMatrix.parentGMID.GetType());

                GroupMatrix allClients = GetAllClients(sConnStr);
                if (uGroupMatrix.parentGMID.Value == allClients.gmID)
                    uGroupMatrix.isCompany = 1;
            }
            if (!String.IsNullOrEmpty(uGroupMatrix.remarks))
                dt.Columns.Add("Remarks", uGroupMatrix.remarks.GetType());

            if (!String.IsNullOrEmpty(uGroupMatrix.isCompany.ToString()))
                dt.Columns.Add("IsCompany", uGroupMatrix.isCompany.GetType());

            if (!String.IsNullOrEmpty(uGroupMatrix.companyCode))
                dt.Columns.Add("CompanyCode", uGroupMatrix.companyCode.GetType());

            if (!String.IsNullOrEmpty(uGroupMatrix.legalName))
                dt.Columns.Add("LegalName", uGroupMatrix.legalName.GetType());

            DataRow dr = dt.NewRow();
            dr["GMID"] = uGroupMatrix.gmID;
            dr["GMDescription"] = uGroupMatrix.gmDescription;
            if (uGroupMatrix.parentGMID.HasValue)
                dr["ParentGMID"] = uGroupMatrix.parentGMID;

            if (!String.IsNullOrEmpty(uGroupMatrix.remarks))
                dr["Remarks"] = uGroupMatrix.remarks;

            if (!String.IsNullOrEmpty(uGroupMatrix.isCompany.ToString()))
                dr["IsCompany"] = uGroupMatrix.isCompany;

            if (!String.IsNullOrEmpty(uGroupMatrix.companyCode))
                dr["CompanyCode"] = uGroupMatrix.companyCode;

            if (!String.IsNullOrEmpty(uGroupMatrix.legalName))
                dr["LegalName"] = uGroupMatrix.legalName;

            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "GMID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uGroupMatrix.gmID;

            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            if (uGroupMatrix.lNaveoModules != null)
            {
                //Matrix
                DataTable dtMatrix = new DataTable();
                dtMatrix.TableName = "GFI_SYS_GroupMatrixNaveoModules";
                dtMatrix.Columns.Add("MID");
                dtMatrix.Columns.Add("GMID");
                dtMatrix.Columns.Add("iID");
                dtMatrix.Columns.Add("oprType", typeof(DataRowState));
                foreach (NaveoModule n in uGroupMatrix.lNaveoModules)
                {
                    DataRow r = dtMatrix.NewRow();
                    r["iID"] = n.iID;
                    r["MID"] = n.mID;
                    r["GMID"] = uGroupMatrix.gmID;
                    r["oprType"] = n.oprType;
                    if (n.mID == 0)
                        r["oprType"] = DataRowState.Added;
                    else if (n.iID == -1)
                        r["oprType"] = DataRowState.Deleted;

                    dtMatrix.Rows.Add(r);
                }
                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 2;
                drCtrl["TblName"] = dtMatrix.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "MID";
                drCtrl["KeyIsIdentity"] = "TRUE";
                if (bInsert)
                {
                    drCtrl["NextIdAction"] = "SET";
                    drCtrl["NextIdFieldName"] = "GMID";
                }
                drCtrl["ParentTblName"] = dt.TableName;
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtMatrix);
            }

            if(uGroupMatrix.lGroupMatrixApprovalProcess != null)
            {
                DataTable dtApprovalProcess = new DataTable();
                dtApprovalProcess.TableName = "GFI_SYS_GroupMatrixApprovalProcess";
                dtApprovalProcess.Columns.Add("iID");
                dtApprovalProcess.Columns.Add("GMID");
                dtApprovalProcess.Columns.Add("Source");
                dtApprovalProcess.Columns.Add("ApprovalName");
                dtApprovalProcess.Columns.Add("ApprovalValue");
                
                dtApprovalProcess.Columns.Add("oprType", typeof(DataRowState));

                foreach(GroupMatrixApprovalProcess gmA in uGroupMatrix.lGroupMatrixApprovalProcess)
                {
                    DataRow r = dtApprovalProcess.NewRow();
                    r["iID"] = gmA.iID;
                    r["GMID"] = gmA.gmID;
                    r["Source"] = gmA.source;
                    r["ApprovalName"] = gmA.approvalName;
                    r["ApprovalValue"] = gmA.approvalValue;
                    r["oprType"] = gmA.oprType;
                    if (gmA.iID == 0)
                        r["oprType"] = DataRowState.Added;
                    else if (gmA.gmID == -1)
                        r["oprType"] = DataRowState.Deleted;

                    dtApprovalProcess.Rows.Add(r);

                }

                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 3;
                drCtrl["TblName"] = dtApprovalProcess.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "iID";
                drCtrl["KeyIsIdentity"] = "TRUE";
                if (bInsert)
                {
                    drCtrl["NextIdAction"] = "SET";
                    drCtrl["NextIdFieldName"] = "GMID";
                }
                drCtrl["ParentTblName"] = dt.TableName;
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtApprovalProcess);


            }

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            Boolean bResult = false;
            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResult.Tables["CTRLTBL"];
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                {
                    baseModel.dbResult = dsResult;
                    baseModel.dbResult.Tables["CTRLTBL"].TableName = "ResultCtrlTbl";
                    baseModel.dbResult.Merge(dsProcess);
                    bResult = false;
                }
                else
                    bResult = true;
            }
            else
            {
                baseModel.dbResult = dsResult;
                bResult = false;
            }

            if (Globals.bDebugMode)
            {
                Globals.dtDevCTRL = dtCtrl;
                Globals.dsDev = dsProcess;
            }

            baseModel.data = bResult;
            
            return baseModel;
        }

        public bool DeleteGroupMatrix(GroupMatrix uGroupMatrix, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_SYS_GroupMatrix", "GMID = '" + uGroupMatrix.gmID + "'", sConnStr))
                return true;
            else
                return false;
        }


        public DataTable GetMatrixLegalName(int GMID,String sConnStr)
        {
            String sql = "select LegalName from GFI_SYS_GroupMatrix where GMID  in (select ParentGMID from GFI_SYS_GroupMatrix WHERE GMID  = " + GMID +")";
            return new Connection(sConnStr).GetDataDT(sql, sConnStr);
        }

    }
}



