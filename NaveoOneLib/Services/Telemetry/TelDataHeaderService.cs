using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.Telemetry;
using NaveoOneLib.Services.Audits;
using NaveoOneLib.Services.Fuels;
using NaveoOneLib.Services.Assets;

namespace NaveoOneLib.Services.Telemetry
{
    public class TelDataHeaderService
    {
        Errors er = new Errors();
        public DataTable GetTelDataHeader(String sConnStr)
        {
            String sqlString = @"SELECT iID, 
MsgHeader, 
Type, 
AssetID, 
UTCdt, 
MsgStatus, 
isMaintenanceMode, 
isFailedCallout, 
Temperature, 
isBatteryLow, 
isAutoConfig, 
Carrier, 
SignalStrength from GFI_TEL_TelDataHeader order by iID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        TelDataHeader GetTelDataHeader(DataRow dr)
        {
            TelDataHeader retTelDataHeader = new TelDataHeader();
            retTelDataHeader.iID = (int)dr["iID"];
            retTelDataHeader.MsgHeader = dr["MsgHeader"].ToString();
            retTelDataHeader.Type = dr["Type"].ToString();
            retTelDataHeader.AssetID = (int)dr["AssetID"];
            retTelDataHeader.UTCdt = (DateTime)dr["UTCdt"];
            retTelDataHeader.MsgStatus = dr["MsgStatus"].ToString();
            retTelDataHeader.isMaintenanceMode = String.IsNullOrEmpty(dr["isMaintenanceMode"].ToString()) ? (int?)null : (int)dr["isMaintenanceMode"];
            retTelDataHeader.isFailedCallout = String.IsNullOrEmpty(dr["isFailedCallout"].ToString()) ? (int?)null : (int)dr["isFailedCallout"];
            retTelDataHeader.Temperature = String.IsNullOrEmpty(dr["Temperature"].ToString()) ? (float?)null : (float)dr["Temperature"];
            retTelDataHeader.isBatteryLow = String.IsNullOrEmpty(dr["isBatteryLow"].ToString()) ? (int?)null : (int)dr["isBatteryLow"];
            retTelDataHeader.isAutoConfig = String.IsNullOrEmpty(dr["isAutoConfig"].ToString()) ? (int?)null : (int)dr["isAutoConfig"];
            retTelDataHeader.Carrier = dr["Carrier"].ToString();
            retTelDataHeader.SignalStrength = String.IsNullOrEmpty(dr["SignalStrength"].ToString()) ? (int?)null : (int)dr["SignalStrength"];
            return retTelDataHeader;
        }

        public TelDataHeader GetTelDataHeaderById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT iID, 
MsgHeader, 
Type, 
AssetID, 
UTCdt, 
MsgStatus, 
isMaintenanceMode, 
isFailedCallout, 
Temperature, 
isBatteryLow, 
isAutoConfig, 
Carrier, 
SignalStrength from GFI_TEL_TelDataHeader
WHERE iID = ?
order by iID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetTelDataHeader(dt.Rows[0]);
        }

        public DataTable GetFuelSensorData(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, List<String> lTypeID, String sConnStr, Boolean bCallibrateData = false)
        {
            String strAssetID = String.Empty;
            foreach (int i in lAssetID)
                strAssetID += i.ToString() + ",";
            strAssetID = strAssetID.Substring(0, strAssetID.Length - 1);

            String strTypeID = String.Empty;
            foreach (String s in lTypeID)
                strTypeID += s.ToString() + "','";
            strTypeID = "'" + strTypeID.Substring(0, strTypeID.Length - 2);

            String sql = @"SELECT * from GFI_TEL_TelDataHeader h
                            inner join GFI_TEL_TelDataDetail d on h.iID = d.HeaderiID
                           where h.UTCdt >= ? and h.UTCdt <= ?
                             and h.AssetID in (" + strAssetID + @")                             
                                order by h.AssetID, h.UTCdt";

            DataTable dtResult = new Connection(sConnStr).GetDataDT(sql, dtFrom.ToString() + "�" + dtTo.ToString(), null, sConnStr);
            foreach (DataRow dr in dtResult.Rows)
                dr["UTCdt"] = Convert.ToDateTime(dr["UTCdt"]).ToLocalTime();

            if (bCallibrateData)
                dtResult = FuelService.GetCallibratedData(dtResult, lAssetID, "SensorReading", sConnStr);
            return dtResult;
        }
        public DataTable GetLiveFuelSensorData(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, List<String> lTypeID, String sConnStr)
        {
            String strAssetID = String.Empty;
            foreach (int i in lAssetID)
                strAssetID += i.ToString() + ",";
            strAssetID = strAssetID.Substring(0, strAssetID.Length - 1);

            String strTypeID = String.Empty;
            foreach (String s in lTypeID)
                strTypeID += s.ToString() + "','";
            strTypeID = "'" + strTypeID.Substring(0, strTypeID.Length - 2);

            String sql = @";with cte as (
                                select *
                                    ,row_number() over(partition by AssetID order by UTCdt desc) as RowNum
	                            from GFI_TEL_TelDataHeader
                                WHERE AssetID in (" + strAssetID + @") 
                                    and UTCdt < getdate() + 1
	                                )
                            select *, 30000 TankCapacity from cte h
                            inner join GFI_TEL_TelDataDetail d on h.iID = d.HeaderiID
                            inner join GFI_FLT_AssetDeviceMap adm on h.AssetID = adm.AssetID
                                where RowNum = 1";

            DataTable dtResult = new Connection(sConnStr).GetDataDT(sql, dtFrom.ToString() + "�" + dtTo.ToString(), null,sConnStr);
            return dtResult;
        }

        Boolean isDuplicate(TelDataHeader uTelDataHeader, DbTransaction tTrans, String sConnStr)//, out int iID, out String strSensor)
        {
            //iID = -1;
            //strSensor = String.Empty;
            Boolean bResult = false;
            String sql = "select * from GFI_TEL_TelDataHeader where AssetID = ? and UTCdt = ?";
            String strParams = uTelDataHeader.AssetID.ToString() + "�" + uTelDataHeader.UTCdt.ToString("dd-MMM-yyyy HH:mm:ss.fff");
            DataTable dt = new Connection(sConnStr).GetDataDT(sql, strParams, tTrans, sConnStr);
            if (dt.Rows.Count > 0)
                bResult = true;
            //{
            //    bResult = true;
            //    iID = (int)(dt.Rows[0]["iID"]);

            //    sql = "select * from GFI_TEL_TelDataDetail where HeaderiID = " + iID.ToString();
            //    dt = new Connection(sConnStr).GetDataDT(sql, tTrans);
            //    foreach (DataRow dr in dt.Rows)
            //        strSensor += dr["Sensor"] + ",";

            //    if (strSensor.Length > 0)
            //        strSensor = strSensor.Substring(0, strSensor.Length - 1);
            //}
            return bResult;
        }
        DataTable TelDataHeaderDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_TEL_TelDataHeader";
            dt.Columns.Add("iID", typeof(int));
            dt.Columns.Add("MsgHeader", typeof(String));
            dt.Columns.Add("Type", typeof(String));
            dt.Columns.Add("AssetID", typeof(int));
            dt.Columns.Add("UTCdt", typeof(DateTime));
            dt.Columns.Add("MsgStatus", typeof(String));
            dt.Columns.Add("isMaintenanceMode", typeof(int));
            dt.Columns.Add("isFailedCallout", typeof(int));
            dt.Columns.Add("Temperature", typeof(float));
            dt.Columns.Add("isBatteryLow", typeof(int));
            dt.Columns.Add("isAutoConfig", typeof(int));
            dt.Columns.Add("Carrier", typeof(String));
            dt.Columns.Add("SignalStrength", typeof(int));

            return dt;
        }
        public Boolean SaveTelDataHeader(TelDataHeader uTelDataHeader, String sConnStr)
        {
            DataTable dt = TelDataHeaderDT();
            dt.Columns.Remove("iID");

            DataRow dr = dt.NewRow();
            dr["MsgHeader"] = uTelDataHeader.MsgHeader;
            dr["Type"] = uTelDataHeader.Type;
            dr["AssetID"] = uTelDataHeader.AssetID;
            dr["UTCdt"] = uTelDataHeader.UTCdt;
            dr["MsgStatus"] = uTelDataHeader.MsgStatus;
            dr["isMaintenanceMode"] = uTelDataHeader.isMaintenanceMode != null ? uTelDataHeader.isMaintenanceMode : (object)DBNull.Value;
            dr["isFailedCallout"] = uTelDataHeader.isFailedCallout != null ? uTelDataHeader.isFailedCallout : (object)DBNull.Value;
            dr["Temperature"] = uTelDataHeader.Temperature != null ? uTelDataHeader.Temperature : (object)DBNull.Value;
            dr["isBatteryLow"] = uTelDataHeader.isBatteryLow != null ? uTelDataHeader.isBatteryLow : (object)DBNull.Value;
            dr["isAutoConfig"] = uTelDataHeader.isAutoConfig != null ? uTelDataHeader.isAutoConfig : (object)DBNull.Value;
            dr["Carrier"] = uTelDataHeader.Carrier;
            dr["SignalStrength"] = uTelDataHeader.SignalStrength != null ? uTelDataHeader.SignalStrength : (object)DBNull.Value;

            AssetDeviceMap ADMap = new AssetDeviceMap();
            ADMap = new AssetDeviceMapService().GetAssetDeviceMapByDevice(uTelDataHeader.DeviceID.ToString(),sConnStr);
            dr["AssetID"] = ADMap.AssetID;
            uTelDataHeader.AssetID = ADMap.AssetID;

            Boolean bResult = false;
            DbTransaction tTrans;
            DbConnection _Conn = Connection.GetConnection( sConnStr);
            _Conn.Open();
            try
            {
                tTrans = _Conn.BeginTransaction();
                if (!isDuplicate(uTelDataHeader, tTrans,sConnStr))
                {
                    Int32 UID = -1;
                    Connection _connection = new Connection(sConnStr);

                    //Inserting completely new
                    UID = Convert.ToInt32(_connection.GetNextID("GFI_TEL_TelDataHeader", tTrans, sConnStr).ToString());
                    dt.Rows.Add(dr);

                    bResult = _connection.GenInsert(dt, tTrans, sConnStr);
                    if (bResult)
                        foreach (TelDataDetail t in uTelDataHeader.lTelDataDetail)
                        {
                            t.HeaderiID = UID;
                            bResult = bResult & new TelDataDetailService().SaveTelDataDetail(t, tTrans, sConnStr);
                        }

                    if (bResult)
                        tTrans.Commit();
                    else
                        tTrans.Rollback();
                }
            }
            catch { bResult = false; }
            finally
            {
                _Conn.Close();
            }
            return bResult;
        }
        public Boolean SaveTelDataHeader(TelDataHeader uTelDataHeader, Boolean bInsert, String sConnStr)
        {
            DataTable dt = TelDataHeaderDT();

            DataRow dr = dt.NewRow();
            dr["iID"] = uTelDataHeader.iID;
            dr["MsgHeader"] = uTelDataHeader.MsgHeader;
            dr["Type"] = uTelDataHeader.Type;
            dr["AssetID"] = uTelDataHeader.AssetID;
            dr["UTCdt"] = uTelDataHeader.UTCdt != null ? uTelDataHeader.UTCdt : (object)DBNull.Value;
            dr["MsgStatus"] = uTelDataHeader.MsgStatus;
            dr["isMaintenanceMode"] = uTelDataHeader.isMaintenanceMode != null ? uTelDataHeader.isMaintenanceMode : (object)DBNull.Value;
            dr["isFailedCallout"] = uTelDataHeader.isFailedCallout != null ? uTelDataHeader.isFailedCallout : (object)DBNull.Value;
            dr["Temperature"] = uTelDataHeader.Temperature != null ? uTelDataHeader.Temperature : (object)DBNull.Value;
            dr["isBatteryLow"] = uTelDataHeader.isBatteryLow != null ? uTelDataHeader.isBatteryLow : (object)DBNull.Value;
            dr["isAutoConfig"] = uTelDataHeader.isAutoConfig != null ? uTelDataHeader.isAutoConfig : (object)DBNull.Value;
            dr["Carrier"] = uTelDataHeader.Carrier;
            dr["SignalStrength"] = uTelDataHeader.SignalStrength != null ? uTelDataHeader.SignalStrength : (object)DBNull.Value;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "iID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uTelDataHeader.iID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }

        public Boolean DeleteTelDataHeader(TelDataHeader uTelDataHeader, String sConnStr)
        {
            Connection conn = new Connection(sConnStr);
            DataTable dtCtrl = conn.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_TEL_TelDataHeader where iID = " + uTelDataHeader.iID.ToString();
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            DataTable dtAudit = new AuditService().LogTran(3, "TelDataHeader", uTelDataHeader.iID.ToString(), Globals.uLogin.UID, uTelDataHeader.iID);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "CallerFunction";
            drCtrl["ParentTblName"] = "GFI_TEL_TelDataHeader";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = conn.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
    }
}
