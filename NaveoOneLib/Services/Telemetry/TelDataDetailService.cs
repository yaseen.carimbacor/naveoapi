using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Telemetry;
using NaveoOneLib.Services.Audits;

namespace NaveoOneLib.Services.Telemetry
{
    class TelDataDetailService
    {
        Errors er = new Errors();
        public DataTable GetTelDataDetail(String sConnStr)
        {
            String sqlString = @"SELECT iID, 
HeaderiID, 
UTCdt, 
Sensor, 
SensorReading from GFI_TEL_TelDataDetail order by iID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public TelDataDetail GetTelDataDetailById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT iID, 
HeaderiID, 
UTCdt, 
Sensor, 
SensorReading, SensorAddress, UOM from GFI_TEL_TelDataDetail
WHERE iID = ?
order by iID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID,sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                TelDataDetail retTelDataDetail = new TelDataDetail();
                retTelDataDetail.iID = (int)dt.Rows[0]["iID"];
                retTelDataDetail.HeaderiID = (int)dt.Rows[0]["HeaderiID"];
                retTelDataDetail.UTCdt = (DateTime)dt.Rows[0]["UTCdt"];
                retTelDataDetail.Sensor = (int)dt.Rows[0]["Sensor"];
                retTelDataDetail.SensorReading = (int)dt.Rows[0]["SensorReading"];
                retTelDataDetail.SensorAddress = String.IsNullOrEmpty(dt.Rows[0]["SensorAddress"].ToString()) ? (int?)null : (int)dt.Rows[0]["SensorAddress"];
                retTelDataDetail.UOM = String.IsNullOrEmpty(dt.Rows[0]["UOM"].ToString()) ? (int?)null : (int)dt.Rows[0]["UOM"];
                return retTelDataDetail;
            }
        }

        public Boolean SaveTelDataDetail(TelDataDetail uTelDataDetail, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_TEL_TelDataDetail";
            //dt.Columns.Add("iID", uTelDataDetail.iID.GetType());
            dt.Columns.Add("HeaderiID", uTelDataDetail.HeaderiID.GetType());
            dt.Columns.Add("UTCdt", uTelDataDetail.UTCdt.GetType());
            dt.Columns.Add("Sensor", uTelDataDetail.Sensor.GetType());
            dt.Columns.Add("SensorReading", uTelDataDetail.SensorReading.GetType());
            dt.Columns.Add("SensorAddress", typeof(int));
            dt.Columns.Add("UOM", typeof(int));

            DataRow dr = dt.NewRow();
            //dr["iID"] = uTelDataDetail.iID;
            dr["HeaderiID"] = uTelDataDetail.HeaderiID;
            dr["UTCdt"] = uTelDataDetail.UTCdt;
            dr["Sensor"] = uTelDataDetail.Sensor;
            dr["SensorReading"] = uTelDataDetail.SensorReading;
            dr["SensorAddress"] = uTelDataDetail.SensorAddress != null ? uTelDataDetail.SensorAddress : (object)DBNull.Value;
            dr["UOM"] = uTelDataDetail.UOM != null ? uTelDataDetail.UOM : (object)DBNull.Value;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenInsert(dt, transaction, sConnStr))
                return true;
            else
                return false;
        }
        public Boolean SaveTelDataDetail(TelDataDetail uTelDataDetail, Boolean bInsert, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_TEL_TelDataDetail";
            dt.Columns.Add("iID", uTelDataDetail.iID.GetType());
            dt.Columns.Add("HeaderiID", uTelDataDetail.HeaderiID.GetType());
            dt.Columns.Add("UTCdt", uTelDataDetail.UTCdt.GetType());
            dt.Columns.Add("Sensor", uTelDataDetail.Sensor.GetType());
            dt.Columns.Add("SensorReading", uTelDataDetail.SensorReading.GetType());
            
            DataRow dr = dt.NewRow();
            dr["iID"] = uTelDataDetail.iID;
            dr["HeaderiID"] = uTelDataDetail.HeaderiID;
            dr["UTCdt"] = uTelDataDetail.UTCdt;
            dr["Sensor"] = uTelDataDetail.Sensor;
            dr["SensorReading"] = uTelDataDetail.SensorReading;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "iID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uTelDataDetail.iID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }

        public Boolean DeleteTelDataDetail(TelDataDetail uTelDataDetail, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_TEL_TelDataDetail where iID = " + uTelDataDetail.iID.ToString();
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            DataTable dtAudit = new AuditService().LogTran(3, "TelDataDetail", uTelDataDetail.iID.ToString(), Globals.uLogin.UID, uTelDataDetail.iID);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "CallerFunction";
            drCtrl["ParentTblName"] = "GFI_TEL_TelDataDetail";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
    }
}
