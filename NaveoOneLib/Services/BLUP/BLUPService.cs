﻿using NaveoOneLib.DBCon;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Services.GMatrix;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NaveoOneLib.Common;
using NaveoOneLib.Models.BLUP;
using NaveoOneLib.WebSpecifics;

namespace NaveoOneLib.Services.BLUP
{
    public class BLUPService
    {
        public DataTable dtGetBlupSpatial(Guid sUserToken, String sConnStr)
        {
            List<Matrix> lm = new MatrixService().GetMatrixByUserToken(sUserToken, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @" ;with cte as
                            (
	                            SELECT z.APL_ID aplid, z.Point_X lon, z.POINT_Y lat, GeomData.ToString().MakeValid() GeomData
		                            --, Geometry::STPointFromText
			                        --    (
				                    --        'POINT(' + (CONVERT(varchar, Point_X, 128) + ' ' + CONVERT(varchar, POINT_Y, 128)) + ')' 
					                --            , 4326
			                        --    ).MakeValid() GeomData
	                            from GFI_GIS_BLUP z
                            )
                            select * from cte";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "BlupSpatial";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables["BlupSpatial"];
        }
        public DataTable dtGetBlupWithinDistanceInMeters(Guid sUserToken, int iDistance, List<int> lBlupID, String sConnStr)
        {
            lBlupID = lBlupID.Distinct().ToList();
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            int UID = new NaveoOneLib.Services.Users.UserService().GetUserID(sUserToken, sConnStr);
            String sql = new LibData().sqlValidatedData(UID, NaveoModels.BLUP, sConnStr);
            sql += @"
CREATE TABLE #Blup
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY CLUSTERED,
	[iBlupID] [int],
)
";
            foreach (int i in lBlupID)
                sql += "insert #Blup (iBlupID) values (" + i.ToString() + ");\r\n";

            sql += @"
;with cte as
(
	select b.* from GFI_GIS_BLUP b 
        inner join #Blup t on b.iID = t.iBlupID
)
select distinct b.iID, b.APL_ID, b.APL_NAME, b.APL_FNAME, b.cat, b.POINT_X lon, b.POINT_Y lat
    , c.GeomData.STDistance(b.GeomData) distance --, dbo.fnCalcDistanceInKMUsingSpatial(c.POINT_Y, c.POINT_X, b.POINT_Y, b.POINT_X) distance
    , b.GeomData.MakeValid().ToString() GeomData
	--, Geometry::STPointFromText
	--	(
	--		'POINT(' + (CONVERT(varchar, b.Point_X, 128) + ' ' + CONVERT(varchar, b.POINT_Y, 128)) + ')' 
	--			, 4326
	--	).MakeValid().ToString() GeomData
from cte c
	inner join GFI_GIS_BLUP b on c.iID = b.iID
    inner join #ValidatedData a on a.StrucID = b.iID
where c.GeomData.STDistance(b.GeomData) <= " + iDistance; //where dbo.fnCalcDistanceInKMUsingSpatial(c.POINT_Y, c.POINT_X, b.POINT_Y, b.POINT_X) <= " + iDistance;

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtResult";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables["dtResult"];
        }
        public DataTable dtGetBlupWithinDistanceInMeters(Guid sUserToken, int iDistance, Double lon, Double lat, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            int UID = new NaveoOneLib.Services.Users.UserService().GetUserID(sUserToken, sConnStr);
            String sql = new LibData().sqlValidatedData(UID, NaveoModels.BLUP, sConnStr);
            sql += @"
select distinct b.iID, b.APL_ID, b.APL_NAME, b.APL_FNAME, b.cat, b.POINT_X lon, b.POINT_Y lat, dbo.fnCalcDistanceInKMUsingSpatial(" + lat + ", " + lon + @", b.POINT_Y, b.POINT_X) distance, GeomData.MakeValid().ToString() GeomData
	--, Geometry::STPointFromText
	--	(
	--		'POINT(' + (CONVERT(varchar, b.Point_X, 128) + ' ' + CONVERT(varchar, b.POINT_Y, 128)) + ')' 
	--			, 4326
	--	).MakeValid().ToString() GeomData
from GFI_GIS_BLUP b
    inner join #ValidatedData a on a.StrucID = b.iID
where dbo.fnCalcDistanceInKMUsingSpatial(" + lat + ", " + lon + @", b.POINT_Y, b.POINT_X) <= " + iDistance;

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtResult";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables["dtResult"];
        }
        public DataTable dtGetBlupContextWithinDistanceInMeters(Guid sUserToken, int iDistance, Double lon, Double lat, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            int UID = new NaveoOneLib.Services.Users.UserService().GetUserID(sUserToken, sConnStr);
            String sql = new LibData().sqlValidatedData(UID, NaveoModels.BLUP, sConnStr);
            sql += @"
select b.iID, b.APL_ID, b.APL_NAME, b.APL_FNAME, b.cat, b.POINT_X lon, b.POINT_Y lat, dbo.fnCalcDistanceInKMUsingSpatial(" + lat + ", " + lon + @", b.POINT_Y, b.POINT_X) distance, GeomData.ToString().MakeValid() GeomData
	--, Geometry::STPointFromText
	--	(
	--		'POINT(' + (CONVERT(varchar, b.Point_X, 128) + ' ' + CONVERT(varchar, b.POINT_Y, 128)) + ')' 
	--			, 4326
	--	).MakeValid().ToString() GeomData
    , r.Assess_Code, r.Assess_Date, r.Assess_Recommend, r.Assess_Status
from GFI_GIS_BLUP b
    inner join #ValidatedData a on a.StrucID = b.iID
    inner join GFI_GIS_BLUPAssessment r on r.BLUPiID = b.iID
where dbo.fnCalcDistanceInKMUsingSpatial(" + lat + ", " + lon + @", b.POINT_Y, b.POINT_X) <= " + iDistance;

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtResult";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables["dtResult"];
        }


        #region BLUP 
        public DataTable dtTree()
        {
            DataTable dtFinal = new DataTable();
            dtFinal.Columns.Add("GMID", typeof(int));
            dtFinal.Columns.Add("GMDescription");
            dtFinal.Columns.Add("ParentGMID");
            dtFinal.Columns.Add("StrucID");
            dtFinal.Columns.Add("MyStatus");
            dtFinal.Columns.Add("MID");
            dtFinal.Columns.Add("MatriXID");

            DataRow row = dtFinal.NewRow();
            row["GMID"] = 1;
            row["GMDescription"] = "ALL PLANNINGS";
            row["ParentGMID"] = -1;
            row["StrucID"] = 0;
            row["MyStatus"] = "AC";
            row["MID"] = 0;
            row["MatriXID"] = 0;
            dtFinal.Rows.Add(row);

            DataRow row1 = dtFinal.NewRow();
            row1["GMID"] = 2;
            row1["GMDescription"] = "COMMERCIAL";
            row1["ParentGMID"] = 1;
            row1["StrucID"] = 0;
            row1["MyStatus"] = "AC";
            row1["MID"] = 0;
            row1["MatriXID"] = 0;
            dtFinal.Rows.Add(row1);

            DataRow row2 = dtFinal.NewRow();
            row2["GMID"] = 3;
            row2["GMDescription"] = "RESIDENTIAL";
            row2["ParentGMID"] = 1;
            row2["StrucID"] = 0;
            row2["MyStatus"] = "AC";
            row2["MID"] = 0;
            row2["MatriXID"] = 0;
            dtFinal.Rows.Add(row2);

            DataRow row3 = dtFinal.NewRow();
            row3["GMID"] = 4;
            row3["GMDescription"] = "MORCELLEMNT";
            row3["ParentGMID"] = 1;
            row3["StrucID"] = 0;
            row3["MyStatus"] = "AC";
            row3["MID"] = 0;
            row3["MatriXID"] = 0;
            dtFinal.Rows.Add(row3);

            DataRow row4 = dtFinal.NewRow();
            row4["GMID"] = 5;
            row4["GMDescription"] = "OPP";
            row4["ParentGMID"] = 1;
            row4["StrucID"] = 0;
            row4["MyStatus"] = "AC";
            row4["MID"] = 0;
            row4["MatriXID"] = 0;
            dtFinal.Rows.Add(row4);

            DataRow row5 = dtFinal.NewRow();
            row5["GMID"] = 6;
            row5["GMDescription"] = "SERV";
            row5["ParentGMID"] = 1;
            row5["StrucID"] = 0;
            row5["MyStatus"] = "AC";
            row5["MID"] = 0;
            row5["MatriXID"] = 0;
            dtFinal.Rows.Add(row5);

            DataRow row6 = dtFinal.NewRow();
            row6["GMID"] = 7;
            row6["GMDescription"] = "SG";
            row6["ParentGMID"] = 1;
            row6["StrucID"] = 0;
            row6["MyStatus"] = "AC";
            row6["MID"] = 0;
            row6["MatriXID"] = 0;
            dtFinal.Rows.Add(row6);

            DataRow row7 = dtFinal.NewRow();
            row7["GMID"] = 8;
            row7["GMDescription"] = "WALL";
            row7["ParentGMID"] = 1;
            row7["StrucID"] = 0;
            row7["MyStatus"] = "AC";
            row7["MID"] = 0;
            row7["MatriXID"] = 0;
            dtFinal.Rows.Add(row7);

            DataRow row8 = dtFinal.NewRow();
            row8["GMID"] = 9;
            row8["GMDescription"] = "INDUSTRIAL";
            row8["ParentGMID"] = 1;
            row8["StrucID"] = 0;
            row8["MyStatus"] = "AC";
            row8["MID"] = 0;
            row8["MatriXID"] = 0;
            dtFinal.Rows.Add(row8);

            //DataRow row9 = dtFinal.NewRow();
            //row9["GMID"] = "COMMERCIAL";
            //row9["GMDescription"] = 2;
            //row9["ParentGMID"] = -1;
            //row9["StrucID"] = 0;
            //row9["MyStatus"] = "AC";
            //row9["MID"] = 0;
            //row9["MatriXID"] = 0;

            return dtFinal;
        }
        DataTable BLUPDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_GIS_BLUP";
            dt.Columns.Add("iID", typeof(int));
            dt.Columns.Add("APL_ID", typeof(String));
            dt.Columns.Add("APL_NAME", typeof(String));
            dt.Columns.Add("APL_FNAME", typeof(String));
            dt.Columns.Add("APL_DATE", typeof(DateTime));
            dt.Columns.Add("APL_EFF_DATE", typeof(DateTime));
            dt.Columns.Add("APL_ADDR1", typeof(String));
            dt.Columns.Add("APL_ADDR2", typeof(String));
            dt.Columns.Add("APL_ADDR3", typeof(String));

            dt.Columns.Add("APL_LOC_PLAN", typeof(String));
            dt.Columns.Add("APL_TV_NO", typeof(String));
            dt.Columns.Add("APL_PROP_DEVP", typeof(String));
            dt.Columns.Add("APL_EXTENT", typeof(float));
            dt.Columns.Add("TYPE", typeof(int));
            dt.Columns.Add("POINT_X", typeof(float));
            dt.Columns.Add("POINT_Y", typeof(float));
            dt.Columns.Add("Status", typeof(String));
            dt.Columns.Add("Cat", typeof(int));
            dt.Columns.Add("OprType", typeof(DataRowState)); //ApprovalID

            dt.Columns.Add("GeomData", typeof(String));
            return dt;
        }
        public BaseModel SaveBLUP(NaveoOneLib.Models.BLUP.BLUP blup, int uLogin, Boolean bInsert, String sConnStr)
        {
            BaseModel baseModel = new BaseModel();
            Boolean bResult = false;
            baseModel.data = bResult;

            if (blup.POINT_X == 0)
                return baseModel;

            if (blup.POINT_Y == 0)
                return baseModel;

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            DataSet dsProcess = new DataSet();

            //GFI_GIS_BLUP
            DataTable dt = BLUPDT();

            DataRow dr = dt.NewRow();
            dr["iID"] = !blup.iID.HasValue ? (object)DBNull.Value : blup.iID;
            dr["APL_ID"] = String.IsNullOrEmpty(blup.APL_ID) ? (object)DBNull.Value : blup.APL_ID;
            dr["APL_NAME"] = String.IsNullOrEmpty(blup.APL_NAME) ? (object)DBNull.Value : blup.APL_NAME;
            dr["APL_FNAME"] = String.IsNullOrEmpty(blup.APL_FNAME) ? (object)DBNull.Value : blup.APL_FNAME;
            dr["APL_DATE"] = blup.APL_DATE.Year == 1 ? (object)DBNull.Value : blup.APL_DATE;
            dr["APL_EFF_DATE"] = blup.APL_EFF_DATE.Year == 1 ? (object)DBNull.Value : blup.APL_EFF_DATE;
            dr["APL_ADDR1"] = String.IsNullOrEmpty(blup.APL_ADDR1) ? (object)DBNull.Value : blup.APL_ADDR1;
            dr["APL_ADDR2"] = String.IsNullOrEmpty(blup.APL_ADDR2) ? (object)DBNull.Value : blup.APL_ADDR2;
            dr["APL_ADDR3"] = String.IsNullOrEmpty(blup.APL_ADDR3) ? (object)DBNull.Value : blup.APL_ADDR3;
            dr["APL_LOC_PLAN"] = String.IsNullOrEmpty(blup.APL_LOC_PLAN) ? (object)DBNull.Value : blup.APL_LOC_PLAN;
            dr["APL_TV_NO"] = String.IsNullOrEmpty(blup.APL_TV_NO) ? (object)DBNull.Value : blup.APL_TV_NO;
            dr["APL_PROP_DEVP"] = String.IsNullOrEmpty(blup.APL_PROP_DEVP) ? (object)DBNull.Value : blup.APL_PROP_DEVP;
            dr["APL_EXTENT"] = String.IsNullOrEmpty(blup.APL_PROP_DEVP) ? (object)DBNull.Value : blup.APL_EXTENT;
            dr["TYPE"] = String.IsNullOrEmpty(blup.TYPE.ToString()) ? (object)DBNull.Value : blup.TYPE;
            dr["POINT_X"] = Convert.ToDouble(blup.POINT_X);
            dr["POINT_Y"] = Convert.ToDouble(blup.POINT_Y);
            dr["Status"] = String.IsNullOrEmpty(blup.Status) ? (object)DBNull.Value : blup.Status;
            dr["GeomData"] = blup.GeomData != null ? blup.GeomData : (object)DBNull.Value;
            //update GFI_GIS_BLUP set GeomData = geometry::STGeomFromText('POINT ('+ cast(POINT_X as varchar(max)) +' ' + cast(POINT_Y as varchar(max))+')', 4326)

            if (blup.TYPE == 1)    //Blup imported
            {
                if (String.IsNullOrEmpty(blup.APL_ID))
                    dr["Cat"] = 1;
                else
                {
                    //from NaveoOneLib.Services.GMatrix.sBlupParentMatrix
                    if (blup.APL_ID.StartsWith("CO")) dr["Cat"] = 2;
                    else if (blup.APL_ID.StartsWith("RE")) dr["Cat"] = 3;
                    else if (blup.APL_ID.StartsWith("MO")) dr["Cat"] = 4;
                    else if (blup.APL_ID.StartsWith("BL")) dr["Cat"] = 4;
                    else if (blup.APL_ID.StartsWith("OP")) dr["Cat"] = 5;
                    else if (blup.APL_ID.StartsWith("SE")) dr["Cat"] = 6;
                    else if (blup.APL_ID.StartsWith("SG")) dr["Cat"] = 7;
                    else if (blup.APL_ID.StartsWith("WA")) dr["Cat"] = 8;
                    else if (blup.APL_ID.StartsWith("IN")) dr["Cat"] = 9;
                    else if (blup.APL_ID.StartsWith("EIA/PER")) dr["Cat"] = 10;
                    else if (blup.APL_ID.StartsWith("Morcellement Board")) dr["Cat"] = 11;
                    else if (blup.APL_ID.StartsWith("Land Conversion")) dr["Cat"] = 12;
                    else dr["Cat"] = blup.Cat;
                }
            }
            else //Nels
                dr["Cat"] = blup.Cat;

            if (bInsert)
                blup.OprType = DataRowState.Added;
            dr["oprType"] = blup.OprType;
            dt.Rows.Add(dr);

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "iID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = blup.iID;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dt);

            if (bInsert)
                if (blup.lMatrix.Count == 0)
                {
                    blup.lMatrix = new NaveoOneLib.Services.GMatrix.MatrixService().GetMatrixByUserID(uLogin, sConnStr);
                    foreach (Matrix m in blup.lMatrix)
                        m.oprType = DataRowState.Added;
                }
            DataTable dtMatrix = new myConverter().ListToDataTable<Matrix>(blup.lMatrix);
            dtMatrix.TableName = "GFI_SYS_GroupMatrixBLUP";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 2;
            drCtrl["TblName"] = dtMatrix.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "MID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "iID";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtMatrix);

            //GFI_GIS_BLUPAssessment
            if (blup.lBlupAssessment.Count > 0)
            {
                DataTable dtBLUPAssessment = new myConverter().ListToDataTable<BLUPAssessment>(blup.lBlupAssessment);
                dtBLUPAssessment.TableName = "GFI_GIS_BLUPAssessment";
                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 3;
                drCtrl["TblName"] = dtBLUPAssessment.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "UID";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdAction"] = "SET";
                drCtrl["NextIdFieldName"] = "BLUPiID";
                drCtrl["ParentTblName"] = dt.TableName;
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtBLUPAssessment);
            }

            //GFI_GIS_BLUPDetails
            if (blup.lBLUPDetails.Count > 0)
            {
                DataTable dtBLUPDetails = new myConverter().ListToDataTable<BLUPDetails>(blup.lBLUPDetails);
                dtBLUPDetails.TableName = "GFI_GIS_BLUPDetails";
                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 4;
                drCtrl["TblName"] = dtBLUPDetails.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "UID";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdAction"] = "SET";
                drCtrl["NextIdFieldName"] = "BLUPiID";
                drCtrl["ParentTblName"] = dt.TableName;
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtBLUPDetails);
            }

            dsProcess.Merge(dtCtrl);
            DataSet dsResults = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResults.Tables["CTRLTBL"];

            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                {
                    baseModel.dbResult = dsResults;
                    baseModel.dbResult.Tables["CTRLTBL"].TableName = "ResultCtrlTbl";
                    baseModel.dbResult.Merge(dsProcess);
                    bResult = false;
                }
                else
                    bResult = true;
            }
            else
                bResult = false;

            baseModel.data = bResult;
            baseModel.additionalData = ""; // dtCtrl.Rows[2].Field <string> ("KeyFieldName");
            return baseModel;
        }

        NaveoOneLib.Models.BLUP.BLUP GETBLUP(DataSet ds)
        {
            if (ds == null || ds.Tables["GFI_GIS_BLUP"].Rows.Count == 0)
                return null;

            DataRow dr = ds.Tables["GFI_GIS_BLUP"].Rows[0];
            NaveoOneLib.Models.BLUP.BLUP retBLUP = new NaveoOneLib.Models.BLUP.BLUP();
            retBLUP.iID = Convert.ToInt32(dr["iID"]);
            retBLUP.APL_ID = String.IsNullOrEmpty(dr["APL_ID"].ToString()) ? string.Empty : dr["APL_ID"].ToString();
            retBLUP.APL_NAME = dr["APL_NAME"].ToString();
            retBLUP.APL_FNAME = dr["APL_FNAME"].ToString();
            retBLUP.APL_DATE = Convert.ToDateTime(dr["APL_DATE"].ToString());
            retBLUP.APL_EFF_DATE = Convert.ToDateTime(dr["APL_EFF_DATE"].ToString());
            retBLUP.APL_ADDR1 = dr["APL_ADDR1"].ToString();
            retBLUP.APL_ADDR2 = dr["APL_ADDR2"].ToString();
            retBLUP.APL_ADDR3 = dr["APL_ADDR3"].ToString();
            retBLUP.APL_LOC_PLAN = String.IsNullOrEmpty(dr["APL_LOC_PLAN"].ToString()) ? string.Empty : dr["APL_LOC_PLAN"].ToString();
            retBLUP.APL_TV_NO = String.IsNullOrEmpty(dr["APL_TV_NO"].ToString()) ? string.Empty : dr["APL_TV_NO"].ToString();
            retBLUP.APL_PROP_DEVP = String.IsNullOrEmpty(dr["APL_PROP_DEVP"].ToString()) ? string.Empty : dr["APL_PROP_DEVP"].ToString();
            retBLUP.APL_EXTENT = String.IsNullOrEmpty(dr["APL_EXTENT"].ToString()) ? 0 : Convert.ToInt32(dr["APL_EXTENT"]);
            retBLUP.TYPE = Convert.ToInt32(dr["TYPE"]);
            retBLUP.POINT_X = String.IsNullOrEmpty(dr["POINT_X"].ToString()) ? 0.0 : Convert.ToDouble(dr["POINT_X"]);
            retBLUP.POINT_Y = String.IsNullOrEmpty(dr["POINT_Y"].ToString()) ? 0.0 : Convert.ToDouble(dr["POINT_Y"]);
            retBLUP.Status = dr["Status"].ToString();
            retBLUP.Cat = Convert.ToInt32(dr["Cat"]);
            retBLUP.GeomData = String.IsNullOrEmpty(dr["GeomData"].ToString()) ? null : dr["GeomData"].ToString();

            List<Matrix> lMatrix = new List<Matrix>();
            if (ds.Tables.Contains("dtMatrix"))
            {
                MatrixService ms = new MatrixService();
                foreach (DataRow dri in ds.Tables["dtMatrix"].Rows)
                    lMatrix.Add(ms.AssignMatrix(dri));
            }
            retBLUP.lMatrix = lMatrix;

            List<BLUPAssessment> lBLUPAssessment = new List<BLUPAssessment>();
            if (ds.Tables.Contains("GFI_GIS_BLUPAssessment"))
            {
                foreach (DataRow dri in ds.Tables["GFI_GIS_BLUPAssessment"].Rows)
                {
                    BLUPAssessment BLA = new BLUPAssessment();
                    BLA.UID = Convert.ToInt32(dri["UID"]);
                    BLA.BLUPiID = Convert.ToInt32(dri["BLUPiID"]);
                    BLA.ASSESS_Code = dri["ASSESS_Code"].ToString();
                    BLA.ASSESS_Detail = dri["ASSESS_Detail"].ToString();
                    BLA.ASSESS_Date = Convert.ToDateTime(dri["ASSESS_Date"].ToString());
                    BLA.ASSESS_Recommend = dri["ASSESS_Recommend"].ToString();
                    BLA.ASSESS_Status = dri["ASSESS_Status"].ToString();
                    BLA.CreatedBy = dri["CreatedBy"].ToString();
                    BLA.CreatedDate = Convert.ToDateTime(dri["CreatedDate"].ToString());
                    BLA.ASSESS_SEQ = Convert.ToInt32(dri["ASSESS_SEQ"]);

                    lBLUPAssessment.Add(BLA);
                }
                retBLUP.lBlupAssessment = lBLUPAssessment;
            }

            List<BLUPDetails> lBLUPDetails = new List<BLUPDetails>();
            if (ds.Tables.Contains("GFI_GIS_BLUPDetails"))
            {
                foreach (DataRow dri in ds.Tables["GFI_GIS_BLUPDetails"].Rows)
                {
                    BLUPDetails BLD = new BLUPDetails();
                    BLD.UID = Convert.ToInt32(dri["UID"]);
                    BLD.BLUPiID = Convert.ToInt32(dri["BLUPiID"]);
                    BLD.BCM_CDATE = Convert.ToDateTime(dri["BCM_CDATE"].ToString());
                    BLD.BCM_CMT = dri["BCM_CMT"].ToString();
                    BLD.REF_DESC = dri["REF_DESC"].ToString();
                    BLD.BCM_STATUS = dri["BCM_STATUS"].ToString();
                    BLD.BCM_DECISION = dri["BCM_DECISION"].ToString();
                    BLD.BCM_SEQ = Convert.ToInt32(dri["BCM_SEQ"]);

                    lBLUPDetails.Add(BLD);
                }

                retBLUP.lBLUPDetails = lBLUPDetails;
            }
            return retBLUP;
        }
        public DataSet GetBlupByAplID(String AplId, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"SELECT iID from GFI_GIS_BLUP WHERE APL_ID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = AplId;
            drSql["sTableName"] = "GFI_GIS_BLUP";
            dtSql.Rows.Add(drSql);
            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            int iID = -1;
            if (ds.Tables[0].Rows.Count > 0)
                if (int.TryParse(ds.Tables[0].Rows[0][0].ToString(), out iID))
                    return GetBlupByiID(iID, sConnStr);
            return null;
        }
        public NaveoOneLib.Models.BLUP.BLUP GetBlupByAPL_ID(String AplId, String sConnStr)
        {
            DataSet ds = GetBlupByAplID(AplId, sConnStr);
            return GETBLUP(ds);
        }
        public Boolean AplIDExists(String AplId, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"SELECT iID from GFI_GIS_BLUP WHERE APL_ID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = AplId;
            drSql["sTableName"] = "GFI_GIS_BLUP";
            dtSql.Rows.Add(drSql);
            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables["GFI_GIS_BLUP"].Rows.Count > 0)
                return true;

            return false;
        }

        public DataSet GetBlupByiID(int iID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"SELECT iID, APL_ID, APL_NAME, APL_FNAME, APL_DATE, APL_EFF_DATE, APL_ADDR1, APL_ADDR2, APL_ADDR3, APL_LOC_PLAN, APL_TV_NO, APL_PROP_DEVP, APL_EXTENT, TYPE, POINT_X, POINT_Y, Status, Cat, GeomData.ToString() as GeomData from GFI_GIS_BLUP WHERE iID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = iID;
            drSql["sTableName"] = "GFI_GIS_BLUP";
            dtSql.Rows.Add(drSql);

            sql = new MatrixService().sGetMatrixId(iID, "GFI_SYS_GroupMatrixBLUP");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);

            sql = @"SELECT * from GFI_GIS_BLUPAssessment WHERE BLUPiID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = iID;
            drSql["sTableName"] = "GFI_GIS_BLUPAssessment";
            dtSql.Rows.Add(drSql);

            sql = @"SELECT * from GFI_GIS_BLUPDetails WHERE BLUPiID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = iID;
            drSql["sTableName"] = "GFI_GIS_BLUPDetails";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds;
        }
        #endregion
    }
}
