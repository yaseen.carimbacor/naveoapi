﻿using NaveoOneLib.DBCon;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Services.GMatrix;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoOneLib.Services.Audits
{
    public class AuditTraceService
    {
        public DataTable GetAuditData(DateTime dtFrom, DateTime dtTo, List<Matrix> lMatrix, String sConnStr, List<String> sortColumns, Boolean sortOrderAsc, List<int> userIds, Boolean filter = false, String type = "")
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            return GetAuditData(dtFrom, dtTo, iParentIDs, sConnStr, sortColumns, sortOrderAsc, userIds, filter, type);
        }
        public DataTable GetAuditData(DateTime dtFrom, DateTime dtTo, List<int> iParentIDs, String sConnStr, List<String> sortColumns, Boolean sortOrderAsc, List<int> userIds, Boolean filter = false, String type = "")
        {
            DataTable retTable = new DataTable();
            DataTable dt = new DataTable();
            DataTable dtLogin = new DataTable();

            if (type != "login") { 
                 dt = new Connection(sConnStr).GetDataDT(
                    filter ? sGetFilteredAuditData(dtFrom, dtTo, userIds, sConnStr, sortColumns, sortOrderAsc, type) : sGetAuditData(dtFrom, dtTo, iParentIDs, sConnStr, sortColumns, sortOrderAsc), 
                    sConnStr
                );
            }
           
            if(filter && (type == "login" || type == "view"))
            {
                dtLogin = new Connection(sConnStr).GetDataDT(
                    sGetFilteredLoginAuditData(dtFrom, dtTo, userIds, sConnStr, sortColumns, sortOrderAsc), 
                    sConnStr);

                foreach(DataRow row in dtLogin.Rows)
                {
                    String value = (row[1].ToString());
                    String newValue;
                    if (row[4].ToString() == "1")
                    {
                         newValue = value + "(Successful)";
                    }
                    else
                    {
                        newValue = value + "(Failed)";
                        row.ItemArray.SetValue(value + "(Failed)", 1);
                    }
                    row[1] = newValue;
                }
                dtLogin.Columns.Remove("IsAccessGranted");

                string nm1 = "Module";
                if (dt.Columns.Contains("module")) nm1 = dt.Columns["module"].ColumnName;
                System.Data.DataColumn Module = new System.Data.DataColumn(nm1, typeof(System.String));
                Module.DefaultValue = "Login";
                dtLogin.Columns.Add(Module);

                string nm2 = "RawRequest";
                if (dt.Columns.Contains("rawrequest")) nm2 = dt.Columns["rawrequest"].ColumnName;
                System.Data.DataColumn RawRequest = new System.Data.DataColumn(nm2, typeof(System.String));
                RawRequest.DefaultValue = "{}";
                dtLogin.Columns.Add(RawRequest);



            }

            if (type == "view")
            {
                retTable.Merge(dt);
                retTable.Merge(dtLogin);
            }
            else if (type == "login")
            {
                retTable.Merge(dtLogin);
            }
            else 
            {
                retTable.Merge(dt);
            }

            return retTable;
        }

        public DataTable GetDrilledAuditById(string controller, int id, string sConnStr, int limit = 5)
        {
            String sql = @"
SELECT * FROM (
            SELECT 
            ROW_NUMBER() OVER (ORDER BY AuditTraceId DESC) as row, 
            AuditTraceId,
            RawRequest,
            u.Names ,
	        auditTrace.Controller as Module,
	        auditTrace.Action ,
	        auditTrace.AccessedOn from GFI_SYS_AuditTrace auditTrace
            inner join GFI_SYS_USER u on auditTrace.UserToken = u.UserToken
            WHERE Controller = '" + controller + "'" + (id > -1 ? " and sID=" + id + " " : "") + @"
) a WHERE row <= " + limit;
            DataTable dt = new Connection(sConnStr).GetDataDT(sql, sConnStr);
            return dt;
        }

        public String sGetAuditData(DateTime dtFrom, DateTime dtTo, List<int> iParentIDs, String sConnStr, List<String> sortColumns, Boolean sortOrderAsc)
        {

            #region sort
            String sSort = "AccessedOn";
            if (sortColumns != null && sortColumns.Count > 0)
            {
                List<String> lCols = new List<string>() { "AuditTraceId", "Names,", "Module", "Action"};
                String ss = String.Join(",", sortColumns
                    .Where(s => !String.IsNullOrWhiteSpace(s))
                    .Where(s => lCols.Contains(s))
                    .Select(s => s));

                if (!String.IsNullOrEmpty(ss))
                    sSort = ss;
            }

            if (sortOrderAsc)
                sSort += " asc";
            else
                sSort += " desc";
            #endregion


            String sql = @"
       select AuditTraceId,
       RawRequest,
       u.Names ,
	   auditTrace.Controller as Module,
	   auditTrace.Action ,
	   auditTrace.AccessedOn from GFI_SYS_AuditTrace auditTrace
       inner join GFI_SYS_USER u on auditTrace.UserToken = u.UserToken
       inner join GFI_SYS_GroupMatrixUser groupMatrixUser  on u.UID = groupMatrixUser.iID
       where groupMatrixUser.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + @") 
       and auditTrace.AccessedOn >= '" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "' and auditTrace.AccessedOn <= '" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff") + @"' 
       order by " + sSort + @"";


            return sql;
        }

        public String sGetFilteredAuditData(DateTime dtFrom, DateTime dtTo, List<int> userIds, String sConnStr, List<String> sortColumns, Boolean sortOrderAsc, String type)
        {

            #region sort
            String sSort = "AccessedOn";
            if (sortColumns != null && sortColumns.Count > 0)
            {
                List<String> lCols = new List<string>() { "AuditTraceId", "Names,", "Module", "Action" };
                String ss = String.Join(",", sortColumns
                    .Where(s => !String.IsNullOrWhiteSpace(s))
                    .Where(s => lCols.Contains(s))
                    .Select(s => s));

                if (!String.IsNullOrEmpty(ss))
                    sSort = ss;
            }

            if (sortOrderAsc)
                sSort += " asc";
            else
                sSort += " desc";
            #endregion

            String userFilter = userIds.Count() > 0 ? " auditTrace.UserToken in ('" + string.Join("', '", new Users.UserService().GetUserTokensById(userIds, sConnStr)) + "')" :"";
            String dateFilter = userIds.Count() > 0 ? " and " : "";
            dateFilter += dtFrom == DateTime.MinValue && dtTo == DateTime.MinValue ? "" : "auditTrace.AccessedOn >= '" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "' and auditTrace.AccessedOn <= '" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff") + @"'";

            String typeFilter = dtFrom == DateTime.MinValue && dtTo == DateTime.MinValue ? "" : " and ";
            if (type.ToLower() == "view") type = "";
            else if (type.ToLower() == "get") type = "save%' OR LOWER(Action) like '%update%' OR LOWER(Action) like '%delete";
            typeFilter += "LOWER(Action) like '%" + type + "%'";

            String sql = @"
       select AuditTraceId,
       sID,
        RawRequest,
       u.Names ,
	   auditTrace.Controller as Module,
	   auditTrace.Action ,
	   auditTrace.AccessedOn from GFI_SYS_AuditTrace auditTrace
       inner join GFI_SYS_USER u on auditTrace.UserToken = u.UserToken
       WHERE " + userFilter + @"
       " + dateFilter + @" 
       " + typeFilter + @" 
       order by " + sSort ;


            return sql;
        }

        public String sGetFilteredLoginAuditData(DateTime dtFrom, DateTime dtTo, List<int> userIds, String sConnStr, List<String> sortColumns, Boolean sortOrderAsc)
        {

            #region sort
            String sSort = "AccessedOn";
            if (sortColumns != null && sortColumns.Count > 0)
            {
                List<String> lCols = new List<string>() { "AuditTraceId", "Names,", "Module", "Action" };
                String ss = String.Join(",", sortColumns
                    .Where(s => !String.IsNullOrWhiteSpace(s))
                    .Where(s => lCols.Contains(s))
                    .Select(s => s));

                if (!String.IsNullOrEmpty(ss))
                    sSort = ss;
            }

            if (sortOrderAsc)
                sSort += " asc";
            else
                sSort += " desc";
            #endregion

            String userFilter = userIds.Count() > 0 ? " u.UserToken in ('" + string.Join("', '", new Users.UserService().GetUserTokensById(userIds, sConnStr)) + "')" : "";
            String dateFilter = userIds.Count() > 0 ? " and " : "";
            dateFilter += dtFrom == DateTime.MinValue && dtTo == DateTime.MinValue ? "" : "auditTrace.AccessedOn >= '" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "' and auditTrace.AccessedOn <= '" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff") + @"'";


            String sql = @"
       select AuditTraceLoginId as AuditTraceId,
        sAction as Action,
       u.Names as Names,
	   auditTrace.AccessedOn,
        auditTrace.IsAccessGranted from GFI_SYS_AuditTraceLogin auditTrace
       inner join GFI_SYS_USER u on auditTrace.Email = u.Email
       WHERE " + userFilter + @"
       " + dateFilter + @" 
       order by " + sSort;


            return sql;
        }

    }
}
