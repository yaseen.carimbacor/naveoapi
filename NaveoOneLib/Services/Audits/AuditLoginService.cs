﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Forms;
using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using NaveoOneLib.Models.Audits;

namespace NaveoOneLib.Services.Audits
{
    public class AuditLoginService
    {
        Errors er = new Errors();
        public bool SaveCoreLoginAudit(DataTable loginAudit, bool bInsert, String sConnStr)
        {
            DataTable dt = new DataTable();
            AuditLogin auditLogin = new AuditLogin();
            if (!dt.Columns.Contains("oprType"))
                dt.Columns.Add("oprType", typeof(DataRowState));
            dt.TableName = "GFI_SYS_LoginAudit";
            dt.Columns.Add("AuditId", auditLogin.AuditId.GetType());
            dt.Columns.Add("AuditUser", auditLogin.AuditUser.GetType());
            dt.Columns.Add("AuditDate", auditLogin.AuditDate.GetType());
            dt.Columns.Add("AuditType", auditLogin.AuditType.GetType());
            dt.Columns.Add("MachineName", auditLogin.MachineName.GetType());
            dt.Columns.Add("clientGroupMachine", auditLogin.ClientGroupName.GetType());
            dt.Columns.Add("PostedDate", auditLogin.PostedDate.GetType());
            dt.Columns.Add("PostedFlag", auditLogin.PostedFlag.GetType());


            foreach (DataRow dri in loginAudit.Rows)
            {
                DataRow dr = dt.NewRow();


                if (bInsert)
                    dr["oprType"] = DataRowState.Added;
                else
                    dr["oprType"] = DataRowState.Modified;

                dr["AuditId"] = dri["AuditId"];
                dr["AuditUser"] = dri["AuditUser"];
                dr["AuditDate"] = dri["AuditDate"];
                dr["AuditType"] = dri["AuditType"];
                dr["MachineName"] = dri["MachineName"];
                dr["clientGroupMachine"] = dri["clientGroupMachine"];
                dr["PostedDate"] = DateTime.Now; ;
                dr["PostedFlag"] = "Y";
                dt.Rows.Add(dr);
            }

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditId";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            //else
            //    drCtrl["NextIdValue"] = auditLogin.AuditId;

            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0 || dri["oprType"].ToString() == "0")
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);

            DataSet DS = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = DS.Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                {
                    bResult = false;

                }
                else
                {
                    UpdateAuditLoginSync(loginAudit);
                    bResult = true;
                }

            }
            else
                bResult = false;

            return bResult;
        }

        public bool SaveAuditLoginService(AuditLogin auditLogin)
        {
            return SQLiteService.SaveAuditLoginService(auditLogin);
        }
        public AuditLogin GetLastLoginInfo(string loginType, string username)
        {
            return SQLiteService.GetLastLoginInfo(loginType, username);
        }
        public DataTable GetLastLoginInfo(string username)
        {
            return SQLiteService.GetLastLoginInfo(username);
        }
        public DataTable GetSynLoginAudit()
        {
            return SQLiteService.GetSynLoginAudit();
        }
        public bool UpdateAuditLoginSync(DataTable auditLogin)
        {
            return SQLiteService.UpdateAuditLoginSync(auditLogin);
        }
    }
}
