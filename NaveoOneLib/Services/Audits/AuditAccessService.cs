﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Forms;
using NaveoOneLib.Common;
using NaveoOneLib.Models;
using System.Data;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models.Audits;

namespace NaveoOneLib.Services.Audits
{
    class AuditAccessService
    {
        Errors er = new Errors();
        public DataTable GetSynAccessAudit()
        {
            return SQLiteService.GetSynAccessAudit();
        }

        public bool SaveCoreAccessAudit(DataTable tableAudit, bool bInsert, String sConnStr)
        {
            DataTable dt = new DataTable();
            AuditAccess auditAccess = new AuditAccess();

            if (!dt.Columns.Contains("oprType"))
                dt.Columns.Add("oprType", typeof(DataRowState));

            dt.TableName = "GFI_SYS_AuditAccess";
            dt.Columns.Add("Iid", auditAccess.IId.GetType());
            dt.Columns.Add("AuditUser", auditAccess.AuditUser.GetType());
            dt.Columns.Add("Control", auditAccess.Control.GetType());
            dt.Columns.Add("AuditDate", auditAccess.AuditDate.GetType());
            dt.Columns.Add("PostedDate", auditAccess.PostedDate.GetType());
            dt.Columns.Add("PostedFlag", auditAccess.PostedFlag.GetType());

            foreach (DataRow dri in tableAudit.Rows)
            {
                DataRow dr = dt.NewRow();

                if (bInsert)
                    dr["oprType"] = DataRowState.Added;
                else
                    dr["oprType"] = DataRowState.Modified;

                dr["Iid"] = dri["Iid"];
                dr["AuditUser"] = dri["AuditUser"];
                dr["Control"] = dri["Control"];
                dr["AuditDate"] = dri["AuditDate"];
                dr["PostedDate"] = DateTime.Now; ;
                dr["PostedFlag"] = "Y";
                dt.Rows.Add(dr);
            }

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "Iid";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            //else
            //    drCtrl["NextIdValue"] = auditLogin.AuditId;

            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0 || dri["oprType"].ToString() == "0")
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);

            DataSet DS = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = DS.Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                {
                    bResult = true;
                    UpdateAccessTableSync(tableAudit);
                }
            }
            else
                bResult = false;

            return bResult;
        }

        public bool UpdateAccessTableSync(DataTable auditAccess)
        {
            return SQLiteService.UpdateAccessTableSync(auditAccess);
        }

        public bool PurgeLocalDb()
        {
            return SQLiteService.PurgeLocalDb();
        }
    }
}
