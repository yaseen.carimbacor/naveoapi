﻿using System;
using System.Collections.Generic;
using System.Linq;
using NaveoOneLib.Common;
using System.Data;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.Zones;
using NaveoOneLib.Models.Audits;
using NaveoOneLib.Models.Drivers;
using NaveoOneLib.Models.Users;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Models.Trips;

namespace NaveoOneLib.Services.Audits
{
    public class AuditTableService
    {
        Errors er = new Errors();
        AuditTableService auditTableService = new AuditTableService();
        public bool SaveAuditTableService(AuditTable auditTable)
        {
            return SQLiteService.SaveAuditTableService(auditTable);
        }

        public DataTable GetSynTableAudit()
        {
            return SQLiteService.GetSynTableAudit();
        }

        public bool SaveCoreTableAudit(DataTable tableAudit, bool bInsert, String sConnStr)
        {
            DataTable dt = new DataTable();
            AuditTable auditTable = new AuditTable();
            if (!dt.Columns.Contains("oprType"))
                dt.Columns.Add("oprType", typeof(DataRowState));

            dt.TableName = "GFI_SYS_Audit";
            dt.Columns.Add("AuditId", auditTable.AuditId.GetType());
            dt.Columns.Add("AuditUser", auditTable.AuditUser.GetType());
            dt.Columns.Add("AuditDate", auditTable.AuditDate.GetType());
            dt.Columns.Add("AuditType", auditTable.AuditType.GetType());
            dt.Columns.Add("ReferenceCode", auditTable.AuditRefCode.GetType());
            dt.Columns.Add("ReferenceDesc", auditTable.AuditRefDesc.GetType());
            dt.Columns.Add("CreatedDate", auditTable.AuditDate.GetType());
            dt.Columns.Add("CallerFunction", auditTable.CallerFunction.GetType());
            dt.Columns.Add("SQLRemarks", auditTable.SqlRemarks.GetType());
            dt.Columns.Add("NewValue", auditTable.OldValue.GetType());
            dt.Columns.Add("OldValue", auditTable.NewValue.GetType());
            dt.Columns.Add("PostedDate", auditTable.PostedDate.GetType());
            dt.Columns.Add("PostedFlag", auditTable.PostedFlag.GetType());

            foreach (DataRow dri in tableAudit.Rows)
            {
                DataRow dr = dt.NewRow();

                if (bInsert)
                    dr["oprType"] = DataRowState.Added;
                else
                    dr["oprType"] = DataRowState.Modified;

                dr["AuditId"] = dri["AuditId"];
                dr["AuditUser"] = dri["AuditUser"];
                dr["AuditDate"] = dri["CreatedDate"];
                dr["AuditType"] = dri["AuditType"];
                dr["ReferenceCode"] = dri["ReferenceCode"];
                dr["ReferenceDesc"] = dri["ReferenceDesc"];
                dr["CallerFunction"] = dri["CallerFunction"];
                dr["SQLRemarks"] = dri["SQLRemarks"];
                dr["CreatedDate"] = dri["CreatedDate"];
                dr["NewValue"] = dri["NewValue"];
                dr["OldValue"] = dri["OldValue"];
                dr["PostedDate"] = DateTime.Now; ;
                dr["PostedFlag"] = "Y";
                dt.Rows.Add(dr);
            }

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditId";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            //else
            //    drCtrl["NextIdValue"] = auditLogin.AuditId;

            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0 || dri["oprType"].ToString() == "0")
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);

            DataSet DS = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = DS.Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                {
                    bResult = true;
                    UpdateAuditTableSync(tableAudit);
                }
            }
            else
                bResult = false;

            return bResult;
        }

        public bool UpdateAuditTableSync(DataTable auditLogin)
        {
            return SQLiteService.UpdateAuditTableSync(auditLogin);
        }

        public bool CreateLocalAudits()
        {
            return SQLiteService.CreateLocalAudits();
        }

        #region AuditZoneType
        public void AuditZoneType(ZoneType zp, ZoneType zpClone, string Type)
        {
            AuditTable auditTable = new AuditTable();
            auditTable.AuditRefCode = zp.ZoneTypeID.ToString();
            auditTable.AuditRefTable = "GFI_FLT_ZoneType";
            auditTable.AuditType = Type;

            if (Type == "I" || Type == "D")
            {
                auditTable.AuditRefDesc = zp.sDescription;
                auditTableService.SaveAuditTableService(auditTable);
                return;

            }

            if (!zp.sComments.Equals(zpClone.sComments))
            {
                auditTable.OldValue = zpClone.sComments;
                auditTable.NewValue = zp.sComments;

                auditTableService.SaveAuditTableService(auditTable);
            }


            if (!zp.sDescription.Equals(zpClone.sDescription))
            {
                auditTable.OldValue = zpClone.sDescription;
                auditTable.NewValue = zp.sDescription;

                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!zp.isSystem.Equals(zpClone.isSystem))
            {
                auditTable.OldValue = zpClone.isSystem;
                auditTable.NewValue = zp.isSystem;

                auditTableService.SaveAuditTableService(auditTable);
            }


        }
        #endregion

        #region AuditAsset
        public void AuditAsset(Asset objNew, Asset objOld, string Type)
        {
            AuditTable auditTable = new AuditTable();
            auditTable.AuditRefCode = objNew.AssetID.ToString();
            auditTable.AuditRefTable = "GFI_FLT_Asset";
            auditTable.AuditType = Type;

            if (Type == "I" || Type == "D")
            {
                auditTable.AuditRefDesc = objNew.AssetName;
                auditTableService.SaveAuditTableService(auditTable);
                return;

            }

            if (!objNew.AssetName.Equals(objOld.AssetName))
            {
                auditTable.OldValue = objOld.AssetName;
                auditTable.NewValue = objNew.AssetName;
                auditTable.AuditRefDesc = "AssetName";
                auditTableService.SaveAuditTableService(auditTable);
            }


            if (!objNew.AssetNumber.Equals(objOld.AssetNumber))
            {
                auditTable.OldValue = objOld.AssetNumber;
                auditTable.NewValue = objNew.AssetNumber;
                auditTable.AuditRefDesc = "AssetNumber";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.AssetType.Equals(objOld.AssetType))
            {
                auditTable.OldValue = objOld.AssetType.ToString();
                auditTable.NewValue = objNew.AssetType.ToString();
                auditTable.AuditRefDesc = "AssetType";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.assetDeviceMap.Equals(objOld.assetDeviceMap))
            {
                auditTable.OldValue = objOld.assetDeviceMap.ToString();
                auditTable.NewValue = objNew.assetDeviceMap.ToString();
                auditTable.AuditRefDesc = "assetDeviceMap";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.Odometer.Equals(objOld.Odometer))
            {
                auditTable.OldValue = objOld.Odometer.ToString();
                auditTable.NewValue = objNew.Odometer.ToString();
                auditTable.AuditRefDesc = "AssetType";
                auditTableService.SaveAuditTableService(auditTable);
            }


            if (!objNew.Status_b.Equals(objOld.Status_b))
            {
                auditTable.OldValue = objOld.Status_b;
                auditTable.NewValue = objNew.Status_b;
                auditTable.AuditRefDesc = "Status_b";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.TimeZoneID.Equals(objOld.TimeZoneID))
            {
                auditTable.OldValue = objOld.TimeZoneID;
                auditTable.NewValue = objNew.TimeZoneID;
                auditTable.AuditRefDesc = "TimeZoneID";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.TimeZoneTS.Equals(objOld.TimeZoneTS))
            {
                auditTable.OldValue = objOld.TimeZoneTS.ToString();
                auditTable.NewValue = objNew.TimeZoneTS.ToString();
                auditTable.AuditRefDesc = "TimeZoneTS";
                auditTableService.SaveAuditTableService(auditTable);
            }

            AssetDeviceMap newlist = objNew.assetDeviceMap;
            AssetDeviceMap oldlist = objOld.assetDeviceMap;

            if (newlist.oprType.Equals(System.Data.DataRowState.Added))
            {
                auditTable.AuditRefCode = newlist.MapID.ToString();
                auditTable.AuditRefTable = "GFI_FLT_AssetDeviceMap";
                auditTable.AuditType = "I";
                auditTable.AuditRefDesc = newlist.DeviceID;

                auditTableService.SaveAuditTableService(auditTable);
            }

            if (newlist.oprType.Equals(System.Data.DataRowState.Deleted))
            {
                auditTable.AuditRefCode = newlist.MapID.ToString();
                auditTable.AuditRefTable = "GFI_FLT_AssetDeviceMap";
                auditTable.AuditType = "D";
                auditTable.AuditRefDesc = oldlist.DeviceID;

                auditTableService.SaveAuditTableService(auditTable);
            }
        }
        #endregion

        #region AuditAssetExtProFields
        public void AuditAssetExtProFields(AssetExtProField objNew, AssetExtProField objOld, string Type)
        {
            AuditTable auditTable = new AuditTable();
            auditTable.AuditRefCode = objNew.FieldId.ToString();
            auditTable.AuditRefTable = "GFI_AMM_AssetExtProFields";
            auditTable.AuditType = Type;

            if (Type == "I" || Type == "D")
            {
                auditTable.AuditRefDesc = objNew.Description;
                auditTableService.SaveAuditTableService(auditTable);
                return;

            }

            if (!objNew.AssetType.Equals(objOld.AssetType))
            {
                auditTable.OldValue = objOld.AssetType;
                auditTable.NewValue = objNew.AssetType;
                auditTable.AuditRefDesc = "AssetType";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.Category.Equals(objOld.Category))
            {
                auditTable.OldValue = objOld.Category;
                auditTable.NewValue = objNew.Category;
                auditTable.AuditRefDesc = "Category";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.DataRetentionDays.Equals(objOld.DataRetentionDays))
            {
                auditTable.OldValue = objOld.DataRetentionDays.ToString();
                auditTable.NewValue = objNew.DataRetentionDays.ToString();
                auditTable.AuditRefDesc = "DataRetentionDays";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.Description.Equals(objOld.Description))
            {
                auditTable.OldValue = objOld.Description;
                auditTable.NewValue = objNew.Description;
                auditTable.AuditRefDesc = "Description";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.DisplayOrder.Equals(objOld.DisplayOrder))
            {
                auditTable.OldValue = objOld.DisplayOrder.ToString();
                auditTable.NewValue = objNew.DisplayOrder.ToString();
                auditTable.AuditRefDesc = "DisplayOrder";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.FieldName.Equals(objOld.FieldName))
            {
                auditTable.OldValue = objOld.FieldName;
                auditTable.NewValue = objNew.FieldName;
                auditTable.AuditRefDesc = "FieldName";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.KeepHistory.Equals(objOld.KeepHistory))
            {
                auditTable.OldValue = objOld.KeepHistory;
                auditTable.NewValue = objNew.KeepHistory;
                auditTable.AuditRefDesc = "KeepHistory";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.THCritical_TimeBased.Equals(objOld.THCritical_TimeBased))
            {
                auditTable.OldValue = objOld.THCritical_TimeBased.ToString();
                auditTable.NewValue = objNew.THCritical_TimeBased.ToString();
                auditTable.AuditRefDesc = "THCritical_TimeBased";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.THCritical_Value.Equals(objOld.THCritical_Value))
            {
                auditTable.OldValue = objOld.THCritical_Value.ToString();
                auditTable.NewValue = objNew.THCritical_Value.ToString();
                auditTable.AuditRefDesc = "THCritical_Value";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.THWarning_TimeBased.Equals(objOld.THWarning_TimeBased))
            {
                auditTable.OldValue = objOld.THWarning_TimeBased.ToString();
                auditTable.NewValue = objNew.THWarning_TimeBased.ToString();
                auditTable.AuditRefDesc = "THWarning_TimeBased";
                auditTableService.SaveAuditTableService(auditTable);
            }


            if (!objNew.THWarning_Value.Equals(objOld.THWarning_Value))
            {
                auditTable.OldValue = objOld.THWarning_Value.ToString();
                auditTable.NewValue = objNew.THWarning_Value.ToString();
                auditTable.AuditRefDesc = "THWarning_Value";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.UnitOfMeasure.Equals(objOld.UnitOfMeasure))
            {
                auditTable.OldValue = objOld.UnitOfMeasure;
                auditTable.NewValue = objNew.UnitOfMeasure;
                auditTable.AuditRefDesc = "UnitOfMeasure";
                auditTableService.SaveAuditTableService(auditTable);
            }
        }

        #endregion

        #region AuditAssetExtProVehicles
        public void AuditAssetExtProVehicles(AssetExtProVehicle objNew, AssetExtProVehicle objOld, string Type)
        {
            AuditTable auditTable = new AuditTable();
            auditTable.AuditRefCode = objNew.AssetId.ToString();
            auditTable.AuditRefTable = "GFI_AMM_AssetExtProVehicles";
            auditTable.AuditType = Type;

            if (Type == "I" || Type == "D")
            {
                auditTable.AuditRefDesc = objNew.Description;
                auditTableService.SaveAuditTableService(auditTable);
                return;

            }

            if (!objNew.Description.Equals(objOld.Description))
            {
                auditTable.OldValue = objOld.Description;
                auditTable.NewValue = objNew.Description;
                auditTable.AuditRefDesc = "Description";

                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.AdditionalInfo.Equals(objOld.AdditionalInfo))
            {
                auditTable.OldValue = objOld.AdditionalInfo;
                auditTable.NewValue = objNew.AdditionalInfo;
                auditTable.AuditRefDesc = "AdditionalInfo";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.EnginePower.Equals(objOld.EnginePower))
            {
                auditTable.OldValue = objOld.EnginePower.ToString();
                auditTable.NewValue = objNew.EnginePower.ToString();
                auditTable.AuditRefDesc = "EnginePower";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.EngineSerialNumber.Equals(objOld.EngineSerialNumber))
            {
                auditTable.OldValue = objOld.EngineSerialNumber;
                auditTable.NewValue = objNew.EngineSerialNumber;
                auditTable.AuditRefDesc = "EngineSerialNumber";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.EngineCapacity.Equals(objOld.EngineCapacity))
            {
                auditTable.OldValue = objOld.EngineCapacity.ToString();
                auditTable.NewValue = objNew.EngineCapacity.ToString();
                auditTable.AuditRefDesc = "EngineCapacity";
                auditTableService.SaveAuditTableService(auditTable);
            }
            if (!objNew.EngineType.Equals(objOld.EngineType))
            {
                auditTable.OldValue = objOld.EngineType;
                auditTable.NewValue = objNew.EngineType;
                auditTable.AuditRefDesc = "EngineType";
                auditTableService.SaveAuditTableService(auditTable);
            }


            if (!objNew.ExpectedLifetime.Equals(objOld.ExpectedLifetime))
            {
                auditTable.OldValue = objOld.ExpectedLifetime.ToString();
                auditTable.NewValue = objNew.ExpectedLifetime.ToString();
                auditTable.AuditRefDesc = "ExpectedLifetime";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.ExternalReference.Equals(objOld.ExternalReference))
            {
                auditTable.OldValue = objOld.ExternalReference;
                auditTable.NewValue = objNew.ExternalReference;
                auditTable.AuditRefDesc = "ExternalReference";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.FuelType.Equals(objOld.FuelType))
            {
                auditTable.OldValue = objOld.FuelType;
                auditTable.NewValue = objNew.FuelType;
                auditTable.AuditRefDesc = "FuelType";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.InService_b.Equals(objOld.InService_b))
            {
                auditTable.OldValue = objOld.InService_b;
                auditTable.NewValue = objNew.InService_b;
                auditTable.AuditRefDesc = "InService_b";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.Make.Equals(objOld.Make))
            {
                auditTable.OldValue = objOld.Make;
                auditTable.NewValue = objNew.Make;
                auditTable.AuditRefDesc = "InService_b";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.Manufacturer.Equals(objOld.Manufacturer))
            {
                auditTable.OldValue = objOld.Manufacturer;
                auditTable.NewValue = objNew.Manufacturer;
                auditTable.AuditRefDesc = "Manufacturer";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.Model.Equals(objOld.Model))
            {
                auditTable.OldValue = objOld.Model;
                auditTable.NewValue = objNew.Model;
                auditTable.AuditRefDesc = "Model";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.PORef.Equals(objOld.PORef))
            {
                auditTable.OldValue = objOld.PORef;
                auditTable.NewValue = objNew.PORef;
                auditTable.AuditRefDesc = "PORef";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.PurchasedDate.Equals(objOld.PurchasedDate))
            {
                auditTable.OldValue = objOld.PurchasedDate.ToString();
                auditTable.NewValue = objNew.PurchasedDate.ToString();
                auditTable.AuditRefDesc = "PurchasedDate";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.PurchasedValue.Equals(objOld.PurchasedValue))
            {
                auditTable.OldValue = objOld.PurchasedValue.ToString();
                auditTable.NewValue = objNew.PurchasedValue.ToString();
                auditTable.AuditRefDesc = "PurchasedValue";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.RegistrationNo.Equals(objOld.RegistrationNo))
            {
                auditTable.OldValue = objOld.RegistrationNo;
                auditTable.NewValue = objNew.RegistrationNo;
                auditTable.AuditRefDesc = "RegistrationNo";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.ResidualValue.Equals(objOld.ResidualValue))
            {
                auditTable.OldValue = objOld.ResidualValue.ToString();
                auditTable.NewValue = objNew.ResidualValue.ToString();
                auditTable.AuditRefDesc = "ResidualValue";
                auditTableService.SaveAuditTableService(auditTable);
            }
            if (!objNew.StdConsumption.Equals(objOld.StdConsumption))
            {
                auditTable.OldValue = objOld.StdConsumption.ToString();
                auditTable.NewValue = objNew.StdConsumption.ToString();
                auditTable.AuditRefDesc = "StdConsumption";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.VehicleTypeDesc.Equals(objOld.VehicleTypeDesc))
            {
                auditTable.OldValue = objOld.VehicleTypeDesc;
                auditTable.NewValue = objNew.VehicleTypeDesc;
                auditTable.AuditRefDesc = "VehicleTypeDesc";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.VehicleTypeId_cbo.Equals(objOld.VehicleTypeId_cbo))
            {
                auditTable.OldValue = objOld.VehicleTypeId_cbo.ToString();
                auditTable.NewValue = objNew.VehicleTypeId_cbo.ToString();
                auditTable.AuditRefDesc = "VehicleTypeId_cbo";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.VIN.Equals(objOld.VIN))
            {
                auditTable.OldValue = objOld.VIN;
                auditTable.NewValue = objNew.VIN;
                auditTable.AuditRefDesc = "VIN";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.YearManufactured.Equals(objOld.YearManufactured))
            {
                auditTable.OldValue = objOld.YearManufactured.ToString();
                auditTable.NewValue = objNew.YearManufactured.ToString();
                auditTable.AuditRefDesc = "YearManufactured";
                auditTableService.SaveAuditTableService(auditTable);
            }
        }
        #endregion

        #region AuditDriver
        public void AuditDriver(Driver objNew, Driver objOld, string Type)
        {
            AuditTable auditTable = new AuditTable();
            auditTable.AuditRefCode = objNew.DriverID.ToString();
            auditTable.AuditRefTable = "GFI_FLT_Driver";
            auditTable.AuditType = Type;

            if (Type == "I" || Type == "D")
            {
                auditTable.AuditRefDesc = objNew.sDriverName;
                auditTableService.SaveAuditTableService(auditTable);
                return;

            }

            if (!objNew.sDriverName.Equals(objOld.sDriverName))
            {
                auditTable.OldValue = objOld.sDriverName;
                auditTable.NewValue = objNew.sDriverName;
                auditTable.AuditRefDesc = "sDriverName";

                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.EmployeeType.Equals(objOld.EmployeeType))
            {
                auditTable.OldValue = objOld.EmployeeType;
                auditTable.NewValue = objNew.EmployeeType;
                auditTable.AuditRefDesc = "EmployeeType";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.iButton.Equals(objOld.iButton))
            {
                auditTable.OldValue = objOld.iButton;
                auditTable.NewValue = objNew.iButton;
                auditTable.AuditRefDesc = "iButton";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.lMatrix.Equals(objOld.lMatrix))
            {
                auditTable.OldValue = objOld.lMatrix.ToString();
                auditTable.NewValue = objNew.lMatrix.ToString();
                auditTable.AuditRefDesc = "lMatrix";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.sComments.Equals(objOld.sComments))
            {
                auditTable.OldValue = objOld.sComments;
                auditTable.NewValue = objNew.sComments;
                auditTable.AuditRefDesc = "sComments";
                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.sEmployeeNo.Equals(objOld.sEmployeeNo))
            {
                auditTable.OldValue = objOld.sEmployeeNo;
                auditTable.NewValue = objNew.sEmployeeNo;
                auditTable.AuditRefDesc = "sEmployeeNo";
                auditTableService.SaveAuditTableService(auditTable);
            }


        }
        #endregion

        #region AuditInsCoverTypes
        public void AuditInsCoverTypes(InsCoverType objNew, InsCoverType objOld, string Type)
        {
            AuditTable auditTable = new AuditTable();
            auditTable.AuditRefCode = objNew.CoverTypeId.ToString();
            auditTable.AuditRefTable = "GFI_AMM_InsCoverTypes";
            auditTable.AuditType = Type;

            if (Type == "I" || Type == "D")
            {
                auditTable.AuditRefDesc = objNew.Description;
                auditTableService.SaveAuditTableService(auditTable);
                return;

            }

            if (!objNew.Description.Equals(objOld.Description))
            {
                auditTable.OldValue = objOld.Description;
                auditTable.NewValue = objNew.Description;
                auditTable.AuditRefDesc = "Description";

                auditTableService.SaveAuditTableService(auditTable);
            }


        }
        #endregion

        #region AuditUser
        public void AuditUser(User objNew, User objOld, string Type)
        {
            AuditTable auditTable = new AuditTable();
            auditTable.AuditRefCode = objNew.Username.ToString();
            auditTable.AuditRefTable = "GFI_SYS_User";
            auditTable.AuditType = Type;

            if (Type == "I" || Type == "D")
            {
                auditTable.AuditRefDesc = objNew.Names;
                auditTableService.SaveAuditTableService(auditTable);
                return;

            }

            if (!objNew.Names.Equals(objOld.Names))
            {
                auditTable.OldValue = objOld.Names;
                auditTable.NewValue = objNew.Names;
                auditTable.AuditRefDesc = "Names";

                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.AccessList.Equals(objOld.AccessList))
            {
                auditTable.OldValue = objOld.AccessList;
                auditTable.NewValue = objNew.AccessList;
                auditTable.AuditRefDesc = "AccessList";

                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.AccessTemplateId.Equals(objOld.AccessTemplateId))
            {
                auditTable.OldValue = objOld.AccessTemplateId.ToString();
                auditTable.NewValue = objNew.AccessTemplateId.ToString();
                auditTable.AuditRefDesc = "AccessTemplateId";

                auditTableService.SaveAuditTableService(auditTable);
            }


            if (!objNew.Email.Equals(objOld.Email))
            {
                auditTable.OldValue = objOld.Email;
                auditTable.NewValue = objNew.Email;
                auditTable.AuditRefDesc = "Email";

                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.lMatrix.Equals(objOld.lMatrix))
            {
                auditTable.OldValue = objOld.lMatrix.ToString();
                auditTable.NewValue = objNew.lMatrix.ToString();
                auditTable.AuditRefDesc = "lMatrix";

                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.MenuLevel_cbo.Equals(objOld.MenuLevel_cbo))
            {
                auditTable.OldValue = objOld.MenuLevel_cbo;
                auditTable.NewValue = objNew.MenuLevel_cbo;
                auditTable.AuditRefDesc = "MenuLevel_cbo";

                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.MobileNo.Equals(objOld.MobileNo))
            {
                auditTable.OldValue = objOld.MobileNo;
                auditTable.NewValue = objNew.MobileNo;
                auditTable.AuditRefDesc = "MobileNo";

                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.Password.Equals(objOld.Password))
            {
                auditTable.OldValue = BaseService.EncryptMe(objOld.Password);
                auditTable.NewValue = BaseService.EncryptMe(objNew.Password);
                auditTable.AuditRefDesc = "Password";

                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.Status_b.Equals(objOld.Status_b))
            {
                auditTable.OldValue = objOld.Status_b;
                auditTable.NewValue = objNew.Status_b;
                auditTable.AuditRefDesc = "Status_b";

                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.StoreUnit_b.Equals(objOld.StoreUnit_b))
            {
                auditTable.OldValue = objOld.StoreUnit_b;
                auditTable.NewValue = objNew.StoreUnit_b;
                auditTable.AuditRefDesc = "StoreUnit_b";

                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.Tel.Equals(objOld.Tel))
            {
                auditTable.OldValue = objOld.Tel;
                auditTable.NewValue = objNew.Tel;
                auditTable.AuditRefDesc = "Tel";

                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.UType_cbo.Equals(objOld.UType_cbo))
            {
                auditTable.OldValue = objOld.UType_cbo;
                auditTable.NewValue = objNew.UType_cbo;
                auditTable.AuditRefDesc = "TUType_cboel";

                auditTableService.SaveAuditTableService(auditTable);
            }

        }
        #endregion

        #region AuditVehicleMaintType
        public void AuditVehicleMaintType(VehicleMaintType objNew, VehicleMaintType objOld, string Type)
        {
            AuditTable auditTable = new AuditTable();
            auditTable.AuditRefCode = objNew.MaintTypeId.ToString();
            auditTable.AuditRefTable = "GFI_AMM_VehicleMaintType";
            auditTable.AuditType = Type;

            if (Type == "I" || Type == "D")
            {
                auditTable.AuditRefDesc = objNew.Description;
                auditTableService.SaveAuditTableService(auditTable);
                return;

            }

            if (!objNew.Description.Equals(objOld.Description))
            {
                auditTable.OldValue = objOld.Description;
                auditTable.NewValue = objNew.Description;
                auditTable.AuditRefDesc = "Description";

                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.MaintCatDesc.Equals(objOld.MaintCatDesc))
            {
                auditTable.OldValue = objOld.MaintCatDesc;
                auditTable.NewValue = objNew.MaintCatDesc;
                auditTable.AuditRefDesc = "MaintCatDesc";

                auditTableService.SaveAuditTableService(auditTable);
            }
            if (!objNew.MaintCatId_cbo.Equals(objOld.MaintCatId_cbo))
            {
                auditTable.OldValue = objOld.MaintCatId_cbo.ToString();
                auditTable.NewValue = objNew.MaintCatId_cbo.ToString();
                auditTable.AuditRefDesc = "MaintCatId_cbo";

                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.OccurrenceDuration.Equals(objOld.OccurrenceDuration))
            {
                auditTable.OldValue = objOld.OccurrenceDuration.ToString();
                auditTable.NewValue = objNew.OccurrenceDuration.ToString();
                auditTable.AuditRefDesc = "OccurrenceDuration";

                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.OccurrenceDurationTh.Equals(objOld.OccurrenceDurationTh))
            {
                auditTable.OldValue = objOld.OccurrenceDurationTh.ToString();
                auditTable.NewValue = objNew.OccurrenceDurationTh.ToString();
                auditTable.AuditRefDesc = "OccurrenceDurationTh";

                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.OccurrenceEngineHrs.Equals(objOld.OccurrenceEngineHrs))
            {
                auditTable.OldValue = objOld.OccurrenceEngineHrs.ToString();
                auditTable.NewValue = objNew.OccurrenceEngineHrs.ToString();
                auditTable.AuditRefDesc = "OccurrenceEngineHrs";

                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.OccurrenceEngineHrsTh.Equals(objOld.OccurrenceEngineHrsTh))
            {
                auditTable.OldValue = objOld.OccurrenceEngineHrsTh.ToString();
                auditTable.NewValue = objNew.OccurrenceEngineHrsTh.ToString();
                auditTable.AuditRefDesc = "OccurrenceEngineHrsTh";

                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.OccurrenceFixedDate.Equals(objOld.OccurrenceFixedDate))
            {
                auditTable.OldValue = objOld.OccurrenceFixedDate.ToString();
                auditTable.NewValue = objNew.OccurrenceFixedDate.ToString();
                auditTable.AuditRefDesc = "OccurrenceFixedDate";

                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.OccurrenceFixedDateTh.Equals(objOld.OccurrenceFixedDateTh))
            {
                auditTable.OldValue = objOld.OccurrenceFixedDateTh.ToString();
                auditTable.NewValue = objNew.OccurrenceFixedDateTh.ToString();
                auditTable.AuditRefDesc = "OccurrenceFixedDateTh";

                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.OccurrenceKM.Equals(objOld.OccurrenceKM))
            {
                auditTable.OldValue = objOld.OccurrenceKM.ToString();
                auditTable.NewValue = objNew.OccurrenceKM.ToString();
                auditTable.AuditRefDesc = "OccurrenceKM";

                auditTableService.SaveAuditTableService(auditTable);
            }
            if (!objNew.OccurrenceKMTh.Equals(objOld.OccurrenceKMTh))
            {
                auditTable.OldValue = objOld.OccurrenceKMTh.ToString();
                auditTable.NewValue = objNew.OccurrenceKMTh.ToString();
                auditTable.AuditRefDesc = "OccurrenceKMTh";

                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.OccurrencePeriod_cbo.Equals(objOld.OccurrencePeriod_cbo))
            {
                auditTable.OldValue = objOld.OccurrencePeriod_cbo;
                auditTable.NewValue = objNew.OccurrencePeriod_cbo;
                auditTable.AuditRefDesc = "OccurrencePeriod_cbo";

                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.OccurrenceType.Equals(objOld.OccurrenceType))
            {
                auditTable.OldValue = objOld.OccurrenceType.ToString();
                auditTable.NewValue = objNew.OccurrenceType.ToString();
                auditTable.AuditRefDesc = "OccurrenceType";

                auditTableService.SaveAuditTableService(auditTable);
            }
        }
        #endregion

        #region AuditVehicleTypes
        public void AuditVehicleTypes(VehicleType objNew, VehicleType objOld, string Type)
        {
            AuditTable auditTable = new AuditTable();
            auditTable.AuditRefCode = objNew.VehicleTypeId.ToString();
            auditTable.AuditRefTable = "GFI_AMM_VehicleTypes";
            auditTable.AuditType = Type;

            if (Type == "I" || Type == "D")
            {
                auditTable.AuditRefDesc = objNew.Description;
                auditTableService.SaveAuditTableService(auditTable);
                return;

            }

            if (!objNew.Description.Equals(objOld.Description))
            {
                auditTable.OldValue = objOld.Description;
                auditTable.NewValue = objNew.Description;
                auditTable.AuditRefDesc = "Description";

                auditTableService.SaveAuditTableService(auditTable);
            }


        }
        #endregion

        #region AuditVehicleMaintStatus
        public void AuditVehicleMaintStatus(VehicleMaintStatus objNew, VehicleMaintStatus objOld, string Type)
        {
            AuditTable auditTable = new AuditTable();
            auditTable.AuditRefCode = objNew.MaintStatusId.ToString();
            auditTable.AuditRefTable = "GFI_AMM_VehicleMaintStatus";
            auditTable.AuditType = Type;

            if (Type == "I" || Type == "D")
            {
                auditTable.AuditRefDesc = objNew.Description;
                auditTableService.SaveAuditTableService(auditTable);
                return;

            }

            if (!objNew.Description.Equals(objOld.Description))
            {
                auditTable.OldValue = objOld.Description;
                auditTable.NewValue = objNew.Description;
                auditTable.AuditRefDesc = "Description";

                auditTableService.SaveAuditTableService(auditTable);
            }


        }
        #endregion

        #region GlobalParams
        public void AuditGlobalParams(GlobalParam objNew, GlobalParam objOld, string Type)
        {
            AuditTable auditTable = new AuditTable();
            auditTable.AuditRefCode = objNew.Iid.ToString();
            auditTable.AuditRefTable = "GFI_SYS_GlobalParams";
            auditTable.AuditType = Type;

            if (Type == "I" || Type == "D")
            {
                auditTable.AuditRefDesc = objNew.ParamName;
                auditTableService.SaveAuditTableService(auditTable);
                return;

            }

            if (!objNew.ParamName.Equals(objOld.ParamName))
            {
                auditTable.OldValue = objOld.ParamName;
                auditTable.NewValue = objNew.ParamName;

                auditTableService.SaveAuditTableService(auditTable);
            }


            if (!objNew.PValue.Equals(objOld.PValue))
            {
                auditTable.OldValue = objOld.PValue;
                auditTable.NewValue = objNew.PValue;

                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.ParamComments.Equals(objOld.ParamComments))
            {
                auditTable.OldValue = objOld.ParamComments;
                auditTable.NewValue = objNew.ParamComments;

                auditTableService.SaveAuditTableService(auditTable);
            }


        }
        #endregion

        #region Module
        public void AuditModule(Module objNew, Module objOld, string Type)
        {
            AuditTable auditTable = new AuditTable();
            auditTable.AuditRefCode = objNew.UID.ToString();
            auditTable.AuditRefTable = "GFI_SYS_Modules";
            auditTable.AuditType = Type;

            if (Type == "I" || Type == "D")
            {
                auditTable.AuditRefDesc = objNew.Command;
                auditTableService.SaveAuditTableService(auditTable);
                return;

            }

            if (!objNew.ModuleName.Equals(objOld.ModuleName))
            {
                auditTable.OldValue = objOld.ModuleName;
                auditTable.NewValue = objNew.ModuleName;

                auditTableService.SaveAuditTableService(auditTable);
            }

        }
        #endregion

        #region GetAudit-GFI_SYS_Audit
        public DataTable GetAudit(String sConnStr)
        {

            DataTable dataTable = new DataTable();
            String sqlString = @"SELECT AuditId, 
			 AuditUser, 
			 AuditDate,
			 CASE AuditType
			 WHEN 'E' THEN 'EDIT'
			 WHEN 'I' THEN 'INSERT'
			 WHEN 'D' THEN 'DELETE'
			 END as AuditType, 
			 ReferenceCode, 
			 ReferenceDesc, 
			 ReferenceTable, 
			 CreatedDate,
			 CallerFunction,
			 SQLRemarks,
			 NewValue,
			 OldValue
			 From GFI_SYS_Audit 
union

SELECT AuditId, 
			 AuditUser, 
			 AuditDate,
			 CASE AuditType
			 WHEN 'E' THEN 'EDIT'
			 WHEN 'I' THEN 'INSERT'
			 WHEN 'D' THEN 'DELETE'
			 END as AuditType, 
			 ReferenceCode, 
			 ReferenceDesc, 
			 ReferenceTable, 
			 CreatedDate,
			 CallerFunction,
			 SQLRemarks,
			 NewValue,
			 OldValue
			 From GFI_ARC_Audit 
order by AuditId";
            return new NaveoOneLib.DBCon.Connection(sConnStr).GetDataDT(sqlString, sConnStr);

        }
        #endregion

        #region GetAuditAccess-GFI_SYS_AuditAccess
        public DataTable GetAuditAccess(String sConnStr)
        {

            DataTable dataTable = new DataTable();
            String sqlString = @"SELECT Iid, 
			 AuditUser, 
			 Control,
			 AuditDate
			 From GFI_SYS_AuditAccess order by Iid";
            return new NaveoOneLib.DBCon.Connection(sConnStr).GetDataDT(sqlString, sConnStr);

        }
        #endregion

        #region GetLoginAudit-GFI_SYS_LoginAudit
        public DataTable GetLoginAudit(String sConnStr)
        {

            DataTable dataTable = new DataTable();
            String sqlString = @"SELECT auditId, 
			 auditUser, 
			 auditDate,
			 CASE auditType 
			 WHEN '01' THEN 'Login'
			 WHEN '02' THEN 'Logout'
			 WHEN '03' THEN 'Login Failure'
			 END as audittype,
			 machineName,
			 clientGroupMachine
			 From GFI_SYS_LoginAudit order by auditId";
            return new NaveoOneLib.DBCon.Connection(sConnStr).GetDataDT(sqlString, sConnStr);

        }
        #endregion

        #region GetAuditUser
        public DataTable GetAuditUser(string table, String sConnStr)
        {
            String sqlString = @"SELECT Distinct AuditUser
			 From " + table;
            return new NaveoOneLib.DBCon.Connection(sConnStr).GetDataDT(sqlString, sConnStr);

        }
        #endregion

        #region AuditZone
        public void AuditZone(Zone objNew, string Type)
        {
            AuditTable auditTable = new AuditTable();
            auditTable.AuditRefCode = objNew.zoneID.ToString();
            auditTable.AuditRefTable = "GFI_FLT_ZoneHeader";
            auditTable.AuditType = Type;

            if (Type == "I" || Type == "D")
            {
                auditTable.AuditRefDesc = objNew.description;
                auditTableService.SaveAuditTableService(auditTable);
                return;
            }
        }

        public void AuditZone(Zone objNew, Zone objOld, string Type)
        {
            AuditTable auditTable = new AuditTable();
            auditTable.AuditRefCode = objNew.zoneID.ToString();
            auditTable.AuditRefTable = "GFI_FLT_ZoneHeader";
            auditTable.AuditType = Type;

            if (Type == "I" || Type == "D")
            {
                auditTable.AuditRefDesc = objNew.description;
                auditTableService.SaveAuditTableService(auditTable);
                return;
            }

            if (!objNew.description.Equals(objOld.description))
            {
                auditTable.OldValue = objOld.description;
                auditTable.NewValue = objNew.description;
                auditTable.AuditRefDesc = "Description";

                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.externalRef.Equals(objOld.externalRef))
            {
                auditTable.OldValue = objOld.externalRef;
                auditTable.NewValue = objNew.externalRef;
                auditTable.AuditRefDesc = "ExternalRef";

                auditTableService.SaveAuditTableService(auditTable);
            }


            if (!objNew.ref2.Equals(objOld.ref2))
            {
                auditTable.OldValue = objOld.ref2;
                auditTable.NewValue = objNew.ref2;
                auditTable.AuditRefDesc = "Ref2";

                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.color.Equals(objOld.color))
            {
                auditTable.OldValue = objOld.color.ToString();
                auditTable.NewValue = objNew.color.ToString();
                auditTable.AuditRefDesc = "Color";

                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.comments.Equals(objOld.comments))
            {
                auditTable.OldValue = objOld.comments;
                auditTable.NewValue = objNew.comments;
                auditTable.AuditRefDesc = "Comments";

                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.displayed.Equals(objOld.displayed))
            {
                auditTable.OldValue = objOld.displayed.ToString(); ;
                auditTable.NewValue = objNew.displayed.ToString();
                auditTable.AuditRefDesc = "ExternalRef";
                auditTableService.SaveAuditTableService(auditTable);
            }


        }
        #endregion

        #region AuditExtTripInfoType
        public void AuditExtTripInfoType(ExtTripInfoType objNew, ExtTripInfoType objOld, string Type)
        {
            AuditTable auditTable = new AuditTable();
            auditTable.AuditRefCode = objNew.IID.ToString();
            auditTable.AuditRefTable = "GFI_FLT_ExtTripInfoType";
            auditTable.AuditType = Type;

            if (Type == "I" || Type == "D")
            {
                auditTable.AuditRefDesc = objNew.TypeName;
                auditTableService.SaveAuditTableService(auditTable);
                return;
            }

            if (!objNew.TypeName.Equals(objOld.TypeName))
            {
                auditTable.OldValue = objOld.TypeName;
                auditTable.NewValue = objNew.TypeName;
                auditTable.AuditRefDesc = "TypeName";

                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.Field1Name.Equals(objOld.Field1Name))
            {
                auditTable.OldValue = objOld.Field1Name.ToString();
                auditTable.NewValue = objNew.Field1Name.ToString();
                auditTable.AuditRefDesc = "Field1Name";

                auditTableService.SaveAuditTableService(auditTable);
            }


            if (!objNew.Field1Label.Equals(objOld.Field1Label))
            {
                auditTable.OldValue = objOld.Field1Label;
                auditTable.NewValue = objNew.Field1Label;
                auditTable.AuditRefDesc = "Field1Label";

                auditTableService.SaveAuditTableService(auditTable);
            }

            if (!objNew.Field1Type.Equals(objOld.Field1Type))
            {
                auditTable.OldValue = objOld.Field1Type.ToString();
                auditTable.NewValue = objNew.Field1Type.ToString();
                auditTable.AuditRefDesc = "Field1Type";

                auditTableService.SaveAuditTableService(auditTable);
            }

            //if (!objNew.Field2Name.Equals(objOld.Field2Name))
            //{
            //     auditTable.OldValue = objOld.Field2Name;
            //     auditTable.NewValue = objNew.Field2Name;
            //     auditTable.AuditRefDesc = "Field2Name";

            //     auditTableService.SaveAuditTableService(auditTable);
            //}

            //if (!objNew.Field2Label.Equals(objOld.Field2Label))
            //{
            //     auditTable.OldValue = objOld.Field2Label;
            //     auditTable.NewValue = objNew.Field2Label;
            //     auditTable.AuditRefDesc = "Field2Label";

            //     auditTableService.SaveAuditTableService(auditTable);
            //}

            //if (!objNew.Field2Type.Equals(objOld.Field2Type))
            //{
            //     auditTable.OldValue = objOld.Field2Type;
            //     auditTable.NewValue = objNew.Field2Type;
            //     auditTable.AuditRefDesc = "Field2Type";

            //     auditTableService.SaveAuditTableService(auditTable);
            //}
        }
        #endregion

        #region AuditExtTripInfo
        public void AuditExtTripInfo(ExtTripInfo objNew, ExtTripInfo objOld, string Type)
        {
            AuditTable auditTable = new AuditTable();
            auditTable.AuditRefCode = objNew.IID.ToString();
            auditTable.AuditRefTable = "GFI_FLT_ExtTripInfo";
            auditTable.AuditType = Type;

            if (Type == "I" || Type == "D")
            {
                auditTable.AuditRefDesc = objNew.AdditionalField1Label;
                auditTableService.SaveAuditTableService(auditTable);

                auditTable.AuditRefDesc = objNew.AdditionalField1Value;
                auditTableService.SaveAuditTableService(auditTable);

                auditTable.AuditRefDesc = objNew.AdditionalField2Label;
                auditTableService.SaveAuditTableService(auditTable);

                auditTable.AuditRefDesc = objNew.AdditionalField2Value;
                auditTableService.SaveAuditTableService(auditTable);

                foreach (ExtTripInfoResource emp in objNew.lEmpBase)
                {
                    auditTable.AuditRefDesc = emp.EmpCode.ToString();
                    auditTableService.SaveAuditTableService(auditTable);
                }

                if (objNew.Field1Name.ToString() != string.Empty)
                {
                    auditTable.AuditRefDesc = objNew.Field1Name.ToString();
                    auditTableService.SaveAuditTableService(auditTable);
                }

                if (objNew.Field1Value.ToString() != string.Empty)
                {
                    auditTable.AuditRefDesc = objNew.Field1Value.ToString();
                    auditTableService.SaveAuditTableService(auditTable);
                }

                if (objNew.Field2Name.ToString() != string.Empty)
                {
                    auditTable.AuditRefDesc = objNew.Field2Name.ToString();
                    auditTableService.SaveAuditTableService(auditTable);
                }

                if (objNew.Field2Value.ToString() != string.Empty)
                {
                    auditTable.AuditRefDesc = objNew.Field2Value.ToString();
                    auditTableService.SaveAuditTableService(auditTable);
                }

                if (objNew.Field3Name.ToString() != string.Empty)
                {
                    auditTable.AuditRefDesc = objNew.Field3Name.ToString();
                    auditTableService.SaveAuditTableService(auditTable);
                }

                if (objNew.Field3Value.ToString() != string.Empty)
                {
                    auditTable.AuditRefDesc = objNew.Field3Value.ToString();
                    auditTableService.SaveAuditTableService(auditTable);
                }

                if (objNew.Field4Name.ToString() != string.Empty)
                {
                    auditTable.AuditRefDesc = objNew.Field4Name.ToString();
                    auditTableService.SaveAuditTableService(auditTable);
                }

                if (objNew.Field4Value.ToString() != string.Empty)
                {
                    auditTable.AuditRefDesc = objNew.Field4Value.ToString();
                    auditTableService.SaveAuditTableService(auditTable);
                }

                if (objNew.Field5Name.ToString() != string.Empty)
                {
                    auditTable.AuditRefDesc = objNew.Field5Name.ToString();
                    auditTableService.SaveAuditTableService(auditTable);
                }

                if (objNew.Field5Value.ToString() != string.Empty)
                {
                    auditTable.AuditRefDesc = objNew.Field5Value.ToString();
                    auditTableService.SaveAuditTableService(auditTable);
                }

                if (objNew.Field6Name.ToString() != string.Empty)
                {
                    auditTable.AuditRefDesc = objNew.Field6Name.ToString();
                    auditTableService.SaveAuditTableService(auditTable);
                }

                if (objNew.Field6Value.ToString() != string.Empty)
                {
                    auditTable.AuditRefDesc = objNew.Field6Value.ToString();
                    auditTableService.SaveAuditTableService(auditTable);
                }
                return;
            }
        }
        #endregion

        #region AuditAsset
        public void AuditAssetWizard(Asset objNew, string Type)
        {
            AuditTable auditTable = new AuditTable();
            auditTable.AuditRefCode = objNew.AssetID.ToString();
            auditTable.AuditRefTable = "GFI_FLT_Asset";
            auditTable.AuditType = Type;

            if (Type == "I" || Type == "D")
            {
                auditTable.AuditRefDesc = objNew.AssetName;
                auditTableService.SaveAuditTableService(auditTable);

                auditTable.AuditRefDesc = objNew.AssetNumber;
                auditTableService.SaveAuditTableService(auditTable);

                auditTable.AuditRefDesc = objNew.AssetType.ToString();
                auditTableService.SaveAuditTableService(auditTable);

                auditTable.AuditRefDesc = objNew.Odometer.ToString();
                auditTableService.SaveAuditTableService(auditTable);
                return;
            }
        }
        public void AuditAssetWizard(Asset objNew, Asset objOld, string Type)
        {
            AuditTable auditTable = new AuditTable();
            auditTable.AuditRefCode = objNew.AssetID.ToString();
            auditTable.AuditRefTable = "GFI_FLT_Asset";
            auditTable.AuditType = Type;

            if (Type == "I" || Type == "D")
            {
                auditTable.AuditRefDesc = objNew.AssetName;
                auditTableService.SaveAuditTableService(auditTable);

                auditTable.AuditRefDesc = objNew.AssetNumber;
                auditTableService.SaveAuditTableService(auditTable);

                auditTable.AuditRefDesc = objNew.AssetType.ToString();
                auditTableService.SaveAuditTableService(auditTable);

                auditTable.AuditRefDesc = objNew.Odometer.ToString();
                auditTableService.SaveAuditTableService(auditTable);
                return;
            }
            if (objNew.AssetName.ToString() != string.Empty)
            {
                if (!objNew.AssetName.Equals(objOld.AssetName))
                {
                    auditTable.OldValue = objOld.AssetName;
                    auditTable.NewValue = objNew.AssetName;
                    auditTable.AuditRefDesc = "AssetName";
                    auditTableService.SaveAuditTableService(auditTable);
                }
            }

            if (objNew.AssetNumber.ToString() != string.Empty)
            {
                if (!objNew.AssetNumber.Equals(objOld.AssetNumber))
                {
                    auditTable.OldValue = objOld.AssetNumber;
                    auditTable.NewValue = objNew.AssetNumber;
                    auditTable.AuditRefDesc = "AssetNumber";
                    auditTableService.SaveAuditTableService(auditTable);
                }
            }

            if (objNew.AssetType.ToString() != string.Empty)
            {
                if (!objNew.AssetType.Equals(objOld.AssetType))
                {
                    auditTable.OldValue = objOld.AssetType.ToString();
                    auditTable.NewValue = objNew.AssetType.ToString();
                    auditTable.AuditRefDesc = "AssetType";
                    auditTableService.SaveAuditTableService(auditTable);
                }
            }

            if (objNew.Odometer.ToString() != string.Empty)
            {
                if (!objNew.AssetType.Equals(objOld.Odometer))
                {
                    auditTable.OldValue = objOld.Odometer.ToString();
                    auditTable.NewValue = objNew.Odometer.ToString();
                    auditTable.AuditRefDesc = "Odometer";
                    auditTableService.SaveAuditTableService(auditTable);
                }
            }

        }
        #endregion

        #region FileExplorer
        public void AuditFileExplorer(string fileName, string path)
        {
            AuditTable auditTable = new AuditTable();
            auditTable.AuditRefCode = Globals.uLogin.Username.ToString();
            auditTable.AuditRefTable = "GFI_FLT_FileExplorer";
            auditTable.AuditType = "I";

            auditTable.AuditRefDesc = fileName;
            auditTableService.SaveAuditTableService(auditTable);

            auditTable.AuditRefDesc = path;
            auditTableService.SaveAuditTableService(auditTable);

            return;
        }
        #endregion
    }
}
