﻿using System;
using System.Collections.Generic;
using System.Text;
using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using NaveoOneLib.Models.Audits;
using NaveoOneLib.Models.Common;

namespace NaveoOneLib.Services.Audits
{
    public class AuditService
    {
        Errors er = new Errors();
        public DataTable GetAudit(String sConnStr)
        {
            String sqlString = @"SELECT AuditId, 
                                        AuditTypes, 
                                        ReferenceCode, 
                                        ReferenceDesc, 
                                        CreatedDate, 
                                        AuditUser, 
                                        ReferenceTable, 
                                        CallerFunction, 
                                        SQLRemarks from SYS_Audit order by CreatedDate desc";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public DataTable LogTran(int iType, String sModule, String Descrip, int iUserID, int TranID)
        {
            String sType = String.Empty; ; 
            switch (iType)
            {
                case 1:
                    sType = "IN";   //1. Insert
                    break;

                case 2:
                    sType = "UP";   //2. Update
                    break;

                case 3:
                    sType = "DL";   //3. Delete
                    break;
            }
            DataTable dt = new DataTable();
            Audit a = new Audit();
            dt.TableName = "SYS_Audit";
            dt.Columns.Add("AuditId", a.AuditId.GetType());
            dt.Columns.Add("AuditTypes");
            dt.Columns.Add("ReferenceCode");
            dt.Columns.Add("ReferenceDesc");
            dt.Columns.Add("CreatedDate", a.CreatedDate.GetType());
            dt.Columns.Add("AuditUser");
            dt.Columns.Add("ReferenceTable");
            dt.Columns.Add("CallerFunction");
            dt.Columns.Add("SQLRemarks");
            dt.Columns.Add("oprType", typeof(DataRowState));

            String sql=String.Empty;
            if (sModule.Length > 20)
            {
                sql += sModule;
                sModule = sModule.Substring(0, 19);
            }
            if (Descrip.Length > 30)
            {
                sql += Descrip;
                Descrip = Descrip.Substring(0, 29);
            }

            DataRow dr = dt.NewRow();
            dr["AuditTypes"] = sType;
            dr["ReferenceCode"] = sModule;
            dr["ReferenceDesc"] = Descrip;
            dr["CreatedDate"] = DateTime.UtcNow;
            dr["AuditUser"] = iUserID.ToString();
            dr["ReferenceTable"] = String.Empty;
            dr["CallerFunction"] = TranID.ToString();
            dr["SQLRemarks"] = sql;
            dr["oprType"] = DataRowState.Added;

            dt.Rows.Add(dr);
            return dt;
        }
        public Audit GetAuditById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT AuditId, 
                                        AuditTypes, 
                                        ReferenceCode, 
                                        ReferenceDesc, 
                                        CreatedDate, 
                                        AuditUser, 
                                        ReferenceTable, 
                                        CallerFunction, 
                                        SQLRemarks from SYS_Audit
                                        WHERE AuditId = ?
                                        order by AuditId";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                Audit retAudit = new Audit();
                retAudit.AuditId = Convert.ToInt16(dt.Rows[0]["AuditId"].ToString());
                retAudit.AuditTypes = dt.Rows[0]["AuditTypes"].ToString();
                retAudit.ReferenceCode = dt.Rows[0]["ReferenceCode"].ToString();
                retAudit.ReferenceDesc = dt.Rows[0]["ReferenceDesc"].ToString();
                retAudit.CreatedDate = (DateTime)dt.Rows[0]["CreatedDate"];
                retAudit.AuditUser = dt.Rows[0]["AuditUser"].ToString();
                retAudit.ReferenceTable = dt.Rows[0]["ReferenceTable"].ToString();
                retAudit.CallerFunction = dt.Rows[0]["CallerFunction"].ToString();
                retAudit.SQLRemarks = dt.Rows[0]["SQLRemarks"].ToString();
                return retAudit;
            }
        }

        public Audit GetLastLoginOut(String email, String type, String sConnStr)
        {
            String sqlString = @"select email, max(AccessedOn) from GFI_SYS_AuditTraceLogin where sAction = '"+ type + "' group by Email";

            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, email, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                Audit retAudit = new Audit();
                retAudit.AuditId = Convert.ToInt16(dt.Rows[0]["AuditId"].ToString());
                retAudit.AuditTypes = dt.Rows[0]["AuditTypes"].ToString();
                retAudit.ReferenceCode = dt.Rows[0]["ReferenceCode"].ToString();
                retAudit.ReferenceDesc = dt.Rows[0]["ReferenceDesc"].ToString();
                retAudit.CreatedDate = (DateTime)dt.Rows[0]["CreatedDate"];
                retAudit.AuditUser = dt.Rows[0]["AuditUser"].ToString();
                retAudit.ReferenceTable = dt.Rows[0]["ReferenceTable"].ToString();
                retAudit.CallerFunction = dt.Rows[0]["CallerFunction"].ToString();
                retAudit.SQLRemarks = dt.Rows[0]["SQLRemarks"].ToString();
                return retAudit;
            }
        }


        private readonly Type str = typeof(String);
        /// <summary>
        /// Insert in GFI_SYS_AuditTrace Table
        /// </summary>
        /// <returns></returns>
        public BaseModel Trace(AuditData sAudit ,String sConnStr, Boolean bInsert)
        {
            #region commented
            //DataTable dt = new DataTable();

            //dt.TableName = "GFI_SYS_AuditTrace";
            //dt.Columns.Add("UserToken", typeof(Guid));
            //dt.Columns.Add("Controller", str);
            //dt.Columns.Add("Action", str);
            //dt.Columns.Add("IsAccessGranted", typeof(int));
            //dt.Columns.Add("AccessedOn", typeof(DateTime));
            //dt.Columns.Add("RawRequest", typeof(String));

            //DataRow dr = dt.NewRow();
            //dr["UserToken"] = userToken;
            //dr["Controller"] = controller;
            //dr["Action"] = action;
            //dr["RawRequest"] = rawRequest;
            //if (isAccessGranted)
            //    dr["IsAccessGranted"] = 1;
            //else
            //    dr["IsAccessGranted"] = 0;
            //dr["AccessedOn"] = DateTime.Now;
            //dt.Rows.Add(dr);

            //Connection _connection = new Connection(sConnStr); ;
            //if (_connection.GenInsert(dt, sConnStr))
            //    return true;
            //else
            //    return false;
            #endregion

            if (!bInsert)
            {
                int AuditTraceId = sAudit.AuditTraceId;
                sAudit.AuditTraceId = this.GetAuditById(sConnStr, AuditTraceId).AuditTraceId;
            }

            DataTable dt = AuditDt();

            DataRow dr = dt.NewRow();
            dr["UserToken"] = sAudit.UserToken;
            dr["Controller"] = sAudit.Controller;
            dr["Action"] = sAudit.Action;
            dr["IsAccessGranted"] = sAudit.IsAccessGranted;
            dr["AccessedOn"] = bInsert ? DateTime.Now : sAudit.AccessedOn;
            dr["RawRequest"] = sAudit.RawRequest;
            dr["sID"] = sAudit.sID;
            dt.Rows.Add(dr);


            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditTraceId";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = sAudit.AuditTraceId;

            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            BaseModel retModel = new BaseModel();
            retModel.data = bResult;
            retModel.additionalData = dtCtrl.Rows[0].Field<string>("NextIdValue");
            return retModel;
        }

        public AuditData GetAuditById(String sConnStr, int AuditTraceId) {

            AuditData audit = new AuditData();
            String sql = @"SELECT * FROM GFI_SYS_AuditTrace WHERE AuditTraceId = " + AuditTraceId + ";";
            DataTable dt = new Connection(sConnStr).GetDataDT(sql, sConnStr);
            audit.AuditTraceId = Convert.ToInt32(dt.Rows[0].Field<long>("AuditTraceId"));
            audit.UserToken = dt.Rows[0].Field<Guid>("UserToken");
            audit.Controller = dt.Rows[0].Field<String>("Controller");
            audit.Action = dt.Rows[0].Field<String>("Action");
            audit.IsAccessGranted = dt.Rows[0].Field<int>("IsAccessGranted");
            audit.AccessedOn = dt.Rows[0].Field<DateTime>("AccessedOn");
            audit.RawRequest = dt.Rows[0].Field<String>("RawRequest");
            return audit;
        }
        DataTable AuditDt()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_AuditTrace";
            dt.Columns.Add("AuditTraceId", typeof(int));
            dt.Columns.Add("UserToken", typeof(String));
            dt.Columns.Add("Controller", typeof(String));
            dt.Columns.Add("Action", typeof(String));
            dt.Columns.Add("IsAccessGranted", typeof(int));
            dt.Columns.Add("AccessedOn", typeof(DateTime));
            dt.Columns.Add("RawRequest", typeof(String));
            dt.Columns.Add("sID", typeof(int));

            return dt;
        }

        /// <summary>
        /// Insert in GFI_SYS_AuditTraceLogin Table
        /// </summary>
        /// <returns></returns>
        public bool Trace(string email, string sAction, bool isAccessGranted, String sConnStr)
        {
            DataTable dt = new DataTable();

            dt.TableName = "GFI_SYS_AuditTraceLogin";
            dt.Columns.Add("Email", str);
            dt.Columns.Add("sAction", str);
            dt.Columns.Add("accessedOn", typeof(DateTime));
            dt.Columns.Add("IsAccessGranted", typeof(int));

            DataRow dr = dt.NewRow();
            dr["Email"] = email;
            dr["sAction"] = sAction;
            dr["IsAccessGranted"] = (isAccessGranted) ? 1 : 0;
            dr["AccessedOn"] = DateTime.Now;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenInsert(dt, sConnStr))
                return true;
            else
                return false;
        }

    }
}

