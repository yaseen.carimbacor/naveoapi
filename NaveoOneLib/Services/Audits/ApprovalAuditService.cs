﻿using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models.Plannings;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoOneLib.Services.Audits
{
    public class ApprovalAuditService
    {

        public Boolean SaveApprovalAudit(Planning objOld, Planning objNew,string Module,int UID , bool bInsert, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            DataSet dsProcess = new DataSet();

            DataTable dtAudit = new DataTable();
       
            if (!dtAudit.Columns.Contains("oprType"))
                dtAudit.Columns.Add("oprType", typeof(DataRowState));

            dtAudit.TableName = "GFI_SYS_APPROVALAUDIT";
            dtAudit.Columns.Add("iID");
            dtAudit.Columns.Add("UserID");
            dtAudit.Columns.Add("OldApprovalProcessID");
            dtAudit.Columns.Add("NewApprovalProcessID");
            dtAudit.Columns.Add("Source");
            dtAudit.Columns.Add("SourceID");


          
            DataRow dr = dtAudit.NewRow();

                if (bInsert)
                    dr["oprType"] = DataRowState.Added;
                else
                    dr["oprType"] = DataRowState.Modified;
                
                if(objOld.ApprovalID ==0)
                   dr["OldApprovalProcessID"] =null;
                 else
                  dr["OldApprovalProcessID"] = objOld.ApprovalID;

            dr["iID"] = 0;
            dr["UserID"] = UID;
            dr["NewApprovalProcessID"] = objNew.ApprovalID;
            dr["Source"] = Module;
            dr["SourceID"] = objNew.PID;
            dtAudit.Rows.Add(dr);

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "iID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "CallerFunction";
            drCtrl["ParentTblName"] = "GFI_SYS_APPROVALAUDIT";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;



        }

        

    }
}
