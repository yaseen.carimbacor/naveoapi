﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Users;
using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Services.Rules;
using System.Net;

namespace NaveoOneLib.Services.Users
{
    public class UserService
    {
        Errors er = new Errors();

        Boolean IsValidEmailAddress(String emailaddress)
        {
            try
            {
                System.Net.Mail.MailAddress m = new System.Net.Mail.MailAddress(emailaddress);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
        Boolean isValidPassword(String sPassword)
        {
            String ErrorMessage = String.Empty;

            if (sPassword.Trim().Length < 8)
            {
                ErrorMessage = "Password should be more than 7 characters";
                return false;
            }
            else if (sPassword.Contains("'"))
            {
                ErrorMessage = "Password should not contain '";
                return false;
            }

            Regex hasNumber = new Regex(@"[0-9]+");
            Regex hasUpperChar = new Regex(@"[A-Z]+");
            //Regex hasMiniMaxChars = new Regex(@".{8,8}");
            Regex hasLowerChar = new Regex(@"[a-z]+");
            Regex hasSymbols = new Regex(@"[!@#$%^&*()_+=\[{\]};:<>|./?,-]");

            if (!hasLowerChar.IsMatch(sPassword))
            {
                ErrorMessage = "Password should contain At least one lower case letter";
                return false;
            }
            else if (!hasUpperChar.IsMatch(sPassword))
            {
                ErrorMessage = "Password should contain At least one upper case letter";
                return false;
            }
            //else if (!hasMiniMaxChars.IsMatch(sPassword))
            //{
            //    ErrorMessage = "Password should not be less than or greater than 8 characters";
            //    return false;
            //}
            else if (!hasNumber.IsMatch(sPassword))
            {
                ErrorMessage = "Password should contain At least one numeric value";
                return false;
            }

            else if (!hasSymbols.IsMatch(sPassword))
            {
                ErrorMessage = "Password should contain At least one special case characters";
                return false;
            }
            else
            {
                return true;
            }
        }
        String CreateRandomPassword(int passwordLength)
        {
            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789!@#$?_-.";
            string LowerAlphaChars = "abcdefghijkmnopqrstuvwxyz";
            string UpperAplahChars = "ABCDEFGHJKLMNOPQRSTUVWXYZ";
            string NumChars = "0123456789";
            string SpecialChars = "!@#$?_-.";

            char[] chars = new char[passwordLength];
            Random rd = new Random();

            for (int i = 0; i < passwordLength; i++)
            {
                switch (i)
                {
                    case 0:
                        chars[i] = LowerAlphaChars[rd.Next(0, LowerAlphaChars.Length)];
                        break;

                    case 1:
                        chars[i] = UpperAplahChars[rd.Next(0, UpperAplahChars.Length)];
                        break;

                    case 2:
                        chars[i] = NumChars[rd.Next(0, NumChars.Length)];
                        break;

                    case 3:
                        chars[i] = SpecialChars[rd.Next(0, SpecialChars.Length)];
                        break;

                    default:
                        chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
                        break;
                }
            }

            String pwd = new string(chars);
            while (!isValidPassword(pwd))
                CreateRandomPassword(passwordLength);

            return pwd;
        }
        public String CreateAPICompany(int passwordLength)
        {
            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789!@#$?_-.*+";
            string LowerAlphaChars = "abcdefghijkmnopqrstuvwxyz";
            string UpperAplahChars = "ABCDEFGHJKLMNOPQRSTUVWXYZ";
            string NumChars = "0123456789";
            string SpecialChars = "!@#$?_-.*+";

            char[] chars = new char[passwordLength];
            Random rd = new Random();

            for (int i = 0; i < passwordLength; i++)
            {
                switch (i)
                {
                    case 0:
                        chars[i] = LowerAlphaChars[rd.Next(0, LowerAlphaChars.Length)];
                        break;

                    case 1:
                        chars[i] = UpperAplahChars[rd.Next(0, UpperAplahChars.Length)];
                        break;

                    case 2:
                        chars[i] = NumChars[rd.Next(0, NumChars.Length)];
                        break;

                    case 3:
                        chars[i] = SpecialChars[rd.Next(0, SpecialChars.Length)];
                        break;

                    default:
                        chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
                        break;
                }
            }

            String pwd = new string(chars);
            return pwd;
        }
        public DataTable GetMobileAccessByUserID(int UID, String sConnStr)
        {
            String sqlString = @"SELECT MobileAccess from GFI_SYS_User where UID = " + UID.ToString() + "";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public DataTable GetAllUsers(String sConnStr)
        {
            String sqlString = @"SELECT * from GFI_SYS_User order by Username";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public DataTable GetAllUsers(List<Matrix> lMatrix, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            String sqlString = @"SELECT DISTINCT u.*
                                from GFI_SYS_User u, GFI_SYS_GroupMatrixUser m
                                where u.UID = m.iID and m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + ") order by Username";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public static String sGetUsersIDnDesc(int UserID, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(UserID, sConnStr);
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            String sql = @"SELECT DISTINCT u.UID, u.Username, u.Names, u.LastName, u.Email
                                from GFI_SYS_User u, GFI_SYS_GroupMatrixUser m
                                where u.UID = m.iID and m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + ") order by u.UID";
            return sql;
        }
        public DataSet GetUserDS(List<Matrix> lMatrix, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            String sql = @"SELECT DISTINCT u.*
                                from GFI_SYS_User u, GFI_SYS_GroupMatrixUser m
                                where u.UID = m.iID and m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + ") order by Username";

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtUser";
            dtSql.Rows.Add(drSql);

            sql = @"select * from GFI_SYS_GroupMatrixUser where GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + ")";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds;
        }
        public String sGetMatrixUserByeMail(String eMail)
        {
            String sql = "SELECT m.MID, m.iID, m.GMID, gm.GMDescription ";
            sql += "from GFI_SYS_GroupMatrixUser m, GFI_SYS_GroupMatrix gm, GFI_SYS_User u ";
            sql += "WHERE m.GMID = gm.GMID and m.iID = u.UID and u.Email = '" + eMail + "'";
            sql += " order by gm.GMID";
            return sql;
        }
        public String sGetMatrixUserByUserID(int UID)
        {
            String sql = "SELECT m.MID, m.iID, m.GMID, gm.GMDescription ";
            sql += "from GFI_SYS_GroupMatrixUser m, GFI_SYS_GroupMatrix gm, GFI_SYS_User u ";
            sql += "WHERE m.GMID = gm.GMID and m.iID = u.UID and u.UID = '" + UID + "'";
            sql += " order by gm.GMID";
            return sql;
        }
        public String sGetMatrixUserByUserToken()
        {
            String sql = sGetMatrixUserByUserID(-999);
            sql = sql.Replace("u.UID = '-999'", "u.UserToken = ?");

            return sql;
        }

        //Additional Fields here
        User GetUser(DataSet ds, String sPassword, String sConnStr, Boolean bHasDetails)
        {
            GlobalParamsService globalParamsService = new GlobalParamsService();
            User u = new User();
            if (ds.Tables["dtUser"].Rows.Count == 0)
            {
                u.QryResult = User.UserResult.InvalidCredentials;
                u.sQryResult = u.QryResult.ToString();
                u.UID = -999;
                return u;
            }

            u.UID = Convert.ToInt32(ds.Tables["dtUser"].Rows[0]["UID"]);
            u.Email = ds.Tables["dtUser"].Rows[0]["Email"].ToString();
            u.Password = NaveoOneLib.Services.BaseService.DecryptMe(ds.Tables["dtUser"].Rows[0]["Password"].ToString());
            u.LoginAttempt = Convert.ToInt32(ds.Tables["dtUser"].Rows[0]["LoginAttempt"]);
            if (!String.IsNullOrEmpty(ds.Tables["dtUser"].Rows[0]["UserToken"].ToString()))   //installer only
                u.UserToken = new Guid(ds.Tables["dtUser"].Rows[0]["UserToken"].ToString());
            u.ExternalAccess = String.IsNullOrEmpty(ds.Tables["dtUser"].Rows[0]["ExternalAccess"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtUser"].Rows[0]["ExternalAccess"]);
            u.TwoStepAttempts = String.IsNullOrEmpty(ds.Tables["dtUser"].Rows[0]["TwoStepAttempts"].ToString()) ? 0 : Convert.ToInt32(ds.Tables["dtUser"].Rows[0]["TwoStepAttempts"]);
            u.TwoStepVerif = ds.Tables["dtUser"].Rows[0]["TwoStepVerif"].ToString();
            u.PswdExpiryDate = String.IsNullOrEmpty(ds.Tables["dtUser"].Rows[0]["PswdExpiryDate"].ToString()) ? (DateTime?)null : (DateTime)ds.Tables["dtUser"].Rows[0]["PswdExpiryDate"];
            u.PswdExpiryWarnDate = String.IsNullOrEmpty(ds.Tables["dtUser"].Rows[0]["PswdExpiryWarnDate"].ToString()) ? (DateTime?)null : (DateTime)ds.Tables["dtUser"].Rows[0]["PswdExpiryWarnDate"];
            u.Status_b = ds.Tables["dtUser"].Rows[0]["Status_b"].ToString();
            u.Names = ds.Tables["dtUser"].Rows[0]["Names"].ToString();
            u.LastName = ds.Tables["dtUser"].Rows[0]["LastName"].ToString();

            if (ds.Tables.Contains("dtAuditTraceLogin") && ds.Tables["dtAuditTraceLogin"].Rows.Count > 0)
                u.lastLogin = (DateTime)ds.Tables["dtAuditTraceLogin"].Rows[0]["accessedOn"];

            if (ds.Tables.Contains("dtAuditTraceLogout") && ds.Tables["dtAuditTraceLogout"].Rows.Count > 0)
                if (ds.Tables["dtAuditTraceLogout"].Rows.Count > 0)
                    u.lastLogout = (DateTime)ds.Tables["dtAuditTraceLogout"].Rows[0]["accessedOn"];

            var systemAdminEmail = globalParamsService.GetGlobalParamsByName("SystemAdminEmail", sConnStr);
            var systemAdminPhone = globalParamsService.GetGlobalParamsByName("SystemAdminPhone", sConnStr);
            u.SystemAdminEmail = systemAdminEmail == null ? "" : ((GlobalParam)systemAdminEmail).PValue;
            u.SystemAdminPhone = systemAdminPhone == null ? 0 : Convert.ToInt32(((GlobalParam)systemAdminPhone).PValue);

            u.bTwoWayAuthEnabled = false;
            if (ds.Tables.Contains("dtUserSec"))
                if (ds.Tables["dtUserSec"].Rows.Count > 0)
                    if (ds.Tables["dtUserSec"].Rows[0]["PValue"].ToString().ToUpper() == "YES")
                        u.bTwoWayAuthEnabled = true;

            if (ds.Tables.Contains("dtRoles"))
            {
                foreach (DataRow r in ds.Tables["dtRoles"].Rows)
                {
                    Role role = new Role();
                    role.RoleID = Convert.ToInt32(r["iID"]);
                    role.Description = r["Description"].ToString();
                    role.UserID = u.UID;
                    u.lRoles.Add(role);
                }
            }

            if (ds.Tables.Contains("dtZoneHeader"))
            {
                foreach (DataRow r in ds.Tables["dtZoneHeader"].Rows)
                {

                    u.ZoneID = Convert.ToInt32(r["ZoneID"]);
                    u.zoneDesc = r["Description"].ToString();
                }
            }

            if (ds.Tables.Contains("dtUserExt"))
            {
                foreach (DataRow r in ds.Tables["dtUserExt"].Rows)
                {
                    UserExtension uE = new UserExtension();
                    uE.UserID = Convert.ToInt32(r["UserID"]);
                    uE.GMApprovalID = Convert.ToInt32(r["GMApprovalID"]);
                    u.userExtension = uE;
                }
            }

            if (ds.Tables.Contains("PlnLKpVal"))
            {
                foreach (DataRow r in ds.Tables["PlnLKpVal"].Rows)
                {

                    u.PlnLkpVal = Convert.ToInt32(r["VID"]);
                    u.PlnKpValDesc = r["Name"].ToString();
                }
            }

            if (bHasDetails)
            {
                u.Username = ds.Tables["dtUser"].Rows[0]["Username"].ToString();
                u.Tel = ds.Tables["dtUser"].Rows[0]["Tel"].ToString();
                u.MobileNo = ds.Tables["dtUser"].Rows[0]["MobileNo"].ToString();
                u.DateJoined = ds.Tables["dtUser"].Rows[0]["DateJoined"].ToString() != "" ? Convert.ToDateTime(ds.Tables["dtUser"].Rows[0]["DateJoined"]) : Constants.NullDateTime;
                u.PasswordUpdateddate = ds.Tables["dtUser"].Rows[0]["PasswordUpdateddate"].ToString() != "" ? Convert.ToDateTime(ds.Tables["dtUser"].Rows[0]["PasswordUpdateddate"]) : Constants.NullDateTime;
                u.AccessList = ds.Tables["dtUser"].Rows[0]["AccessList"].ToString();
                u.StoreUnit_b = ds.Tables["dtUser"].Rows[0]["StoreUnit_b"].ToString();
                u.UType_cbo = ds.Tables["dtUser"].Rows[0]["UType_cbo"].ToString();
                u.CreatedDate = (DateTime)ds.Tables["dtUser"].Rows[0]["CreatedDate"];
                u.Dummy1 = ds.Tables["dtUser"].Rows[0]["Dummy1"].ToString();
                u.UsrTypeID = String.IsNullOrEmpty(ds.Tables["dtUser"].Rows[0]["UsrTypeID"].ToString()) ? 0 : Convert.ToInt32(ds.Tables["dtUser"].Rows[0]["UsrTypeID"]);
                u.UsrTypeDesc = ds.Tables["dtUser"].Rows[0]["UsrTypeDesc"].ToString();
                u.AccessTemplateId = String.IsNullOrEmpty(ds.Tables["dtUser"].Rows[0]["AccessTemplateId"].ToString()) ? 0 : Convert.ToInt32(ds.Tables["dtUser"].Rows[0]["AccessTemplateId"]);
                u.MaxDayMail = String.IsNullOrEmpty(ds.Tables["dtUser"].Rows[0]["MaxDayMail"].ToString()) ? 25 : Convert.ToInt32(ds.Tables["dtUser"].Rows[0]["MaxDayMail"]);
                u.LoginCount = Convert.ToInt32(ds.Tables["dtUser"].Rows[0]["LoginCount"]);

                if (!String.IsNullOrEmpty(ds.Tables["dtUser"].Rows[0]["CreatedBy"].ToString()))
                    u.CreatedBy = Convert.ToInt32(ds.Tables["dtUser"].Rows[0]["CreatedBy"]);
                if (!String.IsNullOrEmpty(ds.Tables["dtUser"].Rows[0]["UpdatedBy"].ToString()))
                    u.UpdatedBy = Convert.ToInt32(ds.Tables["dtUser"].Rows[0]["UpdatedBy"]);

                foreach (DataRow dr in ds.Tables["dtMatrix"].Rows)
                    u.lMatrix.Add(new MatrixService().AssignMatrix(dr));

                if (ds.Tables["dtUser"].Rows[0]["UpdatedBy"].ToString() != "")
                    u.UpdatedDate = (DateTime)ds.Tables["dtUser"].Rows[0]["UpdatedDate"];
                else
                    u.UpdatedDate = Constants.NullDateTime;

                u.ZoneID = String.IsNullOrEmpty(ds.Tables["dtUser"].Rows[0]["ZoneID"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtUser"].Rows[0]["ZoneID"]);
                u.PlnLkpVal = String.IsNullOrEmpty(ds.Tables["dtUser"].Rows[0]["PlnLkpVal"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtUser"].Rows[0]["PlnLkpVal"]);

                u.MobileAccess = String.IsNullOrEmpty(ds.Tables["dtUser"].Rows[0]["MobileAccess"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtUser"].Rows[0]["MobileAccess"]);
            }

            #region UserResult

            //sPassword = NaveoOneLib.Services.BaseService.RSADecrypt(sPassword);


            var maxLoginAttemptRaw = globalParamsService.GetGlobalParamsByName("MaxLoginAttempts", sConnStr);
            int maxLoginAttempt = maxLoginAttemptRaw == null ? 999 : Convert.ToInt32(maxLoginAttemptRaw.PValue);
            if (String.IsNullOrEmpty(u.TwoStepVerif))
            {
                //Forcing edit mode. Neew to do in a better way. quickfix for windows ui
                if (u.LoginAttempt >= maxLoginAttempt)
                {
                    u.QryResult = User.UserResult.Locked;
                    u.Status_b = "LK";
                    lockUser(u.UID, sConnStr);
                }
                else if (sPassword == User.EditModePswd)
                    u.QryResult = User.UserResult.Succeeded;
                else if (u.Password == sPassword)
                    u.QryResult = User.UserResult.Succeeded;
                else
                {
                    u.QryResult = User.UserResult.InvalidCredentials;
                    Boolean b = IncrementLoginAttempt(u.UID, sConnStr);
                    if (b)
                        u.LoginAttempt = u.LoginAttempt + 1;
                }
            }
            else
            {
                //TwoStepVerif
                if (u.TwoStepAttempts >= maxLoginAttempt)
                {
                    u.QryResult = User.UserResult.Locked;
                    u.Status_b = "LK";
                    lockUser(u.UID, sConnStr);
                }
                else if (u.TwoStepVerif == sPassword)
                {
                    u.QryResult = User.UserResult.Succeeded;
                    ResetTwoStepToken(u.UID, sConnStr);
                }
                else
                {
                    u.QryResult = User.UserResult.InvalidCredentials;
                    Boolean b = IncrementTwoStepToken(u.UID, sConnStr);
                    if (b)
                        u.TwoStepAttempts = u.TwoStepAttempts + 1;
                }
            }

            if (u.QryResult == User.UserResult.Succeeded)
                switch (u.Status_b)
                {
                    case "IN":
                        u.QryResult = User.UserResult.Inactive;
                        break;

                    case "TE":
                        u.QryResult = User.UserResult.TokenExpected;
                        break;

                    case "LK":
                        u.QryResult = User.UserResult.Locked;
                        break;

                    case "CP":
                        u.QryResult = User.UserResult.PasswordExpired;
                        break;
                }

            if (u.QryResult == User.UserResult.Succeeded)
            {
                if (u.PswdExpiryWarnDate.HasValue)
                    if (DateTime.Now > u.PswdExpiryWarnDate)
                        u.QryResult = User.UserResult.SucceededButPswdToExpire;

                if (u.PswdExpiryDate.HasValue)
                    if (DateTime.Now > u.PswdExpiryDate)
                        u.QryResult = User.UserResult.PasswordExpired;
            }

            if (u.QryResult == User.UserResult.Succeeded)
            {
                UpdateUserDetails(u.UID, sConnStr);
            }

            #endregion

            u.Password = String.Empty;
            u.sQryResult = u.QryResult.ToString();
            return u;
        }

        List<User> GetUsers(DataSet ds)
        {
            List<User> users = new List<User>();
            DataTable dt = ds.Tables["dtUser"];
            foreach (DataRow row in dt.Rows)
            {
                User u = new User();
                u.UID = Convert.ToInt32(row["UID"]);
                u.Email = row["Email"].ToString();
                u.Password = NaveoOneLib.Services.BaseService.DecryptMe(row["Password"].ToString());
                u.LoginAttempt = Convert.ToInt32(row["LoginAttempt"]);
                if (!String.IsNullOrEmpty(row["UserToken"].ToString()))   //installer only
                    u.UserToken = new Guid(row["UserToken"].ToString());
                u.ExternalAccess = String.IsNullOrEmpty(row["ExternalAccess"].ToString()) ? (int?)null : Convert.ToInt32(row["ExternalAccess"]);
                u.TwoStepAttempts = String.IsNullOrEmpty(row["TwoStepAttempts"].ToString()) ? 0 : Convert.ToInt32(row["TwoStepAttempts"]);
                u.TwoStepVerif = row["TwoStepVerif"].ToString();
                u.PswdExpiryDate = String.IsNullOrEmpty(row["PswdExpiryDate"].ToString()) ? (DateTime?)null : (DateTime)row["PswdExpiryDate"];
                u.PswdExpiryWarnDate = String.IsNullOrEmpty(row["PswdExpiryWarnDate"].ToString()) ? (DateTime?)null : (DateTime)row["PswdExpiryWarnDate"];
                u.Status_b = row["Status_b"].ToString();
                u.Names = row["Names"].ToString();
                u.LastName = row["LastName"].ToString();
                u.AccessedOn = row["AccessedOn"].ToString();
                users.Add(u);

            }

            return users;

        }

        String sRoles(String f)
        {
            String sql = @"select r.* from GFI_SYS_UserRoles ur
	                    inner join GFI_SYS_Roles r on ur.RoleID = r.iID
	                    inner join GFI_SYS_User u on ur.UserID = u.UID
                    WHERE u." + f + " = ?";
            return sql;
        }
        public User GetUserByeMail(String Email, String Password, String sConnStr, Boolean bTwoStepVerify, Boolean bHasDetails, Boolean isSecure = false, String originDomain = "", IPAddress[] originIP = null)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = String.Empty;
            if (bHasDetails)
                sql = "SELECT * from GFI_SYS_User WHERE Email = ?";
            else
                sql = "SELECT UID, UserToken, Names, LastName, email, password, LoginAttempt, TwoStepAttempts, TwoStepVerif, ExternalAccess, PswdExpiryDate, PswdExpiryWarnDate, Status_b from GFI_SYS_User WHERE Email = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = Email;
            drSql["sTableName"] = "dtUser";
            dtSql.Rows.Add(drSql);

            if (bHasDetails)
            {
                sql = sGetMatrixUserByeMail(Email);
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtMatrix";
                dtSql.Rows.Add(drSql);
            }

            if (Password != User.SysIntWrongPassword)
            {
                sql = "select * from GFI_SYS_GlobalParams WHERE Paramname = 'TwoWayAuthenticationEnabledOnSSL'";
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtUserSec";
                dtSql.Rows.Add(drSql);

                sql = sRoles("Email");
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sParam"] = Email;
                drSql["sTableName"] = "dtRoles";
                dtSql.Rows.Add(drSql);
            }
            sql = getAccessSQL(Email, true);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = Email;
            drSql["sTableName"] = "dtAuditTraceLogin";
            dtSql.Rows.Add(drSql);

            sql = getAccessSQL(Email, false);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = Email;
            drSql["sTableName"] = "dtAuditTraceLogout";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            User u = GetUser(ds, Password, sConnStr, bHasDetails);

            #region check bTwoStepVerify
            GlobalParamsService global = new GlobalParamsService();
            String TwoWayAuthenticationEnabledOnSSL = global.GetGlobalParamsByName("TwoWayAuthenticationEnabledOnSSL", sConnStr).PValue;
            String[] PublicURL = global.GetGlobalParamsByName("PublicURL", sConnStr).PValue.Split(';');
            if (!bTwoStepVerify)
            {
                if (TwoWayAuthenticationEnabledOnSSL == "1" || TwoWayAuthenticationEnabledOnSSL.ToLower() == "yes")
                {
                    if (isSecure)
                    {

                        if (PublicURL.Length > 1 && (originDomain != "" && !this.isIpInArr(originDomain, PublicURL)))
                        {
                            if (u.ExternalAccess == 1)
                            {
                                bTwoStepVerify = true;
                            }
                            else
                            {
                                u.QryResult = User.UserResult.Locked;
                            }

                        }

                    }
                    else
                    {
                        u.QryResult = User.UserResult.InvalidCredentials;
                    }
                }
            }
            #endregion


            if (u.QryResult != User.UserResult.TokenExpected)
                if (bTwoStepVerify)
                    if (u.QryResult == User.UserResult.Succeeded)
                    {
                        String s = GenerateTwoStepToken(u.UID, sConnStr);
                        if (s.Length > 2)
                        {
                            //send mail
                            MailSender ms = new MailSender();
                            String sMail = ms.SendMail(Email, "Token Credentials", "Please login using one time password " + s, String.Empty, sConnStr);
                            if (sMail == "Mail Sent")
                                u.QryResult = User.UserResult.TokenExpected;
                            else
                            {
                                ResetTwoStepToken(u.UID, sConnStr);
                                u.QryResult = User.UserResult.CouldNotMailToken;
                            }
                        }
                    }
            return u;
        }

        public BaseModel GetMaintenanceLogIn(Guid UserToken, string ServerName)
        {
            BaseModel baseModel = new BaseModel();



            Connection c = new Connection(NaveoOneLib.Constant.NaveoEntity.MaintDB);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = "select 'GetUser' as TableName\r\n";
            sql += "select * from dbo.\"FleetUser\" fu, dbo.\"FleetServer\" fs where fu.\"FMSUserTOKEN\" = '" + UserToken + "' AND fu.\"FleetServerId\" = fs.\"Id\" AND fs.\"Name\" = '" + ServerName + "'";


            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, NaveoOneLib.Constant.NaveoEntity.MaintDB);

            DataTable dtCustomerVehicle = ds.Tables["GetUser"];

            if (dtCustomerVehicle.Rows.Count > 0)
            {
                baseModel.data = true;
            }
            else
            {
                baseModel.data = false;
            }
            return baseModel;
        }
        private Boolean isIpInArr(String currIp, string[] ips)
        {
            foreach (string ip in ips)
            {
                if (currIp.Contains(ip)) return true;
            }

            return false;
        }

        public bool UpdateUserStatus(User u, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_User";
            dt.Columns.Add("Status_b", u.Status_b.GetType());
            DataRow dr = dt.NewRow();
            dr["Status_b"] = u.Status_b;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "UID = '" + u.UID + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }
        public BaseModel logoutUserByToken(String Token, String sConnStr)
        {
            BaseModel model = new BaseModel();

            String sql = "INSERT INTO GFI_SYS_AuditTraceLogin (Email, sAction, accessedOn, IsAccessGranted) VALUES ((SELECT email FROM GFI_SYS_User WHERE userToken='" + Token + "'),'Logout', getDate(), 1)";

            int iConn = new Connection(sConnStr).dbExecute(sql, sConnStr);
            if (iConn < 1) model.data = false;
            else model.data = true;

            return model;
        }

        public String getAccessSQL(String Email, Boolean login)
        {
            String sql = "SELECT * FROM (SELECT *, ROW_NUMBER() OVER (ORDER BY accessedOn DESC) as r FROM GFI_SYS_AuditTraceLogin WHERE email = '" + Email + "' AND sAction='" + (login ? "Login" : "Logout") + "') as t WHERE t.r = " + (login ? 2 : 1);
            return sql;
        }
        public User GetUserByeMailToken(String Email, String sToken, String sConnStr)
        {
            User u = GetUserByeMail(Email, "TokenPassXXX", sConnStr, false, false);
            if (u.QryResult == User.UserResult.InvalidCredentials)
                if (u.UID > 0) //Chk if InvalidCredentials is not for user that does not exist
                    if (u.TwoStepVerif == sToken)
                        u.QryResult = User.UserResult.Succeeded;
            return u;
        }
        public User GetUserInstaller(String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"SELECT UID, Username, 
                                        Names, 
                                        Email, 
                                        Tel, 
                                        MobileNo, 
                                        DateJoined, 
                                        Status_b, 
                                        Password, 
                                        'PreviousPassword' PreviousPassword,
                                        GetDate() PasswordUpdateddate,
                                        AccessList, 
                                        StoreUnit_b, 
                                        UType_cbo, 
                                        CreatedBy, 
                                        CreatedDate, 
                                        UpdatedBy, 
                                        UpdatedDate, 
                                        Dummy1,
                                        1 as UsrTypeID, '' as UsrTypeDesc,
                                        0 AS AccessTemplateId, 25 as MaxDayMail, 0 as LoginCount,
                                        0 AS LoginAttempt
                                        ,0 ZoneID, 0 PlnLkpVal, 0 MobileAccess, 0 ExternalAccess
                                        , null TwoStepVerif, 0 TwoStepAttempts, '' UserToken, GetDate() PswdExpiryDate, GetDate() PswdExpiryWarnDate
                                        , 'LastName' LastName
                                        from GFI_SYS_User
                                        WHERE Email = ? or email = 'INSTALLER'";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = "Installer@naveo.mu";
            drSql["sTableName"] = "dtUser";
            dtSql.Rows.Add(drSql);

            sql = sGetMatrixUserByeMail("Installer@naveo.mu");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);
            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            return GetUser(ds, String.Empty, sConnStr, true);
        }
        public BaseModel SaveUser(User uUser, Boolean bInsert, String sConnStr)
        {
            BaseModel baseModel = new BaseModel();
            Boolean bResult = false;

            if (bInsert)
            {
                uUser.oprType = DataRowState.Added;
                User u = GetUserByeMail(uUser.Email, User.EditModePswd, sConnStr, false, false);
                if (u.UID != -999) //invalid email
                {
                    baseModel.data = bResult;
                    baseModel.errorMessage = "The user with email " + uUser.Email.ToUpper() + " already exist on the system!!!!! ";
                    return baseModel;
                }
            }

            if (!IsValidEmailAddress(uUser.Email))
            {
                baseModel.errorMessage = "Invalid email address";
                return baseModel;
            }

            if (uUser.Password != null)
            {
                if (!isValidPassword(uUser.Password))
                {
                    baseModel.errorMessage = "Password complexity not met";
                    return baseModel;
                }
            }
            User old = GetUserByeMail(uUser.Email, String.Empty, sConnStr, false, false);

            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_User";
            dt.Columns.Add("UID", typeof(int));
            dt.Columns.Add("Username", typeof(String));
            dt.Columns.Add("Names", typeof(String));
            dt.Columns.Add("LastName", typeof(String));
            dt.Columns.Add("Email", typeof(String));
            dt.Columns.Add("Tel", typeof(String));
            dt.Columns.Add("MobileNo", typeof(String));
            dt.Columns.Add("DateJoined", typeof(DateTime));
            dt.Columns.Add("Status_b", typeof(String));
            dt.Columns.Add("Password", typeof(String));
            dt.Columns.Add("AccessList", typeof(String));
            dt.Columns.Add("StoreUnit_b", typeof(String));
            dt.Columns.Add("UType_cbo", typeof(String));
            dt.Columns.Add("CreatedBy", typeof(int));
            dt.Columns.Add("CreatedDate", typeof(DateTime));
            dt.Columns.Add("UpdatedDate", typeof(DateTime));
            dt.Columns.Add("UpdatedBy", typeof(int));
            dt.Columns.Add("Dummy1", typeof(String));
            dt.Columns.Add("LoginCount", typeof(int));
            dt.Columns.Add("LoginAttempt", typeof(int));
            dt.Columns.Add("AccessTemplateId", typeof(int));
            dt.Columns.Add("UsrTypeID", typeof(int));
            dt.Columns.Add("UsrTypeDesc", typeof(String));
            dt.Columns.Add("MaxDayMail", typeof(int));
            dt.Columns.Add("PreviousPassword", typeof(String));
            dt.Columns.Add("PasswordUpdateddate", typeof(DateTime));
            dt.Columns.Add("ZoneID", typeof(int));
            dt.Columns.Add("PlnLkpVal", typeof(int));
            dt.Columns.Add("MobileAccess", typeof(int));
            dt.Columns.Add("ExternalAccess", typeof(int));
            dt.Columns.Add("TwoStepVerif", typeof(String));
            dt.Columns.Add("TwoStepAttempts", typeof(int));
            dt.Columns.Add("PswdExpiryDate", typeof(DateTime));
            dt.Columns.Add("PswdExpiryWarnDate", typeof(DateTime));
            dt.Columns.Add("oprType", typeof(DataRowState));

            DataRow dr = dt.NewRow();
            dr["UID"] = uUser.UID;
            dr["Username"] = uUser.Username;
            dr["Names"] = uUser.Names;
            dr["LastName"] = uUser.LastName;
            dr["Email"] = uUser.Email;
            dr["Tel"] = uUser.Tel;
            dr["MobileNo"] = uUser.MobileNo;
            dr["Status_b"] = uUser.Status_b;
            dr["AccessList"] = uUser.AccessList;
            dr["StoreUnit_b"] = uUser.StoreUnit_b;
            dr["UType_cbo"] = uUser.UType_cbo;
            dr["Dummy1"] = uUser.Dummy1;
            dr["LoginCount"] = uUser.LoginCount;
            dr["LoginAttempt"] = uUser.LoginAttempt;
            dr["AccessTemplateId"] = uUser.AccessTemplateId;
            dr["PreviousPassword"] = uUser.PreviousPassword;
            dr["AccessTemplateId"] = uUser.AccessTemplateId;
            dr["MaxDayMail"] = uUser.MaxDayMail;
            dr["UsrTypeID"] = 0;// uUser.UsrTypeID;
            dr["UsrTypeDesc"] = "Std User";// uUser.UsrTypeDesc;
            dr["ZoneID"] = uUser.ZoneID != null ? uUser.ZoneID : (object)DBNull.Value;
            dr["PlnLkpVal"] = uUser.PlnLkpVal != null ? uUser.PlnLkpVal : (object)DBNull.Value;
            dr["MobileAccess"] = uUser.MobileAccess != null ? uUser.MobileAccess : (object)DBNull.Value;
            dr["ExternalAccess"] = uUser.ExternalAccess != null ? uUser.ExternalAccess : (object)DBNull.Value;
            dr["TwoStepVerif"] = uUser.TwoStepVerif != null ? uUser.TwoStepVerif : (object)DBNull.Value;
            dr["TwoStepAttempts"] = uUser.TwoStepAttempts;
            dr["PswdExpiryDate"] = uUser.PswdExpiryDate != null ? uUser.PswdExpiryDate : (object)DBNull.Value;
            dr["PswdExpiryWarnDate"] = uUser.PswdExpiryWarnDate != null ? uUser.PswdExpiryWarnDate : (object)DBNull.Value;
            dr["oprType"] = uUser.oprType;

            if (uUser.DateJoined.Year > 2016)
                dr["DateJoined"] = uUser.DateJoined;

            if (uUser.PasswordUpdateddate.Year > 2016)
                dr["PasswordUpdateddate"] = uUser.PasswordUpdateddate;

            if (uUser.Password != null)
            {
                if (uUser.Password == User.EditModePswd)
                    dt.Columns.Remove("Password");
                else
                    dr["Password"] = BaseService.EncryptMe(uUser.Password);
            }
            if (bInsert)
            {
                dr["CreatedBy"] = uUser.CreatedBy != null ? uUser.CreatedBy : (object)DBNull.Value;
                dr["CreatedDate"] = DateTime.Now;
            }
            else
            {
                dr["UpdatedDate"] = DateTime.Now;
                dr["UpdatedBy"] = uUser.UpdatedBy != null ? uUser.UpdatedBy : (object)DBNull.Value;

                dt.Columns.Remove("CreatedBy");
                dt.Columns.Remove("CreatedDate");
                if (uUser.Password == User.EditModePswd)
                    if (dt.Columns.Contains("Password"))
                        dt.Columns.Remove("Password");
            }

            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "UID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uUser.UID;

            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            //if (bInsert)
            //    foreach (Matrix m in uUser.lMatrix)
            //        m.oprType = DataRowState.Added;

            DataTable dtMatrix = new myConverter().ListToDataTable<Matrix>(uUser.lMatrix);
            dtMatrix.TableName = "GFI_SYS_GroupMatrixUser";
            if (old.lMatrix != null)
            {
                foreach (Matrix m in old.lMatrix)
                {
                    DataRow drm = dtMatrix.NewRow();
                    drm["iID"] = m.iID;
                    drm["oprType"] = DataRowState.Deleted;
                }
            }

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 2;
            drCtrl["TblName"] = dtMatrix.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "MID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "SET";
            else
                drCtrl["NextIdValue"] = uUser.UID;
            drCtrl["NextIdFieldName"] = "iID";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtMatrix);

            //Roles
            if (uUser.lRoles.Count > 0)
            {
                DataTable dtRoles = new myConverter().ListToDataTable<Role>(uUser.lRoles);
                dtRoles.Columns.Remove("Description");
                dtRoles.Columns.Remove("lPermissions");
                dtRoles.Columns.Remove("lMatrix");
                dtRoles.TableName = "GFI_SYS_UserRoles";
                dtRoles.DefaultView.Sort = "oprType desc";
                dtRoles = dtRoles.DefaultView.ToTable();

                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 3;
                drCtrl["TblName"] = dtRoles.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "RoleID, UserID";
                drCtrl["KeyIsIdentity"] = "FALSE";
                drCtrl["NextIdAction"] = "SET";
                drCtrl["NextIdFieldName"] = "UserID";
                drCtrl["ParentTblName"] = dt.TableName;
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtRoles);
            }

            if (uUser.userExtension != null)
            {
                DataTable dtUserExt = new DataTable();
                dtUserExt.TableName = "GFI_SYS_UserExt";
                dtUserExt.Columns.Add("UserID", typeof(int));
                dtUserExt.Columns.Add("GMApprovalID", typeof(int));
                dtUserExt.Columns.Add("oprType", typeof(DataRowState));

                DataRow drUserExt = dtUserExt.NewRow();
                drUserExt["UserID"] = uUser.userExtension.UserID;
                drUserExt["GMApprovalID"] = uUser.userExtension.GMApprovalID;
                drUserExt["oprType"] = uUser.userExtension.oprType;
                dtUserExt.Rows.Add(drUserExt);

                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 4;
                drCtrl["TblName"] = dtUserExt.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "UserID";
                drCtrl["KeyIsIdentity"] = "FALSE";
                drCtrl["NextIdAction"] = "SET";
                drCtrl["NextIdFieldName"] = "UserID";
                drCtrl["ParentTblName"] = dt.TableName;
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtUserExt);
            }

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResult.Tables["CTRLTBL"];

            DataRow[] dRowId = null;

            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");
                if (dRow.Length > 0)
                {
                    baseModel.dbResult = dsResult;
                    baseModel.dbResult.Tables["CTRLTBL"].TableName = "ResultCtrlTbl";
                    baseModel.dbResult.Merge(dsProcess);
                    bResult = false;
                }
                else
                {
                    bResult = true;
                    dRowId = dtCtrl.Select();
                }
            }
            else
            {
                bResult = true;
            }

            if (bResult)
            {
                #region insert in live connections
                try
                {
                    LiveConnections lc = new LiveConnections();

                    List<int> lids = new List<int>();
                    int uid = int.Parse(dRowId[0].ItemArray[10].ToString());
                    lids.Add(uid);
                    List<Guid> lusertoken = new NaveoOneLib.Services.Users.UserService().GetUserTokensById(lids, sConnStr);

                    lc.UserToken = lusertoken[0];
                    lc.FleetLogin = 0;
                    lc.MaintenanceLogin = 0;
                    lc.oprType = DataRowState.Added;

                    BaseModel b = new NaveoOneLib.Services.Users.UserService().SaveLiveConnections(lc, true, sConnStr);
                }
                catch (Exception ex)
                {

                }
                #endregion

                #region insert Maintenance
                try
                {
                    UserMaintenanceService.MaintUser maintUser = new UserMaintenanceService.MaintUser();
                    UserMaintenanceService maintUserService = new UserMaintenanceService();
                    maintUser.FleetUserId = int.Parse(dRowId[0].ItemArray[10].ToString());
                    maintUser.GMSPassword = "FleetMS";
                    maintUser.GMSPassword = "Nave0@2019";
                    maintUser.GMSUsername = uUser.Email;
                    maintUser.FirstName = uUser.Names;
                    maintUser.LastName = uUser.LastName;
                    maintUser.Ttile = uUser.Dummy1;
                    //maintUser.IsActive = true;
                    maintUser.oprType = DataRowState.Added;

                    maintUserService.SaveMaintUser(maintUser, bInsert, sConnStr);
                }
                catch (Exception ex)
                {
                    string responseString = ex.ToString();
                    System.IO.File.AppendAllText(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "\\Registers\\maintenance.dat", "\r\n User Creation \r\n" + responseString + "\r\n");
                }
                #endregion
            }

            baseModel.data = bResult;
            return baseModel;
        }

        public User GetUserById(int UID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = "SELECT * from GFI_SYS_User WHERE UID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = UID.ToString();
            drSql["sTableName"] = "dtUser";
            dtSql.Rows.Add(drSql);
            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            DataTable dt = ds.Tables["dtUser"];
            return this.GetUser(ds, ds.Tables["dtUser"].Rows[0]["Password"].ToString(), sConnStr, false);
        }
        public User GetUserById(int UID, String sPassword, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = "SELECT * from GFI_SYS_User WHERE UID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = UID.ToString();
            drSql["sTableName"] = "dtUser";
            dtSql.Rows.Add(drSql);

            sql = "SELECT * from GFI_SYS_UserExt WHERE UserID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = UID.ToString();
            drSql["sTableName"] = "dtUserExt";
            dtSql.Rows.Add(drSql);

            sql = sGetMatrixUserByUserID(UID);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);

            sql = sRoles("UID");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = UID;
            drSql["sTableName"] = "dtRoles";
            dtSql.Rows.Add(drSql);

            //ZoneHeader
            sql = "select ZoneID,Description from GFI_FLT_ZoneHeader where ZoneID = (select ZoneID from GFI_SYS_USER where UID = " + UID + ")";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZoneHeader";
            dtSql.Rows.Add(drSql);

            //Planning LkP VALUE
            sql = "select * from GFI_SYS_LookUpValues where VID = (select PlnLKpVal from GFI_SYS_USER where UID = " + UID + ")";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "PlnLKpVal";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return GetUser(ds, sPassword, sConnStr, true);
        }

        public List<User> GetInactiveUsers(String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();
            GlobalParamsService gs = new GlobalParamsService();

            String sql = @"
--GetInactiveUsers

SELECT * FROM GFI_SYS_User u JOIN(
	SELECT GFI_SYS_User.Email, Max(accessedOn) as accessedOn FROM GFI_SYS_User
	INNER JOIN GFI_SYS_AuditTraceLogin ON GFI_SYS_User.Email = GFI_SYS_AuditTraceLogin.Email
	WHERE GFI_SYS_User.Email in (
		SELECT Email FROM (
			SELECT * FROM(
				SELECT MAX(accessedOn) as a, GFI_SYS_User.Email from GFI_SYS_User 
				INNER JOIN GFI_SYS_AuditTraceLogin ON GFI_SYS_User.Email = GFI_SYS_AuditTraceLogin.Email 
				WHERE UserToken IS NOT NULL AND sAction = 'Login'
				GROUP BY GFI_SYS_User.Email
			) AS b WHERE DATEDIFF(day, b.a, CURRENT_TIMESTAMP) > ?
		) AS c 
	) GROUP BY GFI_SYS_User.Email
) AS d ON u.Email = d.Email;
";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = gs.GetGlobalParamsByName("InactiveUserDuration", sConnStr).PValue;
            drSql["sTableName"] = "dtUser";
            dtSql.Rows.Add(drSql);



            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return GetUsers(ds);
        }
        public User GetUserByUname(String uname, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();
            String sql = "SELECT * from GFI_SYS_User WHERE Username = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = uname;
            drSql["sTableName"] = "dtUser";
            dtSql.Rows.Add(drSql);

            sql = sGetMatrixUserByeMail(uname);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);

            sql = sRoles("Username");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = uname;
            drSql["sTableName"] = "dtRoles";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return GetUser(ds, String.Empty, sConnStr, true);
        }

        public List<Guid> GetUserTokensById(List<int> ids, String sConnStr)
        {
            List<Guid> users = new List<Guid>();
            ids.ForEach(id =>
            {
                Guid guid = UserService.GetUserToken(id, sConnStr);
                users.Add(guid);
            });
            return users;

        }

        public int GetUserID(Guid Token, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = "SELECT UID from GFI_SYS_User WHERE UserToken = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = Token;
            drSql["sTableName"] = "dtUser";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            int i = -1;
            if (ds != null)
                if (ds.Tables.Count > 0)
                    if (ds.Tables[0].Rows.Count > 0)
                        int.TryParse(ds.Tables[0].Rows[0][0].ToString(), out i);

            return i;
        }

        public static String GetEmailByUserToken(Guid Token, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = "SELECT email from GFI_SYS_User WHERE UserToken = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = Token;
            drSql["sTableName"] = "dtUser";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            String s = String.Empty;
            if (ds != null)
                if (ds.Tables.Count > 0)
                    if (ds.Tables[0].Rows.Count > 0)
                        s = ds.Tables[0].Rows[0][0].ToString();

            return s;
        }
        public static Guid GetUserToken(int UID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = "SELECT UserToken from GFI_SYS_User WHERE UID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = UID;
            drSql["sTableName"] = "dtUser";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            Guid g = new Guid();
            if (ds != null)
                if (ds.Tables.Count > 0)
                    if (ds.Tables[0].Rows.Count > 0)
                        Guid.TryParse(ds.Tables[0].Rows[0][0].ToString(), out g);

            return g;
        }

        String GeneratenewRandom()
        {
            Random generator = new Random();
            String r = generator.Next(0, 1000000).ToString("D6");
            if (r.Distinct().Count() == 1)
                GeneratenewRandom();

            return r;
        }
        String GenerateTwoStepToken(int UID, String sConnStr)
        {
            String s = GeneratenewRandom();
            String sql = "update GFI_SYS_User set TwoStepVerif = '" + s + "', TwoStepAttempts = 0, Status_b = 'TE' where UID = " + UID + " and TwoStepVerif is null";

            int iConn = new Connection(sConnStr).dbExecute(sql, sConnStr);
            if (iConn < 1)
                s = "-1";

            return s;
        }
        Boolean ResetTwoStepToken(int UID, String sConnStr)
        {
            String sql = "update GFI_SYS_User set TwoStepVerif = null, TwoStepAttempts = 0 where UID = " + UID;

            int iConn = new Connection(sConnStr).dbExecute(sql, sConnStr);
            if (iConn < 1)
                return false;

            return true;
        }
        Boolean IncrementTwoStepToken(int UID, String sConnStr)
        {
            String sql = "update GFI_SYS_User set TwoStepAttempts = TwoStepAttempts + 1 where UID = " + UID;

            int iConn = new Connection(sConnStr).dbExecute(sql, sConnStr);
            if (iConn < 1)
                return false;

            return true;
        }
        Boolean UpdateUserDetails(int UID, String sConnStr)
        {
            String sql = "update GFI_SYS_User set DateJoined = '" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss") + "',LoginAttempt = 0 ,LoginCount= LoginCount +1 where UID = " + UID;

            int iConn = new Connection(sConnStr).dbExecute(sql, sConnStr);
            if (iConn < 1) return false;

            return true;
        }
        Boolean ResetLoginAttempt(int UID, String sConnStr)
        {
            String sql = "update GFI_SYS_User set LoginAttempt = 0 where UID = " + UID;

            int iConn = new Connection(sConnStr).dbExecute(sql, sConnStr);
            if (iConn < 1)
                return false;

            return true;
        }
        Boolean IncrementLoginAttempt(int UID, String sConnStr)
        {
            String sql = "update GFI_SYS_User set LoginAttempt = LoginAttempt + 1 where UID = " + UID;

            int iConn = new Connection(sConnStr).dbExecute(sql, sConnStr);
            if (iConn < 1)
                return false;

            return true;
        }
        Boolean lockUser(int UID, String sConnStr)
        {
            String sql = "update GFI_SYS_User set Status_b = 'LK' where UID = " + UID;

            int iConn = new Connection(sConnStr).dbExecute(sql, sConnStr);
            if (iConn < 1)
                return false;

            return true;
        }
        public Boolean UnlockUser(int UID, String sConnStr)
        {
            String sql = "update GFI_SYS_User set Status_b = 'AC', TwoStepAttempts = 0 where UID = " + UID;

            int iConn = new Connection(sConnStr).dbExecute(sql, sConnStr);
            if (iConn < 1)
                return false;

            return true;
        }
        public Boolean ChangePassword(String sToken, String sOldPassword, String sNewPassword, String sConnStr, UserExpiryDetails ud = null)
        {
            if (!isValidPassword(sNewPassword))
                return false;

            String newPswd = BaseService.EncryptMe(sNewPassword);
            String expiryDetails = "PswdExpiryDate = null";
            if (ud != null)
            {
                expiryDetails = "PswdExpiryDate='" + DateTime.Now.AddDays(ud.ExpiryDay).ToString("yyyy/MM/dd HH:mm:ss") + "', PswdExpiryWarnDate='" + DateTime.Now.AddDays(ud.ExpiryDay - ud.ExpiryWarnDay).ToString("yyyy/MM/dd HH:mm:ss") + "'";
            }
            String sql = "update GFI_SYS_User set Password = '" + newPswd + "', Status_b = 'AC', " + expiryDetails + " where UserToken = '" + sToken + "'";
            if (sOldPassword != User.BypassOldPswd)
            {
                String oldPswd = BaseService.EncryptMe(sOldPassword);
                sql += " and Password = '" + oldPswd + "'";
            }

            try
            {
                int iConn = new Connection(sConnStr).dbExecute(sql, sConnStr);
                if (iConn < 1)
                    return false;
            }
            catch { return false; }

            return true;
        }

        public void AllowMobileAccess(int UID, String sConnStr)
        {
            String sql = "update GFI_SYS_User set MobileAccess = 1 where UID = " + UID + " and (MobileAccess != 1 or MobileAccess is null)";
            new Connection(sConnStr).dbExecute(sql, sConnStr);
        }
        public Boolean isMobileUser(String email, String sConnStr)
        {
            //Connection c = new Connection(sConnStr);
            //DataTable dtSql = c.dtSql();
            //DataRow drSql = dtSql.NewRow();
            //String sql = "SELECT 1 from GFI_SYS_User WHERE email = ? and MobileAccess = 1";
            //drSql = dtSql.NewRow();
            //drSql["sSQL"] = sql;
            //drSql["sParam"] = email;
            //drSql["sTableName"] = "dtUser";
            //dtSql.Rows.Add(drSql);
            //DataSet ds = c.GetDataDS(dtSql, sConnStr);

            String sql = "SELECT 1 from GFI_SYS_User WHERE email = '" + email + "' and MobileAccess = 1";
            DataTable dt = new Connection(sConnStr).GetDataDT(sql, sConnStr);
            if (dt.Rows.Count > 0)
                return true;
            return false;
        }

        public String RequestPasswordReset(int UID, String email, Boolean bInsert, String sConnStr, String sServerName)
        {
            User u = GetUserByeMail(email, User.EditModePswd, sConnStr, false, false);
            if (u.UID == -999) //invalid email
                return "Invalid email";

            String sPassword = CreateRandomPassword(8);
            String pswd = BaseService.EncryptMe(sPassword);

            String sql = "update GFI_SYS_User set Password = '" + pswd + "', Status_b = 'CP' where email = '" + email + "'";
            Token t = new Token();
            t.sqlCmd = sql;
            t.ExpiryDate = DateTime.Now.AddHours(3);
            t.sData = sPassword;
            t.sAdditionalData = email;
            String gui = new Token().Save(t, true, sConnStr);

            if (gui == String.Empty)
                return "Could not save new password. Please try again";

            User.NaveoDatabases myEnum;
            //Getting only the enum part
            sServerName = sServerName.Replace(".naveo.mu", String.Empty);
            sServerName = sServerName.Replace("https://", String.Empty);
            sServerName = sServerName.Replace("http://", String.Empty);
            if (Enum.TryParse(sServerName, true, out myEnum))
                sServerName = "https://" + sServerName + ".naveo.mu";
            else
                sServerName = "http://" + sServerName;

            MailSender ms = new MailSender();
            String sBody = "Dear User \r\n\r\n";

            string Type = string.Empty;
            string Title = string.Empty;
            if (bInsert)
            {
                Type = "User Creation";
                Title = "User Creation";
                sBody += "You have been added on the Naveo Platform ,Please confirm your email address " + email.ToUpper() + "  by clicking on the link below. This link is valid till " + t.ExpiryDate.ToString("dd - MMM - yyyy HH: mm:ss.fff") + "\r\n";
            }

            else
            {
                User user = GetUserByeMail(email, string.Empty, sConnStr, false, false);
                UID = user.UID;
                Type = "Password Reset";
                Title = "Password reset requested";
                sBody += "We have been requested to reset your password. If this is the case, please confirm by clicking on the link below. This link is valid till " + t.ExpiryDate.ToString("dd - MMM - yyyy HH: mm:ss.fff") + "\r\n";
            }


            sBody += sServerName + "/api/v2.0/ResetPassword?sTokenID=" + gui;
            String sMail = ms.SendMail(email, Title, sBody, String.Empty, sConnStr);


            Boolean s = new NotificationService().PrepareDataNotif(UID, DateTime.Now, sMail, sBody, Type, Title, sConnStr);

            //if (sMail == "Mail Sent")
            // return sMail;

            return sMail;



            //return sServerName + " > " + sMail;
        }
        public Boolean ResetPassword(String sTokenID, String sConnStr)
        {
            Token t = new Token().GetTokensById(sTokenID, sConnStr);
            Boolean b = new Token().ExecuteTokens(sTokenID, sConnStr);

            if (b)
            {
                MailSender ms = new MailSender();
                String sMail = ms.SendMail(t.sAdditionalData, "Password reset", "Please login using one time password " + t.sData, String.Empty, sConnStr);
                if (sMail == "Mail Sent")
                    return true;
            }
            return false;
        }

        public List<User> lGetUsers(List<Matrix> lMatrix, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            return lGetUsers(iParentIDs, sConnStr);
        }
        public List<User> lGetUsers(List<int> iParentIDs, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"SELECT DISTINCT u.*
                                from GFI_SYS_User u, GFI_SYS_GroupMatrixUser m
                                where u.UID = m.iID and m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + ") order by Username";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtUser";
            dtSql.Rows.Add(drSql);

            sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Users);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            List<User> lu = new myConverter().ConvertToList<User>(ds.Tables["dtUser"]);
            foreach (User u in lu)
            {
                DataRow[] dRows = ds.Tables["dtMatrix"].Select("StrucID = " + u.UID + "");
                foreach (DataRow dr in dRows)
                {
                    Matrix mx = new Matrix();
                    mx.MID = Convert.ToInt32(dr["MID"]);
                    mx.iID = Convert.ToInt32(dr["MatrixiID"]);
                    mx.GMID = Convert.ToInt32(dr["ParentGMID"]);
                    mx.GMDesc = dr["GMDescription"].ToString();
                    mx.oprType = DataRowState.Unchanged;
                    u.lMatrix.Add(mx);
                }
            }

            return lu;
        }
        public List<Int32> lGetUsers(int uLogin, String sConnStr)
        {
            List<Int32> li = new List<int>();

            List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);
            if (iParentIDs.Count == 0)
                return li;

            List<User> lu = lGetUsers(iParentIDs, sConnStr);
            foreach (User u in lu)
                li.Add(u.UID);

            return li;
        }

        public string getServerName()
        {
            string sServerName = System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            User.NaveoDatabases myEnum;
            //Getting only the enum part
            sServerName = sServerName.Replace(".naveo.mu", String.Empty);
            //sServerName = sServerName.Replace(":44393", String.Empty);
            sServerName = sServerName.Replace("https://", String.Empty);
            sServerName = sServerName.Replace("http://", String.Empty);
            if (Enum.TryParse(sServerName, true, out myEnum))
                sServerName = sServerName;
            else
                sServerName = sServerName;



            sServerName = sServerName.ToUpper();


            return sServerName;
        }

        public BaseModel DeleteUser(User uUser, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            DataTable dt = _connection.GetDataDT("Select * from GFI_FLT_AutoReportingConfigDetails where UIDType = 'Users' and UIDCategory = 'ID' and UID = " + uUser.UID.ToString(), sConnStr);
            BaseModel baseModel = new BaseModel();
            if (dt.Rows.Count > 0)
            {
                baseModel.data = false;
                baseModel.AdditionalInfo = "An automatic report is configured for this user, Delete not possible";
            }

            else if (_connection.GenDelete("GFI_SYS_User", "UID = '" + uUser.UID + "'", sConnStr))
            {
                baseModel.data = true;

#if DEBUG
                string GMSemail = uUser.Email + "." + "BLACKWIDOW";
#else
                string GMSemail = uUser.Email + "." + getServerName().ToUpper();
#endif

                //Delete in fleet user table
                if (_connection.GenDelete("dbo.\"FleetUser\"", "\"GMSUsername\" = '" + GMSemail + "'", NaveoOneLib.Constant.NaveoEntity.MaintDB))
                {
                    //Delete in aspnetuser table
                    _connection.GenDelete("dbo.\"AspNetUsers\"", "UserName = '" + GMSemail + "'", NaveoOneLib.Constant.NaveoEntity.MaintDB);
                }

            }
            else
                baseModel.data = false;

            return baseModel;
        }

        public bool IncrementLoginCount(string email, int loginCount, String sConnStr)
        {
            User u = new User();
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_User";
            dt.Columns.Add("LoginCount", u.LoginCount.GetType());
            DataRow dr = dt.NewRow();
            dr["LoginCount"] = loginCount + 1;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr);
            if (_connection.GenUpdate(dt, "Email = '" + email + "'", sConnStr))
                return true;
            else
                return false;
        }

        public int GetUserApprovalProcessIdByUserID(int UserID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = "SELECT UE.GMApprovalID FROM GFI_SYS_UserExt UE  inner join GFI_SYS_USER U on U.UID = UE.UserID  where u.UID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = UserID;
            drSql["sTableName"] = "dtGFI_SYS_UserExt";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            int i = -1;
            if (ds != null)
                if (ds.Tables.Count > 0)
                    if (ds.Tables[0].Rows.Count > 0)
                        int.TryParse(ds.Tables[0].Rows[0][0].ToString(), out i);

            return i;
        }

        public int GetUserApprovalProcessValueByUserID(int UID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"select  gma.ApprovalValue from GFI_SYS_UserExt UE 
                            inner join GFI_SYS_USER U on U.UID = UE.UserID
                            inner join GFI_SYS_GroupMatrixApprovalProcess gma  on UE.GMApprovalID = gma.iID
                          where u.UID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = UID;
            drSql["sTableName"] = "dtGFI_SYS_UserExt";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            int i = -1;
            if (ds != null)
                if (ds.Tables.Count > 0)
                    if (ds.Tables[0].Rows.Count > 0)
                        int.TryParse(ds.Tables[0].Rows[0][0].ToString(), out i);

            return i;
        }

        public int GetApprovalProcessValue(int ApprovalID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"select ApprovalValue from GFI_SYS_GroupMatrixApprovalProcess where iID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = ApprovalID;
            drSql["sTableName"] = "ApprovalProcessValue";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            int i = -1;
            if (ds != null)
                if (ds.Tables.Count > 0)
                    if (ds.Tables[0].Rows.Count > 0)
                        int.TryParse(ds.Tables[0].Rows[0][0].ToString(), out i);

            return i;
        }

        public int GetApprovalProcessIdByValue(int ApprovalValue, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"select iID from GFI_SYS_GroupMatrixApprovalProcess where ApprovalValue = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = ApprovalValue;
            drSql["sTableName"] = "ApprovalProcessId";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            int i = -1;
            if (ds != null)
                if (ds.Tables.Count > 0)
                    if (ds.Tables[0].Rows.Count > 0)
                        int.TryParse(ds.Tables[0].Rows[0][0].ToString(), out i);

            return i;
        }

        public BaseModel ResetToDefaultPassword(String email, String sConnStr)
        {
            Boolean bResult = true;

            BaseModel baseModel = new BaseModel();
            if (!IsValidEmailAddress(email))
            {
                bResult = false;
                baseModel.data = bResult;
                baseModel.errorMessage = "Invalid email address";
                return baseModel;
            }

            string sPassword = "Password123.";
            String pswd = BaseService.EncryptMe(sPassword);
            String sql = "update GFI_SYS_User set Password = '" + pswd + "', Status_b = 'CP', loginAttempt = 0 where email = '" + email + "'";

            int iConn = new Connection(sConnStr).dbExecute(sql, sConnStr);
            if (iConn < 1)
            {
                bResult = false;
                baseModel.errorMessage = "Password Reset Failed";
            }

            baseModel.data = bResult;
            return baseModel;
        }

        //public DataTable GetUserApprovalProcess(List<Matrix> lMatrix, String sConnStr)
        //{
        //    List<int> iParentIDs = new List<int>();
        //    foreach (Matrix m in lMatrix)
        //        iParentIDs.Add(m.GMID);

        //    return GetUserApproval(iParentIDs, sConnStr);
        //}

        public dtData GetUserApprovalProcess(int gmid, String sConnStr)
        {
            DataTable dt = new Connection(sConnStr).GetDataDT(sUserApproval(gmid, sConnStr), sConnStr);

            dtData dtData = new dtData();
            dtData.Data = dt;

            return dtData;
        }

        //        public String sUserApproval(List<int> iParentIDs, String sConnStr)
        //        {

        //            string GMIDs = new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr);
        //            string sql = string.Empty;
        //            string[] gmids = GMIDs.Split(',');
        //            DataTable dtApproval = new DataTable();
        //            Boolean bFound = false;
        //            foreach (String g in gmids)
        //            {
        //                DataTable dt = new Connection(sConnStr).GetDataDT(new NaveoOneLib.Services.GMatrix.GroupMatrixService().sGetCompanyGMIDfromGMID(Convert.ToInt32(g)), sConnStr);
        //                foreach (DataRow dr in dt.Rows)
        //                {
        //                    sql = @"
        //select p.iID ,p.Source, p.ApprovalID from GFI_SYS_GroupMatrixApprovalProcess p 
        //inner join GFI_SYS_UserExt userE  on p.iID = userE.GMApprovalID
        //where p.GMID = " + dr["GMID"];

        //                    dtApproval = new Connection(sConnStr).GetDataDT(sql, sConnStr);
        //                    if (dtApproval.Rows.Count > 0)
        //                        bFound = true;
        //                    if (bFound)
        //                        break;
        //                }
        //                if (bFound)
        //                    break;
        //            }
        //            return sql;
        //        }

        public String sUserApproval(int gmid, string sConnStr)
        {
            Boolean bFound = false;
            string sql = string.Empty;
            DataTable dtApproval = new DataTable();
            DataTable dt = new Connection(sConnStr).GetDataDT(new NaveoOneLib.Services.GMatrix.GroupMatrixService().sGetCompanyGMIDfromGMID(gmid), sConnStr);

            if (dt.Rows.Count == 0)
            {
                sql = @"select p.iID ,p.Source, p.ApprovalValue from GFI_SYS_GroupMatrixApprovalProcess p where p.GMID =0";

            }

            foreach (DataRow dr in dt.Rows)
            {
                sql = @"select p.iID ,p.Source, p.ApprovalValue from GFI_SYS_GroupMatrixApprovalProcess p 
                            --inner join GFI_SYS_UserExt userE on p.iID = userE.GMApprovalID
                        where p.GMID = " + dr["GMID"];

                dtApproval = new Connection(sConnStr).GetDataDT(sql, sConnStr);
                if (dtApproval.Rows.Count > 0)
                    bFound = true;

                if (bFound)
                    break;
            }



            return sql;
        }


        public DataTable GetFilterUserApproval(List<string> lIds, int value, String sConnStr)
        {
            DataTable dt = new Connection(sConnStr).GetDataDT(sFilterUserApproval(lIds, value, sConnStr), sConnStr);

            return dt;
        }

        public String sFilterUserApproval(List<string> lIds, int value, string sConnStr)
        {
            String sql = String.Empty;
            String strResult = String.Empty;
            foreach (String g in lIds)
                strResult += g + ",";


            strResult = strResult.TrimEnd(',');

            sql = @"
            select * from GFI_SYS_GroupMatrixApprovalProcess where iID in (" + strResult + ") and ApprovalValue = " + value + "";

            return sql;
        }


        public BaseModel SaveLiveConnections(LiveConnections uUser, Boolean bInsert, String sConnStr)
        {
            BaseModel baseModel = new BaseModel();
            Boolean bResult = false;


            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_LIVECONNECTIONS";
            dt.Columns.Add("iID", typeof(int));
            dt.Columns.Add("UserToken", typeof(Guid));
            dt.Columns.Add("FleetLogin", typeof(String));
            dt.Columns.Add("MaintenanceLogin", typeof(String));
            dt.Columns.Add("oprType", typeof(DataRowState));

            DataRow dr = dt.NewRow();
            dr["iID"] = uUser.iID;
            dr["UserToken"] = uUser.UserToken;
            dr["FleetLogin"] = uUser.FleetLogin;
            dr["MaintenanceLogin"] = uUser.MaintenanceLogin;
            dr["oprType"] = uUser.oprType;


            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "iID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uUser.iID;

            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);


            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResult.Tables["CTRLTBL"];

            DataRow[] dRowId = null;

            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                {
                    baseModel.dbResult = dsResult;
                    baseModel.dbResult.Tables["CTRLTBL"].TableName = "ResultCtrlTbl";
                    baseModel.dbResult.Merge(dsProcess);
                    bResult = false;
                }
                else
                {
                    bResult = true;
                    dRowId = dtCtrl.Select();
                }
            }
            else
            {
                bResult = true;
            }



            baseModel.data = bResult;
            return baseModel;
        }

        public LiveConnections GetLiveConnectionsByUser(Guid UserToken, String sConnStr)
        {
            Connection c = new Connection(sConnStr);


            String sql = "select * from GFI_SYS_LIVECONNECTIONS WHERE UserToken = '" + UserToken.ToString() + "'";
            DataTable dt = c.GetDataDT(sql, sConnStr);

            LiveConnections lc = new LiveConnections();

            if (dt.Rows.Count != 0 || dt.Rows != null)
            {
                lc.iID = Convert.ToInt32(dt.Rows[0]["iID"]);
                lc.UserToken = new Guid(dt.Rows[0]["UserToken"].ToString());
                lc.FleetLogin = Convert.ToInt32(dt.Rows[0]["FleetLogin"]);
                lc.MaintenanceLogin = Convert.ToInt32(dt.Rows[0]["MaintenanceLogin"]);
            }


            return lc;
        }
    }
}