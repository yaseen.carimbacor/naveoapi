using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Users;

namespace NaveoOneLib.Services.Users
{
    public class UserSettingsService
    {
        Errors er = new Errors();
        public DataTable GetUserSettings(String sConnStr)
        {
            String sqlString = @"SELECT iID, 
                                    UserID, 
                                    ModuleID, 
                                    ParamType, 
                                    Parameter, 
                                    ParamValue1, 
                                    ParamValue2, 
                                    ParamValue3, 
                                    ParamDate1, 
                                    ParamDate2 from GFI_SYS_UserSettings order by iID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public UserSetting GetUserSettingsById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT iID, 
                                    UserID, 
                                    ModuleID, 
                                    ParamType, 
                                    Parameter, 
                                    ParamValue1, 
                                    ParamValue2, 
                                    ParamValue3, 
                                    ParamDate1, 
                                    ParamDate2 from GFI_SYS_UserSettings
                                    WHERE iID = ?
                                    order by iID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                UserSetting retUserSettings = new UserSetting();
                retUserSettings.iID = Convert.ToInt32(dt.Rows[0]["iID"]);
                retUserSettings.UserID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                retUserSettings.ModuleID = dt.Rows[0]["ModuleID"].ToString();
                retUserSettings.ParamType = dt.Rows[0]["ParamType"].ToString();
                retUserSettings.Parameter = dt.Rows[0]["Parameter"].ToString();
                retUserSettings.ParamValue1 = dt.Rows[0]["ParamValue1"].ToString();
                retUserSettings.ParamValue2 = dt.Rows[0]["ParamValue2"].ToString();
                retUserSettings.ParamValue3 = dt.Rows[0]["ParamValue3"].ToString();
                retUserSettings.ParamDate1 = (DateTime)dt.Rows[0]["ParamDate1"];
                retUserSettings.ParamDate2 = (DateTime)dt.Rows[0]["ParamDate2"];
                return retUserSettings;
            }
        }
        public bool SaveUserSettings(UserSetting uUserSettings, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_UserSettings";
            dt.Columns.Add("iID", uUserSettings.iID.GetType());
            dt.Columns.Add("UserID", uUserSettings.UserID.GetType());
            dt.Columns.Add("ModuleID", uUserSettings.ModuleID.GetType());
            dt.Columns.Add("ParamType", uUserSettings.ParamType.GetType());
            dt.Columns.Add("Parameter", uUserSettings.Parameter.GetType());
            dt.Columns.Add("ParamValue1", uUserSettings.ParamValue1.GetType());
            dt.Columns.Add("ParamValue2", uUserSettings.ParamValue2.GetType());
            dt.Columns.Add("ParamValue3", uUserSettings.ParamValue3.GetType());
            dt.Columns.Add("ParamDate1", uUserSettings.ParamDate1.GetType());
            dt.Columns.Add("ParamDate2", uUserSettings.ParamDate2.GetType());
            DataRow dr = dt.NewRow();
            dr["iID"] = uUserSettings.iID;
            dr["UserID"] = uUserSettings.UserID;
            dr["ModuleID"] = uUserSettings.ModuleID;
            dr["ParamType"] = uUserSettings.ParamType;
            dr["Parameter"] = uUserSettings.Parameter;
            dr["ParamValue1"] = uUserSettings.ParamValue1;
            dr["ParamValue2"] = uUserSettings.ParamValue2;
            dr["ParamValue3"] = uUserSettings.ParamValue3;
            dr["ParamDate1"] = uUserSettings.ParamDate1;
            dr["ParamDate2"] = uUserSettings.ParamDate2;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenInsert(dt, transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool UpdateUserSettings(UserSetting uUserSettings, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_UserSettings";
            dt.Columns.Add("iID", uUserSettings.iID.GetType());
            dt.Columns.Add("UserID", uUserSettings.UserID.GetType());
            dt.Columns.Add("ModuleID", uUserSettings.ModuleID.GetType());
            dt.Columns.Add("ParamType", uUserSettings.ParamType.GetType());
            dt.Columns.Add("Parameter", uUserSettings.Parameter.GetType());
            dt.Columns.Add("ParamValue1", uUserSettings.ParamValue1.GetType());
            dt.Columns.Add("ParamValue2", uUserSettings.ParamValue2.GetType());
            dt.Columns.Add("ParamValue3", uUserSettings.ParamValue3.GetType());
            dt.Columns.Add("ParamDate1", uUserSettings.ParamDate1.GetType());
            dt.Columns.Add("ParamDate2", uUserSettings.ParamDate2.GetType());
            DataRow dr = dt.NewRow();
            dr["iID"] = uUserSettings.iID;
            dr["UserID"] = uUserSettings.UserID;
            dr["ModuleID"] = uUserSettings.ModuleID;
            dr["ParamType"] = uUserSettings.ParamType;
            dr["Parameter"] = uUserSettings.Parameter;
            dr["ParamValue1"] = uUserSettings.ParamValue1;
            dr["ParamValue2"] = uUserSettings.ParamValue2;
            dr["ParamValue3"] = uUserSettings.ParamValue3;
            dr["ParamDate1"] = uUserSettings.ParamDate1;
            dr["ParamDate2"] = uUserSettings.ParamDate2;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "iID = '" + uUserSettings.iID + "'", transaction,sConnStr))
                return true;
            else
                return false;
        }
        public bool DeleteUserSettings(UserSetting uUserSettings, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_SYS_UserSettings", "iID = '" + uUserSettings.iID + "'", sConnStr))
                return true;
            else
                return false;
        }

        public String sGetUserSettings(int iUserID)
        {
            String sqlString = @"SELECT iID, 
                                    UserID, 
                                    ModuleID, 
                                    ParamType, 
                                    Parameter, 
                                    ParamValue1, 
                                    ParamValue2, 
                                    ParamValue3, 
                                    ParamDate1, 
                                    ParamDate2 from GFI_SYS_UserSettings where UserID = " + iUserID.ToString();
            return sqlString;
        }
        public Boolean SaveUserSettings(UserSetting uUserSettings, Boolean bInsert, out DataSet dsResults, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_UserSettings";
            dt.Columns.Add("iID", uUserSettings.iID.GetType());
            dt.Columns.Add("UserID", uUserSettings.UserID.GetType());
            dt.Columns.Add("ModuleID", uUserSettings.ModuleID.GetType());
            dt.Columns.Add("ParamType", uUserSettings.ParamType.GetType());
            dt.Columns.Add("Parameter", uUserSettings.Parameter.GetType());
            dt.Columns.Add("ParamValue1", uUserSettings.ParamValue1.GetType());
            dt.Columns.Add("ParamValue2", uUserSettings.ParamValue2.GetType());
            dt.Columns.Add("ParamValue3", uUserSettings.ParamValue3.GetType());
            dt.Columns.Add("ParamDate1", uUserSettings.ParamDate1.GetType());
            dt.Columns.Add("ParamDate2", uUserSettings.ParamDate2.GetType());
            DataRow dr = dt.NewRow();
            dr["iID"] = uUserSettings.iID;
            dr["UserID"] = uUserSettings.UserID;
            dr["ModuleID"] = uUserSettings.ModuleID;
            dr["ParamType"] = uUserSettings.ParamType;
            dr["Parameter"] = uUserSettings.Parameter;
            dr["ParamValue1"] = uUserSettings.ParamValue1;
            dr["ParamValue2"] = uUserSettings.ParamValue2;
            dr["ParamValue3"] = uUserSettings.ParamValue3;
            dr["ParamDate1"] = uUserSettings.ParamDate1;
            dr["ParamDate2"] = uUserSettings.ParamDate2;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "iID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uUserSettings.iID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            #region GetData
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new UserSettingsService().sGetUserSettings(Globals.uLogin.UID);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtUserSettings";
            dtSql.Rows.Add(drSql);
            #endregion

            dsProcess.Merge(dtSql);
            dsProcess.Merge(dtCtrl);
            dsResults = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResults.Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
    }
}