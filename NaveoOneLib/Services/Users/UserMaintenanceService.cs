﻿using NaveoOneLib.DBCon;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.Users;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NaveoOneLib.Services.Users
{
    public class UserMaintenanceService
    {

        DataTable FleetUserDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "dbo.\"FleetUser\"";
            dt.Columns.Add("\"Id\"", typeof(int));
            dt.Columns.Add("\"FMSUserTOKEN\"", typeof(String));
            dt.Columns.Add("\"GMSUsername\"", typeof(String));
            dt.Columns.Add("\"GMSPassword\"", typeof(String));
            dt.Columns.Add("\"FleetServerId\"", typeof(int));
            dt.Columns.Add("\"CreatedBy\"", typeof(String));
            dt.Columns.Add("\"DateCreated\"", typeof(DateTime));
            dt.Columns.Add("\"ModifiedBy\"", typeof(String));
            dt.Columns.Add("\"DateModified\"", typeof(DateTime));
            dt.Columns.Add("\"IsActive\"", typeof(String)); //Boolean
            dt.Columns.Add("oprType", typeof(DataRowState));
            return dt;
        }

        DataTable dtAspNetUsers()
        {
            DataTable dt = new DataTable();
            dt.TableName = "dbo.\"AspNetUsers\"";
            dt.Columns.Add("\"Id\"", typeof(string));
            dt.Columns.Add("\"UserId\"", typeof(int));
            dt.Columns.Add("\"Title\"", typeof(String));
            dt.Columns.Add("\"FirstName\"", typeof(String));
            dt.Columns.Add("\"MiddleName\"", typeof(String));
            dt.Columns.Add("\"LastName\"", typeof(String));
            dt.Columns.Add("\"Phone\"", typeof(String));
            dt.Columns.Add("\"Country\"", typeof(String));
            dt.Columns.Add("\"Address\"", typeof(String));
            dt.Columns.Add("\"UserRole\"", typeof(String));
            dt.Columns.Add("\"Approval\"", typeof(String)); //boolean
            dt.Columns.Add("\"LastPasswordChangedDate\"", typeof(DateTime));
            dt.Columns.Add("\"FirstTimeLogin\"", typeof(String)); //boolean
            dt.Columns.Add("\"CreatedDate\"", typeof(DateTime));
            dt.Columns.Add("\"CreatedBy\"", typeof(string));
            dt.Columns.Add("\"UpdatedBy\"", typeof(string));
            dt.Columns.Add("\"UpdatedDate\"", typeof(DateTime));
            dt.Columns.Add("\"Email\"", typeof(String));
            dt.Columns.Add("\"EmailConFirmed\"", typeof(String)); //boolean
            dt.Columns.Add("\"PasswordHash\"", typeof(String));
            dt.Columns.Add("\"SecurityStamp\"", typeof(String));
            dt.Columns.Add("\"PhoneNumber\"", typeof(String));
            dt.Columns.Add("\"PhoneNumberConfirmed\"", typeof(String)); //boolean
            dt.Columns.Add("\"TwoFactorEnabled\"", typeof(String)); //boolean
            dt.Columns.Add("\"LockoutEndDateUtc\"", typeof(DateTime));
            dt.Columns.Add("\"LockOutEnabled\"", typeof(String)); //boolean
            dt.Columns.Add("\"AccessFailedCount\"", typeof(int));
            dt.Columns.Add("\"UserName\"", typeof(String));
            dt.Columns.Add("\"LoginDetail_Id\"", typeof(int));
            dt.Columns.Add("oprType", typeof(DataRowState));


            return dt;

        }

        DataTable dtAspNetRoles()
        {
            DataTable dt = new DataTable();
            dt.TableName = "dbo.\"AspNetUserRoles\"";
            dt.Columns.Add("\"UserId\"", typeof(String));
            dt.Columns.Add("\"RoleId\"", typeof(String));
            dt.Columns.Add("oprType", typeof(DataRowState));

            return dt;

        }

        DataTable ModuleAccess()
        {
            DataTable dt = new DataTable();
            dt.TableName = "dbo.\"ModuleAccess\"";
            dt.Columns.Add("\"Id\"", typeof(int));
            dt.Columns.Add("\"RoleId\"", typeof(String));
            dt.Columns.Add("\"RoleName\"", typeof(String));
            dt.Columns.Add("\"UserId\"", typeof(String));
            dt.Columns.Add("\"Read\"", typeof(String)); //boolean
            dt.Columns.Add("\"Write\"", typeof(String)); //boolean
            dt.Columns.Add("\"Delete\"", typeof(String)); //boolean
            dt.Columns.Add("oprType", typeof(DataRowState));

            return dt;

        }

        public string getServerName()
        {
            string sServerName = System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            User.NaveoDatabases myEnum;
            //Getting only the enum part
            sServerName = sServerName.Replace(".naveo.mu", String.Empty);
            //sServerName = sServerName.Replace(":44393", String.Empty);
            sServerName = sServerName.Replace("https://", String.Empty);
            sServerName = sServerName.Replace("http://", String.Empty);
            if (Enum.TryParse(sServerName, true, out myEnum))
                sServerName = sServerName;
            else
                sServerName = sServerName;



            sServerName = sServerName.ToUpper();


            return sServerName;
        }

        //public int userId = 0;

        public BaseModel SaveMaintUser(MaintUser maintUser, Boolean bInsert, String sConnStr)
        {
            BaseModel baseModel = new BaseModel();
            Boolean bResult = false;
            baseModel.data = bResult;
            int userIdMaint = 0;

            string AspNet = string.Empty;
            //int CustomerId = 0;

            Connection c = new Connection(NaveoOneLib.Constant.NaveoEntity.MaintDB);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            DataSet dsProcess = new DataSet();
            DataTable dtFleetUserToSave = FleetUserDT();
            DataTable dtAspUsers = dtAspNetUsers();
            DataTable dtAspUsersRoles = dtAspNetRoles();
            DataTable dtModuleAccess = ModuleAccess();
            DataTable dtModuleAccessAccount = ModuleAccess();
            DataTable dtModuleAccessAdmin = ModuleAccess();
            DataTable dtModuleAccessAgent = ModuleAccess();
            DataTable dtModuleAccessBooking = ModuleAccess();
            DataTable dtModuleAccessCustomer = ModuleAccess();
            DataTable dtModuleAccessFuel = ModuleAccess();
            DataTable dtModuleAccessPurchaseOrder = ModuleAccess();
            DataTable dtModuleAccessReport = ModuleAccess();
            DataTable dtModuleAccessStore = ModuleAccess();
            DataTable dtModuleAccessSupplier = ModuleAccess();
            DataTable dtModuleAccessTender = ModuleAccess();
            DataTable dtModuleAccessVehicle = ModuleAccess();

            //Table FleetServers
            //sServerName   

            //string sServerName = "BLACKWIDOW";

            string sServerName = getServerName();


            int serverId = 0;
            //Get Server Id

            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            string GMSemail = maintUser.GMSUsername + "." + sServerName;
            String sql = "select 'FleetServer' as TableName;\r\n\r\n";
            sql += "select * from dbo.\"FleetServer\" where \"Name\" = '" + sServerName.ToUpper() + "';\r\n\r\n";
            sql += "select 'FleetUser' as TableName;\r\n\r\n";
            sql += "select * from dbo.\"FleetUser\" where \"GMSUsername\" = '" + GMSemail + "';\r\n";
            sql += "select 'FleetAspUser' as TableName;\r\n\r\n";
            sql += "select * from dbo.\"AspNetUsers\" where \"UserName\" = '" + GMSemail + "';\r\n";
            sql += "\r\n --PostgreSQLGMS";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, NaveoOneLib.Constant.NaveoEntity.MaintDB);
            DataTable dtGM = ds.Tables["FleetServer"];
            DataTable dtFleetUser = ds.Tables["FleetUser"];
            DataTable dtFleetAspUser = ds.Tables["FleetAspUser"];

            if (ds.Tables.Contains("DtErr"))
            {
                return baseModel;
            }

            if (dtGM.Rows.Count > 0)
            {
                DataRow row = dtGM.Rows[0];
                serverId = int.Parse(row["Id"].ToString());
            }

            if (dtFleetUser.Rows.Count > 0)
            {
                DataRow row = dtFleetUser.Rows[0];
                userIdMaint = int.Parse(row["Id"].ToString());
            }

            if (dtFleetAspUser.Rows.Count > 0)
            {
                DataRow row = dtFleetAspUser.Rows[0];
                AspNet = row["Id"].ToString();
            }

            //DataRow[] drFleetUserQuery = dtFleetUser.Select("GMSUsername >= '"+ GMSemail + "' AND FleetServerId = "+ serverId+"");
            //if (drFleetUserQuery.Length > 0)
            // {
            // baseModel.data = false;
            //return baseModel;
            // } 
            //else
            // {
            Guid GUID = UserService.GetUserToken(maintUser.FleetUserId, sConnStr);

            DataRow drFleetUser = dtFleetUserToSave.NewRow();
            if (!bInsert)
            {
                drFleetUser["\"Id\""] = userIdMaint;
            }
            //drFleetUser["Id"] = 1091;//Guid.NewGuid();
            drFleetUser["\"FMSUserTOKEN\""] = GUID.ToString();
            drFleetUser["\"GMSUsername\""] = GMSemail;
            drFleetUser["\"GMSPassword\""] = maintUser.GMSPassword;
            drFleetUser["\"FleetServerId\""] = serverId;
            drFleetUser["\"CreatedBy\""] = "FleetMs";
            drFleetUser["\"DateCreated\""] = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
            //drFleetUser["ModifiedBy"] = null;
            drFleetUser["\"IsActive\""] = true;
            if (bInsert)
            {
                drFleetUser["oprType"] = DataRowState.Added;
            }
            else
            {
                drFleetUser["oprType"] = DataRowState.Modified;
            }

            dtFleetUserToSave.Rows.Add(drFleetUser);
            //dtFleetUserToSave.Columns.Remove("\"IsActive\"");

            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dtFleetUserToSave.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "\"Id\"";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "GET";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtFleetUserToSave);

            //68f9d79d-0aaf-4704-934a-2433cb373ae8
            string sqlToSaveNetUsers = string.Empty;
            string userRole = "Maintenance";
            string PasswordHash = "AO/6Cc4nQ55jJVtagfGvCs+7urMhNfmk2k0M86iuRgh6nvB6WEXjj+EB8eksOv536g==";
            string SecurityStamp = "0a565381-e890-442d-a619-ec3b90ac7c09";

            string userId = string.Empty;
            if (bInsert)
            {
                userId = Guid.NewGuid().ToString();
            }
            else
            {
                userId = AspNet;
            }

            DataRow drAspNetUsers = dtAspUsers.NewRow();
            drAspNetUsers["\"Id\""] = userId;
            drAspNetUsers["\"Title\""] = maintUser.Ttile;
            drAspNetUsers["\"FirstName\""] = maintUser.FirstName;
            //drAspNetUsers["MiddleName"] = null;
            drAspNetUsers["\"LastName\""] = maintUser.LastName;
            //drAspNetUsers["Phone"] = null;
            //drAspNetUsers["Country"] =null;
            //drAspNetUsers["Address"] = null;
            drAspNetUsers["\"Approval\""] = true;
            //dtAspUsers.Columns.Remove("\"Approval\"");
            drAspNetUsers["\"UserRole\""] = userRole;
            //drAspNetUsers["LastPasswordChangedDate"] = DateTime.MinValue;
            //drAspNetUsers["\"FirstTimeLogin\""] = "'false'";
            dtAspUsers.Columns.Remove("\"FirstTimeLogin\"");
            drAspNetUsers["\"CreatedDate\""] = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
            //dtAspUsers.Columns.Remove("\"CreatedDate\"");
            drAspNetUsers["\"CreatedBy\""] = "FleetMs";
            //drAspNetUsers["UpdatedBy"] = null;
            //drAspNetUsers["UpdatedDate"] = null;
            drAspNetUsers["\"Email\""] = GMSemail;
            //drAspNetUsers["\"EmailConFirmed\""] = "'true'";
            dtAspUsers.Columns.Remove("\"EmailConFirmed\"");
            drAspNetUsers["\"PasswordHash\""] = PasswordHash;
            drAspNetUsers["\"SecurityStamp\""] = SecurityStamp;
            //drAspNetUsers["PhoneNumber"] = null;
            //drAspNetUsers["\"PhoneNumberConfirmed\""] = "'false'";
            dtAspUsers.Columns.Remove("\"PhoneNumberConfirmed\"");
            //drAspNetUsers["\"TwoFactorEnabled\""] = "'false'";
            dtAspUsers.Columns.Remove("\"TwoFactorEnabled\"");
            drAspNetUsers["\"UserName\""] = GMSemail;
            //drAspNetUsers["\"AccessFailedCount\""] = 0;
            dtAspUsers.Columns.Remove("\"AccessFailedCount\"");
            //drAspNetUsers["\"LockoutEnabled\""] = "'false'";
            dtAspUsers.Columns.Remove("\"LockoutEnabled\"");
            if (bInsert)
            {
                drAspNetUsers["oprType"] = DataRowState.Added;
            }
            else
            {
                drAspNetUsers["oprType"] = DataRowState.Modified;
            }

            //drAspNetUsers["LoginDetail_Id"] = 0;
            dtAspUsers.Rows.Add(drAspNetUsers);
            dsProcess.Merge(dtAspUsers);

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 2;
            drCtrl["TblName"] = dtAspUsers.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "\"Id\"";
            drCtrl["KeyIsIdentity"] = "FALSE";
            dtCtrl.Rows.Add(drCtrl);

            if (bInsert)
            {
                DataRow drAspNetUserRoles = dtAspUsersRoles.NewRow();
                drAspNetUserRoles["\"UserId\""] = userId;
                drAspNetUserRoles["\"RoleId\""] = "8f9b9476-a6f1-4719-8748-c9ba33758fdc";
                drAspNetUserRoles["oprType"] = DataRowState.Added;
                dtAspUsersRoles.Rows.Add(drAspNetUserRoles);

                drCtrl = dtCtrl.NewRow();
                //drCtrl["ParentTblName"] = dtAspUsers.TableName;
                drCtrl["SeqNo"] = 3;
                drCtrl["TblName"] = dtAspUsersRoles.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "\"Id\"";
                drCtrl["KeyIsIdentity"] = "FALSE";
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtAspUsersRoles);

                DataRow dtModuleAccessRow = dtModuleAccess.NewRow();
                //dtModuleAccessRow["\"Id\""] = 2;
                dtModuleAccessRow["\"UserId\""] = userId;
                dtModuleAccessRow["\"RoleId\""] = "8f9b9476-a6f1-4719-8748-c9ba33758fdc";
                dtModuleAccessRow["\"RoleName\""] = "Maintenance";
                //dtModuleAccess.Columns.Remove("\"Read\"");
                //dtModuleAccess.Columns.Remove("\"Write\"");
                //dtModuleAccess.Columns.Remove("\"Delete\"");
                dtModuleAccessRow["\"Read\""] = true;
                dtModuleAccessRow["\"Write\""] = true;
                dtModuleAccessRow["\"Delete\""] = true;
                dtModuleAccessRow["oprType"] = DataRowState.Added;
                dtModuleAccess.Rows.Add(dtModuleAccessRow);

                drCtrl = dtCtrl.NewRow();
                //drCtrl["ParentTblName"] = dtAspUsers.TableName;
                drCtrl["SeqNo"] = 4;
                drCtrl["TblName"] = dtModuleAccess.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "\"Id\"";
                drCtrl["KeyIsIdentity"] = "TRUE";
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtModuleAccess);

                DataRow dtModuleAccessRowAccount = dtModuleAccessAccount.NewRow();
                dtModuleAccessRowAccount["\"UserId\""] = userId;
                dtModuleAccessRowAccount["\"RoleId\""] = "1efa17ec-bc59-466d-a39c-cdf01570081b";
                dtModuleAccessRowAccount["\"RoleName\""] = "Account";
                //dtModuleAccessAccount.Columns.Remove("\"Read\"");
                //dtModuleAccessAccount.Columns.Remove("\"Write\"");
                //dtModuleAccessAccount.Columns.Remove("\"Delete\"");
                dtModuleAccessRowAccount["\"Read\""] = false;
                dtModuleAccessRowAccount["\"Write\""] = false;
                dtModuleAccessRowAccount["\"Delete\""] = false;
                dtModuleAccessRowAccount["oprType"] = DataRowState.Added;
                dtModuleAccessAccount.Rows.Add(dtModuleAccessRowAccount);

                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 5;
                drCtrl["TblName"] = dtModuleAccessAccount.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "\"Id\"";
                drCtrl["KeyIsIdentity"] = "TRUE";
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtModuleAccessAccount);


                //
                DataRow dtModuleAccessRowAdmin = dtModuleAccessAdmin.NewRow();
                dtModuleAccessRowAdmin["\"UserId\""] = userId;
                dtModuleAccessRowAdmin["\"RoleId\""] = "b32ff184-178b-4ebb-a566-54b13e7776a8";
                dtModuleAccessRowAdmin["\"RoleName\""] = "Administrator";
                //dtModuleAccessAdmin.Columns.Remove("\"Read\"");
                //dtModuleAccessAdmin.Columns.Remove("\"Write\"");
                //dtModuleAccessAdmin.Columns.Remove("\"Delete\"");
                dtModuleAccessRowAdmin["\"Read\""] = false;
                dtModuleAccessRowAdmin["\"Write\""] = false;
                dtModuleAccessRowAdmin["\"Delete\""] = false;
                dtModuleAccessRowAdmin["oprType"] = DataRowState.Added;
                dtModuleAccessAdmin.Rows.Add(dtModuleAccessRowAdmin);

                drCtrl = dtCtrl.NewRow();
                //drCtrl["ParentTblName"] = dtAspUsers.TableName;
                drCtrl["SeqNo"] = 6;
                drCtrl["TblName"] = dtModuleAccessAdmin.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "\"Id\"";
                drCtrl["KeyIsIdentity"] = "TRUE";
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtModuleAccessAdmin);

                //1
                DataRow dtModuleAccessRowAgent = dtModuleAccessAgent.NewRow();
                dtModuleAccessRowAgent["\"UserId\""] = userId;
                dtModuleAccessRowAgent["\"RoleId\""] = "bc85ce43-c87e-474f-8869-920f9f174712";
                dtModuleAccessRowAgent["\"RoleName\""] = "Agent";
                //dtModuleAccessAgent.Columns.Remove("\"Read\"");
                //dtModuleAccessAgent.Columns.Remove("\"Write\"");
                //dtModuleAccessAgent.Columns.Remove("\"Delete\"");
                dtModuleAccessRowAgent["\"Read\""] = false;
                dtModuleAccessRowAgent["\"Write\""] = false;
                dtModuleAccessRowAgent["\"Delete\""] = false;
                dtModuleAccessRowAgent["oprType"] = DataRowState.Added;
                dtModuleAccessAgent.Rows.Add(dtModuleAccessRowAgent);

                drCtrl = dtCtrl.NewRow();
                //drCtrl["ParentTblName"] = dtAspUsers.TableName;
                drCtrl["SeqNo"] = 7;
                drCtrl["TblName"] = dtModuleAccessAgent.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "\"Id\"";
                drCtrl["KeyIsIdentity"] = "TRUE";
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtModuleAccessAgent);

                //2
                DataRow dtModuleAccessRowBooking = dtModuleAccessBooking.NewRow();
                dtModuleAccessRowBooking["\"UserId\""] = userId;
                dtModuleAccessRowBooking["\"RoleId\""] = "a6ee4f75-dabb-49ac-974d-43dcfcbffcec";
                dtModuleAccessRowBooking["\"RoleName\""] = "Booking";
                //dtModuleAccessBooking.Columns.Remove("\"Read\"");
                //dtModuleAccessBooking.Columns.Remove("\"Write\"");
                //dtModuleAccessBooking.Columns.Remove("\"Delete\"");
                dtModuleAccessRowBooking["\"Read\""] = false;
                dtModuleAccessRowBooking["\"Write\""] = false;
                dtModuleAccessRowBooking["\"Delete\""] = false;
                dtModuleAccessRowBooking["oprType"] = DataRowState.Added;
                dtModuleAccessBooking.Rows.Add(dtModuleAccessRowBooking);

                drCtrl = dtCtrl.NewRow();
                //drCtrl["ParentTblName"] = dtAspUsers.TableName;
                drCtrl["SeqNo"] = 8;
                drCtrl["TblName"] = dtModuleAccessBooking.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "\"Id\"";
                drCtrl["KeyIsIdentity"] = "TRUE";
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtModuleAccessBooking);

                //3
                DataRow dtModuleAccessRowCustomer = dtModuleAccessCustomer.NewRow();
                dtModuleAccessRowCustomer["\"UserId\""] = userId;
                dtModuleAccessRowCustomer["\"RoleId\""] = "90a9b356-89f5-4782-9e2b-d76c2be257bc";
                dtModuleAccessRowCustomer["\"RoleName\""] = "Customer";
                //dtModuleAccessCustomer.Columns.Remove("\"Read\"");
                //dtModuleAccessCustomer.Columns.Remove("\"Write\"");
                //dtModuleAccessCustomer.Columns.Remove("\"Delete\"");
                dtModuleAccessRowCustomer["\"Read\""] = false;
                dtModuleAccessRowCustomer["\"Write\""] = false;
                dtModuleAccessRowCustomer["\"Delete\""] = false;
                dtModuleAccessRowCustomer["oprType"] = DataRowState.Added;
                dtModuleAccessCustomer.Rows.Add(dtModuleAccessRowCustomer);

                drCtrl = dtCtrl.NewRow();
                //drCtrl["ParentTblName"] = dtAspUsers.TableName;
                drCtrl["SeqNo"] = 9;
                drCtrl["TblName"] = dtModuleAccessCustomer.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "\"Id\"";
                drCtrl["KeyIsIdentity"] = "TRUE";
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtModuleAccessCustomer);

                //4
                DataRow dtModuleAccessRowFuel = dtModuleAccessFuel.NewRow();
                dtModuleAccessRowFuel["\"UserId\""] = userId;
                dtModuleAccessRowFuel["\"RoleId\""] = "c01ab838-dbd3-4bb4-b3f9-3e256e7c999d";
                dtModuleAccessRowFuel["\"RoleName\""] = "Fuel";
                //dtModuleAccessFuel.Columns.Remove("\"Read\"");
                //dtModuleAccessFuel.Columns.Remove("\"Write\"");
                //dtModuleAccessFuel.Columns.Remove("\"Delete\"");
                dtModuleAccessRowFuel["\"Read\""] = true;
                dtModuleAccessRowFuel["\"Write\""] = true;
                dtModuleAccessRowFuel["\"Delete\""] = true;
                dtModuleAccessRowFuel["oprType"] = DataRowState.Added;
                dtModuleAccessFuel.Rows.Add(dtModuleAccessRowFuel);

                drCtrl = dtCtrl.NewRow();
                //drCtrl["ParentTblName"] = dtAspUsers.TableName;
                drCtrl["SeqNo"] = 10;
                drCtrl["TblName"] = dtModuleAccessFuel.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "\"Id\"";
                drCtrl["KeyIsIdentity"] = "TRUE";
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtModuleAccessFuel);

                //5
                DataRow dtModuleAccessRowPurchaseOrder = dtModuleAccessPurchaseOrder.NewRow();
                dtModuleAccessRowPurchaseOrder["\"UserId\""] = userId;
                dtModuleAccessRowPurchaseOrder["\"RoleId\""] = "b32ff184-178b-4ebb-a566-54b13e7776a8";
                dtModuleAccessRowPurchaseOrder["\"RoleName\""] = "Purchase Order";
                //dtModuleAccessPurchaseOrder.Columns.Remove("\"Read\"");
                //dtModuleAccessPurchaseOrder.Columns.Remove("\"Write\"");
                //dtModuleAccessPurchaseOrder.Columns.Remove("\"Delete\"");
                dtModuleAccessRowPurchaseOrder["\"Read\""] = false;
                dtModuleAccessRowPurchaseOrder["\"Write\""] = false;
                dtModuleAccessRowPurchaseOrder["\"Delete\""] = false;
                dtModuleAccessRowPurchaseOrder["oprType"] = DataRowState.Added;
                dtModuleAccessPurchaseOrder.Rows.Add(dtModuleAccessRowPurchaseOrder);

                drCtrl = dtCtrl.NewRow();
                //drCtrl["ParentTblName"] = dtAspUsers.TableName;
                drCtrl["SeqNo"] = 11;
                drCtrl["TblName"] = dtModuleAccessPurchaseOrder.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "\"Id\"";
                drCtrl["KeyIsIdentity"] = "TRUE";
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtModuleAccessPurchaseOrder);

                //6
                DataRow dtModuleAccessRowReport = dtModuleAccessReport.NewRow();
                dtModuleAccessRowReport["\"UserId\""] = userId;
                dtModuleAccessRowReport["\"RoleId\""] = "4756c0fb-6c11-4a96-9a47-0b9eda9f6d5f";
                dtModuleAccessRowReport["\"RoleName\""] = "Report";
                //dtModuleAccessReport.Columns.Remove("\"Read\"");
                //dtModuleAccessReport.Columns.Remove("\"Write\"");
                //dtModuleAccessReport.Columns.Remove("\"Delete\"");
                dtModuleAccessRowReport["\"Read\""] = true;
                dtModuleAccessRowReport["\"Write\""] = true;
                dtModuleAccessRowReport["\"Delete\""] = true;
                dtModuleAccessRowReport["oprType"] = DataRowState.Added;
                dtModuleAccessReport.Rows.Add(dtModuleAccessRowReport);

                drCtrl = dtCtrl.NewRow();
                //drCtrl["ParentTblName"] = dtAspUsers.TableName;
                drCtrl["SeqNo"] = 12;
                drCtrl["TblName"] = dtModuleAccessReport.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "\"Id\"";
                drCtrl["KeyIsIdentity"] = "TRUE";
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtModuleAccessReport);

                //7
                DataRow dtModuleAccessRowStore = dtModuleAccessStore.NewRow();
                dtModuleAccessRowStore["\"UserId\""] = userId;
                dtModuleAccessRowStore["\"RoleId\""] = "fd7ba057-6980-47d4-b4b3-ead4ad8d78c1";
                dtModuleAccessRowStore["\"RoleName\""] = "Store";
                //dtModuleAccessStore.Columns.Remove("\"Read\"");
                //dtModuleAccessStore.Columns.Remove("\"Write\"");
                //dtModuleAccessStore.Columns.Remove("\"Delete\"");
                dtModuleAccessRowStore["\"Read\""] = false;
                dtModuleAccessRowStore["\"Write\""] = false;
                dtModuleAccessRowStore["\"Delete\""] = false;
                dtModuleAccessRowStore["oprType"] = DataRowState.Added;
                dtModuleAccessStore.Rows.Add(dtModuleAccessRowStore);

                drCtrl = dtCtrl.NewRow();
                //drCtrl["ParentTblName"] = dtAspUsers.TableName;
                drCtrl["SeqNo"] = 13;
                drCtrl["TblName"] = dtModuleAccessStore.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "\"Id\"";
                drCtrl["KeyIsIdentity"] = "TRUE";
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtModuleAccessStore);

                //8
                DataRow dtModuleAccessRowSupplier = dtModuleAccessSupplier.NewRow();
                dtModuleAccessRowSupplier["\"UserId\""] = userId;
                dtModuleAccessRowSupplier["\"RoleId\""] = "4c88cfa1-9e83-44f5-93cd-0026783a45c4";
                dtModuleAccessRowSupplier["\"RoleName\""] = "Supplier";
                //dtModuleAccessSupplier.Columns.Remove("\"Read\"");
                //dtModuleAccessSupplier.Columns.Remove("\"Write\"");
                //dtModuleAccessSupplier.Columns.Remove("\"Delete\"");
                dtModuleAccessRowSupplier["\"Read\""] = false;
                dtModuleAccessRowSupplier["\"Write\""] = false;
                dtModuleAccessRowSupplier["\"Delete\""] = false;
                dtModuleAccessRowSupplier["oprType"] = DataRowState.Added;
                dtModuleAccessSupplier.Rows.Add(dtModuleAccessRowSupplier);

                drCtrl = dtCtrl.NewRow();
                //drCtrl["ParentTblName"] = dtAspUsers.TableName;
                drCtrl["SeqNo"] = 14;
                drCtrl["TblName"] = dtModuleAccessSupplier.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "\"Id\"";
                drCtrl["KeyIsIdentity"] = "TRUE";
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtModuleAccessSupplier);

                //9
                DataRow dtModuleAccessRowTender = dtModuleAccessTender.NewRow();
                dtModuleAccessRowTender["\"UserId\""] = userId;
                dtModuleAccessRowTender["\"RoleId\""] = "d4abd959-d325-490d-9543-b5cd500c3870";
                dtModuleAccessRowTender["\"RoleName\""] = "Tender";
                //dtModuleAccessTender.Columns.Remove("\"Read\"");
                //dtModuleAccessTender.Columns.Remove("\"Write\"");
                //dtModuleAccessTender.Columns.Remove("\"Delete\"");
                dtModuleAccessRowTender["\"Read\""] = false;
                dtModuleAccessRowTender["\"Write\""] = false;
                dtModuleAccessRowTender["\"Delete\""] = false;
                dtModuleAccessRowTender["oprType"] = DataRowState.Added;
                dtModuleAccessTender.Rows.Add(dtModuleAccessRowTender);

                drCtrl = dtCtrl.NewRow();
                //drCtrl["ParentTblName"] = dtAspUsers.TableName;
                drCtrl["SeqNo"] = 15;
                drCtrl["TblName"] = dtModuleAccessTender.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "\"Id\"";
                drCtrl["KeyIsIdentity"] = "TRUE";
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtModuleAccessTender);

                //10
                DataRow dtModuleAccessRowVehicle = dtModuleAccessVehicle.NewRow();
                dtModuleAccessRowVehicle["\"UserId\""] = userId;
                dtModuleAccessRowVehicle["\"RoleId\""] = "0deac62a-00b1-4a57-92d6-47ae2f89fb70";
                dtModuleAccessRowVehicle["\"RoleName\""] = "Vehicle";
                //dtModuleAccessVehicle.Columns.Remove("\"Read\"");
                //dtModuleAccessVehicle.Columns.Remove("\"Write\"");
                //dtModuleAccessVehicle.Columns.Remove("\"Delete\"");
                dtModuleAccessRowVehicle["\"Read\""] = false;
                dtModuleAccessRowVehicle["\"Write\""] = false;
                dtModuleAccessRowVehicle["\"Delete\""] = false;
                dtModuleAccessRowVehicle["oprType"] = DataRowState.Added;
                dtModuleAccessVehicle.Rows.Add(dtModuleAccessRowVehicle);

                drCtrl = dtCtrl.NewRow();
                //drCtrl["ParentTblName"] = dtAspUsers.TableName;
                drCtrl["SeqNo"] = 16;
                drCtrl["TblName"] = dtModuleAccessVehicle.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "\"Id\"";
                drCtrl["KeyIsIdentity"] = "TRUE";
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtModuleAccessVehicle);
            }

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, NaveoOneLib.Constant.NaveoEntity.MaintDB);
            dtCtrl = dsResult.Tables["CTRLTBL"];

            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                {
                    baseModel.dbResult = dsResult;
                    baseModel.dbResult.Tables["CTRLTBL"].TableName = "ResultCtrlTbl";
                    baseModel.dbResult.Merge(dsProcess);
                    bResult = false;
                }
                else
                    bResult = true;
            }

            baseModel.data = bResult;
            return baseModel;
        }


        public class MaintUser
        {

            public int Id { get; set; }

            public string FMSUserToken { get; set; }

            public string GMSUsername { get; set; }

            public string GMSPassword { get; set; }

            public int FleetServerId { get; set; }

            public string CreatedBy { get; set; }

            public DateTime CreatedDate { get; set; }

            public int ModifiedBy { get; set; }

            public DateTime DateModified { get; set; }

            public Boolean IsActive { get; set; }

            public string FirstName { get; set; }

            public string LastName { get; set; }

            public string Ttile { get; set; }

            public DataRowState oprType { get; set; }

            public int FleetUserId { get; set; }

        }


    }


}
