using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using NaveoOneLib.Services;
using System.Data;
using System.Data.Common;

namespace NaveoOneLib.Services.Users
{
    public class UserProfilesService
    {

       Errors er = new Errors();
        public DataTable GetUserProfiles(String sConnStr)
        {
            String sqlString = @"SELECT UID, 
                                        UserId, 
                                        Command, 
                                        AccessRights from GFI_SYS_UserProfiles order by UID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

     
        public UserProfile GetUserProfilesById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT UID, 
                                                UserId, 
                                                Command, 
                                                AccessRights from GFI_SYS_UserProfiles
                                                WHERE UID = ?
                                                order by UID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                UserProfile retUserProfiles = new UserProfile();
                retUserProfiles.UID = Convert.ToInt32(dt.Rows[0]["UID"]);
                retUserProfiles.UserId = Convert.ToInt32(dt.Rows[0]["UserId"]);
                retUserProfiles.Command = dt.Rows[0]["Command"].ToString();


                retUserProfiles.AllowRead = dt.Rows[0]["AllowRead"].ToString();
                retUserProfiles.AllowNew = dt.Rows[0]["AllowNew"].ToString();
                retUserProfiles.AllowEdit = dt.Rows[0]["AllowEdit"].ToString();
                retUserProfiles.AllowDelete = dt.Rows[0]["AllowDelete"].ToString();

                return retUserProfiles;
            }
        }

        //Updated by Vishal-05.11.14
        //----------------------------------------------------------

        public static List<UserProfile> GetUserAccessProfilesById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT GFI_SYS_User.UID, 
                                        Username, 
                                        GFI_SYS_Modules.Command as Command, 
                                        AllowRead,AllowNew,AllowEdit,AllowDelete from GFI_SYS_User
                                        
                                        LEFT OUTER JOIN GFI_SYS_AccessProfiles ON GFI_SYS_User.AccessTemplateId = GFI_SYS_AccessProfiles.TemplateId
                                        LEFT OUTER JOIN GFI_SYS_Modules ON GFI_SYS_AccessProfiles.ModuleId = GFI_SYS_Modules.UID
                                        WHERE GFI_SYS_User.UID = ?
                                        order by GFI_SYS_User.UID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID,sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {                
                List<UserProfile> retUserProfiles = new List<UserProfile>();
                foreach (DataRow data in dt.Rows)
                {
                    retUserProfiles.Add(new UserProfile
                    {
                        UID = Convert.ToInt32(data["UID"]),
                        Command = data["Command"].ToString(),
                        AllowRead = data["AllowRead"].ToString(),
                        AllowNew = data["AllowNew"].ToString(),
                        AllowEdit = data["AllowEdit"].ToString(),
                        AllowDelete = data["AllowDelete"].ToString()
                    });
                }
                return retUserProfiles;
            }
        }


        //public bool SaveUserProfiles(UserProfiles uUserProfiles, DbTransaction transaction)
        //{
        //    DataTable dt = new DataTable();
        //    dt.TableName = "GFI_SYS_UserProfiles";
        //    dt.Columns.Add("UID", uUserProfiles.UID.GetType());
        //    dt.Columns.Add("UserId", uUserProfiles.UserId.GetType());
        //    dt.Columns.Add("Command", uUserProfiles.Command.GetType());
        //    dt.Columns.Add("AccessRights", uUserProfiles.AccessRights.GetType());
        //    DataRow dr = dt.NewRow();
        //    dr["UID"] = uUserProfiles.UID;
        //    dr["UserId"] = uUserProfiles.UserId;
        //    dr["Command"] = uUserProfiles.Command;
        //    dr["AccessRights"] = uUserProfiles.AccessRights;
        //    dt.Rows.Add(dr);

        //    Connection _connection = new Connection(sConnStr); ;
        //    if (_connection.GenInsert(dt, transaction))
        //        return true;
        //    else
        //        return false;
        //}

        public bool SaveUserProfiles(UserProfile uUserProfiles, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_UserProfiles";
            dt.Columns.Add("UID", uUserProfiles.UID.GetType());
            dt.Columns.Add("UserId", uUserProfiles.UserId.GetType());
            dt.Columns.Add("Command", uUserProfiles.Command.GetType());
            dt.Columns.Add("AllowRead", uUserProfiles.AllowRead.GetType());
            dt.Columns.Add("AllowNew", uUserProfiles.AllowNew.GetType());
            dt.Columns.Add("AllowEdit", uUserProfiles.AllowEdit.GetType());
            dt.Columns.Add("AllowDelete", uUserProfiles.AllowDelete.GetType());
            DataRow dr = dt.NewRow();
            dr["UID"] = uUserProfiles.UID;
            dr["UserId"] = uUserProfiles.UserId;
            dr["Command"] = uUserProfiles.Command;
            dr["AllowRead"] = uUserProfiles.AllowRead;
            dr["AllowNew"] = uUserProfiles.AllowNew;
            dr["AllowEdit"] = uUserProfiles.AllowEdit;
            dr["AllowDelete"] = uUserProfiles.AllowDelete;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenInsert(dt, transaction, sConnStr))
                return true;
            else
                return false;
        }


        //public bool UpdateUserProfiles(UserProfiles uUserProfiles, DbTransaction transaction)
        //{
        //    DataTable dt = new DataTable();
        //    dt.TableName = "GFI_SYS_UserProfiles";
        //    dt.Columns.Add("UID", uUserProfiles.UID.GetType());
        //    dt.Columns.Add("UserId", uUserProfiles.UserId.GetType());
        //    dt.Columns.Add("Command", uUserProfiles.Command.GetType());
        //    dt.Columns.Add("AccessRights", uUserProfiles.AccessRights.GetType());
        //    DataRow dr = dt.NewRow();
        //    dr["UID"] = uUserProfiles.UID;
        //    dr["UserId"] = uUserProfiles.UserId;
        //    dr["Command"] = uUserProfiles.Command;
        //    dr["AccessRights"] = uUserProfiles.AccessRights;
        //    dt.Rows.Add(dr);

        //    Connection _connection = new Connection(sConnStr); ;
        //    if (_connection.GenUpdate(dt, "UID = '" + uUserProfiles.UID + "'", transaction))
        //        return true;
        //    else
        //        return false;
        //}

        public bool UpdateUserProfiles(UserProfile uUserProfiles, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_UserProfiles";
            dt.Columns.Add("UID", uUserProfiles.UID.GetType());
            dt.Columns.Add("UserId", uUserProfiles.UserId.GetType());
            dt.Columns.Add("Command", uUserProfiles.Command.GetType());
            dt.Columns.Add("AllowRead", uUserProfiles.AllowRead.GetType());
            dt.Columns.Add("AllowNew", uUserProfiles.AllowNew.GetType());
            dt.Columns.Add("AllowEdit", uUserProfiles.AllowEdit.GetType());
            dt.Columns.Add("AllowDelete", uUserProfiles.AllowDelete.GetType());
            DataRow dr = dt.NewRow();
            dr["UID"] = uUserProfiles.UID;
            dr["UserId"] = uUserProfiles.UserId;
            dr["Command"] = uUserProfiles.Command;
            dr["AllowRead"] = uUserProfiles.AllowRead;
            dr["AllowNew"] = uUserProfiles.AllowNew;
            dr["AllowEdit"] = uUserProfiles.AllowEdit;
            dr["AllowDelete"] = uUserProfiles.AllowDelete;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "UID = '" + uUserProfiles.UID + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }

        //-------------End of Update-----------------------------
        public bool DeleteUserProfiles(UserProfile uUserProfiles, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_SYS_UserProfiles", "UID = '" + uUserProfiles.UID + "'", sConnStr))
                return true;
            else
                return false;
        }
    }
    

}
