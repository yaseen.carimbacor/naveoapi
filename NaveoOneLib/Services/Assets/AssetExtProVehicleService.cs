using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Assets;

namespace NaveoOneLib.Services.Assets
{
    public class AssetExtProVehicleService
    {

        Errors er = new Errors();

        public String sCheckAssetExtProVehicles(int iAssetID)
        {
            String sql = @"SELECT *
                                 FROM GFI_AMM_AssetExtProVehicles 
                                 WHERE AssetId = " + iAssetID.ToString() + @" 
                                 ORDER BY AssetId";
            return sql;
        }
        public DataTable CheckAssetExtProVehicles(String sAssetId, String sConnStr)
        {
            return new Connection(sConnStr).GetDataDT(sCheckAssetExtProVehicles(Convert.ToInt32(sAssetId)), null);
        }

        public String sGetAssetExtProVehicles(String sAssetsList)
        {
            String sqlString = @"SELECT a.AssetId, 
                                    '' as RegistrationNo,
                                    v.VIN, 
                                    v.Description, 
                                    v.Make, 
                                    v.Model, 
                                    v.VehicleTypeId_cbo, 
                                    vt.Description AS TypeDesc,
                                    v.Manufacturer, 
                                    v.YearManufactured, 
                                    v.PurchasedDate, 
                                    v.PurchasedValue, 
                                    v.PORef, 
                                    v.ExpectedLifetime, 
                                    v.ResidualValue, 
                                    v.EngineSerialNumber, 
                                    v.EngineType, 
                                    v.EngineCapacity,
                                    v.EnginePower,
                                    v.FuelType, 
                                    v.StdConsumption, 
                                    v.ExternalReference, 
                                    v.InService_b, 
                                    v.AdditionalInfo, 
                                    v.CreatedDate, 
                                    v.CreatedBy, 
                                    v.UpdatedDate, 
                                    v.UpdatedBy ,
                                       v.[Field1Value]
                                      ,v.[Field2Value]
                                      ,v.[Field3Value]
                                      ,v.[Field4Value]
                                      ,v.[Field5Value]
                                      ,v.[Field6Value]
                                      ,a.[AssetType]      
        
                                 FROM GFI_FLT_Asset a 
										LEFT OUTER JOIN GFI_AMM_AssetExtProVehicles v
											ON a.AssetId = v.AssetId
										LEFT OUTER JOIN GFI_AMM_VehicleTypes vt
											ON v.VehicleTypeId_cbo = vt.VehicleTypeId   
                                 WHERE a.AssetId IN (" + sAssetsList + ") ORDER BY a.AssetId";

            return sqlString;
        }
        public DataTable GetAssetExtProVehicles(String sAssetsList, String sConnStr)
        {
            return new Connection(sConnStr).GetDataDT(sGetAssetExtProVehicles(sAssetsList), sConnStr);
        }

        public String sGetAssetExtProVehiclesById(int AssetID)
        {
            String sql = @"SELECT a.AssetId, 
                                    '' as RegistrationNo,
                                    v.VIN, 
                                    v.Description, 
                                    v.Make, 
                                    v.Model, 
                                    v.VehicleTypeId_cbo, 
                                    vt.Description AS TypeDesc,
                                    v.Manufacturer, 
                                    v.YearManufactured, 
                                    v.PurchasedDate, 
                                    v.PurchasedValue, 
                                    v.PORef, 
                                    v.ExpectedLifetime, 
                                    v.ResidualValue, 
                                    v.EngineSerialNumber, 
                                    v.EngineType, 
                                    v.EngineCapacity,
                                    v.EnginePower,
                                    v.FuelType, 
                                    v.StdConsumption, 
                                    v.ExternalReference, 
                                    v.InService_b, 
                                    v.AdditionalInfo, 
                                    v.CreatedDate, 
                                    v.CreatedBy, 
                                    v.UpdatedDate, 
                                    v.UpdatedBy 
                                 FROM GFI_FLT_Asset a 
										LEFT OUTER JOIN GFI_AMM_AssetExtProVehicles v
											ON a.AssetId = v.AssetId
										LEFT OUTER JOIN GFI_AMM_VehicleTypes vt
											ON v.VehicleTypeId_cbo = vt.VehicleTypeId
                                 WHERE a.AssetId = " + AssetID.ToString() + @" 
                                 ORDER BY a.AssetId";
            return sql;
        }
        public AssetExtProVehicle GetAssetExtProVehiclesById(String iID, String sConnStr)
        {
            DataTable dt = new Connection(sConnStr).GetDataDT(sGetAssetExtProVehiclesById(Convert.ToInt32(iID)), null);
            if (dt.Rows.Count == 0)
                return null;
            else
                return AEPV(dt.Rows[0]);
        }
        public AssetExtProVehicle AEPV(DataRow dr)
        {
            AssetExtProVehicle retAssetExtProVehicles = new AssetExtProVehicle();

            retAssetExtProVehicles.AssetId = Convert.ToInt32(dr["AssetId"]);
            if (dr.Table.Columns.Contains("RegistrationNo"))
                retAssetExtProVehicles.RegistrationNo = dr["RegistrationNo"].ToString();
            retAssetExtProVehicles.VIN = dr["VIN"].ToString();
            retAssetExtProVehicles.Description = dr["Description"].ToString();
            retAssetExtProVehicles.Make = dr["Make"].ToString();
            retAssetExtProVehicles.Model = dr["Model"].ToString();

            if (!String.IsNullOrEmpty(dr["VehicleTypeId_cbo"].ToString()))
                retAssetExtProVehicles.VehicleTypeId_cbo = Convert.ToInt32(dr["VehicleTypeId_cbo"]);
            else
                retAssetExtProVehicles.VehicleTypeId_cbo = 0;

            if (dr.Table.Columns.Contains("TypeDesc"))
                retAssetExtProVehicles.VehicleTypeDesc = dr["TypeDesc"].ToString();

            retAssetExtProVehicles.Manufacturer = dr["Manufacturer"].ToString();

            if (!String.IsNullOrEmpty(dr["YearManufactured"].ToString()))
                retAssetExtProVehicles.YearManufactured = Convert.ToInt32(dr["YearManufactured"]);
            else
                retAssetExtProVehicles.YearManufactured = 0;

            if (!String.IsNullOrEmpty(dr["PurchasedDate"].ToString()))
                retAssetExtProVehicles.PurchasedDate = (DateTime)dr["PurchasedDate"];

            if (!String.IsNullOrEmpty(dr["PurchasedValue"].ToString()))
                retAssetExtProVehicles.PurchasedValue = (decimal)dr["PurchasedValue"];
            else
                retAssetExtProVehicles.PurchasedValue = 0;

            retAssetExtProVehicles.PORef = dr["PORef"].ToString();

            if (!String.IsNullOrEmpty(dr["ExpectedLifetime"].ToString()))
                retAssetExtProVehicles.ExpectedLifetime = Convert.ToInt32(dr["ExpectedLifetime"]);
            else
                retAssetExtProVehicles.ExpectedLifetime = 0;

            if (!String.IsNullOrEmpty(dr["ResidualValue"].ToString()))
                retAssetExtProVehicles.ResidualValue = (decimal)dr["ResidualValue"];
            else
                retAssetExtProVehicles.ResidualValue = 0;

            retAssetExtProVehicles.EngineSerialNumber = dr["EngineSerialNumber"].ToString();
            retAssetExtProVehicles.EngineType = dr["EngineType"].ToString();

            if (!String.IsNullOrEmpty(dr["EngineCapacity"].ToString()))
                retAssetExtProVehicles.EngineCapacity = Convert.ToInt32(dr["EngineCapacity"]);
            else
                retAssetExtProVehicles.EngineCapacity = 0;

            if (!String.IsNullOrEmpty(dr["EnginePower"].ToString()))
                retAssetExtProVehicles.EnginePower = Convert.ToInt32(dr["EnginePower"]);
            else
                retAssetExtProVehicles.EnginePower = 0;

            retAssetExtProVehicles.FuelType = dr["FuelType"].ToString();

            if (!String.IsNullOrEmpty(dr["StdConsumption"].ToString()))
                retAssetExtProVehicles.StdConsumption = (decimal)dr["StdConsumption"];
            else
                retAssetExtProVehicles.StdConsumption = 0;

            retAssetExtProVehicles.ExternalReference = dr["ExternalReference"].ToString();

            retAssetExtProVehicles.InService_b = dr["InService_b"].ToString();

            retAssetExtProVehicles.AdditionalInfo = dr["AdditionalInfo"].ToString();

            if (!String.IsNullOrEmpty(dr["CreatedDate"].ToString()))
                retAssetExtProVehicles.CreatedDate = (DateTime)dr["CreatedDate"];

            if (!String.IsNullOrEmpty(dr["UpdatedDate"].ToString()))
                retAssetExtProVehicles.UpdatedDate = (DateTime)dr["UpdatedDate"];

            if (!String.IsNullOrEmpty(dr["CreatedBy"].ToString()))
                retAssetExtProVehicles.CreatedBy = Convert.ToInt32(dr["CreatedBy"].ToString());
            if (!String.IsNullOrEmpty(dr["UpdatedBy"].ToString()))
                retAssetExtProVehicles.UpdatedBy = Convert.ToInt32(dr["UpdatedBy"].ToString());

            return retAssetExtProVehicles;
        }

        public DataSet GetDetails(int AssetID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = sGetAssetExtProVehiclesById(AssetID);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtAEPV";
            dtSql.Rows.Add(drSql);

            sql = new VehicleMaintTypeLinkService().sGetVehicleMaintTypeLinkForAssetId(AssetID);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtVMTL";
            dtSql.Rows.Add(drSql);

            sql = new AssetExtProXTService().sGetAssetExtProXTForAssetId(AssetID);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtAEPXT";
            dtSql.Rows.Add(drSql);

            sql = sCheckAssetExtProVehicles(AssetID);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtCAEPV";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            foreach (DataRow dr in ds.Tables["dtVMTL"].Rows)
                dr.AcceptChanges();

            return ds;
        }

        private DataTable DTVehicleMaintType(DataTable dsVehicleMaintType, int iRowId, DataRowState soprType)
        {
            DataTable dt = new DataTable();

            dt.TableName = "GFI_AMM_VehicleMaintTypesLink";

            dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Columns.Add("URI", typeof(Int32));
            dt.Columns.Add("AssetId", typeof(Int32));
            dt.Columns.Add("MaintTypeId", typeof(String));
            dt.Columns.Add("CreatedDate", typeof(DateTime));
            dt.Columns.Add("CreatedBy", typeof(String));
            dt.Columns.Add("UpdatedDate", typeof(DateTime));
            dt.Columns.Add("UpdatedBy", typeof(String));

            DataRow dr = dt.NewRow();


            dr["oprType"] = soprType;
            if (soprType == DataRowState.Deleted)
            {
                dr["URI"] = int.Parse(dsVehicleMaintType.Rows[iRowId]["URI", DataRowVersion.Original].ToString());
            }
            else
            {
                dr["AssetId"] = int.Parse(dsVehicleMaintType.Rows[iRowId]["AssetId"].ToString());
                dr["MaintTypeId"] = dsVehicleMaintType.Rows[iRowId]["MaintTypeId"].ToString();
                dr["CreatedDate"] = (DateTime)dsVehicleMaintType.Rows[iRowId]["CreatedDate"];
                dr["CreatedBy"] = dsVehicleMaintType.Rows[iRowId]["CreatedBy"].ToString();
                dr["UpdatedDate"] = (DateTime)dsVehicleMaintType.Rows[iRowId]["UpdatedDate"];
                dr["UpdatedBy"] = dsVehicleMaintType.Rows[iRowId]["UpdatedBy"].ToString();
            }

            dt.Rows.Add(dr);

            return dt;

        }

        public bool SaveAsset(AssetExtProVehicle myAsset, String sConnStr)
        {
            DataTable dt = new DataTable();

            dt.TableName = "GFI_AMM_AssetExtProVehicles";
            dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Columns.Add("AssetId", typeof(int));
            dt.Columns.Add("Description", typeof(String));
            dt.Columns.Add("CreatedDate", typeof(DateTime));
            dt.Columns.Add("CreatedBy", typeof(int));

            DataRow dr = dt.NewRow();

            dr["oprType"] = DataRowState.Added;
            dr["AssetId"] = myAsset.AssetId;
            dr["Description"] = myAsset.Description;
            dr["CreatedDate"] = DateTime.UtcNow;
            dr["CreatedBy"] = Globals.uLogin.UID;

            dt.Rows.Add(dr);

            //Process Data
            Connection _connection = new Connection(sConnStr);

            DataTable CTRLTBL = _connection.CTRLTBL();
            DataSet dsProcess = new DataSet();

            DataRow drCtrl = CTRLTBL.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AssetId";
            drCtrl["KeyIsIdentity"] = "FALSE";

            CTRLTBL.Rows.Add(drCtrl);

            dsProcess.Merge(CTRLTBL);

            dsProcess.Merge(dt);

            DataSet dsResults = _connection.ProcessData(dsProcess, sConnStr);

            CTRLTBL = dsResults.Tables["CTRLTBL"];

            if (CTRLTBL != null)
            {
                DataRow[] dRow = CTRLTBL.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }

        public bool UpdateAsset(AssetExtProVehicle myAsset, String sConnStr)
        {
            DataTable dt = new DataTable();

            dt.TableName = "GFI_AMM_AssetExtProVehicles";
            dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Columns.Add("AssetId", typeof(int));
            dt.Columns.Add("Description", typeof(String));
            dt.Columns.Add("CreatedDate", typeof(DateTime));
            dt.Columns.Add("CreatedBy", typeof(String));

            DataRow dr = dt.NewRow();

            dr["oprType"] = DataRowState.Modified;
            dr["AssetId"] = myAsset.AssetId;
            dr["Description"] = myAsset.Description;
            dr["CreatedDate"] = DateTime.UtcNow;
            dr["CreatedBy"] = Globals.uLogin.Username;

            dt.Rows.Add(dr);

            //Process Data
            Connection _connection = new Connection(sConnStr);

            DataTable CTRLTBL = _connection.CTRLTBL();
            DataSet dsProcess = new DataSet();

            DataRow drCtrl = CTRLTBL.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AssetId";
            drCtrl["KeyIsIdentity"] = "FALSE";

            CTRLTBL.Rows.Add(drCtrl);

            dsProcess.Merge(CTRLTBL);

            dsProcess.Merge(dt);

            DataSet dsResults = _connection.ProcessData(dsProcess, sConnStr);

            CTRLTBL = dsResults.Tables["CTRLTBL"];

            if (CTRLTBL != null)
            {
                DataRow[] dRow = CTRLTBL.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }

        public Boolean SaveAssetExtProVehicles(AssetExtProVehicle uAssetExtProVehicles, DataRowState sAssetoprType, DataTable dtVehicleMaintType, String sAssetList, out DataSet dsResults, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();

            dt.TableName = "GFI_AMM_AssetExtProVehicles";
            dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Columns.Add("AssetId", uAssetExtProVehicles.AssetId.GetType());
            dt.Columns.Add("VIN", uAssetExtProVehicles.VIN.GetType());
            dt.Columns.Add("Description", uAssetExtProVehicles.Description.GetType());
            dt.Columns.Add("Make", uAssetExtProVehicles.Make.GetType());
            dt.Columns.Add("Model", uAssetExtProVehicles.Model.GetType());
            //dt.Columns.Add("VehicleTypeId_cbo", uAssetExtProVehicles.VehicleTypeId_cbo.GetType());
            dt.Columns.Add("Manufacturer", uAssetExtProVehicles.Manufacturer.GetType());
            dt.Columns.Add("YearManufactured", uAssetExtProVehicles.YearManufactured.GetType());
            dt.Columns.Add("PurchasedDate", uAssetExtProVehicles.PurchasedDate.GetType());
            dt.Columns.Add("PurchasedValue", uAssetExtProVehicles.PurchasedValue.GetType());
            dt.Columns.Add("PORef", uAssetExtProVehicles.PORef.GetType());
            dt.Columns.Add("ExpectedLifetime", uAssetExtProVehicles.ExpectedLifetime.GetType());
            dt.Columns.Add("ResidualValue", uAssetExtProVehicles.ResidualValue.GetType());
            dt.Columns.Add("EngineSerialNumber", uAssetExtProVehicles.EngineSerialNumber.GetType());
            dt.Columns.Add("EngineType", uAssetExtProVehicles.EngineType.GetType());
            dt.Columns.Add("EngineCapacity", uAssetExtProVehicles.EngineCapacity.GetType());
            dt.Columns.Add("EnginePower", uAssetExtProVehicles.EnginePower.GetType());
            dt.Columns.Add("FuelType", uAssetExtProVehicles.FuelType.GetType());
            dt.Columns.Add("StdConsumption", uAssetExtProVehicles.StdConsumption.GetType());
            dt.Columns.Add("ExternalReference", uAssetExtProVehicles.ExternalReference.GetType());
            dt.Columns.Add("InService_b", uAssetExtProVehicles.InService_b.GetType());
            dt.Columns.Add("AdditionalInfo", uAssetExtProVehicles.AdditionalInfo.GetType());
            dt.Columns.Add("CreatedDate", uAssetExtProVehicles.CreatedDate.GetType());
            dt.Columns.Add("CreatedBy", typeof(int));
            dt.Columns.Add("UpdatedDate", uAssetExtProVehicles.UpdatedDate.GetType());
            dt.Columns.Add("UpdatedBy", typeof(int));
            DataRow dr = dt.NewRow();

            dr["AssetId"] = uAssetExtProVehicles.AssetId;
            dr["oprType"] = sAssetoprType;
            dr["VIN"] = uAssetExtProVehicles.VIN;
            dr["Description"] = uAssetExtProVehicles.Description;
            dr["Make"] = uAssetExtProVehicles.Make;
            dr["Model"] = uAssetExtProVehicles.Model;
            //dr["VehicleTypeId_cbo"] = uAssetExtProVehicles.VehicleTypeId_cbo;
            dr["Manufacturer"] = uAssetExtProVehicles.Manufacturer;
            dr["YearManufactured"] = uAssetExtProVehicles.YearManufactured;
            dr["PurchasedDate"] = uAssetExtProVehicles.PurchasedDate;
            dr["PurchasedValue"] = uAssetExtProVehicles.PurchasedValue;
            dr["PORef"] = uAssetExtProVehicles.PORef;
            dr["ExpectedLifetime"] = uAssetExtProVehicles.ExpectedLifetime;
            dr["ResidualValue"] = uAssetExtProVehicles.ResidualValue;
            dr["EngineSerialNumber"] = uAssetExtProVehicles.EngineSerialNumber;
            dr["EngineType"] = uAssetExtProVehicles.EngineType;
            dr["EngineCapacity"] = uAssetExtProVehicles.EngineCapacity;
            dr["EnginePower"] = uAssetExtProVehicles.EnginePower;
            dr["FuelType"] = uAssetExtProVehicles.FuelType;
            dr["StdConsumption"] = uAssetExtProVehicles.StdConsumption;
            dr["ExternalReference"] = uAssetExtProVehicles.ExternalReference;
            dr["InService_b"] = uAssetExtProVehicles.InService_b;
            dr["AdditionalInfo"] = uAssetExtProVehicles.AdditionalInfo;
            dr["CreatedDate"] = uAssetExtProVehicles.CreatedDate;
            if (!String.IsNullOrEmpty(uAssetExtProVehicles.CreatedBy.ToString()))
                dr["CreatedBy"] = uAssetExtProVehicles.CreatedBy;
            dr["UpdatedDate"] = uAssetExtProVehicles.UpdatedDate;
            if (!String.IsNullOrEmpty(uAssetExtProVehicles.UpdatedBy.ToString()))
                dr["UpdatedBy"] = uAssetExtProVehicles.UpdatedBy;
            dt.Rows.Add(dr);

            //Process Data
            Connection _connection = new Connection(sConnStr);

            DataTable CTRLTBL = _connection.CTRLTBL();
            DataSet dsProcess = new DataSet();

            DataRow drCtrl = CTRLTBL.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AssetId";
            drCtrl["KeyIsIdentity"] = "FALSE";

            CTRLTBL.Rows.Add(drCtrl);
            dsProcess.Merge(dt);

            if (dtVehicleMaintType.Rows.Count > 0)
            {
                Boolean bCtrlNeeded = false;
                int iSeqNoStart = 3;

                //Update Maintenance Types
                for (int iRow = 0; iRow < dtVehicleMaintType.Rows.Count; iRow++)
                {
                    switch (dtVehicleMaintType.Rows[iRow].RowState)
                    {
                        case DataRowState.Added:
                            int iAssetId = Convert.ToInt32(dtVehicleMaintType.Rows[iRow]["AssetId"]);
                            int iMaintTypeId = Convert.ToInt32(dtVehicleMaintType.Rows[iRow]["MaintTypeId"]);

                            bCtrlNeeded = true;
                            dsProcess.Merge(DTVehicleMaintType(dtVehicleMaintType, iRow, DataRowState.Added));

                            DataRow dRowCtrl = CTRLTBL.NewRow();
                            dRowCtrl["SeqNo"] = iSeqNoStart + iRow;
                            dRowCtrl["TblName"] = "GFI_AMM_VehicleMaintenance_" + dRowCtrl["SeqNo"].ToString();
                            dRowCtrl["CmdType"] = "STOREDPROC";
                            dRowCtrl["Command"] = "EXEC sp_AMM_VehicleMaint_CreateChildRecords " + iAssetId + ", " + iMaintTypeId + "";
                            CTRLTBL.Rows.Add(dRowCtrl);
                            break;

                        case DataRowState.Modified:
                            bCtrlNeeded = true;
                            dsProcess.Merge(DTVehicleMaintType(dtVehicleMaintType, iRow, DataRowState.Modified));
                            break;

                        case DataRowState.Deleted:
                            bCtrlNeeded = true;
                            dsProcess.Merge(DTVehicleMaintType(dtVehicleMaintType, iRow, DataRowState.Deleted));
                            break;
                    }
                }

                if (bCtrlNeeded)
                {
                    drCtrl = CTRLTBL.NewRow();
                    drCtrl["SeqNo"] = 2;
                    drCtrl["TblName"] = "GFI_AMM_VehicleMaintTypesLink";
                    drCtrl["CmdType"] = "TABLE";
                    drCtrl["KeyFieldName"] = "URI";
                    drCtrl["KeyIsIdentity"] = "TRUE";
                    CTRLTBL.Rows.Add(drCtrl);
                }
            }

            #region GetData
            if (sAssetList.Trim().Length > 0)
            {
                DataTable dtSql = _connection.dtSql();
                DataRow drSql = dtSql.NewRow();

                String sql = sGetAssetExtProVehicles(sAssetList);
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtGAEPV";
                dtSql.Rows.Add(drSql);

                sql = sGetAssetExtProVehiclesById(uAssetExtProVehicles.AssetId);
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtAEPV";
                dtSql.Rows.Add(drSql);

                sql = new VehicleMaintTypeLinkService().sGetVehicleMaintTypeLinkForAssetId(uAssetExtProVehicles.AssetId);
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtVMTL";
                dtSql.Rows.Add(drSql);

                sql = new AssetExtProXTService().sGetAssetExtProXTForAssetId(uAssetExtProVehicles.AssetId);
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtAEPXT";
                dtSql.Rows.Add(drSql);

                sql = sCheckAssetExtProVehicles(uAssetExtProVehicles.AssetId);
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtCAEPV";
                dtSql.Rows.Add(drSql);
                dsProcess.Merge(dtSql);
            }
            #endregion

            dsProcess.Merge(CTRLTBL);
            dsResults = _connection.ProcessData(dsProcess, sConnStr);
            foreach (DataRow dri in dsResults.Tables["dtVMTL"].Rows)
                dri.AcceptChanges();

            CTRLTBL = dsResults.Tables["CTRLTBL"];
           
            if (CTRLTBL != null)
            {
                DataRow[] dRow = CTRLTBL.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }
    }
}
