using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Assets;

namespace NaveoOneLib.Services.Assets
{
    public class VehicleTypesService
    {
        Errors er = new Errors();

        public String sGetVehicleTypes()
        {
            String sqlString = @"SELECT VehicleTypeId, 
                    Description, 
                    CreatedDate, 
                    CreatedBy, 
                    UpdatedDate, 
                    UpdatedBy from GFI_AMM_VehicleTypes order by VehicleTypeId";

            return sqlString;
        }

        public string sGetRoadSpeedType()
        {
            String sqlString = @"SELECT VehicleType, 
                    iID
                    --MotorWay, 
                    --RoadA, 
                    --RoadB, 
                   -- RoadC,
                   -- RoadD,
                   -- RoadE,
                   -- RoadG,
                   -- RoadH 
                    from GFI_FLT_RoadSpeedType order by iID";

            return sqlString;


        }

        public DataTable GetRoadSpeedType(string sConnStr)
        {
            DataTable dt = new Connection(sConnStr).GetDataDT(sGetRoadSpeedType(), sConnStr);
            return dt;
        }

        public DataTable GetVehicleTypes( String sConnStr)
        {
            return new Connection(sConnStr).GetDataDT(sGetVehicleTypes(),sConnStr);
        }

        VehicleType getVT(DataRow dr)
        {
            VehicleType retVehicleTypes = new VehicleType();
            retVehicleTypes.VehicleTypeId = Convert.ToInt32(dr["VehicleTypeId"]);
            retVehicleTypes.Description = dr["Description"].ToString();
            retVehicleTypes.CreatedDate = (DateTime)dr["CreatedDate"];
            retVehicleTypes.CreatedBy = dr["CreatedBy"].ToString();
            retVehicleTypes.UpdatedDate = (DateTime)dr["UpdatedDate"];
            retVehicleTypes.UpdatedBy = dr["UpdatedBy"].ToString();
            return retVehicleTypes;

        }
        public VehicleType GetVehicleTypesById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT VehicleTypeId, 
                    Description, 
                    CreatedDate, 
                    CreatedBy, 
                    UpdatedDate, 
                    UpdatedBy from GFI_AMM_VehicleTypes
                    WHERE VehicleTypeId = ?
                    order by VehicleTypeId";

            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID,sConnStr);

            if (dt.Rows.Count == 0)
                return null;
            else
                return getVT(dt.Rows[0]);
        }

        public VehicleType GetVehicleTypesByDescription(String sDescription, String sConnStr)
        {
            String sqlString = @"SELECT VehicleTypeId, 
                    Description, 
                    CreatedDate, 
                    CreatedBy, 
                    UpdatedDate, 
                    UpdatedBy from GFI_AMM_VehicleTypes
                    WHERE Description = ?
                    order by Description";

            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, sDescription, sConnStr);

            if (dt.Rows.Count == 0)
                return null;
            else
            {
                VehicleType retVehicleTypes = new VehicleType();
                retVehicleTypes.VehicleTypeId = Convert.ToInt32(dt.Rows[0]["VehicleTypeId"]);
                retVehicleTypes.Description = dt.Rows[0]["Description"].ToString();
                retVehicleTypes.CreatedDate = (DateTime)dt.Rows[0]["CreatedDate"];
                retVehicleTypes.CreatedBy = dt.Rows[0]["CreatedBy"].ToString();
                retVehicleTypes.UpdatedDate = (DateTime)dt.Rows[0]["UpdatedDate"];
                retVehicleTypes.UpdatedBy = dt.Rows[0]["UpdatedBy"].ToString();
                return retVehicleTypes;
            }
        }

        public bool SaveVehicleTypes(VehicleType uVehicleTypes, DbTransaction transaction, String sConnStr)
        {
            //--- Prepare Dataset ---//

            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_VehicleTypes";
            if (!dt.Columns.Contains("oprType"))
                dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Columns.Add("VehicleTypeId", typeof(int));
            dt.Columns.Add("Description", typeof(String));
            dt.Columns.Add("CreatedDate", typeof(DateTime));
            dt.Columns.Add("CreatedBy", typeof(String));
            dt.Columns.Add("UpdatedDate", typeof(DateTime));
            dt.Columns.Add("UpdatedBy", typeof(String));
            DataRow dr = dt.NewRow();

            dr["oprType"] = DataRowState.Added;
            dr["VehicleTypeId"] = uVehicleTypes.VehicleTypeId;
            dr["Description"] = uVehicleTypes.Description;
            dr["CreatedDate"] = uVehicleTypes.CreatedDate;
            dr["CreatedBy"] = uVehicleTypes.CreatedBy;
            dr["UpdatedDate"] = uVehicleTypes.UpdatedDate;
            dr["UpdatedBy"] = uVehicleTypes.UpdatedBy;
            dt.Rows.Add(dr);

            //--- Prepare Control Table ---//

            Connection _connection = new Connection(sConnStr);

            DataSet dsProcess = new DataSet();
            DataTable CTRLTBL = _connection.CTRLTBL();

            dsProcess.Tables.Add(CTRLTBL);
            dsProcess.Merge(dt);

            DataRow drCtrl = CTRLTBL.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "VehicleTypeId";
            drCtrl["KeyIsIdentity"] = "FALSE";
            CTRLTBL.Rows.Add(drCtrl);

            //--- Process Data ---//

            CTRLTBL = _connection.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            if (CTRLTBL != null)
            {
                DataRow[] dRow = CTRLTBL.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }

        public bool UpdateVehicleTypes(VehicleType uVehicleTypes, DbTransaction transaction, String sConnStr)
        {
            //--- Prepare Dataset ---//

            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_VehicleTypes";
            if (!dt.Columns.Contains("oprType"))
                dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Columns.Add("VehicleTypeId", uVehicleTypes.VehicleTypeId.GetType());
            dt.Columns.Add("Description", uVehicleTypes.Description.GetType());
            dt.Columns.Add("CreatedDate", uVehicleTypes.CreatedDate.GetType());
            dt.Columns.Add("CreatedBy", uVehicleTypes.CreatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uVehicleTypes.UpdatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uVehicleTypes.UpdatedBy.GetType());

            DataRow dr = dt.NewRow();
            dr["oprType"] = DataRowState.Modified;
            dr["VehicleTypeId"] = uVehicleTypes.VehicleTypeId;
            dr["Description"] = uVehicleTypes.Description;
            dr["CreatedDate"] = uVehicleTypes.CreatedDate;
            dr["CreatedBy"] = uVehicleTypes.CreatedBy;
            dr["UpdatedDate"] = uVehicleTypes.UpdatedDate;
            dr["UpdatedBy"] = uVehicleTypes.UpdatedBy;
            dt.Rows.Add(dr);

            //--- Prepare Control Table ---//

            Connection _connection = new Connection(sConnStr);

            DataSet dsProcess = new DataSet();
            DataTable CTRLTBL = _connection.CTRLTBL();

            dsProcess.Tables.Add(CTRLTBL);
            dsProcess.Merge(dt);

            DataRow drCtrl = CTRLTBL.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "VehicleTypeId";
            drCtrl["KeyIsIdentity"] = "FALSE";
            CTRLTBL.Rows.Add(drCtrl);

            //--- Process Data ---//

            CTRLTBL = _connection.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            if (CTRLTBL != null)
            {
                DataRow[] dRow = CTRLTBL.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }

        public bool DeleteVehicleTypes(VehicleType uVehicleTypes, String sConnStr)
        {
            //--- Prepare Dataset ---//

            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_VehicleTypes";
            dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Columns.Add("VehicleTypeId", uVehicleTypes.VehicleTypeId.GetType());

            DataRow dr = dt.NewRow();
            dr["oprType"] = DataRowState.Deleted;
            dr["VehicleTypeId"] = uVehicleTypes.VehicleTypeId;

            dt.Rows.Add(dr);

            //--- Prepare Control Table ---//

            Connection _connection = new Connection(sConnStr);

            DataTable CTRLTBL = _connection.CTRLTBL();
            CTRLTBL.TableName = "CTRLTBL";

            DataSet objDataSet = new DataSet();

            objDataSet.Tables.Add(CTRLTBL);
            objDataSet.Merge(dt);

            DataRow drCtrl = CTRLTBL.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "VehicleTypeId";
            drCtrl["KeyIsIdentity"] = "FALSE";
            CTRLTBL.Rows.Add(drCtrl);

            //--- Process Data ---//

            CTRLTBL = _connection.ProcessData(objDataSet, sConnStr).Tables["CTRLTBL"];

            if (CTRLTBL != null)
            {
                DataRow[] dRow = CTRLTBL.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }
    }
}