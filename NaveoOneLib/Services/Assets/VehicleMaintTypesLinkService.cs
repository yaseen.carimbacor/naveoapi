﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Assets;

namespace NaveoOneLib.Services.Assets
{
    public class VehicleMaintTypeLinkService
    {
        Errors er = new Errors();

        public DataTable GetVehicleMaintList(String sConnStr)
        {
            String sqlString = @"SELECT URI
                                  ,vmtl.MaintURI,
                                  ,vmtl.AssetId
                                  ,vmtl.MaintTypeId
                                  ,vmt.Description
                                  ,NextMaintDate
                                  ,NextMaintDateTG
                                  ,CurrentOdometer
                                  ,NextMaintOdometer
                                  ,NextMaintOdometerTG
                                  ,CurrentEngHrs
                                  ,NextMaintEngHrs
                                  ,NextMaintEngHrsTG
                                  ,Status
                                FROM GFI_AMM_VehicleMaintTypesLink vmtl
                                    INNER JOIN GFI_AMM_VehicleMaintTypes vmt
                                           ON vmtl.MaintTypeId = vmt.MaintTypeId
                                    INNER JOIN GFI_FLT_Asset a
                                ON vmtl.AssetId = a.AssetId";

            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public DataTable GetVehicleMaintList2(String AssetsList, String sConnStr)
        {
            String sqlString = @"SELECT
                                   vmtl.MaintURI
                                  ,vmtl.AssetId
                                  ,a.AssetNumber
                                  ,vmtl.MaintTypeId
                                  ,vmt.Description AS [MaintTypesDesc]
                                  ,vmtl.NextMaintDate
                                  ,vmtl.NextMaintDateTG
                                  ,vmtl.CurrentOdometer
                                  ,vmtl.NextMaintOdometer
                                  ,vmtl.NextMaintOdometerTG
                                  ,vmtl.CurrentEngHrs
                                  ,vmtl.NextMaintEngHrs
                                  ,vmtl.NextMaintEngHrsTG
                                  ,vms.Description AS [MaintStatusDesc]
                                  ,vm.TotalCost
                                FROM
                                  GFI_AMM_VehicleMaintTypesLink vmtl
                                  LEFT OUTER JOIN GFI_AMM_VehicleMaintStatus vms
                                    ON vmtl.Status = vms.MaintStatusId
                                  INNER JOIN GFI_AMM_VehicleMaintTypes vmt
                                    ON vmtl.MaintTypeId = vmt.MaintTypeId
                                  INNER JOIN GFI_FLT_Asset a
                                    ON vmtl.AssetId = a.AssetId
                                  INNER JOIN GFI_AMM_VehicleMaintenance vm
                                    ON vmtl.MaintURI = vm.URI
                                  
                                WHERE vmtl.AssetId IN (" + AssetsList + ") ORDER BY vms.SortOrder ASC, vmtl.NextMaintDate DESC";

            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public DataTable GetAllVehicleMaintList(String AssetsList, String sConnStr)
        {
            String sqlString = @"SELECT
                                   vmtl.MaintURI
                                  ,vmtl.AssetId
                                  ,a.AssetNumber
                                  ,vmtl.MaintTypeId
                                  ,vmt.Description AS [MaintTypesDesc]
                                  ,vmtl.NextMaintDate
                                  ,vmtl.NextMaintDateTG
                                  ,vmtl.CurrentOdometer
                                  ,vmtl.NextMaintOdometer
                                  ,vmtl.NextMaintOdometerTG
                                  ,vmtl.CurrentEngHrs
                                  ,vmtl.NextMaintEngHrs
                                  ,vmtl.NextMaintEngHrsTG
                                  ,vms.Description AS [MaintStatusDesc]
                                  ,vm.TotalCost
                                FROM
                                  GFI_AMM_VehicleMaintTypesLink vmtl
                                  LEFT OUTER JOIN GFI_AMM_VehicleMaintStatus vms
                                    ON vmtl.Status = vms.MaintStatusId
                                  INNER JOIN GFI_AMM_VehicleMaintTypes vmt
                                    ON vmtl.MaintTypeId = vmt.MaintTypeId
                                  INNER JOIN GFI_FLT_Asset a
                                    ON vmtl.AssetId = a.AssetId
                                  INNER JOIN GFI_AMM_VehicleMaintenance vm
                                    ON vmtl.MaintURI = vm.URI
                                  
                                WHERE vmtl.AssetId IN (" + AssetsList + ") ORDER BY vms.SortOrder ASC, vmtl.NextMaintDate DESC";

            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public DataTable GetOpenVehicleMaintList(String AssetsList, String sConnStr)
        {
            String sqlString = @"SELECT
                                   vmtl.MaintURI
                                  ,vmtl.AssetId
                                  ,a.AssetNumber
                                  ,vmtl.MaintTypeId
                                  ,vmt.Description AS [MaintTypesDesc]
                                  ,vmtl.NextMaintDate
                                  ,vmtl.NextMaintDateTG
                                  ,vmtl.CurrentOdometer
                                  ,vmtl.NextMaintOdometer
                                  ,vmtl.NextMaintOdometerTG
                                  ,vmtl.CurrentEngHrs
                                  ,vmtl.NextMaintEngHrs
                                  ,vmtl.NextMaintEngHrsTG
                                  ,vms.Description AS [MaintStatusDesc]
                                     
      
      ,vm.[ContactDetails]
      ,vm.[CompanyName]
      ,vm.[PhoneNumber]
      ,vm.[CompanyRef]
      ,vm.[CompanyRef2]
      ,vm.[MaintDescription]
      ,vm.[StartDate]
      ,vm.[EndDate]
   ,vm.[EstimatedValue]
      ,vm.[VATInclInItemsAmt]
      ,vm.[VATAmount]
     ,(vm.[TotalCost] - vm.[VATAmount]) as Cost
      ,vm.[TotalCost]
   ,vm.[CalculatedOdometer]
      ,vm.[ActualOdometer]
      ,vm.[CalculatedEngineHrs]
      ,vm.[ActualEngineHrs]
      ,vm.[AdditionalInfo]

   ,vt.Description as MaintType
      ,vs.Description as MainStatusType
      ,vc.Description as MainCoverType
                                  
          FROM
                                  GFI_AMM_VehicleMaintTypesLink vmtl
                                  LEFT OUTER JOIN GFI_AMM_VehicleMaintStatus vms  ON vmtl.Status = vms.MaintStatusId
                                  INNER JOIN GFI_AMM_VehicleMaintTypes vmt ON vmtl.MaintTypeId = vmt.MaintTypeId
                                  INNER JOIN GFI_FLT_Asset a  ON vmtl.AssetId = a.AssetId
                                  INNER JOIN GFI_AMM_VehicleMaintenance vm ON vmtl.MaintURI = vm.URI                                  

          --INNER JOIN GFI_AMM_VehicleMaintTypes vt ON vm.MaintTypeId_cbo = vt.MaintTypeId

          --INNER JOIN GFI_AMM_VehicleMaintStatus vs ON vm.MaintStatusId_cbo = vs.MaintStatusId

          --INNER JOIN GFI_AMM_InsCoverTypes vc ON vm.CoverTypeId_cbo = vc.CoverTypeId

          LEFT OUTER JOIN GFI_AMM_VehicleMaintTypes vt ON vm.MaintTypeId_cbo = vt.MaintTypeId

          LEFT OUTER JOIN GFI_AMM_VehicleMaintStatus vs ON vm.MaintStatusId_cbo = vs.MaintStatusId

          LEFT OUTER JOIN GFI_AMM_InsCoverTypes vc ON vm.CoverTypeId_cbo = vc.CoverTypeId
                                  
                                WHERE vmtl.AssetId IN (" + AssetsList + ") and vms.Description NOT IN ('COMPLETED') ORDER BY vms.SortOrder ASC, vmtl.NextMaintDate DESC";

            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        public String sGetVehicleMaintTypeLinkForAssetId(int AssetID)
        {
            String sql = @" SELECT vmtl.URI, 
                                    vmtl.MaintURI, 
                                    vmtl.AssetId, 
                                    vmtl.MaintTypeId,
                                    vmt.Description,
                                    vmtl.Status, 
                                    vms.Description AS MaintStatusDesc, 
                                    vmtl.CreatedDate, 
                                    vmtl.CreatedBy, 
                                    vmtl.UpdatedDate, 
                                    vmtl.UpdatedBy 
                               FROM GFI_AMM_VehicleMaintTypesLink vmtl
			                    INNER JOIN dbo.GFI_AMM_VehicleMaintTypes vmt
				                    ON vmtl.MaintTypeId = vmt.MaintTypeId 
                                LEFT OUTER JOIN dbo.GFI_AMM_VehicleMaintStatus vms
                                    ON vmtl.Status = vms.MaintStatusId
                               WHERE vmtl.AssetId = " + AssetID.ToString() + @"
                               ORDER BY vmtl.MaintTypeId DESC";
            return sql;
        }
        public DataTable GetVehicleMaintTypeLinkForAssetId(String iAssetId, String sConnStr)
        {
            return new Connection(sConnStr).GetDataDT(sGetVehicleMaintTypeLinkForAssetId(Convert.ToInt32(iAssetId)), null);
        }

        public DataTable GetVehicleMaintTypeLinkForAssetIdMaintTypeId(String iAssetId, String iMaintTypeId, String sConnStr)
        {
            String sqlString = @" SELECT vmtl.URI, 
                                    vmtl.MaintURI, 
                                    vmtl.AssetId, 
                                    vmtl.MaintTypeId,
                                    vmt.Description,
                                    vmtl.Status, 
                                    vms.Description AS MaintStatusDesc, 
                                    vmtl.CreatedDate, 
                                    vmtl.CreatedBy, 
                                    vmtl.UpdatedDate, 
                                    vmtl.UpdatedBy 
                               FROM GFI_AMM_VehicleMaintTypesLink vmtl
			                    INNER JOIN GFI_AMM_VehicleMaintTypes vmt
				                    ON vmtl.MaintTypeId = vmt.MaintTypeId 
                                INNER JOIN dbo.GFI_AMM_VehicleMaintStatus vms
                                    ON vmtl.Status = vms.MaintStatusId
                               WHERE vmtl.AssetId = " + iAssetId + " AND vmtl.MaintTypeId = " + iMaintTypeId + 
                               " ORDER BY vmtl.MaintTypeId DESC";

            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public DataTable GetVehicleMaintTypeLinkByMaintURI(String iMaintURI, String sConnStr)
        {
            String sqlString = @" SELECT vmtl.URI, 
                                    vmtl.MaintURI, 
                                    vmtl.AssetId, 
                                    vmtl.MaintTypeId,
                                    vmt.Description,
                                    vmtl.Status, 
                                    vms.Description AS MaintStatusDesc, 
                                    vmtl.CreatedDate, 
                                    vmtl.CreatedBy, 
                                    vmtl.UpdatedDate, 
                                    vmtl.UpdatedBy 
                               FROM GFI_AMM_VehicleMaintTypesLink vmtl
			                    INNER JOIN GFI_AMM_VehicleMaintTypes vmt
				                    ON vmtl.MaintTypeId = vmt.MaintTypeId 
                                INNER JOIN dbo.GFI_AMM_VehicleMaintStatus vms
                                    ON vmtl.Status = vms.MaintStatusId
                               WHERE vmtl.iMaintURI = " + iMaintURI + " ORDER BY vmtl.MaintTypeId DESC";

            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        public bool UpdateVehicleMaintTypeLink(VehicleMaintTypeLink uVehicleMaintTypeLink, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_VehicleMaintTypesLink";
            dt.Columns.Add("MaintURI", uVehicleMaintTypeLink.MaintURI.GetType());
            dt.Columns.Add("AssetId", uVehicleMaintTypeLink.AsssetId.GetType());
            dt.Columns.Add("MaintTypeId", uVehicleMaintTypeLink.MaintTypeId.GetType());
            dt.Columns.Add("NextMaintDate", uVehicleMaintTypeLink.NextMaintDate.GetType());
            dt.Columns.Add("NextMaintDateTG", uVehicleMaintTypeLink.NextMaintDateTG.GetType());
            dt.Columns.Add("CurrentOdometer", uVehicleMaintTypeLink.CurrentOdometer.GetType());
            dt.Columns.Add("NextMaintOdometer", uVehicleMaintTypeLink.NextMaintOdometer.GetType());
            dt.Columns.Add("NextMaintOdometerTG", uVehicleMaintTypeLink.NextMaintOdometerTG.GetType());
            dt.Columns.Add("CurrentEngHrs", uVehicleMaintTypeLink.CurrentEngHrs.GetType());
            dt.Columns.Add("NextMaintEngHrs", uVehicleMaintTypeLink.NextMaintEngHrs.GetType());
            dt.Columns.Add("NextMaintEngHrsTG", uVehicleMaintTypeLink.NextMaintEngHrsTG.GetType());
            dt.Columns.Add("Status", uVehicleMaintTypeLink.Status.GetType());            
            dt.Columns.Add("CreatedDate", uVehicleMaintTypeLink.CreatedDate.GetType());
            dt.Columns.Add("CreatedBy", uVehicleMaintTypeLink.CreatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uVehicleMaintTypeLink.UpdatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uVehicleMaintTypeLink.UpdatedBy.GetType());

            DataRow dr = dt.NewRow();
            dr["MaintURI"] = uVehicleMaintTypeLink.MaintURI;
            dr["AssetId"] = uVehicleMaintTypeLink.AsssetId;
            dr["MaintTypeId"] = uVehicleMaintTypeLink.MaintTypeId;
            dr["NextMaintDate"] = uVehicleMaintTypeLink.NextMaintDate;
            dr["NextMaintDateTG"] = uVehicleMaintTypeLink.NextMaintDateTG;
            dr["CurrentOdometer"] = uVehicleMaintTypeLink.CurrentOdometer;
            dr["NextMaintOdometer"] = uVehicleMaintTypeLink.NextMaintOdometer;
            dr["NextMaintOdometerTG"] = uVehicleMaintTypeLink.NextMaintOdometerTG;
            dr["CurrentEngHrs"] = uVehicleMaintTypeLink.CurrentEngHrs;
            dr["NextMaintEngHrs"] = uVehicleMaintTypeLink.NextMaintEngHrs;
            dr["NextMaintEngHrsTG"] = uVehicleMaintTypeLink.NextMaintEngHrsTG;
            dr["Status"] = uVehicleMaintTypeLink.Status;
            dr["CreatedDate"] = uVehicleMaintTypeLink.CreatedDate;
            dr["CreatedBy"] = uVehicleMaintTypeLink.CreatedBy;
            dr["UpdatedDate"] = uVehicleMaintTypeLink.UpdatedDate;
            dr["UpdatedBy"] = uVehicleMaintTypeLink.UpdatedBy;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "AssetId = '" + uVehicleMaintTypeLink.AsssetId + "' AND MaintTypeId = '" + uVehicleMaintTypeLink.MaintTypeId + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }
        public int ProcessEvents(String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);

            DataTable CTRLTBL = _connection.CTRLTBL();
            DataSet dsProcess = new DataSet();

            DataRow dRowCtrl = CTRLTBL.NewRow();
            dRowCtrl["SeqNo"] = 1;
            dRowCtrl["TblName"] = "GFI_AMM_VehicleMaintenance";
            dRowCtrl["CmdType"] = "STOREDPROC";
            dRowCtrl["Command"] = "EXEC sp_AMM_VehicleMaint_ProcessRecords";
            CTRLTBL.Rows.Add(dRowCtrl);

            dsProcess.Merge(CTRLTBL);

            CTRLTBL = _connection.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            if (CTRLTBL != null)
            {
                DataRow[] dRow = CTRLTBL.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    return 0;
                else
                    return 1;
            }
            else
                return 0;
         }
    }
}
