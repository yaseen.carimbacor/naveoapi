﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Services.Audits;

namespace NaveoOneLib.Services.Assets
{
    class VehicleMaintItemsService
    {

        Errors er = new Errors();

        public String sGetVehicleMaintItemsById(String pMaintURI)
        {
            String sqlString = @"SELECT URI, 
                                    MaintURI, 
                                    ItemCode,
                                    Description, 
                                    Quantity, 
                                    UnitCost, 
                                    CreatedDate, 
                                    CreatedBy, 
                                    UpdatedDate, 
                                    UpdatedBy 
                                  FROM GFI_AMM_VehicleMaintItems
                                  WHERE MaintURI = " + pMaintURI + @"
                                  ORDER BY URI";
            return sqlString;
        }

        public DataTable GetVehicleMaintItemsById(String pMaintURI, String sConnStr)
        {
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sGetVehicleMaintItemsById(pMaintURI), null, sConnStr); 

            return dt;
        }

        public Boolean SaveVehicleMaintItems(VehicleMaintItem uVehicleMaintItems, Boolean bInsert, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_VehicleMaintItems";
            dt.Columns.Add("URI", uVehicleMaintItems.URI.GetType());
            dt.Columns.Add("MaintURI", uVehicleMaintItems.MaintURI.GetType());
            dt.Columns.Add("ItemCode", uVehicleMaintItems.ItemCode.GetType());
            dt.Columns.Add("Description", uVehicleMaintItems.Description.GetType());
            dt.Columns.Add("Quantity", uVehicleMaintItems.Quantity.GetType());
            dt.Columns.Add("UnitCost", uVehicleMaintItems.UnitCost.GetType());
            dt.Columns.Add("CreatedDate", uVehicleMaintItems.CreatedDate.GetType());
            dt.Columns.Add("CreatedBy", uVehicleMaintItems.CreatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uVehicleMaintItems.UpdatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uVehicleMaintItems.UpdatedBy.GetType());

            DataRow dr = dt.NewRow();
            dr["URI"] = uVehicleMaintItems.URI;
            dr["MaintURI"] = uVehicleMaintItems.MaintURI;
            dr["ItemCode"] = uVehicleMaintItems.ItemCode;
            dr["Description"] = uVehicleMaintItems.Description;
            dr["Quantity"] = uVehicleMaintItems.Quantity;
            dr["UnitCost"] = uVehicleMaintItems.UnitCost;
            dr["CreatedDate"] = uVehicleMaintItems.CreatedDate;
            dr["CreatedBy"] = uVehicleMaintItems.CreatedBy;
            dr["UpdatedDate"] = uVehicleMaintItems.UpdatedDate;
            dr["UpdatedBy"] = uVehicleMaintItems.UpdatedBy;
            dt.Rows.Add(dr);


            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "URI";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uVehicleMaintItems.URI;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public Boolean DeleteVehicleMaintItems(VehicleMaintItem uVehicleMaintItems, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_AMM_VehicleMaintItems where URI = " + uVehicleMaintItems.URI.ToString();
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            DataTable dtAudit = new AuditService().LogTran(3, "VehicleMaintItems", uVehicleMaintItems.URI.ToString(), Globals.uLogin.UID, uVehicleMaintItems.URI);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "CallerFunction";
            drCtrl["ParentTblName"] = "GFI_AMM_VehicleMaintItems";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
    }
}
