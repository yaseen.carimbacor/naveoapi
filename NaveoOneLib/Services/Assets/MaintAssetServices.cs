﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NaveoOneLib.Models.Common;
using NaveoOneLib.DBCon;
using System.Data;
using NaveoOneLib.Models.Users;
using System.Data.SqlTypes;
using System.IO;
using System.Drawing;
using NaveoOneLib.Common;
using System.Drawing.Imaging;
using System.Web;
using System.Configuration;
using Telerik.WinControls;

namespace NaveoService.Services
{
    public class MaintAssetServices
    {
        DataTable CustomerDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "dbo.\"Customer\"";
            dt.Columns.Add("\"Id\"", typeof(int));
            dt.Columns.Add("\"Type\"", typeof(int));
            dt.Columns.Add("\"CustomerId\"", typeof(int));
            dt.Columns.Add("\"CustomerRef\"", typeof(String));
            dt.Columns.Add("\"CustomerFullName\"", typeof(String));
            dt.Columns.Add("\"CustomerTelephone\"", typeof(String));
            dt.Columns.Add("\"CustomerAddress\"", typeof(String));
            dt.Columns.Add("\"CustomerJDECode\"", typeof(String));
            dt.Columns.Add("\"CustomerVATNo\"", typeof(String));
            dt.Columns.Add("\"CustomerBRN\"", typeof(String));
            dt.Columns.Add("\"CustomerEmailAddress\"", typeof(String));
            dt.Columns.Add("\"CustomerFax\"", typeof(String));
            dt.Columns.Add("\"CustomerMarkUpCategory\"", typeof(int));
            dt.Columns.Add("\"UseMarkup\"", typeof(int));
            dt.Columns.Add("\"Department\"", typeof(String));
            dt.Columns.Add("\"CreatedBy\"", typeof(String));
            dt.Columns.Add("\"DateCreated\"", typeof(DateTime));
            dt.Columns.Add("\"UpdatedBy\"", typeof(String));
            dt.Columns.Add("\"DateUpdated\"", typeof(DateTime));
            dt.Columns.Add("\"IsFlagged\"", typeof(int));
            dt.Columns.Add("oprType", typeof(DataRowState));

            return dt;
        }

        DataTable VehicleMaintItemsDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "dbo.\"GFI_AMM_VehicleMaintItems\"";
            dt.Columns.Add("\"URI\"", typeof(int));
            dt.Columns.Add("\"MaintURI\"", typeof(int));
            dt.Columns.Add("\"ItemCode\"", typeof(String));
            dt.Columns.Add("\"Description\"", typeof(String));
            dt.Columns.Add("\"Quantity\"", typeof(double));
            dt.Columns.Add("\"UnitCost\"", typeof(double));
            dt.Columns.Add("\"CreatedDate\"", typeof(DateTime));
            dt.Columns.Add("\"CreatedBy\"", typeof(String));
            dt.Columns.Add("\"UpdatedDate\"", typeof(DateTime));
            dt.Columns.Add("\"UpdatedBy\"", typeof(String));
            dt.Columns.Add("oprType", typeof(DataRowState));

            return dt;
        }

        DataTable AssetMaintenanceTypeDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "dbo.\"AssetMaintenaceType\"";
            dt.Columns.Add("\"AssetMaintenceId\"", typeof(int));
            dt.Columns.Add("\"AssetId\"", typeof(int));
            dt.Columns.Add("\"MaintenceId\"", typeof(int));
            dt.Columns.Add("oprType", typeof(DataRowState));

            return dt;
        }

        DataTable CustomerVehicleDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "dbo.\"CustomerVehicle\"";
            dt.Columns.Add("\"Id\"", typeof(int));
            dt.Columns.Add("\"CustomerId\"", typeof(int));
            dt.Columns.Add("\"Number\"", typeof(String));
            dt.Columns.Add("\"OriginalNumber\"", typeof(String));
            dt.Columns.Add("\"Make\"", typeof(int));
            dt.Columns.Add("\"Model\"", typeof(int));
            dt.Columns.Add("\"CustomerName\"", typeof(String));
            dt.Columns.Add("\"FuelType\"", typeof(int));
            dt.Columns.Add("\"ChassisNo\"", typeof(String));
            dt.Columns.Add("\"Category\"", typeof(int));
            dt.Columns.Add("\"Status\"", typeof(int));
            dt.Columns.Add("\"RoadTaxExpiryDate\"", typeof(DateTime));
            dt.Columns.Add("\"PSVLicenseExpiryDate\"", typeof(DateTime));
            dt.Columns.Add("\"FitnessExpiryDate\"", typeof(DateTime));
            dt.Columns.Add("\"RoadTaxAmount\"", typeof(double));
            dt.Columns.Add("\"FitnessAmount\"", typeof(double));
            dt.Columns.Add("\"PSVAmount\"", typeof(double));
            dt.Columns.Add("\"CreatedBy\"", typeof(String));
            dt.Columns.Add("\"DateCreated\"", typeof(DateTime));
            dt.Columns.Add("\"UpdatedBy\"", typeof(String));
            dt.Columns.Add("\"DateUpdated\"", typeof(DateTime));
            dt.Columns.Add("\"IsFlagged\"", typeof(int));
            dt.Columns.Add("\"FleetServerId\"", typeof(int));
            dt.Columns.Add("\"FMSAssetId\"", typeof(int));
            dt.Columns.Add("\"FMSAssetType\"", typeof(int));
            dt.Columns.Add("\"Description\"", typeof(String));
            dt.Columns.Add("\"Section\"", typeof(int));
            dt.Columns.Add("\"VehicleType\"", typeof(int));
            dt.Columns.Add("\"Colour\"", typeof(String));
            dt.Columns.Add("\"SupplierId\"", typeof(int));
            dt.Columns.Add("\"YearManufactured\"", typeof(int));
            dt.Columns.Add("\"PurchasedDate\"", typeof(DateTime));
            dt.Columns.Add("\"RegistrationDate\"", typeof(DateTime));
            dt.Columns.Add("\"PurchasedValue\"", typeof(double));
            dt.Columns.Add("\"ExpectedLifetime\"", typeof(int));
            dt.Columns.Add("\"ResidualValue\"", typeof(String));
            dt.Columns.Add("\"Reference\"", typeof(String));
            dt.Columns.Add("\"IsInService\"", typeof(int));
            dt.Columns.Add("\"EngineSerialNumber\"", typeof(String));
            dt.Columns.Add("\"EngineType\"", typeof(String));
            dt.Columns.Add("\"EngineCapacity\"", typeof(String));
            dt.Columns.Add("\"EnginePower\"", typeof(String));
            dt.Columns.Add("\"StandardConsumption\"", typeof(String));
            dt.Columns.Add("\"Department\"", typeof(String));
            dt.Columns.Add("\"Remarks\"", typeof(String));
            dt.Columns.Add("oprType", typeof(DataRowState));

            return dt;
        }


        DataTable MaintenanceVehicleDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "dbo.\"GFI_AMM_VehicleMaintenance\"";
            dt.Columns.Add("\"URI\"", typeof(int));
            dt.Columns.Add("\"AssetId\"", typeof(int));
            dt.Columns.Add("\"MaintTypeId_cbo\"", typeof(int));
            dt.Columns.Add("\"ContactDetails\"", typeof(String));
            dt.Columns.Add("\"CompanyName\"", typeof(String));
            dt.Columns.Add("\"PhoneNumber\"", typeof(String));
            dt.Columns.Add("\"CompanyRef\"", typeof(String));
            dt.Columns.Add("\"CompanyRef2\"", typeof(String));
            dt.Columns.Add("\"MaintDescription\"", typeof(String));
            dt.Columns.Add("\"StartDate\"", typeof(DateTime));
            dt.Columns.Add("\"EndDate\"", typeof(DateTime));
            dt.Columns.Add("\"MaintStatusId_cbo\"", typeof(int));
            dt.Columns.Add("\"EstimatedValue\"", typeof(double));
            dt.Columns.Add("\"VATInclInItemsAmt\"", typeof(String));
            dt.Columns.Add("\"VATAmount\"", typeof(double));
            dt.Columns.Add("\"TotalCost\"", typeof(double));
            dt.Columns.Add("\"CoverTypeId_cbo\"", typeof(int));
            dt.Columns.Add("\"CalculatedOdometer\"", typeof(int));
            dt.Columns.Add("\"ActualOdometer\"", typeof(int));
            dt.Columns.Add("\"CalculatedEngineHrs\"", typeof(int));
            dt.Columns.Add("\"ActualEngineHrs\"", typeof(int));
            dt.Columns.Add("\"AdditionalInfo\"", typeof(String));
            dt.Columns.Add("\"CreatedDate\"", typeof(DateTime));
            dt.Columns.Add("\"CreatedBy\"", typeof(String));
            dt.Columns.Add("\"UpdatedDate\"", typeof(DateTime));
            dt.Columns.Add("\"UpdatedBy\"", typeof(String));
            dt.Columns.Add("\"AssetStatus\"", typeof(String));
            dt.Columns.Add("\"Comment\"", typeof(String));
            dt.Columns.Add("\"Reminder\"", typeof(DateTime));
            dt.Columns.Add("\"NextMaintenance\"", typeof(DateTime));
            dt.Columns.Add("oprType", typeof(DataRowState));

            return dt;
        }

        DataTable MaintenanceVehicleHistoDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "dbo.\"GFI_AMM_VehicleMaintenanceHisto\"";
            dt.Columns.Add("\"URIHisto\"", typeof(int));
            dt.Columns.Add("\"URI\"", typeof(int));
            dt.Columns.Add("\"AssetId\"", typeof(int));
            dt.Columns.Add("\"MaintTypeId_cbo\"", typeof(int));
            dt.Columns.Add("\"MaintDescription\"", typeof(String));
            dt.Columns.Add("\"StartDate\"", typeof(DateTime));
            dt.Columns.Add("\"EndDate\"", typeof(DateTime));
            dt.Columns.Add("\"MaintStatusId_cboNew\"", typeof(int));
            dt.Columns.Add("\"MaintStatusId_cboOld\"", typeof(int));
            dt.Columns.Add("\"CreatedDate\"", typeof(DateTime));
            dt.Columns.Add("\"flagTreated\"", typeof(int));
            dt.Columns.Add("\"UserId\"", typeof(String));
            dt.Columns.Add("\"RuleId\"", typeof(int));
            dt.Columns.Add("\"NewURI\"", typeof(int));
            dt.Columns.Add("oprType", typeof(DataRowState));

            return dt;
        }

        public string getServerName()
        {
            string sServerName = System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            User.NaveoDatabases myEnum;
            //Getting only the enum part
            sServerName = sServerName.Replace(".naveo.mu", String.Empty);
            //sServerName = sServerName.Replace(":44393", String.Empty);
            sServerName = sServerName.Replace("https://", String.Empty);
            sServerName = sServerName.Replace("http://", String.Empty);
            if (Enum.TryParse(sServerName, true, out myEnum))
                sServerName = sServerName;
            else
                sServerName = sServerName;



            sServerName = sServerName.ToUpper();


            return sServerName;
        }

        public BaseModel SaveMaintAsset(MaintAsset maintAsset, Boolean bInsert, String sConnStr)
        {
            BaseModel baseModel = new BaseModel();
            Boolean bResult = false;
            baseModel.data = bResult;



            DataSet dsProcess = new DataSet();
            DataTable dt = CustomerDT();
            DataTable dtVehicle = CustomerVehicleDT();

            //Table FleetServers
            //sServerName    

            string sServerName = DBConn.Common.ConnName.GetDBName(sConnStr);


            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            sConnStr = "MaintDB";
            string customerId = string.Empty;
            string assetId = string.Empty;
            int serverId = 0;

            //Get Server Id

            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = "select 'FleetServer' as TableName;\r\n";
            sql += "select * from dbo.\"FleetServer\" where \"Name\" = '" + sServerName.ToUpper() + "';\r\n";
            sql += "select 'Customer' as TableName;\r\n";
            sql += "SELECT c.* FROM dbo.\"CustomerVehicle\" cv, dbo.\"Customer\" c, dbo.\"FleetServer\" fs where c.\"Id\" = cv.\"CustomerId\" and cv.\"FMSAssetId\" = '" + maintAsset.AssetID + "' AND cv.\"FleetServerId\" = fs.\"Id\" AND fs.\"Name\" = '" + sServerName.ToUpper() + "' AND cv.\"Remarks\" = '" + maintAsset.Type + "';\r\n";
            sql += "select 'CustomerVehicle' as TableName;\r\n";
            sql += "select cv.* from dbo.\"CustomerVehicle\" cv, dbo.\"FleetServer\" fs where cv.\"FMSAssetId\" = '" + maintAsset.AssetID + "' AND cv.\"FleetServerId\" = fs.\"Id\" AND fs.\"Name\" = '" + sServerName.ToUpper() + "' AND cv.\"Remarks\" = '" + maintAsset.Type + "';";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, NaveoOneLib.Constant.NaveoEntity.MaintDB);
            DataTable dtGM = ds.Tables["FleetServer"];

            if (dtGM == null)
                return baseModel;

            if (dtGM.Rows.Count > 0)
            {
                DataRow row = dtGM.Rows[0];

                serverId = int.Parse(row["Id"].ToString());
            }

            //check if customer exist
            DataTable dtCustomer = ds.Tables["Customer"];

            if (dtCustomer.Rows.Count > 0)
            {
                DataRow row = dtCustomer.Rows[0];

                customerId = row["Id"].ToString();
            }

            DataTable dtCustomerVehicle = ds.Tables["CustomerVehicle"];
            if (dtCustomerVehicle.Rows.Count > 0)
            {
                DataRow row = dtCustomerVehicle.Rows[0];

                assetId = row["Id"].ToString();

                //GetId here

                if (bInsert)
                {
                    baseModel.data = false;
                    return baseModel;
                }
            }

            int.TryParse(customerId, out int CustomerId);
            int.TryParse(assetId, out int AssetId);

            //If update no need to recreate asset
            if (CustomerId == 0)    //new customer
            {
                //Customer table
                DataRow drCustomer = dt.NewRow();
                drCustomer["\"CustomerFullName\""] = maintAsset.AssetCode;
                drCustomer["\"Type\""] = 1;
                drCustomer["\"CreatedBy\""] = "Asset.Webservice";
                drCustomer["\"DateCreated\""] = DateTime.Now;
                drCustomer["\"CustomerRef\""] = "WEBSERV" + DateTime.Now.ToString().Replace("/", "").Replace(" ", "").Replace(":", "");
                //drCustomer["\"IsFlagged\""] = 0;
                dt.Columns.Remove("\"IsFlagged\"");
                drCustomer["\"CustomerMarkUpCategory\""] = 0;
                //drCustomer["\"UseMarkup\""] = 0;
                dt.Columns.Remove("\"UseMarkup\"");
                drCustomer["\"CustomerId\""] = 0;
                drCustomer["oprType"] = DataRowState.Added;
                dt.Rows.Add(drCustomer);

                drCtrl["SeqNo"] = 1;
                drCtrl["TblName"] = dt.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "\"Id\"";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdAction"] = "GET";
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dt);
            }

            DataRow drCustomerVehicle = dtVehicle.NewRow();
            if (!bInsert)
            {
                drCustomerVehicle["\"Id\""] = AssetId;
                drCustomerVehicle["oprType"] = DataRowState.Modified;
            }
            else
            {
                drCustomerVehicle["oprType"] = DataRowState.Added;
            }
            drCustomerVehicle["\"CreatedBy\""] = "Asset.Webservice";
            drCustomerVehicle["\"DateCreated\""] = DateTime.Now;
            drCustomerVehicle["\"DateUpdated\""] = (DateTime)SqlDateTime.MinValue;
            drCustomerVehicle["\"Category\""] = 0;
            drCustomerVehicle["\"ChassisNo\""] = "";
            drCustomerVehicle["\"Colour\""] = maintAsset.sColor != null ? maintAsset.sColor : (object)DBNull.Value;
            drCustomerVehicle["\"CustomerName\""] = maintAsset.AssetName;
            drCustomerVehicle["\"Department\""] = 0;
            drCustomerVehicle["\"Description\""] = maintAsset.Descript;
            drCustomerVehicle["\"EngineCapacity\""] = 0;
            drCustomerVehicle["\"EnginePower\""] = 0;
            drCustomerVehicle["\"EngineSerialNumber\""] = maintAsset.EngineSerialNumber != null ? maintAsset.EngineSerialNumber : (object)DBNull.Value;
            drCustomerVehicle["\"EngineType\""] = 0;
            drCustomerVehicle["\"ExpectedLifetime\""] = 0;
            drCustomerVehicle["\"FitnessAmount\""] = 0;
            drCustomerVehicle["\"FitnessExpiryDate\""] = (DateTime)SqlDateTime.MinValue;
            drCustomerVehicle["\"FleetServerId\""] = serverId;
            drCustomerVehicle["\"FMSAssetId\""] = maintAsset.AssetID;
            drCustomerVehicle["\"FMSAssetType\""] = maintAsset.AssetType;
            drCustomerVehicle["\"FuelType\""] = maintAsset.FuelType != null ? maintAsset.FuelType : 0;
            // drCustomerVehicle["\"IsFlagged\""] = 0;
            dtVehicle.Columns.Remove("\"IsFlagged\"");
            //drCustomerVehicle["\"IsInService\""] = 1;
            dtVehicle.Columns.Remove("\"IsInService\"");
            drCustomerVehicle["\"Make\""] = 9;
            drCustomerVehicle["\"Model\""] = 8;
            drCustomerVehicle["\"Number\""] = maintAsset.AssetCode;
            drCustomerVehicle["\"OriginalNumber\""] = 0;
            drCustomerVehicle["\"PSVAmount\""] = 0;
            drCustomerVehicle["\"PSVLicenseExpiryDate\""] = (DateTime)SqlDateTime.MinValue;
            drCustomerVehicle["\"PurchasedDate\""] = maintAsset.PurchasedDate != null ? maintAsset.PurchasedDate : (DateTime)SqlDateTime.MinValue;
            drCustomerVehicle["\"PurchasedValue\""] = maintAsset.PurchasedValue != null ? maintAsset.PurchasedValue : 0;
            drCustomerVehicle["\"Reference\""] = 0;
            drCustomerVehicle["\"RegistrationDate\""] = (DateTime)SqlDateTime.MinValue;
            drCustomerVehicle["\"Remarks\""] = maintAsset.Type;
            drCustomerVehicle["\"ResidualValue\""] = 0;
            drCustomerVehicle["\"RoadTaxAmount\""] = 0;
            drCustomerVehicle["\"RoadTaxExpiryDate\""] = (DateTime)SqlDateTime.MinValue;
            drCustomerVehicle["\"Section\""] = 0;
            drCustomerVehicle["\"StandardConsumption\""] = maintAsset.RefD != null ? maintAsset.RefD : (object)DBNull.Value;
            drCustomerVehicle["\"Status\""] = 0;
            drCustomerVehicle["\"SupplierId\""] = 0;
            drCustomerVehicle["\"VehicleType\""] = "0";
            drCustomerVehicle["\"YearManufactured\""] = 0;

            dtVehicle.Rows.Add(drCustomerVehicle);

            drCtrl = dtCtrl.NewRow();
            drCtrl["ParentTblName"] = dt.TableName;
            drCtrl["SeqNo"] = 2;
            drCtrl["TblName"] = dtVehicle.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "\"Id\"";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (CustomerId == 0)    //new customer
            {
                drCtrl["NextIdAction"] = "SET";
                drCtrl["NextIdFieldName"] = "\"CustomerId\"";
            }
            else
                drCtrl["NextIdFieldName"] = CustomerId;

            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtVehicle);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, NaveoOneLib.Constant.NaveoEntity.MaintDB);
            dtCtrl = dsResult.Tables["CTRLTBL"];

            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");
                if (dRow.Length > 0)
                {
                    baseModel.dbResult = dsResult;
                    baseModel.dbResult.Tables["CTRLTBL"].TableName = "ResultCtrlTbl";
                    baseModel.dbResult.Merge(dsProcess);
                    bResult = false;
                }
                else
                    bResult = true;
            }

            baseModel.data = bResult;
            return baseModel;
        }

        public static class ConfigurationKeys
        {
            public const string GMSUploadFile = "GMSUploadFile";
        }

        //private static readonly string rootExportFolderPath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings[ConfigurationKeys.GMSUploadFile]);

        public bool ExistAsset(int AssetId, string sConnStr)
        {
            bool pContinue = false;

            Connection c = new Connection(NaveoOneLib.Constant.NaveoEntity.MaintDB);

            string sServerName = DBConn.Common.ConnName.GetDBName(sConnStr);

            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = "select 'CustomerVehicle' as TableName\r\n";
            sql += "select cv.\"Id\" from dbo.\"CustomerVehicle\" cv, dbo.\"FleetServer\" fs where cv.\"FMSAssetId\" = '" + AssetId + "' AND cv.\"FleetServerId\" = fs.\"Id\" AND fs.\"Name\" = '" + sServerName.ToUpper() + "'";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, NaveoOneLib.Constant.NaveoEntity.MaintDB);

            DataTable dtCustomerVehicle = ds.Tables["CustomerVehicle"];

            if (dtCustomerVehicle.Rows.Count > 0)
            {
                DataRow row = dtCustomerVehicle.Rows[0];
                AssetId = int.Parse(row["Id"].ToString());

                pContinue = true;
            }

            return pContinue;
        }

        public BaseModel SaveMaintenance(NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto maintAsset, String sConnStr)
        {
            BaseModel baseModel = new BaseModel();
            Boolean bResult = false;

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            DataSet dsProcess = new DataSet();

            DataTable dtMaintenanceVehcile = MaintenanceVehicleDT();
            DataTable dtVehcileMaintItems = VehicleMaintItemsDT();

            DataTable dtAssetMaintenanceType = AssetMaintenanceTypeDT();

            //Step One one OFF installation

            //Step Two normal insertion

            //Table FleetServers
            //sServerName      

            string sServerName = DBConn.Common.ConnName.GetDBName(sConnStr);


            string customerId = string.Empty;

            int AssetId = 0;

            int MaintType = 0;

            //Get Server Id

            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = "select 'CustomerVehicle' as TableName\r\n";
            sql += "select cv.\"Id\" from dbo.\"CustomerVehicle\" cv, dbo.\"FleetServer\" fs where cv.\"FMSAssetId\" = '" + maintAsset.AssetId + "' AND cv.\"FleetServerId\" = fs.\"Id\" AND fs.\"Name\" = '" + sServerName.ToUpper() + "'";
            sql += "select 'VehicleMaintenance' as TableName\r\n";
            sql += "select vm.* from dbo.\"CustomerVehicle\" cv, dbo.\"FleetServer\" fs, dbo.\"GFI_AMM_VehicleMaintenance\" vm, dbo.\"GFI_AMM_VehicleMaintTypes\" vmt where cv.\"FMSAssetId\" = '" + maintAsset.AssetId + "' AND vm.\"MaintTypeId_cbo\" = vmt.\"MaintTypeId\" AND cv.\"Id\" = vm.\"AssetId\" AND cv.\"FleetServerId\" = fs.\"id\" AND fs.\"Name\" = '" + sServerName.ToUpper() + "' AND vmt.\"Description\" =  '" + maintAsset.MaintenanceType + "' AND (vm.\"MaintStatusId_cbo\" = 2 OR vm.\"MaintStatusId_cbo\" = 3)";
            sql += "select 'MaintenanceType' as TableName\r\n";
            sql += "select vmt.\"MaintTypeId\" from dbo.\"GFI_AMM_VehicleMaintTypes\" vmt where vmt.\"Description\" =  '" + maintAsset.MaintenanceType + "' ";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, NaveoOneLib.Constant.NaveoEntity.MaintDB);

            DataTable dtCustomerVehicle = ds.Tables["CustomerVehicle"];

            if (dtCustomerVehicle.Rows.Count > 0)
            {
                DataRow row = dtCustomerVehicle.Rows[0];
                AssetId = int.Parse(row["Id"].ToString());
            }
            else
            {
                baseModel.data = false;
                return baseModel;
            }

            DataTable dtVehicleMaintenanceType = ds.Tables["MaintenanceType"];
            DataRow rowVMT = dtVehicleMaintenanceType.Rows[0];
            MaintType = int.Parse(rowVMT["MaintTypeId"].ToString());

            DataTable dtVehicleMaintenance = ds.Tables["VehicleMaintenance"];

            //If action = true SAVE COMPLETELY
            if (maintAsset.Action && maintAsset.URI != 0)
            {
                List<decimal> totalCost = new List<decimal>();

                foreach (NaveoOneLib.Models.Maintenance.lstPartDetails item in maintAsset.lstParts)
                {
                    DataRow drVehcileMaintItems = dtVehcileMaintItems.NewRow();
                    drVehcileMaintItems["MaintURI"] = maintAsset.URI;
                    drVehcileMaintItems["Description"] = item.ItemCode;
                    drVehcileMaintItems["Quantity"] = item.Quantity;
                    drVehcileMaintItems["UnitCost"] = item.UnitCost;
                    totalCost.Add((decimal)item.UnitCost);
                    drVehcileMaintItems["CreatedDate"] = DateTime.Now;
                    drVehcileMaintItems["oprType"] = DataRowState.Added;
                    dtVehcileMaintItems.Rows.Add(drVehcileMaintItems);
                }

                drCtrl["SeqNo"] = 1;
                drCtrl["TblName"] = dtVehcileMaintItems.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "URI";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdAction"] = "GET";
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtVehcileMaintItems);

                //INSERT VehicleMaintenance
                DataRow drVehicleMaintenance = dtMaintenanceVehcile.NewRow();
                drVehicleMaintenance["AssetId"] = AssetId;
                drVehicleMaintenance["Reminder"] = maintAsset.StartDate; //New
                drVehicleMaintenance["NextMaintenance"] = maintAsset.StartDate; //New
                drVehicleMaintenance["TotalCost"] = totalCost.Sum(); //New
                drVehicleMaintenance["URI"] = maintAsset.URI;
                drVehicleMaintenance["MaintTypeId_cbo"] = MaintType;
                drVehicleMaintenance["MaintStatusId_cbo"] = 5; //Completed
                drVehicleMaintenance["StartDate"] = maintAsset.StartDate;
                drVehicleMaintenance["EndDate"] = maintAsset.EndDate;
                drVehicleMaintenance["CreatedDate"] = DateTime.Now;
                drVehicleMaintenance["CreatedBy"] = maintAsset.CreatedBy;
                drVehicleMaintenance["Comment"] = maintAsset.Comment;
                drVehicleMaintenance["oprType"] = DataRowState.Modified;
                dtMaintenanceVehcile.Rows.Add(drVehicleMaintenance);

                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 2;
                drCtrl["TblName"] = dtMaintenanceVehcile.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "URI";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdValue"] = maintAsset.URI;
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtMaintenanceVehcile);

            }
            else if (dtVehicleMaintenance.Rows.Count == 0 && !maintAsset.Action) //One Off insertion
            {
                //INSERT One OFF VehicleMaintenance
                DataRow drVehicleMaintenance = dtMaintenanceVehcile.NewRow();
                drVehicleMaintenance["AssetId"] = AssetId;
                drVehicleMaintenance["MaintTypeId_cbo"] = MaintType;
                drVehicleMaintenance["MaintStatusId_cbo"] = 2;
                drVehicleMaintenance["CreatedDate"] = DateTime.Now;
                drVehicleMaintenance["oprType"] = DataRowState.Added;
                dtMaintenanceVehcile.Rows.Add(drVehicleMaintenance);

                drCtrl["SeqNo"] = 1;
                drCtrl["TblName"] = dtMaintenanceVehcile.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "URI";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdAction"] = "GET";
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtMaintenanceVehcile);


                //INSERT One OFF AssetMaintenanceType
                DataRow drAssetMaintenanceType = dtAssetMaintenanceType.NewRow();
                drAssetMaintenanceType["AssetId"] = AssetId;
                drAssetMaintenanceType["MaintenceId"] = MaintType;
                drAssetMaintenanceType["oprType"] = DataRowState.Added;
                dtAssetMaintenanceType.Rows.Add(drAssetMaintenanceType);

                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 2;
                drCtrl["TblName"] = dtAssetMaintenanceType.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "AssetMaintenceId";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdAction"] = "GET";
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtAssetMaintenanceType);
            }
            else
            {
                baseModel.data = false;
                return baseModel;
            }


            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, NaveoOneLib.Constant.NaveoEntity.MaintDB);
            dtCtrl = dsResult.Tables["CTRLTBL"];

            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");
                DataRow[] dRowId = null;

                if (dRow.Length > 0)
                {
                    baseModel.dbResult = dsResult;
                    baseModel.dbResult.Tables["CTRLTBL"].TableName = "ResultCtrlTbl";
                    baseModel.dbResult.Merge(dsProcess);
                    bResult = false;
                }
                else
                {
                    bResult = true;
                    dRowId = dtCtrl.Select();
                    baseModel.additionalData = int.Parse(dRowId[0].ItemArray[10].ToString());
                }

            }

            if (bResult & maintAsset.Action)
            {
                if (maintAsset.PicNames != null & maintAsset.PicNames.Count > 0)
                {
                    String[] images = maintAsset.PicNames[0].Split('`');
                    foreach (String pic in images)
                    {
                        String[] myPic = pic.Split(',');
                        if (myPic.Length == 2)
                        {
                            string picFormat = myPic[0];
                            if (picFormat == "data:image/jpeg;base64")
                            {
                                String imageNames = maintAsset.URI.ToString() + ".jpg";
                                string base64 = myPic[1];
                                byte[] bytes = Convert.FromBase64String(base64);
                                MemoryStream ms = new MemoryStream(bytes);
                                Image image = Image.FromStream(ms);
                                string filePath = ConfigurationManager.AppSettings["Path"].ToString();

                                string directoryPath = filePath + maintAsset.URI + "\\";

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(directoryPath);
                                }

                                image.Save(directoryPath + imageNames, ImageFormat.Jpeg);
                            }
                        }
                    }
                }
            }

            baseModel.data = bResult;
            return baseModel;
        }

        public BaseModel pushCPM(NaveoOneLib.Models.Maintenance.CPMmaintenance cpm, String sConnStr)
        {
            BaseModel baseModel = new BaseModel();
            DataTable dt = new DataTable();

            Boolean bResult = new Boolean();


            if (dt != null)
                bResult = true;

            baseModel.data = bResult;
            return baseModel;
        }

        public BaseModel GetRecurringMaint()
        {
            BaseModel baseModel = new BaseModel();

            Connection c = new Connection(NaveoOneLib.Constant.NaveoEntity.MaintDB);

            string sServerName = string.Empty;
            try
            {
                sServerName = getServerName();

            }
            catch (Exception ex)
            {

                sServerName = DBConn.Common.ConnName.GetDBName("DB1");
            }

            int AssetId = 0;

            //Get Server Id

            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = "select 'CustomerVehicle' as TableName;\r\n";
            sql += "with cte as (select vm.\"URI\", cv.\"Number\", vmt.\"Description\", row_number() over(partition by vm.\"AssetId\", vm.\"MaintTypeId_cbo\" order by vm.\"StartDate\" desc) as RowNum from dbo.\"GFI_AMM_VehicleMaintenance\" vm, dbo.\"GFI_AMM_VehicleMaintTypes\" vmt, dbo.\"CustomerVehicle\" cv, dbo.\"FleetServer\" fs where vmt.\"MaintTypeId\" = vm.\"MaintTypeId_cbo\" and vmt.\"OccurrenceType\" = 1 and cv.\"Id\" = vm.\"AssetId\" and fs.\"Id\" = cv.\"FleetServerId\" and fs.\"Name\" = '" + sServerName.ToUpper() + "') select * from cte where RowNum = 1 ";


            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, NaveoOneLib.Constant.NaveoEntity.MaintDB);

            DataTable dtCustomerVehicle = ds.Tables["CustomerVehicle"];

            if (dtCustomerVehicle.Rows.Count > 0)
            {
                DataRow row = dtCustomerVehicle.Rows[0];
                AssetId = int.Parse(row["URI"].ToString());

                baseModel.data = dtCustomerVehicle;
            }
            else
            {
                baseModel.data = false;
                return baseModel;
            }
            return baseModel;
        }

        public BaseModel GetMaintenanceStatus(string sConnStr)
        {
            BaseModel baseModel = new BaseModel();

            Connection c = new Connection(NaveoOneLib.Constant.NaveoEntity.MaintDB);


            String sServerName = DBConn.Common.ConnName.GetDBName(sConnStr);


            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            //Get Server Id

            String sql = "select 'CustomerVehicle' as TableName;\r\n";
            sql += "select vmh.URIHisto, vmh.\"NewURI\", vmh.\"URI\", cv.\"FMSAssetId\" AS FleetAssetId,cv.\"Number\", vmh.\"AssetId\" AS MaintAssetId, ty.\"Description\" AS TYPE, st.\"Description\" AS NewStatus, sta.\"Description\" AS OldStatus, vmh.\"CreatedDate\", vmh.\"UserId\" ,vmh.\"RuleID\" from dbo.\"GFI_AMM_VehicleMaintenanceHisto\" vmh, dbo.\"CustomerVehicle\" cv, dbo.\"FleetServer\" fs, dbo.\"GFI_AMM_VehicleMaintStatus\" st , dbo.\"GFI_AMM_VehicleMaintStatus\" sta, dbo.\"GFI_AMM_VehicleMaintTypes\" ty where ty.\"MaintTypeId\" = vmh.\"MaintTypeId_cbo\" AND st.\"MaintStatusId\" = vmh.\"MaintStatusId_cboNew\" AND sta.\"MaintStatusId\" = vmh.\"MaintStatusId_cboOld\" AND cv.\"Id\" = vmh.\"AssetId\" and fs.\"Id\" = cv.\"FleetServerId\" and fs.\"Name\" = '" + sServerName.ToUpper() + "' AND vmh.\"flagTreated\" = 10";
            sql += "select 'CustomerVehicleOld' as TableName;\r\n";
            sql += "select vmh.URIHisto, vmh.\"NewURI\", vmh.\"URI\", cv.\"FMSAssetId\" AS FleetAssetId,cv.\"Number\", vmh.\"AssetId\" AS MaintAssetId, ty.\"Description\" AS TYPE, st.\"Description\" AS NewStatus, '' AS OldStatus, vmh.\"CreatedDate\", vmh.\"UserId\" ,vmh.\"RuleID\" from dbo.\"GFI_AMM_VehicleMaintenanceHisto\" vmh, dbo.\"CustomerVehicle\" cv, dbo.\"FleetServer\" fs, dbo.\"GFI_AMM_VehicleMaintStatus\" st , dbo.\"GFI_AMM_VehicleMaintTypes\" ty where ty.\"MaintTypeId\" = vmh.\"MaintTypeId_cbo\" AND st.\"MaintStatusId\" = vmh.\"MaintStatusId_cboNew\" AND cv.\"Id\" = vmh.\"AssetId\" and fs.\"Id\" = cv.\"FleetServerId\" and fs.\"Name\" = '" + sServerName.ToUpper() + "' AND vmh.\"flagTreated\" = 10";


            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, NaveoOneLib.Constant.NaveoEntity.MaintDB);

            DataTable dtCustomerVehicle = ds.Tables["CustomerVehicle"];
            DataTable dtCustomerVehicleOld = ds.Tables["CustomerVehicleOld"];

            if (dtCustomerVehicle.Rows.Count > 0)
            { 
               //
            }
            else if (dtCustomerVehicleOld.Rows.Count > 0)
            {
                dtCustomerVehicle.Merge(dtCustomerVehicleOld);
            }
           

            baseModel.data = dtCustomerVehicle;

            return baseModel;
        }

        public BaseModel FuelUsage(string sConnStr, List<int> AssetIds, DateTime startDate, DateTime endDate)
        {
            BaseModel baseModel = new BaseModel();
            Connection c = new Connection(NaveoOneLib.Constant.NaveoEntity.MaintDB);
            String sServerName = DBConn.Common.ConnName.GetDBName(sConnStr);

            String sAssetsList = String.Empty;
            foreach (int l in AssetIds)
                sAssetsList += l.ToString() + ",";

            if (sAssetsList != String.Empty)
                sAssetsList = sAssetsList.Substring(0, sAssetsList.Length - 1);

            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            //Get Server Id
            String sql = "select 'FuelCost' as TableName;\r\n";
            sql += "select SUM(f.\"AmountInclVAT\") as TotalCost, cv.\"FMSAssetId\" as AssetId from dbo.\"CustomerVehicle\" cv, dbo.\"FleetServer\" fs, dbo.\"Fuel\" f WHERE cv.\"Number\" = f.\"RegistrationNo\" and fs.\"Id\" = cv.\"FleetServerId\" and fs.\"Name\" = '" + sServerName.ToUpper() + "' and cv.\"FMSAssetId\" IN (" + sAssetsList + ") and f.\"RefuelDate\" >= '" + startDate.ToString("yyyy/MM/dd HH:mm:ss") + "' and f.\"RefuelDate\" <= '" + endDate.ToString("yyyy/MM/dd HH:mm:ss") + "' group by cv.\"FMSAssetId\"";
            sql += "select 'FuelQuantity' as TableName;\r\n";
            sql += "select SUM (f.\"QuantityLitres\") as Quantity, cv.\"FMSAssetId\" as AssetId from dbo.\"CustomerVehicle\" cv, dbo.\"FleetServer\" fs, dbo.\"Fuel\" f WHERE cv.\"Number\" = f.\"RegistrationNo\" and fs.\"Id\" = cv.\"FleetServerId\" and fs.\"Name\" = '" + sServerName.ToUpper() + "' and cv.\"FMSAssetId\" IN (" + sAssetsList + ") and f.\"RefuelDate\" >= '" + startDate.ToString("yyyy/MM/dd HH:mm:ss") + "' and f.\"RefuelDate\" <= '" + endDate.ToString("yyyy/MM/dd HH:mm:ss") + "' group by cv.\"FMSAssetId\"";
            sql += "select 'TotalMaintennaceCost' as TableName;\r\n";
            sql += "select SUM (vm.\"TotalCost\") as TotalMaintCost, cv.\"FMSAssetId\" as AssetId from dbo.\"CustomerVehicle\" cv, dbo.\"FleetServer\" fs, dbo.\"GFI_AMM_VehicleMaintenance\" vm WHERE cv.\"Id\" = vm.\"AssetId\" and fs.\"Id\" = cv.\"FleetServerId\" and fs.\"Name\" = '" + sServerName.ToUpper() + "' and cv.\"FMSAssetId\" IN (" + sAssetsList + ") and vm.\"StartDate\" >= '" + startDate.ToString("yyyy/MM/dd HH:mm:ss") + "' and vm.\"EndDate\" <= '" + endDate.ToString("yyyy/MM/dd HH:mm:ss") + "' group by cv.\"FMSAssetId\"";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);
            DataSet ds = c.GetDataDS(dtSql, NaveoOneLib.Constant.NaveoEntity.MaintDB);

            DataTable dtCustomerVehicle = ds.Tables["FuelCost"];
            if (dtCustomerVehicle.Rows.Count > 0)
            {
                baseModel.dbResult = ds;
            }
            else
            {
                baseModel.dbResult = ds;
                return baseModel;
            }

            return baseModel;
        }

        /// <summary>
        /// Function 4 update status on maintenance
        /// </summary>
        /// <param name="URIs"></param>
        /// <returns></returns>
        public BaseModel UpdateflagOnMaintenance(List<int> URIHistos)
        {
            BaseModel baseModel = new BaseModel();

            Connection c = new Connection(NaveoOneLib.Constant.NaveoEntity.MaintDB);

            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            DataSet dsProcess = new DataSet();
            DataTable VehicleHisto = MaintenanceVehicleHistoDT();


            int URIFinal = 0;

            //Update
            foreach (int urihisto in URIHistos)
            {
                DataRow drVehicleHisto = VehicleHisto.NewRow();

                drVehicleHisto["\"flagTreated\""] = 11;
                drVehicleHisto["\"URIHisto\""] = urihisto;
                URIFinal = urihisto;
                drVehicleHisto["oprType"] = DataRowState.Modified;
                VehicleHisto.Rows.Add(drVehicleHisto);
            }

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = VehicleHisto.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "\"URIHisto\"";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdFieldName"] = URIFinal;

            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(VehicleHisto);

            dsProcess.Merge(dtCtrl);


            DataSet dsResult = c.ProcessData(dsProcess, NaveoOneLib.Constant.NaveoEntity.MaintDB);

            dtCtrl = dsResult.Tables["CTRLTBL"];

            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                {
                    baseModel.dbResult = dsResult;
                    baseModel.dbResult.Tables["CTRLTBL"].TableName = "ResultCtrlTbl";
                    baseModel.dbResult.Merge(dsProcess);
                    //bResult = false;
                }
                else
                {
                    baseModel.data = true;
                }

            }

            return baseModel;
        }

        public BaseModel UpdateRuleOnHistoTable(DataTable GetMaintennaceRule, string sConnStr)
        {
            BaseModel baseModel = new BaseModel();

            Connection c = new Connection(NaveoOneLib.Constant.NaveoEntity.MaintDB);

            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            DataSet dsProcess = new DataSet();
            DataTable VehicleHistoInsert = MaintenanceVehicleHistoDT();
            DataTable VehicleHistoUpdate = MaintenanceVehicleHistoDT();

            String sServerName = DBConn.Common.ConnName.GetDBName(sConnStr);




            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            //Get Server Id
            //Get All with ZERO
            String sql = "select 'CustomerVehicle' as TableName;\r\n";
            sql += "select vmh.URIHisto, vmh.\"NewURI\", vmh.\"URI\", cv.\"FMSAssetId\" AS FleetAssetId, vmh.\"AssetId\" AS MaintAssetId, ty.\"Description\" AS TYPE, st.\"Description\" AS NewStatus, sta.\"Description\" AS OldStatus, vmh.\"CreatedDate\", vmh.\"UserId\", vmh.\"RuleId\" from dbo.\"GFI_AMM_VehicleMaintenanceHisto\" vmh, dbo.\"CustomerVehicle\" cv, dbo.\"FleetServer\" fs, dbo.\"GFI_AMM_VehicleMaintStatus\" st , dbo.\"GFI_AMM_VehicleMaintStatus\" sta, dbo.\"GFI_AMM_VehicleMaintTypes\" ty where ty.\"MaintTypeId\" = vmh.\"MaintTypeId_cbo\" AND st.\"MaintStatusId\" = vmh.\"MaintStatusId_cboNew\" AND sta.\"MaintStatusId\" = vmh.\"MaintStatusId_cboOld\" AND cv.\"Id\" = vmh.\"AssetId\" and fs.\"Id\" = cv.\"FleetServerId\" and fs.\"Name\" = '" + sServerName.ToUpper() + "' AND vmh.\"flagTreated\" = 0";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, NaveoOneLib.Constant.NaveoEntity.MaintDB);

            DataTable dtCustomerVehicle = ds.Tables["CustomerVehicle"];

            int URIFinal = 0;

            //Update
            if (dtCustomerVehicle.Rows.Count > 0)
            {
                foreach (DataRow row in dtCustomerVehicle.Rows)
                {
                    DataRow[] drChkURITrue = GetMaintennaceRule.Select("URI = " + row["URI"].ToString());

                    DataRow[] drChkURIFalse = GetMaintennaceRule.Select("URI <> " + row["URI"].ToString() + " and MaintType = '" + row["TYPE"].ToString() + "'");

                    //Update
                    if (drChkURITrue.Length > 0)
                    {
                        DataRow rowTrue = drChkURITrue[0];

                        DataRow drVehicleHistoUpdate = VehicleHistoUpdate.NewRow();
                        URIFinal = int.Parse(row["URIHisto"].ToString());
                        drVehicleHistoUpdate["\"URIHisto\""] = row["URIHisto"].ToString();
                        drVehicleHistoUpdate["\"flagTreated\""] = 10;
                        //drVehicleHisto["\"URI\""] = row["URI"].ToString();
                        drVehicleHistoUpdate["\"NewURI\""] = 0;
                        //Add user
                        drVehicleHistoUpdate["\"UserId\""] = rowTrue["User"].ToString();
                        //Add Rule
                        drVehicleHistoUpdate["\"RuleId\""] = int.Parse(rowTrue["RuleId"].ToString());

                        drVehicleHistoUpdate["oprType"] = DataRowState.Modified;
                        VehicleHistoUpdate.Rows.Add(drVehicleHistoUpdate);
                    }

                    //Insert
                    if (drChkURIFalse.Length > 0)
                    {

                        DataRow rowFalse = drChkURIFalse[0];
                        //Insert New
                        String sqlMaint = "select 'CustomerVehicleProcess' as TableName;\r\n";
                        sqlMaint += "select vm.\"URI\", cv.\"FMSAssetId\" AS FleetAssetId, vm.\"AssetId\" AS MaintAssetId, vm.\"MaintTypeId_cbo\" AS TYPE, vm.\"MaintStatusId_cbo\" AS NewStatus, vm.\"CreatedDate\" from dbo.\"GFI_AMM_VehicleMaintenance\" vm, dbo.\"CustomerVehicle\" cv, dbo.\"FleetServer\" fs, dbo.\"GFI_AMM_VehicleMaintTypes\" ty where ty.\"MaintTypeId\" = vm.\"MaintTypeId_cbo\" AND cv.\"Id\" = vm.\"AssetId\" and fs.\"Id\" = cv.\"FleetServerId\" and fs.\"Name\" = '" + sServerName.ToUpper() + "' and ty.\"Description\" IN ('" + rowFalse["MaintType"].ToString() + "') and cv.\"Number\" IN ('" + rowFalse["Asset"].ToString() + "') and vm.\"URI\" <> '" + rowFalse["URI"].ToString() + "'";
                        DataTable dtSqlMaint = c.dtSql();
                        DataRow drSqlMaint = dtSqlMaint.NewRow();
                        drSqlMaint = dtSqlMaint.NewRow();
                        drSqlMaint["sSQL"] = sqlMaint;
                        drSqlMaint["sTableName"] = "MultipleDTfromQuery";
                        dtSqlMaint.Rows.Add(drSqlMaint);

                        DataSet dsMaint = c.GetDataDS(dtSqlMaint, NaveoOneLib.Constant.NaveoEntity.MaintDB);

                        DataTable dtMaint = dsMaint.Tables["CustomerVehicleProcess"];

                        DataRow rowMaint = dtMaint.Rows[0];
                        
                        if (rowMaint.Table.Rows.Count > 0)
                        {

                            DataRow drVehicleHistoInsert = VehicleHistoInsert.NewRow();

                            drVehicleHistoInsert["\"flagTreated\""] = 10;
                            drVehicleHistoInsert["\"URI\""] = rowFalse["URI"].ToString(); //from object
                            drVehicleHistoInsert["\"NewURI\""] = rowMaint["URI"];
                            drVehicleHistoInsert["\"MaintStatusId_cboNew\""] = rowMaint["NewStatus"];
                            drVehicleHistoInsert["\"MaintTypeId_cbo\""] = rowMaint["TYPE"];
                            drVehicleHistoInsert["\"UserId\""] = rowFalse["User"].ToString();
                            //Add Rule
                            drVehicleHistoInsert["\"RuleId\""] = int.Parse(rowFalse["RuleId"].ToString());
                            drVehicleHistoInsert["\"AssetId\""] = rowMaint["MaintAssetId"];
                            drVehicleHistoInsert["\"CreatedDate\""] = DateTime.Now;

                            drVehicleHistoInsert["oprType"] = DataRowState.Added;
                            VehicleHistoInsert.Rows.Add(drVehicleHistoInsert);
                        }
                        // generate the data you want to insert

                    }



                }


                

                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 1;
                drCtrl["TblName"] = VehicleHistoInsert.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "\"URIHisto\"";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdAction"] = "SET";
                drCtrl["NextIdFieldName"] = "\"URIHisto\"";
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(VehicleHistoInsert);

                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 2;
                drCtrl["TblName"] = VehicleHistoUpdate.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "\"URIHisto\"";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdFieldName"] = URIFinal;
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(VehicleHistoUpdate);

                dsProcess.Merge(dtCtrl);

                DataSet dsResult = c.ProcessData(dsProcess, NaveoOneLib.Constant.NaveoEntity.MaintDB);

                dtCtrl = dsResult.Tables["CTRLTBL"];

                if (dtCtrl != null)
                {
                    DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                    if (dRow.Length > 0)
                    {
                        baseModel.dbResult = dsResult;
                        baseModel.dbResult.Tables["CTRLTBL"].TableName = "ResultCtrlTbl";
                        baseModel.dbResult.Merge(dsProcess);
                        //bResult = false;
                    }
                    else
                    {
                        baseModel.data = true;
                    }

                }

            }

            return baseModel;
        }

        public BaseModel GetFuelData(List<int> lAssetIds ,DateTime dtFrom , DateTime dtTo, string sConnStr)
        {
            BaseModel baseModel = new BaseModel();

            Connection c = new Connection(NaveoOneLib.Constant.NaveoEntity.MaintDB);

            string sServerName = string.Empty;
          
             sServerName = getServerName();

            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            string AssetID = string.Empty;
            foreach (var i in lAssetIds)
                AssetID += i.ToString() + ",";
            AssetID = AssetID.TrimEnd(',');

     

               String dtF = dtFrom.ToString("dd/MMM/yyyy HH:mm:ss tt");
               String dtT = dtTo.ToString("dd/MMM/yyyy HH:mm:ss tt");


               string sql = "select cust.\"FMSAssetId\" AssetId,f.\"Id\",f.\"RefuelDate\", f.\"AmountInclVAT\" amount ,f.\"FillingStation\",f.\"Resource\" Driver ,f.\"QuantityLitres \" Litres  ,f.\"Mileage\" from Fuel f inner join CustomerVehicle cust on f.RegistrationNo = cust.Number inner join FleetServer server on server.Id = cust.FleetServerId where cust.FMSAssetId  in (" + AssetID + ") and f.RefuelDate >= '" + dtF + "' and f.RefuelDate <= '" + dtT + "' and server.Name = '" + sServerName + "' order by RefuelDate desc;";

  
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "fueldata";
                dtSql.Rows.Add(drSql);

            DataTable dt = c.GetDataDT(sql, NaveoOneLib.Constant.NaveoEntity.MaintDB);

            baseModel.data = dt;
            return baseModel;
        }

        public BaseModel GetMaintenanceType()
        {
            BaseModel baseModel = new BaseModel();

            Connection c = new Connection(NaveoOneLib.Constant.NaveoEntity.MaintDB);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            string sql = "select Distinct MaintTypeId Id , Description from GFI_AMM_VehicleMaintTypes order by MaintTypeId asc;";


            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMaintType";
            dtSql.Rows.Add(drSql);

            DataTable dt = c.GetDataDT(sql, NaveoOneLib.Constant.NaveoEntity.MaintDB);

            baseModel.data = dt;
            baseModel.total = dt.Rows.Count;
            return baseModel;
        }
        public class MaintAsset
        {
            public int AssetID { get; set; }
            public String AssetCode { get; set; }
            public int AssetType { get; set; }
            public DateTime dtCreated { get; set; }
            public String Type { get; set; }
            public String AssetName { get; set; }
            public String Descript { get; set; }

            public String AssetNumber { get; set; }

            public int? MakeID { get; set; }

            public int? ModelID { get; set; }

            public int? FuelType { get; set; }

            public String VehicleType { get; set; }

            public String sColor { get; set; }

            public int? YearManufactured { get; set; }

            public DateTime? PurchasedDate { get; set; }

            public Double? PurchasedValue { get; set; }

            public String ExternalRef { get; set; }

            public String EngineSerialNumber { get; set; }

            public String RefD { get; set; }

            public int? AssetCategory { get; set; }
            public String AssetFamily { get; set; }
            public String Owners { get; set; }



            public DataRowState oprType { get; set; }

        }

        public class AssetMaintenanceDetailDto
        {
            public string MaintenanceType { get; set; }

            public Nullable<System.DateTime> CreatedDate { get; set; }

            public Nullable<System.DateTime> StartDate { get; set; }

            public Nullable<System.DateTime> EndDate { get; set; }
            public string CreatedBy { get; set; }

            public string Comment { get; set; }
            public Nullable<int> AssetId { get; set; }

            public List<lstPartDetails> lstParts { get; set; }

        }

        public class Attachment
        {
            public int Id { get; set; }
            public string Path { get; set; }
            public string FileName { get; set; }
            public bool Deleted { get; set; }
            public bool Default { get; set; }
        }

        public class lstPartDetails
        {
            public string ItemCode { get; set; }
            public Nullable<decimal> Quantity { get; set; }
            public Nullable<decimal> UnitCost { get; set; }
            public Nullable<System.DateTime> CreatedDate { get; set; }
            public string CreatedBy { get; set; }
            public Nullable<System.DateTime> UpdatedDate { get; set; }
            public string UpdatedBy { get; set; }
        }
    }
}
