using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Assets;

namespace NaveoOneLib.Services.Assets
{
    public class VehicleMaintenanceService
    {

        Errors er = new Errors();

        public DataSet GetDetails(int iAssetId, int iURI, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = null;
            String sql = string.Empty;

            sql = sGetVehicleMaintenanceByAssetId(iAssetId.ToString());
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtVM";
            dtSql.Rows.Add(drSql);

            sql = sGetVehicleMaintenanceById(iURI.ToString());
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtVMaint";
            dtSql.Rows.Add(drSql);

            sql = new VehicleMaintTypeLinkService().sGetVehicleMaintTypeLinkForAssetId(iAssetId);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtVMTL";
            dtSql.Rows.Add(drSql);

            sql = new VehicleMaintItemsService().sGetVehicleMaintItemsById(iURI.ToString());
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtVMI";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            foreach (DataRow dr in ds.Tables["dtVMI"].Rows)
                dr.AcceptChanges();

            return ds;
        }

        public string sGetVehicleMaintenanceByAssetId(String iAssetId, DateTime? dtFrom = null, DateTime? dtTo = null)
        {
            try
            {
                String sql = @" SELECT vm.URI,                                    
                            vm.AssetId, 
                             a.AssetNumber,
                            vmt.MaintCatId_cbo,
                            vmc.Description AS MaintCatDesc,
                            vm.MaintTypeId_cbo, 
                            vmt.Description AS MaintTypeDesc,
                            vm.ContactDetails,
                            vm.CompanyName, 
                            vm.PhoneNumber, 
                            vm.CompanyRef, 
                            vm.CompanyRef2, 
                            vm.MaintDescription, 
                            vm.StartDate, 
                            vm.EndDate, 
                            vm.AssetStatus,
                            vm.Comment,
                            vm.MaintStatusId_cbo, 
                            vms.Description AS MaintStatusDesc,
                            vm.EstimatedValue,
                            vm.VATInclInItemsAmt,
                            vm.VATAmount,
                            vm.TotalCost,
                            TotalCost - ISNULL(vm.VATAmount, 0) AS AmtExVAT,
                            vm.CoverTypeId_cbo,
                            ict.Description AS CoverTypeDesc,
                            --CalculatedOdometer,
                            dbo.GetCalcOdo(vm.AssetId) AS CalculatedOdometer,
                            vm.ActualOdometer,
                            --CalculatedEngineHrs,
                            dbo.GetCalcEngHrs(vm.AssetId) AS CalculatedEngineHrs,
                            vm.ActualEngineHrs,
                            vm.AdditionalInfo, 
                            vm.CreatedDate, 
                            vm.CreatedBy, 
                            vm.UpdatedDate, 
                            vm.UpdatedBy 
                        FROM GFI_AMM_VehicleMaintenance vm
                        INNER JOIN GFI_FLT_Asset a
                            ON vm.AssetId = a.AssetId 
			            INNER JOIN GFI_AMM_VehicleMaintTypes vmt
				            ON vm.MaintTypeId_cbo = vmt.MaintTypeId 
			            LEFT OUTER JOIN GFI_AMM_VehicleMaintStatus vms
				            ON vm.MaintStatusId_cbo = vms.MaintStatusId
                        LEFT OUTER JOIN GFI_AMM_InsCoverTypes ict
                            ON vm.CoverTypeId_cbo = ict.CoverTypeId
                        INNER JOIN GFI_AMM_VehicleMaintCat vmc
                            ON vmt.MaintCatId_cbo = vmc.MaintCatId
                        WHERE vm.AssetId = " + iAssetId + " ";

                if (dtFrom != null)
                {
                    DateTime df = (DateTime)dtFrom;
                    DateTime dt = (DateTime)dtTo;

                    sql += "and vm.StartDate between '" + df.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "' and '" + dt.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "'";
                }

                sql += " ORDER BY vm.StartDate DESC";
                return sql;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetVehicleMaintenanceByAssetId(String iAssetId, String sConnStr, DateTime? dtFrom = null, DateTime? dtTo = null)
        {
            try
            {
                return new Connection(sConnStr).GetDataTableBy(sGetVehicleMaintenanceByAssetId(iAssetId, dtFrom, dtTo), iAssetId, sConnStr);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string sGetVehicleMaintenanceById(String iURI)
        {
            try
            {
                String sqlString = @" SELECT vm.URI,
                                    vmtl.URI AS VMTLURI,  
                                    vm.AssetId, 
                                     a.AssetNumber,
                                    vmt.MaintCatId_cbo,
                                    vmc.Description AS MaintCatDesc,
                                    vm.MaintTypeId_cbo, 
                                    vmt.Description AS MaintTypeDesc,
                                    vm.ContactDetails,
                                    vm.CompanyName, 
                                    vm.PhoneNumber, 
                                    vm.CompanyRef, 
                                    vm.CompanyRef2, 
                                    vm.MaintDescription, 
                                    vm.StartDate, 
                                    vm.EndDate, 
                                    vm.AssetStatus,
                                    vm.Comment,
                                    vm.MaintStatusId_cbo, 
                                    vms.Description AS MaintStatusDesc,
                                    vm.EstimatedValue,
                                    vm.VATInclInItemsAmt,
                                    vm.VATAmount, 
                                    vm.TotalCost, 
                                    vm.CoverTypeId_cbo,
                                    ict.Description AS CoverTypeDesc,
                                    dbo.GetCalcOdo(vm.AssetId) AS CalculatedOdometer,
                                    ActualOdometer,
                                    dbo.GetCalcEngHrs(vm.AssetId) AS CalculatedEngineHrs,
                                    ActualEngineHrs,
                                    vm.AdditionalInfo, 
                                    vm.CreatedDate, 
                                    vm.CreatedBy, 
                                    vm.UpdatedDate, 
                                    vm.UpdatedBy 
                               FROM GFI_AMM_VehicleMaintenance vm
                                INNER JOIN GFI_FLT_Asset a
                                    ON vm.AssetId = a.AssetId 
			                    INNER JOIN dbo.GFI_AMM_VehicleMaintTypes vmt
				                    ON vm.MaintTypeId_cbo = vmt.MaintTypeId 
			                    LEFT OUTER JOIN GFI_AMM_VehicleMaintStatus vms
				                    ON vm.MaintStatusId_cbo = vms.MaintStatusId
                                LEFT OUTER JOIN GFI_AMM_InsCoverTypes ict
                                    ON vm.CoverTypeId_cbo = ict.CoverTypeId
                                INNER JOIN GFI_AMM_VehicleMaintCat vmc
                                    ON vmt.MaintCatId_cbo = vmc.MaintCatId
                                LEFT OUTER JOIN GFI_AMM_VehicleMaintTypesLink vmtl
                                    ON vm.URI = vmtl.MaintURI
                               WHERE vm.URI = " + iURI + @"
                               ORDER BY vm.URI";

                return sqlString;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public VehicleMaintenance GetVehicleMaintenanceById(String iURI, String sConnStr)
        {
            try
            {
                DataTable dt = new Connection(sConnStr).GetDataTableBy(sGetVehicleMaintenanceById(iURI), iURI, sConnStr);

                if (dt.Rows.Count == 0)
                    return null;
                else
                {
                    VehicleMaintenance retVehicleMaintenance = new VehicleMaintenance();
                    retVehicleMaintenance.URI = Convert.ToInt32(dt.Rows[0]["URI"]);
                    retVehicleMaintenance.VMTLURI = dt.Rows[0]["VMTLURI"].ToString() == String.Empty ? 0 : Convert.ToInt32(dt.Rows[0]["VMTLURI"].ToString());
                    retVehicleMaintenance.AssetId = Convert.ToInt32(dt.Rows[0]["AssetId"]);
                    retVehicleMaintenance.AssetNumber = dt.Rows[0]["AssetNumber"].ToString();
                    retVehicleMaintenance.MaintCatId_cbo = Convert.ToInt32(dt.Rows[0]["MaintCatId_cbo"]);
                    retVehicleMaintenance.MaintCatDesc = dt.Rows[0]["MaintCatDesc"].ToString();
                    retVehicleMaintenance.MaintTypeId_cbo = Convert.ToInt32(dt.Rows[0]["MaintTypeId_cbo"]);
                    retVehicleMaintenance.MaintTypeDesc = dt.Rows[0]["MaintTypeDesc"].ToString();
                    retVehicleMaintenance.ContactDetails = dt.Rows[0]["ContactDetails"].ToString();
                    retVehicleMaintenance.CompanyName = dt.Rows[0]["CompanyName"].ToString();
                    retVehicleMaintenance.PhoneNumber = dt.Rows[0]["PhoneNumber"].ToString();
                    retVehicleMaintenance.CompanyRef = dt.Rows[0]["CompanyRef"].ToString();
                    retVehicleMaintenance.CompanyRef2 = dt.Rows[0]["CompanyRef2"].ToString();
                    retVehicleMaintenance.MaintDescription = dt.Rows[0]["MaintDescription"].ToString();

                    if (!String.IsNullOrEmpty(dt.Rows[0]["StartDate"].ToString()))
                        retVehicleMaintenance.StartDate = (DateTime)dt.Rows[0]["StartDate"];

                    if (!String.IsNullOrEmpty(dt.Rows[0]["EndDate"].ToString()))
                        retVehicleMaintenance.EndDate = (DateTime)dt.Rows[0]["EndDate"];

                    retVehicleMaintenance.MaintStatusId_cbo = Convert.ToInt32(dt.Rows[0]["MaintStatusId_cbo"]);

                    retVehicleMaintenance.MaintStatusDesc = dt.Rows[0]["MaintStatusDesc"].ToString();

                    if (!String.IsNullOrEmpty(dt.Rows[0]["EstimatedValue"].ToString()))
                        retVehicleMaintenance.EstimatedValue = (Decimal)dt.Rows[0]["EstimatedValue"];
                    else
                        retVehicleMaintenance.EstimatedValue = 0;

                    retVehicleMaintenance.VATInclInItemsAmt = dt.Rows[0]["VATInclInItemsAmt"].ToString();

                    if (!String.IsNullOrEmpty(dt.Rows[0]["VATAmount"].ToString()))
                        retVehicleMaintenance.VATAmount = (Decimal)dt.Rows[0]["VATAmount"];
                    else
                        retVehicleMaintenance.VATAmount = 0;

                    if (!String.IsNullOrEmpty(dt.Rows[0]["TotalCost"].ToString()))
                        retVehicleMaintenance.TotalCost = (Decimal)dt.Rows[0]["TotalCost"];
                    else
                        retVehicleMaintenance.TotalCost = 0;

                    if (!String.IsNullOrEmpty(dt.Rows[0]["CoverTypeId_cbo"].ToString()))
                        retVehicleMaintenance.CoverTypeId_cbo = Convert.ToInt32(dt.Rows[0]["CoverTypeId_cbo"]);

                    if (!String.IsNullOrEmpty(dt.Rows[0]["CoverTypeDesc"].ToString()))
                        retVehicleMaintenance.CoverTypeDesc = dt.Rows[0]["CoverTypeDesc"].ToString();

                    if (!String.IsNullOrEmpty(dt.Rows[0]["CalculatedOdometer"].ToString()))
                        retVehicleMaintenance.CalculatedOdometer = System.Convert.ToInt32(dt.Rows[0]["CalculatedOdometer"]);
                    else
                        retVehicleMaintenance.CalculatedOdometer = 0;

                    if (!String.IsNullOrEmpty(dt.Rows[0]["ActualOdometer"].ToString()))
                        retVehicleMaintenance.ActualOdometer =  System.Convert.ToInt32(dt.Rows[0]["ActualOdometer"]);
                    else
                        retVehicleMaintenance.ActualOdometer = 0;

                    if (!String.IsNullOrEmpty(dt.Rows[0]["CalculatedEngineHrs"].ToString()))
                        retVehicleMaintenance.CalculatedEngineHrs = System.Convert.ToInt32(dt.Rows[0]["CalculatedEngineHrs"]);
                    else
                        retVehicleMaintenance.CalculatedEngineHrs = 0;

                    if (!String.IsNullOrEmpty(dt.Rows[0]["ActualEngineHrs"].ToString()))
                        retVehicleMaintenance.ActualEngineHrs = System.Convert.ToInt32(dt.Rows[0]["ActualEngineHrs"]);
                    else
                        retVehicleMaintenance.ActualEngineHrs = 0;

                    retVehicleMaintenance.AdditionalInfo = dt.Rows[0]["AdditionalInfo"].ToString();

                    retVehicleMaintenance.CreatedDate = (DateTime)dt.Rows[0]["CreatedDate"];
                    retVehicleMaintenance.CreatedBy = dt.Rows[0]["CreatedBy"].ToString();
                    retVehicleMaintenance.UpdatedDate = (DateTime)dt.Rows[0]["UpdatedDate"];
                    retVehicleMaintenance.UpdatedBy = dt.Rows[0]["UpdatedBy"].ToString();
                    retVehicleMaintenance.AssetStatus = dt.Rows[0]["AssetStatus"].ToString();
                    retVehicleMaintenance.Comment = dt.Rows[0]["Comment"].ToString();
                    return retVehicleMaintenance;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public VehicleMaintenance VMaint(DataRow dr)
        {
            try
            {
                VehicleMaintenance retVehicleMaintenance = new VehicleMaintenance();
                retVehicleMaintenance.URI = Convert.ToInt32(dr["URI"]);
                retVehicleMaintenance.VMTLURI = dr["VMTLURI"].ToString() == String.Empty ? 0 : Convert.ToInt32(dr["VMTLURI"].ToString());
                retVehicleMaintenance.AssetId = Convert.ToInt32(dr["AssetId"]);
                retVehicleMaintenance.AssetNumber = dr["AssetNumber"].ToString();
                retVehicleMaintenance.MaintCatId_cbo = Convert.ToInt32(dr["MaintCatId_cbo"]);
                retVehicleMaintenance.MaintCatDesc = dr["MaintCatDesc"].ToString();
                retVehicleMaintenance.MaintTypeId_cbo = Convert.ToInt32(dr["MaintTypeId_cbo"]);
                retVehicleMaintenance.MaintTypeDesc = dr["MaintTypeDesc"].ToString();
                retVehicleMaintenance.ContactDetails = dr["ContactDetails"].ToString();
                retVehicleMaintenance.CompanyName = dr["CompanyName"].ToString();
                retVehicleMaintenance.PhoneNumber = dr["PhoneNumber"].ToString();
                retVehicleMaintenance.CompanyRef = dr["CompanyRef"].ToString();
                retVehicleMaintenance.CompanyRef2 = dr["CompanyRef2"].ToString();
                retVehicleMaintenance.MaintDescription = dr["MaintDescription"].ToString();

                if (!String.IsNullOrEmpty(dr["StartDate"].ToString()))
                    retVehicleMaintenance.StartDate = (DateTime)dr["StartDate"];

                if (!String.IsNullOrEmpty(dr["EndDate"].ToString()))
                    retVehicleMaintenance.EndDate = (DateTime)dr["EndDate"];

                retVehicleMaintenance.MaintStatusId_cbo = String.IsNullOrEmpty(dr["MaintStatusId_cbo"].ToString()) ? 0 : Convert.ToInt32(dr["MaintStatusId_cbo"]);

                retVehicleMaintenance.MaintStatusDesc = dr["MaintStatusDesc"].ToString();

                if (!String.IsNullOrEmpty(dr["EstimatedValue"].ToString()))
                    retVehicleMaintenance.EstimatedValue = (Decimal)dr["EstimatedValue"];
                else
                    retVehicleMaintenance.EstimatedValue = 0;

                if (!String.IsNullOrEmpty(dr["VATAmount"].ToString()))
                    retVehicleMaintenance.VATAmount = (Decimal)dr["VATAmount"];
                else
                    retVehicleMaintenance.VATAmount = 0;

                retVehicleMaintenance.VATInclInItemsAmt = dr["VATInclInItemsAmt"].ToString();

                if (!String.IsNullOrEmpty(dr["TotalCost"].ToString()))
                    retVehicleMaintenance.TotalCost = (Decimal)dr["TotalCost"];
                else
                    retVehicleMaintenance.TotalCost = 0;

                if (!String.IsNullOrEmpty(dr["CoverTypeId_cbo"].ToString()))
                    retVehicleMaintenance.CoverTypeId_cbo = Convert.ToInt32(dr["CoverTypeId_cbo"]);

                if (!String.IsNullOrEmpty(dr["CoverTypeDesc"].ToString()))
                    retVehicleMaintenance.CoverTypeDesc = dr["CoverTypeDesc"].ToString();

                if (!String.IsNullOrEmpty(dr["CalculatedOdometer"].ToString()))
                    retVehicleMaintenance.CalculatedOdometer = System.Convert.ToInt32(dr["CalculatedOdometer"]);
                else
                    retVehicleMaintenance.CalculatedOdometer = 0;

                if (!String.IsNullOrEmpty(dr["ActualOdometer"].ToString()))
                    retVehicleMaintenance.ActualOdometer = System.Convert.ToInt32(dr["ActualOdometer"]);
                else
                    retVehicleMaintenance.ActualOdometer = 0;

                if (!String.IsNullOrEmpty(dr["CalculatedEngineHrs"].ToString()))
                    retVehicleMaintenance.CalculatedEngineHrs = System.Convert.ToInt32(dr["CalculatedEngineHrs"]);
                else
                    retVehicleMaintenance.CalculatedEngineHrs = 0;

                if (!String.IsNullOrEmpty(dr["ActualEngineHrs"].ToString()))
                    retVehicleMaintenance.ActualEngineHrs = System.Convert.ToInt32(dr["ActualEngineHrs"]);
                else
                    retVehicleMaintenance.ActualEngineHrs = 0;

                retVehicleMaintenance.AdditionalInfo = dr["AdditionalInfo"].ToString();

                retVehicleMaintenance.CreatedDate = (DateTime)dr["CreatedDate"];
                retVehicleMaintenance.CreatedBy = dr["CreatedBy"].ToString();
                retVehicleMaintenance.UpdatedDate = (DateTime)dr["UpdatedDate"];
                retVehicleMaintenance.UpdatedBy = dr["UpdatedBy"].ToString();

                retVehicleMaintenance.AssetStatus = dr["AssetStatus"].ToString();
                retVehicleMaintenance.Comment = dr["Comment"].ToString();
                
                 return retVehicleMaintenance;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private DataTable DTVehicleMaintItems(DataTable dsVehicleMaintItems, int iRowId, int iMaintUri)
        {
            DataTable dt = new DataTable();

            dt.TableName = "GFI_AMM_VehicleMaintItems";

            dt.Columns.Add("URI", typeof(Int32));
            dt.Columns.Add("MaintURI", typeof(Int32));
            dt.Columns.Add("ItemCode", typeof(String));
            dt.Columns.Add("Description", typeof(String));
            dt.Columns.Add("Quantity", typeof(Int32));
            dt.Columns.Add("UnitCost", typeof(Decimal));
            dt.Columns.Add("CreatedDate", typeof(DateTime));
            dt.Columns.Add("CreatedBy", typeof(String));
            dt.Columns.Add("UpdatedDate", typeof(DateTime));
            dt.Columns.Add("UpdatedBy", typeof(String));

            DataRow dr = dt.NewRow();

            dr["URI"] = 0;
            dr["MaintURI"] = iMaintUri;
            dr["ItemCode"] = dsVehicleMaintItems.Rows[iRowId]["ItemCode"].ToString();
            dr["Description"] = dsVehicleMaintItems.Rows[iRowId]["Description"].ToString();
            dr["Quantity"] = (Decimal)dsVehicleMaintItems.Rows[iRowId]["Quantity"];
            dr["UnitCost"] = (Decimal)dsVehicleMaintItems.Rows[iRowId]["UnitCost"];
            dr["CreatedDate"] = (DateTime)dsVehicleMaintItems.Rows[iRowId]["CreatedDate"];
            dr["CreatedBy"] = dsVehicleMaintItems.Rows[iRowId]["CreatedBy"].ToString();
            dr["UpdatedDate"] = (DateTime)dsVehicleMaintItems.Rows[iRowId]["UpdatedDate"];
            dr["UpdatedBy"] = dsVehicleMaintItems.Rows[iRowId]["UpdatedBy"].ToString();
            
            dt.Rows.Add(dr);

            return dt;

        }

        private DataTable DTVehicleMaintTypeLink(FormModes FormMode, VehicleMaintenance uVehicleMaintenance, VehicleMaintTypeLink uVehicleMaintTypeLink, String sConnStr)
        {
            VehicleMaintType sVehicleMaintType = new VehicleMaintType();

            int iMaintTypeId = uVehicleMaintenance.MaintTypeId_cbo;
            
            int iInsMaintCatId = 0;
            int iMaintStatus_Completed = 0;
            int iMaintStatus_Expired = 0;
            
            DataRowState soprType;

            if (FormMode == FormModes.Add)
            {
                soprType = DataRowState.Added;
                uVehicleMaintTypeLink.URI = 0;
            }
            else
            {
                if (uVehicleMaintenance.VMTLURI == 0)
                {
                    soprType = DataRowState.Added;
                }
                else
                {
                    soprType = DataRowState.Modified;
                }

                uVehicleMaintTypeLink.URI = uVehicleMaintenance.VMTLURI;
            }


            //-------------------------------------------------------------------------------------//
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = null;
            String sql = string.Empty;

            sql = new VehicleMaintTypeService().sGetVehicleMaintTypeById(iMaintTypeId.ToString());
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtVMT";
            dtSql.Rows.Add(drSql);

            sql = new VehicleMaintCatService().sGetVehicleMaintCatByDescription();
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = "RENEWALS";
            drSql["sTableName"] = "dtInsurance";
            dtSql.Rows.Add(drSql);

            sql = new VehicleMaintStatusService().sGetVehicleMaintStatusByDescription();
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = "COMPLETED";
            drSql["sTableName"] = "dtCompleted";
            dtSql.Rows.Add(drSql);

            sql = new VehicleMaintStatusService().sGetVehicleMaintStatusByDescription();
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = "EXPIRED";
            drSql["sTableName"] = "dtExpired";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            sVehicleMaintType = new VehicleMaintTypeService().VMT(ds.Tables["dtVMT"].Rows[0]);
            iInsMaintCatId = Convert.ToInt32(ds.Tables["dtInsurance"].Rows[0]["MaintCatId"]);
            iMaintStatus_Completed = Convert.ToInt32(ds.Tables["dtCompleted"].Rows[0]["MaintStatusId"]);
            iMaintStatus_Expired = Convert.ToInt32(ds.Tables["dtExpired"].Rows[0]["MaintStatusId"]);

            //--------------------------------------------------------------------------

            uVehicleMaintTypeLink.AsssetId = uVehicleMaintenance.AssetId;
            uVehicleMaintTypeLink.MaintURI = uVehicleMaintenance.URI;
            uVehicleMaintTypeLink.MaintTypeId = uVehicleMaintenance.MaintTypeId_cbo;
            uVehicleMaintTypeLink.Status = uVehicleMaintenance.MaintStatusId_cbo;


            if (sVehicleMaintType.OccurrenceType == 1)
            {
                uVehicleMaintTypeLink.NextMaintDate = sVehicleMaintType.OccurrenceFixedDate;
                uVehicleMaintTypeLink.NextMaintDateTG = sVehicleMaintType.OccurrenceFixedDate.AddDays(-sVehicleMaintType.OccurrenceFixedDateTh);

                uVehicleMaintTypeLink.CurrentOdometer = uVehicleMaintenance.ActualOdometer;
                uVehicleMaintTypeLink.NextMaintOdometer = 0;
                uVehicleMaintTypeLink.NextMaintOdometerTG = 0;

                uVehicleMaintTypeLink.CurrentEngHrs = uVehicleMaintenance.ActualEngineHrs;
                uVehicleMaintTypeLink.NextMaintEngHrs = 0;
                uVehicleMaintTypeLink.NextMaintEngHrsTG = 0;
            }
            else if (sVehicleMaintType.OccurrenceType == 2)
            {
                if (sVehicleMaintType.OccurrencePeriod_cbo.ToUpper() == "DAY(S)")
                {
                    if ((uVehicleMaintenance.MaintStatusId_cbo == iMaintStatus_Completed) || (uVehicleMaintenance.MaintStatusId_cbo == iMaintStatus_Expired))
                    {
                        if (sVehicleMaintType.OccurrenceDuration > 0)
                        {
                            if (uVehicleMaintenance.MaintCatId_cbo == iInsMaintCatId)
                            {
                                uVehicleMaintTypeLink.NextMaintDate = uVehicleMaintenance.EndDate.AddDays(sVehicleMaintType.OccurrenceDuration);
                            }
                            else
                            {
                                uVehicleMaintTypeLink.NextMaintDate = uVehicleMaintenance.StartDate.AddDays(sVehicleMaintType.OccurrenceDuration);
                            }

                            DateTime dTemp = (DateTime)uVehicleMaintTypeLink.NextMaintDate;

                            uVehicleMaintTypeLink.NextMaintDateTG = dTemp.AddDays(-sVehicleMaintType.OccurrenceDurationTh);
                        }
                    }
                    else
                    {
                        if (uVehicleMaintenance.MaintCatId_cbo == iInsMaintCatId)
                        {
                            uVehicleMaintTypeLink.NextMaintDate = uVehicleMaintenance.EndDate;
                        }
                        else
                        {
                            uVehicleMaintTypeLink.NextMaintDate = uVehicleMaintenance.StartDate;
                        }

                        DateTime dTemp = (DateTime)uVehicleMaintTypeLink.NextMaintDate;

                        uVehicleMaintTypeLink.NextMaintDateTG = dTemp.AddDays(-sVehicleMaintType.OccurrenceDurationTh);
                    }
                }
                else if (sVehicleMaintType.OccurrencePeriod_cbo.ToUpper() == "MONTH(S)")
                {
                    if ((uVehicleMaintenance.MaintStatusId_cbo == iMaintStatus_Completed) || (uVehicleMaintenance.MaintStatusId_cbo == iMaintStatus_Expired))
                    {
                        if (sVehicleMaintType.OccurrenceDuration > 0)
                        {
                            if (uVehicleMaintenance.MaintCatId_cbo == iInsMaintCatId)
                            {
                                uVehicleMaintTypeLink.NextMaintDate = uVehicleMaintenance.EndDate.AddMonths(sVehicleMaintType.OccurrenceDuration);
                            }
                            else
                            {
                                uVehicleMaintTypeLink.NextMaintDate = uVehicleMaintenance.StartDate.AddMonths(sVehicleMaintType.OccurrenceDuration);
                            }

                            DateTime dTemp = (DateTime)uVehicleMaintTypeLink.NextMaintDate;

                            uVehicleMaintTypeLink.NextMaintDateTG = dTemp.AddMonths(-sVehicleMaintType.OccurrenceDurationTh);
                        }
                    }
                    else
                    {
                        if (uVehicleMaintenance.MaintCatId_cbo == iInsMaintCatId)
                        {
                            uVehicleMaintTypeLink.NextMaintDate = uVehicleMaintenance.EndDate;
                        }
                        else
                        {
                            uVehicleMaintTypeLink.NextMaintDate = uVehicleMaintenance.StartDate;
                        }

                        DateTime dTemp = (DateTime)uVehicleMaintTypeLink.NextMaintDate;

                        uVehicleMaintTypeLink.NextMaintDateTG = dTemp.AddMonths(-sVehicleMaintType.OccurrenceDurationTh);
                    }
                }
                else
                {
                    uVehicleMaintTypeLink.NextMaintDate = uVehicleMaintenance.EndDate;// DateTime.MinValue;
                    uVehicleMaintTypeLink.NextMaintDateTG = uVehicleMaintenance.EndDate;// DateTime.MinValue;
                }

                uVehicleMaintTypeLink.CurrentOdometer = uVehicleMaintenance.ActualOdometer;
                uVehicleMaintTypeLink.NextMaintOdometer = uVehicleMaintenance.CalculatedOdometer + sVehicleMaintType.OccurrenceKM;
                uVehicleMaintTypeLink.NextMaintOdometerTG = uVehicleMaintenance.ActualOdometer + sVehicleMaintType.OccurrenceKMTh;

                uVehicleMaintTypeLink.CurrentEngHrs = uVehicleMaintenance.ActualEngineHrs;
                uVehicleMaintTypeLink.NextMaintEngHrs = uVehicleMaintenance.ActualEngineHrs + sVehicleMaintType.OccurrenceEngineHrs;
                uVehicleMaintTypeLink.NextMaintEngHrsTG = uVehicleMaintenance.ActualEngineHrs + sVehicleMaintType.OccurrenceEngineHrsTh;
            }
            else
            {
                uVehicleMaintTypeLink.NextMaintDate = uVehicleMaintenance.StartDate; 
                uVehicleMaintTypeLink.NextMaintDateTG = uVehicleMaintenance.StartDate; ;

                uVehicleMaintTypeLink.CurrentOdometer = uVehicleMaintenance.ActualOdometer;
                uVehicleMaintTypeLink.NextMaintOdometer = 0;
                uVehicleMaintTypeLink.NextMaintOdometerTG = 0;

                uVehicleMaintTypeLink.CurrentEngHrs = uVehicleMaintenance.ActualEngineHrs;
                uVehicleMaintTypeLink.NextMaintEngHrs = 0;
                uVehicleMaintTypeLink.NextMaintEngHrsTG = 0;

            }

            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_VehicleMaintTypesLink";
            dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Columns.Add("URI", uVehicleMaintTypeLink.URI.GetType());
            dt.Columns.Add("MaintURI", uVehicleMaintTypeLink.MaintURI.GetType());
            dt.Columns.Add("AssetId", uVehicleMaintTypeLink.AsssetId.GetType());
            dt.Columns.Add("MaintTypeId", uVehicleMaintTypeLink.MaintTypeId.GetType());
            dt.Columns.Add("NextMaintDate", typeof(DateTime));
            dt.Columns.Add("NextMaintDateTG", typeof(DateTime));
            dt.Columns.Add("CurrentOdometer", uVehicleMaintTypeLink.CurrentOdometer.GetType());
            dt.Columns.Add("NextMaintOdometer", uVehicleMaintTypeLink.NextMaintOdometer.GetType());
            dt.Columns.Add("NextMaintOdometerTG", uVehicleMaintTypeLink.NextMaintOdometerTG.GetType());
            dt.Columns.Add("CurrentEngHrs", uVehicleMaintTypeLink.CurrentEngHrs.GetType());
            dt.Columns.Add("NextMaintEngHrs", uVehicleMaintTypeLink.NextMaintEngHrs.GetType());
            dt.Columns.Add("NextMaintEngHrsTG", uVehicleMaintTypeLink.NextMaintEngHrsTG.GetType());
            dt.Columns.Add("Status", uVehicleMaintTypeLink.Status.GetType());
            dt.Columns.Add("CreatedDate", uVehicleMaintTypeLink.CreatedDate.GetType());
            dt.Columns.Add("CreatedBy", uVehicleMaintTypeLink.CreatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uVehicleMaintTypeLink.UpdatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uVehicleMaintTypeLink.UpdatedBy.GetType());

            DataRow dr = dt.NewRow();
            dr["oprType"] = soprType;
            dr["URI"] = uVehicleMaintTypeLink.URI;
            dr["MaintURI"] = uVehicleMaintTypeLink.MaintURI;
            dr["AssetId"] = uVehicleMaintTypeLink.AsssetId;
            dr["MaintTypeId"] = uVehicleMaintTypeLink.MaintTypeId;

            if (uVehicleMaintTypeLink.NextMaintDate == DateTime.MinValue)
                dr["NextMaintDate"] = DBNull.Value;
            else
                dr["NextMaintDate"] = uVehicleMaintTypeLink.NextMaintDate;

            if (uVehicleMaintTypeLink.NextMaintDateTG == DateTime.MinValue)
                dr["NextMaintDateTG"] = DBNull.Value;
            else
                dr["NextMaintDateTG"] = uVehicleMaintTypeLink.NextMaintDateTG;
            
            dr["CurrentOdometer"] = uVehicleMaintTypeLink.CurrentOdometer;
            dr["NextMaintOdometer"] = uVehicleMaintTypeLink.NextMaintOdometer;
            dr["NextMaintOdometerTG"] = uVehicleMaintTypeLink.NextMaintOdometerTG;
            dr["CurrentEngHrs"] = uVehicleMaintTypeLink.CurrentEngHrs;
            dr["NextMaintEngHrs"] = uVehicleMaintTypeLink.NextMaintEngHrs;
            dr["NextMaintEngHrsTG"] = uVehicleMaintTypeLink.NextMaintEngHrsTG;
            dr["Status"] = uVehicleMaintTypeLink.Status;
            dr["CreatedDate"] = uVehicleMaintTypeLink.CreatedDate;
            dr["CreatedBy"] = uVehicleMaintTypeLink.CreatedBy;
            dr["UpdatedDate"] = uVehicleMaintTypeLink.UpdatedDate;
            dr["UpdatedBy"] = uVehicleMaintTypeLink.UpdatedBy;
            dt.Rows.Add(dr);

            return dt;
        }

        public bool SaveMaintRec(DataRowState drState, VehicleMaintenance myVehicleMaintRec, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_VehicleMaintenance";
            if (!dt.Columns.Contains("oprType"))
                dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Columns.Add("URI", typeof(int));
            dt.Columns.Add("AssetId", typeof(int));
            dt.Columns.Add("MaintTypeId_cbo", typeof(int));
            dt.Columns.Add("ContactDetails", typeof(String));
            dt.Columns.Add("CompanyName", typeof(String));
            dt.Columns.Add("PhoneNumber", typeof(String));
            dt.Columns.Add("CompanyRef", typeof(String));
            dt.Columns.Add("CompanyRef2", typeof(String));
            dt.Columns.Add("MaintDescription", typeof(String));
            dt.Columns.Add("StartDate", typeof(DateTime));
            dt.Columns.Add("EndDate", typeof(DateTime));
            dt.Columns.Add("MaintStatusId_cbo", typeof(int));
            dt.Columns.Add("EstimatedValue", typeof(Decimal));
            dt.Columns.Add("VATInclInItemsAmt", typeof(String));
            dt.Columns.Add("VATAmount", typeof(Decimal));
            dt.Columns.Add("TotalCost", typeof(Decimal));
            dt.Columns.Add("CoverTypeId_cbo", typeof(int));
            dt.Columns.Add("CalculatedOdometer", typeof(int));
            dt.Columns.Add("ActualOdometer", typeof(int));
            dt.Columns.Add("CalculatedEngineHrs", typeof(int));
            dt.Columns.Add("ActualEngineHrs", typeof(int));
            dt.Columns.Add("AdditionalInfo", typeof(String));
            dt.Columns.Add("CreatedDate", typeof(DateTime));
            dt.Columns.Add("CreatedBy", typeof(String));
            dt.Columns.Add("UpdatedDate", typeof(DateTime));
            dt.Columns.Add("UpdatedBy", typeof(String));

            DataRow dr = dt.NewRow();
            dr["oprType"] = drState;
            dr["URI"] = myVehicleMaintRec.URI;
            dr["AssetId"] = myVehicleMaintRec.AssetId;
            dr["MaintTypeId_cbo"] = myVehicleMaintRec.MaintTypeId_cbo;
            dr["ContactDetails"] = myVehicleMaintRec.ContactDetails;
            dr["CompanyName"] = myVehicleMaintRec.CompanyName;
            dr["PhoneNumber"] = myVehicleMaintRec.PhoneNumber;
            dr["CompanyRef"] = myVehicleMaintRec.CompanyRef;
            dr["CompanyRef2"] = myVehicleMaintRec.CompanyRef2;
            dr["MaintDescription"] = myVehicleMaintRec.MaintDescription;
            dr["StartDate"] = myVehicleMaintRec.StartDate;
            dr["EndDate"] = myVehicleMaintRec.EndDate;
            dr["MaintStatusId_cbo"] = myVehicleMaintRec.MaintStatusId_cbo;
            dr["EstimatedValue"] = myVehicleMaintRec.EstimatedValue;
            dr["VATInclInItemsAmt"] = myVehicleMaintRec.VATInclInItemsAmt;
            dr["VATAmount"] = myVehicleMaintRec.VATAmount;
            dr["TotalCost"] = myVehicleMaintRec.TotalCost;
            dr["CoverTypeId_cbo"] = myVehicleMaintRec.CoverTypeId_cbo;
            dr["CalculatedOdometer"] = myVehicleMaintRec.CalculatedOdometer;
            dr["ActualOdometer"] = myVehicleMaintRec.ActualOdometer;
            dr["CalculatedEngineHrs"] = myVehicleMaintRec.CalculatedEngineHrs;
            dr["ActualEngineHrs"] = myVehicleMaintRec.ActualEngineHrs;
            dr["AdditionalInfo"] = myVehicleMaintRec.AdditionalInfo;
            dr["CreatedDate"] = myVehicleMaintRec.CreatedDate;
            dr["CreatedBy"] = myVehicleMaintRec.CreatedBy;
            dr["UpdatedDate"] = myVehicleMaintRec.UpdatedDate;
            dr["UpdatedBy"] = myVehicleMaintRec.UpdatedBy;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr);
            DataSet ds = new DataSet();

            DataTable CTRLTBL = _connection.CTRLTBL();
            CTRLTBL.TableName = "CTRLTBL";
            ds.Tables.Add(CTRLTBL);

            DataRow drCtrl = null;

            drCtrl = CTRLTBL.Rows.Add();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "URI";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "GET";
            ds.Tables.Add(dt);

            DataSet dsResult = _connection.ProcessData(ds, sConnStr);

            DataTable dtCTRL = dsResult.Tables["CTRLTBL"];

            if (dtCTRL != null)
            {
                DataRow[] dRow = dtCTRL.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }

        public bool SaveMaintTypeLinkRec()
        {
            return false;
        }

        public Boolean SaveVehicleMaintenance(VehicleMaintenance uVehicleMaintenance, DataTable dtVehicleMaintItems, VehicleMaintTypeLink uVehicleMaintTypeLink, int iOccurrenceType, out DataSet dsResult, DbTransaction transaction, String sConnStr)
        {
            try
            {
                DataTable dt = new DataTable();
                dt.TableName = "GFI_AMM_VehicleMaintenance";
                if (!dt.Columns.Contains("oprType"))
                    dt.Columns.Add("oprType", typeof(DataRowState));
                dt.Columns.Add("URI", uVehicleMaintenance.URI.GetType());
                dt.Columns.Add("AssetId", uVehicleMaintenance.AssetId.GetType());
                dt.Columns.Add("MaintTypeId_cbo", uVehicleMaintenance.MaintTypeId_cbo.GetType());
                dt.Columns.Add("ContactDetails", uVehicleMaintenance.ContactDetails.GetType());
                dt.Columns.Add("CompanyName", uVehicleMaintenance.CompanyName.GetType());
                dt.Columns.Add("PhoneNumber", uVehicleMaintenance.PhoneNumber.GetType());
                dt.Columns.Add("CompanyRef", uVehicleMaintenance.CompanyRef.GetType());
                dt.Columns.Add("CompanyRef2", uVehicleMaintenance.CompanyRef.GetType());
                dt.Columns.Add("MaintDescription", uVehicleMaintenance.MaintDescription.GetType());
                dt.Columns.Add("StartDate", uVehicleMaintenance.StartDate.GetType());
                dt.Columns.Add("EndDate", uVehicleMaintenance.EndDate.GetType());
                dt.Columns.Add("MaintStatusId_cbo", uVehicleMaintenance.MaintStatusId_cbo.GetType());
                dt.Columns.Add("EstimatedValue", uVehicleMaintenance.EstimatedValue.GetType());
                dt.Columns.Add("VATInclInItemsAmt", uVehicleMaintenance.VATInclInItemsAmt.GetType());
                dt.Columns.Add("VATAmount", uVehicleMaintenance.VATAmount.GetType());
                dt.Columns.Add("TotalCost", uVehicleMaintenance.TotalCost.GetType());
                dt.Columns.Add("CoverTypeId_cbo", uVehicleMaintenance.CoverTypeId_cbo.GetType());
                dt.Columns.Add("CalculatedOdometer", uVehicleMaintenance.CalculatedOdometer.GetType());
                dt.Columns.Add("ActualOdometer", uVehicleMaintenance.ActualOdometer.GetType());
                dt.Columns.Add("CalculatedEngineHrs", uVehicleMaintenance.CalculatedEngineHrs.GetType());
                dt.Columns.Add("ActualEngineHrs", uVehicleMaintenance.ActualEngineHrs.GetType());
                dt.Columns.Add("AdditionalInfo", uVehicleMaintenance.AdditionalInfo.GetType());
                dt.Columns.Add("CreatedDate", uVehicleMaintenance.CreatedDate.GetType());
                dt.Columns.Add("CreatedBy", uVehicleMaintenance.CreatedBy.GetType());
                dt.Columns.Add("UpdatedDate", uVehicleMaintenance.UpdatedDate.GetType());
                dt.Columns.Add("UpdatedBy", uVehicleMaintenance.UpdatedBy.GetType());
                dt.Columns.Add("AssetStatus", uVehicleMaintenance.AssetStatus.GetType());
                dt.Columns.Add("Comment", uVehicleMaintenance.Comment.GetType());

                DataRow dr = dt.NewRow();
                dr["oprType"] = DataRowState.Added;
                dr["URI"] = 0;
                dr["AssetId"] = uVehicleMaintenance.AssetId;
                dr["MaintTypeId_cbo"] = uVehicleMaintenance.MaintTypeId_cbo;
                dr["ContactDetails"] = uVehicleMaintenance.ContactDetails;
                dr["CompanyName"] = uVehicleMaintenance.CompanyName;
                dr["PhoneNumber"] = uVehicleMaintenance.PhoneNumber;
                dr["CompanyRef"] = uVehicleMaintenance.CompanyRef;
                dr["CompanyRef2"] = uVehicleMaintenance.CompanyRef2;
                dr["MaintDescription"] = uVehicleMaintenance.MaintDescription;
                dr["StartDate"] = uVehicleMaintenance.StartDate;
                dr["EndDate"] = uVehicleMaintenance.EndDate;
                dr["MaintStatusId_cbo"] = uVehicleMaintenance.MaintStatusId_cbo;
                dr["EstimatedValue"] = uVehicleMaintenance.EstimatedValue;
                dr["VATInclInItemsAmt"] = uVehicleMaintenance.VATInclInItemsAmt;
                dr["VATAmount"] = uVehicleMaintenance.VATAmount;
                dr["TotalCost"] = uVehicleMaintenance.TotalCost;
                dr["CoverTypeId_cbo"] = uVehicleMaintenance.CoverTypeId_cbo;
                dr["CalculatedOdometer"] = uVehicleMaintenance.CalculatedOdometer;
                dr["ActualOdometer"] = uVehicleMaintenance.ActualOdometer;
                dr["CalculatedEngineHrs"] = uVehicleMaintenance.CalculatedEngineHrs;
                dr["ActualEngineHrs"] = uVehicleMaintenance.ActualEngineHrs;
                dr["AdditionalInfo"] = uVehicleMaintenance.AdditionalInfo;
                dr["CreatedDate"] = uVehicleMaintenance.CreatedDate;
                dr["CreatedBy"] = uVehicleMaintenance.CreatedBy;
                dr["UpdatedDate"] = uVehicleMaintenance.UpdatedDate;
                dr["UpdatedBy"] = uVehicleMaintenance.UpdatedBy;
                dr["AssetStatus"] = uVehicleMaintenance.AssetStatus;
                dr["Comment"] = uVehicleMaintenance.Comment;
                dt.Rows.Add(dr);

                Connection _connection = new Connection(sConnStr);
                DataSet ds = new DataSet();

                DataTable CTRLTBL = _connection.CTRLTBL();
                CTRLTBL.TableName = "CTRLTBL";

                DataRow drCtrl = null;
                drCtrl = CTRLTBL.Rows.Add();
                drCtrl["SeqNo"] = 1;
                drCtrl["TblName"] = dt.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "URI";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdAction"] = "GET";

                //--------------------------- VehicleMaintItems ---------------------------//
                dtVehicleMaintItems.TableName = "GFI_AMM_VehicleMaintItems";
                if (!dtVehicleMaintItems.Columns.Contains("oprType"))
                    dtVehicleMaintItems.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dRow in dtVehicleMaintItems.Rows)
                {
                    dRow["oprType"] = dRow.RowState;
                }

                drCtrl = CTRLTBL.Rows.Add();
                drCtrl["SeqNo"] = 2;
                drCtrl["TblName"] = dtVehicleMaintItems.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "URI";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdAction"] = "SET";
                drCtrl["NextIdFieldName"] = "MaintURI";
                drCtrl["ParentTblName"] = dt.TableName;

                ////--------------------------- VehicleMaintTypeLink ---------------------------//
                //DataTable dtVehicleMaintTypeLink = new DataTable();
                //dtVehicleMaintTypeLink.TableName = "GFI_AMM_VehicleMaintTypeLink";
                //dtVehicleMaintTypeLink = DTVehicleMaintTypeLink(FormModes.Add, uVehicleMaintenance, uVehicleMaintTypeLink);
                //drCtrl = CTRLTBL.Rows.Add();
                //drCtrl["SeqNo"] = 3;
                //drCtrl["TblName"] = dtVehicleMaintTypeLink.TableName;
                //drCtrl["CmdType"] = "TABLE";
                //drCtrl["KeyFieldName"] = "URI";
                //drCtrl["KeyIsIdentity"] = "TRUE";
                //drCtrl["NextIdAction"] = "SET";
                //drCtrl["NextIdFieldName"] = "MaintURI";
                //drCtrl["ParentTblName"] = dt.TableName;

                //------------------- Gets Data for record that was updated -------------------//
                DataTable dtSql = _connection.dtSql();
                DataRow drSql = null;
                String sql = string.Empty;

                sql = sGetVehicleMaintenanceByAssetId(uVehicleMaintenance.AssetId.ToString());
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtVM";
                dtSql.Rows.Add(drSql);

                sql = sGetVehicleMaintenanceById(uVehicleMaintenance.URI.ToString());
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtVMaint";
                dtSql.Rows.Add(drSql);

                sql = new VehicleMaintTypeLinkService().sGetVehicleMaintTypeLinkForAssetId(uVehicleMaintenance.AssetId);
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtVMTL";
                dtSql.Rows.Add(drSql);

                sql = new VehicleMaintItemsService().sGetVehicleMaintItemsById(uVehicleMaintenance.URI.ToString());
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtVMI";
                dtSql.Rows.Add(drSql);

                //--------------------------- Process Data---------------------------//
                ds.Tables.Add(CTRLTBL);
                ds.Merge(dt);
                ds.Merge(dtVehicleMaintItems);
                //ds.Merge(dtVehicleMaintTypeLink);
                ds.Merge(dtSql);

                dsResult =_connection.ProcessData(ds, sConnStr);

                foreach (DataRow dri in dsResult.Tables["dtVMI"].Rows)
                    dri.AcceptChanges();

                DataTable dtCTRL = dsResult.Tables["CTRLTBL"];
                if (dtCTRL != null)
                {
                    DataRow[] dRow = dtCTRL.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                    if (dRow.Length > 0)
                        return false;
                    else
                        return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public Boolean UpdateVehicleMaintenance(VehicleMaintenance uVehicleMaintenance, DataTable dtVehicleMaintItems, VehicleMaintTypeLink uVehicleMaintTypeLink, int iOccurrenceType, out DataSet dsResult, DbTransaction transaction, String sConnStr)
        {
            try
            {
                DataTable dt = new DataTable();
                dt.TableName = "GFI_AMM_VehicleMaintenance";
                if (!dt.Columns.Contains("oprType"))
                    dt.Columns.Add("oprType", typeof(DataRowState));
                dt.Columns.Add("URI", uVehicleMaintenance.URI.GetType());
                dt.Columns.Add("AssetId", uVehicleMaintenance.AssetId.GetType());
                dt.Columns.Add("MaintTypeId_cbo", uVehicleMaintenance.MaintTypeId_cbo.GetType());
                dt.Columns.Add("ContactDetails", uVehicleMaintenance.ContactDetails.GetType());
                dt.Columns.Add("CompanyName", uVehicleMaintenance.CompanyName.GetType());
                dt.Columns.Add("PhoneNumber", uVehicleMaintenance.PhoneNumber.GetType());
                dt.Columns.Add("CompanyRef", uVehicleMaintenance.CompanyRef.GetType());
                dt.Columns.Add("CompanyRef2", uVehicleMaintenance.CompanyRef2.GetType());
                dt.Columns.Add("MaintDescription", uVehicleMaintenance.MaintDescription.GetType());
                dt.Columns.Add("StartDate", uVehicleMaintenance.StartDate.GetType());
                dt.Columns.Add("EndDate", uVehicleMaintenance.EndDate.GetType());
                dt.Columns.Add("MaintStatusId_cbo", uVehicleMaintenance.MaintStatusId_cbo.GetType());
                dt.Columns.Add("EstimatedValue", uVehicleMaintenance.EstimatedValue.GetType());
                dt.Columns.Add("VATInclInItemsAmt", uVehicleMaintenance.VATInclInItemsAmt.GetType());
                dt.Columns.Add("VATAmount", uVehicleMaintenance.VATAmount.GetType());
                dt.Columns.Add("TotalCost", uVehicleMaintenance.TotalCost.GetType());
                dt.Columns.Add("CoverTypeId_cbo", uVehicleMaintenance.CoverTypeId_cbo.GetType());
                dt.Columns.Add("CalculatedOdometer", uVehicleMaintenance.CalculatedOdometer.GetType());
                dt.Columns.Add("ActualOdometer", uVehicleMaintenance.ActualOdometer.GetType());
                dt.Columns.Add("CalculatedEngineHrs", uVehicleMaintenance.CalculatedEngineHrs.GetType());
                dt.Columns.Add("ActualEngineHrs", uVehicleMaintenance.ActualEngineHrs.GetType());
                dt.Columns.Add("AdditionalInfo", uVehicleMaintenance.AdditionalInfo.GetType());
                dt.Columns.Add("CreatedDate", uVehicleMaintenance.CreatedDate.GetType());
                dt.Columns.Add("CreatedBy", uVehicleMaintenance.CreatedBy.GetType());
                dt.Columns.Add("UpdatedDate", uVehicleMaintenance.UpdatedDate.GetType());
                dt.Columns.Add("UpdatedBy", uVehicleMaintenance.UpdatedBy.GetType());
                dt.Columns.Add("AssetStatus", uVehicleMaintenance.AssetStatus.GetType());
                dt.Columns.Add("Comment", uVehicleMaintenance.Comment.GetType());

                DataRow dr = dt.NewRow();
                dr["oprType"] = DataRowState.Modified;
                dr["URI"] = uVehicleMaintenance.URI;
                dr["AssetId"] = uVehicleMaintenance.AssetId;
                dr["MaintTypeId_cbo"] = uVehicleMaintenance.MaintTypeId_cbo;
                dr["ContactDetails"] = uVehicleMaintenance.ContactDetails;
                dr["CompanyName"] = uVehicleMaintenance.CompanyName;
                dr["PhoneNumber"] = uVehicleMaintenance.PhoneNumber;
                dr["CompanyRef"] = uVehicleMaintenance.CompanyRef;
                dr["CompanyRef2"] = uVehicleMaintenance.CompanyRef2;
                dr["MaintDescription"] = uVehicleMaintenance.MaintDescription;
                dr["StartDate"] = uVehicleMaintenance.StartDate;
                dr["EndDate"] = uVehicleMaintenance.EndDate;
                dr["MaintStatusId_cbo"] = uVehicleMaintenance.MaintStatusId_cbo;
                dr["EstimatedValue"] = uVehicleMaintenance.EstimatedValue;
                dr["VATInclInItemsAmt"] = uVehicleMaintenance.VATInclInItemsAmt;
                dr["VATAmount"] = uVehicleMaintenance.VATAmount;
                dr["TotalCost"] = uVehicleMaintenance.TotalCost;
                dr["CoverTypeId_cbo"] = uVehicleMaintenance.CoverTypeId_cbo;
                dr["CalculatedOdometer"] = uVehicleMaintenance.CalculatedOdometer;
                dr["ActualOdometer"] = uVehicleMaintenance.ActualOdometer;
                dr["CalculatedEngineHrs"] = uVehicleMaintenance.CalculatedEngineHrs;
                dr["ActualEngineHrs"] = uVehicleMaintenance.ActualEngineHrs;
                dr["AdditionalInfo"] = uVehicleMaintenance.AdditionalInfo;
                dr["CreatedDate"] = uVehicleMaintenance.CreatedDate;
                dr["CreatedBy"] = uVehicleMaintenance.CreatedBy;
                dr["UpdatedDate"] = uVehicleMaintenance.UpdatedDate;
                dr["UpdatedBy"] = uVehicleMaintenance.UpdatedBy;
                dr["AssetStatus"] = uVehicleMaintenance.AssetStatus;
                dr["Comment"] = uVehicleMaintenance.Comment;
                dt.Rows.Add(dr);

                Connection _connection = new Connection(sConnStr);
                DataSet ds = new DataSet();

                DataTable CTRLTBL = _connection.CTRLTBL();
                CTRLTBL.TableName = "CTRLTBL";
                
                DataRow drCtrl = null;
                drCtrl = CTRLTBL.Rows.Add();
                drCtrl["SeqNo"] = 1;
                drCtrl["TblName"] = dt.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "URI";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdAction"] = "GET";

                //--------------------------- VehicleMaintItems ---------------------------//
                dtVehicleMaintItems.TableName = "GFI_AMM_VehicleMaintItems";
                if (!dtVehicleMaintItems.Columns.Contains("oprType"))
                    dtVehicleMaintItems.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dRow in dtVehicleMaintItems.Rows)
                {
                    switch (dRow.RowState)
                    {
                        case DataRowState.Added:
                            dRow["oprType"] = dRow.RowState;
                            break;

                        case DataRowState.Modified:
                            dRow["oprType"] = dRow.RowState;
                            break;

                        case DataRowState.Unchanged:
                            dRow["oprType"] = dRow.RowState;
                            break;

                        case DataRowState.Deleted:
                            dRow.RejectChanges();
                            dRow["oprType"] = dRow.RowState;
                            break;
                    }
                }

                drCtrl = CTRLTBL.Rows.Add();
                drCtrl["SeqNo"] = 2;
                drCtrl["TblName"] = dtVehicleMaintItems.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "URI";
                drCtrl["KeyIsIdentity"] = "TRUE";

                //--------------------------- VehicleMaintTypeLink ---------------------------//
                DataTable dtVehicleMaintTypeLink = new DataTable();
                dtVehicleMaintTypeLink.TableName = "GFI_AMM_VehicleMaintTypesLink";
                dtVehicleMaintTypeLink = DTVehicleMaintTypeLink(FormModes.Edit, uVehicleMaintenance, uVehicleMaintTypeLink, sConnStr);
                drCtrl = CTRLTBL.Rows.Add();
                drCtrl["SeqNo"] = 4;
                drCtrl["TblName"] = dtVehicleMaintTypeLink.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "URI";
                drCtrl["KeyIsIdentity"] = "TRUE";

                //---  Deletes old [Completed] records from VehicleMaintTypeLink   ---//
                //---  Retains only Last Completed record for Asset Id & Maintenance Type ---//
                drCtrl = CTRLTBL.Rows.Add();
                drCtrl["SeqNo"] = 5;
                drCtrl["TblName"] = "GFI_AMM_VehicleMaintTypesLink";
                drCtrl["CmdType"] = "STOREDPROC";
                drCtrl["Command"] = "DELETE GFI_AMM_VehicleMaintTypesLink WHERE AssetId = " + uVehicleMaintenance.AssetId + " AND MaintTypeId = " + uVehicleMaintenance.MaintTypeId_cbo + " AND [Status] = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'COMPLETED') AND URI <> (SELECT MAX(URI) FROM dbo.GFI_AMM_VehicleMaintTypesLink WHERE AssetId = " + uVehicleMaintenance.AssetId + " AND MaintTypeId = " + uVehicleMaintenance.MaintTypeId_cbo + " AND [Status] = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'COMPLETED'))";

                //---  Deletes old [Expired] records from VehicleMaintTypeLink   ---//
                //---  Retains only Last Completed record for Asset Id & Maintenance Type ---//
                drCtrl = CTRLTBL.Rows.Add();
                drCtrl["SeqNo"] = 6;
                drCtrl["TblName"] = "GFI_AMM_VehicleMaintTypesLink";
                drCtrl["CmdType"] = "STOREDPROC";
                drCtrl["Command"] = "DELETE GFI_AMM_VehicleMaintTypesLink WHERE AssetId = " + uVehicleMaintenance.AssetId + " AND MaintTypeId = " + uVehicleMaintenance.MaintTypeId_cbo + " AND [Status] = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'EXPIRED') AND URI <> (SELECT MAX(URI) FROM dbo.GFI_AMM_VehicleMaintTypesLink WHERE AssetId = " + uVehicleMaintenance.AssetId + " AND MaintTypeId = " + uVehicleMaintenance.MaintTypeId_cbo + " AND [Status] = (SELECT MaintStatusId FROM GFI_AMM_VehicleMaintStatus WHERE Description = 'EXPIRED'))";

                //------------------- Gets Data for record that was updated -------------------//
                DataTable dtSql = _connection.dtSql();
                DataRow drSql = null;
                String sql = string.Empty;

                sql = sGetVehicleMaintenanceByAssetId(uVehicleMaintenance.AssetId.ToString());
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtVM";
                dtSql.Rows.Add(drSql);

                sql = sGetVehicleMaintenanceById(uVehicleMaintenance.URI.ToString());
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtVMaint";
                dtSql.Rows.Add(drSql);

                sql = new VehicleMaintTypeLinkService().sGetVehicleMaintTypeLinkForAssetId(uVehicleMaintenance.AssetId);
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtVMTL";
                dtSql.Rows.Add(drSql);

                sql = new VehicleMaintItemsService().sGetVehicleMaintItemsById(uVehicleMaintenance.URI.ToString());
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtVMI";
                dtSql.Rows.Add(drSql);

                //--------------------------- Process Data---------------------------//
                ds.Tables.Add(CTRLTBL);
                ds.Merge(dt);
                ds.Merge(dtVehicleMaintItems);
                ds.Merge(dtVehicleMaintTypeLink);
                ds.Merge(dtSql);

                dsResult = _connection.ProcessData(ds, sConnStr);
                foreach (DataRow dri in dsResult.Tables["dtVMI"].Rows)
                    dri.AcceptChanges();

                DataTable dtCTRL = dsResult.Tables["CTRLTBL"];
                if (dtCTRL != null)
                {
                    DataRow[] dRow = dtCTRL.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                    if (dRow.Length > 0)
                        return false;
                    else
                        return true;
                }
                else
                    return false;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public Boolean DeleteVehicleMaintenance(VehicleMaintenance uVehicleMaintenance, DataTable dsVehicleMaintItems, out DataSet dsResult, String sConnStr)
        {
            try
            {
                DataTable dtVehicleMaintenance = new DataTable();
                dtVehicleMaintenance.TableName = "GFI_AMM_VehicleMaintenance";
                dtVehicleMaintenance.Columns.Add("oprType", typeof(DataRowState));
                dtVehicleMaintenance.Columns.Add("URI", uVehicleMaintenance.URI.GetType());

                DataRow drVehicleMaintenance = dtVehicleMaintenance.NewRow();
                drVehicleMaintenance["oprType"] = DataRowState.Deleted;
                drVehicleMaintenance["URI"] = uVehicleMaintenance.URI;
                dtVehicleMaintenance.Rows.Add(drVehicleMaintenance);

                //--- Prepare Control Table ---//
                Connection _connection = new Connection(sConnStr);
                DataSet ds = new DataSet();

                DataTable CTRLTBL = _connection.CTRLTBL();
                CTRLTBL.TableName = "CTRLTBL";

                DataRow drCtrl = CTRLTBL.Rows.Add();
                drCtrl["SeqNo"] = 3;
                drCtrl["TblName"] = dtVehicleMaintenance.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "URI";
                drCtrl["KeyIsIdentity"] = "TRUE";

                //---
                DataTable dtVehicleMaintItems = new DataTable();
                dtVehicleMaintItems.TableName = "GFI_AMM_VehicleMaintItems";
                dtVehicleMaintItems.Columns.Add("oprType", typeof(DataRowState));
                dtVehicleMaintItems.Columns.Add("MaintURI", uVehicleMaintenance.URI.GetType());

                if (dsVehicleMaintItems.Rows.Count > 0)
                {
                    DataRow drVehicleMaintItems = dtVehicleMaintItems.NewRow();
                    drVehicleMaintItems["oprType"] = DataRowState.Deleted;
                    drVehicleMaintItems["MaintURI"] = uVehicleMaintenance.URI;

                    dtVehicleMaintItems.Rows.Add(drVehicleMaintItems);
                }

                drCtrl = CTRLTBL.Rows.Add();
                drCtrl["SeqNo"] = 2;
                drCtrl["TblName"] = dtVehicleMaintItems.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "MaintURI";
                drCtrl["KeyIsIdentity"] = "TRUE"; 

                //---
                DataTable dtVehicleMaintTypeLink = new DataTable();
                dtVehicleMaintTypeLink.TableName = "GFI_AMM_VehicleMaintTypesLink";
                dtVehicleMaintTypeLink.Columns.Add("oprType", typeof(DataRowState));
                dtVehicleMaintTypeLink.Columns.Add("MaintURI", uVehicleMaintenance.URI.GetType());

                DataRow drVehicleMaintTypeLink = dtVehicleMaintTypeLink.NewRow();
                drVehicleMaintTypeLink["oprType"] = DataRowState.Deleted;
                drVehicleMaintTypeLink["MaintURI"] = uVehicleMaintenance.URI;

                dtVehicleMaintTypeLink.Rows.Add(drVehicleMaintTypeLink);

                drCtrl = CTRLTBL.Rows.Add();
                drCtrl["SeqNo"] = 1;
                drCtrl["TblName"] = dtVehicleMaintTypeLink.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "MaintURI";
                drCtrl["KeyIsIdentity"] = "TRUE";

                //--------------------------- Get Data ------------------------------//
                DataTable dtSql = _connection.dtSql();
                DataRow drSql = null;
                String sql = string.Empty;

                sql = sGetVehicleMaintenanceByAssetId(uVehicleMaintenance.AssetId.ToString());
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtVM";
                dtSql.Rows.Add(drSql);

                sql = sGetVehicleMaintenanceById(uVehicleMaintenance.URI.ToString());
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtVMaint";
                dtSql.Rows.Add(drSql);

                sql = new VehicleMaintTypeLinkService().sGetVehicleMaintTypeLinkForAssetId(uVehicleMaintenance.AssetId);
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtVMTL";
                dtSql.Rows.Add(drSql);

                sql = new VehicleMaintItemsService().sGetVehicleMaintItemsById(uVehicleMaintenance.URI.ToString());
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtVMI";
                dtSql.Rows.Add(drSql);

                //--------------------------- Process Data---------------------------//
                
                ds.Tables.Add(CTRLTBL);
                ds.Merge(dtVehicleMaintenance);
                ds.Merge(dtVehicleMaintItems);
                ds.Merge(dtVehicleMaintTypeLink);
                ds.Merge(dtSql);

                dsResult = _connection.ProcessData(ds, sConnStr);
                foreach (DataRow dri in dsResult.Tables["dtVMI"].Rows)
                    dri.AcceptChanges();

                DataTable dtCTRL = dsResult.Tables["CTRLTBL"];
                if (dtCTRL != null)
                {
                    DataRow[] dRow = dtCTRL.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                    if (dRow.Length > 0)
                        return false;
                    else
                        return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Boolean bExistsMaintTypeIDnCompanyRef(String MaintTypeID, String CompanyRef, String sConnStr)
        {
            Boolean b = false;

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = "select * from GFI_AMM_VehicleMaintenance where MaintTypeID_cbo = '" + MaintTypeID + "' and CompanyRef = '" + CompanyRef + "'";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "GPSAsset";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds != null)
                if (ds.Tables.Count > 0)
                    if (ds.Tables[0].Rows.Count > 0)
                        b = true;
            return b;
        }
    }
}
