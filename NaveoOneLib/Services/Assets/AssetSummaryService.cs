﻿using NaveoOneLib.Common;
using NaveoOneLib.Models;
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Zones;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Services.GPS;
using NaveoOneLib.Services.Trips;
using NaveoOneLib.Services.Zones;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace NaveoOneLib.Services.Assets
{
    public class AssetSummaryService
    {
        public DataTable VehicleSummaryOutsideZone(int UserID, int iHOZone, List<int> lZoneType, List<int> lAssetIDs, DateTime dtFr, DateTime dtTo, Boolean bDriver, String sConnStr)
        {
            if (bDriver)
                lAssetIDs = Validations.lValidateDrivers(UserID, lAssetIDs, sConnStr);
            else
                lAssetIDs = Validations.lValidateAssets(UserID, lAssetIDs, sConnStr);

            DataTable dtResult = new DataTable();
            //dtResult.Columns.Add("Date", typeof(DateTime));
            dtResult.Columns.Add("Date", typeof(String));
            dtResult.Columns.Add("VehicleNo", typeof(string));
            dtResult.Columns.Add("NoOfZones", typeof(int));
            dtResult.Columns.Add("Zones", typeof(String));

            //dtResult.Columns.Add("FirstIgnitionOn", typeof(DateTime));
            //dtResult.Columns.Add("LastIgnitionOff", typeof(DateTime));
            //dtResult.Columns.Add("StartTime", typeof(DateTime));
            //dtResult.Columns.Add("ArrivalTime", typeof(DateTime));
            dtResult.Columns.Add("FirstIgnitionOn", typeof(String));
            dtResult.Columns.Add("LastIgnitionOff", typeof(String));
            dtResult.Columns.Add("StartTime", typeof(String));
            dtResult.Columns.Add("ArrivalTime", typeof(String));

            dtResult.Columns.Add("DrivingTime", typeof(TimeSpan));
            dtResult.Columns.Add("IdlingTime", typeof(TimeSpan));
            dtResult.Columns.Add("StoppedTime", typeof(TimeSpan));
            dtResult.Columns.Add("TotalInTransitTime", typeof(TimeSpan));
            dtResult.Columns.Add("TripDistance", typeof(Double));
            dtResult.Columns.Add("Speed70", typeof(int));
            dtResult.Columns.Add("Speed90", typeof(int));
            dtResult.Columns.Add("Speed110", typeof(int));
            dtResult.Columns.Add("HighestSpeed", typeof(int));

            List<Matrix> lm = new MatrixService().GetMatrixByUserID(UserID, sConnStr);
            List<ZoneType> lZoneList = new ZoneTypeService().GetZoneTypeList(lm, sConnStr);
            lZoneType = lZoneType.Distinct().ToList();
            foreach (int i in lZoneType)
                foreach (ZoneType zt in lZoneList)
                    if (zt.ZoneTypeID == i)
                    {
                        string s = zt.sDescription.Replace(" ", "");
                        if (!dtResult.Columns.Contains(s))
                            dtResult.Columns.Add(s, typeof(TimeSpan));
                    }
            DataTable dtTH = new TripHeaderService().GetTripHeaders(lAssetIDs, dtFr, dtTo, 0, String.Empty, bDriver, false, true, lm, sConnStr, null, null, false, null, true).Tables["TripHeader"];
            if (dtTH.Rows.Count == 0)
                return dtResult;

            List<Zone> lZone = new WebSpecifics.LibData().lZone(UserID, sConnStr, null, null);
            Zone HOZone = new Zone().GetZone(iHOZone, lZone);
            foreach (int i in lAssetIDs)
            {
                String sFilter = "AssetID";
                TimeSpan ts = new TimeSpan();
                if (bDriver)
                {
                    sFilter = "DriverID";
                    int x = 0;
                    if (dtTH.Rows.Count > 0)
                        if (int.TryParse(dtTH.Rows[0]["AssetID"].ToString(), out x))
                            ts = new AssetService().tsAsset(x, sConnStr);
                }
                else
                    ts = new AssetService().tsAsset(i, sConnStr);

                DataRow[] filteredRows = dtTH.Select(sFilter + " = " + i);
                if (filteredRows.Length > 0)
                {
                    DataTable dtTrips = filteredRows.CopyToDataTable();
                    DataRow drResult = dtResult.NewRow();

                    //looking for 1st exit zone
                    int iRow = -1;
                    int iStart = 0;
                    foreach (DataRow dr in dtTrips.Rows)
                    {
                        iRow++;

                        String sArrivalZoneID = "wawa";
                        String[] sArrivalZoneIDs = dr["iArrivalZones"].ToString().Split(',');
                        foreach (String s in sArrivalZoneIDs)
                            if (s.Trim() == iHOZone.ToString())
                                sArrivalZoneID = iHOZone.ToString();

                        //Detecting Leaving HO
                        if (dr["DepartureZoneID"].ToString() == iHOZone.ToString() && sArrivalZoneID != iHOZone.ToString())
                        {
                            iStart = iRow;
                            drResult = dtResult.NewRow();
                            drResult["VehicleNo"] = dtTrips.Rows[iRow]["Vehicle"];
                            if(bDriver)
                                drResult["VehicleNo"] = dtTrips.Rows[iRow]["Driver"];

                            DateTime dtDate = Convert.ToDateTime(dtTrips.Rows[iRow]["From"]).Date;
                            DataRow[] rows = dtTrips.Select("GroupDate = #" + dtDate + "#");
                            DataRow result = rows.OrderByDescending(r => r["From"]).LastOrDefault();
                            drResult["FirstIgnitionOn"] = Convert.ToDateTime(result["From"]).ToString("HH:mm");

                            result = rows.OrderByDescending(r => r["To"]).FirstOrDefault();
                            drResult["LastIgnitionOff"] = Convert.ToDateTime(result["To"]).ToString("HH:mm");

                            drResult["Date"] = dtDate.ToString("dd/MM/yyyy");

                            #region getting exact time leaving zone
                            List<int> l = new List<int>();
                            l.Add(i);
                            DataTable dtGpsData = new GPSDataService().GetDebugData(l, ((DateTime)dr["From"]).Subtract(ts), ((DateTime)dr["To"]).Subtract(ts), sConnStr);
                            foreach (DataRow r in dtGpsData.Rows)
                            {
                                if (!HOZone.IsPointInside(HOZone, new Coordinate(Convert.ToDouble(r["Latitude"]), Convert.ToDouble(r["Longitude"]))))
                                {
                                    drResult["StartTime"] = ((DateTime)r["DateTimeGPS_UTC"]).Add(ts).ToString("HH:mm");
                                    break;
                                }
                            }
                            #endregion
                        }

                        //Detecting Arriving HO
                        if (sArrivalZoneID == iHOZone.ToString() && dr["DepartureZoneID"].ToString() != iHOZone.ToString())
                        {
                            #region getting exact time entering zone
                            drResult["ArrivalTime"] = ((DateTime)dr["To"]).ToString("HH:mm");
                            List<int> l = new List<int>();
                            l.Add(i);
                            DataTable dtGpsData = new GPSDataService().GetDebugData(l, ((DateTime)dr["From"]).Subtract(ts), ((DateTime)dr["To"]).Subtract(ts), sConnStr);
                            foreach (DataRow r in dtGpsData.Rows)
                            {
                                if (HOZone.IsPointInside(HOZone, new Coordinate(Convert.ToDouble(r["Latitude"]), Convert.ToDouble(r["Longitude"]))))
                                {
                                    drResult["ArrivalTime"] = ((DateTime)r["DateTimeGPS_UTC"]).Add(ts).ToString("HH:mm");
                                    break;
                                }
                            }
                            #endregion

                            //To compute from iStart to iRow
                            TimeSpan DrivingTime = TimeSpan.Parse("00:00:00"), IdlingTime = TimeSpan.Parse("00:00:00"), StopTime = TimeSpan.Parse("00:00:00");
                            Double dTripDistance = 0;
                            int iHighestSpeed = 0;
                            int iSpeed70 = 0;
                            int iSpeed90 = 0;
                            int iSpeed110 = 0;
                            List<String> lZones = new List<String>();

                            for (int j = iStart; j <= iRow; j++)
                            {
                                if (j != iRow)  //Last trip stop time
                                    if (dtTrips.Rows[j]["stoptime"].ToString().Trim().Length > 0)
                                        StopTime = StopTime.Add((TimeSpan)dtTrips.Rows[j]["stoptime"]);
                                if (dtTrips.Rows[j]["idlingtime"].ToString().Trim().Length > 0)
                                    IdlingTime = IdlingTime.Add((TimeSpan)dtTrips.Rows[j]["idlingtime"]);
                                if (dtTrips.Rows[j]["triptime"].ToString().Trim().Length > 0)
                                    DrivingTime = DrivingTime.Add((TimeSpan)dtTrips.Rows[j]["triptime"]);
                                if (dtTrips.Rows[j]["TripDistance"].ToString().Trim().Length > 0)
                                    dTripDistance += (Double)dtTrips.Rows[j]["TripDistance"];

                                int iSpeed = 0;
                                int.TryParse(dtTrips.Rows[j]["MaxSpeed"].ToString(), out iSpeed);
                                if (iSpeed > iHighestSpeed)
                                    iHighestSpeed = iSpeed;

                                int TripMaxSpeed = 0;
                                int.TryParse(dtTrips.Rows[j]["MaxSpeed"].ToString(), out TripMaxSpeed);
                                if (TripMaxSpeed > 110)
                                {
                                    iSpeed110++;
                                    iSpeed90++;
                                    iSpeed70++;
                                }
                                else if (TripMaxSpeed > 90)
                                {
                                    iSpeed90++;
                                    iSpeed70++;
                                }
                                else if (TripMaxSpeed > 70)
                                    iSpeed70++;

                                //if (dtTrips.Rows[j]["DepartureZone"].ToString().Trim().Length > 0)
                                //    lZones.Add(dtTrips.Rows[j]["DepartureZone"].ToString());
                                lZones.AddRange(dtTrips.Rows[j]["ArrivalZone"].ToString().Split(','));
                            }
                            drResult["DrivingTime"] = DrivingTime;
                            drResult["IdlingTime"] = IdlingTime;
                            drResult["StoppedTime"] = StopTime;
                            drResult["TotalInTransitTime"] = DrivingTime + IdlingTime + StopTime;

                            drResult["TripDistance"] = Math.Round(dTripDistance, 2);
                            drResult["HighestSpeed"] = iHighestSpeed;
                            drResult["Speed110"] = iSpeed110;
                            drResult["Speed90"] = iSpeed90;
                            drResult["Speed70"] = iSpeed70;

                            foreach (int x in lZoneType)
                            {
                                String sZT = String.Empty;
                                TimeSpan ZoneTypeTime = TimeSpan.Parse("00:00:00");
                                for (int j = iStart; j <= iRow; j++)
                                    foreach (ZoneType zt in lZoneList)
                                        if (zt.ZoneTypeID == x)
                                        {
                                            sZT = zt.sDescription.Replace(" ", "");
                                            ZoneTypeTime += GetTime(dtTrips.Rows[j], lZone, zt.sDescription);
                                        }

                                if (!String.IsNullOrEmpty(sZT))
                                    drResult[sZT] = ZoneTypeTime;
                            }

                            int iZoneCount = 0;
                            String zName = String.Empty;
                            foreach (String s in lZones)
                            {
                                String sZone = s.Trim();
                                if (sZone == HOZone.description)
                                    continue;

                                foreach (int j in lZoneType)
                                    foreach (ZoneType zt in HOZone.lZoneType)
                                        if (zt.ZoneTypeID == j)
                                            continue;

                                if (!zName.Contains(sZone))
                                {
                                    zName += sZone + ", ";
                                    iZoneCount++;
                                }
                            }
                            if (zName.Length > 3)
                                zName = zName.Substring(0, zName.Length - 2);
                            drResult["Zones"] = zName;
                            drResult["NoOfZones"] = iZoneCount;

                            ////Detecting Leaving HO > when not detected above
                            //if (String.IsNullOrEmpty(drResult["VehicleNo"].ToString().Trim()))
                            //    drResult["VehicleNo"] = dtTrips.Rows[iRow]["Vehicle"];

                            ////Detecting First Ignition when not detected above
                            //if (String.IsNullOrEmpty(drResult["FirstIgnitionOn_Time"].ToString().Trim()))
                            //{
                            //    DateTime dtDate = Convert.ToDateTime(dtTrips.Rows[iRow]["From"]).Date;
                            //    DataRow[] rows = dtTrips.Select("GroupDate = #" + dtDate + "#");
                            //    DataRow result = rows.OrderByDescending(r => r["From"]).LastOrDefault();
                            //    drResult["FirstIgnitionOn_Time"] = Convert.ToDateTime(result["From"]).ToString("hh:mm tt");

                            //    result = rows.OrderByDescending(r => r["To"]).FirstOrDefault();
                            //    drResult["LastIgnitionOff_Time"] = Convert.ToDateTime(result["To"]).ToString("hh:mm tt");

                            //    drResult["Date_Date"] = dtDate.ToString("dd/MM/yyyy");
                            //}

                            if (!String.IsNullOrEmpty(drResult["Date"].ToString()))
                                dtResult.Rows.Add(drResult.ItemArray);
                        }
                    }
                }
            }

            //DataTable dtFinal = dtResult.Select("Date_Date <> ''").CopyToDataTable();
            //DataView dv = dtResult.DefaultView;
            //dv.Sort = "VehicleNO, Date_Date, StartTime_Time desc";
            //return dv.ToTable();

            if (bDriver)
                dtResult.Columns["VehicleNo"].ColumnName = "Driver";
            return dtResult;
        }

        TimeSpan GetTime(DataRow dr, List<Zone> lz, String sZoneType)
        {
            TimeSpan ts = new TimeSpan(0);
            if (dr["stoptime"].ToString().Trim().Length > 0)
            {
                int iZoneID = 0;
                int.TryParse(dr["ArrivalZoneID"].ToString(), out iZoneID);
                if (iZoneID > 0)
                {
                    //Zone inside zone
                    List<Zone> lZones = new List<Zone>();
                    foreach (Zone z in lz)
                        if (z.IsPointInside(z, new Coordinate(Convert.ToDouble(dr["fEndLat"]), Convert.ToDouble(dr["fEndLon"]))))
                            foreach (ZoneType zt in z.lZoneType)
                                if (zt.sDescription == sZoneType)
                                {
                                    ts = ts.Add((TimeSpan)dr["stoptime"]);
                                    break;
                                }
                }
            }
            return ts;
        }
    }
}
