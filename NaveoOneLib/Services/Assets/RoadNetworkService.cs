﻿using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Maps;
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.WebSpecifics;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace NaveoOneLib.Services.Assets
{
    public class RoadNetworkService
    {
        public BaseModel RoadNetworkFromShp(Guid sUserToken, String shpPath, String sType, int iMatrix, String sConnStr)
        {
            List<String> lResult = new List<string>();
            int iCntNew = 0;
            int iCntUpdated = 0;

            int UID = new NaveoOneLib.Services.Users.UserService().GetUserID(sUserToken, sConnStr);

            BaseModel bm = new BaseModel();
            try
            {
                List<String> lfeatureIDs = NaveoOneLib.Maps.NaveoWebMapsCall.GetAllFeatureIDs(shpPath);
                switch (sType)
                {
                    case "Country":
                        foreach (String id in lfeatureIDs)
                        {
                            List<KeyValuePair<string, string>> lFeatures = NaveoWebMapsCall.GetAllFeaturesByID(shpPath, id);

                            String s = lFeatures.FirstOrDefault(x => x.Key == "CountryID").Value;
                            if (s == null)
                            {
                                lResult.Add("No CountryID : " + lFeatures.FirstOrDefault(x => x.Key == "Name").Value);
                                continue;
                            }

                            int CountryID = Convert.ToInt32(s);
                            Country c = GetCountryByID(CountryID, sConnStr);
                            if (c != null)
                                c.oprType = DataRowState.Modified;
                            else
                            {
                                c = new Country();
                                c.oprType = DataRowState.Added;
                            }
                            c.CountryID = CountryID;
                            c.CountryName = lFeatures.FirstOrDefault(x => x.Key == "Name").Value.ToString() == string.Empty ? null : lFeatures.FirstOrDefault(x => x.Key == "Name").Value;
                            c.iColor = Utils.GeneratePastelColors().ToArgb();
                            c.GeomData = lFeatures.FirstOrDefault(x => x.Key == "GeomData").Value.ToString() == string.Empty ? null : lFeatures.FirstOrDefault(x => x.Key == "GeomData").Value;
                            BaseModel bmSave = SaveCountry(c, sConnStr);
                            if (bmSave.data)
                            {
                                if (c.oprType == DataRowState.Modified)
                                    iCntUpdated++;
                                else
                                    iCntNew++;
                            }
                        }
                        break;

                    case "District":
                        Matrix m = new Matrix();
                        m.GMID = iMatrix;
                        m.oprType = DataRowState.Added;
                        List<Matrix> lMatrix = new List<Matrix>();
                        lMatrix.Add(m);

                        foreach (String id in lfeatureIDs)
                        {
                            List<KeyValuePair<string, string>> lFeatures = NaveoWebMapsCall.GetAllFeaturesByID(shpPath, id);

                            String s = lFeatures.FirstOrDefault(x => x.Key == "DistID").Value;
                            if (s == null)
                            {
                                lResult.Add("No District code : " + lFeatures.FirstOrDefault(x => x.Key == "DistProv").Value);
                                continue;
                            }

                            int DistictID = Convert.ToInt32(s);
                            District district = GetDistrictByID(DistictID, sConnStr);
                            if (district != null)
                                district.oprType = DataRowState.Modified;
                            else
                            {
                                district = new District();
                                district.oprType = DataRowState.Added;
                                district.lMatrix = lMatrix;
                            }
                            district.DistrictID = DistictID;
                            district.DistrictName = lFeatures.FirstOrDefault(x => x.Key == "DistProv").Value.ToString() == string.Empty ? null : lFeatures.FirstOrDefault(x => x.Key == "DistProv").Value;
                            district.iColor = Utils.GeneratePastelColors().ToArgb();
                            district.GeomData = lFeatures.FirstOrDefault(x => x.Key == "GeomData").Value.ToString() == string.Empty ? null : lFeatures.FirstOrDefault(x => x.Key == "GeomData").Value;
                            district.CountryID = lFeatures.FirstOrDefault(x => x.Key == "CountryID").Value.ToString() == string.Empty ? (int?)null : Convert.ToInt32(lFeatures.FirstOrDefault(x => x.Key == "CountryID").Value);
                            BaseModel bmSave = SaveDistrict(district, sConnStr);
                            if (bmSave.data)
                            {
                                if (district.oprType == DataRowState.Modified)
                                    iCntUpdated++;
                                else
                                    iCntNew++;
                            }
                        }
                        break;

                    case "VCA":
                        foreach (String id in lfeatureIDs)
                        {
                            List<KeyValuePair<string, string>> lFeatures = NaveoWebMapsCall.GetAllFeaturesByID(shpPath, id);

                            int DistrictID = Convert.ToInt32(lFeatures.FirstOrDefault(x => x.Key == "DistID").Value);
                            District d = GetDistrictByID(DistrictID, sConnStr);
                            if (d == null)
                                continue;

                            String s = lFeatures.FirstOrDefault(x => x.Key == "VcaID").Value;
                            if (s == null)
                            {
                                lResult.Add("No VCA code : " + lFeatures.FirstOrDefault(x => x.Key == "VcaDist").Value);
                                continue;
                            }
                            int VCAID = Convert.ToInt32(s);
                            VCA vca = GetVCAByID(VCAID, sConnStr);
                            if (vca != null)
                                continue;

                            vca = new VCA();
                            vca.VCAID = VCAID;
                            vca.DistrictID = DistrictID;
                            vca.VCAName = lFeatures.FirstOrDefault(x => x.Key == "VcaDist").Value.ToString() == string.Empty ? null : lFeatures.FirstOrDefault(x => x.Key == "VcaDist").Value;
                            vca.iColor = Utils.GeneratePastelColors().ToArgb();

                            vca.GeomData = lFeatures.FirstOrDefault(x => x.Key == "GeomData").Value.ToString() == string.Empty ? null : lFeatures.FirstOrDefault(x => x.Key == "GeomData").Value;
                            vca.oprType = DataRowState.Added;
                            BaseModel bmSave = SaveVCA(vca, sConnStr);
                            if (bmSave.data)
                            {
                                if (vca.oprType == DataRowState.Modified)
                                    iCntUpdated++;
                                else
                                    iCntNew++;
                            }
                        }
                        break;

                    case "RoadNetwork":
                        foreach (String id in lfeatureIDs)
                        {
                            Boolean bNew = true;
                            List<KeyValuePair<string, string>> lFeatures = NaveoWebMapsCall.GetAllFeaturesByID(shpPath, id);
                            Errors.WriteToLog(id + " " + lFeatures.Count + "\r\n");
                            if (int.TryParse(lFeatures.FirstOrDefault(x => x.Key == "VcaID").Value, out int VCAID))
                            {
                                VCA v = GetVCAByID(VCAID, sConnStr);
                                if (v == null)
                                    continue;
                            }
                            else 
                                continue;

                            String s = lFeatures.FirstOrDefault(x => x.Key == "NaveoID").Value;
                            if (s == null)
                            {
                                lResult.Add("No NaveoID : " + lFeatures.FirstOrDefault(x => x.Key == "NAME").Value);
                                continue;
                            }

                            int RoadID = Convert.ToInt32(s);
                            RoadNetwork rn = GetRoadNetworkByID(RoadID, sConnStr);
                            if (rn != null)
                                bNew = false; //continue;

                            rn = new RoadNetwork();
                            rn.RoadID = RoadID;
                            rn.VCAID = VCAID;
                            rn.RoadName = lFeatures.FirstOrDefault(x => x.Key == "NAME").Value.ToString() == string.Empty ? null : lFeatures.FirstOrDefault(x => x.Key == "NAME").Value;
                            rn.LengthM = lFeatures.FirstOrDefault(x => x.Key == "LengthM").Value.ToString() == string.Empty ? (Double?)null : Convert.ToDouble(lFeatures.FirstOrDefault(x => x.Key == "LengthM").Value);
                            rn.WidthM = lFeatures.FirstOrDefault(x => x.Key == "WIDTH").Value.ToString() == string.Empty ? (Double?)null : Convert.ToDouble(lFeatures.FirstOrDefault(x => x.Key == "WIDTH").Value);
                            rn.SpeedLimit = lFeatures.FirstOrDefault(x => x.Key == "SPD_LIMIT").Value.ToString() == string.Empty ? (int?)null : Convert.ToInt32(Convert.ToDouble(lFeatures.FirstOrDefault(x => x.Key == "SPD_LIMIT").Value));
                            rn.RoadClass = lFeatures.FirstOrDefault(x => x.Key == "RoadClass").Value.ToString() == string.Empty ? null : lFeatures.FirstOrDefault(x => x.Key == "RoadClass").Value;
                            rn.RoadType = lFeatures.FirstOrDefault(x => x.Key == "RoadType").Value.ToString() == string.Empty ? null : lFeatures.FirstOrDefault(x => x.Key == "RoadType").Value;
                            rn.GeomData = lFeatures.FirstOrDefault(x => x.Key == "GeomData").Value.ToString() == string.Empty ? null : lFeatures.FirstOrDefault(x => x.Key == "GeomData").Value;
                            rn.oprType = DataRowState.Added;
                            if (!bNew)
                                rn.oprType = DataRowState.Modified;
                            BaseModel bmSave = SaveRoadNetwork(rn, sConnStr);
                            if (bmSave.data)
                                if (bNew)
                                    iCntNew++;
                                else
                                    iCntUpdated++;
                        }
                        break;
                }

                lResult.Add("Features : " + lfeatureIDs.Count);
                lResult.Add("iCntNew : " + iCntNew);
                lResult.Add("iCntUpdated : " + iCntUpdated);
                lResult.Add("iCntTotal : " + (iCntNew + iCntUpdated));
            }
            catch (Exception ex)
            {
                lResult.Add(ex.ToString());
            }
            bm.data = lResult;
            return bm;
        }

        #region Country
        Country GetCountryByID(int CountryID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"Select CountryID
                                , CountryName, iColor
                                , f.GeomData.ToString() as GeomData
                                , dbo.Geometry2Json(f.GeomData.MakeValid()) Geom2Json
                            from GFI_GIS_Country f
                            where CountryID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = CountryID.ToString();
            drSql["sTableName"] = "dtCountry";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables["dtCountry"].Rows.Count == 0)
                return null;
            else
                return GetCountry(ds);
        }

        Country GetCountry(DataSet ds)
        {
            Country c = new Country();
            c.CountryID = Convert.ToInt32(ds.Tables["dtCountry"].Rows[0]["CountryID"]);
            c.CountryName = ds.Tables["dtCountry"].Rows[0]["CountryName"].ToString();
            c.iColor = Convert.ToInt32(ds.Tables["dtCountry"].Rows[0]["iColor"]);
            c.GeomData = String.IsNullOrEmpty(ds.Tables["dtCountry"].Rows[0]["GeomData"].ToString()) ? null : ds.Tables["dtCountry"].Rows[0]["GeomData"].ToString();

            return c;
        }

        DataTable CountryDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_GIS_Country";
            dt.Columns.Add("CountryID", typeof(int));
            dt.Columns.Add("CountryName", typeof(String));
            dt.Columns.Add("iColor", typeof(int));
            dt.Columns.Add("GeomData", typeof(String));
            dt.Columns.Add("oprType", typeof(DataRowState));

            return dt;
        }
        public BaseModel SaveCountry(Country country, String sConnStr)
        {
            BaseModel baseModel = new BaseModel();
            Boolean bResult = false;
            baseModel.data = bResult;

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            DataSet dsProcess = new DataSet();
            DataTable dt = CountryDT();

            Boolean bInsert = false;
            if (country.oprType == DataRowState.Added)
                bInsert = true;

            //GFI_GIS_Country
            DataRow dr = dt.NewRow();
            dr["CountryID"] = country.CountryID;
            dr["CountryName"] = country.CountryName;
            dr["iColor"] = country.iColor;
            dr["GeomData"] = country.GeomData != null ? country.GeomData : (object)DBNull.Value;
            dr["oprType"] = country.oprType;
            dt.Rows.Add(dr);

            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "CountryID";
            drCtrl["KeyIsIdentity"] = "FALSE";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dt);

            #region GeomData 4326
            String KeyField = "CountryID";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 4;
            drCtrl["TblName"] = "GeomDataField";
            drCtrl["NextIdAction"] = "SetToSP";
            drCtrl["NextIdFieldName"] = KeyField;
            drCtrl["ParentTblName"] = dt.TableName;
            drCtrl["CmdType"] = "STOREDPROC";
            String sql = "update " + dt.TableName + " set GeomData.STSrid = 4326 where " + KeyField + " = ";
            drCtrl["Command"] = sql + country.CountryID.ToString(); //sql + "?";
            if (!bInsert)
                drCtrl["Command"] = sql + country.CountryID.ToString();
            dtCtrl.Rows.Add(drCtrl);

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 5;
            drCtrl["TblName"] = "GeomDataField";
            drCtrl["NextIdAction"] = "SetToSP";
            drCtrl["NextIdFieldName"] = KeyField;
            drCtrl["ParentTblName"] = dt.TableName;
            drCtrl["CmdType"] = "STOREDPROC";
            sql = "update " + dt.TableName + " set GeomData = GeomData.MakeValid() where GeomData.STIsValid() = 0 and " + KeyField + " = ";
            drCtrl["Command"] = sql + country.CountryID.ToString(); //sql + "?";
            if (!bInsert)
                drCtrl["Command"] = sql + country.CountryID.ToString();
            dtCtrl.Rows.Add(drCtrl);

            #endregion

            dsProcess.Merge(dtCtrl);
            DataSet dsResults = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResults.Tables["CTRLTBL"];

            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                {
                    baseModel.dbResult = dsResults;
                    baseModel.dbResult.Tables["CTRLTBL"].TableName = "ResultCtrlTbl";
                    baseModel.dbResult.Merge(dsProcess);
                    bResult = false;
                }
                else
                    bResult = true;
            }
            else
                bResult = false;

            baseModel.data = bResult;
            return baseModel;
        }
        #endregion

        #region District
        District GetDistrictByID(int DistictID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"Select DistrictID
                                , DistrictName, iColor
                                , f.GeomData.ToString() as GeomData
                                , dbo.Geometry2Json(f.GeomData.MakeValid()) Geom2Json
                            from GFI_GIS_District f
                            where DistrictID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = DistictID.ToString();
            drSql["sTableName"] = "dtDistict";
            dtSql.Rows.Add(drSql);

            //Matrix
            sql = new MatrixService().sGetMatrixId(DistictID, "GFI_SYS_GroupMatrixDistrict");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables["dtDistict"].Rows.Count == 0)
                return null;
            else
                return GetDistict(ds);
        }

        District GetDistict(DataSet ds)
        {
            District district = new District();
            district.DistrictID = Convert.ToInt32(ds.Tables["dtDistict"].Rows[0]["DistrictID"]);
            district.DistrictName = ds.Tables["dtDistict"].Rows[0]["DistrictName"].ToString();
            district.iColor = Convert.ToInt32(ds.Tables["dtDistict"].Rows[0]["iColor"]);
            district.GeomData = String.IsNullOrEmpty(ds.Tables["dtDistict"].Rows[0]["GeomData"].ToString()) ? null : ds.Tables["dtDistict"].Rows[0]["GeomData"].ToString();

            //Matrix
            List<Matrix> lMatrix = new List<Matrix>();
            MatrixService ms = new MatrixService();
            foreach (DataRow dr in ds.Tables["dtMatrix"].Rows)
                lMatrix.Add(ms.AssignMatrix(dr));
            district.lMatrix = lMatrix;

            return district;
        }

        DataTable DistrictDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_GIS_District";
            dt.Columns.Add("DistrictID", typeof(int));
            dt.Columns.Add("DistrictName", typeof(String));
            dt.Columns.Add("iColor", typeof(int));
            dt.Columns.Add("GeomData", typeof(String));
            dt.Columns.Add("CountryID", typeof(int));
            dt.Columns.Add("oprType", typeof(DataRowState));

            return dt;
        }
        public BaseModel SaveDistrict(District district, String sConnStr)
        {
            BaseModel baseModel = new BaseModel();
            Boolean bResult = false;
            baseModel.data = bResult;

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            DataSet dsProcess = new DataSet();
            DataTable dt = DistrictDT();

            Boolean bInsert = false;
            if (district.oprType == DataRowState.Added)
                bInsert = true;

            //GFI_GIS_District
            DataRow dr = dt.NewRow();
            dr["DistrictID"] = district.DistrictID;
            dr["DistrictName"] = district.DistrictName;
            dr["iColor"] = district.iColor;
            dr["GeomData"] = district.GeomData != null ? district.GeomData : (object)DBNull.Value;
            dr["CountryID"] = district.CountryID != null ? district.CountryID : (object)DBNull.Value;
            dr["oprType"] = district.oprType;
            dt.Rows.Add(dr);

            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "DistrictID";
            drCtrl["KeyIsIdentity"] = "FALSE";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dt);

            #region GeomData 4326
            String KeyField = "DistrictID";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 4;
            drCtrl["TblName"] = "GeomDataField";
            drCtrl["NextIdAction"] = "SetToSP";
            drCtrl["NextIdFieldName"] = KeyField;
            drCtrl["ParentTblName"] = dt.TableName;
            drCtrl["CmdType"] = "STOREDPROC";
            String sql = "update " + dt.TableName + " set GeomData.STSrid = 4326 where " + KeyField + " = ";
            drCtrl["Command"] = sql + district.DistrictID.ToString(); //sql + "?";
            if (!bInsert)
                drCtrl["Command"] = sql + district.DistrictID.ToString();
            dtCtrl.Rows.Add(drCtrl);

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 5;
            drCtrl["TblName"] = "GeomDataField";
            drCtrl["NextIdAction"] = "SetToSP";
            drCtrl["NextIdFieldName"] = KeyField;
            drCtrl["ParentTblName"] = dt.TableName;
            drCtrl["CmdType"] = "STOREDPROC";
            sql = "update " + dt.TableName + " set GeomData = GeomData.MakeValid() where GeomData.STIsValid() = 0 and " + KeyField + " = ";
            drCtrl["Command"] = sql + district.DistrictID.ToString();
            if (!bInsert)
                drCtrl["Command"] = sql + district.DistrictID.ToString();
            dtCtrl.Rows.Add(drCtrl);
            #endregion

            //MATRIX
            if (district.lMatrix == null || district.lMatrix.Count == 0)
                return baseModel;
            foreach (Matrix m in district.lMatrix)
                m.iID = district.DistrictID;
            DataTable dtMatrix = new myConverter().ListToDataTable<Matrix>(district.lMatrix);
            if (bInsert)
                foreach (DataRow r in dtMatrix.Rows)
                    r["OprType"] = DataRowState.Added;

            dtMatrix.TableName = "GFI_SYS_GroupMatrixDistrict";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 2;
            drCtrl["TblName"] = dtMatrix.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "MID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtMatrix);

            dsProcess.Merge(dtCtrl);
            DataSet dsResults = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResults.Tables["CTRLTBL"];

            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                {
                    baseModel.dbResult = dsResults;
                    baseModel.dbResult.Tables["CTRLTBL"].TableName = "ResultCtrlTbl";
                    baseModel.dbResult.Merge(dsProcess);
                    bResult = false;
                }
                else
                    bResult = true;
            }
            else
                bResult = false;

            baseModel.data = bResult;
            return baseModel;
        }
        #endregion

        #region VCA
        VCA GetVCAByID(int VCAID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"Select VCAID, DistrictID
                                , VCAName, iColor
                                , GeomData.ToString() as GeomData
                                , dbo.Geometry2Json(f.GeomData.MakeValid()) Geom2Json
                            from GFI_GIS_VCA f
                            where VCAID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = VCAID.ToString();
            drSql["sTableName"] = "dtVCA";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables["dtVCA"].Rows.Count == 0)
                return null;
            else
                return GetVCA(ds);
        }

        VCA GetVCA(DataSet ds)
        {
            VCA VCA = new VCA();
            VCA.VCAID = Convert.ToInt32(ds.Tables["dtVCA"].Rows[0]["VCAID"]);
            VCA.DistrictID = Convert.ToInt32(ds.Tables["dtVCA"].Rows[0]["DistrictID"]);
            VCA.VCAName = ds.Tables["dtVCA"].Rows[0]["VCAName"].ToString();
            VCA.iColor = Convert.ToInt32(ds.Tables["dtVCA"].Rows[0]["iColor"]);
            VCA.GeomData = String.IsNullOrEmpty(ds.Tables["dtVCA"].Rows[0]["GeomData"].ToString()) ? null : ds.Tables["dtVCA"].Rows[0]["GeomData"].ToString();

            return VCA;
        }

        DataTable VCADT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_GIS_VCA";
            dt.Columns.Add("VCAID", typeof(int));
            dt.Columns.Add("DistrictID", typeof(int));
            dt.Columns.Add("VCAName", typeof(String));
            dt.Columns.Add("iColor", typeof(int));
            dt.Columns.Add("GeomData", typeof(String));
            dt.Columns.Add("oprType", typeof(DataRowState));

            return dt;
        }
        public BaseModel SaveVCA(VCA VCA, String sConnStr)
        {
            BaseModel baseModel = new BaseModel();
            Boolean bResult = false;
            baseModel.data = bResult;

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            DataSet dsProcess = new DataSet();
            DataTable dt = VCADT();

            //Boolean bInsert = false;
            //if (VCA.oprType == DataRowState.Added)
            //    bInsert = true;

            //GFI_GIS_VCA
            DataRow dr = dt.NewRow();
            dr["VCAID"] = VCA.VCAID;
            dr["DistrictID"] = VCA.DistrictID;
            dr["VCAName"] = VCA.VCAName;
            dr["iColor"] = VCA.iColor;
            dr["GeomData"] = VCA.GeomData != null ? VCA.GeomData : (object)DBNull.Value;
            dr["oprType"] = VCA.oprType;
            dt.Rows.Add(dr);

            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "VCAID";
            drCtrl["KeyIsIdentity"] = "FALSE";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dt);

            #region GeomData 4326
            String KetField = "VCAID";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 4;
            drCtrl["TblName"] = "GeomDataField";
            drCtrl["NextIdAction"] = "SetToSP";
            drCtrl["NextIdFieldName"] = KetField;
            drCtrl["ParentTblName"] = dt.TableName;
            drCtrl["CmdType"] = "STOREDPROC";
            String sql = "update " + dt.TableName + " set GeomData.STSrid = 4326 where " + KetField + " = ";
            drCtrl["Command"] = sql + VCA.VCAID.ToString();
            dtCtrl.Rows.Add(drCtrl);

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 5;
            drCtrl["TblName"] = "GeomDataField";
            drCtrl["NextIdAction"] = "SetToSP";
            drCtrl["NextIdFieldName"] = KetField;
            drCtrl["ParentTblName"] = dt.TableName;
            drCtrl["CmdType"] = "STOREDPROC";
            sql = "update " + dt.TableName + " set GeomData = GeomData.MakeValid() where GeomData.STIsValid() = 0 and " + KetField + " = ";
            drCtrl["Command"] = sql + VCA.VCAID.ToString();
            dtCtrl.Rows.Add(drCtrl);
            #endregion

            dsProcess.Merge(dtCtrl);
            DataSet dsResults = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResults.Tables["CTRLTBL"];

            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                {
                    baseModel.dbResult = dsResults;
                    baseModel.dbResult.Tables["CTRLTBL"].TableName = "ResultCtrlTbl";
                    baseModel.dbResult.Merge(dsProcess);
                    bResult = false;
                }
                else
                    bResult = true;
            }
            else
                bResult = false;

            baseModel.data = bResult;
            return baseModel;
        }
        #endregion

        #region RoadNetwork
        RoadNetwork GetRoadNetworkByID(int RoadID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();
            String sql = @"Select RoadID, VCAID
                                , RoadName, LengthM, WidthM, SpeedLimit, RoadClass, RoadType
                                , GeomData.ToString() as GeomData
                                , dbo.Geometry2Json(GeomData.MakeValid()) Geom2Json
                            from GFI_GIS_RoadNetwork 
                            where RoadID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = RoadID.ToString();
            drSql["sTableName"] = "dtRoadNetwork";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables["dtRoadNetwork"].Rows.Count == 0)
                return null;
            else
                return GetRoadNetwork(ds);
        }

        RoadNetwork GetRoadNetwork(DataSet ds)
        {
            RoadNetwork RoadNetwork = new RoadNetwork();
            RoadNetwork.RoadID = Convert.ToInt32(ds.Tables["dtRoadNetwork"].Rows[0]["RoadID"]);
            RoadNetwork.VCAID = Convert.ToInt32(ds.Tables["dtRoadNetwork"].Rows[0]["VCAID"]);
            RoadNetwork.RoadName = ds.Tables["dtRoadNetwork"].Rows[0]["RoadName"].ToString();
            RoadNetwork.LengthM = String.IsNullOrEmpty(ds.Tables["dtRoadNetwork"].Rows[0]["LengthM"].ToString()) ? (Double?)null : Convert.ToDouble(ds.Tables["dtRoadNetwork"].Rows[0]["LengthM"].ToString());
            RoadNetwork.WidthM = String.IsNullOrEmpty(ds.Tables["dtRoadNetwork"].Rows[0]["WidthM"].ToString()) ? (Double?)null : Convert.ToDouble(ds.Tables["dtRoadNetwork"].Rows[0]["WidthM"].ToString());
            RoadNetwork.SpeedLimit = String.IsNullOrEmpty(ds.Tables["dtRoadNetwork"].Rows[0]["SpeedLimit"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtRoadNetwork"].Rows[0]["SpeedLimit"].ToString());
            RoadNetwork.RoadClass = String.IsNullOrEmpty(ds.Tables["dtRoadNetwork"].Rows[0]["RoadClass"].ToString()) ? null : ds.Tables["dtRoadNetwork"].Rows[0]["RoadClass"].ToString();
            RoadNetwork.RoadType = String.IsNullOrEmpty(ds.Tables["dtRoadNetwork"].Rows[0]["RoadType"].ToString()) ? null : ds.Tables["dtRoadNetwork"].Rows[0]["RoadType"].ToString();
            RoadNetwork.GeomData = String.IsNullOrEmpty(ds.Tables["dtRoadNetwork"].Rows[0]["GeomData"].ToString()) ? null : ds.Tables["dtRoadNetwork"].Rows[0]["GeomData"].ToString();

            return RoadNetwork;
        }

        DataTable RoadNetworkDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_GIS_RoadNetwork";
            dt.Columns.Add("RoadID", typeof(int));
            dt.Columns.Add("VCAID", typeof(int));
            dt.Columns.Add("RoadName", typeof(String));
            dt.Columns.Add("LengthM", typeof(float));
            dt.Columns.Add("WidthM", typeof(float));
            dt.Columns.Add("SpeedLimit", typeof(int));
            dt.Columns.Add("RoadClass", typeof(String));
            dt.Columns.Add("RoadType", typeof(String));
            dt.Columns.Add("GeomData", typeof(String));
            dt.Columns.Add("oprType", typeof(DataRowState));

            return dt;
        }
        public BaseModel SaveRoadNetwork(RoadNetwork RoadNetwork, String sConnStr)
        {
            BaseModel baseModel = new BaseModel();
            Boolean bResult = false;
            baseModel.data = bResult;

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            DataSet dsProcess = new DataSet();
            DataTable dt = RoadNetworkDT();

            //GFI_GIS_RoadNetwork
            DataRow dr = dt.NewRow();
            dr["RoadID"] = RoadNetwork.RoadID;
            dr["VCAID"] = RoadNetwork.VCAID;
            dr["RoadName"] = RoadNetwork.RoadName;
            dr["LengthM"] = RoadNetwork.LengthM != null ? RoadNetwork.LengthM : (object)DBNull.Value;
            dr["WidthM"] = RoadNetwork.WidthM != null ? RoadNetwork.WidthM : (object)DBNull.Value;
            dr["SpeedLimit"] = RoadNetwork.SpeedLimit != null ? RoadNetwork.SpeedLimit : (object)DBNull.Value;
            dr["RoadClass"] = RoadNetwork.RoadClass != null ? RoadNetwork.RoadClass : (object)DBNull.Value;
            dr["RoadType"] = RoadNetwork.RoadType != null ? RoadNetwork.RoadType : (object)DBNull.Value;
            dr["GeomData"] = RoadNetwork.GeomData != null ? RoadNetwork.GeomData : (object)DBNull.Value;
            dr["oprType"] = RoadNetwork.oprType;
            dt.Rows.Add(dr);

            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "RoadID";
            drCtrl["KeyIsIdentity"] = "FALSE";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dt);

            #region GeomData 4326
            String KeyField = "RoadID";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 4;
            drCtrl["TblName"] = "GeomDataField";
            drCtrl["NextIdAction"] = "SetToSP";
            drCtrl["NextIdFieldName"] = KeyField;
            drCtrl["ParentTblName"] = dt.TableName;
            drCtrl["CmdType"] = "STOREDPROC";
            String sql = "update " + dt.TableName + " set GeomData.STSrid = 4326 where " + KeyField + " = ";
            drCtrl["Command"] = sql + RoadNetwork.RoadID;
            dtCtrl.Rows.Add(drCtrl);

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 5;
            drCtrl["TblName"] = "GeomDataField";
            drCtrl["NextIdAction"] = "SetToSP";
            drCtrl["NextIdFieldName"] = KeyField;
            drCtrl["ParentTblName"] = dt.TableName;
            drCtrl["CmdType"] = "STOREDPROC";
            sql = "update " + dt.TableName + " set GeomData = GeomData.MakeValid() where GeomData.STIsValid() = 0 and " + KeyField + " = ";
            drCtrl["Command"] = sql + RoadNetwork.RoadID;
            dtCtrl.Rows.Add(drCtrl);
            #endregion

            dsProcess.Merge(dtCtrl);
            DataSet dsResults = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResults.Tables["CTRLTBL"];

            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                {
                    baseModel.dbResult = dsResults;
                    baseModel.dbResult.Tables["CTRLTBL"].TableName = "ResultCtrlTbl";
                    baseModel.dbResult.Merge(dsProcess);
                    bResult = false;
                }
                else
                    bResult = true;
            }
            else
                bResult = false;

            baseModel.data = bResult;
            return baseModel;
        }

        public DataSet GetRoadNetworkByIds(int UID, List<TreeIDs> lids, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            List<TreeIDs> lDistrict = lids.Where(x => x.type == "District").ToList();
            List<TreeIDs> lVCA = lids.Where(x => x.type == "VCA").ToList();
            List<TreeIDs> lRoad = lids.Where(x => x.type == "Road").ToList();

            String sql = new LibData().sqlValidatedData(UID, NaveoModels.Districts, sConnStr);
            if (lDistrict.Count > 0)
            {
                sql += @"
                    CREATE TABLE #dtDistrict
                    (
	                    [RowNumber] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY CLUSTERED,
	                    [AssetID] [int]
                    )
                        ";
                foreach (TreeIDs i in lDistrict)
                    sql += "insert #dtDistrict (AssetID) values (" + i.id + ");\r\n";

                sql += "select 'dtDistrict' as TableName\r\n";
                sql += @"
                        select d.DistrictID, d.DistrictName, d.iColor, d.GeomData.ToString() as GeomData from GFI_GIS_District d 
                            inner join #dtDistrict f on d.DistrictID = f.AssetID
                            inner join #ValidatedData a on a.StrucID = f.AssetID and a.GMID > 0";
            }
            if (lVCA.Count > 0)
            {
                sql += @"
                    CREATE TABLE #dtVCA
                    (
	                    [RowNumber] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY CLUSTERED,
	                    [AssetID] [int]
                    )
                        ";
                foreach (TreeIDs i in lVCA)
                    sql += "insert #dtVCA (AssetID) values (" + i.id + ");\r\n";

                sql += "select 'dtVCA' as TableName\r\n";
                sql += @"
                        select distinct d.VCAID, d.VCAName, d.DistrictID, d.iColor, d.GeomData.ToString() as GeomData from GFI_GIS_VCA d 
                            inner join #dtVCA f on d.VCAID = f.AssetID
                            inner join #ValidatedData a on a.StrucID = f.AssetID and a.GMID > 0";
            }
            if (lRoad.Count > 0)
            {
                sql += @"
                    CREATE TABLE #dtRoad
                    (
	                    [RowNumber] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY CLUSTERED,
	                    [AssetID] [int]
                    )
                        ";
                foreach (TreeIDs i in lRoad)
                    sql += "insert #dtRoad (AssetID) values (" + i.id + ");\r\n";

                sql += "select 'dtRoad' as TableName\r\n";
                sql += @"
                        --select d.RoadID, d.VCAID, d.RoadName, d.LengthM, d.WidthM, d.SpeedLimit, d.RoadType, d.RoadClass, d.GeomData.ToString() as GeomData from GFI_GIS_RoadNetwork d 
                        --    inner join #dtRoad f on d.RoadID = f.AssetID
                        --    inner join #ValidatedData a on a.StrucID = f.AssetID and a.GMID < 0
select d.RoadID, d.VCAID, d.RoadName, sum(r.LengthM) LengthM, sum(r.WidthM) WidthM, max(r.SpeedLimit) SpeedLimit, max(r.RoadType) RoadType, max(r.RoadClass) RoadClass, geometry::UnionAggregate(r.GeomData).ToString() as GeomData 
from GFI_GIS_RoadNetwork d 
	inner join GFI_GIS_RoadNetwork r on d.RoadName = r.RoadName
    inner join #dtRoad f on d.RoadID = f.AssetID
    inner join #ValidatedData a on a.StrucID = f.AssetID and a.GMID < 0
group by d.roadID, d.RoadName, d.VCAID
";
            }

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds;
        }
        #endregion
    }
}
