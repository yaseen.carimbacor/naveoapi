using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Assets;
using NaveoOneLib.DBCon;

namespace NaveoOneLib.Services.Assets
{
    public class InsCoverTypesService
    {

        Errors er = new Errors();

        public String sGetInsCoverType()
        {
            String sqlString = @"SELECT CoverTypeId, 
                                    Description, 
                                    CreatedDate, 
                                    CreatedBy, 
                                    UpdatedDate, 
                                    UpdatedBy from GFI_AMM_InsCoverTypes order by CoverTypeId";
            return sqlString;
        }

        public DataTable GetInsCoverType(String sConnStr)
        {
            return new Connection(sConnStr).GetDataDT(sGetInsCoverType(), sConnStr);
        }

        public InsCoverType GetInsCoverTypeById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT CoverTypeId, 
                                        Description, 
                                        CreatedDate, 
                                        CreatedBy, 
                                        UpdatedDate, 
                                        UpdatedBy from GFI_AMM_InsCoverTypes
                                        WHERE CoverTypeId = ?
                                        order by CoverTypeId";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID,sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                InsCoverType retInsCoverType = new InsCoverType();
                retInsCoverType.CoverTypeId = Convert.ToInt32(dt.Rows[0]["CoverTypeId"]);
                retInsCoverType.Description = dt.Rows[0]["Description"].ToString();
                retInsCoverType.CreatedDate = (DateTime)dt.Rows[0]["CreatedDate"];
                retInsCoverType.CreatedBy = dt.Rows[0]["CreatedBy"].ToString();
                retInsCoverType.UpdatedDate = (DateTime)dt.Rows[0]["UpdatedDate"];
                retInsCoverType.UpdatedBy = dt.Rows[0]["UpdatedBy"].ToString();
                return retInsCoverType;
            }
        }

        public InsCoverType GetInsCoverTypeByDescription(String sDescription, String sConnStr)
        {
            String sqlString = @"SELECT CoverTypeId, 
                                        Description, 
                                        CreatedDate, 
                                        CreatedBy, 
                                        UpdatedDate, 
                                        UpdatedBy from GFI_AMM_InsCoverTypes
                                        WHERE Description = ?
                                        order by Description";

            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, sDescription, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                InsCoverType retInsCoverType = new InsCoverType();
                retInsCoverType.CoverTypeId = Convert.ToInt32(dt.Rows[0]["CoverTypeId"]);
                retInsCoverType.Description = dt.Rows[0]["Description"].ToString();
                retInsCoverType.CreatedDate = (DateTime)dt.Rows[0]["CreatedDate"];
                retInsCoverType.CreatedBy = dt.Rows[0]["CreatedBy"].ToString();
                retInsCoverType.UpdatedDate = (DateTime)dt.Rows[0]["UpdatedDate"];
                retInsCoverType.UpdatedBy = dt.Rows[0]["UpdatedBy"].ToString();
                return retInsCoverType;
            }
        }

        public bool SaveInsCoverType(InsCoverType uInsCoverType, DbTransaction transaction, String sConnStr)
        {
            //--- Prepare Dataset ---//

            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_InsCoverTypes";
            if (!dt.Columns.Contains("oprType"))
                dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Columns.Add("CoverTypeId", typeof(int));
            dt.Columns.Add("Description", typeof(String));
            dt.Columns.Add("CreatedDate", typeof(DateTime));
            dt.Columns.Add("CreatedBy", typeof(String));
            dt.Columns.Add("UpdatedDate", typeof(DateTime));
            dt.Columns.Add("UpdatedBy", typeof(String));
            
            DataRow dr = dt.NewRow();
            dr["oprType"] = DataRowState.Added;
            dr["CoverTypeId"] = uInsCoverType.CoverTypeId;
            dr["Description"] = uInsCoverType.Description;
            dr["CreatedDate"] = uInsCoverType.CreatedDate;
            dr["CreatedBy"] = uInsCoverType.CreatedBy;
            dr["UpdatedDate"] = uInsCoverType.UpdatedDate;
            dr["UpdatedBy"] = uInsCoverType.UpdatedBy;
            dt.Rows.Add(dr);

            //--- Prepare Control Table ---//

            Connection _connection = new Connection(sConnStr);

            DataSet dsProcess = new DataSet();
            DataTable CTRLTBL = _connection.CTRLTBL();

            dsProcess.Tables.Add(CTRLTBL);
            dsProcess.Merge(dt);

            DataRow drCtrl = CTRLTBL.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "CoverTypeId";
            drCtrl["KeyIsIdentity"] = "FALSE";
            CTRLTBL.Rows.Add(drCtrl);

            //--- Process Data ---//

            CTRLTBL = _connection.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            if (CTRLTBL != null)
            {
                DataRow[] dRow = CTRLTBL.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }

        public bool UpdateInsCoverType(InsCoverType uInsCoverType, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_InsCoverTypes";
            if (!dt.Columns.Contains("oprType"))
                dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Columns.Add("CoverTypeId", uInsCoverType.CoverTypeId.GetType());
            dt.Columns.Add("Description", uInsCoverType.Description.GetType());
            dt.Columns.Add("CreatedDate", uInsCoverType.CreatedDate.GetType());
            dt.Columns.Add("CreatedBy", uInsCoverType.CreatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uInsCoverType.UpdatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uInsCoverType.UpdatedBy.GetType());
            
            DataRow dr = dt.NewRow();
            dr["oprType"] = DataRowState.Modified;
            dr["CoverTypeId"] = uInsCoverType.CoverTypeId;
            dr["Description"] = uInsCoverType.Description;
            dr["CreatedDate"] = uInsCoverType.CreatedDate;
            dr["CreatedBy"] = uInsCoverType.CreatedBy;
            dr["UpdatedDate"] = uInsCoverType.UpdatedDate;
            dr["UpdatedBy"] = uInsCoverType.UpdatedBy;
            dt.Rows.Add(dr);

            //--- Prepare Control Table ---//

            Connection _connection = new Connection(sConnStr);

            DataSet dsProcess = new DataSet();
            DataTable CTRLTBL = _connection.CTRLTBL();

            dsProcess.Tables.Add(CTRLTBL);
            dsProcess.Merge(dt);

            DataRow drCtrl = CTRLTBL.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "CoverTypeId";
            drCtrl["KeyIsIdentity"] = "FALSE";
            CTRLTBL.Rows.Add(drCtrl);

            //--- Process Data ---//

            CTRLTBL = _connection.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            if (CTRLTBL != null)
            {
                DataRow[] dRow = CTRLTBL.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }

        public bool DeleteInsCoverType(InsCoverType uInsCoverType, String sConnStr)
        {
            //--- Prepare Dataset ---//

            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_InsCoverTypes";
            dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Columns.Add("CoverTypeId", uInsCoverType.CoverTypeId.GetType());

            DataRow dr = dt.NewRow();
            dr["oprType"] = DataRowState.Deleted;
            dr["CoverTypeId"] = uInsCoverType.CoverTypeId;

            dt.Rows.Add(dr);

            //--- Prepare Control Table ---//

            Connection _connection = new Connection(sConnStr);

            DataTable CTRLTBL = _connection.CTRLTBL();
            CTRLTBL.TableName = "CTRLTBL";

            DataSet objDataSet = new DataSet();

            objDataSet.Tables.Add(CTRLTBL);
            objDataSet.Merge(dt);

            DataRow drCtrl = CTRLTBL.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "CoverTypeId";
            drCtrl["KeyIsIdentity"] = "FALSE";
            CTRLTBL.Rows.Add(drCtrl);

            //--- Process Data ---//

            CTRLTBL = _connection.ProcessData(objDataSet, sConnStr).Tables["CTRLTBL"];

            if (CTRLTBL != null)
            {
                DataRow[] dRow = CTRLTBL.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }

    }
}
