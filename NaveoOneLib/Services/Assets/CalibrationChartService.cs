using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using System.Globalization;
using NaveoOneLib.Models.Assets;

namespace NaveoOneLib.Services.Assets
{
    public class CalibrationChartService
    {
        Errors er = new Errors();
        public DataTable GetCalibrationChart(String sConnStr)
        {
            String sqlString = @"SELECT c.IID, 
                                    c.AssetID, 
                                    c.DeviceID, 
                                    c.CalType, 
                                    c.SeuqenceNo, 
                                    c.ReadingUOM, 
                                    c.Reading, 
                                    c.ConvertedValue, 
                                    c.ConvertedUOM, 
                                    c.CreatedBy, 
                                    c.CreatedDate, 
                                    c.UpdatedBy, 
                                    c.UpdatedDate, u.Description ConvertedDesc, u1.Description ReadingDesc
                                    from GFI_FLT_CalibrationChart c
                                    inner join GFI_SYS_UOM u on c.ConvertedUOM = u.UID 
                                    inner join GFI_SYS_UOM u1 on c.ReadingUOM = u1.UID
                                  from GFI_FLT_CalibrationChart order by iID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public DataTable GetAllAssetsWithFuel(String sConnStr)
        {
            String sqlString = @"
SELECT distinct adm.AssetID                              
from GFI_FLT_CalibrationChart c
	inner join GFI_FLT_DeviceAuxilliaryMap dam on c.AuxID = dam.Uid
	inner join GFI_FLT_AssetDeviceMap adm on dam.MapID = adm.MapID
order by AssetID";
            DataTable dt = new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
            return dt;
        }

        CalibrationChart GetCalibrationChart(DataRow dr)
        {
            CalibrationChart retCalibrationChart = new CalibrationChart();
            retCalibrationChart.IID = Convert.ToInt64(dr["IID"].ToString());
            retCalibrationChart.AssetID = String.IsNullOrEmpty(dr["AssetID"].ToString())? (int?)null : Convert.ToInt32(dr["AssetID"].ToString());
            retCalibrationChart.DeviceID = dr["DeviceID"].ToString();
            retCalibrationChart.CalType = dr["CalType"].ToString();
            retCalibrationChart.SeuqenceNo = String.IsNullOrEmpty(dr["SeuqenceNo"].ToString()) ? (int?)null : Convert.ToInt32(dr["SeuqenceNo"].ToString());
            retCalibrationChart.ReadingUOM = Convert.ToInt32(dr["ReadingUOM"].ToString());
            retCalibrationChart.Reading = (float)Convert.ToDouble(dr["Reading"]);
            retCalibrationChart.ConvertedValue = (float)Convert.ToDouble(dr["ConvertedValue"]);
            retCalibrationChart.ConvertedUOM = Convert.ToInt32(dr["ConvertedUOM"].ToString());
            retCalibrationChart.CreatedDate = dr["CreatedDate"].ToString().Length == 0 ? (DateTime?)null : (DateTime)dr["CreatedDate"];
            retCalibrationChart.UpdatedDate = dr["UpdatedDate"].ToString().Length == 0 ? DateTime.Now.AddYears(-50) : (DateTime)dr["UpdatedDate"];
            retCalibrationChart.ConvertedUOMDesc = dr["ConvertedDesc"].ToString();
            retCalibrationChart.ReadingUOMDesc = dr["ReadingDesc"].ToString();

            if (!String.IsNullOrEmpty(dr["CreatedBy"].ToString()))
                retCalibrationChart.CreatedBy = Convert.ToInt32(dr["CreatedBy"].ToString());
            if (!String.IsNullOrEmpty(dr["UpdatedBy"].ToString()))
                retCalibrationChart.UpdatedBy = Convert.ToInt32(dr["UpdatedBy"].ToString());

            return retCalibrationChart;
        }

        public CalibrationChart GetCalibrationChartById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT c.IID, 
                                    c.AssetID, 
                                    c.DeviceID, 
                                    c.CalType, 
                                    c.SeuqenceNo, 
                                    c.ReadingUOM, 
                                    c.Reading, 
                                    c.ConvertedValue, 
                                    c.ConvertedUOM, 
                                    c.CreatedBy, 
                                    c.CreatedDate, 
                                    c.UpdatedBy, 
                                    c.UpdatedDate, u.Description ConvertedDesc, u1.Description ReadingDesc
                                    from GFI_FLT_CalibrationChart c
                                    inner join GFI_SYS_UOM u on c.ConvertedUOM = u.UID 
                                    inner join GFI_SYS_UOM u1 on c.ReadingUOM = u1.UID
                                    WHERE c.iID = ?
                                    order by c.iID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetCalibrationChart(dt.Rows[0]);
        }
        public CalibrationChart GetCalibrationChartByAssetId(int iAssetID, String sConnStr)
        {
            String sqlString = @"
SELECT c.IID, 
	adm.AssetID, 
	adm.DeviceID, 
	c.CalType, 
	c.SeuqenceNo, 
	c.ReadingUOM, 
	c.Reading, 
	c.ConvertedValue, 
	c.ConvertedUOM, 
	c.CreatedBy, 
	c.CreatedDate, 
	c.UpdatedBy, 
	c.UpdatedDate, u.Description ConvertedDesc, u1.Description ReadingDesc
from GFI_FLT_CalibrationChart c
	inner join GFI_FLT_DeviceAuxilliaryMap dam on c.AuxID = dam.Uid
	inner join GFI_FLT_AssetDeviceMap adm on dam.MapID = adm.MapID
	inner join GFI_SYS_UOM u on c.ConvertedUOM = u.UID 
	inner join GFI_SYS_UOM u1 on c.ReadingUOM = u1.UID
WHERE adm.AssetID = ?";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iAssetID.ToString(), sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetCalibrationChart(dt.Rows[0]);
        }

        public List<CalibrationChart> GetCalibrationChartLByAssetId(int iAssetID, String sConnStr) //Dinesh 20161201 full implemetation of calibration chart
        {
            String sqlString = @"
                                SELECT c.IID as CalID	                                
                                from GFI_FLT_CalibrationChart c
	                                inner join GFI_FLT_DeviceAuxilliaryMap dam on c.AuxID = dam.Uid
	                                inner join GFI_FLT_AssetDeviceMap adm on dam.MapID = adm.MapID
	                                inner join GFI_SYS_UOM u on c.ConvertedUOM = u.UID 
	                                inner join GFI_SYS_UOM u1 on c.ReadingUOM = u1.UID
                                WHERE adm.AssetID = ?
                                    order by ConvertedValue";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iAssetID.ToString(), sConnStr);

            List<CalibrationChart> lcalc = new List<CalibrationChart>();

            if (dt.Rows.Count == 0)
                return null;

            foreach (DataRow dr in dt.Rows)
                lcalc.Add(GetCalibrationChartById(dr["CalID"].ToString(), sConnStr));

            return lcalc;
        }

        public Boolean SaveCalibrationChart(CalibrationChart uCalibrationChart, Boolean bInsert, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_CalibrationChart";
            dt.Columns.Add("IID", uCalibrationChart.IID.GetType());
            dt.Columns.Add("AssetID", uCalibrationChart.AssetID.GetType());
            dt.Columns.Add("DeviceID", uCalibrationChart.DeviceID.GetType());
            dt.Columns.Add("CalType", uCalibrationChart.CalType.GetType());
            dt.Columns.Add("SeuqenceNo", uCalibrationChart.SeuqenceNo.GetType());
            dt.Columns.Add("ReadingUOM", uCalibrationChart.ReadingUOM.GetType());
            dt.Columns.Add("Reading", uCalibrationChart.Reading.GetType());
            dt.Columns.Add("ConvertedValue", uCalibrationChart.ConvertedValue.GetType());
            dt.Columns.Add("ConvertedUOM", uCalibrationChart.ConvertedUOM.GetType());
            dt.Columns.Add("CreatedBy", uCalibrationChart.CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", uCalibrationChart.CreatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uCalibrationChart.UpdatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uCalibrationChart.UpdatedDate.GetType());

            DataRow dr = dt.NewRow();
            dr["IID"] = uCalibrationChart.IID;
            dr["AssetID"] = uCalibrationChart.AssetID;
            dr["DeviceID"] = uCalibrationChart.DeviceID;
            dr["CalType"] = uCalibrationChart.CalType;
            dr["SeuqenceNo"] = uCalibrationChart.SeuqenceNo;
            dr["ReadingUOM"] = uCalibrationChart.ReadingUOM;
            dr["Reading"] = uCalibrationChart.Reading;
            dr["ConvertedValue"] = uCalibrationChart.ConvertedValue;
            dr["ConvertedUOM"] = uCalibrationChart.ConvertedUOM;
            dr["CreatedBy"] = uCalibrationChart.CreatedBy;
            dr["CreatedDate"] = uCalibrationChart.CreatedDate;
            dr["UpdatedBy"] = uCalibrationChart.UpdatedBy;
            dr["UpdatedDate"] = uCalibrationChart.UpdatedDate;
            dt.Rows.Add(dr);


            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "iID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uCalibrationChart.IID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public Boolean DeleteCalibrationChart(CalibrationChart uCalibrationChart, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_FLT_CalibrationChart where iID = " + uCalibrationChart.IID.ToString();
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            //DataTable dtAudit = new AuditService().LogTran(3, "CalibrationChart", uCalibrationChart.IID.ToString(), Globals.uLogin.UID, Convert.ToInt32(uCalibrationChart.IID));
            //drCtrl = dtCtrl.NewRow();
            //drCtrl["SeqNo"] = 100;
            //drCtrl["TblName"] = dtAudit.TableName;
            //drCtrl["CmdType"] = "TABLE";
            //drCtrl["KeyFieldName"] = "AuditID";
            //drCtrl["KeyIsIdentity"] = "TRUE";
            //drCtrl["NextIdAction"] = "SET";
            //drCtrl["NextIdFieldName"] = "CallerFunction";
            //drCtrl["ParentTblName"] = "GFI_FLT_CalibrationChart";
            //dtCtrl.Rows.Add(drCtrl);
            //dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
    }
}
