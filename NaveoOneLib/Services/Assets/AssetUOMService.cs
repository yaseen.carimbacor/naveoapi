using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Services.Audits;

namespace NaveoOneLib.Services.Assets
{
    class AssetUOMService
    {
        Errors er = new Errors();
        public DataTable GetAssetUOM(String sConnStr)
        {
            String sqlString = @"SELECT iID, 
AssetID, 
UOM, 
Capacity from GFI_FLT_AssetUOM order by iID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        AssetUOM GetAssetUOM(DataRow dr)
        {
            AssetUOM retAssetUOM = new AssetUOM();
            retAssetUOM.iID = Convert.ToInt32(dr["iID"]);
            retAssetUOM.AssetID = Convert.ToInt32(dr["AssetID"]);
            retAssetUOM.UOM = Convert.ToInt32(dr["UOM"]);
            retAssetUOM.Capacity = String.IsNullOrEmpty(dr["Capacity"].ToString()) ? (int?)null : Convert.ToInt32(dr["Capacity"]);
            return retAssetUOM;
        }
        public AssetUOM GetAssetUOMById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT iID, 
AssetID, 
UOM, 
Capacity from GFI_FLT_AssetUOM
WHERE iID = ?
order by iID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetAssetUOM(dt.Rows[0]);
        }
        public List<AssetUOM> lGetAssetUOMByAssetID(int AssetID, String sConnStr)
        {
            List<AssetUOM> l = new List<AssetUOM>();
            String sqlString = @"SELECT * from GFI_FLT_AssetUOM WHERE AssetID = ? order by iID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, AssetID.ToString(), sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                foreach (DataRow dr in dt.Rows)
                    l.Add(GetAssetUOM(dr));
            }
            return l;
        }

        DataTable AssetUOMDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_AssetUOM";
            dt.Columns.Add("iID", typeof(int));
            dt.Columns.Add("AssetID", typeof(int));
            dt.Columns.Add("UOM", typeof(int));
            dt.Columns.Add("Capacity", typeof(int));

            return dt;
        }
        public Boolean SaveAssetUOM(AssetUOM uAssetUOM, Boolean bInsert, String sConnStr)
        {
            DataTable dt = AssetUOMDT();

            DataRow dr = dt.NewRow();
            dr["iID"] = uAssetUOM.iID.ToString() != String.Empty ? uAssetUOM.iID : (object)DBNull.Value;
            dr["AssetID"] = uAssetUOM.AssetID.ToString() != String.Empty ? uAssetUOM.AssetID : (object)DBNull.Value;
            dr["UOM"] = uAssetUOM.UOM.ToString() != String.Empty ? uAssetUOM.UOM : (object)DBNull.Value;
            dr["Capacity"] = uAssetUOM.Capacity != null ? uAssetUOM.Capacity : (object)DBNull.Value;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "iID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uAssetUOM.iID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public Boolean DeleteAssetUOM(AssetUOM uAssetUOM, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_FLT_AssetUOM where iID = " + uAssetUOM.iID.ToString();
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            DataTable dtAudit = new AuditService().LogTran(3, "AssetUOM", uAssetUOM.iID.ToString(), Globals.uLogin.UID, uAssetUOM.iID);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "CallerFunction";
            drCtrl["ParentTblName"] = "GFI_FLT_AssetUOM";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
    }
}
