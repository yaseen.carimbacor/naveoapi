﻿using NaveoOneLib.Common;
using NaveoOneLib.Constant;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Zones;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Services.Zones;
using NaveoOneLib.WebSpecifics;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using NaveoService.Helpers;


namespace NaveoOneLib.Services.Assets
{
    public class FixedAssetService
    {
        DataTable AssetDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_GIS_FixedAsset";
            dt.Columns.Add("AssetID", typeof(int));
            dt.Columns.Add("AssetName", typeof(String));
            dt.Columns.Add("descript", typeof(String));
            dt.Columns.Add("AssetCode", typeof(String));
            dt.Columns.Add("RoadID", typeof(int));
            dt.Columns.Add("LocalityID", typeof(int));
            dt.Columns.Add("VillageID", typeof(int));
            dt.Columns.Add("AssetType", typeof(int));
            dt.Columns.Add("AssetCategory", typeof(int));
            dt.Columns.Add("AssetFamily", typeof(String));
            dt.Columns.Add("Owners", typeof(String));
            dt.Columns.Add("Height", typeof(Decimal));
            dt.Columns.Add("Wattage", typeof(Decimal));
            dt.Columns.Add("Notes", typeof(String));
            dt.Columns.Add("pictureName", typeof(String));
            dt.Columns.Add("dtCreated", typeof(DateTime));
            dt.Columns.Add("MaintLink", typeof(String));
            dt.Columns.Add("SurfaceArea", typeof(String));
            dt.Columns.Add("sValue", typeof(String));
            dt.Columns.Add("Price_m2", typeof(Decimal));
            dt.Columns.Add("ExpDate", typeof(DateTime));
            dt.Columns.Add("MarketValue", typeof(int));
            dt.Columns.Add("TitleDeed", typeof(String));
            dt.Columns.Add("distance", typeof(Decimal));
            dt.Columns.Add("distanceUnit", typeof(String));
            dt.Columns.Add("numberOfP", typeof(int));
            dt.Columns.Add("GeomData", typeof(String));
            dt.Columns.Add("Pin", typeof(String));
            dt.Columns.Add("oprType", typeof(DataRowState));

            return dt;
        }

        FixedAsset GetFixedAsset(DataSet ds)
        {
            FixedAsset retAsset = new FixedAsset();

            //FixedAsset
            retAsset.AssetID = Convert.ToInt32(ds.Tables["dtGISAsset"].Rows[0]["AssetID"]);
            retAsset.AssetCode = ds.Tables["dtGISAsset"].Rows[0]["AssetCode"].ToString();
            retAsset.dtCreated = (DateTime)ds.Tables["dtGISAsset"].Rows[0]["dtCreated"];

            retAsset.AssetName = String.IsNullOrEmpty(ds.Tables["dtGISAsset"].Rows[0]["AssetName"].ToString()) ? null : ds.Tables["dtGISAsset"].Rows[0]["AssetName"].ToString();
            retAsset.Descript = String.IsNullOrEmpty(ds.Tables["dtGISAsset"].Rows[0]["Descript"].ToString()) ? null : ds.Tables["dtGISAsset"].Rows[0]["Descript"].ToString();
            retAsset.RoadID = String.IsNullOrEmpty(ds.Tables["dtGISAsset"].Rows[0]["RoadID"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtGISAsset"].Rows[0]["RoadID"]);
            retAsset.LocalityID = String.IsNullOrEmpty(ds.Tables["dtGISAsset"].Rows[0]["LocalityID"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtGISAsset"].Rows[0]["LocalityID"]);
            retAsset.VillageID = String.IsNullOrEmpty(ds.Tables["dtGISAsset"].Rows[0]["VillageID"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtGISAsset"].Rows[0]["VillageID"]);
            retAsset.AssetCategory = String.IsNullOrEmpty(ds.Tables["dtGISAsset"].Rows[0]["AssetCategory"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtGISAsset"].Rows[0]["AssetCategory"].ToString());
            retAsset.AssetType = Convert.ToInt32(ds.Tables["dtGISAsset"].Rows[0]["AssetType"].ToString());
            retAsset.AssetFamily = String.IsNullOrEmpty(ds.Tables["dtGISAsset"].Rows[0]["AssetFamily"].ToString()) ? null : ds.Tables["dtGISAsset"].Rows[0]["AssetFamily"].ToString();
            retAsset.Owners = String.IsNullOrEmpty(ds.Tables["dtGISAsset"].Rows[0]["Owners"].ToString()) ? null : ds.Tables["dtGISAsset"].Rows[0]["Owners"].ToString();
            retAsset.Height = String.IsNullOrEmpty(ds.Tables["dtGISAsset"].Rows[0]["Height"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtGISAsset"].Rows[0]["Height"]);
            retAsset.Wattage = String.IsNullOrEmpty(ds.Tables["dtGISAsset"].Rows[0]["Wattage"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtGISAsset"].Rows[0]["Wattage"]);
            retAsset.Notes = String.IsNullOrEmpty(ds.Tables["dtGISAsset"].Rows[0]["Notes"].ToString()) ? null : ds.Tables["dtGISAsset"].Rows[0]["Notes"].ToString();
            retAsset.PictureName = String.IsNullOrEmpty(ds.Tables["dtGISAsset"].Rows[0]["PictureName"].ToString()) ? null : ds.Tables["dtGISAsset"].Rows[0]["PictureName"].ToString();
            retAsset.MaintLink = String.IsNullOrEmpty(ds.Tables["dtGISAsset"].Rows[0]["MaintLink"].ToString()) ? null : ds.Tables["dtGISAsset"].Rows[0]["MaintLink"].ToString();
            retAsset.SurfaceArea = String.IsNullOrEmpty(ds.Tables["dtGISAsset"].Rows[0]["SurfaceArea"].ToString()) ? (Double?)null : Convert.ToDouble(ds.Tables["dtGISAsset"].Rows[0]["SurfaceArea"]);
            retAsset.sValue = String.IsNullOrEmpty(ds.Tables["dtGISAsset"].Rows[0]["sValue"].ToString()) ? null : ds.Tables["dtGISAsset"].Rows[0]["sValue"].ToString();
            retAsset.Price_m2 = String.IsNullOrEmpty(ds.Tables["dtGISAsset"].Rows[0]["RoadID"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtGISAsset"].Rows[0]["RoadID"]);
            retAsset.ExpDate = String.IsNullOrEmpty(ds.Tables["dtGISAsset"].Rows[0]["ExpDate"].ToString()) ? (DateTime?)null : Convert.ToDateTime(ds.Tables["dtGISAsset"].Rows[0]["ExpDate"]);
            retAsset.MarketValue = String.IsNullOrEmpty(ds.Tables["dtGISAsset"].Rows[0]["MarketValue"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtGISAsset"].Rows[0]["MarketValue"]);
            retAsset.TitleDeed = String.IsNullOrEmpty(ds.Tables["dtGISAsset"].Rows[0]["TitleDeed"].ToString()) ? null : ds.Tables["dtGISAsset"].Rows[0]["TitleDeed"].ToString();
            retAsset.Distance = String.IsNullOrEmpty(ds.Tables["dtGISAsset"].Rows[0]["Distance"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtGISAsset"].Rows[0]["Distance"]);
            retAsset.DistanceUnit = String.IsNullOrEmpty(ds.Tables["dtGISAsset"].Rows[0]["DistanceUnit"].ToString()) ? null : ds.Tables["dtGISAsset"].Rows[0]["DistanceUnit"].ToString();
            retAsset.NumberOfP = String.IsNullOrEmpty(ds.Tables["dtGISAsset"].Rows[0]["NumberOfP"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtGISAsset"].Rows[0]["NumberOfP"]);
            retAsset.Pin = String.IsNullOrEmpty(ds.Tables["dtGISAsset"].Rows[0]["Pin"].ToString()) ? null : ds.Tables["dtGISAsset"].Rows[0]["Pin"].ToString();
            retAsset.GeomData = String.IsNullOrEmpty(ds.Tables["dtGISAsset"].Rows[0]["GeomData"].ToString()) ? null : ds.Tables["dtGISAsset"].Rows[0]["GeomData"].ToString();
            if (ds.Tables["dtGISAsset"].Columns.Contains("Geom2Json"))
                retAsset.Geom2Json = String.IsNullOrEmpty(ds.Tables["dtGISAsset"].Rows[0]["Geom2Json"].ToString()) ? null : ds.Tables["dtGISAsset"].Rows[0]["Geom2Json"].ToString();

            //Matrix
            List<Matrix> lMatrix = new List<Matrix>();
            MatrixService ms = new MatrixService();
            foreach (DataRow dr in ds.Tables["dtMatrix"].Rows)
                lMatrix.Add(ms.AssignMatrix(dr));
            retAsset.lMatrix = lMatrix;

            //AssetType
            foreach (DataRow dr in ds.Tables["dtAssetType"].Rows)
            {
                GISAssetType gISAssetType = new GISAssetType();
                gISAssetType.iID = Convert.ToInt32(dr["iID"]);
                gISAssetType.AssetType = dr["AssetType"].ToString();
                retAsset.gisAssetType = gISAssetType;
            }

            //AssetCategory
            foreach (DataRow dr in ds.Tables["dtAssetCategory"].Rows)
            {
                GISAssetCategory cat = new GISAssetCategory();
                cat.iID = Convert.ToInt32(dr["iID"]);
                cat.AssetCategory = dr["AssetCategory"].ToString();
                retAsset.gisAssetCategory = cat;
            }

            return retAsset;
        }

        #region AssetType
        GISAssetType GetGISAssetType(DataTable dt)
        {
            GISAssetType g = new Models.Assets.GISAssetType();
            g.iID = Convert.ToInt32(dt.Rows[0]["iID"]);
            g.AssetType = dt.Rows[0]["AssetType"].ToString();

            return g;
        }
        GISAssetType GetGISAssetTypeByType(String sType, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = "select * from GFI_GIS_AssetType where AssetType = ?";
            drSql["sSQL"] = sql;
            drSql["sParam"] = sType;
            drSql["sTableName"] = "dtGISType";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables["dtGISType"].Rows.Count == 0)
                return null;
            else
                return GetGISAssetType(ds.Tables[0]);
        }
        Boolean SaveGISAssetType(GISAssetType at, Boolean bInsert, String sConnStr)
        {
            if (bInsert)
                at.oprType = DataRowState.Added;

            Boolean bResult = false;

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            DataSet dsProcess = new DataSet();

            DataTable dt = new DataTable();
            dt.TableName = "GFI_GIS_AssetType";
            dt.Columns.Add("iID", typeof(int));
            dt.Columns.Add("AssetType", typeof(String));
            dt.Columns.Add("oprType", typeof(DataRowState));

            DataRow dr = dt.NewRow();
            dr["iID"] = at.iID;
            dr["AssetType"] = at.AssetType;
            dr["oprType"] = at.oprType;

            dt.Rows.Add(dr);

            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "iID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = at.iID;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dt);

            dsProcess.Merge(dtCtrl);
            DataSet dsResults = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResults.Tables["CTRLTBL"];

            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        #endregion

        #region AssetCategory
        GISAssetCategory GetGISAssetCategory(DataRow dr)
        {
            GISAssetCategory g = new Models.Assets.GISAssetCategory();
            g.iID = Convert.ToInt32(dr["iID"]);
            g.AssetTypeID = Convert.ToInt32(dr["AssetTypeID"]);
            g.AssetCategory = dr["AssetCategory"].ToString();

            return g;
        }
        GISAssetCategory GetGISAssetCategory(String sCategory, String sType, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = "select c.* from GFI_GIS_AssetCategory c inner join GFI_GIS_AssetType t on c.AssetTypeID = t.iID where t.AssetType = '" + sType + "' and AssetCategory = ?";
            drSql["sSQL"] = sql;
            drSql["sParam"] = sCategory;
            drSql["sTableName"] = "dtGISCategory";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables["dtGISCategory"].Rows.Count == 0)
                return null;
            else
                return GetGISAssetCategory(ds.Tables[0].Rows[0]);
        }
        public Boolean SaveGISAssetCategory(GISAssetCategory ac, Boolean bInsert, String sConnStr)
        {
            if (bInsert)
                ac.oprType = DataRowState.Added;

            Boolean bResult = false;

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            DataSet dsProcess = new DataSet();

            DataTable dt = new DataTable();
            dt.TableName = "GFI_GIS_AssetCategory";
            dt.Columns.Add("iID", typeof(int));
            dt.Columns.Add("AssetTypeID", typeof(int));
            dt.Columns.Add("AssetCategory", typeof(String));
            dt.Columns.Add("oprType", typeof(DataRowState));

            DataRow dr = dt.NewRow();
            dr["iID"] = ac.iID;
            dr["AssetTypeID"] = ac.AssetTypeID;
            dr["AssetCategory"] = ac.AssetCategory;
            dr["oprType"] = ac.oprType;

            dt.Rows.Add(dr);

            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "iID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = ac.iID;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dt);

            dsProcess.Merge(dtCtrl);
            DataSet dsResults = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResults.Tables["CTRLTBL"];

            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        List<GISAssetCategory> GetGISAssetCategoryByType(String assetType, String sConnStr)
        {
            List<GISAssetCategory> l = new List<GISAssetCategory>();
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = "select c.* from GFI_GIS_AssetCategory c inner join GFI_GIS_AssetType t on c.AssetTypeID = t.iID where t.AssetType = ?";
            drSql["sSQL"] = sql;
            drSql["sParam"] = assetType;
            drSql["sTableName"] = "dtGISCategory";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            foreach (DataRow dr in ds.Tables["dtGISCategory"].Rows)
                l.Add(GetGISAssetCategory(dr));
            return l;
        }
        #endregion

        public String sGetFixedAsset(List<Matrix> lMatrix, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            return sGetFixedAsset(iParentIDs, sConnStr);
        }

        //Reza. To use UID when web migration complete
        public String sGetFixedAsset(List<int> iParentIDs, String sConnStr)
        {
            String sql = @"	SELECT Distinct AssetID 
								    , AssetCode, AssetName
									, m.GMDescription,m.GMID,m.ParentGMID, at.AssetType, ac.AssetCategory
                                from GFI_GIS_FixedAsset fa
	                                inner join GFI_SYS_GroupMatrixFixedAsset mFA on fa.AssetID = mFA.iID
									inner join GFI_SYS_GroupMatrix  m on m.GMID = mfa.GMID
                                    inner join GFI_GIS_AssetType at on fa.AssetType = at.iID
                                    inner join GFI_GIS_AssetCategory ac on ac.iID = fa.AssetCategory
                                where m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + ") order by AssetID";

            return sql;
        }

        //    Group -Sub Group- Asset Name xxx Pole- Sodium- MD105200

        public DataTable GetFixedAsset(List<Matrix> lMatrix, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            return GetFixedAsset(iParentIDs, sConnStr);
        }
        public DataTable GetFixedAsset(List<int> iParentIDs, String sConnStr)
        {
            DataTable dt = new Connection(sConnStr).GetDataDT(sGetFixedAsset(iParentIDs, sConnStr), sConnStr);
            return dt;
        }

        public FixedAsset GetFixedAssetById(Guid sUserToken, int AssetID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            //FixedAsset
            String sql = new LibData().sqlValidatedData(sUserToken, NaveoModels.FixedAssets, sConnStr);

            sql += @"Select f.AssetID
                        , f.AssetName
                        , f.descript
                        , f.AssetCode
                        , f.RoadID
                        , f.LocalityID
                        , f.VillageID
                        , f.AssetType
                        , f.AssetCategory
                        , f.AssetFamily
                        , f.Owners
                        , f.Height
                        , f.Wattage
                        , f.Notes
                        , f.pictureName
                        , f.dtCreated 
                        , f.MaintLink
                        , f.SurfaceArea
                        , f.sValue
                        , f.Price_m2
                        , f.ExpDate
                        , f.MarketValue
                        , f.TitleDeed
                        , f.distance
                        , f.distanceUnit
                        , f.numberOfP
                        , f.Pin
                        , f.GeomData.ToString() as GeomData
                       --, dbo.Geometry2Json(f.GeomData) Geom2Json
                    from GFI_GIS_FixedAsset f
                        inner join #ValidatedData a on a.StrucID = f.AssetID
                    where AssetID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = AssetID.ToString();
            drSql["sTableName"] = "dtGISAsset";
            dtSql.Rows.Add(drSql);

            //AssetType
            sql = @"select assetType.iID, assetType.AssetType, fixedAsset.AssetID 
                    from GFI_GIS_FixedAsset fixedAsset
                        inner join GFI_GIS_AssetType assetType on fixedAsset.AssetType = assetType.iID
                    where fixedAsset.AssetID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = AssetID.ToString();
            drSql["sTableName"] = "dtAssetType";
            dtSql.Rows.Add(drSql);

            //AssetCategory
            sql = @"select ac.iID, ac.AssetCategory, fixedAsset.AssetID 
                    from GFI_GIS_FixedAsset fixedAsset
                        inner join GFI_GIS_AssetCategory ac on fixedAsset.AssetCategory = ac.iID
                    where fixedAsset.AssetID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = AssetID.ToString();
            drSql["sTableName"] = "dtAssetCategory";
            dtSql.Rows.Add(drSql);

            //Matrix
            sql = new MatrixService().sGetMatrixId(AssetID, "GFI_SYS_GroupMatrixFixedAsset");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables["dtGISAsset"].Rows.Count == 0)
                return null;
            else
                return GetFixedAsset(ds);
        }
        public FixedAsset GetFixedAssetByAssetCode(Guid sUserToken, String AssetCode, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();
            String sql = "select AssetID from GFI_GIS_FixedAsset where AssetCode = ?";
            drSql["sSQL"] = sql;
            drSql["sParam"] = AssetCode;
            drSql["sTableName"] = "dtGISAsset";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables[0].Rows.Count == 0)
                return null;

            int AssetID = -999;
            if (ds.Tables[0].Rows.Count > 0)
                if (!int.TryParse(ds.Tables[0].Rows[0][0].ToString(), out AssetID))
                    return null;

            dtSql = c.dtSql();
            ds.Clear();
            //FixedAsset
            sql = new LibData().sqlValidatedData(sUserToken, NaveoModels.FixedAssets, sConnStr);
            sql += @"Select f.AssetID
                        , f.AssetName
                        , f.descript
                        , f.AssetCode
                        , f.RoadID
                        , f.LocalityID
                        , f.VillageID
                        , f.AssetType
                        , f.AssetCategory
                        , f.AssetFamily
                        , f.Owners
                        , f.Height
                        , f.Wattage
                        , f.Notes
                        , f.pictureName
                        , f.dtCreated 
                        , f.MaintLink
                        , f.SurfaceArea
                        , f.sValue
                        , f.Price_m2
                        , f.ExpDate
                        , f.MarketValue
                        , f.TitleDeed
                        , f.distance
                        , f.distanceUnit
                        , f.numberOfP
                        , f.Pin
                        , f.GeomData.ToString() as GeomData
                    from GFI_GIS_FixedAsset f
                        inner join #ValidatedData a on a.StrucID = f.AssetID
                    where AssetID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = AssetID.ToString();
            drSql["sTableName"] = "dtGISAsset";
            dtSql.Rows.Add(drSql);

            //AssetType
            sql = @"select assetType.iID, assetType.AssetType, fixedAsset.AssetID 
                    from GFI_GIS_FixedAsset fixedAsset
                        inner join GFI_GIS_AssetType assetType on fixedAsset.AssetType = assetType.iID
                    where fixedAsset.AssetID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = AssetID.ToString();
            drSql["sTableName"] = "dtAssetType";
            dtSql.Rows.Add(drSql);

            //AssetCategory
            sql = @"select ac.iID, ac.AssetCategory, fixedAsset.AssetID 
                    from GFI_GIS_FixedAsset fixedAsset
                        inner join GFI_GIS_AssetCategory ac on fixedAsset.AssetType = ac.iID
                    where fixedAsset.AssetID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = AssetID.ToString();
            drSql["sTableName"] = "dtAssetCategory";
            dtSql.Rows.Add(drSql);

            //Matrix
            sql = new MatrixService().sGetMatrixId(AssetID, "GFI_SYS_GroupMatrixFixedAsset");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);

            ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables["dtGISAsset"].Rows.Count == 0)
                return null;
            else
                return GetFixedAsset(ds);
        }
        public BaseModel SaveFixedAsset(NaveoOneLib.Models.Assets.FixedAsset fixedAsset, int uLogin, Boolean bInsert, String sConnStr)
        {
            BaseModel baseModel = new BaseModel();
            Boolean bResult = false;
            baseModel.data = bResult;

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            DataSet dsProcess = new DataSet();
            DataTable dt = AssetDT();

            if (bInsert)
            {
                fixedAsset.oprType = DataRowState.Added;
                fixedAsset.dtCreated = DateTime.UtcNow;
            }
            if (fixedAsset.AssetCategory.HasValue)
                if (fixedAsset.AssetCategory.Value > NaveoEntity.FixedAssetGroup)
                    fixedAsset.AssetCategory = fixedAsset.AssetCategory - NaveoEntity.FixedAssetGroup;

            //GFI_GIS_FixedAsset
            DataRow dr = dt.NewRow();
            dr["AssetID"] = fixedAsset.AssetID;
            dr["AssetCode"] = fixedAsset.AssetCode;
            dr["dtCreated"] = Convert.ToDateTime(fixedAsset.dtCreated);
            dr["AssetType"] = fixedAsset.AssetType;
            dr["AssetName"] = fixedAsset.AssetName != null ? fixedAsset.AssetName : (object)DBNull.Value;
            dr["descript"] = fixedAsset.Descript != null ? fixedAsset.Descript : (object)DBNull.Value;
            dr["RoadID"] = fixedAsset.RoadID != null ? fixedAsset.RoadID : (object)DBNull.Value;
            dr["LocalityID"] = fixedAsset.LocalityID != null ? fixedAsset.LocalityID : (object)DBNull.Value;
            dr["VillageID"] = fixedAsset.VillageID != null ? fixedAsset.VillageID : (object)DBNull.Value;
            dr["AssetCategory"] = fixedAsset.AssetCategory != null ? fixedAsset.AssetCategory : (object)DBNull.Value;
            dr["AssetFamily"] = fixedAsset.AssetFamily != null ? fixedAsset.AssetFamily : (object)DBNull.Value;
            dr["Owners"] = fixedAsset.Owners != null ? fixedAsset.Owners : (object)DBNull.Value;
            dr["Height"] = fixedAsset.Height != null ? fixedAsset.Height : (object)DBNull.Value;
            dr["Wattage"] = fixedAsset.Wattage != null ? fixedAsset.Wattage : (object)DBNull.Value;
            dr["Notes"] = fixedAsset.Notes != null ? fixedAsset.Notes : (object)DBNull.Value;
            dr["MaintLink"] = fixedAsset.MaintLink != null ? fixedAsset.MaintLink : (object)DBNull.Value;
            dr["SurfaceArea"] = fixedAsset.SurfaceArea != null ? fixedAsset.SurfaceArea : (object)DBNull.Value;
            dr["sValue"] = fixedAsset.sValue != null ? fixedAsset.sValue : (object)DBNull.Value;
            dr["Price_m2"] = fixedAsset.Price_m2 != null ? fixedAsset.Price_m2 : (object)DBNull.Value;
            dr["ExpDate"] = fixedAsset.ExpDate != null ? fixedAsset.ExpDate : (object)DBNull.Value;
            dr["MarketValue"] = fixedAsset.MarketValue != null ? fixedAsset.MarketValue : (object)DBNull.Value;
            dr["TitleDeed"] = fixedAsset.TitleDeed != null ? fixedAsset.TitleDeed : (object)DBNull.Value;
            dr["Distance"] = fixedAsset.Distance != null ? fixedAsset.Distance : (object)DBNull.Value;
            dr["DistanceUnit"] = fixedAsset.DistanceUnit != null ? fixedAsset.DistanceUnit : (object)DBNull.Value;
            dr["NumberOfP"] = fixedAsset.NumberOfP != null ? fixedAsset.NumberOfP : (object)DBNull.Value;
            dr["PictureName"] = fixedAsset.PictureName != null ? fixedAsset.PictureName : (object)DBNull.Value;
            dr["Pin"] = fixedAsset.Pin != null ? fixedAsset.Pin : (object)DBNull.Value;
            dr["GeomData"] = fixedAsset.GeomData != null ? fixedAsset.GeomData : (object)DBNull.Value;
            dr["oprType"] = fixedAsset.oprType;
            dt.Rows.Add(dr);

            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AssetID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = fixedAsset.AssetID;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dt);

            #region GeomData 4326
            String KeyField = "AssetID";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 4;
            drCtrl["TblName"] = "GeomDataField";
            drCtrl["NextIdAction"] = "SetToSP";
            drCtrl["NextIdFieldName"] = KeyField;
            drCtrl["ParentTblName"] = dt.TableName;
            drCtrl["CmdType"] = "STOREDPROC";
            String sql = "update " + dt.TableName + " set GeomData.STSrid = 4326 where " + KeyField + " = ";
            drCtrl["Command"] = sql + "?";
            if (!bInsert)
                drCtrl["Command"] = sql + fixedAsset.AssetID.ToString();
            dtCtrl.Rows.Add(drCtrl);

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 5;
            drCtrl["TblName"] = "GeomDataField";
            drCtrl["NextIdAction"] = "SetToSP";
            drCtrl["NextIdFieldName"] = KeyField;
            drCtrl["ParentTblName"] = dt.TableName;
            drCtrl["CmdType"] = "STOREDPROC";
            sql = "update " + dt.TableName + " set GeomData = GeomData.MakeValid() where GeomData.STIsValid() = 0 and " + KeyField + " = ";
            drCtrl["Command"] = sql + "?";
            if (!bInsert)
                drCtrl["Command"] = sql + fixedAsset.AssetID.ToString();
            dtCtrl.Rows.Add(drCtrl);
            #endregion

            //MATRIX
            if (fixedAsset.lMatrix == null || fixedAsset.lMatrix.Count == 0)
            {
                List<Matrix> l = new List<Matrix>();
                l.Add(new NaveoOneLib.Services.GMatrix.MatrixService().GetMatrixByUserID(uLogin, sConnStr)[0]);
                fixedAsset.lMatrix = l;
            }
            DataTable dtMatrix = new myConverter().ListToDataTable<Matrix>(fixedAsset.lMatrix);
            if (bInsert)
                foreach (DataRow r in dtMatrix.Rows)
                    r["OprType"] = DataRowState.Added;

            dtMatrix.TableName = "GFI_SYS_GroupMatrixFixedAsset";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 2;
            drCtrl["TblName"] = dtMatrix.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "MID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "iID";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtMatrix);

            dsProcess.Merge(dtCtrl);
            DataSet dsResults = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResults.Tables["CTRLTBL"];

            string AssetIDMaint = string.Empty;

            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                {
                    baseModel.dbResult = dsResults;
                    baseModel.dbResult.Tables["CTRLTBL"].TableName = "ResultCtrlTbl";
                    baseModel.dbResult.Merge(dsProcess);
                    bResult = false;
                }
                else
                {
                    AssetIDMaint = dtCtrl.Select("SeqNo = 1 and TblName = 'GFI_GIS_FixedAsset'").FirstOrDefault()["NextIDValue"].ToString();
                    bResult = true;
                }
            }
            else
                bResult = false;

            if (bResult)
            {
                #region Insert in maintenance
                if (bInsert)
                {
                    string AssetTyp = string.Empty;
                    string AssetCat = string.Empty;
                    try
                    {
                        DataTable dtSql = c.dtSql();
                        DataRow drSql = dtSql.NewRow();

                        String sqlMaint = "select 'AssetCategorys' as TableName\r\n";
                        sqlMaint += "select AssetCategory from GFI_GIS_AssetCategory where iID = '" + fixedAsset.AssetCategory + "'\r\n";
                        sqlMaint += "select 'AssetTypes' as TableName\r\n";
                        sqlMaint += "select AssetType from GFI_GIS_AssetType where iID = '" + fixedAsset.AssetType + "'";
                        drSql = dtSql.NewRow();
                        drSql["sSQL"] = sqlMaint;
                        drSql["sTableName"] = "MultipleDTfromQuery";
                        dtSql.Rows.Add(drSql);

                        DataSet ds = c.GetDataDS(dtSql, sConnStr);

                        DataTable dtAssetCategory = ds.Tables["AssetCategorys"];

                        if (dtAssetCategory.Rows.Count > 0)
                        {
                            DataRow row = dtAssetCategory.Rows[0];

                            AssetTyp = row["AssetCategory"].ToString();
                        }

                        DataTable dtAssetType = ds.Tables["AssetTypes"];

                        if (dtAssetType.Rows.Count > 0)
                        {
                            DataRow row = dtAssetType.Rows[0];

                            AssetCat = row["AssetType"].ToString();
                        }

                        NaveoService.Services.MaintAssetServices maintAssetService = new NaveoService.Services.MaintAssetServices();
                        NaveoService.Services.MaintAssetServices.MaintAsset maintAsset = new NaveoService.Services.MaintAssetServices.MaintAsset
                        {
                            AssetID = int.Parse(AssetIDMaint),
                            AssetCode = AssetTyp + "-" + AssetCat + "-" + fixedAsset.AssetCode,
                            //fixedAsset.AssetCode,
                            AssetName = fixedAsset.lMatrix[0].GMDesc,
                            Descript = fixedAsset.Descript,
                            AssetType = fixedAsset.AssetType,
                            AssetCategory = fixedAsset.AssetCategory,

                            Type = "FixedAsset"
                        };
                        maintAssetService.SaveMaintAsset(maintAsset, bInsert, "MaintDB");
                    }
                    catch
                    {
                    }
                }
                #endregion

                if (fixedAsset.PicNames != null)
                {
                    String[] images = fixedAsset.PicNames[0].Split('`');
                    foreach (String pic in images)
                    {
                        String[] myPic = pic.Split(',');
                        if (myPic.Length == 2)
                        {
                            string picFormat = myPic[0];
                            if (picFormat == "data:image/jpeg;base64")
                            {
                                String imageName = Guid.NewGuid().ToString() + ".jpg";
                                string base64 = myPic[1];
                                byte[] bytes = Convert.FromBase64String(base64);
                                MemoryStream ms = new MemoryStream(bytes);
                                Image image = Image.FromStream(ms);
                                image.Save(Globals.DocsPath("FixedAssets\\" + sConnStr + "\\" + fixedAsset.gisAssetType.AssetType + "\\" + fixedAsset.AssetCode) + imageName, ImageFormat.Jpeg);
                            }
                        }
                    }
                }
            }

            baseModel.data = bResult;
            return baseModel;
        }
        public BaseModel SaveFixedAssetsFromShp(Guid sUserToken, String shpPath, int iMatrix, String sConnStr)
        {
            List<String> lResult = new List<string>();
            int iCntNew = 0;
            int iCntUpdated = 0;

            int UID = new NaveoOneLib.Services.Users.UserService().GetUserID(sUserToken, sConnStr);

            Matrix m = new Matrix();
            m.GMID = iMatrix;
            m.oprType = DataRowState.Added;
            List<Matrix> lMatrix = new List<Matrix>();
            lMatrix.Add(m);

            BaseModel bm = new BaseModel();
            String sOriginalPicPath = Globals.DocsPath("Tmp\\OriginalPics");

            try
            {
                List<String> lfeatureIDs = NaveoOneLib.Maps.NaveoWebMapsCall.GetAllFeatureIDs(shpPath);
                foreach (String id in lfeatureIDs)
                {
                    try
                    {
                        List<KeyValuePair<string, string>> lFeatures = NaveoOneLib.Maps.NaveoWebMapsCall.GetAllFeaturesByID(shpPath, id);
                        FixedAsset fixedAsset = new Models.Assets.FixedAsset();

                        fixedAsset.AssetCode = lFeatures.FirstOrDefault(x => x.Key == "AssetCode").Value;
                        if (string.IsNullOrEmpty(fixedAsset.AssetCode))
                        {
                            lResult.Add("NoAssetCode. AssetID : " + lFeatures.FirstOrDefault(x => x.Key == "AssetID").Value);
                            continue;
                        }

                        if (lFeatures.FirstOrDefault(x => x.Key == "dtCreated").Value == null)
                            fixedAsset.dtCreated = DateTime.Now;
                        else
                        {
                            DateTime dtCreated = DateTime.Now;
                            if (DateTime.TryParse(lFeatures.FirstOrDefault(x => x.Key == "dtCreated").Value, out dtCreated))
                                fixedAsset.dtCreated = dtCreated;
                            else
                                fixedAsset.dtCreated = DateTime.Now;
                        }

                        fixedAsset.AssetFamily = lFeatures.FirstOrDefault(x => x.Key == "AssetFamil").Value.ToString() == string.Empty ? null : lFeatures.FirstOrDefault(x => x.Key == "AssetFamil").Value;
                        fixedAsset.AssetName = lFeatures.FirstOrDefault(x => x.Key == "AssetName").Value;
                        fixedAsset.Descript = lFeatures.FirstOrDefault(x => x.Key == "descript").Value;
                        fixedAsset.Owners = lFeatures.FirstOrDefault(x => x.Key == "Owners").Value.ToString() == string.Empty ? null : lFeatures.FirstOrDefault(x => x.Key == "Owners").Value;
                        fixedAsset.Notes = lFeatures.FirstOrDefault(x => x.Key == "Notes").Value.ToString() == string.Empty ? null : lFeatures.FirstOrDefault(x => x.Key == "Notes").Value;

                        fixedAsset.sValue = lFeatures.FirstOrDefault(x => x.Key == "sValue").Value.ToString() == string.Empty ? null : lFeatures.FirstOrDefault(x => x.Key == "sValue").Value;
                        fixedAsset.TitleDeed = lFeatures.FirstOrDefault(x => x.Key == "TitleDeed").Value.ToString() == string.Empty ? null : lFeatures.FirstOrDefault(x => x.Key.ToLower() == "TitleDeed").Value;
                        fixedAsset.DistanceUnit = lFeatures.FirstOrDefault(x => x.Key == "distanceUn").Value.ToString() == string.Empty ? null : lFeatures.FirstOrDefault(x => x.Key == "distanceUn").Value;
                        fixedAsset.Height = lFeatures.FirstOrDefault(x => x.Key == "Height").Value.ToString().Replace("m", String.Empty).Replace("NA", String.Empty) == string.Empty ? (double?)null : Convert.ToDouble(lFeatures.FirstOrDefault(x => x.Key == "Height").Value.Replace("m", String.Empty).Replace("NA", String.Empty));
                        fixedAsset.Wattage = lFeatures.FirstOrDefault(x => x.Key == "Wattage").Value.ToString() == string.Empty ? (double?)null : Convert.ToDouble(lFeatures.FirstOrDefault(x => x.Key == "Wattage").Value);
                        fixedAsset.Price_m2 = lFeatures.FirstOrDefault(x => x.Key == "Price_m2").Value.ToString() == string.Empty ? (double?)null : Convert.ToDouble(lFeatures.FirstOrDefault(x => x.Key == "Price_m2").Value);
                        fixedAsset.SurfaceArea = lFeatures.FirstOrDefault(x => x.Key == "SurfaceAre").Value.ToString() == string.Empty ? (double?)null : Convert.ToDouble(lFeatures.FirstOrDefault(x => x.Key == "SurfaceAre").Value);
                        fixedAsset.Distance = lFeatures.FirstOrDefault(x => x.Key == "distance").Value.ToString() == string.Empty ? (double?)null : Convert.ToDouble(lFeatures.FirstOrDefault(x => x.Key == "distance").Value);
                        fixedAsset.MarketValue = lFeatures.FirstOrDefault(x => x.Key == "MarketValu").Value.ToString() == string.Empty ? (int?)null : Convert.ToInt32(lFeatures.FirstOrDefault(x => x.Key == "MarketValu").Value);
                        fixedAsset.NumberOfP = lFeatures.FirstOrDefault(x => x.Key == "numberOfP").Value.ToString() == string.Empty ? (int?)null : Convert.ToInt32(lFeatures.FirstOrDefault(x => x.Key == "numberOfP").Value);

                        fixedAsset.RoadID = null;// lFeatures.FirstOrDefault(x => x.Key == "RoadID").Value.ToString() == string.Empty ? (int?)null : Convert.ToInt32(lFeatures.FirstOrDefault(x => x.Key == "RoadID").Value);
                        fixedAsset.LocalityID = null;// lFeatures.FirstOrDefault(x => x.Key == "LocalityID").Value.ToString() == string.Empty ? (int?)null : Convert.ToInt32(lFeatures.FirstOrDefault(x => x.Key == "LocalityID").Value);
                        fixedAsset.VillageID = null;// lFeatures.FirstOrDefault(x => x.Key == "VillageID").Value.ToString() == string.Empty ? (int?)null : Convert.ToInt32(lFeatures.FirstOrDefault(x => x.Key == "VillageID").Value);

                        DateTime exp = DateTime.Now;
                        if (lFeatures.FirstOrDefault(x => x.Key == "ExpDate").Value.ToString() == string.Empty)
                            fixedAsset.ExpDate = (DateTime?)null;
                        else
                            if (DateTime.TryParse(lFeatures.FirstOrDefault(x => x.Key == "ExpDate").Value, out exp))
                            fixedAsset.ExpDate = exp;

                        fixedAsset.lMatrix = lMatrix;
                        fixedAsset.PictureName = lFeatures.FirstOrDefault(x => x.Key == "pictureNam").Value.ToString() == string.Empty ? null : lFeatures.FirstOrDefault(x => x.Key == "pictureNam").Value;
                        if (fixedAsset.PictureName != null && fixedAsset.PictureName.Contains("|"))
                            fixedAsset.PictureName = fixedAsset.PictureName.Split('|')[0];

                        String sAssetType = System.IO.Path.GetFileNameWithoutExtension(shpPath);
                        GISAssetType at = GetGISAssetTypeByType(sAssetType, sConnStr);
                        if (at == null)
                        {
                            at = new GISAssetType();
                            at.AssetType = sAssetType;
                            SaveGISAssetType(at, true, sConnStr);
                            at = GetGISAssetTypeByType(sAssetType, sConnStr);
                        }
                        fixedAsset.AssetType = at.iID;

                        String sAssetCategory = lFeatures.FirstOrDefault(x => x.Key == "AssetCateg").Value.ToString() == string.Empty ? null : lFeatures.FirstOrDefault(x => x.Key == "AssetCateg").Value;
                        if (!String.IsNullOrEmpty(sAssetCategory))
                        {
                            GISAssetCategory ac = GetGISAssetCategory(sAssetCategory, sAssetType, sConnStr);
                            if (ac == null)
                            {
                                ac = new GISAssetCategory();
                                ac.AssetCategory = sAssetCategory;
                                ac.AssetTypeID = fixedAsset.AssetType;
                                SaveGISAssetCategory(ac, true, sConnStr);
                                ac = GetGISAssetCategory(sAssetCategory, sAssetType, sConnStr);
                            }
                            fixedAsset.AssetCategory = ac.iID;
                        }

                        fixedAsset.GeomData = lFeatures.FirstOrDefault(x => x.Key == "GeomData").Value.ToString() == string.Empty ? null : lFeatures.FirstOrDefault(x => x.Key == "GeomData").Value;

                        String AssetID = String.Empty;
                        FixedAsset fa = GetFixedAssetByAssetCode(sUserToken, fixedAsset.AssetCode, sConnStr);
                        if (fa == null)
                        {
                            fixedAsset.oprType = DataRowState.Added;
                            bm = SaveFixedAsset(fixedAsset, UID, true, sConnStr);
                            AssetID = bm.AdditionalInfo;
                            lResult.Add("NewAsset : " + AssetID);

                            iCntNew++;
                        }
                        else
                        {
                            fixedAsset.oprType = DataRowState.Modified;
                            fixedAsset.AssetID = fa.AssetID;
                            fixedAsset.lMatrix = fa.lMatrix;
                            foreach (Matrix fam in fixedAsset.lMatrix)
                                fam.oprType = DataRowState.Modified;
                            bm = SaveFixedAsset(fixedAsset, UID, false, sConnStr);
                            AssetID = fixedAsset.AssetID.ToString();
                            lResult.Add("UpdatedAsset : " + AssetID);

                            iCntUpdated++;
                        }
                        lResult.Add("AssetCode : " + fixedAsset.AssetCode);

                        //try
                        //{
                        //    String inputPath = lFeatures.FirstOrDefault(x => x.Key == "picturePat").Value.ToString().Replace("/storage/emulated/0/MapIt/", sOriginalPicPath).Replace('/', '\\');
                        //    string[] inputPathList = inputPath.Split(new Char[] { '|' });
                        //    foreach (string finalPath in inputPathList)
                        //    {
                        //        if (!Globals.bFileExists(finalPath))
                        //            CopyImage(finalPath, Globals.DocsPath("FixedAssets\\" + AssetID));
                        //    }
                        //}
                        //catch { }
                    }
                    catch (Exception ex)
                    {
                        lResult.Add(ex.ToString());
                    }
                }

                lResult.Add("Features : " + lfeatureIDs.Count);
                lResult.Add("iCntNew : " + iCntNew);
                lResult.Add("iCntUpdated : " + iCntUpdated);
                lResult.Add("iCntTotal : " + (iCntNew + iCntUpdated));
                lResult.Add("CompletedAt : " + DateTime.Now);
            }
            catch (Exception ex)
            {
                lResult.Add(ex.ToString());
            }
            bm.data = lResult;
            return bm;
        }

        public static void CopyImage(string SoucePath, string DestPath)
        {
            string FileName = Path.GetFileName(SoucePath);
            DestPath = DestPath + "\\" + FileName;
            File.Copy(SoucePath, DestPath, false);
        }
        private static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }

        public DataTable dtGetFaWithinDistanceInMeters(int UID, Double dDistance, List<int> lBlupID, RoadNetwork roadnetwork, String sConnStr)
        {
            Boolean isVca = false;
            Boolean isRoad = false;
            if (roadnetwork == null)
                roadnetwork = new RoadNetwork();

            dDistance = dDistance / 1000000;

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"
drop table if exists #t
drop table if exists #road
drop table if exists #fa
drop table if exists #District
drop table if exists #vca

";

            sql += new LibData().sqlValidatedData(UID, NaveoModels.FixedAssets, sConnStr);
            sql += @"
CREATE TABLE #fa
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY CLUSTERED,
	[AssetID] [int]
)

CREATE TABLE #vca
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY CLUSTERED,
	[vcaID] [int]
)

CREATE TABLE #district
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY CLUSTERED,
	[districtID] [int]
)

CREATE TABLE #road
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY CLUSTERED,
	[roadId] int
)
";//
            foreach (int i in lBlupID)
                sql += "insert #fa (AssetID) values (" + i.ToString() + ");\r\n";


            if (roadnetwork.district.Count > 0)
                foreach (int i in roadnetwork.district)
                    sql += "insert #district (districtID) values (" + i.ToString() + ");\r\n";
            else if (roadnetwork.vca.Count > 0)
            {
                isVca = true;
                foreach (int i in roadnetwork.vca)
                    sql += "insert #vca (vcaID) values (" + i.ToString() + ");\r\n";
            }
            else
            {
                isRoad = true;
                foreach (int s in roadnetwork.road)
                    sql += "insert #road (roadId) values ('" + s + "');\r\n";
            }

            sql += @"
;with cte as
(
	select a.*, t.AssetType AssetTypeDesc, c.AssetCategory AssetCategoryDesc from GFI_GIS_FixedAsset a 
        inner join #fa f on a.AssetID = f.AssetID
		inner join GFI_GIS_AssetType t on t.iID = a.AssetType
		inner join GFI_GIS_AssetCategory c on c.iID = a.AssetCategory
)
select b.GeomData gData, b.AssetID AS AssetID, b.AssetCode, b.GeomData.MakeValid().ToString() as GeomData
    , c.AssetName, c.AssetTypeDesc, c.AssetCategoryDesc, c.AssetFamily, c.AssetCategoryDesc AssetCategory
    , '' MaintType, '' MaintStatus, CONVERT(varchar(250), '') VCAName, CONVERT(varchar(250), '') RoadName
into #t from cte c
	inner join GFI_GIS_FixedAsset b on c.AssetID = b.AssetID

update #t set VCAName = v.VCAName
from GFI_GIS_VCA v WITH (INDEX(SIndx_GFI_GIS_VCA))
	inner join #t t on t.gData.STIntersects(v.GeomData) = 1
";
            if (isVca)
                sql += @"	inner join #vca t0 on t0.vcaID = v.VCAID";

            sql += @"
;with cte as
(
	select t.*, r.RoadID, r.RoadName rName
		, ROW_NUMBER() OVER (Partition by AssetID order by t.gData.STDistance(r.GeomData)) RowNum
	from #t t
		inner join GFI_GIS_RoadNetwork r WITH (INDEX(SIndx_GFI_GIS_RoadNetwork)) on t.gData.STDistance(r.GeomData) < " + dDistance + @"
)	--select * from cte
	update #t set RoadName = c.rName
		from #t t
			inner join cte c on c.AssetID = t.AssetID
";
            if (isRoad)
                sql += "	inner join #road t0 on t0.roadId = c.RoadID";

            sql += @" 		
    where c.RowNum = 1

ALTER TABLE #T DROP COLUMN gData
select * from #t
";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtResult";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables["dtResult"];
        }
        public DataTable dtGetMaintStatusType(int UID, List<int> lids, List<int> statusId, List<int> typeId, string hostURL, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            if (hostURL == "localhost")
                hostURL = "SUN";

            String sql = @";with cte as
	(select FMSAssetId As AssetIDfleet, vs.Description AS STATUS, vt.Description AS TYPE,  vm.*
		, row_number() over(partition by FMSAssetId order by vm.StartDate desc) as RowNum
	from GFI_AMM_VehicleMaintenance vm
			inner join CustomerVehicle cv on vm.AssetId = cv.Id 
			inner join FleetServer fs on cv.FleetServerId = fs.Id
			inner join GFI_AMM_VehicleMaintStatus vs on vs.MaintStatusId = vm.MaintStatusId_cbo
            inner join GFI_AMM_VehicleMaintTypes vt on vt.MaintTypeId = vm.MaintTypeId_cbo
	where fs.Name = '" + hostURL + "' ";

            if (lids.Count > 0)
            {
                sql += "AND cv.FMSAssetId IN (";
                String s = String.Empty;
                foreach (int i in lids)
                    s += i.ToString() + ", ";
                s = s.Substring(0, s.Length - 2);
                sql += s + ")\r\n";
            }

            if (statusId.Contains(9999))
                statusId.Remove(9999);
            if (statusId.Count > 0)
            {
                sql += "AND vm.MaintStatusId_cbo IN (";
                String s = String.Empty;
                foreach (int i in statusId)
                    s += i.ToString() + ", ";
                s = s.Substring(0, s.Length - 2);
                sql += s + ")\r\n";
            }

            if (typeId.Count > 0)
            {
                sql += "AND vm.MaintTypeId_cbo IN (";
                String s = String.Empty;
                foreach (int i in typeId)
                    s += i.ToString() + ", ";
                s = s.Substring(0, s.Length - 2);
                sql += s + ")\r\n";
            }

            sql += @"
                        AND cv.Remarks = 'FixedAsset'
		                and vs.Description != 'TO SCHEDULE'
	                 )
                     select * from cte where RowNum = 1";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtResult";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables["dtResult"];
        }

        String FixedAssetsFields(String prefix)
        {
            String sql = @" f.AssetID
                        , f.AssetName
                        , f.descript
                        , f.AssetCode
                        , f.RoadID
                        , f.LocalityID
                        , f.VillageID
                        , f.AssetType
                        , f.AssetCategory
                        , f.AssetFamily
                        , f.Owners
                        , f.Height
                        , f.Wattage
                        , f.Notes
                        , f.pictureName
                        , f.dtCreated 
                        , f.MaintLink
                        , f.SurfaceArea
                        , f.sValue
                        , f.Price_m2
                        , f.ExpDate
                        , f.MarketValue
                        , f.TitleDeed
                        , f.distance
                        , f.distanceUnit
                        , f.numberOfP
                        , f.Pin
                        , f.GeomData.ToString() as GeomData ";
            if (String.IsNullOrEmpty(prefix))
                sql = sql.Replace("f.", String.Empty);
            else
                sql = sql.Replace("f.", prefix + ".");

            return sql;
        }
        public DataTable dtGetFaWithinDistanceInMeters(Guid sUserToken, Double dDistance, Double lon, Double lat, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            int UID = new NaveoOneLib.Services.Users.UserService().GetUserID(sUserToken, sConnStr);
            String sql = new LibData().sqlValidatedData(UID, NaveoModels.FixedAssets, sConnStr);
            sql += @"
select b.AssetID, b.AssetCode, b.AssetName, b.AssetType, t.AssetType AssetTypeDesc, c.AssetCategory AssetCategoryDesc, b.AssetFamily
    , b.GeomData.MakeValid().STDistance(GEOMETRY::Point((CONVERT(varchar, " + lon + ", 128)), CONVERT(varchar,  " + lat + @", 128), 4326)) distance
	, b.GeomData.MakeValid().ToString() GeomData
from GFI_GIS_FixedAsset b
    inner join #ValidatedData a on a.StrucID = b.AssetID
	inner join GFI_GIS_AssetCategory c on c.iID = b.AssetCategory
	inner join GFI_GIS_AssetType t on t.iID = b.AssetType
where b.GeomData.MakeValid().STDistance(GEOMETRY::Point((CONVERT(varchar, " + lon + ", 128)), CONVERT(varchar, " + lat + ", 128), 4326)) <= " + dDistance + @"
order by distance desc";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtResult";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables["dtResult"];
        }
        public DataSet GetFaByiID(int AssetID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"SELECT " + FixedAssetsFields("a") + @", t.AssetType AssetTypeDesc, c.AssetCategory AssetCategoryDesc, dbo.Geometry2Json(GeomData) Geom2Json
                    from GFI_GIS_FixedAsset a
                            inner join GFI_GIS_AssetType t on t.iID = a.AssetType
		                    left outer join GFI_GIS_AssetCategory c on c.iID = a.AssetCategory
                    WHERE AssetID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = AssetID;
            drSql["sTableName"] = "GFI_GIS_FixedAsset";
            dtSql.Rows.Add(drSql);

            sql = new MatrixService().sGetMatrixId(AssetID, "GFI_SYS_GroupMatrixFixedAsset");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds;
        }
    }
}

