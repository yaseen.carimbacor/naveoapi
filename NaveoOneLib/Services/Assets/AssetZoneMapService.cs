
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Services.GMatrix;

namespace NaveoOneLib.Services.Assets
{
    class AssetZoneMapService
    {

        Errors er = new Errors();

        public String sGetAssetZoneMap(List<int> iParentIDs, String sConnStr)
        {
            String sqlAsset = @"SELECT AssetID
                                from GFI_FLT_Asset a, GFI_SYS_GroupMatrixAsset m
                                where a.AssetID = m.iID and m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + ")";

            String sql = @"select * from GFI_AMM_AssetZoneMap 
                            WHERE AssetId in (" + sqlAsset + ") order by AssetId";
            return sql;
        }

        public DataTable GetAssetZoneMap(String sConnStr)
        {
            String sqlString = @"SELECT Id, 
                                   AssetID, 
                                   ZoneID, 
                                   Param1, 
                                   Param2, 
                                   Param3, 
                                   CreatedBy, 
                                   CreatedDate, 
                                   ModifiedBy, 
                                   ModifiedDate from GFI_AMM_AssetZoneMap order by TingPow";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public AssetZoneMap GetAssetZoneMapById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT Id, 
                                   AssetID, 
                                   ZoneID, 
                                   Param1, 
                                   Param2, 
                                   Param3, 
                                   CreatedBy, 
                                   CreatedDate, 
                                   ModifiedBy, 
                                   ModifiedDate from GFI_AMM_AssetZoneMap
                                   WHERE TingPow = ?
                                   order by TingPow";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                AssetZoneMap retAssetZoneMap = new AssetZoneMap();
                retAssetZoneMap.Id = Convert.ToInt32(dt.Rows[0]["Id"]);
                retAssetZoneMap.AssetID = Convert.ToInt32(dt.Rows[0]["AssetID"]);
                retAssetZoneMap.ZoneID = Convert.ToInt32(dt.Rows[0]["ZoneID"]);
                retAssetZoneMap.Param1 = dt.Rows[0]["Param1"].ToString();
                retAssetZoneMap.Param2 = dt.Rows[0]["Param2"].ToString();
                retAssetZoneMap.Param3 = dt.Rows[0]["Param3"].ToString();
                retAssetZoneMap.CreatedBy = dt.Rows[0]["CreatedBy"].ToString();
                retAssetZoneMap.CreatedDate = (DateTime)dt.Rows[0]["CreatedDate"];
                retAssetZoneMap.ModifiedBy = dt.Rows[0]["ModifiedBy"].ToString();
                retAssetZoneMap.ModifiedDate = (DateTime)dt.Rows[0]["ModifiedDate"];
                return retAssetZoneMap;
            }
        }
        public bool SaveAssetZoneMap(AssetZoneMap uAssetZoneMap, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_AssetZoneMap";
            dt.Columns.Add("Id", uAssetZoneMap.Id.GetType());
            dt.Columns.Add("AssetID", uAssetZoneMap.AssetID.GetType());
            dt.Columns.Add("ZoneID", uAssetZoneMap.ZoneID.GetType());
            dt.Columns.Add("Param1", uAssetZoneMap.Param1.GetType());
            dt.Columns.Add("Param2", uAssetZoneMap.Param2.GetType());
            dt.Columns.Add("Param3", uAssetZoneMap.Param3.GetType());
            dt.Columns.Add("CreatedBy", uAssetZoneMap.CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", uAssetZoneMap.CreatedDate.GetType());
            dt.Columns.Add("ModifiedBy", uAssetZoneMap.ModifiedBy.GetType());
            dt.Columns.Add("ModifiedDate", uAssetZoneMap.ModifiedDate.GetType());
            DataRow dr = dt.NewRow();
            dr["Id"] = uAssetZoneMap.Id;
            dr["AssetID"] = uAssetZoneMap.AssetID;
            dr["ZoneID"] = uAssetZoneMap.ZoneID;
            dr["Param1"] = uAssetZoneMap.Param1;
            dr["Param2"] = uAssetZoneMap.Param2;
            dr["Param3"] = uAssetZoneMap.Param3;
            dr["CreatedBy"] = uAssetZoneMap.CreatedBy;
            dr["CreatedDate"] = uAssetZoneMap.CreatedDate;
            dr["ModifiedBy"] = uAssetZoneMap.ModifiedBy;
            dr["ModifiedDate"] = uAssetZoneMap.ModifiedDate;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenInsert(dt, transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool UpdateAssetZoneMap(AssetZoneMap uAssetZoneMap, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_AssetZoneMap";
            dt.Columns.Add("Id", uAssetZoneMap.Id.GetType());
            dt.Columns.Add("AssetID", uAssetZoneMap.AssetID.GetType());
            dt.Columns.Add("ZoneID", uAssetZoneMap.ZoneID.GetType());
            dt.Columns.Add("Param1", uAssetZoneMap.Param1.GetType());
            dt.Columns.Add("Param2", uAssetZoneMap.Param2.GetType());
            dt.Columns.Add("Param3", uAssetZoneMap.Param3.GetType());
            dt.Columns.Add("CreatedBy", uAssetZoneMap.CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", uAssetZoneMap.CreatedDate.GetType());
            dt.Columns.Add("ModifiedBy", uAssetZoneMap.ModifiedBy.GetType());
            dt.Columns.Add("ModifiedDate", uAssetZoneMap.ModifiedDate.GetType());
            DataRow dr = dt.NewRow();
            dr["Id"] = uAssetZoneMap.Id;
            dr["AssetID"] = uAssetZoneMap.AssetID;
            dr["ZoneID"] = uAssetZoneMap.ZoneID;
            dr["Param1"] = uAssetZoneMap.Param1;
            dr["Param2"] = uAssetZoneMap.Param2;
            dr["Param3"] = uAssetZoneMap.Param3;
            dr["CreatedBy"] = uAssetZoneMap.CreatedBy;
            dr["CreatedDate"] = uAssetZoneMap.CreatedDate;
            dr["ModifiedBy"] = uAssetZoneMap.ModifiedBy;
            dr["ModifiedDate"] = uAssetZoneMap.ModifiedDate;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "Id = '" + uAssetZoneMap.Id + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool DeleteAssetZoneMap(AssetZoneMap uAssetZoneMap, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_AMM_AssetZoneMap", "Id = '" + uAssetZoneMap.Id + "'", sConnStr))
                return true;
            else
                return false;
        }

    }
}



