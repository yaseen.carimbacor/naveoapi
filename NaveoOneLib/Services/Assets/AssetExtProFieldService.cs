using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Assets;

namespace NaveoOneLib.Services.Assets
{
    public class AssetExtProFieldService
    {

        Errors er = new Errors();

        public String sGetAssetExtProField()
        {
            String sqlString = @"SELECT FieldId, 
                                    AssetType, 
                                    Category, 
                                    FieldName, 
                                    Label, 
                                    Description, 
                                    Type, 
                                    UnitOfMeasure, 
                                    KeepHistory, 
                                    DataRetentionDays, 
                                    THWarning_Value, 
                                    THWarning_TimeBased, 
                                    THCritical_Value, 
                                    THCritical_TimeBased, 
                                    DisplayOrder, 
                                    CreatedDate, 
                                    CreatedBy, 
                                    UpdatedDate, 
                                    UpdatedBy from GFI_AMM_AssetExtProFields order by FieldId";
            return sqlString;
        }

        public DataTable GetAssetExtProField(String sConnStr)
        {
            return new Connection(sConnStr).GetDataDT(sGetAssetExtProField(), sConnStr);
        }

        public AssetExtProField GetAssetExtProFieldById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT FieldId, 
                                    AssetType, 
                                    Category, 
                                    FieldName, 
                                    Label, 
                                    Description, 
                                    Type, 
                                    UnitOfMeasure, 
                                    KeepHistory, 
                                    DataRetentionDays, 
                                    THWarning_Value, 
                                    THWarning_TimeBased, 
                                    THCritical_Value, 
                                    THCritical_TimeBased, 
                                    DisplayOrder, 
                                    CreatedDate, 
                                    CreatedBy, 
                                    UpdatedDate, 
                                    UpdatedBy from GFI_AMM_AssetExtProFields
                                    WHERE FieldId = ?
                                    order by FieldId";

            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                AssetExtProField retAssetExtProField = new AssetExtProField();
                retAssetExtProField.FieldId = Convert.ToInt32(dt.Rows[0]["FieldId"]);
                retAssetExtProField.AssetType = dt.Rows[0]["AssetType"].ToString();
                retAssetExtProField.Category = dt.Rows[0]["Category"].ToString();
                retAssetExtProField.FieldName = dt.Rows[0]["FieldName"].ToString();
                retAssetExtProField.Label = dt.Rows[0]["Label"].ToString();
                retAssetExtProField.Description = dt.Rows[0]["Description"].ToString();
                retAssetExtProField.Type = dt.Rows[0]["Type"].ToString();
                retAssetExtProField.UnitOfMeasure = dt.Rows[0]["UnitOfMeasure"].ToString();
                retAssetExtProField.KeepHistory = dt.Rows[0]["KeepHistory"].ToString();
                retAssetExtProField.DataRetentionDays = Convert.ToInt32(dt.Rows[0]["DataRetentionDays"]);
                retAssetExtProField.THWarning_Value = Convert.ToInt32(dt.Rows[0]["THWarning_Value"]);
                retAssetExtProField.THWarning_TimeBased = Convert.ToInt32(dt.Rows[0]["THWarning_TimeBased"]);
                retAssetExtProField.THCritical_Value = Convert.ToInt32(dt.Rows[0]["THCritical_Value"]);
                retAssetExtProField.THCritical_TimeBased = Convert.ToInt32(dt.Rows[0]["THCritical_TimeBased"]);
                retAssetExtProField.DisplayOrder = Convert.ToInt32(dt.Rows[0]["DisplayOrder"]);
                retAssetExtProField.CreatedDate = (DateTime)dt.Rows[0]["CreatedDate"];
                retAssetExtProField.CreatedBy = dt.Rows[0]["CreatedBy"].ToString();
                retAssetExtProField.UpdatedDate = (DateTime)dt.Rows[0]["UpdatedDate"];
                retAssetExtProField.UpdatedBy = dt.Rows[0]["UpdatedBy"].ToString();
                return retAssetExtProField;
            }
        }

        public AssetExtProField GetAssetExtProFieldByFieldName(String sFieldName, String sConnStr)
        {
            String sqlString = @"SELECT FieldId, 
                                    AssetType, 
                                    Category, 
                                    FieldName, 
                                    Label, 
                                    Description, 
                                    Type, 
                                    UnitOfMeasure, 
                                    KeepHistory, 
                                    DataRetentionDays, 
                                    THWarning_Value, 
                                    THWarning_TimeBased, 
                                    THCritical_Value, 
                                    THCritical_TimeBased, 
                                    DisplayOrder, 
                                    CreatedDate, 
                                    CreatedBy, 
                                    UpdatedDate, 
                                    UpdatedBy from GFI_AMM_AssetExtProFields
                                    WHERE FieldName = ?
                                    order by FieldName";

            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, sFieldName, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                AssetExtProField retAssetExtProField = new AssetExtProField();
                retAssetExtProField.FieldId = Convert.ToInt32(dt.Rows[0]["FieldId"]);
                retAssetExtProField.AssetType = dt.Rows[0]["AssetType"].ToString();
                retAssetExtProField.Category = dt.Rows[0]["Category"].ToString();
                retAssetExtProField.FieldName = dt.Rows[0]["FieldName"].ToString();
                retAssetExtProField.Label = dt.Rows[0]["Label"].ToString();
                retAssetExtProField.Description = dt.Rows[0]["Description"].ToString();
                retAssetExtProField.Type = dt.Rows[0]["Type"].ToString();
                retAssetExtProField.UnitOfMeasure = dt.Rows[0]["UnitOfMeasure"].ToString();
                retAssetExtProField.KeepHistory = dt.Rows[0]["KeepHistory"].ToString();
                retAssetExtProField.DataRetentionDays = Convert.ToInt32(dt.Rows[0]["DataRetentionDays"]);
                retAssetExtProField.THWarning_Value = Convert.ToInt32(dt.Rows[0]["THWarning_Value"]);
                retAssetExtProField.THWarning_TimeBased = Convert.ToInt32(dt.Rows[0]["THWarning_TimeBased"]);
                retAssetExtProField.THCritical_Value = Convert.ToInt32(dt.Rows[0]["THCritical_Value"]);
                retAssetExtProField.THCritical_TimeBased = Convert.ToInt32(dt.Rows[0]["THCritical_TimeBased"]);
                retAssetExtProField.DisplayOrder = Convert.ToInt32(dt.Rows[0]["DisplayOrder"]);
                retAssetExtProField.CreatedDate = (DateTime)dt.Rows[0]["CreatedDate"];
                retAssetExtProField.CreatedBy = dt.Rows[0]["CreatedBy"].ToString();
                retAssetExtProField.UpdatedDate = (DateTime)dt.Rows[0]["UpdatedDate"];
                retAssetExtProField.UpdatedBy = dt.Rows[0]["UpdatedBy"].ToString();
                return retAssetExtProField;
            }
        }

        public bool SaveAssetExtProField(AssetExtProField uAssetExtProField, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_AssetExtProFields";
            dt.Columns.Add("FieldId", uAssetExtProField.FieldId.GetType());
            dt.Columns.Add("AssetType", uAssetExtProField.AssetType.GetType());
            dt.Columns.Add("Category", uAssetExtProField.Category.GetType());
            dt.Columns.Add("FieldName", uAssetExtProField.FieldName.GetType());
            dt.Columns.Add("Label", uAssetExtProField.Label.GetType());
            dt.Columns.Add("Description", uAssetExtProField.Description.GetType());
            dt.Columns.Add("Type", uAssetExtProField.Type.GetType());
            dt.Columns.Add("UnitOfMeasure", uAssetExtProField.UnitOfMeasure.GetType());
            dt.Columns.Add("KeepHistory", uAssetExtProField.KeepHistory.GetType());
            dt.Columns.Add("DataRetentionDays", uAssetExtProField.DataRetentionDays.GetType());
            dt.Columns.Add("THWarning_Value", uAssetExtProField.THWarning_Value.GetType());
            dt.Columns.Add("THWarning_TimeBased", uAssetExtProField.THWarning_TimeBased.GetType());
            dt.Columns.Add("THCritical_Value", uAssetExtProField.THCritical_Value.GetType());
            dt.Columns.Add("THCritical_TimeBased", uAssetExtProField.THCritical_TimeBased.GetType());
            dt.Columns.Add("DisplayOrder", uAssetExtProField.DisplayOrder.GetType());
            dt.Columns.Add("CreatedDate", uAssetExtProField.CreatedDate.GetType());
            dt.Columns.Add("CreatedBy", typeof(string));
            dt.Columns.Add("UpdatedDate", uAssetExtProField.UpdatedDate.GetType());
            dt.Columns.Add("UpdatedBy", typeof(string));
            
            DataRow dr = dt.NewRow();
            dr["FieldId"] = uAssetExtProField.FieldId;
            dr["AssetType"] = uAssetExtProField.AssetType;
            dr["Category"] = uAssetExtProField.Category;
            dr["FieldName"] = uAssetExtProField.FieldName;
            dr["Label"] = uAssetExtProField.Label;
            dr["Description"] = uAssetExtProField.Description;
            dr["Type"] = uAssetExtProField.Type;
            dr["UnitOfMeasure"] = uAssetExtProField.UnitOfMeasure;
            dr["KeepHistory"] = uAssetExtProField.KeepHistory;
            dr["DataRetentionDays"] = uAssetExtProField.DataRetentionDays;
            dr["THWarning_Value"] = uAssetExtProField.THWarning_Value;
            dr["THWarning_TimeBased"] = uAssetExtProField.THWarning_TimeBased;
            dr["THCritical_Value"] = uAssetExtProField.THCritical_Value;
            dr["THCritical_TimeBased"] = uAssetExtProField.THCritical_TimeBased;
            dr["DisplayOrder"] = uAssetExtProField.DisplayOrder;
            dr["CreatedDate"] = uAssetExtProField.CreatedDate;
            dr["CreatedBy"] = uAssetExtProField.CreatedBy;
            dr["UpdatedDate"] = uAssetExtProField.UpdatedDate;
            dr["UpdatedBy"] = uAssetExtProField.UpdatedBy;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); 
            if (_connection.GenInsert(dt, transaction, sConnStr))
                return true;
            else
                return false;
        }

        public bool UpdateAssetExtProField(AssetExtProField uAssetExtProField, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_AssetExtProFields";
            dt.Columns.Add("FieldId", uAssetExtProField.FieldId.GetType());
            dt.Columns.Add("AssetType", uAssetExtProField.AssetType.GetType());
            dt.Columns.Add("Category", uAssetExtProField.Category.GetType());
            dt.Columns.Add("FieldName", uAssetExtProField.FieldName.GetType());
            dt.Columns.Add("Label", uAssetExtProField.Label.GetType());
            dt.Columns.Add("Description", uAssetExtProField.Description.GetType());
            dt.Columns.Add("Type", uAssetExtProField.Type.GetType());
            dt.Columns.Add("UnitOfMeasure", uAssetExtProField.UnitOfMeasure.GetType());
            dt.Columns.Add("KeepHistory", uAssetExtProField.KeepHistory.GetType());
            dt.Columns.Add("DataRetentionDays", uAssetExtProField.DataRetentionDays.GetType());
            dt.Columns.Add("THWarning_Value", uAssetExtProField.THWarning_Value.GetType());
            dt.Columns.Add("THWarning_TimeBased", uAssetExtProField.THWarning_TimeBased.GetType());
            dt.Columns.Add("THCritical_Value", uAssetExtProField.THCritical_Value.GetType());
            dt.Columns.Add("THCritical_TimeBased", uAssetExtProField.THCritical_TimeBased.GetType());
            dt.Columns.Add("DisplayOrder", uAssetExtProField.DisplayOrder.GetType());
            dt.Columns.Add("CreatedDate", uAssetExtProField.CreatedDate.GetType());
            dt.Columns.Add("CreatedBy", uAssetExtProField.CreatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uAssetExtProField.UpdatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uAssetExtProField.UpdatedBy.GetType());
            
            DataRow dr = dt.NewRow();
            dr["FieldId"] = uAssetExtProField.FieldId;
            dr["AssetType"] = uAssetExtProField.AssetType;
            dr["Category"] = uAssetExtProField.Category;
            dr["FieldName"] = uAssetExtProField.FieldName;
            dr["Label"] = uAssetExtProField.Label;
            dr["Description"] = uAssetExtProField.Description;
            dr["Type"] = uAssetExtProField.Type;
            dr["UnitOfMeasure"] = uAssetExtProField.UnitOfMeasure;
            dr["KeepHistory"] = uAssetExtProField.KeepHistory;
            dr["DataRetentionDays"] = uAssetExtProField.DataRetentionDays;
            dr["THWarning_Value"] = uAssetExtProField.THWarning_Value;
            dr["THWarning_TimeBased"] = uAssetExtProField.THWarning_TimeBased;
            dr["THCritical_Value"] = uAssetExtProField.THCritical_Value;
            dr["THCritical_TimeBased"] = uAssetExtProField.THCritical_TimeBased;
            dr["DisplayOrder"] = uAssetExtProField.DisplayOrder;
            dr["CreatedDate"] = uAssetExtProField.CreatedDate;
            dr["CreatedBy"] = uAssetExtProField.CreatedBy;
            dr["UpdatedDate"] = uAssetExtProField.UpdatedDate;
            dr["UpdatedBy"] = uAssetExtProField.UpdatedBy;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "FieldId = '" + uAssetExtProField.FieldId + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }

        public bool DeleteAssetExtProField(AssetExtProField uAssetExtProField, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_AMM_AssetExtProFields", "FieldId = '" + uAssetExtProField.FieldId + "'", sConnStr))
                return true;
            else
                return false;
        }

    }
}


