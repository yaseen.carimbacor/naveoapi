using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Services.GMatrix;

namespace NaveoOneLib.Services.Assets
{
    public class VehicleMaintTypeService
    {

        Errors er = new Errors();

        public String sGetVehicleMaintTypeMatirx(int iMaintTypeId)
        {
            String sqlString = @"SELECT * 
                                 FROM 
                                       GFI_SYS_GroupMatrixMaintType mmt
                                 INNER JOIN GFI_SYS_GroupMatrix gm
                                       ON mmt.GMID = gm.GMID
                                 WHERE iId = " + iMaintTypeId;

            return sqlString;
        }

        public String sGetVehicleMaintType(List<Matrix> lMatrix, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            return sGetVehicleMaintType(iParentIDs, sConnStr);
        }
        public String sGetVehicleMaintType(List<int> iParentIDs, String sConnStr)
        {
            String sqlString = @"
SELECT vmt.MaintTypeId, 
    vmt.MaintCatId_cbo,
    vmc.Description AS MaintCatDesc,
    vmt.Description,
    vmt.OccurrenceType, 
    vmt.OccurrenceFixedDate,
    vmt.OccurrenceDuration,
    vmt.OccurrencePeriod_cbo,
    vmt.OccurrenceKM,
    vmt.OccurrenceEngineHrs,
    vmt.OccurrenceFixedDateTh,
    vmt.OccurrenceDurationTh,
    vmt.OccurrenceKMTh,
    vmt.OccurrenceEngineHrsTh,
    vmt.CreatedDate,
    vmt.CreatedBy, 
    vmt.UpdatedDate, 
    vmt.UpdatedBy 
FROM GFI_AMM_VehicleMaintTypes vmt
    INNER JOIN GFI_AMM_VehicleMaintCat vmc ON vmt.MaintCatId_cbo = vmc.MaintCatId
    INNER JOIN GFI_SYS_GroupMatrixMaintType m ON vmt.MaintTypeId = m.iID
WHERE m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + ") ";

            sqlString += @"
union 
SELECT vmt.MaintTypeId, 
    vmt.MaintCatId_cbo,
    vmc.Description AS MaintCatDesc,
    vmt.Description,
    vmt.OccurrenceType, 
    vmt.OccurrenceFixedDate,
    vmt.OccurrenceDuration,
    vmt.OccurrencePeriod_cbo,
    vmt.OccurrenceKM,
    vmt.OccurrenceEngineHrs,
    vmt.OccurrenceFixedDateTh,
    vmt.OccurrenceDurationTh,
    vmt.OccurrenceKMTh,
    vmt.OccurrenceEngineHrsTh,
    vmt.CreatedDate,
    vmt.CreatedBy, 
    vmt.UpdatedDate, 
    vmt.UpdatedBy
FROM GFI_AMM_VehicleMaintTypes vmt
    INNER JOIN GFI_AMM_VehicleMaintCat vmc ON vmt.MaintCatId_cbo = vmc.MaintCatId
where vmt.CreatedBy = 'INSTALLER'
";
            sqlString += " ORDER BY vmt.MaintTypeId";
            return sqlString;
        }
        public String sGetVehicleMaintTypeForMaintAssign(List<int> iParentIDs, String sConnStr)
        {
            String sqlString = string.Empty;
            sqlString = @"SELECT vmt.MaintTypeId, 
                                vmt.MaintCatId_cbo,
                                vmc.Description AS MaintCatDesc,
                                vmt.Description,
                                vmt.OccurrenceType, 
                                vmt.OccurrenceFixedDate,
                                vmt.OccurrenceDuration,
                                vmt.OccurrencePeriod_cbo,
                                vmt.OccurrenceKM,
                                vmt.OccurrenceEngineHrs,
                                vmt.OccurrenceFixedDateTh,
                                vmt.OccurrenceDurationTh,
                                vmt.OccurrenceKMTh,
                                vmt.OccurrenceEngineHrsTh,
                                vmt.CreatedDate,
                                vmt.CreatedBy, 
                                vmt.UpdatedDate, 
                                vmt.UpdatedBy 
                               FROM GFI_AMM_VehicleMaintTypes vmt
                                    INNER JOIN GFI_AMM_VehicleMaintCat vmc
                                        ON vmt.MaintCatId_cbo = vmc.MaintCatId
                               where vmt.createdby ='INSTALLER'
                               
                               UNION ";
            sqlString += sGetVehicleMaintType(iParentIDs, sConnStr);
            return sqlString;
        }

        public DataTable GetVehicleMaintType(List<Matrix> lMatrix, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            return new Connection(sConnStr).GetDataDT(sGetVehicleMaintType(iParentIDs, sConnStr), sConnStr);
        }

        public DataTable GetVehicleMaintTypeByMaintCatId(String iID, String sConnStr)
        {
            String sqlString = @"SELECT vmt.MaintTypeId, 
                                vmt.Description, 
                                vmt.OccurrenceType
                               FROM GFI_AMM_VehicleMaintTypes vmt
                               WHERE MaintCatId_cbo = ?
                               ORDER BY vmt.MaintCatId_cbo";

            return new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
        }

        public String sGetVehicleMaintTypeById(String iID)
        {
            String sqlString = @"SELECT vmt.MaintTypeId, 
                                vmt.MaintCatId_cbo,
                                vmc.Description AS MaintCatDesc,
                                vmt.Description,
                                vmt.OccurrenceType, 
                                vmt.OccurrenceFixedDate,
                                vmt.OccurrenceDuration,
                                vmt.OccurrencePeriod_cbo,
                                vmt.OccurrenceKM,
                                vmt.OccurrenceEngineHrs,
                                vmt.OccurrenceFixedDateTh,
                                vmt.OccurrenceDurationTh,
                                vmt.OccurrenceKMTh,
                                vmt.OccurrenceEngineHrsTh,
                                vmt.CreatedDate,
                                vmt.CreatedBy, 
                                vmt.UpdatedDate, 
                                vmt.UpdatedBy 
                               FROM GFI_AMM_VehicleMaintTypes vmt
                                    INNER JOIN GFI_AMM_VehicleMaintCat vmc
                                    ON vmt.MaintCatId_cbo = vmc.MaintCatId
                                    WHERE MaintTypeId = " + iID + @"
                               ORDER BY vmt.MaintTypeId";
            return sqlString;
        }

        public VehicleMaintType GetVehicleMaintTypeById(String iID, String sConnStr)
        {

            DataTable dt = new Connection(sConnStr).GetDataTableBy(sGetVehicleMaintTypeById(iID), iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                VehicleMaintType retVehicleMaintType = VMT(dt.Rows[0]);

                DataTable dtMatrix = new Connection(sConnStr).GetDataTableBy(sGetVehicleMaintTypeMatirx(retVehicleMaintType.MaintTypeId), retVehicleMaintType.MaintTypeId.ToString(), sConnStr);

                List<Matrix> lMatrix = new List<Matrix>();
                MatrixService ms = new MatrixService();
                foreach (DataRow dr in dtMatrix.Rows)
                    lMatrix.Add(ms.AssignMatrix(dr));
                retVehicleMaintType.lMatrix = lMatrix;

                return retVehicleMaintType;
            }
        }

        public VehicleMaintType GetVehicleMaintTypeByDescription(String sDescription, String sConnStr)
        {
            String sqlString = @"SELECT vmt.MaintTypeId, 
                                vmt.MaintCatId_cbo,
                                vmc.Description AS MaintCatDesc,
                                vmt.Description,
                                vmt.OccurrenceType, 
                                vmt.OccurrenceFixedDate,
                                vmt.OccurrenceDuration,
                                vmt.OccurrencePeriod_cbo,
                                vmt.OccurrenceKM,
                                vmt.OccurrenceEngineHrs,
                                vmt.OccurrenceFixedDateTh,
                                vmt.OccurrenceDurationTh,
                                vmt.OccurrenceKMTh,
                                vmt.OccurrenceEngineHrsTh,
                                vmt.CreatedDate,
                                vmt.CreatedBy, 
                                vmt.UpdatedDate, 
                                vmt.UpdatedBy 
                               FROM GFI_AMM_VehicleMaintTypes vmt
                                    INNER JOIN GFI_AMM_VehicleMaintCat vmc
                                    ON vmt.MaintCatId_cbo = vmc.MaintCatId
                                    WHERE vmt.Description = ?
                               ORDER BY vmt.Description";

            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, sDescription, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return VMT(dt.Rows[0]);
        }

        public DataTable GetVehicleMaintForAssetId(String pAssetId, String sConnStr)
        {
            String sqlString = @"SELECT 
                                    MaintTypeId, Description
                                FROM 
                                    GFI_AMM_VehicleMaintTypes 
                                WHERE 
                                     MaintTypeId
                                IN (
                                    SELECT 
                                         MaintTypeId
                                    FROM
                                         GFI_AMM_VehicleMaintTypeLink
                                    WHERE
                                         AssetId = ?
                                    )";

            return new Connection(sConnStr).GetDataTableBy(sqlString, pAssetId, sConnStr);
        }

        public VehicleMaintType VMT(DataRow dr)
        {
            VehicleMaintType retVehicleMaintType = new VehicleMaintType();

            retVehicleMaintType.MaintCatDesc = dr["MaintCatDesc"].ToString();
            retVehicleMaintType.MaintTypeId = Convert.ToInt32(dr["MaintTypeId"]);
            retVehicleMaintType.Description = dr["Description"].ToString();

            retVehicleMaintType.OccurrenceType = Convert.ToInt32(dr["OccurrenceType"]);

            if (!String.IsNullOrEmpty(dr["OccurrenceFixedDate"].ToString()))
                retVehicleMaintType.OccurrenceFixedDate = (DateTime)dr["OccurrenceFixedDate"];

            if (!String.IsNullOrEmpty(dr["OccurrenceDuration"].ToString()))
                retVehicleMaintType.OccurrenceDuration = Convert.ToInt32(dr["OccurrenceDuration"]);
            else retVehicleMaintType.OccurrenceDuration = 0;

            if (!String.IsNullOrEmpty(dr["OccurrencePeriod_cbo"].ToString()))
                retVehicleMaintType.OccurrencePeriod_cbo = dr["OccurrencePeriod_cbo"].ToString();

            if (!String.IsNullOrEmpty(dr["OccurrenceKM"].ToString()))
                retVehicleMaintType.OccurrenceKM = Convert.ToInt32(dr["OccurrenceKM"]);
            else retVehicleMaintType.OccurrenceKM = 0;

            if (!String.IsNullOrEmpty(dr["OccurrenceEngineHrs"].ToString()))
                retVehicleMaintType.OccurrenceEngineHrs = Convert.ToInt32(dr["OccurrenceEngineHrs"]);
            else retVehicleMaintType.OccurrenceEngineHrs = 0;

            if (!String.IsNullOrEmpty(dr["OccurrenceFixedDateTh"].ToString()))
                retVehicleMaintType.OccurrenceFixedDateTh = Convert.ToInt32(dr["OccurrenceFixedDateTh"]);
            else retVehicleMaintType.OccurrenceFixedDateTh = 0;

            if (!String.IsNullOrEmpty(dr["OccurrenceDurationTh"].ToString()))
                retVehicleMaintType.OccurrenceDurationTh = Convert.ToInt32(dr["OccurrenceDurationTh"]);
            else retVehicleMaintType.OccurrenceDurationTh = 0;

            if (!String.IsNullOrEmpty(dr["OccurrenceKMTh"].ToString()))
                retVehicleMaintType.OccurrenceKMTh = Convert.ToInt32(dr["OccurrenceKMTh"]);
            else retVehicleMaintType.OccurrenceKMTh = 0;

            if (!String.IsNullOrEmpty(dr["OccurrenceEngineHrsTh"].ToString()))
                retVehicleMaintType.OccurrenceEngineHrsTh = Convert.ToInt32(dr["OccurrenceEngineHrsTh"]);
            else retVehicleMaintType.OccurrenceEngineHrsTh = 0;

            retVehicleMaintType.CreatedDate = (DateTime)dr["CreatedDate"];
            retVehicleMaintType.CreatedBy = dr["CreatedBy"].ToString();
            retVehicleMaintType.UpdatedDate = (DateTime)dr["UpdatedDate"];
            retVehicleMaintType.UpdatedBy = dr["UpdatedBy"].ToString();

            return retVehicleMaintType;
        }

        public Boolean SaveVehicleMaintType(VehicleMaintType uVehicleMaintType, Boolean bInsert, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_VehicleMaintTypes";
            if (!dt.Columns.Contains("oprType"))
                dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Columns.Add("MaintTypeId", typeof(int));
            dt.Columns.Add("MaintCatId_cbo", typeof(int));
            dt.Columns.Add("Description", typeof(String));
            dt.Columns.Add("OccurrenceType", typeof(int));
            dt.Columns.Add("OccurrenceFixedDate", typeof(DateTime));
            dt.Columns.Add("OccurrenceDuration", typeof(int));
            dt.Columns.Add("OccurrencePeriod_cbo", typeof(String));
            dt.Columns.Add("OccurrenceKM", typeof(int));
            dt.Columns.Add("OccurrenceEngineHrs", typeof(int));
            dt.Columns.Add("OccurrenceFixedDateTh", typeof(int));
            dt.Columns.Add("OccurrenceDurationTh", typeof(int));
            dt.Columns.Add("OccurrenceKMTh", typeof(int));
            dt.Columns.Add("OccurrenceEngineHrsTh", typeof(int));
            dt.Columns.Add("CreatedDate", typeof(DateTime));
            dt.Columns.Add("CreatedBy", typeof(String));
            dt.Columns.Add("UpdatedDate", typeof(DateTime));
            dt.Columns.Add("UpdatedBy", typeof(String));

            if (bInsert)
            {
                uVehicleMaintType.CreatedDate = DateTime.Now;
                uVehicleMaintType.CreatedBy = Globals.uLogin == null ? String.Empty : Globals.uLogin.Username;
                uVehicleMaintType.UpdatedDate = Constants.NullDateTime;
                uVehicleMaintType.UpdatedBy = String.Empty;
            }
            else
            {
                uVehicleMaintType.UpdatedDate = DateTime.Now;
                uVehicleMaintType.UpdatedBy = Globals.uLogin == null ? String.Empty : Globals.uLogin.Username;
            }

            DataRow dr = dt.NewRow();
            dr["MaintTypeId"] = uVehicleMaintType.MaintTypeId;
            dr["MaintCatId_cbo"] = uVehicleMaintType.MaintCatId_cbo;
            dr["Description"] = uVehicleMaintType.Description;
            dr["OccurrenceType"] = uVehicleMaintType.OccurrenceType;
            dr["OccurrenceFixedDate"] = uVehicleMaintType.OccurrenceFixedDate;
            dr["OccurrenceDuration"] = uVehicleMaintType.OccurrenceDuration;
            dr["OccurrencePeriod_cbo"] = uVehicleMaintType.OccurrencePeriod_cbo;
            dr["OccurrenceKM"] = uVehicleMaintType.OccurrenceKM;
            dr["OccurrenceEngineHrs"] = uVehicleMaintType.OccurrenceEngineHrs;
            dr["OccurrenceFixedDateTh"] = uVehicleMaintType.OccurrenceFixedDateTh;
            dr["OccurrenceDurationTh"] = uVehicleMaintType.OccurrenceDurationTh;
            dr["OccurrenceKMTh"] = uVehicleMaintType.OccurrenceKMTh;
            dr["OccurrenceEngineHrsTh"] = uVehicleMaintType.OccurrenceEngineHrsTh;
            dr["CreatedDate"] = uVehicleMaintType.CreatedDate;
            dr["CreatedBy"] = uVehicleMaintType.CreatedBy;
            dr["UpdatedDate"] = uVehicleMaintType.UpdatedDate;
            dr["UpdatedBy"] = uVehicleMaintType.UpdatedBy;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr);
            DataSet dsProcess = new DataSet();
            DataTable CTRLTBL = _connection.CTRLTBL();
            dsProcess.Merge(dt);

            DataRow drCtrl = CTRLTBL.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "MaintTypeId";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uVehicleMaintType.MaintTypeId;
            CTRLTBL.Rows.Add(drCtrl);

            //--- Group Matrix ---//
            DataTable dtMatrix = new myConverter().ListToDataTable<Matrix>(uVehicleMaintType.lMatrix);
            dtMatrix.TableName = "GFI_SYS_GroupMatrixMaintType";
            drCtrl = CTRLTBL.NewRow();
            drCtrl["SeqNo"] = 2;
            drCtrl["TblName"] = dtMatrix.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "MID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "iID";
            drCtrl["ParentTblName"] = dt.TableName;
            CTRLTBL.Rows.Add(drCtrl);
            dsProcess.Merge(dtMatrix);

            //--- Process Data ---//
            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(CTRLTBL);
            //-----------------------------
            CTRLTBL = _connection.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (CTRLTBL != null)
            {
                DataRow[] dRow = CTRLTBL.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            //dtCTRL = CTRLTBL;
            return bResult;
        }
        public bool SaveVehicleMaintTypeObsolete(VehicleMaintType uVehicleMaintType, DbTransaction transaction)
        {
            DataTable CTRLTBL = new DataTable();
            
            //SaveVehicleMaintType(uVehicleMaintType, out CTRLTBL, transaction);

            if (CTRLTBL != null)
            {
                DataRow[] dRow = CTRLTBL.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                uVehicleMaintType.MaintTypeId = 0;

                if (dRow.Length > 0)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }
        public bool SaveVehicleMaintTypeObsolete(VehicleMaintType uVehicleMaintType, out DataTable dtCTRL, DbTransaction transaction, String sConnStr)
        {
            //--- Prepare Dataset ---//

            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_VehicleMaintTypes";
            if (!dt.Columns.Contains("oprType"))
                dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Columns.Add("MaintTypeId", uVehicleMaintType.MaintTypeId.GetType());
            dt.Columns.Add("MaintCatId_cbo", uVehicleMaintType.MaintCatId_cbo.GetType());
            dt.Columns.Add("Description", uVehicleMaintType.Description.GetType());

            dt.Columns.Add("OccurrenceType", uVehicleMaintType.OccurrenceType.GetType());
            dt.Columns.Add("OccurrenceFixedDate", uVehicleMaintType.OccurrenceFixedDate.GetType());
            dt.Columns.Add("OccurrenceDuration", uVehicleMaintType.OccurrenceDuration.GetType());
            dt.Columns.Add("OccurrencePeriod_cbo", uVehicleMaintType.OccurrencePeriod_cbo.GetType());
            dt.Columns.Add("OccurrenceKM", uVehicleMaintType.OccurrenceKM.GetType());
            dt.Columns.Add("OccurrenceEngineHrs", uVehicleMaintType.OccurrenceEngineHrs.GetType());

            dt.Columns.Add("OccurrenceFixedDateTh", uVehicleMaintType.OccurrenceFixedDateTh.GetType());
            dt.Columns.Add("OccurrenceDurationTh", uVehicleMaintType.OccurrenceDurationTh.GetType());
            dt.Columns.Add("OccurrenceKMTh", uVehicleMaintType.OccurrenceKMTh.GetType());
            dt.Columns.Add("OccurrenceEngineHrsTh", uVehicleMaintType.OccurrenceEngineHrsTh.GetType());

            dt.Columns.Add("CreatedDate", uVehicleMaintType.CreatedDate.GetType());
            dt.Columns.Add("CreatedBy", uVehicleMaintType.CreatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uVehicleMaintType.UpdatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uVehicleMaintType.UpdatedBy.GetType());

            DataRow dr = dt.NewRow();
            dr["oprType"] = DataRowState.Added;
            dr["MaintTypeId"] = 0;
            dr["MaintCatId_cbo"] = uVehicleMaintType.MaintCatId_cbo;
            dr["Description"] = uVehicleMaintType.Description;

            dr["OccurrenceType"] = uVehicleMaintType.OccurrenceType;
            dr["OccurrenceFixedDate"] = uVehicleMaintType.OccurrenceFixedDate;
            dr["OccurrenceDuration"] = uVehicleMaintType.OccurrenceDuration;
            dr["OccurrencePeriod_cbo"] = uVehicleMaintType.OccurrencePeriod_cbo;
            dr["OccurrenceKM"] = uVehicleMaintType.OccurrenceKM;
            dr["OccurrenceEngineHrs"] = uVehicleMaintType.OccurrenceEngineHrs;

            dr["OccurrenceFixedDateTh"] = uVehicleMaintType.OccurrenceFixedDateTh;
            dr["OccurrenceDurationTh"] = uVehicleMaintType.OccurrenceDurationTh;
            dr["OccurrenceKMTh"] = uVehicleMaintType.OccurrenceKMTh;
            dr["OccurrenceEngineHrsTh"] = uVehicleMaintType.OccurrenceEngineHrsTh;

            dr["CreatedDate"] = uVehicleMaintType.CreatedDate;
            dr["CreatedBy"] = uVehicleMaintType.CreatedBy;
            dr["UpdatedDate"] = uVehicleMaintType.UpdatedDate;
            dr["UpdatedBy"] = uVehicleMaintType.UpdatedBy;
            dt.Rows.Add(dr);

            //--- Prepare Control Table ---//

            Connection _connection = new Connection(sConnStr);

            DataSet dsProcess = new DataSet();
            DataTable CTRLTBL = _connection.CTRLTBL();

            dsProcess.Tables.Add(CTRLTBL);
            dsProcess.Merge(dt);

            DataRow drCtrl = CTRLTBL.NewRow(); 
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "MaintTypeId";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "GET";
            CTRLTBL.Rows.Add(drCtrl);

            //--- Group Matrix ---//

            DataTable dtMatrix = new myConverter().ListToDataTable<Matrix>(uVehicleMaintType.lMatrix);
            dtMatrix.TableName = "GFI_SYS_GroupMatrixMaintType";
            drCtrl = CTRLTBL.NewRow();
            drCtrl["SeqNo"] = 2;
            drCtrl["TblName"] = dtMatrix.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "MID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "iID";
            drCtrl["ParentTblName"] = dt.TableName;
            CTRLTBL.Rows.Add(drCtrl);
            dsProcess.Merge(dtMatrix);

            //--- Process Data ---//

            dtCTRL = _connection.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            if (dtCTRL == null)
                return false;
            else
                return true;
        }

        public bool UpdateVehicleMaintType(VehicleMaintType uVehicleMaintType, DbTransaction transaction, String sConnStr)
        {
            //--- Prepare Dataset ---//

            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_VehicleMaintTypes";
            if (!dt.Columns.Contains("oprType"))
                dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Columns.Add("MaintTypeId", uVehicleMaintType.MaintTypeId.GetType());
            dt.Columns.Add("MaintCatId_cbo", uVehicleMaintType.MaintCatId_cbo.GetType());
            dt.Columns.Add("Description", uVehicleMaintType.Description.GetType());

            dt.Columns.Add("OccurrenceType", uVehicleMaintType.OccurrenceType.GetType());
            dt.Columns.Add("OccurrenceFixedDate", uVehicleMaintType.OccurrenceFixedDate.GetType());
            dt.Columns.Add("OccurrenceDuration", uVehicleMaintType.OccurrenceDuration.GetType());
            dt.Columns.Add("OccurrencePeriod_cbo", uVehicleMaintType.OccurrencePeriod_cbo.GetType());
            dt.Columns.Add("OccurrenceKM", uVehicleMaintType.OccurrenceKM.GetType());
            dt.Columns.Add("OccurrenceEngineHrs", uVehicleMaintType.OccurrenceEngineHrs.GetType());
            
            dt.Columns.Add("OccurrenceFixedDateTh", uVehicleMaintType.OccurrenceFixedDateTh.GetType());
            dt.Columns.Add("OccurrenceDurationTh", uVehicleMaintType.OccurrenceDurationTh.GetType());
            dt.Columns.Add("OccurrenceKMTh", uVehicleMaintType.OccurrenceKMTh.GetType());
            dt.Columns.Add("OccurrenceEngineHrsTh", uVehicleMaintType.OccurrenceEngineHrsTh.GetType());
            
            dt.Columns.Add("CreatedDate", uVehicleMaintType.CreatedDate.GetType());
            dt.Columns.Add("CreatedBy", uVehicleMaintType.CreatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uVehicleMaintType.UpdatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uVehicleMaintType.UpdatedBy.GetType());
            
            DataRow dr = dt.NewRow();

            dr["oprType"] = DataRowState.Modified;
            dr["MaintTypeId"] = uVehicleMaintType.MaintTypeId;
            dr["MaintCatId_cbo"] = uVehicleMaintType.MaintCatId_cbo;
            dr["Description"] = uVehicleMaintType.Description;

            dr["OccurrenceType"] = uVehicleMaintType.OccurrenceType;
            dr["OccurrenceFixedDate"] = uVehicleMaintType.OccurrenceFixedDate;
            dr["OccurrenceDuration"] = uVehicleMaintType.OccurrenceDuration;
            dr["OccurrencePeriod_cbo"] = uVehicleMaintType.OccurrencePeriod_cbo;
            dr["OccurrenceKM"] = uVehicleMaintType.OccurrenceKM;
            dr["OccurrenceEngineHrs"] = uVehicleMaintType.OccurrenceEngineHrs;
            
            dr["OccurrenceFixedDateTh"] = uVehicleMaintType.OccurrenceFixedDateTh;
            dr["OccurrenceDurationTh"] = uVehicleMaintType.OccurrenceDurationTh;
            dr["OccurrenceKMTh"] = uVehicleMaintType.OccurrenceKMTh;
            dr["OccurrenceEngineHrsTh"] = uVehicleMaintType.OccurrenceEngineHrsTh;
            
            dr["CreatedDate"] = uVehicleMaintType.CreatedDate;
            dr["CreatedBy"] = uVehicleMaintType.CreatedBy;
            dr["UpdatedDate"] = uVehicleMaintType.UpdatedDate;
            dr["UpdatedBy"] = uVehicleMaintType.UpdatedBy;
            dt.Rows.Add(dr);

            //--- Prepare Control Table ---//

            Connection _connection = new Connection(sConnStr);

            DataSet dsProcess = new DataSet();
            DataTable CTRLTBL = _connection.CTRLTBL();
            
            dsProcess.Tables.Add(CTRLTBL);
            dsProcess.Merge(dt);

            DataRow drCtrl = CTRLTBL.NewRow(); 
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "MaintTypeId";
            drCtrl["KeyIsIdentity"] = "TRUE";
            CTRLTBL.Rows.Add(drCtrl);

            //--- Group Matrix ---//

            DataTable dtMatrix = new myConverter().ListToDataTable<Matrix>(uVehicleMaintType.lMatrix);
            dtMatrix.TableName = "GFI_SYS_GroupMatrixMaintType";
            drCtrl = CTRLTBL.NewRow();
            drCtrl["SeqNo"] = 2;
            drCtrl["TblName"] = dtMatrix.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "MID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            CTRLTBL.Rows.Add(drCtrl);
            dsProcess.Merge(dtMatrix);

            //--- Process Data ---//

            CTRLTBL = _connection.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            if (CTRLTBL != null)
            {
                DataRow[] dRow = CTRLTBL.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }

        public bool DeleteVehicleMaintType(VehicleMaintType uVehicleMaintType, String sConnStr)
        {
            //--- Prepare Dataset ---//

            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_VehicleMaintTypes";
            dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Columns.Add("MaintTypeId", uVehicleMaintType.MaintTypeId.GetType());

            DataRow dr = dt.NewRow();
            dr["oprType"] = DataRowState.Deleted;
            dr["MaintTypeId"] = uVehicleMaintType.MaintTypeId;

            dt.Rows.Add(dr);

            //--- Prepare Control Table ---//

            Connection _connection = new Connection(sConnStr);

            DataTable CTRLTBL = _connection.CTRLTBL();
            CTRLTBL.TableName = "CTRLTBL";

            DataSet objDataSet = new DataSet();

            objDataSet.Tables.Add(CTRLTBL);
            objDataSet.Merge(dt);

            DataRow drCtrl=  CTRLTBL.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "MaintTypeId";
            drCtrl["KeyIsIdentity"] = "TRUE";
            CTRLTBL.Rows.Add(drCtrl);

            //--- Process Data ---//

            CTRLTBL = _connection.ProcessData(objDataSet, sConnStr).Tables["CTRLTBL"];

            if (CTRLTBL != null)
            {
                DataRow[] dRow = CTRLTBL.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }
    }
}
