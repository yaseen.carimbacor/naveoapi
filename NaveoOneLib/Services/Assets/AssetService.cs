using System;
using System.Collections.Generic;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Users;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Services.Audits;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Services.Users;
using System.Linq;

namespace NaveoOneLib.Services.Assets
{
    public class AssetService
    {
        Errors er = new Errors();

        public DataTable GetUOM(String sConnStr)
        {
            String sqlString = @"SELECT UID, 
                                        Description                                       
                                    from GFI_SYS_UOM 
                                        order by UID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        public String sGetAsset(List<Matrix> lMatrix, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            return sGetAsset(iParentIDs, sConnStr);
        }

        //Reza. To use UID when web migration complete
        public String sGetAsset(List<int> iParentIDs, String sConnStr)
        {
            String sql = @"SELECT Distinct AssetID 
								    , AssetNumber, AssetName
								    , AssetType 
	                                , a.Status_b, TimeZoneID, Odometer
                                from GFI_FLT_Asset a
	                                inner join GFI_SYS_GroupMatrixAsset m on a.AssetID = m.iID
                                where m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + ") order by AssetID";

            return sql;
        }
        public TimeSpan tsAsset(int AssetID, String sConnStr)
        {
            String sql = "select TimeZoneID from GFI_FLT_Asset where AssetID = " + AssetID;
            DataTable dt = new Connection(sConnStr).GetDataDT(sql, sConnStr);

            String tzName = dt.Rows[0]["TimeZoneID"].ToString();
            if (tzName == String.Empty)
                tzName = TimeZone.CurrentTimeZone.StandardName;
            TimeSpan TimeZoneTS = new Utils().GetTimeSpan(tzName);
            return TimeZoneTS;
        }

        public DataTable GetAllAssets(String sConnStr)
        {
            String sqlString = @"SELECT *  from GFI_FLT_Asset order by AssetID";
            DataTable dt = new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
            //foreach (DataRow dr in dt.Rows)
            //    dr["AssetNumber"] = BaseService.Decrypt(dr["AssetNumber"].ToString());
            return dt;
        }

        public DataTable GetAsset(List<Matrix> lMatrix, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            return GetAsset(iParentIDs, sConnStr);
        }
        public DataTable GetAsset(List<int> iParentIDs, String sConnStr)
        {
            DataTable dt = new Connection(sConnStr).GetDataDT(sGetAsset(iParentIDs, sConnStr), sConnStr);
            return dt;
        }

        public DataSet GetAssetByID(int AssetID, String sConnStr)
        {
            String sql = @"
declare @AssetID int
set @AssetID = 77	

select * from GFI_FLT_Asset where AssetID = @AssetID
select * from GFI_FLT_AssetExt where AssetID = @AssetID
select * from GFI_FLT_AssetUOM where AssetID = @AssetID
select * from GFI_SYS_GroupMatrixAsset where iID = @AssetID
select * from GFI_FLT_AssetDeviceMap where AssetID = @AssetID
select * from GFI_FLT_DeviceAuxilliaryMap d inner join GFI_FLT_AssetDeviceMap m on m.MapID = d.MapID where m.AssetID = @AssetID
select * from GFI_FLT_CalibrationChart c inner join GFI_FLT_DeviceAuxilliaryMap d on c.AuxID = d.Uid inner join GFI_FLT_AssetDeviceMap m on m.MapID = d.MapID where m.AssetID = @AssetID
";
            sql = sql.Replace("77", AssetID.ToString());

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();
            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds;
        }
        Asset GetAsset(DataSet ds)
        {

            //ds has tables GPSAsset and DeviceMap
            Asset retAsset = new Asset();




            retAsset.AssetID = Convert.ToInt32(ds.Tables["GPSAsset"].Rows[0]["AssetID"]);
            retAsset.AssetNumber = BaseService.Decrypt(ds.Tables["GPSAsset"].Rows[0]["AssetNumber"].ToString());
            retAsset.AssetName = BaseService.Decrypt(ds.Tables["GPSAsset"].Rows[0]["AssetName"].ToString());
            retAsset.AssetType = Convert.ToInt32(ds.Tables["GPSAsset"].Rows[0]["AssetType"]);

            List<Matrix> lMatrix = new List<Matrix>();
            MatrixService ms = new MatrixService();
            foreach (DataRow dr in ds.Tables["dtMatrix"].Rows)
                lMatrix.Add(ms.AssignMatrix(dr));
            retAsset.lMatrix = lMatrix;

            //retAsset.VehicleTypeId = String.IsNullOrEmpty(ds.Tables["GPSAsset"].Rows[0]["VehicleTypeId"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["GPSAsset"].Rows[0]["VehicleTypeId"]);

            if (ds.Tables.Contains("dtMake"))
                if (ds.Tables["dtMake"].Rows.Count > 0)
                {
                    retAsset.MakeID = String.IsNullOrEmpty(ds.Tables["dtMake"].Rows[0]["VID"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtMake"].Rows[0]["VID"]);
                    retAsset.sMake = String.IsNullOrEmpty(ds.Tables["dtMake"].Rows[0]["Name"].ToString()) ? String.Empty : ds.Tables["dtMake"].Rows[0]["Name"].ToString();
                }
            if (ds.Tables.Contains("dtModel"))
                if (ds.Tables["dtModel"].Rows.Count > 0)
                {
                    retAsset.ModelID = String.IsNullOrEmpty(ds.Tables["dtModel"].Rows[0]["VID"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtModel"].Rows[0]["VID"]);
                    retAsset.sModel = String.IsNullOrEmpty(ds.Tables["dtModel"].Rows[0]["Name"].ToString()) ? String.Empty : ds.Tables["dtModel"].Rows[0]["Name"].ToString();
                }

            if (ds.Tables.Contains("dtColor"))
                if (ds.Tables["dtColor"].Rows.Count > 0)
                {
                    retAsset.ColorID = String.IsNullOrEmpty(ds.Tables["dtColor"].Rows[0]["VID"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtColor"].Rows[0]["VID"]);
                    retAsset.sColor = String.IsNullOrEmpty(ds.Tables["dtColor"].Rows[0]["Name"].ToString()) ? String.Empty : ds.Tables["dtColor"].Rows[0]["Name"].ToString();
                }

            if (ds.Tables.Contains("dtRoadTypeSpeed"))
                if (ds.Tables["dtRoadTypeSpeed"].Rows.Count > 0)
                {
                    retAsset.RoadSpeedTypeId = String.IsNullOrEmpty(ds.Tables["dtRoadTypeSpeed"].Rows[0]["iID"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtRoadTypeSpeed"].Rows[0]["iID"]);
                    retAsset.sRoadSpeedType = String.IsNullOrEmpty(ds.Tables["dtRoadTypeSpeed"].Rows[0]["VehicleType"].ToString()) ? String.Empty : ds.Tables["dtRoadTypeSpeed"].Rows[0]["VehicleType"].ToString();
                }

            if (ds.Tables.Contains("dtVehicleType"))
                if (ds.Tables["dtVehicleType"].Rows.Count > 0)
                    retAsset.VehicleType = ds.Tables["dtVehicleType"].Rows[0]["Description"].ToString();

            if (ds.Tables.Contains("dtUOM"))
                if (ds.Tables["dtUOM"].Rows.Count > 0)
                    retAsset.lUOM = new myConverter().ConvertToList<AssetUOM>(ds.Tables["dtUOM"]);

            if (ds.Tables.Contains("dtAllUOM"))
                if (ds.Tables["dtAllUOM"].Rows.Count > 0)
                    retAsset.dtAllUOM = ds.Tables["dtAllUOM"];




            retAsset.Status_b = ds.Tables["GPSAsset"].Rows[0]["Status_b"].ToString();
            retAsset.TimeZoneID = ds.Tables["GPSAsset"].Rows[0]["TimeZoneID"].ToString();
            retAsset.TimeZoneTS = new Utils().GetTimeSpan(retAsset.TimeZoneID);
            retAsset.Odometer = Convert.ToInt32(ds.Tables["GPSAsset"].Rows[0]["Odometer"]);
            retAsset.CreatedDate = (DateTime)ds.Tables["GPSAsset"].Rows[0]["CreatedDate"];
            if (!String.IsNullOrEmpty(ds.Tables["GPSAsset"].Rows[0]["CreatedBy"].ToString()))
                retAsset.CreatedBy = Convert.ToInt32(ds.Tables["GPSAsset"].Rows[0]["CreatedBy"].ToString());

            if (!String.IsNullOrEmpty(ds.Tables["GPSAsset"].Rows[0]["UpdatedDate"].ToString()))
                retAsset.UpdatedDate = (DateTime)ds.Tables["GPSAsset"].Rows[0]["UpdatedDate"];

            if (!String.IsNullOrEmpty(ds.Tables["GPSAsset"].Rows[0]["UpdatedBy"].ToString()))
                retAsset.UpdatedBy = Convert.ToInt32(ds.Tables["GPSAsset"].Rows[0]["UpdatedBy"]);

            if (ds.Tables.Contains("AssetType"))
                if (ds.Tables["AssetType"].Rows.Count > 0)
                    retAsset.AssetTypeMappingObject = AssetTypeMappingService.GetAssetTypeMapping(ds.Tables["AssetType"].Rows[0]);

            if (ds.Tables.Contains("dtUsers"))
                retAsset.dtUsers = ds.Tables["dtUsers"];

            foreach (DataRow dr in ds.Tables["DeviceMap"].Rows)
            {
                AssetDeviceMap retAssetDeviceMap = new AssetDeviceMap();
                retAssetDeviceMap.MapID = Convert.ToInt32(dr["MapID"]);
                retAssetDeviceMap.AssetID = Convert.ToInt32(dr["AssetID"]);
                retAssetDeviceMap.DeviceID = dr["DeviceID"].ToString();
                retAssetDeviceMap.SerialNumber = dr["SerialNumber"].ToString();
                retAssetDeviceMap.Status_b = dr["Status_b"].ToString();
                if (!String.IsNullOrEmpty(dr["CreatedBy"].ToString()))
                    retAssetDeviceMap.CreatedBy = Convert.ToInt32(dr["CreatedBy"].ToString());
                retAssetDeviceMap.CreatedDate = (DateTime)dr["CreatedDate"];
                if (!String.IsNullOrEmpty(dr["UpdatedBy"].ToString()))
                    retAssetDeviceMap.UpdatedBy = Convert.ToInt32(dr["UpdatedBy"].ToString());
                if (!String.IsNullOrEmpty(dr["UpdatedDate"].ToString()))
                    retAssetDeviceMap.UpdatedDate = (DateTime)dr["UpdatedDate"];
                retAssetDeviceMap.StartDate = (DateTime)dr["StartDate"];
                retAssetDeviceMap.EndDate = (DateTime)dr["EndDate"];

                if (ds.Tables.Contains("DeviceAux"))
                {
                    //Check if this device has any aux
                    DataRow[] auxresult = ds.Tables["DeviceAux"].Select("MapID='" + retAssetDeviceMap.MapID + "'");
                    if (auxresult.Length > 0)
                    {
                        foreach (DataRow a in auxresult)
                        {
                            DeviceAuxilliaryMap aux = new DeviceAuxilliaryMap();
                            aux.MapId = Convert.ToInt32(a["MapID"].ToString());
                            aux.Auxilliary = a["Auxilliary"].ToString();

                            if (!String.IsNullOrEmpty(a["AuxilliaryType"].ToString()))
                                aux.AuxilliaryType = Convert.ToInt32(a["AuxilliaryType"].ToString());

                            if (!String.IsNullOrEmpty(a["Capacity"].ToString()))
                                aux.Capacity = Convert.ToInt32(a["Capacity"].ToString());

                            //aux.CreatedDate = Convert.ToDateTime(a["CreatedDate"].ToString());
                            aux.DeviceId = dr["DeviceID"].ToString();
                            aux.Field1 = a["Field1"].ToString();
                            aux.Field2 = a["Field2"].ToString();
                            if (ds.Tables.Contains("dtCalibrationProduct"))
                                if (ds.Tables["dtCalibrationProduct"].Rows.Count > 0)
                                {
                                    aux.Field3 = String.IsNullOrEmpty(ds.Tables["dtCalibrationProduct"].Rows[0]["VID"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtCalibrationProduct"].Rows[0]["VID"]);
                                    aux.Field3Desc = String.IsNullOrEmpty(ds.Tables["dtCalibrationProduct"].Rows[0]["Name"].ToString()) ? String.Empty : ds.Tables["dtCalibrationProduct"].Rows[0]["Name"].ToString();
                                }

                            aux.Field4 = a["Field4"].ToString() != "" ? Convert.ToInt32(a["Field4"].ToString()) : Constants.NullInt;
                            aux.Field5 = a["Field5"].ToString();
                            aux.Field6 = a["Field6"].ToString() != "" ? Convert.ToDateTime(a["Field6"].ToString()) : DateTime.Now;
                            aux.Uid = Convert.ToInt32(a["Uid"].ToString());

                            if (!String.IsNullOrEmpty(a["Uom"].ToString()))
                                aux.Uom = Convert.ToInt32(a["Uom"].ToString());

                            aux.UpdatedDate = a["UpdatedDate"].ToString() != "" ? Convert.ToDateTime(a["UpdatedDate"].ToString()) : Constants.NullDateTime;
                            aux.Thresholdhigh = a["Thresholdhigh"].ToString() != "" ? Convert.ToInt32(a["Thresholdhigh"].ToString()) : 0;
                            aux.Thresholdlow = a["Thresholdlow"].ToString() != "" ? Convert.ToInt32(a["Thresholdlow"].ToString()) : 0;

                            if (!String.IsNullOrEmpty(a["CreatedBy"].ToString()))
                                aux.CreatedBy = Convert.ToInt32(a["CreatedBy"].ToString());
                            if (!String.IsNullOrEmpty(a["UpdatedBy"].ToString()))
                                aux.UpdatedBy = Convert.ToInt32(a["UpdatedBy"].ToString());

                            retAssetDeviceMap.deviceAuxilliaryMap = aux;



                            //Check if this device has any aux
                            DataRow[] calresult = ds.Tables["Calibration"].Select("AuxID='" + aux.Uid + "'");
                            if (calresult.Length > 0)
                            {
                                foreach (DataRow c in calresult)
                                {
                                    CalibrationChart cal = new CalibrationChart();

                                    cal.AuxID = aux.Uid;
                                    cal.CalType = c["CalType"].ToString();
                                    cal.ConvertedUOM = Convert.ToInt32(c["ConvertedUOM"].ToString());
                                    cal.ConvertedValue = (float)Convert.ToDouble(c["ConvertedValue"].ToString());
                                    cal.CreatedDate = c["CreatedDate"].ToString() != "" ? Convert.ToDateTime(c["CreatedDate"].ToString()) : (DateTime?)null;
                                    cal.DeviceID = dr["DeviceID"].ToString();
                                    cal.IID = Convert.ToInt32(c["IID"].ToString());
                                    cal.Reading = (float)Convert.ToDouble(c["Reading"].ToString());
                                    cal.ReadingUOM = Convert.ToInt32(c["ReadingUOM"].ToString());

                                    if (!String.IsNullOrEmpty(c["SeuqenceNo"].ToString()))
                                        cal.SeuqenceNo = Convert.ToInt32(c["SeuqenceNo"].ToString());

                                    cal.UpdatedDate = a["UpdatedDate"].ToString() != "" ? Convert.ToDateTime(a["UpdatedDate"].ToString()) : (DateTime?)null;

                                    if (!String.IsNullOrEmpty(c["CreatedBy"].ToString()))
                                        cal.CreatedBy = Convert.ToInt32(c["CreatedBy"].ToString());
                                    if (!String.IsNullOrEmpty(c["UpdatedBy"].ToString()))
                                        cal.UpdatedBy = Convert.ToInt32(c["UpdatedBy"].ToString());

                                    aux.lCalibration.Add(cal);
                                }
                            }
                        }

                    }
                }

                retAsset.assetDeviceMap = retAssetDeviceMap;
            }

            List<AssetZoneMap> lZoneMap = new List<AssetZoneMap>();
            if (ds.Tables.Contains("ZoneMap"))
                if (ds.Tables["ZoneMap"].Rows.Count > 0)
                {
                    retAsset.lAssetZoneMap = new List<AssetZoneMap>();
                    foreach (DataRow dr in ds.Tables["ZoneMap"].Rows)
                    {
                        AssetZoneMap retAssetZoneMap = new AssetZoneMap();
                        retAssetZoneMap.Id = Convert.ToInt32(dr["Id"]);
                        retAssetZoneMap.AssetID = Convert.ToInt32(dr["AssetID"]);
                        retAssetZoneMap.ZoneID = Convert.ToInt32(dr["ZoneID"]);
                        retAssetZoneMap.Param1 = dr["Param1"].ToString();
                        retAssetZoneMap.Param2 = dr["Param2"].ToString();
                        retAssetZoneMap.Param3 = dr["Param3"].ToString();
                        retAssetZoneMap.CreatedBy = dr["CreatedBy"].ToString();
                        retAssetZoneMap.CreatedDate = (DateTime)dr["CreatedDate"];
                        retAssetZoneMap.ModifiedBy = dr["ModifiedBy"].ToString();
                        retAssetZoneMap.ModifiedDate = (DateTime)dr["ModifiedDate"];
                        lZoneMap.Add(retAssetZoneMap);
                    }
                    retAsset.lAssetZoneMap = lZoneMap;
                }
            if (ds.Tables.Contains("AssetExt"))
                foreach (DataRow dr in ds.Tables["AssetExt"].Rows)
                {
                    retAsset.YearManufactured = String.IsNullOrEmpty(dr["YearManufactured"].ToString()) ? (int?)null : Convert.ToInt32(dr["YearManufactured"]);
                    retAsset.PurchasedDate = String.IsNullOrEmpty(dr["PurchasedDate"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dr["PurchasedDate"]);
                    retAsset.PurchasedValue = String.IsNullOrEmpty(dr["PurchasedValue"].ToString()) ? (int?)null : Convert.ToInt32(dr["PurchasedValue"]);
                    retAsset.RefA = dr["RefA"].ToString();
                    retAsset.RefB = dr["RefB"].ToString();
                    retAsset.RefC = dr["RefC"].ToString();
                    retAsset.RefD = dr["RefD"].ToString();
                    retAsset.ExternalRef = dr["ExternalRef"].ToString();
                    retAsset.EngineSerialNumber = dr["EngineSerialNumber"].ToString();
                    retAsset.EngineCapacity = String.IsNullOrEmpty(dr["EngineCapacity"].ToString()) ? (int?)null : Convert.ToInt32(dr["EngineCapacity"]);
                    retAsset.EnginePower = String.IsNullOrEmpty(dr["EnginePower"].ToString()) ? (int?)null : Convert.ToInt32(dr["EnginePower"]);
                }


            return retAsset;
        }
        public Asset GetAssetById(int AssetID, int UserID, Boolean bComplete, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            //Asset
            String sql = @"SELECT AssetID
                            , AssetNumber, AssetName
							, AssetType 
	                        , a.Status_b, TimeZoneID, Odometer
	                        , a.CreatedDate 
	                        , a.CreatedBy 
	                        , u.Email eCreatedBy 
	                        , a.UpdatedDate 
	                        , a.UpdatedBy 
	                        , u1.Email eUpdatedBy 
                            , a.VehicleTypeId
                            from GFI_FLT_Asset a
	                            inner join GFI_SYS_GroupMatrixAsset m on a.AssetID = m.iID
	                            left outer join GFI_SYS_User u on u.UID = a.CreatedBy
	                            left outer join GFI_SYS_User u1 on u1.UID = a.UpdatedBy
							WHERE AssetID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = AssetID.ToString();
            drSql["sTableName"] = "GPSAsset";
            dtSql.Rows.Add(drSql);

            //Device Map
            sql = @"SELECT MapID, 
								AssetID, 
								DeviceID, 
								SerialNumber, 
								Status_b, 								
								CreatedBy, 
								CreatedDate, 
								UpdatedBy, 
								UpdatedDate, StartDate, EndDate, MaxNoLogDays
							 from GFI_FLT_AssetDeviceMap 
								WHERE AssetId = ? And Status_b = 'AC' and StartDate < GetDate() and EndDate > Getdate() 
								order by AssetId ";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = AssetID.ToString();
            drSql["sTableName"] = "DeviceMap";
            dtSql.Rows.Add(drSql);

            //Device Auxilliary
            sql = @"SELECT [Uid]
                          ,a.[MapID]
                          ,[Auxilliary]
                          ,[AuxilliaryType]
                          ,[Field1]
                          ,[Field2]
                          ,[Field3]
                          ,[Field4]
                          ,[Field5]
                          ,[Field6]
                          ,a.[CreatedDate]
                          ,a.[CreatedBy]
                          ,a.[UpdatedDate]
                          ,a.[UpdatedBy]
                          ,[Uom]
                          ,[Thresholdhigh]
                          ,[Thresholdlow]
                          ,[Capacity]
                          FROM GFI_FLT_DeviceAuxilliaryMap a,GFI_FLT_AssetDeviceMap d
                          where a.MapId = d.MapID
                          and d.AssetID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = AssetID.ToString();
            drSql["sTableName"] = "DeviceAux";
            dtSql.Rows.Add(drSql);

            //Calibration
            sql = @"SELECT c.[IID]                         
                          ,c.[AuxID]
                          ,c.[CalType]
                          ,c.[SeuqenceNo]
                          ,c.[ReadingUOM]
                          ,c.[Reading]
                          ,c.[ConvertedValue]
                          ,c.[ConvertedUOM]
                          ,c.[CreatedBy]
                          ,c.[CreatedDate]
                          ,c.[UpdatedBy]
                          ,c.[UpdatedDate]
                          FROM GFI_FLT_CalibrationChart c,GFI_FLT_DeviceAuxilliaryMap d,GFI_FLT_AssetDeviceMap a
                          where c.AuxID = d.Uid
                          and d.MapId = a.MapID
                          and a.AssetID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = AssetID.ToString();
            drSql["sTableName"] = "Calibration";
            dtSql.Rows.Add(drSql);


            //RoadTypeSpeed
            sql = @"select rS.iID,
                     rS.VehicleType,
                     rS.MotorWay,
                     rS.RoadA,
                     rS.RoadB,
                     rS.RoadC,
                     rS.RoadD,
                     rS.RoadE,
                     rS.RoadE,
                     rS.RoadG,
                     rS.RoadH
                     FROM GFI_FLT_Asset a
                     inner join GFI_FLT_RoadSpeedType rS
                     on a.RoadSpeedType = rS.iID
                     where a.AssetID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = AssetID.ToString();
            drSql["sTableName"] = "dtRoadTypeSpeed";
            dtSql.Rows.Add(drSql);

            //Matrix
            sql = new MatrixService().sGetMatrixId(AssetID, "GFI_SYS_GroupMatrixAsset");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);

            if (bComplete)
            {
                //dtUsers
                sql = UserService.sGetUsersIDnDesc(UserID, sConnStr);
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtUsers";
                dtSql.Rows.Add(drSql);

                //AssetType
                sql = @"select t.* from GFI_FLT_AssetTypeMapping t
	                    inner join GFI_FLT_Asset a on t.IID = a.AssetType
                    where a.AssetID = " + AssetID + "";
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "AssetType";
                dtSql.Rows.Add(drSql);

                //AssetExt
                sql = "select * from GFI_FLT_AssetExt where AssetID = " + AssetID + "";
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "AssetExt";
                dtSql.Rows.Add(drSql);

                //Type
                sql = "select * from GFI_AMM_VehicleTypes where VehicleTypeId = (select VehicleTypeId from GFI_FLT_Asset where AssetID = " + AssetID + ")";
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtVehicleType";
                dtSql.Rows.Add(drSql);

                //Make
                sql = "select * from GFI_SYS_LookUpValues where VID = (select MakeID from GFI_FLT_AssetExt where AssetID = " + AssetID + ")";
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtMake";
                dtSql.Rows.Add(drSql);

                //Model
                sql = "select * from GFI_SYS_LookUpValues where VID = (select ModelID from GFI_FLT_AssetExt where AssetID = " + AssetID + ")";
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtModel";
                dtSql.Rows.Add(drSql);

                //Color
                sql = "select * from GFI_SYS_LookUpValues where VID = (select ColorID from GFI_FLT_AssetExt where AssetID = " + AssetID + ")";
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtColor";
                dtSql.Rows.Add(drSql);

                //CalibrationProduct
                sql = "select * from GFI_SYS_LookUpValues lV inner join GFI_FLT_DeviceAuxilliaryMap dam  on lv.VID = dam.Field3 inner join GFI_FLT_AssetDeviceMap adm on dam.MapID = adm.MapID where adm.AssetID = " + AssetID + "";
                //"select * from GFI_SYS_LookUpValues where VID = (select Field3 from GFI_FLT_AssetDeviceMap adm inner join GFI_FLT_DeviceAuxilliaryMap dam on adm.MapID = dam.MapID where adm.AssetID = " + AssetID + ")";
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtCalibrationProduct";
                dtSql.Rows.Add(drSql);

                //UOM
                sql = "select * from GFI_FLT_AssetUOM where AssetID = " + AssetID + "";
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtUOM";
                dtSql.Rows.Add(drSql);

                //AllUOM
                sql = "select * from GFI_SYS_UOM";
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtAllUOM";
                dtSql.Rows.Add(drSql);

                //ZoneMap          
                sql = @"SELECT Id, 
								AssetID, 
								ZoneID, 
								Param1, 
								Param2, 
								Param3, 
								CreatedBy, 
								CreatedDate, 
								ModifiedBy, 
								ModifiedDate
							 from GFI_AMM_AssetZoneMap 
								WHERE AssetId = ?  
								order by AssetId ";
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sParam"] = AssetID.ToString();
                drSql["sTableName"] = "ZoneMap";
                dtSql.Rows.Add(drSql);
            }

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables["GPSAsset"].Rows.Count == 0)
                return null;
            else
                return GetAsset(ds);
        }
        public BaseModel Save(Asset uAsset, Boolean bInsert, int uLogin, String sConnStr)
        {
            BaseModel baseModel = new BaseModel();
            Boolean bResult = false;

            DataTable dt = AssetDT();

            if (bInsert)
            {
                uAsset.CreatedDate = DateTime.Now;
                uAsset.CreatedBy = uLogin;
                uAsset.UpdatedDate = Constants.NullDateTime;
                uAsset.oprType = DataRowState.Added;
            }
            else
            {
                uAsset.UpdatedDate = DateTime.Now;
                uAsset.UpdatedBy = uLogin;
            }

            if (String.IsNullOrEmpty(uAsset.TimeZoneID))
                uAsset.TimeZoneID = TimeZone.CurrentTimeZone.StandardName;

            DataRow dr = dt.NewRow();
            dr["AssetID"] = uAsset.AssetID;
            if (uAsset.AssetName == String.Empty)
                uAsset.AssetName = uAsset.AssetNumber;
            else
                uAsset.AssetName = uAsset.AssetName;
            dr["AssetNumber"] = uAsset.AssetNumber.ToString();
            dr["AssetName"] = uAsset.AssetName;
            dr["AssetType"] = uAsset.AssetType;
            if (String.IsNullOrEmpty(uAsset.Status_b))
                uAsset.Status_b = "AC";
            dr["Status_b"] = uAsset.Status_b;
            dr["TimeZoneID"] = uAsset.TimeZoneID;
            dr["Odometer"] = 0; // uAsset.Odometer;
            dr["CreatedDate"] = uAsset.CreatedDate;
            if (uAsset.CreatedBy != null)
                dr["CreatedBy"] = uAsset.CreatedBy;

            dr["UpdatedDate"] = uAsset.UpdatedDate;

            if (uAsset.UpdatedBy != null)
                dr["UpdatedBy"] = uAsset.UpdatedBy;

            dr["oprType"] = uAsset.oprType;
            dr["VehicleTypeId"] = uAsset.VehicleTypeId != null ? uAsset.VehicleTypeId : (object)DBNull.Value;
            dr["ColorID"] = uAsset.ColorID != null ? uAsset.ColorID : (object)DBNull.Value;
            dr["RoadSpeedType"] = uAsset.RoadSpeedTypeId != null ? uAsset.RoadSpeedTypeId : (object)DBNull.Value;

            dt.Rows.Add(dr);

            if (bInsert)
                dt.Columns.Remove("UpdatedDate");
            else
                dt.Columns.Remove("CreatedDate");

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AssetId";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uAsset.AssetID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            //Matrix
            uAsset.lMatrix = Validations.lValidateMatrix(uLogin, uAsset.lMatrix, sConnStr);
            //Foreign Key
            if (!bInsert)
                foreach (Matrix m in uAsset.lMatrix)
                    if (m.iID != uAsset.AssetID)
                        m.iID = uAsset.AssetID;
            DataTable dtMatrix = new myConverter().ListToDataTable<Matrix>(uAsset.lMatrix);
            if (dtMatrix.Rows.Count == 0)
            {
                baseModel.errorMessage += "Asset does not have any matrix.";
                return baseModel;
            }
            dtMatrix.TableName = "GFI_SYS_GroupMatrixAsset";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 2;
            drCtrl["TblName"] = dtMatrix.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "MID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "SET";
            else
                drCtrl["NextIdValue"] = uAsset.AssetID;
            drCtrl["NextIdFieldName"] = "iID";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtMatrix);

            //Add DAM if new
            if (uAsset.assetDeviceMap.deviceAuxilliaryMap.Uid == 0)
                uAsset.assetDeviceMap.deviceAuxilliaryMap.oprType = DataRowState.Added;

            //Delete DAM if there is no callibration
            if (uAsset.assetDeviceMap.deviceAuxilliaryMap.lCalibration.Count == 0)
                uAsset.assetDeviceMap.deviceAuxilliaryMap.oprType = DataRowState.Deleted;

            #region  Device Map
            if (uAsset.assetDeviceMap != null)
            {
                //Foreign Key
                if (!bInsert)
                    if (uAsset.assetDeviceMap.AssetID != uAsset.AssetID)
                        uAsset.assetDeviceMap.AssetID = uAsset.AssetID;

                //need to add a new line in Ctrl to to ensure that only 1 device id is saved on db using CmdType Chk
                String sDeviceID = uAsset.assetDeviceMap.DeviceID;
                if (sDeviceID.Length > 0)
                {
                    drCtrl = dtCtrl.NewRow();
                    drCtrl["SeqNo"] = 6;
                    drCtrl["TblName"] = "ChkIfDeviceIDExist";
                    drCtrl["CmdType"] = "Chk";
                    drCtrl["Command"] = "select * from GFI_FLT_AssetDeviceMap where DeviceID in ('" + sDeviceID + "') and Status_b = 'AC' and StartDate < GetDate() and EndDate > Getdate() and AssetID != " + uAsset.AssetID;
                    dtCtrl.Rows.Add(drCtrl);
                }

                #region dtCreation
                //Device Map Data
                DataTable dtADMap = new DataTable();
                dtADMap.TableName = "GFI_FLT_AssetDeviceMap";
                dtADMap.Columns.Add("MapID");
                dtADMap.Columns.Add("AssetID");
                dtADMap.Columns.Add("DeviceID");
                dtADMap.Columns.Add("SerialNumber");
                dtADMap.Columns.Add("Status_b");
                dtADMap.Columns.Add("CreatedBy");
                dtADMap.Columns.Add("CreatedDate", typeof(DateTime));
                dtADMap.Columns.Add("StartDate", typeof(DateTime));
                dtADMap.Columns.Add("EndDate", typeof(DateTime));
                dtADMap.Columns.Add("UpdatedBy");
                dtADMap.Columns.Add("UpdatedDate", typeof(DateTime));

                dtADMap.Columns.Add("MaxNoLogDays");
                dtADMap.Columns.Add("OPRSeqNo");
                dtADMap.Columns.Add("oprType", uAsset.oprType.GetType());

                //Auxilliary               
                DataTable dtADMapAux = new DataTable();
                dtADMapAux.TableName = "GFI_FLT_DeviceAuxilliaryMap";
                dtADMapAux.Columns.Add("Uid");
                dtADMapAux.Columns.Add("MapId");
                dtADMapAux.Columns.Add("AuxilliaryType");
                dtADMapAux.Columns.Add("Field1");
                dtADMapAux.Columns.Add("Field2");
                dtADMapAux.Columns.Add("Field3");
                dtADMapAux.Columns.Add("Field4");
                dtADMapAux.Columns.Add("Field5");
                dtADMapAux.Columns.Add("Field6");
                dtADMapAux.Columns.Add("Createddate", typeof(DateTime));
                dtADMapAux.Columns.Add("CreatedBy");
                dtADMapAux.Columns.Add("UpdatedDate", typeof(DateTime));
                dtADMapAux.Columns.Add("UpdatedBy");
                dtADMapAux.Columns.Add("Auxilliary");
                dtADMapAux.Columns.Add("Uom");
                dtADMapAux.Columns.Add("Thresholdhigh");
                dtADMapAux.Columns.Add("Thresholdlow");
                dtADMapAux.Columns.Add("Capacity");
                dtADMapAux.Columns.Add("OPRSeqNo");
                dtADMapAux.Columns.Add("oprType", uAsset.oprType.GetType());

                //Calibration               
                DataTable dtCalibration = new DataTable();
                dtCalibration.TableName = "GFI_FLT_CalibrationChart";
                dtCalibration.Columns.Add("IID");
                dtCalibration.Columns.Add("AssetID");
                dtCalibration.Columns.Add("DeviceID");
                dtCalibration.Columns.Add("AuxID");
                dtCalibration.Columns.Add("CalType");
                dtCalibration.Columns.Add("SeuqenceNo");
                dtCalibration.Columns.Add("ReadingUOM");
                dtCalibration.Columns.Add("Reading");
                dtCalibration.Columns.Add("ConvertedValue");
                dtCalibration.Columns.Add("ConvertedUOM");
                dtCalibration.Columns.Add("Createddate", typeof(DateTime));
                dtCalibration.Columns.Add("CreatedBy");
                dtCalibration.Columns.Add("UpdatedDate", typeof(DateTime));
                dtCalibration.Columns.Add("UpdatedBy");
                dtCalibration.Columns.Add("OPRSeqNo");
                dtCalibration.Columns.Add("oprType", uAsset.oprType.GetType());

                if (bInsert)
                {
                    dtADMap.Columns.Remove("UpdatedDate");
                    dtADMapAux.Columns.Remove("UpdatedDate");
                    dtCalibration.Columns.Remove("UpdatedDate");
                }
                else
                {
                    dtADMap.Columns.Remove("CreatedDate");
                    dtADMapAux.Columns.Remove("CreatedDate");
                    dtCalibration.Columns.Remove("Createddate");
                }
                #endregion

                int ic = 7;
                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = ic;
                drCtrl["TblName"] = dtADMap.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "MapID";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdAction"] = "SET";
                drCtrl["NextIdFieldName"] = "AssetID";
                drCtrl["ParentTblName"] = dt.TableName;
                dtCtrl.Rows.Add(drCtrl);

                if (uAsset.assetDeviceMap.oprType == DataRowState.Added)
                {
                    ic++;
                    drCtrl = dtCtrl.NewRow();
                    drCtrl["SeqNo"] = ic;
                    drCtrl["TblName"] = dtADMap.TableName;
                    drCtrl["CmdType"] = "TABLE";
                    drCtrl["KeyFieldName"] = "MapID";
                    drCtrl["KeyIsIdentity"] = "TRUE";
                    drCtrl["NextIdAction"] = "GET";
                    dtCtrl.Rows.Add(drCtrl);
                }

                DataRow h = dtADMap.NewRow();
                h["MapID"] = uAsset.assetDeviceMap.MapID;

                h["AssetID"] = uAsset.assetDeviceMap.AssetID;

                if (!String.IsNullOrEmpty(uAsset.assetDeviceMap.DeviceID))
                    h["DeviceID"] = uAsset.assetDeviceMap.DeviceID;

                if (!String.IsNullOrEmpty(uAsset.assetDeviceMap.SerialNumber))
                    h["SerialNumber"] = uAsset.assetDeviceMap.SerialNumber;

                if (String.IsNullOrEmpty(uAsset.assetDeviceMap.Status_b))
                    uAsset.assetDeviceMap.Status_b = "AC";

                h["Status_b"] = uAsset.assetDeviceMap.Status_b;

                if (uAsset.CreatedBy.HasValue)
                    h["CreatedBy"] = uAsset.CreatedBy;

                if (bInsert)
                    h["CreatedDate"] = uAsset.CreatedDate;//DateTime.UtcNow;

                if (uAsset.UpdatedBy.HasValue)
                    h["UpdatedBy"] = uAsset.UpdatedBy;

                if (!bInsert)
                    h["UpdatedDate"] = uAsset.UpdatedDate;

                if (bInsert)
                    h["StartDate"] = DateTime.Now.AddYears(-1);
                else
                    h["StartDate"] = uAsset.assetDeviceMap.StartDate;

                if (bInsert)
                    h["EndDate"] = DateTime.Now.AddYears(100);
                else
                    h["EndDate"] = uAsset.assetDeviceMap.EndDate;


                h["MaxNoLogDays"] = uAsset.assetDeviceMap.MaxNoLogDays;

                h["OPRSeqNo"] = ic;
                h["oprType"] = uAsset.assetDeviceMap.oprType;
                int iADM = ic;
                dtADMap.Rows.Add(h);

                DeviceAuxilliaryMap d = uAsset.assetDeviceMap.deviceAuxilliaryMap;
                if (d.oprType == DataRowState.Added)
                {
                    ic++;
                    drCtrl = dtCtrl.NewRow();
                    drCtrl["SeqNo"] = ic;
                    drCtrl["TblName"] = "GFI_FLT_DeviceAuxilliaryMap";
                    drCtrl["CmdType"] = "TABLE";
                    drCtrl["KeyFieldName"] = "Uid";
                    drCtrl["KeyIsIdentity"] = "TRUE";
                    drCtrl["NextIdAction"] = "GET";
                    dtCtrl.Rows.Add(drCtrl);
                }
                int iAuxMap = ic;

                DataRow r = dtADMapAux.NewRow();
                r["Uid"] = d.Uid;

                if (!String.IsNullOrEmpty(h["MapID"].ToString()))
                    r["Mapid"] = h["MapID"].ToString();

                if (d.AuxilliaryType.HasValue)
                    r["AuxilliaryType"] = d.AuxilliaryType;

                if (!String.IsNullOrEmpty(d.Auxilliary))
                    r["Auxilliary"] = d.Auxilliary;

                if (!String.IsNullOrEmpty(d.Field1))
                    r["Field1"] = d.Field1;

                if (!String.IsNullOrEmpty(d.Field2))
                    r["Field2"] = d.Field2;

                if (d.Field3.HasValue)
                    r["Field3"] = d.Field3;

                if (d.Field4.HasValue)
                    r["Field4"] = d.Field4;

                if (!String.IsNullOrEmpty(d.Field5))
                    r["Field5"] = d.Field5;

                if (d.Field6.HasValue)
                    r["Field6"] = d.Field6;

                if (bInsert)
                    r["CreatedDate"] = uAsset.CreatedDate;

                if (uAsset.CreatedBy.HasValue)
                    r["CreatedBy"] = uAsset.CreatedBy;

                if (!bInsert)
                    r["UpdatedDate"] = uAsset.UpdatedDate;

                if (uAsset.UpdatedBy.HasValue)
                    r["UpdatedBy"] = uAsset.UpdatedBy;

                if (d.Uom.HasValue)
                    r["Uom"] = d.Uom;

                if (d.Thresholdhigh.HasValue)
                    r["Thresholdhigh"] = d.Thresholdhigh;

                if (d.Thresholdlow.HasValue)
                    r["Thresholdlow"] = d.Thresholdlow;

                if (d.Capacity.HasValue)
                    r["Capacity"] = d.Capacity;

                r["OPRSeqNo"] = iADM;
                r["oprType"] = d.oprType;
                dtADMapAux.Rows.Add(r);

                ic++;
                dtADMapAux.TableName = "GFI_FLT_DeviceAuxilliaryMap";
                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = ic;
                drCtrl["TblName"] = dtADMapAux.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "Uid";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdAction"] = "SET";
                drCtrl["NextIdFieldName"] = "MapId";
                drCtrl["ParentTblName"] = dtADMap.TableName;
                dtCtrl.Rows.Add(drCtrl);

                List<CalibrationChart> cal = uAsset.assetDeviceMap.deviceAuxilliaryMap.lCalibration;
                for (int cc = 0; cc < cal.Count; cc++)
                {
                    DataRow rC = dtCalibration.NewRow();
                    rC["IID"] = cal[cc].IID;

                    if (cal[cc].AssetID.HasValue)
                        rC["AssetID"] = cal[cc].AssetID;

                    if (!String.IsNullOrEmpty(cal[cc].DeviceID))
                        rC["DeviceID"] = cal[cc].DeviceID;

                    rC["AuxID"] = d.Uid;

                    if (!String.IsNullOrEmpty(cal[cc].CalType))
                        rC["CalType"] = cal[cc].CalType;

                    if (cal[cc].SeuqenceNo.HasValue)
                        rC["SeuqenceNo"] = cal[cc].SeuqenceNo;

                    rC["ReadingUOM"] = cal[cc].ReadingUOM;
                    rC["Reading"] = cal[cc].Reading;
                    rC["ConvertedValue"] = cal[cc].ConvertedValue;
                    rC["ConvertedUOM"] = cal[cc].ConvertedUOM;

                    if (bInsert)
                        rC["Createddate"] = uAsset.CreatedDate;

                    if (uAsset.CreatedBy.HasValue)
                        rC["CreatedBy"] = uAsset.CreatedBy;

                    if (!bInsert)
                        rC["UpdatedDate"] = uAsset.UpdatedDate;

                    if (uAsset.UpdatedBy.HasValue)
                        rC["UpdatedBy"] = uAsset.UpdatedBy;

                    rC["OPRSeqNo"] = iAuxMap;
                    rC["oprType"] = cal[cc].oprType;
                    dtCalibration.Rows.Add(rC);

                    ic++;
                    dtCalibration.TableName = "GFI_FLT_CalibrationChart";
                    drCtrl = dtCtrl.NewRow();
                    drCtrl["SeqNo"] = ic;
                    drCtrl["TblName"] = dtCalibration.TableName;
                    drCtrl["CmdType"] = "TABLE";
                    drCtrl["KeyFieldName"] = "IID";
                    drCtrl["KeyIsIdentity"] = "TRUE";
                    drCtrl["NextIdAction"] = "SET";
                    drCtrl["NextIdFieldName"] = "AuxID";
                    drCtrl["ParentTblName"] = dtADMapAux.TableName;
                    dtCtrl.Rows.Add(drCtrl);
                }
                dsProcess.Merge(dtADMap);
                dsProcess.Merge(dtADMapAux);
                dsProcess.Merge(dtCalibration);
            }
            #endregion

            #region GFI_FLT_AssetExt       
            DataTable dtExt = new DataTable();
            dtExt.TableName = "GFI_FLT_AssetExt";

            dtExt.Columns.Add("oprType", typeof(DataRowState));
            dtExt.Columns.Add("AssetId", typeof(int));
            dtExt.Columns.Add("MakeID", typeof(int));
            dtExt.Columns.Add("ModelID", typeof(int));
            dtExt.Columns.Add("ColorID", typeof(int));
            dtExt.Columns.Add("YearManufactured", typeof(int));
            dtExt.Columns.Add("PurchasedDate", typeof(DateTime));
            dtExt.Columns.Add("PurchasedValue", typeof(int));
            dtExt.Columns.Add("EngineSerialNumber", typeof(String));
            dtExt.Columns.Add("EngineCapacity", typeof(int));
            dtExt.Columns.Add("EnginePower", typeof(int));
            dtExt.Columns.Add("RefA", typeof(String));
            dtExt.Columns.Add("RefB", typeof(String));
            dtExt.Columns.Add("RefC", typeof(String));
            dtExt.Columns.Add("RefD", typeof(String));
            dtExt.Columns.Add("ExternalRef", typeof(String));

            DataRow drExt = dtExt.NewRow();
            drExt["AssetId"] = uAsset.AssetID;
            drExt["MakeID"] = uAsset.MakeID != null ? uAsset.MakeID : (object)DBNull.Value;
            drExt["ModelID"] = uAsset.ModelID != null ? uAsset.ModelID : (object)DBNull.Value;
            drExt["ColorID"] = uAsset.ColorID != null ? uAsset.ColorID : (object)DBNull.Value;
            drExt["YearManufactured"] = uAsset.YearManufactured != null ? uAsset.YearManufactured : (object)DBNull.Value;
            drExt["PurchasedDate"] = uAsset.PurchasedDate != null ? uAsset.PurchasedDate : (object)DBNull.Value;
            drExt["PurchasedValue"] = uAsset.PurchasedValue != null ? uAsset.PurchasedValue : (object)DBNull.Value;
            drExt["EngineSerialNumber"] = uAsset.EngineSerialNumber != null ? uAsset.EngineSerialNumber : (object)DBNull.Value;
            drExt["EngineCapacity"] = uAsset.EngineCapacity != null ? uAsset.EngineCapacity : (object)DBNull.Value;
            drExt["EnginePower"] = uAsset.EnginePower != null ? uAsset.EnginePower : (object)DBNull.Value;
            drExt["RefA"] = uAsset.RefA != null ? uAsset.RefA : (object)DBNull.Value;
            drExt["RefB"] = uAsset.RefB != null ? uAsset.RefB : (object)DBNull.Value;
            drExt["RefC"] = uAsset.RefC != null ? uAsset.RefC : (object)DBNull.Value;
            drExt["RefD"] = uAsset.RefD != null ? uAsset.RefD : (object)DBNull.Value;
            drExt["ExternalRef"] = uAsset.ExternalRef != null ? uAsset.ExternalRef : (object)DBNull.Value;

            drExt["oprType"] = uAsset.oprType;
            dtExt.Rows.Add(drExt);

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 15;
            drCtrl["TblName"] = dtExt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AssetId";
            drCtrl["KeyIsIdentity"] = "FALSE";
            drCtrl["NextIdFieldName"] = "AssetId";
            drCtrl["ParentTblName"] = dt.TableName;
            if (bInsert)
                drCtrl["NextIdAction"] = "SET";
            else
                drCtrl["NextIdValue"] = uAsset.AssetID;

            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtExt);
            #endregion

            #region  UOM
            if (uAsset.lUOM != null)
            {
                DataTable dtUOM = new myConverter().ListToDataTable<AssetUOM>(uAsset.lUOM);
                dtUOM.TableName = "GFI_FLT_AssetUOM";
                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 16;
                drCtrl["TblName"] = dtUOM.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "iID";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdAction"] = "SET";
                drCtrl["NextIdFieldName"] = "AssetID";
                drCtrl["ParentTblName"] = dt.TableName;
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtUOM);
            }
            #endregion

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResult.Tables["CTRLTBL"];

            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");
                DataRow[] dRowselectException = dtCtrl.Select("ExceptionInfo IS  NOT NULL");

                if (dRow.Length > 0)
                {
                   
                    baseModel.dbResult = dsResult;
                    baseModel.dbResult.Tables["CTRLTBL"].TableName = "ResultCtrlTbl";
                    baseModel.dbResult.Merge(dsProcess);
                    bResult = false;


                    string errorMessage = string.Empty;
                    foreach (var x in dRowselectException)
                        errorMessage +=  x[12].ToString() + "///";

                    System.IO.File.AppendAllText(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "\\Registers\\AssetCreationOnfLEET.dat", "\r\n Asset Creation on fleet\r\n" +errorMessage.ToString() + "\r\n");
                }
                else
                {
                    string AssetIDMaint = string.Empty;
                    AssetIDMaint = dtCtrl.Select("SeqNo = 1 and TblName = 'GFI_FLT_Asset'").FirstOrDefault()["NextIDValue"].ToString();
                    uAsset.AssetID = int.Parse(AssetIDMaint);
                    bResult = true;
                }

            }
            else
            {
                bResult = false;
            }


            #region Insert in maintenance
            Boolean bMaintResult = false;
            try
            {
                bMaintResult = bSaveMain(uAsset, bInsert, uLogin, sConnStr);

            }catch(Exception ex)
            {

            }
            #endregion

            baseModel.data = bResult;
            baseModel.additionalData = bMaintResult; // dtCtrl.Rows[dtCtrl.Rows.Count - 1].Field<string>("KeyFieldName");
            return baseModel;
        }

        public Boolean bSaveMain(Asset uAsset, Boolean bInsert, int uLogin, String sConnStr)
        {
            Boolean b = false;
            try
            {
                NaveoService.Services.MaintAssetServices maintAssetService = new NaveoService.Services.MaintAssetServices();
                NaveoService.Services.MaintAssetServices.MaintAsset maintAsset = new NaveoService.Services.MaintAssetServices.MaintAsset();

                maintAsset.AssetID = uAsset.AssetID;
                maintAsset.AssetCode = uAsset.AssetNumber;
                try
                {
                    maintAsset.AssetName = uAsset.lMatrix[0].GMDesc;
                }
                catch
                {
                    maintAsset.AssetName = new NaveoOneLib.Services.GMatrix.MatrixService().GetMatrixByUserID(uLogin, sConnStr)[0].GMDesc;
                }
                maintAsset.Descript = string.Empty;
                maintAsset.AssetType = uAsset.AssetType;
                maintAsset.AssetCategory = 0;
                maintAsset.AssetNumber = uAsset.AssetNumber;
                maintAsset.MakeID = uAsset.MakeID;
                maintAsset.ModelID = uAsset.ModelID;
                maintAsset.FuelType = uAsset.assetDeviceMap.deviceAuxilliaryMap.Field3;
                maintAsset.VehicleType = uAsset.VehicleType;
                maintAsset.sColor = uAsset.sColor;
                maintAsset.YearManufactured = uAsset.YearManufactured;
                maintAsset.PurchasedDate = uAsset.PurchasedDate;
                maintAsset.PurchasedValue = uAsset.PurchasedValue;
                maintAsset.ExternalRef = uAsset.ExternalRef;
                maintAsset.EngineSerialNumber = uAsset.EngineSerialNumber;
                maintAsset.RefD = uAsset.RefD;
                maintAsset.Type = "Assets";
                b = (Boolean)maintAssetService.SaveMaintAsset(maintAsset, bInsert, sConnStr).data;
            }
            catch (Exception ex)
            {
                string responseString = ex.ToString();
                System.IO.File.AppendAllText(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "\\Registers\\maintenance.dat", "\r\n Asset Creation \r\n" + responseString + "\r\n");
            }
            return b;
        }

        String sAssetsWizNoMatrix()
        {
            String sql = "select AssetID from GFI_FLT_Asset where AssetID not in (select iID from GFI_SYS_GroupMatrixAsset)";
            return sql;
        }
        public DataTable GetAssetsStatus(List<int> lAssets, List<Matrix> lm, String sConnStr)
        {
            lAssets = lAssets.Distinct().ToList();

            String sql = @"
--GetAssetsStatus
DECLARE @Assets TABLE
(
	iAssetID int
)

DECLARE @Table TABLE
(
    	assetID INT,
    	deviceID NVARCHAR(20),
    	gmDescription NVARCHAR(50),
    	gmID INT,
		dtLastRptd DateTime,
    	speed INT,
    	engineOn INT,
		details nvarchar(500),
		daysNoReport int,
        color nvarchar(15)
)


--Oracle Starts Here
";
            foreach (int i in lAssets)
                sql += "insert @Assets (iAssetID) values (" + i + ");\r\n";
            sql += @"
--Oracle ends Here
insert @Table
	select a.AssetID, d.DeviceID, g.GMDescription, g.GMID, null, null, null, null, null, null
	from GFI_FLT_Asset a
        inner join @Assets t on a.AssetID = t.iAssetID
		inner join GFI_FLT_AssetDeviceMap d on a.AssetID = d.AssetID
		inner join GFI_SYS_GroupMatrixAsset gma on a.AssetID = gma.iID
		inner join GFI_SYS_GroupMatrix g on gma.GMID = g.GMID
";

            #region Detecting No Matrix Assets for AllClients and Root user
            Boolean b = false;
            GroupMatrix root = new GroupMatrixService().GetRoot(sConnStr);
            if (root != null)
            {
                foreach (Matrix m in lm)
                    if (m.GMID == root.gmID)
                    {
                        b = true;
                        break;
                    }
            }
            if (!b)
            {
                root = new GroupMatrixService().GetAllClients(sConnStr);
                if (root != null)
                {
                    foreach (Matrix m in lm)
                        if (m.GMID == root.gmID)
                        {
                            b = true;
                            break;
                        }
                }
            }
            if (b)
            {
                sql += @"
insert @Table(AssetID, DeviceID, GMDescription)
	select a.AssetID, d.DeviceID, '*** No Matrix ***'
		from GFI_FLT_Asset a
			left outer join GFI_FLT_AssetDeviceMap d on a.AssetID = d.AssetID
		where a.AssetID not in (select iID from GFI_SYS_GroupMatrixAsset)
";
            }
            #endregion

            sql += @"
declare @AllClients int
set @AllClients = (select GMID from GFI_SYS_GroupMatrix where GMDescription = '### All Clients ###')

declare @AssetID INT

declare @GMID INT

DECLARE ProcessCursor CURSOR FOR 
	select AssetID, GMID from @Table

open ProcessCursor
	fetch next from ProcessCursor into @AssetID, @GMID
	WHILE @@FETCH_STATUS = 0   
	BEGIN 
		;with cte1 as
		(
		  select T.GMID, T.ParentGMID, T.GMDescription, 1 as lvl
			from GFI_SYS_GroupMatrix as T
		  where T.GMID = @GMID 
		  union all
		  select T.GMID, T.ParentGMID, T.GMDescription, C.lvl+1
			from GFI_SYS_GroupMatrix as T
				inner join cte1 as C on T.GMID = C.ParentGMID
		)
		update @Table set GMDescription = (select GMDescription from cte1 where ParentGMID = @AllClients) 
			where AssetID = @AssetID 
				and (select GMDescription from cte1 where ParentGMID = @AllClients) is not null

		fetch next from ProcessCursor into @AssetID, @GMID
	END
close ProcessCursor
deallocate ProcessCursor

update @Table
	set dtLastRptd = T2.DateTimeGPS_UTC, speed = T2.Speed, EngineOn = T2.EngineOn
		, details = STUFF((SELECT '| ' + d.TypeID + ' : ' + d.TypeValue + ' : ' + d.UOM FROM GFI_GPS_LiveDataDetail d Where d.UID = T2.UID FOR XML PATH('')), 1, 1, '')  
from @Table T1
	left outer join GFI_GPS_LiveData T2 on T1.AssetID = T2.AssetID 
where
	T2.DateTimeGPS_UTC = (SELECT MAX(DateTimeGPS_UTC) FROM GFI_GPS_LiveData a WHERE a.AssetID = T2.AssetID)

update @Table set color = '#F63B5D', details = '*** Never Reported ***' where dtLastRptd is null

update @Table set color = '#D8941D', details = '*** Not Recent *** ' + details where dtLastRptd <= DATEADD(day, DATEDIFF(day, 0, GETDATE()), -1)

update @Table set color = '#B7F737' where color is null

update @Table set color = '#1D33D8' where GMDescription = '*** No Matrix ***'

update @Table set daysNoReport = DATEDIFF(day, dtLastRptd, getUTCdate())

select * from @Table order by AssetID";

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtAssetsStatus";
            dtSql.Rows.Add(drSql);
            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }

        public Asset GetAssetByName(String AssetNumber, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String EncryptAsset = BaseService.Encrypt(AssetNumber); ;
            String sql = @"SELECT AssetID, 
								AssetNumber, AssetName,
								AssetType, 
								Status_b, TimeZoneID, Odometer,
								CreatedDate, 
								CreatedBy, 
								UpdatedDate, 
								UpdatedBy from GFI_FLT_Asset
								WHERE AssetNumber = ?
								order by AssetID";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = EncryptAsset;
            drSql["sTableName"] = "GPSAsset";
            dtSql.Rows.Add(drSql);

            sql = @"SELECT m.MapID, 
								m.AssetID, 
								m.DeviceID, 
								m.SerialNumber, 
								m.Status_b, 
								m.CreatedBy, 
								m.CreatedDate, 
								m.UpdatedBy, 
								m.UpdatedDate, m.StartDate, m.EndDate, m.MaxNoLogDays
							 from GFI_FLT_AssetDeviceMap m, GFI_FLT_Asset a
								WHERE a.AssetId = m.AssetID and a.AssetNumber = ? And m.Status_b = 'AC' and m.StartDate < GetDate() and m.EndDate > Getdate() 
								order by AssetId ";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = EncryptAsset;
            drSql["sTableName"] = "DeviceMap";
            dtSql.Rows.Add(drSql);

            sql = new MatrixService().sGetMatrixIdByField("GFI_FLT_Asset", "AssetNumber", EncryptAsset, "AssetID", "GFI_SYS_GroupMatrixAsset");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables["GPSAsset"].Rows.Count == 0)
                return null;
            else
                return GetAsset(ds);
        }

        public String GetAssetNameByID(int AssetID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            //Asset
            String sql = @"SELECT AssetName from GFI_FLT_Asset WHERE AssetID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = AssetID.ToString();
            drSql["sTableName"] = "GPSAsset";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            String s = BaseService.Decrypt(ds.Tables[0].Rows[0][0].ToString());
            return s;
        }
        public DataTable GetAssetNamesByIDs(List<int> lAssetIDs, String sConnStr)
        {
            if (lAssetIDs.Count == 0)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("AssetID");
                dt.Columns.Add("AssetName");
                return dt;
            }
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String s = String.Empty;
            foreach (int i in lAssetIDs)
                s += i.ToString() + ", ";
            if (s.Length > 2)
                s = s.Substring(0, s.Length - 2);
            s = "(" + s + ") ";

            //Asset
            String sql = @"SELECT AssetID, AssetName from GFI_FLT_Asset WHERE AssetID in " + s;
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "GPSAsset";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            //foreach(DataRow dr in ds.Tables[0].Rows)
            //    dr["AssetName"] = BaseService.Decrypt(dr["AssetName"].ToString());

            return ds.Tables[0];
        }

        public Double GetOdo(int AssetID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = "select dbo.GetCalcOdo(" + AssetID + ")";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtOdo";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            Double d = 0;
            if (ds != null)
                Double.TryParse(ds.Tables[0].Rows[0][0].ToString(), out d);
            return d;
        }
        public Double GetEngHrs(int AssetID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = "select dbo.GetCalcEngHrs(" + AssetID + ")";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtHrs";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            Double d = 0;
            if (ds != null)
                Double.TryParse(ds.Tables[0].Rows[0][0].ToString(), out d);
            return d;
        }

        DataTable AssetDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_Asset";
            dt.Columns.Add("AssetID", typeof(int));
            dt.Columns.Add("AssetNumber", typeof(String));
            dt.Columns.Add("AssetName", typeof(String));
            dt.Columns.Add("AssetType", typeof(int));
            dt.Columns.Add("Status_b", typeof(String));
            dt.Columns.Add("TimeZoneID", typeof(String));
            dt.Columns.Add("Odometer", typeof(int));
            dt.Columns.Add("CreatedDate", typeof(DateTime));
            dt.Columns.Add("CreatedBy", typeof(int));
            dt.Columns.Add("UpdatedDate", typeof(DateTime));
            dt.Columns.Add("UpdatedBy", typeof(int));
            dt.Columns.Add("VehicleTypeId", typeof(int));
            dt.Columns.Add("ColorID", typeof(int));
            dt.Columns.Add("RoadSpeedType", typeof(int));
            dt.Columns.Add("oprType", typeof(DataRowState));
            return dt;
        }

        public Boolean DeActivateAsset(Asset uAsset, int uLogin, String sConnStr)
        {
            uAsset.Status_b = Constants.strNotActive;
            AssetDeviceMap adm = uAsset.assetDeviceMap;
            if (adm.Status_b == Constants.strActive)
            {
                adm.Status_b = Constants.strNotActive;
                adm.EndDate = DateTime.Now.AddDays(-1);
                adm.oprType = DataRowState.Modified;
            }

            return Save(uAsset, false, uLogin, sConnStr).data;
        }

        public Boolean DeleteAssets(List<int> lAssetID, User uLogin, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            String s = String.Empty;
            foreach (int i in lAssetID)
                s += i.ToString() + ", ";
            if (s.Length > 2)
                s = s.Substring(0, s.Length - 2);
            s = "(" + s + ") ";

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 20000;
            String sql = "delete from GFI_FLT_Asset where AssetID in " + s;
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);

            DataSet dsProcess = new DataSet();
            DataTable dtAudit = new AuditService().LogTran(3, "AssetBulkDel", "-888", uLogin.UID, -888);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "BulkDel";
            drCtrl["ParentTblName"] = "GFI_FLT_Asset";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public String GetAssetsRelated(Asset uAsset, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            //Rules
            String sql = "select * from GFI_GPS_Rules r where r.Struc = 'Assets' and r.StrucValue = " + uAsset.AssetID.ToString();
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtRules";
            dtSql.Rows.Add(drSql);

            //AutoReport
            sql = "select * from GFI_FLT_AutoReportingConfig arc, GFI_FLT_AutoReportingConfigDetails arcd where arc.ARID = arcd.ARID and arcd.UIDType = 'Assets' and UIDCategory = 'UID' and UID = " + uAsset.AssetID.ToString();
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtARC";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            String s = String.Empty;
            if (ds.Tables["dtRules"].Rows.Count > 0)
            {
                s = "Asset " + uAsset.AssetNumber + " has Rules \r\n";
                foreach (DataRow dr in ds.Tables["dtRules"].Rows)
                    s += dr["RuleName"].ToString() + "\r\n";
            }

            if (ds.Tables["dtARC"].Rows.Count > 0)
            {
                s = "Asset " + uAsset.AssetNumber + " has Automatic Reports \r\n";
                foreach (DataRow dr in ds.Tables["dtARC"].Rows)
                    s += dr["ReportFormat"].ToString() + "\r\n";
            }
            return s;
        }

        public String sGetOdo(int AssetID)
        {
            String sql = @"select SUM(h.TripDistance) + COALESCE((select top 1 CalculatedOdometer 
						   from dbo.GFI_AMM_VehicleMaintenance 
						   where AssetId = " + AssetID.ToString() + @" and CalculatedOdometer is not null
						   order by URI desc),0)
					   from GFI_GPS_TripHeader h, dbo.GFI_GPS_GPSData g
						   where h.AssetID = " + AssetID.ToString() + @" 
							   and h.GPSDataEndUID = g.UID
							   and g.DateTimeGPS_UTC >= COALESCE(
							   (select top 1 EndDate 
								   from dbo.GFI_AMM_VehicleMaintenance 
								   where AssetId = " + AssetID.ToString() + @" and CalculatedOdometer is not null
								   order by URI desc),DATEADD(YY, -50, GETDATE()))";
            return sql;
        }

        public bool UpdateAsset(Asset uAsset, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_Asset";
            dt.Columns.Add("AssetNumber", uAsset.AssetNumber.GetType());
            dt.Columns.Add("AssetName", uAsset.AssetName.GetType());
            dt.Columns.Add("AssetType", uAsset.AssetType.GetType());
            dt.Columns.Add("Status_b", uAsset.Status_b.GetType());
            dt.Columns.Add("CreatedDate", uAsset.CreatedDate.GetType());
            dt.Columns.Add("CreatedBy", uAsset.CreatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uAsset.UpdatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uAsset.UpdatedBy.GetType());
            DataRow dr = dt.NewRow();
            dr["AssetNumber"] = BaseService.Encrypt(uAsset.AssetNumber.ToString());
            dr["AssetName"] = uAsset.AssetName;
            dr["AssetType"] = uAsset.AssetType;
            dr["Status_b"] = uAsset.Status_b;
            dr["CreatedDate"] = uAsset.CreatedDate;
            dr["CreatedBy"] = uAsset.CreatedBy;
            dr["UpdatedDate"] = uAsset.UpdatedDate;
            dr["UpdatedBy"] = uAsset.UpdatedBy;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "AssetID = '" + uAsset.AssetID + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }

        public bool DeleteAsset(Asset uAsset, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_FLT_Asset", "AssetID = '" + uAsset.AssetID + "'", sConnStr))
                return true;
            else
                return false;
        }

        public static Asset GetAsset(String strID)
        {
            Asset a = new Asset();
            foreach (Asset aa in Globals.lAsset)
                if (aa.AssetID.ToString() == strID)
                {
                    a = aa;
                    break;
                }
            return a;
        }

        public DataTable GetAssetMntDetails(int AssetID, DateTime dtStart, DateTime dtEnd, String sConnStr)
        {


            String sqlString = @"
--GetAssetMntDetails
select ROUND(COALESCE(SUM(h.TripDistance),0), 0) Odometer,COALESCE(SUM(h.TripTime),0) EngineHours 
	                             from GFI_GPS_TripHeader h
                                 where  h.AssetId = '" + AssetID + "' and dtStart >= '" + dtStart.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "' and dtStart <= '" + dtEnd.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "'";

            DataTable dt = new Connection(sConnStr).GetDataDT(sqlString, sConnStr);

            return dt;
        }


        #region Obsolete
        [Obsolete("Method is deprecated, please use Save instead.")]
        public BaseModel SaveAssetWizard(Asset uAsset, Boolean bInsert, int uLogin, String sConnStr)
        {
            BaseModel baseModel = new BaseModel();
            Boolean bResult = false;

            DataTable dt = AssetDT();

            if (bInsert)
            {
                uAsset.CreatedDate = DateTime.Now;
                uAsset.CreatedBy = uLogin;
                uAsset.UpdatedDate = Constants.NullDateTime;
            }
            else
            {
                uAsset.UpdatedDate = DateTime.Now;
                uAsset.UpdatedBy = uLogin;
            }

            if (String.IsNullOrEmpty(uAsset.TimeZoneID))
                uAsset.TimeZoneID = TimeZone.CurrentTimeZone.StandardName;

            DataRow dr = dt.NewRow();
            dr["AssetID"] = uAsset.AssetID;
            if (uAsset.AssetName == String.Empty)
                uAsset.AssetName = BaseService.Encrypt(uAsset.AssetNumber);
            else
                uAsset.AssetName = BaseService.Encrypt(uAsset.AssetName);
            dr["AssetNumber"] = BaseService.Encrypt(uAsset.AssetNumber.ToString());
            dr["AssetName"] = uAsset.AssetName;
            dr["AssetType"] = uAsset.AssetType;
            dr["Status_b"] = uAsset.Status_b;
            dr["TimeZoneID"] = uAsset.TimeZoneID;
            dr["Odometer"] = uAsset.Odometer;
            dr["CreatedDate"] = uAsset.CreatedDate;
            if (uAsset.CreatedBy != null)
                dr["CreatedBy"] = uAsset.CreatedBy;
            dr["UpdatedDate"] = uAsset.UpdatedDate;
            if (uAsset.UpdatedBy != null)
                dr["UpdatedBy"] = uAsset.UpdatedBy;
            dr["oprType"] = uAsset.oprType;

            dr["VehicleTypeId"] = uAsset.VehicleTypeId != null ? uAsset.VehicleTypeId : (object)DBNull.Value;
            dr["ColorID"] = uAsset.ColorID != null ? uAsset.ColorID : (object)DBNull.Value;

            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AssetId";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uAsset.AssetID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            //Matrix
            DataTable dtMatrix = new myConverter().ListToDataTable<Matrix>(uAsset.lMatrix);
            dtMatrix.TableName = "GFI_SYS_GroupMatrixAsset";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 2;
            drCtrl["TblName"] = dtMatrix.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "MID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "iID";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtMatrix);

            //Zone    
            //if (uAsset.lAssetZoneMap != null)
            {
                DataTable dtZone = new myConverter().ListToDataTable<AssetZoneMap>(uAsset.lAssetZoneMap);
                dtZone.TableName = "GFI_AMM_AssetZoneMap";
                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 3;
                drCtrl["TblName"] = dtZone.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "Id";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdAction"] = "SET";
                drCtrl["NextIdFieldName"] = "AssetID";
                drCtrl["ParentTblName"] = dt.TableName;
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtZone);
            }

            //ExtProVehicle       
            if (uAsset.assetExtProVehicles != null)
            {
                DataTable dtVehicle = new DataTable();
                dtVehicle.TableName = "GFI_AMM_AssetExtProVehicles";
                dtVehicle.Columns.Add("AssetId", uAsset.AssetID.GetType());
                dtVehicle.Columns.Add("CreatedDate", uAsset.CreatedDate.GetType());
                dtVehicle.Columns.Add("CreatedBy", typeof(int));
                dtVehicle.Columns.Add("Field1Value", uAsset.assetExtProVehicles.Field1Value.GetType());
                dtVehicle.Columns.Add("Field2Value", uAsset.assetExtProVehicles.Field2Value.GetType());
                dtVehicle.Columns.Add("Field3Value", uAsset.assetExtProVehicles.Field3Value.GetType());
                dtVehicle.Columns.Add("Field4Value", uAsset.assetExtProVehicles.Field4Value.GetType());
                dtVehicle.Columns.Add("Field5Value", uAsset.assetExtProVehicles.Field5Value.GetType());
                dtVehicle.Columns.Add("Field6Value", uAsset.assetExtProVehicles.Field6Value.GetType());

                dtVehicle.Columns.Add("Field1Id");
                dtVehicle.Columns.Add("Field2Id");
                dtVehicle.Columns.Add("Field3Id");
                dtVehicle.Columns.Add("Field4Id");
                dtVehicle.Columns.Add("Field5Id");
                dtVehicle.Columns.Add("Field6Id");

                dtVehicle.Columns.Add("oprType", uAsset.assetExtProVehicles.oprType.GetType());

                DataRow drExtProVehicle = dtVehicle.NewRow();
                drExtProVehicle["AssetId"] = uAsset.AssetID;
                drExtProVehicle["CreatedDate"] = uAsset.assetExtProVehicles.CreatedDate;
                if (uAsset.CreatedBy != null)
                    drExtProVehicle["CreatedBy"] = uAsset.CreatedBy;
                //drExtProVehicle["UpdatedDate"] = uAsset.Assetextprovehicles.UpdatedDate;
                //drExtProVehicle["UpdatedBy"] = uAsset.UpdatedBy;

                drExtProVehicle["Field1Value"] = uAsset.assetExtProVehicles.Field1Value;
                drExtProVehicle["Field2Value"] = uAsset.assetExtProVehicles.Field2Value;
                drExtProVehicle["Field3Value"] = uAsset.assetExtProVehicles.Field3Value;
                drExtProVehicle["Field4Value"] = uAsset.assetExtProVehicles.Field4Value;
                drExtProVehicle["Field5Value"] = uAsset.assetExtProVehicles.Field5Value;
                drExtProVehicle["Field6Value"] = uAsset.assetExtProVehicles.Field6Value;

                drExtProVehicle["Field1Id"] = uAsset.assetExtProVehicles.Field1Id;
                drExtProVehicle["Field2Id"] = uAsset.assetExtProVehicles.Field2Id;
                drExtProVehicle["Field3Id"] = uAsset.assetExtProVehicles.Field3Id;
                drExtProVehicle["Field4Id"] = uAsset.assetExtProVehicles.Field4Id;
                drExtProVehicle["Field5Id"] = uAsset.assetExtProVehicles.Field5Id;
                drExtProVehicle["Field6Id"] = uAsset.assetExtProVehicles.Field6Id;

                drExtProVehicle["oprType"] = uAsset.assetExtProVehicles.oprType;
                dtVehicle.Rows.Add(drExtProVehicle);

                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 4;
                drCtrl["TblName"] = dtVehicle.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "AssetId";
                drCtrl["KeyIsIdentity"] = "FALSE";
                drCtrl["NextIdAction"] = "SET";
                drCtrl["NextIdFieldName"] = "AssetId";
                drCtrl["ParentTblName"] = dt.TableName;
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtVehicle);
            }

            //UOM
            if (uAsset.lUOM != null)
            {
                DataTable dtUOM = new myConverter().ListToDataTable<AssetUOM>(uAsset.lUOM);
                dtUOM.TableName = "GFI_FLT_AssetUOM";
                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 5;
                drCtrl["TblName"] = dtUOM.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "iID";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdAction"] = "SET";
                drCtrl["NextIdFieldName"] = "AssetID";
                drCtrl["ParentTblName"] = dt.TableName;
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtUOM);
            }

            //Device Map
            AssetDeviceMap a = uAsset.assetDeviceMap;
            if (a != null)
            {
                //need to add a new line in Ctrl to to ensure that only 1 device id is saved on db using CmdType Chk
                String sDeviceID = uAsset.assetDeviceMap.DeviceID;
                if (sDeviceID.Length > 0)
                {
                    sDeviceID = sDeviceID.Substring(0, sDeviceID.Length - 1);
                    drCtrl = dtCtrl.NewRow();
                    drCtrl["SeqNo"] = 6;
                    drCtrl["TblName"] = "ChkIfDeviceIDExist";
                    drCtrl["CmdType"] = "Chk";
                    drCtrl["Command"] = "select * from GFI_FLT_AssetDeviceMap where DeviceID in (" + sDeviceID + ") and Status_b = 'AC' and StartDate < GetDate() and EndDate > Getdate() and AssetID != " + uAsset.AssetID;
                    dtCtrl.Rows.Add(drCtrl);
                }

                #region dtCreation
                //Device Map Data
                DataTable dtADMap = new DataTable();
                dtADMap.TableName = "GFI_FLT_AssetDeviceMap";
                dtADMap.Columns.Add("MapID");
                dtADMap.Columns.Add("AssetID");
                dtADMap.Columns.Add("DeviceID");
                dtADMap.Columns.Add("SerialNumber");
                dtADMap.Columns.Add("Status_b");
                dtADMap.Columns.Add("CreatedBy");
                dtADMap.Columns.Add("CreatedDate");
                dtADMap.Columns.Add("UpdatedBy");
                dtADMap.Columns.Add("UpdatedDate");
                dtADMap.Columns.Add("StartDate");
                dtADMap.Columns.Add("EndDate");
                dtADMap.Columns.Add("MaxNoLogDays");
                dtADMap.Columns.Add("OPRSeqNo");
                dtADMap.Columns.Add("oprType", uAsset.oprType.GetType());

                //Auxilliary               
                DataTable dtADMapAux = new DataTable();
                dtADMapAux.TableName = "GFI_FLT_DeviceAuxilliaryMap";
                dtADMapAux.Columns.Add("Uid");
                dtADMapAux.Columns.Add("MapId");
                dtADMapAux.Columns.Add("AuxilliaryType");
                dtADMapAux.Columns.Add("Field1");
                dtADMapAux.Columns.Add("Field2");
                dtADMapAux.Columns.Add("Field3");
                dtADMapAux.Columns.Add("Field4");
                dtADMapAux.Columns.Add("Field5");
                dtADMapAux.Columns.Add("Field6");
                dtADMapAux.Columns.Add("Createddate");
                dtADMapAux.Columns.Add("CreatedBy");
                dtADMapAux.Columns.Add("UpdatedDate");
                dtADMapAux.Columns.Add("UpdatedBy");
                dtADMapAux.Columns.Add("Auxilliary");
                dtADMapAux.Columns.Add("Uom");
                dtADMapAux.Columns.Add("Thresholdhigh");
                dtADMapAux.Columns.Add("Thresholdlow");
                dtADMapAux.Columns.Add("Capacity");
                dtADMapAux.Columns.Add("OPRSeqNo");
                dtADMapAux.Columns.Add("oprType", uAsset.oprType.GetType());

                //Calibration               
                DataTable dtCalibration = new DataTable();
                dtCalibration.TableName = "GFI_FLT_CalibrationChart";
                dtCalibration.Columns.Add("IID");
                dtCalibration.Columns.Add("AssetID");
                dtCalibration.Columns.Add("DeviceID");
                dtCalibration.Columns.Add("AuxID");
                dtCalibration.Columns.Add("CalType");
                dtCalibration.Columns.Add("SeuqenceNo");
                dtCalibration.Columns.Add("ReadingUOM");
                dtCalibration.Columns.Add("Reading");
                dtCalibration.Columns.Add("ConvertedValue");
                dtCalibration.Columns.Add("ConvertedUOM");
                dtCalibration.Columns.Add("Createddate");
                dtCalibration.Columns.Add("CreatedBy");
                dtCalibration.Columns.Add("UpdatedDate");
                dtCalibration.Columns.Add("UpdatedBy");
                dtCalibration.Columns.Add("OPRSeqNo");
                dtCalibration.Columns.Add("oprType", uAsset.oprType.GetType());
                #endregion

                int ic = 7;
                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = ic;
                drCtrl["TblName"] = dtADMap.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "MapID";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdAction"] = "SET";
                drCtrl["NextIdFieldName"] = "AssetID";
                drCtrl["ParentTblName"] = dt.TableName;
                dtCtrl.Rows.Add(drCtrl);

                if (uAsset.assetDeviceMap.oprType == DataRowState.Added)
                {
                    ic++;
                    drCtrl = dtCtrl.NewRow();
                    drCtrl["SeqNo"] = ic;
                    drCtrl["TblName"] = dtADMap.TableName;
                    drCtrl["CmdType"] = "TABLE";
                    drCtrl["KeyFieldName"] = "MapID";
                    drCtrl["KeyIsIdentity"] = "TRUE";
                    drCtrl["NextIdAction"] = "GET";
                    dtCtrl.Rows.Add(drCtrl);
                }

                DataRow h = dtADMap.NewRow();
                h["MapID"] = uAsset.assetDeviceMap.MapID;
                h["AssetID"] = uAsset.assetDeviceMap.AssetID;
                h["DeviceID"] = uAsset.assetDeviceMap.DeviceID;
                h["SerialNumber"] = uAsset.assetDeviceMap.SerialNumber;
                h["Status_b"] = uAsset.assetDeviceMap.Status_b;
                h["CreatedBy"] = uAsset.CreatedBy;
                h["CreatedDate"] = uAsset.assetDeviceMap.CreatedDate;
                h["UpdatedBy"] = uAsset.UpdatedBy;
                h["UpdatedDate"] = uAsset.assetDeviceMap.UpdatedDate;
                h["StartDate"] = uAsset.assetDeviceMap.StartDate;
                h["EndDate"] = uAsset.assetDeviceMap.EndDate;
                h["MaxNoLogDays"] = uAsset.assetDeviceMap.MaxNoLogDays;
                h["OPRSeqNo"] = ic;
                h["oprType"] = uAsset.assetDeviceMap.oprType;
                int iADM = ic;
                dtADMap.Rows.Add(h);

                DeviceAuxilliaryMap d = uAsset.assetDeviceMap.deviceAuxilliaryMap;
                if (d.oprType == DataRowState.Added)
                {
                    ic++;
                    drCtrl = dtCtrl.NewRow();
                    drCtrl["SeqNo"] = ic;
                    drCtrl["TblName"] = "GFI_FLT_DeviceAuxilliaryMap";
                    drCtrl["CmdType"] = "TABLE";
                    drCtrl["KeyFieldName"] = "Uid";
                    drCtrl["KeyIsIdentity"] = "TRUE";
                    drCtrl["NextIdAction"] = "GET";
                    dtCtrl.Rows.Add(drCtrl);
                }
                int iAuxMap = ic;

                DataRow r = dtADMapAux.NewRow();
                r["Uid"] = d.Uid;
                r["Mapid"] = h["MapID"].ToString();
                r["AuxilliaryType"] = d.AuxilliaryType;
                r["Auxilliary"] = d.Auxilliary;
                r["Field1"] = d.Field1;
                r["Field2"] = d.Field2;
                r["Field3"] = d.Field3;
                r["Field4"] = d.Field4;
                r["Field5"] = d.Field5;
                r["Field6"] = d.Field6;
                r["CreatedDate"] = d.CreatedDate;
                r["CreatedBy"] = uAsset.CreatedBy;
                r["UpdatedDate"] = d.UpdatedDate;
                r["UpdatedBy"] = uAsset.UpdatedBy;
                r["Uom"] = d.Uom;
                r["Thresholdhigh"] = d.Thresholdhigh;
                r["Thresholdlow"] = d.Thresholdlow;
                r["Capacity"] = d.Capacity;
                r["OPRSeqNo"] = iADM;
                r["oprType"] = d.oprType;
                dtADMapAux.Rows.Add(r);

                ic++;
                dtADMapAux.TableName = "GFI_FLT_DeviceAuxilliaryMap";
                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = ic;
                drCtrl["TblName"] = dtADMapAux.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "Uid";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdAction"] = "SET";
                drCtrl["NextIdFieldName"] = "MapId";
                drCtrl["ParentTblName"] = dtADMap.TableName;
                dtCtrl.Rows.Add(drCtrl);

                List<CalibrationChart> cal = uAsset.assetDeviceMap.deviceAuxilliaryMap.lCalibration;
                for (int cc = 0; cc < cal.Count; cc++)
                {
                    DataRow rC = dtCalibration.NewRow();
                    rC["IID"] = cal[cc].IID;
                    rC["AssetID"] = cal[cc].AssetID;
                    rC["DeviceID"] = cal[cc].DeviceID;
                    rC["AuxID"] = d.Uid;
                    rC["CalType"] = cal[cc].CalType;
                    rC["SeuqenceNo"] = cal[cc].SeuqenceNo;
                    rC["ReadingUOM"] = cal[cc].ReadingUOM;
                    rC["Reading"] = cal[cc].Reading; ;
                    rC["ConvertedValue"] = cal[cc].ConvertedValue;
                    rC["ConvertedUOM"] = cal[cc].ConvertedUOM;
                    rC["Createddate"] = cal[cc].CreatedDate;
                    rC["CreatedBy"] = uAsset.CreatedBy;
                    rC["UpdatedDate"] = cal[cc].UpdatedDate;
                    rC["UpdatedBy"] = uAsset.UpdatedBy;
                    rC["OPRSeqNo"] = iAuxMap;
                    rC["oprType"] = cal[cc].oprType;
                    dtCalibration.Rows.Add(rC);

                    ic++;
                    dtCalibration.TableName = "GFI_FLT_CalibrationChart";
                    drCtrl = dtCtrl.NewRow();
                    drCtrl["SeqNo"] = ic;
                    drCtrl["TblName"] = dtCalibration.TableName;
                    drCtrl["CmdType"] = "TABLE";
                    drCtrl["KeyFieldName"] = "IID";
                    drCtrl["KeyIsIdentity"] = "TRUE";
                    drCtrl["NextIdAction"] = "SET";
                    drCtrl["NextIdFieldName"] = "AuxID";
                    drCtrl["ParentTblName"] = dtADMapAux.TableName;
                    dtCtrl.Rows.Add(drCtrl);
                }
                dsProcess.Merge(dtADMap);
                dsProcess.Merge(dtADMapAux);
                dsProcess.Merge(dtCalibration);
            }

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResult.Tables["CTRLTBL"];

            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                {
                    baseModel.dbResult = dsResult;
                    baseModel.dbResult.Tables["CTRLTBL"].TableName = "ResultCtrlTbl";
                    baseModel.dbResult.Merge(dsProcess);
                    bResult = false;
                }
                else
                    bResult = true;
            }
            else
                bResult = false;

            baseModel.data = bResult;
            return baseModel;
        }
        [Obsolete("Method is deprecated, please use Save instead.")]
        public Boolean SaveAsset(Asset uAsset, Boolean bInsert, int uLogin, String sConnStr)
        {
            DataTable dt = AssetDT();
            if (bInsert)
            {
                uAsset.CreatedDate = DateTime.Now;
                uAsset.CreatedBy = uLogin;
                uAsset.UpdatedDate = Constants.NullDateTime;
            }
            else
            {
                uAsset.UpdatedDate = DateTime.Now;
                uAsset.UpdatedBy = uLogin;
            }

            if (uAsset.TimeZoneID == String.Empty)
                uAsset.TimeZoneID = TimeZone.CurrentTimeZone.StandardName;

            DataRow dr = dt.NewRow();
            dr["AssetID"] = uAsset.AssetID;
            if (uAsset.AssetName == String.Empty)
                uAsset.AssetName = BaseService.Encrypt(uAsset.AssetNumber);
            dr["AssetNumber"] = BaseService.Encrypt(uAsset.AssetNumber.ToString());
            dr["AssetName"] = uAsset.AssetName;
            dr["AssetType"] = uAsset.AssetType;
            dr["Status_b"] = uAsset.Status_b;
            dr["TimeZoneID"] = uAsset.TimeZoneID;
            dr["Odometer"] = uAsset.Odometer;
            dr["CreatedDate"] = uAsset.CreatedDate;
            if (!String.IsNullOrEmpty(uAsset.CreatedBy.ToString()))
                dr["CreatedBy"] = uAsset.CreatedBy;
            dr["UpdatedDate"] = uAsset.UpdatedDate;
            if (!String.IsNullOrEmpty(uAsset.UpdatedBy.ToString()))
                dr["UpdatedBy"] = uAsset.UpdatedBy;
            dr["VehicleTypeId"] = uAsset.VehicleTypeId != null ? uAsset.VehicleTypeId : (object)DBNull.Value;
            dr["ColorID"] = uAsset.ColorID != null ? uAsset.ColorID : (object)DBNull.Value;

            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AssetId";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uAsset.AssetID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            DataTable dtMatrix = new myConverter().ListToDataTable<Matrix>(uAsset.lMatrix);
            dtMatrix.TableName = "GFI_SYS_GroupMatrixAsset";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 2;
            drCtrl["TblName"] = dtMatrix.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "MID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "iID";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtMatrix);

            if (uAsset.assetDeviceMap != null)
            {
                //need to add a new line in Ctrl to to ensure that only 1 device id is saved on db using CmdType Chk
                String sDeviceID = uAsset.assetDeviceMap.DeviceID;
                if (sDeviceID.Length > 0)
                {
                    sDeviceID = sDeviceID.Substring(0, sDeviceID.Length - 1);
                    drCtrl = dtCtrl.NewRow();
                    drCtrl["SeqNo"] = 3;
                    drCtrl["TblName"] = "ChkIfDeviceIDExist";
                    drCtrl["CmdType"] = "Chk";
                    drCtrl["Command"] = "select * from GFI_FLT_AssetDeviceMap where DeviceID in (" + sDeviceID + ") and Status_b = 'AC' and StartDate < GetDate() and EndDate > Getdate() and AssetID != " + uAsset.AssetID;
                    dtCtrl.Rows.Add(drCtrl);
                }

                List<AssetDeviceMap> lasm = new List<Models.Assets.AssetDeviceMap>();
                lasm.Add(uAsset.assetDeviceMap);
                DataTable dtADMap = new myConverter().ListToDataTable<AssetDeviceMap>(lasm);
                dtADMap.TableName = "GFI_FLT_AssetDeviceMap";
                if (dtADMap.Columns.Contains("lAuxType"))
                    dtADMap.Columns.Remove("lAuxType");

                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 4;
                drCtrl["TblName"] = dtADMap.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "MapID";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdAction"] = "SET";
                drCtrl["NextIdFieldName"] = "AssetID";
                drCtrl["ParentTblName"] = dt.TableName;
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtADMap);
            }

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
                    else
                        if (dri["oprType"].ToString().Trim() == "0")
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }

        #endregion



    }
}