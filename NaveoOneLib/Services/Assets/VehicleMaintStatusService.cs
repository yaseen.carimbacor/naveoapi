using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Assets;

namespace NaveoOneLib.Services.Assets
{
    public class VehicleMaintStatusService
    {

        Errors er = new Errors();

        public String sGetVehicleMaintStatus()
        {
            String sqlString = @"SELECT MaintStatusId, 
                    Description, 
                    SortOrder,
                    CreatedDate, 
                    CreatedBy, 
                    UpdatedDate, 
                    UpdatedBy from GFI_AMM_VehicleMaintStatus order by MaintStatusId";

            return sqlString;
        }

        public DataTable GetVehicleMaintStatus(String sConnStr)
        {
            return new Connection(sConnStr).GetDataDT(sGetVehicleMaintStatus(), sConnStr);
        }

        public VehicleMaintStatus GetVehicleMaintStatusById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT MaintStatusId, 
                            Description,
                            SortOrder, 
                            CreatedDate, 
                            CreatedBy, 
                            UpdatedDate, 
                            UpdatedBy from GFI_AMM_VehicleMaintStatus
                            WHERE MaintStatusId = ?
                            order by MaintStatusId";

            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);

            if (dt.Rows.Count == 0)
                return null;
            else
            {
                VehicleMaintStatus retVehicleMaintStatus = new VehicleMaintStatus();
                retVehicleMaintStatus.MaintStatusId = Convert.ToInt32(dt.Rows[0]["MaintStatusId"]);
                retVehicleMaintStatus.Description = dt.Rows[0]["Description"].ToString();
                retVehicleMaintStatus.SortOrder = Convert.ToInt32(dt.Rows[0]["SortOrder"]);                
                retVehicleMaintStatus.CreatedDate = (DateTime)dt.Rows[0]["CreatedDate"];
                retVehicleMaintStatus.CreatedBy = dt.Rows[0]["CreatedBy"].ToString();
                retVehicleMaintStatus.UpdatedDate = (DateTime)dt.Rows[0]["UpdatedDate"];
                retVehicleMaintStatus.UpdatedBy = dt.Rows[0]["UpdatedBy"].ToString();
                return retVehicleMaintStatus;
            }
        }

        public String sGetVehicleMaintStatusByDescription()
        {
            String sqlString = @"SELECT MaintStatusId, 
                            Description, 
                            SortOrder,
                            CreatedDate, 
                            CreatedBy, 
                            UpdatedDate, 
                            UpdatedBy from GFI_AMM_VehicleMaintStatus
                            WHERE Description = ?
                            order by Description";
            
            return sqlString;
        }


        public VehicleMaintStatus GetVehicleMaintStatusByDescription(String sDescription, String sConnStr)
        {
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sGetVehicleMaintStatusByDescription(), sDescription, sConnStr);

            if (dt.Rows.Count == 0)
                return null;
            else
            {
                VehicleMaintStatus retVehicleMaintStatus = new VehicleMaintStatus();
                retVehicleMaintStatus.MaintStatusId = Convert.ToInt32(dt.Rows[0]["MaintStatusId"]);
                retVehicleMaintStatus.Description = dt.Rows[0]["Description"].ToString();
                retVehicleMaintStatus.SortOrder = Convert.ToInt32(dt.Rows[0]["SortOrder"]);                
                retVehicleMaintStatus.CreatedDate = (DateTime)dt.Rows[0]["CreatedDate"];
                retVehicleMaintStatus.CreatedBy = dt.Rows[0]["CreatedBy"].ToString();
                retVehicleMaintStatus.UpdatedDate = (DateTime)dt.Rows[0]["UpdatedDate"];
                retVehicleMaintStatus.UpdatedBy = dt.Rows[0]["UpdatedBy"].ToString();
                return retVehicleMaintStatus;
            }
        }

        public bool SaveVehicleMaintStatus(VehicleMaintStatus uVehicleMaintStatus, DbTransaction transaction, String sConnStr)
        {
            //--- Prepare Dataset ---//

            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_VehicleMaintStatus";
            if (!dt.Columns.Contains("oprType"))
                dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Columns.Add("MaintStatusId", typeof(int));
            dt.Columns.Add("Description", typeof(String));
            dt.Columns.Add("SortOrder", typeof(int));
            dt.Columns.Add("CreatedDate", typeof(DateTime));
            dt.Columns.Add("CreatedBy", typeof(String));
            dt.Columns.Add("UpdatedDate", typeof(DateTime));
            dt.Columns.Add("UpdatedBy", typeof(String));
            
            DataRow dr = dt.NewRow();
            dr["oprType"] = DataRowState.Added;
            dr["MaintStatusId"] = uVehicleMaintStatus.MaintStatusId;
            dr["Description"] = uVehicleMaintStatus.Description;
            dr["SortOrder"] = uVehicleMaintStatus.SortOrder;
            dr["CreatedDate"] = uVehicleMaintStatus.CreatedDate;
            dr["CreatedBy"] = uVehicleMaintStatus.CreatedBy;
            dr["UpdatedDate"] = uVehicleMaintStatus.UpdatedDate;
            dr["UpdatedBy"] = uVehicleMaintStatus.UpdatedBy;
            dt.Rows.Add(dr);

            //--- Prepare Control Table ---//

            Connection _connection = new Connection(sConnStr);

            DataSet dsProcess = new DataSet();
            DataTable CTRLTBL = _connection.CTRLTBL();

            dsProcess.Tables.Add(CTRLTBL);
            dsProcess.Merge(dt);

            DataRow drCtrl = CTRLTBL.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "MaintStatusId";
            drCtrl["KeyIsIdentity"] = "FALSE";
            CTRLTBL.Rows.Add(drCtrl);

            //--- Process Data ---//

            CTRLTBL = _connection.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            if (CTRLTBL != null)
            {
                DataRow[] dRow = CTRLTBL.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }

        public bool UpdateVehicleMaintStatus(VehicleMaintStatus uVehicleMaintStatus, DbTransaction transaction, String sConnStr)
        {
            //--- Prepare Dataset ---//

            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_VehicleMaintStatus";
            if (!dt.Columns.Contains("oprType"))
                dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Columns.Add("MaintStatusId", uVehicleMaintStatus.MaintStatusId.GetType());
            dt.Columns.Add("Description", uVehicleMaintStatus.Description.GetType());
            dt.Columns.Add("SortOrder", uVehicleMaintStatus.SortOrder.GetType());
            dt.Columns.Add("CreatedDate", uVehicleMaintStatus.CreatedDate.GetType());
            dt.Columns.Add("CreatedBy", uVehicleMaintStatus.CreatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uVehicleMaintStatus.UpdatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uVehicleMaintStatus.UpdatedBy.GetType());
            
            DataRow dr = dt.NewRow();
            dr["oprType"] = DataRowState.Modified;
            dr["MaintStatusId"] = uVehicleMaintStatus.MaintStatusId;
            dr["Description"] = uVehicleMaintStatus.Description;
            dr["SortOrder"] = uVehicleMaintStatus.SortOrder;
            dr["CreatedDate"] = uVehicleMaintStatus.CreatedDate;
            dr["CreatedBy"] = uVehicleMaintStatus.CreatedBy;
            dr["UpdatedDate"] = uVehicleMaintStatus.UpdatedDate;
            dr["UpdatedBy"] = uVehicleMaintStatus.UpdatedBy;
            dt.Rows.Add(dr);

            //--- Prepare Control Table ---//

            Connection _connection = new Connection(sConnStr);

            DataSet dsProcess = new DataSet();
            DataTable CTRLTBL = _connection.CTRLTBL();

            dsProcess.Tables.Add(CTRLTBL);
            dsProcess.Merge(dt);

            DataRow drCtrl = CTRLTBL.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "MaintStatusId";
            drCtrl["KeyIsIdentity"] = "FALSE";
            CTRLTBL.Rows.Add(drCtrl);

            //--- Process Data ---//

            CTRLTBL = _connection.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            if (CTRLTBL != null)
            {
                DataRow[] dRow = CTRLTBL.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }

        public bool DeleteVehicleMaintStatus(VehicleMaintStatus uVehicleMaintStatus, String sConnStr)
        {
            //--- Prepare Dataset ---//

            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_VehicleMaintStatus";
            dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Columns.Add("MaintStatusId", uVehicleMaintStatus.MaintStatusId.GetType());

            DataRow dr = dt.NewRow();
            dr["oprType"] = DataRowState.Deleted;
            dr["MaintStatusId"] = uVehicleMaintStatus.MaintStatusId;

            dt.Rows.Add(dr);

            //--- Prepare Control Table ---//

            Connection _connection = new Connection(sConnStr);

            DataTable CTRLTBL = _connection.CTRLTBL();
            CTRLTBL.TableName = "CTRLTBL";

            DataSet objDataSet = new DataSet();

            objDataSet.Tables.Add(CTRLTBL);
            objDataSet.Merge(dt);

            DataRow drCtrl = CTRLTBL.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "MaintStatusId";
            drCtrl["KeyIsIdentity"] = "FALSE";
            CTRLTBL.Rows.Add(drCtrl);

            //--- Process Data ---//

            CTRLTBL = _connection.ProcessData(objDataSet, sConnStr).Tables["CTRLTBL"];

            if (CTRLTBL != null)
            {
                DataRow[] dRow = CTRLTBL.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }

    }
}