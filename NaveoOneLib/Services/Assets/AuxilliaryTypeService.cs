

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Assets;

namespace NaveoOneLib.Services.Assets
{
    public class AuxilliaryTypeService
    {
        Errors er = new Errors();
        public DataTable GetAuxilliaryType(String sConnStr)
        {
            String sqlString = @"SELECT * from GFI_FLT_AuxilliaryType order by Uid";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public DataTable GetAuxilliaryTypeById(int iID, String sConnStr)
        {
            String sqlString = @"SELECT * from GFI_FLT_AuxilliaryType WHERE Uid = ? order by Uid";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID.ToString(), sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else return dt;
        }

        public DataTable GetAuxilliaryTypeByAssetId(int iID, String sConnStr)
        {
            String sqlString = @"select t.*
 from GFI_FLT_Asset a
 inner join GFI_FLT_AssetDeviceMap adm on a.AssetID = adm.AssetID
 inner join GFI_FLT_DeviceAuxilliaryMap dam on dam.MapID = adm.MapID
 inner join GFI_FLT_AuxilliaryType t on dam.AuxilliaryType = t.Uid
where a.AssetID = ?";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID.ToString(), sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else return dt;
        }

        public DataTable GetAuxilliaryTypeByName(String sName, String sConnStr)
        {
            String sqlString = @"SELECT Uid, 
AuxilliaryTypeName, 
AuxilliaryTypeDescription, 
Field1, 
Field1Label, 
Field1Type, 
Field2, 
Field2Label, 
Field2Type, 
Field3, 
Field3Label, 
Field3Type, 
Field4, 
Field4Label, 
Field4Type, 
Field5, 
Field5Label, 
Field5Type, 
Field6, 
Field6Label, 
Field6Type, 
Field7, 
Field7Label, 
Field7Type, 
CreatedDate, 
CreatedBy, 
UpdatedDate, 
UpdatedBy from GFI_FLT_AuxilliaryType
WHERE AuxilliaryTypeName = '" + sName + "' order by Uid";

            DataTable dt = new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else return dt;
        }
        AuxilliaryType GetAuxilliaryType(DataRow dr)
        {
            AuxilliaryType retAuxilliaryType = new AuxilliaryType();
            retAuxilliaryType.Uid = Convert.ToInt32(dr["Uid"]);
            retAuxilliaryType.AuxilliaryTypeName = dr["AuxilliaryTypeName"].ToString();
            retAuxilliaryType.AuxilliaryTypeDescription = dr["AuxilliaryTypeDescription"].ToString();
            retAuxilliaryType.Field1 = dr["Field1"].ToString();
            retAuxilliaryType.Field1Label = dr["Field1Label"].ToString();
            retAuxilliaryType.Field1Type = dr["Field1Type"].ToString();
            retAuxilliaryType.Field2 = dr["Field2"].ToString();
            retAuxilliaryType.Field2Label = dr["Field2Label"].ToString();
            retAuxilliaryType.Field2Type = dr["Field2Type"].ToString();
            retAuxilliaryType.Field3 = dr["Field3"].ToString();
            retAuxilliaryType.Field3Label = dr["Field3Label"].ToString();
            retAuxilliaryType.Field3Type = dr["Field3Type"].ToString();
            retAuxilliaryType.Field4 = dr["Field4"].ToString();
            retAuxilliaryType.Field4Label = dr["Field4Label"].ToString();
            retAuxilliaryType.Field4Type = dr["Field4Type"].ToString();
            retAuxilliaryType.Field5 = dr["Field5"].ToString();
            retAuxilliaryType.Field5Label = dr["Field5Label"].ToString();
            retAuxilliaryType.Field5Type = dr["Field5Type"].ToString();
            retAuxilliaryType.Field6 = dr["Field6"].ToString();
            retAuxilliaryType.Field6Label = dr["Field6Label"].ToString();
            retAuxilliaryType.Field6Type = dr["Field6Type"].ToString();
            retAuxilliaryType.Field7 = dr["Field7"].ToString();
            retAuxilliaryType.Field7Label = dr["Field7Label"].ToString();
            retAuxilliaryType.Field7Type = dr["Field7Type"].ToString();
            if (!String.IsNullOrEmpty(dr["CreatedDate"].ToString()))
                retAuxilliaryType.CreatedDate = (DateTime)dr["CreatedDate"];
            retAuxilliaryType.CreatedBy = dr["CreatedBy"].ToString();
            if (!String.IsNullOrEmpty(dr["UpdatedDate"].ToString()))
                retAuxilliaryType.UpdatedDate = (DateTime)dr["UpdatedDate"];
            retAuxilliaryType.UpdatedBy = dr["UpdatedBy"].ToString();
            return retAuxilliaryType;
        }
        public AuxilliaryType GetAuxilliaryTypeByNames(String sName, String sConnStr)
        {
            AuxilliaryType at = null;

            DataTable dt = GetAuxilliaryTypeByName(sName, sConnStr);
            if (dt != null)
                at = GetAuxilliaryType(dt.Rows[0]);

            return at;
        }

        public bool SaveAuxilliaryType(AuxilliaryType uAuxilliaryType, Boolean bInsert, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_AuxilliaryType";
            dt.Columns.Add("Uid", uAuxilliaryType.Uid.GetType());
            dt.Columns.Add("AuxilliaryTypeName", uAuxilliaryType.AuxilliaryTypeName.GetType());
            dt.Columns.Add("AuxilliaryTypeDescription", uAuxilliaryType.AuxilliaryTypeDescription.GetType());
            dt.Columns.Add("Field1", uAuxilliaryType.Field1.GetType());
            dt.Columns.Add("Field1Label", uAuxilliaryType.Field1Label.GetType());
            dt.Columns.Add("Field1Type", uAuxilliaryType.Field1Type.GetType());
            dt.Columns.Add("Field2", uAuxilliaryType.Field2.GetType());
            dt.Columns.Add("Field2Label", uAuxilliaryType.Field2Label.GetType());
            dt.Columns.Add("Field2Type", uAuxilliaryType.Field2Type.GetType());
            dt.Columns.Add("Field3", uAuxilliaryType.Field3.GetType());
            dt.Columns.Add("Field3Label", uAuxilliaryType.Field3Label.GetType());
            dt.Columns.Add("Field3Type", uAuxilliaryType.Field3Type.GetType());
            dt.Columns.Add("Field4", uAuxilliaryType.Field4.GetType());
            dt.Columns.Add("Field4Label", uAuxilliaryType.Field4Label.GetType());
            dt.Columns.Add("Field4Type", uAuxilliaryType.Field4Type.GetType());
            dt.Columns.Add("Field5", uAuxilliaryType.Field5.GetType());
            dt.Columns.Add("Field5Label", uAuxilliaryType.Field5Label.GetType());
            dt.Columns.Add("Field5Type", uAuxilliaryType.Field5Type.GetType());
            dt.Columns.Add("Field6", uAuxilliaryType.Field6.GetType());
            dt.Columns.Add("Field6Label", uAuxilliaryType.Field6Label.GetType());
            dt.Columns.Add("Field6Type", uAuxilliaryType.Field6Type.GetType());
            dt.Columns.Add("Field7", uAuxilliaryType.Field7.GetType());
            dt.Columns.Add("Field7Label", uAuxilliaryType.Field7Label.GetType());
            dt.Columns.Add("Field7Type", uAuxilliaryType.Field7Type.GetType());
            if (bInsert)
            {
                dt.Columns.Add("CreatedDate", uAuxilliaryType.CreatedDate.GetType());
                //dt.Columns.Add("CreatedBy", uAuxilliaryType.CreatedBy.GetType());
            }
            else
            {
                dt.Columns.Add("UpdatedDate", uAuxilliaryType.UpdatedDate.GetType());
                //dt.Columns.Add("UpdatedBy", uAuxilliaryType.UpdatedBy.GetType());
            }
            DataRow dr = dt.NewRow();
            dr["Uid"] = uAuxilliaryType.Uid;
            dr["AuxilliaryTypeName"] = uAuxilliaryType.AuxilliaryTypeName;
            dr["AuxilliaryTypeDescription"] = uAuxilliaryType.AuxilliaryTypeDescription;
            dr["Field1"] = uAuxilliaryType.Field1;
            dr["Field1Label"] = uAuxilliaryType.Field1Label;
            dr["Field1Type"] = uAuxilliaryType.Field1Type;
            dr["Field2"] = uAuxilliaryType.Field2;
            dr["Field2Label"] = uAuxilliaryType.Field2Label;
            dr["Field2Type"] = uAuxilliaryType.Field2Type;
            dr["Field3"] = uAuxilliaryType.Field3;
            dr["Field3Label"] = uAuxilliaryType.Field3Label;
            dr["Field3Type"] = uAuxilliaryType.Field3Type;
            dr["Field4"] = uAuxilliaryType.Field4;
            dr["Field4Label"] = uAuxilliaryType.Field4Label;
            dr["Field4Type"] = uAuxilliaryType.Field4Type;
            dr["Field5"] = uAuxilliaryType.Field5;
            dr["Field5Label"] = uAuxilliaryType.Field5Label;
            dr["Field5Type"] = uAuxilliaryType.Field5Type;
            dr["Field6"] = uAuxilliaryType.Field6;
            dr["Field6Label"] = uAuxilliaryType.Field6Label;
            dr["Field6Type"] = uAuxilliaryType.Field6Type;
            dr["Field7"] = uAuxilliaryType.Field7;
            dr["Field7Label"] = uAuxilliaryType.Field7Label;
            dr["Field7Type"] = uAuxilliaryType.Field7Type;
            if (bInsert)
            {
                dr["CreatedDate"] = DateTime.UtcNow;
                //dr["CreatedBy"] = uAuxilliaryType.CreatedBy;
            }
            else
            {
                dr["UpdatedDate"] = DateTime.UtcNow;
                //dr["UpdatedBy"] = uAuxilliaryType.UpdatedBy;
            }
            if (!dt.Columns.Contains("oprType"))
                dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "Uid";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            dtCtrl.Rows.Add(drCtrl);

            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);
            dsProcess.Merge(dtCtrl);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public bool UpdateAuxilliaryType(AuxilliaryType uAuxilliaryType, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_AuxilliaryType";
            dt.Columns.Add("Uid", uAuxilliaryType.Uid.GetType());
            dt.Columns.Add("AuxilliaryTypeName", uAuxilliaryType.AuxilliaryTypeName.GetType());
            dt.Columns.Add("AuxilliaryTypeDescription", uAuxilliaryType.AuxilliaryTypeDescription.GetType());
            dt.Columns.Add("Field1", uAuxilliaryType.Field1.GetType());
            dt.Columns.Add("Field1Label", uAuxilliaryType.Field1Label.GetType());
            dt.Columns.Add("Field1Type", uAuxilliaryType.Field1Type.GetType());
            dt.Columns.Add("Field2", uAuxilliaryType.Field2.GetType());
            dt.Columns.Add("Field2Label", uAuxilliaryType.Field2Label.GetType());
            dt.Columns.Add("Field2Type", uAuxilliaryType.Field2Type.GetType());
            dt.Columns.Add("Field3", uAuxilliaryType.Field3.GetType());
            dt.Columns.Add("Field3Label", uAuxilliaryType.Field3Label.GetType());
            dt.Columns.Add("Field3Type", uAuxilliaryType.Field3Type.GetType());
            dt.Columns.Add("Field4", uAuxilliaryType.Field4.GetType());
            dt.Columns.Add("Field4Label", uAuxilliaryType.Field4Label.GetType());
            dt.Columns.Add("Field4Type", uAuxilliaryType.Field4Type.GetType());
            dt.Columns.Add("Field5", uAuxilliaryType.Field5.GetType());
            dt.Columns.Add("Field5Label", uAuxilliaryType.Field5Label.GetType());
            dt.Columns.Add("Field5Type", uAuxilliaryType.Field5Type.GetType());
            dt.Columns.Add("Field6", uAuxilliaryType.Field6.GetType());
            dt.Columns.Add("Field6Label", uAuxilliaryType.Field6Label.GetType());
            dt.Columns.Add("Field6Type", uAuxilliaryType.Field6Type.GetType());
            dt.Columns.Add("Field7", uAuxilliaryType.Field7.GetType());
            dt.Columns.Add("Field7Label", uAuxilliaryType.Field7Label.GetType());
            dt.Columns.Add("Field7Type", uAuxilliaryType.Field7Type.GetType());
            dt.Columns.Add("CreatedDate", uAuxilliaryType.CreatedDate.GetType());
            dt.Columns.Add("CreatedBy", uAuxilliaryType.CreatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uAuxilliaryType.UpdatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uAuxilliaryType.UpdatedBy.GetType());
            DataRow dr = dt.NewRow();
            dr["Uid"] = uAuxilliaryType.Uid;
            dr["AuxilliaryTypeName"] = uAuxilliaryType.AuxilliaryTypeName;
            dr["AuxilliaryTypeDescription"] = uAuxilliaryType.AuxilliaryTypeDescription;
            dr["Field1"] = uAuxilliaryType.Field1;
            dr["Field1Label"] = uAuxilliaryType.Field1Label;
            dr["Field1Type"] = uAuxilliaryType.Field1Type;
            dr["Field2"] = uAuxilliaryType.Field2;
            dr["Field2Label"] = uAuxilliaryType.Field2Label;
            dr["Field2Type"] = uAuxilliaryType.Field2Type;
            dr["Field3"] = uAuxilliaryType.Field3;
            dr["Field3Label"] = uAuxilliaryType.Field3Label;
            dr["Field3Type"] = uAuxilliaryType.Field3Type;
            dr["Field4"] = uAuxilliaryType.Field4;
            dr["Field4Label"] = uAuxilliaryType.Field4Label;
            dr["Field4Type"] = uAuxilliaryType.Field4Type;
            dr["Field5"] = uAuxilliaryType.Field5;
            dr["Field5Label"] = uAuxilliaryType.Field5Label;
            dr["Field5Type"] = uAuxilliaryType.Field5Type;
            dr["Field6"] = uAuxilliaryType.Field6;
            dr["Field6Label"] = uAuxilliaryType.Field6Label;
            dr["Field6Type"] = uAuxilliaryType.Field6Type;
            dr["Field7"] = uAuxilliaryType.Field7;
            dr["Field7Label"] = uAuxilliaryType.Field7Label;
            dr["Field7Type"] = uAuxilliaryType.Field7Type;
            dr["CreatedDate"] = uAuxilliaryType.CreatedDate;
            dr["CreatedBy"] = uAuxilliaryType.CreatedBy;
            dr["UpdatedDate"] = uAuxilliaryType.UpdatedDate;
            dr["UpdatedBy"] = uAuxilliaryType.UpdatedBy;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "Uid = '" + uAuxilliaryType.Uid + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool DeleteAuxilliaryType(AuxilliaryType uAuxilliaryType, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_FLT_AuxilliaryType", "Uid = '" + uAuxilliaryType.Uid + "'", sConnStr))
                return true;
            else
                return false;
        }

    }
}



