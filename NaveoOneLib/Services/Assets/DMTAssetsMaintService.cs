﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
//using NaveoOneLib.UI;

using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Reflection;
using NaveoOneLib.Models.Assets;

namespace NaveoOneLib.Services.Assets
{

    public class DMTAssetsMaintService
    {
        public event Action<string> DataReceived;

        [MethodImpl(MethodImplOptions.NoInlining)]
        public static string GetMethodName()
        {
            var st = new StackTrace(new StackFrame(1));
            return st.GetFrame(0).GetMethod().Name;
        } 

        public DMTAssetsMaintService()
        {

        }

        private void LogMessage(String sMethodName, String sMessage)
        {
            DataReceived(sMethodName + " - " + sMessage);
        }

        //============================== Get Data Tables ==============================//
        
        public DataSet GetAssetsFromSource(String sConnStr)
        {
            String sqlString = @"SELECT *
                                 FROM Vehicle (nolock)
                                 ORDER BY iID";

            return new Connection(sConnStr).GetDataSet(sqlString, "Geotab");
        }

        public DataSet GetMaintTypesFromSource(String sConnStr)
        {
            String sqlString = @"SELECT *
                                 FROM EventRules (nolock)
                                 ORDER BY iID";

            return new Connection(sConnStr).GetDataSet(sqlString, "Geotab");
        }

        public DataSet GetMaintRecsFromSource(int iEventRuleID, String sConnStr)
        {
            String sqlString = @"SELECT *
                                 FROM Events (nolock)
                                 WHERE iEventRuleID = " + iEventRuleID + @"
                                 ORDER BY iID";

            return new Connection(sConnStr).GetDataSet(sqlString, "Geotab");
        }

        public DataTable GetAssetFromTarget(String sDescription, String sConnStr)
        {
            String sqlString = @"SELECT *
                                 FROM GFI_AMM_AssetExtProVehicles (nolock)
                                 WHERE Description = ?";

            return new Connection(sConnStr).GetDataTableBy(sqlString, sDescription, sConnStr);
        }

        public DataTable GetAllAssetsFromTarget(String sConnStr)
        {
            String sqlString = @"SELECT *
                                 FROM GFI_AMM_AssetExtProVehicles (nolock)
                                 WHERE Description LIKE 'DataMigrationId:%'";

            return new Connection(sConnStr).GetDataTableBy(sqlString, string.Empty, sConnStr);
        }

        public DataTable GetMaintRecsFromTarget(int iMaintTypeId, String sConnStr)
        {
            String sqlString = @"SELECT *
                                 FROM GFI_AMM_VehicleMaintenance (nolock)
                                 WHERE MaintTypeId_cbo = ?";

            return new Connection(sConnStr).GetDataTableBy(sqlString, iMaintTypeId.ToString(), sConnStr);
        }

        public DataTable GetMaintRecsFromTarget2(int iMaintTypeId, String sConnStr)
        {
            String sqlString = @"SELECT *
                                 FROM GFI_AMM_VehicleMaintenance (nolock)
                                 WHERE AssetId IN (SELECT AssetId FROM GFI_AMM_AssetExtProVehicles WHERE Description LIKE 'DataMigrationId:%')
                                 AND MaintTypeId_cbo NOT IN (" + iMaintTypeId + ")";

            return new Connection(sConnStr).GetDataTableBy(sqlString, iMaintTypeId.ToString(), sConnStr);
        }

        public DataTable GetMaintRecsFromTarget3(String sConnStr)
        {
            String sqlString = @"SELECT *
                                 FROM GFI_AMM_VehicleMaintenance (nolock)
                                 WHERE AssetId IN (SELECT AssetId FROM GFI_AMM_AssetExtProVehicles WHERE Description LIKE 'DataMigrationId:%')";

            return new Connection(sConnStr).GetDataTableBy(sqlString, string.Empty, sConnStr);
        }

        //============================== Migration Steps ==============================//

        public int UDF_Migrate_Assets(DataSet dsSrc, String sConnStr)
        {
            LogMessage(GetMethodName(), "Data Migration - Started");

            int iRecCount = 0;

            foreach (DataRow dr in dsSrc.Tables[0].Rows)
            {
                //Get Asset by Description
                string sAssetName = dr["sDescription"].ToString();

                Asset myAsset = new AssetService().GetAssetByName(sAssetName, sConnStr);

                if (myAsset != null)
                {
                    //Save Asset in Assets Maintenance
                    AssetExtProVehicle myAssetMaint = new AssetExtProVehicle();

                    myAssetMaint.AssetId = myAsset.AssetID;
                    myAssetMaint.Description = "DataMigrationId: " + dr["iID"].ToString();

                    AssetExtProVehicleService AEPVS = new AssetExtProVehicleService();

                    if (AEPVS.SaveAsset(myAssetMaint, sConnStr) == true)
                    {
                        iRecCount++;
                        LogMessage(GetMethodName(), "Asset Id [" + myAssetMaint.AssetId + " - " + sAssetName + "] - Migrated successfully");
                    }
                    else
                    {
                        LogMessage(GetMethodName(), "Asset Id [" + myAssetMaint.AssetId + " - " + sAssetName + "] - Error encountered during asset creation, attempting update...");
                        
                        if (AEPVS.UpdateAsset(myAssetMaint, sConnStr) == true)
                        {
                            iRecCount++;
                            LogMessage(GetMethodName(), "Asset Id [" + myAssetMaint.AssetId + " - " + sAssetName + "] - Updated successfully");
                        }
                        else
                        {
                            LogMessage(GetMethodName(), "Asset Id [" + myAssetMaint.AssetId + " - " + sAssetName + "] - Error encountered during asset update");
                        }
                    }
                }
                else
                {
                    LogMessage(GetMethodName(), "Asset [" + sAssetName + "] - Not found on target db");
                }
            }

            LogMessage(GetMethodName(), "Data Migration - Completed");

            return iRecCount;
        }

        public void UDF_Migrate_MaintRecs(DataSet dsSrc, out int iTypes, out int iRecs, String sConnStr)
        {
            LogMessage(GetMethodName(), "Data Migration - Started");

            iTypes = 0;
            iRecs = 0;
            VehicleMaintTypeService vehicleMaintTypeService = new VehicleMaintTypeService();
            foreach (DataRow dr in dsSrc.Tables[0].Rows)
            {
                //Get Maint Type by Description (From Source)

                int iEventRuleID = Convert.ToInt32(dr["iID"]);
                string sMaintTypeName = dr["sDescription"].ToString();
                int iDuration = String.IsNullOrEmpty(dr["iMonths"].ToString()) ?  0 : Convert.ToInt32(dr["iMonths"]);
                int iKilometers = String.IsNullOrEmpty(dr["iKilometers"].ToString()) ? 0 : Convert.ToInt32(dr["iKilometers"]);
                int iEngineHours = String.IsNullOrEmpty(dr["iEngineHours"].ToString()) ? 0 : Convert.ToInt32(dr["iEngineHours"]);


                //Save Maint Type (To Target)

                VehicleMaintType vmt = new VehicleMaintType();
                vmt.MaintCatId_cbo = 0;
                vmt.Description = sMaintTypeName;
                vmt.OccurrenceType = 0;
                vmt.OccurrenceFixedDateTh = 0;
                vmt.OccurrenceDuration = iDuration;
                vmt.OccurrenceDurationTh = 0;
                vmt.OccurrencePeriod_cbo = "MONTH(S)";
                vmt.OccurrenceKM = iKilometers;
                vmt.OccurrenceKMTh = 0;
                vmt.OccurrenceEngineHrs = iEngineHours;
                vmt.OccurrenceEngineHrsTh = 0;
                vmt.CreatedDate = DateTime.UtcNow;
                vmt.CreatedBy = Globals.uLogin.Username;

                DataTable CTRLTBL = new DataTable();

                vehicleMaintTypeService.SaveVehicleMaintType(vmt, true, sConnStr);
                //Reza CTRLTBL was out from SaveVehicleMaintType. Code removed. will use baseModel instead
                if (CTRLTBL != null)
                {
                    DataRow[] dRow = CTRLTBL.Select("TblName = 'GFI_AMM_VehicleMaintType'");

                    if (dRow.Length > 0)
                    {
                        int iRecAffected = Convert.ToInt32(dRow[0]["RecAffected"].ToString());

                        if (iRecAffected > 0)
                        {
                            iTypes++;

                            int iMaintTypeId = Convert.ToInt32(dRow[0]["NextIdValue"].ToString());

                            DataSet dsMaintRecs = GetMaintRecsFromSource(iEventRuleID, sConnStr);

                            iRecs += UDF_Migrate_MaintRecsForMaintType(iMaintTypeId, dsMaintRecs, sConnStr);

                            LogMessage(GetMethodName(), "Maint Type [" + vmt.Description + "] - Migrated successfully");
                        }
                        else
                        {
                            LogMessage(GetMethodName(), "Maint Type [" + vmt.Description + "] - Error encountered during save");
                        }
                    }
                    else
                    {
                        LogMessage(GetMethodName(), "Maint Type [" + vmt.Description + "] - Error encountered during save");
                    }
                }
                else
                    LogMessage(GetMethodName(), "Maint Type [" + vmt.Description + "] - Error encountered during save");

            }

            LogMessage(GetMethodName(), "Data Migration - Completed");

        }

        public int UDF_Migrate_MaintRecsForMaintType(int iMaintTypeId, DataSet dsSrc, String sConnStr)
        {
            LogMessage(GetMethodName(), "Data Migration - Started");

            int iRecCount = 0;

            //Gets Pre-Processing data

            int iMaintStatusId;
            DataRow[] dRow1 = Globals.dtVehicleMaintStatus.Select("Description = 'In Progress'");
            if (dRow1.Length > 0)
                iMaintStatusId = Convert.ToInt32(dRow1[0]["MaintStatusId"]);
            else
                iMaintStatusId = 4;

            int iInsCoverType;
            DataRow[] dRow2 = Globals.dtInsCoverTypes.Select("Description = 'Comprehensive'");
            if (dRow2.Length > 0)
                iInsCoverType = Convert.ToInt32(dRow2[0]["CoverTypeId"]);
            else
                iInsCoverType = 1;


            //Process Data

            foreach (DataRow dr in dsSrc.Tables[0].Rows)
            {
                //Get Maint Type Details
                int iID = Convert.ToInt32(dr["iID"]);
                int iVehicleIdSrc = Convert.ToInt32(dr["iVehicleID"]);
                int iEventRuleID = Convert.ToInt32(dr["iVehicleID"]);
                DateTime dtDateTime = (DateTime)dr["dtDateTime"];
                Boolean bActive = (Boolean)dr["bActive"];
                String sComments = dr["sComments"].ToString();
                int iCurrentOdometer = Convert.ToInt32(dr["iCurrentOdometer"]);
                int iAdjustedOdometer = Convert.ToInt32(dr["iAdjustedOdometer"]);
                int iCurrentEngineHours = Convert.ToInt32(dr["iCurrentEngineHours"]);
                int iAdjustedEngineHours = Convert.ToInt32(dr["iAdjustedEngineHours"]);

                //Get Asset from Target (To get newly allocated AssetId on Target DB)
                string sDescription = "DataMigrationId: " + iVehicleIdSrc;

                DataTable dt = GetAssetFromTarget(sDescription, sConnStr);

                if (dt.Rows.Count > 0)
                {
                    //Save Maint Rec (To Target)
                    VehicleMaintenance VehicleMaintRec = new VehicleMaintenance();
                    
                    VehicleMaintRec.AssetId = Convert.ToInt32(dt.Rows[0]["AssetId"].ToString());
                    VehicleMaintRec.MaintTypeId_cbo = iMaintTypeId;
                    
                    VehicleMaintRec.StartDate = dtDateTime;
                    VehicleMaintRec.EndDate = dtDateTime;

                    VehicleMaintRec.ActualOdometer = iAdjustedOdometer;
                    VehicleMaintRec.CalculatedOdometer = iCurrentOdometer;
                    VehicleMaintRec.ActualEngineHrs = iAdjustedEngineHours;
                    VehicleMaintRec.CalculatedEngineHrs = iCurrentEngineHours;
                    
                    VehicleMaintRec.MaintDescription = sComments;
                    VehicleMaintRec.AdditionalInfo = sComments;

                    VehicleMaintRec.MaintStatusId_cbo = iMaintStatusId;
                    VehicleMaintRec.CoverTypeId_cbo = iInsCoverType;

                    VehicleMaintRec.EstimatedValue = 0;
                    VehicleMaintRec.VATAmount = 0;
                    VehicleMaintRec.TotalCost = 0;

                    VehicleMaintenanceService vms = new VehicleMaintenanceService();

                    if (vms.SaveMaintRec(DataRowState.Added, VehicleMaintRec, sConnStr) == true)
                    {
                        iRecCount++;

                        LogMessage(GetMethodName(), "Maint Record [" + iID.ToString() + "] - Migrated successfully");
                    }
                    else
                    {
                        LogMessage(GetMethodName(), "Asset Record [" + iID.ToString() + "] - Error encountered during save");
                    }
                }
                else
                {
                    LogMessage(GetMethodName(), "Asset Record [" + iVehicleIdSrc.ToString() + "] - Error getting record from Target");
                }
            }

            LogMessage(GetMethodName(), "Data Migration - Completed");

            return iRecCount;
        }

        public int UDF_Process_Data1(String sConnStr)
        {
            LogMessage(GetMethodName(), "Data Processing 1 - Started");

            int iRecCount = 0;

            string sWorkDetails1 = string.Empty;
            string sWorkDetails2 = string.Empty;

            //Gets Pre-Processing data

            int iMaintStatusId_Completed;
            DataRow[] dRow1 = Globals.dtVehicleMaintStatus.Select("Description = 'Completed'");
            if (dRow1.Length > 0)
                iMaintStatusId_Completed = Convert.ToInt32(dRow1[0]["MaintStatusId"]);
            else
                iMaintStatusId_Completed = 5;

            int iMaintStatusId_InProgress;
            DataRow[] dRow2 = Globals.dtVehicleMaintStatus.Select("Description = 'In Progress'");
            if (dRow1.Length > 0)
                iMaintStatusId_InProgress = Convert.ToInt32(dRow2[0]["MaintStatusId"]);
            else
                iMaintStatusId_InProgress = 4;

            //Get Vehicle Maint Type Info

            int iMaintTypeId = 0;
            int iOccurrenceType = 0;

            VehicleMaintType vmt = new VehicleMaintType();
            vmt = new VehicleMaintTypeService().GetVehicleMaintTypeByDescription("Maintenance Record Book", sConnStr);
            if (vmt != null)
            {
                iMaintTypeId = vmt.MaintTypeId;
                iOccurrenceType = vmt.OccurrenceType;
            }
            else
            {
                LogMessage(GetMethodName(), "Could not retrieve Vehicle Maint Type [Maintenance Record Book]. Process Aborted.");
                return 0;
            }

            DataTable dtSrc = GetMaintRecsFromTarget(iMaintTypeId, sConnStr);

            //Define Vehicle Maint Items
            DataTable dtVMI = new DataTable();
            dtVMI.TableName = "GFI_AMM_VehicleMaintItems";
            dtVMI.Columns.Add("oprType", typeof(DataRowState));
            dtVMI.Columns.Add("MaintURI", typeof(int));
            dtVMI.Columns.Add("ItemCode", typeof(string));
            dtVMI.Columns.Add("Description", typeof(string));
            dtVMI.Columns.Add("Quantity", typeof(int));
            dtVMI.Columns.Add("UnitCost", typeof(Decimal));
            dtVMI.Columns.Add("CreatedDate", typeof(DateTime));
            dtVMI.Columns.Add("CreatedBy", typeof(string));

            //Process Data

            foreach (DataRow dr in dtSrc.Rows)
            {
                //Save Maint Rec
                VehicleMaintenance VehicleMaintRec = new VehicleMaintenance();

                VehicleMaintRec.URI = Convert.ToInt32(dr["URI"]);
                
                try
                {
                    VehicleMaintRec.AssetId = Convert.ToInt32(dr["AssetId"]);
                    VehicleMaintRec.MaintTypeId_cbo = Convert.ToInt32(dr["MaintTypeId_cbo"]);

                    VehicleMaintRec.VMTLURI = 0;

                    VehicleMaintRec.ActualOdometer = Convert.ToInt32(dr["ActualOdometer"]);
                    VehicleMaintRec.CalculatedOdometer = Convert.ToInt32(dr["CalculatedOdometer"]);
                    VehicleMaintRec.ActualEngineHrs = Convert.ToInt32(dr["ActualEngineHrs"]);
                    VehicleMaintRec.CalculatedEngineHrs = Convert.ToInt32(dr["CalculatedEngineHrs"]);

                    VehicleMaintRec.MaintDescription = dr["MaintDescription"].ToString();
                    VehicleMaintRec.AdditionalInfo = dr["AdditionalInfo"].ToString();

                    VehicleMaintRec.MaintStatusId_cbo = iMaintStatusId_Completed;

                    //Process field [AdditionalInfo]
                    char[] delimiterChars = { ';' };
                    String[] sAdditionalInfo = dr["AdditionalInfo"].ToString().Split(delimiterChars);

                    //[Start Date & End Date]
                    if ((sAdditionalInfo.Length > 0) && (!string.IsNullOrEmpty(sAdditionalInfo[0])))
                    {
                        try
                        {
                            VehicleMaintRec.StartDate = Convert.ToDateTime(sAdditionalInfo[0]);
                            VehicleMaintRec.EndDate = Convert.ToDateTime(sAdditionalInfo[0]);
                        }
                        catch (Exception ex)
                        {
                            VehicleMaintRec.MaintStatusId_cbo = iMaintStatusId_InProgress;
                            LogMessage(GetMethodName(), "Asset Record [" + VehicleMaintRec.URI.ToString() + "] - " + ex.Message);
                        }
                    }
                    else
                    {
                        VehicleMaintRec.MaintStatusId_cbo = iMaintStatusId_InProgress;
                        LogMessage(GetMethodName(), "Asset Record [" + VehicleMaintRec.URI.ToString() + "] - Date Missing");
                    }

                    //Company Name
                    if ((sAdditionalInfo.Length > 1) && !string.IsNullOrEmpty(sAdditionalInfo[1]))
                    {
                        VehicleMaintRec.CompanyName = sAdditionalInfo[1];
                    }
                    else
                    {
                        LogMessage(GetMethodName(), "Asset Record [" + VehicleMaintRec.URI.ToString() + "] - Company Name Missing");
                    }

                    //PO Number (Reference 1)
                    if ((sAdditionalInfo.Length > 2) && !string.IsNullOrEmpty(sAdditionalInfo[2]))
                    {
                        VehicleMaintRec.CompanyRef = sAdditionalInfo[2];
                    }
                    else
                    {
                        LogMessage(GetMethodName(), "Asset Record [" + VehicleMaintRec.URI.ToString() + "] - PO Number (Reference 1) Missing");
                    }

                    //Invoice Number (Reference 2)
                    if ((sAdditionalInfo.Length > 3) && !string.IsNullOrEmpty(sAdditionalInfo[3]))
                    {
                        VehicleMaintRec.CompanyRef2 = sAdditionalInfo[3];
                    }
                    else
                    {
                        LogMessage(GetMethodName(), "Asset Record [" + VehicleMaintRec.URI.ToString() + "] - Invoice Number (Reference 2) Missing");
                    }

                    //Work Details 1 (Maint Description)
                    if ((sAdditionalInfo.Length > 4) && !string.IsNullOrEmpty(sAdditionalInfo[4]))
                    {
                        sWorkDetails1 = sAdditionalInfo[4];
                    }
                    else
                    {
                        LogMessage(GetMethodName(), "Asset Record [" + VehicleMaintRec.URI.ToString() + "] - Work Details 1 (Maint Description) Missing");
                    }

                    //Work Details 2 (Maint Description)
                    if ((sAdditionalInfo.Length > 5) && !string.IsNullOrEmpty(sAdditionalInfo[5]))
                    {
                        sWorkDetails2 = sAdditionalInfo[5];
                    }
                    else
                    {
                        LogMessage(GetMethodName(), "Asset Record [" + VehicleMaintRec.URI.ToString() + "] - Work Details 2 Missing");
                    }

                    //Amount
                    VehicleMaintRec.TotalCost = 0;
                    if ((sAdditionalInfo.Length > 6) && !string.IsNullOrEmpty(sAdditionalInfo[6]))
                    {
                        VehicleMaintRec.TotalCost = Convert.ToDecimal(sAdditionalInfo[6]);
                    }
                    else
                    {
                        LogMessage(GetMethodName(), "Asset Record [" + VehicleMaintRec.URI.ToString() + "] - Amount Missing");
                    }

                    //VAT
                    VehicleMaintRec.VATAmount = 0;
                    if ((sAdditionalInfo.Length > 7) && !string.IsNullOrEmpty(sAdditionalInfo[7]))
                    {
                        Decimal dVATPercenatge = Convert.ToDecimal(sAdditionalInfo[7]);

                        if (dVATPercenatge > 0)
                        {
                            VehicleMaintRec.VATAmount = VehicleMaintRec.TotalCost * dVATPercenatge / 100;
                            VehicleMaintRec.TotalCost += VehicleMaintRec.VATAmount;
                        }
                    }
                    else
                    {
                        LogMessage(GetMethodName(), "Asset Record [" + VehicleMaintRec.URI.ToString() + "] - VAT Missing");
                    }

                    VehicleMaintRec.CoverTypeId_cbo = Convert.ToInt32(dr["CoverTypeId_cbo"]);
                    VehicleMaintRec.EstimatedValue = (Decimal)dr["EstimatedValue"];

                    //Updates Vehicle Maint Items
                    dtVMI.Clear();

                    DataRow drVMI = dtVMI.NewRow();
                    drVMI["oprType"] = DataRowState.Added;
                    drVMI["MaintURI"] = VehicleMaintRec.URI;
                    drVMI["ItemCode"] = "001";
                    drVMI["Description"] = sWorkDetails2;
                    drVMI["Quantity"] = 1;
                    drVMI["UnitCost"] = VehicleMaintRec.TotalCost + VehicleMaintRec.VATAmount;
                    drVMI["CreatedDate"] = DateTime.UtcNow;
                    drVMI["CreatedBy"] = Globals.uLogin.Username;
                    dtVMI.Rows.Add(drVMI);

                    //Saves Record
                    VehicleMaintTypeLink vmtl = new VehicleMaintTypeLink();
                    DataSet dsOut = new DataSet();

                    VehicleMaintenanceService vms = new VehicleMaintenanceService();

                    if (vms.UpdateVehicleMaintenance(VehicleMaintRec, dtVMI, vmtl, iOccurrenceType, out dsOut, null, sConnStr) == true)
                    {
                        iRecCount++;

                        LogMessage(GetMethodName(), "Maint Record [" + VehicleMaintRec.URI.ToString() + "] / Asset Id [" + VehicleMaintRec.AssetId + "]- Updated successfully");
                    }
                    else
                    {
                        LogMessage(GetMethodName(), "Maint Record [" + VehicleMaintRec.URI.ToString() + "] / Asset Id [" + VehicleMaintRec.AssetId + "] -  Error encountered during save");
                    }
                }
                catch (Exception ex)
                {
                    LogMessage(GetMethodName(), "Maint Record [" + VehicleMaintRec.URI.ToString() + "] - Error: " + ex.Message);
                }
            }

            LogMessage(GetMethodName(), "Data Processing 1 - Completed");

            return iRecCount;
        }

        public int UDF_Process_Data2(String sConnStr)
        {
            LogMessage(GetMethodName(), "Data Processing 2 - Started");

            int iRecCount = 0;

            string sWorkDetails2 = string.Empty;

            //Prep - Define Vehicle Maint Items
            DataTable dtVMI = new DataTable();
            dtVMI.TableName = "GFI_AMM_VehicleMaintItems";
            dtVMI.Columns.Add("oprType", typeof(DataRowState));
            dtVMI.Columns.Add("MaintURI", typeof(int));
            dtVMI.Columns.Add("ItemCode", typeof(string));
            dtVMI.Columns.Add("Description", typeof(string));
            dtVMI.Columns.Add("Quantity", typeof(int));
            dtVMI.Columns.Add("UnitCost", typeof(Decimal));
            dtVMI.Columns.Add("CreatedDate", typeof(DateTime));
            dtVMI.Columns.Add("CreatedBy", typeof(string));

            //Gets Pre-Processing data

            //Get Completion Status [In Progress]
            int iMaintStatusInProgress = 0;
            DataRow[] drMaintStatusInProgress = Globals.dtVehicleMaintStatus.Select("Description = 'In Progress'");
            if (drMaintStatusInProgress.Length > 0)
                iMaintStatusInProgress = Convert.ToInt32(drMaintStatusInProgress[0]["MaintStatusId"]);

            //Get Completion Status [Valid]
            int iMaintStatusValid = 0;
            DataRow[] drMaintStatusValid = Globals.dtVehicleMaintStatus.Select("Description = 'Valid'");
            if (drMaintStatusValid.Length > 0)
                iMaintStatusValid = Convert.ToInt32(drMaintStatusValid[0]["MaintStatusId"]);

            //Get Completion Status [Expired]
            int iMaintStatusExpired = 0;
            DataRow[] drMaintStatusExpired = Globals.dtVehicleMaintStatus.Select("Description = 'Expired'");
            if (drMaintStatusExpired.Length > 0)
                iMaintStatusExpired = Convert.ToInt32(drMaintStatusExpired[0]["MaintStatusId"]);

            //Get Completion Status [Completed]
            int iMaintStatusCompleted = 0;
            DataRow[] drMaintStatusCompleted = Globals.dtVehicleMaintStatus.Select("Description = 'Completed'");
            if (drMaintStatusCompleted.Length > 0)
                iMaintStatusCompleted = Convert.ToInt32(drMaintStatusCompleted[0]["MaintStatusId"]);


            //Get Vehicle Maint Type Info

            int iMaintTypeId = 0;
            int iOccurrenceType = 0;

            VehicleMaintType vmt = new VehicleMaintType();
            vmt = new VehicleMaintTypeService().GetVehicleMaintTypeByDescription("Maintenance Record Book", sConnStr);
            if (vmt != null)
            {
                iMaintTypeId = vmt.MaintTypeId;
                iOccurrenceType = vmt.OccurrenceType;
            }
            else
            {
                LogMessage(GetMethodName(), "Could not retrieve Vehicle Maint Type [Maintenance Record Book]. Process Aborted.");
                return 0;
            }



            //Retrieve Data
            DataTable dtSrc = GetMaintRecsFromTarget2(iMaintTypeId, sConnStr);

            //Process Data

            foreach (DataRow dr in dtSrc.Rows)
            {
                //Save Maint Rec
                VehicleMaintenance VehicleMaintRec = new VehicleMaintenance();

                VehicleMaintRec.URI = Convert.ToInt32(dr["URI"]);

                try
                {
                    VehicleMaintRec.AssetId = Convert.ToInt32(dr["AssetId"]);
                    VehicleMaintRec.MaintTypeId_cbo = Convert.ToInt32(dr["MaintTypeId_cbo"]);

                    //Get Maint Cat Id
                    DataRow[] drMaintTypes = Globals.dtVehicleMaintType.Select("MaintTypeId = " + VehicleMaintRec.MaintTypeId_cbo);
                    if (drMaintTypes.Length > 0)
                    {
                        VehicleMaintRec.MaintCatId_cbo = Convert.ToInt32(drMaintTypes[0]["MaintCatId_cbo"]);
                    }
                    else
                    {
                        LogMessage(GetMethodName(), "Aborting Processing - Maint Cat Id could not be retried for Maint Type [" + VehicleMaintRec.MaintTypeId_cbo.ToString() + "]");
                        return iRecCount;
                    }

                    //---

                    VehicleMaintRec.VMTLURI = 0;

                    VehicleMaintRec.ActualOdometer = Convert.ToInt32(dr["ActualOdometer"]);
                    VehicleMaintRec.CalculatedOdometer = Convert.ToInt32(dr["CalculatedOdometer"]);
                    VehicleMaintRec.ActualEngineHrs = Convert.ToInt32(dr["ActualEngineHrs"]);
                    VehicleMaintRec.CalculatedEngineHrs = Convert.ToInt32(dr["CalculatedEngineHrs"]);

                    VehicleMaintRec.MaintDescription = dr["MaintDescription"].ToString();
                    VehicleMaintRec.AdditionalInfo = dr["AdditionalInfo"].ToString();

                    VehicleMaintRec.StartDate = (DateTime)dr["StartDate"];
                    VehicleMaintRec.EndDate = (DateTime)dr["EndDate"];


                    //Process field [AdditionalInfo]
                    String sAdditionalInfo = VehicleMaintRec.AdditionalInfo.ToUpper();

                    if (sAdditionalInfo.Trim().Length > 0)
                    {
                        DateTime dtResult;

                        if (DateTime.TryParse(sAdditionalInfo, out dtResult))
                        {
                            VehicleMaintRec.EndDate = dtResult;
                            LogMessage(GetMethodName(), "Maint Record [" + VehicleMaintRec.URI.ToString() + "] - DateTime Converted successfully.");
                        }
                    }

                    //Process field [AdditionalInfo]
                    String sString1 = "Exp Date:";
                    if (sAdditionalInfo.Contains(sString1.ToUpper()))
                    {
                        int StartPos = sString1.Length;
                        string dtString = sAdditionalInfo.Substring(StartPos);

                        DateTime dtResult;

                        if (DateTime.TryParse(dtString, out dtResult))
                        {
                            VehicleMaintRec.EndDate = dtResult;
                            LogMessage(GetMethodName(), "Maint Record [" + VehicleMaintRec.URI.ToString() + "] - DateTime Converted successfully.");
                        }
                        else
                        {
                            LogMessage(GetMethodName(), "Maint Record [" + VehicleMaintRec.URI.ToString() + "] - DateTime Conversion Issue.");
                        }
                    }

                    //Process field [AdditionalInfo]
                    String sString2 = "Exp Date -";
                    if (sAdditionalInfo.Contains(sString2.ToUpper()))
                    {
                        int StartPos = sString2.Length;
                        string dtString = VehicleMaintRec.AdditionalInfo.Substring(StartPos);

                        DateTime dtResult;

                        if (DateTime.TryParse(dtString, out dtResult))
                        {
                            VehicleMaintRec.EndDate = dtResult;
                            LogMessage(GetMethodName(), "Maint Record [" + VehicleMaintRec.URI.ToString() + "] - DateTime Converted successfully.");
                        }
                        else
                        {
                            LogMessage(GetMethodName(), "Maint Record [" + VehicleMaintRec.URI.ToString() + "] - DateTime Conversion Issue.");
                        }
                    }

                    //Process field [AdditionalInfo]
                    String sString3 = "Expiry Date -";
                    if (sAdditionalInfo.Contains(sString3.ToUpper()))
                    {
                        int StartPos = sString3.Length;
                        string dtString = VehicleMaintRec.AdditionalInfo.Substring(StartPos);

                        DateTime dtResult;

                        if (DateTime.TryParse(dtString, out dtResult))
                        {
                            VehicleMaintRec.EndDate = dtResult;
                            LogMessage(GetMethodName(), "Maint Record [" + VehicleMaintRec.URI.ToString() + "] - DateTime Converted successfully.");
                        }
                        else
                        {
                            LogMessage(GetMethodName(), "Maint Record [" + VehicleMaintRec.URI.ToString() + "] - DateTime Conversion Issue.");
                        }
                    }

                    String sString4 = "Exp date :";
                    if (sAdditionalInfo.Contains(sString4.ToUpper()))
                    {
                        int StartPos = sString4.Length;
                        string dtString = VehicleMaintRec.AdditionalInfo.Substring(StartPos);

                        DateTime dtResult;

                        if (DateTime.TryParse(dtString, out dtResult))
                        {
                            VehicleMaintRec.EndDate = dtResult;
                            LogMessage(GetMethodName(), "Maint Record [" + VehicleMaintRec.URI.ToString() + "] - DateTime Converted successfully.");
                        }
                        else
                        {
                            LogMessage(GetMethodName(), "Maint Record [" + VehicleMaintRec.URI.ToString() + "] - DateTime Conversion Issue.");
                        }
                    }

                    if (VehicleMaintRec.EndDate < DateTime.Now)
                    {
                        if (VehicleMaintRec.MaintCatId_cbo == 4)
                        {
                            VehicleMaintRec.MaintStatusId_cbo = iMaintStatusExpired;
                        }
                        else
                        {
                            VehicleMaintRec.MaintStatusId_cbo = iMaintStatusCompleted;
                        }
                    }
                    else
                    {
                        if (VehicleMaintRec.MaintCatId_cbo == 4)
                        {
                            VehicleMaintRec.MaintStatusId_cbo = iMaintStatusValid;
                        }
                        else
                        {
                            VehicleMaintRec.MaintStatusId_cbo = iMaintStatusInProgress;
                        }
                    }

                    VehicleMaintRec.TotalCost = 0;
                    VehicleMaintRec.VATAmount = 0;

                    VehicleMaintRec.CoverTypeId_cbo = Convert.ToInt32(dr["CoverTypeId_cbo"]);
                    VehicleMaintRec.EstimatedValue = (Decimal)dr["EstimatedValue"];

                    //Saves Record
                    VehicleMaintTypeLink vmtl = new VehicleMaintTypeLink();
                    DataSet dsOut = new DataSet();

                    VehicleMaintenanceService vms = new VehicleMaintenanceService();

                    if (vms.UpdateVehicleMaintenance(VehicleMaintRec, dtVMI, vmtl, iOccurrenceType, out dsOut, null, sConnStr) == true)
                    {
                        iRecCount++;

                        LogMessage(GetMethodName(), "Maint Record [" + VehicleMaintRec.URI.ToString() + "] - Updated successfully");
                    }
                    else
                    {
                        LogMessage(GetMethodName(), "Maint Record [" + VehicleMaintRec.URI.ToString() + "] - Error encountered during save");
                    }
                }
                catch (Exception ex)
                {
                    LogMessage(GetMethodName(), "Maint Record [" + VehicleMaintRec.URI.ToString() + "] - Error: " + ex.Message);
                }
            }

            LogMessage(GetMethodName(), "Data Processing 2 - Completed");

            return iRecCount;
        }

        public int UDF_Process_Data3(String sConnStr)
        {
            LogMessage(GetMethodName(), "Data Processing 3 - Started");

            int iRecCount = 0;

            string sWorkDetails2 = string.Empty;

            //Prep - Define Vehicle Maint Items
            DataTable dtVMI = new DataTable();
            dtVMI.TableName = "GFI_AMM_VehicleMaintItems";
            dtVMI.Columns.Add("oprType", typeof(DataRowState));
            dtVMI.Columns.Add("MaintURI", typeof(int));
            dtVMI.Columns.Add("ItemCode", typeof(string));
            dtVMI.Columns.Add("Description", typeof(string));
            dtVMI.Columns.Add("Quantity", typeof(int));
            dtVMI.Columns.Add("UnitCost", typeof(Decimal));
            dtVMI.Columns.Add("CreatedDate", typeof(DateTime));
            dtVMI.Columns.Add("CreatedBy", typeof(string));

            //Gets Pre-Processing data

            //Get Completion Status [In Progress]
            int iMaintStatusInProgress = 0;
            DataRow[] drMaintStatusInProgress = Globals.dtVehicleMaintStatus.Select("Description = 'In Progress'");
            if (drMaintStatusInProgress.Length > 0)
                iMaintStatusInProgress = Convert.ToInt32(drMaintStatusInProgress[0]["MaintStatusId"]);

            //Get Completion Status [Valid]
            int iMaintStatusValid = 0;
            DataRow[] drMaintStatusValid = Globals.dtVehicleMaintStatus.Select("Description = 'Valid'");
            if (drMaintStatusValid.Length > 0)
                iMaintStatusValid = Convert.ToInt32(drMaintStatusValid[0]["MaintStatusId"]);

            //Get Completion Status [Expired]
            int iMaintStatusExpired = 0;
            DataRow[] drMaintStatusExpired = Globals.dtVehicleMaintStatus.Select("Description = 'Expired'");
            if (drMaintStatusExpired.Length > 0)
                iMaintStatusExpired = Convert.ToInt32(drMaintStatusExpired[0]["MaintStatusId"]);

            //Get Completion Status [Completed]
            int iMaintStatusCompleted = 0;
            DataRow[] drMaintStatusCompleted = Globals.dtVehicleMaintStatus.Select("Description = 'Completed'");
            if (drMaintStatusCompleted.Length > 0)
                iMaintStatusCompleted = Convert.ToInt32(drMaintStatusCompleted[0]["MaintStatusId"]);


            //Retrieve Data
            DataTable dtSrc = GetMaintRecsFromTarget3(sConnStr);

            //Process Data

            foreach (DataRow dr in dtSrc.Rows)
            {
                //Save Maint Rec
                VehicleMaintenance VehicleMaintRec = new VehicleMaintenance();

                VehicleMaintRec.URI = Convert.ToInt32(dr["URI"]);

                try
                {
                    int iOccurrenceType = 0;

                    VehicleMaintRec.AssetId = Convert.ToInt32(dr["AssetId"]);
                    VehicleMaintRec.MaintTypeId_cbo = Convert.ToInt32(dr["MaintTypeId_cbo"]);

                    //Get Maint Cat Id
                    DataRow[] drMaintTypes = Globals.dtVehicleMaintType.Select("MaintTypeId = " + VehicleMaintRec.MaintTypeId_cbo);
                    if (drMaintTypes.Length > 0)
                    {
                        VehicleMaintRec.MaintCatId_cbo = Convert.ToInt32(drMaintTypes[0]["MaintCatId_cbo"]);
                        iOccurrenceType = Convert.ToInt32(drMaintTypes[0]["OccurrenceType"]);
                    }
                    else
                    {
                        LogMessage(GetMethodName(), "Aborting Processing - Maint Cat Id could not be retried for Maint Type [" + VehicleMaintRec.MaintTypeId_cbo.ToString() + "]");
                        return iRecCount;
                    }

                    //---

                    VehicleMaintRec.VMTLURI = 0;

                    VehicleMaintRec.ActualOdometer = String.IsNullOrEmpty(dr["ActualOdometer"].ToString()) ? 0 : Convert.ToInt32(dr["ActualOdometer"]);
                    VehicleMaintRec.CalculatedOdometer = String.IsNullOrEmpty(dr["CalculatedOdometer"].ToString()) ? 0 : Convert.ToInt32(dr["CalculatedOdometer"]);
                    VehicleMaintRec.ActualEngineHrs = String.IsNullOrEmpty(dr["CalculatedOdometer"].ToString()) ? 0 : Convert.ToInt32(dr["CalculatedOdometer"]);
                    VehicleMaintRec.CalculatedEngineHrs = String.IsNullOrEmpty(dr["CalculatedOdometer"].ToString()) ? 0 : Convert.ToInt32(dr["CalculatedOdometer"]);

                    VehicleMaintRec.MaintDescription = dr["MaintDescription"].ToString();
                    VehicleMaintRec.AdditionalInfo = dr["AdditionalInfo"].ToString();

                    VehicleMaintRec.StartDate = (DateTime)dr["StartDate"];
                    VehicleMaintRec.EndDate = (DateTime)dr["EndDate"];

                    if (VehicleMaintRec.EndDate < DateTime.Now)
                    {
                        if (VehicleMaintRec.MaintCatId_cbo == 4)
                        {
                            VehicleMaintRec.MaintStatusId_cbo = iMaintStatusExpired;
                        }
                        else
                        {
                            VehicleMaintRec.MaintStatusId_cbo = iMaintStatusCompleted;
                        }
                    }
                    else
                    {
                        if (VehicleMaintRec.MaintCatId_cbo == 4)
                        {
                            VehicleMaintRec.MaintStatusId_cbo = iMaintStatusValid;
                        }
                        else
                        {
                            VehicleMaintRec.MaintStatusId_cbo = iMaintStatusInProgress;
                        }
                    }

                    VehicleMaintRec.TotalCost = 0;
                    VehicleMaintRec.VATAmount = 0;

                    VehicleMaintRec.CoverTypeId_cbo = String.IsNullOrEmpty(dr["CoverTypeId_cbo"].ToString()) ? 1 : Convert.ToInt32(dr["CoverTypeId_cbo"]);
                    VehicleMaintRec.EstimatedValue = String.IsNullOrEmpty(dr["EstimatedValue"].ToString()) ? VehicleMaintRec.EstimatedValue : (Decimal)dr["EstimatedValue"];

                    //Saves Record
                    VehicleMaintTypeLink vmtl = new VehicleMaintTypeLink();
                    DataSet dsOut = new DataSet();

                    VehicleMaintenanceService vms = new VehicleMaintenanceService();

                    if (vms.UpdateVehicleMaintenance(VehicleMaintRec, dtVMI, vmtl, iOccurrenceType, out dsOut, null, sConnStr) == true)
                    {
                        iRecCount++;

                        LogMessage(GetMethodName(), "Maint Record [" + VehicleMaintRec.URI.ToString() + "] - Asset Id [" + VehicleMaintRec.AssetId + "] - Updated successfully");
                    }
                    else
                    {
                        LogMessage(GetMethodName(), "Maint Record [" + VehicleMaintRec.URI.ToString() + "] - Asset Id [" + VehicleMaintRec.AssetId + "] - Error encountered during save");
                    }
                }
                catch (Exception ex)
                {
                    LogMessage(GetMethodName(), "Maint Record [" + VehicleMaintRec.URI.ToString() + "] - Asset Id [" + VehicleMaintRec.AssetId + "] - Error: " + ex.Message);
                }
            }

            LogMessage(GetMethodName(), "Data Processing 3 - Completed");

            return iRecCount;
        }

        public int UDF_FinalizeAssetsOnTarget(DataTable dtSrc, String sConnStr)
        {
            int iCount = 0;

            foreach (DataRow dr in dtSrc.Rows)
            {
                String sAssetId = dr["AssetID"].ToString();

                try
                {
                    LogMessage(GetMethodName(), "Processing Asset [" + sAssetId + "]");

                    AssetExtProVehicle AEPV = new AssetExtProVehicleService().GetAssetExtProVehiclesById(sAssetId, sConnStr);

                    AEPV.Description = "Asset Migrated";

                    //Save Asset Record
                    AssetExtProVehicleService AEPVSvc = new AssetExtProVehicleService();
                    bool bUpdateStatus = AEPVSvc.UpdateAsset(AEPV, sConnStr);

                    if (bUpdateStatus == true)
                    {
                        iCount++;
                        LogMessage(GetMethodName(), "Asset [" + sAssetId + "] Processed Successfully");
                    }
                    else
                    {
                        LogMessage(GetMethodName(), "Asset [" + sAssetId + "] - Error: Record Not Updated");
                    }
                }
                catch (Exception ex)
                {
                    LogMessage(GetMethodName(), "Error Encountered - Asset [" + sAssetId + "] - Error: " + ex.Message);
                }
            }

            return iCount;
        }
    }
}
