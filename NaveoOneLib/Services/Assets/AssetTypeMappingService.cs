using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Assets;

namespace NaveoOneLib.Services.Assets
{
    public class AssetTypeMappingService
    {
        Errors er = new Errors();

        public DataTable GetNextMappingId(String sConnStr)
        {
            String sqlString = @"SELECT MAX(MappingId)
                                from GFI_FLT_AssetTypeMapping";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
       
        public DataTable GetAssetTypeMapping(String sConnStr)
        {
            String sqlString = @"SELECT distinct  
                                IID,
                                Description
                                from GFI_FLT_AssetTypeMapping order by IID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        public static AssetTypeMapping GetAssetTypeMapping(DataRow dr)
        {
            AssetTypeMapping retAssetTypeMapping = new AssetTypeMapping();
            retAssetTypeMapping.IID = Convert.ToInt32(dr["IID"]);
            retAssetTypeMapping.Description = dr["Description"].ToString();
            retAssetTypeMapping.AssetTypeId = Convert.ToInt32(dr["AssetTypeId"]);
            retAssetTypeMapping.CreatedBy = dr["CreatedBy"].ToString();
            retAssetTypeMapping.CreatedDate = Convert.ToDateTime(dr["CreatedDate"]);
            retAssetTypeMapping.UpdatedBy = dr["UpdatedBy"].ToString();
            retAssetTypeMapping.UpdatedDate = Convert.ToDateTime(dr["UpdatedDate"]);
            retAssetTypeMapping.MappingId = Convert.ToInt32(dr["MappingId"]);

            return retAssetTypeMapping;
        }

        public AssetTypeMapping GetAssetTypeMappingById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT IID, 
                                MappingId,
                                Description, 
                                AssetTypeId, 
                                CreatedBy, 
                                CreatedDate, 
                                UpdatedBy, 
                                UpdatedDate from GFI_FLT_AssetTypeMapping
                                WHERE IID = ?
                                order by IID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                AssetTypeMapping retExtTripInfoMapping = new AssetTypeMapping();
                retExtTripInfoMapping.IID = Convert.ToInt32(dt.Rows[0]["IID"]);
                retExtTripInfoMapping.MappingId = Convert.ToInt32(dt.Rows[0]["MappingId"]);
                retExtTripInfoMapping.Description = dt.Rows[0]["Description"].ToString();
                retExtTripInfoMapping.AssetTypeId = Convert.ToInt32(dt.Rows[0]["AssetTypeId"]);
                retExtTripInfoMapping.CreatedBy = dt.Rows[0]["CreatedBy"].ToString();
                retExtTripInfoMapping.CreatedDate = (DateTime)dt.Rows[0]["CreatedDate"];
                retExtTripInfoMapping.UpdatedBy = dt.Rows[0]["UpdatedBy"].ToString();
                retExtTripInfoMapping.UpdatedDate = (DateTime)dt.Rows[0]["UpdatedDate"];
                return retExtTripInfoMapping;
            }
        }
        public AssetTypeMapping GetAssetTypeMappingByDescription(String Description, String sConnStr)
        {
            String sqlString = @"SELECT * from GFI_FLT_AssetTypeMapping WHERE Description = ?";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, Description, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetAssetTypeMapping(dt.Rows[0]);
        }

        public Boolean SaveAssetTypeMapping(AssetTypeMapping uAssetTypeMapping, Boolean bInsert, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_AssetTypeMapping";
            dt.Columns.Add("IID", uAssetTypeMapping.IID.GetType());
            dt.Columns.Add("Description", uAssetTypeMapping.Description.GetType());
            dt.Columns.Add("AssetTypeId", uAssetTypeMapping.AssetTypeId.GetType());
            dt.Columns.Add("CreatedBy", uAssetTypeMapping.CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", uAssetTypeMapping.CreatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uAssetTypeMapping.UpdatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uAssetTypeMapping.UpdatedDate.GetType());
            dt.Columns.Add("MappingId", uAssetTypeMapping.MappingId.GetType());

            DataRow dr = dt.NewRow();
            dr["IID"] = uAssetTypeMapping.IID;
            dr["Description"] = uAssetTypeMapping.Description;
            dr["AssetTypeId"] = uAssetTypeMapping.AssetTypeId;
            dr["CreatedBy"] = uAssetTypeMapping.CreatedBy;
            dr["CreatedDate"] = uAssetTypeMapping.CreatedDate;
            dr["UpdatedBy"] = uAssetTypeMapping.UpdatedBy;
            dr["UpdatedDate"] = uAssetTypeMapping.UpdatedDate;
            dr["MappingId"] = uAssetTypeMapping.MappingId;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "IID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uAssetTypeMapping.IID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }

        public bool SaveAssetTypeMapping(List<AssetTypeMapping> ExtTripInfoMapping, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            Connection c = new Connection(sConnStr);
            DataSet dsProcess = new DataSet();
            DataTable dtCtrl = c.CTRLTBL();

            dt.TableName = "GFI_FLT_AssetTypeMapping";
            dt.Columns.Add("IID", ExtTripInfoMapping[0].IID.GetType());
            dt.Columns.Add("MappingId", ExtTripInfoMapping[0].MappingId.GetType());
            dt.Columns.Add("Description", ExtTripInfoMapping[0].Description.GetType());            
            dt.Columns.Add("AssetTypeId", ExtTripInfoMapping[0].AssetTypeId.GetType());
            dt.Columns.Add("CreatedBy", ExtTripInfoMapping[0].CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", ExtTripInfoMapping[0].CreatedDate.GetType());
            dt.Columns.Add("UpdatedBy", ExtTripInfoMapping[0].UpdatedBy.GetType());
            dt.Columns.Add("UpdatedDate", ExtTripInfoMapping[0].UpdatedDate.GetType());

            foreach (AssetTypeMapping uExtTripInfoMapping in ExtTripInfoMapping)
            {
                DataRow dr = dt.NewRow();
                dr["IID"] = uExtTripInfoMapping.IID;
                dr["MappingId"] = uExtTripInfoMapping.MappingId;
                dr["Description"] = uExtTripInfoMapping.Description;                
                dr["AssetTypeId"] = uExtTripInfoMapping.AssetTypeId;
                dr["CreatedBy"] = uExtTripInfoMapping.CreatedBy;
                dr["CreatedDate"] = uExtTripInfoMapping.CreatedDate;
                dr["UpdatedBy"] = uExtTripInfoMapping.UpdatedBy;
                dr["UpdatedDate"] = uExtTripInfoMapping.UpdatedDate;
                dt.Rows.Add(dr);
            }

            dsProcess.Merge(dt);

            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "IID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            dtCtrl.Rows.Add(drCtrl);

            Boolean bInsert = true;
            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0 || dri["oprType"].ToString() == "0")
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            DataSet dsResults = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResults.Tables["CTRLTBL"];

            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }

        public bool UpdateExtTripInfoMapping(List<AssetTypeMapping> ExtTripInfoMapping, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            Connection c = new Connection(sConnStr);
            DataSet dsProcess = new DataSet();
            DataTable dtCtrl = c.CTRLTBL();

            dt.TableName = "GFI_FLT_AssetTypeMapping";
            dt.Columns.Add("IID", ExtTripInfoMapping[0].IID.GetType());
            dt.Columns.Add("MappingId", ExtTripInfoMapping[0].MappingId.GetType());
            dt.Columns.Add("Description", ExtTripInfoMapping[0].Description.GetType());
            dt.Columns.Add("AssetTypeId", ExtTripInfoMapping[0].AssetTypeId.GetType());
            dt.Columns.Add("CreatedBy", ExtTripInfoMapping[0].CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", ExtTripInfoMapping[0].CreatedDate.GetType());
            dt.Columns.Add("UpdatedBy", ExtTripInfoMapping[0].UpdatedBy.GetType());
            dt.Columns.Add("UpdatedDate", ExtTripInfoMapping[0].UpdatedDate.GetType());

            foreach (AssetTypeMapping uExtTripInfoMapping in ExtTripInfoMapping)
            {
                DataRow dr = dt.NewRow();
                dr["IID"] = uExtTripInfoMapping.IID;
                dr["MappingId"] = uExtTripInfoMapping.MappingId;
                dr["Description"] = uExtTripInfoMapping.Description;
                dr["AssetTypeId"] = uExtTripInfoMapping.AssetTypeId;
                dr["CreatedBy"] = uExtTripInfoMapping.CreatedBy;
                dr["CreatedDate"] = uExtTripInfoMapping.CreatedDate;
                dr["UpdatedBy"] = uExtTripInfoMapping.UpdatedBy;
                dr["UpdatedDate"] = uExtTripInfoMapping.UpdatedDate;
                dt.Rows.Add(dr);
            }
            dsProcess.Merge(dt);

            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "IID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            dtCtrl.Rows.Add(drCtrl);

            Boolean bInsert = false;
            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0 || dri["oprType"].ToString() == "0")
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            DataSet dsResults = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResults.Tables["CTRLTBL"];

            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }

        public Boolean DeleteAssetTypeMapping(AssetTypeMapping uAssetTypeMapping, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_FLT_AssetTypeMapping where IID = " + uAssetTypeMapping.IID.ToString();
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            //DataTable dtAudit = new AuditService().LogTran(3, "AssetTypeMapping", uAssetTypeMapping.IID.ToString(), Globals.uLogin.UID, uAssetTypeMapping.IID);
            //drCtrl = dtCtrl.NewRow();
            //drCtrl["SeqNo"] = 100;
            //drCtrl["TblName"] = dtAudit.TableName;
            //drCtrl["CmdType"] = "TABLE";
            //drCtrl["KeyFieldName"] = "AuditID";
            //drCtrl["KeyIsIdentity"] = "TRUE";
            //drCtrl["NextIdAction"] = "SET";
            //drCtrl["NextIdFieldName"] = "CallerFunction";
            //drCtrl["ParentTblName"] = "GFI_FLT_AssetTypeMapping";
            //dtCtrl.Rows.Add(drCtrl);
            //dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }

        public bool Delete(List<AssetTypeMapping> listExtTripInfoMapping, String sConnStr)
        {
            int deleteNo = 0;

            Connection _connection = new Connection(sConnStr);

            foreach (AssetTypeMapping ext in listExtTripInfoMapping)
            {
                if (_connection.GenDelete("GFI_FLT_AssetTypeMapping", "IID = '" + ext.IID + "'", sConnStr))
                    deleteNo++;
            }

            if (deleteNo == listExtTripInfoMapping.Count)
                return true;
            else
                return false;
        }

        public DataTable GetAssetTypeMappingByMappingId(String MID, String sConnStr)
        {
            String sqlString = @"SELECT map.IID, 
                                map.MappingId,
                                type.TypeName,
                                map.Description,                               
                                map.AssetTypeId, 
                                map.CreatedBy, 
                                map.CreatedDate, 
                                map.UpdatedBy, 
                                map.UpdatedDate 
                                from GFI_FLT_AssetTypeMapping map, GFI_FLT_AssetDataType type
                                WHERE map.AssetTypeId = type.IID
                                and MappingId = " + MID;
            DataTable dt = new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
            return dt;
        }
        public List<AssetTypeMapping> GetAssetTypeMappingByMappingId(int MID, String sConnStr)
        {
            List<AssetTypeMapping> lATM = new List<AssetTypeMapping>();
            DataTable dt = GetAssetTypeMappingByMappingId(MID.ToString(), sConnStr);
            foreach (DataRow dr in dt.Rows)
                lATM.Add(GetAssetTypeMapping(dr));

            return lATM;
        }
    }
}
