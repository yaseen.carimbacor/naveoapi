using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Services.Audits;

namespace NaveoOneLib.Services.Assets
{
    class DeviceAuxilliaryMapService
    {
        Errors er = new Errors();

        public DataTable GetDeviceAuxilliaryMapByAssetId(String iID, String sConnStr)
        {
            String sqlString = @"SELECT AssetNumber, Thresholdhigh, Thresholdlow, Capacity, Description, a.AssetID
                                from GFI_FLT_DeviceAuxilliaryMap aux
	                                inner join dbo.GFI_FLT_AssetDeviceMap d on aux.MapID = d.MapID
	                                inner join dbo.GFI_FLT_Asset a  on d.AssetID = a.AssetID
	                                inner join dbo.GFI_SYS_UOM uom on aux.Uom = uom.UID
                                WHERE a.AssetID= ?";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            return dt;
        }

        public DataTable GetDeviceAuxilliaryMap(String sConnStr)
        {
            String sqlString = @"SELECT Uid, 
DeviceId, 
AuxilliaryTypeId, 
Field1, 
Field2, 
Field3, 
Field4, 
Field5, 
Field6, 
CreatedDate, 
CreatedBy, 
UpdatedDate, 
UpdatedBy from GFI_FLT_DeviceAuxilliaryMap order by Uid";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        DeviceAuxilliaryMap myDAM(DataRow dr)
        {
            DeviceAuxilliaryMap retDeviceAuxilliaryMap = new DeviceAuxilliaryMap();
            retDeviceAuxilliaryMap.Uid = Convert.ToInt32(dr["Uid"]);
            retDeviceAuxilliaryMap.MapId = Convert.ToInt32(dr["MapId"]);
            retDeviceAuxilliaryMap.AuxilliaryType = Convert.ToInt32(dr["AuxilliaryType"]);
            retDeviceAuxilliaryMap.Auxilliary = dr["Auxilliary"].ToString();
            retDeviceAuxilliaryMap.Field1 = dr["Field1"].ToString();
            retDeviceAuxilliaryMap.Field2 = dr["Field2"].ToString();
            retDeviceAuxilliaryMap.Field3 = Convert.ToInt32(dr["Field3"]);
            retDeviceAuxilliaryMap.Field4 = Convert.ToInt32(dr["Field4"]);
            retDeviceAuxilliaryMap.Field5 = dr["Field5"].ToString();
            retDeviceAuxilliaryMap.Field6 = (DateTime)dr["Field6"];
            retDeviceAuxilliaryMap.CreatedDate = (DateTime)dr["CreatedDate"];
            retDeviceAuxilliaryMap.UpdatedDate = (DateTime)dr["UpdatedDate"];

            if (!String.IsNullOrEmpty(dr["CreatedBy"].ToString()))
                retDeviceAuxilliaryMap.CreatedBy = Convert.ToInt32(dr["CreatedBy"].ToString());
            if (!String.IsNullOrEmpty(dr["UpdatedBy"].ToString()))
                retDeviceAuxilliaryMap.UpdatedBy = Convert.ToInt32(dr["UpdatedBy"].ToString());

            return retDeviceAuxilliaryMap;
        }
        public DeviceAuxilliaryMap GetDeviceAuxilliaryMapById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT Uid, 
DeviceId, 
AuxilliaryTypeId, 
Field1, 
Field2, 
Field3, 
Field4, 
Field5, 
Field6, 
CreatedDate, 
CreatedBy, 
UpdatedDate, 
UpdatedBy from GFI_FLT_DeviceAuxilliaryMap
WHERE Uid = ?
order by Uid";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return myDAM(dt.Rows[0]);
        }


        public Boolean SaveDeviceAuxilliaryMap(DeviceAuxilliaryMap uDeviceAuxilliaryMap, Boolean bInsert, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_DeviceAuxilliaryMap";
            dt.Columns.Add("Uid", uDeviceAuxilliaryMap.Uid.GetType());
            dt.Columns.Add("MapId", uDeviceAuxilliaryMap.MapId.GetType());
            dt.Columns.Add("AuxilliaryType", uDeviceAuxilliaryMap.AuxilliaryType.GetType());
            dt.Columns.Add("Auxilliary", uDeviceAuxilliaryMap.Auxilliary.GetType());

            dt.Columns.Add("Field1", uDeviceAuxilliaryMap.Field1.GetType());
            dt.Columns.Add("Field2", uDeviceAuxilliaryMap.Field2.GetType());
            dt.Columns.Add("Field3", uDeviceAuxilliaryMap.Field3.GetType());
            dt.Columns.Add("Field4", uDeviceAuxilliaryMap.Field4.GetType());
            dt.Columns.Add("Field5", uDeviceAuxilliaryMap.Field5.GetType());
            dt.Columns.Add("Field6", uDeviceAuxilliaryMap.Field6.GetType());
            dt.Columns.Add("CreatedDate", uDeviceAuxilliaryMap.CreatedDate.GetType());
            dt.Columns.Add("CreatedBy", typeof(int));
            dt.Columns.Add("UpdatedDate", uDeviceAuxilliaryMap.UpdatedDate.GetType());
            dt.Columns.Add("UpdatedBy", typeof(int));

            DataRow dr = dt.NewRow();
            dr["Uid"] = uDeviceAuxilliaryMap.Uid;
            dr["MapId"] = uDeviceAuxilliaryMap.MapId;
            dr["AuxilliaryTypeId"] = uDeviceAuxilliaryMap.AuxilliaryType;
            dr["Auxilliary"] = uDeviceAuxilliaryMap.Auxilliary;
            dr["Field1"] = uDeviceAuxilliaryMap.Field1;
            dr["Field2"] = uDeviceAuxilliaryMap.Field2;
            dr["Field3"] = uDeviceAuxilliaryMap.Field3;
            dr["Field4"] = uDeviceAuxilliaryMap.Field4;
            dr["Field5"] = uDeviceAuxilliaryMap.Field5;
            dr["Field6"] = uDeviceAuxilliaryMap.Field6;
            dr["CreatedDate"] = uDeviceAuxilliaryMap.CreatedDate;
            dr["CreatedBy"] = uDeviceAuxilliaryMap.CreatedBy;
            dr["UpdatedDate"] = uDeviceAuxilliaryMap.UpdatedDate;
            dr["UpdatedBy"] = uDeviceAuxilliaryMap.UpdatedBy;
            dt.Rows.Add(dr);


            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "Uid";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uDeviceAuxilliaryMap.Uid;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public Boolean DeleteDeviceAuxilliaryMap(DeviceAuxilliaryMap uDeviceAuxilliaryMap, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_FLT_DeviceAuxilliaryMap where Uid = " + uDeviceAuxilliaryMap.Uid.ToString();
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            DataTable dtAudit = new AuditService().LogTran(3, "DeviceAuxilliaryMap", uDeviceAuxilliaryMap.Uid.ToString(), Globals.uLogin.UID, uDeviceAuxilliaryMap.Uid);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "CallerFunction";
            drCtrl["ParentTblName"] = "GFI_FLT_DeviceAuxilliaryMap";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
    }
}
