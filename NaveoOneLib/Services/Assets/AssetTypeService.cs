using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Assets;

namespace NaveoOneLib.Services.Assets
{
    class AssetTypeService
    {
        Errors er = new Errors();
        public DataTable GetAssetType(String sConnStr)
        {
            String sqlString = @"SELECT IID, 
TypeName, 
Field1Name, 
Field1Label, 
Field1Type, 
CreatedBy, 
CreatedDate, 
UpdatedBy, 
UpdatedDate, 
sDataSource from GFI_FLT_AssetType order by IID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        AssetType GetAssetType(DataRow dr)
        {
            AssetType retAssetType = new AssetType();
            retAssetType.IID = Convert.ToInt32(dr["IID"]);
            retAssetType.TypeName = dr["TypeName"].ToString();
            retAssetType.Field1Name = dr["Field1Name"].ToString();
            retAssetType.Field1Label = dr["Field1Label"].ToString();
            retAssetType.Field1Type = dr["Field1Type"].ToString();
            retAssetType.CreatedBy = dr["CreatedBy"].ToString();
            retAssetType.CreatedDate = (DateTime)dr["CreatedDate"];
            retAssetType.UpdatedBy = dr["UpdatedBy"].ToString();
            retAssetType.UpdatedDate = (DateTime)dr["UpdatedDate"];
            retAssetType.sDataSource = dr["sDataSource"].ToString();
            return retAssetType;
        }
        public AssetType GetAssetTypeById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT IID, 
TypeName, 
Field1Name, 
Field1Label, 
Field1Type, 
CreatedBy, 
CreatedDate, 
UpdatedBy, 
UpdatedDate, 
sDataSource from GFI_FLT_AssetType
WHERE IID = ?
order by IID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetAssetType(dt.Rows[0]);
        }
        public Boolean SaveAssetType(AssetType uAssetType, Boolean bInsert, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_AssetType";
            dt.Columns.Add("IID", uAssetType.IID.GetType());
            dt.Columns.Add("TypeName", uAssetType.TypeName.GetType());
            dt.Columns.Add("Field1Name", uAssetType.Field1Name.GetType());
            dt.Columns.Add("Field1Label", uAssetType.Field1Label.GetType());
            dt.Columns.Add("Field1Type", uAssetType.Field1Type.GetType());
            dt.Columns.Add("CreatedBy", uAssetType.CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", uAssetType.CreatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uAssetType.UpdatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uAssetType.UpdatedDate.GetType());
            dt.Columns.Add("sDataSource", uAssetType.sDataSource.GetType());

            DataRow dr = dt.NewRow();
            dr["IID"] = uAssetType.IID;
            dr["TypeName"] = uAssetType.TypeName;
            dr["Field1Name"] = uAssetType.Field1Name;
            dr["Field1Label"] = uAssetType.Field1Label;
            dr["Field1Type"] = uAssetType.Field1Type;
            dr["CreatedBy"] = uAssetType.CreatedBy;
            dr["CreatedDate"] = uAssetType.CreatedDate;
            dr["UpdatedBy"] = uAssetType.UpdatedBy;
            dr["UpdatedDate"] = uAssetType.UpdatedDate;
            dr["sDataSource"] = uAssetType.sDataSource;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "IID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uAssetType.IID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public Boolean DeleteAssetType(AssetType uAssetType, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_FLT_AssetType where IID = " + uAssetType.IID.ToString();
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            //DataTable dtAudit = new AuditService().LogTran(3, "AssetType", uAssetType.IID.ToString(), Globals.uLogin.UID, uAssetType.IID);
            //drCtrl = dtCtrl.NewRow();
            //drCtrl["SeqNo"] = 100;
            //drCtrl["TblName"] = dtAudit.TableName;
            //drCtrl["CmdType"] = "TABLE";
            //drCtrl["KeyFieldName"] = "AuditID";
            //drCtrl["KeyIsIdentity"] = "TRUE";
            //drCtrl["NextIdAction"] = "SET";
            //drCtrl["NextIdFieldName"] = "CallerFunction";
            //drCtrl["ParentTblName"] = "GFI_FLT_AssetType";
            //dtCtrl.Rows.Add(drCtrl);
            //dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
    }
}
