using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Assets;

namespace NaveoOneLib.Services.Assets
{
    public class VehicleMaintCatService
    {
        Errors er = new Errors();
        public String sGetVehicleMaintCat()
        {
            String sqlString = @"SELECT MaintCatId, 
                                    Description, 
                                    CreatedDate, 
                                    CreatedBy, 
                                    UpdatedDate, 
                                    UpdatedBy from GFI_AMM_VehicleMaintCat order by MaintCatId";
            return sqlString;
        }
        public DataTable GetVehicleMaintCat(String sConnStr)
        {
            return new Connection(sConnStr).GetDataDT(sGetVehicleMaintCat(), sConnStr);
        }

        public VehicleMaintCat GetVehicleMaintCatById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT MaintCatId, 
                                    Description, 
                                    CreatedDate, 
                                    CreatedBy, 
                                    UpdatedDate, 
                                    UpdatedBy from GFI_AMM_VehicleMaintCat
                                    WHERE MaintCatId = ?
                                    order by MaintCatId";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                VehicleMaintCat retVehicleMaintCat = new VehicleMaintCat();
                retVehicleMaintCat.MaintCatId = Convert.ToInt32(dt.Rows[0]["MaintCatId"]);
                retVehicleMaintCat.Description = dt.Rows[0]["Description"].ToString();
                retVehicleMaintCat.CreatedDate = (DateTime)dt.Rows[0]["CreatedDate"];
                retVehicleMaintCat.CreatedBy = dt.Rows[0]["CreatedBy"].ToString();
                retVehicleMaintCat.UpdatedDate = (DateTime)dt.Rows[0]["UpdatedDate"];
                retVehicleMaintCat.UpdatedBy = dt.Rows[0]["UpdatedBy"].ToString();
                return retVehicleMaintCat;
            }
        }

        public String sGetVehicleMaintCatByDescription()
        {
            String sqlString = @"SELECT MaintCatId, 
                                    Description, 
                                    CreatedDate, 
                                    CreatedBy, 
                                    UpdatedDate, 
                                    UpdatedBy from GFI_AMM_VehicleMaintCat
                                    WHERE Description = ?
                                    order by Description";

            return sqlString;
        }

        public VehicleMaintCat GetVehicleMaintCatByDescription(String sDescription, String sConnStr)
        {
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sGetVehicleMaintCatByDescription(), sDescription, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                VehicleMaintCat retVehicleMaintCat = new VehicleMaintCat();
                retVehicleMaintCat.MaintCatId = Convert.ToInt32(dt.Rows[0]["MaintCatId"]);
                retVehicleMaintCat.Description = dt.Rows[0]["Description"].ToString();
                retVehicleMaintCat.CreatedDate = (DateTime)dt.Rows[0]["CreatedDate"];
                retVehicleMaintCat.CreatedBy = dt.Rows[0]["CreatedBy"].ToString();
                retVehicleMaintCat.UpdatedDate = (DateTime)dt.Rows[0]["UpdatedDate"];
                retVehicleMaintCat.UpdatedBy = dt.Rows[0]["UpdatedBy"].ToString();
                return retVehicleMaintCat;
            }
        }

        public bool SaveVehicleMaintCat(VehicleMaintCat uVehicleMaintCat, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_VehicleMaintCat";
            dt.Columns.Add("MaintCatId", typeof(int));
            dt.Columns.Add("Description", typeof(String));
            dt.Columns.Add("CreatedDate", typeof(DateTime));
            dt.Columns.Add("CreatedBy", typeof(String));
            dt.Columns.Add("UpdatedDate", typeof(DateTime));
            dt.Columns.Add("UpdatedBy", typeof(String));
            
            DataRow dr = dt.NewRow();
            dr["MaintCatId"] = uVehicleMaintCat.MaintCatId;
            dr["Description"] = uVehicleMaintCat.Description;
            dr["CreatedDate"] = uVehicleMaintCat.CreatedDate;
            dr["CreatedBy"] = uVehicleMaintCat.CreatedBy;
            dr["UpdatedDate"] = uVehicleMaintCat.UpdatedDate;
            dr["UpdatedBy"] = uVehicleMaintCat.UpdatedBy;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); 
            if (_connection.GenInsert(dt, transaction, sConnStr))
                return true;
            else
                return false;
        }

        public bool UpdateVehicleMaintCat(VehicleMaintCat uVehicleMaintCat, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_VehicleMaintCat";
            dt.Columns.Add("MaintCatId", uVehicleMaintCat.MaintCatId.GetType());
            dt.Columns.Add("Description", uVehicleMaintCat.Description.GetType());
            dt.Columns.Add("CreatedDate", uVehicleMaintCat.CreatedDate.GetType());
            dt.Columns.Add("CreatedBy", uVehicleMaintCat.CreatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uVehicleMaintCat.UpdatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uVehicleMaintCat.UpdatedBy.GetType());
            
            DataRow dr = dt.NewRow();
            dr["MaintCatId"] = uVehicleMaintCat.MaintCatId;
            dr["Description"] = uVehicleMaintCat.Description;
            dr["CreatedDate"] = uVehicleMaintCat.CreatedDate;
            dr["CreatedBy"] = uVehicleMaintCat.CreatedBy;
            dr["UpdatedDate"] = uVehicleMaintCat.UpdatedDate;
            dr["UpdatedBy"] = uVehicleMaintCat.UpdatedBy;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "MaintCatId = '" + uVehicleMaintCat.MaintCatId + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }

        public bool DeleteVehicleMaintCat(VehicleMaintCat uVehicleMaintCat, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_AMM_VehicleMaintCat", "MaintCatId = '" + uVehicleMaintCat.MaintCatId + "'", sConnStr))
                return true;
            else
                return false;
        }

    }
}
