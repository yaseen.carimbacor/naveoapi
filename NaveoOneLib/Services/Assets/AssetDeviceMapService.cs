using System;
using System.Collections.Generic;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Services.GMatrix;

namespace NaveoOneLib.Services.Assets
{
    public class AssetDeviceMapService
    {

        Errors er = new Errors();
        public DataTable GetAssetDeviceMap(String sConnStr)
        {
            String sqlString = @"SELECT MapID, 
                                        AssetID, 
                                        DeviceID, 
                                        SerialNumber, 
                                        Status_b, 
                                        CreatedBy, 
                                        CreatedDate, 
                                        UpdatedBy, 
                                        UpdatedDate, 
                                        StartDate, 
                                        EndDate
                                        --Aux1Name_cbo, 
                                        --Aux2Name_cbo, 
                                        --Aux3Name_cbo, 
                                        --Aux4Name_cbo, 
                                        --Aux5Name_cbo, 
                                        --Aux6Name_cbo
                                    from GFI_FLT_AssetDeviceMap 
                                        order by AssetID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        AssetDeviceMap GetAssetDeviceMap(DataRow dr)
        {
            AssetDeviceMap retAssetDeviceMap = new AssetDeviceMap();
            retAssetDeviceMap.MapID = Convert.ToInt32(dr["MapID"]);
            retAssetDeviceMap.AssetID = Convert.ToInt32(dr["AssetID"]);
            retAssetDeviceMap.DeviceID = dr["DeviceID"].ToString();
            retAssetDeviceMap.SerialNumber = dr["SerialNumber"].ToString();
            retAssetDeviceMap.Status_b = dr["Status_b"].ToString();
            if (!String.IsNullOrEmpty(dr["CreatedBy"].ToString()))
                retAssetDeviceMap.CreatedBy = Convert.ToInt32(dr["CreatedBy"].ToString());
            retAssetDeviceMap.CreatedDate = (DateTime)dr["CreatedDate"];
            if (!String.IsNullOrEmpty(dr["UpdatedBy"].ToString()))
                retAssetDeviceMap.UpdatedBy = Convert.ToInt32(dr["UpdatedBy"].ToString());
            retAssetDeviceMap.UpdatedDate = (DateTime)dr["UpdatedDate"];
            retAssetDeviceMap.StartDate = (DateTime)dr["StartDate"];
            retAssetDeviceMap.EndDate = (DateTime)dr["EndDate"];
            retAssetDeviceMap.MaxNoLogDays = Convert.ToInt32(dr["MaxNoLogDays"]);
            //retAssetDeviceMap.Aux1Name_cbo = dr["Aux1Name_cbo"].ToString() == String.Empty ? 0 : Convert.ToInt16(dr["Aux1Name_cbo"].ToString());
            //retAssetDeviceMap.Aux2Name_cbo = dr["Aux2Name_cbo"].ToString() == String.Empty ? 0 : Convert.ToInt16(dr["Aux2Name_cbo"].ToString());
            //retAssetDeviceMap.Aux3Name_cbo = dr["Aux3Name_cbo"].ToString() == String.Empty ? 0 : Convert.ToInt16(dr["Aux3Name_cbo"].ToString());
            //retAssetDeviceMap.Aux4Name_cbo = dr["Aux4Name_cbo"].ToString() == String.Empty ? 0 : Convert.ToInt16(dr["Aux4Name_cbo"].ToString());
            //retAssetDeviceMap.Aux5Name_cbo = dr["Aux5Name_cbo"].ToString() == String.Empty ? 0 : Convert.ToInt16(dr["Aux5Name_cbo"].ToString());
            //retAssetDeviceMap.Aux6Name_cbo = dr["Aux6Name_cbo"].ToString() == String.Empty ? 0 : Convert.ToInt16(dr["Aux2Name_cbo"].ToString());
       
            return retAssetDeviceMap;
        }
        public AssetDeviceMap GetAssetDeviceMapById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT MapID, 
                                        AssetID, 
                                        DeviceID, 
                                        SerialNumber, 
                                        Status_b,
                                        CreatedBy, 
                                        CreatedDate, 
                                        UpdatedBy, 
                                        UpdatedDate, 
                                        StartDate, 
                                        EndDate, 
                                        MaxNoLogDays
                                        --Aux1Name_cbo, 
                                        --Aux2Name_cbo, 
                                        --Aux3Name_cbo, 
                                        --Aux4Name_cbo, 
                                        --Aux5Name_cbo, 
                                        --Aux6Name_cbo
                                    from GFI_FLT_AssetDeviceMap 
                                        WHERE MapID = ?
                                        order by MapID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetAssetDeviceMap(dt.Rows[0]);
        }
        public AssetDeviceMap GetAssetDeviceMapByAsset(String AssetId, String sConnStr)
        {
            String sqlString = @"SELECT MapID, 
                                        AssetID, 
                                        DeviceID, 
                                        SerialNumber, 
                                        Status_b, 
                                        CreatedBy, 
                                        CreatedDate, 
                                        UpdatedBy, 
                                        UpdatedDate, StartDate, EndDate, MaxNoLogDays
                                        --Aux1Name_cbo, 
                                        --Aux2Name_cbo, 
                                        --Aux3Name_cbo, 
                                        --Aux4Name_cbo, 
                                        --Aux5Name_cbo, 
                                        --Aux6Name_cbo
                                    from GFI_FLT_AssetDeviceMap 
                                        WHERE AssetId = ? And Status_b = 'AC' and StartDate < GetDate() and EndDate > Getdate() 
                                        order by AssetId ";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, AssetId, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetAssetDeviceMap(dt.Rows[0]);
        }
        public AssetDeviceMap GetAssetDeviceMapByDevice(String DeviceId, String sConnStr)
        {
            String sqlString = @"SELECT MapID, 
                                        AssetID, 
                                        DeviceID, 
                                        SerialNumber, 
                                        Status_b, 
                                        CreatedBy, 
                                        CreatedDate, 
                                        UpdatedBy, 
                                        UpdatedDate, StartDate, EndDate, MaxNoLogDays
                                        --Aux1Name_cbo, 
                                        --Aux2Name_cbo, 
                                        --Aux3Name_cbo, 
                                        --Aux4Name_cbo, 
                                        --Aux5Name_cbo, 
                                        --Aux6Name_cbo
                                    from GFI_FLT_AssetDeviceMap 
                                        WHERE DeviceId = ? and Status_b = 'AC'
                                        --order by AssetId ";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, DeviceId, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetAssetDeviceMap(dt.Rows[0]);
        }
        public String GetActiveDevices(String sConnStr)
        {
            String strResult = " ";
            String sql = "select DeviceID from GFI_FLT_AssetDeviceMap where getdate() between StartDate and EndDate";
            DataTable dt = new Connection(sConnStr).GetDataDT(sql, sConnStr);
            if (dt.Rows.Count > 0)
                foreach (DataRow dr in dt.Rows)
                    strResult += "'" + dr["DeviceID"].ToString() + "',";

            strResult = strResult.Substring(0, strResult.Length - 1);
            return strResult;
        }
        public String sGetAssetDeviceMap(List<int> iParentIDs, String sConnStr)
        {
            String sqlAsset = @"SELECT AssetID
                                from GFI_FLT_Asset a, GFI_SYS_GroupMatrixAsset m
                                where a.AssetID = m.iID and m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs,sConnStr) + ")";

            String sql = @"select * from GFI_FLT_AssetDeviceMap 
                            WHERE AssetId in (" + sqlAsset + @") And Status_b = 'AC' and StartDate < GetDate() and EndDate > Getdate() 
                            order by AssetId ";
            return sql;
        }

        public Boolean SaveAssetDeviceMap(AssetDeviceMap uAssetDeviceMap, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_AssetDeviceMap";
            dt.Columns.Add("AssetID", uAssetDeviceMap.AssetID.GetType());
            dt.Columns.Add("DeviceID", uAssetDeviceMap.DeviceID.GetType());
            dt.Columns.Add("SerialNumber", uAssetDeviceMap.SerialNumber.GetType());
            dt.Columns.Add("Status_b", uAssetDeviceMap.Status_b.GetType());
            dt.Columns.Add("CreatedBy", uAssetDeviceMap.CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", uAssetDeviceMap.CreatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uAssetDeviceMap.UpdatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uAssetDeviceMap.UpdatedDate.GetType());
            dt.Columns.Add("StartDate", uAssetDeviceMap.StartDate.GetType());
            dt.Columns.Add("EndDate", uAssetDeviceMap.EndDate.GetType());
            dt.Columns.Add("MaxNoLogDays", uAssetDeviceMap.MaxNoLogDays.GetType());
            //dt.Columns.Add("Aux1Name_cbo", uAssetDeviceMap.Aux1Name_cbo.GetType());
            //dt.Columns.Add("Aux2Name_cbo", uAssetDeviceMap.Aux2Name_cbo.GetType());
            //dt.Columns.Add("Aux3Name_cbo", uAssetDeviceMap.Aux3Name_cbo.GetType());
            //dt.Columns.Add("Aux4Name_cbo", uAssetDeviceMap.Aux4Name_cbo.GetType());
            //dt.Columns.Add("Aux5Name_cbo", uAssetDeviceMap.Aux5Name_cbo.GetType());
            //dt.Columns.Add("Aux6Name_cbo", uAssetDeviceMap.Aux6Name_cbo.GetType());
            DataRow dr = dt.NewRow();
            dr["AssetID"] = uAssetDeviceMap.AssetID;
            dr["DeviceID"] = uAssetDeviceMap.DeviceID;
            dr["SerialNumber"] = uAssetDeviceMap.SerialNumber;
            dr["Status_b"] = uAssetDeviceMap.Status_b;
            dr["CreatedBy"] = uAssetDeviceMap.CreatedBy;
            dr["CreatedDate"] = uAssetDeviceMap.CreatedDate;
            dr["UpdatedBy"] = uAssetDeviceMap.UpdatedBy;
            dr["UpdatedDate"] = uAssetDeviceMap.UpdatedDate;
            dr["StartDate"] = uAssetDeviceMap.StartDate;
            dr["EndDate"] = uAssetDeviceMap.EndDate;
            dr["MaxNoLogDays"] = uAssetDeviceMap.MaxNoLogDays;
            //dr["Aux1Name_cbo"] = uAssetDeviceMap.Aux1Name_cbo;
            //dr["Aux2Name_cbo"] = uAssetDeviceMap.Aux2Name_cbo;
            //dr["Aux3Name_cbo"] = uAssetDeviceMap.Aux3Name_cbo;
            //dr["Aux4Name_cbo"] = uAssetDeviceMap.Aux4Name_cbo;
            //dr["Aux5Name_cbo"] = uAssetDeviceMap.Aux5Name_cbo;
            //dr["Aux6Name_cbo"] = uAssetDeviceMap.Aux6Name_cbo;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenInsert(dt, transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool UpdateAssetDeviceMap(AssetDeviceMap uAssetDeviceMap, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_AssetDeviceMap";
            dt.Columns.Add("MapID", uAssetDeviceMap.MapID.GetType());
            dt.Columns.Add("AssetID", uAssetDeviceMap.AssetID.GetType());
            dt.Columns.Add("DeviceID", uAssetDeviceMap.DeviceID.GetType());
            dt.Columns.Add("SerialNumber", uAssetDeviceMap.SerialNumber.GetType());
            dt.Columns.Add("Status_b", uAssetDeviceMap.Status_b.GetType());
            dt.Columns.Add("CreatedBy", uAssetDeviceMap.CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", uAssetDeviceMap.CreatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uAssetDeviceMap.UpdatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uAssetDeviceMap.UpdatedDate.GetType());
            dt.Columns.Add("StartDate", uAssetDeviceMap.StartDate.GetType());
            dt.Columns.Add("EndDate", uAssetDeviceMap.EndDate.GetType());
            dt.Columns.Add("MaxNoLogDays", uAssetDeviceMap.MaxNoLogDays.GetType());
            //dt.Columns.Add("Aux1Name_cbo", uAssetDeviceMap.Aux1Name_cbo.GetType());
            //dt.Columns.Add("Aux2Name_cbo", uAssetDeviceMap.Aux2Name_cbo.GetType());
            //dt.Columns.Add("Aux3Name_cbo", uAssetDeviceMap.Aux3Name_cbo.GetType());
            //dt.Columns.Add("Aux4Name_cbo", uAssetDeviceMap.Aux4Name_cbo.GetType());
            //dt.Columns.Add("Aux5Name_cbo", uAssetDeviceMap.Aux5Name_cbo.GetType());
            //dt.Columns.Add("Aux6Name_cbo", uAssetDeviceMap.Aux6Name_cbo.GetType());
            DataRow dr = dt.NewRow();
            dr["MapID"] = uAssetDeviceMap.MapID;
            dr["AssetID"] = uAssetDeviceMap.AssetID;
            dr["DeviceID"] = uAssetDeviceMap.DeviceID;
            dr["SerialNumber"] = uAssetDeviceMap.SerialNumber;
            dr["Status_b"] = uAssetDeviceMap.Status_b;
            dr["CreatedBy"] = uAssetDeviceMap.CreatedBy;
            dr["CreatedDate"] = uAssetDeviceMap.CreatedDate;
            dr["UpdatedBy"] = uAssetDeviceMap.UpdatedBy;
            dr["UpdatedDate"] = uAssetDeviceMap.UpdatedDate;
            dr["StartDate"] = uAssetDeviceMap.StartDate;
            dr["EndDate"] = uAssetDeviceMap.EndDate;
            dr["MaxNoLogDays"] = uAssetDeviceMap.MaxNoLogDays;
            //dr["Aux1Name_cbo"] = uAssetDeviceMap.Aux1Name_cbo;
            //dr["Aux2Name_cbo"] = uAssetDeviceMap.Aux2Name_cbo;
            //dr["Aux3Name_cbo"] = uAssetDeviceMap.Aux3Name_cbo;
            //dr["Aux4Name_cbo"] = uAssetDeviceMap.Aux4Name_cbo;
            //dr["Aux5Name_cbo"] = uAssetDeviceMap.Aux5Name_cbo;
            //dr["Aux6Name_cbo"] = uAssetDeviceMap.Aux6Name_cbo;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "MapID = '" + uAssetDeviceMap.MapID + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool DeleteAssetDeviceMap(AssetDeviceMap uAssetDeviceMap, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_FLT_AssetDeviceMap", "MapID = '" + uAssetDeviceMap.MapID + "'", sConnStr))
                return true;
            else
                return false;
        }
    }
}


