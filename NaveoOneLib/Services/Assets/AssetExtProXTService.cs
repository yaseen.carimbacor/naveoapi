using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Assets;

namespace NaveoOneLib.Services.Assets
{
    class AssetExtProXTService
    {

        Errors er = new Errors();
        public DataTable GetAssetExtProXT(String sConnStr)
        {
            String sqlString = @"SELECT URI, 
                                    AssetId, 
                                    FieldId, 
                                    XTValue, 
                                    CreatedDate, 
                                    CreatedBy, 
                                    UpdatedDate, 
                                    UpdatedBy from GFI_AMM_AssetExtProXT order by FieldId";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        public AssetExtProXT GetAssetExtProXTById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT URI, 
                                    AssetId, 
                                    FieldId, 
                                    XTValue, 
                                    CreatedDate, 
                                    CreatedBy, 
                                    UpdatedDate, 
                                    UpdatedBy from GFI_AMM_AssetExtProXT
                                    WHERE FieldId = ?
                                    order by FieldId";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                AssetExtProXT retAssetExtProXT = new AssetExtProXT();

                retAssetExtProXT.URI = Convert.ToInt32(dt.Rows[0]["URI"]);
                retAssetExtProXT.AssetId = Convert.ToInt32(dt.Rows[0]["AssetId"]);
                retAssetExtProXT.FieldId = Convert.ToInt32(dt.Rows[0]["FieldId"]);
                retAssetExtProXT.XTValue = dt.Rows[0]["XTValue"].ToString();
                retAssetExtProXT.CreatedDate = (DateTime)dt.Rows[0]["CreatedDate"];
                retAssetExtProXT.CreatedBy = dt.Rows[0]["CreatedBy"].ToString();
                retAssetExtProXT.UpdatedDate = (DateTime)dt.Rows[0]["UpdatedDate"];
                retAssetExtProXT.UpdatedBy = dt.Rows[0]["UpdatedBy"].ToString();
                return retAssetExtProXT;

            }
        }

        public String sGetAssetExtProXTForAssetId(int AssetID)
        {
            String sql = @"SELECT URI, 
                                    xt.AssetId, 
                                    xt.FieldId, 
                                     f.FieldName,
                                    xt.XTValue, 
                                    xt.CreatedDate, 
                                    xt.CreatedBy, 
                                    xt.UpdatedDate, 
                                    xt.UpdatedBy 
                                    FROM GFI_AMM_AssetExtProXT xt
                                    INNER JOIN GFI_AMM_AssetExtProFields f
                                    ON xt.FieldId = f.FieldId
                                    WHERE AssetId = " + AssetID.ToString() + @" 
                                    ORDER BY FieldId";
            return sql;
        }
        public DataTable GetAssetExtProXTForAssetId(String iAssetId, String sConnStr)
        {
            return new Connection(sConnStr).GetDataDT(sGetAssetExtProXTForAssetId(Convert.ToInt32(iAssetId)), null);
        }

        public bool SaveAssetExtProXT(AssetExtProXT uAssetExtProXT, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_AssetExtProXT";
            dt.Columns.Add("URI", uAssetExtProXT.URI.GetType());
            dt.Columns.Add("AssetId", uAssetExtProXT.AssetId.GetType());
            dt.Columns.Add("FieldId", uAssetExtProXT.FieldId.GetType());
            dt.Columns.Add("Value", uAssetExtProXT.XTValue.GetType());
            dt.Columns.Add("CreatedDate", uAssetExtProXT.CreatedDate.GetType());
            dt.Columns.Add("CreatedBy", uAssetExtProXT.CreatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uAssetExtProXT.UpdatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uAssetExtProXT.UpdatedBy.GetType());

            DataRow dr = dt.NewRow();
            dr["URI"] = uAssetExtProXT.URI;
            dr["AssetId"] = uAssetExtProXT.AssetId;
            dr["FieldId"] = uAssetExtProXT.FieldId;
            dr["Value"] = uAssetExtProXT.XTValue;
            dr["CreatedDate"] = uAssetExtProXT.CreatedDate;
            dr["CreatedBy"] = uAssetExtProXT.CreatedBy;
            dr["UpdatedDate"] = uAssetExtProXT.UpdatedDate;
            dr["UpdatedBy"] = uAssetExtProXT.UpdatedBy;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenInsert(dt, transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool UpdateAssetExtProXT(AssetExtProXT uAssetExtProXT, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_AssetExtProXT";
            dt.Columns.Add("URI", uAssetExtProXT.URI.GetType());
            dt.Columns.Add("AssetId", uAssetExtProXT.AssetId.GetType());
            dt.Columns.Add("FieldId", uAssetExtProXT.FieldId.GetType());
            dt.Columns.Add("Value", uAssetExtProXT.XTValue.GetType());
            dt.Columns.Add("CreatedDate", uAssetExtProXT.CreatedDate.GetType());
            dt.Columns.Add("CreatedBy", uAssetExtProXT.CreatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uAssetExtProXT.UpdatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uAssetExtProXT.UpdatedBy.GetType());

            DataRow dr = dt.NewRow();
            dr["URI"] = uAssetExtProXT.URI;
            dr["AssetId"] = uAssetExtProXT.AssetId;
            dr["FieldId"] = uAssetExtProXT.FieldId;
            dr["Value"] = uAssetExtProXT.XTValue;
            dr["CreatedDate"] = uAssetExtProXT.CreatedDate;
            dr["CreatedBy"] = uAssetExtProXT.CreatedBy;
            dr["UpdatedDate"] = uAssetExtProXT.UpdatedDate;
            dr["UpdatedBy"] = uAssetExtProXT.UpdatedBy;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "FieldId = '" + uAssetExtProXT.FieldId + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool DeleteAssetExtProXT(AssetExtProXT uAssetExtProXT, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_AMM_AssetExtProXT", "FieldId = '" + uAssetExtProXT.FieldId + "'", sConnStr))
                return true;
            else
                return false;
        }

    }
}
