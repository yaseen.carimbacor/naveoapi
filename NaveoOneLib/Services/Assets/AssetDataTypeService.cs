using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Assets;

namespace NaveoOneLib.Services.Assets
{
    public class AssetDataTypeService
    {
        Errors er = new Errors();
        public DataTable GetAssetDataType(String sConnStr)
        {
            String sqlString = @"SELECT IID, 
TypeName, 
Field1Name, 
Field1Label, 
Field1Type, 
CreatedBy, 
CreatedDate, 
UpdatedBy, 
UpdatedDate, 
sDataSource from GFI_FLT_AssetDataType order by IID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        AssetDataType GetAssetDataType(DataRow dr)
        {
            AssetDataType retAssetDataType = new AssetDataType();
            retAssetDataType.IID = Convert.ToInt32(dr["IID"]);
            retAssetDataType.TypeName = dr["TypeName"].ToString();
            retAssetDataType.Field1Name = dr["Field1Name"].ToString();
            retAssetDataType.Field1Label = dr["Field1Label"].ToString();
            retAssetDataType.Field1Type = dr["Field1Type"].ToString();
            retAssetDataType.CreatedBy = dr["CreatedBy"].ToString();
            retAssetDataType.CreatedDate = (DateTime)dr["CreatedDate"];
            retAssetDataType.UpdatedBy = dr["UpdatedBy"].ToString();
            retAssetDataType.UpdatedDate = (DateTime)dr["UpdatedDate"];
            retAssetDataType.sDataSource = dr["sDataSource"].ToString();
            return retAssetDataType;
        }
        public AssetDataType GetAssetDataTypeById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT IID, 
TypeName, 
Field1Name, 
Field1Label, 
Field1Type, 
CreatedBy, 
CreatedDate, 
UpdatedBy, 
UpdatedDate, 
sDataSource from GFI_FLT_AssetDataType
WHERE IID = ?
order by IID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetAssetDataType(dt.Rows[0]);
        }
        public AssetDataType GetAssetDataTypeByField1Name(String sField1Name, String sConnStr)
        {
            String sqlString = @"SELECT IID, 
TypeName, 
Field1Name, 
Field1Label, 
Field1Type, 
CreatedBy, 
CreatedDate, 
UpdatedBy, 
UpdatedDate, 
sDataSource from GFI_FLT_AssetDataType
WHERE Field1Name = ?
order by IID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, sField1Name, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetAssetDataType(dt.Rows[0]);
        }

        public Boolean SaveAssetDataType(AssetDataType uAssetDataType, Boolean bInsert, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_AssetDataType";
            dt.Columns.Add("IID", uAssetDataType.IID.GetType());
            dt.Columns.Add("TypeName", uAssetDataType.TypeName.GetType());
            dt.Columns.Add("Field1Name", uAssetDataType.Field1Name.GetType());
            dt.Columns.Add("Field1Label", uAssetDataType.Field1Label.GetType());
            dt.Columns.Add("Field1Type", uAssetDataType.Field1Type.GetType());
            dt.Columns.Add("CreatedBy", uAssetDataType.CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", uAssetDataType.CreatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uAssetDataType.UpdatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uAssetDataType.UpdatedDate.GetType());
            dt.Columns.Add("sDataSource", uAssetDataType.sDataSource.GetType());

            DataRow dr = dt.NewRow();
            dr["IID"] = uAssetDataType.IID;
            dr["TypeName"] = uAssetDataType.TypeName;
            dr["Field1Name"] = uAssetDataType.Field1Name;
            dr["Field1Label"] = uAssetDataType.Field1Label;
            dr["Field1Type"] = uAssetDataType.Field1Type;
            dr["CreatedBy"] = uAssetDataType.CreatedBy;
            dr["CreatedDate"] = uAssetDataType.CreatedDate;
            dr["UpdatedBy"] = uAssetDataType.UpdatedBy;
            dr["UpdatedDate"] = uAssetDataType.UpdatedDate;
            dr["sDataSource"] = uAssetDataType.sDataSource;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "IID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uAssetDataType.IID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public Boolean DeleteAssetDataType(AssetDataType uAssetDataType, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_FLT_AssetDataType where IID = " + uAssetDataType.IID.ToString();
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            //DataTable dtAudit = new AuditService().LogTran(3, "AssetDataType", uAssetDataType.IID.ToString(), Globals.uLogin.UID, uAssetDataType.IID);
            //drCtrl = dtCtrl.NewRow();
            //drCtrl["SeqNo"] = 100;
            //drCtrl["TblName"] = dtAudit.TableName;
            //drCtrl["CmdType"] = "TABLE";
            //drCtrl["KeyFieldName"] = "AuditID";
            //drCtrl["KeyIsIdentity"] = "TRUE";
            //drCtrl["NextIdAction"] = "SET";
            //drCtrl["NextIdFieldName"] = "CallerFunction";
            //drCtrl["ParentTblName"] = "GFI_FLT_AssetDataType";
            //dtCtrl.Rows.Add(drCtrl);
            //dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
    }
}
