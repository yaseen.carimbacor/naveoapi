﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;


namespace NaveoOneLib.Services.Assets
{
    public class AssetsMaintTypeService
    {
        Errors er = new Errors();

        private String sGetAssets(String sAssetsList)
        {
            String sqlString = @"SELECT a.AssetId, 
                                        '' as RegistrationNo,
                                        v.Description, 
                                        v.Make, 
                                        v.Model, 
                                        v.VehicleTypeId_cbo,
                                        vt.Description as TypeDesc
                                 FROM GFI_FLT_Asset a 
										INNER JOIN GFI_AMM_AssetExtProVehicles v
											ON a.AssetId = v.AssetId
										LEFT OUTER JOIN GFI_AMM_VehicleTypes vt
											ON v.VehicleTypeId_cbo = vt.VehicleTypeId   
                                 WHERE a.AssetId IN (" + sAssetsList + ") ORDER BY a.AssetId";

            return sqlString;
        }

        private String sGetAssetsMaintTypes(String sAssetsList, String sMaintTypeList, DateTime dtFrom, DateTime dtTo)
        {
            string sDtFrom = dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff"); // dtFrom.ToString("yyyy-MM-dd") + " 00:00:00";
            string sDtTo = dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff"); //dtTo.ToString("yyyy-MM-dd") + " 23:59:59";

            String sqlString = @"
SELECT vm.URI as MaintURI,
    vm.AssetId, 
    '' as RegistrationNo,
    vmt.Description AS MaintType,
    vm.MaintDescription,
    vm.StartDate,
    vm.EndDate,
    vms.Description AS MaintStatus,
    TotalCost - ISNULL(vm.VATAmount, 0) AS CostExVAT,
    vm.VATAmount, 
    th.kms, 
    case when ISNULL(th.kms, 0) <> 0 then ISNULL(CONVERT(DECIMAL(10,2),TotalCost/th.kms), 0) else 0 end as CPKM
    --case when ISNULL(th.kms, 0) <> 0 then ISNULL(CONVERT(DECIMAL(10, 2), TotalCost/th.kms), CONVERT(DECIMAL(10, 2), 0)) else CONVERT(DECIMAL(10,2), 0) end as CPKM
FROM GFI_AMM_VehicleMaintenance vm	
    INNER JOIN GFI_AMM_VehicleMaintTypes vmt
		ON vm.MaintTypeId_cbo = vmt.MaintTypeId
	INNER JOIN GFI_AMM_VehicleMaintStatus vms
		ON vm.MaintStatusId_cbo = vms.MaintStatusId " + @"
    left outer join 
		(select AssetID, sum(TripDistance) kms from GFI_GPS_TripHeader where AssetId IN (" + sAssetsList + @") and (dtStart BETWEEN '" + sDtFrom + "' AND '" + sDtTo + @"') group by AssetID) th on th.AssetID = vm.AssetId
" +@"                                                                                    
WHERE 
    vm.AssetId IN (" + sAssetsList + @") 
AND    vm.MaintTypeId_cbo IN (" + sMaintTypeList + @") 
AND    vm.StartDate BETWEEN '" + sDtFrom + "' AND '" + sDtTo + @"'                 
ORDER BY vm.AssetId";
            return sqlString;
        }

        public DataTable GetAssets(String sAssetsList, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = null;
            String sql = string.Empty;

            sql = sGetAssets(sAssetsList);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtAssets";
            dtSql.Rows.Add(drSql);

            DataSet dsFetch = c.GetDataDS(dtSql, sConnStr);

            return dsFetch.Tables["dtAssets"];
        }

        public DataTable GetAssetsMaintTypes(String sAssetsList, String sMaintTypeList, DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = null;
            String sql = string.Empty;

            sql = sGetAssetsMaintTypes(sAssetsList, sMaintTypeList, dtFrom, dtTo);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtAssetsMaintTypes";
            dtSql.Rows.Add(drSql);

            DataSet dsFetch = c.GetDataDS(dtSql, sConnStr);

            return dsFetch.Tables["dtAssetsMaintTypes"];
        }

        public bool Save(DataTable dtAssets, DataTable dtMaintTypes, DbTransaction transaction, String sConnStr)
        {
            try
            {
                Connection _connection = new Connection(sConnStr);
                DataSet dsProcessData = new DataSet();

                DataTable CTRLTBL = _connection.CTRLTBL();
                CTRLTBL.TableName = "CTRLTBL";

                DataRow drCtrl = null;

                int iSeqNo = 0;

                foreach (DataRow drAsset in dtAssets.Rows)
                {
                    int iAssetId = Convert.ToInt32(drAsset["AssetId"]);

                    foreach (DataRow drMaintType in dtMaintTypes.Rows)
                    {
                        if ((int)drMaintType["IsSelected"] == 1)
                        {
                            int iMaintTypeId = Convert.ToInt32(drMaintType["MaintTypeId"]);

                            drCtrl = CTRLTBL.Rows.Add();
                            drCtrl["SeqNo"] = iSeqNo;
                            drCtrl["TblName"] = "GFI_AMM_VehicleMaintTypeLink_" + iSeqNo.ToString();
                            drCtrl["CmdType"] = "STOREDPROC";
                            drCtrl["Command"] = "EXEC sp_AMM_VehicleMaint_AssignMaintType " + iAssetId + ", " + iMaintTypeId;

                            iSeqNo++;
                        }
                    }
                }

                dsProcessData.Tables.Add(CTRLTBL);

                DataSet dsResultData = _connection.ProcessData(dsProcessData, sConnStr);

                DataTable dtCTRL = dsResultData.Tables["CTRLTBL"];
                if (dtCTRL != null)
                {
                    DataRow[] dRow = dtCTRL.Select("UpdateStatus IS NULL OR UpdateStatus <> 'True'");

                    if (dRow.Length > 0)
                        return false;
                    else
                        return true;
                }
                else
                    return false;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
