

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;

namespace NaveoOneLib.Services
{
    class NaaAddressService
    {

        Errors er = new Errors();
        public DataTable GetNaaAddress(String sConnStr)
        {
            String sqlString = @"SELECT iID,NaaCode, 
            LastName, 
            OtherName,
            concat(LastName,' ', OtherName) as FullName,
            Fax, 
            Street1, 
            Street2, 
            Locality, 
            City, 
            Tel, 
            Email, 
            Mobile,
            CreatedBy, 
            CreatedDate, 
            UpdatedBy, 
            UpdatedDate from GFI_SYS_NaaAddress order by NaaCode";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public NaaAddress GetNaaAddressById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT NaaCode, 
LastName, 
OtherName, 
Street1, 
Street2, 
Locality, 
City, 
Tel, 
Email, 
Mobile, 
CreatedBy, 
CreatedDate, 
UpdatedBy, 
UpdatedDate from GFI_SYS_NaaAddress
WHERE NaaCode = ?
order by NaaCode";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID,sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                NaaAddress retNaaAddress = new NaaAddress();
                retNaaAddress.NaaCode = dt.Rows[0]["NaaCode"].ToString();
                retNaaAddress.LastName = dt.Rows[0]["LastName"].ToString();
                retNaaAddress.OtherName = dt.Rows[0]["OtherName"].ToString();
                retNaaAddress.Street1 = dt.Rows[0]["Street1"].ToString();
                retNaaAddress.Street2 = dt.Rows[0]["Street2"].ToString();
                retNaaAddress.Locality = dt.Rows[0]["Locality"].ToString();
                retNaaAddress.City = dt.Rows[0]["City"].ToString();
                retNaaAddress.Tel = dt.Rows[0]["Tel"].ToString();
                retNaaAddress.Email = dt.Rows[0]["Email"].ToString();
                retNaaAddress.Mobile = dt.Rows[0]["Mobile"].ToString();
                retNaaAddress.CreatedBy = dt.Rows[0]["CreatedBy"].ToString();
                retNaaAddress.CreatedDate = (DateTime)dt.Rows[0]["CreatedDate"];
                retNaaAddress.UpdatedBy = dt.Rows[0]["UpdatedBy"].ToString();
                retNaaAddress.UpdatedDate = (DateTime)dt.Rows[0]["UpdatedDate"];
                return retNaaAddress;
            }
        }
        public bool SaveNaaAddress(NaaAddress uNaaAddress, Boolean bInsert, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_NaaAddress";
            dt.Columns.Add("iID", uNaaAddress.iID.GetType());
            dt.Columns.Add("NaaCode", uNaaAddress.NaaCode.GetType());
            dt.Columns.Add("LastName", uNaaAddress.LastName.GetType());
            dt.Columns.Add("OtherName", uNaaAddress.OtherName.GetType());
            dt.Columns.Add("Street1", uNaaAddress.Street1.GetType());
            dt.Columns.Add("Street2", uNaaAddress.Street2.GetType());
            dt.Columns.Add("Locality", uNaaAddress.Locality.GetType());
            dt.Columns.Add("City", uNaaAddress.City.GetType());
            dt.Columns.Add("Tel", uNaaAddress.Tel.GetType());
            dt.Columns.Add("Email", uNaaAddress.Email.GetType());
            dt.Columns.Add("Mobile", uNaaAddress.Mobile.GetType());
            dt.Columns.Add("Fax", uNaaAddress.Fax.GetType());

            dt.Columns.Add("CreatedBy", uNaaAddress.CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", uNaaAddress.CreatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uNaaAddress.UpdatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uNaaAddress.UpdatedDate.GetType());
            DataRow dr = dt.NewRow();

            dr["iID"] = uNaaAddress.iID;
            dr["NaaCode"] = uNaaAddress.NaaCode;
            dr["LastName"] = uNaaAddress.LastName;
            dr["OtherName"] = uNaaAddress.OtherName;
            dr["Street1"] = uNaaAddress.Street1;
            dr["Street2"] = uNaaAddress.Street2;
            dr["Locality"] = uNaaAddress.Locality;
            dr["City"] = uNaaAddress.City;
            dr["Tel"] = uNaaAddress.Tel;
            dr["Email"] = uNaaAddress.Email;
            dr["Mobile"] = uNaaAddress.Mobile;
            dr["Fax"] = uNaaAddress.Fax;

            dr["CreatedBy"] = uNaaAddress.CreatedBy;
            dr["CreatedDate"] = uNaaAddress.CreatedDate;
            dr["UpdatedBy"] = uNaaAddress.UpdatedBy;
            dr["UpdatedDate"] = uNaaAddress.UpdatedDate;
            if (!dt.Columns.Contains("oprType"))
                dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Rows.Add(dr);


            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "iID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "SET";
            else
                drCtrl["NextIdAction"] = "GET";
            dtCtrl.Rows.Add(drCtrl);

            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);
            dsProcess.Merge(dtCtrl);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public bool UpdateNaaAddress(NaaAddress uNaaAddress, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_NaaAddress";
            dt.Columns.Add("NaaCode", uNaaAddress.NaaCode.GetType());
            dt.Columns.Add("LastName", uNaaAddress.LastName.GetType());
            dt.Columns.Add("OtherName", uNaaAddress.OtherName.GetType());
            dt.Columns.Add("Street1", uNaaAddress.Street1.GetType());
            dt.Columns.Add("Street2", uNaaAddress.Street2.GetType());
            dt.Columns.Add("Locality", uNaaAddress.Locality.GetType());
            dt.Columns.Add("City", uNaaAddress.City.GetType());
            dt.Columns.Add("Tel", uNaaAddress.Tel.GetType());
            dt.Columns.Add("Email", uNaaAddress.Email.GetType());
            dt.Columns.Add("Mobile", uNaaAddress.Mobile.GetType());
            dt.Columns.Add("CreatedBy", uNaaAddress.CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", uNaaAddress.CreatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uNaaAddress.UpdatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uNaaAddress.UpdatedDate.GetType());
            DataRow dr = dt.NewRow();
            dr["NaaCode"] = uNaaAddress.NaaCode;
            dr["LastName"] = uNaaAddress.LastName;
            dr["OtherName"] = uNaaAddress.OtherName;
            dr["Street1"] = uNaaAddress.Street1;
            dr["Street2"] = uNaaAddress.Street2;
            dr["Locality"] = uNaaAddress.Locality;
            dr["City"] = uNaaAddress.City;
            dr["Tel"] = uNaaAddress.Tel;
            dr["Email"] = uNaaAddress.Email;
            dr["Mobile"] = uNaaAddress.Mobile;
            dr["CreatedBy"] = uNaaAddress.CreatedBy;
            dr["CreatedDate"] = uNaaAddress.CreatedDate;
            dr["UpdatedBy"] = uNaaAddress.UpdatedBy;
            dr["UpdatedDate"] = uNaaAddress.UpdatedDate;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "NaaCode = '" + uNaaAddress.NaaCode + "'", transaction,sConnStr))
                return true;
            else
                return false;
        }
        public bool DeleteNaaAddress(NaaAddress uNaaAddress, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_SYS_NaaAddress", "NaaCode = '" + uNaaAddress.NaaCode + "'", sConnStr))
                return true;
            else
                return false;
        }

    }
}


