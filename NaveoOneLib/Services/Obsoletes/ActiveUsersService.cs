﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NaveoOneLib.DBCon;
using NaveoOneLib.Common;
using NaveoOneLib.Models;
//using System.Data.SQLite;
using System.Windows.Forms;
using NaveoOneLib.Models.GMatrix;

namespace NaveoOneLib.Services
{
    class ActiveUsersService
    {
        public bool SaveActiveSession(ActiveUsers aUser, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_ActiveSession";
            if (!dt.Columns.Contains("oprType"))
                dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Columns.Add("UID", aUser.UID.GetType());
            dt.Columns.Add("Username", aUser.Username.GetType());
            dt.Columns.Add("LoginDate", aUser.LoginDate.GetType());
            dt.Columns.Add("MachineName", aUser.MachineName.GetType());
            dt.Columns.Add("UserProfile", aUser.UserProfile.GetType());
            dt.Columns.Add("Company", aUser.Company.GetType());
            dt.Columns.Add("AppVersion", aUser.AppVersion.GetType());
            dt.Columns.Add("UserID", aUser.UserID.GetType());

            DataRow dr = dt.NewRow();
            dr["UID"] = aUser.UID;
            dr["Username"] = aUser.Username;
            dr["LoginDate"] = aUser.LoginDate;
            dr["MachineName"] = aUser.MachineName;
            dr["UserProfile"] = aUser.UserProfile;
            dr["Company"] = aUser.Company;
            dr["AppVersion"] = aUser.AppVersion;
            dr["UserID"] = aUser.UserID;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataSet dsProcess = new DataSet();
            DataTable dtCtrl = c.CTRLTBL();
            dsProcess.Merge(dt);

            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "UID";
            drCtrl["KeyIsIdentity"] = "FALSE";
            drCtrl["NextIdAction"] = "SET";
            dtCtrl.Rows.Add(drCtrl);

            Boolean bInsert = true;

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0 || dri["oprType"].ToString() == "0")
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            DataSet dsResults = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResults.Tables["CTRLTBL"];

            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }

        public bool DeleteActiveSession(string UID, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_SYS_ActiveSession", "UID = '" + UID + "'", sConnStr))
                return true;
            else
                return false;

        }

        /*
        public bool SaveTobeDeletedSession(Models.ActiveUsers aUser)
        {
            bool insert = false;
            string sqlString = @"INSERT INTO GFI_SYS_ActiveSession
                                 VALUES(                                         
                                            @UID, @username, @loginDate, @machineName, @userProfile, @company, @version                                                    
                                        )";

            try
            {
                using (SQLiteConnection conn = DGlobals.GetConnection())
                {
                    using (SQLiteTransaction transaction = conn.BeginTransaction())
                    {
                        try
                        {

                            using (SQLiteCommand sqlCmd = new SQLiteCommand(sqlString, conn))
                            {
                                sqlCmd.Parameters.AddWithValue("@UID", aUser.UID);
                                sqlCmd.Parameters.AddWithValue("@username", aUser.Username);
                                sqlCmd.Parameters.AddWithValue("@loginDate", aUser.LoginDate.ToString("dd-MM-yyyy HH:mm:ss"));
                                sqlCmd.Parameters.AddWithValue("@machineName", aUser.MachineName);
                                sqlCmd.Parameters.AddWithValue("@userProfile", aUser.UserProfile);
                                sqlCmd.Parameters.AddWithValue("@company", aUser.Company);
                                sqlCmd.Parameters.AddWithValue("@version", aUser.AppVersion);

                                sqlCmd.ExecuteNonQuery();
                            }
                            transaction.Commit();

                            insert = true;
                        }
                        catch (SQLiteException ex)
                        {
                            new Errors().ShowMsg(ex.ToString());
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                new Errors().ShowMsg(ex.ToString());
                return false;
            }
            return insert;

        }*/




        public DataTable GetUserSession(int UID, String sConnStr)
        {
            String sqlString = @"SELECT *
                                from GFI_SYS_ActiveSession
                                where UserID = (" + UID + ") order by Username";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        public Models.ActiveUsers SetActiveSession(String sConnStr)
        {
            Models.ActiveUsers aUser = new Models.ActiveUsers();
            aUser.UID = Globals.uLogin.UID;
            aUser.Username = Globals.uLogin.Username;
            aUser.LoginDate = DateTime.UtcNow;
            aUser.MachineName = SystemInformation.ComputerName;
            aUser.UserID = Globals.uLogin.UID;

            NaveoOneLib.Models.AccessTemplate at = new NaveoOneLib.Models.AccessTemplate().GetAccessTemplatesById(Convert.ToString(Globals.uLogin.AccessTemplateId), sConnStr);
           if(at != null)
             aUser.UserProfile = at.TemplateName;
           else
                aUser.UserProfile = "Default";

            foreach (Matrix m in Globals.uLogin.lMatrix)
                aUser.Company = m.GMDesc;

            aUser.AppVersion = Application.ProductVersion;

            return aUser;
        }
    }
}
