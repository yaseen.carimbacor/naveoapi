using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Services.GMatrix;

namespace NaveoOneLib.Services
{
    class AccessTemplateService
    {

        Errors er = new Errors();

        public DataSet GetDetails(int iUID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = null;
            String sql = string.Empty;

            sql = sGetAccessTemplatesById(iUID.ToString());
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtAT";
            dtSql.Rows.Add(drSql);

            sql = new AccessProfileService().sGetAccessProfilesByTemplateId(iUID.ToString());
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtAP";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            foreach (DataRow dr in ds.Tables["dtAP"].Rows)
                dr.AcceptChanges();

            return ds;
        }

        public AccessTemplate ATemplate(DataRow dr)
        {
            try
            {
                AccessTemplate rAccessTemplate = new AccessTemplate();

                rAccessTemplate.UID = (int)dr["UID"];
                rAccessTemplate.TemplateName = dr["TemplateName"].ToString();
                rAccessTemplate.Description = dr["Description"].ToString();

                return rAccessTemplate;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public String sGetAccessTemplates(List<Matrix> lMatrix, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            return sGetAccessTemplates(iParentIDs, sConnStr);
        }
        public String sGetAccessTemplates(List<int> iParentIDs, String sConnStr)
        {
            String sqlString = @"SELECT at.UID, 
                                    at.TemplateName, 
                                    at.Description, 
                                    at.CreatedDate, 
                                    at.CreatedBy, 
                                    at.UpdatedDate, 
                                    at.UpdatedBy 
                                 FROM GFI_SYS_AccessTemplates at
                                    INNER JOIN GFI_SYS_GroupMatrixAccessTemplate m
                                        ON at.UID = m.iID
                               WHERE m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + ") ORDER BY at.UID";
            return sqlString;
        }
        public String sGetAccessTemplatesForUserAssign(List<int> iParentIDs, String sConnStr)
        {
            String sqlString = string.Empty;
            sqlString = @"SELECT at.UID, 
                                    at.TemplateName, 
                                    at.Description, 
                                    at.CreatedDate, 
                                    at.CreatedBy, 
                                    at.UpdatedDate, 
                                    at.UpdatedBy 
                                 FROM GFI_SYS_AccessTemplates at
                               where at.CreatedBy ='INSTALLER'
                               
                               UNION ";
            sqlString += sGetAccessTemplates(iParentIDs, sConnStr);
            return sqlString;
        }


        public DataTable GetAccessTemplates(List<Matrix> lMatrix, String sConnStr)
        {
            return new Connection(sConnStr).GetDataDT(sGetAccessTemplates(lMatrix, sConnStr), sConnStr);
        }

        public String sGetAccessTemplatesById(String iID)
        {
            String sqlString = @"SELECT UID, 
                                    TemplateName, 
                                    Description, 
                                    CreatedDate, 
                                    CreatedBy, 
                                    UpdatedDate, 
                                    UpdatedBy 
                                 FROM GFI_SYS_AccessTemplates
                                 WHERE UID = " + iID + @" ORDER BY UID";

            return sqlString;
        }

        AccessTemplate GetAccessTemplate(DataSet ds)
        {
            //ds has tables dtAccessTemplates and dtMatrix
            AccessTemplate a = new AccessTemplate();
            a.UID = (int)ds.Tables["dtAccessTemplates"].Rows[0]["UID"];
            a.TemplateName = ds.Tables["dtAccessTemplates"].Rows[0]["TemplateName"].ToString();
            a.Description = ds.Tables["dtAccessTemplates"].Rows[0]["Description"].ToString();
            a.CreatedDate = (DateTime)ds.Tables["dtAccessTemplates"].Rows[0]["CreatedDate"];
            a.CreatedBy = ds.Tables["dtAccessTemplates"].Rows[0]["CreatedBy"].ToString();
            a.UpdatedBy = ds.Tables["dtAccessTemplates"].Rows[0]["UpdatedBy"].ToString();
            a.UpdatedDate = (DateTime)ds.Tables["dtAccessTemplates"].Rows[0]["UpdatedDate"];

            List<Matrix> lMatrix = new List<Matrix>();
            MatrixService ms = new MatrixService();
            foreach (DataRow dr in ds.Tables["dtMatrix"].Rows)
                lMatrix.Add(ms.AssignMatrix(dr));
            a.lMatrix = lMatrix;

            return a;
        }
        public AccessTemplate GetAccessTemplatesById(String iID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = sGetAccessTemplatesById(iID);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = iID.ToString();
            drSql["sTableName"] = "dtAccessTemplates";
            dtSql.Rows.Add(drSql);

            sql = new MatrixService().sGetMatrixId(Convert.ToInt32(iID), "GFI_SYS_GroupMatrixAccessTemplate");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables["dtAccessTemplates"].Rows.Count == 0)
                return null;
            else
                return GetAccessTemplate(ds);
        }

        public Boolean SaveAccessTemplates(AccessTemplate uAccessTemplates, Boolean bInsert, DataTable dtAccessProfiles, out DataSet dsResults, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_AccessTemplates";
            if (!dt.Columns.Contains("oprType"))
                dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Columns.Add("UID", uAccessTemplates.UID.GetType());
            dt.Columns.Add("TemplateName", uAccessTemplates.TemplateName.GetType());
            dt.Columns.Add("Description", uAccessTemplates.Description.GetType());
            dt.Columns.Add("CreatedDate", uAccessTemplates.CreatedDate.GetType());
            dt.Columns.Add("CreatedBy", uAccessTemplates.CreatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uAccessTemplates.UpdatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uAccessTemplates.UpdatedBy.GetType());
            if (bInsert)
            {
                uAccessTemplates.CreatedDate = DateTime.Now;
                uAccessTemplates.UpdatedDate = Constants.NullDateTime;
            }
            else
            {
                uAccessTemplates.UpdatedDate = DateTime.Now;
            }

            DataRow dr = dt.NewRow();
            if (bInsert)
                dr["oprType"] = DataRowState.Added;
            else
                dr["oprType"] = DataRowState.Modified;
            dr["UID"] = uAccessTemplates.UID;
            dr["TemplateName"] = uAccessTemplates.TemplateName;
            dr["Description"] = uAccessTemplates.Description;
            dr["CreatedDate"] = DateTime.UtcNow;
            dr["CreatedBy"] = uAccessTemplates.CreatedBy;
            dr["UpdatedDate"] = DateTime.UtcNow;
            dr["UpdatedBy"] = uAccessTemplates.UpdatedBy;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr);
            DataSet dsProcess = new DataSet();
            DataTable CTRLTBL = _connection.CTRLTBL();
            dsProcess.Merge(dt);

            DataRow drCtrl = CTRLTBL.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "UID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uAccessTemplates.UID;
            CTRLTBL.Rows.Add(drCtrl);

            //--- Access Profiles ---//
            if (dtAccessProfiles.Rows.Count > 0)
            {
                dtAccessProfiles.TableName = "GFI_SYS_AccessProfiles";
                if (!dtAccessProfiles.Columns.Contains("oprType"))
                    dtAccessProfiles.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dRow in dtAccessProfiles.Rows)
                {
                    dRow["TemplateId"] = uAccessTemplates.UID;
                    dRow["oprType"] = dRow.RowState;

                    if (bInsert)
                    {
                        dRow["CreatedDate"] = DateTime.UtcNow;
                        dRow["CreatedBy"] = uAccessTemplates.CreatedBy;
                    }
                    else
                    {
                        dRow["UpdatedDate"] = DateTime.UtcNow;
                        dRow["UpdatedBy"] = uAccessTemplates.CreatedBy;
                    }
                }
                if (!bInsert)
                {
                    dtAccessProfiles.Columns.Remove("CreatedDate");
                    dtAccessProfiles.Columns.Remove("CreatedBy");
                }
                dsProcess.Merge(dtAccessProfiles);

                drCtrl = CTRLTBL.NewRow();
                drCtrl["SeqNo"] = 2;
                drCtrl["TblName"] = dtAccessProfiles.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "UID";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdAction"] = "SET";
                drCtrl["NextIdFieldName"] = "TemplateId";
                drCtrl["ParentTblName"] = dt.TableName;
                CTRLTBL.Rows.Add(drCtrl);
            }

            //Matrix
            DataTable dtMatrix = new myConverter().ListToDataTable<Matrix>(uAccessTemplates.lMatrix);
            dtMatrix.TableName = "GFI_SYS_GroupMatrixAccessTemplate";
            dsProcess.Merge(dtMatrix);

            drCtrl = CTRLTBL.NewRow();
            drCtrl["SeqNo"] = 3;
            drCtrl["TblName"] = dtMatrix.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "MID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "iID";
            drCtrl["ParentTblName"] = dt.TableName;
            CTRLTBL.Rows.Add(drCtrl);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0 || dri["oprType"].ToString() == "0")
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            //------------------- Gets Data for record that was updated -------------------//
            DataTable dtSql = _connection.dtSql();
            DataRow drSql = null;
            String sql = String.Empty;

            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in uAccessTemplates.lMatrix)
                iParentIDs.Add(m.GMID);
            sql = sGetAccessTemplates(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtAccessTemplates";
            dtSql.Rows.Add(drSql);


            //--- Process Data ---//
            dsProcess.Merge(CTRLTBL);
            dsProcess.Merge(dtSql);
            dsResults = _connection.ProcessData(dsProcess, sConnStr);

            Boolean bResult = false;
            if (CTRLTBL != null)
            {
                DataRow[] dRow = CTRLTBL.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = true;
                else
                    bResult = false;
            }
            else
                bResult = false;

            return bResult;
        }

        public bool DeleteAccessTemplates(AccessTemplate uAccessTemplates, DataTable dtAccessProfiles, out DataSet dsResults, String sConnStr)
        {
            //--- Prepare Dataset ---//
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_AccessTemplates";
            dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Columns.Add("UID", uAccessTemplates.UID.GetType());

            DataRow dr = dt.NewRow();
            dr["oprType"] = DataRowState.Deleted;
            dr["UID"] = uAccessTemplates.UID;
            dt.Rows.Add(dr);

            //--- Prepare Control Table ---//
            Connection _connection = new Connection(sConnStr);
            DataSet dsProcess = new DataSet();

            DataTable CTRLTBL = _connection.CTRLTBL();
            CTRLTBL.TableName = "CTRLTBL";

            DataRow drCtrl = CTRLTBL.Rows.Add();
            drCtrl["SeqNo"] = 2;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "UID";
            drCtrl["KeyIsIdentity"] = "TRUE";


            //--- Prepare Access Profiles ---//
            dtAccessProfiles.TableName = "GFI_SYS_AccessProfiles";
            if (!dtAccessProfiles.Columns.Contains("oprType"))
                dtAccessProfiles.Columns.Add("oprType", typeof(DataRowState));

            foreach (DataRow dRow in dtAccessProfiles.Rows)
            {
                switch (dRow.RowState)
                {
                    case DataRowState.Unchanged:
                        dRow["oprType"] = DataRowState.Deleted;
                        break;

                    case DataRowState.Added:
                        dRow.RejectChanges();
                        break;

                    case DataRowState.Modified:
                        dRow.RejectChanges();
                        dRow["oprType"] = DataRowState.Deleted;
                        break;

                    case DataRowState.Deleted:
                        dRow.RejectChanges();
                        dRow["oprType"] = DataRowState.Deleted;
                        break;
                }
            }

            drCtrl = CTRLTBL.Rows.Add();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dtAccessProfiles.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "UID";
            drCtrl["KeyIsIdentity"] = "TRUE";

            //------------------- Gets Data for record that was updated -------------------//
            DataTable dtSql = _connection.dtSql();
            DataRow drSql = null;
            String sql = string.Empty;

            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in uAccessTemplates.lMatrix)
                iParentIDs.Add(m.GMID);
            sql = sGetAccessTemplates(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtAccessTemplates";
            dtSql.Rows.Add(drSql);

            sql = new AccessProfileService().sGetAccessProfilesByTemplateId(uAccessTemplates.UID.ToString());
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtAccessProfiles";
            dtSql.Rows.Add(drSql);

            //--- Process Data ---//

            dsProcess.Tables.Add(CTRLTBL);
            dsProcess.Merge(dt);
            dsProcess.Merge(dtAccessProfiles);
            dsProcess.Merge(dtSql);

            dsResults = _connection.ProcessData(dsProcess, sConnStr);

            CTRLTBL = dsResults.Tables["CTRLTBL"];

            if (CTRLTBL != null)
            {
                DataRow[] dRow = CTRLTBL.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }
    }
}