﻿using System.Data;
using NaveoOneLib.Models.Audits;

namespace NaveoOneLib.Services
{
    public class SQLiteService
    {
        public static bool SaveAuditTableService(AuditTable auditTable)
        {
            return false;
        }
        public static DataTable GetSynTableAudit()
        {
            return null;
        }
        public static bool UpdateAuditTableSync(DataTable auditLogin)
        {
            return false;
        }
        public static bool CreateLocalAudits()
        {
            return false;
        }

        #region AuditAccessService
        public static DataTable GetSynAccessAudit()
        {
            return null;
        }
        public static bool UpdateAccessTableSync(DataTable auditAccess)
        {
            return false;
        }
        public static bool PurgeLocalDb()
        {
            return false;
        }
        #endregion

        #region AuditLoginService
        public static bool SaveAuditLoginService(AuditLogin auditLogin)
        {
            return false;
        }
        public static AuditLogin GetLastLoginInfo(string loginType, string username)
        {
            return null;
        }
        public static DataTable GetLastLoginInfo(string username)
        {
            return null;
        }
        public static DataTable GetSynLoginAudit()
        {
            return null;
        }
        public static bool UpdateAuditLoginSync(DataTable auditLogin)
        {
            return false;
        }
        #endregion

    }
}

//All below functions commented because of SQLite
//namespace NaveoOneLib.Services
//{
//    public static class SQLiteService
//    {
//        static Errors er = new Errors();
//        public static void CreateTablesAudits()
//        {
//            try
//            {
//                if (!File.Exists("MiniCore.sqlite"))
//                {
//                    SQLiteConnection.CreateFile("MiniCore.sqlite");
//                    AuditTable au = new AuditTable();
//                    if (au.CreateLocalAudits())
//                    { }

//                }
//            }
//            catch { }
//        }
//        public static SQLiteConnection GetConnection()
//        {
//            try
//            {
//                if (!File.Exists("MiniCore.sqlite"))
//                {
//                    SQLiteConnection.CreateFile("MiniCore.sqlite");
//                    AuditTable au = new AuditTable();
//                    if (au.CreateLocalAudits())
//                    { }

//                }

//                SQLiteConnection connection = new SQLiteConnection("Data Source=MiniCore.sqlite;Version=3;");//("Data Source=" + System.IO.Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location), "MiniCore.sqlite"));
//                connection.Open();
//                return connection;
//            }
//            catch (Exception ex)
//            {
//                MessageBox.Show(ex.Message.ToString());
//                return null;
//            }
//        }
//        public static bool AuditAccess(AuditAccess auditAccess)
//        {
//            bool insert = false;
//            string sqlString = @"INSERT INTO GFI_SYS_AuditAccess
//                                       (AuditUser, Control, AuditDate)
//                                 VALUES(@AuditUser, @Control, @AuditDate)";

//            try
//            {
//                using (SQLiteConnection conn = GetConnection())
//                {
//                    using (SQLiteTransaction transaction = conn.BeginTransaction())
//                    {
//                        try
//                        {
//                            using (SQLiteCommand sqlCmd = new SQLiteCommand(sqlString, conn))
//                            {
//                                auditAccess.AuditDate = DateTime.Now;

//                                if (string.IsNullOrEmpty(auditAccess.AuditUser))
//                                    auditAccess.AuditUser = Globals.uLogin.Username;

//                                //sqlCmd.Parameters.AddWithValue("@auditId", auditLogin.AuditId);
//                                //sqlCmd.Parameters.AddWithValue("@Iid", auditControl.IId);
//                                sqlCmd.Parameters.AddWithValue("@AuditUser", auditAccess.AuditUser);
//                                sqlCmd.Parameters.AddWithValue("@Control", auditAccess.Control);
//                                sqlCmd.Parameters.AddWithValue("@AuditDate", auditAccess.AuditDate.ToString("dd-MM-yyyy HH:mm:ss"));

//                                sqlCmd.ExecuteNonQuery();
//                            }
//                            transaction.Commit();

//                            insert = true;
//                        }
//                        catch (SQLiteException ex)
//                        {
//                            new NaveoOneLib.Common.Errors().ShowMsg(ex.ToString());
//                            return false;
//                        }
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                new NaveoOneLib.Common.Errors().ShowMsg(ex.ToString());
//                return false;
//            }
//            return insert;
//        }

//        public static bool SaveAuditTableService(AuditTable auditTable)
//        {
//            bool insert = false;
//            string sqlString = @"INSERT INTO GFI_SYS_Audit
//            							    (   
//            								AuditUser,                                
//            								AuditType	,
//            								AuditDate,
//            								ReferenceCode	,
//            								ReferenceDesc	,
//            								ReferenceTable	,
//            								CreatedDate	,
//            								CallerFunction	,		
//            								SQLRemarks	,
//            								NewValue	,		
//            								OldValue	
//            								)
//            						   VALUES(                                         
//            								
//            								@AuditUser,                      
//            								@AuditType	,
//            								@AuditDate,
//            								@ReferenceCode	,
//            								@ReferenceDesc	,
//            								@ReferenceTable	,
//            								@CreatedDate	,
//            								@CallerFunction	,		
//            								@SQLRemarks	,
//            								@NewValue	,		
//            								@OldValue	                                            
//            								)";
//            try
//            {
//                using (SQLiteConnection conn = SQLiteService.GetConnection())
//                {
//                    using (SQLiteTransaction transaction = conn.BeginTransaction())
//                    {
//                        try
//                        {
//                            using (SQLiteCommand sqlCmd = new SQLiteCommand(sqlString, conn))
//                            {
//                                auditTable.AuditDate = DateTime.Now;

//                                if (string.IsNullOrEmpty(auditTable.AuditUser))
//                                    auditTable.AuditUser = Globals.uLogin.Username;

//                                //sqlCmd.Parameters.AddWithValue("@auditId", auditLogin.AuditId);
//                                sqlCmd.Parameters.AddWithValue("@auditUser", auditTable.AuditUser);
//                                sqlCmd.Parameters.AddWithValue("@AuditDate", auditTable.AuditDate.ToString("dd-MM-yyyy HH:mm:ss"));
//                                sqlCmd.Parameters.AddWithValue("@CreatedDate", auditTable.AuditDate.ToString("dd-MM-yyyy HH:mm:ss"));
//                                sqlCmd.Parameters.AddWithValue("@auditType", auditTable.AuditType);
//                                sqlCmd.Parameters.AddWithValue("@ReferenceCode", auditTable.AuditRefCode);
//                                sqlCmd.Parameters.AddWithValue("@ReferenceDesc", auditTable.AuditRefDesc);
//                                sqlCmd.Parameters.AddWithValue("@ReferenceTable", auditTable.AuditRefTable);
//                                sqlCmd.Parameters.AddWithValue("@CallerFunction", auditTable.CallerFunction);
//                                sqlCmd.Parameters.AddWithValue("@SQLRemarks", auditTable.SqlRemarks);
//                                sqlCmd.Parameters.AddWithValue("@NewValue", auditTable.NewValue);
//                                sqlCmd.Parameters.AddWithValue("@OldValue", auditTable.OldValue);

//                                sqlCmd.ExecuteNonQuery();
//                            }
//                            transaction.Commit();

//                            insert = true;
//                        }
//                        catch (SQLiteException ex)
//                        {
//                            er.ShowMsg(ex.ToString());
//                            return false;
//                        }
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                er.ShowMsg(ex.ToString());
//                return false;
//            }
//            return insert;
//        }
//        public static DataTable GetSynTableAudit()
//        {
//            DataTable dataTable = new DataTable();

//            string sqlString = @"Select AuditId,  
//            								AuditUser,                                
//            								AuditType	,   
//            								AuditDate, 
//            								ReferenceCode	,
//            								ReferenceDesc	,
//            								ReferenceTable	,
//            								CreatedDate	,
//            								CallerFunction	,		
//            								SQLRemarks	,
//            								NewValue	,		
//            								OldValue ,
//            								PostedFlag,
//            								PostedDate
//            						  FROM  GFI_SYS_Audit
//            						  WHERE PostedDate is NULL or PostedDate ='' 
//            							    ";
//            try
//            {
//                using (SQLiteConnection conn = SQLiteService.GetConnection())
//                {
//                    using (SQLiteCommand orcCmd = new SQLiteCommand(sqlString, conn))
//                    {
//                        using (SQLiteDataAdapter OrcAdapter = new SQLiteDataAdapter(orcCmd))
//                        {
//                            OrcAdapter.Fill(dataTable);
//                            return dataTable;
//                        }
//                    }
//                }
//            }
//            catch (SQLiteException ex)
//            {
//                MessageBox.Show(ex.ToString());
//                return null;
//            }
//        }
//        public static bool UpdateAuditTableSync(DataTable auditLogin)
//        {
//            bool insert = false;

//            try
//            {
//                using (SQLiteConnection conn = SQLiteService.GetConnection())
//                {
//                    using (SQLiteTransaction transaction = conn.BeginTransaction())
//                    {
//                        try
//                        {
//                            using (SQLiteCommand sqlCmd = new SQLiteCommand())
//                            {
//                                sqlCmd.Connection = conn;

//                                foreach (DataRow dr in auditLogin.Rows)
//                                {
//                                    sqlCmd.CommandText = " Update GFI_SYS_Audit set PostedFlag='Y', PostedDate = @PostedDate where PostedDate IS  NULL and AuditId=@AuditId ";
//                                    sqlCmd.Parameters.AddWithValue("@PostedDate", DateTime.Now);
//                                    sqlCmd.Parameters.AddWithValue("@AuditId", dr["AuditId"].ToString());

//                                    sqlCmd.ExecuteNonQuery();
//                                }
//                            }

//                            transaction.Commit();

//                            insert = true;
//                        }
//                        catch (SQLiteException ex)
//                        {
//                            er.ShowMsg(ex.ToString());
//                            return false;
//                        }
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                er.ShowMsg(ex.ToString());
//                return false;
//            }
//            return insert;
//        }
//        public static bool CreateLocalAudits()
//        {
//            bool insert = false;
//            string sqlstring = @"create table `gfi_sys_audit` (
//            							`auditid`	integer primary key autoincrement,
//            							`audituser`	text,
//            							`auditdate`	text,
//            							`audittype`	text,
//            						    `referencecode`	text,
//            						    `referencedesc`	text,
//            						    `referencetable`	text,
//            						    `createddate`	text,
//            						    `callerfunction`	text,		 
//            						    `sqlremarks`	text,
//            							`newvalue`	text,		 
//            							`oldvalue`	text ,
//            							`posteddate`	text,
//            							`postedflag`	text
//            						);
//            
//            						create table 'gfi_sys_loginaudit' (
//            							`auditid`	integer primary key autoincrement,
//            							`audituser`	text,
//            							`auditdate`	text,
//            							`audittype`	text,
//            							`machinename`	text,
//            							`clientgroupmachine`	text,
//            							`posteddate`	text,
//            							`postedflag`	text
//            						);
//            
//            						create table `gfi_sys_auditaccess` (
//            						 `iid` integer primary key autoincrement,
//            						 `audituser` varchar not null,
//            						 `control` varchar not null,
//            						 `auditdate` text not null,
//            							`posteddate`	text,
//            							`postedflag`	text
//            							);
//            						";
//            try
//            {
//                using (SQLiteConnection conn = GetConnection())
//                {
//                    using (SQLiteTransaction transaction = conn.BeginTransaction())
//                    {
//                        try
//                        {
//                            using (SQLiteCommand sqlcmd = new SQLiteCommand(sqlstring, conn))
//                            {
//                                sqlcmd.ExecuteNonQuery();
//                            }
//                            transaction.Commit();

//                            insert = true;
//                        }
//                        catch (SQLiteException ex)
//                        {
//                            er.ShowMsg(ex.ToString());
//                            return false;
//                        }
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                er.ShowMsg(ex.ToString());
//                return false;
//            }
//            return insert;
//        }

//        #region AuditAccessService
//        public static DataTable GetSynAccessAudit()
//        {
//            DataTable dataTable = new DataTable();

//            string sqlString = @"Select Iid,  
//                                                    AuditUser,                                
//                                                    Control	,   
//                                                    AuditDate,
//                                                    PostedFlag,
//                                                    PostedDate
//                                            FROM  GFI_SYS_AuditAccess
//                                            WHERE PostedDate is NULL or PostedDate ='' 
//                                                   ";

//            try
//            {
//                using (SQLiteConnection conn = SQLiteService.GetConnection())
//                {
//                    using (SQLiteCommand orcCmd = new SQLiteCommand(sqlString, conn))
//                    {

//                        using (SQLiteDataAdapter OrcAdapter = new SQLiteDataAdapter(orcCmd))
//                        {
//                            OrcAdapter.Fill(dataTable);
//                            return dataTable;
//                        }
//                    }
//                }
//            }
//            catch (SQLiteException ex)
//            {
//                MessageBox.Show(ex.ToString());
//                return null;
//            }
//        }
//        public static bool UpdateAccessTableSync(DataTable auditAccess)
//        {
//            bool insert = false;
//            try
//            {
//                using (SQLiteConnection conn = SQLiteService.GetConnection())
//                {
//                    using (SQLiteTransaction transaction = conn.BeginTransaction())
//                    {
//                        try
//                        {
//                            using (SQLiteCommand sqlCmd = new SQLiteCommand())
//                            {
//                                sqlCmd.Connection = conn;

//                                foreach (DataRow dr in auditAccess.Rows)
//                                {
//                                    sqlCmd.CommandText = " Update GFI_SYS_AuditAccess set PostedFlag='Y', PostedDate = @PostedDate where PostedDate IS  NULL and Iid=@Iid ";
//                                    sqlCmd.Parameters.AddWithValue("@PostedDate", DateTime.Now);
//                                    sqlCmd.Parameters.AddWithValue("@Iid", dr["Iid"].ToString());

//                                    sqlCmd.ExecuteNonQuery();
//                                }
//                            }

//                            transaction.Commit();
//                            insert = true;
//                        }
//                        catch (SQLiteException ex)
//                        {
//                            er.ShowMsg(ex.ToString());
//                            return false;
//                        }
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                er.ShowMsg(ex.ToString());
//                return false;
//            }
//            return insert;
//        }
//        public static bool PurgeLocalDb()
//        {
//            bool insert = false;
//            string sqlString = @"delete from GFI_SYS_Audit where PostedDate is not null;
//                                             delete from GFI_SYS_LoginAudit where PostedDate is not null and strftime('%d',PostedDate) - strftime('%d','now') >= 10;
//                                             delete from GFI_SYS_AuditAccess where PostedDate is not null;
//                                                   ";
//            try
//            {
//                using (SQLiteConnection conn = SQLiteService.GetConnection())
//                {
//                    using (SQLiteTransaction transaction = conn.BeginTransaction())
//                    {
//                        try
//                        {
//                            using (SQLiteCommand sqlCmd = new SQLiteCommand(sqlString, conn))
//                            {
//                                sqlCmd.ExecuteNonQuery();
//                            }
//                            transaction.Commit();

//                            insert = true;
//                        }
//                        catch (SQLiteException ex)
//                        {
//                            er.ShowMsg(ex.ToString());
//                            return false;
//                        }
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                er.ShowMsg(ex.ToString());
//                return false;
//            }
//            return insert;
//        }
//        #endregion

//        #region AuditLoginService
//        public static bool SaveAuditLoginService(AuditLogin auditLogin)
//        {
//            bool insert = false;
//            string sqlString = @"INSERT INTO GFI_SYS_LoginAudit
//                                       (  auditUser ,auditDate ,auditType ,machineName ,clientGroupMachine )
//                                 VALUES(                                         
//                                            @auditUser ,@auditDate ,@auditType ,@machineName ,@clientGroupMachine                                                    
//                                        )";

//            try
//            {
//                using (SQLiteConnection conn = SQLiteService.GetConnection())
//                {
//                    using (SQLiteTransaction transaction = conn.BeginTransaction())
//                    {
//                        try
//                        {
//                            using (SQLiteCommand sqlCmd = new SQLiteCommand(sqlString, conn))
//                            {
//                                auditLogin.AuditDate = DateTime.Now;

//                                if (string.IsNullOrEmpty(auditLogin.AuditUser))
//                                    auditLogin.AuditUser = Globals.uLogin.Username;


//                                auditLogin.ClientGroupName = SystemInformation.UserDomainName;
//                                auditLogin.MachineName = SystemInformation.ComputerName;

//                                //sqlCmd.Parameters.AddWithValue("@auditId", auditLogin.AuditId);
//                                sqlCmd.Parameters.AddWithValue("@auditUser", auditLogin.AuditUser);
//                                sqlCmd.Parameters.AddWithValue("@auditDate", auditLogin.AuditDate.ToString("dd-MM-yyyy hh:mm:ss"));
//                                sqlCmd.Parameters.AddWithValue("@auditType", auditLogin.AuditType);
//                                sqlCmd.Parameters.AddWithValue("@machineName", auditLogin.MachineName);
//                                sqlCmd.Parameters.AddWithValue("@clientGroupMachine", auditLogin.ClientGroupName);

//                                sqlCmd.ExecuteNonQuery();
//                            }
//                            transaction.Commit();

//                            insert = true;
//                        }
//                        catch (SQLiteException ex)
//                        {
//                            er.ShowMsg(ex.ToString());
//                            return false;
//                        }
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                er.ShowMsg(ex.ToString());
//                return false;
//            }
//            return insert;
//        }
//        public static AuditLogin GetLastLoginInfo(string loginType, string username)
//        {
//            DataTable dataTable = new DataTable();

//            AuditLogin auditLogin = new AuditLogin();

//            string sqlString = "select  auditUser , max(auditDate) auditDate ,auditType ,machineName ,clientGroupMachine  from GFI_SYS_LoginAudit where auditUser =@auditUser and auditType=@auditType  ";
//            try
//            {
//                using (SQLiteConnection conn = SQLiteService.GetConnection())
//                {

//                    using (SQLiteCommand orcCmd = new SQLiteCommand(sqlString, conn))
//                    {

//                        orcCmd.Parameters.AddWithValue("auditUser", username);
//                        orcCmd.Parameters.AddWithValue("auditType", loginType);

//                        using (SQLiteDataAdapter OrcAdapter = new SQLiteDataAdapter(orcCmd))
//                        {
//                            OrcAdapter.Fill(dataTable);
//                            if (dataTable.Rows.Count == 0 || string.IsNullOrEmpty(dataTable.Rows[0]["auditUser"].ToString()))
//                                return auditLogin;
//                            else
//                            {


//                                auditLogin.AuditUser = dataTable.Rows[0]["auditUser"].ToString();
//                                auditLogin.AuditDate = DateTime.Parse(dataTable.Rows[0]["auditDate"].ToString());
//                                auditLogin.AuditType = dataTable.Rows[0]["auditType"].ToString();
//                                auditLogin.MachineName = dataTable.Rows[0]["machineName"].ToString();
//                                auditLogin.ClientGroupName = dataTable.Rows[0]["clientGroupMachine"].ToString();


//                                return auditLogin; ;
//                            }
//                        }
//                    }
//                }
//            }
//            catch (SQLiteException ex)
//            {
//                MessageBox.Show(ex.ToString());
//                return null;
//            }
//        }
//        public static DataTable GetLastLoginInfo(string username)
//        {
//            DataTable dataTable = new DataTable();

//            string sqlString = "select  auditUser , max(auditDate) auditDate ,auditType ,machineName ,clientGroupMachine from GFI_SYS_LoginAudit                                 where auditUser =@auditUser and auditType IN ('01','02','03') GROUP BY auditUser,auditType ,machineName ,clientGroupMachine  ";
//            try
//            {
//                using (SQLiteConnection conn = SQLiteService.GetConnection())
//                {

//                    using (SQLiteCommand orcCmd = new SQLiteCommand(sqlString, conn))
//                    {

//                        orcCmd.Parameters.AddWithValue("auditUser", username);

//                        using (SQLiteDataAdapter OrcAdapter = new SQLiteDataAdapter(orcCmd))
//                        {
//                            OrcAdapter.Fill(dataTable);
//                            return dataTable;
//                        }
//                    }
//                }
//            }
//            catch (SQLiteException ex)
//            {
//                MessageBox.Show(ex.ToString());
//                return null;
//            }

//        }
//        public static DataTable GetSynLoginAudit()
//        {
//            DataTable dataTable = new DataTable();

//            string sqlString = "select  auditId, auditUser , auditDate auditDate ,auditType ,machineName ,clientGroupMachine, PostedDate, PostedFlag  from GFI_SYS_LoginAudit where PostedDate IS NULL or PostedDate=''";

//            try
//            {
//                using (SQLiteConnection conn = SQLiteService.GetConnection())
//                {

//                    using (SQLiteCommand orcCmd = new SQLiteCommand(sqlString, conn))
//                    {

//                        using (SQLiteDataAdapter OrcAdapter = new SQLiteDataAdapter(orcCmd))
//                        {
//                            OrcAdapter.Fill(dataTable);
//                            return dataTable;
//                        }
//                    }
//                }
//            }
//            catch (SQLiteException ex)
//            {
//                MessageBox.Show(ex.ToString());
//                return null;
//            }
//        }
//        public static bool UpdateAuditLoginSync(DataTable auditLogin)
//        {
//            bool insert = false;


//            try
//            {
//                using (SQLiteConnection conn = SQLiteService.GetConnection())
//                {
//                    using (SQLiteTransaction transaction = conn.BeginTransaction())
//                    {
//                        try
//                        {

//                            using (SQLiteCommand sqlCmd = new SQLiteCommand())
//                            {
//                                sqlCmd.Connection = conn;

//                                foreach (DataRow dr in auditLogin.Rows)
//                                {
//                                    sqlCmd.CommandText = " Update GFI_SYS_LoginAudit set  PostedFlag='Y', PostedDate = @PostedDate where PostedDate IS  NULL and AuditId=@AuditId ";
//                                    sqlCmd.Parameters.AddWithValue("@PostedDate", DateTime.Now);
//                                    sqlCmd.Parameters.AddWithValue("@AuditId", dr["AuditId"].ToString());

//                                    sqlCmd.ExecuteNonQuery();
//                                }

//                            }

//                            transaction.Commit();

//                            insert = true;
//                        }
//                        catch (SQLiteException ex)
//                        {
//                            er.ShowMsg(ex.ToString());
//                            return false;
//                        }
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                er.ShowMsg(ex.ToString());
//                return false;
//            }
//            return insert;
//        }
//        #endregion
//    }
//}