using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Services.Audits;

namespace NaveoOneLib.Services
{
    class SessionNaveoService
    {
        Errors er = new Errors();
        public DataTable GetSessionNaveo(String sConnStr)
        {
            String sqlString = @"SELECT Id, 
                                    Uid, 
                                    IpAddress, 
                                    MachineName, 
                                    Type, 
                                    InsertedDate, 
                                    ExpiryDate, 
                                    Token from GFI_SYS_SessionNaveo order by Token";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        SessionNaveo GetSessionNaveo(DataRow dr)
        {
            SessionNaveo retSessionNaveo = new SessionNaveo();
            retSessionNaveo.Id = (int)dr["Id"];
            retSessionNaveo.Uid = (int)dr["Uid"];
            retSessionNaveo.IpAddress = dr["IpAddress"].ToString();
            retSessionNaveo.MachineName = dr["MachineName"].ToString();
            retSessionNaveo.Type = dr["Type"].ToString();
            retSessionNaveo.InsertedDate = (DateTime)dr["InsertedDate"];
            retSessionNaveo.ExpiryDate = (DateTime)dr["ExpiryDate"];
            retSessionNaveo.Token = dr["Token"].ToString();
            return retSessionNaveo;
        }
        public SessionNaveo GetSessionNaveoByToken(String Token, String sConnStr)
        {
            String sqlString = @"SELECT Id, 
                                    Uid, 
                                    IpAddress, 
                                    MachineName, 
                                    Type, 
                                    InsertedDate, 
                                    ExpiryDate, 
                                    Token from GFI_SYS_SessionNaveo
                                    WHERE Token = ?
                                    order by Token";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, Token,sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetSessionNaveo(dt.Rows[0]);
        }
        public Boolean SaveSessionNaveo(SessionNaveo uSessionNaveo, Boolean bInsert, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_SessionNaveo";
            dt.Columns.Add("Uid", uSessionNaveo.Uid.GetType());
            dt.Columns.Add("IpAddress", uSessionNaveo.IpAddress.GetType());
            dt.Columns.Add("MachineName", uSessionNaveo.MachineName.GetType());
            dt.Columns.Add("Type", uSessionNaveo.Type.GetType());
            dt.Columns.Add("ExpiryDate", uSessionNaveo.ExpiryDate.GetType());
            dt.Columns.Add("Token", uSessionNaveo.Token.GetType());

            DataRow dr = dt.NewRow();
            dr["Uid"] = uSessionNaveo.Uid;
            dr["IpAddress"] = uSessionNaveo.IpAddress;
            dr["MachineName"] = uSessionNaveo.MachineName;
            dr["Type"] = uSessionNaveo.Type;
            dr["ExpiryDate"] = uSessionNaveo.ExpiryDate;
            dr["Token"] = uSessionNaveo.Token;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "Token";
            drCtrl["KeyIsIdentity"] = "FALSE";
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                {
                    bResult = false;

                    //For debugging purposes
                    Globals.dtDevCTRL = dtCtrl;
                }
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public Boolean DeleteSessionNaveo(SessionNaveo uSessionNaveo, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_SYS_SessionNaveo where Token = '" + uSessionNaveo.Token.ToString() + "'";
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            DataTable dtAudit = new AuditService().LogTran(3, "SessionNaveo", uSessionNaveo.Token.ToString(), uSessionNaveo.Uid, uSessionNaveo.Uid);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "CallerFunction";
            drCtrl["ParentTblName"] = "GFI_SYS_SessionNaveo";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
    }
}
