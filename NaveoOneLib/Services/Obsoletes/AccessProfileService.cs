using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;

namespace NaveoOneLib.Services
{
    public class AccessProfileService
    {

        Errors er = new Errors();
        public DataTable GetAccessProfiles(String sConnStr)
        {
            String sqlString = @"SELECT UID, 
                                        TemplateId, 
                                        Command, 
                                        ModuleId,
                                        AllowRead,
                                        AllowNew,
                                        AllowEdit,
                                        AllowDelete,
                                        CreatedDate, 
                                        CreatedBy, 
                                        UpdatedDate, 
                                        UpdatedBy 
                                 FROM GFI_SYS_AccessProfiles 
                                 ORDER BY UID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        public AccessProfile GetAccessProfilesById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT UID, 
                                        TemplateId, 
                                        Command, 
                                        ModuleId,
                                        AllowRead,
                                        AllowNew,
                                        AllowEdit,
                                        AllowDelete,
                                        CreatedDate, 
                                        CreatedBy, 
                                        UpdatedDate, 
                                        UpdatedBy 
                                    WHERE UID = ?
                                    ORDER BY UID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                AccessProfile retAccessProfiles = new AccessProfile();
                retAccessProfiles.UID = (int)dt.Rows[0]["UID"];
                retAccessProfiles.TemplateId = (int)dt.Rows[0]["UserId"];
                retAccessProfiles.Command = dt.Rows[0]["Command"].ToString();
                retAccessProfiles.AllowRead = dt.Rows[0]["AllowRead"].ToString();
                retAccessProfiles.AllowNew = dt.Rows[0]["AllowNew"].ToString();
                retAccessProfiles.AllowEdit = dt.Rows[0]["AllowEdit"].ToString();
                retAccessProfiles.AllowDelete = dt.Rows[0]["AllowDelete"].ToString();
                retAccessProfiles.CreatedDate = (DateTime)dt.Rows[0]["CreatedDate"];
                retAccessProfiles.CreatedBy = dt.Rows[0]["CreatedBy"].ToString();
                retAccessProfiles.UpdatedDate = (DateTime)dt.Rows[0]["UpdatedDate"];
                retAccessProfiles.UpdatedBy = dt.Rows[0]["UpdatedBy"].ToString();

                return retAccessProfiles;
            }
        }

        public String sGetAccessProfilesByTemplateId(String iTemplateId)
        {
            String sqlString = @"SELECT UID, 
                                        TemplateId, 
                                        Command,
                                        ModuleId,
                                        AllowRead,
                                        AllowNew,
                                        AllowEdit,
                                        AllowDelete,
                                        CreatedDate, 
                                        CreatedBy, 
                                        UpdatedDate, 
                                        UpdatedBy 
                                        FROM GFI_SYS_AccessProfiles
                                        WHERE TemplateId = " + iTemplateId.ToString() + " ORDER BY UID";
            return sqlString;
        }
        public DataTable GetAccessProfilesByTemplateId(String sTemplateId, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = null;
            String sql = string.Empty;

            sql = sGetAccessProfilesByTemplateId(sTemplateId);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtAccessProfiles";
            dtSql.Rows.Add(drSql);

            DataSet dsFetch = c.GetDataDS(dtSql, sConnStr);

            return dsFetch.Tables["dtAccessProfiles"];
        }

        public bool SaveAccessProfiles(AccessProfile uAccessProfiles, out DataSet dsResults, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_AccessProfiles";
            if (!dt.Columns.Contains("oprType"))
                dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Columns.Add("UID", uAccessProfiles.UID.GetType());
            dt.Columns.Add("TemplateId", uAccessProfiles.TemplateId.GetType());
            dt.Columns.Add("ModuleId", uAccessProfiles.TemplateId.GetType());
            dt.Columns.Add("Command", uAccessProfiles.Command.GetType());
            dt.Columns.Add("AllowRead", uAccessProfiles.AllowRead.GetType());
            dt.Columns.Add("AllowNew", uAccessProfiles.AllowNew.GetType());
            dt.Columns.Add("AllowEdit", uAccessProfiles.AllowEdit.GetType());
            dt.Columns.Add("AllowDelete", uAccessProfiles.AllowDelete.GetType());
            dt.Columns.Add("CreatedDate", uAccessProfiles.CreatedDate.GetType());
            dt.Columns.Add("CreatedBy", uAccessProfiles.CreatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uAccessProfiles.UpdatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uAccessProfiles.UpdatedBy.GetType());

            DataRow dr = dt.NewRow();
            dr["oprType"] = DataRowState.Added;
            dr["UID"] = uAccessProfiles.UID;
            dr["TemplateId"] = uAccessProfiles.TemplateId;
            dr["ModuleId"] = uAccessProfiles.ModuleId;
            dr["Command"] = uAccessProfiles.Command;
            dr["AllowRead"] = uAccessProfiles.AllowRead;
            dr["AllowNew"] = uAccessProfiles.AllowNew;
            dr["AllowEdit"] = uAccessProfiles.AllowEdit;
            dr["AllowDelete"] = uAccessProfiles.AllowDelete;
            dr["CreatedDate"] = DateTime.UtcNow;
            dr["CreatedBy"] = uAccessProfiles.CreatedBy;
            dr["UpdatedDate"] = DateTime.UtcNow;
            dr["UpdatedBy"] = uAccessProfiles.CreatedBy;
            dt.Rows.Add(dr);


            //--- Prepare Control Table ---//
            Connection _connection = new Connection(sConnStr); ;
            DataSet dsProcess = new DataSet();
            DataTable CTRLTBL = _connection.CTRLTBL();

            dsProcess.Tables.Add(CTRLTBL);
            dsProcess.Merge(dt);

            DataRow drCtrl = CTRLTBL.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "UID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            CTRLTBL.Rows.Add(drCtrl);

            //------------------- Gets Data for record that was updated -------------------//
            DataTable dtSql = _connection.dtSql();
            DataRow drSql = null;
            String sql = string.Empty;

            sql = sGetAccessProfilesByTemplateId(uAccessProfiles.TemplateId.ToString());
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtAccessProfiles";
            dtSql.Rows.Add(drSql);

            dsProcess.Merge(dtSql);

            //--- Process Data ---//
            dsResults = _connection.ProcessData(dsProcess, sConnStr);

            CTRLTBL = dsResults.Tables["CTRLTBL"];

            if (CTRLTBL != null)
            {
                DataRow[] dRow = CTRLTBL.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                uAccessProfiles.UID = 0;

                if (dRow.Length > 0)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }

        public bool UpdateAccessProfiles(AccessProfile uAccessProfiles, out DataSet dsResults, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_AccessProfiles";
            if (!dt.Columns.Contains("oprType"))
                dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Columns.Add("UID", uAccessProfiles.UID.GetType());
            dt.Columns.Add("UserId", uAccessProfiles.TemplateId.GetType());
            dt.Columns.Add("Command", uAccessProfiles.Command.GetType());
            dt.Columns.Add("AllowRead", uAccessProfiles.AllowRead.GetType());
            dt.Columns.Add("AllowNew", uAccessProfiles.AllowNew.GetType());
            dt.Columns.Add("AllowEdit", uAccessProfiles.AllowEdit.GetType());
            dt.Columns.Add("AllowDelete", uAccessProfiles.AllowDelete.GetType());
            dt.Columns.Add("CreatedDate", uAccessProfiles.CreatedDate.GetType());
            dt.Columns.Add("CreatedBy", uAccessProfiles.CreatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uAccessProfiles.UpdatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uAccessProfiles.UpdatedBy.GetType());
            
            DataRow dr = dt.NewRow();
            dr["oprType"] = DataRowState.Modified;
            dr["UID"] = uAccessProfiles.UID;
            dr["UserId"] = uAccessProfiles.TemplateId;
            dr["Command"] = uAccessProfiles.Command;
            dr["AllowRead"] = uAccessProfiles.AllowRead;
            dr["AllowNew"] = uAccessProfiles.AllowNew;
            dr["AllowEdit"] = uAccessProfiles.AllowEdit;
            dr["AllowDelete"] = uAccessProfiles.AllowDelete;
            dr["CreatedDate"] = uAccessProfiles.CreatedDate;
            dr["CreatedBy"] = uAccessProfiles.CreatedBy;
            dr["UpdatedDate"] = DateTime.UtcNow;
            dr["UpdatedBy"] = uAccessProfiles.CreatedBy;
            dt.Rows.Add(dr);

            //--- Prepare Control Table ---//
            Connection _connection = new Connection(sConnStr);

            DataSet dsProcess = new DataSet();
            DataTable CTRLTBL = _connection.CTRLTBL();

            dsProcess.Tables.Add(CTRLTBL);
            dsProcess.Merge(dt);

            DataRow drCtrl = CTRLTBL.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "UID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            CTRLTBL.Rows.Add(drCtrl);

            //------------------- Gets Data for record that was updated -------------------//
            DataTable dtSql = _connection.dtSql();
            DataRow drSql = null;
            String sql = string.Empty;

            sql = sGetAccessProfilesByTemplateId(uAccessProfiles.TemplateId.ToString());
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtAccessProfiles";
            dtSql.Rows.Add(drSql);

            dsProcess.Merge(dtSql);

            //--- Process Data ---//
            dsResults = _connection.ProcessData(dsProcess, sConnStr);

            CTRLTBL = dsResults.Tables["CTRLTBL"];

            if (CTRLTBL != null)
            {
                DataRow[] dRow = CTRLTBL.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    return false;
                else
                    return true;
            }
            else
                return false;

        }
        public bool DeleteAccessProfiles(AccessProfile uAccessProfiles, out DataSet dsResults, String sConnStr)
        {
            //--- Prepare Dataset ---//
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_AccessProfiles";
            dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Columns.Add("UID", uAccessProfiles.UID.GetType());

            DataRow dr = dt.NewRow();
            dr["oprType"] = DataRowState.Deleted;
            dr["UID"] = uAccessProfiles.UID;

            dt.Rows.Add(dr);

            //--- Prepare Control Table ---//
            Connection _connection = new Connection(sConnStr);

            DataTable CTRLTBL = _connection.CTRLTBL();
            CTRLTBL.TableName = "CTRLTBL";

            DataSet dsProcess = new DataSet();

            dsProcess.Tables.Add(CTRLTBL);
            dsProcess.Merge(dt);

            DataRow drCtrl = CTRLTBL.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "UID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            CTRLTBL.Rows.Add(drCtrl);

            //------------------- Gets Data for record that was updated -------------------//
            DataTable dtSql = _connection.dtSql();
            DataRow drSql = null;
            String sql = string.Empty;

            sql = sGetAccessProfilesByTemplateId(uAccessProfiles.TemplateId.ToString());
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtAccessProfiles";
            dtSql.Rows.Add(drSql);

            dsProcess.Merge(dtSql);

            //--- Process Data ---//
            dsResults = _connection.ProcessData(dsProcess, sConnStr);

            CTRLTBL = dsResults.Tables["CTRLTBL"];

            if (CTRLTBL != null)
            {
                DataRow[] dRow = CTRLTBL.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }
    }
}
