

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;

namespace NaveoOneLib.Services
{
    public partial class DataSnapshotService
    {
        Errors er = new Errors();
        public DataTable GetDataSnapshot(String sConnStr)
        {
            String sqlString = @"SELECT UId, 
            GroupingId, 
            FieldName, 
            FieldValue, 
            CreatedBy, 
            CreatedDate, 
            UpdatedBy, 
            UpdatedDate from GFI_PLA_DataSnapshot order by TingPow";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public DataSnapshot GetDataSnapshotById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT UId, 
            GroupingId, 
            FieldName, 
            FieldValue, 
            CreatedBy, 
            CreatedDate, 
            UpdatedBy, 
            UpdatedDate from GFI_PLA_DataSnapshot
            WHERE TingPow = ?
            order by TingPow";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                DataSnapshot retDataSnapshot = new DataSnapshot();
                retDataSnapshot.UId = (int)dt.Rows[0]["UId"];
                retDataSnapshot.GroupingId = (int)dt.Rows[0]["GroupingId"];
                retDataSnapshot.FieldName = dt.Rows[0]["FieldName"].ToString();
                retDataSnapshot.FieldValue = dt.Rows[0]["FieldValue"].ToString();
                retDataSnapshot.CreatedBy = dt.Rows[0]["CreatedBy"].ToString();
                retDataSnapshot.CreatedDate = (DateTime)dt.Rows[0]["CreatedDate"];
                retDataSnapshot.UpdatedBy = dt.Rows[0]["UpdatedBy"].ToString();
                retDataSnapshot.UpdatedDate = (DateTime)dt.Rows[0]["UpdatedDate"];
                return retDataSnapshot;
            }
        }

        public DataTable GetDataSnapshotByGroupId(String gID, String sConnStr)
        {
            String sqlString = @"SELECT UId, 
            GroupingId, 
            FieldName, 
            FieldValue, 
            CreatedBy, 
            CreatedDate, 
            UpdatedBy, 
            UpdatedDate from GFI_PLA_DataSnapshot
            WHERE GroupingId = ?
            order by Uid";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, gID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                return dt;
                //DataSnapshot retDataSnapshot = new DataSnapshot();
                //retDataSnapshot.UId = (int)dt.Rows[0]["UId"];
                //retDataSnapshot.GroupingId = (int)dt.Rows[0]["GroupingId"];
                //retDataSnapshot.FieldName = dt.Rows[0]["FieldName"].ToString();
                //retDataSnapshot.FieldValue = dt.Rows[0]["FieldValue"].ToString();
                //retDataSnapshot.CreatedBy = dt.Rows[0]["CreatedBy"].ToString();
                //retDataSnapshot.CreatedDate = (DateTime)dt.Rows[0]["CreatedDate"];
                //retDataSnapshot.UpdatedBy = dt.Rows[0]["UpdatedBy"].ToString();
                //retDataSnapshot.UpdatedDate = (DateTime)dt.Rows[0]["UpdatedDate"];
                //return retDataSnapshot;
            }
        }

        public bool SaveDataSnapshot(List<DataSnapshot> lDataSnapshot, Boolean bInsert, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_PLA_DataSnapshot";

            DataSnapshot uDataSnapshot = lDataSnapshot[0];
            dt.Columns.Add("UId", uDataSnapshot.UId.GetType());
            dt.Columns.Add("GroupingId", uDataSnapshot.GroupingId.GetType());
            dt.Columns.Add("FieldName", uDataSnapshot.FieldName.GetType());
            dt.Columns.Add("FieldValue", uDataSnapshot.FieldValue.GetType());
            dt.Columns.Add("CreatedBy", uDataSnapshot.CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", uDataSnapshot.CreatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uDataSnapshot.UpdatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uDataSnapshot.UpdatedDate.GetType());
            if (!dt.Columns.Contains("oprType"))
                dt.Columns.Add("oprType", typeof(DataRowState));

            foreach (DataSnapshot dSnap in lDataSnapshot)
            {
                DataRow dr = dt.NewRow();
                dr["UId"] = dSnap.UId;
                dr["GroupingId"] = dSnap.GroupingId;
                dr["FieldName"] = dSnap.FieldName;
                dr["FieldValue"] = dSnap.FieldValue;
                dr["CreatedBy"] = dSnap.CreatedBy;
                dr["CreatedDate"] = dSnap.CreatedDate;
                dr["UpdatedBy"] = dSnap.UpdatedBy;
                dr["UpdatedDate"] = dSnap.UpdatedDate;
                dt.Rows.Add(dr);
            }

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "UId";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            dtCtrl.Rows.Add(drCtrl);

            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);
            dsProcess.Merge(dtCtrl);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;


        }
        public bool SaveDataSnapshot(DataSnapshot uDataSnapshot, Boolean bInsert, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_PLA_DataSnapshot";
            dt.Columns.Add("UId", uDataSnapshot.UId.GetType());
            dt.Columns.Add("GroupingId", uDataSnapshot.GroupingId.GetType());
            dt.Columns.Add("FieldName", uDataSnapshot.FieldName.GetType());
            dt.Columns.Add("FieldValue", uDataSnapshot.FieldValue.GetType());
            dt.Columns.Add("CreatedBy", uDataSnapshot.CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", uDataSnapshot.CreatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uDataSnapshot.UpdatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uDataSnapshot.UpdatedDate.GetType());

            DataRow dr = dt.NewRow();
            dr["UId"] = uDataSnapshot.UId;
            dr["GroupingId"] = uDataSnapshot.GroupingId;
            dr["FieldName"] = uDataSnapshot.FieldName;
            dr["FieldValue"] = uDataSnapshot.FieldValue;
            dr["CreatedBy"] = uDataSnapshot.CreatedBy;
            dr["CreatedDate"] = uDataSnapshot.CreatedDate;
            dr["UpdatedBy"] = uDataSnapshot.UpdatedBy;
            dr["UpdatedDate"] = uDataSnapshot.UpdatedDate;
            if (!dt.Columns.Contains("oprType"))
                dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "UId";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            dtCtrl.Rows.Add(drCtrl);

            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);
            dsProcess.Merge(dtCtrl);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;


        }
        public bool UpdateDataSnapshot(DataSnapshot uDataSnapshot, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_PLA_DataSnapshot";
            dt.Columns.Add("UId", uDataSnapshot.UId.GetType());
            dt.Columns.Add("GroupingId", uDataSnapshot.GroupingId.GetType());
            dt.Columns.Add("FieldName", uDataSnapshot.FieldName.GetType());
            dt.Columns.Add("FieldValue", uDataSnapshot.FieldValue.GetType());
            dt.Columns.Add("CreatedBy", uDataSnapshot.CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", uDataSnapshot.CreatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uDataSnapshot.UpdatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uDataSnapshot.UpdatedDate.GetType());
            DataRow dr = dt.NewRow();
            dr["UId"] = uDataSnapshot.UId;
            dr["GroupingId"] = uDataSnapshot.GroupingId;
            dr["FieldName"] = uDataSnapshot.FieldName;
            dr["FieldValue"] = uDataSnapshot.FieldValue;
            dr["CreatedBy"] = uDataSnapshot.CreatedBy;
            dr["CreatedDate"] = uDataSnapshot.CreatedDate;
            dr["UpdatedBy"] = uDataSnapshot.UpdatedBy;
            dr["UpdatedDate"] = uDataSnapshot.UpdatedDate;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "Uid = '" + uDataSnapshot.UId + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool DeleteDataSnapshot(DataSnapshot uDataSnapshot, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_PLA_DataSnapshot", "Uid = '" + uDataSnapshot.UId + "'", sConnStr))
                return true;
            else
                return false;
        }

    }
}



