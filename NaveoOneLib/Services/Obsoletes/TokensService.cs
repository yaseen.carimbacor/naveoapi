using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;

namespace NaveoOneLib.Services
{
    class TokensService
    {
        Errors er = new Errors();
        public Token GetTokensById(String sTokenID, String sConnStr)
        {
            
 
            Connection c = new Connection(sConnStr);
            String sql = @"SELECT * from GFI_SYS_Tokens WHERE TokenID ='" + sTokenID + "' ";
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dt";
            dtSql.Rows.Add(drSql);
            DataSet ds = c.GetDataDS(dtSql, sConnStr);


            if (ds.Tables["dt"].Rows.Count ==0)
                return null;
            else
               return GetTokens(ds.Tables["dt"].Rows[0]);
           
        }

        Token GetTokens(DataRow dr)
        {
            Token retTokens = new Token();
            retTokens.TokenID = dr["TokenID"].ToString();
            retTokens.sqlCmd = dr["sqlCmd"].ToString();
            retTokens.ExpiryDate = (DateTime)dr["ExpiryDate"];
            retTokens.sData = dr["sData"].ToString();
            retTokens.sAdditionalData = dr["sAdditionalData"].ToString();
            return retTokens;
        }

        DataTable TokensDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_Tokens";
            dt.Columns.Add("TokenID", typeof(String));
            dt.Columns.Add("sqlCmd", typeof(String));
            dt.Columns.Add("ExpiryDate", typeof(DateTime));
            dt.Columns.Add("sData", typeof(String));
            dt.Columns.Add("sAdditionalData", typeof(String));

            return dt;
        }

        public String SaveTokens(Token uTokens, Boolean bInsert, String sConnStr)
        {
            DataTable dt = TokensDT();

            DataRow dr = dt.NewRow();
            uTokens.TokenID = Guid.NewGuid().ToString();
            dr["TokenID"] = uTokens.TokenID;
            dr["sqlCmd"] = uTokens.sqlCmd;
            dr["ExpiryDate"] = uTokens.ExpiryDate;
            dr["sData"] = uTokens.sData;
            dr["sAdditionalData"] = uTokens.sAdditionalData;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "TokenID";
            drCtrl["KeyIsIdentity"] = "False";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uTokens.TokenID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            String TokenGuid = String.Empty;
            if (bResult)
                TokenGuid = uTokens.TokenID;

            return TokenGuid;
        }

        public Boolean DeleteTokens(Token uTokens, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_SYS_Tokens where TokenID = " + uTokens.TokenID.ToString();
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }

        public Boolean ExecuteTokens(String sTokenID, String sConnStr)
        {
            Token t = GetTokensById(sTokenID, sConnStr);

            if (t != null)
            {
                if (t.ExpiryDate < DateTime.UtcNow)
                    return false;
            }
            else
            {
                return false;
            }
                
         

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "NoneA";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 60;
            String sql = t.sqlCmd;
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 2;
            drCtrl["TblName"] = "NoneB";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 60;
            sql = "delete from GFI_SYS_Tokens where TokenID = '" + sTokenID + "'";
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 3;
            drCtrl["TblName"] = "NoneC";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 60;
            sql = "delete from GFI_SYS_Tokens where ExpiryDate < '" + DateTime.UtcNow.ToString("yyyy/MM/dd HH:mm:ss") + "'";
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);

            DataSet dsProcess = new DataSet();

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
    }
}
