using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;

namespace NaveoOneLib.Services
{
    public class SecuMonitorAssetsService
    {
        Errors er = new Errors();

        public String GetAllSecuMonitorDevices(String sConnStr)
        {
            String strResult = " ";
            String sql = "select DeviceID from GFI_FLT_SecuMonitorAssets";
            DataTable dt = new Connection(sConnStr).GetDataDT(sql,sConnStr);
            if (dt.Rows.Count > 0)
                foreach (DataRow dr in dt.Rows)
                    strResult += "'" + dr["DeviceID"].ToString() + "',";

            strResult = strResult.Substring(0, strResult.Length - 1);
            return strResult;
        }

        public DataTable GetSecuMonitorAssets( String sConnStr)
        {
            String sqlString = @"SELECT iID, 
            DisplayName, 
            DeviceID, 
            TimeZoneID, 
            PanicID, 
            PanicValue from GFI_FLT_SecuMonitorAssets order by iID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        SecuMonitorAsset GetSecuMonitorAssets(DataRow dr)
        {
            SecuMonitorAsset retSecuMonitorAssets = new SecuMonitorAsset();
            retSecuMonitorAssets.iID = (int)dr["iID"];
            retSecuMonitorAssets.DisplayName = dr["DisplayName"].ToString();
            retSecuMonitorAssets.DeviceID = dr["DeviceID"].ToString();
            retSecuMonitorAssets.TimeZoneID = dr["TimeZoneID"].ToString();
            retSecuMonitorAssets.PanicID = dr["PanicID"].ToString();
            retSecuMonitorAssets.PanicValue = dr["PanicValue"].ToString();
            return retSecuMonitorAssets;
        }
        public SecuMonitorAsset GetSecuMonitorAssetsById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT iID, 
            DisplayName, 
            DeviceID, 
            TimeZoneID, 
            PanicID, 
            PanicValue from GFI_FLT_SecuMonitorAssets
            WHERE iID = ?
            order by iID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID,  sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetSecuMonitorAssets(dt.Rows[0]);
        }
        public Boolean SaveSecuMonitorAssets(SecuMonitorAsset uSecuMonitorAssets, Boolean bInsert, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_SecuMonitorAssets";
            dt.Columns.Add("iID", uSecuMonitorAssets.iID.GetType());
            dt.Columns.Add("DisplayName", uSecuMonitorAssets.DisplayName.GetType());
            dt.Columns.Add("DeviceID", uSecuMonitorAssets.DeviceID.GetType());
            dt.Columns.Add("TimeZoneID", uSecuMonitorAssets.TimeZoneID.GetType());
            dt.Columns.Add("PanicID", uSecuMonitorAssets.PanicID.GetType());
            dt.Columns.Add("PanicValue", uSecuMonitorAssets.PanicValue.GetType());

            DataRow dr = dt.NewRow();
            dr["iID"] = uSecuMonitorAssets.iID;
            dr["DisplayName"] = uSecuMonitorAssets.DisplayName;
            dr["DeviceID"] = uSecuMonitorAssets.DeviceID;
            dr["TimeZoneID"] = uSecuMonitorAssets.TimeZoneID;
            dr["PanicID"] = uSecuMonitorAssets.PanicID;
            dr["PanicValue"] = uSecuMonitorAssets.PanicValue;
            dt.Rows.Add(dr);


            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "iID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uSecuMonitorAssets.iID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public Boolean DeleteSecuMonitorAssets(SecuMonitorAsset uSecuMonitorAssets, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_FLT_SecuMonitorAssets where iID = " + uSecuMonitorAssets.iID.ToString();
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            //DataTable dtAudit = new AuditService().LogTran(3, "SecuMonitorAssets", uSecuMonitorAssets.iID.ToString(), Globals.uLogin.UID, uSecuMonitorAssets.TingPow);
            //drCtrl = dtCtrl.NewRow();
            //drCtrl["SeqNo"] = 100;
            //drCtrl["TblName"] = dtAudit.TableName;
            //drCtrl["CmdType"] = "TABLE";
            //drCtrl["KeyFieldName"] = "AuditID";
            //drCtrl["KeyIsIdentity"] = "TRUE";
            //drCtrl["NextIdAction"] = "SET";
            //drCtrl["NextIdFieldName"] = "CallerFunction";
            //drCtrl["ParentTblName"] = "GFI_FLT_SecuMonitorAssets";
            //dtCtrl.Rows.Add(drCtrl);
            //dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
    }
}
