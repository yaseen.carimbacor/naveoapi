

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;

namespace NaveoOneLib.Services
{
    class EmpBaseService
    {

        Errors er = new Errors();
        public DataTable GetEmpBase(String sConnStr)
        {
            String sqlString = @"SELECT 
                                iID,
                                EmpCode, 
                                NaaCode, 
                                EmpType, 
                                EmpCategory, 
                                DateJoined, 
                                CreatedBy, 
                                CreatedDate, 
                                UpdatedBy, 
                                TID,
                                UpdatedDate from GFI_HRM_EmpBase order by EmpCode";

            sqlString = @"
SELECT 
    e.iID
    , e.EmpCode 
    , e.NaaCode 
    , e.EmpType 
    , e.EmpCategory 
    , e.DateJoined 
    , e.CreatedBy 
    , e.CreatedDate 
    , e.UpdatedBy 
    , e.TID
    , e.UpdatedDate 
    , n.LastName
    , n.OtherName
from GFI_HRM_EmpBase e 
left outer join GFI_SYS_NaaAddress n on e.NaaCode = n.iID
order by EmpCode";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        public DataTable GetExtCodeByTeamId(String teamId, String sConnStr)
        {
            String sqlString = @"SELECT GFI_HRM_EmpBase.iID,
                Convert(nvarchar(50),GFI_SYS_NaaAddress.LastName)+'-'+Convert(nvarchar(50),GFI_SYS_NaaAddress.OtherName) 
                as NaaName from GFI_HRM_EmpBase ,GFI_SYS_NaaAddress,GFI_HRM_Team 
                where GFI_HRM_EmpBase.NaaCode = GFI_SYS_NaaAddress.iID
                and GFI_HRM_EmpBase.TID  =GFI_HRM_Team.iID                    
                and GFI_HRM_Team.iID= ?
                order by EmpCode";
            return new Connection(sConnStr).GetDataTableBy(sqlString, teamId, sConnStr);
        }

        public EmpBase GetEmpBaseById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT EmpCode, 
NaaCode, 
EmpType, 
EmpCategory, 
DateJoined, 
CreatedBy, 
CreatedDate, 
UpdatedBy, 
UpdatedDate from GFI_HRM_EmpBase
WHERE TingPow = ?
order by TingPow";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                EmpBase retEmpBase = new EmpBase();
                retEmpBase.iID = (int)dt.Rows[0]["iID"];
                retEmpBase.EmpCode = dt.Rows[0]["EmpCode"].ToString();
                retEmpBase.NaaCode = (int)dt.Rows[0]["NaaCode"];
                retEmpBase.TID = (int)dt.Rows[0]["TID"];
                retEmpBase.EmpType = dt.Rows[0]["EmpType"].ToString();
                retEmpBase.EmpCategory = dt.Rows[0]["EmpCategory"].ToString();
                retEmpBase.DateJoined = (DateTime)dt.Rows[0]["DateJoined"];
                retEmpBase.CreatedBy = dt.Rows[0]["CreatedBy"].ToString();
                retEmpBase.CreatedDate = (DateTime)dt.Rows[0]["CreatedDate"];
                retEmpBase.UpdatedBy = dt.Rows[0]["UpdatedBy"].ToString();
                retEmpBase.UpdatedDate = (DateTime)dt.Rows[0]["UpdatedDate"];
                return retEmpBase;
            }
        }
        public bool SaveEmpBase(EmpBase uEmpBase, Boolean bInsert, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_HRM_EmpBase";
            dt.Columns.Add("iID", uEmpBase.EmpCode.GetType());
            dt.Columns.Add("EmpCode", uEmpBase.EmpCode.GetType());
            dt.Columns.Add("NaaCode", uEmpBase.NaaCode.GetType());
            dt.Columns.Add("TID", uEmpBase.NaaCode.GetType());
            dt.Columns.Add("EmpType", uEmpBase.EmpType.GetType());
            dt.Columns.Add("EmpCategory", uEmpBase.EmpCategory.GetType());
            dt.Columns.Add("DateJoined", uEmpBase.DateJoined.GetType());
            dt.Columns.Add("CreatedBy", uEmpBase.CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", uEmpBase.CreatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uEmpBase.UpdatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uEmpBase.UpdatedDate.GetType());

            DataRow dr = dt.NewRow();
            dr["iID"] = uEmpBase.iID;
            dr["EmpCode"] = uEmpBase.EmpCode;
            dr["NaaCode"] = uEmpBase.NaaCode;
            dr["TID"] = uEmpBase.TID;
            dr["EmpType"] = uEmpBase.EmpType;
            dr["EmpCategory"] = uEmpBase.EmpCategory;
            dr["DateJoined"] = uEmpBase.DateJoined;
            dr["CreatedBy"] = uEmpBase.CreatedBy;
            dr["CreatedDate"] = uEmpBase.CreatedDate;
            dr["UpdatedBy"] = uEmpBase.UpdatedBy;
            dr["UpdatedDate"] = uEmpBase.UpdatedDate;
            if (!dt.Columns.Contains("oprType"))
                dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "iID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "SET";
            else
                drCtrl["NextIdAction"] = "GET";
            dtCtrl.Rows.Add(drCtrl);

            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);
            dsProcess.Merge(dtCtrl);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;

        }
        public bool UpdateEmpBase(EmpBase uEmpBase, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_HRM_EmpBase";
            dt.Columns.Add("EmpCode", uEmpBase.EmpCode.GetType());
            dt.Columns.Add("NaaCode", uEmpBase.NaaCode.GetType());
            dt.Columns.Add("EmpType", uEmpBase.EmpType.GetType());
            dt.Columns.Add("EmpCategory", uEmpBase.EmpCategory.GetType());
            dt.Columns.Add("DateJoined", uEmpBase.DateJoined.GetType());
            dt.Columns.Add("CreatedBy", uEmpBase.CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", uEmpBase.CreatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uEmpBase.UpdatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uEmpBase.UpdatedDate.GetType());
            DataRow dr = dt.NewRow();
            dr["EmpCode"] = uEmpBase.EmpCode;
            dr["NaaCode"] = uEmpBase.NaaCode;
            dr["EmpType"] = uEmpBase.EmpType;
            dr["EmpCategory"] = uEmpBase.EmpCategory;
            dr["DateJoined"] = uEmpBase.DateJoined;
            dr["CreatedBy"] = uEmpBase.CreatedBy;
            dr["CreatedDate"] = uEmpBase.CreatedDate;
            dr["UpdatedBy"] = uEmpBase.UpdatedBy;
            dr["UpdatedDate"] = uEmpBase.UpdatedDate;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "TingPow = '" + uEmpBase.EmpCode + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool DeleteEmpBase(EmpBase uEmpBase, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_HRM_EmpBase", "TingPow = '" + uEmpBase.EmpCode + "'", sConnStr))
                return true;
            else
                return false;
        }

    }
}



