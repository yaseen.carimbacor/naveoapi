using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;

namespace NaveoOneLib.Services
{
    class SecuMonitorToBSeenService
    {
        Errors er = new Errors();
        public DataTable GetSecuMonitorToBSeen(String sConnStr)
        {
            String sqlString = @"SELECT AssetID,displayName from GFI_FLT_SecuMonitorToBSeen 
left outer join GFI_FLT_SecuMonitorAssets on GFI_FLT_SecuMonitorToBSeen.AssetID = 
GFI_FLT_SecuMonitorAssets.iID

order by AssetID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        SecuMonitorToBSeen GetSecuMonitorToBSeen(DataRow dr)
        {
            SecuMonitorToBSeen retSecuMonitorToBSeen = new SecuMonitorToBSeen();
            retSecuMonitorToBSeen.AssetID = (int)dr["AssetID"];
            retSecuMonitorToBSeen.DisplayName = dr["DisplayName"].ToString();
            return retSecuMonitorToBSeen;
        }
        public SecuMonitorToBSeen GetSecuMonitorToBSeenById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT AssetID,displayName from GFI_FLT_SecuMonitorToBSeen 
left outer join GFI_FLT_SecuMonitorAssets on GFI_FLT_SecuMonitorToBSeen.AssetID = 
GFI_FLT_SecuMonitorAssets.iID
WHERE AssetID = ?
order by AssetID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID,sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetSecuMonitorToBSeen(dt.Rows[0]);
        }
        public Boolean SaveSecuMonitorToBSeen(SecuMonitorToBSeen uSecuMonitorToBSeen, Boolean bInsert, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_SecuMonitorToBSeen";
            dt.Columns.Add("AssetID", uSecuMonitorToBSeen.AssetID.GetType());

            DataRow dr = dt.NewRow();
            dr["AssetID"] = uSecuMonitorToBSeen.AssetID;
            dt.Rows.Add(dr);


            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "TingPow";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uSecuMonitorToBSeen.AssetID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public Boolean DeleteSecuMonitorToBSeen(SecuMonitorToBSeen uSecuMonitorToBSeen, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_FLT_SecuMonitorToBSeen where AssetID = " + uSecuMonitorToBSeen.AssetID.ToString();
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            //DataTable dtAudit = new AuditService().LogTran(3, "SecuMonitorToBSeen", uSecuMonitorToBSeen.TingPow.ToString(), Globals.uLogin.UID, uSecuMonitorToBSeen.TingPow);
            //drCtrl = dtCtrl.NewRow();
            //drCtrl["SeqNo"] = 100;
            //drCtrl["TblName"] = dtAudit.TableName;
            //drCtrl["CmdType"] = "TABLE";
            //drCtrl["KeyFieldName"] = "AuditID";
            //drCtrl["KeyIsIdentity"] = "TRUE";
            //drCtrl["NextIdAction"] = "SET";
            //drCtrl["NextIdFieldName"] = "CallerFunction";
            //drCtrl["ParentTblName"] = "GFI_FLT_SecuMonitorToBSeen";
            //dtCtrl.Rows.Add(drCtrl);
            //dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
    }
}
