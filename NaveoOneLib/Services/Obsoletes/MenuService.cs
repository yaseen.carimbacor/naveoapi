﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NaveoOneLib.DBCon;

namespace NaveoOneLib.Services
{
    class MenuService
    {
        public static String sGetMenu()
        {
            String sql = @"SELECT 
                            UID
                              ,ModuleName
                              ,Description
                              ,Command     
                              ,MenuHeader      
                              ,MenuName
                              ,OrderIndex
                              ,Title
                              ,Summary
                              ,GFI_SYS_Modules.MenuHeaderId      
                              ,GFI_SYS_Modules.MenuGroupId
                              ,GFI_SYS_Modules.OrderIndex      
                              FROM GFI_SYS_Modules, GFI_SYS_MenuHeader,GFI_SYS_MenuGroup    
                              where GFI_SYS_Modules.MenuHeaderId = GFI_SYS_MenuHeader.MenuHeaderId  
                              and GFI_SYS_Modules.MenuGroupId = GFI_SYS_MenuGroup.MenuId   
                              order by GFI_SYS_Modules.MenuHeaderId,GFI_SYS_Modules.MenuGroupId,GFI_SYS_Modules.OrderIndex";

            return sql;
        }
        public DataTable GetMenu(String sConnStr)
        {
            DataTable dt = new Connection(sConnStr).GetDataDT(sGetMenu(), sConnStr);
            return dt;
        }
    }
}
