﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Telerik.WinControls.UI;
using System.Drawing;
using System.Data;
using NaveoOneLib.Common;
using System.Windows.Forms;
using NaveoOneLib.Models;
using NaveoOneLib.Services;
using NaveoOneLib.Maps;
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.Trips;
using NaveoOneLib.Models.Zones;

namespace NaveoOneLib.UILogics
{
    public class FormatGrid
    {
        public DataTable dt = new DataTable();
        String _Report;
        Color myColor;

        public FormatGrid()
        {
        }
        void gv_CellFormatting(object sender, CellFormattingEventArgs e)
        {
            try
            {
                String sValue = String.Empty;
                switch (_Report)
                {
                    case "Live Tracking":
                        String strDirectionPath = Globals.DeviceDirectionPath();
                        //sValue = e.CellElement.Value.ToString();
                        int imageSize = 20;
                        e.CellElement.ImageLayout = ImageLayout.None;
                        e.CellElement.ImageAlignment = ContentAlignment.MiddleLeft;
                        e.CellElement.TextImageRelation = TextImageRelation.ImageBeforeText;
                        e.CellElement.TextAlignment = ContentAlignment.MiddleLeft;
                        switch (e.CellElement.ColumnInfo.Name)
                        {
                            case "Status":
                                {
                                    switch (sValue)
                                    {
                                        case "Suspect":
                                            e.CellElement.Image = Image.FromFile(strDirectionPath + @"Suspect.png").GetThumbnailImage(imageSize, imageSize, null, new IntPtr());
                                            break;

                                        default:
                                            e.CellElement.Image = Image.FromFile(e.Row.Cells["DirectionImage"].Value.ToString()).GetThumbnailImage(imageSize, imageSize, null, new IntPtr());
                                            break;
                                    }
                                }
                                break;

                            default:
                                e.CellElement.ResetValue(LightVisualElement.ImageProperty, Telerik.WinControls.ValueResetFlags.Local);
                                e.CellElement.ResetValue(LightVisualElement.ImageAlignmentProperty, Telerik.WinControls.ValueResetFlags.Local);
                                e.CellElement.ResetValue(LightVisualElement.ImageLayoutProperty, Telerik.WinControls.ValueResetFlags.Local);
                                break;
                        }
                        break;

                    case "Trip History":
                        if (e.CellElement.Value != null)
                            sValue = e.CellElement.Value.ToString();
                        try
                        {
                            myColor = Color.FromArgb((int)e.CellElement.RowInfo.Cells["iColor"].Value);
                        }
                        catch { }

                        switch (e.CellElement.ColumnInfo.Name)
                        {
                            case "ArrivalAddress":
                                e.CellElement.BackColor = myColor;
                                e.CellElement.DrawFill = true;
                                e.CellElement.GradientStyle = Telerik.WinControls.GradientStyles.Solid;
                                break;

                            case "ArrivalZone":
                                e.CellElement.BackColor = myColor;
                                e.CellElement.DrawFill = true;
                                e.CellElement.GradientStyle = Telerik.WinControls.GradientStyles.Solid;
                                break;

                            default:
                                e.CellElement.ResetValue(LightVisualElement.BackColorProperty, Telerik.WinControls.ValueResetFlags.Local);
                                e.CellElement.ResetValue(LightVisualElement.DrawFillProperty, Telerik.WinControls.ValueResetFlags.Local);
                                e.CellElement.ResetValue(LightVisualElement.GradientStyleProperty, Telerik.WinControls.ValueResetFlags.Local);
                                break;
                        }
                        break;

                    case "CustomerVisit":
                        if (e.CellElement.Value != null)
                            sValue = e.CellElement.Value.ToString();
                        try
                        {
                            myColor = Color.FromArgb((int)e.CellElement.RowInfo.Cells["iColor"].Value);
                        }
                        catch { }

                        switch (e.CellElement.ColumnInfo.Name)
                        {
                            case "Zone":
                                e.CellElement.BackColor = myColor;
                                e.CellElement.DrawFill = true;
                                e.CellElement.GradientStyle = Telerik.WinControls.GradientStyles.Solid;
                                break;

                            default:
                                e.CellElement.ResetValue(LightVisualElement.BackColorProperty, Telerik.WinControls.ValueResetFlags.Local);
                                e.CellElement.ResetValue(LightVisualElement.DrawFillProperty, Telerik.WinControls.ValueResetFlags.Local);
                                e.CellElement.ResetValue(LightVisualElement.GradientStyleProperty, Telerik.WinControls.ValueResetFlags.Local);
                                break;
                        }
                        break;
                }
            }
            catch { }
        }

        public RadGridView GridFormattingPostData(string report, RadGridView gv)
        {
            switch (report)
            {
                case "Auxiliary":
                    if (gv.Rows.Count < 1)
                        return gv;

                    gv.Columns["AssetId"].HeaderText = "Asset Id";
                    gv.Columns["AssetId"].Width = 75;
                    gv.Columns["RegistrationNo"].HeaderText = "Registration No";
                    gv.Columns["RegistrationNo"].Width = 150;
                    gv.Columns["Auxiliary"].HeaderText = "Auxiliary";
                    gv.Columns["Auxiliary"].Width = 75;
                    gv.Columns["TimeOn"].HeaderText = "Time On";
                    gv.Columns["TimeOn"].Width = 155;
                    gv.Columns["TimeOff"].HeaderText = "Time Off";
                    gv.Columns["TimeOff"].Width = 155;
                    gv.Columns["Duration"].HeaderText = "Duration";
                    gv.Columns["Duration"].Width = 120;
                    break;

                case "Daily Summary":
                    if (gv.Rows.Count < 1)
                        return gv;

                    gv.Columns["Date"].FormatString = "{0: dd/MM/yyyy}";
                    gv.Columns["TripStartTime"].FormatString = "{0: HH:mm:ss}";
                    gv.Columns["TripEndTime"].FormatString = "{0: HH:mm:ss}";

                    gv.Columns["RegistrationNo"].HeaderText = "Registration No";
                    gv.Columns["TripCount"].HeaderText = "Trips";
                    gv.Columns["TripStartTime"].HeaderText = "Start Time";
                    gv.Columns["TripEndTime"].HeaderText = "End Time";
                    gv.Columns["TripDistance"].HeaderText = "KM Travelled";
                    gv.Columns["InZoneStoppedTime"].HeaderText = "In Zone Stopped Time";
                    gv.Columns["OutZoneStoppedTime"].HeaderText = "Out Zone Stopped Time";
                    break;

                case "Exception":
                    if (gv.Rows.Count < 1)
                        return gv;

                    gv.Columns["AssetID"].IsVisible = false;
                    gv.Columns["AssetID"].VisibleInColumnChooser = false;
                    gv.Columns["AssetName"].HeaderText = "AssetID";
                    gv.Columns["ParentRuleID"].IsVisible = false;
                    gv.Columns["ParentRuleID"].VisibleInColumnChooser = false;
                    gv.Columns["PKID"].IsVisible = false;
                    gv.Columns["PKID"].VisibleInColumnChooser = false;
                    break;

                case "Live Tracking":
                    try
                    {
                        if (gv.Rows.Count < 1)
                            return gv;

                        gv.Columns["DSLR"].HeaderText = "Last Report";
                        gv.Columns["RegistrationNo"].HeaderText = "Registration No";
                        gv.Columns["DriverName"].HeaderText = "Driver Name";
                        gv.Columns["dtLocal"].HeaderText = "Last Data Received";
                        gv.Columns["Address"].HeaderText = "Address";
                        gv.Columns["DeviceId"].HeaderText = "Device Id";

                        gv.Columns["DeviceId"].TextAlignment = ContentAlignment.MiddleLeft;
                        gv.Columns["Longitude"].IsVisible = false;
                        gv.Columns["Longitude"].VisibleInColumnChooser = false;
                        gv.Columns["Latitude"].IsVisible = false;
                        gv.Columns["Latitude"].VisibleInColumnChooser = false;
                        gv.Columns["DirectionImage"].IsVisible = false;
                        gv.Columns["DirectionImage"].VisibleInColumnChooser = false;

                        gv.Columns["Address"].IsVisible = false;
                        gv.Columns["DeviceId"].IsVisible = false;
                        gv.Columns["AssetID"].IsVisible = false;
                        gv.Columns["DriverID"].IsVisible = false;

                        gv.BestFitColumns(Telerik.WinControls.UI.BestFitColumnMode.AllCells);
                    }
                    catch (Exception ex)// throw Exception
                    {
                        if (Globals.bDebugMode)
                        {
                            MessageBox.Show(ex.ToString());
                        }
                    }

                    break;
                case "Trip History":
                    if (gv.Rows.Count < 1)
                        return gv;

                    gv.BestFitColumns();
                    gv.Columns["iColor"].IsVisible = false;
                    gv.Columns["iColor"].VisibleInColumnChooser = false;

                    gv.Columns["iID"].IsVisible = false;
                    gv.Columns["DepartureZoneId"].IsVisible = false;
                    gv.Columns["ArrivalZoneId"].IsVisible = false;

                    if (gv.Columns.Contains("OverSpeed1Time"))
                        gv.Columns["OverSpeed1Time"].IsVisible = false;
                    if (gv.Columns.Contains("OverSpeed2Time"))
                        gv.Columns["OverSpeed2Time"].IsVisible = false;
                    if (gv.Columns.Contains("Accel"))
                        gv.Columns["Accel"].IsVisible = false;
                    if (gv.Columns.Contains("Brake"))
                        gv.Columns["Brake"].IsVisible = false;
                    if (gv.Columns.Contains("Corner"))
                        gv.Columns["Corner"].IsVisible = false;

                    if (gv.Columns.Contains("Lon"))
                        gv.Columns["Lon"].IsVisible = false;
                    if (gv.Columns.Contains("Lat"))
                        gv.Columns["Lat"].IsVisible = false;

                    foreach (GridViewDataColumn c in gv.Columns)
                    {
                        if (c.DataType == typeof(TimeSpan))
                            c.FormatString = "{0:%h}h {0:%m}m {0:%s}s";
                    }
                    break;

                case "Customer Visit":
                    if (gv.Rows.Count < 1)
                        return gv;

                    gv.BestFitColumns();
                    foreach (GridViewDataColumn c in gv.Columns)
                    {
                        if (c.DataType == typeof(TimeSpan))
                            c.FormatString = "{0:%h}h {0:%m}m {0:%s}s";
                    }
                    break;
            }
            return gv;
        }
        public RadGridView GridFormatingEvent(string report, RadGridView gv)
        {
            _Report = report;
            gv.CellFormatting += new CellFormattingEventHandler(gv_CellFormatting);
            return gv;
        }
        public RadGridView GridFormatting(string report, RadGridView gv)
        {
            gv = GridFormatingEvent(report, gv);
            //gv.AllowEditRow = false;
            _Report = report;
            switch (report)
            {
                case "Passenger":
                    gv.CellFormatting -= new CellFormattingEventHandler(gv_CellFormatting);
                    break;
                case "Zone Visited Not Visited":
                    gv.CellFormatting -= new CellFormattingEventHandler(gv_CellFormatting);
                    CustomSummaryItem itsZVNV = new CustomSummaryItem("InZoneDuration", "{0:%d}d {0:%h}h {0:%m}m", GridAggregateFunction.Sum);
                    GridViewSummaryRowItem summaryRowItemTHZVNV = new GridViewSummaryRowItem(new GridViewSummaryItem[] { itsZVNV });
                    gv.SummaryRowsBottom.Add(summaryRowItemTHZVNV);
                    gv.BestFitColumns();
                    break;

                case "Zone Visited Detail":

                    //GridViewSummaryItem summaryItem_DistanceTravelled = new GridViewSummaryItem();
                    //summaryItem_DistanceTravelled.Name = "DistanceTravelled";
                    //summaryItem_DistanceTravelled.Aggregate = GridAggregateFunction.Sum;

                    //GridViewSummaryItem summaryItem_TotalTrips = new GridViewSummaryItem();
                    //summaryItem_TotalTrips.Name = "TotalTrips";
                    //summaryItem_TotalTrips.Aggregate = GridAggregateFunction.Sum;

                    //GridViewSummaryRowItem summaryRowItemAS = new GridViewSummaryRowItem();
                    //summaryRowItemAS.Add(summaryItem_DistanceTravelled);
                    //summaryRowItemAS.Add(summaryItem_TotalTrips);
                    //gv.SummaryRowsBottom.Add(summaryRowItemAS);

                    gv.CellFormatting -= new CellFormattingEventHandler(gv_CellFormatting);
                    CustomSummaryItem its1 = new CustomSummaryItem("StopDuration", "{0:%d}d {0:%h}h {0:%m}m", GridAggregateFunction.Sum);
                    GridViewSummaryRowItem summaryRowItemTH1 = new GridViewSummaryRowItem(new GridViewSummaryItem[] { its1 });
                    gv.SummaryRowsBottom.Add(summaryRowItemTH1);
                    gv.BestFitColumns();
                    break;

                case "Zones Visited":
                    CustomSummaryItem stsZV = new CustomSummaryItem("StopDuration", "{0:%d}d {0:%h}h {0:%m}m", GridAggregateFunction.Sum);
                    GridViewSummaryRowItem summaryRowItemZV = new GridViewSummaryRowItem(new GridViewSummaryItem[] { stsZV });

                    gv.SummaryRowsBottom.Add(summaryRowItemZV);
                    gv.BestFitColumns();
                    gv.CellFormatting -= new CellFormattingEventHandler(gv_CellFormatting);
                    break;

                case "Assets Maintenance - Costs Analysis Report":
                    gv.MasterTemplate.ShowTotals = true;
                    GridViewSummaryItem summaryItem_CostExVAT = new GridViewSummaryItem();
                    summaryItem_CostExVAT.Name = "CostExVAT";
                    summaryItem_CostExVAT.Aggregate = GridAggregateFunction.Sum;

                    GridViewSummaryItem summaryItem_VATAmount = new GridViewSummaryItem();
                    summaryItem_VATAmount.Name = "VATAmount";
                    summaryItem_VATAmount.Aggregate = GridAggregateFunction.Sum;

                    GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem();
                    summaryRowItem.Add(summaryItem_CostExVAT);
                    summaryRowItem.Add(summaryItem_VATAmount);

                    gv.SummaryRowsBottom.Add(summaryRowItem);

                    GridViewCommandColumn gridViewCommandColumn1 = new GridViewCommandColumn();
                    gridViewCommandColumn1.Name = "MaintURI";
                    gridViewCommandColumn1.UseDefaultText = false;
                    gridViewCommandColumn1.HeaderText = "Maint Id";
                    gridViewCommandColumn1.FieldName = "MaintURI";
                    gridViewCommandColumn1.Width = 60;
                    gv.MasterTemplate.Columns.Add(gridViewCommandColumn1);
                    break;

                case "Trip Summary":
                    gv.CellFormatting -= new CellFormattingEventHandler(gv_CellFormatting);
                    break;

                case "Customer Visit":
                    gv.MasterTemplate.ShowTotals = true;
                    GridViewSummaryItem CountSum = new GridViewSummaryItem("Count", "{0}", GridAggregateFunction.Sum);
                    CountSum.FormatString = "{0:f2}";

                    CustomSummaryItem durSum = new CustomSummaryItem("Duration", "{0:%d}d {0:%h}h {0:%m}m", GridAggregateFunction.Sum);
                    GridViewSummaryRowItem summaryRowItemCus = new GridViewSummaryRowItem(new GridViewSummaryItem[] { CountSum, durSum });
                    gv.SummaryRowsBottom.Add(summaryRowItemCus);
                    gv.CellFormatting -= new CellFormattingEventHandler(gv_CellFormatting);
                    break;

                case "Exception":
                    gv.CellFormatting -= new CellFormattingEventHandler(gv_CellFormatting);
                    break;

                case "Auxiliary":
                    CustomSummaryItem csiDurationAux = new CustomSummaryItem("Duration", "{0:%d}d {0:%h}h {0:%m}m", GridAggregateFunction.Sum);
                    GridViewSummaryRowItem summaryRowItemAux = new GridViewSummaryRowItem(new GridViewSummaryItem[] { csiDurationAux });
                    gv.SummaryRowsBottom.Add(summaryRowItemAux);
                    gv.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.None;
                    gv.CellFormatting -= new CellFormattingEventHandler(gv_CellFormatting);
                    break;

                case "Live Tracking":
                    break;

                case "Trip History":
                    GridViewCheckBoxColumn CBColumn = new GridViewCheckBoxColumn();
                    CBColumn.HeaderText = "Display";
                    CBColumn.NullValue = "0";
                    CBColumn.Width = 1;
                    CBColumn.Name = "Display";
                    CBColumn.EnableHeaderCheckBox = true;
                    gv.Columns.Insert(0, CBColumn);

                    gv.MasterTemplate.ShowTotals = true;
                    GridViewSummaryItem maxSpeed = new GridViewSummaryItem("TripDistance", "{0}", GridAggregateFunction.Sum);
                    GridViewSummaryItem summaryItem2 = new GridViewSummaryItem("MaxSpeed", "{0}", GridAggregateFunction.Max);
                    maxSpeed.FormatString = "{0:f2}";

                    CustomSummaryItem its = new CustomSummaryItem("IdlingTime", "{0:%d}d {0:%h}h {0:%m}m", GridAggregateFunction.Sum);
                    CustomSummaryItem sts = new CustomSummaryItem("StopTime", "{0:%d}d {0:%h}h {0:%m}m", GridAggregateFunction.Sum);
                    CustomSummaryItem tts = new CustomSummaryItem("TripTime", "{0:%d}d {0:%h}h {0:%m}m", GridAggregateFunction.Sum);

                    CustomSummaryItem os1ts = new CustomSummaryItem("OverSpeed1Time", "{0:%d}d {0:%h}h {0:%m}m", GridAggregateFunction.Sum);
                    CustomSummaryItem os2ts = new CustomSummaryItem("OverSpeed2Time", "{0:%d}d {0:%h}h {0:%m}m", GridAggregateFunction.Sum);

                    GridViewSummaryRowItem summaryRowItemTH = new GridViewSummaryRowItem(new GridViewSummaryItem[] { maxSpeed, summaryItem2, its, sts, tts, os1ts, os2ts });
                    gv.SummaryRowsBottom.Add(summaryRowItemTH);
                    break;

                case "Daily Summary":
                    GridViewSummaryItem summaryItem_TripCount = new GridViewSummaryItem();
                    summaryItem_TripCount.Name = "TripCount";
                    summaryItem_TripCount.Aggregate = GridAggregateFunction.Sum;

                    GridViewSummaryItem summaryItem_TripDistance = new GridViewSummaryItem();
                    summaryItem_TripDistance.Name = "TripDistance";
                    summaryItem_TripDistance.Aggregate = GridAggregateFunction.Sum;

                    GridViewSummaryRowItem summaryRowItemDaily = new GridViewSummaryRowItem();
                    summaryRowItemDaily.Add(summaryItem_TripCount);
                    summaryRowItemDaily.Add(summaryItem_TripDistance);

                    gv.SummaryRowsBottom.Add(summaryRowItemDaily);
                    //for (int i = 0; i < gv.ColumnCount; i++)
                    //    gv.Columns[1].Width = 200;
                    gv.BestFitColumns();
                    gv.CellFormatting -= new CellFormattingEventHandler(gv_CellFormatting);
                    break;
            }
            return gv;
        }

        public DataSet DataFormatting(string report, DataSet ds, String sConnStr)
        {
            return DataFormatting(report, ds, null, sConnStr);
        }
        public DataSet DataFormatting(string report, DataSet ds, List<TripHeader> lTrip, String sConnStr)
        {
            DataSet dsResult = new DataSet();
            DataTable TriList = new DataTable();
            DataTable dtMain = new DataTable();
            DataView view = new DataView();
            DataTable dtDistinctVehicle = new DataTable();
            int dveh = 0;
            int rawcount = 0;
            myConverter convert = new myConverter();
            switch (report)
            {
                case "Zone Visited":
                    #region Zone Visited
                    lTrip = new ESRIMapService().GetAddress(lTrip, Globals.lZone, sConnStr);
                    if (lTrip.Count > 0)
                    {
                        lTrip = lTrip.OrderBy(o => o.AssetID).ThenBy(o => o.GPSDataStart.DateTimeGPS_UTC).ToList();
                    }
                    //---
                    dtMain = convert.ListToDataTable(lTrip);
                    dsResult.Tables.Add(dtMain);
                    #endregion
                    break;

                case "Passenger":
                    #region Passenger Report
                    dtMain = ds.Tables[0];
                    dtMain.Columns.Add("Client", typeof(String));
                    dtMain.Columns.Add("Asset", typeof(String));
                    dtMain.Columns.Add("SeatingCapacity", typeof(String));
                    dtMain.Columns.Add("Contractor", typeof(String));
                    dtMain.Columns.Add("iButtonID", typeof(String));
                    dtMain.Columns.Add("PassengerName", typeof(String));
                    dtMain.Columns.Add("Dept", typeof(String));
                    dtMain.Columns.Add("RouteCode", typeof(String));
                    dtMain.Columns.Add("TimeIn", typeof(String));
                    dtMain.Columns.Add("Address", typeof(String));

                    ESRIMapService esriMap = new ESRIMapService();

                    esriMap.ReadProjectNLoadMap();

                    foreach (DataRow drP in dtMain.Rows)
                    {
                        //Get Client
                        Double dLon = (Double)drP["Longitude"];
                        Double dLat = (Double)drP["Latitude"];

                        drP["Client"] = esriMap.GetZone(dLon, dLat, Globals.lZone, sConnStr);
                        drP["Address"] = esriMap.GetAddress(dLon, dLat);

                        drP["iButtonID"] = drP["TypeValue"].ToString();

                        foreach (var q in Globals.lDriver.AsEnumerable())
                        {
                            if (q.EmployeeType == "Passenger")
                            {
                                if (q.iButton.ToString() == drP["TypeValue"].ToString())
                                {
                                    //  drP["iButtonID"] = q.iButton;
                                    drP["PassengerName"] = q.sDriverName != null ? q.sDriverName : q.iButton.ToString();
                                }
                            }
                        }

                        foreach (DataRow r1 in Globals.dtGMAsset.Rows)
                        {
                            if (r1["StrucID"].ToString() == drP["AssetID"].ToString())
                            {
                                drP["Asset"] = r1["GMDescription"].ToString();
                                foreach (DataRow r2 in Globals.dtGMAsset.Rows)
                                {
                                    if (r2["GMID"].ToString() == r1["ParentGMID"].ToString())
                                        drP["Contractor"] = r2["GMDescription"].ToString();
                                }
                            }
                        }
                    }

                    TriList = dtMain.Copy();
                    dsResult.Tables.Add(TriList);
                    #endregion
                    break;

                case "Trip Summary":
                    #region Trip Summary
                    TriList = ds.Tables[0];
                    rawcount = TriList.Rows.Count;

                    //Create Main Datatable
                    dtMain = SetDataTable("Trip Summary", new DataTable("dtMain"));
                    //Get the total ZoneTypes
                    foreach (DataRow r in Globals.dtZoneTypes.Rows)
                    {
                        if (!dtMain.Columns.Contains(r["sDescription"].ToString()))
                            dtMain.Columns.Add(r["sDescription"].ToString());
                    }

                    //Get unique Asset
                    view = new DataView(TriList);
                    dtDistinctVehicle = view.ToTable(true, "Vehicle");
                    dveh = dtDistinctVehicle.Rows.Count;

                    //Get data for each Asset         
                    foreach (DataRow vehRow in dtDistinctVehicle.Rows)
                    {
                        string exp1 = "Vehicle = '" + vehRow[0] + "'";
                        DataRow[] vehArray = null;
                        vehArray = TriList.Select(exp1);
                        DataTable dtVehValues = TriList.Clone();
                        foreach (DataRow row in vehArray)
                        {
                            dtVehValues.ImportRow(row);
                        }
                        //Get total trip count
                        int TripCount = dtVehValues.Rows.Count;

                        //Get total Triptime
                        TimeSpan totaltriptime = new TimeSpan();
                        foreach (DataRow triptime in dtVehValues.Rows)
                        {
                            TimeSpan intervalVal;
                            TimeSpan.TryParse((triptime["Triptime"].ToString()), out intervalVal);
                            totaltriptime = totaltriptime + intervalVal;
                        }

                        //Get total Stoptime
                        TimeSpan totalstoptime = new TimeSpan();
                        foreach (DataRow stoptime in dtVehValues.Rows)
                        {
                            TimeSpan intervalVal;
                            TimeSpan.TryParse((stoptime["Stoptime"].ToString()), out intervalVal);
                            totalstoptime = totalstoptime + intervalVal;
                        }


                        //Get unique zones from Asset data
                        DataView zoneview = new DataView(dtVehValues);
                        DataTable dtDistinctZone = zoneview.ToTable(true, "ArrivalZoneID");
                        int dzone = dtDistinctZone.Rows.Count;

                        //Build the table
                        DataRow r = dtMain.NewRow();
                        r["Asset"] = vehRow[0];
                        r["Total Trip"] = TripCount;
                        r["Total Triptime"] = String.Format("{0}d {1}hr {2}min", totaltriptime.Days, totaltriptime.Hours, totaltriptime.Minutes);
                        r["Total Stoptime"] = String.Format("{0}d {1}hr {2}min", totalstoptime.Days, totalstoptime.Hours, totalstoptime.Minutes);

                        //Get data for each unique zone in current vehicle
                        TimeSpan variable;
                        TimeSpan variable1 = new TimeSpan();
                        foreach (DataRow zoneRow in dtDistinctZone.Rows)
                        {
                            string exp2 = "ArrivalZoneID = '" + zoneRow[0] + "' and Vehicle = '" + vehRow[0] + "'";
                            DataRow[] zoneArray = null;
                            zoneArray = TriList.Select(exp2);
                            DataTable dtZoneValues = TriList.Clone();
                            foreach (DataRow row in zoneArray)
                            {
                                dtZoneValues.ImportRow(row);
                            }
                            int ztotal = dtZoneValues.Rows.Count;
                            DataView dv1 = dtZoneValues.DefaultView;
                            dv1.Sort = "From asc";
                            DataTable sortedZoneDT1 = dv1.ToTable();

                            //Get duration: difference of each last departure and first arrival

                            foreach (DataRow duration in sortedZoneDT1.Rows)
                            {
                                variable = Convert.ToDateTime(duration["To"].ToString()) - Convert.ToDateTime(duration["From"].ToString());
                                variable1 = variable1 + variable;
                            }

                        }
                        //Get zone type for each zone
                        foreach (DataRow zoneRow in dtDistinctZone.Rows)
                        {
                            if (zoneRow[0].ToString() != "0")
                            {
                                List<ZoneType> z1 = Globals.lZone.FirstOrDefault(o => o.zoneID == (int)zoneRow[0]).lZoneType;
                                foreach (ZoneType z2 in z1)
                                {
                                    r[z2.sDescription] = String.Format("{0}d {1}hr {2}min", variable1.Days, variable1.Hours, variable1.Minutes); ;
                                }
                            }
                        }
                        dtMain.Rows.Add(r);
                    }
                    dsResult.Tables.Add(dtMain);
                    break;
                    #endregion

                case "Customer Visit":
                    #region Customer Visit
                    TriList = ds.Tables[0];
                    rawcount = TriList.Rows.Count;
                    //Create Main Datatable
                    dtMain = new FormatGrid().SetDataTable("Customer Visit", null);

                    view = new DataView(TriList);
                    dtDistinctVehicle = view.ToTable(true, "Vehicle");
                    dveh = dtDistinctVehicle.Rows.Count;
                    //Get data for each vehicle         

                    foreach (DataRow vehRow in dtDistinctVehicle.Rows)
                    {
                        string exp1 = "Vehicle = '" + vehRow[0] + "'";
                        DataRow[] vehArray = null;
                        vehArray = TriList.Select(exp1);
                        DataTable dtVehValues = TriList.Clone();
                        foreach (DataRow row in vehArray)
                        {
                            dtVehValues.ImportRow(row);
                        }
                        int vtotal = dtVehValues.Rows.Count;

                        //Get driver name
                        string sDriver = null;
                        foreach (DataRow driverRow in dtVehValues.Rows)
                        {
                            sDriver = driverRow["Driver"].ToString();
                            break;
                        }

                        //Get unique zone from vehicle data
                        DataView zoneview = new DataView(dtVehValues);
                        DataTable dtDistinctZone = zoneview.ToTable(true, "ArrivalZoneID");
                        int dzone = dtDistinctZone.Rows.Count;

                        //Get data for each unique zone in current vehicle
                        foreach (DataRow zoneRow in dtDistinctZone.Rows)
                        {
                            if (zoneRow[0].ToString() != "0")
                            {
                                string exp2 = "ArrivalZoneID = '" + zoneRow[0] + "' and Vehicle = '" + vehRow[0] + "'";
                                DataRow[] zoneArray = null;
                                zoneArray = TriList.Select(exp2);
                                DataTable dtZoneValues = TriList.Clone();
                                foreach (DataRow row in zoneArray)
                                {
                                    dtZoneValues.ImportRow(row);
                                }
                                int ztotal = dtZoneValues.Rows.Count;

                                //get total count,duration, first arrival and last departure                                          
                                //sort by first arrival desc
                                DataView dv1 = dtZoneValues.DefaultView;
                                dv1.Sort = "From asc";
                                DataTable sortedZoneDT1 = dv1.ToTable();
                                string firstArrival = string.Empty;

                                foreach (DataRow zoneRow2 in sortedZoneDT1.Rows)
                                {
                                    firstArrival = zoneRow2["From"].ToString();
                                    break;
                                }

                                //sort by first arrival desc
                                DataView dv2 = dtZoneValues.DefaultView;
                                dv2.Sort = "To desc";
                                DataTable sortedZoneDT2 = dv2.ToTable();
                                string lastDeparture = string.Empty;
                                foreach (DataRow zoneRow2 in sortedZoneDT2.Rows)
                                {
                                    lastDeparture = zoneRow2["To"].ToString();
                                    break;
                                }
                                //TimeSpan variable = Convert.ToDateTime(lastDeparture) - Convert.ToDateTime(firstArrival);
                                //string Duration = Math.Round(variable.TotalHours, 2) + ":" + Math.Round(variable.TotalMinutes, 2) + ":" + Math.Round(variable.TotalSeconds, 2);

                                //Get Trip duration: difference of each trip's last departure and first arrival
                                TimeSpan variable;
                                TimeSpan variable1 = new TimeSpan();

                                foreach (DataRow duration in sortedZoneDT1.Rows)
                                {
                                    variable = Convert.ToDateTime(duration["To"].ToString()) - Convert.ToDateTime(duration["From"].ToString());
                                    variable1 = variable1 + variable;
                                }

                                //Get zone, zone type and zone extn ref
                                StringBuilder sZT = new StringBuilder();
                                List<ZoneType> z1 = Globals.lZone.FirstOrDefault(o => o.zoneID == (int)zoneRow[0]).lZoneType;
                                foreach (ZoneType z2 in z1)
                                {
                                    sZT = sZT.Append(z2.sDescription);
                                }

                                //Built the table
                                DataRow r = dtMain.NewRow();
                                r["Vehicle"] = vehRow[0];
                                r["VehicleGroup"] = "";
                                r["Driver"] = sDriver;
                                r["Zone"] = Globals.lZone.FirstOrDefault(x => x.zoneID == (int)zoneRow[0]).description;
                                r["ZoneType"] = sZT;
                                r["ZoneExternalRef"] = "";
                                r["FirstArrival"] = firstArrival;
                                r["LastDeparture"] = lastDeparture;
                                //r["Duration"] = String.Format("{0}d {1}hr {2}min", variable1.Days, variable1.Hours, variable1.Minutes); ;
                                r["TripDuration"] = variable1;

                                //r["StopDuration"] = dtZoneValues.Compute("Sum(StopTime)", "");
                                var st = dtZoneValues.Compute("Sum(StopTime)", "");
                                if (st != null)
                                    r["StopDuration"] = st;
                                else
                                    r["StopDuration"] = "00:00:00";

                                r["Count"] = dtZoneValues.Rows.Count; ;
                                dtMain.Rows.Add(r);

                            }
                        }
                    }
                    dsResult.Tables.Add(dtMain);
                    break;
                    #endregion

                case "Exception":
                    #region Exception
                    DataTable dtDetails = new DataTable("dtDetails");
                    DataTable dtParent = new DataTable("dtParent");

                    ESRIMapService ESRIMap = new ESRIMapService();
                    ESRIMap.ReadProjectNLoadMap();

                    dtParent = SetDataTable("Exception", dt);
                    dtParent.TableName = "dtParent";

                    dtParent.Clear();
                    dtDetails.Clear();
                    dtDetails = ds.Tables["ExceptionsT"];
                    dtDetails.Columns.Add("Location");
                    dtDetails.Columns.Add("Zone");
                    dtDetails.Columns.Add("Duration");

                    foreach (DataRow dr in dtDetails.Rows)
                    {
                        Asset a = new Asset();
                        foreach (Asset aa in Globals.lAsset)
                            if (aa.AssetID.ToString() == dr["AssetID"].ToString())
                            {
                                a = aa;
                                break;
                            }
                        DateTime dtloc = DateTime.UtcNow;
                        DateTime.TryParse(dr["DateTimeGPS_UTC"].ToString(), out dtloc);
                        dr["DateTimeGPS_UTC"] = dtloc + a.TimeZoneTS;

                        DateTime.TryParse(dr["ExcpCreated"].ToString(), out dtloc);
                        dr["ExcpCreated"] = dtloc + a.TimeZoneTS;

                        DateTime.TryParse(dr["Exception_Date"].ToString(), out dtloc);
                        dr["Exception_Date"] = dtloc + a.TimeZoneTS;
                    }

                    //Populating Parent table. this will be the report
                    DataView DetailsView = new DataView(dtDetails);
                    String[] grp = new String[3];
                    grp[0] = "AssetID";
                    grp[1] = "ParentRuleID"; ;
                    grp[2] = "GroupID";
                    DataTable dtDistinctDetailsID = DetailsView.ToTable(true, grp);
                    foreach (DataRow dr in dtDistinctDetailsID.Rows)
                    {
                        DataRow drID = dtParent.NewRow();

                        DataTable dtGrp = dtDetails.Select("AssetID = " + dr["AssetID"].ToString() + "and GroupID = " + dr["GroupID"].ToString() + "and ParentRuleID = " + dr["ParentRuleID"].ToString()).CopyToDataTable();
                        int iRows = dtGrp.Rows.Count;

                        drID["GroupID"] = dtGrp.Rows[0]["GroupID"].ToString();
                        drID["AssetName"] = Globals.dtGMAsset.Select("StrucID = " + dtGrp.Rows[0]["AssetID"].ToString() + "")[0]["GMDescription"].ToString();
                        drID["AssetID"] = dtGrp.Rows[0]["AssetID"].ToString();
                        drID["ExcpCreated"] = dtGrp.Rows[0]["ExcpCreated"].ToString();

                        if (dtGrp.Rows[0]["DriverID"].ToString().Trim().Length > 0)
                        {
                            drID["DriverName"] = Globals.dtGMDriver.Select("StrucID = " + dtGrp.Rows[0]["DriverID"].ToString() + "")[0]["GMDescription"].ToString();

                            DateTime dtGStart = Convert.ToDateTime(dtGrp.AsEnumerable().Min(o => o["DateTimeGPS_UTC"]));
                            DateTime dtGEnd = Convert.ToDateTime(dtGrp.AsEnumerable().Max(o => o["DateTimeGPS_UTC"]));
                            TimeSpan tsG = dtGEnd - dtGStart;
                            drID["Duration"] = tsG;

                            try
                            {
                                DataTable dtCreate = dtGrp.Select("Exception_Date = ExcpCreated").CopyToDataTable();
                                DateTime dtCreateStart = Convert.ToDateTime(dtCreate.AsEnumerable().Min(o => o["DateTimeGPS_UTC"]));
                                DateTime dtCreateEnd = Convert.ToDateTime(dtCreate.AsEnumerable().Max(o => o["DateTimeGPS_UTC"]));
                                TimeSpan tsCreate = dtCreateEnd - dtCreateStart;
                                drID["DurationTriggered"] = tsCreate;
                            }
                            catch { }

                            double dMaxSpeed = Convert.ToDouble(dtGrp.AsEnumerable().Max(o => o["speed"]));
                            drID["MaxSpeed"] = dMaxSpeed;

                            drID["Address"] = ESRIMap.GetAddress(Convert.ToDouble(dtGrp.Rows[0]["fLongitude"]), Convert.ToDouble(dtGrp.Rows[0]["fLatitude"]));

                            drID["StartUID"] = dtGrp.Rows[0]["UID"];
                            drID["StartDateTime"] = dtGrp.Rows[0]["DateTimeGPS_UTC"];
                            drID["EndUID"] = dtGrp.Rows[iRows - 1]["UID"];
                            drID["EndDateTime"] = dtGrp.Rows[iRows - 1]["DateTimeGPS_UTC"];
                        }
                        drID["RuleName"] = dtGrp.Rows[0]["RuleName"].ToString();
                        drID["ParentRuleID"] = dtGrp.Rows[0]["ParentRuleID"].ToString();

                        dtParent.Rows.Add(drID);
                    }
                    dsResult.Merge(dtParent);
                    dsResult.Merge(dtDetails);
                    break;
                    #endregion
            }
            return dsResult;
        }

        public RadGridView SetDataSource(RadGridView gv, string report)
        {
            if (report == "Trip History")
                dt = SetDataTable(report, new myConverter().ListToDataTable<TripHeader>(new List<TripHeader>()));
            else
                dt = SetDataTable(report, new DataTable());
            DataRow r1 = dt.NewRow();
            DataRow r2 = dt.NewRow();
            switch (report)
            {
                case "Zone Visited Detail":
                    r1["TripID"] = "123";
                    r1["RegistrationNo"] = "666";
                    r1["DepartureZone"] = "DepartureZone";
                    r1["ArrivalZone"] = "ArrivalZone";
                    r1["DepartureTime"] = new DateTime(2015, 4, 1, 2, 45, 55);
                    r1["ArrivalTime"] = new DateTime(2015, 4, 1, 2, 45, 55);
                    r1["RestartTime"] = new DateTime(2015, 4, 1, 2, 45, 55);
                    r1["StopDuration"] = new TimeSpan(0, 30, 22);
                    r1["TripDistance"] = "20";
                    r1["Weight"] = "90";

                    r2["TripID"] = "123";
                    r2["RegistrationNo"] = "666";
                    r2["DepartureZone"] = "DepartureZone";
                    r2["ArrivalZone"] = "ArrivalZone";
                    r2["DepartureTime"] = new DateTime(2015, 4, 1, 2, 45, 55);
                    r2["ArrivalTime"] = new DateTime(2015, 4, 1, 2, 45, 55);
                    r2["RestartTime"] = new DateTime(2015, 4, 1, 2, 45, 55);
                    r2["StopDuration"] = new TimeSpan(0, 30, 22);
                    r2["TripDistance"] = "20";
                    r2["Weight"] = "90";
                    break;

                case "Asset Summary":
                    r1["RegistrationNo"] = "123";
                    r1["Driver"] = "12Driver3";
                    r1["DistanceTravelled"] = "100";
                    r1["StopsUnder15m"] = "5";
                    r1["StopsUnder30m"] = "10";
                    r1["StopsUnder1hr"] = "12";
                    r1["StopsUnder5hr"] = "20";
                    r1["TotalTrips"] = "45";
                    r1["DrivingTime"] = new TimeSpan(3, 5, 6);
                    r1["ActualOdometer"] = "45000";
                    r1["SpeedOver70KM"] = "5";
                    r1["SpeedOver110KM"] = "2";

                    r2["RegistrationNo"] = "123";
                    r2["Driver"] = "12Driver3";
                    r2["DistanceTravelled"] = "100";
                    r2["StopsUnder15m"] = "5";
                    r2["StopsUnder30m"] = "10";
                    r2["StopsUnder1hr"] = "12";
                    r2["StopsUnder5hr"] = "20";
                    r2["TotalTrips"] = "45";
                    r2["DrivingTime"] = new TimeSpan(3, 5, 6);
                    r2["ActualOdometer"] = "45000";
                    r2["SpeedOver70KM"] = "5";
                    r2["SpeedOver110KM"] = "2";
                    break;

                case "Zones Visited":
                    r1["RegistrationNo"] = "578";
                    r1["DepartureZone"] = "DepartureZone";
                    r1["ArrivalZone"] = "ArrivalZone";
                    r1["DepartureTime"] = new DateTime(2015, 4, 1); ;
                    r1["ArrivalTime"] = new DateTime(2015, 4, 1); ;
                    r1["RestartTime"] = new DateTime(2015, 4, 1); ;
                    r1["StopDuration"] = new TimeSpan(4, 5, 6);

                    r2["RegistrationNo"] = "578";
                    r2["DepartureZone"] = "DepartureZone";
                    r2["ArrivalZone"] = "ArrivalZone";
                    r2["DepartureTime"] = new DateTime(2015, 4, 1); ;
                    r2["ArrivalTime"] = new DateTime(2015, 4, 1); ;
                    r2["RestartTime"] = new DateTime(2015, 4, 1); ;
                    r2["StopDuration"] = new TimeSpan(4, 5, 6);
                    break;

                case "Passenger":
                    r1["Client"] = "Client";
                    r1["Asset"] = "Asset";
                    r1["SeatingCapacity"] = "89";
                    r1["Contractor"] = "Contractor";
                    r1["iButtonID"] = "iButtonID";
                    r1["PassengerName"] = "PassengerName";
                    r1["Dept"] = "Dept";
                    r1["RouteCode"] = "Route1";
                    r1["TimeIn"] = "TimeIn";
                    r1["Address"] = "Address";

                    r2["Client"] = "Client";
                    r2["Asset"] = "Asset";
                    r2["SeatingCapacity"] = "67";
                    r2["Contractor"] = "Contractor";
                    r2["iButtonID"] = "iButtonID";
                    r2["PassengerName"] = "PassengerName";
                    r2["Dept"] = "Dept";
                    r2["RouteCode"] = "Route1";
                    r2["TimeIn"] = "TimeIn";
                    r2["Address"] = "Address";
                    break;

                case "Trip Summary":
                    r1["Asset"] = "AssetName";
                    r1["Total Trip"] = "8";
                    r1["Total Triptime"] = new TimeSpan(3, 45, 55);
                    r1["Total Stoptime"] = new TimeSpan(1, 45, 55);

                    r2["Asset"] = "AssetName";
                    r2["Total Trip"] = "8";
                    r2["Total Triptime"] = new TimeSpan(3, 45, 55);
                    r2["Total Stoptime"] = new TimeSpan(1, 45, 55);
                    break;

                case "Exception":
                    r1["AssetName"] = "AssetName";
                    r1["AssetID"] = "AsseD";
                    r1["Duration"] = "Duration";
                    r1["MaxSpeed"] = "90";
                    r1["ParentRuleID"] = "ParentRuleID";
                    r1["RuleName"] = "RuleName";
                    r1["Zone"] = "Zone";
                    r1["Address"] = "Address";
                    r1["StartDateTime"] = new DateTime(2015, 4, 1); ;
                    r1["EndDateTime"] = new DateTime(2015, 4, 1);
                    r1["MaxSpeedUID"] = "90";

                    r2["AssetName"] = "AssetName";
                    r2["AssetID"] = "AssetID";
                    r2["Duration"] = "Duration";
                    r2["MaxSpeed"] = "90";
                    r2["ParentRuleID"] = "ParentRuleID";
                    r2["RuleName"] = "RuleName";
                    r2["Zone"] = "Zone";
                    r2["Address"] = "Address";
                    r2["StartDateTime"] = new DateTime(2015, 4, 1); ;
                    r2["EndDateTime"] = new DateTime(2015, 4, 1);
                    r2["MaxSpeedUID"] = "90";
                    gv.Refresh();
                    break;

                case "Trip History":
                    DataRow r3 = dt.NewRow();
                    r3["Vehicle"] = "Vehicle";
                    r3["Driver"] = "Driver";
                    r3["From"] = new DateTime(2015, 4, 1, 12, 30, 00);
                    r3["TripTime"] = new TimeSpan(5);
                    r3["To"] = new DateTime(2015, 4, 1, 12, 30, 00);
                    r3["TripDistance"] = 100;
                    r3["DepartureAddress"] = "DepartureAddress";
                    r3["ArrivalAddress"] = "ArrivalAddress";
                    r3["DepartureZone"] = "DepartureZone";
                    r3["ArrivalZone"] = "ArrivalZone";
                    r3["MaxSpeed"] = 120;
                    r3["GroupDate"] = new DateTime(2015, 4, 1);
                    r3["iColor"] = 0;
                    dt.Rows.Add(r3);

                    DataRow r4 = dt.NewRow();
                    r4["Vehicle"] = "Vehicle";
                    r4["Driver"] = "Driver";
                    r4["From"] = new DateTime(2015, 4, 1, 12, 30, 00);
                    r4["TripTime"] = new TimeSpan(15);
                    r4["To"] = new DateTime(2015, 4, 1, 12, 30, 00);
                    r4["TripDistance"] = 66;
                    r4["DepartureAddress"] = "DepartureAddress";
                    r4["ArrivalAddress"] = "ArrivalAddress";
                    r4["DepartureZone"] = "DepartureZone";
                    r4["ArrivalZone"] = "ArrivalZone";
                    r4["MaxSpeed"] = 125;
                    r4["GroupDate"] = new DateTime(2015, 4, 1);
                    r4["iColor"] = 0;
                    dt.Rows.Add(r4);
                    gv.DataSource = dt;
                    break;

                case "Auxiliary":
                    r1["AssetId"] = "AssetId";
                    r1["RegistrationNo"] = "RegistrationNo";
                    r1["Auxiliary"] = "Auxiliary";
                    r1["TimeOn"] = new DateTime(2015, 2, 1, 12, 14, 00);
                    r1["TimeOff"] = new DateTime(2015, 2, 1, 12, 14, 00);
                    r1["Duration"] = new TimeSpan();

                    r2["AssetId"] = "AssetId";
                    r2["RegistrationNo"] = "RegistrationNo";
                    r2["Auxiliary"] = "Auxiliary";
                    r2["TimeOn"] = new DateTime(2015, 2, 1, 12, 14, 00);
                    r2["TimeOff"] = new DateTime(2015, 2, 1, 12, 14, 00);
                    r2["Duration"] = new TimeSpan();
                    break;

                case "Zone Visited Not Visited":
                    DataRow rZVNV1 = dt.NewRow();
                    rZVNV1["Device"] = "Device";
                    rZVNV1["Zone"] = "Customer Zone";
                    rZVNV1["DateTimeIn"] = new DateTime(2015, 2, 1, 12, 14, 00);
                    rZVNV1["DateTimeOut"] = new DateTime(2015, 2, 1, 12, 14, 00);
                    rZVNV1["Duration"] = new TimeSpan();
                    dt.Rows.Add(rZVNV1);

                    DataRow rZVNV2 = dt.NewRow();
                    rZVNV2["Device"] = "Device";
                    rZVNV2["Zone"] = "Customer Zone";
                    rZVNV2["DateTimeIn"] = new DateTime(2015, 2, 1, 12, 14, 00);
                    rZVNV2["DateTimeOut"] = new DateTime(2015, 2, 1, 12, 14, 00);
                    rZVNV2["Duration"] = new TimeSpan();
                    dt.Rows.Add(rZVNV2);
                    break;

                case "Live Tracking":
                    DataRow r7 = dt.NewRow();
                    r7["DSLR"] = "DSLR";
                    r7["RegistrationNo"] = "RegistrationNo";
                    r7["DriverName"] = "DriverName";
                    r7["dtLocal"] = "RegistrationNo";
                    r7["Address"] = "Address";
                    r7["DeviceId"] = "DeviceId";
                    r7["Longitude"] = "Longitude";
                    r7["Latitude"] = "Latitude";
                    r7["DirectionImage"] = "DirectionImage";
                    dt.Rows.Add(r7);

                    DataRow r8 = dt.NewRow();
                    r8["DSLR"] = "DSLR";
                    r8["RegistrationNo"] = "RegistrationNo";
                    r8["DriverName"] = "DriverName";
                    r8["dtLocal"] = "RegistrationNo";
                    r8["Address"] = "Address";
                    r8["DeviceId"] = "DeviceId";
                    r8["Longitude"] = "Longitude";
                    r8["Latitude"] = "Latitude";
                    r8["DirectionImage"] = "DirectionImage";
                    dt.Rows.Add(r8);
                    break;

                case "Daily Summary":
                    DataRow r9 = dt.NewRow();
                    r9["Date"] = new DateTime(2015, 2, 1, 12, 14, 00);
                    r9["RegistrationNo"] = "RegistrationNo";
                    r9["Driver"] = "DriverName";
                    r9["TripCount"] = "RegistrationNo";
                    r9["TripStartTime"] = new DateTime(2015, 2, 1, 12, 14, 00);
                    r9["TripEndTime"] = new DateTime(2015, 2, 1, 12, 14, 00);
                    r9["TripDistance"] = 1000;
                    r9["IdlingTime"] = new TimeSpan();
                    r9["InZoneStoppedTime"] = new TimeSpan();
                    r9["OutZoneStoppedTime"] = new TimeSpan();
                    dt.Rows.Add(r9);

                    DataRow r11 = dt.NewRow();
                    r11["Date"] = new DateTime(2015, 2, 1, 12, 14, 00);
                    r11["RegistrationNo"] = "RegistrationNo";
                    r11["Driver"] = "DriverName";
                    r11["TripCount"] = "RegistrationNo";
                    r11["TripStartTime"] = new DateTime(2015, 2, 1, 12, 14, 00);
                    r11["TripEndTime"] = new DateTime(2015, 2, 1, 12, 14, 00);
                    r11["TripDistance"] = 1000;
                    r11["IdlingTime"] = new TimeSpan();
                    r11["InZoneStoppedTime"] = new TimeSpan();
                    r11["OutZoneStoppedTime"] = new TimeSpan();
                    dt.Rows.Add(r11);
                    break;

                case "Customer Visit":
                    r1["Vehicle"] = "Vehicle";
                    r1["VehicleGroup"] = "VehicleGroup";
                    r1["Driver"] = "Driver";
                    r1["Zone"] = "Zone";

                    r2["ZoneType"] = "ZoneType";
                    r2["ZoneExternalRef"] = "ZoneExternalRef";
                    r2["FirstArrival"] = new DateTime(2015, 2, 1, 12, 14, 00);
                    r2["LastDeparture"] = new DateTime(2015, 2, 1, 12, 14, 00);
                    r2["Duration"] = new TimeSpan(17, 20, 55);
                    r2["Count"] = 110;
                    r2["Vehicle"] = "Vehicle";
                    r2["VehicleGroup"] = "VehicleGroup";
                    r2["Driver"] = "Driver";
                    r2["Zone"] = "Zone";
                    r2["ZoneType"] = "ZoneType";
                    r2["ZoneExternalRef"] = "ZoneExternalRef";
                    r2["FirstArrival"] = new DateTime(2015, 2, 1, 12, 14, 00);
                    r2["LastDeparture"] = new DateTime(2015, 2, 1, 12, 14, 00);
                    r2["Duration"] = new TimeSpan(17, 20, 55);
                    r2["Count"] = 110;
                    break;
            }
            dt.Rows.Add(r1);
            dt.Rows.Add(r2);
            dt.AcceptChanges();
            gv.DataSource = dt;
            return gv;
        }
        public DataTable SetDataTable(string report, DataTable dt)
        {
            if (dt == null)
                dt = new DataTable();
            switch (report)
            {
                case "Zone Visited Detail":
                    dt.Columns.Add("TripID");
                    dt.Columns.Add("RegistrationNo", typeof(string));
                    dt.Columns.Add("DepartureZone", typeof(string));
                    dt.Columns.Add("ArrivalZone", typeof(string));
                    dt.Columns.Add("DepartureTime", typeof(DateTime));
                    dt.Columns.Add("ArrivalTime", typeof(DateTime));
                    dt.Columns.Add("RestartTime", typeof(DateTime));
                    dt.Columns.Add("StopDuration", typeof(TimeSpan));
                    dt.Columns.Add("TripDistance", typeof(string));
                    dt.Columns.Add("Weight");
                    break;

                case "Asset Summary":
                    dt.Columns.Add("RegistrationNo", typeof(string));
                    dt.Columns.Add("Driver", typeof(string));
                    dt.Columns.Add("DistanceTravelled", typeof(int));
                    dt.Columns.Add("StopsUnder15m", typeof(int));
                    dt.Columns.Add("StopsUnder30m", typeof(int));
                    dt.Columns.Add("StopsUnder1hr", typeof(int));
                    dt.Columns.Add("StopsUnder5hr", typeof(int));
                    dt.Columns.Add("TotalTrips", typeof(int));
                    dt.Columns.Add("DrivingTime", typeof(TimeSpan));
                    dt.Columns.Add("ActualOdometer", typeof(int));
                    dt.Columns.Add("SpeedOver70KM", typeof(int));
                    dt.Columns.Add("SpeedOver110KM", typeof(int));
                    break;

                case "Zones Visited":
                    dt.TableName = "RecordDataTable";
                    dt.Columns.Add("RegistrationNo", typeof(string));
                    dt.Columns.Add("DepartureZone", typeof(string));
                    dt.Columns.Add("ArrivalZone", typeof(string));
                    dt.Columns.Add("DepartureTime", typeof(DateTime));
                    dt.Columns.Add("ArrivalTime", typeof(DateTime));
                    dt.Columns.Add("RestartTime", typeof(DateTime));
                    dt.Columns.Add("StopDuration", typeof(TimeSpan));
                    break;
                case "Assets Maintenance - Costs Analysis Report":

                    break;

                case "Passenger":
                    dt.Columns.Add("Client", typeof(String));
                    dt.Columns.Add("Asset", typeof(String));
                    dt.Columns.Add("SeatingCapacity", typeof(String));
                    dt.Columns.Add("Contractor", typeof(String));
                    dt.Columns.Add("iButtonID", typeof(String));
                    dt.Columns.Add("PassengerName", typeof(String));
                    dt.Columns.Add("Dept", typeof(String));
                    dt.Columns.Add("RouteCode", typeof(String));
                    dt.Columns.Add("TimeIn", typeof(String));
                    dt.Columns.Add("Address", typeof(String));
                    break;

                case "Trip Summary":
                    dt.Columns.Add("Asset");
                    dt.Columns.Add("Total Trip");
                    dt.Columns.Add("Total Triptime");
                    dt.Columns.Add("Total Stoptime");
                    break;

                case "Exception":
                    if (dt.Columns.Contains("PKID"))
                        break;
                    dt.Columns.Add("PKID");
                    dt.Columns.Add("GroupID");
                    dt.Columns.Add("AssetName");
                    dt.Columns.Add("AssetID");
                    dt.Columns.Add("DriverName");
                    dt.Columns.Add("Duration");
                    dt.Columns.Add("MaxSpeed");
                    dt.Columns.Add("ParentRuleID");
                    dt.Columns.Add("RuleName");
                    dt.Columns.Add("Zone");
                    dt.Columns.Add("Address");
                    dt.Columns.Add("StartUID");
                    dt.Columns.Add("StartDateTime");
                    dt.Columns.Add("EndUID");
                    dt.Columns.Add("EndDateTime");
                    dt.Columns.Add("MaxSpeedUID");
                    dt.Columns.Add("ExcpCreated");
                    dt.Columns.Add("DurationTriggered");
                    break;

                case "Trip History":
                    dt.Columns.Remove("TDetails");
                    dt.Columns.Remove("ExceptionFlag");
                    dt.Columns.Remove("GPSDataStartUID");
                    dt.Columns.Remove("GPSDataEndUID");
                    dt.Columns.Remove("GPSDataEnd");
                    dt.Columns.Remove("GPSDataStart");
                    dt.Columns.Remove("Geo_DeviceID");
                    dt.Columns.Remove("Geo_iButton");
                    dt.Columns.Remove("Geo_StartTripTime");
                    dt.Columns.Remove("Geo_EndTripTime");
                    dt.Columns.Remove("CMth");
                    dt.Columns.Remove("AssetID");
                    dt.Columns.Remove("DriverID");
                    dt.Columns.Remove("myAsset");
                    dt.Columns.Remove("myDriver");
                    dt.Columns.Add("Vehicle");
                    dt.Columns.Add("Driver");
                    dt.Columns["Driver"].SetOrdinal(0);
                    dt.Columns["Vehicle"].SetOrdinal(0);
                    dt.Columns.Add("From", typeof(DateTime));
                    dt.Columns.Add("To", typeof(DateTime));
                    dt.Columns.Add("GroupDate", typeof(DateTime));
                    dt.Columns.Add("iColor", typeof(int));
                    dt.Columns.Add("Lon", typeof(Double));
                    dt.Columns.Add("Lat", typeof(Double));
                    break;

                case "Auxiliary":
                    dt.TableName = "Summary";
                    dt.Columns.Add("AssetId", typeof(String));
                    dt.Columns.Add("RegistrationNo", typeof(String));
                    dt.Columns.Add("Auxiliary", typeof(String));
                    dt.Columns.Add("TimeOn", typeof(DateTime));
                    dt.Columns.Add("TimeOff", typeof(DateTime));
                    dt.Columns.Add("Duration", typeof(TimeSpan));
                    break;

                case "Zone Visited Not Visited":
                    dt.Columns.Add("Device", typeof(String));
                    dt.Columns.Add("Zone", typeof(String));
                    dt.Columns.Add("DateTimeIn", typeof(DateTime));
                    dt.Columns.Add("DateTimeOut", typeof(DateTime));
                    dt.Columns.Add("Duration", typeof(TimeSpan));
                    break;

                case "Live Tracking":
                    dt.Columns.Add("DSLR", typeof(String));
                    dt.Columns.Add("RegistrationNo", typeof(String));
                    dt.Columns.Add("DriverName", typeof(String));
                    dt.Columns.Add("dtLocal", typeof(String));
                    dt.Columns.Add("Address", typeof(String));
                    dt.Columns.Add("DeviceId", typeof(String));
                    dt.Columns.Add("Longitude", typeof(String));
                    dt.Columns.Add("Latitude", typeof(String));
                    dt.Columns.Add("DirectionImage", typeof(String));
                    break;

                case "Daily Summary":
                    dt.TableName = "Summary";
                    dt.Columns.Add("Date", typeof(DateTime));
                    dt.Columns.Add("RegistrationNo", typeof(string));
                    dt.Columns.Add("Driver", typeof(string));
                    dt.Columns.Add("TripCount", typeof(string));
                    dt.Columns.Add("TripStartTime", typeof(DateTime));
                    dt.Columns.Add("TripEndTime", typeof(DateTime));
                    dt.Columns.Add("TripDistance", typeof(float));
                    dt.Columns.Add("IdlingTime", typeof(TimeSpan));
                    dt.Columns.Add("InZoneStoppedTime", typeof(TimeSpan));
                    dt.Columns.Add("OutZoneStoppedTime", typeof(TimeSpan));
                    break;

                case "Customer Visit":
                    dt.Columns.Add("Vehicle", typeof(string));
                    dt.Columns.Add("VehicleGroup", typeof(string));
                    dt.Columns.Add("Driver", typeof(string));
                    dt.Columns.Add("Zone", typeof(string));
                    dt.Columns.Add("ZoneType", typeof(string));
                    dt.Columns.Add("ZoneExternalRef", typeof(string));
                    dt.Columns.Add("FirstArrival", typeof(DateTime));
                    dt.Columns.Add("LastDeparture", typeof(DateTime));
                    dt.Columns.Add("TripDuration", typeof(TimeSpan));
                    dt.Columns.Add("StopDuration", typeof(TimeSpan));
                    dt.Columns.Add("Count", typeof(Int32));
                    break;
            }
            return dt;
        }
    }
}