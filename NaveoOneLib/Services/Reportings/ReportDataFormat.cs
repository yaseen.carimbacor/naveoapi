﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NaveoOneLib.Models.Trips;

namespace NaveoOneLib.UILogics
{
    public class ReportDataFormat
    {
        public DataTable DailySummaryReport(List<TripHeader> lTrip, List<int> selList, Boolean byDriver)
        {
            DataTable RecordDataTable = new DataTable();
            RecordDataTable = new FormatGrid().SetDataTable("Daily Summary", RecordDataTable);

            //For each Asset
            for (int i = 0; i < selList.Count; i++)
            {
                var vDates = lTrip.Where(item => item.myAsset.AssetID == selList[i])
                                                        .OrderBy(o => o.GPSDataStart.DateTimeGPS_UTC.ToLocalTime())
                                                        .GroupBy(o => o.GPSDataStart.DateTimeGPS_UTC.ToLocalTime().Date)
                                                        .ToList();
                if (byDriver)
                    vDates = lTrip.Where(item => item.myDriver.DriverID == selList[i])
                                                        .OrderBy(o => o.GPSDataStart.DateTimeGPS_UTC.ToLocalTime())
                                                        .GroupBy(o => o.GPSDataStart.DateTimeGPS_UTC.ToLocalTime().Date)
                                                        .ToList();

                for (int j = 0; j < vDates.Count; j++)
                {
                    List<TripHeader> lDailyTrip = lTrip.Where(item => item.myAsset.AssetID == selList[i])
                                                        .Where(o => o.GPSDataStart.DateTimeGPS_UTC.ToLocalTime().Date == vDates[j].Key)
                                                        .ToList();
                    if (byDriver)
                        lDailyTrip = lTrip.Where(item => item.myDriver.DriverID == selList[i])
                                                            .Where(o => o.GPSDataStart.DateTimeGPS_UTC.ToLocalTime().Date == vDates[j].Key)
                                                            .ToList();

                    DataRow dr = RecordDataTable.NewRow();
                    dr["Date"] = vDates[j].Key.ToLocalTime();
                    dr["TripCount"] = lDailyTrip.Count;
                    dr["TripStartTime"] = lDailyTrip[0].GPSDataStart.DateTimeGPS_UTC.ToLocalTime();
                    dr["TripEndTime"] = lDailyTrip[lDailyTrip.Count - 1].GPSDataEnd.DateTimeGPS_UTC.ToLocalTime();
                    dr["RegistrationNo"] = lDailyTrip[0].myAsset.AssetNumber;

                    String sDriver = String.Empty;
                    var vDriver = lDailyTrip.GroupBy(o => o.myDriver).ToList();
                    for (int v = 0; v < vDriver.Count; v++)
                        if (!sDriver.Contains(vDriver[v].Key.sDriverName))
                            sDriver += vDriver[v].Key.sDriverName + ", ";
                    if (sDriver.Length > 1)
                        sDriver = sDriver.Substring(0, sDriver.Length - 2);
                    dr["Driver"] = sDriver;

                    float fTripDistance = 0;
                    TimeSpan IdlingTime = TimeSpan.Parse("00:00:00");
                    TimeSpan tsStopInZone = TimeSpan.Parse("00:00:00");
                    TimeSpan tsStopOutZone = TimeSpan.Parse("00:00:00");
                    foreach (TripHeader t in lDailyTrip)
                    {
                        fTripDistance += t.TripDistance;
                        IdlingTime += t.IdlingTime;

                        if (t.ArrivalZoneID == 0)
                            tsStopOutZone += t.StopTime;
                        else
                            tsStopInZone += t.StopTime;
                    }

                    dr["TripDistance"] = fTripDistance;
                    dr["IdlingTime"] = IdlingTime;
                    dr["InZoneStoppedTime"] = tsStopInZone;
                    dr["OutZoneStoppedTime"] = tsStopOutZone;

                    RecordDataTable.Rows.Add(dr);
                }
            }
            return RecordDataTable;
        }
    }
}
