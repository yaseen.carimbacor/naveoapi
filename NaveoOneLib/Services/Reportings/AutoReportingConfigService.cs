using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Reportings;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Services.Audits;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.Users;

namespace NaveoOneLib.Services.Reportings
{
    public class AutoReportingConfigService
    {
        Errors er = new Errors();
        AutoReportingConfig GetACL(DataRow dr)
        {
            AutoReportingConfig ACL = new AutoReportingConfig();
            ACL.ARID = (int)dr["ARID"];
            ACL.TargetID = String.IsNullOrEmpty(dr["TargetID"].ToString()) ? (int?)null : (int)dr["TargetID"];
            ACL.TargetType = dr["TargetType"].ToString();
            ACL.ReportID = String.IsNullOrEmpty(dr["ReportID"].ToString()) ? (int?)null : (int)dr["ReportID"];
            ACL.ReportPeriod = String.IsNullOrEmpty(dr["ReportPeriod"].ToString()) ? (int?)null : (int)dr["ReportPeriod"];
            ACL.TriggerTime = dr["TriggerTime"].ToString();
            ACL.TriggerInterval = dr["TriggerInterval"].ToString();
            ACL.CreatedBy = dr["CreatedBy"].ToString();
            ACL.ReportFormat = dr["ReportFormat"].ToString();
            ACL.ReportType = dr["ReportType"].ToString();
            ACL.ReportPath = dr["ReportPath"].ToString();
            ACL.SaveToDisk = (int)dr["SaveToDisk"];
            ACL.GridXML = dr["GridXML"].ToString();

            return ACL;

        }

        AutoReportingConfig GetACLDetails(DataSet ds)
        {
            AutoReportingConfig ACL = new AutoReportingConfig();


            //dtAutoReportingConfig

            DataTable dtAutoReportingConfig = ds.Tables["dtAutoReportingConfig"];
            DataTable dtAutoReportingConfigDetails = ds.Tables["dtAutoReportingConfigDetails"];

            ACL.ARID = (int)dtAutoReportingConfig.Rows[0]["ARID"];
            ACL.TargetID = String.IsNullOrEmpty(dtAutoReportingConfig.Rows[0]["TargetID"].ToString()) ? (int?)null : (int)dtAutoReportingConfig.Rows[0]["TargetID"];
            ACL.TargetType = dtAutoReportingConfig.Rows[0]["TargetType"].ToString();
            ACL.ReportID = String.IsNullOrEmpty(dtAutoReportingConfig.Rows[0]["ReportID"].ToString()) ? (int?)null : (int)dtAutoReportingConfig.Rows[0]["ReportID"];
            ACL.ReportPeriod = String.IsNullOrEmpty(dtAutoReportingConfig.Rows[0]["ReportPeriod"].ToString()) ? (int?)null : (int)dtAutoReportingConfig.Rows[0]["ReportPeriod"];
            ACL.TriggerTime = dtAutoReportingConfig.Rows[0]["TriggerTime"].ToString();
            ACL.TriggerInterval = dtAutoReportingConfig.Rows[0]["TriggerInterval"].ToString();
            ACL.CreatedBy = dtAutoReportingConfig.Rows[0]["CreatedBy"].ToString();
            ACL.ReportFormat = dtAutoReportingConfig.Rows[0]["ReportFormat"].ToString();
            ACL.ReportType = dtAutoReportingConfig.Rows[0]["ReportType"].ToString();
            ACL.ReportPath = dtAutoReportingConfig.Rows[0]["ReportPath"].ToString();
            ACL.SaveToDisk = (int)dtAutoReportingConfig.Rows[0]["SaveToDisk"];
            ACL.GridXML = dtAutoReportingConfig.Rows[0]["GridXML"].ToString();


             foreach(DataRow dr in dtAutoReportingConfigDetails.Rows)
              {
                AutoReportingConfigDetail AD = new AutoReportingConfigDetail();
                AD.ARDID = (int)dr["ARDID"];
                AD.ARID = (int)dr["ARID"];
                AD.UID = (int)dr["UID"];
                AD.UIDType = dr["UIDType"].ToString();
                AD.UIDCategory = dr["UIDCategory"].ToString();
                AD.sMisc = dr["sMisc"].ToString();
        
                ACL.lDetails.Add(AD);


              }

            return ACL;

        }

        public String sGetAutoReportingConfig(List<int> iParentIDs, String sConnStr)
        {
            String sql = @"
SELECT Distinct arc.* 
    from GFI_FLT_AutoReportingConfig  arc
    inner join GFI_FLT_AutoReportingConfigDetails d on d.ARID = arc.ARID  
    where  (d.UIDType = 'Assets' and d.UIDCategory = 'GMID' and d.uid in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + @"))
	    or (d.UIDType = 'Assets' 
                and d.UIDCategory = 'ID' 
                and d.uid in 
                        (Select Distinct AssetID                   
                            from GFI_FLT_Asset a, GFI_SYS_GroupMatrixAsset m
                            where a.AssetID = m.iID and m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + @")
                        )
			)
        and arc.ReportID < 100    
     order by ARID";
            return sql;
        }

        public DataTable GetAutoReportingConfig(List<Matrix> lMatrix, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            return GetAutoReportingConfig(iParentIDs, sConnStr);
        }
        public DataTable GetAutoReportingConfig(List<int> iParentIDs, String sConnStr)
        {
            String sql = sGetAutoReportingConfig(iParentIDs, sConnStr);
            return new Connection(sConnStr).GetDataDT(sql, sConnStr);
        }
        public AutoReportingConfig GetAutoReportingConfigById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT [ARID]
                                    ,[TargetID]
                                    ,[TargetType]
                                    ,[ReportID]
                                    ,[ReportPeriod]
                                    ,[TriggerTime]
                                    ,[TriggerInterval]
                                    ,[CreatedBy]
                                    ,[ReportFormat]
                                    ,[ReportType]
                                    ,[ReportPath]
                                    ,[SaveToDisk]
                                    ,[GridXML] from GFI_FLT_AutoReportingConfig
                                WHERE ARID = ?
                                order by ARID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetACL(dt.Rows[0]);
        }



        public AutoReportingConfig GetAutoReportingById(String iID, String sConnStr)
        {


            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();


            int illd = Convert.ToInt32(iID);
            String sql = "select * from GFI_FLT_AutoReportingConfig where ARID= ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = illd;
            drSql["sTableName"] = "dtAutoReportingConfig";
            dtSql.Rows.Add(drSql);



             sql = "select * from GFI_FLT_AutoReportingConfigDetails where ARID= ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = illd;
            drSql["sTableName"] = "dtAutoReportingConfigDetails";
            dtSql.Rows.Add(drSql);


            DataSet ds = c.GetDataDS(dtSql, sConnStr);  //
            return GetACLDetails(ds);

        }

        public DataTable GetAutoRprtDetails(string ARID,String sConnStr)
        {
            String sqlString = @"select ad.* from GFI_FLT_AutoReportingConfig a inner join  GFI_FLT_AutoReportingConfigDetails ad on a.ARID = ad.ARID  where ad.UIDType ='Users' and a.ARID = " + ARID; 
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        public List<AutoReportingConfig> GetAutoReportingConfigList(String sConnStr)
        {
            String sqlString = @"SELECT [ARID]
                                  ,[TargetID]
                                  ,[TargetType]
                                  ,[ReportID]
                                  ,[ReportPeriod]
                                  ,[TriggerTime]
                                  ,[TriggerInterval]
                                  ,[CreatedBy]
                                  ,[ReportFormat]
                                  ,[ReportType]
                                  ,[ReportPath]
                                  ,[SaveToDisk]
                                  ,[GridXML] from GFI_FLT_AutoReportingConfig
                                where ReportID not in (110, 111, 112, 113)";

            DataTable dt = new Connection(sConnStr).GetDataDT(sqlString, sConnStr);

            List<AutoReportingConfig> l = new List<AutoReportingConfig>();

            if (dt.Rows.Count == 0)
                return null;
            else
            {
                foreach (DataRow dr in dt.Rows)
                    l.Add(GetACL(dr));

                return l;
            }
        }
        public String sGetARCbyTargetID(String sTargetID)
        {
            String sql = "select * from GFI_FLT_AutoReportingConfig where TargetID = " + sTargetID;
            return sql;
        }
        public String sGetARCDetailsbyTargetID(String sTargetID)
        {
            String sql = @"select d.* from GFI_FLT_AutoReportingConfig h
	                            inner join GFI_FLT_AutoReportingConfigDetails d on h.ARID = d.ARID 
                            where h.TargetID = " + sTargetID;
            return sql;
        }
        public List<AutoReportingConfig> GetListARC(DataSet ds)
        {
            //ds has dtARC, dtARCDetails
            List<AutoReportingConfig> lARC = new List<AutoReportingConfig>();
            foreach (DataRow dr in ds.Tables["dtARC"].Rows)
            {
                AutoReportingConfig acl = GetACL(dr);
                List<AutoReportingConfigDetail> l = new List<AutoReportingConfigDetail>();
                foreach (DataRow drd in ds.Tables["dtARCDetails"].Select("ARID = " + dr["ARID".ToString()]))
                    l.Add(new AutoReportingConfigDetailsService().getARCD(drd));

                acl.lDetails = l;
                lARC.Add(acl);
            }

            return lARC;
        }

        public DataSet PrepareSaveDsForACL(AutoReportingConfig uAutoReportingConfig, int iCtrlIncrement, Boolean bInsert, String sConnStr)
        {
            DataSet ds = new DataSet();
            Connection _connection = new Connection(sConnStr); ;
            DataTable CTRLTBL = _connection.CTRLTBL();
            CTRLTBL.TableName = "CTRLTBL";

            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_AutoReportingConfig";
            if (!dt.Columns.Contains("oprType"))
                dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Columns.Add("ARID", typeof(int));
            dt.Columns.Add("TargetID", typeof(int));
            dt.Columns.Add("TargetType", typeof(String));
            dt.Columns.Add("ReportID", typeof(int));
            dt.Columns.Add("ReportPeriod", typeof(int));
            dt.Columns.Add("TriggerTime", typeof(String));
            dt.Columns.Add("TriggerInterval", typeof(String));
            dt.Columns.Add("CreatedBy", typeof(String));
            dt.Columns.Add("ReportFormat", typeof(String));
            dt.Columns.Add("ReportType", typeof(String));
            dt.Columns.Add("ReportPath", typeof(String));
            dt.Columns.Add("SaveToDisk", typeof(int));
            dt.Columns.Add("GridXML", typeof(String));
            dt.Columns.Add("OPRSeqNo", typeof(int));
            DataRow dr = dt.NewRow();
            dr["ARID"] = uAutoReportingConfig.ARID;
            if (bInsert)
                dr["oprType"] = DataRowState.Added;
            else
                dr["oprType"] = uAutoReportingConfig.oprType;
            dr["TargetID"] = uAutoReportingConfig.TargetID != null ? uAutoReportingConfig.TargetID : (object)DBNull.Value; 
            dr["TargetType"] = uAutoReportingConfig.TargetType;
            dr["ReportID"] = uAutoReportingConfig.ReportID != null ? uAutoReportingConfig.ReportID : (object)DBNull.Value; 
            dr["ReportPeriod"] = uAutoReportingConfig.ReportPeriod != null ? uAutoReportingConfig.ReportPeriod : (object)DBNull.Value; 
            dr["TriggerTime"] = uAutoReportingConfig.TriggerTime;
            dr["TriggerInterval"] = uAutoReportingConfig.TriggerInterval;
            dr["CreatedBy"] = uAutoReportingConfig.CreatedBy;
            dr["ReportFormat"] = uAutoReportingConfig.ReportFormat != null ? uAutoReportingConfig.ReportFormat : String.Empty;
            dr["ReportType"] = uAutoReportingConfig.ReportType;
            dr["ReportPath"] = uAutoReportingConfig.ReportPath;
            dr["SaveToDisk"] = uAutoReportingConfig.SaveToDisk;
            dr["GridXML"] = uAutoReportingConfig.GridXML;
            dr["OPRSeqNo"] = 1 + iCtrlIncrement;
            dt.Rows.Add(dr);
            ds.Merge(dt);

            DataRow drCTRL = CTRLTBL.NewRow();
            drCTRL["SeqNo"] = 1 + iCtrlIncrement;
            drCTRL["TblName"] = dt.TableName;
            drCTRL["CmdType"] = "TABLE";
            drCTRL["KeyFieldName"] = "ARID";
            drCTRL["KeyIsIdentity"] = "TRUE";
            drCTRL["NextIdAction"] = "GET";
            if (uAutoReportingConfig.oprType == DataRowState.Deleted)
                drCTRL["NextIdAction"] = String.Empty;
            else if (uAutoReportingConfig.oprType == DataRowState.Modified)
                drCTRL["NextIdAction"] = String.Empty;
            CTRLTBL.Rows.Add(drCTRL);

            if (uAutoReportingConfig.oprType != DataRowState.Deleted)
            {
                drCTRL = CTRLTBL.NewRow();
                drCTRL["SeqNo"] = 2 + iCtrlIncrement;
                drCTRL["TblName"] = dt.TableName;
                drCTRL["CmdType"] = "TABLE";
                drCTRL["NextIdAction"] = "SET";
                drCTRL["NextIdFieldName"] = "TargetID";
                drCTRL["ParentTblName"] = "GFI_GPS_Rules";
                CTRLTBL.Rows.Add(drCTRL);
            }

            if (uAutoReportingConfig.lDetails.Count != 0)
            {
                DataTable dtARConfigDetails = new myConverter().ListToDataTable<AutoReportingConfigDetail>(uAutoReportingConfig.lDetails);
                dtARConfigDetails.Columns.Add("OPRSeqNo", typeof(int));
                dtARConfigDetails.TableName = "GFI_FLT_AutoReportingConfigDetails";
                foreach (DataRow drOpr in dtARConfigDetails.Rows)
                {
                    drOpr["OPRSeqNo"] = 1 + iCtrlIncrement;
                    if (bInsert)
                        drOpr["oprType"] = DataRowState.Added;
                }
                ds.Merge(dtARConfigDetails);

                drCTRL = CTRLTBL.NewRow();
                drCTRL["SeqNo"] = 3 + iCtrlIncrement;
                drCTRL["TblName"] = dtARConfigDetails.TableName;
                drCTRL["CmdType"] = "TABLE";
                drCTRL["KeyFieldName"] = "ARDID";
                drCTRL["KeyIsIdentity"] = "TRUE";
                drCTRL["NextIdAction"] = "SET";
                drCTRL["NextIdFieldName"] = "ARID";

                drCTRL["ParentTblName"] = dt.TableName;
                CTRLTBL.Rows.Add(drCTRL);
            }


            //if (!bInsert)
            //{
            //    drCTRL["NextIdAction"] = String.Empty;
            //    drCTRL["NextIdValue"] = dtARConfigDetails.Rows[0]["ARID"];
            //}
         

            ds.Tables.Add(CTRLTBL);
            return ds;
        }
        public BaseModel SaveAutoReportingConfig(int UID, AutoReportingConfig uAutoReportingConfig, DataTable dtARConfigDetails, Boolean bInsert, String sConnStr)
        {
            //Valdations           
            //var uniqueCount = LRules
            //.GroupBy(u => u.Struc)
            //.Select(grp => grp.ToList())
            //.ToList().Count();

            //if (uniqueCount >= 3)
            //{
            //    var time = LRules
            //    .Where(u => u.Struc == "Time")
            //   .Select(u => u.StrucValue)
            //   .ToList();

            //    int[] arr = new int[2];
            //    int index = 0;
            //    foreach (object obj in time)
            //    {
            //        arr[index] = (int)Math.Round(Convert.ToDouble(obj), 0);
            //        index += 1;
            //    }
            //    if (arr[0] == arr[1])
            //    {
            //        //MessageBox.Show("Start time and end time should not be same");
            //        return false;
            //    }
            //}
            //else
            //    return false;

            BaseModel baseModel = new BaseModel();
            Boolean bResult = false;

            //String sType = "Assets";
            //if (!bValidateData(dtARConfigDetails, sType, UID, sConnStr))
            //{
            //    baseModel.errorMessage = "No Access to " + sType;
            //    return baseModel;
            //}
            //sType = "Users";
            //if (!bValidateData(dtARConfigDetails, sType, UID, sConnStr))
            //{
            //    baseModel.errorMessage = "No Access to " + sType;
            //    return baseModel;
            //}
            //sType = "Rules";
            //if (!bValidateData(dtARConfigDetails, sType, UID, sConnStr))
            //{
            //    baseModel.errorMessage = "No Access to " + sType;
            //    return baseModel;
            //}
            //sType = "Drivers";
            //if (!bValidateData(dtARConfigDetails, sType, UID, sConnStr))
            //{
            //    baseModel.errorMessage = "No Access to " + sType;
            //    return baseModel;
            //}

            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_AutoReportingConfig";
            if (!dt.Columns.Contains("oprType"))
                dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Columns.Add("ARID", typeof(int));
            dt.Columns.Add("TargetID", typeof(int));
            dt.Columns.Add("TargetType", typeof(string));
            dt.Columns.Add("ReportID", typeof(String));
            dt.Columns.Add("ReportPeriod", typeof(int));
            dt.Columns.Add("TriggerTime", typeof(String));
            dt.Columns.Add("TriggerInterval", typeof(String));
            dt.Columns.Add("CreatedBy", typeof(String));
            dt.Columns.Add("ReportFormat", typeof(String));
            dt.Columns.Add("ReportType", typeof(String));
            dt.Columns.Add("ReportPath", typeof(String));
            dt.Columns.Add("SaveToDisk", typeof(int));
            dt.Columns.Add("GridXML", typeof(String));

            DataRow dr = dt.NewRow();
            if (bInsert)
                dr["oprType"] = DataRowState.Added;
            else
            {
                dr["ARID"] = uAutoReportingConfig.ARID;
                dr["oprType"] = DataRowState.Modified;
            }

            if (uAutoReportingConfig.TargetID.HasValue)
                dr["TargetID"] = uAutoReportingConfig.TargetID;

            dr["TargetType"] = uAutoReportingConfig.TargetType;

            if (uAutoReportingConfig.ReportID.HasValue)
                dr["ReportID"] = uAutoReportingConfig.ReportID;

            if (uAutoReportingConfig.ReportPeriod.HasValue)
                dr["ReportPeriod"] = uAutoReportingConfig.ReportPeriod;

            dr["TriggerTime"] = uAutoReportingConfig.TriggerTime;
            dr["TriggerInterval"] = uAutoReportingConfig.TriggerInterval;

            dr["CreatedBy"] = uAutoReportingConfig.CreatedBy;

            if (!(string.IsNullOrEmpty(uAutoReportingConfig.ReportFormat)))
                dr["ReportFormat"] = uAutoReportingConfig.ReportFormat;

            dr["ReportType"] = uAutoReportingConfig.ReportType;
            dr["ReportPath"] = uAutoReportingConfig.ReportPath;
            dr["SaveToDisk"] = uAutoReportingConfig.SaveToDisk;
            dr["GridXML"] = uAutoReportingConfig.GridXML;

            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr);
            DataSet ds = new DataSet();

            DataTable CTRLTBL = _connection.CTRLTBL();
            CTRLTBL.TableName = "CTRLTBL";

            DataRow drCTRL = null;
            drCTRL = CTRLTBL.NewRow();
            drCTRL["SeqNo"] = 1;
            drCTRL["TblName"] = dt.TableName;
            drCTRL["CmdType"] = "TABLE";
            drCTRL["KeyFieldName"] = "ARID";
            drCTRL["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCTRL["NextIdAction"] = "GET";
            else
            {
                drCTRL["NextIdAction"] = String.Empty;
                drCTRL["NextIdValue"] = uAutoReportingConfig.ARID;
            }

            CTRLTBL.Rows.Add(drCTRL);

            drCTRL = CTRLTBL.NewRow();
            drCTRL["SeqNo"] = 2;
            drCTRL["TblName"] = "GFI_FLT_AutoReportingConfigDetails";
            drCTRL["CmdType"] = "TABLE";
            drCTRL["KeyFieldName"] = "ARDID";
            drCTRL["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCTRL["NextIdAction"] = "SET";
            else
            {
                drCTRL["NextIdAction"] = "GET";
                drCTRL["NextIdValue"] = uAutoReportingConfig.ARID;
            }

            drCTRL["NextIdFieldName"] = "ARID";
            drCTRL["ParentTblName"] = dt.TableName;
            CTRLTBL.Rows.Add(drCTRL);

            //--- Process Data ---

            ds.Tables.Add(CTRLTBL);
            ds.Merge(dt);
            ds.Merge(dtARConfigDetails);

            DataSet dsResults = _connection.ProcessData(ds, sConnStr);

            DataTable dtCtrl = dsResults.Tables["CTRLTBL"];

            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                {
                    baseModel.dbResult = dsResults;
                    baseModel.dbResult.Tables["CTRLTBL"].TableName = "ResultCtrlTbl";
                    baseModel.dbResult.Merge(ds);
                    bResult = false;
                }
                else
                    bResult = true;
            }

            baseModel.data = bResult;
            return baseModel;
        }
        Boolean bValidateGMatrix(int UID, string sConnStr, DataRow[] drCat)
        {
            
            foreach (DataRow r in drCat)
            {
                DataTable dtGM = new NaveoOneLib.WebSpecifics.LibData().dtGMMatrix(UID, sConnStr);

                DataRow[] x = dtGM.Select("GMID = " + r["UID"]);
                if (x.Length == 0)
                    return false;
            }

            return true;
        }
        Boolean bValidateData1(DataTable dtARConfigDetails, String sType, int UID, String sConnStr)
        {
            Boolean b = false;
            DataRow[] drType = dtARConfigDetails.Select("UIDType = '" + sType + "'");
            if (drType.Length != 0)
            {
                switch (sType)
                {
                    case "Assets":
                        foreach (DataRow r in drType)
                        {
                            if (r["UIDCategory"].ToString() == "GMID")
                            {
                                Boolean bGm = bValidateGMatrix(UID, sConnStr, drType);
                                if (!bGm)
                                    return bGm;

                            }
                            else if (r["UIDCategory"].ToString() == "ID")
                            {
                                DataTable dtGMAsset = new NaveoOneLib.WebSpecifics.LibData().dtGMAsset(UID, sConnStr);
                                DataRow[] x = dtGMAsset.Select("StrucID = " + r["UID"]);
                                if (x.Length == 0)
                                    return b;
                            }


                        }
                        return true;

                    case "Users":
                        foreach (DataRow r in drType)
                        {
                            if (r["UIDCategory"].ToString() == "GMID")
                            {
                                Boolean bGm = bValidateGMatrix(UID, sConnStr, drType);
                                if (!bGm)
                                    return bGm;
                            }
                            else if (r["UIDCategory"].ToString() == "ID")
                            {
                                DataTable dtGMUser = new NaveoOneLib.WebSpecifics.LibData().dtGMUser(UID, false, sConnStr);
                                DataRow[] x = dtGMUser.Select("StrucID = " + r["UID"]);
                                if (x.Length == 0)
                                    return b;
                            }


                        }
                        return true;

                    case "Rules":
                        foreach (DataRow r in drType)
                        {
                            if (r["UIDCategory"].ToString() == "GMID")
                            {
                                Boolean bGm = bValidateGMatrix(UID, sConnStr, drType);
                                if (!bGm)
                                    return bGm;
                            }
                            else if (r["UIDCategory"].ToString() == "ID")
                            {
                                DataTable dtGMRule = new NaveoOneLib.WebSpecifics.LibData().dtGMRule(UID, sConnStr);
                                DataRow[] x = dtGMRule.Select("StrucID = " + r["UID"]);
                                if (x.Length == 0)
                                    return b;

                            }


                        }
                        return true;

                    case "Drivers":
                        foreach (DataRow r in drType)
                        {
                            if (r["UIDCategory"].ToString() == "GMID")
                            {
                                Boolean bGm = bValidateGMatrix(UID, sConnStr, drType);
                                if (!bGm)
                                    return bGm;
                            }
                            else if (r["UIDCategory"].ToString() == "ID")
                            {
                                DataTable dtGMDriver = new NaveoOneLib.WebSpecifics.LibData().dtGMDriver(UID, sConnStr);
                                DataRow[] x = dtGMDriver.Select("StrucID = " + r["UID"]);
                                if (x.Length == 0)
                                    return b;

                            }
                        }
                        return true;
                }
            }

            return true;

            //DataRow[] drCat = dtARConfigDetails.Select("UIDType = '" + sType + "' and UIDCategory = 'ID'");
            //if (drCat.Length > 0)
            //{
            //    DataTable dtGMAsset = new NaveoOneLib.WebSpecifics.LibData().dtGMAsset(UID, sConnStr);
            //    foreach (DataRow r in drCat)
            //    {
            //        DataRow[] x = dtGMAsset.Select("StrucID = " + r["UID"]);
            //        if (x.Length == 0)
            //            return b;
            //    }
            //}

            //drCat = dtARConfigDetails.Select("UIDType = '" + sType + "' and UIDCategory = 'GMID'");
            //if (drCat.Length > 0)
            //{
            //    DataTable dtGM = new NaveoOneLib.WebSpecifics.LibData().dtGMMatrix(UID, sConnStr);
            //    foreach (DataRow r in drCat)
            //    {
            //        DataRow[] x = dtGM.Select("GMID = " + r["UID"]);
            //        if (x.Length == 0)
            //            return b;
            //    }
            //}

        }
        public BaseModel DeleteARC(int ARID, int UID, String sConnStr)
        {
            BaseModel baseModel = new BaseModel();
            Boolean bResult = false;

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = @"delete from GFI_SYS_Notification where ARID = " + ARID.ToString();
            sql += "\r\n";
            sql += "delete from GFI_FLT_AutoReportingConfig where ARID = " + ARID.ToString();

            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            DataTable dtAudit = new AuditService().LogTran(3, "ARC", "Deleted ARID : " + ARID.ToString(), UID, ARID);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "CallerFunction";
            drCtrl["ParentTblName"] = "GFI_FLT_AutoReportingConfig";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                {
                    baseModel.dbResult = dsResult;
                    baseModel.dbResult.Tables["CTRLTBL"].TableName = "ResultCtrlTbl";
                    baseModel.dbResult.Merge(dsProcess);
                    bResult = false;
                }
                else
                    bResult = true;
            }
            else
                bResult = false;

            baseModel.data = bResult;
            return baseModel;
        }



        Boolean bValidateData(DataTable dtARConfigDetails, String sType, int UID, String sConnStr)
        {
            Boolean b = false;
            DataRow[] drType = dtARConfigDetails.Select("UIDType = '" + sType + "'");
            if (drType.Length == 0)
            {
                switch (sType)
                {
                    case "Assets":
                        return b;

                    case "Users":
                        return b;
                }
            }


            switch (sType)
            {
               
                #region Assets
                case "Assets":
                    DataRow[] drCat = dtARConfigDetails.Select("UIDType = '" + sType + "' and UIDCategory = 'ID'");
                    if (drCat.Length > 0)
                    {
                        DataTable dtGMAsset = new NaveoOneLib.WebSpecifics.LibData().dtGMAsset(UID, sConnStr);
                        foreach (DataRow r in drCat)
                        {
                            DataRow[] x = dtGMAsset.Select("StrucID = " + r["UID"]);
                            if (x.Length == 0)
                                return b;
                        }
                    }

                    drCat = dtARConfigDetails.Select("UIDType = '" + sType + "' and UIDCategory = 'GMID'");
                    if (drCat.Length > 0)
                    {
                    //    DataTable dtGM = new NaveoOneLib.WebSpecifics.LibData().dtGMMatrix(UID, sConnStr);
                    //    foreach (DataRow r in drCat)
                    //    {
                    //        DataRow[] x = dtGM.Select("GMID = " + r["UID"]);
                    //        if (x.Length == 0)
                    //            return b;
                    //    }

                        Boolean bGm = bValidateGMatrix(UID, sConnStr, drCat);
                        if (!bGm)
                            return bGm;
                    }
                    break;
                #endregion

                #region Users
                case "Users":
                    DataRow[] drCat1 = dtARConfigDetails.Select("UIDType = '" + sType + "' and UIDCategory = 'ID'");
                    if (drCat1.Length > 0)
                    {
                        DataTable dtGMUser = new NaveoOneLib.WebSpecifics.LibData().dtGMUser(UID, false, sConnStr);
                        foreach (DataRow r in drCat1)
                        {
                            DataRow[] x = dtGMUser.Select("StrucID = " + r["UID"]);
                            if (x.Length == 0)
                                return b;
                        }
                    }

                    drCat1 = dtARConfigDetails.Select("UIDType = '" + sType + "' and UIDCategory = 'GMID'");
                    if (drCat1.Length > 0)
                    {
                        Boolean bGm = bValidateGMatrix(UID, sConnStr, drCat1);
                        if (!bGm)
                            return bGm;
                    }
                    break;
                #endregion

                #region Rules
                  case "Rules":
                    DataRow[] drCat2 = dtARConfigDetails.Select("UIDType = '" + sType + "' and UIDCategory = 'ID'");
                    if (drCat2.Length > 0)
                    {
                        DataTable dtGMRule = new NaveoOneLib.WebSpecifics.LibData().dtGMRule(UID, sConnStr);
                        foreach (DataRow r in drCat2)
                        {
                            DataRow[] x = dtGMRule.Select("StrucID = " + r["UID"]);
                            if (x.Length == 0)
                                return b;
                        }
                    }

                    drCat2 = dtARConfigDetails.Select("UIDType = '" + sType + "' and UIDCategory = 'GMID'");
                    if (drCat2.Length > 0)
                    {
                        Boolean bGm = bValidateGMatrix(UID, sConnStr, drCat2);
                        if (!bGm)
                            return bGm;
                    }
                    break;
                #endregion

                #region Drivers
                case "Drivers":
                    DataRow[] drCat3 = dtARConfigDetails.Select("UIDType = '" + sType + "' and UIDCategory = 'ID'");
                    if (drCat3.Length > 0)
                    {
                        DataTable dtGMDriver = new NaveoOneLib.WebSpecifics.LibData().dtGMDriver(UID, sConnStr);
                        foreach (DataRow r in drCat3)
                        {
                            DataRow[] x = dtGMDriver.Select("StrucID = " + r["UID"]);
                            if (x.Length == 0)
                                return b;
                        }
                    }

                    drCat3 = dtARConfigDetails.Select("UIDType = '" + sType + "' and UIDCategory = 'GMID'");
                    if (drCat3.Length > 0)
                    {
                        Boolean bGm = bValidateGMatrix(UID, sConnStr, drCat3);
                        if (!bGm)
                            return bGm;
                    }
                    break;
                    #endregion
            }
            return true;
        }

        public string getServerName()
        {

            string sServerName = System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            User.NaveoDatabases myEnum;
            //Getting only the enum part
            sServerName = sServerName.Replace(".naveo.mu", String.Empty);
            //sServerName = sServerName.Replace(":44393", String.Empty);
            sServerName = sServerName.Replace("https://", String.Empty);
            sServerName = sServerName.Replace("http://", String.Empty);
            if (Enum.TryParse(sServerName, true, out myEnum))
                sServerName = sServerName;
            else
                sServerName = sServerName;



            sServerName = sServerName.ToUpper();


            return sServerName;
        }

        public DataTable GetPart1()
        {

            BaseModel bm = new BaseModel();

            string sServerName = "KIDS";//getServerName();

            String sql = @"select vehMain.URI as MaintId 
,custVeh.FMSAssetId AssetId 
,custVeh.Number AssetName 
,vehMainCat.Description Maintenance_Type
,vehMainType.Description 
,vehMain.StartDate 
,vehMain.EndDate
,vehMaintStatus.Description AssetStatus
,vehMain.VATAmount VATAmount
,vehMain.TotalCost TotalCost
,(vehMain.TotalCost - vehMain.VATAmount) CostExVAT
from CustomerVehicle custVeh  
inner join GFI_AMM_VehicleMaintenance vehMain  on custVeh.Id = vehMain.AssetId
inner join GFI_AMM_VehicleMaintTypes vehMainType on vehMain.MaintTypeId_cbo = vehMainType.MaintTypeId
inner join GFI_AMM_VehicleMaintCat  vehMainCat on vehMainType.MaintCatId_cbo = vehMainCat.MaintCatId
inner join GFI_AMM_VehicleMaintStatus vehMaintStatus on vehMain.MaintStatusId_cbo = vehMaintStatus.MaintStatusId
inner join FleetServer fs on custVeh.FleetServerId = fs.Id
where fs.Name = '" + sServerName + "' and ( vehMain.MaintStatusId_cbo = 5 or vehMain.MaintStatusId_cbo = 6)";

            DataTable dt1 = new  Connection("MaintDB").GetDataDT(sql, "MaintDB");


            return dt1;


        }

        public DataTable GetPart2()
        {

            BaseModel bm = new BaseModel();

            string sServerName = "KIDS";//getServerName();

            String sql = @"select f.Id as MaintId 
,custVeh.FMSAssetId AssetId 
,custVeh.Number AssetName 
,f.FuelType Maintenance_Type
,'FuelUsage' Description
,f.RefuelDate  StartDate
,f.RefuelDate EndDate
,'Completed' AssetStatus
,(f.AmountInclVAT - f.AmountInclVAT) / 1.15 FuelVATAmount
,f.AmountInclVAT / 1.15  FuelTotalCost
,f.AmountInclVAT FuelCostExVAT from CustomerVehicle custVeh
inner join fuel f on custVeh.Number = f.RegistrationNo
inner join FleetServer fs on custVeh.FleetServerId = fs.Id
where fs.Name =  '" + sServerName + "'  and custVeh.Remarks = 'Assets' and f.IsFlagged =0;";

            DataTable dt1 = new Connection("MaintDB").GetDataDT(sql, "MaintDB");


            return dt1;


        }
    }
}