﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telerik.WinControls.UI.Export.SpreadExport;
using Telerik.WinControls.UI.Export.ExcelML;
using Telerik.WinControls.UI.Export;
using Telerik.WinControls.UI;
using Telerik.Windows.Documents.Spreadsheet.Model;
using System.Drawing;
using NaveoOneLib.Common;
using Telerik.WinControls.UI.Export.HTML;
using System.Text.RegularExpressions;

namespace NaveoOneLib.UILogics
{
    public static partial class GridExporter
    {
        static String _Title;
        static String _PeriodFrom;
        static String _PeriodTo;
        static RadGridView gv = new RadGridView();

        public static String ProcessData(String sSheetName, String dtFrom, String dtTo, String ReportType, RadGridView myRG)
        {
            String sFileName = String.Empty;
            InitVariables(sSheetName, dtFrom, dtTo, myRG, true);

            gv.BindingContext = new System.Windows.Forms.BindingContext();
            gv.LoadElementTree();
            if (gv.RowCount < 1)
                return sFileName;

            sFileName = String.Empty;
            switch (ReportType)
            {
                case "EXCEL":
                    sFileName = Globals.TempPath(String.Empty) + Guid.NewGuid() + sSheetName + ".xls";
                    RADGrid_xlsExport(sSheetName, _PeriodFrom, _PeriodTo, sFileName, myRG);
                    break;

                case "CSV":
                    sFileName = Globals.TempPath(String.Empty) + Guid.NewGuid() + sSheetName + ".csv";
                    RADGrid_csvExport(sSheetName, _PeriodFrom, _PeriodTo, sFileName, myRG);
                    break;

                case "PDF":
                    sFileName = Globals.TempPath(String.Empty) + Guid.NewGuid() + sSheetName + ".pdf";
                    RADGrid_pdfExport(sSheetName, _PeriodFrom, _PeriodTo, sFileName, myRG);
                    break;

                case "TEXT":
                    sFileName = Globals.TempPath(String.Empty) + Guid.NewGuid() + sSheetName + ".txt";
                    RADGrid_txtExport(sSheetName, _PeriodFrom, _PeriodTo, sFileName, myRG);
                    break;
            }

            return sFileName;
        }
        static void InitVariables(String sSheetName, String dtFrom, String dtTo, RadGridView myRG, Boolean bInitTime)
        {
            _Title = sSheetName;
            _PeriodFrom = dtFrom;
            _PeriodTo = dtTo;
            gv = myRG;

            if (bInitTime)
            {
                if (gv.Columns.Contains("From"))
                {
                    gv.Columns["From"].ExcelExportType = DisplayFormatType.Custom;
                    gv.Columns["From"].ExcelExportFormatString = "dd/mm/yyyy hh:mm:ss";
                }

                if (gv.Columns.Contains("To"))
                {
                    gv.Columns["To"].ExcelExportType = DisplayFormatType.Custom;
                    gv.Columns["To"].ExcelExportFormatString = "dd/mm/yyyy hh:mm:ss";
                }

                if (gv.Columns.Contains("TripStartTime"))
                {
                    gv.Columns["TripStartTime"].ExcelExportType = DisplayFormatType.Custom;
                    gv.Columns["TripStartTime"].ExcelExportFormatString = "dd/mm/yyyy hh:mm:ss";
                }

                if (gv.Columns.Contains("TripEndTime"))
                {
                    gv.Columns["TripEndTime"].ExcelExportType = DisplayFormatType.Custom;
                    gv.Columns["TripEndTime"].ExcelExportFormatString = "dd/mm/yyyy hh:mm:ss";
                }
            }
        }
        public static void RADGrid_xlsExport(String sSheetName, String dtFrom, String dtTo, String sFileName, RadGridView myRG)
        {
            InitVariables(sSheetName, dtFrom, dtTo, myRG, true);
            //Following should be < 31 char + no special char
            Regex illegalInFileName = new Regex(@"[\\/:*?""<>|]");
            String sTitle = illegalInFileName.Replace(_Title, "");
            if (sTitle.Length > 31)
                sTitle = sTitle.Substring(0, 30);

            ExportToExcelML exporter = new ExportToExcelML(gv);
            exporter.ExcelCellFormatting += new Telerik.WinControls.UI.Export.ExcelML.ExcelCellFormattingEventHandler(exporter_ExcelCellFormatting);
            exporter.ExcelTableCreated += new ExcelTableCreatedEventHandler(exporter_ExcelTableCreated);
            exporter.HiddenColumnOption = Telerik.WinControls.UI.Export.HiddenOption.DoNotExport;
            exporter.ExportVisualSettings = true;
            exporter.SheetMaxRows = ExcelMaxRows._1048576;
            exporter.SheetName = sTitle;
            exporter.RunExport(sFileName);
        }
        public static void RADGrid_xlsxExport(String sSheetName, String dtFrom, String dtTo, String sFileName, RadGridView myRG)
        {
            InitVariables(sSheetName, dtFrom, dtTo, myRG, false);
            //Following should be < 31 char + no special char
            Regex illegalInFileName = new Regex(@"[\\/:*?""<>|]");
            String sTitle = illegalInFileName.Replace(_Title, "");
            if (sTitle.Length > 31)
                sTitle = sTitle.Substring(0, 30);

            SpreadExport spreadExporter = new SpreadExport(gv);
            
            spreadExporter.CellFormatting += new Telerik.WinControls.UI.Export.SpreadExport.CellFormattingEventHandler(spreadExporter_CellFormatting);
            spreadExporter.WorkbookCreated += new Telerik.WinControls.UI.Export.SpreadExport.WorkbookCreatedEventHandler(spreadExporter_WorkbookCreated);
            spreadExporter.ExportFormat = Telerik.WinControls.UI.Export.SpreadExport.SpreadExportFormat.Xlsx;
            spreadExporter.ExportVisualSettings = true;
            spreadExporter.PagingExportOption = Telerik.WinControls.UI.Export.PagingExportOption.AllPages;
            spreadExporter.SheetName = sTitle.ToString();
            if (Globals.DeleteFile(sFileName))
                spreadExporter.RunExport(sFileName);
        }
        public static void RADGrid_csvExport(String sSheetName, String dtFrom, String dtTo, String sFileName, RadGridView myRG)
        {
            InitVariables(sSheetName, dtFrom, dtTo, myRG, true);

            SpreadExport spreadExporterCSV = new SpreadExport(gv);
            //spreadExporterCSV.CellFormatting += new Telerik.WinControls.UI.Export.SpreadExport.CellFormattingEventHandler(spreadExporter_CellFormatting);
            spreadExporterCSV.WorkbookCreated += new Telerik.WinControls.UI.Export.SpreadExport.WorkbookCreatedEventHandler(spreadExporterCSV_WorkbookCreated);
            spreadExporterCSV.ExportFormat = Telerik.WinControls.UI.Export.SpreadExport.SpreadExportFormat.Csv;
            spreadExporterCSV.ExportVisualSettings = true;
            spreadExporterCSV.PagingExportOption = Telerik.WinControls.UI.Export.PagingExportOption.AllPages;
            spreadExporterCSV.SheetName = _Title.ToString();
            spreadExporterCSV.RunExport(sFileName);
        }
        public static void RADGrid_pdfExport(String sSheetName, String dtFrom, String dtTo, String sFileName, RadGridView myRG)
        {
            InitVariables(sSheetName, dtFrom, dtTo, myRG, false);

            SpreadExport spreadExporterPDF = new SpreadExport(gv);
            spreadExporterPDF.CellFormatting += new Telerik.WinControls.UI.Export.SpreadExport.CellFormattingEventHandler(spreadExporter_CellFormatting);
            spreadExporterPDF.WorkbookCreated += new Telerik.WinControls.UI.Export.SpreadExport.WorkbookCreatedEventHandler(spreadExporter_WorkbookCreated);
            spreadExporterPDF.ExportFormat = Telerik.WinControls.UI.Export.SpreadExport.SpreadExportFormat.Pdf;
            spreadExporterPDF.ExportVisualSettings = true;
            spreadExporterPDF.PagingExportOption = Telerik.WinControls.UI.Export.PagingExportOption.AllPages;
            spreadExporterPDF.SheetName = _Title.ToString();
            spreadExporterPDF.RunExport(sFileName);
        }
        public static void RADGrid_txtExport(String sSheetName, String dtFrom, String dtTo, String sFileName, RadGridView myRG)
        {
            InitVariables(sSheetName, dtFrom, dtTo, myRG, true);

            SpreadExport spreadExporter = new SpreadExport(gv);
            spreadExporter.CellFormatting += new Telerik.WinControls.UI.Export.SpreadExport.CellFormattingEventHandler(spreadExporter_CellFormatting);
            spreadExporter.WorkbookCreated += new Telerik.WinControls.UI.Export.SpreadExport.WorkbookCreatedEventHandler(spreadExporterCSV_WorkbookCreated);
            spreadExporter.ExportFormat = Telerik.WinControls.UI.Export.SpreadExport.SpreadExportFormat.Txt;
            spreadExporter.ExportVisualSettings = true;
            spreadExporter.PagingExportOption = Telerik.WinControls.UI.Export.PagingExportOption.AllPages;
            spreadExporter.SheetName = _Title.ToString();
            spreadExporter.RunExport(sFileName);
        }
        public static void RADGrid_htmlExport(String sSheetName, String dtFrom, String dtTo, String sFileName, RadGridView myRG)
        {
            InitVariables(sSheetName, dtFrom, dtTo, myRG, true);

            ExportToHTML exporter = new ExportToHTML(gv);
            exporter.HTMLCellFormatting += new HTMLCellFormattingEventHandler(exporter_HTMLCellFormatting);
            exporter.HTMLTableCaptionFormatting += new HTMLTableCaptionFormattingEventHandler(exporter_HTMLTableCaptionFormatting);
            exporter.FileExtension = "";
            exporter.HiddenColumnOption = Telerik.WinControls.UI.Export.HiddenOption.DoNotExport;
            exporter.FitWidthSize = 1000;
            exporter.AutoSizeColumns = true;
            exporter.ExportVisualSettings = true;
            exporter.TableCaption = "Table";
            exporter.RunExport(sFileName);
        }

        static void exporter_ExcelTableCreated(object sender, ExcelTableCreatedEventArgs e)
        {
            SingleStyleElement style = ((ExportToExcelML)sender).AddCustomExcelRow(e.ExcelTableElement, 16, _Title.ToString());
            style.FontStyle.Bold = true;
            style.FontStyle.FontName = "Rockwell";//"Calibri";
            style.FontStyle.Size = 13;
            style.FontStyle.Color = Color.White;
            style.InteriorStyle.Color = Color.Blue;
            style.InteriorStyle.Pattern = InteriorPatternType.Solid;
            style.AlignmentElement.HorizontalAlignment = HorizontalAlignmentType.Left;
            style.AlignmentElement.VerticalAlignment = VerticalAlignmentType.Center;

            if (_PeriodFrom == null)
                _PeriodFrom = String.Empty;
            else
            {
                SingleStyleElement style1 = ((ExportToExcelML)sender).AddCustomExcelRow(e.ExcelTableElement, 16, "From: " + _PeriodFrom);
                style1.FontStyle.Bold = true;
                style1.FontStyle.FontName = "Rockwell";//"Calibri";
                style1.FontStyle.Size = 13;
                style1.FontStyle.Color = Color.Black;
                style1.InteriorStyle.Color = Color.White;
                style1.InteriorStyle.Pattern = InteriorPatternType.Solid;
                style1.AlignmentElement.HorizontalAlignment = HorizontalAlignmentType.Left;
                style1.AlignmentElement.VerticalAlignment = VerticalAlignmentType.Center;
            }
            if (_PeriodTo == null)
                _PeriodTo = String.Empty;
            else
            {
                SingleStyleElement style2 = ((ExportToExcelML)sender).AddCustomExcelRow(e.ExcelTableElement, 16, "To: " + _PeriodTo);
                style2.FontStyle.Bold = true;
                style2.FontStyle.FontName = "Rockwell";
                style2.FontStyle.Size = 13;
                style2.FontStyle.Color = Color.Black;
                style2.InteriorStyle.Color = Color.White;
                style2.InteriorStyle.Pattern = InteriorPatternType.Solid;
                style2.AlignmentElement.HorizontalAlignment = HorizontalAlignmentType.Left;
                style2.AlignmentElement.VerticalAlignment = VerticalAlignmentType.Center;
            }
        }
        static void exporter_ExcelCellFormatting(object sender, Telerik.WinControls.UI.Export.ExcelML.ExcelCellFormattingEventArgs e)
        {
            if (e.GridRowInfoType == typeof(GridViewTableHeaderRowInfo))
            {
                if (e.GridCellInfo.RowInfo.HierarchyLevel == 0)
                {
                    e.ExcelStyleElement.FontStyle.Color = Color.Red;
                    e.ExcelStyleElement.FontStyle.Bold = true;
                }
                BorderStyles border = new BorderStyles();
                border.Color = Color.Pink;
                border.Weight = 2;
                border.LineStyle = LineStyle.Continuous;
                border.PositionType = PositionType.Bottom;

                e.ExcelStyleElement.Borders.Add(border);
                e.ExcelStyleElement.AlignmentElement.WrapText = false;
            }
        }

        static void spreadExporter_WorkbookCreated(object sender, Telerik.WinControls.UI.Export.SpreadExport.WorkbookCreatedEventArgs e)
        {
            Worksheet worksheet1 = (Worksheet)e.Workbook.Sheets[0];
            worksheet1.Columns[worksheet1.UsedCellRange].AutoFitWidth();

            foreach (GridViewDataColumn column in gv.Columns)
            {
                if (column.DataType == typeof(DateTime))
                {
                    CellSelection selection = worksheet1.Cells[0, column.Index, gv.Rows.Count, column.Index];
                    selection.SetFormat(new CellValueFormat("dd/mm/yyyy hh:mm:ss"));
                }
            }

            //Add Headers
            PatternFill solidPatternFill = new PatternFill(PatternType.Solid, System.Windows.Media.Colors.Blue, System.Windows.Media.Colors.Transparent);
            PatternFill solidPatternFill1 = new PatternFill(PatternType.Solid, System.Windows.Media.Colors.White, System.Windows.Media.Colors.Transparent);
            CellValueFormat textFormat = new CellValueFormat("@");

            Telerik.Windows.Documents.Spreadsheet.Model.Worksheet worksheet = (Telerik.Windows.Documents.Spreadsheet.Model.Worksheet)e.Workbook.Sheets[0];
            worksheet.Columns[worksheet.UsedCellRange].AutoFitWidth();

            CellRange range = new CellRange(0, 0, 0, gv.Columns.Count - 1);
            CellSelection header = worksheet.Cells[range];

            if (header.CanInsertOrRemove(range, ShiftType.Down))
                header.Insert(InsertShiftType.Down);

            header.Merge();
            header.SetFormat(textFormat);
            header.SetHorizontalAlignment(Telerik.Windows.Documents.Spreadsheet.Model.RadHorizontalAlignment.Left);
            header.SetVerticalAlignment(Telerik.Windows.Documents.Spreadsheet.Model.RadVerticalAlignment.Center);
            header.SetFontFamily(new ThemableFontFamily("Rockwell"));
            //header.SetFontSize(18);
            header.SetFill(solidPatternFill);
            header.SetValue(_Title.ToString());
            header.SetIsBold(true);
            header.SetForeColor(new ThemableColor(System.Windows.Media.Colors.White));

            //2nd Row Header     
            if (!string.IsNullOrEmpty(_PeriodFrom.ToString()))
            {
                CellRange range1 = new CellRange(1, 0, 1, gv.Columns.Count - 1);
                CellSelection header1 = worksheet.Cells[range1];

                if (header1.CanInsertOrRemove(range1, ShiftType.Down))
                    header1.Insert(InsertShiftType.Down);

                header1.Merge();
                header1.SetFormat(textFormat);
                header1.SetHorizontalAlignment(Telerik.Windows.Documents.Spreadsheet.Model.RadHorizontalAlignment.Left);
                header1.SetVerticalAlignment(Telerik.Windows.Documents.Spreadsheet.Model.RadVerticalAlignment.Center);
                header1.SetFontFamily(new ThemableFontFamily("Rockwell"));
                //header1.SetFontSize(11);
                header1.SetFill(solidPatternFill1);
                header1.SetValue("From:" + _PeriodFrom.ToString());
                header.SetIsBold(true);
                header1.SetForeColor(new ThemableColor(System.Windows.Media.Colors.Black));
            }

            //3nd Row Header  
            if (!string.IsNullOrEmpty(_PeriodTo.ToString()))
            {
                         
                CellRange range2 = new CellRange(2, 0, 2, gv.Columns.Count - 1);
                CellSelection header2 = worksheet.Cells[range2];

                if (header2.CanInsertOrRemove(range2, ShiftType.Down))
                    header2.Insert(InsertShiftType.Down);

                header2.Merge();
                header2.SetFormat(textFormat);
                header2.SetHorizontalAlignment(Telerik.Windows.Documents.Spreadsheet.Model.RadHorizontalAlignment.Left);
                header2.SetVerticalAlignment(Telerik.Windows.Documents.Spreadsheet.Model.RadVerticalAlignment.Center);
                header2.SetFontFamily(new ThemableFontFamily("Rockwell"));
                //header2.SetFontSize(11);
                header2.SetFill(solidPatternFill1);
                header2.SetValue("To:" + _PeriodTo.ToString());
                header2.SetIsBold(true);
                header2.SetForeColor(new ThemableColor(System.Windows.Media.Colors.Black));
            }

            ///Set the height
            //worksheet.Rows[0].SetHeight(new RowHeight(20, true));
            //worksheet.Rows[1].SetHeight(new RowHeight(13, true));
            //worksheet.Rows[2].SetHeight(new RowHeight(13, true));


            //Fit column into pages
            //Worksheet worksheet2 = (Worksheet)e.Workbook.Sheets[0];
            //double pageWidth = worksheet2.WorksheetPageSetup.PageSize.Width - (worksheet2.WorksheetPageSetup.Margins.Left + worksheet2.WorksheetPageSetup.Margins.Right);
            //double gridColumnsWidth = 0;

            //for (int colIndex = 0; colIndex < worksheet2.UsedCellRange.ColumnCount; colIndex++)
            //{
            //    ColumnWidth colWidth = worksheet2.Columns[colIndex].GetWidth().Value;
            //    gridColumnsWidth += colWidth.Value;
            //}
            //worksheet2.WorksheetPageSetup.ScaleFactor = new System.Windows.Size(pageWidth / (gridColumnsWidth + 1), 1);
        }
        static void spreadExporter_CellFormatting(object sender, Telerik.WinControls.UI.Export.SpreadExport.CellFormattingEventArgs e)
        {
            if (e.GridRowInfoType == typeof(GridViewTableHeaderRowInfo))
            {
                e.CellStyleInfo.Underline = true;

                if (e.GridCellInfo.RowInfo.HierarchyLevel == 0)
                {
                    e.CellStyleInfo.BackColor = Color.DeepSkyBlue;
                    e.CellStyleInfo.ForeColor = Color.Black;
                }
                else if (e.GridCellInfo.RowInfo.HierarchyLevel == 1)
                {
                    e.CellStyleInfo.BackColor = Color.LightSkyBlue;
                }
            }

            if (e.GridRowInfoType == typeof(GridViewHierarchyRowInfo))
            {
                if (e.GridCellInfo.RowInfo.HierarchyLevel == 0)
                {
                    e.CellStyleInfo.IsItalic = true;
                    e.CellStyleInfo.FontSize = 12;
                    e.CellStyleInfo.BackColor = Color.GreenYellow;
                }
                else if (e.GridCellInfo.RowInfo.HierarchyLevel == 1)
                {
                    e.CellStyleInfo.ForeColor = Color.DarkGreen;
                    e.CellStyleInfo.BackColor = Color.LightGreen;
                }
            }
        }
        static void spreadExporterCSV_WorkbookCreated(object sender, WorkbookCreatedEventArgs e)
        {
            Worksheet worksheet1 = (Worksheet)e.Workbook.Sheets[0];
            worksheet1.Columns[worksheet1.UsedCellRange].AutoFitWidth();

            foreach (GridViewDataColumn column in gv.Columns)
            {
                if (column.DataType == typeof(DateTime))
                {
                    CellSelection selection = worksheet1.Cells[0, column.Index, gv.Rows.Count, column.Index];
                    selection.SetFormat(new CellValueFormat("dd/mm/yyyy hh:mm:ss"));
                }
            }
        }

        static void exporter_HTMLCellFormatting(object sender, Telerik.WinControls.UI.Export.HTML.HTMLCellFormattingEventArgs e)
        {
            e.HTMLCellElement.Styles.Remove("font-family");
            e.HTMLCellElement.Styles.Add("font-family", "Arial Unicode MS");

            if (e.GridColumnIndex == 1 && e.GridRowInfoType == typeof(GridViewDataRowInfo))
            {
                e.HTMLCellElement.Value = "Test value";
                e.HTMLCellElement.Styles.Add("background-color", ColorTranslator.ToHtml(Color.Orange));
            }
        }
        static void exporter_HTMLTableCaptionFormatting(object sender, HTMLTableCaptionFormattingEventArgs e)
        {
            e.TableCaptionElement.Styles.Add("background-color", ColorTranslator.ToHtml(Color.Red));
            e.TableCaptionElement.Styles.Add("font-size", "200%");
            e.TableCaptionElement.Styles.Add("color", ColorTranslator.ToHtml(Color.White));
            e.TableCaptionElement.Styles.Add("font-weight", "bold");
            e.CaptionText = _Title.ToString();
        }
    }
}
