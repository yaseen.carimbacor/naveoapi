using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Reportings;

namespace NaveoOneLib.Services.Reportings
{
    public class AutoReportingConfigDetailsService
    {
        Errors er = new Errors();
        public DataTable GetAutoReportingConfigDetails(String sConnStr)
        {
            String sqlString = @"SELECT * from GFI_FLT_AutoReportingConfigDetails order by ARDID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        public AutoReportingConfigDetail getARCD(DataRow dr)
        {
            AutoReportingConfigDetail a = new AutoReportingConfigDetail();

            a.ARDID = Convert.ToInt32(dr["ARDID"]);
            a.ARID = Convert.ToInt32(dr["ARID"]);
            a.UID = Convert.ToInt32(dr["UID"]);
            a.UIDType = dr["UIDType"].ToString();
            a.UIDCategory = dr["UIDCategory"].ToString();
            a.sMisc = dr["sMisc"].ToString();

            return a;
        }

        public AutoReportingConfigDetail GetAutoReportingConfigDetailsById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT * from GFI_FLT_AutoReportingConfigDetails WHERE ARDID = ? order by ARDID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;

            return getARCD(dt.Rows[0]);
        }

        public List<AutoReportingConfigDetail> GetAutoReportingConfigDetailsByIdList(String iID, String sConnStr)
        {
            String sqlString = @"SELECT * from GFI_FLT_AutoReportingConfigDetails WHERE ARID = ? order by ARID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            List<AutoReportingConfigDetail> l = new List<AutoReportingConfigDetail>();
            
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                foreach (DataRow dr in dt.Rows)
                    l.Add(getARCD(dr));
                return l;
            }
        }

        public bool SaveAutoReportingConfigDetails(AutoReportingConfigDetail uAutoReportingConfigDetails, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_AutoReportingConfigDetails";
            //dt.Columns.Add("ARDID", uAutoReportingConfigDetails.ARDID.GetType());
            dt.Columns.Add("ARID", uAutoReportingConfigDetails.ARID.GetType());
            dt.Columns.Add("UID", uAutoReportingConfigDetails.UID.GetType());
            dt.Columns.Add("UIDType", uAutoReportingConfigDetails.UIDType.GetType());
            dt.Columns.Add("UIDCategory", uAutoReportingConfigDetails.UIDCategory.GetType());
            dt.Columns.Add("sMisc", uAutoReportingConfigDetails.sMisc.GetType());
            DataRow dr = dt.NewRow();
           // dr["ARDID"] = uAutoReportingConfigDetails.ARDID;
            dr["ARID"] = uAutoReportingConfigDetails.ARID;
            dr["UID"] = uAutoReportingConfigDetails.UID;
            dr["UIDType"] = uAutoReportingConfigDetails.UIDType;
            dr["UIDCategory"] = uAutoReportingConfigDetails.UIDCategory;
            dr["sMisc"] = uAutoReportingConfigDetails.sMisc;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenInsert(dt, transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool UpdateAutoReportingConfigDetails(AutoReportingConfigDetail uAutoReportingConfigDetails, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_AutoReportingConfigDetails";
            //dt.Columns.Add("ARDID", uAutoReportingConfigDetails.ARDID.GetType());
            dt.Columns.Add("ARID", uAutoReportingConfigDetails.ARID.GetType());
            dt.Columns.Add("UID", uAutoReportingConfigDetails.UID.GetType());
            dt.Columns.Add("UIDType", uAutoReportingConfigDetails.UIDType.GetType());
            dt.Columns.Add("UIDCategory", uAutoReportingConfigDetails.UIDCategory.GetType());
            dt.Columns.Add("sMisc", uAutoReportingConfigDetails.sMisc.GetType());
            DataRow dr = dt.NewRow();
           // dr["ARDID"] = uAutoReportingConfigDetails.ARDID;
            dr["ARID"] = uAutoReportingConfigDetails.ARID;
            dr["UID"] = uAutoReportingConfigDetails.UID;
            dr["UIDType"] = uAutoReportingConfigDetails.UIDType;
            dr["UIDCategory"] = uAutoReportingConfigDetails.UIDCategory;
            dr["sMisc"] = uAutoReportingConfigDetails.sMisc;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "ARDID = '" + uAutoReportingConfigDetails.ARDID + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool DeleteAutoReportingConfigDetails(AutoReportingConfigDetail uAutoReportingConfigDetails, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_FLT_AutoReportingConfigDetails", "ARDID = '" + uAutoReportingConfigDetails.ARDID + "'", sConnStr))
                return true;
            else
                return false;
        }
    }
}