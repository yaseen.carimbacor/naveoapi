﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NaveoOneLib.Models.Common;
using NaveoOneLib.DBCon;
using System.Data;
using NaveoOneLib.Models.Users;
using System.Data.SqlTypes;
using System.IO;
using System.Drawing;
using NaveoOneLib.Common;
using NaveoOneLib.Services;
using NaveoService.Services;
using System.Drawing.Imaging;
using System.Web;
using System.Configuration;
using NaveoOneLib.Services.GMatrix;

namespace NaveoOneLib.Services.AccidentManagement
{
    public class AccidentManagementServices
    {
        DataTable AccidentManagDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_AccidentManagement";
            dt.Columns.Add("Acc_id", typeof(int));
            dt.Columns.Add("AssetID", typeof(int));
            dt.Columns.Add("Acc_AccidentDate", typeof(DateTime));
            dt.Columns.Add("DriverID", typeof(int));
            dt.Columns.Add("Acc_DriverContactNo", typeof(String));
            dt.Columns.Add("Acc_SectionDepartment", typeof(String));
            dt.Columns.Add("Acc_AccidentSiteLocation", typeof(String));
            dt.Columns.Add("Acc_GPSLocation", typeof(String));
            dt.Columns.Add("Acc_ReportedToASF", typeof(bool));
            dt.Columns.Add("Acc_ReportedToPoliceStation", typeof(Boolean));
            dt.Columns.Add("Acc_MemoFromHOD", typeof(Boolean));
            dt.Columns.Add("Acc_OB", typeof(String));
            dt.Columns.Add("Acc_OtherPartyInvolved", typeof(String));
            dt.Columns.Add("Acc_TransportRequest", typeof(Boolean));
            dt.Columns.Add("Acc_RequestStatus", typeof(String));
            dt.Columns.Add("Acc_CustomTripDetails", typeof(Boolean));
            dt.Columns.Add("Acc_From", typeof(String));
            dt.Columns.Add("Acc_To", typeof(String));
            dt.Columns.Add("Acc_AuthorisedTrip", typeof(Boolean));
            dt.Columns.Add("Acc_AuthorisedOfficer", typeof(Boolean));
            dt.Columns.Add("Acc_AssessmentOfDamageCost", typeof(double));
            dt.Columns.Add("Acc_AssessorTravelingCost", typeof(double));
            dt.Columns.Add("Acc_DateOfEstimatesSent", typeof(DateTime));
            dt.Columns.Add("Acc_DateOfSurvey", typeof(DateTime));
            dt.Columns.Add("Acc_SurveyorName", typeof(String));
            dt.Columns.Add("Acc_SurveyorContactNo", typeof(String));
            dt.Columns.Add("Acc_Status", typeof(String));
            dt.Columns.Add("Acc_Remarks", typeof(String));
            dt.Columns.Add("Acc_flagDelete", typeof(Boolean));
            dt.Columns.Add("oprType", typeof(DataRowState));

            return dt;
        }

        DataTable AccidentOtherPartyDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_AccidentOtherPartyInvolved";
            dt.Columns.Add("Acc_Oth_id", typeof(int));
            dt.Columns.Add("Acc_id", typeof(int));
            dt.Columns.Add("VID", typeof(int));
            dt.Columns.Add("Acc_OtherPartyOther", typeof(String));
            dt.Columns.Add("Acc_OtherPartyRegistration", typeof(String));
            dt.Columns.Add("Acc_flagDelete", typeof(Boolean));
            dt.Columns.Add("oprType", typeof(DataRowState));

            return dt;
        }

        DataTable AccidentAccompannyingDriverDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_AccidentAccompanyingDriver";
            dt.Columns.Add("Acc_Acc_Driv_id", typeof(int));
            dt.Columns.Add("Acc_id", typeof(int));
            dt.Columns.Add("DriverID", typeof(int));
            dt.Columns.Add("Acc_Acc_Driv_AuthorisedPerson", typeof(Boolean));
            dt.Columns.Add("Acc_Acc_Driv_Status", typeof(String));
            dt.Columns.Add("Acc_Acc_Driv_AuthorisingOfficer", typeof(String));
            dt.Columns.Add("Acc_Acc_Driv_Action", typeof(String));
            dt.Columns.Add("Acc_flagDelete", typeof(Boolean));
            dt.Columns.Add("oprType", typeof(DataRowState));

            return dt;
        }

        /// <summary>
        /// Update / Insert / Delete
        /// </summary>
        /// <param name="accidentMng"></param>
        /// <param name="sConnStr"></param>
        /// <returns></returns>
        public BaseModel AccidentManagement(NaveoOneLib.Models.AccidentManagement.AccidentManagementDetailDto accidentMng, String sConnStr)
        {
            BaseModel baseModel = new BaseModel();
            Boolean bResult = false;

            Boolean bInsert = false;

            int AccidentId = 0;

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            DataSet dsProcess = new DataSet();

            DataTable dtAccidentMng = AccidentManagDT();
            DataTable dtAccidentOthParty = AccidentOtherPartyDT();

            DataTable dtAccompannyingDriver = AccidentAccompannyingDriverDT();

            if (accidentMng.Acc_id == 0)
            {
                bInsert = true;
            }
            else
            {
                bInsert = false; //Update
            }


            DataRow drAccidentMng = dtAccidentMng.NewRow();
            if (!bInsert)
            {
                drAccidentMng["Acc_id"] = accidentMng.Acc_id;
            }
            drAccidentMng["AssetID"] = accidentMng.AssetID;
            drAccidentMng["Acc_AccidentDate"] = accidentMng.AccidentDate;
            drAccidentMng["DriverID"] = accidentMng.DriverID;
            drAccidentMng["Acc_DriverContactNo"] = accidentMng.DriverContactNo;
            drAccidentMng["Acc_SectionDepartment"] = accidentMng.SectionDepartment;
            drAccidentMng["Acc_AccidentSiteLocation"] = accidentMng.AccidentSiteLocation;
            drAccidentMng["Acc_GPSLocation"] = accidentMng.GPSLocation;
            drAccidentMng["Acc_ReportedToASF"] = accidentMng.ReportedToASF;
            drAccidentMng["Acc_ReportedToPoliceStation"] = accidentMng.ReportedToPoliceStation;
            drAccidentMng["Acc_MemoFromHOD"] = accidentMng.MemoFromHOD;
            drAccidentMng["Acc_OtherPartyInvolved"] = accidentMng.OtherPartyInvolved;
            drAccidentMng["Acc_TransportRequest"] = accidentMng.TransportRequest;
            drAccidentMng["Acc_RequestStatus"] = accidentMng.RequestStatus;
            drAccidentMng["Acc_CustomTripDetails"] = accidentMng.CustomTripDetails;
            drAccidentMng["Acc_From"] = accidentMng.From;
            drAccidentMng["Acc_To"] = accidentMng.To;
            drAccidentMng["Acc_AuthorisedTrip"] = accidentMng.AuthorisedTrip;
            drAccidentMng["Acc_AuthorisedOfficer"] = accidentMng.AuthorisedOfficer;
            drAccidentMng["Acc_AssessmentOfDamageCost"] = accidentMng.AssessmentOfDamageCost;
            drAccidentMng["Acc_AssessorTravelingCost"] = accidentMng.AssessorTravelingCost;



            if (accidentMng.DateOfEstimatesSent == DateTime.MinValue)
            {
                accidentMng.DateOfEstimatesSent = DateTime.Parse("01/01/1753");
            }

            if (accidentMng.DateOfSurvey == DateTime.MinValue)
            {
                accidentMng.DateOfSurvey = DateTime.Parse("01/01/1753");
            }
            drAccidentMng["Acc_DateOfEstimatesSent"] = accidentMng.DateOfEstimatesSent;
            drAccidentMng["Acc_DateOfSurvey"] = accidentMng.DateOfSurvey;
            drAccidentMng["Acc_SurveyorName"] = accidentMng.SurveyorName;
            drAccidentMng["Acc_SurveyorContactNo"] = accidentMng.SurveyorContactNo;
            drAccidentMng["Acc_Status"] = accidentMng.Status;
            drAccidentMng["Acc_Remarks"] = accidentMng.Remarks;

            if (accidentMng.flagDelete) //Flag Delete
            {
                drAccidentMng["Acc_flagDelete"] = accidentMng.flagDelete;
            }

            if (bInsert) // Flag Update
            {
                drAccidentMng["oprType"] = DataRowState.Added;
            }
            else
            {
                drAccidentMng["oprType"] = DataRowState.Modified;
            }

            dtAccidentMng.Rows.Add(drAccidentMng);

            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dtAccidentMng.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "Acc_id";
            drCtrl["KeyIsIdentity"] = "TRUE";
            //drCtrl["NextIdAction"] = "GET";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = accidentMng.Acc_id;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAccidentMng);

            //Get PK Acc_id to insert

            if (accidentMng.lstNameOfPerson.Count > 0)
            {
                // First loop
                foreach (NaveoOneLib.Models.AccidentManagement.lstNameOfPersonAccompanying accompanying in accidentMng.lstNameOfPerson)
                {
                    DataRow drAccompannyingDriver = dtAccompannyingDriver.NewRow();
                    //drAccompannyingDriver["MaintURI"] = maintAsset.URI; --To be fill by query
                    drAccompannyingDriver["DriverID"] = accompanying.DriverID;
                    drAccompannyingDriver["Acc_Acc_Driv_AuthorisedPerson"] = accompanying.Authorised;
                    drAccompannyingDriver["Acc_Acc_Driv_Status"] = accompanying.Status;
                    drAccompannyingDriver["Acc_Acc_Driv_AuthorisingOfficer"] = accompanying.AuthorisingOfficer;
                    drAccompannyingDriver["Acc_Acc_Driv_Action"] = accompanying.Action;
                    if (bInsert)
                    {
                        drAccompannyingDriver["oprType"] = DataRowState.Added;
                    }
                    else
                    {
                        drAccompannyingDriver["oprType"] = DataRowState.Modified;
                    }
                    dtAccompannyingDriver.Rows.Add(drAccompannyingDriver);
                }
                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 2;
                drCtrl["TblName"] = dtAccompannyingDriver.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "Acc_id";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdAction"] = "SET";
                drCtrl["NextIdFieldName"] = "Acc_id";
                drCtrl["ParentTblName"] = dtAccidentMng.TableName;
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtAccompannyingDriver);
            }


            if (accidentMng.lstPartyInvolved.Count > 0)
            {
                //Second loop
                foreach (NaveoOneLib.Models.AccidentManagement.lstOtherPartyInvolved partyInvolved in accidentMng.lstPartyInvolved)
                {
                    DataRow drAccidentOthParty = dtAccidentOthParty.NewRow();
                    //drAccompannyingDriver["MaintURI"] = maintAsset.URI; --To be fill by query
                    drAccidentOthParty["VID"] = partyInvolved.VID;
                    drAccidentOthParty["Acc_OtherPartyOther"] = partyInvolved.Others;
                    drAccidentOthParty["Acc_OtherPartyRegistration"] = partyInvolved.Registration;
                    drAccidentOthParty["oprType"] = DataRowState.Added;
                    dtAccidentOthParty.Rows.Add(drAccidentOthParty);
                }
                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 3;
                drCtrl["TblName"] = dtAccidentOthParty.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "Acc_id";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdAction"] = "SET";
                drCtrl["NextIdFieldName"] = "Acc_id";
                drCtrl["ParentTblName"] = dtAccidentMng.TableName;
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtAccidentOthParty);
            }

            dsProcess.Merge(dtCtrl);

            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResult.Tables["CTRLTBL"];

            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");
                DataRow[] dRowId = null;

                if (dRow.Length > 0)
                {
                    baseModel.dbResult = dsResult;
                    baseModel.dbResult.Tables["CTRLTBL"].TableName = "ResultCtrlTbl";
                    baseModel.dbResult.Merge(dsProcess);
                    bResult = false;
                }
                else
                {
                    bResult = true;
                    dRowId = dtCtrl.Select();
                    AccidentId = int.Parse(dRowId[0].ItemArray[10].ToString());
                }

            }

            if (bResult)
            {
                if (accidentMng.PicNames != null & accidentMng.PicNames.Count > 0)
                {
                    String[] images = accidentMng.PicNames[0].Split('`');
                    foreach (String pic in images)
                    {
                        String[] myPic = pic.Split(',');
                        if (myPic.Length == 2)
                        {
                            string picFormat = myPic[0];

                            string base64 = myPic[1];
                            byte[] bytes = Convert.FromBase64String(base64);
                            MemoryStream ms = new MemoryStream(bytes);

                            string filePath = ConfigurationManager.AppSettings["Path"].ToString();

                            string directoryPath = filePath + AccidentId;

                            if (!Directory.Exists(directoryPath))
                            {
                                Directory.CreateDirectory(directoryPath);
                            }

                            String imageNames = string.Empty;

                            switch (picFormat)
                            {
                                case "data:image/jpeg;base64":
                                    imageNames = AccidentId + ".jpg";
                                    Image image = Image.FromStream(ms);
                                    image.Save(directoryPath + imageNames, ImageFormat.Jpeg);
                                    break;
                                case "data:application/pdf;base64":
                                    using (FileStream file = new FileStream(directoryPath + DateTime.Now.ToString("MM_dd_yyyy_hh_mm_tt") + ".pdf", FileMode.Create, FileAccess.Write))
                                    {
                                        byte[] bytesPDF = new byte[ms.Length];
                                        ms.Read(bytesPDF, 0, (int)ms.Length);
                                        file.Write(bytesPDF, 0, bytes.Length);
                                        ms.Close();
                                    }
                                    break;
                                case "data:application/vnd.ms-excel;base64":

                                    var path = Path.Combine(directoryPath, DateTime.Now.ToString("MM_dd_yyyy_hh_mm_ss") + ".xlsx");
                                    using (var fs = new FileStream(path, FileMode.Create, FileAccess.Write))
                                    {
                                        // Write content of your memory stream into file stream
                                        ms.WriteTo(fs);
                                    }

                                    break;
                                case "data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64":

                                    var pathDoc = Path.Combine(directoryPath, DateTime.Now.ToString("MM_dd_yyyy_hh_mm_ss") + ".doc");
                                    using (var fs = new FileStream(pathDoc, FileMode.Create, FileAccess.Write))
                                    {
                                        // Write content of your memory stream into file stream
                                        ms.WriteTo(fs);
                                    }

                                    break;
                                default:
                                    baseModel.AdditionalInfo = "File format not supported";
                                    break;
                            }
                        }
                    }
                }
            }

            baseModel.data = bResult;
            return baseModel;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accidentMng"></param>
        /// <param name="sConnStr"></param>
        /// <returns></returns>
        public BaseModel ListAccidentManagement(NaveoOneLib.Models.AccidentManagement.lstAccidentManagementDto accidentMng, Guid sUserToken, String sConnStr)
        {
            BaseModel baseModel = new BaseModel();


            int UID = new NaveoOneLib.Services.Users.UserService().GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);
            DataTable dtAssetData = new NaveoOneLib.Services.Assets.AssetService().GetAsset(lm, sConnStr);

            string strRowCommaSeparated = "";

            int cpt = 0;
            foreach (DataRow dr in dtAssetData.Rows)
            {

                if (cpt == 0)
                {
                    strRowCommaSeparated = dr["AssetID"].ToString();
                }
                else
                {
                    strRowCommaSeparated += ", " + dr["AssetID"].ToString();
                }



                cpt++;
            }

            Connection c = new Connection(sConnStr);

            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = "select am.Acc_id, am.Acc_OtherPartyInvolved, am.Acc_AuthorisedOfficer, am.Acc_AssessmentOfDamageCost, am.Acc_AssessorTravelingCost, am.Acc_Remarks, am.AssetID, am.Acc_AccidentDate from GFI_SYS_AccidentManagement am where am.AssetID IN (" + strRowCommaSeparated + ") AND am.Acc_AccidentDate >= '" + accidentMng.StartDate.ToString("yyyy/MM/dd HH:mm:ss") + "' AND am.Acc_AccidentDate <= '" + accidentMng.EndDate.ToString("yyyy/MM/dd HH:mm:ss") + "' AND am.acc_flagdelete <> 'TRUE' ";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;

            //drSql["sParam"] = accidentMng.StartDate.ToString("MM/dd/yyyy HH:mm:ss") + "¬" + accidentMng.StartDate.ToString("MM/dd/yyyy HH:mm:ss");
            drSql["sTableName"] = "AccidentManagement";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_id"))
                ds.Tables["AccidentManagement"].Columns["acc_id"].ColumnName = "Acc_id";

            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_otherpartyinvolved"))
                ds.Tables["AccidentManagement"].Columns["acc_otherpartyinvolved"].ColumnName = "Acc_OtherPartyInvolved";

            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_authorisedofficer"))
                ds.Tables["AccidentManagement"].Columns["acc_authorisedofficer"].ColumnName = "Acc_AuthorisedOfficer";

            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_assessmentofdamagecost"))
                ds.Tables["AccidentManagement"].Columns["acc_assessmentofdamagecost"].ColumnName = "Acc_AssessmentOfDamageCost";

            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_assessortravelingcost"))
                ds.Tables["AccidentManagement"].Columns["acc_assessortravelingcost"].ColumnName = "Acc_AssessorTravelingCost";

            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_remarks"))
                ds.Tables["AccidentManagement"].Columns["acc_remarks"].ColumnName = "Acc_Remarks";

            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_accidentdate"))
                ds.Tables["AccidentManagement"].Columns["acc_accidentdate"].ColumnName = "Acc_AccidentDate";

            baseModel.data = ds;
            baseModel.dbResult = ds;
            return baseModel;
        }

        public BaseModel getAssetId(NaveoOneLib.Models.AccidentManagement.getAssetIdDto accidentMng, String sConnStr)
        {
            BaseModel baseModel = new BaseModel();

            Connection c = new Connection(sConnStr);

            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = "select am.Acc_id, CONCAT  (' RS ',am.Acc_AssessmentOfDamageCost, ' - ', am.Acc_AccidentDate) AS Description from GFI_SYS_AccidentManagement am where am.AssetID IN (" + accidentMng.AssetId + ") AND am.acc_flagdelete <> 'TRUE' ";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;

            //drSql["sParam"] = accidentMng.StartDate.ToString("MM/dd/yyyy HH:mm:ss") + "¬" + accidentMng.StartDate.ToString("MM/dd/yyyy HH:mm:ss");
            drSql["sTableName"] = "AccidentManagement";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            baseModel.data = ds;
            return baseModel;
        }


        public BaseModel GetAccidentMngByAccId(NaveoOneLib.Models.AccidentManagement.getAccidentMngDto accidentMng, String sConnStr)
        {
            //int AccidentId = 0;
            BaseModel baseModel = new BaseModel();

            Connection c = new Connection(sConnStr);

            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = "select 'AccidentManagement' as TableName;\r\n";
            sql += @"select Acc_id
                      ,AssetID
                      ,Acc_AccidentDate
                      ,DriverID
                      ,Acc_DriverContactNo
                      ,Acc_SectionDepartment
                      ,Acc_AccidentSiteLocation
                      ,Acc_GPSLocation
                      ,Acc_ReportedToASF
                      ,Acc_ReportedToPoliceStation
                      ,Acc_MemoFromHOD
                      ,Acc_OB
                      ,Acc_OtherPartyInvolved
                      ,Acc_TransportRequest
                      ,Acc_RequestStatus
                      ,Acc_CustomTripDetails
                      ,Acc_From
                      ,Acc_To
                      ,Acc_AuthorisedTrip
                      ,Acc_AuthorisedOfficer
                      ,Acc_AssessmentOfDamageCost
                      ,Acc_AssessorTravelingCost
                      ,Acc_DateOfEstimatesSent
                      ,Acc_DateOfSurvey
                      ,Acc_SurveyorName
                      ,Acc_SurveyorContactNo
                      ,Acc_Status
                      ,Acc_Remarks
                      ,Acc_flagDelete from gfi_sys_accidentmanagement where acc_id = " + accidentMng.Acc_id + ";\r\n";
            sql += "select 'AccidentOtherParty' as TableName;\r\n";
            sql += @"select Acc_Oth_id
                      ,Acc_id
                      ,VID
                      ,Acc_OtherPartyOther
                      ,Acc_OtherPartyRegistration
                      ,Acc_flagDelete from gfi_sys_accidentotherpartyinvolved where acc_id = " + accidentMng.Acc_id + ";\r\n";
            sql += "select 'AccidentAccompanyingDriver' as TableName;\r\n";
            sql += @"select Acc_Acc_Driv_id
                      ,Acc_id
                      ,DriverID
                      ,Acc_Acc_Driv_AuthorisedPerson
                      ,Acc_Acc_Driv_Status
                      ,Acc_Acc_Driv_AuthorisingOfficer
                      ,Acc_Acc_Driv_Action
                      ,Acc_flagDelete from gfi_sys_accidentaccompanyingdriver where acc_id = " + accidentMng.Acc_id + ";\r\n";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;

            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_id"))
                ds.Tables["AccidentManagement"].Columns["acc_id"].ColumnName = "Acc_id";

            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_otherpartyinvolved"))
                ds.Tables["AccidentManagement"].Columns["acc_otherpartyinvolved"].ColumnName = "Acc_OtherPartyInvolved";

            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_authorisedofficer"))
                ds.Tables["AccidentManagement"].Columns["acc_authorisedofficer"].ColumnName = "Acc_AuthorisedOfficer";

            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_assessmentofdamagecost"))
                ds.Tables["AccidentManagement"].Columns["acc_assessmentofdamagecost"].ColumnName = "Acc_AssessmentOfDamageCost";

            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_assessortravelingcost"))
                ds.Tables["AccidentManagement"].Columns["acc_assessortravelingcost"].ColumnName = "Acc_AssessorTravelingCost";

            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_remarks"))
                ds.Tables["AccidentManagement"].Columns["acc_remarks"].ColumnName = "Acc_Remarks";

            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_accidentdate"))
                ds.Tables["AccidentManagement"].Columns["acc_accidentdate"].ColumnName = "Acc_AccidentDate";

            if (ds.Tables["AccidentManagement"].Columns.Contains("assetid"))
                ds.Tables["AccidentManagement"].Columns["assetid"].ColumnName = "AssetID";

            if (ds.Tables["AccidentManagement"].Columns.Contains("driverid"))
                ds.Tables["AccidentManagement"].Columns["driverid"].ColumnName = "DriverID";

            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_drivercontactno"))
                ds.Tables["AccidentManagement"].Columns["acc_drivercontactno"].ColumnName = "Acc_DriverContactNo";

            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_sectiondepartment"))
                ds.Tables["AccidentManagement"].Columns["acc_sectiondepartment"].ColumnName = "Acc_SectionDepartment";

            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_accidentsitelocation"))
                ds.Tables["AccidentManagement"].Columns["acc_accidentsitelocation"].ColumnName = "Acc_AccidentSiteLocation";

            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_gpslocation"))
                ds.Tables["AccidentManagement"].Columns["acc_gpslocation"].ColumnName = "Acc_GPSLocation";


            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_reportedtoasf"))
                ds.Tables["AccidentManagement"].Columns["acc_reportedtoasf"].ColumnName = "Acc_ReportedToASF";

            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_reportedtopolicestation"))
                ds.Tables["AccidentManagement"].Columns["acc_reportedtopolicestation"].ColumnName = "Acc_ReportedToPoliceStation";

            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_memofromhod"))
                ds.Tables["AccidentManagement"].Columns["acc_memofromhod"].ColumnName = "Acc_MemoFromHOD";

            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_ob"))
                ds.Tables["AccidentManagement"].Columns["acc_ob"].ColumnName = "Acc_OB";

            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_transportrequest"))
                ds.Tables["AccidentManagement"].Columns["acc_transportrequest"].ColumnName = "Acc_TransportRequest";

            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_requeststatus"))
                ds.Tables["AccidentManagement"].Columns["acc_requeststatus"].ColumnName = "Acc_RequestStatus";

            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_customtripdetails"))
                ds.Tables["AccidentManagement"].Columns["acc_customtripdetails"].ColumnName = "Acc_CustomTripDetails";

            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_from"))
                ds.Tables["AccidentManagement"].Columns["acc_from"].ColumnName = "Acc_From";

            if (ds.Tables["AccidentManagement"].Columns.Contains("Acc_to"))
                ds.Tables["AccidentManagement"].Columns["Acc_to"].ColumnName = "Acc_To";

            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_authorisedtrip"))
                ds.Tables["AccidentManagement"].Columns["acc_authorisedtrip"].ColumnName = "Acc_AuthorisedTrip";

            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_dateofestimatessent"))
                ds.Tables["AccidentManagement"].Columns["acc_dateofestimatessent"].ColumnName = "Acc_DateOfEstimatesSent";

            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_dateofsurvey"))
                ds.Tables["AccidentManagement"].Columns["acc_dateofsurvey"].ColumnName = "Acc_DateOfSurvey";

            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_surveyorname"))
                ds.Tables["AccidentManagement"].Columns["acc_surveyorname"].ColumnName = "Acc_SurveyorName";

            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_surveyorcontactno"))
                ds.Tables["AccidentManagement"].Columns["acc_surveyorcontactno"].ColumnName = "Acc_SurveyorContactNo";

            if (ds.Tables["AccidentManagement"].Columns.Contains("acc_status"))
                ds.Tables["AccidentManagement"].Columns["acc_status"].ColumnName = "Acc_Status";

            //DataTable dtAccidentMng = ds.Tables["AccidentManagement"];

            //if (dtAccidentMng.Rows.Count > 0)
            // {
            // DataRow row = dtAccidentMng.Rows[0];
            // AccidentId = int.Parse(row["acc_id"].ToString());
            //}

            baseModel.data = ds;
            return baseModel;
        }

    }
}
