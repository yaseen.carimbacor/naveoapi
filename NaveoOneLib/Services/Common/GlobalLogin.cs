﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NaveoOneLib.DBCon;
using System.Data;
using NaveoOneLib.Common;
using NaveoOneLib.Models;
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Zones;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.Drivers;
using NaveoOneLib.Services.Drivers;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Services.Assets;
using NaveoOneLib.Services.Zones;

namespace NaveoOneLib.Services
{
    public class GlobalLogin
    {
        Errors er = new Errors();

        public static void CoreInitAllInOne(List<Matrix> lMatrix, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            CoreInitAllInOne(iParentIDs, sConnStr);
        }
        public static void CoreInitAllInOne(List<int> iParentIDs, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new ZoneService().sGetZoneHeader(iParentIDs, sConnStr, null, null);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZoneH";
            dtSql.Rows.Add(drSql);

            sql = new ZoneService().sGetZoneDetail(iParentIDs, sConnStr, null, null);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZoneD";
            dtSql.Rows.Add(drSql);

            sql = new ZoneTypeService().sGetZoneTypes(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZoneTypes";
            dtSql.Rows.Add(drSql);

            sql = new ZoneHeadZoneTypeService().sGetZHZTs(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZHZT";
            dtSql.Rows.Add(drSql);

            sql = new AssetService().sGetAsset(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtAssets";
            dtSql.Rows.Add(drSql);

            sql = new AssetDeviceMapService().sGetAssetDeviceMap(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtAssetMap";
            dtSql.Rows.Add(drSql);

            sql = new DriverService().sGetDriver(iParentIDs, String.Empty,false,Driver.ResourceType.Driver, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtDriver";
            dtSql.Rows.Add(drSql);

            sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Groups);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMNone";
            dtSql.Rows.Add(drSql);

            sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Assets);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMAsset";
            dtSql.Rows.Add(drSql);

            sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Users);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMUser";
            dtSql.Rows.Add(drSql);

            sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Zones);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMZone";
            dtSql.Rows.Add(drSql);

            sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.ZoneTypes);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMZoneType";
            dtSql.Rows.Add(drSql);

            sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Drivers);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMDriver";
            dtSql.Rows.Add(drSql);

            sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Rules);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMRule";
            dtSql.Rows.Add(drSql);

            sql = "select getdate()";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtServerDate";
            dtSql.Rows.Add(drSql);

            sql = new MatrixService().sGetMatrixByGMID(iParentIDs, "GFI_SYS_GroupMatrixZone");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMLZone";
            dtSql.Rows.Add(drSql);

            sql = new MatrixService().sGetMatrixByGMID(iParentIDs, "GFI_SYS_GroupMatrixAsset");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMLAsset";
            dtSql.Rows.Add(drSql);

            sql = new MatrixService().sGetMatrixByGMID(iParentIDs, "GFI_SYS_GroupMatrixDriver");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMLDriver";
            dtSql.Rows.Add(drSql);

            sql = new MatrixService().sGetMatrixByGMID(iParentIDs, "GFI_SYS_GroupMatrixUser");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMLUser";
            dtSql.Rows.Add(drSql);

            #region Maintenance
            sql = new AssetExtProFieldService().sGetAssetExtProField();
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "AssetExtProFields";
            dtSql.Rows.Add(drSql);

            sql = new VehicleMaintCatService().sGetVehicleMaintCat();
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtVehicleMaintCat";
            dtSql.Rows.Add(drSql);

            sql = new InsCoverTypesService().sGetInsCoverType();
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtInsCoverTypes";
            dtSql.Rows.Add(drSql);

            sql = new VehicleMaintStatusService().sGetVehicleMaintStatus();
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtVehicleMaintStatus";
            dtSql.Rows.Add(drSql);

            sql = new VehicleMaintTypeService().sGetVehicleMaintType(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtVehicleMaintType";
            dtSql.Rows.Add(drSql);

            sql = new VehicleMaintTypeService().sGetVehicleMaintTypeForMaintAssign(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtVehicleMaintTypeAssign";
            dtSql.Rows.Add(drSql);

            sql = new VehicleTypesService().sGetVehicleTypes();
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtVehicleTypes";
            dtSql.Rows.Add(drSql);
            #endregion

            #region Security
            sql = new AccessTemplateService().sGetAccessTemplatesForUserAssign(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtAccessTemplatesForUserAssign";
            dtSql.Rows.Add(drSql);


            sql = new GlobalParamsService().sGetGlobalParamsByName("EXCEPSRC");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGlobalParams";
            dtSql.Rows.Add(drSql);

            #endregion

            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            //--- Zone ---
            DataTable dtZoneD = ds.Tables["dtZoneD"];
            List<ZoneDetail> ld = new myConverter().ConvertToList<ZoneDetail>(dtZoneD);
            DataTable dtZoneH = ds.Tables["dtZoneH"];
            Globals.dtZoneTypes = ds.Tables["dtZoneTypes"];
            List<Zone> lz = new myConverter().ConvertToList<Zone>(dtZoneH);
            foreach (Zone z in lz)
                z.zoneDetails = ld.FindAll(o => o.zoneID == z.zoneID);
            DataTable dtZHZT = ds.Tables["dtZHZT"];
            List<ZoneHeadZoneType> lzhzt = new myConverter().ConvertToList<ZoneHeadZoneType>(dtZHZT);
            foreach (Zone z in lz)
                z.lZoneType = new ZoneTypeService().getMyZoneTypes(lzhzt.FindAll(o => o.ZoneHeadID == z.zoneID), Globals.dtZoneTypes);

            foreach (Zone z in lz)
            {
                DataRow[] dRows = ds.Tables["dtGMZone"].Select("StrucID = " + z.zoneID + "");

                foreach (DataRow dr in dRows)
                {
                    Matrix mx = new Matrix();
                    mx.MID = Convert.ToInt32(dr["MID"]);
                    mx.iID = Convert.ToInt32(dr["StrucID"]);
                    mx.GMID = Convert.ToInt32(dr["GMID"]);

                    String strDesc = String.Empty;
                    DataRow[] drGMDesc = ds.Tables["dtGMZone"].Select("GMID = " + dr["ParentGMID"] + "");
                    if (drGMDesc.Length > 0)
                        strDesc = drGMDesc[0]["GMDescription"].ToString();

                    mx.GMDesc = strDesc;
                    mx.oprType = DataRowState.Unchanged;
                    z.lMatrix.Add(mx);
                }
            }

            Globals.lZone = lz;

            //--- Asset ---
            DataTable dtAsset = ds.Tables["dtAssets"];
            //foreach (DataRow dr in dtAsset.Rows)
            //    dr["AssetNumber"] = BaseService.Decrypt(dr["AssetNumber"].ToString());
            List<Asset> la = new myConverter().ConvertToList<Asset>(dtAsset);
            DataTable dtDeviceMap = ds.Tables["dtAssetMap"];
            List<AssetDeviceMap> ldm = new myConverter().ConvertToList<AssetDeviceMap>(dtDeviceMap);
            foreach (Asset a in la)
                a.assetDeviceMap = ldm.FindAll(o => o.AssetID == a.AssetID).FirstOrDefault();

            foreach (Asset a in la)
            {
                String tzName = a.TimeZoneID;
                if (tzName == String.Empty)
                    tzName = TimeZone.CurrentTimeZone.StandardName;
                a.TimeZoneTS = new Utils().GetTimeSpan(tzName);

                DataRow[] dRows = ds.Tables["dtGMLAsset"].Select("iID = " + a.AssetID + "");

                foreach (DataRow dr in dRows)
                {
                    Matrix mx = new Matrix();
                    mx.MID = Convert.ToInt32(dr["MID"]);
                    mx.iID = Convert.ToInt32(dr["iID"]);
                    mx.GMID = Convert.ToInt32(dr["GMID"]);
                    mx.GMDesc = dr["GMDescription"].ToString();
                    mx.oprType = DataRowState.Unchanged;
                    a.lMatrix.Add(mx);
                }
            }

            Globals.lAsset = la;

            //--- Driver ---
            DataTable dtDriver = ds.Tables["dtDriver"];
            List<Driver> ldr = new myConverter().ConvertToList<Driver>(dtDriver);

            foreach (Driver d in ldr)
            {
                DataRow[] dRows = ds.Tables["dtGMLDriver"].Select("iID = " + d.DriverID + "");

                foreach (DataRow dr in dRows)
                {
                    Matrix mx = new Matrix();
                    mx.MID = Convert.ToInt32(dr["MID"]);
                    mx.iID = Convert.ToInt32(dr["iID"]);
                    mx.GMID = Convert.ToInt32(dr["GMID"]);
                    mx.GMDesc = dr["GMDescription"].ToString();
                    mx.oprType = DataRowState.Unchanged;
                    d.lMatrix.Add(mx);
                }
            }
            Globals.lDriver = ldr;

            Globals.dtGMNone = ds.Tables["dtGMNone"];
            Globals.dtGMZone = ds.Tables["dtGMZone"];
            Globals.dtGMZoneType = ds.Tables["dtGMZoneType"];
            Globals.dtGMUser = ds.Tables["dtGMUser"];
            Globals.dtGMAsset = ds.Tables["dtGMAsset"];
            //foreach (DataRow dr in ds.Tables["dtGMAsset"].Rows)
            //    if (dr[0].ToString().Contains("-"))
            //        dr[1] = BaseService.Decrypt(dr[1].ToString());
            Globals.dtGMDriver = ds.Tables["dtGMDriver"];

            Globals.dtGMLZone = ds.Tables["dtGMLZone"];

            Globals.dtAssetExtProFields = ds.Tables["dtAssetExtProFields"];
            Globals.dtVehicleMaintCat = ds.Tables["dtVehicleMaintCat"];
            Globals.dtInsCoverTypes = ds.Tables["dtInsCoverTypes"];
            Globals.dtVehicleMaintStatus = ds.Tables["dtVehicleMaintStatus"];
            Globals.dtVehicleMaintType = ds.Tables["dtVehicleMaintType"];
            Globals.dtVehicleMaintTypeAssign = ds.Tables["dtVehicleMaintTypeAssign"];
            Globals.dtVehicleTypes = ds.Tables["dtVehicleTypes"];
            Globals.dtAccessTemplatesForUserAssign = ds.Tables["dtAccessTemplatesForUserAssign"];

            //dtGlobalParams
            Globals.ExeSource = ds.Tables["dtGlobalParams"].Rows[0]["PValue"].ToString();
        }

        public static void CoreInitFLT(List<Matrix> lMatrix, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            CoreInitFLT(iParentIDs, sConnStr);
        }
        public static void CoreInitFLT(List<int> iParentIDs, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new AssetService().sGetAsset(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtAsset";
            dtSql.Rows.Add(drSql);

            sql = new AssetDeviceMapService().sGetAssetDeviceMap(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtAssetMap";
            dtSql.Rows.Add(drSql);

            sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Groups);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);

            sql = new DriverService().sGetDriver(iParentIDs, String.Empty,false,Driver.ResourceType.Driver, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtDriver";
            dtSql.Rows.Add(drSql);

            sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Assets);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMAsset";
            dtSql.Rows.Add(drSql);

            sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Groups);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMNone";
            dtSql.Rows.Add(drSql);

            sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Users);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMUser";
            dtSql.Rows.Add(drSql);

            sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Drivers);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMDriver";
            dtSql.Rows.Add(drSql);

            sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Rules);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMRule";
            dtSql.Rows.Add(drSql);

            sql = "select getdate()";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtServerDate";
            dtSql.Rows.Add(drSql);

            sql = new MatrixService().sGetMatrixByGMID(iParentIDs, "GFI_SYS_GroupMatrixAsset");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMLAsset";
            dtSql.Rows.Add(drSql);

            sql = new MatrixService().sGetMatrixByGMID(iParentIDs, "GFI_SYS_GroupMatrixDriver");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMLDriver";
            dtSql.Rows.Add(drSql);

            sql = new MatrixService().sGetMatrixByGMID(iParentIDs, "GFI_SYS_GroupMatrixUser");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMLUser";
            dtSql.Rows.Add(drSql);

            sql = new GlobalParamsService().sGetGlobalParamsByName("TickerMessage");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtTickerMessage";
            dtSql.Rows.Add(drSql);

            #region Maintenance
            sql = new AssetExtProFieldService().sGetAssetExtProField();
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "AssetExtProFields";
            dtSql.Rows.Add(drSql);

            sql = new VehicleMaintCatService().sGetVehicleMaintCat();
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtVehicleMaintCat";
            dtSql.Rows.Add(drSql);

            sql = new InsCoverTypesService().sGetInsCoverType();
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtInsCoverTypes";
            dtSql.Rows.Add(drSql);

            sql = new VehicleMaintStatusService().sGetVehicleMaintStatus();
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtVehicleMaintStatus";
            dtSql.Rows.Add(drSql);

            sql = new VehicleMaintTypeService().sGetVehicleMaintType(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtVehicleMaintType";
            dtSql.Rows.Add(drSql);

            sql = new VehicleMaintTypeService().sGetVehicleMaintTypeForMaintAssign(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtVehicleMaintTypeAssign";
            dtSql.Rows.Add(drSql);

            sql = new VehicleTypesService().sGetVehicleTypes();
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtVehicleTypes";
            dtSql.Rows.Add(drSql);
            #endregion

            #region Security
            sql = new AccessTemplateService().sGetAccessTemplatesForUserAssign(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtAccessTemplatesForUserAssign";
            dtSql.Rows.Add(drSql);


            sql = new GlobalParamsService().sGetGlobalParamsByName("EXCEPSRC");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGlobalParams";
            dtSql.Rows.Add(drSql);

            #endregion

            sql = "select max(DateTimeGPS_UTC) from GFI_ARC_GPSData";
            sql = "select GETDATE()";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtArchive";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            //foreach (DataTable dt in ds.Tables)
            //new Utils().WriteDTtoTxtFile(System.Windows.Forms.Application.StartupPath.ToString() + "\\Errors\\" + dt.TableName + ".txt", dt);


            //--- Asset ---
            DataTable dtAsset = ds.Tables["dtAsset"];
            //foreach (DataRow dr in dtAsset.Rows)
            //    dr["AssetNumber"] = BaseService.Decrypt(dr["AssetNumber"].ToString());
            List<Asset> la = new myConverter().ConvertToList<Asset>(dtAsset);
            DataTable dtDeviceMap = ds.Tables["dtAssetMap"];
            List<AssetDeviceMap> ldm = new myConverter().ConvertToList<AssetDeviceMap>(dtDeviceMap);
            foreach (Asset a in la)
                a.assetDeviceMap = ldm.FindAll(o => o.AssetID == a.AssetID).FirstOrDefault();

            foreach (Asset a in la)
            {
                String tzName = a.TimeZoneID;
                if (tzName == String.Empty)
                    tzName = TimeZone.CurrentTimeZone.StandardName;
                a.TimeZoneTS = new Utils().GetTimeSpan(tzName);

                DataRow[] dRows = ds.Tables["dtGMLAsset"].Select("iID = " + a.AssetID + "");

                foreach (DataRow dr in dRows)
                {
                    Matrix mx = new Matrix();
                    mx.MID = Convert.ToInt32(dr["MID"]);
                    mx.iID = Convert.ToInt32(dr["iID"]);
                    mx.GMID = Convert.ToInt32(dr["GMID"]);
                    mx.GMDesc = dr["GMDescription"].ToString();
                    mx.oprType = DataRowState.Unchanged;
                    a.lMatrix.Add(mx);
                }
            }

            Globals.lAsset = la;

            //--- Driver ---
            DataTable dtDriver = ds.Tables["dtDriver"];
            List<Driver> ldr = new myConverter().ConvertToList<Driver>(dtDriver);

            foreach (Driver d in ldr)
            {
                DataRow[] dRows = ds.Tables["dtGMLDriver"].Select("iID = " + d.DriverID + "");

                foreach (DataRow dr in dRows)
                {
                    Matrix mx = new Matrix();
                    mx.MID = Convert.ToInt32(dr["MID"]);
                    mx.iID = Convert.ToInt32(dr["iID"]);
                    mx.GMID = Convert.ToInt32(dr["GMID"]);
                    mx.GMDesc = dr["GMDescription"].ToString();
                    mx.oprType = DataRowState.Unchanged;
                    d.lMatrix.Add(mx);
                }
            }
            Globals.lDriver = ldr;

            Globals.dtGMNone = ds.Tables["dtGMNone"];
            Globals.dtGMUser = ds.Tables["dtGMUser"];
            Globals.dtGMAsset = ds.Tables["dtGMAsset"];
            //foreach (DataRow dr in ds.Tables["dtGMAsset"].Rows)
            //    if (dr[0].ToString().Contains("-"))
            //        dr[1] = BaseService.Decrypt(dr[1].ToString());
            Globals.dtGMDriver = ds.Tables["dtGMDriver"];

            Globals.dtAsset = ds.Tables["dtAsset"];
            Globals.dtDriver = ds.Tables["dtDriver"];
            Globals.dtMatrix = ds.Tables["dtMatrix"];

            Globals.dtAssetExtProFields = ds.Tables["dtAssetExtProFields"];
            Globals.dtVehicleMaintCat = ds.Tables["dtVehicleMaintCat"];
            Globals.dtInsCoverTypes = ds.Tables["dtInsCoverTypes"];
            Globals.dtVehicleMaintStatus = ds.Tables["dtVehicleMaintStatus"];
            Globals.dtVehicleMaintType = ds.Tables["dtVehicleMaintType"];
            Globals.dtVehicleMaintTypeAssign = ds.Tables["dtVehicleMaintTypeAssign"];
            Globals.dtVehicleTypes = ds.Tables["dtVehicleTypes"];
            Globals.dtAccessTemplatesForUserAssign = ds.Tables["dtAccessTemplatesForUserAssign"];

            //dtGlobalParams
            Globals.ExeSource = ds.Tables["dtGlobalParams"].Rows[0]["PValue"].ToString();
            if (ds.Tables["dtTickerMessage"].Rows.Count > 0)
                Globals.sLoginMessage = ds.Tables["dtTickerMessage"].Rows[0]["PValue"].ToString();

            //if (ds.Tables["dtArchive"].Rows.Count > 0)
            //    if (!String.IsNullOrEmpty(ds.Tables["dtArchive"].Rows[0][0].ToString()))
            //        Globals.dtArchive = Convert.ToDateTime(ds.Tables["dtArchive"].Rows[0][0]);
        }

        public static void CoreInitGIS(List<Matrix> lMatrix, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            CoreInitGIS(iParentIDs, sConnStr);
        }
        public static void CoreInitGIS(List<int> iParentIDs, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new ZoneService().sGetZoneHeader(iParentIDs, sConnStr, null, null);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZoneH";
            dtSql.Rows.Add(drSql);

            sql = new ZoneService().sGetZoneDetail(iParentIDs, sConnStr, null, null);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZoneD";
            dtSql.Rows.Add(drSql);

            sql = new ZoneTypeService().sGetZoneTypes(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZoneTypes";
            dtSql.Rows.Add(drSql);

            sql = new ZoneHeadZoneTypeService().sGetZHZTs(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZHZT";
            dtSql.Rows.Add(drSql);

            sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Zones);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMZone";
            dtSql.Rows.Add(drSql);

            sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.ZoneTypes);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMZoneType";
            dtSql.Rows.Add(drSql);

            sql = new MatrixService().sGetMatrixByGMID(iParentIDs, "GFI_SYS_GroupMatrixZone");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMLZone";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            //--- Zone ---
            DataTable dtZoneD = ds.Tables["dtZoneD"];
            List<ZoneDetail> ld = new myConverter().ConvertToList<ZoneDetail>(dtZoneD);
            DataTable dtZoneH = ds.Tables["dtZoneH"];
            Globals.dtZoneTypes = ds.Tables["dtZoneTypes"];
            List<Zone> lz = new myConverter().ConvertToList<Zone>(dtZoneH);
            foreach (Zone z in lz)
                z.zoneDetails = ld.FindAll(o => o.zoneID == z.zoneID);
            DataTable dtZHZT = ds.Tables["dtZHZT"];
            List<ZoneHeadZoneType> lzhzt = new myConverter().ConvertToList<ZoneHeadZoneType>(dtZHZT);
            foreach (Zone z in lz)
                z.lZoneType = new ZoneTypeService().getMyZoneTypes(lzhzt.FindAll(o => o.ZoneHeadID == z.zoneID), Globals.dtZoneTypes);

            foreach (Zone z in lz)
                foreach (ZoneType zt in z.lZoneType)
                    zt.oprType = DataRowState.Unchanged;

            foreach (Zone z in lz)
            {
                DataRow[] dRows = ds.Tables["dtGMZone"].Select("StrucID = " + z.zoneID + "");

                foreach (DataRow dr in dRows)
                {
                    Matrix mx = new Matrix();
                    mx.MID = Convert.ToInt32(dr["MID"]);
                    mx.iID = Convert.ToInt32(dr["StrucID"]);
                    //Reza 24?Jul/2015  mx.GMID =Convert.ToInt32(dr["GMID"];
                    mx.GMID = Convert.ToInt32(dr["ParentGMID"]);

                    String strDesc = String.Empty;
                    DataRow[] drGMDesc = ds.Tables["dtGMZone"].Select("GMID = " + dr["ParentGMID"] + "");
                    if (drGMDesc.Length > 0)
                        strDesc = drGMDesc[0]["GMDescription"].ToString();

                    mx.GMDesc = strDesc;
                    mx.oprType = DataRowState.Unchanged;
                    z.lMatrix.Add(mx);
                }
            }

            Globals.lZone = lz;

            Globals.dtGMZone = ds.Tables["dtGMZone"];
            Globals.dtGMZoneType = ds.Tables["dtGMZoneType"];
            Globals.dtGMLZone = ds.Tables["dtGMLZone"];
        }

        public static List<Zone> lZone(List<Matrix> lMatrix, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            return lZone(iParentIDs, sConnStr);
        }
        public static List<Zone> lZoneOri(List<int> iParentIDs, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new ZoneService().sGetZoneHeader(iParentIDs, sConnStr, null, null);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZoneH";
            dtSql.Rows.Add(drSql);

            sql = new ZoneService().sGetZoneDetail(iParentIDs, sConnStr, null, null);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZoneD";
            dtSql.Rows.Add(drSql);

            sql = new ZoneTypeService().sGetZoneTypes(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZoneTypes";
            dtSql.Rows.Add(drSql);

            sql = new ZoneHeadZoneTypeService().sGetZHZTs(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZHZT";
            dtSql.Rows.Add(drSql);

            sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Zones);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMZone";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            //--- Zone ---
            DataTable dtZoneD = ds.Tables["dtZoneD"];
            List<ZoneDetail> ld = new myConverter().ConvertToList<ZoneDetail>(dtZoneD);
            DataTable dtZoneH = ds.Tables["dtZoneH"];
            Globals.dtZoneTypes = ds.Tables["dtZoneTypes"];
            List<Zone> lz = new myConverter().ConvertToList<Zone>(dtZoneH);
            foreach (Zone z in lz)
                z.zoneDetails = ld.FindAll(o => o.zoneID == z.zoneID);
            DataTable dtZHZT = ds.Tables["dtZHZT"];
            List<ZoneHeadZoneType> lzhzt = new myConverter().ConvertToList<ZoneHeadZoneType>(dtZHZT);
            foreach (Zone z in lz)
                z.lZoneType = new ZoneTypeService().getMyZoneTypes(lzhzt.FindAll(o => o.ZoneHeadID == z.zoneID), Globals.dtZoneTypes);

            foreach (Zone z in lz)
                foreach (ZoneType zt in z.lZoneType)
                    zt.oprType = DataRowState.Unchanged;

            foreach (Zone z in lz)
            {
                DataRow[] dRows = ds.Tables["dtGMZone"].Select("StrucID = " + z.zoneID + "");

                foreach (DataRow dr in dRows)
                {
                    Matrix mx = new Matrix();
                    mx.MID = Convert.ToInt32(dr["MID"]);
                    mx.iID = Convert.ToInt32(dr["StrucID"]);
                    mx.GMID = Convert.ToInt32(dr["ParentGMID"]);

                    String strDesc = String.Empty;
                    DataRow[] drGMDesc = ds.Tables["dtGMZone"].Select("GMID = " + dr["ParentGMID"] + "");
                    if (drGMDesc.Length > 0)
                        strDesc = drGMDesc[0]["GMDescription"].ToString();

                    mx.GMDesc = strDesc;
                    mx.oprType = DataRowState.Unchanged;
                    z.lMatrix.Add(mx);
                }
            }

            return lz;
        }
        public static List<Zone> lZone(List<int> iParentIDs, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new ZoneService().sGetZoneHeader(iParentIDs, sConnStr, null, null);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZoneH";
            dtSql.Rows.Add(drSql);

            sql = new ZoneService().sGetZoneDetail(iParentIDs, sConnStr, null, null);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZoneD";
            dtSql.Rows.Add(drSql);

            sql = new ZoneTypeService().sGetZoneTypes(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZoneTypes";
            dtSql.Rows.Add(drSql);

            sql = new ZoneHeadZoneTypeService().sGetZHZTs(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZHZT";
            dtSql.Rows.Add(drSql);

            sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Zones);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMZone";
            dtSql.Rows.Add(drSql);

            sql = new MatrixService().sGetMatrixByGMID(iParentIDs, "GFI_SYS_GroupMatrixZone");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMLZone";
            dtSql.Rows.Add(drSql);

            sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.ZoneTypes);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMZoneType";
            dtSql.Rows.Add(drSql);


            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            //--- Zone ---
            DataTable dtZoneD = ds.Tables["dtZoneD"];
            DataTable dtZoneH = ds.Tables["dtZoneH"];
            DataTable dtZHZT = ds.Tables["dtZHZT"];
            DataTable dtZoneTypes = ds.Tables["dtZoneTypes"];

            List<ZoneDetail> ld = new myConverter().ConvertToList<ZoneDetail>(dtZoneD);
            List<ZoneHeadZoneType> lzhzt = new myConverter().ConvertToList<ZoneHeadZoneType>(dtZHZT);
            List<Zone> lz = new myConverter().ConvertToList<Zone>(dtZoneH);

            foreach (Zone z in lz)
            {
                z.zoneDetails = ld.FindAll(o => o.zoneID == z.zoneID);
                z.lZoneType = new ZoneTypeService().getMyZoneTypes(lzhzt.FindAll(o => o.ZoneHeadID == z.zoneID), dtZoneTypes, true);

                DataRow[] dRows = ds.Tables["dtGMZone"].Select("StrucID = " + z.zoneID + "");
                foreach (DataRow dr in dRows)
                {
                    Matrix mx = new Matrix();
                    mx.MID = Convert.ToInt32(dr["MID"]);
                    mx.iID = Convert.ToInt32(dr["StrucID"]);
                    mx.GMID = Convert.ToInt32(dr["ParentGMID"]);

                    String strDesc = String.Empty;
                    DataRow[] drGMDesc = ds.Tables["dtGMZone"].Select("GMID = " + dr["ParentGMID"] + "");
                    if (drGMDesc.Length > 0)
                        strDesc = drGMDesc[0]["GMDescription"].ToString();

                    mx.GMDesc = strDesc;
                    mx.oprType = DataRowState.Unchanged;
                    z.lMatrix.Add(mx);
                }
            }

            Globals.dtZoneTypes = ds.Tables["dtZoneTypes"];
            Globals.dtGMZone = ds.Tables["dtGMZone"];
            Globals.dtGMZoneType = ds.Tables["dtGMZoneType"];
            Globals.dtGMLZone = ds.Tables["dtGMLZone"];
            Globals.lZone = lz;
            return lz;
        }

        public static void CoreInitMatrix(List<Matrix> lMatrix, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            CoreInitMatrix(iParentIDs, sConnStr);
        }
        public static void CoreInitMatrix(List<int> iParentIDs, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new AssetService().sGetAsset(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtAsset";
            dtSql.Rows.Add(drSql);

            sql = new AssetDeviceMapService().sGetAssetDeviceMap(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtAssetMap";
            dtSql.Rows.Add(drSql);

            sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Groups);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);

            sql = new DriverService().sGetDriver(iParentIDs, String.Empty,false,Driver.ResourceType.Driver, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtDriver";
            dtSql.Rows.Add(drSql);

            sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Assets);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMAsset";
            dtSql.Rows.Add(drSql);

            sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Groups);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMNone";
            dtSql.Rows.Add(drSql);

            sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Users);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMUser";
            dtSql.Rows.Add(drSql);

            sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Drivers);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMDriver";
            dtSql.Rows.Add(drSql);

            sql = new MatrixService().sGetMatrixByGMID(iParentIDs, "GFI_SYS_GroupMatrixAsset");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMLAsset";
            dtSql.Rows.Add(drSql);

            sql = new MatrixService().sGetMatrixByGMID(iParentIDs, "GFI_SYS_GroupMatrixDriver");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMLDriver";
            dtSql.Rows.Add(drSql);

            sql = new MatrixService().sGetMatrixByGMID(iParentIDs, "GFI_SYS_GroupMatrixUser");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMLUser";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            //--- Asset ---
            DataTable dtAsset = ds.Tables["dtAsset"];
            //foreach (DataRow dr in dtAsset.Rows)
            //    dr["AssetNumber"] = BaseService.Decrypt(dr["AssetNumber"].ToString());
            List<Asset> la = new myConverter().ConvertToList<Asset>(dtAsset);
            DataTable dtDeviceMap = ds.Tables["dtAssetMap"];
            List<AssetDeviceMap> ldm = new myConverter().ConvertToList<AssetDeviceMap>(dtDeviceMap);
            foreach (Asset a in la)
                a.assetDeviceMap = ldm.FindAll(o => o.AssetID == a.AssetID).FirstOrDefault();

            foreach (Asset a in la)
            {
                String tzName = a.TimeZoneID;
                if (tzName == String.Empty)
                    tzName = TimeZone.CurrentTimeZone.StandardName;
                a.TimeZoneTS = new Utils().GetTimeSpan(tzName);

                DataRow[] dRows = ds.Tables["dtGMLAsset"].Select("iID = " + a.AssetID + "");

                foreach (DataRow dr in dRows)
                {
                    Matrix mx = new Matrix();
                    mx.MID = Convert.ToInt32(dr["MID"]);
                    mx.iID = Convert.ToInt32(dr["iID"]);
                    mx.GMID = Convert.ToInt32(dr["GMID"]);
                    mx.GMDesc = dr["GMDescription"].ToString();
                    mx.oprType = DataRowState.Unchanged;
                    a.lMatrix.Add(mx);
                }
            }

            Globals.lAsset = la;

            //--- Driver ---
            DataTable dtDriver = ds.Tables["dtDriver"];
            List<Driver> ldr = new myConverter().ConvertToList<Driver>(dtDriver);

            foreach (Driver d in ldr)
            {
                DataRow[] dRows = ds.Tables["dtGMLDriver"].Select("iID = " + d.DriverID + "");

                foreach (DataRow dr in dRows)
                {
                    Matrix mx = new Matrix();
                    mx.MID = Convert.ToInt32(dr["MID"]);
                    mx.iID = Convert.ToInt32(dr["iID"]);
                    mx.GMID = Convert.ToInt32(dr["GMID"]);
                    mx.GMDesc = dr["GMDescription"].ToString();
                    mx.oprType = DataRowState.Unchanged;
                    d.lMatrix.Add(mx);
                }
            }
            Globals.lDriver = ldr;

            Globals.dtGMNone = ds.Tables["dtGMNone"];
            Globals.dtGMUser = ds.Tables["dtGMUser"];
            Globals.dtGMAsset = ds.Tables["dtGMAsset"];
            //foreach (DataRow dr in ds.Tables["dtGMAsset"].Rows)
            //    if (dr[0].ToString().Contains("-"))
            //        dr[1] = BaseService.Decrypt(dr[1].ToString());
            Globals.dtGMDriver = ds.Tables["dtGMDriver"];

            Globals.dtAsset = ds.Tables["dtAsset"];
            Globals.dtDriver = ds.Tables["dtDriver"];
            Globals.dtMatrix = ds.Tables["dtMatrix"];
        }

        public static String GetSvrDate(String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = "select getdate()";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtServerDate";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            String s = "Not yet connected";
            try
            {
                DateTime dt = (DateTime)ds.Tables["dtServerDate"].Rows[0][0];
                s = dt.ToString();
            }
            catch
            {
                s = "Could not connect";
            }

            return s;
        }

        public void Resync_AssetList(List<Matrix> lMatrix, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = new AssetService().sGetAsset(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtAssets";
            dtSql.Rows.Add(drSql);

            sql = new AssetDeviceMapService().sGetAssetDeviceMap(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtAssetMap";
            dtSql.Rows.Add(drSql);

            sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Assets);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMAsset";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            DataTable dtAsset = ds.Tables["dtAssets"];
            //foreach (DataRow dr in dtAsset.Rows)
            //    dr["AssetNumber"] = BaseService.Decrypt(dr["AssetNumber"].ToString());
            List<Asset> la = new myConverter().ConvertToList<Asset>(dtAsset);

            DataTable dtDeviceMap = ds.Tables["dtAssetMap"];
            List<AssetDeviceMap> ldm = new myConverter().ConvertToList<AssetDeviceMap>(dtDeviceMap);
            foreach (Asset a in la)
                a.assetDeviceMap = ldm.FindAll(o => o.AssetID == a.AssetID).FirstOrDefault();
            Globals.lAsset = la;
        }
    }
}










