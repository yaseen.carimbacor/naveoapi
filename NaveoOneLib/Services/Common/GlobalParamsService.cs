using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using NaveoOneLib.Models.Common;
using System.Data;
using System.Data.Common;

namespace NaveoOneLib.Services
{
    public class GlobalParamsService
    {
        Errors er = new Errors();
       public  GlobalParam GetMe(DataRow dr)
        {
            GlobalParam gp = new GlobalParam();
            gp.Iid = Convert.ToInt32(dr["iid"]);
            gp.ParamName = dr["ParamName"].ToString();
            gp.PValue = dr["PValue"].ToString();
            if (gp.ParamName.ToUpper().Substring(0, 3).Equals("PWD"))
                gp.PValue = BaseService.DecryptMe(gp.PValue);

            gp.ParamComments = dr["ParamComments"].ToString();
            return gp;
        }

        public DataTable GetGlobalParams(String sConnStr)
        {
            String sqlString = @"SELECT iid, 
                                   ParamName, 
                                   PValue, 
                                   ParamComments from GFI_SYS_GlobalParams order by iid";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public GlobalParam GetGlobalParamsById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT iid, 
                                        ParamName, 
                                        PValue, 
                                        ParamComments from GFI_SYS_GlobalParams
                                        WHERE iid = '" + iID + "' order by iid";
            DataTable dt = new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetMe(dt.Rows[0]);
        }
        public GlobalParam GetGlobalParamsByName(String sParamName, String sConnStr)
        {
            String sqlString = @"SELECT iid, 
                                        ParamName, 
                                        PValue, 
                                        ParamComments from GFI_SYS_GlobalParams
                                        WHERE Paramname = ?";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, sParamName, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetMe(dt.Rows[0]);
        }
        public String sGetGlobalParamsByName(String sParamName)
        {
            String sqlString = @"SELECT iid, 
                                        ParamName, 
                                        PValue, 
                                        ParamComments from GFI_SYS_GlobalParams
                                        WHERE ParamName = '" + sParamName + "'";
            return sqlString;
        }
        public static void GetSMTPSettings(String sConnStr)
        {
            if (Globals.SmtpClientPort != -1)
                return;

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = "select * from GFI_SYS_GlobalParams";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGlobalParams";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            foreach (DataRow dr in ds.Tables["dtGlobalParams"].Rows)
            {
                switch (dr["ParamName"].ToString())
                {
                    case "SmtpClientHost":
                        Globals.SmtpClientHost = dr["PValue"].ToString();
                        break;

                    case "SmtpClientPort":
                        Globals.SmtpClientPort = Convert.ToInt16(dr["PValue"].ToString());
                        break;

                    case "SmtpClientEnableSsl":
                        String s = dr["PValue"].ToString();
                        if (s == "1")
                            Globals.SmtpClientEnableSsl = true;
                        else
                            Globals.SmtpClientEnableSsl = false;
                        break;

                    case "SmtpClientUsrName":
                        Globals.SmtpClientUsrName = dr["PValue"].ToString();
                        break;

                    case "PwdSmtpClientUsr":
                        Globals.SmtpClientUsrPwd = BaseService.DecryptMe(dr["PValue"].ToString());
                        break;

                    case "SmtpClientSenderMail":
                        Globals.SmtpClientSenderMail = dr["PValue"].ToString();
                        break;
                }
            }
        }

        public Boolean SaveGlobalParams(GlobalParam uGlobalParams, Boolean bInsert, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_GlobalParams";
            dt.Columns.Add("iid", uGlobalParams.Iid.GetType());
            dt.Columns.Add("ParamName", uGlobalParams.ParamName.GetType());
            dt.Columns.Add("PValue", uGlobalParams.PValue.GetType());
            dt.Columns.Add("ParamComments", uGlobalParams.ParamComments.GetType());

            if (uGlobalParams.ParamName.ToUpper().Substring(0, 3).Equals("PWD"))
                uGlobalParams.PValue = BaseService.EncryptMe(uGlobalParams.PValue);

            DataRow dr = dt.NewRow();
            dr["iid"] = uGlobalParams.Iid;
            dr["ParamName"] = uGlobalParams.ParamName;
            dr["PValue"] = uGlobalParams.PValue;
            dr["ParamComments"] = uGlobalParams.ParamComments;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "IID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uGlobalParams.Iid;

            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }

        public bool UpdateGlobalParams(GlobalParam uGlobalParams, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_GlobalParams";
            //dt.Columns.Add("iid", uGlobalParams.Iid.GetType());
            dt.Columns.Add("ParamName", uGlobalParams.ParamName.GetType());
            dt.Columns.Add("PValue", uGlobalParams.PValue.GetType());
            dt.Columns.Add("ParamComments", uGlobalParams.ParamComments.GetType());
            DataRow dr = dt.NewRow();
            //dr["iid"] = uGlobalParams.Iid;
            dr["ParamName"] = uGlobalParams.ParamName;
            dr["PValue"] = uGlobalParams.PValue;
            dr["ParamComments"] = uGlobalParams.ParamComments;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "iid = '" + uGlobalParams.Iid + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }

        public Boolean UpdateGlobalParam(GlobalParam uGlobalParam)
        {

            return true;
        }
        public bool DeleteGlobalParams(GlobalParam uGlobalParams, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_SYS_GlobalParams", "iid = '" + uGlobalParams.Iid + "'", sConnStr))
                return true;
            else
                return false;
        }
    }
}