﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Security;
using System.Security.Cryptography;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Windows.Forms;
using System.Threading;
using Org.BouncyCastle.Crypto;
using System.IO;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Crypto.Encodings;
using Org.BouncyCastle.Crypto.Engines;

//////////////////////////////////////////////////////////////////////////
// Base Serive class to provide for custom functions avalaible thoughout the app
// E.g fillcombo
//
//
//
//
//
/////////////////////////////////////////////////////////////////////////

namespace NaveoOneLib.Services
{
    public class BaseService
    {
        Errors er = new Errors();

        public static string strLacle = "14naveocore";

        // old key

        // static string strLacle = "cornaveoeng";

        public static string EncryptMe(string PlainText)
        {
            TripleDES des = CreateDES(strLacle);

            ICryptoTransform ct = des.CreateEncryptor();

            byte[] input = Encoding.Unicode.GetBytes(PlainText.Trim());
            byte[] buffer = ct.TransformFinalBlock(input, 0, input.Length);

            return Convert.ToBase64String(buffer);
        }

        public static string DecryptMe(string CypherText)
        {
            try
            {
                byte[] b = Convert.FromBase64String(CypherText.Trim());

                TripleDES des = CreateDES(strLacle);
                ICryptoTransform ct = des.CreateDecryptor();
                byte[] output = ct.TransformFinalBlock(b, 0, b.Length);

                string strCheck = Encoding.Unicode.GetString(output);
                return Encoding.Unicode.GetString(output);
            }
            catch { }
            return String.Empty;
        }

        public static string Encrypt(string PlainText)
        {
            return (PlainText);
        }

        public static string Decrypt(string CypherText)
        {
            return (CypherText);
        }

        public static string RSADecrypt(String encrypt)
        {
            var bytesToDecrypt = Convert.FromBase64String(encrypt); // string to decrypt, base64 encoded

            AsymmetricCipherKeyPair keyPair;

            TextReader reader = new StringReader(@"-----BEGIN RSA PRIVATE KEY-----
  MIICWwIBAAKBgQC9qk2AeigGBRn9rAlMz6e54RhNGHSCqdbS40ZIeu+G8cooLC+p
  Ie9BQuVRiB+nh5TB2eNiImVAl7S7YqZzgAIIjcuQ03/jUcZwb5ERbRtcDvC7tOAD
  dJbTvewBtvy/LdRGm3Yb0xxs7r2sxSS7/TDW7u7uY1sQ5WD/uDX2uRbEIwIDAQAB
  AoGAWwCcoRl7nK/T8bAa4jz8eCZ1t4AVXyBIj/U2os5BoVjgK3hQAfHjT0feqckL
  jyFxkbWO8WYZeO2RbRt/TAnObyGpyzmSwJ14bRPx8NtayY8wJ/s970mU37Z9YLQw
  w598MRgvqaza6uguA35szCoQD1S8FgYXdBpU2dKIr8e2eukCQQD0c8K6CbVbmTCu
  ly3YBIt3jTujKIIkfpOwGqopFMC+P7+q+Osvz51Rhky/rBAHnnA8x4ThA+aQcQlL
  ohQNRF1/AkEAxp/8YRJiGJbaKjed/0UAItudODecWHiATztGEg75uFbvKPNBObZG
  JjFQAx/UhRVGnwsM2DXAlmavN9Oi+J+zXQJANB5+VRX+0eHOjFr1TkphxgKZxYa8
  gLVwHasBvrKLCcUjw3tBAbedTiV5NaCk5kr0mTDc9vDS9tby6WpMJiv+VwJAfd3A
  FtuJYj+l/H7GFo9pKK22kXtcDgw+cvcCzkrDIHemknKj8Kh7B5TUOUaga70xxXNB
  LnnRkv17J4triNXa2QJAaMeUxwZfFVCtsDaol1WZngXjPYFsXGTupIckrghLYfLv
  qQ2Ky3u/tSMnholLjXLlyzFU5ECRcM9SXsREOJuYZA==
  -----END RSA PRIVATE KEY-----");

            //using (TextReader reader = File.OpenText(Globals.GetNaveoOnePath() + @"\Naveo\privatekey.pem")) // file containing RSA PKCS1 private key
            keyPair = (AsymmetricCipherKeyPair)new PemReader(reader).ReadObject();

            var decryptEngine = new Pkcs1Encoding(new RsaEngine());
            decryptEngine.Init(false, keyPair.Private);

            var decrypted = Encoding.UTF8.GetString(decryptEngine.ProcessBlock(bytesToDecrypt, 0, bytesToDecrypt.Length));
            return decrypted;
        }


        static TripleDES CreateDES(string key)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            TripleDES des = new TripleDESCryptoServiceProvider();
            des.Key = md5.ComputeHash(Encoding.Unicode.GetBytes(key));
            des.IV = new byte[des.BlockSize / 8];
            return des;
        }

        public static DataTable PopulateCBO(string tableName, string id, string value, string condition, String sConnStr)
        {
            string sqlString = " SELECT " + id + "," + value +
                               " FROM " + tableName +
                               " WHERE " + condition +
                               " ORDER BY 2";

            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        public static void fillCombo(string tableName, string id, string value, string condition, ComboBox cmbox, String sConnStr)
        {
            DataTable dt = PopulateCBO(tableName, id, value, condition, sConnStr);
            cmbox.DataSource = dt;
            cmbox.DisplayMember = dt.Columns[value].ColumnName;
            cmbox.ValueMember = dt.Columns[id].ColumnName;
            cmbox.Refresh();
            cmbox.SelectedIndex = -1;

            cmbox.DropDownStyle = ComboBoxStyle.DropDown;
            cmbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            cmbox.AutoCompleteSource = AutoCompleteSource.ListItems;
        }

        public static void fillCombo(string tableName, string id, string value, string condition, Telerik.WinControls.UI.RadDropDownList cmbox, String sConnStr)
        {
            DataTable dt = PopulateCBO(tableName, id, value, condition, sConnStr);
            cmbox.DataSource = dt;
            cmbox.DisplayMember = dt.Columns[value].ColumnName;
            cmbox.ValueMember = dt.Columns[id].ColumnName;
            cmbox.Refresh();
            cmbox.SelectedIndex = -1;

            cmbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
        }

        public static void fillComboConCat(string tableName, string id, string value, string condition, ComboBox cmbox, String sConnStr)
        {
            DataTable dt = PopulateCBO(tableName, id, id + " +'-'+ " + value + " as " + value, condition, sConnStr);
            cmbox.DataSource = dt;

            //Reza added. Query having join 2 tables
            String[] sTable = tableName.Split(',');
            if (sTable.Length > 1)
            {
                cmbox.DisplayMember = dt.Columns[1].ColumnName;
                cmbox.ValueMember = dt.Columns[0].ColumnName;
            }
            else
            {
                cmbox.DisplayMember = dt.Columns[value].ColumnName;
                cmbox.ValueMember = dt.Columns[id].ColumnName;
            }


            cmbox.Refresh();
            cmbox.SelectedIndex = -1;

            cmbox.DropDownStyle = ComboBoxStyle.DropDown;
            cmbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            cmbox.AutoCompleteSource = AutoCompleteSource.ListItems;

        }

        public static void fillComboConCatCust(string tableName, string id, string value, string condition, ComboBox cmbox, String sConnStr)
        {
            DataTable dt = PopulateCBO(tableName, id, value + " +'-'+ " + id + " as " + value, condition, sConnStr);
            cmbox.DataSource = dt;

            //Reza added. Query having join 2 tables
            String[] sTable = tableName.Split(',');
            if (sTable.Length > 1)
            {
                cmbox.DisplayMember = dt.Columns[1].ColumnName;
                cmbox.ValueMember = dt.Columns[0].ColumnName;
            }
            else
            {
                cmbox.DisplayMember = dt.Columns[value].ColumnName;
                cmbox.ValueMember = dt.Columns[id].ColumnName;
            }


            cmbox.Refresh();
            cmbox.SelectedIndex = -1;

            cmbox.DropDownStyle = ComboBoxStyle.DropDown;
            cmbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            cmbox.AutoCompleteSource = AutoCompleteSource.ListItems;

        }
        public static void fillComboWithMultipleTables(String tableNames, String ValueMember, String DisplayMember, String condition, ComboBox cmbox, String sConnStr)
        {
            //Reza added. Query having join multiple tables
            String[] sTable = tableNames.Split(',');
            String[] strCol = ValueMember.Split(',');
            String[] strMem = DisplayMember.Split(',');
            String[] strCon = condition.Split(',');
            String sql = String.Empty;
            int i = 0;
            foreach (String t in sTable)
            {
                sql += " select " + strCol[i].Trim() + "," + strMem[i].Trim();
                sql += " FROM " + t + " WHERE " + strCon[i].Trim() + " Union ";
                i++;
            }
            sql = sql.Substring(0, sql.Length - 6);
            DataTable dt = new Connection(sConnStr).GetDataDT(sql, sConnStr);

            cmbox.DataSource = dt;
            cmbox.DisplayMember = dt.Columns[strMem[0]].ColumnName;
            cmbox.ValueMember = dt.Columns[strCol[0]].ColumnName;
            cmbox.SelectedIndex = -1;
            cmbox.Refresh();


            cmbox.DropDownStyle = ComboBoxStyle.DropDown;
            cmbox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            cmbox.AutoCompleteSource = AutoCompleteSource.ListItems;

        }

        public static void ClearTextBoxes(Control control)
        {
            foreach (Control c in control.Controls)
            {
                if (c is System.Windows.Forms.TextBox)
                {

                    ((System.Windows.Forms.TextBox)c).ForeColor = System.Drawing.Color.Black;
                    ((System.Windows.Forms.TextBox)c).BackColor = System.Drawing.Color.White;
                    ((System.Windows.Forms.TextBox)c).Text = "";
                    //((System.Windows.Forms.TextBox)c).CharacterCasing = CharacterCasing.Upper;

                }

                if (c is System.Windows.Forms.ComboBox)
                {
                    ((System.Windows.Forms.ComboBox)c).ForeColor = System.Drawing.Color.Black;
                    ((System.Windows.Forms.ComboBox)c).BackColor = System.Drawing.Color.White;
                    ((System.Windows.Forms.ComboBox)c).SelectedIndex = -1;
                    // ((System.Windows.Forms.ComboBox)c). = CharacterCasing.Upper;

                }

                if (c is System.Windows.Forms.CheckBox)
                {

                    ((System.Windows.Forms.CheckBox)c).Checked = false;

                }

                if (c is System.Windows.Forms.GroupBox)
                {
                    ClearTextBoxes(c);

                }

                if (c.HasChildren)
                {
                    ClearTextBoxes(c);
                }
            }
        }

        public static void LockControls(Control control)
        {
            // control.Enabled = false;

            foreach (Control c in control.Controls)
            {
                if (c is System.Windows.Forms.TextBox)
                {

                    ((System.Windows.Forms.TextBox)c).ForeColor = System.Drawing.Color.Black;
                    ((System.Windows.Forms.TextBox)c).BackColor = System.Drawing.Color.LightGray;
                    ((System.Windows.Forms.TextBox)c).Enabled = false;

                }

                if (c is System.Windows.Forms.ComboBox)
                {
                    ((System.Windows.Forms.ComboBox)c).ForeColor = System.Drawing.Color.Black;
                    ((System.Windows.Forms.ComboBox)c).BackColor = System.Drawing.Color.LightGray;
                    ((System.Windows.Forms.ComboBox)c).Enabled = false;
                }

                if (c is System.Windows.Forms.DateTimePicker)
                {

                    ((System.Windows.Forms.DateTimePicker)c).Enabled = false;

                }

                if (c is System.Windows.Forms.CheckBox)
                {

                    ((System.Windows.Forms.CheckBox)c).Enabled = false;

                }

                if (c is System.Windows.Forms.GroupBox)
                {
                    LockControls(c);
                }

                if (c.HasChildren)
                {
                    LockControls(c);
                }
            }
        }

        public static void UnLockControls(Control control)
        {
            control.Enabled = true;

            foreach (Control c in control.Controls)
            {
                if (c is System.Windows.Forms.TextBox)
                {

                    ((System.Windows.Forms.TextBox)c).ForeColor = System.Drawing.Color.Black;
                    ((System.Windows.Forms.TextBox)c).BackColor = System.Drawing.Color.White;
                    ((System.Windows.Forms.TextBox)c).Enabled = true;
                    //((System.Windows.Forms.TextBox)c).CharacterCasing = CharacterCasing.Upper;

                }

                if (c is System.Windows.Forms.ComboBox)
                {
                    ((System.Windows.Forms.ComboBox)c).ForeColor = System.Drawing.Color.Black;
                    ((System.Windows.Forms.ComboBox)c).BackColor = System.Drawing.Color.White;
                    ((System.Windows.Forms.ComboBox)c).Enabled = true;
                    ((System.Windows.Forms.ComboBox)c).SelectedIndex = -1;
                }

                if (c is System.Windows.Forms.DateTimePicker)
                {

                    ((System.Windows.Forms.DateTimePicker)c).Enabled = true;

                }

                if (c is System.Windows.Forms.CheckBox)
                {

                    ((System.Windows.Forms.CheckBox)c).Enabled = true;

                }

                if (c is System.Windows.Forms.GroupBox)
                {
                    UnLockControls(c);
                }

                if (c.HasChildren)
                {
                    UnLockControls(c);
                }


            }
        }

        public static string GetCheckBox(CheckBox chkBox)
        {
            if (chkBox.Checked == true)
                return Constants.strActive;
            else
                return Constants.strNotActive;

        }

        public static bool SetCheckBox(string strCheck)
        {
            if (strCheck == Constants.strActive)
                return true;
            else if (strCheck == Constants.strNotActive)
                return false;
            else
                return false;
        }

        public static string GenUniqueCode(string strTableName, bool bDate, string strPrefix, String sConnStr)
        {
            //String Function to return unik code for strTablename
            //Use date or not in code based on bDate
            //Add strPrefix as prefix as defined in constants

            string strCode = "";
            string sqlString = "";
            if (bDate)
            {
                sqlString = "  SELECT CONVERT(VARCHAR(11), SYSDATETIME(), 112) + replicate('0', 4  - len(cast( count(*)+1  as nvarchar))) + cast( count(*)+1  as nvarchar) as rowno " +
                                 "    FROM " + strTableName +
                                 "   WHERE convert(date, CreatedDate) = convert(date,GETDATE())";
            }
            else
            {
                sqlString = "  SELECT  replicate('0', 3 - len(cast( count(*)+1  as nvarchar))) + cast( count(*)+1  as nvarchar) as rowno " +
                       "    FROM " + strTableName +
                       "   WHERE convert(date, CreatedDate) = convert(date,GETDATE())";

            }

            DataTable dt = new Connection(sConnStr).GetDataDT(sqlString, sConnStr);

            strCode = strPrefix + dt.Rows[0]["rowno"].ToString();
            return strCode;

        }


    }
}
