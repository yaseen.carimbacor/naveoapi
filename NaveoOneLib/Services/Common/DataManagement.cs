﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NaveoOneLib.DBCon;
using System.Data;
using NaveoOneLib.Common;
using System.IO;

namespace NaveoOneLib.Services
{
    public class DataManagement
    {
        public static String ProcessArchiving(List<int> lAssetID, String strDate, String sConnStr, int MaxMinutestoRun = 120, int MaxMinutesPerAsset = 12, Boolean bScheduled = true)
        {
            DateTime dtStart = DateTime.Now;
            String lblFinish = String.Empty;

            try
            {
                try
                {
                    FileInfo f = new FileInfo(Globals.LogPath() + "Archive.txt");
                    if (f.Length > 100000)
                        File.Delete(Globals.LogPath() + "Archive.txt");

                    DeleteInvalidData(sConnStr);

                    File.Delete(Globals.LogPath() + "ArchiveRunning.txt");
                }
                catch { }

                foreach (int i in lAssetID)
                {
                    try
                    {
                        String s = " AssetID : " + i + " started @" + DateTime.Now + " ended @";
                        lblFinish += DateTime.Now + " " + ProcessArchivingL(i, strDate, MaxMinutesPerAsset, sConnStr) + " for AssetID " + i + "\r\n";
                        File.AppendAllText(Globals.LogPath() + "ArchiveRunning.txt", s + DateTime.Now + "\r\n");

                        if (MaxMinutestoRun > 0)
                            if (Math.Abs(DateTime.Now.Minute - dtStart.Minute) >= MaxMinutestoRun)
                            {
                                lblFinish += MaxMinutestoRun + "mins reached. Stopped @ " + DateTime.Now + " with AssetID " + i + "\r\n";
                                break;
                            }

                        if (bScheduled)
                        {
                            if (DateTime.Now.Hour > 6)
                                if (DateTime.Now.Hour < 18)
                                {
                                    lblFinish += "6 am reached. Stopped @ " + DateTime.Now + " with AssetID " + i + "\r\n";
                                    File.AppendAllText(Globals.LogPath() + "ArchiveRunning.txt", "6 am reached. Stopped @ " + DateTime.Now + " with AssetID " + i + "\r\n");
                                    break;
                                }
                        }
                    }
                    catch (Exception ex)
                    {
                        lblFinish += DateTime.Now + " Error [" + ex.ToString() + "] for AssetID " + i + "\r\n";
                        File.AppendAllText(Globals.LogPath() + "ArchiveRunning.txt", DateTime.Now + " Error [" + ex.ToString() + "] for AssetID " + i + "\r\n");
                    }
                }
            }
            catch { lblFinish += DateTime.Now + " Error Detected..."; }

            File.AppendAllText(Globals.LogPath() + "ArchiveRunning.txt", DateTime.Now + " Completed \r\n");
            return lblFinish;
        }
        public static String ProcessPurge(List<int> lAssetID, DateTime dtDate, Boolean bDeleteAsset, String sConnStr)
        {
            String lblFinish = String.Empty;
            foreach (int i in lAssetID)
            {
                List<int> l = new List<int>();
                l.Add(i);
                lblFinish += ProcessPurgeL(l, dtDate, bDeleteAsset, sConnStr) + " for AssetID " + i + "\r\n";
            }

            return lblFinish;
        }

        static void DeleteInvalidData(String sConnStr)
        {
            String lblFinish = String.Empty;
            String sql = @"
Declare @dtStart datetime
set @dtStart = getdate()
delete top(100) GFI_GPS_GPSData where DateTimeGPS_UTC < '2010-04-05 15:53:28.857'
while @@rowcount > 0
begin
	delete top(100) GFI_GPS_GPSData where DateTimeGPS_UTC < '2010-04-05 15:53:28.857'

	if(DATEDIFF(SS, @dtStart, GETDATE()) > 120)
		break;
end

set @dtStart = getdate()
delete top(100) GFI_GPS_GPSData where DateTimeGPS_UTC > '2050-04-05 15:53:28.857'
while @@rowcount > 0
begin
	delete top(100) GFI_GPS_GPSData where DateTimeGPS_UTC > '2050-04-05 15:53:28.857'

	if(DATEDIFF(SS, @dtStart, GETDATE()) > 120)
		break;
end";

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["Command"] = sql;
            drCtrl["TimeOut"] = 800;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dtCtrl);

            DataSet dsResult = new DataSet();
            dsResult = c.ProcessData(dsProcess, sConnStr);
        }

        static String ProcessArchivingL(int iAssetID, String strDate, int MaxMinutesPerAsset, String sConnStr)
        {
            if (MaxMinutesPerAsset < 0)
                MaxMinutesPerAsset = 120;

            String lblFinish = String.Empty;
            String sqlAsset = @"
--Archive
CREATE TABLE #TripHeader
(
	[iID] [int] PRIMARY KEY CLUSTERED,
	[dtStart] [DateTime]
)

CREATE TABLE #GPSData
(
	[UID] [int] PRIMARY KEY CLUSTERED,
	[AssetID] [int],
	[DateTimeGPS_UTC] [DateTime]
)

CREATE TABLE #Assets
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL,
	[iAssetID] [int] PRIMARY KEY CLUSTERED
)

insert #Assets (iAssetID) values (" + iAssetID.ToString() + ")\r\n";

            String sql = sqlAsset + "\r\n";
            sql += "\r\n";
            sql += @"
declare @dtStart datetime
declare @MaxRows int
declare @inTrans int
declare @dtTripEnd datetime
declare @GPSIDTripEnd int

set @dtStart = getdate()

set @MaxRows = 5

--Declare @Run int
--set @Run = 0

set @inTrans = 0

--prepare (@MaxRows) line migrations
select 1
while @@ROWCOUNT > 0
begin
	if(DATEDIFF(MI, @dtStart, GETDATE()) > " + MaxMinutesPerAsset + @")
		break;

	Begin Try
        --set @Run = @Run + 1
	    select top 1 @dtTripEnd = dtEnd, @GPSIDTripEnd = GPSDataEndUID
		    from (
				    select top (@MaxRows) h.* from GFI_GPS_TripHeader h
					    inner join #Assets a on h.AssetID = a.iAssetID
				    where h.dtStart < '" + strDate + " 00:00:01'" + @"
					    order by h.dtStart asc
				    ) x
	    order by dtStart desc
	    select @dtTripEnd, @GPSIDTripEnd

	    --Trips
		begin tran
	            set @inTrans = 1
                
				delete from #TripHeader
				insert #TripHeader (iID, dtStart)
					select top (@MaxRows) h.iID, h.dtStart from GFI_GPS_TripHeader h
			            inner join #Assets a on h.AssetID = a.iAssetID
		            where h.dtStart <= @dtTripEnd and GPSDataEndUID <= @GPSIDTripEnd
		            order by h.dtStart

                insert GFI_ARC_TripHeader (iID, AssetID, DriverID, ExceptionFlag, MaxSpeed, IdlingTime, StopTime, TripDistance, TripTime, GPSDataStartUID, GPSDataEndUID, dtStart, fStartLon, fStartLat, dtEnd, fEndLon, fEndLat, OverSpeed1Time, OverSpeed2Time, Accel, Brake, Corner, DepartureAddress, ArrivalAddress)
	                select top (@MaxRows) h.* from GFI_GPS_TripHeader h
			            inner join #Assets a on h.AssetID = a.iAssetID
		            where h.dtStart <= @dtTripEnd and GPSDataEndUID <= @GPSIDTripEnd
		            order by h.dtStart

	            insert GFI_ARC_TripDetail (iID, HeaderiID, GPSDataUID)
		            select d.* from #TripHeader h
			            --inner join #Assets a on h.AssetID = a.iAssetID
			            inner join GFI_GPS_TripDetail d on h.iID = d.HeaderiID
		            --where h.dtStart <= @dtTripEnd and GPSDataEndUID <= @GPSIDTripEnd
			            order by h.dtStart;

	            select 1
	            while @@ROWCOUNT > 0
	            begin
		            delete top ((@MaxRows)) GFI_GPS_TripHeader
			            from GFI_GPS_TripHeader h
				            inner join #Assets a on h.AssetID = a.iAssetID
			            where h.dtStart <= @dtTripEnd and GPSDataEndUID <= @GPSIDTripEnd
	            end
        commit tran
        set @inTrans = 0

        --Exceptions
		begin tran
	        set @inTrans = 1
		    insert GFI_ARC_Exceptions (iID, RuleID, GPSDataID, AssetID, DriverID, Severity, GroupID, Posted, InsertedAt, DateTimeGPS_UTC)
			    select h.* from GFI_GPS_Exceptions h
				    inner join #Assets a on h.AssetID = a.iAssetID
			    where h.InsertedAt <= @dtTripEnd and GPSDataID <= @GPSIDTripEnd;

	        --notification

	        --GPSData
			delete from #GPSData
			insert #GPSData
			    select g.UID, g.AssetID, g.DateTimeGPS_UTC from GFI_GPS_GPSData g 
					    inner join #Assets a ON g.AssetID = a.iAssetID
				    where g.DateTimeGPS_UTC <= @dtTripEnd and g.UID <= @GPSIDTripEnd;

		    insert GFI_ARC_GPSData (UID, AssetID, DateTimeGPS_UTC, DateTimeServer, Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, StopFlag, TripDistance, TripTime, WorkHour, DriverID, RoadSpeed)
			    select g.* from GFI_GPS_GPSData g 
					    inner join #Assets a ON g.AssetID = a.iAssetID
				    where g.DateTimeGPS_UTC <= @dtTripEnd and g.UID <= @GPSIDTripEnd;

	        --GPSDataDetail
		    insert GFI_ARC_GPSDataDetail (GPSDetailID, UID, TypeID, TypeValue, UOM, Remarks)
			    select d.* from #GPSData g
					    inner join #Assets a ON g.AssetID = a.iAssetID
					    inner join GFI_GPS_GPSDataDetail d on g.UID = d.UID
				    where g.DateTimeGPS_UTC <= @dtTripEnd and g.UID <= @GPSIDTripEnd;

	        select 1
	        while @@ROWCOUNT > 0
	        begin
		        delete top ((@MaxRows)) GFI_GPS_GPSData
			        from GFI_GPS_GPSData g 
				        inner join #Assets a ON g.AssetID = a.iAssetID
			        where g.DateTimeGPS_UTC <= @dtTripEnd and g.UID <= @GPSIDTripEnd;
	        end

            --Deleting from GFI_SYS_Notification
	        select 1
	        while @@ROWCOUNT > 0
	        begin
		        delete top ((@MaxRows)) GFI_SYS_Notification
		        from GFI_SYS_Notification h
			        inner join #Assets a on h.AssetID = a.iAssetID
		        where h.InsertedAt < @dtTripEnd;
	        end

	        select 1
	        while @@ROWCOUNT > 0
	        begin
		        delete top ((@MaxRows)) GFI_GPS_Exceptions
		        from GFI_GPS_Exceptions h
			        inner join #Assets a on h.AssetID = a.iAssetID
		        where h.InsertedAt < @dtTripEnd;
	        end

        commit tran
        set @inTrans = 0

	   --Audit
		begin tran
	        set @inTrans = 1

		    insert GFI_ARC_Audit (auditId, auditUser, auditDate, auditType, ReferenceCode, ReferenceDesc, ReferenceTable, CreatedDate, CallerFunction, SQLRemarks, NewValue, OldValue, PostedDate, PostedFlag, UserID)
			    select a.* from GFI_SYS_Audit a 
				    where a.auditDate <= @dtTripEnd;

	        select 1
	        while @@ROWCOUNT > 0
	        begin
		        delete top ((@MaxRows)) GFI_SYS_Audit
			        where auditDate <= @dtTripEnd;
	        end
        commit tran
        set @inTrans = 0
	end try
	begin catch
        if(@inTrans = 1)
        begin
		    rollback tran
        end

		begin try
			Declare @MyAssetID int
			select @MyAssetID = max(iAssetID) from #Assets
			insert GFI_GPS_ProcessErrors (sError, sOrigine, AssetID, dtUTC) values (ERROR_MESSAGE(), 'Archiving' , @MyAssetID, @dtStart)
		end try
		begin catch
		end catch

	end catch

    --restart loop if needed
	select top 1 h.* from GFI_GPS_TripHeader h
		inner join #Assets a on h.AssetID = a.iAssetID
	where h.dtStart < '" + strDate + " 00:00:01'" + @"
end

--select @Run Run
";
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["Command"] = sql;
            drCtrl["TimeOut"] = (MaxMinutesPerAsset * 60) + 10;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dtCtrl);

            DataSet dsResult = c.ProcessData(dsProcess, sConnStr, false);
            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            if (bResult)
                lblFinish = "Completed successfully. RecAffected : " + dtCtrl.Rows[0]["RecAffected"].ToString();
            else
                lblFinish = dtCtrl.Rows[0]["ExceptionInfo"].ToString() + " RecAffected : " + dtCtrl.Rows[0]["RecAffected"].ToString();

            return lblFinish;
        }
        static String ProcessPurgeL(List<int> lAssetID, DateTime dtDate, Boolean bDeleteAsset, String sConnStr)
        {
            if (bDeleteAsset)
                dtDate = DateTime.Now.AddYears(10);
            String strDate = dtDate.ToString("dd-MMM-yyyy");

            String lblFinish = String.Empty;
            String sqlAsset = @"
CREATE TABLE #Assets
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL,
	[iAssetID] [int] PRIMARY KEY CLUSTERED,
)
";
            String sAsset = String.Empty;
            foreach (int i in lAssetID)
                sAsset += "insert #Assets (iAssetID) values (" + i.ToString() + ")\r\n";

            String sql = sqlAsset + sAsset + @"
select 1
while @@ROWCOUNT > 0
begin
	delete top (100) GFI_GPS_TripHeader
		from GFI_GPS_TripHeader h 
			inner join #Assets a ON h.AssetID = a.iAssetID
		where h.dtStart < '" + strDate + @" 00:00:01'
 end;";

            sql += @"
select 1
while @@ROWCOUNT > 0
begin
	delete top (100) GFI_SYS_Notification
	    from GFI_SYS_Notification n
		    inner join #Assets a on n.AssetID = a.iAssetID
	    where n.DateTimeGPS_UTC < '" + strDate + @" 00:00:01'
end;";

            sql += @"
select 1
while @@ROWCOUNT > 0
begin
    delete top (1000) GFI_GPS_Exceptions
	    from GFI_GPS_Exceptions e
		    inner join #Assets a on e.AssetID = a.iAssetID
	    where e.DateTimeGPS_UTC < '" + strDate + @" 00:00:01'
end;";

            sql += @"
select 1
while @@ROWCOUNT > 0
begin
    delete top (100) GFI_GPS_GPSData
	    from GFI_GPS_GPSData g 
		    inner join #Assets a ON g.AssetID = a.iAssetID
	    where g.DateTimeGPS_UTC < '" + strDate + @" 00:00:01'
end;";

            sql += @"
select 1
while @@ROWCOUNT > 0
begin
    delete top (100) GFI_GPS_LiveData
	    from GFI_GPS_LiveData g 
		    inner join #Assets a ON g.AssetID = a.iAssetID
	    where g.DateTimeGPS_UTC < '" + strDate + @" 00:00:01'
end;";

            //Archive Tables
            sql += @"
select 1
while @@ROWCOUNT > 0
begin
    delete top (100) GFI_ARC_TripHeader
	    from GFI_ARC_TripHeader h
		    inner join #Assets a on h.AssetID = a.iAssetID
	    where h.dtStart < '" + strDate + @" 00:00:01'
end;";

            sql += @"
select 1
while @@ROWCOUNT > 0
begin
    delete top (1000) GFI_ARC_Exceptions
	    from GFI_ARC_Exceptions e
		    inner join #Assets a on e.AssetID = a.iAssetID
	    where e.DateTimeGPS_UTC < '" + strDate + @" 00:00:01'
end;";

            sql += @"
select 1
while @@ROWCOUNT > 0
begin
    delete top (100) GFI_ARC_GPSData
	    from GFI_ARC_GPSData g 
		    inner join #Assets a ON g.AssetID = a.iAssetID
	    where g.DateTimeGPS_UTC < '" + strDate + @" 00:00:01'
end;";
            if (bDeleteAsset)
            {
                sql += @"
    delete GFI_FLT_Asset
	    from GFI_FLT_Asset g 
		    inner join #Assets a ON g.AssetID = a.iAssetID
";
                sql = sql.Replace("where", "--where");

                sql += "delete GFI_GPS_Rules from GFI_GPS_Rules r inner join #Assets a on a.iAssetID = r.StrucValue where Struc = 'Assets' and StrucType = 'ID'";
            }

            /*
select 1, count(10) from GFI_GPS_TripHeader h inner join #Assets a ON h.AssetID = a.iAssetID
select 2, count(10) from GFI_SYS_Notification n inner join #Assets a on n.AssetID = a.iAssetID
select 3, count(10) from GFI_GPS_Exceptions e inner join #Assets a on e.AssetID = a.iAssetID
select 4, count(10) from GFI_GPS_GPSData g inner join #Assets a ON g.AssetID = a.iAssetID
select 5, count(10) from GFI_GPS_LiveData g inner join #Assets a ON g.AssetID = a.iAssetID
select 6, count(10) from GFI_ARC_TripHeader h inner join #Assets a on h.AssetID = a.iAssetID
select 7, count(10) from GFI_ARC_Exceptions e inner join #Assets a on e.AssetID = a.iAssetID
select 8, count(10)from GFI_ARC_GPSData g inner join #Assets a ON g.AssetID = a.iAssetID
select * from GFI_GPS_Rules where ParentRuleID in
	(select ParentRuleID from GFI_GPS_Rules r inner join #Assets a on a.iAssetID = r.StrucValue where Struc = 'Assets' and StrucType = 'ID')
            */

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["Command"] = sql;
            drCtrl["TimeOut"] = 800; // 999999999;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dtCtrl);

            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            if (bResult)
                lblFinish = "Completed successfully. RecAffected : " + dtCtrl.Rows[0]["RecAffected"].ToString();
            else
                lblFinish = dtCtrl.Rows[0]["ExceptionInfo"].ToString() + " RecAffected : " + dtCtrl.Rows[0]["RecAffected"].ToString();

            return lblFinish;
        }

        public static int MaxAssetID(String sConnStr)
        {
            int iMax = 100;
            try
            {
                Connection c = new Connection(sConnStr);
                DataTable dtSql = c.dtSql();
                DataRow drSql = dtSql.NewRow();

                String sql = "select max(AssetID) from GFI_FLT_Asset";
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "dtResult";
                dtSql.Rows.Add(drSql);

                DataSet ds = c.GetDataDS(dtSql, sConnStr);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        int.TryParse(ds.Tables[0].Rows[0][0].ToString(), out iMax);
            }
            catch { }
            return iMax;
        }
        public static List<int> lAssetIDs(String sConnStr)
        {
            List<int> l = new List<int>();
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = "select AssetID from GFI_FLT_Asset";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtResult";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds != null)
                if (ds.Tables.Count > 0)
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        int i = -1;
                        int.TryParse(dr["AssetID"].ToString(), out i);
                        l.Add(i);
                    }
            return l;
        }
    }
}
