﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;
using System.Windows.Forms;

using NaveoOneLib.Models;
using NaveoOneLib.Common;
using NaveoOneLib.Models.Common;

namespace NaveoOneLib.Services
{
    public class MailSender
    {
        public String SendMail(String Adressee, String Title, String strBody, String strFileName, String sConnStr)
        {
            String s = String.Empty;
            GlobalParamsService.GetSMTPSettings(sConnStr);

            // Init the smtp client and setting the network credentials
            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Host = Globals.SmtpClientHost;
            smtpClient.Port = Globals.SmtpClientPort;
            smtpClient.EnableSsl = Globals.SmtpClientEnableSsl;
            if (!String.IsNullOrEmpty(Globals.SmtpClientUsrName))
                smtpClient.Credentials = new NetworkCredential(Globals.SmtpClientUsrName, Globals.SmtpClientUsrPwd);

            //NetworkCredential credential = new NetworkCredential();
            //credential.UserName = "naveo.mu1@gmail.com";
            //credential.Password = "Nav4dmin1234";
            //smtpClient.Credentials = credential;
            //smtpClient.Credentials = new NetworkCredential("Naveo75@gmail.com", "ebsadmin");
            //smtpClient.Credentials = new NetworkCredential("noreply@naveo.mu", "ebs4dmin");

            //GOC
            //smtpclient.EnableSsl = false;
            //smtpclient.Port = 25;
            //smtpclient.Host = "202.123.27.104";
            //email.Sender = email.From = email.ReplyTo = new MailAddress("NaveoNoReply@govmu.org", "NaveoNoReply@govmu.org");

            // Create MailMessage
            MailMessage message = new MailMessage();
            try
            {
                if (Validations.EmailValidation.IsEmail(Adressee))
                {
                    message.Body = strBody;
                    message.To.Add(new MailAddress(Adressee, Adressee.Substring(0, Adressee.IndexOf("@"))));
                    message.Subject = Title;
                    message.From = new MailAddress(Globals.SmtpClientSenderMail, "NAVEO ONE System");

                    if (strFileName != String.Empty)
                    {
                        ContentType contentType = new ContentType();
                        contentType.MediaType = MediaTypeNames.Application.Octet;
                        if (strFileName.EndsWith(".zip"))
                            contentType.MediaType = MediaTypeNames.Application.Zip;

                        String extension = Path.GetExtension(strFileName);
                        contentType.Name = "NaveoReport" + extension;

                        Attachment attachment = new Attachment(strFileName, contentType);
                        message.Attachments.Add(attachment);
                        smtpClient.Timeout = 1000000;
                    }

                    // Send Mail via SmtpClient
                    smtpClient.Send(message);
                    message.Dispose();
                    s = "Mail Sent";
                }
                else
                {
                    s = "Could not send. Unknown reason";
                    s = "Could not send. Invalid Address";
                }
            }
            catch (Exception ex)
            {
                s = "Could not send..." + ex.ToString();
            }
            finally
            {
                message.Dispose();
            }

            return s;
        }
        public void SendMailAsync(String Adressee, String Title, String strBody, String strFileName, String sConnStr)
        {
            String s = String.Empty;
            GlobalParamsService.GetSMTPSettings(sConnStr);

            // Init the smtp client and setting the network credentials
            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Host = Globals.SmtpClientHost;
            smtpClient.Port = Globals.SmtpClientPort;
            smtpClient.EnableSsl = Globals.SmtpClientEnableSsl;
            if (!String.IsNullOrEmpty(Globals.SmtpClientUsrName))
                smtpClient.Credentials = new NetworkCredential(Globals.SmtpClientUsrName, Globals.SmtpClientUsrPwd);

            MailMessage message = new MailMessage();
            try
            {
                if (Validations.EmailValidation.IsEmail(Adressee))
                {
                    message.Body = strBody;
                    message.To.Add(new MailAddress(Adressee, Adressee.Substring(0, Adressee.IndexOf("@"))));
                    message.Subject = Title;
                    message.From = new MailAddress(Globals.SmtpClientSenderMail, "NAVEO ONE System");

                    if (strFileName != String.Empty)
                    {
                        smtpClient.Timeout = 1000000;
                        Attachment attachment = new Attachment(strFileName);
                        message.Attachments.Add(attachment);
                    }

                    // Send Mail via SmtpClient
                    Object state = message;
                    smtpClient.SendCompleted += new SendCompletedEventHandler(smtpClient_SendCompleted);
                    smtpClient.SendAsync(message, state);
                }
            }
            catch { }
        }
        void smtpClient_SendCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            try
            {
                MailMessage mail = e.UserState as MailMessage;
                if (!e.Cancelled && e.Error != null)
                {
                }
                mail.Dispose();
            }
            catch { }
        }
    }
}