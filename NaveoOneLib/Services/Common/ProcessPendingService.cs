using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NaveoService.Services;
using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.GPS;

namespace NaveoOneLib.Services
{
    public class ProcessPendingService
    {
        Errors er = new Errors();
        public DataTable GetProcessPending(String sConnStr)
        {
            String sqlString = @"SELECT iID, 
							 AssetID, 
							 dtDateFrom, 
							 ProcessCode, 
							 Processing from GFI_GPS_ProcessPending order by iID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public ProcessPending GetProcessPendingById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT iID, 
							 AssetID, 
							 dtDateFrom, 
							 ProcessCode, 
							 Processing from GFI_GPS_ProcessPending
							 WHERE iID = ?
							 order by iID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                ProcessPending retProcessPending = new ProcessPending();
                retProcessPending.iID = Convert.ToInt32(dt.Rows[0]["iID"]);
                retProcessPending.AssetID = Convert.ToInt32(dt.Rows[0]["AssetID"]);
                retProcessPending.dtDateFrom = (DateTime)dt.Rows[0]["dtDateFrom"];
                retProcessPending.ProcessCode = dt.Rows[0]["ProcessCode"].ToString();
                retProcessPending.Processing = Convert.ToInt32(dt.Rows[0]["Processing"]);
                return retProcessPending;
            }
        }
        public bool SaveProcessPending(ProcessPending uProcessPending, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_GPS_ProcessPending";
            dt.Columns.Add("iID", uProcessPending.iID.GetType());
            dt.Columns.Add("AssetID", uProcessPending.AssetID.GetType());
            dt.Columns.Add("dtDateFrom", uProcessPending.dtDateFrom.GetType());
            dt.Columns.Add("ProcessCode", uProcessPending.ProcessCode.GetType());
            dt.Columns.Add("Processing", uProcessPending.Processing.GetType());
            DataRow dr = dt.NewRow();
            dr["iID"] = uProcessPending.iID;
            dr["AssetID"] = uProcessPending.AssetID;
            dr["dtDateFrom"] = uProcessPending.dtDateFrom;
            dr["ProcessCode"] = uProcessPending.ProcessCode;
            dr["Processing"] = uProcessPending.Processing;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenInsert(dt, transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool UpdateProcessPending(ProcessPending uProcessPending, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_GPS_ProcessPending";
            dt.Columns.Add("iID", uProcessPending.iID.GetType());
            dt.Columns.Add("AssetID", uProcessPending.AssetID.GetType());
            dt.Columns.Add("dtDateFrom", uProcessPending.dtDateFrom.GetType());
            dt.Columns.Add("ProcessCode", uProcessPending.ProcessCode.GetType());
            dt.Columns.Add("Processing", uProcessPending.Processing.GetType());
            DataRow dr = dt.NewRow();
            dr["iID"] = uProcessPending.iID;
            dr["AssetID"] = uProcessPending.AssetID;
            dr["dtDateFrom"] = uProcessPending.dtDateFrom;
            dr["ProcessCode"] = uProcessPending.ProcessCode;
            dr["Processing"] = uProcessPending.Processing;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "iID = '" + uProcessPending.iID + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool DeleteProcessPending(ProcessPending uProcessPending, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_GPS_ProcessPending", "iID = '" + uProcessPending.iID + "'", sConnStr))
                return true;
            else
                return false;
        }

        public Boolean RegisterDevicesForProcessing(int? AssetID, DateTime utcDateTime, String strProcessCode, DbTransaction tran, String sConnStr)
        {
            Connection myConn = new Connection(sConnStr);
            String sql = "exec RegisterDevicesForProcessing @AssetID = " + AssetID.ToString();
            sql += ", @dtDateFrom = '" + String.Format("{0:yyyy-MMM-dd HH:mm:ss}", utcDateTime);
            sql += "', @ProcessCode = '" + strProcessCode + "'";
            int iRAffected = myConn.dbExecute(sql, tran, sConnStr);
            //if (iRAffected > 0)
            return true;
            //else
            //    return false;
        }
        public Boolean RegisterDevicesForLive(Int32 UID, GPSData uGPSData, List<GPSDataDetail> GPSDetails, Connection myConn, DbTransaction tran, String sConnStr)
        {
            DataTable dtLive = new DataTable();
            dtLive.TableName = "GFI_GPS_LiveData";
            dtLive.Columns.Add("UID", uGPSData.UID.GetType());
            dtLive.Columns.Add("AssetID", uGPSData.AssetID.GetType());
            dtLive.Columns.Add("DriverID", uGPSData.DeviceID.GetType());
            dtLive.Columns.Add("DateTimeGPS_UTC", uGPSData.DateTimeGPS_UTC.GetType());
            dtLive.Columns.Add("DateTimeServer", uGPSData.DateTimeServer.GetType());
            dtLive.Columns.Add("Longitude", uGPSData.Longitude.GetType());
            dtLive.Columns.Add("Latitude", uGPSData.Latitude.GetType());
            dtLive.Columns.Add("LongLatValidFlag", uGPSData.LongLatValidFlag.GetType());
            dtLive.Columns.Add("Speed", uGPSData.Speed.GetType());
            dtLive.Columns.Add("EngineOn", uGPSData.EngineOn.GetType());
            dtLive.Columns.Add("StopFlag", uGPSData.StopFlag.GetType());
            dtLive.Columns.Add("TripDistance", uGPSData.TripDistance.GetType());
            dtLive.Columns.Add("TripTime", uGPSData.TripTime.GetType());
            dtLive.Columns.Add("WorkHour", uGPSData.WorkHour.GetType());

            DataRow drLive = dtLive.NewRow();
            drLive["UID"] = UID;
            drLive["AssetID"] = uGPSData.AssetID;
            drLive["DriverID"] = uGPSData.DriverID;
            drLive["DateTimeGPS_UTC"] = uGPSData.DateTimeGPS_UTC;
            drLive["DateTimeServer"] = DateTime.Now;
            drLive["Longitude"] = uGPSData.Longitude;
            drLive["Latitude"] = uGPSData.Latitude;
            drLive["LongLatValidFlag"] = uGPSData.LongLatValidFlag;
            drLive["Speed"] = uGPSData.Speed;
            drLive["EngineOn"] = uGPSData.EngineOn;
            drLive["StopFlag"] = 9;// uGPSData.StopFlag;
            drLive["TripDistance"] = 0;// uGPSData.TripDistance;
            drLive["TripTime"] = 0;// uGPSData.TripTime;
            drLive["WorkHour"] = uGPSData.WorkHour;
            dtLive.Rows.Add(drLive);

            GPSDataDetail gd = new GPSDataDetail();
            DataTable dtLiveDetails = new DataTable();
            dtLiveDetails.TableName = "GFI_GPS_LiveDataDetail";
            dtLiveDetails.Columns.Add("UID", gd.UID.GetType());
            dtLiveDetails.Columns.Add("TypeID", gd.TypeID.GetType());
            dtLiveDetails.Columns.Add("TypeValue", gd.TypeValue.GetType());
            dtLiveDetails.Columns.Add("UOM", gd.UOM.GetType());
            dtLiveDetails.Columns.Add("Remarks", gd.Remarks.GetType());

            //Connection myConn = new Connection(sConnStr);
            Boolean bResult = myConn.GenInsert(dtLive, tran, sConnStr);

            if (bResult)
            {
                DataRow drLiveDetails = dtLiveDetails.NewRow();

                foreach (GPSDataDetail g in GPSDetails)
                {
                    drLiveDetails = dtLiveDetails.NewRow();
                    drLiveDetails["UID"] = UID;
                    drLiveDetails["TypeID"] = g.TypeID;
                    drLiveDetails["TypeValue"] = g.TypeValue;
                    drLiveDetails["UOM"] = g.UOM;
                    drLiveDetails["Remarks"] = g.Remarks;
                    dtLiveDetails.Rows.Add(dtLiveDetails);
                    bResult = bResult & myConn.GenInsert(dtLiveDetails, tran, sConnStr);
                }
            }

            String sql = @";with cte as (
						  select uid, AssetID, 
							 DriverID,
							 DateTimeGPS_UTC, 
							 LongLatValidFlag, 
							 row_number() over(partition by AssetID order by DateTimeGPS_UTC desc) as RowNum
						   from GFI_GPS_LiveData
						  WHERE AssetID = " + uGPSData.AssetID.ToString() + @"
							 and DateTimeGPS_UTC < getdate() + 1
							 and LongLatValidFlag = 1 
							 )
					   delete from GFI_GPS_LiveData 
						   where uid in
							   (select uid from cte where RowNum > 6)";
            int iRAffected = myConn.dbExecute(sql, tran, sConnStr);
            return true;
        }

        public String ProcessDevices(String sConnStr)
        {
            String sResult = String.Empty;
            try
            {
                int iRAffected = 0;
                Connection c = new Connection(sConnStr);

                DataTable dtAssets = c.GetDataDT(NaveoIntel.ProcessIntel.sGetDevicesToBeProcessed("Trips"), sConnStr);
                foreach (DataRow dr in dtAssets.Rows)
                {
                    int iAssetID = 0;
                    int.TryParse(dr["AssetID"].ToString(), out iAssetID);
                    try
                    {
                        iRAffected += c.dbExecuteWithoutTransaction(NaveoIntel.ProcessIntel.sProcessTrips(iAssetID), 600, sConnStr);
                        sResult += "Process Trips ok for AssetID " + iAssetID + " - " + iRAffected.ToString() + "\r\n";
                    }
                    catch (Exception ex)
                    {
                        sResult += "Process Trips Failed for AssetID " + iAssetID + " - " + ex.Message + "\r\n";
                    }
                }
            }
            catch (Exception ex)
            {
                sResult += "\r\n Trips failed with error " + ex.Message;
            }
            return sResult;
        }

        public String ProcessExceptions(String sConnStr)
        {
            String sResult = String.Empty;
            try
            {
                int iRAffected = 0;
                Connection c = new Connection(sConnStr);

                DataTable dtAssets = c.GetDataDT(NaveoIntel.ProcessIntel.sGetDevicesToBeProcessed("Exceptions"), sConnStr);
                foreach (DataRow dr in dtAssets.Rows)
                {
                    int iAssetID = 0;
                    int.TryParse(dr["AssetID"].ToString(), out iAssetID);
                    try
                    {
                        iRAffected += c.dbExecuteWithoutTransaction(NaveoIntel.ProcessIntel.sProcessExceptions(iAssetID), 600, sConnStr);
                        sResult += DateTime.Now + " Process Exceptions ok for AssetID " + iAssetID + " - " + iRAffected.ToString() + "\r\n";
                    }
                    catch (Exception ex)
                    {
                        sResult += DateTime.Now + "Process Exceptions Failed for AssetID " + iAssetID + " - " + ex.Message + "\r\n";
                        Errors.WriteToErrorLog(ex);
                    }
                }
                if (String.IsNullOrEmpty(sResult))
                    sResult += "No exceptions to process";

                DateTime dateTime = DateTime.Now;

                try
                {
                    sResult += DateTime.Now + " Processing Planning Exceptions... \r\n";
                    BaseModel bmplanningExcepitions = new NaveoOneLib.Services.Rules.ExceptionsService().getPlanningExceptions(sConnStr);
                    sResult += bmplanningExcepitions.additionalData.ToString();
                    TimeSpan ts = DateTime.Now - dateTime;
                    sResult += DateTime.Now + " Processing Planning Exceptions completed  after " + ts.Duration().ToString() + "\r\n";

                } catch(Exception ex)
                {
                    sResult += DateTime.Now + "Processing Planning Exceptions Failed  - " + ex.Message + "\r\n";
                    Errors.WriteToErrorLog(ex);
                }


                DateTime dateTime1 = DateTime.Now;
                try
                {
                    sResult += DateTime.Now + " Processing Alert Maintenance... \r\n";
                    var status = new NaveoOneLib.Services.Rules.AlertService().GenerateAlertMaintenance(sConnStr);
                    sResult += status.ToString();
                    TimeSpan ts = DateTime.Now - dateTime1;
                    sResult += DateTime.Now + " Processing Alert Maintenance completed  after " + ts.Duration().ToString() + "\r\n";

                }
                catch (Exception ex)
                {
                    sResult += DateTime.Now + "Processing Alert Maintenance Failed  - " + ex.Message + "\r\n";
                    Errors.WriteToErrorLog(ex);
                }



               

                c.dbExecuteWithoutTransaction(NaveoIntel.ProcessIntel.sProcessNotReportedExceptions(), 600, sConnStr);
                //c.dbExecuteWithoutTransaction(NaveoIntel.ProcessIntel.sProcessMaintenanceExceptions(), 600, sConnStr);
            }
            catch (Exception ex)
            {
                sResult += "\r\n Exceptions failed with error " + ex.Message;
            }
            return sResult;
        }

        public Boolean updateRoadSpeed(String sConnStr)
        {
            Boolean bResult = true;

            try
            {
                GlobalParam g = new GlobalParam();
                g = new GlobalParamsService().GetGlobalParamsByName("RoadSpeed", sConnStr);
                if (g != null)
                {
                    Connection c = new Connection(sConnStr);
                    DataTable dtSql = c.dtSql();
                    DataRow drSql = dtSql.NewRow();

                    //String sql = "select top 3000 UID, AssetID, Longitude, Latitude, RoadSpeed from GFI_GPS_GPSData where DateTimeGPS_UTC >= DATEADD(dd, -1, DATEDIFF(dd, 0, GETDATE())) and UID >= " + g.PValue + " order by UID";
                    String sql = "select top 3000 UID, AssetID, Longitude, Latitude, RoadSpeed from GFI_GPS_GPSData where UID >= " + g.PValue + " and RoadSpeed is null order by UID";
                           //sql = "select top 3000 UID, AssetID, Longitude, Latitude, RoadSpeed from GFI_GPS_GPSData where datetimeserver >= DATEADD(dd, -1, DATEDIFF(dd, 0, GETDATE())) and UID >= " + g.PValue + " order by UID";
                           //sql = "select UID, AssetID, Longitude, Latitude, RoadSpeed from GFI_GPS_GPSData where UID >= 64926779";
                    drSql = dtSql.NewRow();
                    drSql["sSQL"] = sql;
                    drSql["sTableName"] = "dtDebug";
                    dtSql.Rows.Add(drSql);

                    DataSet ds = c.GetDataDS(dtSql, sConnStr);

                    NaveoOneLib.Maps.ESRIMapService esriMap = new NaveoOneLib.Maps.ESRIMapService();
                    Boolean b = esriMap.LoadMapNeeded();
                    if (b)
                        esriMap.LoadMap();
                    esriMap.ReadProjectNLoadMap();

                    sql = @"
--Update Road Speed
create table #Debug
(
    [UID] [int] NOT NULL,
    [AssetID] int null,
    [RoadSpeed] [int] NULL,
    [RoadType] nvarchar(25) NULL,
    [FinalRoadSpeed] [int] NULL,
    [TypeRoadSpeed] [int] NULL
)

--Oracle Starts Here
";                    
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        try
                        {
                            if (String.IsNullOrEmpty(dr["RoadSpeed"].ToString()))
                            {
                                String[] RoadSpeed = esriMap.GetRoadSpeed((Double)dr["Longitude"], (Double)dr["Latitude"]);
                                int iSpeed = -1;
                                int.TryParse(RoadSpeed[0], out iSpeed);
                                if (iSpeed > 0)
                                    sql += "insert #Debug (UID, AssetID, RoadSpeed, RoadType) values (" + dr["UID"].ToString() + ", " + dr["AssetID"].ToString() + ", " + iSpeed.ToString() + ", '" + RoadSpeed[1] + "');\r\n";
                            }
                        }
                        catch
                        {
                            bResult = false;
                        }
                    }

                    esriMap.UnloadProject();
                    esriMap = null;

                    if (bResult)
                    {
                        sql += "\r\n";
                        sql += @"
update #Debug
    set TypeRoadSpeed = r.MotorWay
from #Debug d
    inner join GFI_GPS_GPSData g on d.UID = g.UID
	inner join GFI_FLT_Asset a on a.AssetID = g.AssetID
	inner join GFI_FLT_RoadSpeedType r on r.iID = a.RoadSpeedType
where d.RoadType = 'MotorWay'

update #Debug
    set TypeRoadSpeed = r.RoadA
from #Debug d
    inner join GFI_GPS_GPSData g on d.UID = g.UID
	inner join GFI_FLT_Asset a on a.AssetID = g.AssetID
	inner join GFI_FLT_RoadSpeedType r on r.iID = a.RoadSpeedType
where d.RoadType = 'RoadA'

update #Debug
    set TypeRoadSpeed = r.RoadB
from #Debug d
    inner join GFI_GPS_GPSData g on d.UID = g.UID
	inner join GFI_FLT_Asset a on a.AssetID = g.AssetID
	inner join GFI_FLT_RoadSpeedType r on r.iID = a.RoadSpeedType
where d.RoadType = 'RoadB'

update #Debug
    set TypeRoadSpeed = r.RoadC
from #Debug d
    inner join GFI_GPS_GPSData g on d.UID = g.UID
	inner join GFI_FLT_Asset a on a.AssetID = g.AssetID
	inner join GFI_FLT_RoadSpeedType r on r.iID = a.RoadSpeedType
where d.RoadType = 'RoadC'

update #Debug
    set TypeRoadSpeed = r.RoadD
from #Debug d
    inner join GFI_GPS_GPSData g on d.UID = g.UID
	inner join GFI_FLT_Asset a on a.AssetID = g.AssetID
	inner join GFI_FLT_RoadSpeedType r on r.iID = a.RoadSpeedType
where d.RoadType = 'RoadD'

update #Debug
    set TypeRoadSpeed = r.RoadE
from #Debug d
    inner join GFI_GPS_GPSData g on d.UID = g.UID
	inner join GFI_FLT_Asset a on a.AssetID = g.AssetID
	inner join GFI_FLT_RoadSpeedType r on r.iID = a.RoadSpeedType
where d.RoadType = 'RoadE'

update #Debug
    set TypeRoadSpeed = r.RoadF
from #Debug d
    inner join GFI_GPS_GPSData g on d.UID = g.UID
	inner join GFI_FLT_Asset a on a.AssetID = g.AssetID
	inner join GFI_FLT_RoadSpeedType r on r.iID = a.RoadSpeedType
where d.RoadType = 'RoadF'

update #Debug
    set TypeRoadSpeed = r.RoadG
from #Debug d
    inner join GFI_GPS_GPSData g on d.UID = g.UID
	inner join GFI_FLT_Asset a on a.AssetID = g.AssetID
	inner join GFI_FLT_RoadSpeedType r on r.iID = a.RoadSpeedType
where d.RoadType = 'RoadG'

update #Debug
    set TypeRoadSpeed = r.RoadH
from #Debug d
    inner join GFI_GPS_GPSData g on d.UID = g.UID
	inner join GFI_FLT_Asset a on a.AssetID = g.AssetID
	inner join GFI_FLT_RoadSpeedType r on r.iID = a.RoadSpeedType
where d.RoadType = 'RoadH'

--min from multiple columns
;with cte as
(
	SELECT UID,
				(
					SELECT MIN([value].[MinValue])
					FROM
					(
						VALUES
							(RoadSpeed),
							(TypeRoadSpeed)
					) AS [value] ([MinValue])
			   ) AS [MinValue]
	FROM #Debug
)
update #Debug 
	set FinalRoadSpeed = cte.MinValue
from #Debug d
	inner join cte on cte.UID = d.UID
-- end of minFromMultipleColumns

UPDATE GFI_GPS_GPSData 
    SET RoadSpeed = T2.FinalRoadSpeed
FROM GFI_GPS_GPSData T1
INNER JOIN  #Debug T2 ON T2.UID = T1.UID
";

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            sql += "\r\n";
                            sql += "update GFI_SYS_GlobalParams set PValue = " + ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1]["UID"] + " where ParamName = 'RoadSpeed'";
                        }
                        dtSql.Rows.Clear();
                        drSql = dtSql.NewRow();
                        drSql["sSQL"] = sql;
                        drSql["sTableName"] = "MultipleDTfromQuery";
                        dtSql.Rows.Add(drSql);

                        ds = c.GetDataDS(dtSql, sConnStr);
                        if (ds.Tables.Contains("dtErr"))
                            bResult = false;
                    }
                }
            }
            catch
            {
                bResult = false;
            }
            return bResult;
        }

        public BaseModel updateTripAddress(String sConnStr)
        {
            Boolean bResult = true;
            BaseModel bm = new Models.Common.BaseModel();

            try
            {
                GlobalParam g = new GlobalParam();
                g = new GlobalParamsService().GetGlobalParamsByName("TripAddress", sConnStr);
                if (g != null)
                {
                    Connection c = new Connection(sConnStr);
                    DataTable dtSql = c.dtSql();
                    DataRow drSql = dtSql.NewRow();

                    //-5 as test. should be 0
                    String sql = "select top 1000 iID, fStartLon, fStartLat, fEndLon, fEndLat from GFI_GPS_TripHeader where dtStart >= DATEADD(dd, -5, DATEDIFF(dd, 0, GETDATE())) and ArrivalAddress is null and iID >= " + g.PValue + " order by iID";
                    drSql = dtSql.NewRow();
                    drSql["sSQL"] = sql;
                    drSql["sTableName"] = "dtTrips";
                    dtSql.Rows.Add(drSql);

                    sql = "select top 1000 * from GFI_GPS_TripWizNoAddress where 1 = 1 order by iID";
                    drSql = dtSql.NewRow();
                    drSql["sSQL"] = sql;
                    drSql["sTableName"] = "TripWizNoAddress";
                    dtSql.Rows.Add(drSql);

                    sql = @"delete from GFI_GPS_TripWizNoAddress
                            	where iid in (select top 1000 iID from GFI_GPS_TripWizNoAddress where 1 = 1 order by iID)";
                    drSql = dtSql.NewRow();
                    drSql["sSQL"] = sql;
                    drSql["sTableName"] = "dtDeleted";
                    dtSql.Rows.Add(drSql);

                    DataSet ds = c.GetDataDS(dtSql, sConnStr);

                    NaveoOneLib.Maps.ESRIMapService esriMap = new NaveoOneLib.Maps.ESRIMapService();
                    Boolean b = esriMap.LoadMapNeeded();
                    if (b)
                        esriMap.LoadMap();
                    esriMap.ReadProjectNLoadMap();

                    sql = @"--TripAddresses
                            create table #Trips
                            (
                                [iID] [int] NOT NULL,
                                [DepartureAddress] nvarchar(250) NULL,
                                [ArrivalAddress] nvarchar(250) NULL
                            )

                            ";
                    foreach (DataRow dr in ds.Tables["dtTrips"].Rows)
                    {
                        try
                        {
                            String sDeparture = esriMap.GetAddress(Convert.ToDouble(dr["fStartLon"]), Convert.ToDouble(dr["fStartLat"])).Replace("'", " ");
                            String sArrival = esriMap.GetAddress(Convert.ToDouble(dr["fEndLon"]), Convert.ToDouble(dr["fEndLat"])).Replace("'", " ");
                            String s = "insert #Trips values (" + dr["iID"].ToString() + ", ";
                            if (!String.IsNullOrEmpty(sDeparture.Trim()))
                            {
                                if (sDeparture.Length > 250)
                                    sDeparture = sDeparture.Substring(0, 240) + "CUT";
                                s += "'" + sDeparture.Trim() + "', ";
                            }
                            else
                                s += "null, ";

                            if (!String.IsNullOrEmpty(sArrival.Trim()))
                            {
                                if (sArrival.Length > 250)
                                    sArrival = sDeparture.Substring(0, 240) + "CUT";
                                s += "'" + sArrival.Trim() + "'";
                            }
                            else
                                s += "null";

                            s += ") --Oracle Add SemiColumn \r\n";

                            sql += s;
                        }
                        catch (Exception ex)
                        {
                            Errors.WriteToErrorLog(ex);
                        }
                    }

                    sql += "\r\n";
                    sql += @"create table #TripWizNoAddress
                            (
                                [iID] [int] NOT NULL,
                                [DepartureAddress] nvarchar(250) NULL,
                                [ArrivalAddress] nvarchar(250) NULL
                            )

                            ";
                    foreach (DataRow dr in ds.Tables["TripWizNoAddress"].Rows)
                    {
                        try
                        {
                            String sDeparture = String.Empty;
                            if (!String.IsNullOrEmpty(dr["fStartLon"].ToString()))
                                sDeparture = esriMap.GetAddress(Convert.ToDouble(dr["fStartLon"]), Convert.ToDouble(dr["fStartLat"])).Replace("'", " ");

                            String sArrival = String.Empty;
                            if (!String.IsNullOrEmpty(dr["fEndLon"].ToString()))
                                sArrival = esriMap.GetAddress(Convert.ToDouble(dr["fEndLon"]), Convert.ToDouble(dr["fEndLat"])).Replace("'", " ");

                            String s = "insert #TripWizNoAddress values (" + dr["HeaderID"].ToString() + ", ";
                            if (!String.IsNullOrEmpty(sDeparture.Trim()))
                            {
                                if (sDeparture.Length > 250)
                                    sDeparture = sDeparture.Substring(0, 240) + "CUT";
                                s += "'" + sDeparture.Trim() + "', ";
                            }
                            else
                                s += "null, ";

                            if (!String.IsNullOrEmpty(sArrival.Trim()))
                            {
                                if (sArrival.Length > 250)
                                    sArrival = sDeparture.Substring(0, 240) + "CUT";
                                s += "'" + sArrival.Trim() + "'";
                            }
                            else
                                s += "null";

                            s += ") --Oracle Add SemiColumn \r\n";

                            sql += s;
                        }
                        catch (Exception ex)
                        {
                            Errors.WriteToErrorLog(ex);
                        }
                    }

                    esriMap.UnloadProject();
                    esriMap = null;

                    sql += "\r\n";
                    sql += @"
UPDATE GFI_GPS_TripHeader 
    SET DepartureAddress = T2.DepartureAddress, ArrivalAddress = T2.ArrivalAddress
FROM GFI_GPS_TripHeader T1
    INNER JOIN #Trips T2 ON T2.iID = T1.iID

UPDATE GFI_GPS_TripHeader 
    SET DepartureAddress = T2.DepartureAddress
FROM GFI_GPS_TripHeader T1
    INNER JOIN #TripWizNoAddress T2 ON T2.iID = T1.iID
where T2.DepartureAddress is not null

UPDATE GFI_GPS_TripHeader 
    SET ArrivalAddress = T2.ArrivalAddress
FROM GFI_GPS_TripHeader T1
INNER JOIN #TripWizNoAddress T2 ON T2.iID = T1.iID
    where T2.ArrivalAddress is not null
";

                    if (ds.Tables["dtTrips"].Rows.Count > 0)
                    {
                        sql += "\r\n";
                        sql += "update GFI_SYS_GlobalParams set PValue = " + ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1]["iID"] + " where ParamName = 'TripAddress'";
                    }

                    dtSql.Rows.Clear();
                    drSql = dtSql.NewRow();
                    drSql["sSQL"] = sql;
                    drSql["sTableName"] = "MultipleDTfromQuery";
                    dtSql.Rows.Add(drSql);

                    ds = c.GetDataDS(dtSql, sConnStr, 300);
                    if (ds.Tables.Contains("dtErr"))
                    {
                        Errors.WriteToLog(sql);
                        bResult = false;
                        bm.dbResult = ds;
                    }
                }
            }
            catch (Exception ex)
            {
                bResult = false;
                bm.errorMessage += ex.Message;
            }

            bm.data = bResult;
            return bm;
        }
    }
}
