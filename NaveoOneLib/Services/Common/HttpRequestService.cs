﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Script.Serialization;
using System.Net;
using NaveoOneLib.Common;
using System.Linq;

namespace NaveoOneLib.Services
{
    public class HttpRequestService
    {
        public class Body
        {
            public string message { get; set; }
            public string email { get; set; }
        }

        public static String SendPushNotifOnMobile(Body myBody, String sConnStr)
        {
            if (Constants.bIsDevEnvironment)
                return "Dev " + SendPushNotifFromDevSvr(myBody, sConnStr);
            return "Live " + SendPushNotifFromLiveSvr(myBody, sConnStr);
        }

        public static String SendPushNotifFromLiveSvr(Body myBody, String sConnStr)
        {
            //Body myBody = new Body() { message = "my message xxx", email = "support@naveo.mu" };
            if (!new Services.Users.UserService().isMobileUser(myBody.email, sConnStr))
                return "Not Sent. Email not registered on mobile devices.";
            //Link
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://api.wearemobimove.com/mm/api/notification/send");//http://www.phongphong.com/mm/api/notification/send

            //Body
            string json = new JavaScriptSerializer().Serialize(myBody);
            byte[] data = Encoding.ASCII.GetBytes(json);

            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = data.Length;

            //Headers
            //API key/secret :  naveoapi:YWguz5noYot2hPmd > https://www.base64encode.org/
            //new one //API key/secret :  naveoapi:sHXdNLv7qM7VfHRF > //Basic bmF2ZW9hcGk6N25BQ0JQS3lyUWFUMFFXSG9SOWNBQTBKQ1haS2Zqa2I=
            // request.Headers.Add("Authorization", "Basic bmF2ZW9hcGk6WVdndXo1bm9Zb3QyaFBtZA==");//bmF2ZW9hcGk6c0hYZE5MdjdxTTdWZkhSRg==
            request.Headers.Add("Authorization", "Basic bmF2ZW9hcGk6N25BQ0JQS3lyUWFUMFFXSG9SOWNBQTBKQ1haS2Zqa2I=");
            request.Headers.Add("X-APPLICATION-CODE", "NAVEO");

            //Sending
            using (System.IO.Stream stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            String responseString = String.Empty;
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                responseString = response.StatusDescription;
            }
            catch (Exception ex)
            {
                responseString = ex.ToString();
            }
            return responseString;
        }
        public static String SendPushNotifFromDevSvr(Body myBody, String sConnStr)
        {
            //Body myBody = new Body() { message = "my message xxx", email = "support@naveo.mu" };
            if (!new Services.Users.UserService().isMobileUser(myBody.email, sConnStr))
                return "Not Sent. Email not registered on mobile devices.";
            //Link
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://www.phongphong.com/mm/api/notification/send");

            //Body
            string json = new JavaScriptSerializer().Serialize(myBody);
            byte[] data = Encoding.ASCII.GetBytes(json);

            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = data.Length;

            //Headers
            //API key/secret :  naveoapi:YWguz5noYot2hPmd > https://www.base64encode.org/
            request.Headers.Add("Authorization", "Basic bmF2ZW9hcGk6c0hYZE5MdjdxTTdWZkhSRg==");
            request.Headers.Add("X-APPLICATION-CODE", "NAVEO");

            //Sending
            using (System.IO.Stream stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            String responseString = String.Empty;
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                responseString = response.StatusDescription;
            }
            catch (Exception ex)
            {
                responseString = ex.ToString();
            }
            return responseString;
        }

        #region usin Async. Commented
        //using System.Threading.Tasks;
        //using System.Net.Http;

        //var x = await SendPushNotifOnMobileAsync(new Body() { message = "my message 123", email = "support@naveo.mu" });
        //public static async Task<String> SendPushNotifOnMobileAsync(Body myBody)
        //{
        //    string url = "http://www.phongphong.com/mm/api/notification/send";
        //    Body cust = new Body() { message = "my message", email = "support@naveo.mu" };
        //    var json = new JavaScriptSerializer().Serialize(myBody);
        //    Dictionary<string, string> h = new Dictionary<string, string>();
        //    h.Add("Authorization", "Basic bmF2ZW9hcGk6c0hYZE5MdjdxTTdWZkhSRg==");
        //    h.Add("X-APPLICATION-CODE", "NAVEO");
        //    HttpResponseMessage response = await PushNotifOnMobile(HttpMethod.Post, url, json, h);
        //    string responseText = response.ToString();
        //    //string responseText = await response.Content.ReadAsStringAsync();
        //    return responseText;
        //}
        ///// <summary>
        ///// Makes an async HTTP Request
        ///// </summary>
        ///// <param name="pMethod">Those methods you know: GET, POST, HEAD, etc...</param>
        ///// <param name="pUrl">Very predictable...</param>
        ///// <param name="pJsonContent">String data to POST on the server</param>
        ///// <param name="pHeaders">If you use some kind of Authorization you should use this</param>
        ///// <returns></returns>
        //public static async Task<HttpResponseMessage> PushNotifOnMobile(HttpMethod pMethod, string pUrl, string pJsonContent, Dictionary<string, string> pHeaders)
        //{
        //    HttpClient _Client = new HttpClient();
        //    var httpRequestMessage = new HttpRequestMessage();
        //    httpRequestMessage.Method = pMethod;
        //    httpRequestMessage.RequestUri = new Uri(pUrl);
        //    foreach (var head in pHeaders)
        //    {
        //        httpRequestMessage.Headers.Add(head.Key, head.Value);
        //    }
        //    switch (pMethod.Method)
        //    {
        //        case "POST":
        //            HttpContent httpContent = new StringContent(pJsonContent, Encoding.UTF8, "application/json");
        //            httpRequestMessage.Content = httpContent;
        //            break;
        //    }

        //    var s = await _Client.SendAsync(httpRequestMessage);
        //    return s;
        //}
        #endregion
    }

    public static class Ssl
    {
        private static readonly string[] TrustedHosts = new[]
        {
            "RezaHost.NaveoDomain.com"
            , "159.8.86.167"
            , "localhost"
        };

        public static void EnableTrustedHosts()
        {
            ServicePointManager.ServerCertificateValidationCallback =
            (sender, certificate, chain, errors) =>
            {
                if (errors == System.Net.Security.SslPolicyErrors.None)
                {
                    return true;
                }

                var request = sender as HttpWebRequest;
                if (request != null)
                {
                    return TrustedHosts.Contains(request.RequestUri.Host);
                }

                return false;
            };
        }
    }

}
