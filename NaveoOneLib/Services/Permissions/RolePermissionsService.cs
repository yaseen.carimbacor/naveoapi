using System;
using System.Collections.Generic;
using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using System.Data;
using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Services.Audits;

namespace NaveoOneLib.Services.Permissions
{
    class RolePermissionsService
    {
        Errors er = new Errors();
        public DataTable GetRolePermissions(Guid sUserToken, String sConnStr)
        {
            List<Matrix> lm = new MatrixService().GetMatrixByUserToken(sUserToken, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);
            String sqlString = @"SELECT RoleID, 
PermissionID, 
iCreate, 
iRead, 
iUpdate, 
iDelete from GFI_SYS_RolePermissions order by RoleID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        RolePermission GetRolePermissions(DataSet ds)
        {
            DataRow dr = ds.Tables["dtRolePermissions"].Rows[0];
            RolePermission retRolePermissions = new RolePermission();
            retRolePermissions.RoleID = (int)dr["RoleID"];
            retRolePermissions.PermissionID = (int)dr["PermissionID"];
            retRolePermissions.iCreate = String.IsNullOrEmpty(dr["iCreate"].ToString()) ? (int?)null : (int)dr["iCreate"];
            retRolePermissions.iRead = String.IsNullOrEmpty(dr["iRead"].ToString()) ? (int?)null : (int)dr["iRead"];
            retRolePermissions.iUpdate = String.IsNullOrEmpty(dr["iUpdate"].ToString()) ? (int?)null : (int)dr["iUpdate"];
            retRolePermissions.iDelete = String.IsNullOrEmpty(dr["iDelete"].ToString()) ? (int?)null : (int)dr["iDelete"];

            List<Matrix> lMatrix = new List<Matrix>();
            MatrixService ms = new MatrixService();
            foreach (DataRow r in ds.Tables["dtMatrix"].Rows)
                lMatrix.Add(ms.AssignMatrix(r));
            retRolePermissions.lMatrix = lMatrix;
            return retRolePermissions;
        }
        public RolePermission GetRolePermissionsById(String iID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow(); String sql = @"SELECT RoleID, 
PermissionID, 
iCreate, 
iRead, 
iUpdate, 
iDelete from GFI_SYS_RolePermissions WHERE RoleID = ? ";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtRolePermissions";
            dtSql.Rows.Add(drSql);

            //Matrix
            sql = new MatrixService().sGetMatrixId(Convert.ToInt32(iID), "GFI_SYS_GroupMatrixRolePermissions");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables["dtRolePermissions"].Rows.Count == 0)
                return null;
            else
                return GetRolePermissions(ds);
        }
        DataTable RolePermissionsDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_RolePermissions";
            dt.Columns.Add("RoleID", typeof(int));
            dt.Columns.Add("PermissionID", typeof(int));
            dt.Columns.Add("iCreate", typeof(int));
            dt.Columns.Add("iRead", typeof(int));
            dt.Columns.Add("iUpdate", typeof(int));
            dt.Columns.Add("iDelete", typeof(int));

            return dt;
        }
        public Boolean SaveRolePermissions(RolePermission uRolePermissions, Boolean bInsert, String sConnStr)
        {
            DataTable dt = RolePermissionsDT();

            DataRow dr = dt.NewRow();
            dr["RoleID"] = uRolePermissions.RoleID;
            dr["PermissionID"] = uRolePermissions.PermissionID;
            dr["iCreate"] = uRolePermissions.iCreate != null ? uRolePermissions.iCreate : (object)DBNull.Value;
            dr["iRead"] = uRolePermissions.iRead != null ? uRolePermissions.iRead : (object)DBNull.Value;
            dr["iUpdate"] = uRolePermissions.iUpdate != null ? uRolePermissions.iUpdate : (object)DBNull.Value;
            dr["iDelete"] = uRolePermissions.iDelete != null ? uRolePermissions.iDelete : (object)DBNull.Value;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "RoleID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uRolePermissions.RoleID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            //Matrix
            DataTable dtMatrix = new myConverter().ListToDataTable<Matrix>(uRolePermissions.lMatrix);
            dtMatrix.TableName = "GFI_SYS_GroupMatrixRolePermissions";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 2;
            drCtrl["TblName"] = dtMatrix.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "MID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "iID";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtMatrix);


            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public Boolean DeleteRolePermissions(RolePermission uRolePermissions, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_SYS_RolePermissions where RoleID = " + uRolePermissions.RoleID.ToString();
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            DataTable dtAudit = new AuditService().LogTran(3, "RolePermissions", uRolePermissions.RoleID.ToString(), Globals.uLogin.UID, uRolePermissions.RoleID);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "CallerFunction";
            drCtrl["ParentTblName"] = "GFI_SYS_RolePermissions";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
    }
}
