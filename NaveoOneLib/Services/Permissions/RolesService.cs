using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Services.Audits;

namespace NaveoOneLib.Services.Permissions
{
    public class RolesService
    {
        Errors er = new Errors();
        public DataTable GetRoles(Guid sUserToken, String sConnStr)
        {
            List<Matrix> lm = new MatrixService().GetMatrixByUserToken(sUserToken, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            String sqlString = @"SELECT r.iID, Description 
                                from GFI_SYS_Roles r, GFI_SYS_GroupMatrixRoles m
                                where r.iID = m.iID and m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + ") union select r.iID, Description from GFI_SYS_Roles r where Description = '### Default_Role ###'  ";

            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        public DataTable GetRolesByRoleName(String Name, String sConnStr)
        {

            String sqlString = @"SELECT * 
                                    from GFI_SYS_Roles 
                                    where Description = '" + Name + "' ";

            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }


        public DataTable GetPermissionIdByRoleId(int Id, String sConnStr)
        {

            String sqlString = @"SELECT PermissionID 
                                    from GFI_SYS_RolePermissions 
                                    where RoleID = '"+ Id +"' ";

            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        public DataTable GetRoleMatrix(int Id, String sConnStr)
        {

            String sqlString = @"SELECT * 
                                    from GFI_SYS_GroupMatrixRoles 
                                    where iID = '" + Id + "' ";

            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        Role GetRoles(DataSet ds)
        {
            DataRow dr = ds.Tables["dtRoles"].Rows[0];
            Role retRoles = new Role();
            retRoles.RoleID = (int)dr["iID"];
            retRoles.Description = dr["Description"].ToString();

            List<Matrix> lMatrix = new List<Matrix>();
            MatrixService ms = new MatrixService();
            foreach (DataRow r in ds.Tables["dtMatrix"].Rows)
                lMatrix.Add(ms.AssignMatrix(r));
            retRoles.lMatrix = lMatrix;

            List<RolePermission> lPermissions = new List<RolePermission>();
            foreach (DataRow r in ds.Tables["dtPermission"].Rows)
            {
                RolePermission rp = new RolePermission();
                rp.RoleID = (int)r["RoleID"];
                rp.PermissionID = (int)r["PermissionID"];
                rp.iCreate = String.IsNullOrEmpty(r["iCreate"].ToString()) ? 0 : (int)r["iCreate"];
                rp.iRead = String.IsNullOrEmpty(r["iRead"].ToString()) ? 0 : (int)r["iRead"];
                rp.iUpdate = String.IsNullOrEmpty(r["iUpdate"].ToString()) ? 0 : (int)r["iUpdate"];
                rp.iDelete = String.IsNullOrEmpty(r["iDelete"].ToString()) ? 0 : (int)r["iDelete"];
                rp.Report1 = String.IsNullOrEmpty(r["Report1"].ToString()) ? 0 : (int)r["Report1"];
                rp.Report2 = String.IsNullOrEmpty(r["Report2"].ToString()) ? 0 : (int)r["Report2"];
                rp.Report3 = String.IsNullOrEmpty(r["Report3"].ToString()) ? 0 : (int)r["Report3"];
                rp.Report4 = String.IsNullOrEmpty(r["Report4"].ToString()) ? 0 : (int)r["Report4"];

                lPermissions.Add(rp);
            }
            retRoles.lPermissions = lPermissions;

            return retRoles;
        }
        public Role GetRolesById(int iID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"SELECT * from GFI_SYS_Roles WHERE iID = " + iID;
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtRoles";
            dtSql.Rows.Add(drSql);

            //Matrix
            sql = new MatrixService().sGetMatrixId(Convert.ToInt32(iID), "GFI_SYS_GroupMatrixRoles");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);

            //Permissions
            sql = @"SELECT r.* 
                    from GFI_SYS_RolePermissions r 
                        inner join GFI_SYS_Permissions p on p.iID = r.PermissionID 
                    WHERE r.RoleID = " + iID;
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtPermission";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables["dtRoles"].Rows.Count == 0)
                return null;
            else
                return GetRoles(ds);
        }

        DataTable RolesDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_Roles";
            dt.Columns.Add("iID", typeof(int));
            dt.Columns.Add("Description", typeof(String));

            return dt;
        }
        public BaseModel SaveRoles(Role uRoles, Boolean bInsert, String sConnStr)
        {
            if (bInsert)
            {
                String sql = "select * from GFI_SYS_Roles where Description = '" + uRoles.Description + "'";
                DataTable dtChk = new Connection(sConnStr).GetDataDT(sql, sConnStr);
                if (dtChk.Rows.Count > 0)
                    return new BaseModel()
                    {
                        errorMessage = "Role already exist",
                        data = false
                    };
            }

            DataTable dt = RolesDT();
            DataRow dr = dt.NewRow();
            dr["iID"] = uRoles.RoleID;
            dr["Description"] = uRoles.Description;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "iID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uRoles.RoleID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            //Matrix
            DataTable dtMatrix = new myConverter().ListToDataTable<Matrix>(uRoles.lMatrix);
            dtMatrix.TableName = "GFI_SYS_GroupMatrixRoles";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 2;
            drCtrl["TblName"] = dtMatrix.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "MID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "iID";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtMatrix);

            //Permission
            DataTable dtPermissions = new myConverter().ListToDataTable<RolePermission>(uRoles.lPermissions);
            dtPermissions.TableName = "GFI_SYS_RolePermissions";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 3;
            drCtrl["TblName"] = dtPermissions.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "RoleID";
            drCtrl["KeyIsIdentity"] = "False";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "RoleID";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtPermissions);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                {
                    if (bInsert)
                        dri["oprType"] = DataRowState.Added;
                    else
                        if (dri["oprType"].ToString().Trim().Length == 0)
                        dri["oprType"] = DataRowState.Modified;
                }
            }

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResult.Tables["CTRLTBL"];

            BaseModel baseModel = new BaseModel();
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                {
                    baseModel.dbResult = dsResult;
                    baseModel.dbResult.Tables["CTRLTBL"].TableName = "ResultCtrlTbl";
                    baseModel.dbResult.Merge(dsProcess);
                    bResult = false;
                }
                else
                    bResult = true;
            }
            else
                bResult = false;

            baseModel.data = bResult;
            return baseModel;
        }
        public Boolean DeleteRoles(Role uRoles, int uLogin, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_SYS_Roles where iID = " + uRoles.RoleID.ToString();
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            DataTable dtAudit = new AuditService().LogTran(3, "Roles", uRoles.RoleID.ToString(), uLogin, uRoles.RoleID);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "CallerFunction";
            drCtrl["ParentTblName"] = "GFI_SYS_Roles";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
    }
}
