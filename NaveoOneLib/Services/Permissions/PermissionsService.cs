﻿using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Services.GMatrix;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace NaveoOneLib.Services.Permissions
{
    public class PermissionsService
    {
        public static Boolean isAuthorized(Guid UserToken, String ControllerName, String ActionName, String sConnStr)
        {
            Boolean b = false;
            try
            {
                string col = "iRead";
                N1Controller n1Controller = new N1Controller();
                var listControllerName = n1Controller.listControllerName();
                foreach (var x in listControllerName)
                {
                    #region TripReport
                    if (ControllerName == x && ActionName == "DailySummarizedTrips")
                    {
                        string concat = "Report1_";
                        ActionName = concat += ActionName;
                    }

                    if (ControllerName == x && ActionName == "GetSummarizedTrips")
                    {
                        string concat = "Report2_";
                        ActionName = concat += ActionName;
                    }

                    if (ControllerName == x && ActionName == "GetDailyTripTime")
                    {
                        string concat = "Report3_";
                        ActionName = concat += ActionName;
                    }

                    if (ControllerName == x && ActionName == "AssetSummaryOutsideHO")
                    {
                        string concat = "Report4_";
                        ActionName = concat += ActionName;
                    }
                    #endregion

                    #region ZoneVisitedReport
                    if (ControllerName == x && ActionName == "GetZoneVisitedReport")
                    {
                        string concat = "Report1_";
                        ActionName = concat += ActionName;
                    }

                    #endregion

                    #region Temperature
                    if (ControllerName == x && ActionName == "GetTemperature")
                    {
                        string concat = "Report1_";
                        ActionName = concat += ActionName;
                    }

                    if (ControllerName == x && ActionName == "GetCustomizedTemp")
                    {
                        string concat = "Report2_";
                        ActionName = concat += ActionName;
                    }
                    #endregion

                    #region FuelReport
                    if (ControllerName == x && ActionName == "GetFuelData")
                    {
                        string concat = "Report1_";
                        ActionName = concat += ActionName;
                    }

                    #endregion

                    #region Exceptions
                    if (ControllerName == x && ActionName == "GetExceptions")
                    {
                        string concat = "Report1_";
                        ActionName = concat += ActionName;
                    }
                    #endregion

                    #region SensorData
                    if (ControllerName == x && ActionName == "DebugData")
                    {
                        string concat = "Report1_";
                        ActionName = concat += ActionName;
                    }
                    #endregion

                    #region Planning
                    if (ControllerName == x && ActionName == "GetPlanningActualReport")
                    {
                        string concat = "Report1_";
                        ActionName = concat += ActionName;
                    }

                    if (ControllerName == x && ActionName == "GetCustomTrips")
                    {
                        string concat = "Report1_";
                        ActionName = concat += ActionName;

                    }

                    if (ControllerName == x && ActionName == "GetCustomTripReport")
                    {
                        string concat = "Report2_";
                        ActionName = concat += ActionName;

                    }

                    #endregion

                    #region MOLG
                    if (ControllerName == x && ActionName == "GetMokaCustomTripReport")
                    {
                        string concat = "Report1_";
                        ActionName = concat += ActionName;
                    }


                    #endregion


                    #region Configurations
                    if (ControllerName == x && ActionName == "GetMonitoring")
                    {
                        string concat = "Report1_";
                        ActionName = concat += ActionName;
                    }


                    if (ControllerName == x && ActionName == "GetDeviceData")
                    {
                        string concat = "Report2_";
                        ActionName = concat += ActionName;
                    }


                    #endregion
                }


                if (ActionName.Contains("Save"))
                    ActionName = ActionName.Replace("Save", "create");

                String actName = ActionName;

                if (!ActionName.Contains("Report1") || !ActionName.Contains("Report2") || !ActionName.Contains("Report3") || !ActionName.Contains("Report4"))
                {
                    if (ActionName.Length > 6)
                        actName = ActionName.ToLower().Substring(0, 6);
                }
                else
                {
                    actName = actName.Substring(0, actName.IndexOf("_"));
                }

                switch (actName.ToLower())
                {
                    case "create":
                        col = "iCreate";
                        break;
                    case "update":
                        col = "iUpdate";
                        break;
                    case "delete":
                        col = "iDelete";
                        break;
                    case "report1":
                        col = "Report1";
                        break;
                    case "report2":
                        col = "Report2";
                        break;
                    case "report3":
                        col = "Report3";
                        break;
                    case "report4":
                        col = "Report4";
                        break;
                    default:
                        return true;
                }


                if (ControllerName == "ScheduleRequest")
                    ControllerName = "Schedule";

                String sql = @"SELECT count(1) cnt
                                FROM GFI_SYS_RolePermissions rp
									inner join GFI_SYS_Permissions p on rp.PermissionID = p.iID
	                                inner join GFI_SYS_UserRoles r on rp.RoleID = r.RoleID
	                                inner join GFI_SYS_User u on r.UserID = u.UID
                                WHERE u.UserToken = '" + UserToken + @"'
                                    and ControllerName = '" + ControllerName + @"'
                                    and " + col + " = 1";

                Connection c = new Connection(sConnStr);
                DataTable dtSql = c.dtSql();
                DataRow drSql = dtSql.NewRow();
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                //drSql["sTableName"] = "MultipleDTfromQuery";
                dtSql.Rows.Add(drSql);

                DataSet ds = c.GetDataDS(dtSql, sConnStr);

                int i = 0;
                int.TryParse(ds.Tables[0].Rows[0][0].ToString(), out i);
                if (i > 0)
                    b = true;
            }
            catch { }

            return b;
        }

        public static String isAuthorized(String UserToken, String ControllerName, String sConnStr)
        {
            DataTable dt = new DataTable();
            try
            {
                String sql = @"SELECT p.iCreate, p.iRead, p.iUpdate, p.iDelete
                                FROM GFI_SYS_RolePermissions p
	                                inner join GFI_SYS_UserRoles r on p.RoleID = r.RoleID
	                                inner join GFI_SYS_User u on r.UserID = u.UID
                                WHERE u.UserToken = '" + UserToken + @"'
                                    and ControllerName = '" + ControllerName + "'";

                Connection c = new Connection(sConnStr);
                DataTable dtSql = c.dtSql();
                DataRow drSql = dtSql.NewRow();
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                //drSql["sTableName"] = "MultipleDTfromQuery";
                dtSql.Rows.Add(drSql);

                DataSet ds = c.GetDataDS(dtSql, sConnStr);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        dt = ds.Tables[0];
            }
            catch { }

            return JsonConvert.SerializeObject(dt);
        }

        public static String GetUserPermissions(String UserToken, String sConnStr)
        {
            DataTable dt = new DataTable();
            try
            {
                String sql = @"SELECT p.iCreate, p.iRead, p.iUpdate, p.iDelete
                                FROM GFI_SYS_RolePermissions p
	                                inner join GFI_SYS_UserRoles r on p.RoleID = r.RoleID
	                                inner join GFI_SYS_User u on r.UserID = u.UID
                                WHERE u.UserToken = '" + UserToken + @"'";

                Connection c = new Connection(sConnStr);
                DataTable dtSql = c.dtSql();
                DataRow drSql = dtSql.NewRow();
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                dtSql.Rows.Add(drSql);

                DataSet ds = c.GetDataDS(dtSql, sConnStr);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        dt = ds.Tables[0];
            }
            catch { }

            return JsonConvert.SerializeObject(dt);
        }

        DataTable PermissionDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_Permissions";
            dt.Columns.Add("iID", typeof(int));
            dt.Columns.Add("ControllerName", typeof(String));
            dt.Columns.Add("ModuleID", typeof(int));
            dt.Columns.Add("oprType", typeof(DataRowState));

            return dt;
        }

        Permission GetPermission(DataRow dr)
        {
            Permission p = new Permission();
            p.iID = Convert.ToInt32(dr["iID"]);
            p.ControllerName = dr["ControllerName"].ToString();
            p.ModuleID = Convert.ToInt32(dr["ModuleID"]);

            return p;
        }

        public Boolean SavePermissions(Permission uPermissions, Boolean bInsert, String sConnStr)
        {
            DataTable dt = PermissionDT();

            DataRow dr = dt.NewRow();
            dr["iID"] = Convert.ToInt32(uPermissions.iID);
            dr["ControllerName"] = uPermissions.ControllerName.ToString();
            dr["ModuleID"] = Convert.ToInt32(uPermissions.ModuleID);
            dr["oprType"] = uPermissions.oprType;

            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "iID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uPermissions.iID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }

        public Permission GetPermisionsByControllerName(String ControllerName, String sConnStr)
        {
            String sqlString = @"SELECT * from GFI_SYS_Permissions WHERE ControllerName = ?";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, ControllerName, sConnStr);

            if (dt.Rows.Count == 0)
                return null;
            else
                return GetPermission(dt.Rows[0]);
        }

        public DataTable GetPermission(string sConnStr)
        {
            String sqlString = @"SELECT *
                                    from GFI_SYS_Permissions 
                                    ORDER BY iID";


            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);


        }

        public DataTable GetPermissionByUserMatrix(List<Matrix> lMatrix, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            return GetPermission(iParentIDs, sConnStr);
        }

        public DataTable GetPermission(List<int> iParentIDs, String sConnStr)
        {
            DataTable dt = new Connection(sConnStr).GetDataDT(sGetPermissions(iParentIDs, sConnStr), sConnStr);
            return dt;
        }

        public String sGetPermissions(List<int> iParentIDs, String sConnStr)
        {


            string GMIDs = new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr);
            string sql = string.Empty;
            string[] gmids = GMIDs.Split(',');
            DataTable dtPerm = new DataTable();
            Boolean bFound = false;
            foreach (String g in gmids)
            {
                DataTable dt = new Connection(sConnStr).GetDataDT(new NaveoOneLib.Services.GMatrix.GroupMatrixService().sGetCompanyGMIDfromGMID(Convert.ToInt32(g)), sConnStr);
                foreach(DataRow dr in dt.Rows)
                {
                     sql = @"
select Distinct P.iID as PermissionID ,P.ControllerName 
    from GFI_SYS_GroupMatrix m
        inner join GFI_SYS_GroupMatrixNaveoModules GMN on m.GMID = GMN.GMID
        inner join GFI_SYS_NaveoModules NM on NM.iID = GMN.iID
        inner join GFI_SYS_Permissions P on P.ModuleID = NM.iID
where m.GMID = " + dr["GMID"] + "order by PermissionID asc";

                    dtPerm = new Connection(sConnStr).GetDataDT(sql, sConnStr);
                    if (dtPerm.Rows.Count > 0)
                        bFound = true;

                    if (bFound)
                        break;
                }
                if (bFound)
                    break;


            }


            return sql;
        }


        public DataTable GetPermissionByUserID(int UID, String sConnStr)
        {
            DataTable dt = new Connection(sConnStr).GetDataDT(sGetPermissionsByUID(UID, sConnStr), sConnStr);
            return dt;
        }

        public DataTable GeTGMIDSISCOMPANY(String sConnStr)
        {
            string sql = @"SELECT GMID FROM GFI_SYS_GroupMatrix WHERE IsCompany = 1";

            DataTable dt = new Connection(sConnStr).GetDataDT(sql, sConnStr);
            return dt;
        }

        public String sGetPermissionsByUID(int uid, String sConnStr)
        {
            String sql = @"select roles.Description as roleName ,permission.ControllerName as ModuleName, rolePermissions.iCreate ,rolePermissions.iUpdate,rolePermissions.iRead,rolePermissions.iDelete,
                          rolePermissions.Report1,rolePermissions.Report2 , rolePermissions.Report3 ,rolePermissions.Report4 from GFI_SYS_UserRoles userRoles
                          inner join GFI_SYS_Roles roles
                          on userRoles.RoleID = roles.iID
                          inner join GFI_SYS_RolePermissions rolePermissions
                          on roles.iID = rolePermissions.RoleID
						  inner join GFI_SYS_Permissions permission 
						  on rolePermissions.PermissionID = permission.iID
                          where userRoles.UserID =" + uid + " order by rolePermissions.PermissionID";

            return sql;

        }

    }
}

