﻿using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using NaveoOneLib.Models.Permissions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace NaveoOneLib.Services.Permissions
{
    class SecurityTokenService
    {
        SecurityToken GetSecurityToken(DataRow dr)
        {
            SecurityToken retSecurityToken = new SecurityToken();
            retSecurityToken.OneTimeToken = new Guid(dr["OneTimeToken"].ToString());
            //if (dr.Table.Columns.Contains("UID")) retSecurityToken.UID = (int)dr["UID"];
            if (dr.Table.Columns.Contains("UserToken")) retSecurityToken.UserToken = new Guid(dr["UserToken"].ToString());
            if (dr.Table.Columns.Contains("expiryDate")) retSecurityToken.expiryDate = (DateTime)dr["expiryDate"];
            if (dr.Table.Columns.Contains("timeOut")) retSecurityToken.timeOut = (DateTime)dr["timeOut"];
            retSecurityToken.IsAuthorized = true;

            return retSecurityToken;
        }

        DataTable SecurityTokenDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_SecurityToken";
            dt.Columns.Add("OneTimeToken", typeof(Guid));
            dt.Columns.Add("UID", typeof(int));
            dt.Columns.Add("UserToken", typeof(Guid));

            return dt;
        }
        public SecurityToken InsertSecurityToken(SecurityToken uSecurityToken, String sConnStr)
        {
            DataTable dt = SecurityTokenDT();
            DataRow dr = dt.NewRow();
            uSecurityToken.OneTimeToken = Guid.NewGuid();
            dr["OneTimeToken"] = uSecurityToken.OneTimeToken;
            //dr["UID"] = uSecurityToken.UID;
            dr["UserToken"] = uSecurityToken.UserToken;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "OneTimeToken";
            drCtrl["KeyIsIdentity"] = "False";
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();
            String sql = "Select * from GFI_SYS_SecurityToken where OneTimeToken = ?";
            drSql["sSQL"] = sql;
            drSql["sParam"] = uSecurityToken.OneTimeToken;
            dtSql.Rows.Add(drSql);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                        dri["oprType"] = DataRowState.Added;
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];
            return GetSecurityToken(dsProcess.Tables["GFI_SYS_SecurityToken"].Rows[0]);
        }
        public SecurityToken UpdateSecurityToken(Guid OneTimeToken, String sConnStr)
        {
            String sql = "update GFI_SYS_SecurityToken set TimeOut = DateAdd(mi, 20, getdate()) where OneTimeToken = '" + OneTimeToken + "' and getdate() <= ExpiryDate and getdate() <= TimeOut ";
            sql += @"
if(@@rowcount > 0) 
begin
    select 'GFI_SYS_SecurityToken' AS TableName
    select '" + OneTimeToken + @"' OneTimeToken 
end
else
    delete from GFI_SYS_SecurityToken where OneTimeToken = '" + OneTimeToken + @"'
    delete from GFI_SYS_SecurityToken where ExpiryDate < getdate() - 1";
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            drSql["sSQL"] = sql;
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            if (ds.Tables.Contains("GFI_SYS_SecurityToken"))
                if (ds.Tables["GFI_SYS_SecurityToken"].Rows.Count > 0)
                    return GetSecurityToken(ds.Tables["GFI_SYS_SecurityToken"].Rows[0]);

            return new SecurityToken() { OneTimeToken = SecurityToken.DEFAULT_GUID };
        }
        public Boolean DeleteSecurityToken(SecurityToken uSecurityToken, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_SYS_SecurityToken where OneTimeToken = '" + uSecurityToken.OneTimeToken.ToString() + "'";
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }

        #region Obsolete
        public SecurityToken UpdateSecurityToken_OneTime(Guid OneTimeToken, String sConnStr)
        {
            Guid newGuid = Guid.NewGuid();

            String sql = "update GFI_SYS_SecurityToken set OneTimeToken = '" + newGuid + "', TimeOut = DateAdd(mi, 20, getdate()) where OneTimeToken = '" + OneTimeToken + "' and getdate() <= ExpiryDate and getdate() <= TimeOut ";
            sql += @"
if(@@rowcount > 0) 
begin
    select 'GFI_SYS_SecurityToken' AS TableName
    select * from GFI_SYS_SecurityToken where OneTimeToken = '" + newGuid + @"'
end
else
    delete from GFI_SYS_SecurityToken where OneTimeToken = '" + OneTimeToken + "'";

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            drSql["sSQL"] = sql;
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            if (ds.Tables.Contains("GFI_SYS_SecurityToken"))
                if (ds.Tables["GFI_SYS_SecurityToken"].Rows.Count > 0)
                    return GetSecurityToken(ds.Tables["GFI_SYS_SecurityToken"].Rows[0]);

            return null;
        }
        #endregion
    }
}
