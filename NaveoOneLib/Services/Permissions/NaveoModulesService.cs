using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Services.Audits;

namespace NaveoOneLib.Services.Permissions
{
    public class NaveoModulesService
    {
        Errors er = new Errors();


        public DataTable GetNaveoModuleFromMatrixById(string id,string gmid, String sConnStr)
        {
  

            String sqlString = @"SELECT *
                                    from GFI_SYS_GroupMatrixNaveoModules 
                                    where  iid = " + id +" and gmid ="+ gmid+"";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        public DataTable GetNaveoModules(Guid sUserToken, String sConnStr)
        {
            List<Matrix> lm = new MatrixService().GetMatrixByUserToken(sUserToken, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            String sqlString = @"SELECT iID, Description description
                                    from GFI_SYS_NaveoModules";

            //String sqlString = @"SELECT distinct n.iID, Description description
            //                        from GFI_SYS_NaveoModules n
	           //                         inner join GFI_SYS_GroupMatrixNaveoModules m on n.iID = m.iID
            //                        where m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + ")";


            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public DataTable GetAllNaveoModules(String sConnStr)
        {


            String sqlString = @"SELECT *
                                    from GFI_SYS_NaveoModules ";
	                                    
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        NaveoModule GetNaveoModules(DataSet ds)
        {
            DataRow dr = ds.Tables["dtNaveoModules"].Rows[0];
            NaveoModule retNaveoModules = new NaveoModule();
            retNaveoModules.iID = (int)dr["iID"];
            retNaveoModules.description = dr["Description"].ToString();

            List<Matrix> lMatrix = new List<Matrix>();
            MatrixService ms = new MatrixService();
            foreach (DataRow r in ds.Tables["dtMatrix"].Rows)
                lMatrix.Add(ms.AssignMatrix(r));
            retNaveoModules.lMatrix = lMatrix;

            List<Permission> lPermission = new List<Permission>();
            foreach (DataRow r in ds.Tables["dtPermissions"].Rows)
            {
                Permission retPermissions = new Permission();
                retPermissions.iID = (int)r["iID"];
                retPermissions.ControllerName = r["ControllerName"].ToString();
                retPermissions.ModuleID = (int)r["ModuleID"];
                lPermission.Add(retPermissions);
            }
            retNaveoModules.lPermissions = lPermission;

            return retNaveoModules;
        }
        public NaveoModule GetNaveoModulesByName(String Name, String sConnStr)
        {
            String sqlString = @"SELECT * from GFI_SYS_NaveoModules WHERE Description = ?";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, Name, sConnStr);
            if (dt.Rows.Count == 0)
                return null;

            int iID = 0;
            if (!int.TryParse(dt.Rows[0]["iID"].ToString(), out iID))
                return null;

            Connection c = new Connection(sConnStr);

            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();
            String sql = @"SELECT * from GFI_SYS_NaveoModules WHERE iID = " + iID;
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtNaveoModules";
            dtSql.Rows.Add(drSql);

            //Matrix
            sql = new MatrixService().sGetMatrixId(iID, "GFI_SYS_GroupMatrixNaveoModules");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);

            //Permissions
            sql = "select * from GFI_SYS_Permissions where ModuleID = " + iID;
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtPermissions";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables["dtNaveoModules"].Rows.Count == 0)
                return null;
            else
                return GetNaveoModules(ds);
        }

        DataTable NaveoModulesDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_NaveoModules";
            dt.Columns.Add("iID", typeof(int));
            dt.Columns.Add("Description", typeof(String));

            return dt;
        }
        public Boolean SaveNaveoModules(NaveoModule uNaveoModules, Boolean bInsert, String sConnStr)
        {
            DataTable dt = NaveoModulesDT();

            DataRow dr = dt.NewRow();
            dr["iID"] = uNaveoModules.iID;
            dr["Description"] = uNaveoModules.description;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "iID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uNaveoModules.iID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            //Matrix
            DataTable dtMatrix = new myConverter().ListToDataTable<Matrix>(uNaveoModules.lMatrix);
            dtMatrix.TableName = "GFI_SYS_GroupMatrixNaveoModules";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 2;
            drCtrl["TblName"] = dtMatrix.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "MID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "iID";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtMatrix);

            //Permissions
            DataTable dtPermissions = new myConverter().ListToDataTable<Permission>(uNaveoModules.lPermissions);
            dtPermissions.TableName = "GFI_SYS_Permissions";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 3;
            drCtrl["TblName"] = dtPermissions.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "iID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "ModuleID";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtPermissions);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public Boolean DeleteNaveoModules(NaveoModule uNaveoModules, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_SYS_NaveoModules where iID = " + uNaveoModules.iID.ToString();
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            DataTable dtAudit = new AuditService().LogTran(3, "NaveoModules", uNaveoModules.iID.ToString(), Globals.uLogin.UID, uNaveoModules.iID);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "CallerFunction";
            drCtrl["ParentTblName"] = "GFI_SYS_NaveoModules";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
    }
}
