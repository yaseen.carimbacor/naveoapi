using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Permissions;

namespace NaveoOneLib.Services.Permissions
{
    public class ModuleService
    {
        Errors er = new Errors();

        Module myModule(DataRow dr)
        {
            Module retModules = new Module();
            retModules.UID = Convert.ToInt32(dr["UID"]);
            retModules.ModuleName = dr["ModuleName"].ToString();
            retModules.Description = dr["Description"].ToString();
            retModules.Command = dr["Command"].ToString();
            retModules.CreatedDate = (DateTime)dr["CreatedDate"];
            retModules.CreatedBy = dr["CreatedBy"].ToString();
            retModules.UpdatedDate = (DateTime)dr["UpdatedDate"];
            retModules.UpdatedBy = dr["UpdatedBy"].ToString();
            return retModules;
        }
        public DataTable GetModules(String sConnStr)
        {
            String sqlString = @"SELECT UID, 
                ModuleName, 
                Description,
                Command, 
                CreatedDate, 
                CreatedBy, 
                UpdatedDate, 
                UpdatedBy from GFI_SYS_Modules order by ModuleName";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public Module GetModulesById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT UID, 
                    ModuleName, 
                    Description, 
                    Command,
                    CreatedDate, 
                    CreatedBy, 
                    UpdatedDate, 
                    UpdatedBy from GFI_SYS_Modules
                    WHERE UID = ?
                    order by UID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;

            return myModule(dt.Rows[0]);
        }
        public Module GetModulesByName(String sModuleName, String sConnStr)
        {
            String sqlString = @"SELECT UID, 
                    ModuleName, 
                    Description, 
                    Command,
                    CreatedDate, 
                    CreatedBy, 
                    UpdatedDate, 
                    UpdatedBy from GFI_SYS_Modules
                    WHERE ModuleName = ?
                    order by UID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, sModuleName, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            return myModule(dt.Rows[0]);
        }
        public Module GetModulesByCommand(String sCommand, String sConnStr)
        {
             String sqlString = @"SELECT UID, 
                    ModuleName, 
                    Description, 
                    Command,
                    CreatedDate, 
                    CreatedBy, 
                    UpdatedDate, 
                    UpdatedBy from GFI_SYS_Modules
                    WHERE Command = ?
                    order by UID";
             DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, sCommand,sConnStr);
             if (dt.Rows.Count == 0)
                  return null;
            return myModule(dt.Rows[0]);
        }
        public bool SaveModules(Module uModules, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_Modules";
            dt.Columns.Add("ModuleName", uModules.ModuleName.GetType());
            dt.Columns.Add("Description", uModules.Description.GetType());
            dt.Columns.Add("Command", uModules.Description.GetType());
            dt.Columns.Add("CreatedDate", uModules.CreatedDate.GetType());
            dt.Columns.Add("CreatedBy", uModules.CreatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uModules.UpdatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uModules.UpdatedBy.GetType());

            DataRow dr = dt.NewRow();
            dr["ModuleName"] = uModules.ModuleName;
            dr["Description"] = uModules.Description;
            dr["Command"] = uModules.Command;
            dr["CreatedDate"] = uModules.CreatedDate;
            dr["CreatedBy"] = uModules.CreatedBy;
            dr["UpdatedDate"] = uModules.UpdatedDate;
            dr["UpdatedBy"] = uModules.UpdatedBy;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenInsert(dt, transaction,sConnStr))
                return true;
            else
                return false;
        }


        public bool SaveModules(Module uModules, Boolean bInsert, String sConnStr)
        {
            DataTable dtModule = new DataTable();
            dtModule.TableName = "GFI_SYS_Modules";

            dtModule.Columns.Add("UID", uModules.UID.GetType());
            dtModule.Columns.Add("Command", uModules.Command.GetType());
            dtModule.Columns.Add("CreatedBy", uModules.CreatedBy.GetType());
            dtModule.Columns.Add("CreatedDate", uModules.CreatedDate.GetType());
            dtModule.Columns.Add("Description", uModules.Description.GetType());
            dtModule.Columns.Add("MenuGroupId", uModules.MenuGroupId.GetType());
            dtModule.Columns.Add("MenuHeaderId", uModules.MenuHeaderId.GetType());
            dtModule.Columns.Add("ModuleName", uModules.ModuleName.GetType());
            dtModule.Columns.Add("OrderIndex", uModules.OrderIndex.GetType());
            dtModule.Columns.Add("Summary", uModules.Summary.GetType());
            dtModule.Columns.Add("Title", uModules.Title.GetType());
            dtModule.Columns.Add("UpdatedBy", uModules.UpdatedBy.GetType());
            dtModule.Columns.Add("UpdatedDate", uModules.UpdatedDate.GetType());

            DataRow dr = dtModule.NewRow();
            dr["UID"] = uModules.UID;
            dr["Command"] = uModules.Command;
            dr["CreatedBy"] = uModules.CreatedBy;
            dr["CreatedDate"] = uModules.CreatedDate;
            dr["Description"] = uModules.Description;
            dr["MenuGroupId"] = uModules.MenuGroupId;
            dr["MenuHeaderId"] = uModules.MenuHeaderId;
            dr["ModuleName"] = uModules.ModuleName;
            dr["OrderIndex"] = uModules.OrderIndex;
            dr["Summary"] = uModules.Summary;
            dr["Title"] = uModules.Title;
            dr["UpdatedBy"] = uModules.UpdatedBy;
            dr["UpdatedDate"] = uModules.UpdatedDate;
            if (!dtModule.Columns.Contains("oprType"))
                dtModule.Columns.Add("oprType", typeof(DataRowState));
            dtModule.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dtModule.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "UID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            dtCtrl.Rows.Add(drCtrl);

            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dtModule);
            dsProcess.Merge(dtCtrl);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }


        public bool UpdateModules(Module uModules, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_Modules";
            dt.Columns.Add("UID", uModules.UID.GetType());
            dt.Columns.Add("ModuleName", uModules.ModuleName.GetType());
            dt.Columns.Add("Description", uModules.Description.GetType());
            dt.Columns.Add("Command", uModules.Description.GetType());
            dt.Columns.Add("CreatedDate", uModules.CreatedDate.GetType());
            dt.Columns.Add("CreatedBy", uModules.CreatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uModules.UpdatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uModules.UpdatedBy.GetType());
            DataRow dr = dt.NewRow();
            dr["UID"] = uModules.UID;
            dr["ModuleName"] = uModules.ModuleName;
            dr["Description"] = uModules.Description;
            dr["Command"] = uModules.Command;
            dr["CreatedDate"] = uModules.CreatedDate;
            dr["CreatedBy"] = uModules.CreatedBy;
            dr["UpdatedDate"] = uModules.UpdatedDate;
            dr["UpdatedBy"] = uModules.UpdatedBy;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "UID = '" + uModules.UID + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool DeleteModules(Module uModules, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_SYS_Modules", "UID = '" + uModules.UID + "'",sConnStr))
                return true;
            else
                return false;
        }

    }
}