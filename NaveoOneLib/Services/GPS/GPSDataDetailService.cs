using System;
using System.Collections.Generic;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.GPS;

namespace NaveoOneLib.Services.GPS
{
    public class GPSDataDetailService
    {
        Errors er = new Errors();
        GPSDataDetail GetGpsDataDetail(DataRow dr)
        {
            GPSDataDetail retGPSDataDetail = new GPSDataDetail();
            retGPSDataDetail.GPSDetailID = Convert.ToInt64(dr["GPSDetailID"]);
            retGPSDataDetail.UID = Convert.ToInt32(dr["UID"]);
            retGPSDataDetail.TypeID = dr["TypeID"].ToString();
            retGPSDataDetail.TypeValue = dr["TypeValue"].ToString();
            retGPSDataDetail.UOM = dr["UOM"].ToString();
            retGPSDataDetail.Remarks = dr["Remarks"].ToString();
            return retGPSDataDetail;
        }

        public DataTable GetGPSDataDetail(String sConnStr)
        {
            String sqlString = @"SELECT GPSDetailID, 
                                        UID, 
                                        TypeID, 
                                        TypeValue, 
                                        UOM, 
                                        Remarks from GFI_GPS_GPSDataDetail order by UID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public GPSDataDetail GetGPSDataDetailById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT GPSDetailID, 
                                        UID, 
                                        TypeID, 
                                        TypeValue, 
                                        UOM, 
                                        Remarks from GFI_GPS_GPSDataDetail
                                        WHERE GPSDetailID = ?
                                        order by UID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                GPSDataDetail retGPSDataDetail = new GPSDataDetail();
                retGPSDataDetail.GPSDetailID = Convert.ToInt64(dt.Rows[0]["GPSDetailID"]);

                retGPSDataDetail.UID = Convert.ToInt32(dt.Rows[0]["UID"]);
                retGPSDataDetail.TypeID = dt.Rows[0]["TypeID"].ToString();
                retGPSDataDetail.TypeValue = dt.Rows[0]["TypeValue"].ToString();
                retGPSDataDetail.UOM = dt.Rows[0]["UOM"].ToString();
                retGPSDataDetail.Remarks = dt.Rows[0]["Remarks"].ToString();
                return retGPSDataDetail;
            }
        }
        public List<GPSDataDetail> GetGPSDataDetailsByUID(int UID, String sConnStr)
        {
            List<GPSDataDetail> l = new List<GPSDataDetail>();
            String sql = @"select * from GFI_GPS_GPSDataDetail where UID = " + UID;
            DataTable dt = new Connection(sConnStr).GetDataDT(sql, sConnStr);
            foreach (DataRow dr in dt.Rows)
                l.Add(GetGpsDataDetail(dr));
            return l;
        }

        public bool UpdateGPSDataDetail(GPSDataDetail uGPSDataDetail, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_GPS_GPSDataDetail";
            dt.Columns.Add("UID", uGPSDataDetail.UID.GetType());
            dt.Columns.Add("TypeID", uGPSDataDetail.TypeID.GetType());
            dt.Columns.Add("TypeValue", uGPSDataDetail.TypeValue.GetType());
            dt.Columns.Add("UOM", uGPSDataDetail.UOM.GetType());
            dt.Columns.Add("Remarks", uGPSDataDetail.Remarks.GetType());
            DataRow dr = dt.NewRow();
            dr["GPSDetailID"] = uGPSDataDetail.GPSDetailID;
            dr["UID"] = uGPSDataDetail.UID;
            dr["TypeID"] = uGPSDataDetail.TypeID;
            dr["TypeValue"] = uGPSDataDetail.TypeValue;
            dr["UOM"] = uGPSDataDetail.UOM;
            dr["Remarks"] = uGPSDataDetail.Remarks;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "GPSDetailID = '" + uGPSDataDetail.GPSDetailID + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool DeleteGPSDataDetail(GPSDataDetail uGPSDataDetail, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_GPS_GPSDataDetail", "GPSDetailID = '" + uGPSDataDetail.GPSDetailID + "'", sConnStr))
                return true;
            else
                return false;
        }

        public List<GPSDataDetail> GetLogReason(String strLogReason)
        {
            List<GPSDataDetail> lr = new List<GPSDataDetail>();
            GPSDataDetail gpsDetail;

            if (strLogReason.Contains("(DriverID)"))
            {
                gpsDetail = new GPSDataDetail();
                gpsDetail.TypeID = "DriverID";
                //Atrack
                String s = "Inserted";
                try
                {
                    if (strLogReason.Contains("(DriverID)["))
                    {
                        int iStart = strLogReason.IndexOf("[") + 1;
                        int iEnd = strLogReason.IndexOf("]");
                        s = strLogReason.Substring(iStart, iEnd - iStart);
                    }
                }
                catch { }
                gpsDetail.TypeValue = s;
                lr.Add(gpsDetail);
            }

            if (strLogReason.Contains("(PassengerID)"))
            {
                gpsDetail = new GPSDataDetail();
                gpsDetail.TypeID = "PassengerID";
                int i = strLogReason.IndexOf("[");
                int j = strLogReason.IndexOf("]");
                try
                {
                    gpsDetail.TypeValue = strLogReason.Substring(i + 1, j - i - 1);
                }
                catch { }
                lr.Add(gpsDetail);
            }


            if (strLogReason.Contains("(Ignition)"))
            {
                gpsDetail = new GPSDataDetail();
                gpsDetail.TypeID = "Ignition";
                if (strLogReason.Contains("(IgnOff)"))
                    gpsDetail.TypeValue = "Off";
                else
                    gpsDetail.TypeValue = "On";

                lr.Add(gpsDetail);
            }

            if (strLogReason.Contains("(Heading)"))
            {
                gpsDetail = new GPSDataDetail();
                gpsDetail.TypeID = "Heading";
                lr.Add(gpsDetail);
            }

            if (strLogReason.Contains("(Distance)"))    //Testing Atrack 4
            {
                gpsDetail = new GPSDataDetail();
                gpsDetail.TypeID = "Distance";
                lr.Add(gpsDetail);
            }

            if (strLogReason.Contains("(HeadingOlny)")) //Testing Atrack 5
            {
                gpsDetail = new GPSDataDetail();
                gpsDetail.TypeID = "HeadingOlny";
                lr.Add(gpsDetail);
            }

            if (strLogReason.Contains("(Time)"))
            {
                gpsDetail = new GPSDataDetail();
                gpsDetail.TypeID = "Idling";
                lr.Add(gpsDetail);
            }

            if (strLogReason.Contains("(BeginOfStop)"))
            {
                gpsDetail = new GPSDataDetail();
                gpsDetail.TypeID = "Idling";
                gpsDetail.TypeValue = "BeginOfStop";
                lr.Add(gpsDetail);
            }

            if (strLogReason.Contains("(EndOfStop)"))
            {
                gpsDetail = new GPSDataDetail();
                gpsDetail.TypeID = "Idling";
                gpsDetail.TypeValue = "EndOfStop";
                lr.Add(gpsDetail);
            }

            if (strLogReason.Contains("(Reset)"))
            {
                gpsDetail = new GPSDataDetail();
                gpsDetail.TypeID = "Reset";
                gpsDetail.TypeValue = "Powered off";
                if (strLogReason.Contains("(main power up)"))
                    gpsDetail.TypeValue = "Powered on";

                lr.Add(gpsDetail);
            }

            if (strLogReason.Contains("(ModemReset)"))
            {
                gpsDetail = new GPSDataDetail();
                gpsDetail.TypeID = "ModemReset";
                gpsDetail.TypeValue = "1";
                lr.Add(gpsDetail);
            }

            if (strLogReason.Contains("(Heartbeat)"))
            {
                gpsDetail = new GPSDataDetail();
                gpsDetail.TypeID = "Heartbeat";
                lr.Add(gpsDetail);
            }

            if (strLogReason.Contains("(HarshAS)"))
            {
                gpsDetail = new GPSDataDetail();
                gpsDetail.TypeID = "Harsh Accelleration";

                if (strLogReason.Contains("(HarshAS_Start)"))
                    gpsDetail.TypeValue = "Started";

                if (strLogReason.Contains("(HarshAS_End)"))
                    gpsDetail.TypeValue = "Ended";

                lr.Add(gpsDetail);
            }

            if (strLogReason.Contains("(HarshBS)"))
            {
                gpsDetail = new GPSDataDetail();
                gpsDetail.TypeID = "Harsh Breaking";

                if (strLogReason.Contains("(HarshBS_Start)"))
                    gpsDetail.TypeValue = "Started";

                if (strLogReason.Contains("(HarshBS_End)"))
                    gpsDetail.TypeValue = "Ended";

                lr.Add(gpsDetail);
            }

            if (strLogReason.Contains("(HarshCN)"))
            {
                gpsDetail = new GPSDataDetail();
                gpsDetail.TypeID = "Harsh Cornering";
                lr.Add(gpsDetail);
            }

            if (strLogReason.Contains("(Impact)"))
            {
                gpsDetail = new GPSDataDetail();
                gpsDetail.TypeID = "Impact";
                lr.Add(gpsDetail);
            }

            if (strLogReason.Contains("(Heading)") && strLogReason.Contains("(IgnOff)"))
            {
                gpsDetail = new GPSDataDetail();
                gpsDetail.TypeID = "InvalidGpsSignals";
                lr.Add(gpsDetail);
            }

            if (strLogReason.Contains("TimeIntervalElasped(???)") && strLogReason.Contains("(IgnOff)"))
            {
                gpsDetail = new GPSDataDetail();
                gpsDetail.TypeID = "InvalidGpsSignals";
                lr.Add(gpsDetail);
            }

            if (strLogReason.Contains("(GpsShutdown)"))
            {
                gpsDetail = new GPSDataDetail();
                gpsDetail.TypeID = "No GPS";
                lr.Add(gpsDetail);
            }

            if (strLogReason.Contains("(InvalidGpsSignals)"))
            {
                gpsDetail = new GPSDataDetail();
                gpsDetail.TypeID = "InvalidGpsSignals";
                lr.Add(gpsDetail);
            }

            if (strLogReason.Contains("(Aux)"))
            {
                if (strLogReason.Contains("(Aux1On)"))
                {
                    gpsDetail = new GPSDataDetail();
                    gpsDetail.TypeID = "Aux1";
                    gpsDetail.TypeValue = "On";
                    lr.Add(gpsDetail);
                }
                if (strLogReason.Contains("(Aux1Off)"))
                {
                    gpsDetail = new GPSDataDetail();
                    gpsDetail.TypeID = "Aux1";
                    gpsDetail.TypeValue = "Off";
                    lr.Add(gpsDetail);
                }
                if (strLogReason.Contains("(Aux2On)"))
                {
                    gpsDetail = new GPSDataDetail();
                    gpsDetail.TypeID = "Aux2";
                    gpsDetail.TypeValue = "On";
                    lr.Add(gpsDetail);
                }
                if (strLogReason.Contains("(Aux2Off)"))
                {
                    gpsDetail = new GPSDataDetail();
                    gpsDetail.TypeID = "Aux2";
                    gpsDetail.TypeValue = "Off";
                    lr.Add(gpsDetail);
                }
                if (strLogReason.Contains("(Aux3On)"))
                {
                    gpsDetail = new GPSDataDetail();
                    gpsDetail.TypeID = "Aux3";
                    gpsDetail.TypeValue = "On";
                    lr.Add(gpsDetail);
                }
                if (strLogReason.Contains("(Aux3Off)"))
                {
                    gpsDetail = new GPSDataDetail();
                    gpsDetail.TypeID = "Aux3";
                    gpsDetail.TypeValue = "Off";
                    lr.Add(gpsDetail);
                }
                if (strLogReason.Contains("(Aux4On)"))
                {
                    gpsDetail = new GPSDataDetail();
                    gpsDetail.TypeID = "Aux4";
                    gpsDetail.TypeValue = "On";
                    lr.Add(gpsDetail);
                }
                if (strLogReason.Contains("(Aux4Off)"))
                {
                    gpsDetail = new GPSDataDetail();
                    gpsDetail.TypeID = "Aux4";
                    gpsDetail.TypeValue = "Off";
                    lr.Add(gpsDetail);
                }
            }

            if (strLogReason.Contains("(???)"))
            {
                gpsDetail = new GPSDataDetail();
                String g = strLogReason.Replace("(???)", "");
                g = g.Trim();
                if (g.Length > 30)
                    g = g.Substring(0, 29);
                gpsDetail.TypeID = g;
                lr.Add(gpsDetail);
            }

            return lr;
        }
        public Boolean SaveGPSDataDetail(GPSDataDetail uGPSDataDetail, Int32 UID, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_GPS_GPSDataDetail";
            dt.Columns.Add("UID", uGPSDataDetail.UID.GetType());
            dt.Columns.Add("TypeID", uGPSDataDetail.TypeID.GetType());
            dt.Columns.Add("TypeValue", uGPSDataDetail.TypeValue.GetType());
            dt.Columns.Add("UOM", uGPSDataDetail.UOM.GetType());
            dt.Columns.Add("Remarks", uGPSDataDetail.Remarks.GetType());
            DataRow dr = dt.NewRow();
            dr["UID"] = uGPSDataDetail.UID;
            dr["TypeID"] = uGPSDataDetail.TypeID;
            dr["TypeValue"] = uGPSDataDetail.TypeValue;
            dr["UOM"] = uGPSDataDetail.UOM;
            dr["Remarks"] = uGPSDataDetail.Remarks;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.InsertGPSDataDetail(dt, UID, transaction, sConnStr))
                return true;
            else
                return false;
        }
    }
}

