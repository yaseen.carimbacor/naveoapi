﻿using NaveoOneLib.DBCon;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Services.Zones;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoOneLib.Services.GPS
{
    public class IdleService
    {
        public DataSet getIdles(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, string reportType, int idleInMinutes, int idleSpeed, Boolean bGetZones, String sType, List<int> zIDs, List<Matrix> lMatrix, Boolean bDriver, String sConnStr, int? Page, int? LimitPerPage)
        {
            Pagination p = Pagination.GetRowNums(Page, LimitPerPage);
            lAssetID = lAssetID.Distinct().ToList();

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"
--ZoneVisitedReport
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 

CREATE TABLE #Assets
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL,
	[AssetID] [int] PRIMARY KEY CLUSTERED,
    Asset nvarchar(120),
    TimeZone int Default 0
)

Declare @IdlingSpeed int
Declare @IdleDuration int
Declare @IdlingTime int

set @IdlingSpeed = " + idleSpeed + @"
set @IdleDuration = (60 * " + idleInMinutes + @")
set @IdlingTime = 0

";

            foreach (int i in lAssetID)
                sql += "insert #Assets (AssetID) values (" + i.ToString() + ")\r\n\r\n";

            sql += @"
UPDATE #Assets
    SET Asset = T2.AssetNumber, TimeZone = T3.TotalMinutes
FROM #Assets T1
    INNER JOIN GFI_FLT_Asset T2 ON T2.AssetID = T1.AssetID
    INNER JOIN GFI_SYS_TimeZones T3 on T2.TimeZoneID = T3.StandardName

--Get all records
;WITH CTE AS 
(
	SELECT g.UID, g.DateTimeGPS_UTC, CONVERT(VARCHAR, DATEADD(mi, a.TimeZone, g.DateTimeGPS_UTC), 108) dtLocalTime, CONVERT(VARCHAR(11), DATEADD(mi, a.TimeZone, g.DateTimeGPS_UTC), 106) dtLocal
		, g.Speed, g.Longitude, g.Latitude
		, a.AssetID, a.Asset
		, rownum = ROW_NUMBER() OVER (PARTITION by g.AssetID ORDER BY g.DateTimeGPS_UTC)
	from GFI_GPS_GPSData g 
		inner join #Assets a on a.AssetID = g.AssetID
	where g.DateTimeGPS_UTC >= ? and g.DateTimeGPS_UTC <= ? and g.LongLatValidFlag = 1
)
SELECT cte.* into #trip 
	FROM CTE
order by Assetid, DateTimeGPS_UTC

--Get only idle details with Next record id
;WITH CTE AS 
(
	SELECT *
	from #trip where speed < @IdlingSpeed
)
SELECT distinct t.*
	, nex.RowNum NextIdleValue
	, null GrpID
	, null Duration
    , convert(nvarchar(20), null)  sDuration
	, null RowNumByGrp
    , convert(nvarchar(2500), null)  sZones
    , null iZone
    , convert(nvarchar(2500), null) iArrivalZones
    , convert(nvarchar(2500), null) sZone
    , null iZoneColor
	, null FinalGrpID
	, null FinalDuration
into #idles 
FROM #trip t
	left outer join CTE on t.UID = CTE.UID
	LEFT JOIN CTE nex ON nex.RowNum = CTE.RowNum + 1 and nex.AssetID = cte.AssetID
order by AssetID, DateTimeGPS_UTC

;with cte as
(
	SELECT *, ROW_NUMBER () OVER(partition by AssetID order by nextIdleValue) - NextIdleValue AS Grp
	FROM #idles
)
update #idles 
    set GrpID = R.Grp
from #idles T
	inner join cte R on R.UID = T.UID

;with cte as
(
	SELECT AssetID, GrpID, max(DateTimeGPS_UTC) maxTime
		, DATEDIFF(ss, min(DateTimeGPS_UTC), max(DateTimeGPS_UTC)) Duration
	FROM #idles
	group by AssetID, GrpID
)
update #idles
	set Duration = t.Duration
FROM #idles i
	inner join cte t on t.GrpID = i.GrpID and t.AssetID = i.AssetID
where t.Duration > @IdleDuration

;with cte as
(
	SELECT *, ROW_NUMBER() OVER (PARTITION BY GrpID ORDER BY Assetid, DateTimeGPS_UTC) AS RunningRowNum
		FROM #idles 
	where Duration is not null
)
update #idles
	set RowNumByGrp = t.RunningRowNum 
FROM #idles i
	inner join cte t on t.UID = i.UID

delete from #idles where Duration is null

update #idles set sDuration = CONVERT(varchar, DATEADD(s, Duration, 0), 108)

";

            String zoneFilterJoin = String.Empty;
            #region Zones
            if (bGetZones)
            {
                String zoneFilterTable = String.Empty;
                String s = ZoneService.sGetZoneIDs(lMatrix, sConnStr);

                Boolean bZoneFiler = false;
                switch (sType)
                {
                    case "Zones":
                        bZoneFiler = true;
                        break;

                    case "ZoneTypes":
                        bZoneFiler = true;
                        break;
                }

                if (bZoneFiler)
                {
                    zoneFilterTable = @"
CREATE TABLE #ZoneFilter
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL,
	[iID] [int] PRIMARY KEY CLUSTERED
)

";
                    zIDs = zIDs.Distinct().ToList();
                    foreach (int i in zIDs)
                        zoneFilterTable += "insert #ZoneFilter (iID) values (" + i.ToString() + ")\r\n\r\n";

                    if (sType == "Zones")
                        zoneFilterJoin = "inner join #ZoneFilter zf on zh.ZoneID = zf.iid inner join @tTmpDistinctZone t on zf.iid = t.i ";
                    else
                        zoneFilterJoin = "inner join GFI_FLT_ZoneHeadZoneType T1 on T1.ZoneHeadID = zh.ZoneID inner join #ZoneFilter zf on zf.iID = T1.ZoneTypeID inner join @tTmpDistinctZone t on T1.ZoneHeadID = t.i ";
                }

                s += zoneFilterTable + @"
;with cte as
(
	select h.*, zh.*
    from 
	    (
		    select *
			    , Geometry::STPointFromText('POINT(' + CONVERT(varchar, Longitude) + ' ' + CONVERT(varchar, Latitude) + ')', 4326) pointFr
		    from #idles
		) h
		inner join GFI_FLT_ZoneHeader zh WITH (INDEX(SIndx_GFI_FLT_ZoneHeader)) on zh.GeomData.STIntersects(h.pointFr) = 1 
		" + zoneFilterJoin + @"
	where Duration is not null
)
select UID, ZoneID, Description, Color into #ArrivalZoneTrips from cte

update #idles 
    set sZones = STUFF(
		                (SELECT ', ' + e.Description FROM #ArrivalZoneTrips e Where e.UID = h.UID FOR XML PATH(''))
	                , 1, 1, ''),
        iArrivalZones = STUFF(
		                (SELECT ', ' + CONVERT(varchar(25), e.ZoneID) FROM #ArrivalZoneTrips e Where e.UID = h.UID FOR XML PATH(''))
	                , 1, 1, ''),
        --sArrivalZonesColor = STUFF(
		--                (SELECT ', ' + CONVERT(varchar(25), e.Color) FROM #ArrivalZoneTrips e Where e.UID = h.UID FOR XML PATH(''))
	    --            , 1, 1, ''),
        iZone = (SELECT top 1 e.ZoneID FROM #ArrivalZoneTrips e Where e.UID = h.UID ),
        sZone = (SELECT top 1 e.Description FROM #ArrivalZoneTrips e Where e.UID = h.UID ),
        iZoneColor = (SELECT top 1 e.Color FROM #ArrivalZoneTrips e Where e.UID = h.UID )
from #idles h 
    where Duration is not null --and (RowNumByGrp % 10) = 1
";
                sql += s;
            }
            #endregion

            sql += @"
-----
alter table #idles add RN int
alter table #idles add FG int

;with cte as 
(
	SELECT UID, ROW_NUMBER() OVER(ORDER BY AssetID, DateTimeGPS_UTC) AS RowNum FROM #idles
)
update #idles 
	set RN = t.Rownum
from #idles i
	inner join cte t on t.UID = i.UID

;WITH CTE AS 
(
	select * from #idles
)
SELECT t.UID, nex.RN nexTN, ROW_NUMBER() OVER(PARTITION BY t.AssetID, t.iZone ORDER BY t.DateTimeGPS_UTC) - t.RN FG
into #tmpIdles from CTE t
	LEFT JOIN CTE nex ON nex.RN = t.RN + 1 and nex.AssetID = t.AssetID
order by t.AssetID, t.DateTimeGPS_UTC

update #idles
	set FG = t.FG
from #idles i
	inner join #tmpIdles t on t.UID = i.UID

update #idles set FG = null where iZone is null
update #idles set FinalGrpID = FG

alter table #idles Drop column RN 
alter table #idles drop column FG 
-----
;with cte as
(
	SELECT AssetID, FinalGrpID, min(DateTimeGPS_UTC) minTime, max(DateTimeGPS_UTC) maxTime, DATEDIFF(ss, min(DateTimeGPS_UTC), max(DateTimeGPS_UTC)) Duration
	FROM #idles
	group by AssetID, FinalGrpID, CONVERT(date, DateTimeGPS_UTC)
)
update #idles
	set FinalDuration = t.Duration
FROM #idles i
	inner join cte t on t.FinalGrpID = i.FinalGrpID and t.AssetID = i.AssetID
		inner join #Assets a on a.AssetID = i.AssetID

delete from #idles where FinalDuration < @IdleDuration
select distinct h.uid, e.ZoneID, e.Description ZoneDesc
    into #ZonesVisited 
FROM #idles h 
    inner join #ArrivalZoneTrips e on e.UID = h.UID

update #idles 
    set sDuration = CAST(FLOOR(FinalDuration / 86400) AS VARCHAR(10)) + ' Day(s), ' +
						CONVERT(VARCHAR, DATEADD(SECOND, FinalDuration, '19000101'), 8)

update #idles set sDuration = '0 Day(s), 00:00:00', FinalDuration = 0 where sDuration is null and iZone is not null

;with cte as
(
	SELECT *, ROW_NUMBER() OVER (PARTITION BY FinalGrpID ORDER BY Assetid, dtLocalTime desc) AS RunningRowNum
		FROM #idles 
	where Duration is not null
)
update #idles
	set RowNumByGrp = t.RunningRowNum 
FROM #idles i
	inner join cte t on t.UID = i.UID

select * into #PivotData from #idles 
	where FinalDuration is not null 
        and iZone is not null 
        and RowNumByGrp = 1 --(RowNumByGrp % 10) = 1
union
select * from #idles 
	where 
		FinalDuration >= @IdleDuration
		and iZone is not null 
		and FinalGrpID is null
order by Assetid, DateTimeGPS_UTC

select 'idles' as TableName
select *, DATEADD(second, FinalDuration * -1, dtLocalTime) + dtLocal dtLocalFrom from #PivotData order by Assetid, DateTimeGPS_UTC

select 'ZonesVisited'as TableName";
            if (reportType == "Visited" || reportType == "Both")
                sql += @"
select distinct ZoneID, ZoneDesc from #ZonesVisited

select 'ZoneDetailsVisited' as TableName
select zd.* from (select distinct ZoneID from #ZonesVisited) z
    inner join GFI_FLT_ZoneDetail zd on zd.ZoneID = z.ZoneID
";
            else sql += "\r\nSelect 1;";
            sql += @"
select 'NotVisited' as TableName";
            if (reportType == "Not Visited" || reportType == "Both")
                sql += @"
                select Distinct zh.ZoneID ,zh.Description from GFI_FLT_ZoneHeader zh 
" + zoneFilterJoin + @"
    --inner join @tTmpDistinctZone t on zh.ZoneID = t.i
    left outer join #ZonesVisited z on z.ZoneID = zh.ZoneID
    --left outer join #idles i on zh.ZoneID = i.iZone
where z.ZoneID is null

select 'ZoneDetailsNotVisited' as TableName
select Distinct zh.* from GFI_FLT_ZoneDetail zh 
" + zoneFilterJoin + @"
    --inner join @tTmpDistinctZone t on zh.ZoneID = t.i
    left outer join #ZonesVisited z on z.ZoneID = zh.ZoneID
where z.ZoneID is null
";
            else sql += "\r\nSelect 1";
            sql += @"

select 'ZoneVisitedTotal' as TableName
select Asset, sZone, iZone, Count(1) cnt
	from #idles 
where RowNumByGrp = 1 and sZone is not null
	group by Asset, sZone, iZone

--Pivot Table
DECLARE @DynamicPivotQuery AS NVARCHAR(MAX), @PivotColumnNames AS NVARCHAR(MAX)

--Get distinct values of the PIVOT Column
SELECT @PivotColumnNames = ISNULL(@PivotColumnNames + ',','') + QUOTENAME(sZone)
	FROM (SELECT DISTINCT sZone FROM #PivotData) AS cat
 where sZone is not null order by sZone

SET @DynamicPivotQuery = 
N'
SELECT * FROM 
(
	select Asset, dtLocal, iZone, sZone
	from #PivotData
) as s
PIVOT
(
	Count(iZone)
	for sZone in (' + @PivotColumnNames + ')
) as pvt 
order by CONVERT(datetime, dtLocal), Asset
';

select 'PivotData' as TableName
--Execute the Dynamic Pivot Query
EXEC sp_executesql @DynamicPivotQuery

--drop table #ArrivalZoneTrips
--drop table #Assets
--drop table #idles
--drop table #trip
--drop table #ZoneFilter
--drop table #ZonesVisited
--drop table #PivotData
--drop table #tmpIdles
";

            String strParamMain = dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = strParamMain;
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr, 1800);
            if (ds.Tables.Contains("PivotData"))
                if (ds.Tables["PivotData"].Columns.Count > 1)
                    if (ds.Tables["PivotData"].Columns[0].ColumnName == "dtlocal")
                        ds.Tables["PivotData"].Columns["dtlocal"].SetOrdinal(1);
            return ds;
        }

        public DataSet getIdlesForVCA(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, string reportType, int idleInMinutes, int idleSpeed, Boolean bGetVCA, String sType, List<int> vIDs, List<Matrix> lMatrix, Boolean bDriver, String sConnStr, int? Page, int? LimitPerPage)
        {
            Pagination p = Pagination.GetRowNums(Page, LimitPerPage);
            lAssetID = lAssetID.Distinct().ToList();

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 

Declare @IdlingSpeed int
set @IdlingSpeed = " + idleSpeed + @"

Declare @IdleDuration int
set @IdleDuration = (60 * " + idleInMinutes + @")

Declare @IdlingTime int
set @IdlingTime = 0

CREATE TABLE #Assets
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL,
	[AssetID] [int] PRIMARY KEY CLUSTERED,
    Asset nvarchar(120),
    TimeZone int Default 0
)
";

            foreach (int i in lAssetID)
                sql += "insert #Assets (AssetID) values (" + i.ToString() + ")\r\n";

            sql += @"
UPDATE #Assets
    SET Asset = T2.AssetNumber, TimeZone = T3.TotalMinutes
FROM #Assets T1
    INNER JOIN GFI_FLT_Asset T2 ON T2.AssetID = T1.AssetID
    INNER JOIN GFI_SYS_TimeZones T3 on T2.TimeZoneID = T3.StandardName

--Get all records
;WITH CTE AS 
(
	SELECT g.UID, g.DateTimeGPS_UTC, CONVERT(VARCHAR, DATEADD(mi, a.TimeZone, g.DateTimeGPS_UTC), 108) dtLocalTime, CONVERT(VARCHAR(11), DATEADD(mi, a.TimeZone, g.DateTimeGPS_UTC), 106) dtLocal
		, g.Speed, g.Longitude, g.Latitude
		, a.AssetID, a.Asset
		, rownum = ROW_NUMBER() OVER (PARTITION by g.AssetID ORDER BY g.DateTimeGPS_UTC)
	from GFI_GPS_GPSData g 
		inner join #Assets a on a.AssetID = g.AssetID
	where g.DateTimeGPS_UTC >= ? and g.DateTimeGPS_UTC <= ? and g.LongLatValidFlag = 1
)
SELECT cte.* into #trip 
	FROM CTE
order by Assetid, DateTimeGPS_UTC

--Get only idle details with Next record id
;WITH CTE AS 
(
	SELECT *
	from #trip where speed < @IdlingSpeed
)
SELECT distinct t.*
	, nex.RowNum NextIdleValue
	, null GrpID
	, null Duration
    , convert(nvarchar(20), null)  sDuration
	, null RowNumByGrp
    , convert(nvarchar(2500), null)  sVcas
    , null iVca
    , convert(nvarchar(2500), null) iVcaZones
    , convert(nvarchar(2500), null) sVca
    , null iVcaColor
	, null FinalGrpID
	, null FinalDuration
into #idles 
FROM #trip t
	left outer join CTE on t.UID = CTE.UID
	LEFT JOIN CTE nex ON nex.RowNum = CTE.RowNum + 1 and nex.AssetID = cte.AssetID
order by AssetID, DateTimeGPS_UTC

;with cte as
(
	SELECT *, ROW_NUMBER () OVER(partition by AssetID order by nextIdleValue) - NextIdleValue AS Grp
	FROM #idles
)
update #idles set GrpID = R.Grp
from #idles T
	inner join cte R on R.UID = T.UID

;with cte as
(
	SELECT AssetID, GrpID, max(DateTimeGPS_UTC) maxTime
		, DATEDIFF(ss, min(DateTimeGPS_UTC), max(DateTimeGPS_UTC)) Duration
	FROM #idles
	group by AssetID, GrpID
)
update #idles
	set Duration = t.Duration
FROM #idles i
	inner join cte t on t.GrpID = i.GrpID and t.AssetID = i.AssetID
where 
	t.Duration > @IdleDuration

;with cte as
(
	SELECT *, ROW_NUMBER() OVER (PARTITION BY GrpID ORDER BY Assetid, DateTimeGPS_UTC) AS RunningRowNum
		FROM #idles 
	where Duration is not null
)
update #idles
	set RowNumByGrp = t.RunningRowNum 
FROM #idles i
	inner join cte t on t.UID = i.UID

delete from #idles where Duration is null
update #idles set sDuration = CONVERT(varchar, DATEADD(s, Duration, 0), 108)

";

            String vcaFilterJoin = String.Empty;
            #region Zones
            if (bGetVCA)
            {
                String vcaFilterTable = String.Empty;
                String s = "--Select * from GFI_GIS_VCA"; //ZoneService.sGetZoneIDs(lMatrix, sConnStr);

                Boolean bZoneFiler = false;
                switch (sType)
                {
                    case "VCA":
                        bZoneFiler = true;
                        break;

                }

                if (bZoneFiler)
                {
                    vcaFilterTable = @"
CREATE TABLE #VcaFilter
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL,
	[iID] [int] PRIMARY KEY CLUSTERED
)
";
                    vIDs = vIDs.Distinct().ToList();
                    foreach (int i in vIDs)
                        vcaFilterTable += "insert #VcaFilter (iID) values (" + i.ToString() + ")\r\n";


                    vcaFilterJoin = "inner join #VcaFilter vf on vca.VCAID = vf.iid";

                }

                s += vcaFilterTable + @"  
;with cte as
(
	select h.*, vca.*, 1 inVca
    from 
	    (
		    select *
			    , Geometry::STPointFromText('POINT(' + CONVERT(varchar, Longitude) + ' ' + CONVERT(varchar, Latitude) + ')', 4326) pointFr
		    from #idles
		) h
		inner join GFI_GIS_VCA vca WITH (INDEX(SIndx_GFI_GIS_VCA)) on vca.GeomData.STIntersects(h.pointFr) = 1 
		" + vcaFilterJoin + @"
	where Duration is not null
)
select UID, VCAID, VCAName, iColor into #ArrivalVcaTrips from cte

update #idles 
    set sVcas = STUFF(
		                (SELECT ', ' + e.VCAName FROM #ArrivalVcaTrips e Where e.UID = h.UID FOR XML PATH(''))
	                , 1, 1, ''),
        iVcaZones = STUFF(
		                (SELECT ', ' + CONVERT(varchar(25), e.VCAID) FROM #ArrivalVcaTrips e Where e.UID = h.UID FOR XML PATH(''))
	                , 1, 1, ''),
        --sArrivalZonesColor = STUFF(
		--                (SELECT ', ' + CONVERT(varchar(25), e.Color) FROM #ArrivalVcaTrips e Where e.UID = h.UID FOR XML PATH(''))
	    --            , 1, 1, ''),
        iVca = (SELECT top 1 e.VCAID FROM #ArrivalVcaTrips e Where e.UID = h.UID ),
        sVca= (SELECT top 1 e.VCAID FROM #ArrivalVcaTrips e Where e.UID = h.UID ),
        iVcaColor = (SELECT top 1 e.iColor FROM #ArrivalVcaTrips e Where e.UID = h.UID )
from #idles h where Duration is not null --and (RowNumByGrp % 10) = 1
";
                sql += s;
            }
            #endregion

            sql += @"
-----
alter table #idles add RN int
alter table #idles add FG int

;with cte as 
(
	SELECT UID, ROW_NUMBER() OVER(ORDER BY AssetID, DateTimeGPS_UTC) AS RowNum FROM #idles
)
update #idles 
	set RN = t.Rownum
from #idles i
	inner join cte t on t.UID = i.UID

;WITH CTE AS 
(
	select * from #idles
)
SELECT t.UID, nex.RN nexTN, ROW_NUMBER() OVER(PARTITION BY t.AssetID, t.iVca ORDER BY t.DateTimeGPS_UTC) - t.RN FG
into #tmpIdles from CTE t
	LEFT JOIN CTE nex ON nex.RN = t.RN + 1 and nex.AssetID = t.AssetID
order by t.AssetID, t.DateTimeGPS_UTC

update #idles
	set FG = t.FG
from #idles i
	inner join #tmpIdles t on t.UID = i.UID

update #idles set FG = null where iVca is null
update #idles set FinalGrpID = FG

alter table #idles Drop column RN 
alter table #idles drop column FG 
-----
;with cte as
(
	SELECT AssetID, FinalGrpID, min(DateTimeGPS_UTC) minTime, max(DateTimeGPS_UTC) maxTime, DATEDIFF(ss, min(DateTimeGPS_UTC), max(DateTimeGPS_UTC)) Duration
	FROM #idles
	group by AssetID, FinalGrpID
)
update #idles
	set FinalDuration = t.Duration
FROM #idles i
	inner join cte t on t.FinalGrpID = i.FinalGrpID and t.AssetID = i.AssetID
		inner join #Assets a on a.AssetID = i.AssetID

delete from #idles where FinalDuration < @IdleDuration
select distinct h.uid, e.VCAID, e.VCAName VcaDesc
    into #VcaVisited 
FROM #idles h 
    inner join #ArrivalVcaTrips e on e.UID = h.UID

update #idles 
    set sDuration = CAST(FLOOR(FinalDuration / 86400) AS VARCHAR(10)) + ' Day(s), ' +
						CONVERT(VARCHAR, DATEADD(SECOND, FinalDuration, '19000101'), 8)

update #idles set sDuration = '0 Day(s), 00:00:00', FinalDuration = 0 where sDuration is null and iVca is not null

;with cte as
(
	SELECT *, ROW_NUMBER() OVER (PARTITION BY FinalGrpID ORDER BY Assetid, dtLocalTime desc) AS RunningRowNum
		FROM #idles 
	where Duration is not null
)
update #idles
	set RowNumByGrp = t.RunningRowNum 
FROM #idles i
	inner join cte t on t.UID = i.UID

select * into #PivotData from #idles 
	where FinalDuration is not null 
        and iVca is not null 
        and RowNumByGrp = 1 --(RowNumByGrp % 10) = 1
union
select * from #idles 
	where 
		FinalDuration >= @IdleDuration
		and iVca is not null 
		and FinalGrpID is null
order by Assetid, DateTimeGPS_UTC

select 'idles' as TableName
select *, DATEADD(second, FinalDuration * -1, dtLocalTime) + dtLocal dtLocalFrom from #PivotData order by Assetid, DateTimeGPS_UTC

select 'VcasVisited'as TableName";
            if (reportType == "Visited" || reportType == "Both")
                sql += @"
select distinct VCAID, VcaDesc from #VcaVisited
";

            else sql += "\r\nSelect 1";
            sql += @"

select 'VcaVisitedTotal' as TableName
select Asset, sVca, Count(1) cnt
	from #idles 
where RowNumByGrp = 1 and sVca is not null
	group by Asset, sVca

--Pivot Table
DECLARE @DynamicPivotQuery AS NVARCHAR(MAX), @PivotColumnNames AS NVARCHAR(MAX)

--Get distinct values of the PIVOT Column
SELECT @PivotColumnNames = ISNULL(@PivotColumnNames + ',','') + QUOTENAME(sVca)
	FROM (SELECT DISTINCT sVca FROM #PivotData) AS cat
 where sVca is not null order by sVca

SET @DynamicPivotQuery = 
N'
SELECT * FROM 
(
	select Asset, dtLocal, iVca, sVca
	from #PivotData
) as s
PIVOT
(
	Count(iVca)
	for sVca in (' + @PivotColumnNames + ')
) as pvt 
order by CONVERT(datetime, dtLocal), Asset
';

select 'PivotData' as TableName
--Execute the Dynamic Pivot Query
EXEC sp_executesql @DynamicPivotQuery

--drop table #ArrivalVcaTrips
--drop table #Assets
--drop table #idles
--drop table #trip
--drop table #VcaFilter
--drop table #VcaVisited
--drop table #PivotData
--drop table #tmpIdles
";

            String strParamMain = dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = strParamMain;
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr, 1800);
            return ds;
        }
    }
}

