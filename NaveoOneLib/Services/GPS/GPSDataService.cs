using System;
using System.Collections.Generic;
using System.Text;
using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using System.IO;
using NaveoOneLib.WebSpecifics;
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.GPS;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.Drivers;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Services.Assets;
using NaveoOneLib.Services.Zones;
using NaveoOneLib.Services.Fuels;
using NaveoOneLib.Services.Drivers;
using NaveoOneLib.Models.Fuels;
using System.Linq;
using NaveoOneLib.Maps;
using NaveoOneLib.Services.Trips;

namespace NaveoOneLib.Services.GPS
{
    public class GPSDataService
    {
        // Errors er = new Errors();
        public DataTable GetGPSData(String sConnStr)
        {
            String sql = @"SELECT UID, 
                                        AssetID, 
                                        DriverID,
                                        DateTimeGPS_UTC, 
                                        DateTimeServer, 
                                        Longitude, 
                                        Latitude, 
                                        LongLatValidFlag, 
                                        Speed, 
                                        EngineOn, 
                                        StopFlag, 
                                        TripDistance, 
                                        TripTime, 
                                        WorkHour from GFI_GPS_GPSData 
                                        order by UID";
            return new Connection(sConnStr).GetDataDT(sql, sConnStr);
        }

        public DataSet GetGPSDataDS(int FromiID, int ToiID, int AssetID, DbTransaction trans, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            String sql = @"SELECT UID, 
                                        AssetID, 
                                        DriverID,
                                        DateTimeGPS_UTC, 
                                        DateTimeServer, 
                                        Longitude, 
                                        Latitude, 
                                        LongLatValidFlag, 
                                        Speed, 
                                        EngineOn, 
                                        StopFlag, 
                                        TripDistance, 
                                        TripTime, 
                                        WorkHour from GFI_GPS_GPSData
                                        where UID >= ? and UID <= ? ";
            if (AssetID != -1)
                sql += " and AssetID = ? ";
            sql += " order by DateTimeGPS_UTC";
            String strParams = FromiID.ToString() + "�" + ToiID.ToString();
            if (AssetID != -1)
                strParams += "�" + AssetID.ToString();
            DataTable dtGPSData = c.GetDataDT(sql, strParams, trans, sConnStr);
            dtGPSData.TableName = "GPSData";
            DataSet ds = new DataSet();
            ds.Merge(dtGPSData);

            sql = "SELECT d.* from GFI_GPS_GPSData h, GFI_GPS_GPSDataDetail d ";
            sql += "where h.uid = d.uid and h.UID >= ? and h.UID <= ?";
            if (AssetID != -1)
                sql += " and h.AssetID = ?";
            DataTable dtGPSDataDetail = c.GetDataDT(sql, strParams, trans, sConnStr);
            dtGPSDataDetail.TableName = "GPSDataDetail";
            ds.Merge(dtGPSDataDetail);

            sql = "select * from dbo.GFI_FLT_Asset";
            strParams = String.Empty;
            if (AssetID != -1)
            {
                sql += " where AssetID = ?";
                strParams = AssetID.ToString();
            }
            DataTable dtGPSAsset = c.GetDataDT(sql, strParams, trans, sConnStr);
            dtGPSAsset.TableName = "GPSAsset";
            ds.Merge(dtGPSAsset);

            return ds;
        }

        List<GPSData> GetGPSData(DataSet ds)
        {
            //ds has 2 tables GPSData, GPSDataDetail
            List<GPSData> l = new List<GPSData>();

            Boolean bExistsLocalTime = false;
            if (ds.Tables["GPSData"].Columns.Contains("dtLocalTime"))
                bExistsLocalTime = true;
            Boolean bExistsAsset = false;
            if (ds.Tables["GPSData"].Columns.Contains("Asset"))
                bExistsAsset = true;
            Boolean bAssetStatus = false;
            if (ds.Tables["GPSData"].Columns.Contains("AssetStatus"))
                bAssetStatus = true;



            foreach (DataRow dr in ds.Tables["GPSData"].Rows)
            {
                GPSData retGPSData = new GPSData();
                retGPSData.UID = Convert.ToInt32(dr["UID"]);
                retGPSData.AssetID = Convert.ToInt32(dr["AssetID"]);

                if (!string.IsNullOrEmpty(dr["maintenancestatus"].ToString()))
                {
                    retGPSData.MaintenanceStatus = Convert.ToInt32(dr["maintenancestatus"]);
                }
                else
                {
                    retGPSData.MaintenanceStatus = 1;
                }

                retGPSData.LastOdometer = 0;

                retGPSData.DateTimeServer = (DateTime)dr["DateTimeServer"];
                retGPSData.Longitude = Convert.ToDouble(dr["Longitude"]);
                retGPSData.Latitude = Convert.ToDouble(dr["Latitude"]);
                retGPSData.Speed = Convert.ToDouble(dr["Speed"]);
                retGPSData.EngineOn = Convert.ToInt16(dr["EngineOn"]);

                if (bExistsLocalTime)
                {
                    retGPSData.dtLocalTime = Convert.ToDateTime(dr["dtLocalTime"]);
                    retGPSData.LastReportedInLocalTime = Convert.ToDateTime(dr["dtLocalTime"]);
                }

                if (bExistsLocalTime)
                    retGPSData.AssetNum = dr["Asset"].ToString();

                retGPSData.DateTimeGPS_UTC = Convert.ToDateTime(dr["DateTimeGPS_UTC"]);
                retGPSData.LongLatValidFlag = Convert.ToInt32(dr["LongLatValidFlag"]);

                retGPSData.StopFlag = Convert.ToInt32(dr["StopFlag"]);
                retGPSData.TripDistance = Convert.ToDouble(dr["TripDistance"]);
                retGPSData.TripTime = Convert.ToDouble(dr["TripTime"]);

                retGPSData.DriverID = Convert.ToInt32(dr["DriverID"]);

                if (bAssetStatus)
                    if (dr["AssetStatus"].ToString() == "Not Available")
                        retGPSData.Availability = dr["Comment"].ToString();

                if (ds.Tables["GPSData"].Columns.Contains("liveZone"))
                    retGPSData.Zone = dr["liveZoneID"].ToString() + ":" + dr["liveZone"].ToString();

                if (ds.Tables.Contains("Fuel"))
                {
                    DataRow[] drFuel = ds.Tables["Fuel"].Select("AssetID = " + retGPSData.AssetID.ToString());
                    if (drFuel.Length > 0)
                    {
                        Fuel f = new Models.Fuels.Fuel();
                        f.AssetID = Convert.ToInt32(drFuel[0]["AssetID"]);
                        f.ConvertedValue = Convert.ToInt32(drFuel[0]["ConvertedValue"]);
                        f.fType = drFuel[0]["fType"].ToString();
                        f.GPSDataId = Convert.ToInt32(drFuel[0]["UID"]);
                        f.dtLocalTime = Convert.ToDateTime(drFuel[0]["dtLocalTime"]);
                        f.Latitude = Convert.ToDouble(drFuel[0]["Latitude"]);
                        f.Longitude = Convert.ToDouble(drFuel[0]["Longitude"]);
                        f.UOM = drFuel[0]["Description"].ToString();
                        retGPSData.lastFuel = f;
                    }
                }

                if (ds.Tables.Contains("Heartbeat"))
                {
                    DataRow[] drLastReported = ds.Tables["Heartbeat"].Select("AssetID = " + retGPSData.AssetID.ToString());
                    if (drLastReported.Length > 0)
                    {
                        String sLastReported = drLastReported[0]["dtLocalTime"].ToString();
                        DateTime dLastReported = retGPSData.LastReportedInLocalTime;
                        if (DateTime.TryParse(sLastReported, out dLastReported))
                            if (dLastReported > retGPSData.LastReportedInLocalTime)
                                retGPSData.LastReportedInLocalTime = dLastReported;
                    }
                }

                List<GPSDataDetail> lgdg = new List<GPSDataDetail>();
                DataRow[] dataRows = ds.Tables["GPSDataDetail"].Select("UID = " + retGPSData.UID.ToString());
                foreach (DataRow dri in dataRows)
                {
                    GPSDataDetail g = new GPSDataDetail();
                    g.GPSDetailID = Convert.ToInt64(dri["GPSDetailID"]);
                    g.UID = Convert.ToInt32(dri["UID"]);
                    g.TypeID = dri["TypeID"].ToString();
                    g.TypeValue = dri["TypeValue"].ToString();
                    g.UOM = dri["UOM"].ToString();
                    g.Remarks = dri["Remarks"].ToString();
                    //Live Map Odo
                    if (ds.Tables["GPSData"].Columns.Contains("odo"))
                        g.Remarks = dr["odo"].ToString();

                    lgdg.Add(g);
                }
                retGPSData.GPSDataDetails = lgdg;

                l.Add(retGPSData);
            }

            return l;
        }

        public GPSData GetGPSDataByUID(int UID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"select * from GFI_GPS_GPSData where UID = " + UID;
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "GPSData";
            dtSql.Rows.Add(drSql);

            sql = @"select * from GFI_GPS_GPSDataDetail where UID = " + UID;
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "GPSDataDetail";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return GetGPSData(ds)[0];
        }

        public GPSData GetGPSDataById(String iID, String sConnStr)
        {
            List<GPSData> l = GetGPSData(Convert.ToInt32(iID), Convert.ToInt32(iID), -1, null, sConnStr);
            if (l.Count == 0)
                return null;
            else
                return l[0];
        }
        public GPSData GetGPSDataId(int AssetID, DateTime DateTimeGPS_UTC, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            String sql = @"SELECT UID, 
                                        AssetID, 
                                        DriverID,
                                        DateTimeGPS_UTC, 
                                        DateTimeServer, 
                                        Longitude, 
                                        Latitude, 
                                        LongLatValidFlag, 
                                        Speed, 
                                        EngineOn, 
                                        StopFlag, 
                                        TripDistance, 
                                        TripTime, 
                                        WorkHour from GFI_GPS_GPSData
                                        WHERE AssetID = ? and DateTimeGPS_UTC = ?
                                        order by UID";
            DataTable dtGPSData = c.GetDataDT(sql, AssetID.ToString() + "�" + DateTimeGPS_UTC.ToString(), null, sConnStr);
            if (dtGPSData.Rows.Count == 0)
            {
                sql = @"SELECT top 1 UID, 
                                        AssetID, 
                                        DriverID,
                                        DateTimeGPS_UTC, 
                                        DateTimeServer, 
                                        Longitude, 
                                        Latitude, 
                                        LongLatValidFlag, 
                                        Speed, 
                                        EngineOn, 
                                        StopFlag, 
                                        TripDistance, 
                                        TripTime, 
                                        WorkHour from GFI_GPS_GPSData
                                        WHERE AssetID = ? and DateTimeGPS_UTC >= ?
                                        order by DateTimeGPS_UTC";
                dtGPSData = c.GetDataDT(sql, AssetID.ToString() + "�" + DateTimeGPS_UTC.ToString(), null, sConnStr);
                if (dtGPSData.Rows.Count == 0)
                    return null;
                else
                    DateTimeGPS_UTC = (DateTime)dtGPSData.Rows[0]["DateTimeGPS_UTC"];
            }

            dtGPSData.TableName = "GPSData";
            DataSet ds = new DataSet();
            ds.Merge(dtGPSData);

            sql = "SELECT d.* from GFI_GPS_GPSData h, GFI_GPS_GPSDataDetail d ";
            sql += "where h.uid = d.uid and h.DateTimeGPS_UTC = ? and h.AssetID = ?";
            DataTable dtGPSDataDetail = c.GetDataDT(sql, DateTimeGPS_UTC.ToString() + "�" + AssetID.ToString(), null, sConnStr);
            dtGPSDataDetail.TableName = "GPSDataDetail";
            ds.Merge(dtGPSDataDetail);

            sql = "select * from dbo.GFI_FLT_Asset where AssetID = ?";
            DataTable dtGPSAsset = c.GetDataDT(sql, AssetID.ToString(), null, sConnStr);
            dtGPSAsset.TableName = "GPSAsset";
            ds.Merge(dtGPSAsset);

            List<GPSData> l = GetGPSData(ds);
            if (l.Count == 0)
                return null;
            else
                return l[0];
        }

        public List<GPSData> GetLatestGPSData(int uLogin, List<String> lAssets, Boolean byDriver, String sConnStr)
        {
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin, sConnStr);
            List<int> lAllAssetID = new LibData().GetAllAssets(lm, sConnStr);

            String sAssets = String.Empty;
            foreach (String s in lAssets)
                sAssets += "'" + BaseService.Encrypt(s) + "', ";
            if (sAssets.Length > 0)
                sAssets = sAssets.Substring(0, sAssets.Length - 2);

            String sql = "select AssetID, AssetNumber from GFI_FLT_Asset where AssetNumber in (" + sAssets + ")";
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtAssets";
            dtSql.Rows.Add(drSql);
            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            List<int> lAssetID = new List<int>();
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                int iAssetID = 0;
                int.TryParse(dr["AssetID"].ToString(), out iAssetID);
                int x = lAllAssetID.Find(y => y == iAssetID);
                if (x != 0)
                    lAssetID.Add(x);
            }
            if (lAssetID.Count == 0)
                return new List<GPSData>();

            return GetLatestGPSData(lAssetID, new List<string>(), byDriver, 0, false, sConnStr);
        }
        public List<GPSData> GetLatestGPSData(int uLogin, Boolean byDriver, String sConnStr)
        {
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin, sConnStr);
            return GetLatestGPSData(lm, byDriver, sConnStr);
        }
        public List<GPSData> GetLatestGPSData(List<Matrix> lMatrix, Boolean byDriver, String sConnStr)
        {
            List<int> lAssetID = new LibData().GetAllAssets(lMatrix, sConnStr);

            DataSet ds = GetLatestGPSDataDS(lAssetID, new List<string>(), byDriver, 0, false, sConnStr);
            List<GPSData> l = GetGPSData(ds);
            return l;
        }
        public List<GPSData> GetLatestGPSData(List<int> lAssetID, List<String> lType, Boolean byDriver, int uLogin, Boolean bGetZones, String sConnStr)
        {
            DataSet ds = GetLatestGPSDataDS(lAssetID, lType, byDriver, uLogin, bGetZones, sConnStr);

            List<GPSData> l = new List<GPSData>();
            int aa = 0;
            try
            {
                DataTable dtOdometer = new TripHeaderService().getInitialOdoInput(lAssetID, DateTime.Now, sConnStr);

                l = GetGPSData(ds);

                foreach (GPSData data in l)
                {
                    aa = data.AssetID;
                    if (aa > 0)
                    {
                        DataRow[] drOdo = dtOdometer.Select("assetID = " + data.AssetID);

                        if (drOdo.Length > 0)
                        {
                            decimal.TryParse(drOdo[0]["RealOdometer"].ToString(), out decimal RealOdometer);
                            data.LastOdometer = Math.Round(RealOdometer);
                        }
                    }
                    
                }
            }
            catch (Exception ex)
            {


                System.IO.File.AppendAllText(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "\\Registers\\live.dat", "\r\n 2nd Asset Creation on fleet\r\n Asset :" + string.Join(",", lAssetID.Select(n => n.ToString()).ToArray()) + ex.ToString() + "\r\n");

            }
            if (bGetZones)
                l = new Maps.ESRIMapService().LatestGPSData(l, ds);

            int bb = 0;

            try
            {
                foreach (GPSData g in l)
                {
                    if (g.Availability.Contains("Suspect"))
                    {
                        if (ds.Tables.Contains("HeartBeat"))
                        {
                            bb = g.AssetID;
                            DataRow[] drs = ds.Tables["HeartBeat"].Select("AssetID = " + g.AssetID);
                            if (drs.Length > 0)
                            {
                                DateTime dtHB = DateTime.UtcNow;
                                if (DateTime.TryParse(drs[0]["dtLocalTime"].ToString(), out dtHB))
                                {
                                    dtHB = Convert.ToDateTime(drs[0]["dtLocalTime"]);
                                    if (Math.Abs((dtHB - DateTime.UtcNow).TotalHours) < 5)
                                        g.Availability = g.Availability.Replace("Suspect", "Stop");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "\\Registers\\live.dat", "\r\n 3rd Asset Creation on fleet\r\n Asset :" + bb + ex.ToString() + "\r\n");

            }

            //unit never reported.
            try
            {
                foreach (int i in lAssetID)
                {
                    DataRow[] dr = ds.Tables["GPSData"].Select("AssetID = " + i.ToString());
                    if (dr.Length == 0)
                    {
                        DataRow[] drAsset = ds.Tables["GPSAsset"].Select("iAssetID = " + i);
                        if (drAsset.Length == 0)
                        {
                            l.Add(new GPSData
                            {
                                AssetID = i,
                                AssetNum = drAsset[0]["Asset"].ToString(),
                                Availability = "Suspect||Never reported",
                                DriverID = 1,
                                LongLatValidFlag = 0,
                                Latitude = 0.0,
                                Longitude = 0.0,
                                Speed = 0,
                                MaintenanceStatus = 1
                            });
                        }
                        else
                            l.Add(new GPSData
                            {
                                AssetID = i,
                                Availability = "Suspect||No access to asset",
                                DriverID = 1,
                                LongLatValidFlag = 0,
                                Latitude = 0.0,
                                Longitude = 0.0,
                                Speed = 0,
                                MaintenanceStatus = 1
                            });
                    }
                }
            }
            catch { }

            return l;
        }
        DataSet GetLatestGPSDataDS(List<int> lAssetID, List<String> lType, Boolean byDriver, int uLogin, Boolean bGetZones, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = sGetLatestGPSData(lAssetID, lType, byDriver, uLogin, bGetZones, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);
            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            return ds;
        }
        public String sGetLatestGPSData(List<int> lAssetID, List<String> lType, Boolean byDriver, int uLogin, Boolean bGetZones, String sConnStr)
        {
            if (byDriver)
            {
                String g = "(";
                foreach (int i in lAssetID)
                    g += i.ToString() + ", ";
                g = g.Substring(0, g.Length - 2) + ")";
                String s = @"
;with cte as 
(
    select l.AssetID
        , row_number() over(partition by l.AssetID order by l.DateTimeGPS_UTC desc) as RowNum
	from GFI_GPS_LiveData l
    WHERE DateTimeGPS_UTC < GETUTCDATE() + 1 and l.DriverID in " + g + @"
)
select cte.*
from cte where RowNum = 1
";

                DataTable dt = new Connection(sConnStr).GetDataDT(s, sConnStr);
                lAssetID = new List<int>();
                foreach (DataRow dr in dt.Rows)
                    lAssetID.Add(Convert.ToInt32(dr["AssetID"]));
            }

            String sql = @"
--RetrieveLiveData
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 

CREATE TABLE #Assets
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL,
	[iAssetID] [int] PRIMARY KEY CLUSTERED,
    Asset nvarchar(120),
    maintenancestatus int,
    TimeZone int Default 0
)
--Oracle Starts Here
";
            String sAsset = String.Empty;
            foreach (int i in lAssetID)
                sAsset += "insert #Assets (iAssetID) values (" + i.ToString() + ");\r\n";

            sql += sAsset + @"
UPDATE #Assets
    SET Asset = T2.AssetNumber, TimeZone = T3.TotalMinutes, maintenancestatus = T2.maintenancestatus
FROM #Assets T1
    INNER JOIN GFI_FLT_Asset T2 ON T2.AssetID = T1.iAssetID
    INNER JOIN GFI_SYS_TimeZones T3 on T2.TimeZoneID = T3.StandardName

--CREATE TEMP TABLE PostgreSQLGFI_GPS_LiveData AS 
;with cte as 
(
    select l.uid, l.AssetID, a.Asset, a.maintenancestatus,
        l.DriverID,
        l.DateTimeGPS_UTC, DATEADD(mi, a.TimeZone, l.DateTimeGPS_UTC) dtLocalTime,
        l.DateTimeServer, 
        l.Longitude, 
        l.Latitude, 
        l.LongLatValidFlag, 
        l.Speed, 
        l.EngineOn, 
        l.StopFlag, 
        l.TripDistance, 
        l.TripTime, 
        convert(nvarchar(250), '') liveZone, null liveZoneID,
        row_number() over(partition by l.AssetID order by l.DateTimeGPS_UTC desc) as RowNum
	from [GFI_GPS_LiveData] l
";
            if (lType != null && lType.Count > 0)
                sql += " inner join GFI_GPS_LiveDataDetail d on l.uid = d.uid and d.TypeID in (" + String.Join(",", lType.Select(s => "'" + s + "'")) + ") ";

            sql += @"--Cont
    inner join #Assets a on a.iAssetID = l.AssetID
    WHERE DateTimeGPS_UTC < GETUTCDATE() + 1
)
select cte.*, '' AssetStatus, '' Comment into #GFI_GPS_LiveData 
from cte where RowNum <= 2

";

            #region Zones Related
            if (bGetZones)
            {
                List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin, sConnStr);
                sql += ZoneService.sGetZoneIDs(lm, sConnStr);
                sql += @"
                        --CREATE TEMP TABLE PostgreSQLLiveZoneTmp AS 
                        ;with cte as
                        (
	                        select h.*, z.*
                            from 
	                            (
		                            select *
			                            , Geometry::STPointFromText('POINT(' + CONVERT(varchar, Longitude) + ' ' + CONVERT(varchar, Latitude) + ')', 4326) pointFr
		                            from #GFI_GPS_LiveData
	                            ) h
								inner join 
									(
										select zh.* from GFI_FLT_ZoneHeader zh --WITH (INDEX(SIndx_GFI_FLT_ZoneHeader))
											--inner join @tTmpDistinctZone t on zh.ZoneID = t.i
									) z on z.GeomData.STIntersects(pointFr) = 1
                        )
                        select * into #LiveZoneTmp from cte

                        --CREATE TEMP TABLE PostgreSQLLiveZone AS 
                        select t1.* into #LiveZone 
                            from #LiveZoneTmp t1
                                inner join @tTmpDistinctZone t2 on t1.ZoneID = t2.i


                        update #GFI_GPS_LiveData 
                            set liveZoneID = (SELECT top 1 e.ZoneID FROM #LiveZone e Where e.UID = h.UID)
	                            , liveZone = coalesce ((SELECT top 1 Coalesce(e.Description, '') FROM #LiveZone e Where e.UID = h.UID) , '')            
                        from #GFI_GPS_LiveData h;
                        ";
            }
            #endregion

            sql += @"
SELECT 'GPSData' AS TableName
select T1.*, 0 odo from #GFI_GPS_LiveData T1;

SELECT 'GPSDataDetail' AS TableName
SELECT d.* from GFI_GPS_LiveDataDetail d
    inner join #GFI_GPS_LiveData l on l.uid = d.uid;

SELECT 'GPSDriver' AS TableName
select distinct d.DriverID, d.sDriverName from #GFI_GPS_LiveData l 
    inner join GFI_FLT_Driver d on d.DriverID = l.DriverID;

SELECT 'Heartbeat' AS TableName
select f1.AssetID, DATEADD(mi, f2.TimeZone, f1.DateTimeGPS_UTC) dtLocalTime 
from dbo.GFI_GPS_HeartBeat f1 
    inner join #Assets f2 on f1.AssetID = f2.iAssetID;

SELECT 'Fuel' AS TableName
;with cte as
(
    select f.AssetID, f.ConvertedValue, f.fType, f.UID
        , DATEADD(mi, a.TimeZone, f.DateTimeGPS_UTC) dtLocalTime
        , f.Latitude, f.Longitude, u.Description 
	    , row_number() over(partition by f.AssetID order by f.DateTimeGPS_UTC desc) as RowNum
    from GFI_GPS_FuelData f
	    inner join GFI_SYS_UOM u on f.UOM = u.UID
		inner join #Assets a on a.iAssetID = f.AssetID 
)
select * from cte where RowNum = 1;

SELECT 'GpsAsset' AS TableName
select * from #Assets
";

            return sql;
        }

        public String sGetLatestGPSDataForAllAssets()
        {
            String sql = @"
;with cte as (
    select 'CoreDB1' SvrDBName,
		l.uid, l.AssetID, a.AssetName,
        l.DriverID, dr.sDriverName
        DateTimeGPS_UTC, 
        DateTimeServer, 
        Longitude, 
        Latitude, 
        LongLatValidFlag, 
        Speed, 
        EngineOn, 
        StopFlag, 
        TripDistance, 
        TripTime, 
        row_number() over(partition by l.AssetID order by l.DateTimeGPS_UTC desc) as RowNum
        , '' FN
	from GFI_GPS_LiveData l
		inner join GFI_GPS_LiveDataDetail d on l.UID = d.UID
		inner join GFI_FLT_Asset a on a.AssetID = l.AssetID
		inner join GFI_FLT_Driver dr on dr.DriverID = l.DriverID
    WHERE DateTimeGPS_UTC < GETUTCDATE() + 1
	    )
select cte.* 
    , '' AssetStatus, '' Comment
from cte
    where RowNum <= 2

";
            return sql;
        }

        public List<GPSData> GetGPSData(int FromiID, int ToiID, int AssetID, DbTransaction trans, String sConnStr)
        {
            DataSet ds = GetGPSDataDS(FromiID, ToiID, AssetID, trans, sConnStr);
            List<GPSData> l = GetGPSData(ds);
            return l;
        }

        public List<GPSData> GetGetDebugDataList(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            return new GPSDataService().GetDebugDataList(lAssetID, dtFrom, dtTo, sConnStr);
        }

        public List<GPSData> GetDebugDataList(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            String strAssetID = String.Empty;
            foreach (int i in lAssetID)
                strAssetID += i.ToString() + ",";
            strAssetID = strAssetID.Substring(0, strAssetID.Length - 1);

            String sqlString = @"SELECT UID, 
                                        AssetID, 
                                        DriverID,
                                        DateTimeGPS_UTC, 
                                        DateTimeServer, 
                                        Longitude, 
                                        Latitude, 
                                        LongLatValidFlag, 
                                        Speed, 
                                        EngineOn, 
                                        StopFlag, 
                                        TripDistance, 
                                        TripTime, 
                                        WorkHour from GFI_GPS_GPSData h
                                        WHERE AssetID IN (" + strAssetID + @") 
                                              AND h.DateTimeGPS_UTC >= ? and h.DateTimeGPS_UTC <= ?
                                        order by UID";

            DataTable dt = new Connection(sConnStr).GetDataDT(sqlString, dtFrom.ToString() + "�" + dtTo.ToString(), null, sConnStr);
            List<GPSData> glist = new List<GPSData>();
            foreach (DataRow row in dt.Rows)
            {
                GPSData retGPSData = new GPSData();

                retGPSData.UID = Convert.ToInt32(row["UID"]);
                retGPSData.AssetID = Convert.ToInt32(row["AssetID"]);
                retGPSData.DateTimeGPS_UTC = (DateTime)row["DateTimeGPS_UTC"];
                retGPSData.DateTimeServer = (DateTime)row["DateTimeServer"];
                retGPSData.Longitude = (double)row["Longitude"];
                retGPSData.Latitude = (double)row["Latitude"];
                retGPSData.DriverID = Convert.ToInt32(row["DriverID"]);


                try
                {
                    retGPSData.Speed = (float)row["Speed"];
                }
                catch
                {
                    retGPSData.Speed = 0;
                }
                finally
                {

                }

                retGPSData.EngineOn = Convert.ToInt32(row["EngineOn"]);
                retGPSData.StopFlag = Convert.ToInt32(row["StopFlag"]);
                try
                {
                    retGPSData.TripDistance = (float)row["TripDistance"];
                }
                catch
                {
                    retGPSData.TripDistance = 0;
                }

                try
                {
                    retGPSData.TripTime = (float)row["TripTime"];
                }
                catch
                {
                    retGPSData.TripTime = 0;
                }
                retGPSData.WorkHour = Convert.ToInt32(row["WorkHour"]);

                glist.Add(retGPSData);

            }
            return glist;
        }

        public DataTable GetDebugData(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            String strAssetID = String.Empty;
            foreach (int i in lAssetID)
                strAssetID += i.ToString() + ",";
            strAssetID = strAssetID.Substring(0, strAssetID.Length - 1);

            String sql = @"SELECT * from GFI_GPS_GPSData h, GFI_GPS_GPSDataDetail d 
                            where h.uid = d.uid 
	                            and h.DateTimeGPS_UTC >= ? and h.DateTimeGPS_UTC <= ?
	                            and h.AssetID in (" + strAssetID + ") order by h.AssetID";

            DataTable dtResult = new Connection(sConnStr).GetDataDT(sql, dtFrom.ToString() + "�" + dtTo.ToString(), null, sConnStr);
            return dtResult;
        }
        public DataTable GetDebugData(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, List<String> lTypeID, String sConnStr)
        {
            String strAssetID = String.Empty;
            foreach (int i in lAssetID)
                strAssetID += i.ToString() + ",";
            strAssetID = strAssetID.Substring(0, strAssetID.Length - 1);

            String strTypeID = String.Empty;
            foreach (String s in lTypeID)
                strTypeID += s.ToString() + "','";
            strTypeID = "'" + strTypeID.Substring(0, strTypeID.Length - 2);

            String sql = @"SELECT * from GFI_GPS_GPSData h
                            inner join GFI_GPS_GPSDataDetail d on h.uid = d.uid 
	                        where h.DateTimeGPS_UTC >= ? and h.DateTimeGPS_UTC <= ?
	                            and h.AssetID in (" + strAssetID + @") 
                                and d.TypeID in (" + strTypeID + @")
                        order by h.AssetID, DateTimeGPS_UTC";

            DataTable dtResult = new Connection(sConnStr).GetDataDT(sql, dtFrom.ToString() + "�" + dtTo.ToString(), null, sConnStr);
            return dtResult;
        }
        public DataTable GetDebugData(int AssetID, DateTime dtFrom, DateTime dtTo, List<String> lTypeID, int TimeInterval, String sConnStr)
        {
            String strTypeID = String.Empty;
            foreach (String s in lTypeID)
                strTypeID += s.ToString() + "','";
            strTypeID = "'" + strTypeID.Substring(0, strTypeID.Length - 2);

            String sql = @"
select h.* into #GFI_GPS_GPSData
from GFI_GPS_GPSData h
	inner join GFI_GPS_GPSDataDetail d on h.uid = d.uid 
where h.DateTimeGPS_UTC >= ? and h.DateTimeGPS_UTC <= ?
	and h.AssetID = " + AssetID + @" 
	and d.TypeID in (" + strTypeID + @")

select d.* into #GFI_GPS_GPSDataDetail
from GFI_GPS_GPSData h
	inner join GFI_GPS_GPSDataDetail d on h.uid = d.uid 
where h.DateTimeGPS_UTC >= ? and h.DateTimeGPS_UTC <= ?
	and h.AssetID = " + AssetID + @" 
	and d.TypeID in (" + strTypeID + @")

;with cte as
(
	SELECT 
		'LocalDate' = CONVERT(VARCHAR(12), CAST(DATEADD(MINUTE, t.TotalMinutes, g.DateTimeGPS_UTC) AS date), 101)
		, 'LocalTimeThirty' = IIF(LEN(CAST(DATEPART(HOUR, DATEADD(MINUTE, t.TotalMinutes, g.DateTimeGPS_UTC)) AS char(2))) = 1, '0' + CAST(DATEPART(HOUR, DATEADD(MINUTE, t.TotalMinutes, g.DateTimeGPS_UTC)) AS char(1)), CAST(DATEPART(HOUR, DATEADD(MINUTE, t.TotalMinutes, g.DateTimeGPS_UTC)) AS char(2))) + ':' + IIF(CAST(DATEPART(MINUTE, DATEADD(MINUTE, t.TotalMinutes, g.DateTimeGPS_UTC))/30 AS char(2))='1', '30', '00')
		, UID
		FROM #GFI_GPS_GPSData g
		inner join GFI_FLT_Asset t1 on t1.AssetID = g.AssetID
		inner join GFI_SYS_TimeZones t on t1.TimeZoneID = t.StandardName
	GROUP BY
		CAST(DATEADD(MINUTE, t.TotalMinutes, g.DateTimeGPS_UTC) AS date)
		, DATEPART(HOUR, DATEADD(MINUTE, t.TotalMinutes, g.DateTimeGPS_UTC))
		, DATEPART(MINUTE, DATEADD(MINUTE, t.TotalMinutes, g.DateTimeGPS_UTC))/30
		, UID
)
, cte1 as
(
    select *
	    , Row_Number() Over (Partition By DateDiff(MINUTE, 30, LocalTimeThirty) Order By NewId()) RN
	    , Row_Number() Over (Partition By DateDiff(day, 0, LocalDate) Order By NewId()) RNDay
	    from cte
)	
select * 
	into #Thirty
from cte1 c;

select h.UID, h.AssetID, d.TypeID, d.TypeValue, d.UOM, t1.AssetName, t.LocalTimeThirty AS LocalTimeThirty
    , DATEADD(MINUTE, tz.TotalMinutes, h.DateTimeGPS_UTC) localTime, CONVERT(varchar, DATEADD(MINUTE, tz.TotalMinutes, h.DateTimeGPS_UTC), 105) GrpDate, h.DateTimeGPS_UTC
into #FinalData 
from #GFI_GPS_GPSData h
	inner join #Thirty t on h.UID = t.UID
	inner join #GFI_GPS_GPSDataDetail d on h.uid = d.uid 
	inner join GFI_FLT_Asset t1 on t1.AssetID = h.AssetID
	inner join GFI_SYS_TimeZones tz on t1.TimeZoneID = tz.StandardName

--Pivot Table
DECLARE @DynamicPivotQuery AS NVARCHAR(MAX), @PivotColumnNames AS NVARCHAR(MAX)

--Get distinct values of the PIVOT Column
SELECT @PivotColumnNames= ISNULL(@PivotColumnNames + ',','') + QUOTENAME(LocalTimeThirty)
	FROM (SELECT DISTINCT LocalTimeThirty FROM #FinalData) AS cat
order by LocalTimeThirty

SET @DynamicPivotQuery = 
N'
SELECT * FROM 
(
    SELECT GrpDate, AssetID, AssetName, TypeID, TypeValue, UOM, LocalTimeThirty  
    FROM #FinalData 
) as s
PIVOT
(
	max(TypeValue) for LocalTimeThirty in (' + @PivotColumnNames + ')
) as pvt 
order by AssetName
';

--select 'Pivot' as TableName
--Execute the Dynamic Pivot Query
EXEC sp_executesql @DynamicPivotQuery

--print @DynamicPivotQuery
--select * from #found
drop table #Thirty
drop table #FinalData 
";
            DataTable dtResult = new Connection(sConnStr).GetDataDT(sql, dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "�" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "�" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "�" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "�" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "�" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff"), null, sConnStr);
            return dtResult;
        }

        String sGetDebugData(List<int> lAssetID, String sWorkHrs, int? mintemp, int? maxtemp, List<String> lTypeID, String sConnStr, int? Page, int? LimitPerPage)
        {
            String strTypeID = String.Empty;
            foreach (String s in lTypeID)
                strTypeID += "'" + s.ToString() + "', ";
            if (!String.IsNullOrEmpty(strTypeID))
                strTypeID = strTypeID.Substring(0, strTypeID.Length - 2);

            sWorkHrs = sWorkHrs.Replace("h.dtStart", "h.DateTimeGPS_UTC");

            String sql = @"
--GetSensorData
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 

CREATE TABLE #Assets
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL,
	[iAssetID] [int] PRIMARY KEY CLUSTERED
)

";
            foreach (int i in lAssetID)
                sql += "insert #Assets (iAssetID) values (" + i.ToString() + ");\r\n";

            sql += @"
select h.RowNum, h.UID, h.AssetID, h.DateTimeGPS_UTC, h.DateTimeServer, h.details, h.Longitude, h.Latitude, h.LongLatValidFlag, h.Speed, h.EngineOn, h.StopFlag, h.TripDistance,
CAST(DATEADD(second, h.TripTime, '1900-01-01') AS TIME) as TripTime, h.WorkHour, h.DriverID, h.RoadSpeed, h.localTime, h.details, h.AssetName as assetName, h.sDriverName from 
    (
        SELECT row_number() OVER (order by h.DateTimeGPS_UTC) AS RowNum
			, h.*, DATEADD(MINUTE, t.TotalMinutes, h.DateTimeGPS_UTC) localTime
			, + STUFF(
			(SELECT '| ' + d.TypeID + ' : ' + d.TypeValue + ' : ' + d.UOM FROM GFI_GPS_GPSDataDetail d Where d.UID = h.UID FOR XML PATH(''))
			, 1, 1, '') As details 
			, t1.assetName, t2.sDriverName from GFI_GPS_GPSData h 
            --xUsedBelow
        inner join #Assets a on a.iAssetID = h.AssetID
		inner join GFI_FLT_Asset t1 on t1.AssetID = h.AssetID
		inner join GFI_FLT_Driver t2 on t2.DriverID = h.DriverID
    	inner join GFI_SYS_TimeZones t on t1.TimeZoneID = t.StandardName
        where h.DateTimeGPS_UTC >= ? and h.DateTimeGPS_UTC <= ?
    ) h
where 1 = 1 ";

            if (!String.IsNullOrEmpty(strTypeID))
            {
                sql = sql.Replace("GFI_GPS_GPSDataDetail d Where d.UID = h.UID FOR XML PATH", "GFI_GPS_GPSDataDetail d Where d.UID = h.UID and d.TypeID in (" + strTypeID + ") FOR XML PATH");
                sql = sql.Replace("--xUsedBelow", "inner join GFI_GPS_GPSDataDetail d on d.UID = h.UID and d.TypeID in (" + strTypeID + ")");
            }
            if (mintemp.HasValue && maxtemp.HasValue)
                sql += " and TRY_CONVERT(float,d.TypeValue) >= " + mintemp + " and  TRY_CONVERT(float,d.TypeValue) <= " + maxtemp + " ";

            if (Page.HasValue)
            {
                Pagination p = Pagination.GetRowNums(Page, LimitPerPage);
                sql += " and (h.RowNum between " + p.RowFrom + " and " + p.RowTo + ") ";
            }
            sql += sWorkHrs + " order by h.AssetID, DateTimeGPS_UTC";

            return sql;
        }

        /// <summary>
        /// IF AUX
        /// </summary>
        /// <param name="lAssetID"></param>
        /// <param name="sWorkHrs"></param>
        /// <param name="mintemp"></param>
        /// <param name="maxtemp"></param>
        /// <param name="lTypeID"></param>
        /// <param name="sConnStr"></param>
        /// <param name="Page"></param>
        /// <param name="LimitPerPage"></param>
        /// <param name="IfAux"></param>
        /// <returns></returns>
        String sGetDebugData(List<int> lAssetID, String sWorkHrs, int? mintemp, int? maxtemp, List<String> lTypeID, String sConnStr, int? Page, int? LimitPerPage, bool IfAux)
        {


            string[] strTypeID = new string[lTypeID.Count];
            int found = 0;

            foreach (String s in lTypeID)
            {
                found = s.IndexOf(":");
                strTypeID[0] = s.Substring(0, found);
                //strTypeID[1] = s.Substring(found + 1);

            }


            String sql = @"
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 

CREATE TABLE #Assets
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL,
	[iAssetID] [int] PRIMARY KEY CLUSTERED,
)
";
            foreach (int i in lAssetID)
                sql += "insert #Assets (iAssetID) values (" + i.ToString() + ")\r\n";

            sql += @"
select row_number() OVER (order by h.DateTimeGPS_UTC) AS RowNumber, 'AUX2' AS Auxiliary, 'DOOR' AS AuxiliaryDescription,  h.UID, Longitude, Latitude, IIF(d.TypeValue='On', '0', '1') AS TypeValues, DATEADD(MINUTE, tz.TotalMinutes, h.DateTimeGPS_UTC) AS LocalTime, a.iAssetID, t1.AssetNumber
	, t2.sDriverName into #AuxON from GFI_GPS_GPSData h 
    inner join GFI_GPS_GPSDataDetail d on d.UID = h.UID and d.TypeID in ('AUX2')
       inner join #Assets a on a.iAssetID = h.AssetID
       inner join GFI_FLT_Asset t1 on t1.AssetID = h.AssetID
       inner join GFI_SYS_TimeZones tz on t1.TimeZoneID = tz.StandardName
       inner join GFI_FLT_Driver t2 on t2.DriverID = h.DriverID
where h.DateTimeGPS_UTC >= ? and h.DateTimeGPS_UTC <= ?
order by h.DateTimeGPS_UTC

--drop table #FinalDataAux;

;with cte as
(
       select * from #AuxON z1 where z1.RowNumber = 1 and TypeValues = 1
       UNION
       select Z2.* from #AuxON z1
       inner join #AuxON z2 on z2.TypeValues = 1 and z2.RowNumber = z1.RowNumber + 1 and z1.TypeValues = 0
       union
       --Leaving
       select Z2.* from #AuxON z1
              inner join #AuxON z2 on z2.TypeValues = 0 and z2.RowNumber = z1.RowNumber + 1 and z1.TypeValues = 1
)
select cte.*, t1.LocalTime As EndDate, CAST(DATEADD(second, DATEDIFF ( ss , cte.LocalTime , t1.LocalTime ), '1900-01-01') AS TIME) Duration INTO #FinalDataAux from cte
       inner join #AuxON t1 on t1.RowNumber = cte.RowNumber + 1
where cte.TypeValues = 1

";

            /* if (!String.IsNullOrEmpty(strTypeID[0]) & !String.IsNullOrEmpty(strTypeID[1]))
             {
                 sql = sql.Replace("'AUX2' AS Auxiliary", "" + strTypeID[0] + " AS Auxiliary");
                 sql = sql.Replace("'DOOR' AS AuxiliaryDescription", "" + strTypeID[1] + " AS AuxiliaryDescription");
             }
            /* if (mintemp.HasValue && maxtemp.HasValue)
                 sql += " and TRY_CONVERT(float,d.TypeValue) >= " + mintemp + " and  TRY_CONVERT(float,d.TypeValue) <= " + maxtemp + " ";*/


            //sql += sWorkHrs + " order by h.AssetID, DateTimeGPS_UTC";

            return sql;
        }

        public DataTable GetDebugData(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, String sWorkHrs, int? mintemp, int? maxtemp, List<String> lTypeID, String sConnStr, int? Page, int? LimitPerPage)
        {
            String sql = sGetDebugData(lAssetID, sWorkHrs, mintemp, maxtemp, lTypeID, sConnStr, Page, LimitPerPage);
            DataTable dtResult = new Connection(sConnStr).GetDataDT(sql, dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "�" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff"), null, sConnStr);
            return dtResult;
        }

        public DataTable GetAuxData(int UID, List<int> lAssetID, DateTime dtFrom, DateTime dtTo, String sWorkHrs, int? mintemp, int? maxtemp, List<String> lTypeID, String sConnStr, int? Page, int? LimitPerPage)
        {
            //lTypeID = AUX2:Door
            String sql = sGetDebugData(lAssetID, sWorkHrs, mintemp, maxtemp, lTypeID, sConnStr, Page, LimitPerPage, true);

            List<Matrix> lMatrix = new MatrixService().GetMatrixByUserID(UID, sConnStr);
            String s = ZoneService.sGetZoneIDs(lMatrix, sConnStr);
            sql += s;

            sql += @"
;with cteFinal as
(
    select h.*, zh.*, 1 inZone
    from
        (
            select*

                , Geometry::STPointFromText('POINT(' + CONVERT(varchar, Longitude) + ' ' + CONVERT(varchar, Latitude) + ')', 4326) pointFr
            from #FinalDataAux
		) h
        inner join GFI_FLT_ZoneHeader zh WITH (INDEX(SIndx_GFI_FLT_ZoneHeader)) on zh.GeomData.STIntersects(h.pointFr) = 1 
        inner join @tTmpDistinctZone t on zh.ZoneID = t.i
)
select RowNumber, ZoneID, Longitude, Latitude, Description as Zone, Color, LocalTime AS DateTimeON, EndDate AS DateTimeOff, Duration, AssetNumber, sDriverName, Auxiliary, AuxiliaryDescription into #ZoneData from cteFinal
ALTER TABLE #FinalDataAux
ADD ZoneID int
ALTER TABLE #FinalDataAux
ADD Zone nvarchar(255)
ALTER TABLE #FinalDataAux
ADD Color int
update #FinalDataAux set ZoneID = t1.ZoneID, Zone = t1.Zone, Color = t1.Color
from #ZoneData t1
    inner join #FinalDataAux t2 on t1.RowNumber=t2.RowNumber
select RowNumber, ZoneID, Longitude, Latitude, Zone, Color, LocalTime AS DateTimeON, EndDate AS DateTimeOff, Duration, AssetNumber, sDriverName, Auxiliary, AuxiliaryDescription from #FinalDataAux
";

            if (Page.HasValue)
            {
                Pagination p = Pagination.GetRowNums(Page, LimitPerPage);
                sql += " where (RowNumber between " + p.RowFrom + " and " + p.RowTo + ") ";
            }

            DataTable dtResult = new Connection(sConnStr).GetDataDT(sql, dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "�" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff"), null, sConnStr);

            dtResult.Columns.Add("address");
            DataRow[] drAddress = dtResult.Select();
            SimpleAddress.GetAddresses(drAddress, "address", "Longitude", "Latitude");

            return dtResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lAssetID"></param>
        /// <param name="dtFrom"></param>
        /// <param name="dtTo"></param>
        /// <param name="sWorkHrs"></param>
        /// <param name="mintemp"></param>
        /// <param name="maxtemp"></param>
        /// <param name="lTypeID"></param>
        /// <param name="sConnStr"></param>
        /// <param name="Page"></param>
        /// <param name="LimitPerPage"></param>
        /// <param name="IfAux2"></param>
        /// <author>Yaseen</author>
        /// <returns></returns>
        public DataTable GetDebugData(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, String sWorkHrs, int? mintemp, int? maxtemp, List<String> lTypeID, String sConnStr, int? Page, int? LimitPerPage, bool IfAux2)
        {
            String sql = sGetDebugData(lAssetID, sWorkHrs, mintemp, maxtemp, lTypeID, sConnStr, Page, LimitPerPage);
            DataTable dtResult = new Connection(sConnStr).GetDataDT(sql, dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "�" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff"), null, sConnStr);
            return dtResult;
        }

        public int GetDebugDataCnt(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, String sWorkHrs, int? mintemp, int? maxtemp, List<String> lTypeID, String sConnStr)
        {
            String sql = sGetDebugData(lAssetID, sWorkHrs, mintemp, maxtemp, lTypeID, sConnStr, null, null);
            sql = sql.Replace("select h.RowNum, h.UID, h.AssetID, h.DateTimeGPS_UTC, h.DateTimeServer, h.details, h.Longitude, h.Latitude, h.LongLatValidFlag, h.Speed, h.EngineOn, h.StopFlag, h.TripDistance, CAST(DATEADD(second, h.TripTime, '1900-01-01') AS TIME) as TripTime, h.WorkHour, h.DriverID, h.RoadSpeed, h.localTime, h.details, h.AssetName as assetName, h.sDriverName from ", "select count(1) Totals from ");
            sql = sql.Replace(" order by h.AssetID, DateTimeGPS_UTC", String.Empty);
            DataTable dtResult = new Connection(sConnStr).GetDataDT(sql, dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "�" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff"), null, sConnStr);
            int x = 0;
            if (dtResult != null)
                if (dtResult.Rows.Count > 0)
                    int.TryParse(dtResult.Rows[0][0].ToString(), out x);

            return x;
        }

        public DataTable GetGPSDataOnly(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            String strAssetID = String.Empty;
            foreach (int i in lAssetID)
                strAssetID += i.ToString() + ",";
            strAssetID = strAssetID.Substring(0, strAssetID.Length - 1);

            String sql = @"SELECT * from GFI_GPS_GPSData
	                        where DateTimeGPS_UTC >= ? and DateTimeGPS_UTC <= ?
	                            and AssetID in (" + strAssetID + @") 
                        order by AssetID, DateTimeGPS_UTC";

            DataTable dtResult = new Connection(sConnStr).GetDataDT(sql, dtFrom.ToString() + "�" + dtTo.ToString(), null, sConnStr);
            return dtResult;
        }
        public DataTable GetGPSDataDistanceFromPt(int AssetID, DateTime dtFrom, DateTime dtTo, Double dLat, Double dLon, String sConnStr)
        {
            List<int> lAssetID = new List<int>();
            lAssetID.Add(AssetID);
            DataTable dtResult = GetGPSDataOnly(lAssetID, dtFrom, dtTo, sConnStr);

            dtResult.Columns.Add("DistanceInM");
            foreach (DataRow dr in dtResult.Rows)
                dr["DistanceInM"] = GeoCalc.CalcDistanceInM(Convert.ToDouble(dr["Latitude"].ToString()), dLat, Convert.ToDouble(dr["Longitude"].ToString()), dLon);

            return dtResult;
        }

        public DataTable GetDebugDataCalOriginal(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, List<String> lTypeID, String sConnStr)
        {
            /*
             * Callibration Logic as follows
             * Callibration Reading     Converted
             *              1572        10
             *              382         75
             * 1572 - 382 + 1 = 1191 ReadingGap
             * 75 - 10 + 1 = 66 ConvertedGap
             * Suppose reading value is 871
             * calculation
             * ReadingGap ------ ConvertedGap
             * 1191       ------ 66
             * 871        ------ x
             * 871 must be substracted from Reading
             * x = ((382 - 871)/1191) * 66 = 48.26
            */
            DataTable dtResult = GetDebugData(lAssetID, dtFrom, dtTo, lTypeID, sConnStr);
            dtResult.Columns.Add("ConvertedValue");
            dtResult.Columns.Add("ConvertedUOMDesc");

            List<Asset> la = new List<Asset>();
            foreach (int i in lAssetID)
                la.Add(new AssetService().GetAssetById(i, 0, false, sConnStr));
            TimeSpan ts = la[0].TimeZoneTS;

            CalibrationChart CalChart = new CalibrationChart();
            try
            {
                List<CalibrationChart> lCalChart = new CalibrationChartService().GetCalibrationChartLByAssetId(lAssetID[0], sConnStr);

                if (lCalChart == null)
                {
                    // Message no propoer calibration found
                    foreach (DataRow dri in dtResult.Rows)
                    {
                        dri["ConvertedValue"] = dri["TypeValue"];
                        dri["ConvertedUOMDesc"] = dri["UOM"];
                        dri["DateTimeGPS_UTC"] = (DateTime)dri["DateTimeGPS_UTC"] + ts;
                    }
                }
                else if (lCalChart.Count == 1)
                {
                    //Single Line Callibration
                    //Adding minimum in callibation
                    CalibrationChart c = lCalChart[0].GetClone();
                    c.Reading = 1;
                    c.ConvertedValue = 1;
                    lCalChart.Insert(0, (c));
                }

                //Logic as follows:
                //suppose Callibration chart is 
                //100% ---- 50L
                //10%  ---- 15L
                //1%   ---- 1L
                //Suppose Reading value is 12% > gap between 10% - 100%
                //1st Gap is 90% --- 35L
                //1 unit > 1/90*35
                //ReadingIndent > 2% > 2*1 unit > 2*(1/90*35)
                //Final Result = LowValue + ReadingIndent > 15L + ReadingIndent
                if (lCalChart.Count > 1)
                {
                    foreach (DataRow dri in dtResult.Rows)
                    {
                        Double LowKey, LowValue, HighKey, HighValue;
                        for (int i = 0; i < lCalChart.Count - 1; i++)
                        {
                            Double dReadingFuel = Convert.ToDouble(dri["TypeValue"]);
                            if (
                                    (dReadingFuel >= lCalChart[i].Reading && dReadingFuel <= lCalChart[i + 1].Reading)  // normal calibration
                                    ||
                                    (dReadingFuel <= lCalChart[i].Reading && dReadingFuel >= lCalChart[i + 1].Reading)  // reverse calibration
                                )
                            {
                                LowKey = lCalChart[i].Reading;
                                LowValue = lCalChart[i].ConvertedValue;
                                HighKey = lCalChart[i + 1].Reading;
                                HighValue = lCalChart[i + 1].ConvertedValue;

                                // reverse calibration. Still testing
                                if (LowKey > HighKey)
                                {
                                    LowKey = lCalChart[i + 1].Reading;
                                    LowValue = lCalChart[i + 1].ConvertedValue;
                                    HighKey = lCalChart[i].Reading;
                                    HighValue = lCalChart[i].ConvertedValue;
                                }

                                Double dReadingGap = Math.Abs(LowKey - HighKey);
                                Double dConvertedGap = Math.Abs(LowValue - HighValue);
                                Double dReadingValue = Convert.ToDouble(dri["TypeValue"]);
                                //dReadingValue = 11;
                                Double SingleReadingUnitConvertion = (1.0 / dReadingGap) * dConvertedGap;
                                Double dIndent = Math.Abs(dReadingValue - LowKey) * SingleReadingUnitConvertion;
                                Double dConvertedValue = LowValue + dIndent;

                                if ((dReadingFuel <= lCalChart[i].Reading && dReadingFuel >= lCalChart[i + 1].Reading))  // reverse calibration
                                    dConvertedValue = LowValue - dIndent;
                                //dConvertedValue = Math.Abs(dConvertedValue - HighValue);

                                dri["ConvertedValue"] = dConvertedValue.ToString("0");
                                dri["ConvertedUOMDesc"] = lCalChart[i].ConvertedUOMDesc;
                                dri["DateTimeGPS_UTC"] = (DateTime)dri["DateTimeGPS_UTC"] + ts;

                                i = lCalChart.Count + 5;
                            }
                        }
                    }
                }
            }
            catch { }

            try
            {
                DataRow[] matches = dtResult.Select("ConvertedValue is null ");
                foreach (DataRow row in matches)
                    dtResult.Rows.Remove(row);

                foreach (DataRow dr in dtResult.Rows)
                    if (dr.RowState == DataRowState.Deleted)
                        dr.AcceptChanges();
                dtResult.AcceptChanges();
            }
            catch { };

            return dtResult;
        }
        public DataTable GetDebugDataCal(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, List<String> lTypeID, String sConnStr)
        {
            DataTable dtResult = GetDebugData(lAssetID, dtFrom, dtTo, lTypeID, sConnStr);
            dtResult = FuelService.GetCallibratedData(dtResult, lAssetID, "TypeValue", sConnStr);
            return dtResult;
        }

        public DataTable GetDebugDataCalIgn(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, List<String> lTypeID, String sConnStr)
        {
            DataTable dtResult = GetDebugDataCal(lAssetID, dtFrom, dtTo, lTypeID, sConnStr);

            var lTypeIgn = new List<string> { "Ignition", "Fuel" };
            DataTable dtIgnInfo = GetDebugData(lAssetID, dtFrom, dtTo, lTypeIgn, sConnStr);

            dtResult.Columns.Add("IngnitionInfo", typeof(String));
            dtIgnInfo.Columns.Add("IngnitionInfo", typeof(String));

            int u = 0;
            for (u = 0; u <= dtIgnInfo.Rows.Count - 1; u++)
            {
                if (u > 0)
                {
                    if (dtIgnInfo.Rows[u]["TypeID"].ToString() == "Ignition")
                    {
                        int v = 0;
                        for (v = 0; v < 7; v++)
                        {
                            if ((u - v) > 0)
                            {
                                if (dtIgnInfo.Rows[u - v]["TypeID"].ToString() == "Ignition")
                                {
                                    if ((u - v - 1) > 0)
                                        dtIgnInfo.Rows[u - v - 1]["IngnitionInfo"] = dtIgnInfo.Rows[u]["TypeValue"].ToString();
                                }
                            }
                        }
                    }
                }
                if (u == 0)
                {
                    if (dtIgnInfo.Rows[u]["TypeID"].ToString() == "Ignition")
                    {
                        dtIgnInfo.Rows[u + 1]["IngnitionInfo"] = dtIgnInfo.Rows[u]["TypeValue"].ToString();
                    }
                }
            }

            foreach (DataRow dri in dtResult.Rows)
            {
                DataRow[] dr = dtIgnInfo.Select("UID = " + dri["UID"].ToString() + "AND TypeID='Fuel'");
                if (dr.Length > 0)
                {
                    dri["IngnitionInfo"] = dr[0]["IngnitionInfo"].ToString();
                }
            }

            return dtResult;
        }

        public DataTable GetFuelUsageData(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, List<String> lTypeID, String sConnStr)
        {
            String strAssetID = String.Empty;
            foreach (int i in lAssetID)
                strAssetID += i.ToString() + ",";
            strAssetID = strAssetID.Substring(0, strAssetID.Length - 1);

            String strTypeID = String.Empty;
            foreach (String s in lTypeID)
                strTypeID += s.ToString() + "',";
            strTypeID = "'" + strTypeID.Substring(0, strTypeID.Length - 1);

            String sql = @"SELECT * FROM 
                            GFI_GPS_GPSData h INNER JOIN GFI_GPS_GPSDataDetail d
                            ON h.uid = d.uid 
                            WHERE 
                             d.GPSDetailID IN (SELECT MAX(GPSDetailID)
			                            from GFI_GPS_GPSData h, GFI_GPS_GPSDataDetail d 
			                            where h.uid = d.uid 
				                            and h.DateTimeGPS_UTC >= ? and h.DateTimeGPS_UTC <= ?
				                            and h.AssetID in (" + strAssetID + @") 
                                            and d.TypeID in (" + strTypeID + @")
			                            GROUP BY h.AssetID
                            )
			                            ORDER BY h.AssetID ";

            DataTable dtResult = new Connection(sConnStr).GetDataDT(sql, dtFrom.ToString() + "�" + dtTo.ToString(), null, sConnStr);

            return dtResult;
        }

        DataSet GetGPSDataDS(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            DataSet ds = new DataSet();
            Connection conn = new Connection(sConnStr);
            String strAssetID = String.Empty;
            foreach (int i in lAssetID)
                strAssetID += i.ToString() + ",";
            strAssetID = strAssetID.Substring(0, strAssetID.Length - 1);

            String sql = @"select * from GFI_GPS_GPSData where UID in
	                        (select GPSDataStartUID from GFI_GPS_TripHeader h
                            where h.AssetID in (" + strAssetID + @") 
	                            and (select DateTimeGPS_UTC from GFI_GPS_GPSData where h.GPSDataStartUID = UID ) >= ?
	                            and (select DateTimeGPS_UTC from GFI_GPS_GPSData where h.GPSDataEndUID = UID ) <= ?
	                        )
                        union
                        select * from GFI_GPS_GPSData where UID in
	                        (select GPSDataEndUID from GFI_GPS_TripHeader h
                            where h.AssetID in (" + strAssetID + @") 
	                            and (select DateTimeGPS_UTC from GFI_GPS_GPSData where h.GPSDataStartUID = UID ) >= ?
	                            and (select DateTimeGPS_UTC from GFI_GPS_GPSData where h.GPSDataEndUID = UID ) <= ?
	                        )";
            DataTable dtGPSData = conn.GetDataDT(sql, dtFrom.ToString() + "�" + dtTo.ToString() + "�" + dtFrom.ToString() + "�" + dtTo.ToString(), null, sConnStr);
            dtGPSData.TableName = "GPSData";
            ds.Merge(dtGPSData);

            if (dtGPSData.Rows.Count == 0)
                return null;


            sql = @"select * from GFI_GPS_GPSDataDetail where UID in
                        (
                        select UID from GFI_GPS_GPSData where UID in
	                        (select GPSDataStartUID from GFI_GPS_TripHeader h
		                        where h.AssetID in (" + strAssetID + @") 
			                        and (select DateTimeGPS_UTC from GFI_GPS_GPSData where h.GPSDataStartUID = UID ) >= ?
			                        and (select DateTimeGPS_UTC from GFI_GPS_GPSData where h.GPSDataEndUID = UID ) <= ?
	                        )
                        union
                        select UID from GFI_GPS_GPSData where UID in
	                        (select GPSDataEndUID from GFI_GPS_TripHeader h
		                        where h.AssetID in (" + strAssetID + @") 
			                        and (select DateTimeGPS_UTC from GFI_GPS_GPSData where h.GPSDataStartUID = UID ) >= ?
			                        and (select DateTimeGPS_UTC from GFI_GPS_GPSData where h.GPSDataEndUID = UID ) <= ?
	                        )
                        )";
            DataTable dtGPSDataDetail = conn.GetDataDT(sql, dtFrom.ToString() + "�" + dtTo.ToString() + "�" + dtFrom.ToString() + "�" + dtTo.ToString(), null, sConnStr);
            dtGPSDataDetail.TableName = "GPSDataDetail";
            ds.Merge(dtGPSDataDetail);

            //DataTable dtAsset = new AssetService().GetAsset(Globals.uLogin.MatrixID);
            //dtAsset.TableName = "Asset";
            //ds.Merge(dtAsset);

            //DataTable dtDriver = new DriverService().GetDriver(Globals.uLogin.MatrixID);
            //dtDriver.TableName = "Driver";
            //ds.Merge(dtDriver);

            return ds;
        }
        public List<GPSData> GetGetGPSDataDS(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            List<GPSData> l = new List<GPSData>();

            DataSet ds = GetGPSDataDS(lAssetID, dtFrom, dtTo, sConnStr);
            if (ds == null)
                return l;

            l = new myConverter().ConvertToList<GPSData>(ds.Tables["GPSData"]);
            List<GPSDataDetail> lGPSDataDetail = new myConverter().ConvertToList<GPSDataDetail>(ds.Tables["GPSDataDetail"]);
            //List<Asset> lAsset = new myConverter().ConvertToList<Asset>(ds.Tables["Asset"]);
            //List<Driver> lDriver = new myConverter().ConvertToList<Driver>(ds.Tables["Driver"]);

            foreach (GPSData g in l)
                g.GPSDataDetails = lGPSDataDetail.FindAll(o => o.UID == g.UID);

            return l;
        }

        public List<GPSData> GetGPSDatatList(String strAsset, String sConnStr)
        {
            String sqlString = @"SELECT UID, 
                                        AssetID, 
                                        DriverID,
                                        DateTimeGPS_UTC, 
                                        DateTimeServer, 
                                        Longitude, 
                                        Latitude, 
                                        LongLatValidFlag, 
                                        Speed, 
                                        EngineOn, 
                                        StopFlag, 
                                        TripDistance, 
                                        TripTime, 
                                        WorkHour from GFI_GPS_GPSData
                                        WHERE AssetID = ?
                                        order by UID";

            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, strAsset, sConnStr);
            List<GPSData> glist = new List<GPSData>();

            foreach (DataRow row in dt.Rows)
            {
                GPSData retGPSData = new GPSData();

                retGPSData.UID = Convert.ToInt32(row["UID"]);
                retGPSData.AssetID = Convert.ToInt32(row["AssetID"]);
                retGPSData.DateTimeGPS_UTC = (DateTime)row["DateTimeGPS_UTC"];
                retGPSData.DateTimeServer = (DateTime)row["DateTimeServer"];
                retGPSData.Longitude = (double)row["Longitude"];
                retGPSData.Latitude = (double)row["Latitude"];
                retGPSData.DriverID = Convert.ToInt32(row["DriverID"]);


                retGPSData.Speed = (float)row["Speed"];
                retGPSData.EngineOn = Convert.ToInt32(row["EngineOn"]);
                retGPSData.StopFlag = Convert.ToInt32(row["StopFlag"]);
                retGPSData.TripDistance = (float)row["TripDistance"];
                retGPSData.TripTime = (float)row["TripTime"];
                retGPSData.WorkHour = Convert.ToInt32(row["WorkHour"]);

                glist.Add(retGPSData);
            }
            return glist;
        }
        Boolean isDuplicate(GPSData uGPSData, DbTransaction tTrans, out int UID, out String strLogReason, String sConnStr)
        {
            UID = -1;
            strLogReason = String.Empty;
            Boolean bResult = false;
            String sql = "select * from GFI_GPS_GPSData where AssetID = ? and DateTimeGPS_UTC = ?";
            String strParams = uGPSData.AssetID.ToString() + "�" + uGPSData.DateTimeGPS_UTC.ToString("dd-MMM-yyyy HH:mm:ss.fff");
            DataTable dt = new Connection(sConnStr).GetDataDT(sql, strParams, tTrans, sConnStr);
            if (dt.Rows.Count > 0)
            {
                bResult = true;
                UID = Convert.ToInt32((dt.Rows[0]["UID"]));

                sql = "select * from GFI_GPS_GPSDataDetail where UID = " + UID.ToString();
                dt = new Connection(sConnStr).GetDataDT(sql, tTrans, sConnStr);
                foreach (DataRow dr in dt.Rows)
                    strLogReason += dr["TypeID"] + ",";

                if (strLogReason.Length > 0)
                    strLogReason = strLogReason.Substring(0, strLogReason.Length - 1);
            }
            return bResult;
        }
        public Boolean SaveGPSData(GPSData uGPSData, List<GPSDataDetail> GPSDetails, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_GPS_GPSData";
            dt.Columns.Add("AssetID", uGPSData.AssetID.GetType());
            dt.Columns.Add("DriverID", uGPSData.DeviceID.GetType());
            dt.Columns.Add("DateTimeGPS_UTC", uGPSData.DateTimeGPS_UTC.GetType());
            dt.Columns.Add("DateTimeServer", uGPSData.DateTimeServer.GetType());
            dt.Columns.Add("Longitude", uGPSData.Longitude.GetType());
            dt.Columns.Add("Latitude", uGPSData.Latitude.GetType());
            dt.Columns.Add("LongLatValidFlag", uGPSData.LongLatValidFlag.GetType());
            dt.Columns.Add("Speed", uGPSData.Speed.GetType());
            dt.Columns.Add("EngineOn", uGPSData.EngineOn.GetType());
            dt.Columns.Add("StopFlag", uGPSData.StopFlag.GetType());
            dt.Columns.Add("TripDistance", uGPSData.TripDistance.GetType());
            dt.Columns.Add("TripTime", uGPSData.TripTime.GetType());
            dt.Columns.Add("WorkHour", uGPSData.WorkHour.GetType());
            DataRow dr = dt.NewRow();

            AssetDeviceMap ADMap = new AssetDeviceMap();
            ADMap = new AssetDeviceMapService().GetAssetDeviceMapByDevice(uGPSData.DeviceID.ToString(), sConnStr);
            dr["AssetID"] = ADMap.AssetID;
            uGPSData.AssetID = ADMap.AssetID;

            Driver Udriver = new Driver();
            Udriver = new DriverService().GetDriverByIbutton(uGPSData.iButton, sConnStr);
            dr["DriverID"] = Udriver.DriverID;
            uGPSData.DriverID = Udriver.DriverID;

            if (uGPSData.Longitude == 0 || uGPSData.Latitude == 0)
                uGPSData.LongLatValidFlag = 0;

            dr["DateTimeGPS_UTC"] = uGPSData.DateTimeGPS_UTC;
            dr["DateTimeServer"] = DateTime.Now;
            dr["Longitude"] = uGPSData.Longitude;
            dr["Latitude"] = uGPSData.Latitude;
            dr["LongLatValidFlag"] = uGPSData.LongLatValidFlag;
            dr["Speed"] = uGPSData.Speed;
            dr["EngineOn"] = uGPSData.EngineOn;
            dr["WorkHour"] = uGPSData.WorkHour;
            dr["StopFlag"] = 9;     //uGPSData.StopFlag;
            dr["TripDistance"] = 0; //uGPSData.TripDistance;
            dr["TripTime"] = 0;     //uGPSData.TripTime;

            Boolean bResult = false;
            DbTransaction tTrans;
            DbConnection _Conn = Connection.GetConnection(sConnStr);
            _Conn.Open();
            try
            {
                tTrans = _Conn.BeginTransaction();

                Int32 UID = -1;
                String strLogReason = String.Empty;
                Boolean bIsDuplicate = isDuplicate(uGPSData, tTrans, out UID, out strLogReason, sConnStr);
                Connection _connection = new Connection(sConnStr);
                GPSDataDetailService gpsDataDetailService = new GPSDataDetailService();
                if (bIsDuplicate)
                {
                    //inserting in details only for different LogReason
                    bResult = true;
                    foreach (GPSDataDetail g in GPSDetails)
                    {
                        if (!strLogReason.Contains(g.TypeID))
                        {
                            g.UID = UID;
                            //bResult = bResult & _connection.InsertGPSDataDetail(dt, UID, tTrans);

                            //g.Remarks = "Duplicate GPSData Lat " + uGPSData.Latitude.ToString() + "Lon " + uGPSData.Longitude.ToString();
                            bResult = bResult & gpsDataDetailService.SaveGPSDataDetail(g, UID, tTrans, sConnStr);
                        }
                    }
                }
                else
                {
                    //Inserting completely new
                    UID = Convert.ToInt32(_connection.GetNextID("GFI_GPS_GPSData", tTrans, sConnStr).ToString());

                    dt.Rows.Add(dr);

                    //bResult = _connection.GenInsert(dt, tTrans);
                    bResult = _connection.InsertGPSData(dt, UID, uGPSData.AssetID, tTrans, sConnStr);
                    if (bResult)
                        foreach (GPSDataDetail g in GPSDetails)
                        {
                            g.UID = UID;
                            bResult = bResult & gpsDataDetailService.SaveGPSDataDetail(g, UID, tTrans, sConnStr);
                        }

                    //bResult = bResult & _connection.DeleteOldLive(uGPSData.AssetID, tTrans);

                    ProcessPendingService pps = new ProcessPendingService();
                    if (bResult)
                        if (uGPSData.LongLatValidFlag == 1)
                            bResult = bResult & pps.RegisterDevicesForProcessing(ADMap.AssetID, uGPSData.DateTimeGPS_UTC, "Trips", tTrans, sConnStr);

                    bResult = bResult & pps.RegisterDevicesForProcessing(ADMap.AssetID, uGPSData.DateTimeGPS_UTC, "Exceptions", tTrans, sConnStr);
                    //bResult = bResult & pps.RegisterDevicesForLive(UID, uGPSData, GPSDetails, _connection, tTrans);
                }

                if (bResult)
                    tTrans.Commit();
                else
                    tTrans.Rollback();
            }
            catch { bResult = false; }
            finally
            {
                _Conn.Close();
            }
            return bResult;
        }

        public Boolean BulkSaveGPSData(List<GPSData> lGPSData, String sConnStr)
        {
            Boolean bResult = false;
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = @"
--Live
CREATE TABLE #GFI_GPS_GPSData
(
iid int,
DeviceID varchar(50),
iButton varchar(50),
DateTimeGPS_UTC datetime,
Longitude float,
Latitude float,
LongLatValidFlag int,
Speed float,
EngineOn int,
StopFlag int,
TripDistance int,
TripTime int,
WorkHour int,
AssetID int, DriverID int, GPSDataID int
);

CREATE TABLE #GFI_GPS_GPSDataDetail
(
	UID [int] NULL,
	TypeID [nvarchar](30) NULL,
	TypeValue [nvarchar](30) NULL,
	UOM [nvarchar](15) NULL,
	Remarks [nvarchar](50) NULL
);
--Oracle Starts Here
";
            String sqlSub = String.Empty;
            int i = 0;
            foreach (GPSData uGPSData in lGPSData)
            {
                List<GPSDataDetail> GPSDetails = uGPSData.GPSDataDetails;
                if (GPSDetails == null)
                    continue;

                i++;
                if (uGPSData.Longitude == 0 || uGPSData.Latitude == 0)
                    uGPSData.LongLatValidFlag = 0;

                sqlSub += "\r\ninsert #GFI_GPS_GPSData values (" + i.ToString() + ", '" + uGPSData.DeviceID + "'"
                    + ", '" + uGPSData.iButton + "', "
                    + "'" + uGPSData.DateTimeGPS_UTC.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "', "
                    + uGPSData.Longitude + ", "
                    + uGPSData.Latitude + ", "
                    + uGPSData.LongLatValidFlag + ", "
                     + uGPSData.Speed + ", "
                     + uGPSData.EngineOn + ", "
                     + "9,0,0,"
                     + uGPSData.WorkHour.ToString()
                     + ",0,0,0);";

                foreach (GPSDataDetail d in uGPSData.GPSDataDetails)
                {
                    sqlSub += "\r\n insert #GFI_GPS_GPSDataDetail values (" + i.ToString() + ", "
                        + "'" + d.TypeID.ToString() + "', "
                        + "'" + d.TypeValue.ToString() + "', "
                        + "'" + d.UOM + "', "
                        + "'" + d.Remarks + "');";
                    sqlSub += "\r\n";
                }
            }

            sql += sqlSub;
            sql += @"
--TingPow
Begin Try
	begin tran

	--Preparing temp tables
	UPDATE #GFI_GPS_GPSData 
		SET AssetID = T2.AssetID
	FROM  #GFI_GPS_GPSData T1
	INNER JOIN GFI_FLT_AssetDeviceMap T2 ON T2.DeviceId COLLATE DATABASE_DEFAULT = T1.DeviceID COLLATE DATABASE_DEFAULT and Status_b = 'AC'

	UPDATE #GFI_GPS_GPSData 
		SET GPSDataID = T2.UID
	FROM  #GFI_GPS_GPSData T1
	INNER JOIN GFI_GPS_GPSData T2 ON T1.AssetID = T2.AssetID and t1.DateTimeGPS_UTC = t2.DateTimeGPS_UTC

	UPDATE #GFI_GPS_GPSData 
		SET DriverID = T2.DriverID
	FROM  #GFI_GPS_GPSData T1
	INNER JOIN GFI_FLT_Driver T2 ON T2.iButton COLLATE DATABASE_DEFAULT = T1.iButton COLLATE DATABASE_DEFAULT and EmployeeType = 'Driver' and (RTRIM(LTRIM(T2.iButton)) != '' or RTRIM(LTRIM(T2.iButton)) != '1')

	UPDATE #GFI_GPS_GPSData 
		SET DriverID = T2.DriverID
	FROM  #GFI_GPS_GPSData T1
	INNER JOIN GFI_FLT_Driver T2 ON T2.iButton = '00000000' and T2.EmployeeType = 'Driver'
	where T1.DriverID = 0

	--For AT200, PassengerID is treated as DriverID. So we delete the PassengerID, registered as driver in N1, from #GPSDataDetails 
	--delete from #GFI_GPS_GPSDataDetail
	--	from #GFI_GPS_GPSDataDetail t1
	--		inner join GFI_FLT_Driver t2 on t1.TypeValue COLLATE DATABASE_DEFAULT = t2.iButton COLLATE DATABASE_DEFAULT where t2.EmployeeType = 'Driver' and (t2.iButton != '' or RTRIM(LTRIM(T2.iButton)) != '1')

	UPDATE #GFI_GPS_GPSData 
	    SET Speed = 0
	where EngineOn = 0 and Speed > 0

	--UPDATE #GFI_GPS_GPSData 
	--    SET DriverID = (SELECT DriverID from GFI_FLT_Driver  WHERE iButton = '00000000' and EmployeeType = 'Driver')
	--where DriverID = 0

	--Heartbeat.
    --begin try
		;with cte as
			(
				SELECT *, RowNum = ROW_NUMBER() OVER (PARTITION BY T2.AssetID ORDER BY T2.DateTimeGPS_UTC DESC)
					FROM #GFI_GPS_GPSDataDetail T1
				INNER JOIN #GFI_GPS_GPSData T2 ON T2.iID = T1.UID
					where TypeID = 'Heartbeat'
						and GPSDataID = 0
			)
		delete GFI_GPS_HeartBeat
		--select * 
			from GFI_GPS_HeartBeat h
				inner join cte on cte.AssetID = h.AssetID

		;with cte as
			(
				SELECT *, RowNum = ROW_NUMBER() OVER (PARTITION BY T2.AssetID ORDER BY T2.DateTimeGPS_UTC DESC)
					FROM #GFI_GPS_GPSDataDetail T1
				INNER JOIN #GFI_GPS_GPSData T2 ON T2.iID = T1.UID
					where TypeID = 'Heartbeat'
						and GPSDataID = 0
			)
		insert GFI_GPS_HeartBeat (AssetID, DateTimeGPS_UTC, DateTimeServer, Longitude, Latitude)
			select AssetID, DateTimeGPS_UTC, Getdate(), Longitude, Latitude
				from CTE where RowNum = 1

		delete from #GFI_GPS_GPSDataDetail where TypeID = 'Heartbeat'

		SET ANSI_WARNINGS OFF
		;with cte as
			(
				select T1.iID --, count(T2.UID) as NumberOfDetails
					from #GFI_GPS_GPSData T1
				left join #GFI_GPS_GPSDataDetail T2 on T1.iID = T2.UID
					group by T1.iID
				having count(T2.UID) = 0 --having ISNULL(count(T2.UID),0) = 0
			)
		delete #GFI_GPS_GPSData
		--select * 
			from #GFI_GPS_GPSData g
				inner join cte on cte.iid = g.iid
		SET ANSI_WARNINGS ON

	--end try
	--begin catch
	--end catch

	--delete from #GFI_GPS_GPSData where AssetID = 0

    --Insert Header
	;WITH CTE AS 
		(
			SELECT T1.*, RowNum = ROW_NUMBER() OVER (PARTITION BY AssetID, DateTimeGPS_UTC ORDER BY DateTimeGPS_UTC DESC)
			from #GFI_GPS_GPSData T1 where GPSDataID = 0
		)
	insert into GFI_GPS_GPSData (AssetID, DriverID, DateTimeGPS_UTC, DateTimeServer, Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, StopFlag, TripDistance, TripTime, WorkHour)
		select AssetID, DriverID, DateTimeGPS_UTC, GETDATE(), Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, StopFlag, TripDistance, TripTime, WorkHour 
			from CTE where RowNum = 1   --PostGreSQL order by DateTimeGPS_UTC

	--Updating back #GFI_GPS_GPSData
	UPDATE #GFI_GPS_GPSData 
		SET GPSDataID = T2.UID
	FROM #GFI_GPS_GPSData T1
	INNER JOIN GFI_GPS_GPSData T2 ON T1.AssetID = T2.AssetID and t1.DateTimeGPS_UTC = t2.DateTimeGPS_UTC

	UPDATE #GFI_GPS_GPSDataDetail 
		SET UID = T2.GPSDataID
	FROM #GFI_GPS_GPSDataDetail T1
	INNER JOIN #GFI_GPS_GPSData T2 ON T2.iID = T1.UID

	insert GFI_GPS_GPSDataDetail (UID, TypeID, TypeValue, UOM, Remarks)
		--select d.UID, d.TypeID, d.TypeValue, d.UOM, d.Remarks
		select distinct d.UID, d.TypeID, d.TypeValue, d.UOM, d.Remarks
			from #GFI_GPS_GPSDataDetail d
				left outer join GFI_GPS_GPSDataDetail d1 on d1.UID = d.UID and d1.TypeID COLLATE DATABASE_DEFAULT = d.TypeID COLLATE DATABASE_DEFAULT
			where d1.UID is null

	--Insert LiveHeader
	--;WITH CTE AS 
	--	(
	--		SELECT *, RowNum = ROW_NUMBER() OVER (PARTITION BY AssetID ORDER BY DateTimeGPS_UTC DESC)
	--		from #GFI_GPS_GPSData
	--	)
	--insert GFI_GPS_LiveData (UID, AssetID, DriverID, DateTimeGPS_UTC, DateTimeServer, Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, StopFlag, TripDistance, TripTime, WorkHour)
	--	select GPSDataID, AssetID, DriverID, DateTimeGPS_UTC, GETDATE(), Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, StopFlag, TripDistance, TripTime, WorkHour 
	--		from CTE where RowNum = 1 and GPSDataID not in (select UID from GFI_GPS_LiveData)

	;WITH CTE AS 
		(
			SELECT T1.*, RowNum = ROW_NUMBER() OVER (PARTITION BY AssetID ORDER BY DateTimeGPS_UTC DESC)
			from #GFI_GPS_GPSData T1 where LongLatValidFlag = 1
		)
	insert GFI_GPS_LiveData (UID, AssetID, DriverID, DateTimeGPS_UTC, DateTimeServer, Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, StopFlag, TripDistance, TripTime, WorkHour)
		select c.GPSDataID, c.AssetID, c.DriverID, c.DateTimeGPS_UTC, GETDATE(), c.Longitude, c.Latitude, c.LongLatValidFlag, c.Speed, c.EngineOn, c.StopFlag, c.TripDistance, c.TripTime, c.WorkHour 
			from CTE c
			    left outer join GFI_GPS_LiveData l on l.UID = c.GPSDataID
			where RowNum = 1 
			    and l.UID is null   --PostGreSQL order by DateTimeGPS_UTC

	;WITH CTE AS 
		(
			SELECT T1.*, RowNum = ROW_NUMBER() OVER (PARTITION BY AssetID ORDER BY DateTimeGPS_UTC DESC)
			from #GFI_GPS_GPSData T1 where LongLatValidFlag = 1
		)
	insert GFI_GPS_LiveDataDetail (UID, TypeID, TypeValue, UOM, Remarks)
		select d.UID, d.TypeID, d.TypeValue, d.UOM, d.Remarks
			from #GFI_GPS_GPSDataDetail d
				left outer join GFI_GPS_LiveDataDetail d1 on d1.UID = d.UID and d1.TypeID COLLATE DATABASE_DEFAULT = d.TypeID COLLATE DATABASE_DEFAULT
				inner join cte g on g.GPSDataID = d.UID 
			where RowNum = 1 
				and d1.GPSDetailID is null

	;with cte as 
			(
				select uid, AssetID, 
					DriverID,
					DateTimeGPS_UTC, 
					LongLatValidFlag, 
					row_number() over(partition by AssetID order by DateTimeGPS_UTC desc) as RowNum
				from GFI_GPS_LiveData
				WHERE DateTimeGPS_UTC < getdate() + 1
			)
	delete GFI_GPS_LiveData 
		--select * 
		from GFI_GPS_LiveData l
			inner join cte on cte.UID = l.UID
		where cte.RowNum > 6

    --RegisterDevicesForProcessing
    Declare @ProcessTable Table (AssetID int, dt DateTime)
    ;WITH CTE AS 
    (
	    SELECT T1.*, RowNum = ROW_NUMBER() OVER (PARTITION BY AssetID ORDER BY DateTimeGPS_UTC ASC)
	    from #GFI_GPS_GPSData T1
    )
    insert @ProcessTable
	    select AssetID, DateTimeGPS_UTC from CTE where RowNum = 1

    --Cursor Starts for RegisterDevicesForProcessing
	Declare @AssetID int
	Declare @dt DateTime
	Declare c Cursor for
		select * from @ProcessTable

    OPEN c
	    FETCH NEXT FROM c INTO @AssetID, @dt 
	    WHILE @@FETCH_STATUS = 0   
	    BEGIN  
		    EXEC RegisterDevicesForProcessing @AssetID, @dt, 'Trips'
		    EXEC RegisterDevicesForProcessing @AssetID, @dt, 'Exceptions'
		    FETCH NEXT FROM c INTO @AssetID, @dt 
	    END   
    CLOSE c
    DEALLOCATE c
    --Cursor ends

    commit;
End try
begin catch     
    --SQLLiveExceptions
	rollback

	SELECT
		ERROR_NUMBER() AS ErrorNumber
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;

		begin try
			insert GFI_GPS_ProcessErrors (sError, sOrigine, sFieldValue) values (ERROR_MESSAGE() + ' - Error occured at: ' + CONVERT(VARCHAR(20),  ERROR_LINE()) + ERROR_PROCEDURE(), 'LiveDwn', ERROR_LINE())
		end try
		begin catch
		end catch
end catch

drop table #GFI_GPS_GPSData;
drop table #GFI_GPS_GPSDataDetail
";
            //File.AppendAllText(Globals.LogPath() + "LiveDwn.txt", sql);
            //return true;

            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            if (!bResult)
            {
                try
                {
                    FileInfo f = new FileInfo(Globals.LogPath() + "LiveDwn.txt");
                    if (f.Length > 100000)
                        File.Delete(Globals.LogPath() + "LiveDwn.txt");
                }
                catch { }

                File.AppendAllText(Globals.LogPath() + "LiveDwn.txt", "\r\n**********Errors Detected*************\r\n");
                File.AppendAllText(Globals.LogPath() + "LiveDwn.txt", DateTime.Now.ToString());
                File.AppendAllText(Globals.LogPath() + "LiveDwn.txt", "\r\n**************************************\r\n");
                foreach (DataRow dr in dtCtrl.Rows)
                {
                    foreach (DataColumn dc in dtCtrl.Columns)
                        File.AppendAllText(Globals.LogPath() + "LiveDwn.txt", "\r\n" + dc.ColumnName + " : " + dr[dc].ToString());
                    File.AppendAllText(Globals.LogPath() + "LiveDwn.txt", "\r\n");
                }
            }

            return bResult;
        }

        public Boolean BulkSaveGPSDataForSecuMonitor(List<GPSData> lGPSData, String sConnStr)
        {
            Boolean bResult = false;
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = @"
CREATE TABLE #GFI_GPS_SecuMonitorLiveData
(
iid int,
DeviceID varchar(50),
iButton varchar(50),
DateTimeGPS_UTC datetime,
Longitude float,
Latitude float,
LongLatValidFlag int,
Speed float,
EngineOn int,
StopFlag int,
TripDistance int,
TripTime int,
WorkHour int,
AssetID int, DriverID int, GPSDataID int
)

CREATE TABLE #GFI_GPS_SecuMonitorLiveDataDetail
(
	UID [int] NULL,
	TypeID [nvarchar](30) NULL,
	TypeValue [nvarchar](30) NULL,
	UOM [nvarchar](15) NULL,
	Remarks [nvarchar](50) NULL
)
";
            String sqlSub = String.Empty;
            int i = 0;
            foreach (GPSData uGPSData in lGPSData)
            {
                List<GPSDataDetail> GPSDetails = uGPSData.GPSDataDetails;
                if (GPSDetails == null)
                    continue;

                i++;
                if (uGPSData.Longitude == 0 || uGPSData.Latitude == 0)
                    uGPSData.LongLatValidFlag = 0;

                sqlSub += "\r\ninsert #GFI_GPS_SecuMonitorLiveData values (" + i.ToString() + ", '" + uGPSData.DeviceID + "'"
                    + ", '" + uGPSData.iButton + "', "
                    + "'" + uGPSData.DateTimeGPS_UTC.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "', "
                    + uGPSData.Longitude + ", "
                    + uGPSData.Latitude + ", "
                    + uGPSData.LongLatValidFlag + ", "
                     + uGPSData.Speed + ", "
                     + uGPSData.EngineOn + ", "
                     + "9,0,0,"
                     + uGPSData.WorkHour.ToString()
                     + ",0,0,0);";

                foreach (GPSDataDetail d in uGPSData.GPSDataDetails)
                {
                    sqlSub += "\r\n insert #GFI_GPS_SecuMonitorLiveDataDetail values (" + i.ToString() + ", "
                        + "'" + d.TypeID.ToString() + "', "
                        + "'" + d.TypeValue.ToString() + "', "
                        + "'" + d.UOM + "', "
                        + "'" + d.Remarks + "');";
                    sqlSub += "\r\n";
                }
            }

            sql += sqlSub;
            sql += @"
--TingPow
Begin Try
	begin tran

	--Preparing temp tables
	UPDATE #GFI_GPS_SecuMonitorLiveData 
		SET AssetID = T2.iID
	FROM  #GFI_GPS_SecuMonitorLiveData T1
	INNER JOIN  GFI_FLT_SecuMonitorAssets T2 ON T2.DeviceId COLLATE DATABASE_DEFAULT = T1.DeviceID COLLATE DATABASE_DEFAULT

	UPDATE #GFI_GPS_SecuMonitorLiveData 
		SET GPSDataID = T2.UID
	FROM  #GFI_GPS_SecuMonitorLiveData T1
	INNER JOIN  GFI_GPS_SecuMonitorLiveData T2 ON T1.AssetID = T2.AssetID and t1.DateTimeGPS_UTC = t2.DateTimeGPS_UTC

    --Insert Header
	;WITH CTE AS 
		(
			SELECT *, RowNum = ROW_NUMBER() OVER (PARTITION BY AssetID, DateTimeGPS_UTC ORDER BY DateTimeGPS_UTC DESC)
			from #GFI_GPS_SecuMonitorLiveData where GPSDataID = 0
		)
	insert into GFI_GPS_SecuMonitorLiveData (AssetID, DriverID, DateTimeGPS_UTC, DateTimeServer, Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, StopFlag, TripDistance, TripTime, WorkHour)
		select AssetID, 1, DateTimeGPS_UTC, GETDATE(), Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, StopFlag, TripDistance, TripTime, WorkHour 
			from CTE where RowNum = 1

	--Updating back #GFI_GPS_SecuMonitorLiveData
	UPDATE #GFI_GPS_SecuMonitorLiveData 
		SET GPSDataID = T2.UID
	FROM  #GFI_GPS_SecuMonitorLiveData T1
	INNER JOIN  GFI_GPS_SecuMonitorLiveData T2 ON T1.AssetID = T2.AssetID and t1.DateTimeGPS_UTC = t2.DateTimeGPS_UTC

	UPDATE #GFI_GPS_SecuMonitorLiveDataDetail 
		SET UID = T2.GPSDataID
	FROM #GFI_GPS_SecuMonitorLiveDataDetail T1
	INNER JOIN #GFI_GPS_SecuMonitorLiveData T2 ON T2.iID = T1.UID

	insert GFI_GPS_SecuMonitorLiveDataDetail (UID, TypeID, TypeValue, UOM, Remarks)
		select d.UID, d.TypeID, d.TypeValue, d.UOM, d.Remarks
			from #GFI_GPS_SecuMonitorLiveDataDetail d
				left outer join GFI_GPS_SecuMonitorLiveDataDetail d1 on d1.UID = d.UID and d1.TypeID COLLATE DATABASE_DEFAULT = d.TypeID COLLATE DATABASE_DEFAULT
			where d1.UID is null

	insert GFI_FLT_SecuMonitorToBSeen
		select distinct h.AssetID from #GFI_GPS_SecuMonitorLiveData h
			inner join #GFI_GPS_SecuMonitorLiveDataDetail d on h.GPSDataID = d.UID
            inner join GFI_FLT_SecuMonitorAssets a on d.TypeID COLLATE DATABASE_DEFAULT = a.PanicID COLLATE DATABASE_DEFAULT 
                and d.TypeValue COLLATE DATABASE_DEFAULT = a.PanicValue COLLATE DATABASE_DEFAULT
		where h.AssetID not in (select AssetID from GFI_FLT_SecuMonitorToBSeen)

    ;with 
		cte as 
			(
				select uid, AssetID, 
					DriverID,
					DateTimeGPS_UTC, 
					LongLatValidFlag, 
					row_number() over(partition by AssetID order by DateTimeGPS_UTC desc) as RowNum
				from GFI_GPS_SecuMonitorLiveData
				WHERE DateTimeGPS_UTC < getdate() + 1
			)
	delete GFI_GPS_SecuMonitorLiveData 
		--select * 
		from GFI_GPS_SecuMonitorLiveData l
			inner join cte on cte.UID = l.UID
		where cte.RowNum > 6

    commit
End try
begin catch
	rollback
	SELECT
		ERROR_NUMBER() AS ErrorNumber
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_STATE() AS ErrorState
		,ERROR_PROCEDURE() AS ErrorProcedure
		,ERROR_LINE() AS ErrorLine
		,ERROR_MESSAGE() AS ErrorMessage;
end catch

drop table #GFI_GPS_SecuMonitorLiveData
drop table #GFI_GPS_SecuMonitorLiveDataDetail
";
            //File.AppendAllText(Globals.LogPath() + "LiveDwn.txt", sql);
            //return true;

            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            if (!bResult)
            {
                try
                {
                    FileInfo f = new FileInfo(Globals.LogPath() + "LiveDwn.txt");
                    if (f.Length > 100000)
                        File.Delete(Globals.LogPath() + "LiveDwn.txt");
                }
                catch { }

                File.AppendAllText(Globals.LogPath() + "LiveDwn.txt", "\r\n**********Errors Detected*************\r\n");
                File.AppendAllText(Globals.LogPath() + "LiveDwn.txt", DateTime.Now.ToString());
                File.AppendAllText(Globals.LogPath() + "LiveDwn.txt", "\r\n**************************************\r\n");
                foreach (DataRow dr in dtCtrl.Rows)
                {
                    foreach (DataColumn dc in dtCtrl.Columns)
                        File.AppendAllText(Globals.LogPath() + "LiveDwn.txt", "\r\n" + dc.ColumnName + " : " + dr[dc].ToString());
                    File.AppendAllText(Globals.LogPath() + "LiveDwn.txt", "\r\n");
                }
            }

            return bResult;
        }

        public bool UpdateGPSData(GPSData uGPSData, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_GPS_GPSData";
            dt.Columns.Add("AssetID", uGPSData.AssetID.GetType());
            dt.Columns.Add("DriverID", uGPSData.DeviceID.GetType());
            dt.Columns.Add("DateTimeGPS_UTC", uGPSData.DateTimeGPS_UTC.GetType());
            dt.Columns.Add("DateTimeServer", uGPSData.DateTimeServer.GetType());
            dt.Columns.Add("Longitude", uGPSData.Longitude.GetType());
            dt.Columns.Add("Latitude", uGPSData.Latitude.GetType());
            dt.Columns.Add("LongLatValidFlag", uGPSData.LongLatValidFlag.GetType());
            dt.Columns.Add("Speed", uGPSData.Speed.GetType());
            dt.Columns.Add("EngineOn", uGPSData.EngineOn.GetType());
            dt.Columns.Add("StopFlag", uGPSData.StopFlag.GetType());
            dt.Columns.Add("TripDistance", uGPSData.TripDistance.GetType());
            dt.Columns.Add("TripTime", uGPSData.TripTime.GetType());
            dt.Columns.Add("WorkHour", uGPSData.WorkHour.GetType());
            DataRow dr = dt.NewRow();

            AssetDeviceMap ADMap = new AssetDeviceMap();
            ADMap = new AssetDeviceMapService().GetAssetDeviceMapByDevice(uGPSData.DeviceID.ToString(), sConnStr);

            dr["AssetID"] = ADMap.AssetID;
            dr["DriverID"] = uGPSData.DeviceID;
            dr["DateTimeGPS_UTC"] = uGPSData.DateTimeGPS_UTC;
            dr["DateTimeServer"] = uGPSData.DateTimeServer;
            dr["Longitude"] = uGPSData.Longitude;
            dr["Latitude"] = uGPSData.Latitude;
            dr["LongLatValidFlag"] = uGPSData.LongLatValidFlag;
            dr["Speed"] = uGPSData.Speed;
            dr["EngineOn"] = uGPSData.EngineOn;
            dr["StopFlag"] = uGPSData.StopFlag;
            dr["TripDistance"] = uGPSData.TripDistance;
            dr["TripTime"] = uGPSData.TripTime;
            dr["WorkHour"] = uGPSData.WorkHour;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "UID = '" + uGPSData.UID + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool DeleteGPSData(GPSData uGPSData, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_GPS_GPSData", "UID = '" + uGPSData.UID + "'", sConnStr))
                return true;
            else
                return false;
        }
    }
}