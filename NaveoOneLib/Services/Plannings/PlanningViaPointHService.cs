using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Plannings;
using NaveoOneLib.Services.Audits;

namespace NaveoOneLib.Services.Plannings
{
    public class PlanningViaPointHService
    {
        //Errors er = new Errors();

        public String sGetPlanningViaPointHByPID(int PID)
        {
            String sql = "select * from GFI_PLN_PlanningViaPointH where PID = " + PID;
            return sql;
        }
        public String sGetPlanningViaPointDByPID(int PID)
        {
            String sql = "select d.* from  GFI_PLN_PlanningViaPointH h inner join GFI_PLN_PlanningViaPointD d on h.iID = d.HID where h.PID = " + PID;
          
            return sql;
        }




        public DataTable GetPlanningViaPointH(String sConnStr)
        {
            String sqlString = @"SELECT * from GFI_PLN_PlanningViaPointH order by PID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public PlanningViaPointH GetPlanningViaPointH(DataRow dr)
        {
            PlanningViaPointH retPlanningViaPointH = new PlanningViaPointH();
            retPlanningViaPointH.iID = Convert.ToInt32(dr["iID"]);
            retPlanningViaPointH.PID = Convert.ToInt32(dr["PID"]);
            retPlanningViaPointH.SeqNo = Convert.ToInt32(dr["SeqNo"]);
            retPlanningViaPointH.ZoneID = String.IsNullOrEmpty(dr["ZoneID"].ToString()) ? (int?)null : Convert.ToInt32(dr["ZoneID"]);

            retPlanningViaPointH.Lat = String.IsNullOrEmpty(dr["Lat"].ToString()) ? (Double?)null : Convert.ToDouble(dr["Lat"].ToString());
            retPlanningViaPointH.Lon = String.IsNullOrEmpty(dr["Lon"].ToString()) ? (Double?)null : Convert.ToDouble(dr["Lon"].ToString());
            retPlanningViaPointH.LocalityVCA = String.IsNullOrEmpty(dr["LocalityVCA"].ToString()) ? (int?)null : Convert.ToInt32(dr["LocalityVCA"]);
            retPlanningViaPointH.Buffer = String.IsNullOrEmpty(dr["Buffer"].ToString()) ? (int?)null : Convert.ToInt32(dr["Buffer"]);
            retPlanningViaPointH.Remarks = dr["Remarks"].ToString();
            retPlanningViaPointH.dtUTC = String.IsNullOrEmpty(dr["dtUTC"].ToString()) ? (DateTime?)null : (DateTime)dr["dtUTC"];
            retPlanningViaPointH.minDuration = String.IsNullOrEmpty(dr["minDuration"].ToString()) ? (int?)null : Convert.ToInt32(dr["minDuration"]);
            retPlanningViaPointH.maxDuration = String.IsNullOrEmpty(dr["maxDuration"].ToString()) ? (int?)null : Convert.ToInt32(dr["maxDuration"]);
            return retPlanningViaPointH;
        }

        public PlanningViaPointH GetPlanningViaPointHById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT * from GFI_PLN_PlanningViaPointH WHERE PID = ?";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetPlanningViaPointH(dt.Rows[0]);
        }
        DataTable PlanningViaPointHDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_PLN_PlanningViaPointH";
            dt.Columns.Add("iID", typeof(int));
            dt.Columns.Add("PID", typeof(int));
            dt.Columns.Add("SeqNo", typeof(int));
            dt.Columns.Add("ZoneID", typeof(int));
            dt.Columns.Add("Lat", typeof(float));
            dt.Columns.Add("Lon", typeof(float));
            dt.Columns.Add("LocalityVCA", typeof(int));
            dt.Columns.Add("Buffer", typeof(int));
            dt.Columns.Add("Remarks", typeof(String));
            dt.Columns.Add("dtUTC", typeof(DateTime));
            dt.Columns.Add("minDuration", typeof(int));
            dt.Columns.Add("maxDuration", typeof(int));

            return dt;
        }
        public Boolean SavePlanningViaPointH(PlanningViaPointH uPlanningViaPointH, Boolean bInsert, String sConnStr)
        {
            DataTable dt = PlanningViaPointHDT();

            DataRow dr = dt.NewRow();
            dr["iID"] = uPlanningViaPointH.iID;
            dr["PID"] = uPlanningViaPointH.PID;
            dr["SeqNo"] = uPlanningViaPointH.SeqNo;
            dr["ZoneID"] = uPlanningViaPointH.ZoneID != null ? uPlanningViaPointH.ZoneID : (object)DBNull.Value;
            dr["Lat"] = uPlanningViaPointH.Lat != null ? uPlanningViaPointH.Lat : (object)DBNull.Value;
            dr["Lon"] = uPlanningViaPointH.Lon != null ? uPlanningViaPointH.Lon : (object)DBNull.Value;
            dr["LocalityVCA"] = uPlanningViaPointH.LocalityVCA != null ? uPlanningViaPointH.LocalityVCA : (object)DBNull.Value;
            dr["Buffer"] = uPlanningViaPointH.Buffer != null ? uPlanningViaPointH.Buffer : (object)DBNull.Value;
            dr["Remarks"] = uPlanningViaPointH.Remarks;
            dr["dtUTC"] = uPlanningViaPointH.dtUTC != null ? uPlanningViaPointH.dtUTC : (object)DBNull.Value;
            dr["minDuration"] = uPlanningViaPointH.minDuration != null ? uPlanningViaPointH.minDuration : (object)DBNull.Value;
            dr["maxDuration"] = uPlanningViaPointH.maxDuration != null ? uPlanningViaPointH.maxDuration : (object)DBNull.Value;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "PID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uPlanningViaPointH.PID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public Boolean DeletePlanningViaPointH(PlanningViaPointH uPlanningViaPointH, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_PLN_PlanningViaPointH where PID = " + uPlanningViaPointH.PID.ToString();
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            DataTable dtAudit = new AuditService().LogTran(3, "PlanningViaPointH", uPlanningViaPointH.PID.ToString(), Globals.uLogin.UID, uPlanningViaPointH.PID);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "CallerFunction";
            drCtrl["ParentTblName"] = "GFI_PLN_PlanningViaPointH";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
    }
}
