using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Plannings;
using NaveoOneLib.Services.Audits;

namespace NaveoOneLib.Services.Plannings
{
    class ScheduledPlanService
    {
        Errors er = new Errors();

        public String sScheduledPlanByHID(int HID, String sConnStr)
        {
            String sql = String.Empty;
            sql = "SELECT * from GFI_PLN_ScheduledPlan WHERE HID = " + HID.ToString();
            return sql;
        }

        public DataTable GetScheduledPlan(String sConnStr)
        {
            String sqlString = @"SELECT iID, 
HID, 
PID from GFI_PLN_ScheduledPlan order by HID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        public ScheduledPlan GetScheduledPlan(DataRow dr)
        {
            ScheduledPlan retScheduledPlan = new ScheduledPlan();
            retScheduledPlan.iID = Convert.ToInt32(dr["iID"]);
            retScheduledPlan.HID = Convert.ToInt32(dr["HID"]);
            retScheduledPlan.PID = Convert.ToInt32(dr["PID"]);
            return retScheduledPlan;
        }
        public ScheduledPlan GetScheduledPlanById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT iID, 
HID, 
PID from GFI_PLN_ScheduledPlan
WHERE HID = ?
order by HID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID,sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetScheduledPlan(dt.Rows[0]);
        }

        DataTable ScheduledPlanDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_PLN_ScheduledPlan";
            dt.Columns.Add("iID", typeof(int));
            dt.Columns.Add("HID", typeof(int));
            dt.Columns.Add("PID", typeof(int));

            return dt;
        }
        public Boolean SaveScheduledPlan(ScheduledPlan uScheduledPlan, Boolean bInsert, String sConnStr)
        {
            DataTable dt = ScheduledPlanDT();
            DataRow dr = dt.NewRow();
            dr["iID"] = uScheduledPlan.iID;
            dr["HID"] = uScheduledPlan.HID;
            dr["PID"] = uScheduledPlan.PID;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "HID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uScheduledPlan.HID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public Boolean DeleteScheduledPlan(ScheduledPlan uScheduledPlan, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_PLN_ScheduledPlan where HID = " + uScheduledPlan.HID.ToString();
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            DataTable dtAudit = new AuditService().LogTran(3, "ScheduledPlan", uScheduledPlan.HID.ToString(), Globals.uLogin.UID, uScheduledPlan.HID);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "CallerFunction";
            drCtrl["ParentTblName"] = "GFI_PLN_ScheduledPlan";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
    }
}
