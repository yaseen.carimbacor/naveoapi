using System;
using System.Collections.Generic;
using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Plannings;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Services.Audits;
using System.Linq;
using NaveoOneLib.Services.Rules;

namespace NaveoOneLib.Services.Plannings
{
    public class PlanningService
    {
        Errors er = new Errors();

        public DataTable GetPlanning(String sConnStr, DateTime dtFrom, DateTime dtTo)
        {

            String sqlString = @"SELECT * from GFI_PLN_Planning where CreatedDate >= '" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "' and CreatedDate <= '" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff") + @"' and Approval = 0 order by PID asc";

            DataTable dt = new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
            return dt;
        }

        Planning GetPlanning(DataSet ds)
        {
            DataRow dr = ds.Tables["Planning"].Rows[0];
            Planning retPlanning = new Planning();
            retPlanning.PID = Convert.ToInt32(dr["PID"]);
            retPlanning.Approval = String.IsNullOrEmpty(dr["Approval"].ToString()) ? (int?)null : Convert.ToInt32(dr["Approval"]);
            retPlanning.Remarks = dr["Remarks"].ToString();
            retPlanning.CreatedDate = String.IsNullOrEmpty(dr["CreatedDate"].ToString()) ? (DateTime?)null : (DateTime)dr["CreatedDate"];
            retPlanning.UpdatedDate = String.IsNullOrEmpty(dr["UpdatedDate"].ToString()) ? (DateTime?)null : (DateTime)dr["UpdatedDate"];
            retPlanning.CreatedBy = String.IsNullOrEmpty(dr["CreatedBy"].ToString()) ? (int?)null : Convert.ToInt32(dr["CreatedBy"]);
            retPlanning.UpdatedBy = String.IsNullOrEmpty(dr["UpdatedBy"].ToString()) ? (int?)null : Convert.ToInt32(dr["UpdatedBy"]);
            retPlanning.requestCode = String.IsNullOrEmpty(dr["RequestCode"].ToString()) ? string.Empty : dr["RequestCode"].ToString();
            retPlanning.parentRequestCode = String.IsNullOrEmpty(dr["ParentRequestCode"].ToString()) ? string.Empty : dr["ParentRequestCode"].ToString();
            retPlanning.Ref = String.IsNullOrEmpty(dr["Ref"].ToString()) ? string.Empty : dr["Ref"].ToString();
            retPlanning.sComments = String.IsNullOrEmpty(dr["sComments"].ToString()) ? string.Empty : dr["sComments"].ToString();
            retPlanning.closureComments = String.IsNullOrEmpty(dr["closureComments"].ToString()) ? string.Empty : dr["closureComments"].ToString();
            retPlanning.urgent = Convert.ToInt32(dr["Urgent"]);
            retPlanning.type = dr["Type"].ToString();
            retPlanning.ApprovalID = String.IsNullOrEmpty(dr["ApprovalID"].ToString()) ? 0 : Convert.ToInt32(dr["ApprovalID"]);
           


            List<Matrix> lMatrix = new List<Matrix>();
            if (ds.Tables.Contains("dtMatrix"))
            {
                MatrixService ms = new MatrixService();
                foreach (DataRow dri in ds.Tables["dtMatrix"].Rows)
                    lMatrix.Add(ms.AssignMatrix(dri));
            }
            retPlanning.lMatrix = lMatrix;

            List<PlanningLkup> lPlanningLkup = new List<PlanningLkup>();
            if (ds.Tables.Contains("PlanningLkup"))
            {
                PlanningLkupService pl = new PlanningLkupService();
                foreach (DataRow dri in ds.Tables["PlanningLkup"].Rows)
                {
                    PlanningLkup retPlanningLkup = new PlanningLkup();
                    retPlanningLkup.iID = Convert.ToInt32(dri["iID"]);
                    retPlanningLkup.PID = Convert.ToInt32(dri["PID"]);
                    retPlanningLkup.LookupValueID = Convert.ToInt32(dri["LookupValueID"]);
                    retPlanningLkup.LookupType = dri["LookupType"].ToString();

                    if (ds.Tables.Contains("PlanningLOOKUP"))
                        if (ds.Tables["PlanningLOOKUP"].Rows.Count > 0)
                        {
                            foreach (DataRow drii in ds.Tables["PlanningLOOKUP"].Rows)
                            {
                                if (dri["LookupValueID"].ToString() != string.Empty)
                                {
                                    if (dri["LookupValueID"].ToString() == drii["VID"].ToString())
                                    {
                                        retPlanningLkup.LookupValueDesc = drii["Name"].ToString();
                                        retPlanningLkup.LookupType = drii["Description"].ToString();
                                    }

                                }
                            }
                        }

                    lPlanningLkup.Add(retPlanningLkup);
                }

            }
            retPlanning.lPlanningLkup = lPlanningLkup;

            List<PlanningResource> lPlanningResource = new List<PlanningResource>();
            if (ds.Tables.Contains("PlanningResource"))
            {
                PlanningResourceService pr = new PlanningResourceService();
                foreach (DataRow drResource in ds.Tables["PlanningResource"].Rows)
                {
                    PlanningResource retPlanningResource = new PlanningResource();
                    retPlanningResource.iID = Convert.ToInt32(drResource["iID"]);
                    retPlanningResource.PID = Convert.ToInt32(drResource["PID"]);
                    retPlanningResource.DriverID = Convert.ToInt32(drResource["DriverID"]);
                    retPlanningResource.ResourceType = drResource["ResourceType"].ToString();

                    if (drResource["CustomType"].ToString() != string.Empty)
                        retPlanningResource.CustomType = Convert.ToInt32(drResource["CustomType"]);

                    if (drResource["MileageAllowance"].ToString() != string.Empty)
                        retPlanningResource.MileageAllowance = Convert.ToInt32(drResource["MileageAllowance"]);

                    if (drResource["TravelGrant"].ToString() != string.Empty)
                        retPlanningResource.TravelGrant = Convert.ToInt32(drResource["TravelGrant"]);

                    retPlanningResource.Remarks = drResource["Remarks"].ToString();

                    if (ds.Tables.Contains("Resource"))
                        if (ds.Tables["Resource"].Rows.Count > 0)
                        {
                            foreach (DataRow drR in ds.Tables["Resource"].Rows)
                            {
                                if (drResource["DriverID"].ToString() != String.Empty)
                                {
                                    if (drResource["DriverID"].ToString() == drR["DriverID"].ToString())
                                    {
                                        retPlanningResource.DriverName = drR["sDriverName"].ToString();
                                    }
                                }
                            }
                        }

                    lPlanningResource.Add(retPlanningResource);
                }

            }
            retPlanning.lPlanningResource = lPlanningResource;

            List<PlanningViaPointH> lvia = new List<PlanningViaPointH>();
            if (ds.Tables.Contains("ViaH"))
            {
                PlanningViaPointHService hs = new PlanningViaPointHService();
                foreach (DataRow dri in ds.Tables["ViaH"].Rows)
                {
                    PlanningViaPointH h = new PlanningViaPointH();
                    h.iID = Convert.ToInt32(dri["iID"]);
                    h.PID = Convert.ToInt32(dri["PID"]);
                    h.SeqNo = Convert.ToInt32(dri["SeqNo"]);
                    h.ZoneID = String.IsNullOrEmpty(dri["ZoneID"].ToString()) ? (int?)null : Convert.ToInt32(dri["ZoneID"]);

                    h.Lat = String.IsNullOrEmpty(dri["Lat"].ToString()) ? (Double?)null : Convert.ToDouble(dri["Lat"].ToString());
                    h.Lon = String.IsNullOrEmpty(dri["Lon"].ToString()) ? (Double?)null : Convert.ToDouble(dri["Lon"].ToString());
                    h.LocalityVCA = String.IsNullOrEmpty(dri["LocalityVCA"].ToString()) ? (int?)null : Convert.ToInt32(dri["LocalityVCA"]);
                    h.Buffer = String.IsNullOrEmpty(dri["Buffer"].ToString()) ? (int?)null : Convert.ToInt32(dri["Buffer"]);
                    h.Remarks = dri["Remarks"].ToString();
                    h.dtUTC = String.IsNullOrEmpty(dri["dtUTC"].ToString()) ? (DateTime?)null : (DateTime)dri["dtUTC"];
                    h.minDuration = String.IsNullOrEmpty(dri["minDuration"].ToString()) ? 0 : Convert.ToInt32(dri["minDuration"]);


                    if (ds.Tables.Contains("Zone"))
                        if (ds.Tables["Zone"].Rows.Count > 0)
                        {
                            foreach (DataRow drZone in ds.Tables["Zone"].Rows)
                            {
                                if (dri["ZoneID"].ToString() != String.Empty)
                                {
                                    if ((int)dri["ZoneID"] == (int)drZone["ZoneID"])
                                    {
                                        h.ZoneDesc = drZone["Description"].ToString();
                                    }
                                }
                            }
                        }


                    if (ds.Tables.Contains("VCA"))
                        if (ds.Tables["VCA"].Rows.Count > 0)
                        {
                            foreach (DataRow drVca in ds.Tables["VCA"].Rows)
                            {
                                if (dri["LocalityVCA"].ToString() != String.Empty)
                                {
                                    if ((int)dri["LocalityVCA"] == (int)drVca["VCAID"])
                                    {
                                        h.LocalityVCADesc = drVca["VCAName"].ToString();
                                    }
                                }
                            }
                        }


                    DataRow[] drd = ds.Tables["ViaD"].Select("HID = " + h.iID);
                    PlanningViaPointDService detailS = new PlanningViaPointDService();
                    foreach (DataRow dd in drd)
                    {
                        PlanningViaPointD retPlanningViaPointD = new PlanningViaPointD();
                        retPlanningViaPointD.HID = Convert.ToInt32(dd["HID"]);
                        retPlanningViaPointD.iID = Convert.ToInt32(dd["iID"]);
                        retPlanningViaPointD.UOM = String.IsNullOrEmpty(dd["UOM"].ToString()) ? (int?)null : Convert.ToInt32(dd["UOM"]);
                        retPlanningViaPointD.Capacity = String.IsNullOrEmpty(dd["Capacity"].ToString()) ? (int?)null : Convert.ToInt32(dd["Capacity"]);
                        retPlanningViaPointD.Remarks = dd["Remarks"].ToString();
                        h.lDetail.Add(retPlanningViaPointD);
                    }


                    lvia.Add(h);




                }
            }
            retPlanning.lPlanningViaPointH = lvia;

            return retPlanning;
        }
        public Planning GetPlanningById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT * from GFI_PLN_Planning
                                    WHERE PID = ?
                                    order by PID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;

            DataSet ds = new DataSet();
            dt.TableName = "Planning";
            ds.Merge(dt);
            return GetPlanning(ds);
        }
        public Planning GetPlanningByPID(int PID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"SELECT * from GFI_PLN_Planning WHERE PID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = PID;
            drSql["sTableName"] = "Planning";
            dtSql.Rows.Add(drSql);

            sql = new MatrixService().sGetMatrixId(PID, "GFI_SYS_GroupMatrixPlanning");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);

            sql = new PlanningLkupService().sGetPlanningLkupByPID(PID);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "PlanningLkup";
            dtSql.Rows.Add(drSql);

            sql = new PlanningResourceService().sPlanningResourceByPID(PID);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "PlanningResource";
            dtSql.Rows.Add(drSql);

            PlanningViaPointHService pvps = new PlanningViaPointHService();
            sql = pvps.sGetPlanningViaPointHByPID(PID);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "ViaH";
            dtSql.Rows.Add(drSql);

            sql = pvps.sGetPlanningViaPointDByPID(PID);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "ViaD";
            dtSql.Rows.Add(drSql);


            #region LOOKUPVALUES
            sql = @"select Distinct v.VID ,v.Name ,t.Description ,p.PID from GFI_PLN_PlanningLkup p inner join GFI_SYS_LookUpValues v on p.LookupValueID =v.VID inner join GFI_SYS_LookUpTypes t on t.TID =v.TID where p.PID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = PID;
            drSql["sTableName"] = "PlanningLOOKUP";
            dtSql.Rows.Add(drSql);
            #endregion

            #region RESOURCES
            sql = @"select Distinct d.DriverID ,d.sDriverName ,d.EmployeeType ,R.PID from GFI_PLN_PlanningResource r inner join GFI_FLT_DRIVER  d on r.DriverID =d.DriverID where r.PID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = PID;
            drSql["sTableName"] = "Resource";
            dtSql.Rows.Add(drSql);
            #endregion

            #region ZONE
            sql = @"select  Distinct z.ZoneID ,z.Description ,viaH.PID from  GFI_PLN_PlanningViaPointH viaH inner join GFI_FLT_ZoneHeader z on viaH.ZoneID = z.ZoneID where  viaH.PID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = PID;
            drSql["sTableName"] = "Zone";
            dtSql.Rows.Add(drSql);
            #endregion


            #region VCA
            sql = @"select Distinct vca.VCAID,vca.VCAName,viaH.PID from  GFI_PLN_PlanningViaPointH viaH inner join GFI_GIS_VCA vca on viaH.LocalityVCA = vca.VCAID where  viaH.PID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = PID;
            drSql["sTableName"] = "VCA";
            dtSql.Rows.Add(drSql);
            #endregion


            #region UOM
            sql = @"select * from GFI_SYS_UOM";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = PID;
            drSql["sTableName"] = "UOM";
            dtSql.Rows.Add(drSql);
            #endregion

            #region LookupValues
            sql = @"select * from GFI_SYS_LookUpValues";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "GFI_SYS_LookUpValues";
            dtSql.Rows.Add(drSql);
            #endregion

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables["Planning"].Rows.Count == 0)
                return null;
            else
                return GetPlanning(ds);
        }

        DataTable PlanningDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_PLN_Planning";
            dt.Columns.Add("PID", typeof(int));
            dt.Columns.Add("Approval", typeof(int));
            dt.Columns.Add("Remarks", typeof(String));
            dt.Columns.Add("CreatedDate", typeof(DateTime));
            dt.Columns.Add("UpdatedDate", typeof(DateTime));
            dt.Columns.Add("CreatedBy", typeof(int));
            dt.Columns.Add("UpdatedBy", typeof(int));
            dt.Columns.Add("ApprovalID", typeof(int));
           
            dt.Columns.Add("urgent", typeof(int));
            dt.Columns.Add("requestCode", typeof(String));
            dt.Columns.Add("parentRequestCode", typeof(String));
            dt.Columns.Add("type", typeof(String));
            dt.Columns.Add("sComments", typeof(String));
            dt.Columns.Add("closureComments", typeof(String));
            dt.Columns.Add("Ref", typeof(String));
            dt.Columns.Add("oprType", typeof(DataRowState)); //ApprovalID

            return dt;
        }
        public BaseModel SavePlanning(Planning uPlanning, List<DateTime> lDates, int UID, Boolean bInsert, String sConnStr)
        {
            BaseModel baseModel = new BaseModel();
            Boolean bResult = false;

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            DataSet dsProcess = new DataSet();
            String sql = String.Empty;

            int ApprovalID = new NaveoOneLib.Services.Users.UserService().GetUserApprovalProcessIdByUserID(UID, sConnStr);
            int ApprovalValue = new NaveoOneLib.Services.Users.UserService().GetUserApprovalProcessValueByUserID(UID, sConnStr);
            uPlanning.ApprovalID = ApprovalID;
            

            if (lDates == null || lDates.Count == 0)
            {
                lDates = new List<DateTime>();
                if (uPlanning.lPlanningViaPointH.Count > 0)
                    lDates.Add(uPlanning.lPlanningViaPointH[0].dtUTC.Value);
            }


            int iRecurrent = 0;
            foreach (DateTime date in lDates)
            {
                iRecurrent += 1000;

                //GFI_PLN_Planning
                DataTable dt = PlanningDT();
                dt.Columns.Add("OPRSeqNo", typeof(int));

                DataRow dr = dt.NewRow();
                dr["PID"] = uPlanning.PID;
                dr["Approval"] = uPlanning.Approval != null ? uPlanning.Approval : (object)DBNull.Value;
                dr["Remarks"] = String.IsNullOrEmpty(uPlanning.Remarks) ? (object)DBNull.Value : uPlanning.Remarks;
                dr["oprType"] = uPlanning.OprType;
                if (bInsert)
                {
                    uPlanning.CreatedDate = DateTime.UtcNow;
                    uPlanning.CreatedBy = UID;
                    uPlanning.UpdatedDate = Constants.NullDateTime;
                }
                else
                {
                    uPlanning.UpdatedDate = DateTime.UtcNow;
                    uPlanning.UpdatedBy = UID;
                }

                dr["urgent"] = uPlanning.urgent;
                dr["type"] = String.IsNullOrEmpty(uPlanning.type) ? (object)DBNull.Value : uPlanning.type;
                dr["sComments"] = String.IsNullOrEmpty(uPlanning.sComments) ? (object)DBNull.Value : uPlanning.sComments;
                dr["closureComments"] = String.IsNullOrEmpty(uPlanning.closureComments) ? (object)DBNull.Value : uPlanning.closureComments;
                dr["Ref"] = String.IsNullOrEmpty(uPlanning.Ref) ? (object)DBNull.Value : uPlanning.Ref;
                dr["requestCode"] = String.IsNullOrEmpty(uPlanning.requestCode) ? (object)DBNull.Value : uPlanning.requestCode;
                dr["parentRequestCode"] = String.IsNullOrEmpty(uPlanning.parentRequestCode) ? (object)DBNull.Value : uPlanning.parentRequestCode;
                dr["ApprovalID"] = uPlanning.ApprovalID;
               

                dr["CreatedDate"] = uPlanning.CreatedDate == null ? (object)DBNull.Value : uPlanning.CreatedDate;
                if (uPlanning.CreatedBy != null)
                    dr["CreatedBy"] = uPlanning.CreatedBy;

                dr["UpdatedDate"] = uPlanning.UpdatedDate == null ? (object)DBNull.Value : uPlanning.UpdatedDate;
                if (uPlanning.UpdatedBy != null)
                    dr["UpdatedBy"] = uPlanning.UpdatedBy;

                dr["OPRSeqNo"] = 1 + iRecurrent;
                dt.Rows.Add(dr);

                if (bInsert)
                    dt.Columns.Remove("UpdatedDate");
                else
                    dt.Columns.Remove("CreatedDate");

                if (bInsert)
                {
                        sql = @"exec sp_MOHRequestCode 'RET'";

                        drCtrl = dtCtrl.NewRow();
                        drCtrl["SeqNo"] = 1;
                        drCtrl["ParentTblName"] = dt.TableName;
                        drCtrl["KeyFieldName"] = "RequestCode";
                        drCtrl["NextIdAction"] = "GetFromSP";
                        drCtrl["TimeOut"] = 1000;
                        drCtrl["Command"] = sql;
                        drCtrl["SeqNo"] = 1 + iRecurrent;
                        dtCtrl.Rows.Add(drCtrl);

                        if (iRecurrent == 1000)
                        {
                            drCtrl = dtCtrl.NewRow();
                            drCtrl["SeqNo"] = 2 + iRecurrent;
                            drCtrl["ParentTblName"] = dt.TableName;
                            drCtrl["NextIdAction"] = "GetFromField";
                            drCtrl["KeyFieldName"] = "RequestCode";
                            drCtrl["NextIdFieldName"] = "ParentRequestCode";
                            drCtrl["TimeOut"] = 1000;
                            drCtrl["SeqNo"] = 1 + iRecurrent;
                            dtCtrl.Rows.Add(drCtrl);
                        }

                    

                }

                //GFI_PLN_Planning
                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 3 + iRecurrent;
                drCtrl["TblName"] = dt.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "PID";
                drCtrl["KeyIsIdentity"] = "TRUE";
                if (bInsert)
                    drCtrl["NextIdAction"] = "GET";
                else
                    drCtrl["NextIdValue"] = uPlanning.PID;
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dt);

                //GFI_SYS_GroupMatrixPlanning
                if (uPlanning.lMatrix == null || uPlanning.lMatrix.Count == 0)
                    uPlanning.lMatrix = new MatrixService().GetMatrixByUserID(UID, sConnStr);
                DataTable dtMatrix = new myConverter().ListToDataTable<Matrix>(uPlanning.lMatrix);
                dtMatrix.TableName = "GFI_SYS_GroupMatrixPlanning";
                dtMatrix.Columns.Add("OPRSeqNo", typeof(int));
                foreach (DataRow r in dtMatrix.Rows)
                    r["OPRSeqNo"] = 3 + iRecurrent;
                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 4 + iRecurrent;
                drCtrl["TblName"] = dtMatrix.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "MID";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdAction"] = "SET";
                drCtrl["NextIdFieldName"] = "iID";
                drCtrl["ParentTblName"] = dt.TableName;
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtMatrix);

                //GFI_PLN_PlanningLkup
                if (uPlanning.lPlanningLkup != null)
                    if (uPlanning.lPlanningLkup.Count > 0)
                    {
                        DataTable dtLKup = new myConverter().ListToDataTable<PlanningLkup>(uPlanning.lPlanningLkup);
                        dtLKup.TableName = "GFI_PLN_PlanningLkup";
                        dtLKup.Columns.Add("OPRSeqNo", typeof(int));
                        foreach (DataRow r in dtLKup.Rows)
                            r["OPRSeqNo"] = 3 + iRecurrent;
                        drCtrl = dtCtrl.NewRow();
                        drCtrl["SeqNo"] = 5 + iRecurrent;
                        drCtrl["TblName"] = dtLKup.TableName;
                        drCtrl["CmdType"] = "TABLE";
                        drCtrl["KeyFieldName"] = "iID";
                        drCtrl["KeyIsIdentity"] = "TRUE";
                        drCtrl["NextIdAction"] = "SET";
                        drCtrl["NextIdFieldName"] = "PID";
                        drCtrl["ParentTblName"] = dt.TableName;
                        dtCtrl.Rows.Add(drCtrl);
                        dtLKup.Columns.Remove("LookupValueDesc");
                        dsProcess.Merge(dtLKup);
                    }

                //GFI_PLN_PlanningResource
                if (uPlanning.lPlanningResource != null)
                    if (uPlanning.lPlanningResource.Count > 0)
                    {
                        foreach (PlanningResource p in uPlanning.lPlanningResource)
                            if (!p.iSource.HasValue)
                                p.iSource = 1;

                        DataTable dtResource = new myConverter().ListToDataTable<PlanningResource>(uPlanning.lPlanningResource);
                        dtResource.TableName = "GFI_PLN_PlanningResource";
                        dtResource.Columns.Add("OPRSeqNo", typeof(int));
                        foreach (DataRow r in dtResource.Rows)
                            r["OPRSeqNo"] = 3 + iRecurrent;
                        drCtrl = dtCtrl.NewRow();
                        drCtrl["SeqNo"] = 6 + iRecurrent;
                        drCtrl["TblName"] = dtResource.TableName;
                        drCtrl["CmdType"] = "TABLE";
                        drCtrl["KeyFieldName"] = "iID";
                        drCtrl["KeyIsIdentity"] = "TRUE";
                        drCtrl["NextIdAction"] = "SET";
                        drCtrl["NextIdFieldName"] = "PID";
                        drCtrl["ParentTblName"] = dt.TableName;
                        dtCtrl.Rows.Add(drCtrl);
                        dtResource.Columns.Remove("DriverName");
                        dsProcess.Merge(dtResource);
                    }

                //GFI_PLN_PlanningViaPointH
                if (uPlanning.lPlanningViaPointH != null)
                    if (uPlanning.lPlanningViaPointH.Count > 0)
                    {
                        for (int i = 0; i < uPlanning.lPlanningViaPointH.Count; i++)
                        {
                            DateTime dtFrom = date.Date;
                            dtFrom = dtFrom.Add(uPlanning.lPlanningViaPointH[i].dtUTC.Value.ToUniversalTime().TimeOfDay);
                            uPlanning.lPlanningViaPointH[i].dtUTC = dtFrom;

                            uPlanning.lPlanningViaPointH[i].SeqNo = i + 1;
                            if (i == uPlanning.lPlanningViaPointH.Count - 1)
                                uPlanning.lPlanningViaPointH[i].SeqNo = 999;
                        }
                        DataTable dtViaH = new myConverter().ListToDataTable<PlanningViaPointH>(uPlanning.lPlanningViaPointH);
                        dtViaH.TableName = "GFI_PLN_PlanningViaPointH";
                        dtViaH.Columns.Remove("lDetail");
                        dtViaH.Columns.Add("OPRSeqNo", typeof(int));
                        foreach (DataRow r in dtViaH.Rows)
                            r["OPRSeqNo"] = 3 + iRecurrent;
                        dtViaH.Columns.Remove("ZoneDesc");
                        dtViaH.Columns.Remove("LocalityVCADesc");
                        dsProcess.Merge(dtViaH);

                        foreach (PlanningViaPointH h in uPlanning.lPlanningViaPointH)
                        {
                            //Get iID
                            drCtrl = dtCtrl.NewRow();
                            drCtrl["SeqNo"] = 7 + iRecurrent + h.SeqNo;
                            drCtrl["TblName"] = dtViaH.TableName;
                            drCtrl["CmdType"] = "TABLE";
                            drCtrl["KeyFieldName"] = "iID";
                            drCtrl["KeyIsIdentity"] = "TRUE";
                            if (bInsert)
                                drCtrl["NextIdAction"] = "GET";
                            dtCtrl.Rows.Add(drCtrl);

                            //Set PID
                            drCtrl = dtCtrl.NewRow();
                            drCtrl["SeqNo"] = 7 + iRecurrent;
                            drCtrl["TblName"] = dtViaH.TableName;
                            drCtrl["CmdType"] = "TABLE";
                            drCtrl["KeyFieldName"] = "iID";
                            drCtrl["KeyIsIdentity"] = "TRUE";
                            drCtrl["NextIdAction"] = "SET";
                            drCtrl["NextIdFieldName"] = "PID";
                            drCtrl["ParentTblName"] = dt.TableName;
                            dtCtrl.Rows.Add(drCtrl);

                            //GFI_PLN_PlanningViaPointD
                            DataTable dtViaD = new myConverter().ListToDataTable<PlanningViaPointD>(h.lDetail);
                            dtViaD.TableName = "GFI_PLN_PlanningViaPointD";
                            dtViaD.Columns.Add("OPRSeqNo", typeof(int));
                            foreach (DataRow r in dtViaD.Rows)
                                r["OPRSeqNo"] = 7 + iRecurrent + h.SeqNo;

                            drCtrl = dtCtrl.NewRow();
                            drCtrl["SeqNo"] = 9 + iRecurrent;
                            drCtrl["TblName"] = dtViaD.TableName;
                            drCtrl["CmdType"] = "TABLE";
                            drCtrl["KeyFieldName"] = "iID";
                            drCtrl["KeyIsIdentity"] = "TRUE";
                            drCtrl["NextIdAction"] = "SET";
                            drCtrl["NextIdFieldName"] = "HID";
                            drCtrl["ParentTblName"] = dtViaH.TableName;
                            dtCtrl.Rows.Add(drCtrl);
                            dtViaD.Columns.Remove("uomDesc");
                            dsProcess.Merge(dtViaD);
                        }
                    }
            }

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResult.Tables["CTRLTBL"];

            #region Audit Approval
            NaveoOneLib.Models.Plannings.Planning planningOLD = new NaveoOneLib.Models.Plannings.Planning();
            NaveoOneLib.Models.Plannings.Planning planningNew = new NaveoOneLib.Models.Plannings.Planning();

            if (uPlanning.type == "150")
            {
                DataRow[] dRowAudit = dtCtrl.Select("SeqNo ='1003'");
                planningNew.ApprovalID = uPlanning.ApprovalID;
                planningNew.PID = Convert.ToInt32(dRowAudit[0][10]);

                Boolean updatePlanningApproval = new NaveoOneLib.Services.Audits.ApprovalAuditService().SaveApprovalAudit(planningOLD, planningNew, "TranpsortRequest", UID, bInsert, sConnStr);
            }

            #endregion

            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                {
                    baseModel.dbResult = dsResult;
                    baseModel.dbResult.Tables["CTRLTBL"].TableName = "ResultCtrlTbl";
                    baseModel.dbResult.Merge(dsProcess);
                    bResult = false;
                }
                else
                    bResult = true;
            }
            else
                bResult = false;

            baseModel.data = bResult;
            baseModel.total = planningNew.PID;
            return baseModel;
        }
        public Boolean DeletePlanning(Planning uPlanning, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_PLN_Planning where PID = " + uPlanning.PID.ToString();
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            DataTable dtAudit = new AuditService().LogTran(3, "Planning", uPlanning.PID.ToString(), Globals.uLogin.UID, uPlanning.PID);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "CallerFunction";
            drCtrl["ParentTblName"] = "GFI_PLN_Planning";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }

        public DataTable GetPlanningRequest(DateTime dtFrom, DateTime dtTo, List<Matrix> lMatrix,int? PID, String sConnStr, List<String> sortColumns, Boolean sortOrderAsc)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            return GetPlanningRequest(dtFrom, dtTo, iParentIDs,PID, sConnStr, sortColumns, sortOrderAsc);
        }
        public DataTable GetPlanningRequest(DateTime dtFrom, DateTime dtTo, List<int> iParentIDs,int? PID,String sConnStr, List<String> sortColumns, Boolean sortOrderAsc)
        {
            DataTable dt = new Connection(sConnStr).GetDataDT(sGetPlanningRequest(dtFrom, dtTo, iParentIDs,PID, sConnStr, sortColumns, sortOrderAsc), sConnStr);
            return dt;
        }
        public String sGetPlanningRequest(DateTime dtFrom, DateTime dtTo, List<int> iParentIDs, int? PID, String sConnStr, List<String> sortColumns, Boolean sortOrderAsc)
        {

            #region sort
            String sSort = "dtFrom";
            if (sortColumns != null && sortColumns.Count > 0)
            {
                List<String> lCols = new List<string>() { "PID", "dtFrom,", "RequestCode", "Departure", "Arrival", "Approval", "ProcessedBy", "ProcessedDate" };
                String ss = String.Join(",", sortColumns
                    .Where(s => !String.IsNullOrWhiteSpace(s))
                    .Where(s => lCols.Contains(s))
                    .Select(s => s));

                if (!String.IsNullOrEmpty(ss))
                    sSort = ss;
            }

            if (sortOrderAsc)
                sSort += " asc";
            else
                sSort += " desc";
            #endregion


            String sql = @"
--GetPlanningRequest

drop table if exists #Planning;
SELECT distinct p.*
	, GetDate() dtFrom
	, GetDate() dtTo
	, Convert(nvarchar(500), null) ZoneFr
	, Convert(nvarchar(500), null) VCAFr
	, Convert(nvarchar(500), null) ClientFr
	, Convert(nvarchar(max), null) ViaPoints
	, Convert(nvarchar(500), null) ZoneTo
	, Convert(nvarchar(500), null) VCATo
	, Convert(nvarchar(500), null) ClientTo
	, Convert(nvarchar(500), null) Departure
	, Convert(nvarchar(500), null) DepartureID
	, Convert(nvarchar(500), null) Arrival
    , Convert(nvarchar(500), null) ArrivalID
    , Convert(nvarchar(500), null) ProcessedBy
	, GetDate()  ProccessedDate
    , Convert(nvarchar(500), null) CoordinatesFrom
	, Convert(nvarchar(500), null) CoordinatesTo
into #Planning from GFI_PLN_Planning p
    inner join GFI_PLN_PlanningViaPointH h on h.PID = p.PID
	inner join GFI_SYS_GroupMatrixPlanning m on p.PID = m.iID";


            if (PID.HasValue)
                sql += @" where h.PID = " + PID.Value;
            else
            {
        

                sql += @" where m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + @")
    and h.dtUTC >= '" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "' and h.dtUTC <= '" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff") + @"'--and p.GMApprovalValue = 100
order by " + sSort + @" 

";
            }




            sql += @" 
update #Planning 
	set dtFrom = h.dtUTC
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
where h.SeqNo = 1

update #Planning 
	set dtTo = h.dtUTC
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
where h.SeqNo = 999

update #Planning 
	set ZoneFr = z.Description ,DepartureID =z.ZoneID
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_FLT_ZoneHeader z on h.ZoneID = z.ZoneID 
where h.SeqNo = 1

update #Planning 
	set ZoneTo = z.Description ,ArrivalID =z.ZoneID
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_FLT_ZoneHeader z on h.ZoneID = z.ZoneID 
where h.SeqNo = 999

update #Planning 
	set CoordinatesFrom = concat(zh.Latitude ,',',zh.Longitude)
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_FLT_ZoneHeader z on h.ZoneID = z.ZoneID 
	inner join GFI_FLT_ZoneDetail zh on z.ZoneID = zh.ZoneID 
where h.SeqNo = 1


update #Planning 
	set CoordinatesTo = concat(zh.Latitude ,',',zh.Longitude)
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_FLT_ZoneHeader z on h.ZoneID = z.ZoneID
	inner join GFI_FLT_ZoneDetail zh on z.ZoneID = zh.ZoneID  
where h.SeqNo = 999

update #Planning 
	set VCAFr = v.VcaName
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_GIS_VCA v on h.LocalityVCA = v.VCAID 
where h.SeqNo = 1

update #Planning 
	set VCATo = v.VcaName
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_GIS_VCA v on h.LocalityVCA = v.VCAID 
where h.SeqNo = 999

update #Planning 
	set CoordinatesFrom = concat(vD.Latitude ,',',vD.Longitude)
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_FLT_VCAHeader v on h.LocalityVCA = v.VCAID 
	inner join GFI_FLT_VCADetail vD on v.VCAID = vD.VCAID 
where h.SeqNo = 1


update #Planning 
	set CoordinatesTo =concat(vD.Latitude ,',',vD.Longitude)
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_FLT_VCAHeader v on h.LocalityVCA = v.VCAID 
	inner join GFI_FLT_VCADetail vD on v.VCAID = vD.VCAID 
where h.SeqNo = 999

update #Planning 
	set ClientFr = c.Name
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_SYS_CustomerMaster c on h.ClientID = c.ClientID 
where h.SeqNo = 1

update #Planning 
	set ClientTo = c.Name
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_SYS_CustomerMaster c on h.ClientID = c.ClientID 
where h.SeqNo = 999

update #Planning 
	set Departure = CONVERT(nvarchar(15), h.Lat) + ', ' + CONVERT(nvarchar(15), h.Lon)
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
where h.SeqNo = 1 and h.Lat is not null

update #Planning set Departure = ZoneFr where Departure is null

update #Planning set Departure = VCAFr where Departure is null

update #Planning set Departure = ClientFr where Departure is null

update #Planning 
	set Arrival = CONVERT(nvarchar(15), h.Lat) + ', ' + CONVERT(nvarchar(15), h.Lon)
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
where h.SeqNo = 999 and h.Lat is not null


update #Planning 
     set ProccessedDate = h.dtUTC
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
where h.SeqNo = 1

update #Planning
     set ProcessedBy =  u.Names
	 from  #Planning p 
	 inner join GFI_SYS_USER u on u.UID = p.CreatedBy

update #Planning set Arrival = ZoneTo where Arrival is null

update #Planning set Arrival = VCATo where Arrival is null

update #Planning set Arrival = ClientTo where Arrival is null

select * from #Planning order by " + sSort + @"
";
            return sql;
        }

        public DataTable GetPlanningActual(DateTime dtFrom, DateTime dtTo, List<int> lAssetIds, int? PID, String sConnStr)
        {
            DataTable dt = new Connection(sConnStr).GetDataDT(sGetPlanningActual(dtFrom, dtTo, lAssetIds, PID, sConnStr), sConnStr);
            return dt;
        }
        public String sGetPlanningActual(DateTime dtFrom, DateTime dtTo, List<int> lAssetIds, int? PID, String sConnStr)
        {
            String sql = @"
--GetPlanningActual

select    scheduled.HID as ScheduledID
        , planning.PID
		, viaPointH.SeqNo
        , viaPointH.Remarks
		, viaPointH.ZoneID
		, zoneHeader.Description as ZoneName
        , zoneHeader.NoOfHousehold as NoOfHousehold
        , zoneHeader.ExternalRef as ZoneDistanceCovered
		, viaPointH.LocalityVCA as VcaId
        , vcaHeader.VCAName as VcaName
		, concat(viaPointH.Lat ,',',viaPointH.Lon) as coordinates
        , tz.TotalMinutes as TimeZone
		, DATEADD(mi, tz.TotalMinutes, srp.dtUTC) as dtUTC
        , viaPointH.Buffer
		, DATEADD(mi, -viaPointH.Buffer,  DATEADD(mi, tz.TotalMinutes, srp.dtUTC)) as minTime
		, DATEADD(mi, viaPointH.Buffer,  DATEADD(mi, tz.TotalMinutes, srp.dtUTC)) as maxTime
		, asset.AssetID
		, asset.AssetName as AssetName
		, driver.DriverID
		, driver.sDriverName as DriverName
        , viaPointH.minDuration
		, viaPointH.maxDuration
from GFI_PLN_Scheduled scheduled
          left join  GFI_PLN_Scheduled matrixscheduled on scheduled.HID = matrixScheduled.HID
          left join GFI_PLN_ScheduledAsset scheduledAsset on scheduled.HID = scheduledAsset.HID
		  left join GFI_FLT_Driver driver on scheduledAsset.DriverID =driver.DriverID
		  left join GFI_FLT_Asset asset on scheduledAsset.AssetID =asset.AssetID
          left join GFI_PLN_ScheduledPlan scheduledPlan on scheduled.HID = scheduledPlan.HID
          left join GFI_PLN_Planning planning on scheduledPlan.PID = planning.PID
		  left join GFI_PLN_PlanningViaPointH viaPointH on planning.PID = viaPointH.PID
		  left join GFI_FLT_ZoneHeader zoneHeader  on viaPointH.ZoneID = zoneHeader.ZoneID
		  left join GFI_GIS_VCA vcaHeader  on viaPointH.LocalityVCA = vcaHeader.VCAID
		  left join  GFI_PLN_PlanningViaPointD viaPointD on viaPointH.iID = viaPointD.iID
          inner join gfi_pln_scheduledrouteviapoints srp on srp.ViaPointHId = viaPointH.iID
          inner join GFI_SYS_TimeZones tZ on tZ.StandardName = asset.TimeZoneID";
            if (PID.HasValue) {
                sql += @" where planning.PID = " + PID.Value;
            }
            else
            {
                string AssetID = string.Empty;
                foreach (var i in lAssetIds)
                    AssetID += i.ToString() + ",";
                AssetID = AssetID.TrimEnd(',');
                sql += @"
                where scheduledAsset.AssetID in (" + AssetID + @") 
    		        and  srp.dtUTC >= '" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "' and srp.dtUTC <= '" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff") + @"' 
                order by viaPointH.SeqNo asc";
            } 


            return sql;
        }

        public DataTable GetPlanningViaPoints(int Pid, String sConnStr)
        {
            DataTable dt = new Connection(sConnStr).GetDataDT(sPlanningViaPoints(Pid, sConnStr), sConnStr);
            return dt;
        }
        public String sPlanningViaPoints(int PID, String sConnStr)
        {
            String sql = @"
select  viaPoint.PID ,
        scheduledplan.HID,
        viaPoint.SeqNo ,
        viaPoint.ZoneID,
		zoneHeader.Description as ZoneName,
		CONCAT(zoneDetail.Latitude,',', zoneDetail.Longitude) as zoneCoordinates,
        viaPoint.LocalityVCA as VcaId,
		vcaHeader.Name as VcaName,
        viaPoint.Lat,
        viaPoint.Lon
from  GFI_PLN_Planning planning
 left join GFI_PLN_PlanningViaPointH viaPoint on planning.PID = viaPoint.PID
 left join GFI_FLT_ZoneHeader zoneHeader on zoneHeader.ZoneID = viaPoint.ZoneID
 left join GFI_FLT_ZoneDetail zoneDetail on zoneDetail.ZoneID = zoneHeader.ZoneID
 left join GFI_FLT_VCAHeader vcaHeader on vcaHeader.VCAID = viaPoint.LocalityVCA
 left join GFI_PLN_ScheduledPlan scheduledplan on planning.PID = scheduledplan.PID
 where planning.PID = " + PID + @"  and zoneDetail.CordOrder = 0
 order by viaPoint.SeqNo asc;
";
            return sql;

        }


        public DataTable GetScheduledPlannedViaPoints(List<int> lPids, String sConnStr)
        {
            DataTable dt = new Connection(sConnStr).GetDataDT(sScheduledPlannedViaPoints(lPids, sConnStr), sConnStr);
            return dt;
        }
        public String sScheduledPlannedViaPoints(List<int> lpids, String sConnStr)
        {
            string PID = string.Empty;
            foreach (var i in lpids)
                PID += i.ToString() + ",";
            PID = PID.TrimEnd(',');


    
            String sql = @"
select 
    vp.iID as viapointid
  ,  vp.SeqNo as seqno
  , vp.PID as pid
  , vp.ZoneID as zoneid
  , z.description as zonename
  , vp.localityvca as vcaId
  , vp.dtUTC as viapointTime
  , v.name as vca 
  , p.RequestCode as requestcode 
  , concat(vp.lat, ',' ,vp.lon) as coords  from GFI_PLN_Planning p
    inner join GFI_PLN_PlanningViaPointH vp on p.PID = vp.PID
    left join GFI_FLT_ZoneHeader z on  z.ZoneID = vp.ZoneID
    left join GFI_FLT_VCAHeader v on  v.VCAID = vp.localityvca
 where vp.PID in  (" + PID + ") order by vp.SeqNo asc;";

            return sql;

        }


        public dtData GetTripDetails(DateTime dtFrom, DateTime dtTo, int assetId, String sConnStr)
        {
            DataTable dt = new Connection(sConnStr).GetDataDT(sTripDetails(dtFrom, dtTo, assetId, sConnStr), sConnStr);

            dtData dtD = new dtData();
            dtD.Data = dt; ;
            dtD.Rowcnt = dt.Rows.Count;

            return dtD;
        }
        public String sTripDetails(DateTime dtFrom, DateTime dtTo, int assetId, String sConnStr)
        {
            String sql = @"
  SELECT  gpsData.UID
        , asset.AssetName
		, gpsData.AssetID 
		, DATEADD(mi, timeZones.TotalMinutes, gpsData.DateTimeGPS_UTC) dtLocalTime ,gpsData.Latitude,gpsData.Longitude
		, gpsData.TripDistance
		, gpsData.TripTime
		, gpsData.Speed
  from GFI_FLT_Asset asset
  inner join GFI_SYS_TimeZones timeZones on asset.TimeZoneID = timeZones.StandardName
  inner join GFI_GPS_GPSData gpsData on gpsData.AssetID = asset.AssetID
  where gpsData.AssetID = " + assetId + @"  and LongLatValidFlag = 1
  and gpsData.DateTimeGPS_UTC >= '" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "' and gpsData.DateTimeGPS_UTC <= '" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff") + @"'
";
            return sql;
        }

        public Boolean UpdatePlanningTableForApproval(Planning uPlanningNew, int UID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "UPDATE GFI_PLN_Planning set ApprovalID  = " + uPlanningNew.ApprovalID.ToString() + " where PID =" + uPlanningNew.PID.ToString();
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();



            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }

        public String sendPLanningMail(int UID,String emailAddress,String Type,String Title,String Body,String sConnStr)
        {
            MailSender ms = new MailSender();
            String sMail = String.Empty;
            
            sMail = ms.SendMail(emailAddress, Title, Body, String.Empty, sConnStr);
            Boolean s = new NotificationService().PrepareDataNotif(UID, DateTime.Now, sMail, Body, Type, Title, sConnStr);
            if (sMail == "Mail Sent")
                return sMail;

            return sMail;
        }

        public DataTable GetPlanningResourceDetailsForApproval(int DriverID, String sConnStr)
        {
            DataTable dt = new Connection(sConnStr).GetDataDT(sGetPlanningResourceDetailsForApproval(DriverID, sConnStr), sConnStr);

            if (dt == null)
                return null;
            else
                return dt;
        }
        public String sGetPlanningResourceDetailsForApproval(int DriverID, String sConnStr)
        {
            String sql = @"
            SELECT 
                            resource.PID 
                          , driver.DriverID 
                          , driver.sDriverName
                          , driver.Email 
                          , resource.Remarks as Level 
           FROM GFI_FLT_Driver driver 
           INNER JOIN GFI_PLN_PlanningResource resource ON driver.DriverID = resource.DriverID
           FROM driver.DriverID = " + DriverID + @"";

            return sql;
        }

        #region Terra
        public BaseModel SaveTerraPlanning(List<Planning> lPlanning, List<DateTime> lDates, int UID, Boolean bInsert, String sConnStr)
        {
            BaseModel baseModel = new BaseModel();
            Boolean bResult = false;

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            DataSet dsProcess = new DataSet();
            String sql = String.Empty;
            string sPids = string.Empty;

            NaveoOneLib.Models.Plannings.Planning planningOLD = new NaveoOneLib.Models.Plannings.Planning();
            NaveoOneLib.Models.Plannings.Planning planningNew = new NaveoOneLib.Models.Plannings.Planning();

            int iRecurrent = 0;
            foreach (var uPlanning in lPlanning)
            {

                if (lDates == null || lDates.Count == 0)
                {
                    lDates = new List<DateTime>();
                    if (uPlanning.lPlanningViaPointH.Count > 0)
                        lDates.Add(uPlanning.lPlanningViaPointH[0].dtUTC.Value);
                }

                int ApprovalID = new NaveoOneLib.Services.Users.UserService().GetUserApprovalProcessIdByUserID(UID, sConnStr);
                int ApprovalValue = new NaveoOneLib.Services.Users.UserService().GetUserApprovalProcessValueByUserID(UID, sConnStr);
                uPlanning.ApprovalID = ApprovalID;


                iRecurrent += 1000;

                //GFI_PLN_Planning
                DataTable dt = PlanningDT();
                dt.Columns.Add("OPRSeqNo", typeof(int));

                DataRow dr = dt.NewRow();
                dr["PID"] = uPlanning.PID;
                dr["Approval"] = uPlanning.Approval != null ? uPlanning.Approval : (object)DBNull.Value;
                dr["Remarks"] = String.IsNullOrEmpty(uPlanning.Remarks) ? (object)DBNull.Value : uPlanning.Remarks;
                dr["oprType"] = uPlanning.OprType;
                if (bInsert)
                {
                    uPlanning.CreatedDate = DateTime.UtcNow;
                    uPlanning.CreatedBy = UID;
                    uPlanning.UpdatedDate = Constants.NullDateTime;
                }
                else
                {
                    uPlanning.UpdatedDate = DateTime.UtcNow;
                    uPlanning.UpdatedBy = UID;
                }

                dr["urgent"] = uPlanning.urgent;
                dr["type"] = String.IsNullOrEmpty(uPlanning.type) ? (object)DBNull.Value : uPlanning.type;
                dr["sComments"] = String.IsNullOrEmpty(uPlanning.sComments) ? (object)DBNull.Value : uPlanning.sComments;
                dr["closureComments"] = String.IsNullOrEmpty(uPlanning.closureComments) ? (object)DBNull.Value : uPlanning.closureComments;
                dr["Ref"] = String.IsNullOrEmpty(uPlanning.Ref) ? (object)DBNull.Value : uPlanning.Ref;
                dr["requestCode"] = String.IsNullOrEmpty(uPlanning.requestCode) ? (object)DBNull.Value : uPlanning.requestCode;
                dr["parentRequestCode"] = String.IsNullOrEmpty(uPlanning.parentRequestCode) ? (object)DBNull.Value : uPlanning.parentRequestCode;
                dr["ApprovalID"] = uPlanning.ApprovalID;


                dr["CreatedDate"] = uPlanning.CreatedDate == null ? (object)DBNull.Value : uPlanning.CreatedDate;
                if (uPlanning.CreatedBy != null)
                    dr["CreatedBy"] = uPlanning.CreatedBy;

                dr["UpdatedDate"] = uPlanning.UpdatedDate == null ? (object)DBNull.Value : uPlanning.UpdatedDate;
                if (uPlanning.UpdatedBy != null)
                    dr["UpdatedBy"] = uPlanning.UpdatedBy;

                dr["OPRSeqNo"] = 1 + iRecurrent;
                dt.Rows.Add(dr);

                if (bInsert)
                    dt.Columns.Remove("UpdatedDate");
                else
                    dt.Columns.Remove("CreatedDate");

                if (bInsert)
                {

                    sql = @"exec sp_MOHRequestCode 'TER'";

                    drCtrl = dtCtrl.NewRow();
                    drCtrl["SeqNo"] = 1;
                    drCtrl["ParentTblName"] = dt.TableName;
                    drCtrl["KeyFieldName"] = "RequestCode";
                    drCtrl["NextIdAction"] = "GetFromSP";
                    drCtrl["TimeOut"] = 1000;
                    drCtrl["Command"] = sql;
                    drCtrl["SeqNo"] = 1 + iRecurrent;
                    dtCtrl.Rows.Add(drCtrl);

                    if (iRecurrent == 1000)
                    {
                        drCtrl = dtCtrl.NewRow();
                        drCtrl["SeqNo"] = 2 + iRecurrent;
                        drCtrl["ParentTblName"] = dt.TableName;
                        drCtrl["NextIdAction"] = "GetFromField";
                        drCtrl["KeyFieldName"] = "RequestCode";
                        drCtrl["NextIdFieldName"] = "ParentRequestCode";
                        drCtrl["TimeOut"] = 1000;
                        drCtrl["SeqNo"] = 1 + iRecurrent;
                        dtCtrl.Rows.Add(drCtrl);
                    }



                }

                //GFI_PLN_Planning
                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 3 + iRecurrent;
                drCtrl["TblName"] = dt.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "PID";
                drCtrl["KeyIsIdentity"] = "TRUE";
                if (bInsert)
                    drCtrl["NextIdAction"] = "GET";
                else
                    drCtrl["NextIdValue"] = uPlanning.PID;
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dt);

                //GFI_SYS_GroupMatrixPlanning
                if (uPlanning.lMatrix == null || uPlanning.lMatrix.Count == 0)
                    uPlanning.lMatrix = new MatrixService().GetMatrixByUserID(UID, sConnStr);
                DataTable dtMatrix = new myConverter().ListToDataTable<Matrix>(uPlanning.lMatrix);
                dtMatrix.TableName = "GFI_SYS_GroupMatrixPlanning";
                dtMatrix.Columns.Add("OPRSeqNo", typeof(int));
                foreach (DataRow r in dtMatrix.Rows)
                    r["OPRSeqNo"] = 3 + iRecurrent;
                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 4 + iRecurrent;
                drCtrl["TblName"] = dtMatrix.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "MID";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdAction"] = "SET";
                drCtrl["NextIdFieldName"] = "iID";
                drCtrl["ParentTblName"] = dt.TableName;
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtMatrix);

                //GFI_PLN_PlanningLkup
                if (uPlanning.lPlanningLkup != null)
                    if (uPlanning.lPlanningLkup.Count > 0)
                    {
                        DataTable dtLKup = new myConverter().ListToDataTable<PlanningLkup>(uPlanning.lPlanningLkup);
                        dtLKup.TableName = "GFI_PLN_PlanningLkup";
                        dtLKup.Columns.Add("OPRSeqNo", typeof(int));
                        foreach (DataRow r in dtLKup.Rows)
                            r["OPRSeqNo"] = 3 + iRecurrent;
                        drCtrl = dtCtrl.NewRow();
                        drCtrl["SeqNo"] = 5 + iRecurrent;
                        drCtrl["TblName"] = dtLKup.TableName;
                        drCtrl["CmdType"] = "TABLE";
                        drCtrl["KeyFieldName"] = "iID";
                        drCtrl["KeyIsIdentity"] = "TRUE";
                        drCtrl["NextIdAction"] = "SET";
                        drCtrl["NextIdFieldName"] = "PID";
                        drCtrl["ParentTblName"] = dt.TableName;
                        dtCtrl.Rows.Add(drCtrl);
                        dtLKup.Columns.Remove("LookupValueDesc");
                        dsProcess.Merge(dtLKup);
                    }

                //GFI_PLN_PlanningResource
                if (uPlanning.lPlanningResource != null)
                    if (uPlanning.lPlanningResource.Count > 0)
                    {
                        foreach (PlanningResource p in uPlanning.lPlanningResource)
                            if (!p.iSource.HasValue)
                                p.iSource = 1;

                        DataTable dtResource = new myConverter().ListToDataTable<PlanningResource>(uPlanning.lPlanningResource);
                        dtResource.TableName = "GFI_PLN_PlanningResource";
                        dtResource.Columns.Add("OPRSeqNo", typeof(int));
                        foreach (DataRow r in dtResource.Rows)
                            r["OPRSeqNo"] = 3 + iRecurrent;
                        drCtrl = dtCtrl.NewRow();
                        drCtrl["SeqNo"] = 6 + iRecurrent;
                        drCtrl["TblName"] = dtResource.TableName;
                        drCtrl["CmdType"] = "TABLE";
                        drCtrl["KeyFieldName"] = "iID";
                        drCtrl["KeyIsIdentity"] = "TRUE";
                        drCtrl["NextIdAction"] = "SET";
                        drCtrl["NextIdFieldName"] = "PID";
                        drCtrl["ParentTblName"] = dt.TableName;
                        dtCtrl.Rows.Add(drCtrl);
                        dtResource.Columns.Remove("DriverName");
                        dsProcess.Merge(dtResource);
                    }

                //GFI_PLN_PlanningViaPointH
                if (uPlanning.lPlanningViaPointH != null)
                    if (uPlanning.lPlanningViaPointH.Count > 0)
                    {
                        for (int i = 0; i < uPlanning.lPlanningViaPointH.Count; i++)
                        {
                            DateTime dtFrom = lDates[0].Date;
                            dtFrom = dtFrom.Add(uPlanning.lPlanningViaPointH[i].dtUTC.Value.ToUniversalTime().TimeOfDay);
                            uPlanning.lPlanningViaPointH[i].dtUTC = dtFrom;

                            uPlanning.lPlanningViaPointH[i].SeqNo = i + 1;
                            if (i == uPlanning.lPlanningViaPointH.Count - 1)
                                uPlanning.lPlanningViaPointH[i].SeqNo = 999;
                        }
                        DataTable dtViaH = new myConverter().ListToDataTable<PlanningViaPointH>(uPlanning.lPlanningViaPointH);
                        dtViaH.TableName = "GFI_PLN_PlanningViaPointH";
                        dtViaH.Columns.Remove("lDetail");
                        dtViaH.Columns.Add("OPRSeqNo", typeof(int));
                        foreach (DataRow r in dtViaH.Rows)
                            r["OPRSeqNo"] = 3 + iRecurrent;
                        dtViaH.Columns.Remove("ZoneDesc");
                        dtViaH.Columns.Remove("LocalityVCADesc");
                        dsProcess.Merge(dtViaH);

                        foreach (PlanningViaPointH h in uPlanning.lPlanningViaPointH)
                        {
                            //Get iID
                            drCtrl = dtCtrl.NewRow();
                            drCtrl["SeqNo"] = 7 + iRecurrent + h.SeqNo;
                            drCtrl["TblName"] = dtViaH.TableName;
                            drCtrl["CmdType"] = "TABLE";
                            drCtrl["KeyFieldName"] = "iID";
                            drCtrl["KeyIsIdentity"] = "TRUE";
                            if (bInsert)
                                drCtrl["NextIdAction"] = "GET";
                            dtCtrl.Rows.Add(drCtrl);

                            //Set PID
                            drCtrl = dtCtrl.NewRow();
                            drCtrl["SeqNo"] = 7 + iRecurrent;
                            drCtrl["TblName"] = dtViaH.TableName;
                            drCtrl["CmdType"] = "TABLE";
                            drCtrl["KeyFieldName"] = "iID";
                            drCtrl["KeyIsIdentity"] = "TRUE";
                            drCtrl["NextIdAction"] = "SET";
                            drCtrl["NextIdFieldName"] = "PID";
                            drCtrl["ParentTblName"] = dt.TableName;
                            dtCtrl.Rows.Add(drCtrl);

                            //GFI_PLN_PlanningViaPointD
                            DataTable dtViaD = new myConverter().ListToDataTable<PlanningViaPointD>(h.lDetail);
                            dtViaD.TableName = "GFI_PLN_PlanningViaPointD";
                            dtViaD.Columns.Add("OPRSeqNo", typeof(int));
                            foreach (DataRow r in dtViaD.Rows)
                                r["OPRSeqNo"] = 7 + iRecurrent + h.SeqNo;

                            drCtrl = dtCtrl.NewRow();
                            drCtrl["SeqNo"] = 9 + iRecurrent;
                            drCtrl["TblName"] = dtViaD.TableName;
                            drCtrl["CmdType"] = "TABLE";
                            drCtrl["KeyFieldName"] = "iID";
                            drCtrl["KeyIsIdentity"] = "TRUE";
                            drCtrl["NextIdAction"] = "SET";
                            drCtrl["NextIdFieldName"] = "HID";
                            drCtrl["ParentTblName"] = dtViaH.TableName;
                            dtCtrl.Rows.Add(drCtrl);
                            dtViaD.Columns.Remove("uomDesc");
                            dsProcess.Merge(dtViaD);
                        }
                    }


     
            }


            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResult.Tables["CTRLTBL"];
            DataRow[] dRowAudit = dtCtrl.Select("TblName ='GFI_PLN_Planning'");
            foreach (DataRow dr in dRowAudit)
            {


                int PID = Convert.ToInt32(dr["NextIdValue"]);
                string pids = PID.ToString();

                sPids += pids + ",";
                

            }

            sPids = sPids.Remove(sPids.Length - 1, 1);



            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                {
                    baseModel.dbResult = dsResult;
                    baseModel.dbResult.Tables["CTRLTBL"].TableName = "ResultCtrlTbl";
                    baseModel.dbResult.Merge(dsProcess);
                    bResult = false;
                }
                else
                    bResult = true;
            }
            else
                bResult = false;

            baseModel.data = bResult;
            baseModel.errorMessage = sPids;
            //baseModel.total = planningNew.PID;
            return baseModel;
        }
        public DataTable GetPlanningByParentRequestCode(string ParentRequestCode, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataSet ds = new DataSet();

            String sql1 = @"
SELECT 
     planning.ParentRequestCode,planning.RequestCode,planning.PID ,planning.Remarks,planning.Type,planning.Approval,planning.ApprovalID,
     viaH.iID as  viaHiID,viaH.PID,viaH.SeqNo,viaH.ZoneID ,z.Description,viaH.dtUTC,viaH.Buffer,viaH.Remarks as Soil_Type,
     viaD.iID as viaDiID,viaD.HID,viaD.Remarks as SoilType ,viaD.UOM as AreaInHectaresID  ,viaD.Remarks as AreaInHectares,
     Scheduled.HID as ScheduledID ,
     schPlan.iID as schPlaniID  , schPlan.HID as schPlanHID ,schPlan.PID as schPlanPID,
    schAsset.iID as ScheduledAssetiID ,schAsset.HID as ScheduledAssetHID ,schAsset.AssetID , asset.AssetName ,schAsset.DriverID ,driver.sDriverName
FROM 
GFI_PLN_Planning planning
         INNER JOIN GFI_PLN_PlanningViaPointH viaH 
		    on viaH.PID  =planning.PID
         LEFT JOIN GFI_PLN_PlanningViaPointD viaD
		    on viaD.HID = viaH.iID
		 LEFT JOIN GFI_SYS_UOM uom 
		    on uom.UID =viaD.UOM
		 INNER JOIN GFI_FLT_ZoneHeader z
		   on z.ZoneID = viaH.ZoneID
		 LEFT JOIN GFI_PLN_ScheduledPlan schPlan
		   on schPlan.PID = planning.PID
		 LEFT JOIN GFI_PLN_Scheduled Scheduled
		   on Scheduled.HID =schPlan.HID
		 LEFT JOIN GFI_PLN_ScheduledAsset schAsset 
		   on schAsset.HID =schPlan.HID
		LEFT JOIN GFI_FLT_Asset asset 
		   on asset.AssetID = schAsset.AssetID
		LEFT JOIN GFI_FLT_Driver driver 
		 on driver.DriverID =schAsset.DriverID
WHERE planning.ParentRequestCode ='" + ParentRequestCode + "' and planning.Type ='200'";

            DataTable dtPlanning = c.GetDataDT(sql1, sConnStr);
            dtPlanning.TableName = "dtPlanning";

            return dtPlanning;
        }
        public DataTable GetPlanningLookupByParentRequestCode(string ParentRequestCode, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataSet ds = new DataSet();

            String sql1 = @"
SELECT 
plnLkp.* ,lkpValues.Description as LookUpValue ,lkpTypes.Code as LookupTypes
FROM 
GFI_PLN_Planning planning
         INNER JOIN GFI_PLN_PlanningLkup plnLkp 
		    on plnLkp.PID  =planning.PID
         INNER JOIN GFI_SYS_LookUpValues lkpValues
		    on lkpValues.VID = plnLkp.LookupValueID
		 INNER JOIN GFI_SYS_LookUpTypes lkpTypes
		   on lkpTypes.TID = lkpValues.TID
WHERE planning.ParentRequestCode ='" + ParentRequestCode + "' and planning.Type ='200'";

            DataTable dtPlanninglkup = c.GetDataDT(sql1, sConnStr);
            dtPlanninglkup.TableName = "dtPlanninglkup";

      
            return dtPlanninglkup;
        }
        public dtData GetFieldCode(int ZoneID, String sConnStr)
        {
            String sql = @"
           select ZoneID,Description as FieldCode,ExternalRef as AreaInHectare,Ref2 as Perimeter, LookUpValue as SoilType from GFI_FLT_ZoneHeader
           where ZoneID = " + ZoneID + @"";

            DataTable dt = new Connection(sConnStr).GetDataDT(sql, sConnStr);
            dtData mydata = new dtData();
            mydata.Data = dt;
            mydata.Rowcnt = dt.Rows.Count;

            if (mydata == null)
                return null;
            else
                return mydata;
        }

        #endregion








        #region planning v/s acutal

        public static int GetUserID(Guid sUserToken, String sConnStr)
        {
            int UID = new NaveoOneLib.Services.Users.UserService().GetUserID(sUserToken, sConnStr);
            return UID;
        }


        public dtData GetPlanningActual(Guid sUserToken, List<Matrix> lMatrix, DateTime dtFrom, DateTime dtTo, List<int> lAssetIds, int? PID, string sConnStr)
        {
            dtData dtR = new dtData();

            List<Matrix> lm = new List<Matrix>();

            if (lMatrix.Count > 0 && lMatrix != null)
            {
                lm = lMatrix;
            } else
            {
                int UID = GetUserID(sUserToken, sConnStr);
                lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);
            }


            DataTable dtPlanning = new NaveoOneLib.Services.Plannings.PlanningService().GetPlanningActual(dtFrom, dtTo, lAssetIds, PID, sConnStr);
            dtPlanning.Columns.Add("Color");
            dtPlanning.Columns.Add("Duration");
            dtPlanning.Columns.Add("TimeEnteredZone");
            dtPlanning.Columns.Add("Status");
            dtPlanning.Columns.Add("Time");
            dtPlanning.Columns.Add("Concatcolor");
            dtPlanning.Columns.Add("NoOfUnplannedStops");
            dtPlanning.Columns.Add("TotalDurationOfStops");

            if (dtPlanning.Rows.Count == 0)
            {
                dtR.Data = null;
                return dtR;
            }

            int rows = dtPlanning.Rows.Count;
            DataView view = new DataView(dtPlanning);
            DataTable distinctValues = view.ToTable(true, "ScheduledID");
            try
            {
                foreach (DataRow dr in distinctValues.Rows)
                {
                    var rowColl = dtPlanning.AsEnumerable();
                    int scheduleid = Convert.ToInt32(dr["ScheduledID"].ToString());
                    var drSelectedRows = rowColl.Where(r => r.Field<int>("ScheduledID") == scheduleid);

                    foreach (DataRow drPlanning in drSelectedRows)
                    {
                        DateTime dtUTC = Convert.ToDateTime(drPlanning["dtutc"].ToString());

                        drPlanning["dtutc"] = Convert.ToDateTime(drPlanning["dtutc"].ToString());
                        drPlanning["minTime"] = Convert.ToDateTime(drPlanning["minTime"].ToString());
                        drPlanning["maxTime"] = Convert.ToDateTime(drPlanning["maxTime"].ToString());
                        string color = string.Empty;
                        string message = string.Empty;
                        string timeConcat = string.Empty;
                        //string Concatcolor = string.Empty;

                        #region never visited
                        drPlanning["Color"] = "Red";
                        drPlanning["Duration"] = "0, 00:00:00";
                        drPlanning["TimeEnteredZone"] = null;
                        //message = ", Not Visited";
                        //timeConcat = ", 0, 00:00:00";
                        //color = ", Red";
                        #endregion

                        #region Zone or Vca Determination 
                        //This region determines whether is a zone or vca
                        // assign zone name ,vca name or zone id or vca id to a global variable i.e ZoneVcaName , VcaZoneId to cater for planning v/s actual

                        DataSet ds = new DataSet();
                        string NotVcaORZone = string.Empty;
                        string ZoneVcaName = string.Empty;
                        int VcaZoneId = 0;

                        if (drPlanning["ZoneName"].ToString() != string.Empty)
                        {
                            NotVcaORZone = "Zones";
                            ZoneVcaName = drPlanning["ZoneName"].ToString();
                            VcaZoneId = Convert.ToInt32(drPlanning["ZoneID"]);
                        }

                        if (drPlanning["VcaName"].ToString() != string.Empty)
                        {
                            NotVcaORZone = "VCA";
                            ZoneVcaName = drPlanning["VcaName"].ToString();
                            VcaZoneId = Convert.ToInt32(drPlanning["VcaId"]);
                        }
                        #endregion

                        #region Planning V/S Actual logic 
                        //planning v/s actual logic 
                        if (ZoneVcaName != string.Empty)
                        {
                            drPlanning["ZoneName"] = ZoneVcaName;

                            List<int> zORvIds = new List<int>();
                            zORvIds.Add(VcaZoneId);

                            List<int> lAssetId = new List<int>();
                            lAssetId.Add((int)drPlanning["AssetID"]);
                            lAssetId = lAssetId.Distinct().ToList();

                            if (NotVcaORZone == "VCA")
                                ds = new NaveoOneLib.Services.GPS.IdleService().getIdlesForVCA(lAssetId, dtFrom, dtTo, "Both", 0, 50, true, NotVcaORZone, zORvIds, lm, false, sConnStr, null, null);

                            if (NotVcaORZone == "Zones")
                                ds = new NaveoOneLib.Services.GPS.IdleService().getIdles(lAssetId, dtFrom, dtTo, "Both", 0, 50, true, NotVcaORZone, zORvIds, lm, false, sConnStr, null, null);

                            if (ds.Tables["idles"].Rows.Count != 0)
                            {
                                #region Check nearest time from arrival and departure visited time based on planning scheduled datetime
                                //Check nearest time from arrival visited time based on planning datetime
                                List<DateTime> LArrivalZoneBeforeTime = new List<DateTime>();
                                List<DateTime> LDepartureTime = new List<DateTime>();

                                foreach (DataRow x in ds.Tables["idles"].Rows)
                                {
                                    DateTime dateTime = Convert.ToDateTime(x["dtLocal"].ToString());
                                    //DateTime dateTimeddd = Convert.ToDateTime(x["DateTimeGPS_UTC"].ToString()).ToLocalTime();
                                    TimeSpan time = Convert.ToDateTime(x["dtLocalTime"]).TimeOfDay;
                                    DateTime dtLocalTime = dateTime.Date.Add(time);

                                    DateTime arrivaldate = Convert.ToDateTime(x[22].ToString());
                                    DateTime departuredate = dtLocalTime;
                                    LArrivalZoneBeforeTime.Add(arrivaldate);
                                    LDepartureTime.Add(departuredate);
                                }

                                var closestArrivalTime = (from t in LArrivalZoneBeforeTime
                                                          orderby (t - dtUTC).Duration()
                                                          select t
                                                          ).First();

                                var closestDepartureTime = (from t in LDepartureTime
                                                            orderby (t - dtUTC).Duration()
                                                            select t
                                                            ).First();

                                var sDurationForArrival = (from r in ds.Tables["idles"].AsEnumerable()
                                                           where r.Field<DateTime>("dtLocalFrom") == closestArrivalTime
                                                           select r.Field<string>("sDuration")
                                                           ).First().ToString();


                                var sDurationForDeparture = (from r in ds.Tables["idles"].AsEnumerable()
                                                             where r.Field<DateTime>("DateTimeGPS_UTC").ToLocalTime() == closestDepartureTime
                                                             select r.Field<string>("sDuration")
                                                             ).First().ToString();

                                sDurationForArrival = sDurationForArrival.Replace("Day(s)", "");
                                sDurationForDeparture = sDurationForDeparture.Replace("Day(s)", "");
                                #endregion

                                #region Early
                                //Early Arrival
                                 if (closestArrivalTime <= Convert.ToDateTime(drPlanning["minTime"]))
                                {
                                    color = "#00b300";
                                    drPlanning["Color"] = "#00b300";
                                    drPlanning["Duration"] = sDurationForArrival;
                                    drPlanning["TimeEnteredZone"] = closestArrivalTime;
                                    //message = string.Empty;
                                    //timeConcat = string.Empty;
                                    message += ",Early Arrival";
                                    timeConcat += "," + closestArrivalTime;
                                    //Concatcolor += ", #00b300";
                                }

                                //Early Departure
                                if (closestDepartureTime <= Convert.ToDateTime(drPlanning["minTime"]))
                                {
                                    color = "#00b300";
                                    drPlanning["Color"] = "#00b300";
                                    drPlanning["Duration"] = sDurationForDeparture;
                                    drPlanning["TimeEnteredZone"] = closestDepartureTime;
                                    //message = string.Empty;
                                    //timeConcat = string.Empty;
                                    message += ",Early Departure";
                                    timeConcat += "," + closestDepartureTime;
                                    //Concatcolor += ", Green";
                                }
                                #endregion

                                #region Late
                                //Late Arrival
                                if (closestArrivalTime >= Convert.ToDateTime(drPlanning["maxTime"]))
                                {
                                    color = "#ef7700";
                                    drPlanning["Color"] = "#ef7700";
                                    drPlanning["Duration"] = sDurationForArrival;
                                    drPlanning["TimeEnteredZone"] = closestArrivalTime;
                                    //message = string.Empty;
                                    //timeConcat = string.Empty;
                                    message += ",Late Arrival";
                                    timeConcat += "," + closestArrivalTime;
                                    //Concatcolor += ", #ffb366";
                                }

                                //Late Departure
                                if (closestDepartureTime >= Convert.ToDateTime(drPlanning["maxTime"]))
                                {
                                    color = "#ef7700";
                                    drPlanning["Color"] = "#ef7700";
                                    drPlanning["Duration"] = sDurationForDeparture;
                                    drPlanning["TimeEnteredZone"] = closestDepartureTime;
                                    //message = string.Empty;
                                    //timeConcat = string.Empty;
                                    message += ",Late Departure";
                                    timeConcat += "," + closestDepartureTime;
                                    //Concatcolor += ", #ef7700";
                                }
                                #endregion

                                #region ONTIME
                                //ON TIME
                                int VisitedArrival = 0;
                                if (closestArrivalTime >= Convert.ToDateTime(drPlanning["minTime"]) && closestArrivalTime <= Convert.ToDateTime(drPlanning["maxTime"]))
                                {
                                    VisitedArrival = 1;
                                }

                                //ON TIME
                                if (VisitedArrival != 0)
                                {
                                    color = "#00ff00";
                                    drPlanning["Color"] = "#00ff00";
                                    drPlanning["Duration"] = sDurationForArrival;
                                    drPlanning["TimeEnteredZone"] = closestArrivalTime;
                                    //message = string.Empty;
                                    //timeConcat = string.Empty;
                                    message += ",On Time";
                                    timeConcat += "," + closestArrivalTime;
                                    //Concatcolor += ", #00ff00";
                                }
                                #endregion

                                message = message.TrimStart(',');
                                timeConcat = timeConcat.TrimStart(',');
                                //Concatcolor = Concatcolor.TrimStart(',');
                                drPlanning["Status"] = message;
                                drPlanning["Time"] = timeConcat;
                            }
                        }
                        else
                        {
                            if (drPlanning["coordinates"].ToString() != string.Empty)
                            {
                                drPlanning["ZoneName"] = drPlanning["coordinates"].ToString();
                            }
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {

            }
            String T = string.Empty;

            #region case sensitive logic for PostGres sql
            if (dtPlanning.Columns.Contains("scheduledid"))
                dtPlanning.Columns["scheduledid"].ColumnName = "ScheduledID";

            if (dtPlanning.Columns.Contains("pid"))
                dtPlanning.Columns["pid"].ColumnName = "PID";

            if (dtPlanning.Columns.Contains("seqno"))
                dtPlanning.Columns["seqno"].ColumnName = "SeqNo";

            if (dtPlanning.Columns.Contains("remarks"))
                dtPlanning.Columns["remarks"].ColumnName = "Remarks";

            if (dtPlanning.Columns.Contains("zoneid"))
                dtPlanning.Columns["zoneid"].ColumnName = "ZoneID";

            if (dtPlanning.Columns.Contains("zonename"))
                dtPlanning.Columns["zonename"].ColumnName = "ZoneName";

            if (dtPlanning.Columns.Contains("vcaid"))
                dtPlanning.Columns["vcaid"].ColumnName = "VcaId";

            if (dtPlanning.Columns.Contains("vcaname"))
                dtPlanning.Columns["vcaname"].ColumnName = "VcaName";

            if (dtPlanning.Columns.Contains("dtutc"))
                dtPlanning.Columns["dtutc"].ColumnName = "dtUTC";

            if (dtPlanning.Columns.Contains("buffer"))
                dtPlanning.Columns["buffer"].ColumnName = "Buffer";

            if (dtPlanning.Columns.Contains("assetid"))
                dtPlanning.Columns["assetid"].ColumnName = "AssetID";


            if (dtPlanning.Columns.Contains("assetname"))
                dtPlanning.Columns["assetname"].ColumnName = "AssetName";


            if (dtPlanning.Columns.Contains("driverid"))
                dtPlanning.Columns["driverid"].ColumnName = "DriverID";


            if (dtPlanning.Columns.Contains("drivername"))
                dtPlanning.Columns["drivername"].ColumnName = "DriverName";

            if (dtPlanning.Columns.Contains("minduration"))
                dtPlanning.Columns["minduration"].ColumnName = "minDuration";

            if (dtPlanning.Columns.Contains("maxduration"))
                dtPlanning.Columns["maxduration"].ColumnName = "maxDuration";

            if (dtPlanning.Columns.Contains("color"))
                dtPlanning.Columns["color"].ColumnName = "Color";

            if (dtPlanning.Columns.Contains("duration"))
                dtPlanning.Columns["duration"].ColumnName = "Duration";

            if (dtPlanning.Columns.Contains("timeeneteredzone"))
                dtPlanning.Columns["timeeneteredzone"].ColumnName = "TimeEnteredZone";

            if (dtPlanning.Columns.Contains("noofunplannedstops"))
                dtPlanning.Columns["noofunplannedstops"].ColumnName = "NoOfUnplannedStops";

            if (dtPlanning.Columns.Contains("totaldurationofstops"))
                dtPlanning.Columns["totaldurationofstops"].ColumnName = "TotalDurationOfStops";

            #endregion

            dtR.Data = dtPlanning;
            dtR.Rowcnt = dtPlanning.Rows.Count;

            return dtR;
        }

        #endregion




    }
}
