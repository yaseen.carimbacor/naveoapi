using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Plannings;
using NaveoOneLib.Services.Audits;

namespace NaveoOneLib.Services.Plannings
{
   public  class ScheduledAssetService
    {
        Errors er = new Errors();

        public String sScheduledAssetByHID(int HID)
        {
            String sql = String.Empty;
            sql = @"select sa.* ,a.assetnumber,d.sdrivername   from gfi_pln_scheduledasset sa  
inner join  gfi_flt_driver d on d.driverid = sa.driverid
inner join gfi_flt_asset a on sa.assetid = a.AssetID WHERE sa.HID = " + HID.ToString();
            return sql;
        }

        public DataTable GetScheduledAsset(String sConnStr)
        {
            String sqlString = @"SELECT iID, 
HID, 
AssetID, 
DriverID from GFI_PLN_ScheduledAsset order by HID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        public ScheduledAsset GetScheduledAsset(DataRow dr)
        {
            ScheduledAsset retScheduledAsset = new ScheduledAsset();
            retScheduledAsset.iID = Convert.ToInt32(dr["iID"]);
            retScheduledAsset.HID = Convert.ToInt32(dr["HID"]);
            retScheduledAsset.AssetID = String.IsNullOrEmpty(dr["AssetID"].ToString()) ? (int?)null : Convert.ToInt32(dr["AssetID"]);
            retScheduledAsset.DriverID = String.IsNullOrEmpty(dr["DriverID"].ToString()) ? (int?)null : Convert.ToInt32(dr["DriverID"]);
            return retScheduledAsset;
        }
        public ScheduledAsset GetScheduledAssetById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT iID, 
HID, 
AssetID, 
DriverID from GFI_PLN_ScheduledAsset
WHERE HID = ?
order by HID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID,sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetScheduledAsset(dt.Rows[0]);
        }

        DataTable ScheduledAssetDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_PLN_ScheduledAsset";
            dt.Columns.Add("iID", typeof(int));
            dt.Columns.Add("HID", typeof(int));
            dt.Columns.Add("AssetID", typeof(int));
            dt.Columns.Add("DriverID", typeof(int));

            return dt;
        }
        public Boolean SaveScheduledAsset(ScheduledAsset uScheduledAsset, Boolean bInsert, String sConnStr)
        {
            DataTable dt = ScheduledAssetDT();

            DataRow dr = dt.NewRow();
            dr["iID"] = uScheduledAsset.iID;
            dr["HID"] = uScheduledAsset.HID;
            dr["AssetID"] = uScheduledAsset.AssetID != null ? uScheduledAsset.AssetID : (object)DBNull.Value;
            dr["DriverID"] = uScheduledAsset.DriverID != null ? uScheduledAsset.DriverID : (object)DBNull.Value;
            dt.Rows.Add(dr);


            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "HID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uScheduledAsset.HID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public Boolean DeleteScheduledAsset(ScheduledAsset uScheduledAsset, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_PLN_ScheduledAsset where HID = " + uScheduledAsset.HID.ToString();
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            DataTable dtAudit = new AuditService().LogTran(3, "ScheduledAsset", uScheduledAsset.HID.ToString(), Globals.uLogin.UID, uScheduledAsset.HID);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "CallerFunction";
            drCtrl["ParentTblName"] = "GFI_PLN_ScheduledAsset";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }

        public String sScheduledResourcesByHID(int HID)
        {
            String sql = String.Empty;

            sql = "SELECT sA.HID, d.sDriverName ,d.EmployeeType,d.DriverID from GFI_PLN_ScheduledAsset sA inner join GFI_FLT_Driver d on sA.DriverID = d.DriverID  WHERE sA.HID = " + HID.ToString();
            return sql;
        }



        public DataTable getScheduledResourcesByHID(int HID,String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();


            DataTable dt = c.GetDataDT(sScheduledResourcesByHID(HID), sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return dt;




        }

    }
}
