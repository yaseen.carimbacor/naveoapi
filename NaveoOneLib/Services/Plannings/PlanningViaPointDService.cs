using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Plannings;
using NaveoOneLib.Services.Audits;

namespace NaveoOneLib.Services.Plannings
{
    class PlanningViaPointDService
    {
        Errors er = new Errors();
        public DataTable GetPlanningViaPointD(String sConnStr)
        {
            String sqlString = @"SELECT iID, 
HID, 
UOM, 
Capacity, 
Remarks from GFI_PLN_PlanningViaPointD order by HID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public PlanningViaPointD GetPlanningViaPointD(DataRow dr)
        {
            PlanningViaPointD retPlanningViaPointD = new PlanningViaPointD();
            retPlanningViaPointD.HID = Convert.ToInt32(dr["HID"]);
            retPlanningViaPointD.iID = Convert.ToInt32(dr["iID"]);
            retPlanningViaPointD.UOM = String.IsNullOrEmpty(dr["UOM"].ToString()) ? (int?)null : Convert.ToInt32(dr["UOM"]);
            retPlanningViaPointD.Capacity = String.IsNullOrEmpty(dr["Capacity"].ToString()) ? (int?)null : Convert.ToInt32(dr["Capacity"]);
            retPlanningViaPointD.Remarks = dr["Remarks"].ToString();
            return retPlanningViaPointD;
        }
        public PlanningViaPointD GetPlanningViaPointDById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT iID, 
HID, 
UOM, 
Capacity, 
Remarks from GFI_PLN_PlanningViaPointD
WHERE HID = ?
order by HID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID,sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetPlanningViaPointD(dt.Rows[0]);
        }
        DataTable PlanningViaPointDDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_PLN_PlanningViaPointD";
            dt.Columns.Add("iID", typeof(int));
            dt.Columns.Add("HID", typeof(int));
            dt.Columns.Add("UOM", typeof(int));
            dt.Columns.Add("Capacity", typeof(int));
            dt.Columns.Add("Remarks", typeof(String));

            return dt;
        }
        public Boolean SavePlanningViaPointD(PlanningViaPointD uPlanningViaPointD, Boolean bInsert, String sConnStr)
        {
            DataTable dt = PlanningViaPointDDT();

            DataRow dr = dt.NewRow();
            dr["iID"] = uPlanningViaPointD.iID;
            dr["HID"] = uPlanningViaPointD.HID;
            dr["UOM"] = uPlanningViaPointD.UOM;
            dr["Capacity"] = uPlanningViaPointD.Capacity != null ? uPlanningViaPointD.Capacity : (object)DBNull.Value;
            dr["Remarks"] = uPlanningViaPointD.Remarks;
            dt.Rows.Add(dr);


            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "HID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uPlanningViaPointD.HID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public Boolean DeletePlanningViaPointD(PlanningViaPointD uPlanningViaPointD, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_PLN_PlanningViaPointD where HID = " + uPlanningViaPointD.HID.ToString();
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            DataTable dtAudit = new AuditService().LogTran(3, "PlanningViaPointD", uPlanningViaPointD.HID.ToString(), Globals.uLogin.UID, uPlanningViaPointD.HID);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "CallerFunction";
            drCtrl["ParentTblName"] = "GFI_PLN_PlanningViaPointD";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
    }
}
