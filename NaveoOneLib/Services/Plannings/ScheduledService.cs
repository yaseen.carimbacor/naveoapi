using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Plannings;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Services.Audits;
using NaveoOneLib.Models.Common;

namespace NaveoOneLib.Services.Plannings
{
    public class ScheduledService
    {
        Errors er = new Errors();

        public DataTable GetScheduled(String sConnStr)
        {
            String sqlString = @"SELECT HID, 
AssetSelectionID, 
DriverSelectionID, 
Status, 
Remarks, 
intField, 
CreatedDate, 
UpdatedDate, 
CreatedBy, 
UpdatedBy from GFI_PLN_Scheduled order by HID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        public DataTable GetSchedueldViaPoints(String sConnStr, String PID)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"select vH.PID ,vH.SeqNo,vH.ZoneID,vH.LocalityVCA,
                           concat(vH.Lat,',',vH.Lon) as Coord ,
                           zH.Description as zoneDescription,
                           vca.Name as vcaDescription 
                           from GFI_PLN_Planning p
                           LEFT OUTER JOIN GFI_PLN_PlanningViaPointH vH ON  p.PID =vH.PID 
                           LEFT OUTER JOIN GFI_FLT_ZoneHeader zH on zH.ZoneID =vH.ZoneID
                           LEFT OUTER JOIN GFI_FLT_VCAHeader vca on vca.VCAID = vh.LocalityVCA
                           where vH.PID=?";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = PID;
            drSql["sTableName"] = "dtScheduledViaPoints";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            DataTable dtScheduledViaPoints = ds.Tables["dtScheduledViaPoints"];

            if (dtScheduledViaPoints.Rows.Count != 0)
                return dtScheduledViaPoints;
            else return null;
        }

        Scheduled GetScheduled(DataSet ds)
        {
            DataRow dr = ds.Tables["dtScheduled"].Rows[0];
            Scheduled retScheduled = new Scheduled();
            retScheduled.HID = Convert.ToInt32(dr["HID"]);
            retScheduled.AssetSelectionID = String.IsNullOrEmpty(dr["AssetSelectionID"].ToString()) ? (int?)null : Convert.ToInt32(dr["AssetSelectionID"]);
            retScheduled.DriverSelectionID = String.IsNullOrEmpty(dr["DriverSelectionID"].ToString()) ? (int?)null : Convert.ToInt32(dr["DriverSelectionID"]);
            retScheduled.Status = String.IsNullOrEmpty(dr["Status"].ToString()) ? (int?)null : Convert.ToInt32(dr["Status"]);
            retScheduled.Remarks = dr["Remarks"].ToString();
            retScheduled.intField = String.IsNullOrEmpty(dr["intField"].ToString()) ? (int?)null : Convert.ToInt32(dr["intField"]);
            retScheduled.CreatedDate = String.IsNullOrEmpty(dr["CreatedDate"].ToString()) ? (DateTime?)null : (DateTime)dr["CreatedDate"];
            retScheduled.UpdatedDate = String.IsNullOrEmpty(dr["UpdatedDate"].ToString()) ? (DateTime?)null : (DateTime)dr["UpdatedDate"];
            retScheduled.CreatedBy = String.IsNullOrEmpty(dr["CreatedBy"].ToString()) ? (int?)null : Convert.ToInt32(dr["CreatedBy"]);
            retScheduled.UpdatedBy = String.IsNullOrEmpty(dr["UpdatedBy"].ToString()) ? (int?)null : Convert.ToInt32(dr["UpdatedBy"]);

            if (ds.Tables.Contains("dtSchViaPoint"))
            {
                retScheduled.lSchRouteViaPoints = new List<ScheduledRouteViaPoint>();
                foreach(DataRow drv in ds.Tables["dtSchViaPoint"].Rows)
                {
                    ScheduledRouteViaPoint scheduledRouteViaPoint = new ScheduledRouteViaPoint();
                    scheduledRouteViaPoint.iID = drv.Field<int>("iID");
                    scheduledRouteViaPoint.HID = drv.Field<int>("HID");
                    scheduledRouteViaPoint.ViaPointHId = drv.Field<int>("ViaPointHId");
                    scheduledRouteViaPoint.SeqNo = drv.Field<int>("SeqNo");
                    scheduledRouteViaPoint.dtUTC = drv.Field<DateTime>("dtUTC").ToLocalTime();
                    scheduledRouteViaPoint.OprType = DataRowState.Unchanged;
                    retScheduled.lSchRouteViaPoints.Add(scheduledRouteViaPoint);
                }
            }           



            List<Matrix> lMatrix = new List<Matrix>();
            if (ds.Tables.Contains("dtMatrix"))
            {
                MatrixService ms = new MatrixService();
                foreach (DataRow dri in ds.Tables["dtMatrix"].Rows)
                    lMatrix.Add(ms.AssignMatrix(dri));
            }
            retScheduled.lMatrix = lMatrix;

            List<ScheduledPlan> lSchPlan = new List<ScheduledPlan>();
            if (ds.Tables.Contains("dtSchPlan"))
            {
                ScheduledPlanService sp = new ScheduledPlanService();
                foreach (DataRow dri in ds.Tables["dtSchPlan"].Rows)
                    lSchPlan.Add(sp.GetScheduledPlan(dri));
            }
            retScheduled.lSchPlan = lSchPlan;

            List<ScheduledAsset> lSchAsset = new List<ScheduledAsset>();
            if (ds.Tables.Contains("dtSchAsset"))
            {
                ScheduledAssetService sa = new ScheduledAssetService();
                foreach (DataRow dri in ds.Tables["dtSchAsset"].Rows)
                    lSchAsset.Add(sa.GetScheduledAsset(dri));
            }
            retScheduled.lSchAsset = lSchAsset;


            List<PlanningResource> lPlanningResource = new List<PlanningResource>();
            if (ds.Tables.Contains("dtSchResource"))
            {
             
                foreach (DataRow dri in ds.Tables["dtSchResource"].Rows)
                {
                    PlanningResource pr = new PlanningResource();
                    pr.iID = Convert.ToInt32(dri["iID"].ToString());
                    pr.PID = Convert.ToInt32(dri["PID"].ToString());
                    pr.DriverID = Convert.ToInt32(dri["DriverID"].ToString());
                    pr.iSource = Convert.ToInt32(dri["iSource"].ToString());
             
                    lPlanningResource.Add(pr);
                }
                    
            }
            retScheduled.lAdditionalResources = lPlanningResource;

            return retScheduled;
        }
        public Scheduled GetScheduledById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT HID, 
AssetSelectionID, 
DriverSelectionID, 
Status, 
Remarks, 
intField, 
CreatedDate, 
UpdatedDate, 
CreatedBy, 
UpdatedBy from GFI_PLN_Scheduled
WHERE HID = ?
order by HID";

            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;

            DataSet ds = new DataSet();
            dt.TableName = "dtScheduled";
            ds.Merge(dt);
            return GetScheduled(ds);
        }
        public Scheduled GetScheduledByHID(int HID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"SELECT* from GFI_PLN_Scheduled WHERE HID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = HID;
            drSql["sTableName"] = "dtScheduled";
            dtSql.Rows.Add(drSql);

            sql = new MatrixService().sGetMatrixId(HID, "GFI_SYS_GroupMatrixScheduled");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);

            sql = new ScheduledPlanService().sScheduledPlanByHID(HID, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtSchPlan";
            dtSql.Rows.Add(drSql);

            //            sql = @"select s.iid, s.hid, h.iid as viapointhid, h.dtutc from gfi_pln_scheduledrouteviapoints s
            //inner join GFI_PLN_PlanningViaPointH h on s.viapointhid = h.iid WHERE s.hid = ?";

            sql = @"select * from gfi_pln_scheduledrouteviapoints 
                    WHERE hid = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = HID;
            drSql["sTableName"] = "dtSchViaPoint";
            dtSql.Rows.Add(drSql);

            sql = new ScheduledAssetService().sScheduledAssetByHID(HID);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtSchAsset";
            dtSql.Rows.Add(drSql);

           
            sql = @"select r.* ,d.sdrivername from gfi_pln_planningresource r inner join gfi_pln_scheduledplan p on p.PID = r.PID 
 inner join gfi_flt_driver d
 on d.driverid = r.driverid where  r.iSource = 2 and  p.HID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = HID;
            drSql["sTableName"] = "dtSchResource";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables["dtScheduled"].Rows.Count == 0)
                return null;
            else
                return GetScheduled(ds);
        }
        DataTable ScheduledDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_PLN_Scheduled";
            dt.Columns.Add("HID", typeof(int));
            dt.Columns.Add("AssetSelectionID", typeof(int));
            dt.Columns.Add("DriverSelectionID", typeof(int));
            dt.Columns.Add("Status", typeof(int));
            dt.Columns.Add("Remarks", typeof(String));
            dt.Columns.Add("intField", typeof(int));
            dt.Columns.Add("CreatedDate", typeof(DateTime));
            dt.Columns.Add("UpdatedDate", typeof(DateTime));
            dt.Columns.Add("CreatedBy", typeof(int));
            dt.Columns.Add("UpdatedBy", typeof(int));
            dt.Columns.Add("oprType", typeof(DataRowState));

            return dt;
        }
        public dynamic SaveScheduled(Scheduled uScheduled, List<PlanningResource> AdditionalResources, int UID, Boolean bInsert, String sConnStr ,Boolean returnHID = false)
        {
            DataTable dt = ScheduledDT();

            DataRow dr = dt.NewRow();
            dr["HID"] = uScheduled.HID;
            dr["AssetSelectionID"] = uScheduled.AssetSelectionID.HasValue ? uScheduled.AssetSelectionID : (object)DBNull.Value;
            dr["DriverSelectionID"] = uScheduled.DriverSelectionID.HasValue ? uScheduled.DriverSelectionID : (object)DBNull.Value;
            dr["Status"] = uScheduled.Status.HasValue ? uScheduled.Status : (object)DBNull.Value;
            dr["Remarks"] = uScheduled.Remarks.ToString();
            dr["intField"] = uScheduled.intField.HasValue ? uScheduled.intField : (object)DBNull.Value;
            dr["CreatedDate"] = uScheduled.CreatedDate.HasValue ? uScheduled.CreatedDate : (object)DBNull.Value;
            dr["UpdatedDate"] = uScheduled.UpdatedDate.HasValue ? uScheduled.UpdatedDate : (object)DBNull.Value;
            dr["CreatedBy"] = uScheduled.CreatedBy.HasValue ? uScheduled.CreatedBy : (object)DBNull.Value;
            dr["UpdatedBy"] = uScheduled.UpdatedBy.HasValue ? uScheduled.UpdatedBy : (object)DBNull.Value;
            dr["oprType"] = uScheduled.OprType;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "HID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uScheduled.HID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            int iRecurrent = 1000;
            #region GFI_PLN_Planning
            if (uScheduled.lSchPlan != null)
                if (uScheduled.lSchPlan.Count > 0)
                {
                    DataTable dtPlan = new DataTable();
                    dtPlan.TableName = "GFI_PLN_Planning";
                    dtPlan.Columns.Add("PID", typeof(int));
                    dtPlan.Columns.Add("Approval", typeof(int));
                    dtPlan.Columns.Add("OPRSeqNo", typeof(int));
                    dtPlan.Columns.Add("oprType", typeof(DataRowState));

                    foreach (ScheduledPlan s in uScheduled.lSchPlan)
                    {
                        DataRow r = dtPlan.NewRow();
                        r["PID"] = s.PID;
                        r["Approval"] = 100;
                        r["OPRSeqNo"] = 2 + iRecurrent;
                        r["oprType"] = DataRowState.Modified;
                        dtPlan.Rows.Add(r);
                    }

                    drCtrl = dtCtrl.NewRow();
                    drCtrl["SeqNo"] = 2 + iRecurrent;
                    drCtrl["TblName"] = dtPlan.TableName;
                    drCtrl["CmdType"] = "TABLE";
                    drCtrl["KeyFieldName"] = "PID";
                    drCtrl["KeyIsIdentity"] = "TRUE";

                    //Comment following to keep Approval 100. Actually it is HID
                    drCtrl["NextIdAction"] = "SET";
                    drCtrl["NextIdFieldName"] = "Approval";
                    drCtrl["ParentTblName"] = dt.TableName;

                    dtCtrl.Rows.Add(drCtrl);
                    dsProcess.Merge(dtPlan);
                }
            #endregion
            #region GFI_PLN_ScheduledAsset
            if (uScheduled.lSchAsset != null)
                if (uScheduled.lSchAsset.Count > 0)
                {
                    DataTable dtAssets = new myConverter().ListToDataTable<ScheduledAsset>(uScheduled.lSchAsset);
                    dtAssets.TableName = "GFI_PLN_ScheduledAsset";
                    dtAssets.Columns.Add("OPRSeqNo", typeof(int));
                    foreach (DataRow r in dtAssets.Rows)
                        r["OPRSeqNo"] = 3 + iRecurrent;
                    drCtrl = dtCtrl.NewRow();
                    drCtrl["SeqNo"] = 3 + iRecurrent;
                    drCtrl["TblName"] = dtAssets.TableName;
                    drCtrl["CmdType"] = "TABLE";
                    drCtrl["KeyFieldName"] = "iID";
                    drCtrl["KeyIsIdentity"] = "TRUE";
                    drCtrl["NextIdAction"] = "SET";
                    drCtrl["NextIdFieldName"] = "HID";
                    drCtrl["ParentTblName"] = dt.TableName;
                    dtCtrl.Rows.Add(drCtrl);
                    dsProcess.Merge(dtAssets);
                }
            #endregion
            #region GFI_PLN_ScheduledPlan
            if (uScheduled.lSchPlan != null)
                if (uScheduled.lSchPlan.Count > 0)
                {
                    //foreach (ScheduledPlan s in uScheduled.lSchPlan)
                    //    s.PID = s.PlanningRequest.PID;

                    DataTable dtSchPlan = new myConverter().ListToDataTable<ScheduledPlan>(uScheduled.lSchPlan);
                    //dtSchPlan.Columns.Remove("PlanningRequest");
                    dtSchPlan.TableName = "GFI_PLN_ScheduledPlan";
                    dtSchPlan.Columns.Add("OPRSeqNo", typeof(int));
                    foreach (DataRow r in dtSchPlan.Rows)
                        r["OPRSeqNo"] = 4 + iRecurrent;
                    drCtrl = dtCtrl.NewRow();
                    drCtrl["SeqNo"] = 4 + iRecurrent;
                    drCtrl["TblName"] = dtSchPlan.TableName;
                    drCtrl["CmdType"] = "TABLE";
                    drCtrl["KeyFieldName"] = "iID";
                    drCtrl["KeyIsIdentity"] = "TRUE";
                    drCtrl["NextIdAction"] = "SET";
                    drCtrl["NextIdFieldName"] = "HID";
                    drCtrl["ParentTblName"] = dt.TableName;
                    dtCtrl.Rows.Add(drCtrl);
                    dsProcess.Merge(dtSchPlan);
                }
            #endregion
            #region GFI_PLN_PlanningResource
            if (AdditionalResources != null)
                if (AdditionalResources.Count > 0)
                {
                    List<PlanningResource> lPR = new List<Models.Plannings.PlanningResource>();
                    foreach (PlanningResource p in AdditionalResources)
                        if (!p.iSource.HasValue)
                            if (uScheduled.lSchPlan != null)
                                if (uScheduled.lSchPlan.Count > 0)
                                    foreach (ScheduledPlan sp in uScheduled.lSchPlan)
                                    {
                                        PlanningResource planningResource = new PlanningResource();

                                        planningResource.iID = p.iID;

                                        if(bInsert)
                                          planningResource.PID = sp.PID;
                                        else
                                            planningResource.PID = p.PID;

                                        planningResource.iSource = 2;

                                        planningResource.DriverID = p.DriverID;
                                        planningResource.oprType = p.oprType;
                                        lPR.Add(planningResource);
                                    }
                    DataTable dtAdditionalResources = new myConverter().ListToDataTable<PlanningResource>(lPR);
                    dtAdditionalResources.Columns.Remove("DriverName");
                    dtAdditionalResources.TableName = "GFI_PLN_PlanningResource";
                    dtAdditionalResources.Columns.Add("OPRSeqNo", typeof(int));
                    foreach (DataRow r in dtAdditionalResources.Rows)
                        r["OPRSeqNo"] = 5 + iRecurrent;
                    drCtrl = dtCtrl.NewRow();
                    drCtrl["SeqNo"] = 5 + iRecurrent;
                    drCtrl["TblName"] = dtAdditionalResources.TableName;
                    drCtrl["CmdType"] = "TABLE";
                    drCtrl["KeyFieldName"] = "iID";
                    drCtrl["KeyIsIdentity"] = "TRUE";
                    //drCtrl["NextIdAction"] = "SET";
                    //drCtrl["NextIdFieldName"] = "HID";
                    //drCtrl["ParentTblName"] = dt.TableName;
                    dtCtrl.Rows.Add(drCtrl);
                    dsProcess.Merge(dtAdditionalResources);
                }
            #endregion
            #region GFI_SYS_GroupMatrixPlanning
            if (uScheduled.lMatrix == null || uScheduled.lMatrix.Count == 0)
                uScheduled.lMatrix = new MatrixService().GetMatrixByUserID(UID, sConnStr);
            DataTable dtMatrix = new myConverter().ListToDataTable<Matrix>(uScheduled.lMatrix);
            dtMatrix.TableName = "GFI_SYS_GroupMatrixScheduled";
            dtMatrix.Columns.Add("OPRSeqNo", typeof(int));
            foreach (DataRow r in dtMatrix.Rows)
                r["OPRSeqNo"] = 6 + iRecurrent;
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 6 + iRecurrent;
            drCtrl["TblName"] = dtMatrix.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "MID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "iID";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtMatrix);
            #endregion
            #region GFI_PLN_ScheduledRouteViaPoints
            if (uScheduled.lSchRouteViaPoints != null)
                if (uScheduled.lSchRouteViaPoints.Count > 0)
                {


                    DataTable dtScheduledRouteViaPoints = new myConverter().ListToDataTable<ScheduledRouteViaPoint>(uScheduled.lSchRouteViaPoints);

                    if (dtScheduledRouteViaPoints.Columns.Contains("ViaPointName"))
                        dtScheduledRouteViaPoints.Columns.Remove("ViaPointName");

                    //dtSchPlan.Columns.Remove("PlanningRequest");
                    dtScheduledRouteViaPoints.TableName = "GFI_PLN_ScheduledRouteViaPoints";
                    dtScheduledRouteViaPoints.Columns.Add("OPRSeqNo", typeof(int));
                    foreach (DataRow r in dtScheduledRouteViaPoints.Rows)
                        r["OPRSeqNo"] = 7 + iRecurrent;
                    drCtrl = dtCtrl.NewRow();
                    drCtrl["SeqNo"] = 7 + iRecurrent;
                    drCtrl["TblName"] = dtScheduledRouteViaPoints.TableName;
                    drCtrl["CmdType"] = "TABLE";
                    drCtrl["KeyFieldName"] = "iID";
                    drCtrl["KeyIsIdentity"] = "TRUE";
                    drCtrl["NextIdAction"] = "SET";
                    drCtrl["NextIdFieldName"] = "HID";
                    drCtrl["ParentTblName"] = dt.TableName;
                    dtCtrl.Rows.Add(drCtrl);
                    dsProcess.Merge(dtScheduledRouteViaPoints);
                }
            #endregion


            foreach (DataTable dti in dsProcess.Tables)
                foreach (DataRow dri in dti.Rows)
                    if (bInsert)
                        dri["oprType"] = DataRowState.Added;


            if (bInsert)
            {
                foreach (DataTable dti in dsProcess.Tables)
                    if (dti.TableName == "GFI_PLN_Planning")
                        foreach (DataRow dri in dti.Rows)
                            dri["oprType"] = DataRowState.Modified;


            }


            #region checking if pln is still uncheduled
            if (bInsert)
            {
                String strResult = String.Empty;
                foreach (ScheduledPlan s in uScheduled.lSchPlan)
                    strResult += s.PID + ", ";
                if (!String.IsNullOrEmpty(strResult))
                    strResult = strResult.Substring(0, strResult.Length - 2);

                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 888;
                drCtrl["TblName"] = "ChkForScheduled";
                drCtrl["CmdType"] = "Chk";
                drCtrl["Command"] = "select * from GFI_PLN_Planning where Approval != 0 and PID in (" + strResult + ")";
                dtCtrl.Rows.Add(drCtrl);
            }
            
            #endregion

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;


            //return new HID
            if (bResult && returnHID)
            {
                return Convert.ToInt32(dtCtrl.Select("SeqNo = 1 and TblName = 'GFI_PLN_Scheduled'").FirstOrDefault()["NextIDValue"].ToString());
            }


            return bResult;
        }
        public Boolean DeleteScheduled(Scheduled uScheduled, int UID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = String.Empty;
            sql += "\r\n";
            sql += @"update GFI_PLN_Planning set Approval = 0
                     where PID in (select sp.PID from GFI_PLN_Scheduled s 
                     inner join GFI_PLN_ScheduledPlan Sp 
			         on Sp.HID  = s.HID where s.HID ='"+ uScheduled.HID+"');";
            sql += "\r\n";
            sql += @"delete from GFI_PLN_Scheduled where HID = " + uScheduled.HID.ToString();

            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            DataTable dtAudit = new AuditService().LogTran(3, "Scheduled", uScheduled.HID.ToString(), UID, uScheduled.HID);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "CallerFunction";
            drCtrl["ParentTblName"] = "GFI_PLN_Scheduled";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }

        public DataTable GetPlanningScheduleRequest(DateTime dtFrom, DateTime dtTo, List<Matrix> lMatrix,List<int> lAssetids, String sConnStr, List<String> sortColumns, Boolean sortOrderAsc)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            return GetPlanningScheduleRequest(dtFrom, dtTo, iParentIDs, lAssetids, sConnStr, sortColumns, sortOrderAsc);
        }
        public DataTable GetPlanningScheduleRequest(DateTime dtFrom, DateTime dtTo, List<int> iParentIDs, List<int> lAssetids, String sConnStr, List<String> sortColumns, Boolean sortOrderAsc)
        {
            DataTable dt = new Connection(sConnStr).GetDataDT(sGetPlanningScheduleRequest(dtFrom, dtTo, iParentIDs, lAssetids, sConnStr, sortColumns, sortOrderAsc), sConnStr);
            return dt;
        }
        public String sGetPlanningScheduleRequest(DateTime dtFrom, DateTime dtTo, List<int> iParentIDs, List<int> lAssetIds, String sConnStr, List<String> sortColumns, Boolean sortOrderAsc)
        {
            #region sort
            String sSort = "ScheduleId";
            if (sortColumns != null && sortColumns.Count > 0)
            {
                List<String> lCols = new List<string>() { "PID", "ScheduleId,RequestDate,CreatedDate,StartTime,EndTime,Departure,Arrival,Asset,Driver,Approval" };
                String ss = String.Join(",", sortColumns
                    .Where(s => !String.IsNullOrWhiteSpace(s))
                    .Where(s => lCols.Contains(s))
                    .Select(s => s));

                if (!String.IsNullOrEmpty(ss))
                    sSort = ss;
            }

            if (sortOrderAsc)
                sSort += " asc";
            else
                sSort += " desc";
            #endregion

            String sql = @"
--GetScheduleRequest
drop table if exists #Planning;
SELECT distinct p.ClosureComments as PlanningOperationCode,p.PID ,p.Approval as Status ,p.Type ,p.ParentRequestCode,p.RequestCode, p.Remarks 
    , Convert(nvarchar(500), null) ScheduleId
	, GetDate() RequestDate
	, GetDate() ScheduledDate 
    , Convert(nvarchar(500), null)   StartTime
	, Convert(nvarchar(500), null) EndTime
	, GetDate() dtFrom
	, GetDate() dtTo
	, Convert(nvarchar(500), null) ZoneFr
	, Convert(nvarchar(500), null) VCAFr
	, Convert(nvarchar(500), null) ClientFr
	, Convert(nvarchar(max), null) ViaPoints
	, Convert(nvarchar(500), null) ZoneTo
	, Convert(nvarchar(500), null) VCATo
	, Convert(nvarchar(500), null) ClientTo
	, Convert(nvarchar(500), null) Departure
    , Convert(int, null) DepartureID
	, Convert(nvarchar(500), null) Arrival
    , Convert(int, null) ArrivalID
    , Convert(int, null) AssetID
    , Convert(nvarchar(500), null) Asset
    , Convert(int, null) DriverID
	, Convert(nvarchar(500), null) Driver
    , Convert(nvarchar(500), null) ZoneArea
    , Convert(nvarchar(500), null) SoilType
    , Convert(nvarchar(500), null) ToolType
    , Convert(int, null) Buffer
    , Convert(nvarchar(500), null) TripType
    , Convert(nvarchar(500), null) TripTypeColor
    , Convert(nvarchar(500), null) CoordinatesFrom
    , Convert(nvarchar(500), null) CoordinatesTo
into #Planning from GFI_PLN_Planning p
    left join GFI_PLN_PlanningViaPointH h on h.PID = p.PID
	left join GFI_SYS_GroupMatrixPlanning m on p.PID = m.iID
	left join GFI_PLN_ScheduledPlan sp  on p.PID = sp.PID
	left join GFI_PLN_Scheduled s on s.HID = sp.HID
    left join GFI_PLN_ScheduledAsset  scheduledAsset on scheduledAsset.HID = s.HID
where m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + @") 
    and h.dtUTC >= '" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "' and h.dtUTC <= '" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "'"; 

            if(lAssetIds != null)
            {
                if (lAssetIds.Count > 0)
                {
                    string AssetID = string.Empty;
                    foreach (var i in lAssetIds)
                        AssetID += i.ToString() + ",";
                    AssetID = AssetID.TrimEnd(',');


                    sql += " and scheduledAsset.AssetID in (" + AssetID + ");";
                }
            }
               



            sql += @"

update #Planning 
	set TripType = lkv.name , TripTypeColor = lkv.Value2
from #Planning p 
	inner join gfi_pln_planninglkup lkp on p.PID = lkp.PID
	inner join gfi_sys_lookupvalues lkv on lkp.lookupvalueid = lkv.vid
	inner join gfi_sys_lookuptypes lkt on lkv.tid = lkt.tid
where lkt.code = 'TRIP_TYPE';

update #Planning 
	set Buffer = h.Buffer
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
where h.SeqNo = 1;

update #Planning 
	set Buffer = h.Buffer
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
where h.SeqNo = 999;


update #Planning 
	set dtFrom = h.dtUTC
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
where h.SeqNo = 1;

update #Planning 
	set dtTo = h.dtUTC ,SoilType = h.Remarks
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
where h.SeqNo = 999;

update #Planning 
	set StartTime = (select convert(varchar, h.dtUTC, 108))
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
where h.SeqNo = 1;

update #Planning 
	set EndTime = (select convert(varchar, h.dtUTC, 108))
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
where h.SeqNo = 999;


update #Planning 
	set ZoneFr = z.Description ,ZoneArea = z.ExternalRef ,DepartureID =z.ZoneID
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_FLT_ZoneHeader z on h.ZoneID = z.ZoneID 
where h.SeqNo = 1;

update #Planning 
	set ZoneTo = z.Description ,ZoneArea = z.ExternalRef,ArrivalID =z.ZoneID
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_FLT_ZoneHeader z on h.ZoneID = z.ZoneID 
where h.SeqNo = 999;


update #Planning 
	set VCAFr = v.VCAName
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_GIS_VCA v on h.LocalityVCA = v.VCAID 
where h.SeqNo = 1;

update #Planning 
	set VCATo = v.VCAName
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_GIS_VCA v on h.LocalityVCA = v.VCAID 
where h.SeqNo = 999;


update #Planning 
	set ClientFr = c.Name
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_SYS_CustomerMaster c on h.ClientID = c.ClientID 
where h.SeqNo = 1;

update #Planning 
	set ClientTo = c.Name
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_SYS_CustomerMaster c on h.ClientID = c.ClientID 
where h.SeqNo = 999;

update #Planning 
	set Departure = CONVERT(nvarchar(15), h.Lat) + ', ' + CONVERT(nvarchar(15), h.Lon)
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
where h.SeqNo = 1 and h.Lat is not null;

update #Planning set Departure = ZoneFr where Departure is null;

update #Planning set Departure = VCAFr where Departure is null;

update #Planning set Departure = ClientFr where Departure is null;

update #Planning 
	set Arrival = CONVERT(nvarchar(15), h.Lat) + ', ' + CONVERT(nvarchar(15), h.Lon)
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
where h.SeqNo = 999 and h.Lat is not null;


update #Planning 
	set AssetID = a.AssetID
from 
 #Planning p 
	inner join GFI_PLN_ScheduledPlan sP on p.PID = sP.PID
	inner join GFI_PLN_Scheduled s on s.HID = sP.HID
	inner join GFI_PLN_ScheduledAsset sA on s.HID =sA.HID
	inner join GFI_FLT_Asset  a on a.AssetID =sA.AssetID;

update #Planning 
	set Asset = a.AssetName
from 
 #Planning p 
	inner join GFI_PLN_ScheduledPlan sP on p.PID = sP.PID
	inner join GFI_PLN_Scheduled s on s.HID = sP.HID
	inner join GFI_PLN_ScheduledAsset sA on s.HID =sA.HID
	inner join GFI_FLT_Asset  a on a.AssetID =sA.AssetID;

update #Planning 
	set Driver = d.sDriverName
from 
 #Planning p 
	inner join GFI_PLN_ScheduledPlan sP on p.PID = sP.PID
	inner join GFI_PLN_Scheduled s on s.HID = sP.HID
	inner join GFI_PLN_ScheduledAsset sA on s.HID =sA.HID
	inner join GFI_FLT_Driver  d on d.DriverID =sA.DriverID;

update #Planning 
	set DriverID = d.DriverID
from 
 #Planning p 
	inner join GFI_PLN_ScheduledPlan sP on p.PID = sP.PID
	inner join GFI_PLN_Scheduled s on s.HID = sP.HID
	inner join GFI_PLN_ScheduledAsset sA on s.HID =sA.HID
	inner join GFI_FLT_Driver  d on d.DriverID =sA.DriverID;

update #Planning 
	set ScheduleId = s.HID
from 
 #Planning p 
	inner join GFI_PLN_ScheduledPlan sP on p.PID = sP.PID
	inner join GFI_PLN_Scheduled s on s.HID = sP.HID;


update #Planning 
	set RequestDate = p.dtFrom
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID;

update #Planning 
	set ScheduledDate = s.CreatedDate
from 
 #Planning p 
	inner join GFI_PLN_ScheduledPlan sP on p.PID = sP.PID
	inner join GFI_PLN_Scheduled s on s.HID = sP.HID;

update #Planning 
	set CoordinatesFrom = concat(zh.Latitude ,',',zh.Longitude)
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_FLT_ZoneHeader z on h.ZoneID = z.ZoneID 
	inner join GFI_FLT_ZoneDetail zh on z.ZoneID = zh.ZoneID 
where h.SeqNo = 1


update #Planning 
	set CoordinatesTo = concat(zh.Latitude ,',',zh.Longitude)
from #Planning p 
	inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
	inner join GFI_FLT_ZoneHeader z on h.ZoneID = z.ZoneID
	inner join GFI_FLT_ZoneDetail zh on z.ZoneID = zh.ZoneID  
where h.SeqNo = 999


update #Planning set Arrival = ZoneTo where Arrival is null;

update #Planning set Arrival = VCATo where Arrival is null;

update #Planning set Arrival = ClientTo where Arrival is null;

update #Planning set CoordinatesFrom = Departure where departureid = '';

update #Planning set CoordinatesTo = Arrival where arrivalid = '';

select * from #Planning order by " + sSort + @";

drop table #Planning;
";
            return sql;
        }

        public String GetScheduledDriver(DateTime dtFrom, DateTime dtTo,String sConnStr)
        {
         
            String sql = @"
select Distinct schAsset.iid,d.DriverID,d.sdrivername from 
gfi_pln_planningviapointh viaPoint
inner join GFI_PLN_ScheduledPlan schPlan on viaPoint.PID = schPlan.PID
inner join GFI_PLN_ScheduledAsset schAsset on schAsset.HID = schPlan.HID
inner join GFI_FLT_ASSET a on a.AssetID = schAsset.AssetID
inner join GFI_FLT_DRIVER d on d.DRIVERID = schAsset.DriverID
where  viaPoint.dtUTC >= '" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "' and viaPoint.dtUTC <= '" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff") + @"'; 
";
            return sql;
        }

        public DataTable GetScheduledAssets(DateTime dtFrom, DateTime dtTo,String sConnStr)
        {
            DataTable dt = new Connection(sConnStr).GetDataDT(GetScheduledAsset(dtFrom, dtTo, sConnStr), sConnStr);
            return dt;
        }


        public String sGetDriverAssetAssg(DateTime dtFrom, DateTime dtTo, String sConnStr)
        {

            String sql = @"
select 
driverAsset.drvasset_id as id 
,CONCAT  (d.sdrivername, '/ ',a.assetname) as DriverAsset 
,d.DriverID,d.sdrivername ,
a.AssetID ,
a.AssetName from 
gfi_pln_driverasset driverAsset
inner join GFI_FLT_ASSET a on a.AssetID = driverAsset.AssetID
inner join GFI_FLT_DRIVER d on d.DRIVERID = driverAsset.DriverID
where driverAsset.drvasset_datefrom >='" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "' and  drvasset_dateto <= '" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff") + @"'; 
";
            return sql;
        }

        public dtData GetDriverAssetAssg(DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            DataTable dt = new Connection(sConnStr).GetDataDT(sGetDriverAssetAssg(dtFrom, dtTo, sConnStr), sConnStr);

            dtData dtd = new dtData();
            dtd.Data = dt;

            return dtd;
        }

        public DataTable GetScheduledDrivers(DateTime dtFrom, DateTime dtTo,String sConnStr)
        {
            DataTable dt = new Connection(sConnStr).GetDataDT(GetScheduledDriver(dtFrom, dtTo, sConnStr), sConnStr);
            return dt;
        }

        public String GetScheduledAsset(DateTime dtFrom, DateTime dtTo, String sConnStr)
        {

            String sql = @"
select Distinct schAsset.iid,a.AssetID ,a.AssetName from 
gfi_pln_planningviapointh viaPoint
inner join GFI_PLN_ScheduledPlan schPlan on viaPoint.PID = schPlan.PID
inner join GFI_PLN_ScheduledAsset schAsset on schAsset.HID = schPlan.HID
inner join GFI_FLT_ASSET a on a.AssetID = schAsset.AssetID
inner join GFI_FLT_DRIVER d on d.DRIVERID = schAsset.DriverID
where  viaPoint.dtUTC >= '" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "' and viaPoint.dtUTC <= '" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff") + @"'; 
";
            return sql;
        }


        public DataTable GetNearestZone(int zoneId, String sConnStr)
        {
            DataTable dt = new Connection(sConnStr).GetDataDT(sGetNearestZone(zoneId, sConnStr), sConnStr);
            return dt;
        }

        public String sGetNearestZone(int zoneId, String sConnStr)
        {

            String sql = @"
select d.DriverID  as id
,d.sDriverName as description
,z.ZoneID
,z.description
,ST_Distance((select GeomData from GFI_FLT_ZoneHeader where ZoneID = " + zoneId + "), z.GeomData) * cosd(42.3521)  as distance  from GFI_FLT_DRIVER d inner join GFI_FLT_ZoneHeader z on  d.ZoneID = z.ZoneID where d.employeetype = 'Driver' order by distance asc";
            return sql;
        }

        #region customized terra

        public DataTable GetPlanningLookUp(int PID,String sConnStr)
        {
            DataTable dt = new Connection(sConnStr).GetDataDT(sGetPlanningLookUp(PID,sConnStr), sConnStr);
            return dt;
        }

        public String sGetPlanningLookUp(int PID, String sConnStr)
        {

            String sql = @"SELECT lkup.PID
                                 ,lkpValues.Name as LookupValues 
                                 ,lkpValues.VID
                                 ,lkpValues.Value as value
                                 ,lkpValues.Value2 
                                 ,lkpTypes.Description as LookType 
                                  FROM GFI_PLN_PlanningLkup lkup
                                  LEFT JOIN  GFI_SYS_LookUpValues lkpValues on lkup.LookupValueID = lkpValues.VID
                                  LEFT JOIN GFI_SYS_LookUpTypes lkpTypes on lkpTypes.TID = lkpValues.TID
                                  WHERE lkup.PID =" + PID + "";

            return sql;
        }


        public  int getCount(String sConnStr)
        {
            String sql = @"select COUNT (ParentRequestCode) from GFI_PLN_Planning where type = 200";

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            //drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            int i = 0;
            int.TryParse(ds.Tables[0].Rows[0][0].ToString(), out i);
           
            return i;
        }

        #endregion

    }
}
