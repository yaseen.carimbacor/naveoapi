using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Plannings;
using NaveoOneLib.Services.Audits;


namespace NaveoOneLib.Services.Plannings
{
    class PlanningLkupService
    {
        Errors er = new Errors();

        public String sGetPlanningLkupByPID(int PID)
        {
            String sql = String.Empty;
            sql = "SELECT * from GFI_PLN_PlanningLkup WHERE PID = " + PID.ToString();
            return sql;
        }

        public DataTable GetPlanningLkup(String sConnStr)
        {
            String sqlString = @"SELECT iID, 
PID, 
LookupValueID, 
LookupType from GFI_PLN_PlanningLkup order by PID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public PlanningLkup GetPlanningLkup(DataRow dr)
        {
            PlanningLkup retPlanningLkup = new PlanningLkup();
            retPlanningLkup.iID = Convert.ToInt32(dr["iID"]);
            retPlanningLkup.PID = Convert.ToInt32(dr["PID"]);
            retPlanningLkup.LookupValueID = Convert.ToInt32(dr["LookupValueID"]);
            retPlanningLkup.LookupType = dr["LookupType"].ToString();
            return retPlanningLkup;
        }
        public PlanningLkup GetPlanningLkupById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT iID, 
PID, 
LookupValueID, 
LookupType from GFI_PLN_PlanningLkup
WHERE PID = ?
order by PID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetPlanningLkup(dt.Rows[0]);
        }
        DataTable PlanningLkupDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_PLN_PlanningLkup";
            dt.Columns.Add("iID", typeof(int));
            dt.Columns.Add("PID", typeof(int));
            dt.Columns.Add("LookupValueID", typeof(int));
            dt.Columns.Add("LookupType", typeof(String));

            return dt;
        }
        public Boolean SavePlanningLkup(PlanningLkup uPlanningLkup, Boolean bInsert, String sConnStr)
        {
            DataTable dt = PlanningLkupDT();

            DataRow dr = dt.NewRow();
            dr["iID"] = uPlanningLkup.iID;
            dr["PID"] = uPlanningLkup.PID;
            dr["LookupValueID"] = uPlanningLkup.LookupValueID;
            dr["LookupType"] = uPlanningLkup.LookupType;
            dt.Rows.Add(dr);


            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "PID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uPlanningLkup.PID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public Boolean DeletePlanningLkup(PlanningLkup uPlanningLkup, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_PLN_PlanningLkup where PID = " + uPlanningLkup.PID.ToString();
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            DataTable dtAudit = new AuditService().LogTran(3, "PlanningLkup", uPlanningLkup.PID.ToString(), Globals.uLogin.UID, uPlanningLkup.PID);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "CallerFunction";
            drCtrl["ParentTblName"] = "GFI_PLN_PlanningLkup";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
    }
}
