﻿using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Plannings;
using NaveoOneLib.Services.GMatrix;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoOneLib.Services.Plannings
{
    public class CustomizedTripsService
    {

        DataTable CustomizedTripsDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_CustomizedTrips";
            dt.Columns.Add("iID", typeof(int));
            dt.Columns.Add("AssetID", typeof(int));
            dt.Columns.Add("DriverID", typeof(int));
            dt.Columns.Add("DateFrom", typeof(DateTime));
            dt.Columns.Add("DateTo", typeof(DateTime));
            dt.Columns.Add("Description", typeof(String));
            dt.Columns.Add("ZoneID", typeof(int));
            dt.Columns.Add("LookUpValue", typeof(int));
            dt.Columns.Add("IsStopped", typeof(int));
            dt.Columns.Add("CountAsWorkHour", typeof(int));
            dt.Columns.Add("Duration", typeof(int));
            dt.Columns.Add("Field1", typeof(String));
            dt.Columns.Add("Field2", typeof(String));
            dt.Columns.Add("Field3", typeof(String));
            dt.Columns.Add("Field4", typeof(String));
            dt.Columns.Add("Field5", typeof(String));
            dt.Columns.Add("oprType", typeof(DataRowState));

            return dt;
        }

        public BaseModel SaveCustomizedTrips(List<CustomizedTrips> lCustomizedTrips, int UID, Boolean bInsert, String sConnStr)
        {
            BaseModel baseModel = new BaseModel();
            Boolean bResult = false;

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            DataSet dsProcess = new DataSet();


            DataTable dt = CustomizedTripsDT();

          
           #region GFI_FLT_CustomizedTrips
            DataTable dtCustomizedTrips = new myConverter().ListToDataTable<CustomizedTrips>(lCustomizedTrips);
         
            dtCustomizedTrips.TableName = "GFI_FLT_CustomizedTrips";
      
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dtCustomizedTrips.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "iID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "GET";
            drCtrl["ParentTblName"] = dtCustomizedTrips.TableName;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtCustomizedTrips);
            dsProcess.Merge(dtCtrl);
            #endregion

            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResult.Tables["CTRLTBL"];

            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                {

                    bResult = false;
                }
                else
                {
                    bResult = true;
                }
            }
            else
                bResult = false;

            baseModel.data = bResult;
            return baseModel;


        }

        public DataTable GetCustomizedTrips(DateTime dtFrom, DateTime dtTo, List<int> lAssetIds, String sConnStr, List<String> sortColumns, Boolean sortOrderAsc)
        {
            DataTable dt = new Connection(sConnStr).GetDataDT(sGetCustomizedTrips(dtFrom, dtTo,lAssetIds, sConnStr, sortColumns, sortOrderAsc), sConnStr);
            return dt;
        }
        public String sGetCustomizedTrips(DateTime dtFrom, DateTime dtTo, List<int> lAssetIds, String sConnStr, List<String> sortColumns, Boolean sortOrderAsc)
        {

            #region sort
            //String sSort = "dtFrom";
            //if (sortColumns != null && sortColumns.Count > 0)
            //{
            //    List<String> lCols = new List<string>() { "PID", "dtFrom,", "RequestCode", "Departure", "Arrival", "Approval", "ProcessedBy", "ProcessedDate" };
            //    String ss = String.Join(",", sortColumns
            //        .Where(s => !String.IsNullOrWhiteSpace(s))
            //        .Where(s => lCols.Contains(s))
            //        .Select(s => s));

            //    if (!String.IsNullOrEmpty(ss))
            //        sSort = ss;
            //}

            //if (sortOrderAsc)
            //    sSort += " asc";
            //else
            //    sSort += " desc";
            #endregion

            string AssetID = string.Empty;
            foreach (var i in lAssetIds)
                AssetID += i.ToString() + ",";
            AssetID = AssetID.TrimEnd(',');

            String sql = @"
select CONVERT(VARCHAR,custom.DateFrom, 111) dtLocalTime,custom.* ,asset.AssetNumber ,driver.sDriverName ,zone.Description as ZoneName ,lkp.Description as SoilType from GFI_FLT_CustomizedTrips custom
LEFT join GFI_FLT_Asset asset  on custom.AssetID = asset.AssetID
LEFT join GFI_FLT_Driver driver on custom.DriverID =driver.DriverID
LEFT join GFI_FLT_ZoneHeader zone on custom.ZoneID = zone.ZoneID
LEFT join GFI_SYS_LookUpValues lkp on custom.LookUpValue = lkp.VID
where custom.DateFrom >= '" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "' and custom.DateTo <= '" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "' " +
"and custom.AssetID in ("+ AssetID + ") order by DateFrom  asc";

            return sql;


        }


    }
}
