using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Plannings;
using NaveoOneLib.Services.Audits;

namespace NaveoOneLib.Services.Plannings
{
    public class PlanningResourceService
    {
        Errors er = new Errors();

        public String sPlanningResourceByPID(int PID)
        {
            String sql = String.Empty;
            sql = "SELECT * from GFI_PLN_PlanningResource WHERE  iSource = 1 and PID = " + PID.ToString();
            return sql;
        }

        public DataTable GetPlanningResource(String sConnStr)
        {
            String sqlString = @"SELECT * from GFI_PLN_PlanningResource order by PID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public List<PlanningResource> GetPlanningResource(DataTable dt)
        {
            List<PlanningResource> lRetPlanningResource = new List<PlanningResource>();
            
            foreach (DataRow dr in dt.Rows)
            {
                PlanningResource retPlanningResource = new PlanningResource();
                retPlanningResource.iID = Convert.ToInt32(dr["iID"]);
                retPlanningResource.PID = Convert.ToInt32(dr["PID"]);
                retPlanningResource.DriverID = Convert.ToInt32(dr["DriverID"]);
                retPlanningResource.ResourceType = dr["ResourceType"].ToString();
                retPlanningResource.iSource = String.IsNullOrEmpty(dr["iSource"].ToString()) ? (int?)null : Convert.ToInt32(dr["iSource"]);
                retPlanningResource.MileageAllowance = String.IsNullOrEmpty(dr["MileageAllowance"].ToString()) ? (int?)null : Convert.ToInt32(dr["MileageAllowance"]);
                retPlanningResource.CustomType = String.IsNullOrEmpty(dr["CustomType"].ToString()) ? (int?)null : Convert.ToInt32(dr["CustomType"]);
                retPlanningResource.TravelGrant = String.IsNullOrEmpty(dr["TravelGrant"].ToString()) ? (int?)null : Convert.ToInt32(dr["TravelGrant"]);

                lRetPlanningResource.Add(retPlanningResource);

            }
            return lRetPlanningResource;
        }
        public List<PlanningResource> GetPlanningResourceById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT * from GFI_PLN_PlanningResource WHERE PID = ?";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID,sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetPlanningResource(dt);
        }
        DataTable PlanningResourceDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_PLN_PlanningResource";
            dt.Columns.Add("iID", typeof(int));
            dt.Columns.Add("PID", typeof(int));
            dt.Columns.Add("DriverID", typeof(int));
            dt.Columns.Add("ResourceType", typeof(String));
            dt.Columns.Add("iSource", typeof(int));

            return dt;
        }
        public Boolean SavePlanningResource(PlanningResource uPlanningResource, Boolean bInsert, String sConnStr)
        {
            DataTable dt = PlanningResourceDT();

            DataRow dr = dt.NewRow();
            dr["iID"] = uPlanningResource.iID;
            dr["PID"] = uPlanningResource.PID;
            dr["DriverID"] = uPlanningResource.DriverID;
            dr["ResourceType"] = uPlanningResource.ResourceType;
            dr["iSource"] = uPlanningResource.iSource.HasValue ? uPlanningResource.iSource : 1;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "PID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uPlanningResource.PID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public Boolean DeletePlanningResource(PlanningResource uPlanningResource, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_PLN_PlanningResource where PID = " + uPlanningResource.PID.ToString();
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            DataTable dtAudit = new AuditService().LogTran(3, "PlanningResource", uPlanningResource.PID.ToString(), Globals.uLogin.UID, uPlanningResource.PID);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "CallerFunction";
            drCtrl["ParentTblName"] = "GFI_PLN_PlanningResource";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
    }
}
