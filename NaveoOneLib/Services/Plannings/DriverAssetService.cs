﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NaveoOneLib.Models.Common;
using NaveoOneLib.DBCon;
using System.Data;
using NaveoOneLib.Models.Users;
using System.Data.SqlTypes;
using System.IO;
using System.Drawing;
using NaveoOneLib.Common;
using NaveoOneLib.Services;
using NaveoService.Services;
using System.Drawing.Imaging;
using System.Web;
using System.Configuration;
using NaveoOneLib.Services.GMatrix;
namespace NaveoOneLib.Services.Plannings
{
    public class DriverAssetService
    {

        DataTable DriverAssetDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_PLN_DriverAsset";
            dt.Columns.Add("drvAsset_id", typeof(int));
            dt.Columns.Add("AssetID", typeof(int));
            dt.Columns.Add("DriverID", typeof(int));
            dt.Columns.Add("drvAsset_DateFrom", typeof(DateTime));
            dt.Columns.Add("drvAsset_DateTo", typeof(DateTime));
            dt.Columns.Add("drvAsset_Remarks", typeof(String));
            dt.Columns.Add("drvAsset_CreatedDate", typeof(String));
            dt.Columns.Add("drvAsset_UpdatedDate", typeof(String));
            dt.Columns.Add("drvAsset_CreatedBy", typeof(String));
            dt.Columns.Add("drvAsset_UpdatedBy", typeof(String));
            dt.Columns.Add("drvAsset_flagDelete", typeof(Boolean));
            dt.Columns.Add("oprType", typeof(DataRowState));

            return dt;
        }

        public BaseModel DriverAssetMng(NaveoOneLib.Models.DriverAsset.DriverAssetDto driverAssetMng, String sConnStr)
        {
            BaseModel baseModel = new BaseModel();
            Boolean bResult = false;

            Boolean bInsert = false;

            int AccidentId = 0;

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            DataSet dsProcess = new DataSet();

            DataTable dtDriverAsset = DriverAssetDT();


            if (driverAssetMng.drvAsset_id == 0)
            {
                bInsert = true;
            }
            else
            {
                bInsert = false; //Update
            }


            DataRow drDriverAsset = dtDriverAsset.NewRow();
            if (!bInsert)
            {
                drDriverAsset["drvAsset_id"] = driverAssetMng.drvAsset_id;
            }
            drDriverAsset["AssetID"] = driverAssetMng.AssetID;
            drDriverAsset["DriverID"] = driverAssetMng.DriverID;
             drDriverAsset["drvAsset_DateFrom"] = driverAssetMng.drvAsset_DateFrom;

            drDriverAsset["drvAsset_DateTo"] = driverAssetMng.drvAsset_DateTo;
            drDriverAsset["drvAsset_Remarks"] = driverAssetMng.drvAsset_Remarks;
            drDriverAsset["drvAsset_CreatedDate"] = driverAssetMng.drvAsset_CreatedDate;
            drDriverAsset["drvAsset_UpdatedDate"] = driverAssetMng.drvAsset_UpdatedDate;
            drDriverAsset["drvAsset_CreatedBy"] = driverAssetMng.drvAsset_CreatedBy;
            drDriverAsset["drvAsset_UpdatedBy"] = driverAssetMng.drvAsset_UpdatedBy;


            if (driverAssetMng.drvAsset_flagDelete) //Flag Delete
            {
                drDriverAsset["drvAsset_flagDelete"] = driverAssetMng.drvAsset_flagDelete;
            }

            if (bInsert) // Flag Update
            {
                drDriverAsset["oprType"] = DataRowState.Added;
            }
            else
            {
                drDriverAsset["oprType"] = DataRowState.Modified;
            }

            dtDriverAsset.Rows.Add(drDriverAsset);

            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dtDriverAsset.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "drvAsset_id";
            drCtrl["KeyIsIdentity"] = "TRUE";
            //drCtrl["NextIdAction"] = "GET";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = driverAssetMng.drvAsset_id;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtDriverAsset);


            dsProcess.Merge(dtCtrl);

            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResult.Tables["CTRLTBL"];

            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");
                DataRow[] dRowId = null;

                if (dRow.Length > 0)
                {
                    baseModel.dbResult = dsResult;
                    baseModel.dbResult.Tables["CTRLTBL"].TableName = "ResultCtrlTbl";
                    baseModel.dbResult.Merge(dsProcess);
                    bResult = false;
                }
                else
                {
                    bResult = true;
                    dRowId = dtCtrl.Select();
                    AccidentId = int.Parse(dRowId[0].ItemArray[10].ToString());
                }

            }

            baseModel.data = bResult;
            return baseModel;
        }

        public BaseModel ListDriverAssetMng(NaveoOneLib.Models.DriverAsset.lstDriverAssetDto driverAssetMng, Guid sUserToken, String sConnStr)
        {
            BaseModel baseModel = new BaseModel();


            int UID = new NaveoOneLib.Services.Users.UserService().GetUserID(sUserToken, sConnStr);
            List<NaveoOneLib.Models.GMatrix.Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);
            DataTable dtAssetData = new NaveoOneLib.Services.Assets.AssetService().GetAsset(lm, sConnStr);

            string strRowCommaSeparated = "";

            int cpt = 0;
            foreach (DataRow dr in dtAssetData.Rows)
            {

                if (cpt == 0)
                {
                    strRowCommaSeparated = dr["AssetID"].ToString();
                }
                else
                {
                    strRowCommaSeparated += ", " + dr["AssetID"].ToString();
                }



                cpt++;
            }

            Connection c = new Connection(sConnStr);

            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = "select drvAsset_id, GFI_FLT_Driver.sDriverName, GFI_FLT_Asset.AssetNumber, drvAsset_DateFrom, drvAsset_DateTo, drvAsset_Remarks from GFI_PLN_DriverAsset, GFI_FLT_Asset, GFI_FLT_Driver where GFI_PLN_DriverAsset.AssetID = GFI_FLT_Asset.AssetID and GFI_PLN_DriverAsset.DriverID = GFI_FLT_Driver.DriverID and GFI_PLN_DriverAsset.AssetID IN (" + strRowCommaSeparated + ") AND drvAsset_DateFrom >= '" + driverAssetMng.StartDate.ToString("MM/dd/yyyy HH:mm:ss") + "' AND drvAsset_DateTo <= '" + driverAssetMng.EndDate.ToString("MM/dd/yyyy HH:mm:ss") + "' AND drvAsset_flagDelete <> '1' ";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;

            //drSql["sParam"] = accidentMng.StartDate.ToString("MM/dd/yyyy HH:mm:ss") + "¬" + accidentMng.StartDate.ToString("MM/dd/yyyy HH:mm:ss");
            drSql["sTableName"] = "DriverAsset";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            baseModel.data = ds;
            return baseModel;
        }
    }
}
