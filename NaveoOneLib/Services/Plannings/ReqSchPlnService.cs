﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Common;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.Drivers;
using NaveoOneLib.Services.GMatrix;

namespace NaveoOneLib.Services.Plannings
{
    public class ReqSchPlnService
    {
        public static DataTable GetDataForLatLon(int AssetID, DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"
select *, dbo.fnCalcDistanceInKM(g.Latitude, h.Lat, g.Longitude, h.Lon) * 1000 DistanceInM
	from GFI_PLN_Scheduled s
		inner join GFI_PLN_ScheduledAsset a on a.HID = s.HID
		inner join GFI_PLN_ScheduledPlan p on p.HID = s.HID
		inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
		inner join GFI_GPS_GPSData g on g.AssetID = a.AssetID and (g.DateTimeGPS_UTC between DATEADD(mi, h.Buffer * -1, h.dtUTC) and DATEADD(mi, h.Buffer, h.dtUTC))
where a.AssetID = ?
    and (h.dtUTC between ? and ?)";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = AssetID + "¬" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");
            drSql["sTableName"] = "dtScheduled";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }

        public static DataTable GetDataForZones(int AssetID, DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"
select *, dbo.isPointInZone(g.Longitude, g.Latitude, h.ZoneID) isPointInZone
	from GFI_PLN_Scheduled s
		inner join GFI_PLN_ScheduledAsset a on a.HID = s.HID
		inner join GFI_PLN_ScheduledPlan p on p.HID = s.HID
		inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
		inner join GFI_GPS_GPSData g on g.AssetID = a.AssetID and (g.DateTimeGPS_UTC between DATEADD(mi, h.Buffer * -1, h.dtUTC) and DATEADD(mi, h.Buffer, h.dtUTC))
where a.AssetID = ?
    and (h.dtUTC between ? and ?)";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = AssetID + "¬" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");
            drSql["sTableName"] = "dtScheduled";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }
        public static DataTable GetDataForZones(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"
CREATE TABLE #Assets
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL,
	[iAssetID] [int] PRIMARY KEY CLUSTERED,
)
";
            lAssetID = lAssetID.Distinct().ToList();
            foreach (int i in lAssetID)
                sql += "insert #Assets (iAssetID) values (" + i.ToString() + ")\r\n";

            sql += @"
select *, dbo.isPointInZone(g.Longitude, g.Latitude, h.ZoneID) isPointInZone
	from GFI_PLN_Scheduled s
		inner join GFI_PLN_ScheduledAsset a on a.HID = s.HID
        inner join #Assets a1 on a1.iAssetID = a.HID
		inner join GFI_PLN_ScheduledPlan p on p.HID = s.HID
		inner join GFI_PLN_PlanningViaPointH h on p.PID = h.PID
		inner join GFI_GPS_GPSData g on g.AssetID = a.AssetID and (g.DateTimeGPS_UTC between DATEADD(mi, h.Buffer * -1, h.dtUTC) and DATEADD(mi, h.Buffer, h.dtUTC))
where (h.dtUTC between ? and ?)";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");
            drSql["sTableName"] = "dtScheduled";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }

        public static DataTable GetScheduledData(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"
CREATE TABLE #Assets
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL,
	[iAssetID] [int] PRIMARY KEY CLUSTERED,
)
";
            lAssetID = lAssetID.Distinct().ToList();
            foreach (int i in lAssetID)
                sql += "insert #Assets (iAssetID) values (" + i.ToString() + ")\r\n";

            sql += @"
select s.HID ScheduleiID, p.PID, h.SeqNo HeaderSeqNo
	, a.AssetID, a.DriverID
    , c.Surname + ' ' + c.Name Customer
    , z.Description Zone
    , v.Name VCA
    , h.dtUTC
    , dr.sDriverName Driver, p.Ref 
	, h.Lat MapLat, h.Lon MapLon
    , p.Remarks, lv.Description, lv.Value
from GFI_PLN_ScheduledPlan s
	inner join GFI_PLN_Planning p on p.PID = s.PID
    inner join GFI_SYS_GroupMatrixPlanning m on p.PID = m.iID

	inner join GFI_PLN_ScheduledAsset a on a.HID = s.HID
	inner join #Assets aa on aa.iAssetID = a.AssetID
	inner join GFI_PLN_PlanningViaPointH h on h.PID = p.PID
	inner join GFI_PLN_PlanningLkup l on  l.PID = p.PID
	inner join GFI_SYS_LookUpValues lv on lv.VID = l.LookupValueID

	left outer join GFI_SYS_CustomerMaster c on h.ClientID = c.ClientID
	left outer join GFI_FLT_ZoneHeader z on z.ZoneID = h.ZoneID
	left outer join GFI_FLT_VCAHeader v on v.VCAID = h.LocalityVCA

	left outer join GFI_FLT_Driver dr on a.DriverID = dr.DriverID

where h.dtUTC between ? and ? ";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = dtFrom.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss.fff");
            drSql["sTableName"] = "dtScheduledPIDs";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            DataTable dtResult = new DataTable();
            dtResult.Columns.Add("ScheduleiID");
            dtResult.Columns.Add("AssetID");
            dtResult.Columns.Add("DriverID");
            dtResult.Columns.Add("ViaPoints");
            dtResult.Columns.Add("dtFr");
            dtResult.Columns.Add("dtto");
            dtResult.Columns.Add("DriverName");
            dtResult.Columns.Add("Remarks");
            dtResult.Columns.Add("Lkup");
            dtResult.Columns.Add("LkupColor");

            DataView view = new DataView(ds.Tables[0]);
            DataTable distinctValues = view.ToTable(true, "ScheduleiID");

            foreach(DataRow dr in distinctValues.Rows )
            {
                int i = Convert.ToInt32(dr["ScheduleiID"]);
                DataTable dt = ds.Tables[0].Select("ScheduleiID = " + i).CopyToDataTable();
                DataRow r = dtResult.NewRow();

                r["ScheduleiID"] = dr["ScheduleiID"];
                r["AssetID"] = dt.Rows[0]["AssetID"];
                r["DriverID"] = dt.Rows[0]["DriverID"];

                DataView v = dt.DefaultView;
                v.Sort = "dtUTC";
                DataTable sortedTable = v.ToTable();

                DateTime min = sortedTable.Rows[0].Field<DateTime>("dtUTC");
                DateTime max = sortedTable.Rows[sortedTable.Rows.Count - 1].Field<DateTime>("dtUTC");
                r["dtFr"] = min;
                r["dtto"] = max;

                String sVia = String.Empty;
                foreach (DataRow drr in sortedTable.Rows)
                    if (!sVia.Contains(drr["Customer"].ToString()))
                        sVia += drr["Customer"].ToString() + ", ";

                foreach (DataRow drr in sortedTable.Rows)
                    if (!sVia.Contains(drr["Zone"].ToString()))
                        sVia += drr["Zone"].ToString() + ", ";

                foreach (DataRow drr in sortedTable.Rows)
                    if (!sVia.Contains(drr["VCA"].ToString()))
                        sVia += drr["VCA"].ToString() + ", ";

                foreach (DataRow drr in sortedTable.Rows)
                    if (!sVia.Contains(drr["MapLat"].ToString()))
                        sVia += drr["MapLat"].ToString() + ", " + drr["MapLat"].ToString() + ", ";

                if (sVia.Length > 2)
                    sVia = sVia.Substring(0, sVia.Length - 2);
                r["ViaPoints"] = sVia;


                r["DriverName"]= dt.Rows[0]["Driver"];
                r["Remarks"]=dt.Rows[0]["Remarks"];
                r["Lkup"]=dt.Rows[0]["Description"];

                foreach (DataRow drr in dt.Rows)
                    if (!String.IsNullOrEmpty(drr["Value"].ToString()))
                    {
                        r["LkupColor"] = drr["Value"];
                        break;
                    }

                dtResult.Rows.Add(r);
            }

            return dtResult;
        }
        public static DataTable GetScheduledPIDs(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"
CREATE TABLE #Assets
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL,
	[iAssetID] [int] PRIMARY KEY CLUSTERED,
)
";
            lAssetID = lAssetID.Distinct().ToList();
            foreach (int i in lAssetID)
                sql += "insert #Assets (iAssetID) values (" + i.ToString() + ")\r\n";

            sql += @"
select h.iID HeaderiID, s.HID, p.PID, pln.RequestCode
from GFI_PLN_Scheduled s 
	inner join GFI_PLN_ScheduledAsset a on a.HID = s.HID
	inner join #Assets aa on aa.iAssetID = a.AssetID
	inner join GFI_PLN_ScheduledPlan p on p.HID = s.HID
	inner join GFI_PLN_PlanningViaPointH h on h.PID = p.PID
    inner join GFI_PLN_Planning pln on pln.PID = p.PID
where (h.dtUTC between ? and ?)";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = dtFrom.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss.fff");
            drSql["sTableName"] = "dtScheduledPIDs";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }
        public static DataTable GetHappeningData(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"
CREATE TABLE #Assets
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL,
	[iAssetID] [int] PRIMARY KEY CLUSTERED,
)
";
            lAssetID = lAssetID.Distinct().ToList();
            foreach (int i in lAssetID)
                sql += "insert #Assets (iAssetID) values (" + i.ToString() + ")\r\n";

            sql += @"
--isPointInZone
;with CTE as
(
	select h.iID HeaderiID, s.HID, a.AssetID Asset, a.DriverID Driver, p.PID, h.SeqNo, h.dtUTC, h.Buffer, h.ZoneID, g.*, dbo.isPointInZone(g.Longitude, g.Latitude, h.ZoneID) isPointInZone
	from GFI_PLN_Scheduled s 
		inner join GFI_PLN_ScheduledAsset a on a.HID = s.HID
		inner join #Assets aa on aa.iAssetID = a.AssetID
		inner join GFI_PLN_ScheduledPlan p on p.HID = s.HID
		inner join GFI_PLN_PlanningViaPointH h on h.PID = p.PID
		left outer join GFI_GPS_GPSData g on g.AssetID = a.AssetID and (g.DateTimeGPS_UTC between DATEADD(mi, h.Buffer * -1, h.dtUTC) and DATEADD(mi, h.Buffer, h.dtUTC))
	where h.ZoneID is not null
		and (h.dtUTC between ? and ?)
)
select HeaderiID, HID, Asset, Driver, PID, 0 ViaHiID, 0 minDistanceInM, max(isPointInZone) isPointInZone, 0 isPointInVAC, 0 InTime into #isPointInZone from CTE group by HID, Asset, Driver, PID, HeaderiID

alter table #isPointInZone add ZName nvarchar(250)
alter table #isPointInZone add RCode nvarchar(14)
alter table #isPointInZone add Ref nvarchar(250)
alter table #isPointInZone add Officer nvarchar(250)
alter table #isPointInZone add Pname nvarchar(250)
alter table #isPointInZone add Pname1 nvarchar(250)

update #isPointInZone set InTime = 1 where isPointInZone = 1

update #isPointInZone 
	set ViaHiID = T2.iID
from #isPointInZone T1
	inner join GFI_PLN_PlanningViaPointH T2 on t2.PID = t1.PID

update #isPointInZone 
	set ZName = SUBSTRING (T2.Description, 0,250)
from #isPointInZone T1
	inner join GFI_PLN_PlanningViaPointH h on T1.HeaderiID = h.iID
	inner join GFI_FLT_ZoneHeader T2 on t2.ZoneID = h.ZoneID

update #isPointInZone 
	set RCode = T2.RequestCode
from #isPointInZone T1
	inner join GFI_PLN_Planning T2 on t2.PID = t1.PID

update #isPointInZone 
	set ref = T2.Ref
from #isPointInZone T1
	inner join GFI_PLN_Planning T2 on t2.PID = t1.PID

update #isPointInZone 
	set Officer = d.sDriverName
from #isPointInZone T1
	inner join GFI_PLN_PlanningResource T2 on t2.PID = t1.PID and T2.ResourceType = 'OFFICER'
	inner join GFI_FLT_Driver d on d.DriverID = T2.DriverID

update #isPointInZone 
	set PName = SUBSTRING (T2.Surname, 0,250) + ' ' + SUBSTRING (T2.Name, 0,250)
from #isPointInZone T1
	inner join GFI_PLN_PlanningViaPointH h on T1.HeaderiID = h.iID
	inner join GFI_SYS_CustomerMaster T2 on t2.ClientID = h.ClientID

update #isPointInZone 
	set PName1 = h.Remarks
from #isPointInZone T1
	inner join GFI_PLN_PlanningViaPointH h on T1.HeaderiID = h.iID

--isPointInVCA
;with CTE as
(
	select h.iID HeaderiID, s.HID, a.AssetID Asset, a.DriverID Driver, p.PID, h.SeqNo, h.dtUTC, h.Buffer, h.ZoneID, g.*, dbo.isPointInVCA(g.Longitude, g.Latitude, h.LocalityVCA) isPointInVCA
	from GFI_PLN_Scheduled s 
		inner join GFI_PLN_ScheduledAsset a on a.HID = s.HID
		inner join #Assets aa on aa.iAssetID = a.AssetID
		inner join GFI_PLN_ScheduledPlan p on p.HID = s.HID
		inner join GFI_PLN_PlanningViaPointH h on h.PID = p.PID
		left outer join GFI_GPS_GPSData g on g.AssetID = a.AssetID and (g.DateTimeGPS_UTC between DATEADD(mi, h.Buffer * -1, h.dtUTC) and DATEADD(mi, h.Buffer, h.dtUTC))
	where h.LocalityVCA is not null
		and (h.dtUTC between ? and ?)
)
select HeaderiID, HID, Asset, Driver, PID, 0 ViaHiID, 0 minDistanceInM, 0 isPointInZone, max(isPointInVCA) isPointInVCA, 0 InTime into #isPointInVCA from CTE group by HID, Asset, Driver, PID, HeaderiID

alter table #isPointInVCA add ZName nvarchar(250)
alter table #isPointInVCA add RCode nvarchar(14)
alter table #isPointInVCA add Ref nvarchar(250)
alter table #isPointInVCA add Officer nvarchar(250)
alter table #isPointInVCA add Pname nvarchar(250)
alter table #isPointInVCA add Pname1 nvarchar(250)

update #isPointInVCA set InTime = 1 where isPointInVCA = 1

update #isPointInVCA 
	set ViaHiID = T2.iID
from #isPointInVCA T1
	inner join GFI_PLN_PlanningViaPointH T2 on t2.PID = t1.PID

update #isPointInVCA 
	set ZName = SUBSTRING (T2.Name, 0,250)
from #isPointInVCA T1
	inner join GFI_PLN_PlanningViaPointH h on T1.HeaderiID = h.iID 
	inner join GFI_FLT_VCAHeader T2 on t2.VCAID = h.LocalityVCA

update #isPointInVCA 
	set RCode = T2.RequestCode
from #isPointInVCA T1
	inner join GFI_PLN_Planning T2 on t2.PID = t1.PID

update #isPointInVCA 
	set ref = T2.Ref
from #isPointInZone T1
	inner join GFI_PLN_Planning T2 on t2.PID = t1.PID

update #isPointInVCA 
	set Officer = d.sDriverName
from #isPointInZone T1
	inner join GFI_PLN_PlanningResource T2 on t2.PID = t1.PID and T2.ResourceType = 'OFFICER'
	inner join GFI_FLT_Driver d on d.DriverID = T2.DriverID

update #isPointInVCA 
	set PName = SUBSTRING (T2.Surname, 0,250) + ' ' + SUBSTRING (T2.Name, 0,250)
from #isPointInZone T1
	inner join GFI_PLN_PlanningViaPointH h on T1.HeaderiID = h.iID
	inner join GFI_SYS_CustomerMaster T2 on t2.ClientID = h.ClientID

update #isPointInVCA 
	set PName1 = h.Remarks
from #isPointInZone T1
	inner join GFI_PLN_PlanningViaPointH h on T1.HeaderiID = h.iID

--isPointInNearLatLon
;with CTE as
(
	select h.iID HeaderiID, h.iID, s.HID, a.AssetID Asset, a.DriverID Driver, p.PID, h.SeqNo, h.dtUTC, h.Buffer, h.ZoneID, g.*, dbo.fnCalcDistanceInKM(g.Latitude, h.Lat, g.Longitude, h.Lon) * 1000 DistanceInM
	from GFI_PLN_Scheduled s 
		inner join GFI_PLN_ScheduledAsset a on a.HID = s.HID
		inner join #Assets aa on aa.iAssetID = a.AssetID
		inner join GFI_PLN_ScheduledPlan p on p.HID = s.HID
		inner join GFI_PLN_PlanningViaPointH h on h.PID = p.PID
		left outer join GFI_GPS_GPSData g on g.AssetID = a.AssetID and (g.DateTimeGPS_UTC between DATEADD(mi, h.Buffer * -1, h.dtUTC) and DATEADD(mi, h.Buffer, h.dtUTC))
	where h.Lon is not null
		and (h.dtUTC between ? and ?)
)
select HeaderiID, HID, Asset, Driver, PID, 0 ViaHiID, min(DistanceInM) minDistanceInM, 0 isPointInZone, 0 isPointInVAC, 0 InTime into #DistanceInM from CTE group by HID, Asset, Driver, PID, HeaderiID

alter table #DistanceInM add ZName nvarchar(250)
alter table #DistanceInM add RCode nvarchar(14)
alter table #DistanceInM add Ref nvarchar(250)
alter table #DistanceInM add Officer nvarchar(250)
alter table #DistanceInM add Pname nvarchar(250)
alter table #DistanceInM add Pname1 nvarchar(250)

update #DistanceInM set InTime = 1 where minDistanceInM <= 250
update #DistanceInM 
	set ViaHiID = T2.iID
from #DistanceInM T1
	inner join GFI_PLN_PlanningViaPointH T2 on t2.PID = t1.PID

update #DistanceInM 
	set ZName = CAST(h.Lat as varchar(15)) + ', ' + CAST(h.Lon as varchar(15))
from #DistanceInM T1
	inner join GFI_PLN_PlanningViaPointH h on T1.HeaderiID = h.iID 

update #DistanceInM 
	set RCode = T2.RequestCode
from #DistanceInM T1
	inner join GFI_PLN_Planning T2 on t2.PID = t1.PID

update #DistanceInM 
	set ref = T2.Ref
from #isPointInZone T1
	inner join GFI_PLN_Planning T2 on t2.PID = t1.PID

update #DistanceInM 
	set Officer = d.sDriverName
from #isPointInZone T1
	inner join GFI_PLN_PlanningResource T2 on t2.PID = t1.PID and T2.ResourceType = 'OFFICER'
	inner join GFI_FLT_Driver d on d.DriverID = T2.DriverID

update #DistanceInM 
	set PName = SUBSTRING (T2.Surname, 0,250) + ' ' + SUBSTRING (T2.Name, 0,250)
from #isPointInZone T1
	inner join GFI_PLN_PlanningViaPointH h on T1.HeaderiID = h.iID
	inner join GFI_SYS_CustomerMaster T2 on t2.ClientID = h.ClientID

update #DistanceInM 
	set PName1 = h.Remarks
from #isPointInZone T1
	inner join GFI_PLN_PlanningViaPointH h on T1.HeaderiID = h.iID

select *, DATEADD(mi, h.Buffer * -1, h.dtUTC) dtFr, DATEADD(mi, h.Buffer, h.dtUTC) dtTo, d.sDriverName, a.AssetNumber
	from #isPointInZone r 
		inner join GFI_PLN_PlanningViaPointH h on r.HeaderiID = h.iID
		inner join GFI_FLT_Asset a on a.AssetID = r.Asset
		inner join GFI_FLT_Driver d on d.DriverID = r.Driver
union
select *, DATEADD(mi, h.Buffer * -1, h.dtUTC) dtFr, DATEADD(mi, h.Buffer, h.dtUTC) dtTo, d.sDriverName, a.AssetNumber
	from #isPointInVCA r 
		inner join GFI_PLN_PlanningViaPointH h on r.HeaderiID = h.iID
		inner join GFI_FLT_Asset a on a.AssetID = r.Asset
		inner join GFI_FLT_Driver d on d.DriverID = r.Driver
union
select *, DATEADD(mi, h.Buffer * -1, h.dtUTC) dtFr, DATEADD(mi, h.Buffer, h.dtUTC) dtTo, d.sDriverName, a.AssetNumber
	from #DistanceInM r 
		inner join GFI_PLN_PlanningViaPointH h on r.HeaderiID = h.iID
		inner join GFI_FLT_Asset a on a.AssetID = r.Asset
		inner join GFI_FLT_Driver d on d.DriverID = r.Driver

/*
drop table #isPointInZone
drop table #isPointInVCA
drop table #DistanceInM
drop table #Assets
*/
";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = dtFrom.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss.fff")
                + "¬" + dtFrom.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss.fff")
                + "¬" + dtFrom.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss.fff");
            drSql["sTableName"] = "dtScheduled";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            //foreach (DataRow dr in ds.Tables[0].Rows)
            //    dr["AssetNumber"] = BaseService.Decrypt(dr["AssetNumber"].ToString());

            return ds.Tables[0];
        }
        public static DataTable GetPlanningList(DateTime dtFrom, DateTime dtTo, int UID, String sConnStr)
        {
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = sPlanningList(iParentIDs, null, sConnStr);

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");
            drSql["sTableName"] = "dtList";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }
        public static DataTable GetAllViaPoints(List<int> lPIDs, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = sPlanningList(null, lPIDs, sConnStr);

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtList";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }
        public static DataTable GetTripParamsPerPID(int PID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"
declare @PID int
set @PID = " + PID + @"

declare @dtFrom datetime
declare @dtTo datetime
declare @AssetID int

select @dtFrom = dtUTC from GFI_PLN_PlanningViaPointH where pid = @PID and SeqNo = 1
select @dtTo = dtUTC from GFI_PLN_PlanningViaPointH where pid = @PID and SeqNo = 999
select @AssetID = AssetID from GFI_PLN_ScheduledAsset a 
	inner join GFI_PLN_ScheduledPlan p on p.HID = a.HID
where pid = @PID

--select @dtFrom dtFrom, @dtTo dtTo, @AssetID AssetID

select @dtFrom = max(dtStart) from GFI_GPS_TripHeader where dtStart <= @dtFrom and AssetID = @AssetID
select @dtTo = min(dtEnd) from GFI_GPS_TripHeader where dtEnd >= @dtTo and AssetID = @AssetID
if(@dtTo is null)
	select @dtTo = dtUTC from GFI_PLN_PlanningViaPointH where pid = @PID and SeqNo = 999

select @dtFrom dtFrom, @dtTo dtTo, @AssetID AssetID
";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtTripParams";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }
        public static DataTable GetTripParamsPerHID(int HID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"
declare @HID int
set @HID = " + HID + @"

declare @dtFrom datetime
declare @dtTo datetime
declare @AssetIDs VARCHAR(8000)

select @dtFrom = min(dtUTC) from GFI_PLN_ScheduledPlan s
	inner join GFI_PLN_PlanningViaPointH h on s.PID = h.PID
where s.HID = @HID and h.SeqNo = 1

select @dtTo = max(dtUTC) from GFI_PLN_ScheduledPlan s
	inner join GFI_PLN_PlanningViaPointH h on s.PID = h.PID
where s.HID = @HID and h.SeqNo = 999

select @AssetIDs = COALESCE(@AssetIDs + ', ', '') + ISNULL(CAST(a.AssetID as nvarchar(50)), '0') 
from GFI_PLN_ScheduledAsset a 
	inner join GFI_PLN_ScheduledPlan p on p.HID = a.HID
where p.HID = @HID

select @dtFrom = max(dtStart) 
	from GFI_GPS_TripHeader h
		inner join GFI_PLN_ScheduledAsset a on a.AssetID = h.AssetID
		inner join GFI_PLN_ScheduledPlan p on p.HID = a.HID
	where dtStart <= @dtFrom 

select @dtTo = min(dtEnd) 
	from GFI_GPS_TripHeader h
		inner join GFI_PLN_ScheduledAsset a on a.AssetID = h.AssetID
		inner join GFI_PLN_ScheduledPlan p on p.HID = a.HID
	where dtEnd >= @dtTo

if(@dtTo is null)
	select @dtTo = max(dtUTC) from GFI_PLN_ScheduledPlan s
		inner join GFI_PLN_PlanningViaPointH h on s.PID = h.PID
	where s.HID = @HID and h.SeqNo = 999

select @dtFrom dtFrom, @dtTo dtTo, @AssetIDs AssetID
";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtTripParams";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }
        public static DataSet GetRelatedPIDs(int PID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"
select t1.PID 
from GFI_PLN_ScheduledPlan t1
	inner join (select HID from GFI_PLN_ScheduledPlan where PID = " + PID + ") t2 on t1.HID = t2.HID";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtIDs";
            dtSql.Rows.Add(drSql);

            sql = @"select Approval from GFI_PLN_Planning where pid =" + PID;
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtPln";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds;
        }
        public static Boolean Unschedule(int PID, int UID, String sConnStr)
        {
            String sql = @"
insert GFI_SYS_Audit(auditDate, auditType, ReferenceCode, ReferenceTable, UserID)
	select GETDATE(), 'D', t1.PID, 'GFI_PLN_Scheduled', " + UID + @" from GFI_PLN_ScheduledPlan t1
			inner join (select HID from GFI_PLN_ScheduledPlan where PID = " + PID + @") t2 on t1.HID = t2.HID

update GFI_PLN_Planning set approval = 0
	where PID in (
					select t1.PID
					from GFI_PLN_ScheduledPlan t1
						inner join (select HID from GFI_PLN_ScheduledPlan where PID = " + PID + @") t2 on t1.HID = t2.HID
					)

delete from GFI_PLN_PlanningResource
     where PID in (
                     select t1.PID
                     from GFI_PLN_ScheduledPlan t1
                        inner join (select HID from GFI_PLN_ScheduledPlan where PID = " + PID + @") t2 on t1.HID = t2.HID
                    )

delete from GFI_PLN_Scheduled
	where HID in (
					select t1.HID
					from GFI_PLN_ScheduledPlan t1
						inner join (select HID from GFI_PLN_ScheduledPlan where PID = " + PID + @") t2 on t1.HID = t2.HID
					)
";
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["Command"] = sql;
            drCtrl["TimeOut"] = 30;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dtCtrl);

            DataSet dsResult = new DataSet();
            dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }

        public static String sPlanningList(List<int> iParentIDs, List<int> lPIDs, String sConnStr)
        {
            //0 - Not Scheduled
            //100 - Scheduled
            //500 - closed

            String sql = @"
select h.iID HeaderID, h.SeqNo HeaderSeqNo
    , c.Surname PName
    , h.Remarks PName1
    , u.Description UOM
    , z.Description Add1
    , v.Name Add2, cast (h.Lat as nvarchar(20)) + ', ' + cast(h.Lon as nvarchar(20)) Add3
    , c.Tel1, c.Tel2, c.MobileNumber
    , p.Type FetchConvey
    , h.dtUTC
    , p.Approval, p.RequestCode, p.Urgent, p.PID, dr.sDriverName Officer, p.sComments Officer1, p.ClosureComments, p.Ref 
	, h.Lat MapLat, h.Lon MapLon, zd.Latitude ZonLat, zd.Longitude ZonLon, vd.Latitude VCALat, vd.Longitude VCALon
	, s.HID ScheduleiID
    , sch.Remarks
into #myTable
from GFI_PLN_PlanningViaPointH h
	inner join GFI_PLN_PlanningViaPointD d on h.iID = d.HID
	inner join GFI_PLN_Planning p on h.PID = p.PID
    inner join GFI_SYS_GroupMatrixPlanning m on p.PID = m.iID
	left outer join GFI_SYS_CustomerMaster c on h.ClientID = c.ClientID
	left outer join GFI_FLT_ZoneHeader z on z.ZoneID = h.ZoneID
	left outer join GFI_FLT_ZoneDetail zd on zd.CordOrder = 0 and zd.ZoneID = z.ZoneID
	left outer join GFI_FLT_VCAHeader v on v.VCAID = h.LocalityVCA
	left outer join GFI_FLT_VCADetail vd on vd.CordOrder = 0 and vd.VCAID = v.VCAID
	left outer join GFI_SYS_UOM u on u.UID = d.UOM
	left outer join GFI_PLN_PlanningResource r on r.PID = p.PID and r.ResourceType  = 'OFFICER'
	left outer join GFI_FLT_Driver dr on r.DriverID = dr.DriverID
	left outer join GFI_PLN_ScheduledPlan s on s.PID = p.PID
	left outer join GFI_PLN_Scheduled sch on sch.HID = s.HID
";

            if (iParentIDs == null)
            {
                String s = "(";
                foreach (int i in lPIDs)
                    s += i + ", ";
                if (s.Length > 2)
                    s = s.Substring(0, s.Length - 2);
                s += ")";
                sql += "where h.pid in " + s;
            }
            else
                sql += " where h.dtUTC between ? and ? and m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + ")";

            sql += @"
alter table #myTable add Departure nvarchar(500)
alter table #myTable add Arrival nvarchar(500)

UPDATE #myTable 
    SET Departure = T2.Add1
FROM #myTable T1
	inner join (select PID, HeaderSeqNo, Add1 from #myTable where HeaderSeqNo = 1 and Add1 is not null) T2 on T2.PID = T1.PID and T2.HeaderSeqNo = T1.HeaderSeqNo

UPDATE #myTable 
    SET Departure = T2.Add2
FROM #myTable T1
	inner join (select PID, HeaderSeqNo, Add2 from #myTable where HeaderSeqNo = 1 and Add2 is not null) T2 on T2.PID = T1.PID and T2.HeaderSeqNo = T1.HeaderSeqNo

UPDATE #myTable 
    SET Departure = T2.Add3
FROM #myTable T1
	inner join (select PID, HeaderSeqNo, Add3 from #myTable where HeaderSeqNo = 1 and Add3 is not null) T2 on T2.PID = T1.PID and T2.HeaderSeqNo = T1.HeaderSeqNo

UPDATE #myTable 
    SET Arrival = T2.Add1
FROM #myTable T1
	inner join (select PID, HeaderSeqNo, Add1 from #myTable where HeaderSeqNo = 999 and Add1 is not null) T2 on T2.PID = T1.PID and T2.HeaderSeqNo = T1.HeaderSeqNo

UPDATE #myTable 
    SET Arrival = T2.Add2
FROM #myTable T1
	inner join (select PID, HeaderSeqNo, Add2 from #myTable where HeaderSeqNo = 999 and Add2 is not null) T2 on T2.PID = T1.PID and T2.HeaderSeqNo = T1.HeaderSeqNo

UPDATE #myTable 
    SET Arrival = T2.Add3
FROM #myTable T1
	inner join (select PID, HeaderSeqNo, Add3 from #myTable where HeaderSeqNo = 999 and Add3 is not null) T2 on T2.PID = T1.PID and T2.HeaderSeqNo = T1.HeaderSeqNo

UPDATE #myTable 
    SET Departure = T2.Departure
FROM #myTable T1
	inner join (select PID, Departure from #myTable where Departure is not null group by PID, Departure) T2 on T2.PID = T1.PID 

UPDATE #myTable 
    SET Arrival = T2.Arrival
FROM #myTable T1
	inner join (select PID, Arrival from #myTable where Arrival is not null group by PID, Arrival) T2 on T2.PID = T1.PID 

select distinct * from #myTable
";
            return sql;
        }

        public static DataTable GetRosterData(int uLogin, DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            if (dtFrom == dtTo)
                dtTo = dtTo.AddHours(2);

            String sql = @"
--drop table #myRoster

declare @dtFr DateTime
set @dtFr = DATEADD(day, 0, DATEDIFF(day, 0, ?))
set @dtFr = DATEADD(second, DATEDIFF(second, GETDATE(), GETUTCDATE()), @dtFr);

declare @dtTo DateTime
set @dtTo = DATEADD(day, 0, DATEDIFF(day, 0, ?))
set @dtTo = DATEADD(second, DATEDIFF(second, GETDATE(), GETUTCDATE()), @dtTo);

--if(@dtFr = @dtTo)
--	set @dtTo = DATEADD(day, 1, @dtFr)

create table #myRoster
(
	GRID int
	, DynTeamID int
	, AssetID int
	, DriverID int
	, OfficerID int
	, AssetName nvarchar(100)
	, DriverName nvarchar(100)
	, OfficerName nvarchar(100)
	, rDate DateTime
	, DateFr DateTime
	, DateTo DateTime
)
insert #myRoster (DynTeamID, rDate)
	select r.DynTeamID, r.rDate
		from GFI_RST_RosterHeader h 
			inner join GFI_RST_Roster r on r.GRID = h.iID
        where r.rDate between @dtFr and @dtTo --and h.isOriginal = 0
	group by r.DynTeamID, r.rDate

update #myRoster 
	set GRID = T2.GRID, AssetID = T2.AssetID, DateFr = T2.TimeFr, DateTo = T2.TimeTo
from #myRoster T1
	inner join 
		(
			select r.GRID, r.DynTeamID, r.AssetID, r.DriverID, r.rDate
				, r.rDate + DATEADD(day, 0 - DATEDIFF(day, 0, s.TimeFr), s.TimeFr) TimeFr
				, r.rDate + DATEADD(day, 0 - DATEDIFF(day, 0, s.TimeTo), s.TimeTo) TimeTo
			from GFI_RST_RosterHeader h
				inner join GFI_RST_Roster r on r.GRID = h.iID --and h.isOriginal = 0
				inner join GFI_RST_Shift s on r.SID = s.SID
		) T2 on T2.rDate = T1.rDate and T2.DynTeamID = T1.DynTeamID

--Driver
update #myRoster 
	set DriverID = d.DriverID, DriverName = d.sDriverName
from #myRoster T1
	inner join 
		(
			select r.GRID, r.DynTeamID, r.AssetID, r.DriverID, r.rDate
				, DATEADD(day, 0, DATEDIFF(day, 0, r.rDate)) + DATEADD(day, 0 - DATEDIFF(day, 0, s.TimeFr), s.TimeFr) TimeFr
				, DATEADD(day, 0, DATEDIFF(day, 0, r.rDate)) + DATEADD(day, 0 - DATEDIFF(day, 0, s.TimeTo), s.TimeTo) TimeTo
			from GFI_RST_RosterHeader h
				inner join GFI_RST_Roster r on r.GRID = h.iID
				inner join GFI_RST_Shift s on r.SID = s.SID
		) T2 on T2.rDate = T1.rDate and T2.DynTeamID = T1.DynTeamID
	inner join GFI_FLT_Driver d on d.DriverID = t2.DriverID and d.EmployeeType = 'Driver'
    inner join GFI_SYS_GroupMatrixDriver m on d.DriverID = m.iID where m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + @")
--Officer
update #myRoster 
	set OfficerID = d.DriverID, OfficerName = d.sDriverName
from #myRoster T1
	inner join 
		(
			select r.GRID, r.DynTeamID, r.AssetID, r.DriverID, r.rDate--, s.TimeFr, s.TimeTo
				, DATEADD(day, 0, DATEDIFF(day, 0, r.rDate)) + DATEADD(day, 0 - DATEDIFF(day, 0, s.TimeFr), s.TimeFr) TimeFr
				, DATEADD(day, 0, DATEDIFF(day, 0, r.rDate)) + DATEADD(day, 0 - DATEDIFF(day, 0, s.TimeTo), s.TimeTo) TimeTo
			from GFI_RST_RosterHeader h
				inner join GFI_RST_Roster r on r.GRID = h.iID
				inner join GFI_RST_Shift s on r.SID = s.SID		) T2 on T2.rDate = T1.rDate and T2.DynTeamID = T1.DynTeamID
	inner join GFI_FLT_Driver d on d.DriverID = t2.DriverID and d.EmployeeType != 'Driver'
    inner join GFI_SYS_GroupMatrixDriver m on d.DriverID = m.iID where m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + @")

--Should delete instead of update
update #myRoster set DriverID = -1 where DriverID is null
update #myRoster set OfficerID = -1 where OfficerID is null

select rank() OVER (ORDER BY Grid, DynTeamID, DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), rDate) ) iID, * from #myRoster where AssetID is not null
";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");
            drSql["sTableName"] = "dtRoster";
            dtSql.Rows.Add(drSql);

            sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Assets);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMAsset";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            //foreach (DataRow dr in ds.Tables["dtGMAsset"].Rows)
            //    if (dr[0].ToString().Contains("-"))
            //        dr[1] = BaseService.Decrypt(dr[1].ToString());

            foreach (DataRow dr in ds.Tables["dtRoster"].Rows)
                dr["AssetName"] = ds.Tables["dtGMAsset"].Select("StrucID = " + dr["AssetID"].ToString())[0]["GMDescription"].ToString();

            return ds.Tables["dtRoster"];
        }

        static DataTable GetPlanningListObsolete(DateTime dtFrom, DateTime dtTo, int UID, String sConnStr)
        {
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"
select h.iID HeaderID, h.SeqNo HeaderSeqNo
    , c.Surname PName
    , h.Remarks PName1
    , u.Description UOM
    , z.Description Add1
    , v.Name Add2, cast (h.Lat as nvarchar(20)) + ', ' + cast(h.Lon as nvarchar(20)) Add3
    , c.Tel1, c.Tel2, c.MobileNumber
    , p.Type FetchConvey
    , h.dtUTC
    , p.Approval, p.RequestCode, p.Urgent, p.PID, dr.sDriverName Officer, p.sComments Officer1, p.ClosureComments, p.Ref 
	, h.Lat MapLat, h.Lon MapLon, zd.Latitude ZonLat, zd.Longitude ZonLon, vd.Latitude VCALat, vd.Longitude VCALon
	, s.HID ScheduleiID
    , sch.Remarks
from GFI_PLN_PlanningViaPointH h
	inner join GFI_PLN_PlanningViaPointD d on h.iID = d.HID
	inner join GFI_PLN_Planning p on h.PID = p.PID
    inner join GFI_SYS_GroupMatrixPlanning m on p.PID = m.iID
	left outer join GFI_SYS_CustomerMaster c on h.ClientID = c.ClientID
	left outer join GFI_FLT_ZoneHeader z on z.ZoneID = h.ZoneID
	left outer join GFI_FLT_ZoneDetail zd on zd.CordOrder = 0 and zd.ZoneID = z.ZoneID
	left outer join GFI_FLT_VCAHeader v on v.VCAID = h.LocalityVCA
	left outer join GFI_FLT_VCADetail vd on vd.CordOrder = 0 and vd.VCAID = v.VCAID
	left outer join GFI_SYS_UOM u on u.UID = d.UOM
	left outer join GFI_PLN_PlanningResource r on r.PID = p.PID and r.ResourceType  = 'OFFICER'
	left outer join GFI_FLT_Driver dr on r.DriverID = dr.DriverID
	left outer join GFI_PLN_ScheduledPlan s on s.PID = p.PID
	left outer join GFI_PLN_Scheduled sch on sch.HID = s.HID
where h.dtUTC between ? and ? and m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + ")";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");
            drSql["sTableName"] = "dtList";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }
        static DataTable GetHappeningDataObsolete(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"
CREATE TABLE #Assets
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL,
	[iAssetID] [int] PRIMARY KEY CLUSTERED,
)
";
            lAssetID = lAssetID.Distinct().ToList();
            foreach (int i in lAssetID)
                sql += "insert #Assets (iAssetID) values (" + i.ToString() + ")\r\n";

            sql += @"
--isPointInZone
;with CTE as
(
	select s.HID, a.AssetID Asset, a.DriverID Driver, p.PID, h.SeqNo, h.dtUTC, h.Buffer, h.ZoneID, g.*, dbo.isPointInZone(g.Longitude, g.Latitude, h.ZoneID) isPointInZone
		from GFI_PLN_Scheduled s 
			inner join GFI_PLN_ScheduledAsset a on a.HID = s.HID
			inner join #Assets aa on aa.iAssetID = a.AssetID
			inner join GFI_PLN_ScheduledPlan p on p.HID = s.HID
			inner join GFI_PLN_PlanningViaPointH h on h.PID = p.PID
			left outer join GFI_GPS_GPSData g on g.AssetID = a.AssetID and (g.DateTimeGPS_UTC between DATEADD(mi, h.Buffer * -1, h.dtUTC) and DATEADD(mi, h.Buffer, h.dtUTC))
		where h.ZoneID is not null
)
select HID, Asset, Driver, PID, 0 ViaHiID, 0 minDistanceInM, max(isPointInZone) isPointInZone, 0 isPointInVAC, 0 InTime into #isPointInZone from CTE group by HID, Asset, Driver, PID

alter table #isPointInZone add ZName nvarchar(250)
alter table #isPointInZone add RCode nvarchar(14)

update #isPointInZone set InTime = 1 where isPointInZone = 1

update #isPointInZone 
	set ViaHiID = T2.iID
from #isPointInZone T1
	inner join GFI_PLN_PlanningViaPointH T2 on t2.PID = t1.PID

update #isPointInZone 
	set ZName = SUBSTRING (T2.Description, 0,250)
from #isPointInZone T1
	inner join GFI_PLN_PlanningViaPointH h on T1.PID = h.PID and T1.ViaHiID = h.iID
	inner join GFI_FLT_ZoneHeader T2 on t2.ZoneID = h.ZoneID

update #isPointInZone 
	set RCode = T2.RequestCode
from #isPointInZone T1
	inner join GFI_PLN_Planning T2 on t2.PID = t1.PID


--isPointInVCA
;with CTE as
(
	select s.HID, a.AssetID Asset, a.DriverID Driver, p.PID, h.SeqNo, h.dtUTC, h.Buffer, h.ZoneID, g.*, dbo.isPointInVCA(g.Longitude, g.Latitude, h.LocalityVCA) isPointInVCA
		from GFI_PLN_Scheduled s 
			inner join GFI_PLN_ScheduledAsset a on a.HID = s.HID
			inner join #Assets aa on aa.iAssetID = a.AssetID
			inner join GFI_PLN_ScheduledPlan p on p.HID = s.HID
			inner join GFI_PLN_PlanningViaPointH h on h.PID = p.PID
			left outer join GFI_GPS_GPSData g on g.AssetID = a.AssetID and (g.DateTimeGPS_UTC between DATEADD(mi, h.Buffer * -1, h.dtUTC) and DATEADD(mi, h.Buffer, h.dtUTC))
		where h.LocalityVCA is not null
)
select HID, Asset, Driver, PID, 0 ViaHiID, 0 minDistanceInM, 0 isPointInZone, max(isPointInVCA) isPointInVCA, 0 InTime into #isPointInVCA from CTE group by HID, Asset, Driver, PID

alter table #isPointInVCA add ZName nvarchar(250)
alter table #isPointInVCA add RCode nvarchar(14)

update #isPointInVCA set InTime = 1 where isPointInVCA = 1

update #isPointInVCA 
	set ViaHiID = T2.iID
from #isPointInVCA T1
	inner join GFI_PLN_PlanningViaPointH T2 on t2.PID = t1.PID

update #isPointInVCA 
	set ZName = SUBSTRING (T2.Name, 0,250)
from #isPointInVCA T1
	inner join GFI_PLN_PlanningViaPointH h on T1.PID = h.PID and T1.ViaHiID = h.iID
	inner join GFI_FLT_VCAHeader T2 on t2.VCAID = h.LocalityVCA

update #isPointInVCA 
	set RCode = T2.RequestCode
from #isPointInVCA T1
	inner join GFI_PLN_Planning T2 on t2.PID = t1.PID


--isPointInNearLatLon
;with CTE as
(
	select s.HID, a.AssetID Asset, a.DriverID Driver, p.PID, h.SeqNo, h.dtUTC, h.Buffer, h.ZoneID, g.*, dbo.fnCalcDistanceInKM(g.Latitude, h.Lat, g.Longitude, h.Lon) * 1000 DistanceInM
		from GFI_PLN_Scheduled s 
			inner join GFI_PLN_ScheduledAsset a on a.HID = s.HID
			inner join #Assets aa on aa.iAssetID = a.AssetID
			inner join GFI_PLN_ScheduledPlan p on p.HID = s.HID
			inner join GFI_PLN_PlanningViaPointH h on h.PID = p.PID
			left outer join GFI_GPS_GPSData g on g.AssetID = a.AssetID and (g.DateTimeGPS_UTC between DATEADD(mi, h.Buffer * -1, h.dtUTC) and DATEADD(mi, h.Buffer, h.dtUTC))
		where h.Lon is not null
)
select HID, Asset, Driver, PID, 0 ViaHiID, min(DistanceInM) minDistanceInM, 0 isPointInZone, 0 isPointInVAC, 0 InTime into #DistanceInM from CTE group by HID, Asset, Driver, PID

alter table #DistanceInM add ZName nvarchar(250)
alter table #DistanceInM add RCode nvarchar(14)

update #DistanceInM set InTime = 1 where minDistanceInM <= 250
update #DistanceInM 
	set ViaHiID = T2.iID
from #DistanceInM T1
	inner join GFI_PLN_PlanningViaPointH T2 on t2.PID = t1.PID

update #DistanceInM 
	set ZName = 'h.Lat h.Lon'
from #DistanceInM T1
	inner join GFI_PLN_PlanningViaPointH h on T1.PID = h.PID and T1.ViaHiID = h.iID

update #DistanceInM 
	set RCode = T2.RequestCode
from #DistanceInM T1
	inner join GFI_PLN_Planning T2 on t2.PID = t1.PID




select *, DATEADD(mi, h.Buffer * -1, h.dtUTC) dtFr, DATEADD(mi, h.Buffer, h.dtUTC) dtTo
	from #isPointInZone r 
		inner join GFI_PLN_PlanningViaPointH h on r.PID = h.PID and r.ViaHiID = h.iID
union
select *, DATEADD(mi, h.Buffer * -1, h.dtUTC) dtFr, DATEADD(mi, h.Buffer, h.dtUTC) dtTo
	from #isPointInVCA r 
		inner join GFI_PLN_PlanningViaPointH h on r.PID = h.PID and r.ViaHiID = h.iID
union
select *, DATEADD(mi, h.Buffer * -1, h.dtUTC) dtFr, DATEADD(mi, h.Buffer, h.dtUTC) dtTo
	from #DistanceInM r 
		inner join GFI_PLN_PlanningViaPointH h on r.PID = h.PID and r.ViaHiID = h.iID

/*
drop table #isPointInZone
drop table #isPointInVCA
drop table #DistanceInM
drop table #Assets
*/";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");
            drSql["sTableName"] = "dtScheduled";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }
        static DataTable GetHappeningData1(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"
CREATE TABLE #Assets
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL,
	[iAssetID] [int] PRIMARY KEY CLUSTERED,
)
";
            lAssetID = lAssetID.Distinct().ToList();
            foreach (int i in lAssetID)
                sql += "insert #Assets (iAssetID) values (" + i.ToString() + ")\r\n";

            sql += @"
--isPointInZone
;with CTE as
(
	select h.iID HeaderiID, s.HID, a.AssetID Asset, a.DriverID Driver, p.PID, h.SeqNo, h.dtUTC, h.Buffer, h.ZoneID, g.*, dbo.isPointInZone(g.Longitude, g.Latitude, h.ZoneID) isPointInZone
        , p.Ref, dr.sDriverName Officer
        , c.Surname PName
        , h.Remarks PName1
		from GFI_PLN_Scheduled s 
			inner join GFI_PLN_ScheduledAsset a on a.HID = s.HID
			inner join #Assets aa on aa.iAssetID = a.AssetID
			inner join GFI_PLN_ScheduledPlan p on p.HID = s.HID
			inner join GFI_PLN_PlanningViaPointH h on h.PID = p.PID
			left outer join GFI_GPS_GPSData g on g.AssetID = a.AssetID and (g.DateTimeGPS_UTC between DATEADD(mi, h.Buffer * -1, h.dtUTC) and DATEADD(mi, h.Buffer, h.dtUTC))
	
            left outer join GFI_PLN_PlanningResource r on r.PID = p.PID and r.ResourceType  = 'OFFICER'
	        left outer join GFI_SYS_CustomerMaster c on h.ClientID = c.ClientID
	        left outer join GFI_FLT_Driver dr on r.DriverID = dr.DriverID
		where h.ZoneID is not null
)
select HeaderiID, HID, Asset, Driver, PID, 0 ViaHiID, 0 minDistanceInM, max(isPointInZone) isPointInZone, 0 isPointInVAC, 0 InTime into #isPointInZone from CTE group by HID, Asset, Driver, PID, HeaderiID

alter table #isPointInZone add ZName nvarchar(250)
alter table #isPointInZone add RCode nvarchar(14)

update #isPointInZone set InTime = 1 where isPointInZone = 1

update #isPointInZone 
	set ViaHiID = T2.iID
from #isPointInZone T1
	inner join GFI_PLN_PlanningViaPointH T2 on t2.PID = t1.PID

update #isPointInZone 
	set ZName = SUBSTRING (T2.Description, 0,250)
from #isPointInZone T1
	inner join GFI_PLN_PlanningViaPointH h on T1.PID = h.PID and T1.ViaHiID = h.iID
	inner join GFI_FLT_ZoneHeader T2 on t2.ZoneID = h.ZoneID

update #isPointInZone 
	set RCode = T2.RequestCode
from #isPointInZone T1
	inner join GFI_PLN_Planning T2 on t2.PID = t1.PID


--isPointInVCA
;with CTE as
(
	select h.iID HeaderiID, s.HID, a.AssetID Asset, a.DriverID Driver, p.PID, h.SeqNo, h.dtUTC, h.Buffer, h.ZoneID, g.*, dbo.isPointInVCA(g.Longitude, g.Latitude, h.LocalityVCA) isPointInVCA
		from GFI_PLN_Scheduled s 
			inner join GFI_PLN_ScheduledAsset a on a.HID = s.HID
			inner join #Assets aa on aa.iAssetID = a.AssetID
			inner join GFI_PLN_ScheduledPlan p on p.HID = s.HID
			inner join GFI_PLN_PlanningViaPointH h on h.PID = p.PID
			left outer join GFI_GPS_GPSData g on g.AssetID = a.AssetID and (g.DateTimeGPS_UTC between DATEADD(mi, h.Buffer * -1, h.dtUTC) and DATEADD(mi, h.Buffer, h.dtUTC))
		where h.LocalityVCA is not null
)
select HeaderiID, HID, Asset, Driver, PID, 0 ViaHiID, 0 minDistanceInM, 0 isPointInZone, max(isPointInVCA) isPointInVCA, 0 InTime into #isPointInVCA from CTE group by HID, Asset, Driver, PID, HeaderiID

alter table #isPointInVCA add ZName nvarchar(250)
alter table #isPointInVCA add RCode nvarchar(14)

update #isPointInVCA set InTime = 1 where isPointInVCA = 1

update #isPointInVCA 
	set ViaHiID = T2.iID
from #isPointInVCA T1
	inner join GFI_PLN_PlanningViaPointH T2 on t2.PID = t1.PID

update #isPointInVCA 
	set ZName = SUBSTRING (T2.Name, 0,250)
from #isPointInVCA T1
	inner join GFI_PLN_PlanningViaPointH h on T1.PID = h.PID and T1.ViaHiID = h.iID
	inner join GFI_FLT_VCAHeader T2 on t2.VCAID = h.LocalityVCA

update #isPointInVCA 
	set RCode = T2.RequestCode
from #isPointInVCA T1
	inner join GFI_PLN_Planning T2 on t2.PID = t1.PID


--isPointInNearLatLon
;with CTE as
(
	select h.iID HeaderiID, h.iID, s.HID, a.AssetID Asset, a.DriverID Driver, p.PID, h.SeqNo, h.dtUTC, h.Buffer, h.ZoneID, g.*, dbo.fnCalcDistanceInKM(g.Latitude, h.Lat, g.Longitude, h.Lon) * 1000 DistanceInM
		from GFI_PLN_Scheduled s 
			inner join GFI_PLN_ScheduledAsset a on a.HID = s.HID
			inner join #Assets aa on aa.iAssetID = a.AssetID
			inner join GFI_PLN_ScheduledPlan p on p.HID = s.HID
			inner join GFI_PLN_PlanningViaPointH h on h.PID = p.PID
			left outer join GFI_GPS_GPSData g on g.AssetID = a.AssetID and (g.DateTimeGPS_UTC between DATEADD(mi, h.Buffer * -1, h.dtUTC) and DATEADD(mi, h.Buffer, h.dtUTC))
		where h.Lon is not null
)
select HeaderiID, HID, Asset, Driver, PID, 0 ViaHiID, min(DistanceInM) minDistanceInM, 0 isPointInZone, 0 isPointInVAC, 0 InTime into #DistanceInM from CTE group by HID, Asset, Driver, PID, HeaderiID

alter table #DistanceInM add ZName nvarchar(250)
alter table #DistanceInM add RCode nvarchar(14)

update #DistanceInM set InTime = 1 where minDistanceInM <= 250
update #DistanceInM 
	set ViaHiID = T2.iID
from #DistanceInM T1
	inner join GFI_PLN_PlanningViaPointH T2 on t2.PID = t1.PID

update #DistanceInM 
	set ZName = 'h.Lat h.Lon'
from #DistanceInM T1
	inner join GFI_PLN_PlanningViaPointH h on T1.PID = h.PID and T1.ViaHiID = h.iID

update #DistanceInM 
	set RCode = T2.RequestCode
from #DistanceInM T1
	inner join GFI_PLN_Planning T2 on t2.PID = t1.PID




select *, DATEADD(mi, h.Buffer * -1, h.dtUTC) dtFr, DATEADD(mi, h.Buffer, h.dtUTC) dtTo
	from #isPointInZone r 
		inner join GFI_PLN_PlanningViaPointH h on r.HeaderiID = h.iID
union
select *, DATEADD(mi, h.Buffer * -1, h.dtUTC) dtFr, DATEADD(mi, h.Buffer, h.dtUTC) dtTo
	from #isPointInVCA r 
		inner join GFI_PLN_PlanningViaPointH h on r.HeaderiID = h.iID
union
select *, DATEADD(mi, h.Buffer * -1, h.dtUTC) dtFr, DATEADD(mi, h.Buffer, h.dtUTC) dtTo
	from #DistanceInM r 
		inner join GFI_PLN_PlanningViaPointH h on r.HeaderiID = h.iID

/*
drop table #isPointInZone
drop table #isPointInVCA
drop table #DistanceInM
drop table #Assets
*/
";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");
            drSql["sTableName"] = "dtScheduled";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables[0];
        }
        static DataTable GetRosterDataObsolete(int uLogin, DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"select r.GRID, r.DynTeamID, r.AssetID, r.DriverID, r.rDate--, s.TimeFr, s.TimeTo
	                            , DATEADD(day, 0, DATEDIFF(day, 0, r.rDate)) + DATEADD(day, 0 - DATEDIFF(day, 0, s.TimeFr), s.TimeFr) TimeFr
	                            , DATEADD(day, 0, DATEDIFF(day, 0, r.rDate)) + DATEADD(day, 0 - DATEDIFF(day, 0, s.TimeTo), s.TimeTo) TimeTo
                            from GFI_RST_RosterHeader h
	                            inner join GFI_RST_Roster r on r.GRID = h.iID
	                            inner join GFI_RST_Shift s on r.SID = s.SID
                            where r.rDate between ? and ? and h.isOriginal = 0";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");
            drSql["sTableName"] = "dtRoster";
            dtSql.Rows.Add(drSql);

            sql = @"select r.DynTeamID, r.rDate
	                    from GFI_RST_Roster r
                    where r.rDate between ? and ?
                        group by r.DynTeamID, r.rDate";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");
            drSql["sTableName"] = "dtGroup";
            dtSql.Rows.Add(drSql);

            List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Assets);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMAsset";
            dtSql.Rows.Add(drSql);

            //sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, "Drivers");
            //drSql = dtSql.NewRow();
            //drSql["sSQL"] = sql;
            //drSql["sTableName"] = "dtGMDriver";
            //dtSql.Rows.Add(drSql);

            sql = "select DriverID, sDriverName, EmployeeType from GFI_FLT_Driver";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtResources";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            //foreach (DataRow dr in ds.Tables["dtGMAsset"].Rows)
            //    if (dr[0].ToString().Contains("-"))
            //        dr[1] = BaseService.Decrypt(dr[1].ToString());

            DataTable dt = new DataTable();
            dt.Columns.Add("GRID", typeof(int));
            dt.Columns.Add("DynTeamID", typeof(int));
            dt.Columns.Add("AssetID", typeof(int));
            dt.Columns.Add("DriverID", typeof(int));
            dt.Columns.Add("OfficerID", typeof(int));
            dt.Columns.Add("AssetName", typeof(String));
            dt.Columns.Add("DriverName", typeof(String));
            dt.Columns.Add("OfficerName", typeof(String));
            dt.Columns.Add("DateFr", typeof(DateTime));
            dt.Columns.Add("DateTo", typeof(DateTime));

            foreach (DataRow dr in ds.Tables["dtGroup"].Rows)
            {
                DataRow[] rowsPerTeam = ds.Tables["dtRoster"].Select("rDate='" + dr["rDate"].ToString() + "' and DynTeamID = " + dr["DynTeamID"].ToString());

                int iGrid = 0;
                int iDynTeam = 0;
                int iAssetID = 0;
                int iDriverID = 0;
                String sDriverName = String.Empty;
                int iOfficerID = 0;
                String sOfficerName = String.Empty;
                for (int i = 0; i < rowsPerTeam.Length; i++)
                {
                    if (i == 0)
                    {
                        iGrid = Convert.ToInt32(rowsPerTeam[0]["GRID"]);
                        iDynTeam = Convert.ToInt32(rowsPerTeam[0]["DynTeamID"]);
                        iAssetID = Convert.ToInt32(rowsPerTeam[0]["AssetID"]);
                    }

                    int x = Convert.ToInt32(rowsPerTeam[i]["DriverID"]);
                    DataRow[] drx = ds.Tables["dtResources"].Select("DriverID = " + x.ToString());
                    foreach (DataRow y in drx)
                    {
                        if (y["EmployeeType"].ToString() == Driver.ResourceType.Driver.ToString())
                        {
                            iDriverID = Convert.ToInt32(rowsPerTeam[i]["DriverID"]);
                            sDriverName = y["sDriverName"].ToString();
                        }
                        else
                        {
                            iOfficerID = Convert.ToInt32(rowsPerTeam[i]["DriverID"]);
                            sOfficerName = y["sDriverName"].ToString();
                        }
                    }
                }

                DataRow r = dt.NewRow();
                r["GRID"] = iGrid;
                r["DynTeamID"] = iDynTeam;
                r["AssetID"] = iAssetID;
                r["DriverID"] = iDriverID;
                r["OfficerID"] = iOfficerID;
                r["AssetName"] = ds.Tables["dtGMAsset"].Select("StrucID = " + iAssetID.ToString())[0]["GMDescription"].ToString();
                r["DriverName"] = sDriverName;
                r["OfficerName"] = sOfficerName;
                r["DateFr"] = rowsPerTeam[0]["TimeFr"];
                r["DateTo"] = rowsPerTeam[0]["TimeTo"];
                dt.Rows.Add(r);
            }

            return dt;
        }
    }
}
