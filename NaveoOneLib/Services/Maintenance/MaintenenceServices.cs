﻿using NaveoOneLib.DBCon;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoOneLib.Services.Maintenance
{
    public class MaintenenceServices
    {
        DataTable MaintenanceVehicleDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "dbo.\"GFI_AMM_VehicleMaintenance\"";
            dt.Columns.Add("\"URI\"", typeof(int));
            dt.Columns.Add("\"AssetId\"", typeof(int));
            dt.Columns.Add("\"MaintTypeId_cbo\"", typeof(int));
            dt.Columns.Add("\"ContactDetails\"", typeof(String));
            dt.Columns.Add("\"CompanyName\"", typeof(String));
            dt.Columns.Add("\"PhoneNumber\"", typeof(String));
            dt.Columns.Add("\"CompanyRef\"", typeof(String));
            dt.Columns.Add("\"CompanyRef2\"", typeof(String));
            dt.Columns.Add("\"MaintDescription\"", typeof(String));
            dt.Columns.Add("\"StartDate\"", typeof(DateTime));
            dt.Columns.Add("\"EndDate\"", typeof(DateTime));
            dt.Columns.Add("\"MaintStatusId_cbo\"", typeof(int));
            dt.Columns.Add("\"EstimatedValue\"", typeof(double));
            dt.Columns.Add("\"VATInclInItemsAmt\"", typeof(String));
            dt.Columns.Add("\"VATAmount\"", typeof(double));
            dt.Columns.Add("\"TotalCost\"", typeof(double));
            dt.Columns.Add("\"CoverTypeId_cbo\"", typeof(int));
            dt.Columns.Add("\"CalculatedOdometer\"", typeof(int));
            dt.Columns.Add("\"ActualOdometer\"", typeof(int));
            dt.Columns.Add("\"CalculatedEngineHrs\"", typeof(int));
            dt.Columns.Add("\"ActualEngineHrs\"", typeof(int));
            dt.Columns.Add("\"AdditionalInfo\"", typeof(String));
            dt.Columns.Add("\"CreatedDate\"", typeof(DateTime));
            dt.Columns.Add("\"CreatedBy\"", typeof(String));
            dt.Columns.Add("\"UpdatedDate\"", typeof(DateTime));
            dt.Columns.Add("\"UpdatedBy\"", typeof(String));
            dt.Columns.Add("\"AssetStatus\"", typeof(String));
            dt.Columns.Add("\"Comment\"", typeof(String));
            dt.Columns.Add("\"Reminder\"", typeof(DateTime));
            dt.Columns.Add("\"NextMaintenance\"", typeof(DateTime));
            dt.Columns.Add("\"flagTriggeredByScheduler\"", typeof(int));
            dt.Columns.Add("\"parentURI\"", typeof(int));
            dt.Columns.Add("oprType", typeof(DataRowState));

            return dt;
        }

        public class detailsOfEngineHours
        {
            public int AssetId { get; set; }
            public double EngineHours { get; set; }
            public double Odometers { get; set; }

        }

        public class detailsOfAsset
        {
            public int AssetId { get; set; }
            public DateTime StartDate { get; set; }
        }

        public String ProcessScheduler(String sConnStr)
        {
            //TODO get servername by connection

            StringBuilder sb = new StringBuilder();

            String sResult = String.Empty;

            List<detailsOfEngineHours> lstOdometerAndEngineHours = new List<detailsOfEngineHours>();

            List<detailsOfAsset> lstDetailOfAsset = new List<detailsOfAsset>();

            detailsOfEngineHours EngineHour = new detailsOfEngineHours();

            detailsOfAsset detailsOfAsset = new detailsOfAsset();

            String ServerName = DBConn.Common.ConnName.GetDBName(sConnStr);

            //DBConn.Common.ConnName.GetDBName(sConnStr);

            //NaveoOneLib.DBCon.Common.GetAPIConnStr(sConnStr);

            try
            {
                Connection c = new Connection("MaintDB");

                DataTable dtCtrl = c.CTRLTBL();
                DataRow drCtrl = dtCtrl.NewRow();
                DataSet dsProcess = new DataSet();
                DataTable dtCase1 = MaintenanceVehicleDT();
                DataTable dtCase2 = MaintenanceVehicleDT();
                DataTable dtCase3 = MaintenanceVehicleDT();


                // 
                DataTable dtSql = c.dtSql();
                DataRow drSql = dtSql.NewRow();

                int URI = 0;

                //Get Status Id 2, 3
                String sql = "select 'ListStatus' as TableName;\r\n";
                sql += "select vm.\"URI\", vm.\"AssetId\", vm.\"MaintTypeId_cbo\" from dbo.\"GFI_AMM_VehicleMaintenance\" vm, dbo.\"GFI_AMM_VehicleMaintTypes\" vmt, dbo.\"FleetServer\" fs, dbo.\"CustomerVehicle\" cv where vm.\"MaintStatusId_cbo\" IN (2, 3) AND vm.\"MaintTypeId_cbo\" = vmt.\"MaintTypeId\"  and vmt.\"OccurrenceDuration\" > 0 and vm.\"EndDate\" < GETDATE() and cv.\"Id\" = vm.\"AssetId\" and fs.\"Id\" = cv.\"FleetServerId\" and fs.\"Name\" = '" + ServerName + "' ;\r\n";

                //Case 2
                sql += "select 'GetOdometerReading' as TableName;\r\n";
                sql += "with cte as (select cv.\"FMSAssetId\", vm.*, vmt.\"OccurrenceKM\", vmt.\"OccurrenceType\", vmt.\"OccurrenceKMTh\" , row_number() over(partition by vm.\"AssetId\", vm.\"MaintTypeId_cbo\" order by vm.\"StartDate\" desc) as RowNum from dbo.\"GFI_AMM_VehicleMaintenance\" vm, dbo.\"GFI_AMM_VehicleMaintTypes\" vmt, dbo.\"CustomerVehicle\" cv, dbo.\"FleetServer\" fs where vm.\"MaintStatusId_cbo\" IN (5,20) AND vm.\"MaintTypeId_cbo\" = vmt.\"MaintTypeId\" and cv.\"Id\" = vm.\"AssetId\" and vmt.\"OccurrenceKM\" > 0 and fs.\"Id\" = cv.\"FleetServerId\" and fs.\"Name\" = '" + ServerName + "') select * from cte where RowNum = 1 ;\r\n";

                //Case 3
                sql += "select 'FinalReading' as TableName;\r\n";
                sql += "select cv.\"FMSAssetId\", vm.*, vmt.\"OccurrenceKM\" from dbo.\"GFI_AMM_VehicleMaintenance\" vm, dbo.\"GFI_AMM_VehicleMaintTypes\" vmt, dbo.\"CustomerVehicle\" cv, dbo.\"FleetServer\" fs where vm.\"MaintStatusId_cbo\" IN (2) AND vm.\"MaintTypeId_cbo\" = vmt.\"MaintTypeId\" and cv.\"Id\" = vm.\"AssetId\" and vmt.\"OccurrenceKM\" > 0 and fs.\"Id\" = cv.\"FleetServerId\" and fs.\"Name\" = '" + ServerName + "';\r\n";
                sql += "\r\n --PostgreSQLGMS";

                sb.Append(sql);

                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "MultipleDTfromQuery";
                dtSql.Rows.Add(drSql);

                DataSet ds = c.GetDataDS(dtSql, NaveoOneLib.Constant.NaveoEntity.MaintDB);
                DataTable dtGM = ds.Tables["ListStatus"];

                //Case 1
                if (dtGM.Rows.Count > 0)
                {
                    foreach (DataRow item in dtGM.Rows)
                    {
                        DataRow drVehicle = dtCase1.NewRow();
                        //MaintTypeId_cbo = 4
                        if (item["MaintTypeId_cbo"].ToString() == "4")
                        {
                            sb.Append(item["URI"]);
                            drVehicle["\"MaintStatusId_cbo\""] = 7;
                            drVehicle["\"URI\""] = item["URI"];
                        }
                        else
                        {
                            drVehicle["\"MaintStatusId_cbo\""] = 1;
                            drVehicle["\"URI\""] = item["URI"];
                        }

                        drVehicle["oprType"] = DataRowState.Modified;
                        dtCase1.Rows.Add(drVehicle);
                    }

                    drCtrl = dtCtrl.NewRow();
                    drCtrl["SeqNo"] = 1;
                    drCtrl["TblName"] = dtCase1.TableName;
                    drCtrl["CmdType"] = "TABLE";
                    drCtrl["KeyFieldName"] = "\"URI\"";
                    drCtrl["KeyIsIdentity"] = "TRUE";
                    drCtrl["NextIdFieldName"] = URI;

                    dtCtrl.Rows.Add(drCtrl);
                    dsProcess.Merge(dtCase1);
                }

                int a = 0;
                try
                {
                    //Case 2
                    DataTable dtStatus = ds.Tables["GetOdometerReading"];

                    if (dtStatus.Rows.Count > 0)
                    {
                        /*foreach (DataRow item in dtStatus.Rows)
                        {
                            detailsOfAsset.AssetId = int.Parse(item["FMSAssetId"].ToString());

                            detailsOfAsset.StartDate = DateTime.Parse(item["StartDate"].ToString());

                            lstDetailOfAsset.Add(detailsOfAsset);

                        }*/

                        foreach (DataRow item in dtStatus.Rows)
                        {
                            a = int.Parse(item["FMSAssetId"].ToString());
                            DataTable dtSqlFleet = c.dtSql();
                            DataRow drSqlFleet = dtSqlFleet.NewRow();

                            DataRow drVehicleTrip = dtCase2.NewRow();
                            //overdue and to scheduler do
                            if (item["EndDate"] != null)
                            {

                                //Check Odometer Entry from Maintenance

                                /* DataTable dtSqlMaintDBOdometre = c.dtSql();
                                 DataRow drSqlMaintDBOdometre = dtSqlMaintDBOdometre.NewRow();

                                 String sqlMaintBDOdometer = "select 'ListStatus' as TableName\r\n";
                                 sqlMaintBDOdometer += "select * from dbo.\"GFI_AMM_VehicleMaintenance\" where \"AssetId\" = '" + item["AssetId"] + "' and \"MaintTypeId_cbo\" = 20 and \"ActualOdometer\" IS NOT NULL order by \"CreatedDate\" desc\r\n";

                                 drSqlMaintDBOdometre = dtSqlMaintDBOdometre.NewRow();
                                 drSqlMaintDBOdometre["sSQL"] = sqlMaintBDOdometer;
                                 drSqlMaintDBOdometre["sTableName"] = "MultipleDTfromQuery";
                                 dtSqlMaintDBOdometre.Rows.Add(drSqlMaintDBOdometre);

                                 DataSet dsOdometre = c.GetDataDS(dtSqlMaintDBOdometre, NaveoOneLib.Constant.NaveoEntity.MaintDB);

                                 DataTable dtListOdometer = dsOdometre.Tables["ListStatus"];*/

                                String StartDate = string.Empty;

                                int ActualOdometer = 0;

                                /*if (dtListOdometer.Rows.Count > 0) //To schedule already exist do not create new one
                                {
                                    DataRow rowStatus = dtListOdometer.Rows[0];

                                    if (rowStatus["StartDate"].ToString() != string.Empty)
                                    {
                                        StartDate = DateTime.Parse(rowStatus["StartDate"].ToString()).ToString("yyyy/MM/dd HH:mm:ss");
                                    }

                                    if (rowStatus["ActualOdometer"].ToString() != string.Empty)
                                    {
                                        ActualOdometer = Convert.ToInt32(rowStatus["ActualOdometer"]);
                                    }

                                }
                                //End Odometer entry
                                if (string.IsNullOrEmpty(StartDate))
                                {
                                    StartDate = DateTime.Parse(item["EndDate"].ToString()).ToString("yyyy/MM/dd HH:mm:ss");
                                }*/

                                //Get On record only

                                //Start date to change
                                String sqlFleetValue = "\r\n --PostgreSQLROND \r\n";
                                sqlFleetValue += "select 'ListStatus' as TableName\r\n";
                                sqlFleetValue += "select ROUND(COALESCE(SUM(h.TripDistance),0), 0) Odometer,COALESCE(SUM(h.TripTime),0) EngineHours from GFI_GPS_TripHeader h where h.AssetId = '" + item["FMSAssetId"] + "' and dtStart >= '" + item["EndDate"] + "' and dtStart <= '" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "'\r\n";

                                drSqlFleet = dtSqlFleet.NewRow();
                                drSqlFleet["sSQL"] = sqlFleetValue;
                                drSqlFleet["sTableName"] = "MultipleDTfromQuery";
                                dtSqlFleet.Rows.Add(drSqlFleet);

                                DataSet dsListStatus = c.GetDataDS(dtSqlFleet, sConnStr);

                                DataTable dtList = dsListStatus.Tables["ListStatus"];

                                if (dtList.Rows.Count > 0)
                                {
                                    DataRow row = dtList.Rows[0];
                                    int EngineHours = Convert.ToInt32(row["EngineHours"]);
                                    int Odometers = Convert.ToInt32(row["Odometer"]);
                                    EngineHours = EngineHours / 3600;

                                    if (item["ActualEngineHrs"].ToString() != string.Empty)
                                    {
                                        EngineHour.EngineHours = EngineHours + int.Parse(item["ActualEngineHrs"].ToString());
                                    }
                                    else
                                    {
                                        EngineHour.EngineHours = EngineHours;
                                    }


                                    //EngineHour.Odometers = Odometers + ActualOdometer;


                                    DataRow drVehicleToSchedule = dtCase2.NewRow();

                                    int calculatedEngineHrs = 0;

                                    int calculatedOdometer = 0;

                                    int OccurrenceKMTh = 0;

                                    int OccurrenceKM = 0;

                                    if (item["CalculatedEngineHrs"].ToString() != string.Empty)
                                    {
                                        calculatedEngineHrs = int.Parse(item["CalculatedEngineHrs"].ToString());
                                    }

                                    if (item["CalculatedOdometer"].ToString() != string.Empty)
                                    {
                                        calculatedOdometer = int.Parse(item["CalculatedOdometer"].ToString());
                                    }

                                    if (item["OccurrenceKM"].ToString() != string.Empty)
                                    {
                                        OccurrenceKM = int.Parse(item["OccurrenceKM"].ToString());
                                    }

                                    if (item["OccurrenceKMTh"].ToString() != string.Empty)
                                    {
                                        OccurrenceKMTh = int.Parse(item["OccurrenceKMTh"].ToString());
                                    }
                                    if (item["ActualOdometer"].ToString() != string.Empty)
                                    {
                                        ActualOdometer = int.Parse(item["ActualOdometer"].ToString());
                                    }

                                    /* if (calculatedOdometer > 0)
                                     {
                                         EngineHour.Odometers = Odometers + ActualOdometer;
                                     }
                                     else
                                     {*/
                                    EngineHour.Odometers = Odometers;
                                    //}

                                    if (EngineHour.Odometers >= OccurrenceKMTh && EngineHour.Odometers <= OccurrenceKM)
                                    {

                                        DataTable dtSqlGetStatus = c.dtSql();

                                        DataRow drSqlGetStatus = dtSqlGetStatus.NewRow();

                                        String sqlGetStatus = "select 'ListStatus' as TableName\r\n";
                                        sqlGetStatus += "select * from dbo.\"GFI_AMM_VehicleMaintenance\" vm  where vm.\"MaintStatusId_cbo\" = 2 and vm.\"MaintTypeId_cbo\" = '" + item["MaintTypeId_cbo"] + "' and vm.\"AssetId\" = '" + item["AssetId"] + "' \r\n";

                                        drSqlGetStatus = dtSqlGetStatus.NewRow();
                                        drSqlGetStatus["sSQL"] = sqlGetStatus;
                                        drSqlGetStatus["sTableName"] = "MultipleDTfromQuery";
                                        dtSqlGetStatus.Rows.Add(drSqlGetStatus);

                                        DataSet dsGetValue = c.GetDataDS(dtSqlGetStatus, NaveoOneLib.Constant.NaveoEntity.MaintDB);

                                        DataTable dtGetValue = dsGetValue.Tables["ListStatus"];

                                        int URIStatus = 0;

                                        if (dtGetValue.Rows.Count > 0) //To schedule already exist do not create new one
                                        {
                                            DataRow rowStatus = dtGetValue.Rows[0];

                                            URIStatus = int.Parse(rowStatus["URI"].ToString());

                                        }
                                        if (URIStatus == 0 && int.Parse(item["OccurrenceType"].ToString()) != 3)
                                        {
                                            drVehicleToSchedule["\"AssetId\""] = item["AssetId"];
                                            drVehicleToSchedule["\"MaintTypeId_cbo\""] = item["MaintTypeId_cbo"];
                                            drVehicleToSchedule["\"MaintStatusId_cbo\""] = 2;
                                            drVehicleToSchedule["\"CreatedDate\""] = DateTime.Now;
                                            drVehicleToSchedule["\"CreatedBy\""] = "GMS SCHEDULER";
                                            drVehicleToSchedule["\"CalculatedOdometer\""] = calculatedOdometer;
                                            //drVehicleToSchedule["\"flagTriggeredByScheduler\""] = 2; //ToSchedule
                                            //drVehicleToSchedule["\"parentURI\""] = item["URI"];
                                            drVehicleToSchedule["oprType"] = DataRowState.Added;
                                            dtCase2.Rows.Add(drVehicleToSchedule);
                                        }
                                    }
                                    //Add new case when extreme case of overdue if there is one to schedule
                                    //
                                    else if (EngineHour.Odometers > OccurrenceKM) //If insert once
                                    {

                                        //Condition 1 IF No overdue, INSERTION
                                        DataTable dtSqlGetStatus = c.dtSql();

                                        DataRow drSqlGetStatus = dtSqlGetStatus.NewRow();

                                        String sqlGetStatus = "select 'ListStatus' as TableName\r\n";
                                        sqlGetStatus += "select * from dbo.\"GFI_AMM_VehicleMaintenance\" vm  where (vm.\"MaintStatusId_cbo\" = 1 OR vm.\"MaintStatusId_cbo\" = 2) and vm.\"MaintTypeId_cbo\" = '" + item["MaintTypeId_cbo"] + "' and vm.\"AssetId\" = '" + item["AssetId"] + "' \r\n";

                                        drSqlGetStatus = dtSqlGetStatus.NewRow();
                                        drSqlGetStatus["sSQL"] = sqlGetStatus;
                                        drSqlGetStatus["sTableName"] = "MultipleDTfromQuery";
                                        dtSqlGetStatus.Rows.Add(drSqlGetStatus);

                                        DataSet dsGetValue = c.GetDataDS(dtSqlGetStatus, NaveoOneLib.Constant.NaveoEntity.MaintDB);

                                        DataTable dtGetValue = dsGetValue.Tables["ListStatus"];


                                        int URIStatus = 0;

                                        if (dtGetValue.Rows.Count > 0) //OverDue already exist do not create new one
                                        {
                                            DataRow rowStatus = dtGetValue.Rows[0];

                                            URIStatus = int.Parse(rowStatus["URI"].ToString());

                                        }

                                        //Condition 2 if item["flagTriggeredByScheduler"] != 1

                                        if (URIStatus == 0)
                                        {
                                            drVehicleToSchedule["\"AssetId\""] = item["AssetId"];
                                            drVehicleToSchedule["\"MaintTypeId_cbo\""] = item["MaintTypeId_cbo"];
                                            drVehicleToSchedule["\"MaintStatusId_cbo\""] = 1;
                                            drVehicleToSchedule["\"CreatedDate\""] = DateTime.Now;
                                            drVehicleToSchedule["\"CreatedBy\""] = "GMS SCHEDULER";
                                            //drVehicleToSchedule["\"flagTriggeredByScheduler\""] = 1; //OverDue
                                            //drVehicleToSchedule["\"parentURI\""] = item["URI"];

                                            drVehicleToSchedule["oprType"] = DataRowState.Added;
                                            dtCase2.Rows.Add(drVehicleToSchedule);
                                        }


                                    }
                                }
                            }
                        }
                        drCtrl = dtCtrl.NewRow();

                        drCtrl["SeqNo"] = 2;
                        drCtrl["TblName"] = dtCase2.TableName;
                        drCtrl["CmdType"] = "TABLE";
                        drCtrl["KeyFieldName"] = "\"URI\"";
                        drCtrl["KeyIsIdentity"] = "TRUE";
                        drCtrl["NextIdAction"] = "GET";

                        dtCtrl.Rows.Add(drCtrl);
                        dsProcess.Merge(dtCase2);
                    }

                }
                catch (Exception ex)
                {
                    string s = ex.ToString();
                    int b = a;


                }

                //Case 3
                DataTable dtFinal = ds.Tables["FinalReading"];

                //Take last completed
                DataTable dtOdometerReading = ds.Tables["GetOdometerReading"];
                //DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");
                //dtCtrl.Select("SeqNo = 1 and TblName = 'GFI_FLT_Asset'").FirstOrDefault()["NextIDValue"].ToString();

                if (dtFinal.Rows.Count > 0)
                {
                    //DateTime StartDate = new DateTime();

                    foreach (DataRow item in dtFinal.Rows)
                    {
                        DataRow[] dRow = dtOdometerReading.Select("AssetId = '" + item["AssetId"] + "'");

                        DataTable dtSqlFinal = c.dtSql();
                        DataRow drSqlFinal = dtSqlFinal.NewRow();

                        //Check Odometer Entry from Maintenance
                        //Take date from last completed servicing

                        DataTable dtSqlMaintDBOdometre = c.dtSql();
                        DataRow drSqlMaintDBOdometre = dtSqlMaintDBOdometre.NewRow();

                        String sqlMaintBDOdometer = "select 'ListStatus' as TableName\r\n";
                        sqlMaintBDOdometer += "select * from dbo.\"GFI_AMM_VehicleMaintenance\" where \"AssetId\" = '" + item["AssetId"] + "' and \"MaintTypeId_cbo\" = 20 and \"ActualOdometer\" IS NOT NULL order by \"StartDate\" desc\r\n";

                        drSqlMaintDBOdometre = dtSqlMaintDBOdometre.NewRow();
                        drSqlMaintDBOdometre["sSQL"] = sqlMaintBDOdometer;
                        drSqlMaintDBOdometre["sTableName"] = "MultipleDTfromQuery";
                        dtSqlMaintDBOdometre.Rows.Add(drSqlMaintDBOdometre);

                        DataSet dsOdometre = c.GetDataDS(dtSqlMaintDBOdometre, NaveoOneLib.Constant.NaveoEntity.MaintDB);

                        DataTable dtListOdometer = dsOdometre.Tables["ListStatus"];

                        int ActualOdometerAdmin = 0;
                        DateTime ActualOdometerStartDate = new DateTime();

                        if (dtListOdometer.Rows.Count > 0)
                        {
                            DataRow rowStatus = dtListOdometer.Rows[0];

                            ActualOdometerAdmin = Convert.ToInt32(rowStatus["ActualOdometer"]);

                            ActualOdometerStartDate = DateTime.Parse(rowStatus["StartDate"].ToString());

                        }

                        String StartDate = string.Empty;

                        int ActualOdometer = 0;

                        if (dRow.Length > 0) //To schedule already exist do not create new one
                        {
                            //DataRow rowStatus = dtListOdometer.Rows[0];
                            if (dRow[0]["EndDate"].ToString() != string.Empty)
                            {
                                StartDate = DateTime.Parse(dRow[0]["EndDate"].ToString()).ToString("yyyy/MM/dd HH:mm:ss");
                            }

                            if (dRow[0]["CalculatedOdometer"].ToString() != string.Empty)
                            {
                                ActualOdometer = Convert.ToInt32(dRow[0]["CalculatedOdometer"]);
                            }
                        }
                        //End Odometer entry

                        //if (string.IsNullOrEmpty(StartDate))
                        //{
                        //    StartDate = DateTime.Parse(DateTime.MinValue.ToString()).ToString("yyyy/MM/dd HH:mm:ss");
                        //}

                        //StartDate = DateTime.Parse(item["StartDate"].ToString());




                        if (!string.IsNullOrEmpty(StartDate))
                        {

                            String sqlFleetValue = "\r\n --PostgreSQLROND \r\n";
                            sqlFleetValue += "select 'ListStatus' as TableName;\r\n";
                            sqlFleetValue += " select ROUND(COALESCE(SUM(h.TripDistance),0), 0) Odometer,COALESCE(SUM(h.TripTime),0) EngineHours from GFI_GPS_TripHeader h where h.AssetId = '" + item["FMSAssetId"] + "' and dtStart >= '" + StartDate + "' and dtStart <= '" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "'\r\n";

                            drSqlFinal = dtSqlFinal.NewRow();
                            drSqlFinal["sSQL"] = sqlFleetValue;
                            drSqlFinal["sTableName"] = "MultipleDTfromQuery";
                            dtSqlFinal.Rows.Add(drSqlFinal);

                            DataSet dsListStatus = c.GetDataDS(dtSqlFinal, sConnStr);

                            DataTable dtListFinal = dsListStatus.Tables["ListStatus"];
                            DataRow row = dtListFinal.Rows[0];

                            int EngineHours = 0;

                            int Odometers = 0;

                            int ActualEngineHrs = 0;

                            //int ActualOdometer = 0;

                            int OccurrenceKM = 0;

                            int calculatedOdometer = 0;

                            if (ActualOdometerAdmin - ActualOdometer > 0 && ActualOdometerStartDate > DateTime.Parse(dRow[0]["EndDate"].ToString()))
                            {
                                EngineHour.Odometers = ActualOdometerAdmin - ActualOdometer;
                            }
                            else
                            {
                                if (row["Odometer"].ToString() != string.Empty)
                                {
                                    Odometers = Convert.ToInt32(row["Odometer"]);
                                    EngineHour.Odometers = Odometers;
                                }
                            }

                            if (row["EngineHours"].ToString() != string.Empty)
                            {
                                EngineHours = Convert.ToInt32(row["EngineHours"]);
                            }


                            if (item["ActualEngineHrs"].ToString() != string.Empty)
                            {
                                ActualEngineHrs = int.Parse(item["ActualEngineHrs"].ToString());
                            }

                            //if (item["ActualOdometer"].ToString() != string.Empty)
                            //{
                            //    ActualOdometer = int.Parse(item["ActualOdometer"].ToString());
                            //}

                            if (item["OccurrenceKM"].ToString() != string.Empty)
                            {
                                OccurrenceKM = int.Parse(item["OccurrenceKM"].ToString());
                            }

                            if (item["CalculatedOdometer"].ToString() != string.Empty)
                            {
                                calculatedOdometer = int.Parse(item["CalculatedOdometer"].ToString());
                            }

                            EngineHours = EngineHours / 3600;


                            EngineHour.EngineHours = EngineHours + ActualEngineHrs;

                            /* if (calculatedOdometer > 0)
                             {
                                 EngineHour.Odometers = Odometers + ActualOdometer;
                             }
                             else*/
                            //{

                            //}



                            DataRow drVehicleToSchedule = dtCase3.NewRow();

                            if (EngineHour.Odometers > OccurrenceKM)
                            {

                                DataTable dtSqlGetStatus = c.dtSql();

                                DataRow drSqlGetStatus = dtSqlGetStatus.NewRow();

                                String sqlGetStatus = "select 'ListStatus' as TableName\r\n";
                                sqlGetStatus += "select * from dbo.\"GFI_AMM_VehicleMaintenance\" vm  where vm.\"MaintStatusId_cbo\" = 1 and vm.\"MaintTypeId_cbo\" = '" + item["MaintTypeId_cbo"] + "' and vm.\"AssetId\" = '" + item["AssetId"] + "' \r\n";

                                drSqlGetStatus = dtSqlGetStatus.NewRow();
                                drSqlGetStatus["sSQL"] = sqlGetStatus;
                                drSqlGetStatus["sTableName"] = "MultipleDTfromQuery";
                                dtSqlGetStatus.Rows.Add(drSqlGetStatus);

                                DataSet dsGetValue = c.GetDataDS(dtSqlGetStatus, NaveoOneLib.Constant.NaveoEntity.MaintDB);

                                DataTable dtGetValue = dsGetValue.Tables["ListStatus"];


                                int URIStatus = 0;

                                if (dtGetValue.Rows.Count > 0) //OverDue already exist do not create new one
                                {
                                    DataRow rowStatus = dtGetValue.Rows[0];

                                    URIStatus = int.Parse(rowStatus["URI"].ToString());

                                }

                                if (URIStatus == 0)
                                {
                                    drVehicleToSchedule["\"URI\""] = item["URI"];
                                    drVehicleToSchedule["\"MaintStatusId_cbo\""] = 1;

                                    drVehicleToSchedule["oprType"] = DataRowState.Modified;
                                    dtCase3.Rows.Add(drVehicleToSchedule);
                                }
                            }
                        }

                    }
                    drCtrl = dtCtrl.NewRow();

                    drCtrl["SeqNo"] = 3;
                    drCtrl["TblName"] = dtCase3.TableName;
                    drCtrl["CmdType"] = "TABLE";
                    drCtrl["KeyFieldName"] = "\"URI\"";
                    drCtrl["KeyIsIdentity"] = "TRUE";
                    drCtrl["NextIdFieldName"] = URI;

                    dtCtrl.Rows.Add(drCtrl);
                    dsProcess.Merge(dtCase3);
                }

                dsProcess.Merge(dtCtrl);

                //Process
                DataSet dsResult = c.ProcessData(dsProcess, NaveoOneLib.Constant.NaveoEntity.MaintDB);
                //DataSet dsResult = c.ProcessData(dsProcess, "TEST");
                dtCtrl = dsResult.Tables["CTRLTBL"];

                if (dtCtrl != null)
                {
                    DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                    if (dRow.Length > 0)
                    {
                        //baseModel.dbResult = dsResult;
                        //baseModel.dbResult.Tables["CTRLTBL"].TableName = "ResultCtrlTbl";
                        //baseModel.dbResult.Merge(dsProcess);
                        //bResult = false;
                    }
                    else
                    { }
                    // bResult = true;
                }

            }
            catch (Exception ex)
            {
                sResult += "\r\n Trips failed with error " + ex.Message;
            }
            //NavLib.Data.Logger.WriteToLog(sb.ToString());
            //File.AppendAllText(@"C:\" + "log.txt", sb.ToString());
            //sb.Clear();

            return sResult + sb.ToString();
        }
    }
}
