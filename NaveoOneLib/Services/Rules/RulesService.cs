using System;
using System.Collections.Generic;
using System.Linq;
using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Reportings;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Services.Reportings;
using NaveoOneLib.Services.Audits;
using NaveoOneLib.Models.Rules;

namespace NaveoOneLib.Services.Rules
{
    public class RulesService
    {
        Errors er = new Errors();

        public String sGetRules(List<Matrix> lMatrix, string ruleCategory, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            return sGetRules(iParentIDs, ruleCategory ,sConnStr);
        }
        public String sGetRules(List<int> iParentIDs, string ruleCategory,String sConnStr)
        {

   
            string sqlwhere = string.Empty;

            if (ruleCategory == "engine")
                sqlwhere = "and r.StrucValue = '0' and r.Struc = 'Exception Type'";

            if (ruleCategory == "driving")
                sqlwhere = "and r.StrucValue = '1' and r.Struc = 'Exception Type'";

            if (ruleCategory == "planning")
                sqlwhere = "and r.StrucValue = '2' and r.Struc = 'Exception Type'";

            if (ruleCategory == "maintenance")
                sqlwhere = "and r.StrucValue = '3' and r.Struc = 'Exception Type'";

            if (ruleCategory == "fuel")
            {
                sqlwhere = "where r.Struc = 'Fuel' ";
                
            }
                

            String sql = @"SELECT r.RuleID, 
                        r.ParentRuleID, 
                        r.RuleType, 
                        r.MinTriggerValue, 
                        r.MaxTriggerValue, 
                        r.Struc, 
                        r.StrucValue, 
                        r.StrucCondition,
                        r.StrucType,
                        r.RuleName 
                        from GFI_GPS_Rules r, GFI_SYS_GroupMatrixRule m
                        where ParentRuleID = RuleID
                            and r.RuleID = m.iID  " + sqlwhere + " and m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + ") order by RuleID";



            if (ruleCategory == "fuel")
            {

              sql = sql.Substring(0, sql.IndexOf("and m.GMID") + 1);
              sql = sql.TrimEnd('a');
              sql = sql.Replace(@", GFI_SYS_GroupMatrixRule m
                        where ParentRuleID = RuleID
                            and r.RuleID = m.iID","");
            }


            return sql;



            


        }

        public DataTable GetRules(List<Matrix> lMatrix,string ruleCategory, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            return GetRules(iParentIDs, ruleCategory, sConnStr);
        }
        public DataTable GetRules(List<int> iParentIDs, string ruleCategory, String sConnStr)
        {
            DataTable dt = new Connection(sConnStr).GetDataDT(sGetRules(iParentIDs, ruleCategory, sConnStr), sConnStr);
            return dt;
        }

        Models.Rules.Rule GetRules(DataSet ds)
        {
            DataRow[] drPrim = ds.Tables["dtRules"].Select("RuleID = ParentRuleID");
            DataRow[] drOthers = ds.Tables["dtRules"].Select("RuleID <> ParentRuleID");

            Models.Rules.Rule r = new Models.Rules.Rule();
            r.RuleID = Convert.ToInt32(drPrim[0]["RuleID"]);
            r.ParentRuleID = Convert.ToInt32(drPrim[0]["ParentRuleID"]);
            r.RuleType = Convert.ToInt32(drPrim[0]["RuleType"]);
            r.MinTriggerValue = Convert.ToInt32(drPrim[0]["MinTriggerValue"]);
            r.MaxTriggerValue = Convert.ToInt32(drPrim[0]["MaxTriggerValue"]);
            r.Struc = drPrim[0]["Struc"].ToString();
            r.StrucValue = drPrim[0]["StrucValue"].ToString();
            r.StrucCondition = drPrim[0]["StrucCondition"].ToString();
            r.RuleName = drPrim[0]["RuleName"].ToString();
            r.StrucType = drPrim[0]["StrucType"].ToString();

            List<Models.Rules.Rule> lr = new List<Models.Rules.Rule>();
            foreach (DataRow dr in drOthers)
            {
                Models.Rules.Rule ro = new Models.Rules.Rule();
                ro.RuleID = Convert.ToInt32(dr["RuleID"]);
                ro.ParentRuleID = Convert.ToInt32(dr["ParentRuleID"]);
                ro.RuleType = Convert.ToInt32(dr["RuleType"]);
                ro.MinTriggerValue = Convert.ToInt32(dr["MinTriggerValue"]);
                ro.MaxTriggerValue = Convert.ToInt32(dr["MaxTriggerValue"]);
                ro.Struc = dr["Struc"].ToString();
                ro.StrucValue = dr["StrucValue"].ToString();
                ro.StrucCondition = dr["StrucCondition"].ToString();
                ro.RuleName = dr["RuleName"].ToString();
                ro.StrucType = dr["StrucType"].ToString();
                lr.Add(ro);
            }
            r.lRules = lr;

            List<Matrix> lMatrix = new List<Matrix>();
            MatrixService ms = new MatrixService();
            foreach (DataRow dr in ds.Tables["dtMatrix"].Rows)
                lMatrix.Add(ms.AssignMatrix(dr));
            r.lMatrix = lMatrix;

            if (ds.Tables.Contains("dtARC"))
                r.NotifyList = new AutoReportingConfigService().GetListARC(ds);

            return r;
        }
        public Models.Rules.Rule GetRulesByParentId(String iID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"SELECT RuleID, 
                                        ParentRuleID, 
                                        RuleType, 
                                        MinTriggerValue, 
                                        MaxTriggerValue, 
                                        Struc, 
                                        StrucValue, 
                                        StrucCondition,
                                        StrucType,
                                        RuleName from GFI_GPS_Rules 
                                        where ParentRuleID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = iID.ToString();
            drSql["sTableName"] = "dtRules";
            dtSql.Rows.Add(drSql);

            sql = new MatrixService().sGetMatrixId(Convert.ToInt32(iID), "GFI_SYS_GroupMatrixRule");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);

            sql = new AutoReportingConfigService().sGetARCbyTargetID(iID);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtARC";
            dtSql.Rows.Add(drSql);

            sql = new AutoReportingConfigService().sGetARCDetailsbyTargetID(iID);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtARCDetails";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables["dtRules"].Rows.Count == 0)
                return null;
            else
                return GetRules(ds);
        }
        public Boolean SaveRulesList(List<Models.Rules.Rule> LRules, List<AutoReportingConfig> LARC, Boolean bInsert, int uLogin, String sConnStr)
        {
            //Valdations           
            var uniqueCount = LRules
            .GroupBy(u => u.Struc)
            .Select(grp => grp.ToList())
            .ToList().Count();

            //if (bInsert)
            //{
            //    if (uniqueCount >= 3)
            //    {
            //        var time = LRules
            //        .Where(u => u.Struc == "Time")
            //       .Select(u => u.StrucValue)
            //       .ToList();

            //        if (time.Count > 0)
            //        {
            //            int[] arr = new int[2];
            //            int index = 0;
            //            foreach (object obj in time)
            //            {
            //                arr[index] = (int)Math.Round(Convert.ToDouble(obj), 0);
            //                index += 1;
            //            }
            //            if (arr[0] == arr[1])
            //            {
            //                //MessageBox.Show("Start time and end time should not be same");
            //                return false;
            //            }
            //        }
            //    }
            //    else
            //        return false;
            //}

            Models.Rules.Rule uRules = new Models.Rules.Rule();
            DataTable dt = new DataTable();
            dt.TableName = "GFI_GPS_Rules";
            dt.Columns.Add("RuleID", uRules.RuleID.GetType());
            dt.Columns.Add("ParentRuleID", uRules.ParentRuleID.GetType());
            dt.Columns.Add("RuleType", uRules.RuleType.GetType());
            dt.Columns.Add("MinTriggerValue", uRules.MinTriggerValue.GetType());
            dt.Columns.Add("MaxTriggerValue", uRules.MaxTriggerValue.GetType());
            dt.Columns.Add("Struc", typeof(String));
            dt.Columns.Add("StrucValue", typeof(String));
            dt.Columns.Add("StrucCondition", typeof(String));
            dt.Columns.Add("RuleName", typeof(String));
            dt.Columns.Add("StrucType", typeof(String));
            dt.Columns.Add("oprType", uRules.oprType.GetType());


            foreach (Models.Rules.Rule r in LRules)
            {
                if (bInsert)
                    r.oprType = DataRowState.Added;
                else
                    r.ParentRuleID = LRules[0].ParentRuleID;

                DataRow dr = dt.NewRow();
                dr["RuleID"] = r.RuleID;
                dr["ParentRuleID"] = r.ParentRuleID;
                dr["RuleType"] = r.RuleType;
                dr["MinTriggerValue"] = r.MinTriggerValue;
                dr["MaxTriggerValue"] = r.MaxTriggerValue;
                dr["Struc"] = r.Struc;
                dr["StrucValue"] = r.StrucValue;
                dr["StrucCondition"] = r.StrucCondition;
                dr["RuleName"] = r.RuleName;
                dr["StrucType"] = r.StrucType;
                dr["oprType"] = r.oprType;

                //if (r.StrucType == "Speed")
                //{
                //    dr["Struc"] = "Speed";
                //    dr["RuleType"] = 4;
                //}
                    


                dt.Rows.Add(dr);
            }

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "RuleID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
            {
                drCtrl["NextIdAction"] = String.Empty; ;
                drCtrl["NextIdValue"] = LRules[0].ParentRuleID;
            }

            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 2;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "RuleID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "ParentRuleID";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess = new DataSet();
            dsProcess.Merge(dt);

            //Saving only where ParentRuleID = RuleID
            if (LRules[0].lMatrix.Count == 0)
            {
                List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin, sConnStr);

                LRules[0].lMatrix.Add(lm[0]);
                DataTable dtMatrix = new myConverter().ListToDataTable<Matrix>(LRules[0].lMatrix);
                foreach (DataRow dri in dtMatrix.Rows)
                    dri["oprType"] = DataRowState.Added;

                dtMatrix.TableName = "GFI_SYS_GroupMatrixRule";
                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 3;
                drCtrl["TblName"] = dtMatrix.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "MID";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdAction"] = "SET";
                drCtrl["NextIdFieldName"] = "iID";
                drCtrl["ParentTblName"] = dt.TableName;
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtMatrix);
            }

            int iTranType = 2;
            if (bInsert)
                iTranType = 1;
            DataTable dtAudit = new AuditService().LogTran(iTranType, "Rules", LRules[0].RuleName, uLogin, 0);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "CallerFunction";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            DataSet dsACL = new DataSet();
            int iSeqNo = 3;
            foreach (AutoReportingConfig acl in LARC)
            {

                //if(bInsert)
                    acl.CreatedBy = uLogin.ToString();

                DataSet dsTemp = new AutoReportingConfigService().PrepareSaveDsForACL(acl, iSeqNo, bInsert, sConnStr);
                foreach (DataTable dtTemp in dsTemp.Tables)
                    dsProcess.Merge(dtTemp);

                iSeqNo++;
                iSeqNo++;
                iSeqNo++;
            }

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (dti.TableName == "CTRLTBL")
                    continue;
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");
                if (dRow.Length > 0)
                {
             
                     foreach(DataRow x  in dtCtrl.Rows)
                    {
                        string s = x["ExceptionInfo"].ToString();

                        if(x != null)
                        {
                            System.IO.File.AppendAllText(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "\\Registers\\ruless.dat", "\r\n rule Creation \r\n" + s.ToString() + "\r\n");
                        }
                       
                    }
                    bResult = false;
                }
                else
                {
                    bResult = true;
             
                }
            }
            else
                bResult = false;

            return bResult;
        }
        public Boolean DeleteRules(int iParentRuleID, String strRuleName, int iLoginUID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = String.Empty;
            sql += "\r\n";
            sql += @"delete from GFI_SYS_Notification where ARID in 
	                    (select ARID from GFI_FLT_AutoReportingConfig where TargetID = " + iParentRuleID.ToString() + " and ReportID > 100);";
            sql += "\r\n";
            sql += "\r\n";
            sql += @"delete from GFI_SYS_Notification 
	                    where ExceptionID in (select iID from GFI_GPS_Exceptions where ruleid = " + iParentRuleID.ToString() + ");";
            sql += "\r\n";
            sql += "\r\n";
            sql += "delete from GFI_GPS_ExceptionsHeader where RuleID = " + iParentRuleID.ToString() + ";";
            sql += "\r\n";
            sql += "\r\n";
            sql += "delete from GFI_FLT_AutoReportingConfig where TargetID = " + iParentRuleID.ToString() + " and ReportID > 100;";
            sql += "\r\n";
            sql += "\r\n";
            sql += "delete from GFI_GPS_Rules where ParentRuleID = " + iParentRuleID.ToString() +";";

            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            DataTable dtAudit = new AuditService().LogTran(3, "Rules", strRuleName, iLoginUID, iParentRuleID);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "CallerFunction";
            drCtrl["ParentTblName"] = "GFI_GPS_Rules";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }

        public Models.Rules.Rule GetFuelRule(String fuelType, String sConnStr)
        {
            //fuelType : Fill, Drop
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"SELECT * from GFI_GPS_Rules where Struc = 'Fuel' and StrucValue = '" + fuelType + "'";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtRules";
            dtSql.Rows.Add(drSql);

            sql = new MatrixService().sGetMatrixId(-1, "GFI_SYS_GroupMatrixRule");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);

            sql = "select * from GFI_FLT_AutoReportingConfig where TargetID = (SELECT Top 1 ParentRuleID from GFI_GPS_Rules where Struc = 'Fuel' and StrucValue = '" + fuelType + "')";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtARC";
            dtSql.Rows.Add(drSql);

            sql = @"select d.* from GFI_FLT_AutoReportingConfig h
	                            inner join GFI_FLT_AutoReportingConfigDetails d on h.ARID = d.ARID 
                            where h.TargetID = (SELECT Top 1 ParentRuleID from GFI_GPS_Rules where Struc = 'Fuel' and StrucValue = '" + fuelType + "')";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtARCDetails";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables["dtRules"].Rows.Count == 0)
                return null;
            else
                return GetRules(ds);
        }
        public Boolean SaveFuelRule(Models.Rules.Rule r, String sConnStr)
        {
            Models.Rules.Rule uRules = new Models.Rules.Rule();
            DataTable dt = new DataTable();
            dt.TableName = "GFI_GPS_Rules";
            dt.Columns.Add("RuleID", typeof(int));
            dt.Columns.Add("ParentRuleID", typeof(int));
            dt.Columns.Add("RuleType", typeof(int));
            dt.Columns.Add("MinTriggerValue", typeof(int));
            dt.Columns.Add("MaxTriggerValue", typeof(int));
            dt.Columns.Add("Struc", typeof(String));
            dt.Columns.Add("StrucValue", typeof(String));
            dt.Columns.Add("StrucCondition", typeof(String));
            dt.Columns.Add("RuleName", typeof(String));
            dt.Columns.Add("StrucType", typeof(String));
            dt.Columns.Add("oprType", typeof(DataRowState));

            DataRow dr = dt.NewRow();
            dr["RuleID"] = r.RuleID;
            dr["ParentRuleID"] = r.ParentRuleID;
            dr["RuleType"] = r.RuleType;
            dr["MinTriggerValue"] = r.MinTriggerValue;
            dr["MaxTriggerValue"] = r.MaxTriggerValue;
            dr["Struc"] = r.Struc;
            dr["StrucValue"] = r.StrucValue;
            dr["StrucCondition"] = r.StrucCondition;
            dr["RuleName"] = r.RuleName;
            dr["StrucType"] = r.StrucType;
            dr["oprType"] = DataRowState.Added;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "RuleID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "GET";

            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 2;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "RuleID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "ParentRuleID";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess = new DataSet();
            dsProcess.Merge(dt);

            List<AutoReportingConfig> LARC = new List<AutoReportingConfig>();
            for (int i = 110; i <= 113; i++)
            {
                AutoReportingConfig arc = new AutoReportingConfig();
                arc.lDetails = new List<AutoReportingConfigDetail>();
                arc.ReportID = i;
                arc.oprType = DataRowState.Added;

                LARC.Add(arc);
            }
            int iSeqNo = 3;
            foreach (AutoReportingConfig acl in LARC)
            {
                DataSet dsTemp = new AutoReportingConfigService().PrepareSaveDsForACL(acl, iSeqNo, true, sConnStr);
                foreach (DataTable dtTemp in dsTemp.Tables)
                    dsProcess.Merge(dtTemp);

                iSeqNo++;
                iSeqNo++;
                iSeqNo++;
            }

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (dti.TableName == "CTRLTBL")
                    continue;
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                        dri["oprType"] = DataRowState.Added;
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public BaseModel UpdateFuelRuleNotif(int UID, String fuelType, List<int> NewUsers, List<notifyList> lnF, String sConnStr)
        {
            BaseModel baseModel = new BaseModel();

            Models.Rules.Rule rule = GetFuelRule(fuelType, sConnStr);

            String sql = String.Empty;

            foreach (var x in lnF)
            {
                String ARID = "-1";
                foreach (AutoReportingConfig arc in rule.NotifyList)
                {
                    if (x.notificationType == arc.ReportID.Value || x.notificationType == arc.ReportID.Value || x.notificationType == arc.ReportID.Value || x.notificationType == arc.ReportID)
                    {
                        ARID = arc.ARID.ToString();

                        sql += "insert GFI_FLT_AutoReportingConfigDetails (ARID, UID, UIDType, UIDCategory) values ("
                                 + ARID + ", " + x.userId.ToString() + ", 'Users', 'ID') \r\n";
                    }



                    if (rule.Struc != "Fuel")
                        return baseModel;
                }
            }

            Boolean bResult = false;
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            dsProcess.Merge(dtCtrl);


            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                {
                    baseModel.dbResult = dsResult;
                    baseModel.dbResult.Tables["CTRLTBL"].TableName = "ResultCtrlTbl";
                    baseModel.dbResult.Merge(dsProcess);
                    bResult = false;
                }
                else
                    bResult = true;
            }
            else
                bResult = false;

            baseModel.data = bResult;
            return baseModel;
        }
        public BaseModel GetFuelRuleNotif(int UID, String fuelType, String sConnStr)
        {
            //Sending All users in for the Rule notification.
            //Fuel rules saved automatically when Fill/Drop detected.
            //Notif generated if user attached to Notification rule.
            BaseModel baseModel = new BaseModel();
            List<int> lUsers = new List<int>();

            DataTable dtGMUser = new NaveoOneLib.WebSpecifics.LibData().dtGMUser(UID, false, sConnStr);
            Models.Rules.Rule fuelRule = GetFuelRule(fuelType, sConnStr);

            //Verifying if user in same matrix and adding
            foreach (DataRow dr in dtGMUser.Rows)
                foreach (AutoReportingConfig arc in fuelRule.NotifyList)
                    if (arc.ReportID == 113)
                        foreach (AutoReportingConfigDetail d in arc.lDetails)
                            if (d.UIDType == "Users")
                                if (d.UIDCategory == "ID")
                                    if (dr["StrucID"].ToString() == d.UID.ToString())
                                        lUsers.Add(d.UID);

            baseModel.data = lUsers;
            return baseModel;
        }

        public DataTable GetRules(String sConnStr)
        {
            String sqlString = @"SELECT RuleID, 
                                        ParentRuleID, 
                                        RuleType, 
                                        MinTriggerValue, 
                                        MaxTriggerValue, 
                                        Struc, 
                                        StrucValue, 
                                        StrucCondition,
                                        StrucType,
                                        RuleName from GFI_GPS_Rules 
                                        where ParentRuleID = RuleID and order by RuleID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public Models.Rules.Rule GetRulesById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT RuleID, 
                                        ParentRuleID, 
                                        RuleType, 
                                        MinTriggerValue, 
                                        MaxTriggerValue, 
                                        Struc, 
                                        StrucValue,
                                        StrucType, 
                                        StrucCondition,RuleName from GFI_GPS_Rules
                                        WHERE RuleID = ?
                                        order by RuleID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                Models.Rules.Rule retRules = new Models.Rules.Rule();
                retRules.RuleID = (int)dt.Rows[0]["RuleID"];
                retRules.ParentRuleID = (int)dt.Rows[0]["ParentRuleID"];
                retRules.RuleType = (int)dt.Rows[0]["RuleType"];
                retRules.MinTriggerValue = (int)dt.Rows[0]["MinTriggerValue"];
                retRules.MaxTriggerValue = (int)dt.Rows[0]["MaxTriggerValue"];
                retRules.Struc = dt.Rows[0]["Struc"].ToString();
                retRules.StrucValue = dt.Rows[0]["StrucValue"].ToString();
                retRules.StrucCondition = dt.Rows[0]["StrucCondition"].ToString();
                retRules.RuleName = dt.Rows[0]["RuleName"].ToString();
                retRules.StrucType = dt.Rows[0]["StrucType"].ToString();
                return retRules;
            }
        }
        public DataTable GetRulesByAssetId(String iID, String sConnStr)
        {
            String sqlString = @"SELECT RuleID, 
                                        ParentRuleID, 
                                        RuleType, 
                                        MinTriggerValue, 
                                        MaxTriggerValue, 
                                        Struc, 
                                        StrucValue, 
                                        StrucCondition,RuleName ,
                                        StrucType from GFI_GPS_Rules
                                        WHERE StrucValue = ?
                                        AND Struc ='Assets'
                                        order by StrucValue,ParentRuleID,RuleID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return dt;
        }
        public DataTable GetRulesByParentRuleID(String iID, String sConnStr)
        {
      
            String sqlString = @"SELECT r.RuleID, 
                                        r.ParentRuleID, 
                                        r.RuleType, 
                                        r.MinTriggerValue, 
                                        r.MaxTriggerValue, 
                                        r.Struc, 
                                        r.StrucValue, 
                                        r.StrucCondition,RuleName ,
                                        r.StrucType,
										matrixrule.MID ,
										matrixrule.GMID,
										matrixrule.iID
										from GFI_GPS_Rules r
										inner join GFI_SYS_GroupMatrixRule matrixrule on r.ParentRuleID = matrixrule.iID
                                        WHERE ParentRuleID = ?
                                        order by StrucValue,ParentRuleID,RuleID";

            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return dt;
        }
        public bool SaveRules(Models.Rules.Rule uRules, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_GPS_Rules";
            //dt.Columns.Add("RuleID", uRules.RuleID.GetType());
            dt.Columns.Add("ParentRuleID", uRules.ParentRuleID.GetType());
            dt.Columns.Add("RuleType", uRules.RuleType.GetType());
            dt.Columns.Add("MinTriggerValue", uRules.MinTriggerValue.GetType());
            dt.Columns.Add("MaxTriggerValue", uRules.MaxTriggerValue.GetType());
            dt.Columns.Add("Struc", uRules.Struc.GetType());
            dt.Columns.Add("StrucValue", uRules.StrucValue.GetType());
            dt.Columns.Add("StrucCondition", uRules.StrucCondition.GetType());
            dt.Columns.Add("RuleName", uRules.RuleName.GetType());
            dt.Columns.Add("StrucType", uRules.StrucType.GetType());
            DataRow dr = dt.NewRow();
            //dr["RuleID"] = uRules.RuleID;
            dr["ParentRuleID"] = uRules.ParentRuleID;
            dr["RuleType"] = uRules.RuleType;
            dr["MinTriggerValue"] = uRules.MinTriggerValue;
            dr["MaxTriggerValue"] = uRules.MaxTriggerValue;
            dr["Struc"] = uRules.Struc;
            dr["StrucValue"] = uRules.StrucValue;
            dr["StrucCondition"] = uRules.StrucCondition;
            dr["RuleName"] = uRules.RuleName;
            dr["StrucType"] = uRules.StrucType;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenInsert(dt, transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool UpdateRules(Models.Rules.Rule uRules, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_GPS_Rules";
            dt.Columns.Add("RuleID", uRules.RuleID.GetType());
            dt.Columns.Add("ParentRuleID", uRules.ParentRuleID.GetType());
            dt.Columns.Add("RuleType", uRules.RuleType.GetType());
            dt.Columns.Add("MinTriggerValue", uRules.MinTriggerValue.GetType());
            dt.Columns.Add("MaxTriggerValue", uRules.MaxTriggerValue.GetType());
            dt.Columns.Add("Struc", uRules.Struc.GetType());
            dt.Columns.Add("StrucValue", uRules.StrucValue.GetType());
            dt.Columns.Add("StrucCondition", uRules.StrucCondition.GetType());
            dt.Columns.Add("RuleName", uRules.RuleName.GetType());
            dt.Columns.Add("StrucType", uRules.StrucType.GetType());
            DataRow dr = dt.NewRow();
            dr["RuleID"] = uRules.RuleID;
            dr["ParentRuleID"] = uRules.ParentRuleID;
            dr["RuleType"] = uRules.RuleType;
            dr["MinTriggerValue"] = uRules.MinTriggerValue;
            dr["MaxTriggerValue"] = uRules.MaxTriggerValue;
            dr["Struc"] = uRules.Struc;
            dr["StrucValue"] = uRules.StrucValue;
            dr["StrucCondition"] = uRules.StrucCondition;
            dr["RuleName"] = uRules.RuleName;
            dr["StrucType"] = uRules.StrucType;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "RuleID = '" + uRules.RuleID + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }

        public AlertCount GetExcepDashboardCounts(List<Matrix> lMatrix,String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();


            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            //Engine Count
            String sql = @"select Count(*)  EngineRuleCount from GFI_GPS_Rules  r  inner join  GFI_SYS_GroupMatrixRule mr on r.ParentRuleID = mr.iID 
                           where r.StrucValue = '0' and r.Struc = 'Exception Type' and mr.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + ")";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtEngineRuleCount";
            dtSql.Rows.Add(drSql);

            //Driving Count
            sql = @"select Count(*) DrivingRuleCount from GFI_GPS_Rules  r  inner join  GFI_SYS_GroupMatrixRule mr on r.ParentRuleID = mr.iID 
                           where r.StrucValue = '1' and r.Struc = 'Exception Type' and mr.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + ")";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtDrivingRuleCount";
            dtSql.Rows.Add(drSql);

            //Planning Count
            sql = @"select Count(*) PlanningRuleCount from GFI_GPS_Rules  r  inner join  GFI_SYS_GroupMatrixRule mr on r.ParentRuleID = mr.iID 
                           where r.StrucValue = '2' and r.Struc = 'Exception Type' and mr.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + ")";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtPlanningRuleCount";
            dtSql.Rows.Add(drSql);

            //Maintenance Count
            sql = @"select Count(*) MaintenanceRuleCount from GFI_GPS_Rules  r  inner join  GFI_SYS_GroupMatrixRule mr on r.ParentRuleID = mr.iID 
                           where r.StrucValue = '3' and r.Struc = 'Exception Type' and mr.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + ")";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMaintenanceRuleCount";
            dtSql.Rows.Add(drSql);

            //Fuel Count
            sql = @"select Count(*) FuelRuleCount from GFI_GPS_Rules 
                           where  Struc = 'Fuel'";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtFuelRuleCount";
            dtSql.Rows.Add(drSql);


            //total Count
            sql = @"select Count(*) TotalRuleCount from GFI_GPS_Rules  r  inner join  GFI_SYS_GroupMatrixRule mr on r.ParentRuleID = mr.iID 
                           where  r.StrucValue in (0,1,2,3) and r.Struc = 'Exception Type' and  mr.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + ")";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtTotalRuleCount";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            AlertCount aC = new AlertCount();

           
            if (ds.Tables["dtEngineRuleCount"].Rows.Count != 0)
                  aC.engineRuleCount = ds.Tables["dtEngineRuleCount"].Rows[0].Field<int>("EngineRuleCount");


            if (ds.Tables["dtDrivingRuleCount"].Rows.Count != 0)
                aC.drivingRuleCount = ds.Tables["dtDrivingRuleCount"].Rows[0].Field<int>("DrivingRuleCount");

            if (ds.Tables["dtPlanningRuleCount"].Rows.Count != 0)
                aC.planningRuleCount = ds.Tables["dtPlanningRuleCount"].Rows[0].Field<int>("PlanningRuleCount");

            if (ds.Tables["dtMaintenanceRuleCount"].Rows.Count != 0)
                aC.maintenanceRuleCount = ds.Tables["dtMaintenanceRuleCount"].Rows[0].Field<int>("MaintenanceRuleCount");

            if (ds.Tables["dtFuelRuleCount"].Rows.Count != 0)
                aC.fuelRuleCount = ds.Tables["dtFuelRuleCount"].Rows[0].Field<int>("FuelRuleCount");

            if (ds.Tables["dtTotalRuleCount"].Rows.Count != 0)
                aC.totalRuleCount = ds.Tables["dtTotalRuleCount"].Rows[0].Field<int>("TotalRuleCount") + 2;

            return aC;
        }



        public DataTable GetRulesbyStruc(String Struc,String sConnStr)
        {
            String sqlString = @"select * from gfi_gps_rules where Struc = '"+ Struc +"'";


            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public DataTable GetRulebyParentRuleID(String parentruleid, String sConnStr)
        {
            String sqlString = @"select * from gfi_gps_rules where ParentRuleID = '" + parentruleid + "'";


            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public DataTable dtGetMaintenanceRule(String sConnStr)
        {
            String sqlString = @"select rules.ParentRuleID,Struc ,rules.StrucValue as URI ,ad.UID as userid from gfi_gps_rules rules
left join GFI_FLT_AutoReportingConfig a on a.TargetID = rules.ParentRuleID
left join GFI_FLT_AutoReportingConfigDetails ad on ad.ARID =a.ARID
inner join gfi_sys_user u on u.UID = ad.UID
where rules.Struc = 'AssetMaintenance'";


            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }



        public DataTable dtGetMaintenanceRuleUsers(String sConnStr)
        {
            String sqlString = @"select rules.ParentRuleID,Struc ,rules.StrucValue as URI ,ad.UID as userid from gfi_gps_rules rules
left join GFI_FLT_AutoReportingConfig a on a.TargetID = rules.ParentRuleID
left join GFI_FLT_AutoReportingConfigDetails ad on ad.ARID =a.ARID
inner join gfi_sys_user u on u.UID = ad.UID
where rules.Struc = 'AssetMaintenance'
 ";


            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        public DataTable GetMaintenanceRule(String sConnStr)
        {
            DataTable dt = dtGetMaintenanceRule(sConnStr);

            DataTable dtFinal = new DataTable();
            dtFinal.Columns.Add("RuleID");
            dtFinal.Columns.Add("URI");
            dtFinal.Columns.Add("Asset");
            dtFinal.Columns.Add("MaintType");
            dtFinal.Columns.Add("User");

            DataView view = new DataView(dt);
            DataTable distinctValues = view.ToTable(true, "ParentRuleID");
    
            foreach (DataRow dr in distinctValues.Rows)
            {

                var rowColl = dt.AsEnumerable();
                int ParentRuleID = Convert.ToInt32(dr["ParentRuleID"].ToString());
                var drSelectedRows = rowColl.Where(r => r.Field<int>("ParentRuleID") == ParentRuleID);
                string concatuserid = string.Empty;
                BaseModel bmRecurMaint = new NaveoService.Services.MaintAssetServices().GetRecurringMaint();
                DataTable dtRecurData = bmRecurMaint.data;
                if (drSelectedRows != null && drSelectedRows.Count() > 0)
                {
                    DataTable dtnew = drSelectedRows.CopyToDataTable();
                    DataView view1 = new DataView(dtnew);
                    DataTable distinctValues1 = view1.ToTable(true, "userid");

                    foreach (DataRow drN in distinctValues1.Rows)
                    {
                        concatuserid += drN["userid"].ToString() + ",";

                    }

                    concatuserid = concatuserid.Substring(0, concatuserid.Length - 1);



                    foreach (DataRow drN in drSelectedRows)
                    {

                        int uri = Convert.ToInt32(drN["URI"].ToString());

                        var assetName = dtRecurData.AsEnumerable().Where(recur => recur.Field<int>("URI") == uri).Select(r => r.Field<string>("Number")).FirstOrDefault();
                        var maintypeName = dtRecurData.AsEnumerable().Where(recur => recur.Field<int>("URI") == uri).Select(r => r.Field<string>("Description")).FirstOrDefault();

                        if (assetName != null && maintypeName != null)
                        {
                            DataRow drNew = dtFinal.NewRow();
                            drNew["RuleID"] = Convert.ToInt32(drN["ParentRuleID"].ToString());
                            drNew["Asset"] = assetName.ToString();
                            drNew["MaintType"] = maintypeName.ToString();
                            drNew["URI"] = Convert.ToInt32(drN["URI"].ToString());
                            drNew["User"] = concatuserid;
                            dtFinal.Rows.Add(drNew);

                        }

                    }

                }

                

            }

            dtFinal = dtFinal.DefaultView.ToTable(true, "RuleID", "Asset", "MaintType","URI","User");
            return dtFinal;


        }


        public BaseModel UpdateRuleMaintenance(int ruleid, int oldURI, int newURI,String sConnStr)
        {
            BaseModel baseModel = new BaseModel();




            String sql = String.Empty;
            sql = "update gfi_gps_rules set StrucValue = "+ newURI + " where parentruleid = "+ ruleid + " and StrucType = 'URI' and Struc = 'AssetMaintenance' and StrucValue = " + oldURI +"";
               

            Boolean bResult = false;
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            dsProcess.Merge(dtCtrl);


            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                {
                    baseModel.dbResult = dsResult;
                    baseModel.dbResult.Tables["CTRLTBL"].TableName = "ResultCtrlTbl";
                    baseModel.dbResult.Merge(dsProcess);
                    bResult = false;
                }
                else
                    bResult = true;
            }
            else
                bResult = false;

            baseModel.data = bResult;
            return baseModel;
        }




    } }