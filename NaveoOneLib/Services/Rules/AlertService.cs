﻿using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.GMatrix;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Documents.Spreadsheet.Expressions.Functions;
using Convert = System.Convert;

namespace NaveoOneLib.Services.Rules
{
    public class AlertService
    {

        #region  ALERT ON PLANNING

        public class comparison
        {
            public DateTime DateFrom { get; set; }


            public DateTime DateTo { get; set; }

            public string type { get; set; }
        }



        public dtData filteredClass(List<comparison> lc, Guid sUserToken, List<Matrix> lm, List<int> LAssetIds, string sConnStr)
        {
            dtData finaldata = new dtData();
            DataTable dtFinal1 = new DataTable();
            DataTable dtFinal2 = new DataTable();
            
            foreach (var x in lc)
            {

                dtData mydtData = new NaveoOneLib.Services.Plannings.PlanningService().GetPlanningActual(sUserToken, lm, x.DateFrom, x.DateTo, LAssetIds, null, sConnStr);
                dtFinal1 = mydtData.Data;

                if (dtFinal1 != null)
                {
                    if (x.type.ToLower().ToString() == "not visited")
                    {

                        var rows = (dtFinal1.AsEnumerable()
                                    .Where(row => row.Field<string>("Status") == null));

                        if (rows.Any())
                        {
                            dtFinal2.Merge(rows.CopyToDataTable());
                        }


                    }
                    else
                    {

                        var rows = (dtFinal1.AsEnumerable()
                                    .Where(row => row.Field<string>("Status") != null));


                        if (rows.Any())
                        {
                            dtFinal2.Merge(rows.CopyToDataTable());
                        }


                    }


                }
                
            }

      
            finaldata.Data = dtFinal2;
     
          
            return finaldata;
        }

        public BaseModel GenerateAlertPlanning(List<Matrix> lm, int ruleid, List<string> lalertValue, List<int> LAssetIds, string sConnStr)
        {
            //Guid sUserToken = new Guid("ED20F533-2919-465B-AC9A-4D6DDC125B1F");
            Guid sUserToken = new Guid();

            //Variables
            #region variables

            DataTable dt = new DataTable();
            dt.Columns.Add("ScheduleID");
            dt.Columns.Add("PID");

            dt.Columns.Add("RuleName");
            dt.Columns.Add("AssetID");
            dt.Columns.Add("Asset");
            dt.Columns.Add("DriverID");
            dt.Columns.Add("Driver");
            dt.Columns.Add("ViaPoint");
            dt.Columns.Add("ExceptedTime");
            dt.Columns.Add("Buffer");
            dt.Columns.Add("ActualTime");
            dt.Columns.Add("DurationInsideZone");
            dt.Columns.Add("Status");
            BaseModel bM = new BaseModel();


            #endregion


           


            DateTime DtFrom = new DateTime();
            DateTime DtTo = new DateTime();
            List<comparison> lc = new List<comparison>();

            comparison cSecond = new comparison();
            int countv = 0;
            foreach (var x in lalertValue)
            {
                
                if (x.ToLower() == "not visited")
                {
                    TimeSpan timeNow = DateTime.Now.TimeOfDay;
                    TimeSpan start = TimeSpan.Parse("23:00");

                    if (start > timeNow)
                    {
                        TimeSpan sixAm = TimeSpan.Parse("06:00");
                        TimeSpan sixPm = TimeSpan.Parse("18:00");
                        string sDtFrom = DateTime.Now.ToShortDateString();
                        string sDtTo = DateTime.Now.ToShortDateString();

                        DtFrom = Convert.ToDateTime(sDtFrom.ToString()).Add(sixAm).ToUniversalTime();
                        DtTo = Convert.ToDateTime(sDtTo.ToString()).Add(sixPm).ToUniversalTime();



                    }
                    else
                    {



                        DtFrom = DateTime.Now.AddMinutes(-15).ToUniversalTime();
                        DtTo = DateTime.Now.ToUniversalTime().AddMinutes(15).ToUniversalTime();

                    }

                    comparison cFirst = new comparison();
                    cFirst.type = x;
                    cFirst.DateFrom = DtFrom;
                    cFirst.DateTo = DtTo;

                    lc.Add(cFirst);


                }
                else
                {
                    countv++;
                    //DtFrom = new DateTime(2020, 06, 02, 15, 00, 40).ToUniversalTime();
                    //DtTo = new DateTime(2020, 06, 02, 15, 00, 40).ToUniversalTime();

                    DtFrom = DateTime.Now.ToUniversalTime();
                    DtTo = DateTime.Now.ToUniversalTime();
                    DtFrom = DtFrom.AddMinutes(-15);
                    DtTo = DtTo.AddMinutes(15);
                    if (countv == 1)
                    {
                        cSecond.type = "öther status";
                        cSecond.DateFrom = DtFrom;
                        cSecond.DateTo = DtTo;

                        lc.Add(cSecond);
                    }
                }
            }




            //build new datatable to be triggered as exceptions and save also on table exceptions
            #region build new datatable to be triggered as exceptions and save also on table exceptions
            //DateTime dtFrom = DateTime.Now.ToUniversalTime();
            //DateTime dtTo = DateTime.Now.ToUniversalTime();
            //2020-02-26 02:15:38.00
            //dtData mydtData = new NaveoOneLib.Services.Plannings.PlanningService().GetPlanningActual(sUserToken, lm, DtFrom, DtTo, LAssetIds, null, sConnStr);

            dtData mydtData = filteredClass(lc,sUserToken, lm, LAssetIds, sConnStr);

                #endregion


                 if (mydtData.Data.Rows.Count != 0)
                  {
                    DataView view = new DataView(mydtData.Data);
                    DataTable distinctScheduleID = view.ToTable(true, "ScheduledID");

                    //mydtData.Data.DefaultView.Sort = "ScheduledID asc";
                    //mydtData.Data = mydtData.Data.DefaultView.ToTable();

                    foreach (DataRow dr in mydtData.Data.Rows)
                    {


                        int count = -1;
                        String Status = dr["Status"].ToString().ToLower();
                        String Time = dr["Time"].ToString().ToLower();
                        String ScheduleId = dr["ScheduledID"].ToString();
                        String PID = dr["PID"].ToString();

                       
                        if (Status == string.Empty)
                            Status = "Not Visited";

                        Status = Status.ToLower();
                        string[] splitstatus = Status.Split(',');


                        string[] splittime = Time.Split(',');


                        foreach (var x in lalertValue)
                        {

                        var filteredstatus = (from ss in splitstatus
                                              where ss.Equals(x.ToLower())
                                              select ss).FirstOrDefault();

                        if (filteredstatus != null)
                            {
                                count++;

                                DataRow newRow = dt.NewRow();
                                newRow["RuleName"] = ruleid.ToString();
                                newRow["ScheduleID"] = ScheduleId;
                                newRow["PID"] = PID;
                                newRow["Status"] = filteredstatus;
                                newRow["AssetID"] = dr["AssetID"].ToString();
                                newRow["DriverID"] = dr["DriverID"].ToString();
                                newRow["Asset"] = dr["AssetName"].ToString();
                                newRow["Driver"] = dr["DriverName"].ToString();
                                newRow["ViaPoint"] = dr["ZoneName"].ToString();
                                newRow["ExceptedTime"] = dr["dtUTC"].ToString();
                                newRow["Buffer"] = dr["Buffer"].ToString();
                                newRow["ActualTime"] = splittime[count].ToString();
                                newRow["DurationInsideZone"] = dr["Duration"].ToString();

                                dt.Rows.Add(newRow);
                            }

                        }

                        
                    }

                }

            

           
       




            bM.data = dt;
            return bM;


        }

        #endregion


        #region ALERT ON MAINTENANCE



        
        public String GenerateAlertMaintenance(String sConnStr)
        {
            DataTable dtNotification = NotificationService.NotificationDT();
            List<int> lUriHisto = new List<int>();
            string s = string.Empty;
            string errormessage = string.Empty;
            int ruleid = 0;
            Boolean bSave = false;
            try
            {
                BaseModel bm = new NaveoService.Services.MaintAssetServices().GetMaintenanceStatus(sConnStr);
                DataTable dt = bm.data;

                if (dt.Rows.Count > 0 && dt.Rows != null)
                {
                    foreach (DataRow dr in dt.Rows)
                    {


                        string userid = dr["UserId"].ToString();
                        string[] splituserid = userid.Split(',');
                        ruleid = Convert.ToInt32(dr["RuleID"].ToString());
                        foreach (var xxx in splituserid)
                        {
                            string title = "Alert for " + dr["Type"].ToString();
                            String body = string.Empty;

                            if(dr["OldStatus"].ToString() != string.Empty)
                                 body = "Alert " + dr["Type"].ToString() + " on " + dr["Number"].ToString() + " changes status from '" + dr["OldStatus"].ToString() + "'  to  '" + dr["NewStatus"].ToString() + "' ";
                            else
                                body = "Alert " + dr["Type"].ToString() + " on " + dr["Number"].ToString() + " changes status to '" + dr["NewStatus"].ToString() + "' ";


                            DataRow drNew = dtNotification.NewRow();
                            //drNew["AIRD"] = 0;
                            drNew["UID"] = Convert.ToInt32(xxx.ToString());
                            drNew["Status"] = 100;
                            //drNew["InsertedAt"] = DateTime.Now.ToString();
                            //drNew["ReportID"] = DateTime.Now;
                            drNew["ParentRuleID"] = Convert.ToInt32(dr["RuleID"].ToString());
                            drNew["AssetID"] = Convert.ToInt32(dr["FleetAssetId"].ToString());
                            drNew["sBody"] = body;
                            drNew["Type"] = "Rule";
                            drNew["Title"] = title.ToString();
                            dtNotification.Rows.Add(drNew);
                        }

                        
                        if (dr["URIHisto"].ToString() != string.Empty || dr["URIHisto"].ToString() != "0")
                        {
                            int uriHisto = Convert.ToInt32(dr["URIHisto"].ToString());

                            lUriHisto.Add(uriHisto);

                        }

                    }

                    BaseModel bmUpdateHisto = new BaseModel();

                    //lUriHisto.Add(14);
                    if (lUriHisto.Count > 0)
                    {
                        bmUpdateHisto = new NaveoService.Services.MaintAssetServices().UpdateflagOnMaintenance(lUriHisto);

                    }


                   if(dtNotification.Rows.Count != 0)
                         bSave = new NotificationService().SaveNotification(dtNotification, sConnStr);




                    if (bSave)
                    {
                        var xxx = from ss in dt.AsEnumerable()
                                  where ss.Field<int>("NewURI") != 0
                                  select ss;

                        foreach (var xs in xxx)
                        {
                            int ruleId = Convert.ToInt32(xs["RuleID"].ToString());
                            int newURI = Convert.ToInt32(xs["NewURI"].ToString());
                            int oldURI = Convert.ToInt32(xs["URI"].ToString());

                            BaseModel bn = new NaveoOneLib.Services.Rules.RulesService().UpdateRuleMaintenance(ruleId, oldURI, newURI, sConnStr);
                        }




                    }


                    s += "Processing Alert Maintenance OK for RuleID : " + ruleid + " \r\n";

                } else
                {

                    bSave = false;
                    s = "No Processing of Alert Maintenance";
                }



            } catch(Exception ex)
            {
                bSave = false;
                errormessage = ex.ToString();
                s += "Processing Alert Maintenance FAILED..,  " + errormessage + " \r\n";
            }

            //s += "Processing Alert Maintenance FAILED,  " + errormessage + " \r\n";

            return s;
        }
     

        #endregion

    }
}

