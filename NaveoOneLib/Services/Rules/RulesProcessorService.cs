﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.GPS;
using NaveoOneLib.Models.Rules;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Zones;
using NaveoOneLib.Services.Assets;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Services.Zones;

namespace NaveoOneLib.Services.Rules
{
    public class RulesProcessorService
    {

        int? iRuleParentID = null;
        String strSeverity = "";
        Boolean bTriggerFired = true;
        String strExMessage = "[  ";

        public void RuleProcessor(GPSData gpsCord, String sConnStr)
        {
            /// <Rule Types
            /// 
            /// 1   Zones
            /// 2   Assets
            /// 3   Drivers
            /// 4   Speed
            /// 5   Time
            /// 6   Auxilaries

            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            /// 
            List<RuleException> exeTrigger = new List<RuleException>();


            //Check If asset Has rules attached
            DataTable dtRules = new DataTable();
            DataTable dtRulesToProcess = new DataTable();


            Asset chkRuleAsset = new AssetService().GetAssetById(gpsCord.AssetID, 0, false, sConnStr);

            strExMessage += chkRuleAsset.AssetNumber.ToString();


            //To Recode. implemienting List Group Matrix
            List<int> iGMID = new List<int>();
                iGMID = new GroupMatrixService().GetAllParentsfromGMID(chkRuleAsset.lMatrix[0].GMID, sConnStr);

            // List<int> iGMID = new List<int>();
            RulesService rulesService = new RulesService();
            foreach (int i in iGMID)
            {
                if (i > 3) //Reference starting point for all clients
                {
                    dtRules = rulesService.GetRulesByAssetId(i.ToString(), sConnStr);
                    if (dtRules != null)
                    {
                        foreach (DataRow ptRuleRow in dtRules.Rows)
                        {
                            dtRulesToProcess = rulesService.GetRulesByParentRuleID(ptRuleRow["ParentRuleID"].ToString(), sConnStr);
                            ProcessParentRuleGroup(dtRulesToProcess, gpsCord, sConnStr);
                        }
                        break;
                    }

                }

            }

            if (dtRules.Rows.Count==0)
            {
                dtRules = rulesService.GetRulesByAssetId(gpsCord.AssetID.ToString(), sConnStr);

                if (dtRules != null)
                {
                    foreach (DataRow ptRuleRow in dtRules.Rows)
                    {
                        dtRulesToProcess = rulesService.GetRulesByParentRuleID(ptRuleRow["ParentRuleID"].ToString(), sConnStr);
                        ProcessParentRuleGroup(dtRulesToProcess, gpsCord, sConnStr);
                    }

                }
            }

            if (dtRules == null)
            {
                return;
            }

           // bTriggerFired = true;

            if (bTriggerFired)
            {
                RuleException ExRule = new RuleException();
                ExceptionsService exceptionService = new ExceptionsService();

                if (exceptionService.GetExceptionsByGPSDataID(gpsCord.UID.ToString(), sConnStr) != null)
                {
                    ExRule.GPSDataID = gpsCord.UID;
                    if (exceptionService.DeleteExceptionsByGPSDataID(ExRule,sConnStr))
                    { }
                }
                ExRule.GPSDataID = gpsCord.UID;
                ExRule.RuleID = (int)iRuleParentID;
                ExRule.Severity = strSeverity + " - " + strExMessage;
                ExRule.AssetID = gpsCord.AssetID;
                ExRule.DriverID = gpsCord.DriverID;
                exceptionService.SaveExceptions(ExRule, null, sConnStr);
            }
        }
        private void ProcessParentRuleGroup(DataTable RuleGroup, GPSData gpsCord, String sConnStr)
        {

            Boolean trZone = true;
            Boolean trTime = true;
            Boolean trSpeed = true;
            Boolean bAuxillaries = true;
            Boolean Baux1 = false;
            Boolean Baux2 = false;
            Boolean Baux3 = false;
            Boolean Baux4 = false;
            Boolean Baux5 = false;
            Boolean Baux6 = false;

            iRuleParentID = (int)RuleGroup.Rows[0]["ParentRuleID"];
            strSeverity = RuleGroup.Rows[0]["RuleName"].ToString();
            ZoneService zoneService = new ZoneService();
            foreach (DataRow dr in RuleGroup.Rows)
            {
                if (dr["RuleType"].ToString() == "1")  // Condition for Zones
                {
                    //To review dr["StrucValue"]
                    List<Zone> ZonesList = new List<Zone>();// = new Zone().GetZoneList(Convert.ToInt16(dr["StrucValue"]));

                    foreach (Zone zn in ZonesList)
                    {
                        if (dr["StrucCondition"].ToString() == "Inside zone bounds")
                        {
                            trZone = zoneService.IsPointInside(zn, new Coordinate(gpsCord.Latitude, gpsCord.Longitude), sConnStr);
                            strExMessage += " Inside Zone: " + zn.description;

                        }
                        else
                        {
                            trZone = false;

                        }
                        if (dr["StrucCondition"].ToString() == "Outside zone bounds")
                        {
                            trZone = zoneService.IsPointInside(zn, new Coordinate(gpsCord.Latitude, gpsCord.Longitude), sConnStr);
                            strExMessage += " Outside Zone: " + zn.description;

                            if (trZone == false)
                                trZone = true;
                        }
                        else
                        {
                            trZone = false;
                        }
                        //Codes For Entering
                    }
                }
                if (dr["RuleType"].ToString() == "4")  // Condition for Speed
                {
                    if (dr["StrucCondition"].ToString() == "<=")
                    {
                        if (dr["StrucValue"].ToString() != "")
                        {
                            if (gpsCord.Speed <= Convert.ToInt16(dr["StrucValue"].ToString()))
                            {
                                trSpeed = true;
                                strExMessage += " Speed : " + gpsCord.Speed.ToString() + " <= " + dr["StrucValue"].ToString();

                            }
                            else
                            {
                                trSpeed = false;

                            }
                        }

                    }

                    if (dr["StrucCondition"].ToString() == ">=")
                    {
                        if (dr["StrucValue"].ToString() != "")
                        {
                            if (gpsCord.Speed >= Convert.ToInt16(dr["StrucValue"].ToString()))
                            {
                                trSpeed = true;
                                strExMessage += " Speed : " + gpsCord.Speed.ToString() + " >= " + dr["StrucValue"].ToString();

                            }
                            else
                            {
                                trSpeed = false;
                            }
                        }

                    }

                }


                if (dr["RuleType"].ToString() == "6") //For Auxilarries
                {
                    if (dr["StrucValue"].ToString() == "Aux1")
                    {
                        if (dr["StrucCondition"].ToString() != "=1")
                        {
                            Baux1 = true;
                            strExMessage += " Aux6 Activated " ;
                        }
                        else
                        {
                            Baux1 = false;
                        
                        }

                    }
                    if (dr["StrucValue"].ToString() == "Aux2")
                    {
                        if (dr["StrucCondition"].ToString() != "=1")
                        {
                            Baux2 = true;
                            strExMessage += " Aux6 Activated " ;
                        }
                        else
                        {
                            Baux2 = false;

                        }

                    }
                    if (dr["StrucValue"].ToString() == "Aux3")
                    {
                        if (dr["StrucCondition"].ToString() != "=1")
                        {
                            Baux3 = true;
                            strExMessage += " Aux6 Activated " ;
                        }
                        else
                        {
                            Baux3 = false;

                        }

                    }
                    if (dr["StrucValue"].ToString() == "Aux4")
                    {
                        if (dr["StrucCondition"].ToString() != "=1")
                        {
                            Baux4 = true;
                            strExMessage += " Aux6 Activated " ;
                        }
                        else
                        {
                            Baux4 = false;

                        }

                    }
                    if (dr["StrucValue"].ToString() == "Aux5")
                    {
                        if (dr["StrucCondition"].ToString() != "=1")
                        {
                            Baux5 = true;
                            strExMessage += " Aux6 Activated " ;
                        }
                        else
                        {
                            Baux5 = false;

                        }

                    }
                    if (dr["StrucValue"].ToString() == "Aux6")
                    {
                        if (dr["StrucCondition"].ToString() != "=1")
                        {
                            Baux6 = true;
                            strExMessage += " Aux6 Activated " ;
                        }
                        else
                        {
                            Baux6 = false;

                        }

                    }

                    bAuxillaries = (Baux1 || Baux2 || Baux3 || Baux4 || Baux5 || Baux6);

                }

                if (dr["RuleType"].ToString() == "5")  // Condition for Time
                {
                    if (dr["StrucCondition"].ToString() == "<=")
                    {
                        Double dtmp=Convert.ToInt32(dr["StrucValue"]);
                        TimeSpan ts = TimeSpan.FromSeconds(dtmp);
                        DateTime dt = Convert.ToDateTime(DateTime.Now.ToString("dd/MMM/yyyy"));
                        dt = dt + ts;


                        if (gpsCord.DateTimeGPS_UTC <= dt)
                        {
                            trTime= true;
                            strExMessage += " Time : " + gpsCord.DateTimeGPS_UTC + " <= " + dt.ToString(); 

                        }
                        else
                        {
                            trTime = false;
                        }

                    }

                    if (dr["StrucCondition"].ToString() == ">=")
                    {
                        Double dtmp = Convert.ToInt32(dr["StrucValue"]);
                        TimeSpan ts = TimeSpan.FromSeconds(dtmp);
                        DateTime dt = Convert.ToDateTime(DateTime.Now.ToString("dd/MMM/yyyy"));
                        dt = dt + ts;

                        if (gpsCord.DateTimeGPS_UTC >= dt)
                        {
                            trTime = true;
                            strExMessage += " Time : " + gpsCord.DateTimeGPS_UTC + " >= " + dt.ToString(); 

                        }
                        else
                        {
                            trTime = false;
                        }
                    }
                }
                bTriggerFired = (trSpeed && trZone && bAuxillaries && trTime);
            }
        }
    }
}
