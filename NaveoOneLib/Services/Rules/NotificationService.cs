﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NaveoOneLib.Models;
using System.Data;
using NaveoOneLib.DBCon;
using NaveoOneLib.Common;
using NaveoOneLib.Services;

using System.Globalization;
using System.Text.RegularExpressions;
using System.Diagnostics;
using NaveoOneLib.Maps;
using static NaveoOneLib.Services.HttpRequestService;
using System.Threading.Tasks;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Users;
using NaveoOneLib.Models.Rules;
using NaveoOneLib.Models.GPS;
using NaveoOneLib.Services.GPS;
using NaveoOneLib.Services.Users;
//using System.Speech.Synthesis;

namespace NaveoOneLib.Services.Rules
{
    /* ReportID
    110 SMS Notification
    111
    112 Email Notification
    113 Push Notification 
    */
    public partial class NotificationService
    {
        public static StringBuilder ShowPopUpNotification(int UID, String sConnStr)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                //Fetch Unread Notifications
                String sql = @"
                        Select 'dtResult' as TableName
                        SELECT n.DateTimeGPS_UTC, a.AssetName Asset, r.RuleName, n.ReportID, n.Status, a.TimeZoneID
                        from GFI_SYS_Notification n
	                        inner join GFI_GPS_Exceptions e on n.ExceptionID = e.iID
	                        inner join GFI_FLT_Asset a on e.AssetID = a.AssetID
	                        inner join GFI_GPS_Rules r on e.RuleID = r.RuleID
                        WHERE n.Status = 0 

                            and n.ReportID in ('111','113')
                            and n.uid = " + UID.ToString() + @"

                        UPDATE GFI_SYS_Notification
                            SET SendAt = getdate() ,Status = 2 
                        from GFI_SYS_Notification
                        WHERE Status = 0  
                            and ReportID in ('111','113')
                            and uid = " + UID.ToString();

                Connection c = new Connection(sConnStr);
                DataTable dtSql = c.dtSql();
                DataRow drSql = dtSql.NewRow();

                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "MultipleDTfromQuery";
                dtSql.Rows.Add(drSql);
                DataSet ds = c.GetDataDS(dtSql, sConnStr);

                if (ds != null)
                    if (ds.Tables.Contains("dtResult"))
                        foreach (DataRow r in ds.Tables["dtResult"].Rows)
                        {
                            String tzName = r["TimeZoneID"].ToString();
                            if (tzName == String.Empty)
                                tzName = TimeZone.CurrentTimeZone.StandardName;
                            TimeSpan ts = new Utils().GetTimeSpan(tzName);

                            sb.Append(BaseService.Decrypt(r["Asset"].ToString()) + " Broken Exception " + r["RuleName"].ToString() + " on " + ((DateTime)r["DateTimeGPS_UTC"] + ts).ToString() + "\n");

                            if (r["ReportID"].ToString() == "113")
                                if (sb.Length > 0)
                                    sb.Append("TingPow113");
                            //Console.Beep(5000, 500);
                            //SpeechSynthesizer ss = new SpeechSynthesizer();
                            //ss.Volume = 100;
                            //ss.Rate = 0;
                            //ss.SelectVoiceByHints(VoiceGender.Female);
                            //ss.SpeakAsync(sb.ToString());
                        }
            }
            catch (Exception ex)
            {
                sb.Append(ex.Message);
            }
            return sb;
        }
        public static DataTable GetPushNotifData(String sConnStr)
        {
            DataTable dt = new DataTable();
            try
            {
                //Fetch Unread Notifications
                //113 Popup wiz sound
                String sql = @"
Select 'dtResult' as TableName
SELECT u.email, n.Title
from GFI_SYS_Notification n 
inner join GFI_SYS_User u on u.UID = n.UID
WHERE n.Status = 0 and n.ReportID = 113 and n.InsertedAt >= GETDATE() - 1

UPDATE GFI_SYS_Notification
    SET SendAt = getdate() ,Status = 2 
from GFI_SYS_Notification
WHERE Status = 0  
    and ReportID = 113";

                Connection c = new Connection(sConnStr);
                DataTable dtSql = c.dtSql();
                DataRow drSql = dtSql.NewRow();

                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "MultipleDTfromQuery";
                dtSql.Rows.Add(drSql);
                DataSet ds = c.GetDataDS(dtSql, sConnStr);

                if (ds != null)
                    if (ds.Tables.Contains("dtResult"))
                        dt = ds.Tables["dtResult"];
            }
            catch (Exception ex)
            {
                dt = new DataTable();
                dt.Columns.Add("email");
                dt.Columns.Add("Title");
                DataRow dr = dt.NewRow();
                dr["email"] = "Support@naveo.mu";
                dr["Title"] = sConnStr + " > " + ex.ToString();
            }
            return dt;
        }

        public Boolean SaveNotification(DataTable dtNotification, String sConnStr)
        {
            if (dtNotification.Columns.Contains("DateTimeGPS_Local"))
                dtNotification.Columns.Remove("DateTimeGPS_Local");

            if (dtNotification.Columns.Contains("Email"))
                dtNotification.Columns.Remove("Email");

            if (dtNotification.Columns.Contains("SMSNumber"))
                dtNotification.Columns.Remove("SMSNumber");

            if (dtNotification.Columns.Contains("Asset"))
                dtNotification.Columns.Remove("Asset");

            if (dtNotification.Columns.Contains("Driver"))
                dtNotification.Columns.Remove("Driver");

            if (dtNotification.Columns.Contains("RuleName"))
                dtNotification.Columns.Remove("RuleName");

            if (dtNotification.Columns.Contains("Lat"))
                dtNotification.Columns.Remove("Lat");

            if (dtNotification.Columns.Contains("Lon"))
                dtNotification.Columns.Remove("Lon");

            if (dtNotification.Columns.Contains("StartLat"))
                dtNotification.Columns.Remove("StartLat");

            if (dtNotification.Columns.Contains("StartLon"))
                dtNotification.Columns.Remove("StartLon");

            if (dtNotification.Columns.Contains("RuleCondition"))
                dtNotification.Columns.Remove("RuleCondition");

            DataTable dt = dtNotification;
            dt.TableName = "GFI_SYS_Notification";

            if (dt.Columns.Contains("DriverID"))
                foreach (DataRow dr in dt.Rows)
                    if (String.IsNullOrEmpty(dr["DriverID"].ToString()))
                        dr["DriverID"] = DBNull.Value;

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "NID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            if (dtNotification.Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(dtNotification.Rows[0]["ParentRuleID"].ToString()))
                {
                    drCtrl = dtCtrl.NewRow();
                    drCtrl["SeqNo"] = 2;
                    drCtrl["TblName"] = "None";
                    drCtrl["CmdType"] = "STOREDPROC";
                    drCtrl["Command"] = "UPDATE GFI_GPS_Exceptions SET Posted = 2 WHERE Posted = 1 and InsertedAt > getdate() - 1;";
                    dtCtrl.Rows.Add(drCtrl);
                }
            }

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                        dri["oprType"] = DataRowState.Added;
            }

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public Boolean UpdateExceptionsProcessed(String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["Command"] = "UPDATE GFI_GPS_Exceptions SET Posted = 2 WHERE Posted = 1;";
            dtCtrl.Rows.Add(drCtrl);

            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public DataSet GetNotificationData(String sConnStr)
        {
            String sql = @"
--GetNotificationData
--SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 

select 'dtARC' as TableName
SELECT arc.ARID 
    , TargetID 
    , ReportID 
    , ReportPeriod                 
    , d.UID
    , UIDType
    , UIDCategory   
    , Username
    , Names
    , Email
FROM GFI_FLT_AutoReportingConfig  arc                        
    left outer join GFI_FLT_AutoReportingConfigDetails d on arc.ARID = d.ARID
    left outer join GFI_SYS_User u on d.UID = u.UID
WHERE ReportID in (110, 111, 112, 113);
      
UPDATE GFI_GPS_Exceptions
    SET Posted = 1 
FROM GFI_GPS_Exceptions WITH (READPAST)
WHERE Posted = 0 and InsertedAt >= GETDATE() - 1;

select 'dtException' as TableName
SELECT iID 
    , e.RuleID
    , e.GPSDataID
    , e.AssetID
    , e.DriverID
    , e.Severity
    , e.GroupID
    , g.DateTimeGPS_UTC
    , g.Speed
    , g.Latitude
    , g.Longitude
    , a.AssetName
    , a.AssetNumber
	, a.TimeZoneID
    , d.sDriverName
    , r.RuleName 
    , r.ParentRuleID
from GFI_GPS_Exceptions e WITH (READPAST)
    left outer join GFI_GPS_GPSData g WITH (READPAST) on e.GPSDataID = g.UID
    left outer join GFI_FLT_Asset a on  e.AssetID = a.AssetID
    left outer join GFI_FLT_Driver d on  e.DriverID = d.DriverID
    left outer join GFI_GPS_Rules r on  e.RuleID = r.RuleID
WHERE Posted = 1 and e.InsertedAt >= GETDATE() - 1 
    order by g.DateTimeGPS_UTC;";

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);
            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            return ds;
        }
        static String IsMaxMailSent(string UID, String sConnStr)
        {
            //0 MaxMailExceeded 
            //1 MaxMailReached
            //2 MaxMailNotReached
            String sql = @"
SELECT count(distinct nid) as TotalSent 
	FROM GFI_SYS_Notification n
WHERE n.UID = " + UID.ToString() + @"
    and n.Status = 2 
    and dateadd(dd, datediff(dd, 0, n.SendAt), 0) = dateadd(dd, datediff(dd, 0, GETDATE()), 0)
    and n.ReportID in (110, 112);

select MaxDayMail from GFI_SYS_User WHERE UID = " + UID.ToString();

            DataSet ds = new Connection(sConnStr).GetDS(sql, sConnStr);

            if (ds.Tables[0].Rows.Count >= 0)
            {
                int i = 100;
                if (int.TryParse(ds.Tables[0].Rows[0][0].ToString(), out i))
                {
                    int iMaxMail = 25;
                    int.TryParse(ds.Tables[1].Rows[0][0].ToString(), out iMaxMail);

                    if (i == iMaxMail)
                        return "MaxMailReached";

                    if (i > iMaxMail)
                        return "MaxMailExceeded";
                }
                return "MaxMailNotReached";
            }

            return "MaxMailNotReached";
        }
        String NoOfMailsSent2Day()
        {
            String sql = @"
SELECT u.Email, count(distinct nid) as count 
	FROM GFI_SYS_Notification n
		inner join GFI_FLT_AutoReportingConfigDetails d on n.ARID = d.ARID
		inner join GFI_SYS_User u on u.UID = n.UID
    WHERE n.Status = 2 
		and dateadd(dd, datediff(dd, 0, n.SendAt), 0) = dateadd(dd, datediff(dd, 0, GETDATE()), 0)
	Group by u.Email
	order by u.Email
";
            return sql;
        }
        String NoOfMailsSentPerDay()
        {
            String sql = @"
SELECT dateadd(dd, datediff(dd, 0, n.SendAt), 0) sendAt, count(distinct nid) as count 
	FROM GFI_SYS_Notification n
		inner join GFI_FLT_AutoReportingConfigDetails d on n.ARID = d.ARID
    WHERE n.Status = 2 
		and n.SendAt > '2015-12-15 00:48:08.703'
        and n.ReportID in (110, 112)
	Group by dateadd(dd, datediff(dd, 0, n.SendAt), 0)
	order by dateadd(dd, datediff(dd, 0, n.SendAt), 0)";
            return sql;
        }
        String NoOfMailsSentPerDayPerMail()
        {
            String sql = @"
SELECT dateadd(dd, datediff(dd, 0, n.SendAt), 0) sendAt, u.Email, count(distinct nid) as count 
	FROM GFI_SYS_Notification n
		inner join GFI_FLT_AutoReportingConfigDetails d on n.ARID = d.ARID
   		inner join GFI_SYS_User u on u.UID = n.UID
 WHERE n.Status = 2 
		--and dateadd(dd, datediff(dd, 0, n.SendAt), 0) = dateadd(dd, datediff(dd, 0, GETDATE()), 0)
		and n.SendAt > '2015-12-15 00:48:08.703'
        and n.ReportID in (110, 112)
	Group by u.Email, dateadd(dd, datediff(dd, 0, n.SendAt), 0)
	order by u.Email
";
            return sql;
        }

        private static Boolean sendNotificationData(DataTable dtNotification, DataSet dsZones, String sConnStr)
        {
            Boolean b = true;
            MailSender ms = new MailSender();
            SmsSender sms = new Services.SmsSender();
            ESRIMapService esriMap = new ESRIMapService();
            esriMap.ReadProjectNLoadMap();

            foreach (DataRow r in dtNotification.Rows)
            {
                if (r["ReportID"].ToString() == "110" || r["ReportID"].ToString() == "112")//Email Notification
                {
                    String sEmail = String.Empty;
                    if (r["ReportID"].ToString() == "112")  //Email Notification
                        sEmail = r["Email"].ToString();
                    else
                        sEmail = r["SMSNumber"].ToString(); //This should be SMS

                    ReportNames.Report re = (ReportNames.Report)Convert.ToInt32(r["ReportID"].ToString());
                    if (sEmail != String.Empty)
                    {
                        //Check if max mails has been sent 
                        String MailsReceived = IsMaxMailSent(r["UID"].ToString(), sConnStr);
                        if (MailsReceived == "MaxMailNotReached" || MailsReceived == "MaxMailReached")
                        {
                            StringBuilder sb = new StringBuilder();
                            String ss = r["Lon"].ToString();
                            Double dLon = Convert.ToDouble(r["Lon"].ToString());
                            Double dLat = Convert.ToDouble(r["Lat"].ToString());
                            Double dSLon = Convert.ToDouble(r["StartLon"].ToString());
                            Double dSLat = Convert.ToDouble(r["StartLat"].ToString());

                            sb.Append("\n" + r["Driver"].ToString() + " In ");
                            String sAssetName = String.Empty;
                            //try
                            //{
                            sAssetName = r["Asset"].ToString();
                            //sAssetName = BaseService.Decrypt(r["Asset"].ToString());
                            //}
                            //catch { }
                            sb.Append(sAssetName + " Broke '");
                            sb.Append(r["RuleName"].ToString() + "' On ");
                            sb.Append(r["DateTimeGPS_Local"].ToString() + ".");

                            sb.Append("\r\n");
                            String sAddressWording = String.Empty;
                            String strAddress = esriMap.GetZone(dSLon, dSLat, dsZones, sConnStr) + " " + esriMap.GetAddress(dSLon, dSLat);
                            if (dLon != dSLon)
                                sAddressWording = strAddress + " to " + esriMap.GetZone(dLon, dLat, dsZones, sConnStr) + " " + esriMap.GetAddress(dLon, dLat);
                            else
                                sAddressWording = "Place : " + strAddress;
                            String sSMS = sb.ToString() + " " + sAddressWording;

                            sb.Append(sAddressWording);
                            sb.Append("\n\n");

                            #region GPSDataDetails
                            try
                            {
                                int iGPSUID = -1;
                                if (int.TryParse(r["GPSDataUID"].ToString(), out iGPSUID))
                                {
                                    String sGPSDataDetails = String.Empty;
                                    try
                                    {
                                        //Not yet tested. Thats why in Try Catch
                                        GPSData g = new GPSDataService().GetGPSDataByUID(iGPSUID, sConnStr);
                                        if (g.GPSDataDetails.Count > 0)
                                        {
                                            sGPSDataDetails = "\r\nAdditional Information :\r\n";

                                            int iSpeed = 0;
                                            int.TryParse(g.Speed.ToString(), out iSpeed);
                                            sGPSDataDetails += "Speed : " + iSpeed + "\r\n";

                                            foreach (GPSDataDetail d in g.GPSDataDetails)
                                                sGPSDataDetails += d.TypeID + " " + d.TypeValue + " " + d.UOM + "\r\n";
                                        }
                                    }
                                    catch
                                    {
                                        List<GPSDataDetail> l = new GPSDataDetailService().GetGPSDataDetailsByUID(iGPSUID, sConnStr);
                                        if (l.Count > 0)
                                        {
                                            sGPSDataDetails = "Additional Information... : ";
                                            foreach (GPSDataDetail d in l)
                                                sGPSDataDetails += d.TypeID + " " + d.TypeValue + " " + d.UOM + " ";
                                        }
                                    }

                                    sb.Append(sGPSDataDetails);
                                    sb.Append("\n\n");
                                }
                            }
                            catch { }
                            #endregion

                            //string sURL = string.Format("https://www.here.com/?map={0},{1},14,normal", dLon.ToString(), dLat.ToString());
                            string sURL = string.Format("http://maps.google.com/maps?q=loc:{0},{1}", dLat.ToString(), dLon.ToString());
                            sb.Append(sURL);

                            var RuleCon = r["RuleCondition"].ToString();

                            String sTitle = sAssetName + " Broke Exception " + r["RuleName"].ToString() + RuleCon;
                            if (sTitle.Length > 400)
                                sTitle = sTitle.Substring(0, 399);

                            if (MailsReceived == "MaxMailReached")
                                sb.Insert(0, "Maximum mail per day sent. You will no more be notified by mail today. \r\n");

                            String s = String.Empty;
                            if (r["ReportID"].ToString() == "112")
                                s = ms.SendMail(sEmail.ToString(), sTitle, sb.ToString(), String.Empty, sConnStr);
                            else
                                s = sms.SendSMS(sEmail.ToString(), sSMS);

                            if (s == "Mail Sent")
                            {
                                r["Status"] = 2;
                                r["SendAt"] = DateTime.Now;
                            }
                            else
                            {
                                r["Status"] = 0;
                                r["SendAt"] = null;
                            }

                            r["Err"] = s;
                            r["sBody"] = sb.ToString();
                            r["Title"] = sTitle;
                        }
                        else
                        {
                            r["Status"] = 3;
                            r["SendAt"] = DateTime.Now;
                            r["Err"] = "Max of 25 mails sent already.";
                        }
                    }
                }
                else if (r["ReportID"].ToString() == "113")//Push Notification
                {
                    String sEmail = r["Email"].ToString();
                    if (sEmail != String.Empty)
                    {
                        StringBuilder sb = new StringBuilder();
                        Double dLon = Convert.ToDouble(r["Lon"].ToString());
                        Double dLat = Convert.ToDouble(r["Lat"].ToString());
                        Double dSLon = Convert.ToDouble(r["StartLon"].ToString());
                        Double dSLat = Convert.ToDouble(r["StartLat"].ToString());

                        sb.Append("\n" + r["Driver"].ToString() + " In ");
                        String sAssetName = String.Empty;
                        sAssetName = r["Asset"].ToString();
                        sb.Append(sAssetName + " Broke '");
                        sb.Append(r["RuleName"].ToString() + "' On ");
                        sb.Append(r["DateTimeGPS_Local"].ToString() + ".");

                        //String sAddressWording = String.Empty;
                        //String strAddress = esriMap.GetZone(dSLon, dSLat, dsZones, sConnStr) + " " + esriMap.GetAddress(dSLon, dSLat);
                        //if (dLon != dSLon)
                        //    sAddressWording = strAddress + " to " + esriMap.GetZone(dLon, dLat, dsZones, sConnStr) + " " + esriMap.GetAddress(dLon, dLat);
                        //else
                        //    sAddressWording = "Place : " + strAddress;

                        //sb.Append(sAddressWording);

                        String sTitle = sAssetName + " Broke Exception " + r["RuleName"].ToString();
                        String response = SendPushNotifOnMobile(new Body() { message = sb.ToString(), email = sEmail }, sConnStr);

                        r["Status"] = 2;
                        r["SendAt"] = DateTime.Now;

                        r["Err"] = "Push notif " + response;
                        r["sBody"] = sb.ToString();
                        r["Title"] = sTitle;
                    }
                }

                //Still in for loop
                DataTable dt = dtNotification.Clone();
                dt.Rows.Add(r.ItemArray);
                Boolean bSave = new NotificationService().SaveNotification(dt, sConnStr);
                if (!bSave)
                {
                    try
                    {
                        Errors.WriteToLog("\r\n\r\n...Err during save Notif : ");
                        foreach (DataRow dr in dt.Rows)
                            foreach (DataColumn dc in dt.Columns)
                                Errors.WriteToLog(dc.ToString() + " > " + dr[dc].ToString());
                    }
                    catch { }
                }
                b = b && bSave;
            }
            esriMap.UnloadProject();

            return b;
        }

        public static String getNotificationData(String sConnStr)
        {
            String sResult = "Processing Notification Data. ";
            ESRIMapService esriMap = new ESRIMapService();
            Boolean bMap = esriMap.LoadMapNeeded();
            if (bMap)
                esriMap.LoadMap();

            try
            {
                //Get data from multiple table in dataset
                Notification n = new Notification();
                DataSet ds = new NotificationService().GetNotificationData(sConnStr);

                if (ds == null)
                    return sResult + "No Data...";

                if (ds.Tables.Count == 0)
                    return sResult + "No Data";

                try
                {
                    if (ds.Tables.Contains("dtException"))
                    {
                        if (ds.Tables["dtException"].Rows.Count == 0 || ds.Tables["dtException"] == null)
                            return sResult + "No exception Data.";
                    }
                }
                catch (Exception ex)
                {
                    return sResult + ex.ToString() + "dtEXCEPTIONS";
                }

                DataTable dtDistinctDetailsID = new DataTable();
                if (ds.Tables.Contains("dtException"))
                {
                    //Group by Exception Table
                    DataView DetailsView = new DataView(ds.Tables["dtException"]);
                    String[] grp = new String[3];
                    grp[0] = "AssetID";
                    grp[1] = "ParentRuleID";
                    grp[2] = "GroupID";

                    dtDistinctDetailsID = DetailsView.ToTable(true, grp);
                }
                //loop into each AutoReportConfig 
                DataTable dtNotification = NotificationDT();
                //Assumptions made : All users are in same group
                List<Matrix> lMatrix = new List<Matrix>();
                User myUser = new User();
                UserService userService = new UserService();
                if (ds.Tables.Contains("dtException"))
                {
                    if (ds.Tables.Contains("dtARC"))
                    {
                        foreach (DataRow r in ds.Tables["dtARC"].Rows)
                        {
                            int iUIDChk = -1;
                            if (!Int32.TryParse(r["UID"].ToString(), out iUIDChk))
                                continue;

                            if (dtDistinctDetailsID != null)
                            {
                                DataRow[] rows = dtDistinctDetailsID.Select("ParentRuleID='" + r["TargetID"].ToString() + "'");
                                if (rows.Length == 0)
                                    continue;

                                List<int> li = new List<int>();

                                int iilD = 0;
                                int ParentRuleID = 0;

                                #region GMID
                                if (r["UIDCategory"].ToString() == "GMID")
                                {
                                    #region Checking if user has access to Asset
                                    foreach (DataRow rExp in rows)
                                    {
                                        int i = -1;
                                        int.TryParse(r["UID"].ToString(), out i);
                                        DataTable dtGMAsset = new NaveoOneLib.WebSpecifics.LibData().dtGMAsset(i, sConnStr);
                                        DataRow[] drAsset = dtGMAsset.Select("StrucID = " + rExp["AssetID"].ToString());
                                        if (drAsset.Length == 0)
                                            continue;
                                    }
                                    #endregion

                                    li.Add(Convert.ToInt32(r["UID"].ToString()));
                                    List<User> lu = userService.lGetUsers(li, sConnStr);
                                    foreach (User u in lu)
                                    {
                                        try
                                        {
                                            foreach (Matrix m in u.lMatrix)
                                                if (!lMatrix.Contains(m))
                                                    lMatrix.Add(m);

                                            foreach (DataRow rExp in rows)
                                            {
                                                //Add into Notification List
                                                DataRow N = dtNotification.NewRow();
                                                N["ARID"] = (int)r["ARID"];

                                                DataRow iIDs = ds.Tables["dtException"]
                                                    .Select("AssetID = '" + rExp["AssetID"].ToString()
                                                    + "' and RuleID= '" + rExp["ParentRuleID"].ToString()
                                                    + "' and GroupID = '" + rExp["GroupID"].ToString() + "'").LastOrDefault(); //[0];
                                                DataRow SiID = ds.Tables["dtException"]
                                                       .Select("AssetID = '" + rExp["AssetID"].ToString()
                                                       + "' and RuleID= '" + rExp["ParentRuleID"].ToString()
                                                       + "' and GroupID = '" + rExp["GroupID"].ToString() + "'").FirstOrDefault();

                                                if (SiID != null)
                                                {
                                                    iilD = (int)SiID["iID"];

                                                    N["ExceptionID"] = (int)SiID["iID"];
                                                    N["Status"] = 0;
                                                    N["UID"] = u.UID;
                                                    N["UIDCategory"] = "ID";
                                                    N["SendAt"] = null;
                                                    N["ReportID"] = r["ReportID"].ToString();
                                                    N["GPSDataUID"] = SiID["GPSDataID"];

                                                    N["AssetID"] = SiID["AssetID"].ToString();
                                                    N["DriverID"] = SiID["DriverID"].ToString();
                                                    ParentRuleID = Convert.ToInt32(rExp["ParentRuleID"]);
                                                    N["ParentRuleID"] = rExp["ParentRuleID"];

                                                    N["Email"] = u.Email.ToString();
                                                    N["SMSNumber"] = u.MobileNo.ToString();
                                                    N["Asset"] = SiID["AssetNumber"].ToString();
                                                    N["Driver"] = SiID["sDriverName"].ToString();
                                                    N["RuleName"] = SiID["RuleName"].ToString();

                                                    N["Lat"] = String.IsNullOrEmpty(iIDs["Latitude"].ToString()) ? 0 : (double)iIDs["Latitude"];
                                                    N["Lon"] = String.IsNullOrEmpty(iIDs["Longitude"].ToString()) ? 0 : (double)iIDs["Longitude"];
                                                    N["StartLat"] = String.IsNullOrEmpty(SiID["Latitude"].ToString()) ? 0 : (double)SiID["Latitude"];
                                                    N["StartLon"] = String.IsNullOrEmpty(SiID["Longitude"].ToString()) ? 0 : (double)SiID["Longitude"];

                                                    N["Type"] = "Rule";
                                                    String tzName = SiID["TimeZoneID"].ToString();
                                                    if (tzName == String.Empty)
                                                        tzName = TimeZone.CurrentTimeZone.StandardName;
                                                    TimeSpan ts = new Utils().GetTimeSpan(tzName);
                                                    //N["DateTimeGPS_Local"] = ((DateTime)iIDs["DateTimeGPS_UTC"] + ts).ToString();
                                                    //N["DateTimeGPS_UTC"] = ((DateTime)iIDs["DateTimeGPS_UTC"]).ToString();

                                                    N["DateTimeGPS_Local"] = String.IsNullOrEmpty(SiID["DateTimeGPS_UTC"].ToString()) ? DateTime.UtcNow.ToString() : ((DateTime)SiID["DateTimeGPS_UTC"] + ts).ToString();
                                                    N["DateTimeGPS_UTC"] = String.IsNullOrEmpty(SiID["DateTimeGPS_UTC"].ToString()) ? DateTime.UtcNow.ToString() : ((DateTime)SiID["DateTimeGPS_UTC"]).ToString();

                                                    NaveoOneLib.Models.Rules.Rule rrr = new RulesService().GetRulesByParentId(rExp["ParentRuleID"].ToString(), sConnStr);
                                                    if (rrr != null)
                                                    {
                                                        var Condition = rrr.lRules.Where(x => x.StrucType == "Speed").Select(x => x.StrucCondition).FirstOrDefault();
                                                        var value = rrr.lRules.Where(x => x.StrucType == "Speed").Select(x => x.StrucValue).FirstOrDefault();
                                                        string concat = " with condition Speed " + Condition + " " + value + " km/h";

                                                        N["RuleCondition"] = concat;
                                                    }
                                                    dtNotification.Rows.Add(N);
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            int ARID = (int)r["ARID"];
                                            int ExceptionID = iilD;
                                            string errorMessage = ex.ToString();

                                            Errors.WriteToLog("\r\n\r\n...Err during save preparing of Data : ");
                                            Errors.WriteToLog("\r\n\r\n...GMID PART");
                                            Errors.WriteToLog("\r\n\r\n...ARID " + ARID);
                                            Errors.WriteToLog("\r\n\r\n...ExceptionID " + ExceptionID);
                                            Errors.WriteToLog("\r\n\r\n...ParentRuleID " + ParentRuleID);
                                            Errors.WriteToLog("\r\n\r\n...Erorr: " + errorMessage);
                                            //return sResult + errorMessage;
                                        }
                                    }
                                }
                                #endregion
                                #region UID
                                else//Only IDS 
                                {
                                    foreach (DataRow rExp in rows)
                                    {
                                        try
                                        {
                                            #region Checking if user has access to Asset
                                            int i = -1;
                                            int.TryParse(r["UID"].ToString(), out i);
                                            DataTable dtGMAsset = new NaveoOneLib.WebSpecifics.LibData().dtGMAsset(i, sConnStr);
                                            DataRow[] drAsset = dtGMAsset.Select("StrucID = " + rExp["AssetID"].ToString());
                                            if (drAsset.Length == 0)
                                                continue;
                                            #endregion

                                            User u = userService.GetUserById((Convert.ToInt32(r["UID"].ToString())), User.EditModePswd, sConnStr);
                                            foreach (Matrix m in u.lMatrix)
                                                if (!lMatrix.Contains(m))
                                                    lMatrix.Add(m);

                                            //Add into Notification List
                                            DataRow N = dtNotification.NewRow();
                                            N["ARID"] = (int)r["ARID"];
                                            DataRow iIDs = ds.Tables["dtException"]
                                                .Select("AssetID = '" + rExp["AssetID"].ToString()
                                                + "' and RuleID= '" + rExp["ParentRuleID"].ToString()
                                                + "' and GroupID = '" + rExp["GroupID"].ToString() + "'").LastOrDefault(); //.[0];
                                            DataRow SiID = ds.Tables["dtException"]
                                                   .Select("AssetID = '" + rExp["AssetID"].ToString()
                                                   + "' and RuleID= '" + rExp["ParentRuleID"].ToString()
                                                   + "' and GroupID = '" + rExp["GroupID"].ToString() + "'").FirstOrDefault();
                                            N["ExceptionID"] = (int)iIDs["iID"];
                                            N["Status"] = 0;
                                            N["UID"] = r["UID"];
                                            N["UIDCategory"] = r["UIDCategory"];
                                            N["SendAt"] = null;
                                            N["ReportID"] = r["ReportID"].ToString();
                                            N["GPSDataUID"] = SiID["GPSDataID"];
                                            N["AssetID"] = iIDs["AssetID"].ToString();
                                            N["DriverID"] = iIDs["DriverID"].ToString();
                                            N["ParentRuleID"] = rExp["ParentRuleID"];
                                            N["Email"] = u.Email.ToString();
                                            N["SMSNumber"] = u.MobileNo.ToString();
                                            N["Asset"] = iIDs["AssetNumber"].ToString();
                                            N["Driver"] = iIDs["sDriverName"].ToString();
                                            N["RuleName"] = iIDs["RuleName"].ToString();
                                            N["Lat"] = String.IsNullOrEmpty(iIDs["Latitude"].ToString()) ? 0 : (double)iIDs["Latitude"];
                                            N["Lon"] = String.IsNullOrEmpty(iIDs["Longitude"].ToString()) ? 0 : (double)iIDs["Longitude"];
                                            N["StartLat"] = String.IsNullOrEmpty(SiID["Latitude"].ToString()) ? 0 : (double)SiID["Latitude"];
                                            N["StartLon"] = String.IsNullOrEmpty(SiID["Longitude"].ToString()) ? 0 : (double)SiID["Longitude"];
                                            N["Type"] = "Rule";
                                            String tzName = iIDs["TimeZoneID"].ToString();
                                            if (tzName == String.Empty)
                                                tzName = TimeZone.CurrentTimeZone.StandardName;
                                            TimeSpan ts = new Utils().GetTimeSpan(tzName);
                                            N["DateTimeGPS_Local"] = String.IsNullOrEmpty(SiID["DateTimeGPS_UTC"].ToString()) ? DateTime.UtcNow.ToString() : ((DateTime)SiID["DateTimeGPS_UTC"] + ts).ToString();
                                            N["DateTimeGPS_UTC"] = String.IsNullOrEmpty(SiID["DateTimeGPS_UTC"].ToString()) ? DateTime.UtcNow.ToString() : ((DateTime)SiID["DateTimeGPS_UTC"]).ToString();

                                            NaveoOneLib.Models.Rules.Rule rrr = new RulesService().GetRulesByParentId(rExp["ParentRuleID"].ToString(), sConnStr);
                                            if (rrr != null)
                                            {
                                                var Condition = rrr.lRules.Where(x => x.StrucType == "Speed").Select(x => x.StrucCondition).FirstOrDefault();
                                                var value = rrr.lRules.Where(x => x.StrucType == "Speed").Select(x => x.StrucValue).FirstOrDefault();
                                                string concat = " with condition Speed " + Condition + " " + value + " km/h";

                                                N["RuleCondition"] = concat;
                                            }
                                            dtNotification.Rows.Add(N);
                                        }
                                        catch (Exception ex)
                                        {
                                            string a = ex.ToString();
                                            int ARID = (int)r["ARID"];
                                            int ExceptionID = iilD;
                                            string errorMessage = ex.ToString();

                                            Errors.WriteToLog("\r\n\r\n...Err during save preparing of Data : ");
                                            Errors.WriteToLog("\r\n\r\n...ID PART");
                                            Errors.WriteToLog("\r\n\r\n...ARID " + ARID);
                                            Errors.WriteToLog("\r\n\r\n...ExceptionID " + ExceptionID);
                                            Errors.WriteToLog("\r\n\r\n...ParentRuleID " + ParentRuleID);
                                            Errors.WriteToLog("\r\n\r\n...Erorr: " + errorMessage);
                                        }
                                    }
                                }
                                #endregion
                            }
                        }
                    }
                }

                if (dtNotification.Rows.Count == 0)
                {
                    try
                    {
                        Boolean b = new NotificationService().UpdateExceptionsProcessed(sConnStr);
                        return sResult + b.ToString() + " No exceptions notification detected";
                    }
                    catch (Exception ex)
                    {
                    }
                }

                List<int> lm = new List<int>();
                foreach (Matrix m in lMatrix)
                    if (!lm.Contains(m.GMID))
                        lm.Add(m.GMID);
                DataSet dsZones = new WebSpecifics.LibData().dsZones(lm, sConnStr, null, null);
                if (sendNotificationData(dtNotification, dsZones, sConnStr))
                    return sResult + "Rules Exception is Successful";
                else
                    return sResult + "Error in Rules Exception. Seems could not save in Notification Table ";
            }
            catch (Exception ex)
            {
                return sResult + "Error in Rules Exception: " + ex.ToString();
            }
            finally
            {
                esriMap = null;
            }
        }

        //public static String getNotificationData(String sConnStr)
        //{
        //    String sResult = "Processing Notification Data. ";
        //    ESRIMapService esriMap = new ESRIMapService();
        //    Boolean bMap = esriMap.LoadMapNeeded();
        //    if (bMap)
        //        esriMap.LoadMap();

        //    try
        //    {
        //        //Get data from multiple table in dataset
        //        Notification n = new Notification();
        //        DataSet ds = new NotificationService().GetNotificationData(sConnStr);

        //        if (ds == null)
        //            return sResult + "No Data...";

        //        if (ds.Tables.Count == 0)
        //            return sResult + "No Data";

        //        if(ds.Tables["dtException"].Rows.Count == 0)
        //            return sResult + "No exception Data";

        //        //Group by Exception Table
        //        DataView DetailsView = new DataView(ds.Tables["dtException"]);
        //        String[] grp = new String[3];
        //        grp[0] = "AssetID";
        //        grp[1] = "ParentRuleID";
        //        grp[2] = "GroupID";

        //        DataTable dtDistinctDetailsID = DetailsView.ToTable(true, grp);

        //        //loop into each AutoReportConfig 
        //        DataTable dtNotification = NotificationDT();
        //        //Assumptions made : All users are in same group
        //        List<Matrix> lMatrix = new List<Matrix>();
        //        User myUser = new User();
        //        UserService userService = new UserService();
        //        foreach (DataRow r in ds.Tables["dtARC"].Rows)
        //        {
        //            int iUIDChk = -1;
        //            if (!Int32.TryParse(r["UID"].ToString(), out iUIDChk))
        //                continue;

        //            DataRow[] rows = dtDistinctDetailsID.Select("ParentRuleID='" + r["TargetID"].ToString() + "'");
        //            List<int> li = new List<int>();
        //            #region GMID
        //            if (r["UIDCategory"].ToString() == "GMID")
        //            {
        //                #region Checking if user has access to Asset
        //                foreach (DataRow rExp in rows)
        //                {
        //                    int i = -1;
        //                    int.TryParse(r["UID"].ToString(), out i);
        //                    DataTable dtGMAsset = new NaveoOneLib.WebSpecifics.LibData().dtGMAsset(i, sConnStr);
        //                    DataRow[] drAsset = dtGMAsset.Select("StrucID = " + rExp["AssetID"].ToString());
        //                    if (drAsset.Length == 0)
        //                        continue;
        //                }
        //                #endregion

        //                li.Add(Convert.ToInt32(r["UID"].ToString()));
        //                List<User> lu = userService.lGetUsers(li, sConnStr);
        //                foreach (User u in lu)
        //                {
        //                    foreach (Matrix m in u.lMatrix)
        //                        if (!lMatrix.Contains(m))
        //                            lMatrix.Add(m);

        //                    foreach (DataRow rExp in rows)
        //                    {
        //                        //Add into Notification List
        //                        DataRow N = dtNotification.NewRow();
        //                        N["ARID"] = (int)r["ARID"];
        //                        DataRow iIDs = ds.Tables["dtException"]
        //                            .Select("AssetID = '" + rExp["AssetID"].ToString()
        //                            + "' and RuleID= '" + rExp["ParentRuleID"].ToString()
        //                            + "' and GroupID = '" + rExp["GroupID"].ToString() + "'").LastOrDefault(); //[0];
        //                        N["ExceptionID"] = (int)iIDs["iID"];
        //                        N["Status"] = 0;
        //                        N["UID"] = u.UID;
        //                        N["UIDCategory"] = "ID";
        //                        N["SendAt"] = null;
        //                        N["ReportID"] = r["ReportID"].ToString();
        //                        N["GPSDataUID"] = iIDs["GPSDataID"];

        //                        N["AssetID"] = iIDs["AssetID"].ToString();
        //                        N["DriverID"] = iIDs["DriverID"].ToString();
        //                        N["ParentRuleID"] = rExp["ParentRuleID"];

        //                        N["Email"] = u.Email.ToString();
        //                        N["SMSNumber"] = u.MobileNo.ToString();
        //                        N["Asset"] = iIDs["AssetNumber"].ToString();
        //                        N["Driver"] = iIDs["sDriverName"].ToString();
        //                        N["RuleName"] = iIDs["RuleName"].ToString();
        //                        N["Lat"] = (double)iIDs["Latitude"];
        //                        N["Lon"] = (double)iIDs["Longitude"];
        //                        N["StartLat"] = (double)iIDs["Latitude"];   //was SiID
        //                        N["StartLon"] = (double)iIDs["Longitude"];   //was SiID

        //                        N["Type"] = "Rule";
        //                        String tzName = iIDs["TimeZoneID"].ToString();
        //                        if (tzName == String.Empty)
        //                            tzName = TimeZone.CurrentTimeZone.StandardName;
        //                        TimeSpan ts = new Utils().GetTimeSpan(tzName);
        //                        N["DateTimeGPS_Local"] = ((DateTime)iIDs["DateTimeGPS_UTC"] + ts).ToString();
        //                        N["DateTimeGPS_UTC"] = ((DateTime)iIDs["DateTimeGPS_UTC"]).ToString();

        //                        dtNotification.Rows.Add(N);
        //                    }
        //                }
        //            }
        //            #endregion
        //            #region UID
        //            else//Only IDS 
        //            {
        //                foreach (DataRow rExp in rows)
        //                {
        //                    #region Checking if user has access to Asset
        //                    int i = -1;
        //                    int.TryParse(r["UID"].ToString(), out i);
        //                    DataTable dtGMAsset = new NaveoOneLib.WebSpecifics.LibData().dtGMAsset(i, sConnStr);
        //                    DataRow[] drAsset = dtGMAsset.Select("StrucID = " + rExp["AssetID"].ToString());
        //                    if (drAsset.Length == 0)
        //                        continue;
        //                    #endregion
        //                    User u = userService.GetUserById((Convert.ToInt32(r["UID"].ToString())), User.EditModePswd, sConnStr);
        //                    foreach (Matrix m in u.lMatrix)
        //                        if (!lMatrix.Contains(m))
        //                            lMatrix.Add(m);

        //                    //Add into Notification List
        //                    DataRow N = dtNotification.NewRow();
        //                    N["ARID"] = (int)r["ARID"];
        //                    DataRow iIDs = ds.Tables["dtException"]
        //                        .Select("AssetID = '" + rExp["AssetID"].ToString()
        //                        + "' and RuleID= '" + rExp["ParentRuleID"].ToString()
        //                        + "' and GroupID = '" + rExp["GroupID"].ToString() + "'").LastOrDefault(); //.[0];
        //                    DataRow SiID = ds.Tables["dtException"]
        //                           .Select("AssetID = '" + rExp["AssetID"].ToString()
        //                           + "' and RuleID= '" + rExp["ParentRuleID"].ToString()
        //                           + "' and GroupID = '" + rExp["GroupID"].ToString() + "'").FirstOrDefault();
        //                    N["ExceptionID"] = (int)iIDs["iID"];
        //                    N["Status"] = 0;
        //                    N["UID"] = r["UID"];
        //                    N["UIDCategory"] = r["UIDCategory"];
        //                    N["SendAt"] = null;
        //                    N["ReportID"] = r["ReportID"].ToString();
        //                    N["GPSDataUID"] = iIDs["GPSDataID"];

        //                    N["AssetID"] = iIDs["AssetID"].ToString();
        //                    N["DriverID"] = iIDs["DriverID"].ToString();
        //                    N["ParentRuleID"] = rExp["ParentRuleID"];

        //                    N["Email"] = u.Email.ToString();
        //                    N["SMSNumber"] = u.MobileNo.ToString();
        //                    N["Asset"] = iIDs["AssetNumber"].ToString();
        //                    N["Driver"] = iIDs["sDriverName"].ToString();
        //                    N["RuleName"] = iIDs["RuleName"].ToString();
        //                    N["Lat"] = String.IsNullOrEmpty(iIDs["Latitude"].ToString()) ? 0 : (double)iIDs["Latitude"];
        //                    N["Lon"] = String.IsNullOrEmpty(iIDs["Longitude"].ToString()) ? 0 : (double)iIDs["Longitude"];
        //                    N["StartLat"] = String.IsNullOrEmpty(SiID["Latitude"].ToString()) ? 0 : (double)SiID["Latitude"];
        //                    N["StartLon"] = String.IsNullOrEmpty(SiID["Longitude"].ToString()) ? 0 : (double)SiID["Longitude"];

        //                    N["Type"] = "Rule";
        //                    String tzName = iIDs["TimeZoneID"].ToString();
        //                    if (tzName == String.Empty)
        //                        tzName = TimeZone.CurrentTimeZone.StandardName;
        //                    TimeSpan ts = new Utils().GetTimeSpan(tzName);
        //                    N["DateTimeGPS_Local"] = String.IsNullOrEmpty(iIDs["DateTimeGPS_UTC"].ToString()) ? DateTime.UtcNow.ToString() : ((DateTime)iIDs["DateTimeGPS_UTC"] + ts).ToString();
        //                    N["DateTimeGPS_UTC"] = String.IsNullOrEmpty(iIDs["DateTimeGPS_UTC"].ToString()) ? DateTime.UtcNow.ToString() : ((DateTime)iIDs["DateTimeGPS_UTC"]).ToString();

        //                    dtNotification.Rows.Add(N);
        //                }
        //            }
        //            #endregion
        //        }

        //        if (dtNotification.Rows.Count == 0)
        //        {
        //            Boolean b = new NotificationService().UpdateExceptionsProcessed(sConnStr);
        //            return sResult + b.ToString() + " No exceptions notification detected";
        //        }

        //        List<int> lm = new List<int>();
        //        foreach (Matrix m in lMatrix)
        //            if (!lm.Contains(m.GMID))
        //                lm.Add(m.GMID);
        //        DataSet dsZones = new WebSpecifics.LibData().dsZones(lm, sConnStr, null, null);
        //        if (sendNotificationData(dtNotification, dsZones, sConnStr))
        //            return sResult + "Rules Exception is Successful";
        //        else
        //            return sResult + "Error in Rules Exception. Seems could not save in Notification Table ";
        //    }
        //    catch (Exception ex)
        //    {
        //        return sResult + "Error in Rules Exception: " + ex.ToString();
        //    }
        //    finally
        //    {
        //        esriMap = null;
        //    }
        //}
        public static DataTable NotificationDT()
        {
            DataTable dtNotification = new DataTable();
            dtNotification.Columns.Add("ARID");
            dtNotification.Columns.Add("ExceptionID");
            dtNotification.Columns.Add("Status");
            dtNotification.Columns.Add("UID");
            dtNotification.Columns.Add("UIDCategory");
            dtNotification.Columns.Add("SendAt");
            dtNotification.Columns.Add("ReportID");
            dtNotification.Columns.Add("GPSDataUID");

            dtNotification.Columns.Add("DateTimeGPS_Local");
            dtNotification.Columns.Add("DateTimeGPS_UTC");

            dtNotification.Columns.Add("Err");
            dtNotification.Columns.Add("sBody");
            dtNotification.Columns.Add("Type");
            dtNotification.Columns.Add("Title");

            dtNotification.Columns.Add("AssetID");
            dtNotification.Columns.Add("DriverID");
            dtNotification.Columns.Add("ParentRuleID");

            dtNotification.Columns.Add("Email");
            dtNotification.Columns.Add("Asset");
            dtNotification.Columns.Add("Driver");
            dtNotification.Columns.Add("RuleName");
            dtNotification.Columns.Add("Lat");
            dtNotification.Columns.Add("Lon");
            dtNotification.Columns.Add("StartLon");
            dtNotification.Columns.Add("StartLat");

            dtNotification.Columns.Add("VirtualEmail");
            dtNotification.Columns.Add("SMSNumber");
            dtNotification.Columns.Add("RuleCondition");

            return dtNotification;
        }

        //running once a day, in ReportSender
        public static String ProcessDriverLicenseExpiryNotif(String sConnStr)
        {
            String sql = @"
DECLARE @FistName nvarchar(50), @LastName nvarchar(50),  
       @License nvarchar(50), @expDate nvarchar(50), @message nvarchar(max),
	   @Email nvarchar(50), @userId int, @Nic  nvarchar(50), @source nvarchar(2) ;  

DECLARE driver_cursor CURSOR FOR   
 select [sDriverName],[sFirtName] ,[LicenseNo2] ,[Licenseno2Expiry]
       ,[Ref1] , 'N' as expire FROM [dbo].[GFI_FLT_Driver]
  where Status = 'Active' and   GETDATE()  >= DATEADD(day, -30,[Licenseno2Expiry] )
		and  GETDATE() <=[Licenseno2Expiry]
  union 
   select [sDriverName],[sFirtName] ,[LicenseNo2] ,[Licenseno2Expiry]
       ,[Ref1] ,'Y' as expire FROM [dbo].[GFI_FLT_Driver]
  where Status = 'Active' and  GETDATE() > [Licenseno2Expiry]

OPEN driver_cursor  
  
FETCH NEXT FROM driver_cursor   
INTO @FistName, @LastName , @License, @expDate, @Nic, @source
  
WHILE @@FETCH_STATUS = 0  
BEGIN  
 
	IF (@source='N')
		SELECT @message = 'Please note that the service license number '+ @License+ ' for driver '+ @FistName + ' ' + @LastName+' will expire on ' +  @expDate       
	ELSE
	     SELECT @message = 'Please note that the service license number '+ @License+ ' for driver '+ @FistName + ' ' + @LastName+' has expired since ' +  @expDate       
   
    DECLARE officer_cursor CURSOR FOR   
	select  b.Email , b.UID from Officers a, AspNetUsers b
	where a.OfficerLevel ='Requesting_Approving_Scheduling_Officer'
	and a.[User] = b.Id
  
    OPEN officer_cursor  
    FETCH NEXT FROM officer_cursor INTO @Email , @userId  
  
    WHILE @@FETCH_STATUS = 0  
    BEGIN  
  
      IF NOT EXISTS(SELECT 1 FROM GFI_SYS_Notification WHERE Type = 'LExpiry' 
	  and UID = @userId AND   CONVERT(nvarchar(30), InsertedAt, 103)= CONVERT(nvarchar(30), GETDATE(), 103) 
	  and sBody= @message
	   )
        INSERT INTO GFI_SYS_Notification  (UIDCategory, Status, Type, SendAt,Title, sBody,UID )
		values  ('ID','100' ,'LExpiry', NULL,'Driver License Expiry', @message, @userId  )

        FETCH NEXT FROM officer_cursor INTO @Email , @userId   
    END  
  
    CLOSE officer_cursor  
    DEALLOCATE officer_cursor  
        -- Get the next vendor.  
    FETCH NEXT FROM driver_cursor   
    INTO @FistName, @LastName , @License, @expDate, @Nic, @source
END   
CLOSE driver_cursor;  
DEALLOCATE driver_cursor;  ";

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dtCtrl);

            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return "ProcessDriverLicenseExpiryNotif " + bResult + "\r\n";
        }

        //running every 15 mins, in ReportSender
        public static String SendMailsFromNotifTable(String sConnStr)
        {
            String sql = @"
Select 'Notif' AS TableName
select * from GFI_SYS_Notification n
	inner join GFI_SYS_User u on n.UID = u.UID
where SendAt is null 
    and Status = 100 
    and InsertedAt >= DATEADD(day, -1, GETDATE())
";

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);
            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            DataTable dt = ds.Tables["Notif"];

            sql = String.Empty;
            MailSender ms = new MailSender();
            foreach (DataRow dr in dt.Rows)
            {
                String sEmail = dr["Type"].ToString().Equals("TRequest") ? dr["VirtualEmail"].ToString().Trim() : dr["email"].ToString().Trim();
                String s = ms.SendMail(sEmail, dr["Title"].ToString(), dr["sBody"].ToString(), String.Empty, sConnStr);
                if (s == "Mail Sent")
                {
                    String dtn = DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss tt");
                    dr["Status"] = 200;
                    dr["SendAt"] = dtn;
                    dr["Err"] = s;

                    sql += "update GFI_SYS_Notification set Status = 200, SendAt = '" + dtn.ToString().Trim() + "', Err = '" + s + "' where NID = " + dr["NID"] + " \r\n";
                }
                else
                {
                    dr["Status"] = 100;
                    dr["SendAt"] = DBNull.Value;
                    dr["Err"] = s;

                    sql += "update GFI_SYS_Notification set Status = 100, SendAt = null, Err = '" + s + "' where NID = " + dr["NID"] + " \r\n";
                }
            }

            Boolean bResult = false;
            if (!String.IsNullOrEmpty(sql))
            {
                c = new Connection(sConnStr);
                DataTable dtCtrl = c.CTRLTBL();
                DataRow drCtrl = dtCtrl.NewRow();

                drCtrl["SeqNo"] = 1;
                drCtrl["TblName"] = "None";
                drCtrl["CmdType"] = "STOREDPROC";
                drCtrl["Command"] = sql;
                dtCtrl.Rows.Add(drCtrl);

                DataSet dsProcess = new DataSet();
                dsProcess.Merge(dtCtrl);
                DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

                dtCtrl = dsResult.Tables["CTRLTBL"];
                if (dtCtrl != null)
                {
                    DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                    if (dRow.Length > 0)
                        bResult = false;
                    else
                        bResult = true;
                }
                else
                    bResult = false;
            }
            else
                return "SendMailsFromNotifTable: No data \r\n";

            return "SendMailsFromNotifTable " + bResult + "\r\n";
        }

        public Boolean PrepareDataNotif(int UID, DateTime dtSent, string Err, string sBody, string Type, string Title, string sConnStr)
        {

            DataTable dtNotification = NotificationService.NotificationDT();

            DataRow dr = dtNotification.NewRow();
            dr["UID"] = UID;
            dr["SendAt"] = dtSent.ToString("MM/dd/yyyy HH:mm:ss");
            dr["Err"] = Err.ToString();
            dr["sBody"] = sBody.ToString();
            dr["Type"] = Type.ToString();
            dr["Title"] = Title.ToString();
            dr["UIDCategory"] = string.Empty;
            dtNotification.Rows.Add(dr);


            Boolean bSave = new NotificationService().SaveNotification(dtNotification, sConnStr);

            return bSave;

        }
    }
}