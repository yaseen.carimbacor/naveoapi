using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Rules;
using NaveoOneLib.Services.Zones;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.WebSpecifics;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Services.Users;
//using Twilio.Rest.Wireless.V1.Sim;
using Org.BouncyCastle.Math;
using System.Runtime.InteropServices.ComTypes;
using Telerik.WinControls.UI;

namespace NaveoOneLib.Services.Rules
{
    public class ExceptionsService
    {

        Errors er = new Errors();
        public string ExeSrc = "";

        public DataTable GetExceptions(String sConnStr)
        {
            String sqlString = @"SELECT iID, 
                            RuleID, 
                            GPSDataID, 
                            AssetID, 
                            DriverID, 
                            Severity from GFI_GPS_Exceptions order by iID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public RuleException GetExceptionsById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT iID, 
                                        RuleID, 
                                        GPSDataID, 
                                        AssetID, 
                                        DriverID, 
                                        Severity from GFI_GPS_Exceptions
                                        WHERE iID = ?
                                        order by iID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                RuleException retExceptions = new RuleException();
                retExceptions.iID = (int)dt.Rows[0]["iID"];
                retExceptions.RuleID = (int)dt.Rows[0]["RuleID"];
                retExceptions.GPSDataID = (Int64)dt.Rows[0]["GPSDataID"];
                retExceptions.AssetID = (int)dt.Rows[0]["AssetID"];
                retExceptions.DriverID = (int)dt.Rows[0]["DriverID"];
                retExceptions.Severity = dt.Rows[0]["Severity"].ToString();
                return retExceptions;
            }
        }

        public RuleException GetExceptionsByGPSDataID(String iID, String sConnStr)
        {
            String sqlString = @"SELECT iID, 
                                        RuleID, 
                                        GPSDataID, 
                                        AssetID, 
                                        DriverID, 
                                        Severity from GFI_GPS_Exceptions
                                        WHERE GPSDataID = ?
                                        order by iID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                RuleException retExceptions = new RuleException();
                retExceptions.iID = (int)dt.Rows[0]["iID"];
                retExceptions.RuleID = (int)dt.Rows[0]["RuleID"];
                retExceptions.GPSDataID = (Int64)dt.Rows[0]["GPSDataID"];
                retExceptions.AssetID = (int)dt.Rows[0]["AssetID"];
                retExceptions.DriverID = (int)dt.Rows[0]["DriverID"];
                retExceptions.Severity = dt.Rows[0]["Severity"].ToString();
                return retExceptions;
            }
        }

        public String GetExceptionsCommentByGPSDataID(String iID, String sConnStr)
        {
            String sqlString = @"SELECT e.[iID]
                                    , e.[RuleID]
                                    , e.[GPSDataID]
                                    , e.[AssetID]
                                    , e.[DriverID]
                                    , e.[Severity]
                                    , e.[GroupID]
                                    , e.[Posted]
                                    , e.[InsertedAt]
                                    , e.[DateTimeGPS_UTC]
	                                , r.RuleName
                                FROM GFI_GPS_Exceptions e
	                                inner join GFI_GPS_Rules r on e.RuleID = r.RuleID
                                WHERE GPSDataID = ?
                                order by iID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return String.Empty;
            else
            {

                return "Alert :" + dt.Rows[0]["rulename"].ToString(); ;
            }
        }

        //public List<TripDetail> GetTripExceptions(List<TripDetail> lstTripDetails)
        //{
        //    String strTripDetailsIds = "(";

        //    foreach (TripDetail tds in lstTripDetails)
        //    {
        //        strTripDetailsIds = tds.GPSDataUID.ToString() + ",";
        //    }

        //    strTripDetailsIds = strTripDetailsIds.Substring(0, strTripDetailsIds.Length - 1) + ")";

        //    String sqlString = @"SELECT iID, 
        //                                RuleID, 
        //                                GPSDataID, 
        //                                AssetID, 
        //                                DriverID, 
        //                                Severity from GFI_GPS_Exceptions
        //                                WHERE GPSDataID = ?
        //                                order by iID";
        //    DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID);
        //    if (dt.Rows.Count == 0)
        //        return null;
        //    else
        //    {
        //        Exceptions retExceptions = new Exceptions();
        //        retExceptions.iID = (int)dt.Rows[0]["iID"];
        //        retExceptions.RuleID = (int)dt.Rows[0]["RuleID"];
        //        retExceptions.GPSDataID = (Int64)dt.Rows[0]["GPSDataID"];
        //        retExceptions.AssetID = (int)dt.Rows[0]["AssetID"];
        //        retExceptions.DriverID = (int)dt.Rows[0]["DriverID"];
        //        retExceptions.Severity = dt.Rows[0]["Severity"].ToString();
        //        return retExceptions;
        //    }
        //}

        public DataTable GetDashboardExceptions(List<int> lAssetID, List<int> lRules, DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            String strAssetID = String.Empty;
            foreach (int i in lAssetID)
                strAssetID += i.ToString() + ",";
            if (strAssetID.Length > 1)
                strAssetID = strAssetID.Substring(0, strAssetID.Length - 1);

            String s = String.Empty;
            foreach (int i in lRules)
                s += i.ToString() + ", ";
            if (s.Length > 2)
                s = s.Substring(0, s.Length - 2);

            String sql = @"
	select top 5 x.RuleID, count(x.RuleID) qty
		, r.RuleName
	from
		(select RuleID, count(RuleID) qty, GroupID from GFI_GPS_Exceptions where InsertedAt >= ? and InsertedAt <= ? and AssetID in (" + strAssetID + @") group by  RuleID, GroupID) x
	inner join GFI_GPS_Rules r on x.RuleID = r.RuleID
";
            if (lRules.Count > 0)
                sql += "where r.RuleID in (" + s + ") ";
            sql += " group by x.RuleID, r.RuleName order by qty desc";

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "�" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");
            drSql["sParam"]
                = dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff")
                + "�" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");
            drSql["sTableName"] = "ExceptionsT";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            DataTable dt = ds.Tables["ExceptionsT"];

            return dt;
        }

        public bool SaveExceptions(RuleException uExceptions, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_GPS_Exceptions";
            dt.Columns.Add("RuleID", uExceptions.RuleID.GetType());
            dt.Columns.Add("GPSDataID", uExceptions.GPSDataID.GetType());
            dt.Columns.Add("AssetID", uExceptions.AssetID.GetType());
            dt.Columns.Add("DriverID", uExceptions.DriverID.GetType());
            dt.Columns.Add("Severity", uExceptions.Severity.GetType());
            DataRow dr = dt.NewRow();
            // dr["iID"] = uExceptions.iID;
            dr["RuleID"] = uExceptions.RuleID;
            dr["GPSDataID"] = uExceptions.GPSDataID;
            dr["AssetID"] = uExceptions.AssetID;
            dr["DriverID"] = uExceptions.DriverID;
            dr["Severity"] = uExceptions.Severity;
            dt.Rows.Add(dr);


            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenInsert(dt, transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool UpdateExceptions(RuleException uExceptions, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_GPS_Exceptions";
            dt.Columns.Add("iID", uExceptions.iID.GetType());
            dt.Columns.Add("RuleID", uExceptions.RuleID.GetType());
            dt.Columns.Add("GPSDataID", uExceptions.GPSDataID.GetType());
            dt.Columns.Add("AssetID", uExceptions.AssetID.GetType());
            dt.Columns.Add("DriverID", uExceptions.DriverID.GetType());
            dt.Columns.Add("Severity", uExceptions.Severity.GetType());
            DataRow dr = dt.NewRow();
            dr["iID"] = uExceptions.iID;
            dr["RuleID"] = uExceptions.RuleID;
            dr["GPSDataID"] = uExceptions.GPSDataID;
            dr["AssetID"] = uExceptions.AssetID;
            dr["DriverID"] = uExceptions.DriverID;
            dr["Severity"] = uExceptions.Severity;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "iID = '" + uExceptions.iID + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool DeleteExceptions(RuleException uExceptions, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_GPS_Exceptions", "iID = '" + uExceptions.iID + "'", sConnStr))
                return true;
            else
                return false;
        }
        public bool DeleteExceptionsByGPSDataID(RuleException uExceptions, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_GPS_Exceptions", "GPSDataID = '" + uExceptions.GPSDataID + "'", sConnStr))
                return true;
            else
                return false;
        }

        public static String sSQLExceptions(Guid UserToken, List<int> lAssetID, List<int> lRules, DateTime dtFrom, DateTime dtTo, Boolean bDriver, Boolean bCreateZoneTable, Boolean bChkArc, String sConnStr)
        {
            int UID = -1;
            NaveoModels nm = NaveoModels.Assets;
            if (bDriver)
                nm = NaveoModels.Drivers;

            List<int> lValidatedAssets = new List<int>();
            if (lAssetID == null)
                lValidatedAssets = LibData.lValidateData(UserToken, nm, sConnStr, out UID);
            else
            {
                lValidatedAssets = lAssetID;
                UID = new UserService().GetUserID(UserToken, sConnStr);
            }
            String sql = "\r\n--GetExcetpionssql \r\n";
            sql += "SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED --drop table script ";
            sql += "\r\n";

            sql += new LibData().sqlValidatedData(UserToken, NaveoModels.Assets, sConnStr);

            sql += @"
CREATE TABLE #Assets
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY CLUSTERED,
	[iAssetID] [int]
);";
            foreach (int i in lValidatedAssets)
                sql += "insert #Assets (iAssetID) values (" + i.ToString() + ");\r\n";

            sql += @"
--SelectIntoStarts
;with cte as
(
    select distinct RuleID, AssetID, GroupID, dateFrom, DateTo, d.ParentGMID
    	, CONVERT(varchar, DATEADD(s, DATEDIFF(SECOND, dateFrom, dateTo), 0), 108) AS duration
    from GFI_GPS_ExceptionsHeader h
        inner join #Assets a on h.AssetID = a.iAssetID
        inner join #ValidatedData d on d.StrucID = a.iAssetID
";
            String sWhereCmd = " where h.dateFrom >= ? and h.dateFrom <= ? ";
            if (lRules != null && lRules.Count > 0)
            {
                String sRules = String.Empty;
                foreach (int i in lRules)
                    sRules += i.ToString() + ", ";
                sRules = sRules.Substring(0, sRules.Length - 2);
                sWhereCmd += " and h.RuleID in (" + sRules + ") ";
            }

            if (bDriver)
                sql = sql.Replace("h.AssetID", "h.DriverID");

            sql += sWhereCmd;
            sWhereCmd = sWhereCmd.Replace("h.dateFrom", "h.DateTimeGPS_UTC");
            sql += @"
)
select a.assetID, a.assetName
	, r.ruleID, r.ruleName
	 , cte.dateFrom, cte.dateTo, cte.groupID, cte.duration, cte.ParentGMID
	, DATEADD(MINUTE, t.TotalMinutes, cte.dateFrom) dtLocalFrom
	, DATEADD(MINUTE, t.TotalMinutes, cte.dateTo) dtLocalTo
    , DATEADD(MINUTE, t.TotalMinutes, cte.dateTo) dateTimeToLocal
    , convert(nvarchar(max), '')  sZones
    , 1 driverID
    , convert(nvarchar(250), '')  sDriverName
    , 0 maxSpeed
    , null roadSpeed
	, CAST(0.0 AS float) lon, CAST(0.0 AS float) lat
    , convert(nvarchar(2500), '') sSite
into #Exceptions from cte
	inner join GFI_GPS_Rules r on cte.RuleID = r.RuleID
	inner join  GFI_FLT_Asset a on cte.AssetID = a.AssetID
    inner join GFI_SYS_TimeZones t on a.TimeZoneID = t.StandardName
where 
	r.RuleID = r.ParentRuleID;
--SelectIntoEnds

select distinct *, CAST(0.0 AS float) lon, CAST(0.0 AS float) lat, 0 Speed, null roadSpeed
 into #ExceptionsDetails from GFI_GPS_Exceptions h
        inner join #Assets a on h.AssetID = a.iAssetID
        inner join #ValidatedData d on d.StrucID = a.iAssetID
	" + sWhereCmd + @"

 update #ExceptionsDetails
	set Speed = g.Speed, lon = g.Longitude, lat = g.Latitude, roadSpeed = g.RoadSpeed, DriverID = g.DriverID
from #ExceptionsDetails e
	inner join GFI_GPS_GPSData g on e.GPSDataID = g.UID

update #Exceptions
	set DriverID = d.DriverID, sDriverName = d.sDriverName, roadSpeed = t2.RoadSpeed, Lon = t2.lon, lat = t2.lat
from #Exceptions t1
	inner join #ExceptionsDetails t2 on t1.dateFrom = t2.DateTimeGPS_UTC and t1.AssetID = t2.AssetID and t1.GroupID = t2.GroupID and t1.RuleID = t2.RuleID
    inner join GFI_FLT_Driver d on d.DriverID = t2.DriverID

;with cte as
(
	select t1.GroupID, max(t2.speed) maxSpeed, t1.AssetID, T1.RuleID
	from #Exceptions t1
		inner join #ExceptionsDetails t2 on t1.GroupID = t2.GroupID and t1.AssetID = t2.AssetID and t1.RuleID = t2.RuleID and t2.DateTimeGPS_UTC >= t1.dateFrom and t2.DateTimeGPS_UTC <= t1.dateTo
	group by t1.GroupID, t1.AssetID, t1.RuleID
)
update #Exceptions
	set maxSpeed = cte.maxSpeed
from #Exceptions t1
	inner join cte on t1.GroupID = cte.GroupID and t1.AssetID = cte.AssetID and t1.RuleID = cte.RuleID

update #Exceptions
	set sSite = GMID.GMDescription
from #Exceptions t1
	inner join #ValidatedData vd on t1.AssetID = vd.StrucID 
	inner join #ValidatedData GMID on vd.ParentGMID = GMID.GMID
";

            #region Zones
            String s = "\r\n";
            if (bCreateZoneTable)
            {
                List<Matrix> lMatrix = new LibData().lMatrix(UID, sConnStr);
                s += ZoneService.sGetZoneIDs(lMatrix, sConnStr);
                s += @"
                        --SelectIntoStarts ZoneExceptionsTmp
                        ;with cte as
                        (
	                        select e.*, z.*
							from 
	                            (
		                            select *
			                            , Geometry::STPointFromText('POINT(' + CONVERT(varchar, lon, 128) + ' ' + CONVERT(varchar, lat, 128) + ')', 4326) pointFr
		                            from #ExceptionsDetails
	                            ) e
								inner join 
									(
										select zh.* from GFI_FLT_ZoneHeader zh WITH (INDEX(SIndx_GFI_FLT_ZoneHeader))
											--inner join @tTmpDistinctZone t on zh.ZoneID = t.i
									) z on z.GeomData.STIntersects(pointFr) = 1
                        )
                        select * into #ZoneExceptionsTmp from cte

                        select t1.* into #ZoneExceptions 
                            from #ZoneExceptionsTmp t1
                                inner join @tTmpDistinctZone t2 on t1.ZoneID = t2.i

                        --SelectIntoEnds

                        update #Exceptions --OracleSpecialExceptions
                            set sZones = STUFF(
		                                        (SELECT distinct ', ' + e.Description FROM #ZoneExceptions e Where e.RuleID = h.RuleID and e.GroupID = h.GroupID and e.AssetID = h.AssetID and e.DateTimeGPS_UTC >= h.dateFrom and e.DateTimeGPS_UTC <= h.dateTo FOR XML PATH(''))
	                                        , 1, 1, '')
                        from #Exceptions h
                        ";
            }

            sql += s;
            #endregion

            sql += @"
SELECT 'dtExceptionHeader' AS TableName 
select *, CONVERT(VARCHAR(10),[dtLocalFrom],103) as DateOnly,CONVERT(VARCHAR(10),[dtLocalFrom],108) as TimeOnly, CONCAT(AssetID, RuleID, GroupID) UID from #Exceptions order by assetName, dateFrom, ruleName

SELECT 'dtExceptionDetails' AS TableName
select distinct e.GPSDataID uid, e.Lon fLongitude, e.Lat fLatitude, e.dateTimeGPS_UTC
	, DATEADD(MINUTE, t.TotalMinutes, e.DateTimeGPS_UTC) dateTimeToLocal, e.speed, e.RoadSpeed roadSpeed
	, r.RuleName ruleName, r.ParentRuleID parentRuleID
	, e.AssetID assetID, e.severity severity,e.GroupID groupID, e.RuleID ruleID
	, a.AssetName assetName 
from #Exceptions x
	inner join #ExceptionsDetails e on x.GroupID = e.GroupID and x.AssetID = e.AssetID and x.RuleID = e.RuleID --and x.gpsDataID = e.GPSDataID
    inner join GFI_GPS_Rules r on e.RuleID = r.RuleID
    inner join GFI_FLT_Asset a on e.AssetID = a.AssetID
    inner join GFI_SYS_TimeZones t on a.TimeZoneID = t.StandardName
order by e.AssetID, e.RuleID, e.GroupID, e.DateTimeGPS_UTC
";
            if (bChkArc)
            {
                String sqlArc = sql.Replace("GFI_GPS_Exceptions", "GFI_ARC_Exceptions");
                sqlArc = sql.Replace("GFI_GPS_GPSData", "GFI_ARC_GPSData");
                sql = sqlArc;
            }



            return sql;
        }
        public DataSet GetExceptions(Guid UserToken, List<int> lAssetID, List<int> lRules, DateTime dtFrom, DateTime dtTo, Boolean bDriver, Boolean bCreateZoneTable, String sConnStr)
        {
            Boolean bChkArc = Globals.bNeedToQryArc(sConnStr, dtFrom);
            String sql = sSQLExceptions(UserToken, lAssetID, lRules, dtFrom, dtTo, bDriver, bCreateZoneTable, bChkArc, sConnStr);
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "�" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "�" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "�" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");

            if (bChkArc)
            {
                drSql["sParam"]
                    = dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff")
                    + "�" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff")
                    + "�" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff")
                    + "�" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");
            }
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds;
        }

        public static String sSQLExceptions(List<int> lAssetID, List<int> lRules, DateTime dtFrom, DateTime dtTo, String sWorkHrs, Boolean bDriver, Boolean bTotalsOnly, List<Matrix> lMatrix, Boolean bChkArc)
        {
            String strAssetID = String.Empty;
            foreach (int i in lAssetID)
                strAssetID += i.ToString() + ",";
            strAssetID = strAssetID.Substring(0, strAssetID.Length - 1);

            String sql = @"
--SelectIntoStarts. Exceptions Codes Starting
;with cte as
(
	select assetID, ruleID, groupID, min(DateTimeGPS_UTC) dtFrom, max(DateTimeGPS_UTC) dtTo
        , CONVERT(varchar, DATEADD(s, DATEDIFF(SECOND, min(DateTimeGPS_UTC), max(DateTimeGPS_UTC)), 0), 108) AS duration
	from GFI_GPS_Exceptions e
	    where AssetID in (" + strAssetID + @")
		    and e.DateTimeGPS_UTC >= ? and e.DateTimeGPS_UTC <= ?";
            if (bDriver)
                sql = sql.Replace("where AssetID in (", "where DriverID in (");

            String sRules = String.Empty;
            if (lRules != null && lRules.Count > 0)
            {
                foreach (int i in lRules)
                    sRules += i.ToString() + ", ";
                sRules = sRules.Substring(0, sRules.Length - 2);
                sRules = " and e.RuleID in (" + sRules + ") ";
            }
            if (!String.IsNullOrEmpty(sRules))
                sql += sRules;

            sql += @"
	group by AssetID, RuleID, GroupID ";
            if (bChkArc)
            {
                sql += @"
          --ArchiveData
          UNION select assetID, ruleID, groupID, min(DateTimeGPS_UTC) dtFrom, max(DateTimeGPS_UTC) dtTo
        , CONVERT(varchar, DATEADD(s, DATEDIFF(SECOND, min(DateTimeGPS_UTC), max(DateTimeGPS_UTC)), 0), 108) AS duration
    from GFI_ARC_Exceptions e
        where AssetID in (" + strAssetID + @")
            and e.DateTimeGPS_UTC >= ? and e.DateTimeGPS_UTC <= ? ";
                if (bDriver)
                    sql = sql.Replace("where AssetID in (", "where DriverID in (");

                String sRulesARC = String.Empty;
                if (lRules != null && lRules.Count > 0)
                {
                    foreach (int i in lRules)
                        sRules += i.ToString() + ", ";
                    sRules = sRules.Substring(0, sRules.Length - 2);
                    sRules = " and e.RuleID in (" + sRules + ") ";
                }
                if (!String.IsNullOrEmpty(sRules))
                    sql += sRules;

                sql += @"
	group by AssetID, RuleID, GroupID ";
            }

            sql += @"
)
select a.assetID, a.assetName
	, r.ruleID, r.ruleName
	, cte.dtFrom, cte.dtTo, cte.groupID, cte.duration
	, DATEADD(MINUTE, t.TotalMinutes, cte.dtFrom) dateTimeToLocal
	, -1 gpsDataID, CAST(0.0 AS float) lon, CAST(0.0 AS float) lat
    , convert(nvarchar(2500), '')  sZones
    , 1 driverID
    , convert(nvarchar(250), '')  sDriverName
    , 0 maxSpeed
    , null roadSpeed
into #Exceptions from cte
	inner join GFI_GPS_Rules r on cte.RuleID = r.RuleID
	inner join  GFI_FLT_Asset a on cte.AssetID = a.AssetID
    inner join GFI_SYS_TimeZones t on a.TimeZoneID = t.StandardName
where 
	r.RuleID = r.ParentRuleID

--SelectIntoEnds
                    ";

            if (!bTotalsOnly)
                sql += @"
update #Exceptions
	set GPSDataID = t2.GPSDataID, Lon = g.Longitude, Lat = g.Latitude, DriverID = d.DriverID, sDriverName = d.sDriverName, roadSpeed = g.RoadSpeed
from #Exceptions t1
	inner join GFI_GPS_Exceptions t2 on t1.dtFrom = t2.DateTimeGPS_UTC and t1.AssetID = t2.AssetID and t1.GroupID = t2.GroupID
	inner join GFI_GPS_GPSData g on t2.GPSDataID = g.UID
    inner join GFI_FLT_Driver d on d.DriverID = g.DriverID

;with cte as
(
	select t1.GroupID, max(g.speed) maxSpeed
	from #Exceptions t1
		inner join GFI_GPS_Exceptions t2 on t1.GroupID = t2.GroupID and t1.AssetID = t2.AssetID
		inner join GFI_GPS_GPSData g on t2.GPSDataID = g.UID
	group by t1.GroupID
)
update #Exceptions
	set maxSpeed = cte.maxSpeed
from #Exceptions t1
	inner join cte on t1.GroupID = cte.GroupID
";

            #region Zones
            if (lMatrix != null)
            {
                String s = "\r\n";
                s += @"
                        --SelectIntoStarts ZoneExceptionsTmp

                        ;with cte as
                        (
	                        select e.*, z.*
							from 
	                            (
		                            select *
			                            , Geometry::STPointFromText('POINT(' + CONVERT(varchar, lon) + ' ' + CONVERT(varchar, lat) + ')', 4326) pointFr
		                            from #Exceptions
	                            ) e
								inner join 
									(
										select zh.* from GFI_FLT_ZoneHeader zh --WITH (INDEX(SIndx_GFI_FLT_ZoneHeader))
											--inner join @tTmpDistinctZone t on zh.ZoneID = t.i
									) z on z.GeomData.STIntersects(pointFr) = 1
                        )
                        select * into #ZoneExceptionsTmp from cte

                        select t1.* into #ZoneExceptions 
                            from #ZoneExceptionsTmp t1
                                inner join @tTmpDistinctZone t2 on t1.ZoneID = t2.i

                        --SelectIntoEnds
                        ";

                s += @"
                        update #Exceptions --OracleSpecialExceptions
                            set sZones = STUFF(
		                                        (SELECT distinct ', ' + e.Description FROM #ZoneExceptions e Where e.GPSDataID = h.GPSDataID FOR XML PATH(''))
	                                        , 1, 1, '')
                                --, iArrivalZones = STUFF(
		                        --,                 (SELECT ', ' + CONVERT(varchar(10), e.ZoneID) FROM #ArrivalZoneTrips e Where e.iID = h.iID FOR XML PATH(''))
	                            --,             , 1, 1, '')
                                --, sArrivalZonesColor = STUFF(
		                        --,                 (SELECT ', ' + CONVERT(varchar(10), e.Color) FROM #ArrivalZoneTrips e Where e.iID = h.iID FOR XML PATH(''))
	                            --,             , 1, 1, '')
                                --, iArrivalZone = (SELECT top 1 e.ZoneID FROM #ArrivalZoneTrips e Where e.iID = h.iID )
                                --, sArrivalZone = (SELECT top 1 e.Description FROM #ArrivalZoneTrips e Where e.iID = h.iID )
                                --, iArrivalZoneColor = (SELECT top 1 e.Color FROM #ArrivalZoneTrips e Where e.iID = h.iID )
                        from #Exceptions h
";
                sql += s;
            }
            #endregion

            sql += "\r\nSELECT 'dtExceptionHeader' AS TableName \r\n";
            if (bTotalsOnly)
                sql += "select r.ruleID, r.ruleName, count(1) cnt from #Exceptions r group by r.ruleID, r.ruleName --Oracle Add SemiColumn";
            else
                sql += "select * from #Exceptions order by AssetID, RuleName, dtFrom --Oracle Add SemiColumn";

            sql += @"

--SelectIntoStarts ExceptionDetails
select g.UID uid, g.Longitude fLongitude, g.Latitude fLatitude, g.dateTimeGPS_UTC
	, DATEADD(MINUTE, t.TotalMinutes, g.DateTimeGPS_UTC) dateTimeToLocal, g.speed, g.RoadSpeed roadSpeed
	, r.RuleName ruleName, r.ParentRuleID parentRuleID
	, e.AssetID assetID, e.severity severity,e.GroupID groupID, e.RuleID ruleID
	, a.AssetName assetName 
into #ExceptionDetails from GFI_GPS_Exceptions e 
    inner join GFI_GPS_Rules r on e.RuleID = r.ParentRuleID
    inner join GFI_FLT_Asset a on e.AssetID = a.AssetID
    inner join GFI_SYS_TimeZones t on a.TimeZoneID = t.StandardName
    inner join GFI_GPS_GPSData g on e.GPSDataID = g.UID
	inner join #Exceptions t1 on t1.AssetID = e.AssetID and t1.GroupID = e.GroupID and t1.RuleID = e.RuleID and e.DateTimeGPS_UTC >= t1.dtFrom and e.DateTimeGPS_UTC <= t1.dtTo and g.AssetID = t1.AssetID
	    where r.RuleID = r.ParentRuleID
order by e.AssetID, e.RuleID, e.GroupID, g.DateTimeGPS_UTC";
            if (bChkArc)
            {
                sql += @" 

INSERT INTO #ExceptionDetails
-- into _PostGre.ExceptionDetails
select g.UID uid, g.Longitude fLongitude, g.Latitude fLatitude, g.dateTimeGPS_UTC
	, DATEADD(MINUTE, t.TotalMinutes, g.DateTimeGPS_UTC) dateTimeToLocal, g.speed, g.RoadSpeed roadSpeed
	, r.RuleName ruleName, r.ParentRuleID parentRuleID
	, e.AssetID assetID, e.severity severity,e.GroupID groupID, e.RuleID ruleID
	, a.AssetName assetName 
from GFI_ARC_Exceptions e 
    inner join GFI_GPS_Rules r on e.RuleID = r.ParentRuleID
    inner join GFI_FLT_Asset a on e.AssetID = a.AssetID
    inner join GFI_SYS_TimeZones t on a.TimeZoneID = t.StandardName
    inner join GFI_ARC_GPSData g on e.GPSDataID = g.UID
	inner join #Exceptions t1 on t1.AssetID = e.AssetID and t1.GroupID = e.GroupID and t1.RuleID = e.RuleID and e.DateTimeGPS_UTC >= t1.dtFrom and e.DateTimeGPS_UTC <= t1.dtTo and g.AssetID = t1.AssetID
	    where r.RuleID = r.ParentRuleID
order by e.AssetID, e.RuleID, e.GroupID, g.DateTimeGPS_UTC";
            }

            sql += @"

--SelectIntoEnds

SELECT 'dtExceptionDetails' AS TableName
select * from #ExceptionDetails --Oracle Add SemiColumn
--Exceptions Codes ending
";
            if (bChkArc)
            {
                String sqlArc = sql.Replace("GFI_GPS_Exceptions", "GFI_ARC_Exceptions");
                sqlArc = sql.Replace("GFI_GPS_GPSData", "GFI_ARC_GPSData");
                sql = sqlArc;
            }

            return sql;
        }
        public DataSet GetExceptions(List<int> lAssetID, List<int> lRules, DateTime dtFrom, DateTime dtTo, String sWorkHrs, Boolean bDriver, Boolean bTotalsOnly, List<Matrix> lMatrix, String sConnStr)
        {
            Boolean bChkArc = Globals.bNeedToQryArc(sConnStr, dtFrom);
            String sql = sSQLExceptions(lAssetID, lRules, dtFrom, dtTo, sWorkHrs, bDriver, bTotalsOnly, lMatrix, bChkArc);
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "�" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");

            if (bChkArc)
            {
                drSql["sParam"]
                    = dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff")
                    + "�" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff")
                    + "�" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff")
                    + "�" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");
            }
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds;
        }

        public DataSet GetExceptionsOld(List<int> lAssetID, List<int> lRules, DateTime dtFrom, DateTime dtTo, String sWorkHrs, Boolean bDriver, Boolean bTotalsOnly, String sConnStr)
        {
            Boolean bChkArc = Globals.bNeedToQryArc(sConnStr, dtFrom);

            String strAssetID = String.Empty;
            foreach (int i in lAssetID)
                strAssetID += i.ToString() + ",";
            strAssetID = strAssetID.Substring(0, strAssetID.Length - 1);

            String sql = @"select g.UID uid, g.Longitude fLongitude, g.Latitude fLatitude, g.DateTimeGPS_UTC dateTimeGPS_UTC
                            , DATEADD(MINUTE, t.TotalMinutes, g.DateTimeGPS_UTC) dateTimeToLocal
                            , t.TotalMinutes TimeStampInMins
                            , g.Speed speed, g.DriverID driverID
	                        , r.RuleName ruleName, r.ParentRuleID parentRuleID
	                        , e.AssetID assetID, e.severity severity,e.GroupID groupID, e.RuleID ruleID
                            , CONVERT(DATETIME, CONVERT(VARCHAR(20), e.InsertedAT, 120)) as exception_date    --Web
	                        , CONVERT(DATETIME, CONVERT(VARCHAR(20), f.ExcpCreated, 120)) excpCreated
    						, a.AssetName assetName 
                    ";

            if (bTotalsOnly)
                sql = "select count(1)";

            sql += @"from GFI_GPS_Exceptions (nolock) e 
                        inner join GFI_GPS_Rules r on e.RuleID = r.ParentRuleID
                        inner join GFI_FLT_Asset a on e.AssetID = a.AssetID
                        inner join GFI_SYS_TimeZones t on a.TimeZoneID = t.StandardName
                        left outer join GFI_GPS_GPSData (nolock) g on e.GPSDataID = g.UID
                    	left outer join (select min(InsertedAT) ExcpCreated, RuleID, AssetID, GroupID from GFI_GPS_Exceptions (nolock) where AssetID in (" + strAssetID + @") group by AssetID, RuleID, GroupID) f on e.AssetID = f.AssetID and e.RuleID = f.RuleID and e.GroupID = f.GroupID
	                        where 
	                            e.AssetID in (" + strAssetID + @")
		                        and r.RuleID = r.ParentRuleID
		                        and e.DateTimeGPS_UTC >= ? and e.DateTimeGPS_UTC <= ?";

            sWorkHrs = sWorkHrs.Replace("h.dtStart", "e.DateTimeGPS_UTC");
            sql += sWorkHrs;

            if (lRules.Count > 0)
            {
                String s = String.Empty;
                foreach (int i in lRules)
                    s += i.ToString() + ", ";
                s = s.Substring(0, s.Length - 2);
                s = " and r.ParentRuleID in (" + s + ") ";
                sql += "\r\n";
                sql += s;
                sql += "\r\n";
            }
            if (bChkArc)
            {
                String sqlArc = sql.Replace("GFI_GPS_Exceptions e", "GFI_ARC_Exceptions e");
                sqlArc = sql.Replace("GFI_GPS_GPSData g", "GFI_ARC_GPSData g");
                sql = sql + "\r\n union \r\n" + sqlArc;
            }

            if (!bTotalsOnly)
                sql += " order by e.AssetID, e.RuleID, e.GroupID, g.DateTimeGPS_UTC";

            if (bDriver)
                sql = sql.Replace("e.AssetID", "e.DriverID");

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "�" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");
            if (bChkArc)
            {
                drSql["sParam"]
                    = dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff")
                    + "�" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff")
                    + "�" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff")
                    + "�" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");
            }
            drSql["sTableName"] = "ExceptionsT";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds;
        }



        public DataTable GetGroupIDExceptions(String sConnStr)
        {
            String sqlString = @"select GroupID from GFI_GPS_Exceptions";

            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        public Boolean SavePlanningExceptions(List<PlanningExceptions> lExceptions, Boolean bInsert, String sConnStr)
        {

            

            Boolean bResult = false;
            List<int> lcount = new List<int>();
            foreach (var uExceptions in lExceptions)
            {


                if (bInsert)
                    uExceptions.oprtype = DataRowState.Added;

                int randomnumber = 0;
                int rand = 0;
                var random = new Random();
                randomnumber = random.Next();




                if (!lcount.Contains(randomnumber))
                {
                    rand = randomnumber;
                    lcount.Add(rand);
                }
                else
                {
                    rand = random.Next();
                    lcount.Add(rand);
                }



                DataTable dtExceptions = new DataTable();
                dtExceptions.TableName = "GFI_GPS_Exceptions";
                dtExceptions.Columns.Add("iID", typeof(int));
                dtExceptions.Columns.Add("RuleID", typeof(int));
                dtExceptions.Columns.Add("GPSDataID", typeof(BigInteger));
                dtExceptions.Columns.Add("AssetID", typeof(int));
                dtExceptions.Columns.Add("DriverID", typeof(int));
                dtExceptions.Columns.Add("Severity", typeof(String));
                dtExceptions.Columns.Add("GroupID", typeof(int));
                dtExceptions.Columns.Add("Posted", typeof(int));
                dtExceptions.Columns.Add("InsertedAt", typeof(DateTime));
                dtExceptions.Columns.Add("DateTimeGPS_UTC", typeof(DateTime));
                dtExceptions.Columns.Add("OPRSeqNo", typeof(int));
                dtExceptions.Columns.Add("OprType", typeof(DataRowState));


                DataTable dtExceptionsHeader = new DataTable();
                dtExceptionsHeader.TableName = "GFI_GPS_ExceptionsHeader";
                dtExceptionsHeader.Columns.Add("iID", typeof(int));
                dtExceptionsHeader.Columns.Add("RuleID", typeof(int));
                dtExceptionsHeader.Columns.Add("AssetID", typeof(int));
                dtExceptionsHeader.Columns.Add("GroupID", typeof(int));
                dtExceptionsHeader.Columns.Add("dateFrom", typeof(DateTime));
                dtExceptionsHeader.Columns.Add("dateTo", typeof(DateTime));
                dtExceptionsHeader.Columns.Add("OPRSeqNo", typeof(int));
                dtExceptionsHeader.Columns.Add("OprType", typeof(DataRowState));


                DataTable dtExceptionAdditionalInfo = new DataTable();
                dtExceptionAdditionalInfo.TableName = "GFI_GPS_ExceptionAdditionalInfo";
                dtExceptionAdditionalInfo.Columns.Add("iID", typeof(int));
                dtExceptionAdditionalInfo.Columns.Add("ExcepID", typeof(int));
                dtExceptionAdditionalInfo.Columns.Add("ExceptionType", typeof(string));
                dtExceptionAdditionalInfo.Columns.Add("Date1", typeof(DateTime));
                dtExceptionAdditionalInfo.Columns.Add("Date2", typeof(DateTime));
                dtExceptionAdditionalInfo.Columns.Add("Field1", typeof(string));
                dtExceptionAdditionalInfo.Columns.Add("Field2", typeof(string));
                dtExceptionAdditionalInfo.Columns.Add("Field3", typeof(string));
                dtExceptionAdditionalInfo.Columns.Add("Field4", typeof(string));
                dtExceptionAdditionalInfo.Columns.Add("sAddress", typeof(string));
                dtExceptionAdditionalInfo.Columns.Add("OPRSeqNo", typeof(int));
                dtExceptionAdditionalInfo.Columns.Add("OprType", typeof(DataRowState));


                DataRow drr = dtExceptions.NewRow();
                drr["RuleID"] = uExceptions.RuleID;
                //drr["GPSDataID"] = uExceptions.GPSDataID;
                drr["AssetID"] = uExceptions.AssetID;
                drr["DriverID"] = uExceptions.DriverID;
                //drr["Severity"] = uExceptions.Severity;
                drr["GroupID"] = rand;
                drr["Posted"] = uExceptions.Posted;
                drr["InsertedAt"] = Convert.ToDateTime(uExceptions.InsertedAt);
                drr["DateTimeGPS_UTC"] = Convert.ToDateTime(uExceptions.AcutalTime);
                drr["OprType"] = uExceptions.oprtype;
                drr["OPRSeqNo"] = 1;
                dtExceptions.Rows.Add(drr);


                DataRow dr = dtExceptionsHeader.NewRow();

                dr["RuleID"] = uExceptions.RuleID;
                dr["AssetID"] = uExceptions.AssetID;
                dr["GroupID"] = rand;
                dr["dateFrom"] = Convert.ToDateTime(uExceptions.ExpectedTime);
                dr["dateTo"] = Convert.ToDateTime(uExceptions.ExpectedTime);
                dr["OprType"] = uExceptions.oprtype;
                dr["OPRSeqNo"] = 2;
                dtExceptionsHeader.Rows.Add(dr);



                DataRow drrr = dtExceptionAdditionalInfo.NewRow();
                drrr["ExcepID"] = 0;
                drrr["ExceptionType"] = uExceptions.ExceptionType;
                drrr["Date1"] = Convert.ToDateTime(uExceptions.ExpectedTime);
                drrr["Date2"] = Convert.ToDateTime(uExceptions.AcutalTime);
                drrr["Field1"] = uExceptions.viapoint;
                drrr["Field2"] = uExceptions.Buffer;
                drrr["Field3"] = uExceptions.Durations;
                drrr["Field4"] = uExceptions.AlertStatus;
                drrr["OprType"] = uExceptions.oprtype;
                drrr["OPRSeqNo"] = 3;
                dtExceptionAdditionalInfo.Rows.Add(drrr);





                Connection c = new Connection(sConnStr);
                DataTable dtCtrl = c.CTRLTBL();
                DataRow drCtrl = dtCtrl.NewRow();
                DataSet dsProcess = new DataSet();

                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 1;
                drCtrl["TblName"] = dtExceptions.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "iID";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdAction"] = "GET";
                dtCtrl.Rows.Add(drCtrl);

                dsProcess.Merge(dtExceptions);


                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 2;
                drCtrl["TblName"] = dtExceptionsHeader.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "iID";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdAction"] = "GET";
                dtCtrl.Rows.Add(drCtrl);

                dsProcess.Merge(dtExceptionsHeader);





                //Get iID
                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 3;
                drCtrl["TblName"] = dtExceptionAdditionalInfo.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "iID";
                drCtrl["KeyIsIdentity"] = "TRUE";
                if (bInsert)
                    drCtrl["NextIdAction"] = "GET";
                dtCtrl.Rows.Add(drCtrl);

                //Set ExcepID
                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 4;
                drCtrl["TblName"] = dtExceptionAdditionalInfo.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "iID";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdAction"] = "SET";
                drCtrl["NextIdFieldName"] = "ExcepID";
                drCtrl["ParentTblName"] = dtExceptions.TableName;
                dtCtrl.Rows.Add(drCtrl);

                dsProcess.Merge(dtExceptionAdditionalInfo);



                dsProcess.Merge(dtCtrl);
                dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

           
                if (dtCtrl != null)
                {
                    DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");
                    if (dRow.Length > 0)
                    {

                        foreach (DataRow x in dtCtrl.Rows)
                        {
                            string s = x["ExceptionInfo"].ToString();

                            if (x != null)
                            {
                                System.IO.File.AppendAllText(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "\\Registers\\ruless.dat", "\r\n rule Creation \r\n" + s.ToString() + "\r\n");
                            }

                        }
                        bResult = false;
                    }
                    else
                    {
                        bResult = true;

                    }
                }
                else
                    bResult = false;


                //return bResult;


            }


            return bResult;

        }

        public DataTable GetPlanningExceptionsReport(List<int> lAssetIDs , List<int> lRuleIDs,DateTime dtFrom ,DateTime dtTo,Boolean byDriver, String sConnStr)
        {


            String strAssetID = String.Empty;
            foreach (int i in lAssetIDs)
                strAssetID += i.ToString() + ",";
            if (strAssetID.Length > 1)
                strAssetID = strAssetID.Substring(0, strAssetID.Length - 1);



            String strRuleID = String.Empty;
            foreach (int i in lRuleIDs)
                strRuleID += i.ToString() + ",";
            if (strRuleID.Length > 1)
                strRuleID = strRuleID.Substring(0, strRuleID.Length - 1);


            string sSql = string.Empty;
            if(byDriver)
            {
                sSql = "exc.DriverID in (" + strAssetID + ")";

            } else
            {
                sSql = " exc.AssetID in (" + strAssetID + ")";
            }

            String sqlString = @"select excAdd.iID, rules.RuleName 
            ,asset.AssetNumber as Asset 
            ,driver.sDriverName as Driver 
            ,excAdd.Field1 as ViaPoint 
            ,excAdd.Date1 as ExpectedTime
            ,excAdd.Field2 as Buffer
            ,excAdd.Date2 as ActualTime
            ,excAdd.Field3 as Durations
            ,excAdd.Field4 as Status
from GFI_GPS_Rules rules 
left join GFI_GPS_Exceptions exc on rules.RuleID = exc.RuleID
left join GFI_GPS_ExceptionAdditionalInfo excAdd  on excAdd.ExcepID = exc.iID
left join GFI_FLT_Asset asset on asset.AssetID = exc.AssetID
left join GFI_FLT_Driver driver on driver.DriverID = exc.DriverID
where " + sSql + " and exc.RuleID in (" + strRuleID+") and excAdd.Date1 >= '"+ dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "'  and excAdd.Date1 <= '" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "' ";


            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }


        public DataTable GetFuelExceptionsReport(List<int> lAssetIDs, List<int> lRuleIDs, DateTime dtFrom, DateTime dtTo, Boolean byDriver, String sConnStr)
        {


            String strAssetID = String.Empty;
            foreach (int i in lAssetIDs)
                strAssetID += i.ToString() + ",";
            if (strAssetID.Length > 1)
                strAssetID = strAssetID.Substring(0, strAssetID.Length - 1);



            String strRuleID = String.Empty;
            foreach (int i in lRuleIDs)
                strRuleID += i.ToString() + ",";
            if (strRuleID.Length > 1)
                strRuleID = strRuleID.Substring(0, strRuleID.Length - 1);


            string sSql = string.Empty;
            if (byDriver)
            {
                sSql = "f.AssetID in (" + strAssetID + ")";

            }
            else
            {
                sSql = " f.AssetID in  (" + strAssetID + ")";
            }

            String sqlString = @";WITH CTE AS 
(SELECT rownum = ROW_NUMBER() OVER (ORDER BY DateTimeGPS_UTC)
        , DATEADD(MINUTE, t.TotalMinutes, f.DateTimeGPS_UTC) localTime, f.* ,a.AssetName --rules.RuleName
    FROM GFI_GPS_FuelData f
        inner join GFI_FLT_Asset a on f.AssetID = a.AssetID
        inner join GFI_SYS_TimeZones t on a.TimeZoneID = t.StandardName
		--inner join GFI_GPS_Rules rules  on rules.StrucValue = f.fType
    where " +sSql + @"
        and f.DateTimeGPS_UTC >= '" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + @"' 
        and f.DateTimeGPS_UTC <= '" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff") + @"' 
        --and rules.ParentRuleID in (" + strRuleID + @")
)
SELECT cte.UID gpsUID ,cte.AssetID assetID,cte.AssetName Asset ,cte.localTime DateTimeRule, cte.ConvertedValue convertedValue, u.Description uom, cte.fType ConditionType , cte.ConvertedValue - prev.ConvertedValue DifferenceInLitres, cte.Longitude lon, cte.Latitude lat,null Zone
	FROM CTE
	    inner join GFI_SYS_UOM u on u.UID = cte.UOM
        LEFT JOIN CTE prev ON prev.rownum = CTE.rownum - 1
order by DateTimeRule
";


            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        public class statusMessage
        {
            public int ruleID { get; set; }

            public string status { get; set; }
        }


        public DataTable GetPlanningExceptionsDetails(String sConnStr)
        {
            String sqlString = @"select E.RuleID ,a.Field4 as status ,a.Date1 as excpected  ,a.Date2 as actual ,a.Field1 as zone 
                                 from GFI_GPS_Exceptions e inner join GFI_GPS_ExceptionAdditionalInfo a  on e.iID = a.ExcepID where a.ExceptionType = 'Planning'";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        public BaseModel getPlanningExceptions(String sConnStr)
        {
            List<statusMessage> sMessge = new List<statusMessage>();
            BaseModel bm = new BaseModel();
            string errormesssage = string.Empty;
            string sresult = string.Empty;
            try
            {
                DataTable dtRulesByPlanningStruc = new RulesService().GetRulesbyStruc("Planning", sConnStr);

                //DataTable dtRulesByPlanningStruc = dtRulesByPlanningStruc1.AsEnumerable().Where(a => a.Field<int>("ParentRuleID") == 1857).CopyToDataTable();


                if (dtRulesByPlanningStruc.Rows.Count > 0 && dtRulesByPlanningStruc != null)
                 {
                    DataView view = new DataView(dtRulesByPlanningStruc);
                    DataTable distinctParentRuleID = view.ToTable(true, "ParentRuleID");

                    

                    //loop all distinct parentruleid
                    foreach (DataRow dr in distinctParentRuleID.Rows)
                    {
                        List<NaveoOneLib.Models.GMatrix.Matrix> lm = new List<NaveoOneLib.Models.GMatrix.Matrix>();
                        int ParentRuleID = Convert.ToInt32(dr["ParentRuleID"].ToString());

                        //check if parentruleid exist in table and take all data
                        //var drSelectedParentRuleIdRows = dtRulesByPlanningStruc.AsEnumerable().Where(r => r.Field<int>("ParentRuleID") == ParentRuleID);
                        DataTable dtSelectedParentRuleIdRows = new RulesService().GetRulesByParentRuleID(ParentRuleID.ToString(), sConnStr);

                        //var firstRow = dtSelectedParentRuleIdRows.Rows.Take(1);
                        //string rulename = firstRow.Select(r => r.Field<String>("RuleName")).First();

                        List<int> lAssetIds = new List<int>();
                        List<string> lAlertCond = new List<string>();

                        if (dtSelectedParentRuleIdRows != null)
                        {

                            NaveoOneLib.Models.GMatrix.Matrix matrix = new NaveoOneLib.Models.GMatrix.Matrix();
                            matrix.GMID = Convert.ToInt32(dtSelectedParentRuleIdRows.Rows[0]["GMID"].ToString());
                            matrix.MID = Convert.ToInt32(dtSelectedParentRuleIdRows.Rows[0]["MID"].ToString());
                            matrix.iID = Convert.ToInt32(dtSelectedParentRuleIdRows.Rows[0]["iID"].ToString());
                            lm.Add(matrix);

                            //loop through all selected parentruleid and take assets and ruleconditions
                            foreach (DataRow drRows in dtSelectedParentRuleIdRows.Rows)
                            {



                                //Assets with GMID
                                if (drRows["Struc"].ToString() == "Assets" && drRows["StrucType"].ToString() == "GMID")
                                {
                                    int AssetGMID = Convert.ToInt32(drRows["StrucValue"].ToString());
                                    List<NaveoOneLib.Models.GMatrix.Matrix> lmatrix = new List<NaveoOneLib.Models.GMatrix.Matrix>();
                                    NaveoOneLib.Models.GMatrix.Matrix m = new NaveoOneLib.Models.GMatrix.Matrix();
                                    m.GMID = AssetGMID;
                                    lmatrix.Add(m);

                                    dtData dtd = new NaveoOneLib.WebSpecifics.LibData().NaveoMasterData(0, lmatrix, sConnStr, NaveoModels.Assets, null, null);
                                    lAssetIds = dtd.Data.AsEnumerable().Select(r => r.Field<int>("id")).ToList();


                                }

                                //Assets with ID
                                if (drRows["Struc"].ToString() == "Assets" && drRows["StrucType"].ToString() == "ID")
                                {
                                    int AssetId = Convert.ToInt32(drRows["StrucValue"].ToString());
                                    lAssetIds.Add(AssetId);

                                }


                                //Planning RuleConditions
                                if (drRows["StrucValue"].ToString() == "Planning")
                                {
                                    string planningStrucCondition = drRows["StrucCondition"].ToString();
                                    lAlertCond.Add(planningStrucCondition);

                                }

                            }



                            //if rule has assets and ruleconditions then get exceptions data and save to exceptions table
                            if (lAlertCond.Count() != 0 && lAssetIds.Count() != 0)
                            {
                                //get exceptions data for planning
                                BaseModel bmGenerateAlertPlanning = new NaveoOneLib.Services.Rules.AlertService().GenerateAlertPlanning(lm, ParentRuleID, lAlertCond, lAssetIds, sConnStr);
                                DataTable dtFinal = bmGenerateAlertPlanning.data;


                                DataTable saveExceptionsPlanning = new NaveoOneLib.Services.Rules.ExceptionsService().GetPlanningExceptionsDetails(sConnStr);

                                if (dtFinal != null  &&  dtFinal.Rows.Count != 0)
                                {
                                    //save to exceptions table
                                    List<NaveoOneLib.Models.Rules.PlanningExceptions> lExceptions = new List<NaveoOneLib.Models.Rules.PlanningExceptions>();
                                    foreach (DataRow drG in bmGenerateAlertPlanning.data.Rows)
                                    {
                                        DateTime dtNotVisited = new DateTime(1999, 12, 31);
                                        NaveoOneLib.Models.Rules.PlanningExceptions e = new NaveoOneLib.Models.Rules.PlanningExceptions();
                                        e.RuleID = Convert.ToInt32(drG["RuleName"]);

                                        e.AssetID = Convert.ToInt32(drG["AssetID"]);
                                        e.DriverID = Convert.ToInt32(drG["DriverID"]);
                                        e.InsertedAt = DateTime.Now;
                                        e.DateTimeGPS_UTC = Convert.ToDateTime(drG["ExceptedTime"].ToString());
                                        e.ExpectedTime = Convert.ToDateTime(drG["ExceptedTime"].ToString());
                                        e.AcutalTime = String.IsNullOrEmpty(drG["ActualTime"].ToString()) ? dtNotVisited : Convert.ToDateTime(drG["ActualTime"].ToString());
                                        e.Buffer = Convert.ToInt32(drG["Buffer"].ToString());
                                        e.Durations = drG["DurationInsideZone"].ToString();
                                        e.AlertStatus = drG["Status"].ToString();
                                        e.ExceptionType = "Planning";
                                        e.viapoint = drG["ViaPoint"].ToString();

                                        DataTable resultQuery = new DataTable();
                                        if (saveExceptionsPlanning != null)
                                        {
                                            var rows = from r in saveExceptionsPlanning.AsEnumerable()
                                                        where ((r.Field<int>("RuleID") == e.RuleID) && (r.Field<string>("status") == e.AlertStatus) && (r.Field<DateTime>("excpected") == e.ExpectedTime) && (r.Field<DateTime>("actual") == e.AcutalTime) && (r.Field<string>("zone") == e.viapoint))
                                                        select r;

                                            if (rows.Any())
                                                resultQuery = rows.CopyToDataTable();
                                        }

                                        
                                        if(resultQuery.Rows.Count == 0)
                                            lExceptions.Add(e);
                                    }


                                    Boolean bmSave = false;

                                    statusMessage sm = new statusMessage();

                                    sm.ruleID = ParentRuleID;

                                    if (lExceptions.Count() != 0)
                                    {
                                        
                                        bmSave = new NaveoOneLib.Services.Rules.ExceptionsService().SavePlanningExceptions(lExceptions, true, sConnStr);

                                        if (bmSave == true)
                                            sm.status = "OK";
                                        else
                                            sm.status += "FAILED";


                                    } else
                                    {
                                        sm.status += "No Processing of Planning Exceptions ,Exceptions already triggered .. \r\n";
                                    }

                     
                                  
                                  

                                    sMessge.Add(sm);

                                } else
                                {
                                    sresult = "No Process of  Planning Exceptions \r\n";
                                }

                            }
                        }

                    }
                } else
                {
                    sresult = "No Process of  Planning Exceptions \r\n";
                }

            }
            catch(Exception ex)
            {

                sresult += "Process Planning Exceptions crashed "+ ex.ToString() + "\r\n";
            }

      
            if (sMessge.Count > 0 && sMessge != null)
            {
                foreach (var x in sMessge)
                {
                    sresult += "Process Planning Exceptions " + x.status + " for RuleID " + x.ruleID.ToString() + "\r\n";
                }
            }

            bm.data = sMessge;
            bm.additionalData = sresult;            
                
            return bm;
        }

    }
}