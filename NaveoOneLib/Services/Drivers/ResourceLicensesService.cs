using System;
using System.Collections.Generic;
using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using System.Data;
using NaveoOneLib.Models.Drivers;
using NaveoOneLib.Services.Audits;

namespace NaveoOneLib.Services.Drivers
{
    class ResourceLicensesService
    {
        Errors er = new Errors();
        public DataTable GetResourceLicenses(String sConnStr)
        {
            String sqlString = @"SELECT iID, 
DriverID, 
sType, 
sCategory, 
sNumber, 
IssueDate, 
ExpiryDate from GFI_FLT_ResourceLicenses order by DriverID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public ResourceLicense GetResourceLicenses(DataRow dr)
        {
            ResourceLicense retResourceLicenses = new ResourceLicense();
            retResourceLicenses.iID = Convert.ToInt32(dr["iID"]);
            retResourceLicenses.DriverID = Convert.ToInt32(dr["DriverID"]);
            retResourceLicenses.sType = dr["sType"].ToString();
            retResourceLicenses.sCategory = dr["sCategory"].ToString();
            retResourceLicenses.sNumber = dr["sNumber"].ToString();
            retResourceLicenses.IssueDate = String.IsNullOrEmpty(dr["IssueDate"].ToString()) ? (DateTime?)null : (DateTime)dr["IssueDate"];
            retResourceLicenses.ExpiryDate = String.IsNullOrEmpty(dr["ExpiryDate"].ToString()) ? (DateTime?)null : (DateTime)dr["ExpiryDate"];
            return retResourceLicenses;
        }
        public ResourceLicense GetResourceLicensesById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT iID, 
DriverID, 
sType, 
sCategory, 
sNumber, 
IssueDate, 
ExpiryDate from GFI_FLT_ResourceLicenses
WHERE DriverID = ?
order by DriverID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetResourceLicenses(dt.Rows[0]);
        }
        public List<ResourceLicense> GetListByDriver(int DriverID, String sConnStr)
        {
            String sql = @"SELECT * from GFI_FLT_ResourceLicenses WHERE DriverID = ?";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sql, DriverID.ToString(), sConnStr);
            if (dt.Rows.Count == 0)
                return null;

            List<ResourceLicense> l = new List<ResourceLicense>();
            foreach(DataRow dr in dt.Rows)
                l.Add(GetResourceLicenses(dr));

            return l;
        }

        DataTable ResourceLicensesDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_ResourceLicenses";
            dt.Columns.Add("iID", typeof(int));
            dt.Columns.Add("DriverID", typeof(int));
            dt.Columns.Add("sType", typeof(String));
            dt.Columns.Add("sCategory", typeof(String));
            dt.Columns.Add("sNumber", typeof(String));
            dt.Columns.Add("IssueDate", typeof(DateTime));
            dt.Columns.Add("ExpiryDate", typeof(DateTime));

            return dt;
        }
        public Boolean SaveResourceLicenses(ResourceLicense uResourceLicenses, Boolean bInsert, String sConnStr)
        {
            DataTable dt = ResourceLicensesDT();

            DataRow dr = dt.NewRow();
            dr["iID"] = uResourceLicenses.iID;
            dr["DriverID"] = uResourceLicenses.DriverID;
            dr["sType"] = uResourceLicenses.sType;
            dr["sCategory"] = uResourceLicenses.sCategory;
            dr["sNumber"] = uResourceLicenses.sNumber;
            dr["IssueDate"] = uResourceLicenses.IssueDate != null ? uResourceLicenses.IssueDate : (object)DBNull.Value;
            dr["ExpiryDate"] = uResourceLicenses.ExpiryDate != null ? uResourceLicenses.ExpiryDate : (object)DBNull.Value;
            dt.Rows.Add(dr);


            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "DriverID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uResourceLicenses.DriverID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public Boolean DeleteResourceLicenses(ResourceLicense uResourceLicenses, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_FLT_ResourceLicenses where DriverID = " + uResourceLicenses.DriverID.ToString();
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            DataTable dtAudit = new AuditService().LogTran(3, "ResourceLicenses", uResourceLicenses.DriverID.ToString(), Globals.uLogin.UID, uResourceLicenses.DriverID);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "CallerFunction";
            drCtrl["ParentTblName"] = "GFI_FLT_ResourceLicenses";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
    }
}