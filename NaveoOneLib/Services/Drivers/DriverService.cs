﻿using System;
using System.Collections.Generic;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Drivers;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Models.Common;

namespace NaveoOneLib.Services.Drivers
{
    public class DriverService
    {
        Errors er = new Errors();
        public String sGetDriver(int uLogin, String sqlDriverIds, Boolean bShowEmail, Driver.ResourceType rt, String sConnStr)
        {
            List<Matrix> lMatrix = new MatrixService().GetMatrixByUserID(uLogin, sConnStr);

            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            return sGetDriver(iParentIDs, sqlDriverIds, bShowEmail, rt, sConnStr);
        }
        public String sGetDriver(List<Matrix> lMatrix, String sqlDriverIds, Boolean bShowEmail, Driver.ResourceType rt, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            return sGetDriver(iParentIDs, sqlDriverIds, bShowEmail, rt, sConnStr);
        }
        public String sGetDriver(List<int> iParentIDs, String sqlDriverIds, Boolean bShowEmail, Driver.ResourceType rt, String sConnStr)
        {
            //String sql = @"SELECT u.* 
            //                    from GFI_FLT_Driver u, GFI_SYS_GroupMatrixDriver m
            //                    where EmployeeType = '" + rt.ToString() + @"' and
            //                        u.DriverID = m.iID and m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + ") " + sqlDriverIds;

            String sql = @"SELECT u.* 
                                from GFI_FLT_Driver u
                                    left outer join GFI_SYS_User t on u.UserId = t.UID
		                            inner join GFI_SYS_GroupMatrixDriver m on u.DriverID = m.iID
                          ";
            if (bShowEmail)
                sql = sql.Replace("SELECT u.*", "SELECT u.*, t.Email eMailAddress");

            if (rt != Driver.ResourceType.All)
                sql += " where EmployeeType = '" + rt.ToString() + "'";
            sql += @" and m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + ") " + sqlDriverIds;

            if (rt == Driver.ResourceType.Driver)
                sql += " union " + sGetDefaultDriver(bShowEmail);

            return sql;
        }
        //public String sGetDriver(List<int> iParentIDs)
        //{
        //    return sGetDriver(iParentIDs, Driver.ResourceType.Driver);
        //}
        String sGetDefaultDriver(Boolean showEmail)
        {


            String sql = @"SELECT *  from GFI_FLT_Driver 
                                    WHERE iButton = '00000000' and EmployeeType = 'Driver'";

            if (showEmail)
                sql = sql.Replace("SELECT *  from GFI_FLT_Driver ", "SELECT  d.* ,d.Address1 as address1  from GFI_FLT_Driver d");

            return sql;
        }
        public DataTable GetDriver(List<Matrix> lMatrix, Driver.ResourceType rt, Boolean bShowMail, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            return GetDriver(iParentIDs, rt, bShowMail, sConnStr);
        }
        public DataTable GetDriver(List<int> iParentIDs, Driver.ResourceType rt, Boolean bShowMail, String sConnStr)
        {
            DataTable dt = new Connection(sConnStr).GetDataDT(sGetDriver(iParentIDs, String.Empty, bShowMail, rt, sConnStr), sConnStr);
            if (dt.Rows.Count == 0)
                if (rt == Driver.ResourceType.Driver)
                    new Connection(sConnStr).GetDataDT(sGetDefaultDriver(false), sConnStr);

            return dt;
        }
        Driver GetDriver(DataSet ds)
        {
            Driver d = new Driver();
            d.DriverID = Convert.ToInt32(ds.Tables["dtDriver"].Rows[0]["DriverID"]);
            d.sDriverName = ds.Tables["dtDriver"].Rows[0]["sDriverName"].ToString();
            d.iButton = ds.Tables["dtDriver"].Rows[0]["iButton"].ToString();
            d.sEmployeeNo = ds.Tables["dtDriver"].Rows[0]["sEmployeeNo"].ToString();
            d.sComments = ds.Tables["dtDriver"].Rows[0]["sComments"].ToString();
            d.EmployeeType = ds.Tables["dtDriver"].Rows[0]["EmployeeType"].ToString();

            d.Gender = ds.Tables["dtDriver"].Rows[0]["Gender"].ToString();
            d.Address1 = ds.Tables["dtDriver"].Rows[0]["Address1"].ToString();
            d.Address2 = ds.Tables["dtDriver"].Rows[0]["Address2"].ToString();
            d.City = ds.Tables["dtDriver"].Rows[0]["City"].ToString();
            d.Nationality = ds.Tables["dtDriver"].Rows[0]["Nationality"].ToString();
            d.sFirtName = ds.Tables["dtDriver"].Rows[0]["sFirtName"].ToString();
            d.LicenseNo1 = ds.Tables["dtDriver"].Rows[0]["LicenseNo1"].ToString();
            d.LicenseNo2 = ds.Tables["dtDriver"].Rows[0]["LicenseNo2"].ToString();
            d.Ref1 = ds.Tables["dtDriver"].Rows[0]["Ref1"].ToString();
            d.Status = ds.Tables["dtDriver"].Rows[0]["Status"].ToString();
            d.Address3 = ds.Tables["dtDriver"].Rows[0]["Address3"].ToString();
            d.District = ds.Tables["dtDriver"].Rows[0]["District"].ToString();
            d.PostalCode = ds.Tables["dtDriver"].Rows[0]["PostalCode"].ToString();
            d.MobileNumber = ds.Tables["dtDriver"].Rows[0]["MobileNumber"].ToString();
            d.Reason = ds.Tables["dtDriver"].Rows[0]["Reason"].ToString();
            d.Department = ds.Tables["dtDriver"].Rows[0]["Department"].ToString();
            d.Email = ds.Tables["dtDriver"].Rows[0]["Email"].ToString();
            d.sLastName = ds.Tables["dtDriver"].Rows[0]["sLastName"].ToString();
            d.OfficerLevel = ds.Tables["dtDriver"].Rows[0]["OfficerLevel"].ToString();
            d.OfficerTitle = ds.Tables["dtDriver"].Rows[0]["OfficerTitle"].ToString();
            d.Phone = ds.Tables["dtDriver"].Rows[0]["Phone"].ToString();
            d.Title = ds.Tables["dtDriver"].Rows[0]["Title"].ToString();
            d.CountryCode = ds.Tables["dtDriver"].Rows[0]["CountryCode"].ToString();
            d.ASPUser = ds.Tables["dtDriver"].Rows[0]["ASPUser"].ToString();

            d.DateOfBirth = String.IsNullOrEmpty(ds.Tables["dtDriver"].Rows[0]["DateOfBirth"].ToString()) ? (DateTime?)null : (DateTime)ds.Tables["dtDriver"].Rows[0]["DateOfBirth"];
            d.LicenseNo1Expiry = String.IsNullOrEmpty(ds.Tables["dtDriver"].Rows[0]["LicenseNo1Expiry"].ToString()) ? (DateTime?)null : (DateTime)ds.Tables["dtDriver"].Rows[0]["LicenseNo1Expiry"];
            d.Licenseno2Expiry = String.IsNullOrEmpty(ds.Tables["dtDriver"].Rows[0]["Licenseno2Expiry"].ToString()) ? (DateTime?)null : (DateTime)ds.Tables["dtDriver"].Rows[0]["Licenseno2Expiry"];
            d.CreatedDate = String.IsNullOrEmpty(ds.Tables["dtDriver"].Rows[0]["CreatedDate"].ToString()) ? (DateTime?)null : (DateTime)ds.Tables["dtDriver"].Rows[0]["CreatedDate"];
            d.UpdatedDate = String.IsNullOrEmpty(ds.Tables["dtDriver"].Rows[0]["UpdatedDate"].ToString()) ? (DateTime?)null : (DateTime)ds.Tables["dtDriver"].Rows[0]["UpdatedDate"];
            d.UserId = String.IsNullOrEmpty(ds.Tables["dtDriver"].Rows[0]["UserId"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtDriver"].Rows[0]["UserId"]);
            d.ZoneID = String.IsNullOrEmpty(ds.Tables["dtDriver"].Rows[0]["ZoneID"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtDriver"].Rows[0]["ZoneID"]);

            d.CreatedBy = String.IsNullOrEmpty(ds.Tables["dtDriver"].Rows[0]["CreatedBy"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtDriver"].Rows[0]["CreatedBy"]);
            d.UpdatedBy = String.IsNullOrEmpty(ds.Tables["dtDriver"].Rows[0]["UpdatedBy"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtDriver"].Rows[0]["UpdatedBy"]);

            d.Type_Institution = String.IsNullOrEmpty(ds.Tables["dtDriver"].Rows[0]["Type_Institution"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtDriver"].Rows[0]["Type_Institution"]);
            d.Type_Driver = String.IsNullOrEmpty(ds.Tables["dtDriver"].Rows[0]["Type_Driver"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtDriver"].Rows[0]["Type_Driver"]);
            d.Type_Grade = String.IsNullOrEmpty(ds.Tables["dtDriver"].Rows[0]["Type_Grade"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtDriver"].Rows[0]["Type_Grade"]);
            d.HomeNo = String.IsNullOrEmpty(ds.Tables["dtDriver"].Rows[0]["HomeNo"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtDriver"].Rows[0]["HomeNo"]);
            d.Photo = String.IsNullOrEmpty(ds.Tables["dtDriver"].Rows[0]["Photo"].ToString()) ? (byte[])null : (byte[])ds.Tables["dtDriver"].Rows[0]["Photo"];
            //MemoryStream ms = new MemoryStream((byte[])dt.Rows[0]["ImageData"]);
            //picImage.Image = Image.FromStream(ms);

            //Zones 
            if (ds.Tables.Contains("dtZones"))
                if (ds.Tables["dtZones"].Rows.Count > 0)
                {
                    int? a = Convert.ToInt32(d.ZoneID);

                    a = String.IsNullOrEmpty(ds.Tables["dtZones"].Rows[0]["ZoneID"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtZones"].Rows[0]["ZoneID"]);
                    d.ZoneDesc = String.IsNullOrEmpty(ds.Tables["dtZones"].Rows[0]["Description"].ToString()) ? String.Empty : ds.Tables["dtZones"].Rows[0]["Description"].ToString();
                }


            //District
            if (ds.Tables.Contains("dtDistrict"))
                if (ds.Tables["dtDistrict"].Rows.Count > 0)
                {
                    int? a = Convert.ToInt32(d.District);

                    a = String.IsNullOrEmpty(ds.Tables["dtDistrict"].Rows[0]["VID"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtDistrict"].Rows[0]["VID"]);
                    d.DistrictDesc = String.IsNullOrEmpty(ds.Tables["dtDistrict"].Rows[0]["Name"].ToString()) ? String.Empty : ds.Tables["dtDistrict"].Rows[0]["Name"].ToString();
                }


            //DriverType
            if (ds.Tables.Contains("dtDriverType"))
                if (ds.Tables["dtDriverType"].Rows.Count > 0)
                {


                    d.Type_Driver = String.IsNullOrEmpty(ds.Tables["dtDriverType"].Rows[0]["VID"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtDriverType"].Rows[0]["VID"]);
                    d.Type_DriverDesc = String.IsNullOrEmpty(ds.Tables["dtDriverType"].Rows[0]["Name"].ToString()) ? String.Empty : ds.Tables["dtDriverType"].Rows[0]["Name"].ToString();
                }


            //GradeType
            if (ds.Tables.Contains("dtGradeType"))
                if (ds.Tables["dtGradeType"].Rows.Count > 0)
                {


                    d.Type_Grade = String.IsNullOrEmpty(ds.Tables["dtGradeType"].Rows[0]["VID"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtGradeType"].Rows[0]["VID"]);
                    d.Type_GradeDesc = String.IsNullOrEmpty(ds.Tables["dtGradeType"].Rows[0]["Name"].ToString()) ? String.Empty : ds.Tables["dtGradeType"].Rows[0]["Name"].ToString();
                }

            //Institution
            if (ds.Tables.Contains("dtInstitution"))
                if (ds.Tables["dtInstitution"].Rows.Count > 0)
                {


                    d.Type_Institution = String.IsNullOrEmpty(ds.Tables["dtInstitution"].Rows[0]["VID"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtInstitution"].Rows[0]["VID"]);
                    d.Type_InstitutionDesc = String.IsNullOrEmpty(ds.Tables["dtInstitution"].Rows[0]["Name"].ToString()) ? String.Empty : ds.Tables["dtInstitution"].Rows[0]["Name"].ToString();
                }


            //City
            if (ds.Tables.Contains("dtCity"))
                if (ds.Tables["dtCity"].Rows.Count > 0)
                {

                    int? a = Convert.ToInt32(d.City);
                    a = String.IsNullOrEmpty(ds.Tables["dtCity"].Rows[0]["VID"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtCity"].Rows[0]["VID"]);
                    d.CityDesc = String.IsNullOrEmpty(ds.Tables["dtCity"].Rows[0]["Name"].ToString()) ? String.Empty : ds.Tables["dtCity"].Rows[0]["Name"].ToString();
                }

            //Users
            if (ds.Tables.Contains("dtUsers"))
                if (ds.Tables["dtUsers"].Rows.Count > 0)
                {


                    d.UserId = String.IsNullOrEmpty(ds.Tables["dtUsers"].Rows[0]["UID"].ToString()) ? (int?)null : Convert.ToInt32(ds.Tables["dtUsers"].Rows[0]["UID"]);
                    d.UserDesc = String.IsNullOrEmpty(ds.Tables["dtUsers"].Rows[0]["Names"].ToString()) ? String.Empty : ds.Tables["dtUsers"].Rows[0]["Names"].ToString();
                }


            if (ds.Tables.Contains("dtLic"))
            {
                ResourceLicensesService s = new ResourceLicensesService();
                List<ResourceLicense> l = new List<ResourceLicense>();
                foreach (DataRow dr in ds.Tables["dtLic"].Rows)
                    l.Add(s.GetResourceLicenses(dr));
                d.Licenses = l;
            }

            List<Matrix> lMatrix = new List<Matrix>();
            MatrixService ms = new MatrixService();
            foreach (DataRow dr in ds.Tables["dtMatrix"].Rows)
                lMatrix.Add(ms.AssignMatrix(dr));
            d.lMatrix = lMatrix;

            return d;
        }
        public Driver GetDriverById(String iID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"SELECT * from GFI_FLT_Driver
                                    WHERE DriverID = ?
                                    order by DriverID";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = iID.ToString();
            drSql["sTableName"] = "dtDriver";
            dtSql.Rows.Add(drSql);

            sql = new MatrixService().sGetMatrixId(Convert.ToInt32(iID), "GFI_SYS_GroupMatrixDriver");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);

            sql = @"SELECT * from GFI_FLT_ResourceLicenses WHERE DriverID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = iID.ToString();
            drSql["sTableName"] = "dtLic";
            dtSql.Rows.Add(drSql);

            //Zones 
            sql = "select zH.ZoneID,zH.Description,d.DriverID from GFI_FLT_ZoneHeader zH inner join GFI_FLT_Driver d  on zH.ZoneID = d.ZoneID where d.DriverID =?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = iID.ToString();
            drSql["sTableName"] = "dtZones";
            dtSql.Rows.Add(drSql);

            //District
            sql = "select lV.VID,lV.Name,d.DriverID from GFI_SYS_LookUpValues lV inner join GFI_FLT_Driver d  on lv.VID = d.District where d.DriverID =?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = iID.ToString();
            drSql["sTableName"] = "dtDistrict";
            dtSql.Rows.Add(drSql);



            //Type_Driver
            sql = "select lV.VID,lV.Name,d.DriverID from GFI_SYS_LookUpValues lV inner join GFI_FLT_Driver d  on lv.VID = d.Type_Driver where d.DriverID =?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = iID.ToString();
            drSql["sTableName"] = "dtDriverType";
            dtSql.Rows.Add(drSql);

            //Type_Grade
            sql = "select lV.VID,lV.Name,d.DriverID from GFI_SYS_LookUpValues lV inner join GFI_FLT_Driver d  on lv.VID = d.Type_Grade where d.DriverID =?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = iID.ToString();
            drSql["sTableName"] = "dtGradeType";
            dtSql.Rows.Add(drSql);

            //Institution
            sql = "select lV.VID,lV.Name,d.DriverID from GFI_SYS_LookUpValues lV inner join GFI_FLT_Driver d  on lv.VID = d.Type_Institution where d.DriverID =?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = iID.ToString();
            drSql["sTableName"] = "dtInstitution";
            dtSql.Rows.Add(drSql);

            //Institution
            sql = "select lV.VID,lV.Name,d.DriverID from GFI_SYS_LookUpValues lV inner join GFI_FLT_Driver d  on lv.VID = d.City where d.DriverID =?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = iID.ToString();
            drSql["sTableName"] = "dtCity";
            dtSql.Rows.Add(drSql);

            //Users
            sql = "select u.UID ,u.Names  from GFI_FLT_Driver d inner join GFI_SYS_User u  on d.UserId =u.UID where d.DriverID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = iID.ToString();
            drSql["sTableName"] = "dtUsers";
            dtSql.Rows.Add(drSql);


            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables["dtDriver"].Rows.Count == 0)
                return GetDefaultDriver(sConnStr);
            else
                return GetDriver(ds);
        }
        public DataTable GetDriverByName(String driverName, String sConnStr)
        {
            String sqlString = @"SELECT * from GFI_FLT_Driver WHERE sDriverName = ? order by DriverID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, driverName, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return dt;
        }
        public Driver GetDriverByIbutton(String iButton, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"SELECT DriverID, 
                                    sDriverName, 
                                    iButton, 
                                    sEmployeeNo, 
                                    sComments, EmployeeType from GFI_FLT_Driver
                                    WHERE iButton = ? and EmployeeType = 'Driver'
                                    order by DriverID";

            if (iButton.Trim() == String.Empty)
                iButton = "00000000";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = iButton;
            drSql["sTableName"] = "dtDriver";
            dtSql.Rows.Add(drSql);

            sql = new MatrixService().sGetMatrixIdByField("GFI_FLT_Driver", "iButton", iButton, "DriverID", "GFI_SYS_GroupMatrixDriver");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables["dtDriver"].Rows.Count == 0)
                return GetDefaultDriver(sConnStr);
            else
                return GetDriver(ds);
        }
        public Driver GetDefaultDriver(String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String iButton = "00000000";
            String sql = @"SELECT * from GFI_FLT_Driver
                                    WHERE iButton = ? and EmployeeType = 'Driver'
                                    order by DriverID";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = iButton;
            drSql["sTableName"] = "dtDriver";
            dtSql.Rows.Add(drSql);

            sql = new MatrixService().sGetMatrixIdByField("GFI_FLT_Driver", "iButton", iButton, "DriverID", "GFI_SYS_GroupMatrixDriver");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            Driver d = new Driver();
            if (ds.Tables["dtDriver"].Rows.Count == 0)
            {
                GroupMatrix gmRoot = new GroupMatrix();
                gmRoot = new GroupMatrixService().GetRoot(sConnStr);

                d = new Driver();
                d.sDriverName = "Default Driver";
                d.sComments = "Driver for vehicles with no Driver";
                d.sEmployeeNo = "INSTALLER";
                d.iButton = "00000000";
                d.EmployeeType = Driver.ResourceType.Driver.ToString();

                Matrix m = new Matrix();
                m.GMID = gmRoot.gmID;
                m.oprType = DataRowState.Added;
                d.lMatrix.Add(m);

                new DriverService().SaveDriver(d, true, sConnStr);
                d = GetDefaultDriver(sConnStr);
            }
            else
                d = GetDriver(ds);

            return d;
        }
        DataTable DriverDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_Driver";
            dt.Columns.Add("DriverID", typeof(int));
            dt.Columns.Add("sDriverName", typeof(String));
            dt.Columns.Add("iButton", typeof(String));
            dt.Columns.Add("sEmployeeNo", typeof(String));
            dt.Columns.Add("sComments", typeof(String));
            dt.Columns.Add("EmployeeType", typeof(String));
            dt.Columns.Add("DateOfBirth", typeof(DateTime));
            dt.Columns.Add("Gender", typeof(String));

            dt.Columns.Add("Address1", typeof(String));
            dt.Columns.Add("Address2", typeof(String));
            dt.Columns.Add("City", typeof(String));
            dt.Columns.Add("Nationality", typeof(String));
            dt.Columns.Add("sFirtName", typeof(String));
            dt.Columns.Add("LicenseNo1", typeof(String));
            dt.Columns.Add("LicenseNo1Expiry", typeof(DateTime));
            dt.Columns.Add("LicenseNo2", typeof(String));
            dt.Columns.Add("Licenseno2Expiry", typeof(DateTime));
            dt.Columns.Add("Ref1", typeof(String));
            dt.Columns.Add("ZoneID", typeof(String));
            dt.Columns.Add("CreatedDate", typeof(DateTime));
            dt.Columns.Add("CreatedBy", typeof(String));
            dt.Columns.Add("UpdatedDate", typeof(DateTime));
            dt.Columns.Add("UpdatedBy", typeof(String));
            dt.Columns.Add("Status", typeof(String));
            dt.Columns.Add("Address3", typeof(String));
            dt.Columns.Add("District", typeof(String));
            dt.Columns.Add("PostalCode", typeof(String));
            dt.Columns.Add("MobileNumber", typeof(String));
            dt.Columns.Add("Reason", typeof(String));
            dt.Columns.Add("Department", typeof(String));
            dt.Columns.Add("Email", typeof(String));
            dt.Columns.Add("sLastName", typeof(String));
            dt.Columns.Add("OfficerLevel", typeof(String));
            dt.Columns.Add("OfficerTitle", typeof(String));
            dt.Columns.Add("Phone", typeof(String));
            dt.Columns.Add("Title", typeof(String));
            dt.Columns.Add("ASPUser", typeof(String));
            dt.Columns.Add("CountryCode", typeof(String));

            dt.Columns.Add("UserId", typeof(int));

            dt.Columns.Add("Type_Institution", typeof(int));
            dt.Columns.Add("Type_Driver", typeof(int));
            dt.Columns.Add("Type_Grade", typeof(int));
            dt.Columns.Add("HomeNo", typeof(int));
            dt.Columns.Add("Photo", typeof(byte[]));
            dt.Columns.Add("oprType", typeof(DataRowState));

            return dt;
        }
        public BaseModel SaveDriver(Driver uDriver, Boolean bInsert, String sConnStr)
        {
            BaseModel bm = new Models.Common.BaseModel();
            //Insert only in Sysinit. No update allowed
            if (uDriver.iButton == "00000000")
            {
                if (bInsert)
                {
                    if (uDriver.sDriverName != "Default Driver")
                    {
                        bm.data = false;
                        bm.errorMessage = "Default Driver id not suitable";
                        return bm;
                    }
                }
                else
                {
                    bm.data = false;
                    bm.errorMessage = "Default Driver id";
                    return bm;
                }
            }

            if (bInsert)
            {
                if (uDriver.EmployeeType == Driver.ResourceType.Driver.ToString())
                {
                    Boolean b = CheckIFiButtonExist(uDriver.iButton, sConnStr);
                    if (b)
                    {
                        bm.data = false;
                        bm.errorMessage = "Driver token already exist on system";
                        return bm;
                    }
                }
            }
            DataTable dt = DriverDT();
            DataRow dr = dt.NewRow();
            dr["DriverID"] = uDriver.DriverID;
            dr["sDriverName"] = uDriver.sDriverName;
            dr["iButton"] = uDriver.iButton;
            dr["sEmployeeNo"] = uDriver.sEmployeeNo;
            dr["sComments"] = uDriver.sComments;
            dr["EmployeeType"] = uDriver.EmployeeType;

            dr["Gender"] = uDriver.Gender;
            dr["Address1"] = uDriver.Address1;
            dr["Address2"] = uDriver.Address2;
            dr["City"] = uDriver.City;
            dr["Nationality"] = uDriver.Nationality;
            dr["sFirtName"] = uDriver.sFirtName;
            dr["LicenseNo1"] = uDriver.LicenseNo1;
            dr["LicenseNo2"] = uDriver.LicenseNo2;
            dr["Ref1"] = uDriver.Ref1;
            dr["ZoneID"] = uDriver.ZoneID;
            dr["Status"] = uDriver.Status;
            dr["Address3"] = uDriver.Address3;
            dr["District"] = uDriver.District;
            dr["PostalCode"] = uDriver.PostalCode;
            dr["MobileNumber"] = uDriver.MobileNumber;
            dr["Reason"] = uDriver.Reason;
            dr["Department"] = uDriver.Department;
            dr["Email"] = uDriver.Email;
            dr["sLastName"] = uDriver.sLastName;
            dr["OfficerLevel"] = uDriver.OfficerLevel;
            dr["OfficerTitle"] = uDriver.OfficerTitle;
            dr["Phone"] = uDriver.Phone;
            dr["CountryCode"] = uDriver.CountryCode;
            dr["Title"] = uDriver.Title;
            dr["ASPUser"] = uDriver.ASPUser;

            dr["DateOfBirth"] = uDriver.DateOfBirth != null ? uDriver.DateOfBirth : (object)DBNull.Value;
            dr["LicenseNo1Expiry"] = uDriver.LicenseNo1Expiry != null ? uDriver.LicenseNo1Expiry : (object)DBNull.Value;
            dr["Licenseno2Expiry"] = uDriver.Licenseno2Expiry != null ? uDriver.Licenseno2Expiry : (object)DBNull.Value;
            dr["CreatedDate"] = uDriver.CreatedDate != null ? uDriver.CreatedDate : (object)DBNull.Value;
            dr["UpdatedDate"] = uDriver.UpdatedDate != null ? uDriver.UpdatedDate : (object)DBNull.Value;

            dr["CreatedBy"] = uDriver.CreatedBy != null ? uDriver.CreatedBy : (object)DBNull.Value;
            dr["UpdatedBy"] = uDriver.UpdatedBy != null ? uDriver.UpdatedBy : (object)DBNull.Value;
            dr["UserId"] = uDriver.UserId != null ? uDriver.UserId : (object)DBNull.Value;

            dr["Type_Institution"] = uDriver.Type_Institution != null ? uDriver.Type_Institution : (object)DBNull.Value;
            dr["Type_Driver"] = uDriver.Type_Driver != null ? uDriver.Type_Driver : (object)DBNull.Value;
            dr["Type_Grade"] = uDriver.Type_Grade != null ? uDriver.Type_Grade : (object)DBNull.Value;
            dr["HomeNo"] = uDriver.HomeNo != null ? uDriver.HomeNo : (object)DBNull.Value;
            dr["Photo"] = uDriver.Photo != null ? uDriver.Photo : (object)DBNull.Value;

            dt.Rows.Add(dr);
            if (uDriver.Photo == null)
                dt.Columns.Remove("Photo");

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "DriverID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uDriver.DriverID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            DataTable dtMatrix = new myConverter().ListToDataTable<Matrix>(uDriver.lMatrix);
            dtMatrix.TableName = "GFI_SYS_GroupMatrixDriver";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 2;
            drCtrl["TblName"] = dtMatrix.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "MID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "iID";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtMatrix);

            if (uDriver.Licenses != null)
            {
                DataTable dtLicenses = new myConverter().ListToDataTable<ResourceLicense>(uDriver.Licenses);
                dtLicenses.TableName = "GFI_FLT_ResourceLicenses";
                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 3;
                drCtrl["TblName"] = dtLicenses.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "iID";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdAction"] = "SET";
                drCtrl["NextIdFieldName"] = "DriverID";
                drCtrl["ParentTblName"] = dt.TableName;
                dtCtrl.Rows.Add(drCtrl);
                dsProcess.Merge(dtLicenses);
            }

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResult.Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                {
                    bm.dbResult = dsResult;
                    bm.dbResult.Tables["CTRLTBL"].TableName = "ResultCtrlTbl";
                    bm.dbResult.Merge(dsProcess);
                    bResult = false;
                }
                else
                    bResult = true;
            }
            else
                bResult = false;

            bm.data = bResult;
            return bm;
        }
        bool UpdateDriver(Driver uDriver, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = DriverDT();
            DataRow dr = dt.NewRow();
            dr["DriverID"] = uDriver.DriverID;
            dr["sDriverName"] = uDriver.sDriverName;
            dr["iButton"] = uDriver.iButton;
            dr["sEmployeeNo"] = uDriver.sEmployeeNo;
            dr["sComments"] = uDriver.sComments;
            dr["EmployeeType"] = uDriver.EmployeeType;

            dr["Gender"] = uDriver.Gender;
            dr["Address1"] = uDriver.Address1;
            dr["Address2"] = uDriver.Address2;
            dr["City"] = uDriver.City;
            dr["Nationality"] = uDriver.Nationality;
            dr["sFirtName"] = uDriver.sFirtName;
            dr["LicenseNo1"] = uDriver.LicenseNo1;
            dr["LicenseNo2"] = uDriver.LicenseNo2;
            dr["Ref1"] = uDriver.Ref1;
            dr["ZoneID"] = uDriver.ZoneID;
            dr["Status"] = uDriver.Status;
            dr["Address3"] = uDriver.Address3;
            dr["District"] = uDriver.District;
            dr["PostalCode"] = uDriver.PostalCode;
            dr["MobileNumber"] = uDriver.MobileNumber;
            dr["Reason"] = uDriver.Reason;
            dr["Department"] = uDriver.Department;
            dr["Email"] = uDriver.Email;
            dr["sLastName"] = uDriver.sLastName;
            dr["OfficerLevel"] = uDriver.OfficerLevel;
            dr["OfficerTitle"] = uDriver.OfficerTitle;
            dr["Phone"] = uDriver.Phone;
            dr["Title"] = uDriver.Title;
            dr["ASPUser"] = uDriver.ASPUser;

            dr["DateOfBirth"] = uDriver.DateOfBirth != null ? uDriver.DateOfBirth : (object)DBNull.Value;
            dr["LicenseNo1Expiry"] = uDriver.LicenseNo1Expiry != null ? uDriver.LicenseNo1Expiry : (object)DBNull.Value;
            dr["Licenseno2Expiry"] = uDriver.Licenseno2Expiry != null ? uDriver.Licenseno2Expiry : (object)DBNull.Value;
            dr["CreatedDate"] = uDriver.CreatedDate != null ? uDriver.CreatedDate : (object)DBNull.Value;
            dr["UpdatedDate"] = uDriver.UpdatedDate != null ? uDriver.UpdatedDate : (object)DBNull.Value;

            dr["CreatedBy"] = uDriver.CreatedBy != null ? uDriver.CreatedBy : (object)DBNull.Value;
            dr["UpdatedBy"] = uDriver.UpdatedBy != null ? uDriver.UpdatedBy : (object)DBNull.Value;
            dr["UserId"] = uDriver.UserId != null ? uDriver.UserId : (object)DBNull.Value;

            dr["Type_Institution"] = uDriver.Type_Institution != null ? uDriver.Type_Institution : (object)DBNull.Value;
            dr["Type_Driver"] = uDriver.Type_Driver != null ? uDriver.Type_Driver : (object)DBNull.Value;
            dr["Type_Grade"] = uDriver.Type_Grade != null ? uDriver.Type_Grade : (object)DBNull.Value;
            dr["HomeNo"] = uDriver.HomeNo != null ? uDriver.HomeNo : (object)DBNull.Value;

            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "DriverID = '" + uDriver.DriverID + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool DeleteDriver(Driver uDriver, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_FLT_Driver", "DriverID = '" + uDriver.DriverID + "'", sConnStr))
                return true;
            else
                return false;
        }
        public String sGetPassenger(List<Matrix> lMatrix, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            return sGetPassenger(iParentIDs, sConnStr);
        }
        public String sGetPassenger(List<int> iParentIDs, String sConnStr)
        {
            String sql = @"SELECT DriverID, iButton,
                                    sDriverName, 
                                    sEmployeeNo, 
                                    sComments, EmployeeType 
                                from GFI_FLT_Driver u, GFI_SYS_GroupMatrixDriver m
                                where EmployeeType = 'Passenger'
                                    and u.DriverID = m.iID and m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + ") ";
            sql += " union " + sGetDefaultDriver(false);
            return sql;
        }
        public DataTable GetPassenger(List<Matrix> lMatrix, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            return GetPassenger(iParentIDs, sConnStr);
        }
        public DataTable GetPassenger(List<int> iParentIDs, String sConnStr)
        {
            DataTable dt = new Connection(sConnStr).GetDataDT(sGetPassenger(iParentIDs, sConnStr), sConnStr);
            return dt;
        }
        public Driver GetPassengerByIbutton(String iButton, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"SELECT DriverID, 
                                    sDriverName, 
                                    iButton, 
                                    sEmployeeNo, 
                                    sComments, EmployeeType from GFI_FLT_Driver
                                    WHERE iButton = ? and EmployeeType = 'Passenger'
                                    order by DriverID";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = iButton;
            drSql["sTableName"] = "dtDriver";
            dtSql.Rows.Add(drSql);

            sql = new MatrixService().sGetMatrixIdByField("GFI_FLT_Driver", "iButton", iButton, "DriverID", "GFI_SYS_GroupMatrixDriver");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return GetDriver(ds);
        }
        public Boolean CheckIFiButtonExist(string iButton, string sConnStr)
        {
            string sql = @"
                         --select 'dtCount' as TableName
                         select count(*) cnt from GFI_FLT_DRIVER where iButton = '" + iButton + "'";

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtCount";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            Boolean b = false;
            int i = 0;
            if (int.TryParse(ds.Tables["dtCount"].Rows[0]["cnt"].ToString(), out i))
                if (i >= 1)
                    b = true;

            return b;
        }

        public void InsertDriverID(String unitID, String striButtonID, DateTime dateTime, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_GPS_DriversDriving";
            dt.Columns.Add("unitID", typeof(String));
            dt.Columns.Add("DriverID", typeof(String));
            dt.Columns.Add("dtDateTime", typeof(DateTime));
            dt.Columns.Add("oprType", typeof(DataRowState));

            DataRow dr = dt.NewRow();
            dr["unitID"] = unitID;
            dr["DriverID"] = striButtonID;
            dr["dtDateTime"] = dateTime;
            dr["oprType"] = DataRowState.Added;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "unitID";
            drCtrl["KeyIsIdentity"] = "FALSE";
            dtCtrl.Rows.Add(drCtrl);

            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);
            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);
        }
        public DataSet GetDriverID(String unitID, String sConnStr)
        {
            String sql = "select * from GFI_GPS_DriversDriving where unitID = '" + unitID + "'";
            DataSet ds = new Connection(sConnStr).GetDataSet(sql, sConnStr);
            return ds;
        }
        public void DeleteDriverID(String unitID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_GPS_DriversDriving where unitID = '" + unitID + "'";
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);
        }



        public DataTable GetLiscenseDriverExpiry(List<Matrix> lMatrix, Boolean bShowMail, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            return GetLiscenseDriverExpiry(iParentIDs, bShowMail, sConnStr);
        }
        public DataTable GetLiscenseDriverExpiry(List<int> iParentIDs, Boolean bShowMail, String sConnStr)
        {
            DataTable dt = new Connection(sConnStr).GetDataDT(sGetLiscenseExpiryForDriver(iParentIDs, sConnStr), sConnStr);

            if (dt.Rows.Count == 0)
                return null;

            return dt;
        }


        String sGetLiscenseExpiryForDriver(List<int> iParentIDs, string sConnStr) 
        {
            //sql += @" where m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + @")
            String sql = @"
--GetDriverExpiry

select d.DriverID,
d.sDriverName,
d.employeetype as semployeeno,
rs.ExpiryDate,
DATEDIFF(month ,expirydate, getDate()) AS Date
from gfi_flt_driver d
inner join gfi_flt_resourcelicenses rs on d.driverId = rs.driverId
inner join gfi_sys_groupmatrixdriver m on m.iid = d.DriverID
where employeetype = 'Driver'";

            sql += @" and m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + @")";


            return sql;
        }



    }
}