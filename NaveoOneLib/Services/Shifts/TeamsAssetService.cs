using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Shifts;
using NaveoOneLib.Services.Audits;

namespace NaveoOneLib.Services.Shifts
{
    class TeamsAssetService
    {
        Errors er = new Errors();
        public DataTable GetTeamsAsset(String sConnStr)
        {
            String sqlString = @"SELECT iID, 
TID, 
AssetID from GFI_RST_TeamsAsset order by TID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        TeamsAsset GetTeamsAsset(DataRow dr)
        {
            TeamsAsset retTeamsAsset = new TeamsAsset();
            retTeamsAsset.iID = (int)dr["iID"];
            retTeamsAsset.TID = (int)dr["TID"];
            retTeamsAsset.AssetID = (int)dr["AssetID"];
            return retTeamsAsset;
        }
        public TeamsAsset GetTeamsAssetById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT iID, 
TID, 
AssetID from GFI_RST_TeamsAsset
WHERE TID = ?
order by TID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetTeamsAsset(dt.Rows[0]);
        }
        DataTable TeamsAssetDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_RST_TeamsAsset";
            dt.Columns.Add("iID", typeof(int));
            dt.Columns.Add("TID", typeof(int));
            dt.Columns.Add("AssetID", typeof(int));

            return dt;
        }
        public Boolean SaveTeamsAsset(TeamsAsset uTeamsAsset, Boolean bInsert, String sConnStr)
        {
            DataTable dt = TeamsAssetDT();

            DataRow dr = dt.NewRow();
            dr["iID"] = uTeamsAsset.iID;
            dr["TID"] = uTeamsAsset.TID;
            dr["AssetID"] = uTeamsAsset.AssetID;
            dt.Rows.Add(dr);


            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "TID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uTeamsAsset.TID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public Boolean DeleteTeamsAsset(TeamsAsset uTeamsAsset, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_RST_TeamsAsset where TID = " + uTeamsAsset.TID.ToString();
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            DataTable dtAudit = new AuditService().LogTran(3, "TeamsAsset", uTeamsAsset.TID.ToString(), Globals.uLogin.UID, uTeamsAsset.TID);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "CallerFunction";
            drCtrl["ParentTblName"] = "GFI_RST_TeamsAsset";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
    }
}
