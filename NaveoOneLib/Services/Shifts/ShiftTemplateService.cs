﻿using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NaveoOneLib.Models.Shifts;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Models.Common;

namespace NaveoOneLib.Services.Shifts
{
    public class ShiftTemplateService
    {
        Errors er = new Errors();

        public DataTable GetShiftTemplate(String sConnStr)
        {
            String sqlString = @"SELECT * from GFI_RST_ShiftTemplate order by iID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        public DataTable GetDistinctShiftTemplate(String sConnStr)
        {
            String sqlString = @"SELECT Distinct GroupID,Description from GFI_RST_ShiftTemplate order by GroupID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        Models.Shifts.ShiftTemplate GetShiftTemplates(DataSet ds)
        {
            DataRow[] drPrim = ds.Tables["dtShiftTemplate"].Select("iID = GroupID");
            DataRow[] drOthers = ds.Tables["dtShiftTemplate"].Select("iID <> GroupID");

            Models.Shifts.ShiftTemplate sT = new Models.Shifts.ShiftTemplate();
            sT.iID = Convert.ToInt32(drPrim[0]["iID"]);
            sT.GroupID = Convert.ToInt32(drPrim[0]["GroupID"]);
            sT.SID = Convert.ToInt32(drPrim[0]["SID"]);
            sT.OrderNo = Convert.ToInt32(drPrim[0]["OrderNo"]);
            sT.Description = drPrim[0]["Description"].ToString();

            List<Models.Shifts.ShiftTemplate> lr = new List<Models.Shifts.ShiftTemplate>();
            foreach (DataRow dr in drOthers)
            {
                Models.Shifts.ShiftTemplate sTo = new Models.Shifts.ShiftTemplate();
                sTo.iID = Convert.ToInt32(dr["iID"]);
                sTo.GroupID = Convert.ToInt32(dr["GroupID"]);
                sTo.SID = String.IsNullOrEmpty(dr["SID"].ToString()) ? (int?)null : Convert.ToInt32(dr["SID"]);
                sTo.OrderNo = Convert.ToInt32(dr["OrderNo"]);
                sTo.Description = dr["Description"].ToString();
                lr.Add(sTo);
            }
            sT.lShiftTemplates = lr;

            List<Matrix> lMatrix = new List<Matrix>();
            MatrixService ms = new MatrixService();
            foreach (DataRow dr in ds.Tables["dtMatrix"].Rows)
                lMatrix.Add(ms.AssignMatrix(dr));
            sT.lMatrix = lMatrix;

            return sT;
        }

        public Models.Shifts.ShiftTemplate GetShiftTemplateByGroupId(int iID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"SELECT * from GFI_RST_ShiftTemplate where GroupID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = iID.ToString();
            drSql["sTableName"] = "dtShiftTemplate";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables["dtShiftTemplate"].Rows.Count == 0)
                return null;
            else
                return GetShiftTemplates(ds);
        }


        public DataTable GetShiftTemplateWithShiftDetailsByGroupId(int iID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();



            String sqlString = @"select st.iid as shittemplateid ,st.description as ShiftTemplate ,s.sid as shiftid,s.sname as shiftname from GFI_RST_SHIFT s 
                           inner join GFI_RST_SHIFTTEMPLATE st  on s.sid = st.sid
                           where st.groupid = " + iID.ToString() + " order by orderno asc";

            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);


          
        }

        public BaseModel SaveShiftTemplateList(List<Models.Shifts.ShiftTemplate> LShiftTemplates, Boolean bInsert, int uLogin, String sConnStr)
        {
            BaseModel bm = new BaseModel();

            NaveoOneLib.Models.Shifts.ShiftTemplate uShiftTemplate = new NaveoOneLib.Models.Shifts.ShiftTemplate();
            DataTable dt = new DataTable();
            dt.TableName = "GFI_RST_ShiftTemplate";
            dt.Columns.Add("iID", typeof(int));
            dt.Columns.Add("GroupID", typeof(int));
            dt.Columns.Add("SID", typeof(int));
            dt.Columns.Add("OrderNo", typeof(int));
            dt.Columns.Add("Description", typeof(String));
            dt.Columns.Add("oprType", uShiftTemplate.oprType.GetType());

            foreach (Models.Shifts.ShiftTemplate sT in LShiftTemplates)
            {
                if (bInsert)
                    sT.oprType = DataRowState.Added;
                else
                    sT.GroupID = LShiftTemplates[0].GroupID;

                DataRow dr = dt.NewRow();
                dr["iID"] = sT.iID;
                dr["GroupID"] = sT.GroupID;
                dr["SID"] = sT.SID.HasValue ? sT.SID : (object)DBNull.Value;
                dr["OrderNo"] = sT.OrderNo;
                dr["Description"] = sT.Description;
                dr["oprType"] = sT.oprType;
                dt.Rows.Add(dr);
            }

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "iID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
            {
                drCtrl["NextIdAction"] = String.Empty; ;
                drCtrl["NextIdValue"] = LShiftTemplates[0].GroupID;
            }

            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 2;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "iID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "GroupID";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (dti.TableName == "CTRLTBL")
                    continue;
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;

            DataRow[] dRowId = null;

            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                {
                    dRowId = dtCtrl.Select();
                    bResult = true;
                }

            }
            else
                bResult = false;


            if (dRowId != null)
            {
                bm.data = dRowId;
                bm.additionalData = "TRUE";
            }
            else
            {
                bm.data = false;
                bm.additionalData = "FALSE";
            }


            return bm;
        }
    }
}
