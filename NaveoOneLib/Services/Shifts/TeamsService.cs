using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Shifts;
using NaveoOneLib.Services.Audits;

namespace NaveoOneLib.Services.Shifts
{
    class TeamsService
    {
        Errors er = new Errors();
        public DataTable GetTeams(String sConnStr)
        {
            String sqlString = @"SELECT TID, 
sName, 
sComments, 
GUID from GFI_RST_Teams order by TID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        Teams GetTeams(DataRow dr)
        {
            Teams retTeams = new Teams();
            retTeams.TID = (int)dr["TID"];
            retTeams.sName = dr["sName"].ToString();
            retTeams.sComments = dr["sComments"].ToString();
            retTeams.GUID = dr["GUID"].ToString();
            return retTeams;
        }

        public Teams GetTeamsById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT TID, 
sName, 
sComments, 
GUID from GFI_RST_Teams
WHERE TID = ?
order by TID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID,sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetTeams(dt.Rows[0]);
        }

        DataTable TeamsDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_RST_Teams";
            dt.Columns.Add("TID", typeof(int));
            dt.Columns.Add("sName", typeof(String));
            dt.Columns.Add("sComments", typeof(String));
            dt.Columns.Add("GUID", typeof(String));

            return dt;
        }
        public Boolean SaveTeams(Teams uTeams, Boolean bInsert, String sConnStr)
        {
            DataTable dt = TeamsDT();

            DataRow dr = dt.NewRow();
            dr["TID"] = uTeams.TID;
            dr["sName"] = uTeams.sName;
            dr["sComments"] = uTeams.sComments;
            dr["GUID"] = uTeams.GUID;
            dt.Rows.Add(dr);


            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "TID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uTeams.TID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public Boolean DeleteTeams(Teams uTeams, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_RST_Teams where TID = " + uTeams.TID.ToString();
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            DataTable dtAudit = new AuditService().LogTran(3, "Teams", uTeams.TID.ToString(), Globals.uLogin.UID, uTeams.TID);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "CallerFunction";
            drCtrl["ParentTblName"] = "GFI_RST_Teams";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
    }
}
