

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Shifts;

namespace NaveoOneLib.Services.Shifts
{
    public class TeamService
    {

        Errors er = new Errors();
        public DataTable GetTeam(String sConnStr)
        {
            String sqlString = @"SELECT iID, TeamCode, 
TeamSize, 
TeamLeader, 
MainRole, 
CreatedBy, 
CreatedDate, 
UpdatedBy, 
UpdatedDate from GFI_HRM_Team order by TeamCode";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public Team GetTeamById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT TeamCode, 
TeamSize, 
TeamLeader, 
MainRole, 
CreatedBy, 
CreatedDate, 
UpdatedBy, 
UpdatedDate from GFI_HRM_Team
WHERE TeamCode = ?
order by TeamCode";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                Team retTeam = new Team();
                retTeam.iID = (int)dt.Rows[0]["iID"];
                retTeam.TeamCode = dt.Rows[0]["TeamCode"].ToString();
                retTeam.TeamSize = dt.Rows[0]["TeamSize"].ToString();
                retTeam.TeamLeader = dt.Rows[0]["TeamLeader"].ToString();
                retTeam.MainRole = dt.Rows[0]["MainRole"].ToString();
                retTeam.CreatedBy = dt.Rows[0]["CreatedBy"].ToString();
                retTeam.CreatedDate = (DateTime)dt.Rows[0]["CreatedDate"];
                retTeam.UpdatedBy = dt.Rows[0]["UpdatedBy"].ToString();
                retTeam.UpdatedDate = (DateTime)dt.Rows[0]["UpdatedDate"];
                return retTeam;
            }
        }
        public bool SaveTeam(Team uTeam, Boolean bInsert, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_HRM_Team";
            dt.Columns.Add("iID", uTeam.iID.GetType());
            dt.Columns.Add("TeamCode", uTeam.TeamCode.GetType());
            dt.Columns.Add("TeamSize", uTeam.TeamSize.GetType());
            dt.Columns.Add("TeamLeader", uTeam.TeamLeader.GetType());
            dt.Columns.Add("MainRole", uTeam.MainRole.GetType());
            dt.Columns.Add("CreatedBy", uTeam.CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", uTeam.CreatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uTeam.UpdatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uTeam.UpdatedDate.GetType());
            DataRow dr = dt.NewRow();
            dr["iID"] = uTeam.iID;
            dr["TeamCode"] = uTeam.TeamCode;
            dr["TeamSize"] = uTeam.TeamSize;
            dr["TeamLeader"] = uTeam.TeamLeader;
            dr["MainRole"] = uTeam.MainRole;
            dr["CreatedBy"] = uTeam.CreatedBy;
            dr["CreatedDate"] = uTeam.CreatedDate;
            dr["UpdatedBy"] = uTeam.UpdatedBy;
            dr["UpdatedDate"] = uTeam.UpdatedDate;
            if (!dt.Columns.Contains("oprType"))
                dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "iID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "SET";
            else
                drCtrl["NextIdAction"] = "GET";
            dtCtrl.Rows.Add(drCtrl);

            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);
            dsProcess.Merge(dtCtrl);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public bool UpdateTeam(Team uTeam, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_HRM_Team";
            dt.Columns.Add("TeamCode", uTeam.TeamCode.GetType());
            dt.Columns.Add("TeamSize", uTeam.TeamSize.GetType());
            dt.Columns.Add("TeamLeader", uTeam.TeamLeader.GetType());
            dt.Columns.Add("MainRole", uTeam.MainRole.GetType());
            dt.Columns.Add("CreatedBy", uTeam.CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", uTeam.CreatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uTeam.UpdatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uTeam.UpdatedDate.GetType());
            DataRow dr = dt.NewRow();
            dr["TeamCode"] = uTeam.TeamCode;
            dr["TeamSize"] = uTeam.TeamSize;
            dr["TeamLeader"] = uTeam.TeamLeader;
            dr["MainRole"] = uTeam.MainRole;
            dr["CreatedBy"] = uTeam.CreatedBy;
            dr["CreatedDate"] = uTeam.CreatedDate;
            dr["UpdatedBy"] = uTeam.UpdatedBy;
            dr["UpdatedDate"] = uTeam.UpdatedDate;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "TeamCode = '" + uTeam.TeamCode + "'", transaction,sConnStr))
                return true;
            else
                return false;
        }
        public bool DeleteTeam(Team uTeam, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_HRM_Team", "TeamCode = '" + uTeam.TeamCode + "'",  sConnStr))
                return true;
            else
                return false;
        }

    }
}



