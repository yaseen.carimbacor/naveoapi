using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Shifts;
using NaveoOneLib.Services.Audits;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Services.GMatrix;

namespace NaveoOneLib.Services.Shifts
{
    public class ShiftService
    {
        Errors er = new Errors();

        public String sGetShift(List<Matrix> lMatrix, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            return sGetShift(iParentIDs, sConnStr);
        }

        public String sGetShift(List<int> iParentIDs, String sConnStr)
        {
            String sql = @"SELECT 
                        Distinct
                        s.SID, 
                        s.sName, 
                        s.sComments, 
                        s.TimeFr, 
                        s.TimeTo
                        from GFI_RST_Shift s, GFI_SYS_GroupMatrixShift m
                        where  s.SID = m.iID and m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + ") order by SID";

            return sql;
        }
        public DataTable GetShifts(List<Matrix> lMatrix, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            return GetShifts(iParentIDs, sConnStr);
        }
        public DataTable GetShifts(List<int> iParentIDs, String sConnStr)
        {
            DataTable dt = new Connection(sConnStr).GetDataDT(sGetShift(iParentIDs, sConnStr), sConnStr);
            return dt;
        }

        Shift GetShift(DataSet ds)
        {
            Shift retShift = new Shift();

            DataTable dtRules = ds.Tables["dtShift"];
        
            foreach (DataRow dr in dtRules.Rows)
            {
                retShift.SID = (int)dr["SID"];
                retShift.sName = dr["sName"].ToString();
                retShift.sComments = dr["sComments"].ToString();
                retShift.TimeFr = (DateTime)dr["TimeFr"];
                retShift.TimeTo = (DateTime)dr["TimeTo"];
            }

            if (ds.Tables.Contains("dtMatrix"))
            {
                List<Matrix> lMatrix = new List<Matrix>();
                MatrixService ms = new MatrixService();
                foreach (DataRow dr in ds.Tables["dtMatrix"].Rows)
                    lMatrix.Add(ms.AssignMatrix(dr));
                retShift.lMatrix = lMatrix;
            }

            return retShift;
        }
        public Shift GetShiftById(int iID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"SELECT SID, 
                                        sName, 
                                        sComments, 
                                        TimeFr, 
                                        TimeTo from GFI_RST_Shift
                                        WHERE SID = ?
                                        order by SID";


            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = iID.ToString();
            drSql["sTableName"] = "dtShift";
            dtSql.Rows.Add(drSql);

            sql = new MatrixService().sGetMatrixId(Convert.ToInt32(iID), "GFI_SYS_GroupMatrixShift");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);


            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables["dtShift"].Rows.Count == 0)
                return null;
            else
                return GetShift(ds);
        }
        public Shift GetShiftByName(String sName, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"SELECT SID, 
                                 sName, 
                                 sComments, 
                                 TimeFr, 
                                 TimeTo from GFI_RST_Shift
                                 WHERE sName = ?";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = sName.ToString();
            drSql["sTableName"] = "dtShift";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables["dtShift"].Rows.Count == 0)
                return null;
            else
                return GetShift(ds);
        }

        DataTable ShiftDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_RST_Shift";
            dt.Columns.Add("SID", typeof(int));
            dt.Columns.Add("sName", typeof(String));
            dt.Columns.Add("sComments", typeof(String));
            dt.Columns.Add("TimeFr", typeof(DateTime));
            dt.Columns.Add("TimeTo", typeof(DateTime));

            return dt;
        }
        public Boolean SaveShift(Shift uShift, Boolean bInsert,String sConnStr)
        {
            DataTable dt = ShiftDT();

            DataRow dr = dt.NewRow();
            dr["SID"] = uShift.SID;
            dr["sName"] = uShift.sName;
            dr["sComments"] = uShift.sComments;
            dr["TimeFr"] = uShift.TimeFr != null ? uShift.TimeFr : (object)DBNull.Value;
            dr["TimeTo"] = uShift.TimeTo != null ? uShift.TimeTo : (object)DBNull.Value;
            dt.Rows.Add(dr);


            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "SID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uShift.SID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

         
            DataTable dtMatrix = new myConverter().ListToDataTable<Matrix>(uShift.lMatrix);
            dtMatrix.TableName = "GFI_SYS_GroupMatrixShift";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 2;
            drCtrl["TblName"] = dtMatrix.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "MID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "iID";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtMatrix);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public Boolean DeleteShift(Guid sUserToken, Shift uShift, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            int UID = new NaveoOneLib.Services.Users.UserService().GetUserID(sUserToken, sConnStr);

            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_SYS_GroupMatrixShift where iID = " + uShift.SID.ToString();
            sql += ";\r\n";
            sql += "delete from GFI_RST_Shift where SID = " + uShift.SID.ToString();
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            DataTable dtAudit = new AuditService().LogTran(3, "Shift", uShift.SID.ToString(), UID, uShift.SID);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "CallerFunction";
            drCtrl["ParentTblName"] = "GFI_RST_Shift";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
    }
}
