using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Shifts;
using NaveoOneLib.Services.Audits;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Models.GMatrix;

namespace NaveoOneLib.Services.Shifts
{
    public class RosterService
    {
        Errors er = new Errors();


        public String sGetRoster(DateTime dtFrom, DateTime dtTo, List<int> iParentIDs, String sConnStr, List<String> sortColumns, Boolean sortOrderAsc)
        {
            #region sort
            String sSort = "RosterDate";
            if (sortColumns != null && sortColumns.Count > 0)
            {
                List<String> lCols = new List<string>() { "RosterDate", "ResourceName", "Asset" };
                String ss = String.Join(",", sortColumns
                    .Where(s => !String.IsNullOrWhiteSpace(s))
                    .Where(s => lCols.Contains(s))
                    .Select(s => s));

                if (!String.IsNullOrEmpty(ss))
                    sSort = ss;
            }

            if (sortOrderAsc)
                sSort += " asc";
            else
                sSort += " desc";
            #endregion

            String sql = @"
--ListRoster

SELECT Distinct r.GRID, 
                                           
				                           r.SID,
				                           s.sName,
				                           r.rDate as RosterDate,
                                           rh.RosterName,
				                           rR.DriverID as ResourceID,
	                                       d.sDriverName as ResourceName,
                                           d.EmployeeType as ResourceType,
				                           rA.AssetID,
				                           a.AssetName as Asset,
                                           concat(convert(varchar, rh.dtFrom, 103), '-', convert(varchar, rh.dtTo, 103)) as RosterPeriod,
                                           convert(varchar, rh.dtFrom, 103) as dtFrom,
                                           convert(varchar, rh.dtTo, 103) as dtTo,
										   rH.sRemarks
                                           from GFI_RST_Roster r
										   inner join GFI_RST_RosterHeader rH on r.GRID = rh.iID
	                                       inner join GFI_SYS_GroupMatrixRoster m on m.iID = rH.iID
		                                   left join GFI_RST_RosterResources rR on rR.RID = rH.iID
										   left join GFI_FLT_DRIVER d on d.DriverID = rR.DriverID
				                           left join GFI_RST_RosterAssets rA on rA.RID = rH.iID
										   left join GFI_FLT_Asset a on a.AssetID = rA.AssetID
				                           inner join GFI_RST_Shift s on s.SID = r.SID    
                                           where m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + @")
                                           and r.rDate >= '" + dtFrom.ToString("yyyy-MM-dd HH:mm:ss.fff") + "' and  r.rDate  <='" + dtTo.ToString("yyyy-MM-dd HH:mm:ss.fff") + @"' 
                                           order by " + sSort + @"";

            return sql;
        }

        public DataTable GetRoster(DateTime dtFrom, DateTime dtTo, List<Matrix> lMatrix, String sConnStr, List<String> sortColumns, Boolean sortOrderAsc)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            return GetRoster(dtFrom, dtTo, iParentIDs, sConnStr, sortColumns, sortOrderAsc);
        }

        public DataTable GetRoster(DateTime dtFrom, DateTime dtTo, List<int> iParentIDs, String sConnStr, List<String> sortColumns, Boolean sortOrderAsc)
        {
            DataTable dt = new Connection(sConnStr).GetDataDT(sGetRoster(dtFrom, dtTo, iParentIDs, sConnStr, sortColumns, sortOrderAsc), sConnStr);
            return dt;
        }

        public DataTable GetRoster(String sConnStr)
        {
            String sqlString = @"SELECT RID, 
GRID, 
SID, 
TeamID, 
rDate, 
isLocked, 
isOriginal from GFI_RST_Roster order by RID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        Roster GetRoster(DataSet ds)
        {

            DataRow dr = ds.Tables["GFI_RST_RosterHeader"].Rows[0];
            Roster retRoster = new Roster();
            retRoster.iID = (int)dr["IID"];
            retRoster.RosterName = dr["RosterName"].ToString();
            retRoster.dtFrom = (DateTime)dr["dtFrom"];
            retRoster.dtTo = (DateTime)dr["dtTo"];
            retRoster.sRemarks = dr["sRemarks"].ToString();
            retRoster.CreatedBy = String.IsNullOrEmpty(dr["CreatedBy"].ToString()) ? (int?)null : Convert.ToInt32(dr["CreatedBy"]);
            retRoster.CreatedDate = String.IsNullOrEmpty(dr["CreatedDate"].ToString()) ? (DateTime?)null : (DateTime)dr["CreatedDate"];
            retRoster.UpdatedBy = String.IsNullOrEmpty(dr["UpdatedBy"].ToString()) ? (int?)null : Convert.ToInt32(dr["UpdatedBy"]);
            retRoster.UpdatedDate = String.IsNullOrEmpty(dr["UpdatedDate"].ToString()) ? (DateTime?)null : (DateTime)dr["UpdatedDate"];
            retRoster.isLocked = String.IsNullOrEmpty(dr["isLocked"].ToString()) ? (int?)null : (int)dr["isLocked"];
            retRoster.isOriginal = String.IsNullOrEmpty(dr["isOriginal"].ToString()) ? (int?)null : (int)dr["isOriginal"];


            List<Matrix> lMatrix = new List<Matrix>();
            if (ds.Tables.Contains("dtMatrix"))
            {
                MatrixService ms = new MatrixService();
                foreach (DataRow dri in ds.Tables["dtMatrix"].Rows)
                    lMatrix.Add(ms.AssignMatrix(dri));
            }
            retRoster.lMatrix = lMatrix;

            List<RosterDetails> lrosterDetails = new List<RosterDetails>();
            if (ds.Tables.Contains("GFI_RST_Roster"))
            {

                foreach (DataRow dri in ds.Tables["GFI_RST_Roster"].Rows)
                {
                    RosterDetails rD = new RosterDetails();
                    rD.RID = (int)dri["RID"];
                    rD.GRID = (int)dri["GRID"];
                    rD.SID = (int)dri["SID"];
                    rD.Shiftdesc = dri["shiftName"].ToString();
                    rD.rDate = (DateTime)dri["rDate"];
                    rD.TeamID = String.IsNullOrEmpty(dri["TeamID"].ToString()) ? 0 : Convert.ToInt32(dri["TeamID"]);
                    rD.ShiftTemplateDesc = dri["shitTemplate"].ToString();
                    rD.ShiftTemplateId = String.IsNullOrEmpty(dri["shifttemplateid"].ToString()) ? 0 : Convert.ToInt32(dri["shifttemplateid"]);

                    lrosterDetails.Add(rD);

                }
            }

            retRoster.lRosterDetails = lrosterDetails;
            if (ds.Tables.Contains("GFI_RST_RosterAssets"))
            {
                DataRow drAssets = ds.Tables["GFI_RST_RosterAssets"].Rows[0];
                RosterAssets rA = new RosterAssets();
                rA.iID = (int)drAssets["iID"];
                rA.RID = (int)drAssets["RID"];
                rA.AssetID = (int)drAssets["AssetID"];
                rA.AssetName = drAssets["AssetName"].ToString();

                retRoster.rosterAsset = rA;

            }


            List<RosterResources> lRosterResources = new List<RosterResources>();
            if (ds.Tables.Contains("GFI_RST_RosterResources"))
            {
                foreach (DataRow dri in ds.Tables["GFI_RST_RosterResources"].Rows)
                {
                    RosterResources rA = new RosterResources();
                    rA.iID = (int)dri["iID"];
                    rA.RID = (int)dri["RID"];
                    rA.DriverID = (int)dri["DriverID"];
                    rA.DriverName = dri["sDriverName"].ToString();

                    lRosterResources.Add(rA);
                }
            }

            retRoster.lResources = lRosterResources;



            return retRoster;
        }
        public Roster GetRosterById(String iID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"SELECT * from GFI_RST_RosterHeader WHERE iID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = iID;
            drSql["sTableName"] = "GFI_RST_RosterHeader";
            dtSql.Rows.Add(drSql);

            sql = @"SELECT r.*,s.sName as shiftName,sT.description as shitTemplate ,st.groupid as shifttemplateid from GFI_RST_Roster r  
                   inner join GFI_RST_Shift s on s.SID = r.SID  
                   left join gfi_rst_shifttemplate  st on  r.grid = st.groupid  
                   WHERE GRID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = iID;
            drSql["sTableName"] = "GFI_RST_Roster";
            dtSql.Rows.Add(drSql);

            sql = @"SELECT rA.iID,rA.RID,a.AssetName as AssetName,a.AssetID from GFI_RST_RosterAssets rA  inner join GFI_FLT_Asset a on rA.AssetID = a.AssetID where rA.RID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = iID;
            drSql["sTableName"] = "GFI_RST_RosterAssets";
            dtSql.Rows.Add(drSql);


            sql = @"SELECT rR.iID,rR.RID,d.sDriverName,d.DriverID from GFI_RST_RosterResources rR  inner join GFI_FLT_Driver d on rR.DriverID = d.DriverID where rR.RID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = iID;
            drSql["sTableName"] = "GFI_RST_RosterResources";
            dtSql.Rows.Add(drSql);

            sql = new MatrixService().sGetMatrixId(Convert.ToInt32(iID), "GFI_SYS_GroupMatrixRoster");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables["GFI_RST_RosterHeader"].Rows.Count == 0)
                return null;
            else
                return GetRoster(ds);


        }
        DataTable RosterDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_RST_RosterHeader";
            dt.Columns.Add("iID", typeof(int));
            dt.Columns.Add("RosterName", typeof(string));
            dt.Columns.Add("dtFrom", typeof(DateTime));
            dt.Columns.Add("dtTo", typeof(DateTime));
            dt.Columns.Add("sRemarks", typeof(string));
            dt.Columns.Add("isLocked", typeof(int));
            dt.Columns.Add("isOriginal", typeof(int));
            dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Columns.Add("CreatedDate", typeof(DateTime));
            dt.Columns.Add("CreatedBy", typeof(int));
            dt.Columns.Add("UpdatedDate", typeof(DateTime));
            dt.Columns.Add("UpdatedBy", typeof(int));

            return dt;
        }
        public Boolean SaveRoster(Roster uRoster, int UID, Boolean bInsert, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            DataSet dsProcess = new DataSet();

            #region RosterHeader
            DataTable dt = RosterDT();

            DataRow dr = dt.NewRow();
            dr["iID"] = (int)uRoster.iID;
            dr["RosterName"] = uRoster.RosterName != null ? uRoster.RosterName.ToString() : string.Empty;
            dr["dtFrom"] = uRoster.dtFrom != null ? uRoster.dtFrom : (object)DBNull.Value;
            dr["dtTo"] = uRoster.dtTo != null ? uRoster.dtTo : (object)DBNull.Value;
            dr["sRemarks"] = uRoster.sRemarks;
            dr["isLocked"] = uRoster.isLocked != null ? (int)uRoster.isLocked : 0;
            dr["isOriginal"] = uRoster.isOriginal != null ? (int)uRoster.isOriginal : 0;
            dr["oprType"] = uRoster.oprType;

            if (bInsert)
            {
                dr["CreatedDate"] = DateTime.UtcNow;
                dr["CreatedBy"] = UID;
            }
            else
            {
                dr["UpdatedDate"] = DateTime.UtcNow;
                dr["UpdatedBy"] = UID;
            }

            dt.Rows.Add(dr);

            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "iID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uRoster.iID;
            dtCtrl.Rows.Add(drCtrl);

            dsProcess.Merge(dt);

            #endregion


            #region RosterDetails
            DataTable dtRoster = new DataTable();
            dtRoster.TableName = "GFI_RST_Roster";
            dtRoster.Columns.Add("RID");
            dtRoster.Columns.Add("GRID");
            dtRoster.Columns.Add("SID");
            //dtRoster.Columns.Add("TeamID");
            dtRoster.Columns.Add("rDate", typeof(DateTime));
            // dtRoster.Columns.Add("OPRSeqNo");
            dtRoster.Columns.Add("oprType", typeof(DataRowState));

            foreach (var rosterDetails in uRoster.lRosterDetails)
            {
                if (bInsert)
                    rosterDetails.oprType = DataRowState.Added;
                else
                    rosterDetails.GRID = rosterDetails.GRID;


                if (rosterDetails.SID == 0)
                    rosterDetails.oprType = 0;


                DataRow drRosterDetails = dtRoster.NewRow();
                if (!bInsert)
                {
                    drRosterDetails["RID"] = rosterDetails.RID;
                }
                
                drRosterDetails["GRID"] = rosterDetails.GRID;
                drRosterDetails["SID"] = rosterDetails.SID;
                //drRosterDetails["TeamID"] = 0;
                drRosterDetails["rDate"] = rosterDetails.rDate;
                drRosterDetails["oprType"] = rosterDetails.oprType;
                dtRoster.Rows.Add(drRosterDetails);
            }

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 2;
            drCtrl["TblName"] = dtRoster.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "RID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "GRID";
             drCtrl["ParentTblName"] = "GFI_RST_RosterHeader";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtRoster);
            #endregion
            /*
            #region MatrixHeader
            DataTable dtMatrixHeader = new myConverter().ListToDataTable<Matrix>(uRoster.lMatrix);
            dtMatrixHeader.TableName = "GFI_SYS_GroupMatrixRoster";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 3;
            drCtrl["TblName"] = dtMatrixHeader.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "MID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uRoster.iID;
            drCtrl["NextIdFieldName"] = "iID";
            //drCtrl["ParentTblName"] = dt.TableName;
            drCtrl["ParentTblName"] = "GFI_RST_RosterHeader";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtMatrixHeader);
            #endregion*/

            #region MatrixHeader
            DataTable dtMatrix = new myConverter().ListToDataTable<Matrix>(uRoster.lMatrix);
            dtMatrix.TableName = "GFI_SYS_GroupMatrixRoster";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] =3 ;
            drCtrl["TblName"] = dtMatrix.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "MID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "SET";
            else
                drCtrl["NextIdValue"] = uRoster.iID;
            drCtrl["NextIdFieldName"] = "iID";
            //drCtrl["ParentTblName"] = dt.TableName;
            drCtrl["ParentTblName"] = "GFI_RST_RosterHeader";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtMatrix);
            #endregion

            #region resources
            if (uRoster.lResources != null)
                if (uRoster.lResources.Count > 0)
                {
                    DataTable dtResource = new myConverter().ListToDataTable<RosterResources>(uRoster.lResources);
                    dtResource.Columns.Remove("DriverName");
                    dtResource.TableName = "GFI_RST_RosterResources";
                    foreach (DataRow row in dtResource.Rows)
                        row["RID"]= uRoster.iID;

                    drCtrl = dtCtrl.NewRow();
                    drCtrl["SeqNo"] = 3;
                    drCtrl["TblName"] = dtResource.TableName;
                    drCtrl["CmdType"] = "TABLE";
                    drCtrl["KeyFieldName"] = "iID";
                    drCtrl["KeyIsIdentity"] = "TRUE";
                    drCtrl["NextIdAction"] = "SET";
                    drCtrl["NextIdFieldName"] = "RID";
                    drCtrl["ParentTblName"] = "GFI_RST_RosterHeader";
                    dtCtrl.Rows.Add(drCtrl);
                    dsProcess.Merge(dtResource);
                }

            #endregion

            #region Assets
            DataTable dtAssets = new DataTable();
            dtAssets.TableName = "GFI_RST_RosterAssets";
            dtAssets.Columns.Add("iID");
            dtAssets.Columns.Add("RID");
            dtAssets.Columns.Add("AssetID");
            dtAssets.Columns.Add("oprType", typeof(DataRowState));

            DataRow drAssets = dtAssets.NewRow();
            drAssets["iID"] = uRoster.rosterAsset.iID;
            drAssets["RID"] = uRoster.rosterAsset.RID;
            drAssets["AssetID"] = uRoster.rosterAsset.AssetID;
            drAssets["oprType"] = uRoster.rosterAsset.oprType;
            dtAssets.Rows.Add(drAssets);

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 4;
            drCtrl["TblName"] = dtAssets.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "iID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "RID";
            drCtrl["ParentTblName"] = "GFI_RST_RosterHeader";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAssets);

            #endregion

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }

        //public Boolean SaveRoster(Roster uRoster, Boolean bInsert, String sConnStr)
        //{
        //    DataTable dt = RosterDT();

        //    DataRow dr = dt.NewRow();
        //    dr["RID"] = uRoster.RID;
        //    dr["GRID"] = uRoster.GRID;
        //    dr["SID"] = uRoster.SID;
        //    dr["TeamID"] = uRoster.TeamID;
        //    dr["rDate"] = uRoster.rDate != null ? uRoster.rDate : (object)DBNull.Value;
        //    dr["isLocked"] = uRoster.isLocked != null ? uRoster.isLocked : (object)DBNull.Value;
        //    dr["isOriginal"] = uRoster.isOriginal != null ? uRoster.isOriginal : (object)DBNull.Value;
        //    dt.Rows.Add(dr);

        //    Connection c = new Connection(sConnStr);
        //    DataTable dtCtrl = c.CTRLTBL();
        //    DataRow drCtrl = dtCtrl.NewRow();
        //    drCtrl["SeqNo"] = 1;
        //    drCtrl["TblName"] = dt.TableName;
        //    drCtrl["CmdType"] = "TABLE";
        //    drCtrl["KeyFieldName"] = "RID";
        //    drCtrl["KeyIsIdentity"] = "TRUE";
        //    if (bInsert)
        //        drCtrl["NextIdAction"] = "GET";
        //    else
        //        drCtrl["NextIdValue"] = uRoster.RID;
        //    dtCtrl.Rows.Add(drCtrl);
        //    DataSet dsProcess = new DataSet();
        //    dsProcess.Merge(dt);

        //    ////RosterResource
        //    //DataTable dtRosterResource = new myConverter().ListToDataTable<RosterResource>(uRoster.lResources);
        //    //dtRosterResource.TableName = "GFI_RST_RosterResource";
        //    //drCtrl = dtCtrl.NewRow();
        //    //drCtrl["SeqNo"] = 2;
        //    //drCtrl["TblName"] = dtRosterResource.TableName;
        //    //drCtrl["CmdType"] = "TABLE";
        //    //drCtrl["KeyFieldName"] = "iID";
        //    //drCtrl["KeyIsIdentity"] = "TRUE";
        //    //drCtrl["NextIdAction"] = "SET";
        //    //drCtrl["NextIdFieldName"] = "RID";
        //    //drCtrl["ParentTblName"] = dt.TableName;
        //    //dtCtrl.Rows.Add(drCtrl);
        //    //dsProcess.Merge(dtRosterResource);

        //    ////RosterAsset
        //    //DataTable dtRosterAsset = new myConverter().ListToDataTable<RosterAsset>(uRoster.lAssets);
        //    //dtRosterAsset.TableName = "GFI_RST_RosterAsset";
        //    //drCtrl = dtCtrl.NewRow();
        //    //drCtrl["SeqNo"] = 3;
        //    //drCtrl["TblName"] = dtRosterAsset.TableName;
        //    //drCtrl["CmdType"] = "TABLE";
        //    //drCtrl["KeyFieldName"] = "iID";
        //    //drCtrl["KeyIsIdentity"] = "TRUE";
        //    //drCtrl["NextIdAction"] = "SET";
        //    //drCtrl["NextIdFieldName"] = "RID";
        //    //drCtrl["ParentTblName"] = dt.TableName;
        //    //dtCtrl.Rows.Add(drCtrl);
        //    //dsProcess.Merge(dtRosterAsset);

        //    foreach (DataTable dti in dsProcess.Tables)
        //    {
        //        if (!dti.Columns.Contains("oprType"))
        //            dti.Columns.Add("oprType", typeof(DataRowState));

        //        foreach (DataRow dri in dti.Rows)
        //            if (dri["oprType"].ToString().Trim().Length == 0)
        //            {
        //                if (bInsert)
        //                    dri["oprType"] = DataRowState.Added;
        //                else
        //                    dri["oprType"] = DataRowState.Modified;
        //            }
        //    }

        //    dsProcess.Merge(dtCtrl);
        //    dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

        //    Boolean bResult = false;
        //    if (dtCtrl != null)
        //    {
        //        DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

        //        if (dRow.Length > 0)
        //            bResult = false;
        //        else
        //            bResult = true;
        //    }
        //    else
        //        bResult = false;

        //    return bResult;
        //}
        //public Boolean DeleteRoster(Roster uRoster, String sConnStr)
        //{
        //    Connection c = new Connection(sConnStr);
        //    DataTable dtCtrl = c.CTRLTBL();
        //    DataRow drCtrl = dtCtrl.NewRow();

        //    drCtrl = dtCtrl.NewRow();
        //    drCtrl["SeqNo"] = 1;
        //    drCtrl["TblName"] = "None";
        //    drCtrl["CmdType"] = "STOREDPROC";
        //    drCtrl["TimeOut"] = 1000;
        //    String sql = "delete from GFI_RST_Roster where RID = " + uRoster.RID.ToString();
        //    drCtrl["Command"] = sql;
        //    dtCtrl.Rows.Add(drCtrl);
        //    DataSet dsProcess = new DataSet();

        //    DataTable dtAudit = new AuditService().LogTran(3, "Roster", uRoster.RID.ToString(), Globals.uLogin.UID, uRoster.RID);
        //    drCtrl = dtCtrl.NewRow();
        //    drCtrl["SeqNo"] = 100;
        //    drCtrl["TblName"] = dtAudit.TableName;
        //    drCtrl["CmdType"] = "TABLE";
        //    drCtrl["KeyFieldName"] = "AuditID";
        //    drCtrl["KeyIsIdentity"] = "TRUE";
        //    drCtrl["NextIdAction"] = "SET";
        //    drCtrl["NextIdFieldName"] = "CallerFunction";
        //    drCtrl["ParentTblName"] = "GFI_RST_Roster";
        //    dtCtrl.Rows.Add(drCtrl);
        //    dsProcess.Merge(dtAudit);

        //    dsProcess.Merge(dtCtrl);
        //    DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

        //    dtCtrl = dsResult.Tables["CTRLTBL"];
        //    Boolean bResult = false;
        //    if (dtCtrl != null)
        //    {
        //        DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

        //        if (dRow.Length > 0)
        //            bResult = false;
        //        else
        //            bResult = true;
        //    }
        //    else
        //        bResult = false;

        //    return bResult;
        //}
    }
}
