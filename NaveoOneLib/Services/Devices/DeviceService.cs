﻿using NaveoOneLib.Models.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoOneLib.Services.Devices
{
    public class DeviceService
    {
        public dtData getDevices()
        {
            DataTable dt = new DataTable("Devices");
            dt.Columns.Add("Models");
            dt.Rows.Add("ATRACK");
            dt.Rows.Add("AT100");
            dt.Rows.Add("ORION");
            dt.Rows.Add("MVT100");
            dt.Rows.Add("MVT380");

            dtData dtdata = new dtData();
            dtdata.Data = dt;

            return dtdata;
        }

        public dtData SendCmds(String DeviceModel, String DeviceUnit, String DeviceCmd)
        {
            dtData dtdata = new dtData();
            return dtdata;
        }
    }
}
