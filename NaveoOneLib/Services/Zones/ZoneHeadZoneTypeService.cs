using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Zones;
using NaveoOneLib.Services.GMatrix;

namespace NaveoOneLib.Services.Zones
{
    public class ZoneHeadZoneTypeService
    {

        Errors er = new Errors();

        public String sGetZHZTs(List<int> iParentIDs, String sConnStr)
        {
            String sql = @"SELECT z.ZoneID from GFI_FLT_ZoneHeader z, GFI_SYS_GroupMatrixZone m
                                where z.ZoneID = m.iID and m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + ")";

            sql = "select * from GFI_FLT_ZoneHeadZoneType where ZoneHeadID in (" + sql + ")";
            return sql;
        }


        public DataTable GetZoneHeadZoneType(String sConnStr)
        {
            String sqlString = @"SELECT ZoneHeadID, 
                                    ZoneTypeID, 
                                    iID from GFI_FLT_ZoneHeadZoneType order by iID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public ZoneHeadZoneType GetZoneHeadZoneTypeById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT ZoneHeadID, 
                                    ZoneTypeID, 
                                    iID from GFI_FLT_ZoneHeadZoneType
                                    WHERE iID = ?
                                    order by iID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                ZoneHeadZoneType retZoneHeadZoneType = new ZoneHeadZoneType();
                retZoneHeadZoneType.ZoneHeadID = Convert.ToInt32(dt.Rows[0]["ZoneHeadID"]);
                retZoneHeadZoneType.ZoneTypeID = Convert.ToInt32(dt.Rows[0]["ZoneTypeID"]);
                retZoneHeadZoneType.iID = Convert.ToInt32(dt.Rows[0]["iID"]);
                return retZoneHeadZoneType;
            }
        }
        public bool SaveZoneHeadZoneType(ZoneHeadZoneType uZoneHeadZoneType, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_ZoneHeadZoneType";
            dt.Columns.Add("ZoneHeadID", uZoneHeadZoneType.ZoneHeadID.GetType());
            dt.Columns.Add("ZoneTypeID", uZoneHeadZoneType.ZoneTypeID.GetType());
            //dt.Columns.Add("iID", uZoneHeadZoneType.iID.GetType());
            DataRow dr = dt.NewRow();
            dr["ZoneHeadID"] = uZoneHeadZoneType.ZoneHeadID;
            dr["ZoneTypeID"] = uZoneHeadZoneType.ZoneTypeID;
            //dr["iID"] = uZoneHeadZoneType.iID;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr);
            String sql = "select * from GFI_FLT_ZoneHeadZoneType where ZoneHeadID = " + uZoneHeadZoneType.ZoneHeadID.ToString() 
                + " and ZoneTypeID = " + uZoneHeadZoneType.ZoneTypeID.ToString();
            if (_connection.GetDataDT(sql, transaction, sConnStr).Rows.Count > 0)
                return true;
            if (_connection.GenInsert(dt, transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool UpdateZoneHeadZoneType(ZoneHeadZoneType uZoneHeadZoneType, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_ZoneHeadZoneType";
            dt.Columns.Add("ZoneHeadID", uZoneHeadZoneType.ZoneHeadID.GetType());
            dt.Columns.Add("ZoneTypeID", uZoneHeadZoneType.ZoneTypeID.GetType());
            dt.Columns.Add("iID", uZoneHeadZoneType.iID.GetType());
            DataRow dr = dt.NewRow();
            dr["ZoneHeadID"] = uZoneHeadZoneType.ZoneHeadID;
            dr["ZoneTypeID"] = uZoneHeadZoneType.ZoneTypeID;
            dr["iID"] = uZoneHeadZoneType.iID;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "iID = '" + uZoneHeadZoneType.iID + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool DeleteZoneHeadZoneType(ZoneHeadZoneType uZoneHeadZoneType, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_FLT_ZoneHeadZoneType", "iID = '" + uZoneHeadZoneType.iID + "'", sConnStr))
                return true;
            else
                return false;
        }
    }
}