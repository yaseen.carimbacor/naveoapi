using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Zones;

namespace NaveoOneLib.Services.Zones
{
     public class ExtZonePropsService
     {

          Errors er = new Errors();
          public DataTable GetExtZoneProps(String sConnStr)
          {
               String sqlString = @"SELECT Uid, 
                                             ZoneId, 
                                             ZoneType, 
                                             ApplicationNum, 
                                             ApplicationType, 
                                             ApplicationDate, 
                                             ONames, 
                                             Surname, 
                                             AddressL1, 
                                             TVNumber, 
                                             LinkPDF, 
                                             LinkIMG from GFI_AMM_ExtZoneProps order by Uid";
               return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
          }
          public ExtZoneProps GetExtZonePropsById(String iID, String sConnStr)
          {
               String sqlString = @"SELECT Uid, 
                                             ZoneId, 
                                             ZoneType, 
                                             ApplicationNum, 
                                             ApplicationType, 
                                             ApplicationDate, 
                                             ONames, 
                                             Surname, 
                                             AddressL1, 
                                             TVNumber, 
                                             LinkPDF, 
                                             LinkIMG from GFI_AMM_ExtZoneProps
                                             WHERE Uid = ?
                                             order by Uid";
               DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
               if (dt.Rows.Count == 0)
                    return null;
               else
               {
                    ExtZoneProps retExtZoneProps = new ExtZoneProps();
                    retExtZoneProps.Uid = Convert.ToInt32(dt.Rows[0]["Uid"]);
                    retExtZoneProps.ZoneId = Convert.ToInt32(dt.Rows[0]["ZoneId"]);
                    retExtZoneProps.ZoneType = dt.Rows[0]["ZoneType"].ToString();
                    retExtZoneProps.ApplicationNum = dt.Rows[0]["ApplicationNum"].ToString();
                    retExtZoneProps.ApplicationType = dt.Rows[0]["ApplicationType"].ToString();
                    retExtZoneProps.ApplicationDate = (DateTime)dt.Rows[0]["ApplicationDate"];
                    retExtZoneProps.ONames = dt.Rows[0]["ONames"].ToString();
                    retExtZoneProps.Surname = dt.Rows[0]["Surname"].ToString();
                    retExtZoneProps.AddressL1 = dt.Rows[0]["AddressL1"].ToString();
                    retExtZoneProps.TVNumber = dt.Rows[0]["TVNumber"].ToString();
                    retExtZoneProps.LinkPDF = dt.Rows[0]["LinkPDF"].ToString();
                    retExtZoneProps.LinkIMG = dt.Rows[0]["LinkIMG"].ToString();
                    return retExtZoneProps;
               }
          }

          public ExtZoneProps GetExtZonePropsByZoneId(String iID, String sConnStr)
          {
               String sqlString = @"SELECT Uid, 
                                             ZoneId, 
                                             ZoneType, 
                                             ApplicationNum, 
                                             ApplicationType, 
                                             ApplicationDate, 
                                             ONames, 
                                             Surname, 
                                             AddressL1, 
                                             TVNumber, 
                                             LinkPDF, 
                                             LinkIMG from GFI_AMM_ExtZoneProps
                                             WHERE ZoneId = ?
                                             order by ZoneId";
               DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
               if (dt.Rows.Count == 0)
                    return null;
               else
               {
                    ExtZoneProps retExtZoneProps = new ExtZoneProps();
                    retExtZoneProps.Uid = Convert.ToInt16( dt.Rows[0]["Uid"]);
                    retExtZoneProps.ZoneId = Convert.ToInt16(dt.Rows[0]["ZoneId"]);
                    retExtZoneProps.ZoneType = dt.Rows[0]["ZoneType"].ToString();
                    retExtZoneProps.ApplicationNum = dt.Rows[0]["ApplicationNum"].ToString();
                    retExtZoneProps.ApplicationType = dt.Rows[0]["ApplicationType"].ToString();
                    retExtZoneProps.ApplicationDate = (DateTime)dt.Rows[0]["ApplicationDate"];
                    retExtZoneProps.ONames = dt.Rows[0]["ONames"].ToString();
                    retExtZoneProps.Surname = dt.Rows[0]["Surname"].ToString();
                    retExtZoneProps.AddressL1 = dt.Rows[0]["AddressL1"].ToString();
                    retExtZoneProps.TVNumber = dt.Rows[0]["TVNumber"].ToString();
                    retExtZoneProps.LinkPDF = dt.Rows[0]["LinkPDF"].ToString();
                    retExtZoneProps.LinkIMG = dt.Rows[0]["LinkIMG"].ToString();
                    return retExtZoneProps;
               }
          }
          public bool SaveExtZoneProps(ExtZoneProps uExtZoneProps, DbTransaction transaction, String sConnStr)
          {
               DataTable dt = new DataTable();
               dt.TableName = "GFI_AMM_ExtZoneProps";
               dt.Columns.Add("Uid", uExtZoneProps.Uid.GetType());
               dt.Columns.Add("ZoneId", uExtZoneProps.ZoneId.GetType());
               dt.Columns.Add("ZoneType", uExtZoneProps.ZoneType.GetType());
               dt.Columns.Add("ApplicationNum", uExtZoneProps.ApplicationNum.GetType());
               dt.Columns.Add("ApplicationType", uExtZoneProps.ApplicationType.GetType());
               dt.Columns.Add("ApplicationDate", uExtZoneProps.ApplicationDate.GetType());
               dt.Columns.Add("ONames", uExtZoneProps.ONames.GetType());
               dt.Columns.Add("Surname", uExtZoneProps.Surname.GetType());
               dt.Columns.Add("AddressL1", uExtZoneProps.AddressL1.GetType());
               dt.Columns.Add("TVNumber", uExtZoneProps.TVNumber.GetType());
               dt.Columns.Add("LinkPDF", uExtZoneProps.LinkPDF.GetType());
               dt.Columns.Add("LinkIMG", uExtZoneProps.LinkIMG.GetType());
               DataRow dr = dt.NewRow();
               dr["Uid"] = uExtZoneProps.Uid;
               dr["ZoneId"] = uExtZoneProps.ZoneId;
               dr["ZoneType"] = uExtZoneProps.ZoneType;
               dr["ApplicationNum"] = uExtZoneProps.ApplicationNum;
               dr["ApplicationType"] = uExtZoneProps.ApplicationType;
               dr["ApplicationDate"] = uExtZoneProps.ApplicationDate;
               dr["ONames"] = uExtZoneProps.ONames;
               dr["Surname"] = uExtZoneProps.Surname;
               dr["AddressL1"] = uExtZoneProps.AddressL1;
               dr["TVNumber"] = uExtZoneProps.TVNumber;
               dr["LinkPDF"] = uExtZoneProps.LinkPDF;
               dr["LinkIMG"] = uExtZoneProps.LinkIMG;
               dt.Rows.Add(dr);

               Connection _connection = new Connection(sConnStr); ;
               if (_connection.GenInsert(dt, transaction, sConnStr))
                    return true;
               else
                    return false;
          }
          public bool UpdateExtZoneProps(ExtZoneProps uExtZoneProps, DbTransaction transaction, String sConnStr)
          {
               DataTable dt = new DataTable();
               dt.TableName = "GFI_AMM_ExtZoneProps";
               dt.Columns.Add("Uid", uExtZoneProps.Uid.GetType());
               dt.Columns.Add("ZoneId", uExtZoneProps.ZoneId.GetType());
               dt.Columns.Add("ZoneType", uExtZoneProps.ZoneType.GetType());
               dt.Columns.Add("ApplicationNum", uExtZoneProps.ApplicationNum.GetType());
               dt.Columns.Add("ApplicationType", uExtZoneProps.ApplicationType.GetType());
               dt.Columns.Add("ApplicationDate", uExtZoneProps.ApplicationDate.GetType());
               dt.Columns.Add("ONames", uExtZoneProps.ONames.GetType());
               dt.Columns.Add("Surname", uExtZoneProps.Surname.GetType());
               dt.Columns.Add("AddressL1", uExtZoneProps.AddressL1.GetType());
               dt.Columns.Add("TVNumber", uExtZoneProps.TVNumber.GetType());
               dt.Columns.Add("LinkPDF", uExtZoneProps.LinkPDF.GetType());
               dt.Columns.Add("LinkIMG", uExtZoneProps.LinkIMG.GetType());
               DataRow dr = dt.NewRow();
               dr["Uid"] = uExtZoneProps.Uid;
               dr["ZoneId"] = uExtZoneProps.ZoneId;
               dr["ZoneType"] = uExtZoneProps.ZoneType;
               dr["ApplicationNum"] = uExtZoneProps.ApplicationNum;
               dr["ApplicationType"] = uExtZoneProps.ApplicationType;
               dr["ApplicationDate"] = uExtZoneProps.ApplicationDate;
               dr["ONames"] = uExtZoneProps.ONames;
               dr["Surname"] = uExtZoneProps.Surname;
               dr["AddressL1"] = uExtZoneProps.AddressL1;
               dr["TVNumber"] = uExtZoneProps.TVNumber;
               dr["LinkPDF"] = uExtZoneProps.LinkPDF;
               dr["LinkIMG"] = uExtZoneProps.LinkIMG;
               dt.Rows.Add(dr);

               Connection _connection = new Connection(sConnStr); ;
               if (_connection.GenUpdate(dt, "Uid = '" + uExtZoneProps.Uid + "'", transaction, sConnStr))
                    return true;
               else
                    return false;
          }
          public bool DeleteExtZoneProps(ExtZoneProps uExtZoneProps, String sConnStr)
          {
               Connection _connection = new Connection(sConnStr);
               if (_connection.GenDelete("GFI_AMM_ExtZoneProps", "Uid = '" + uExtZoneProps.Uid + "'", sConnStr))
                    return true;
               else
                    return false;
          }

     }
}


