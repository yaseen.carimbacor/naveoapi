﻿using System;
using System.Collections.Generic;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Zones;

namespace NaveoOneLib.Services.Zones
{
    public class ZoneDetailService
    {

        Errors er = new Errors();
        public DataTable GetZoneDetail(String sConnStr)
        {
            String sqlString = @"SELECT iID, 
                                    Latitude, 
                                    Longitude, 
                                    ZoneID, 
                                    CordOrder from GFI_FLT_ZoneDetail order by iID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        public static List<ZoneDetail> lZoneDetails(DataTable dt)
        {
            List<ZoneDetail> l = new List<ZoneDetail>();
            foreach (DataRow dr in dt.Rows)
            {
                ZoneDetail retZoneDetail = new ZoneDetail();
                retZoneDetail.iID = Convert.ToInt32(dr["iID"]);
                retZoneDetail.latitude = (float)Convert.ToDouble(dr["Latitude"]);
                retZoneDetail.longitude = (float)Convert.ToDouble(dr["Longitude"]);
                retZoneDetail.zoneID = Convert.ToInt32(dr["ZoneID"]);
                retZoneDetail.cordOrder = Convert.ToInt32(dr["CordOrder"]);
                l.Add(retZoneDetail);
            }
            return l;
        }

        public List<ZoneDetail> GetZoneDetailsByZoneId(int ZoneID, String sConnStr)
        {
            String sqlString = @"SELECT iID, 
                                    Latitude, 
                                    Longitude, 
                                    ZoneID, 
                                    CordOrder from GFI_FLT_ZoneDetail
                                    WHERE ZoneID = ?
                                    order by iID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, ZoneID.ToString(), sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                List<ZoneDetail> l = new List<ZoneDetail>();
                foreach (DataRow dr in dt.Rows)
                {
                    ZoneDetail retZoneDetail = new ZoneDetail();
                    retZoneDetail.iID = Convert.ToInt32(dr["iID"]);
                    retZoneDetail.latitude = (float)Convert.ToDouble(dr["Latitude"]);
                    retZoneDetail.longitude = (float)Convert.ToDouble(dr["Longitude"]);
                    retZoneDetail.zoneID = Convert.ToInt32(dr["ZoneID"]);
                    retZoneDetail.cordOrder = Convert.ToInt32(dr["CordOrder"]);
                    l.Add(retZoneDetail);
                }
                return l;
            }
        }
        public Boolean SaveZoneDetail(ZoneDetail uZoneDetail, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_ZoneDetail";
            dt.Columns.Add("Latitude", uZoneDetail.latitude.GetType());
            dt.Columns.Add("Longitude", uZoneDetail.longitude.GetType());
            dt.Columns.Add("ZoneID", uZoneDetail.zoneID.GetType());
            dt.Columns.Add("CordOrder", uZoneDetail.cordOrder.GetType());
            DataRow dr = dt.NewRow();
            dr["Latitude"] = uZoneDetail.latitude;
            dr["Longitude"] = uZoneDetail.longitude;
            dr["ZoneID"] = uZoneDetail.zoneID;
            dr["CordOrder"] = uZoneDetail.cordOrder;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenInsert(dt, transaction, sConnStr))
                return true;
            else
                return false;
        }

        public bool UpdateZoneDetail(ZoneDetail uZoneDetail, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_ZoneDetail";
            dt.Columns.Add("iID", uZoneDetail.iID.GetType());
            dt.Columns.Add("Latitude", uZoneDetail.latitude.GetType());
            dt.Columns.Add("Longitude", uZoneDetail.longitude.GetType());
            dt.Columns.Add("ZoneID", uZoneDetail.zoneID.GetType());
            dt.Columns.Add("CordOrder", uZoneDetail.cordOrder.GetType());
            DataRow dr = dt.NewRow();
            dr["iID"] = uZoneDetail.iID;
            dr["Latitude"] = uZoneDetail.latitude;
            dr["Longitude"] = uZoneDetail.longitude;
            dr["ZoneID"] = uZoneDetail.zoneID;
            dr["CordOrder"] = uZoneDetail.cordOrder;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "iID = '" + uZoneDetail.iID + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool DeleteZoneDetail(ZoneDetail uZoneDetail, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_FLT_ZoneDetail", "iID = '" + uZoneDetail.iID + "'", sConnStr))
                return true;
            else
                return false;
        }

    }
}
