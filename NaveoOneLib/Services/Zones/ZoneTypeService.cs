using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Zones;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Models.Common;

namespace NaveoOneLib.Services.Zones
{
    public class ZoneTypeService
    {
        Errors er = new Errors();
        public DataTable GetZoneType(String sConnStr)
        {
            String sqlString = @"SELECT * from GFI_FLT_ZoneType order by ZoneTypeID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        ZoneType GetZoneType(DataSet ds)
        {
            ZoneType zt = new ZoneType();
            zt.ZoneTypeID = Convert.ToInt32(ds.Tables["ZoneType"].Rows[0]["ZoneTypeID"]);
            zt.sDescription = ds.Tables["ZoneType"].Rows[0]["sDescription"].ToString();
            zt.sComments = ds.Tables["ZoneType"].Rows[0]["sComments"].ToString();
            zt.isSystem = ds.Tables["ZoneType"].Rows[0]["isSystem"].ToString();

            List<Matrix> lMatrix = new List<Matrix>();
            MatrixService ms = new MatrixService();
            foreach (DataRow dr in ds.Tables["dtMatrix"].Rows)
                lMatrix.Add(ms.AssignMatrix(dr));
            zt.lMatrix = lMatrix;

            return zt;
        }
        public ZoneType GetZoneTypeById(String iID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();


            String sql = @"SELECT * from GFI_FLT_ZoneType
                                    WHERE ZoneTypeID = ?
                                    order by ZoneTypeID";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = iID.ToString();
            drSql["sTableName"] = "ZoneType";
            dtSql.Rows.Add(drSql);

            sql = new MatrixService().sGetMatrixId(Convert.ToInt32(iID), "GFI_SYS_GroupMatrixZoneType");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables["ZoneType"].Rows.Count == 0)
                return null;
            else
                return GetZoneType(ds);
        }
        public ZoneType GetZoneTypeByDescription(String Description, String sConnStr)
        {
            String sqlString = @"SELECT * from GFI_FLT_ZoneType
                                    WHERE sDescription = ?
                                    order by sDescription";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, Description, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                ZoneType retZoneType = new ZoneType();
                retZoneType.ZoneTypeID = Convert.ToInt32(dt.Rows[0]["ZoneTypeID"]);
                retZoneType.sDescription = dt.Rows[0]["sDescription"].ToString();
                retZoneType.sComments = dt.Rows[0]["sComments"].ToString();
                retZoneType.isSystem = dt.Rows[0]["isSystem"].ToString();
                return retZoneType;
            }
        }
        public ZoneType GetZoneTypeByComments(String Comments, String sConnStr)
        {
            String sqlString = @"SELECT * from GFI_FLT_ZoneType
                                    WHERE sComments = ?
                                    order by sDescription";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, Comments, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                ZoneType retZoneType = new ZoneType();
                retZoneType.ZoneTypeID = Convert.ToInt32(dt.Rows[0]["ZoneTypeID"]);
                retZoneType.sDescription = dt.Rows[0]["sDescription"].ToString();
                retZoneType.sComments = dt.Rows[0]["sComments"].ToString();
                retZoneType.isSystem = dt.Rows[0]["isSystem"].ToString();
                return retZoneType;
            }
        }

        public List<ZoneType> getMyZoneTypes(List<ZoneHeadZoneType> lzhzt, DataTable dtZoneType, Boolean isInit = false)
        {
            List<ZoneType> l = new List<ZoneType>();
            foreach (ZoneHeadZoneType zhzt in lzhzt)
            {
                DataRow[] dr = dtZoneType.Select("ZoneTypeID = " + zhzt.ZoneTypeID.ToString());
                foreach (DataRow dri in dr)
                {
                    ZoneType z = new ZoneType();
                    z.ZoneTypeID = Convert.ToInt32(dri["ZoneTypeID"]);
                    z.sDescription = dri["sDescription"].ToString();
                    z.sComments = dri["sComments"].ToString();
                    z.isSystem = dri["isSystem"].ToString();
                    z.ZHZTiID = zhzt.iID;

                    if (isInit)
                        z.oprType = DataRowState.Unchanged;

                    l.Add(z);
                }
            }

            return l;
        }

        public String sGetZoneTypes(List<int> iParentIDs, String sConnStr)
        {
            String sql = @"SELECT z.* from GFI_FLT_ZoneType z, GFI_SYS_GroupMatrixZoneType m
                                where z.ZoneTypeID = m.iID and m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + ")";
            sql += " union " + sGetSystemTypes() + "\r\n order by [sDescription] --Remove from Oracle";
            return sql;
        }

        public String sGetZoneTypesById(String strFilter)
        {
            String sql = @"SELECT z.* from GFI_FLT_ZoneType z  where ZoneID in (" + strFilter + ")";
            sql += " union " + sGetSystemTypes() + "order by [sDescription]";
            return sql;
        }
        public String sGetSystemTypes()
        {
            return "SELECT * from GFI_FLT_ZoneType where isSystem = '1'";
        }

        public DataTable GetZoneTypes(List<Matrix> lMatrix, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            return GetZoneTypes(iParentIDs, sConnStr);
        }
        public DataTable GetZoneTypes(List<int> iParentIDs, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = sGetZoneTypes(iParentIDs, sConnStr);
            DataTable dt = c.GetDataDT(sql,  sConnStr);

            return dt;
        }

        public List<ZoneType> GetZoneTypeList(List<Matrix> lMatrix, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            return GetZoneTypeList(iParentIDs, sConnStr);
        }
        public List<ZoneType> GetZoneTypeList(List<int> iParentIDs, String sConnStr)
        {
            List<ZoneType> l = new List<ZoneType>();
            DataTable dtZoneType = GetZoneTypes(iParentIDs,  sConnStr);
            if (dtZoneType.Rows.Count == 0)
                return l;

            l = new myConverter().ConvertToList<ZoneType>(dtZoneType);
            return l;
        }

        public Boolean SaveZoneType(ZoneType uZoneType, Boolean bInsert, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_ZoneType";
            dt.Columns.Add("ZoneTypeID", typeof(Int32));
            dt.Columns.Add("sDescription", typeof(String));
            dt.Columns.Add("sComments", typeof(String));
            dt.Columns.Add("isSystem", typeof(String));


            DataRow dr = dt.NewRow();
            dr["ZoneTypeID"] = uZoneType.ZoneTypeID;
            dr["sDescription"] = uZoneType.sDescription;
            dr["sComments"] = uZoneType.sComments;
            if (uZoneType.isSystem != "1")
                uZoneType.isSystem = "0";
            dr["isSystem"] = uZoneType.isSystem;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "ZoneTypeID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uZoneType.ZoneTypeID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            DataTable dtMatrix = new myConverter().ListToDataTable(uZoneType.lMatrix);
            dtMatrix.TableName = "GFI_SYS_GroupMatrixZoneType";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 2;
            drCtrl["TblName"] = dtMatrix.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "MID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "iID";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtMatrix);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }

        public bool UpdateZoneType(ZoneType uZoneType, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_ZoneType";
            dt.Columns.Add("ZoneTypeID", uZoneType.ZoneTypeID.GetType());
            dt.Columns.Add("sDescription", uZoneType.sDescription.GetType());
            dt.Columns.Add("sComments", uZoneType.sComments.GetType());
            dt.Columns.Add("isSystem", uZoneType.isSystem.GetType());
            DataRow dr = dt.NewRow();
            dr["ZoneTypeID"] = uZoneType.ZoneTypeID;
            dr["sDescription"] = uZoneType.sDescription;
            dr["sComments"] = uZoneType.sComments;
            dr["isSystem"] = uZoneType.isSystem;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "ZoneTypeID = '" + uZoneType.ZoneTypeID + "'", transaction,  sConnStr))
                return true;
            else
                return false;
        }
        public bool DeleteZoneType(ZoneType uZoneType, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_FLT_ZoneType", "ZoneTypeID = '" + uZoneType.ZoneTypeID + "'", sConnStr))
                return true;
            else
                return false;
        }


        public dtData GetZoneListFromZoneIDs(Guid sUserToken, List<int> lZoneTypeIDs, String sConnStr, int? Page, int? LimitPerPage)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = sGetZoneTypeFromZoneTypeIDs(lZoneTypeIDs, sConnStr, Page, LimitPerPage);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZoneTypeH";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            DataTable dt = ds.Tables["dtZoneTypeH"];

            if (dt.Columns.Contains("zonetypeid"))
            {
                dt.Columns["zonetypeid"].ColumnName = "ZoneTypeID";

            }
            if (dt.Columns.Contains("sdescription"))
            {
                dt.Columns["sdescription"].ColumnName = "sDescription";
            }
            if (dt.Columns.Contains("scomments"))
            {
                dt.Columns["scomments"].ColumnName = "sComments";
            }
            


            dtData dtR = new dtData();
            dtR.Data = dt;
            if (Page == 1)
                dtR.Rowcnt = GetNumberOfZones(sUserToken, sConnStr);
            else
                dtR.Rowcnt = 0;

            return dtR;
        }



        public int GetNumberOfZones(Guid sUserToken, String sConnStr)
        {
            List<Matrix> lm = new MatrixService().GetMatrixByUserToken(sUserToken, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"
SELECT 'Tot' AS TableName
	SELECT count(distinct z.ZoneTypeID) from GFI_FLT_ZoneType z
		inner join GFI_SYS_GroupMatrixZoneType m on z.ZoneTypeID = m.iID
    where m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + @")
";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            int x = 0;
            if (ds != null)
                if (ds.Tables.Contains("Tot"))
                    if (ds.Tables["Tot"].Rows.Count > 0)
                        int.TryParse(ds.Tables["Tot"].Rows[0][0].ToString(), out x);

            return x;
        }
        public String sGetZoneTypeFromZoneTypeIDs(List<int> lZoneIDs, String sConnStr, int? Page, int? LimitPerPage)
        {
            String s = String.Empty;
            foreach (int i in lZoneIDs)
                s += i + ", ";
            if (s.Length > 2)
                s = s.Substring(0, s.Length - 2);

            Pagination p = Pagination.GetRowNums(Page, LimitPerPage);
            String sql = @"
;with cte as
(
	SELECT ROW_NUMBER() OVER (Order by ZoneTypeID) AS RowNumber
    , z.ZoneTypeID, z.sDescription, z.sComments, z.isSystem from GFI_FLT_ZoneType z
    where z.ZoneTypeID in (" + s + @")
)
 select * from cte";

            if (Page.HasValue)
                sql += " where RowNumber >= " + p.RowFrom + " and RowNumber <= " + p.RowTo;

            return sql;
        }


        public DataSet GetZoneTypeID(int Id, String sConnStr)
        {


            String sql = @"SELECT *
                               FROM GFI_FLT_ZoneType  where ZoneTypeID = " + Id + "";
                                

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZoneType";
            dtSql.Rows.Add(drSql);

            sql = @"SELECT * FROM GFI_SYS_GroupMatrixZoneType where iID = " + Id +"";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            DataTable dt = ds.Tables["dtZoneType"];

            if (dt.Columns.Contains("zonetypeid"))
            {
                dt.Columns["zonetypeid"].ColumnName = "ZoneTypeID";

            }
            if (dt.Columns.Contains("sdescription"))
            {
                dt.Columns["sdescription"].ColumnName = "sDescription";
            }
            if (dt.Columns.Contains("scomments"))
            {
                dt.Columns["scomments"].ColumnName = "sComments";
            }

            dt = ds.Tables["dtMatrix"];
            if (dt.Columns.Contains("mid"))
            {
                dt.Columns["mid"].ColumnName = "MID";

            }
            if (dt.Columns.Contains("gmid"))
            {
                dt.Columns["gmid"].ColumnName = "GMID";
            }
            if (dt.Columns.Contains("iid"))
            {
                dt.Columns["iid"].ColumnName = "iID";
            }
            return ds;
        }

    }
}