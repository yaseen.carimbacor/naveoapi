using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Zones;
using NaveoOneLib.Services.Audits;

namespace NaveoOneLib.Services.Zones
{
    public class VCADetailService
    {
        Errors er = new Errors();
        public DataTable GetVCADetail(String sConnStr)
        {
            String sqlString = @"SELECT iID, 
Latitude, 
Longitude, 
VCAID, 
CordOrder from GFI_FLT_VCADetail order by VCAID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        VCADetail GetVCADetail(DataRow dr)
        {
            VCADetail retVCADetail = new VCADetail();
            retVCADetail.iID = Convert.ToInt32(dr["iID"]);
            retVCADetail.Latitude = (float)Convert.ToDouble(dr["Latitude"]);
            retVCADetail.Longitude = (float)Convert.ToDouble(dr["Longitude"]);
            retVCADetail.VCAID = Convert.ToInt32(dr["VCAID"]);
            retVCADetail.CordOrder = Convert.ToInt32(dr["CordOrder"]);
            return retVCADetail;
        }

        public List<VCADetail> GetVCADetailsByVCAID(int VCAID, String sConnStr)
        {
            String sqlString = @"SELECT * from GFI_FLT_VCADetail WHERE VCAID = ?";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, VCAID.ToString(), sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                List<VCADetail> l = new List<VCADetail>();
                foreach (DataRow dr in dt.Rows)
                    l.Add(GetVCADetail(dr));
                
                return l;
            }
        }

        public VCADetail GetVCADetailById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT iID, 
Latitude, 
Longitude, 
VCAID, 
CordOrder from GFI_FLT_VCADetail
WHERE VCAID = ?
order by VCAID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetVCADetail(dt.Rows[0]);
        }
        DataTable VCADetailDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_VCADetail";
            dt.Columns.Add("iID", typeof(int));
            dt.Columns.Add("Latitude", typeof(float));
            dt.Columns.Add("Longitude", typeof(float));
            dt.Columns.Add("VCAID", typeof(int));
            dt.Columns.Add("CordOrder", typeof(int));

            return dt;
        }
        public Boolean SaveVCADetail(VCADetail uVCADetail, Boolean bInsert, String sConnStr)
        {
            DataTable dt = VCADetailDT();

            DataRow dr = dt.NewRow();
            dr["iID"] = uVCADetail.iID;
            dr["Latitude"] = uVCADetail.Latitude;
            dr["Longitude"] = uVCADetail.Longitude;
            dr["VCAID"] = uVCADetail.VCAID;
            dr["CordOrder"] = uVCADetail.CordOrder;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "VCAID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uVCADetail.VCAID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public Boolean DeleteVCADetail(VCADetail uVCADetail, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_FLT_VCADetail where VCAID = " + uVCADetail.VCAID.ToString();
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            DataTable dtAudit = new AuditService().LogTran(3, "VCADetail", uVCADetail.VCAID.ToString(), Globals.uLogin.UID, uVCADetail.VCAID);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "CallerFunction";
            drCtrl["ParentTblName"] = "GFI_FLT_VCADetail";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
    }
}
