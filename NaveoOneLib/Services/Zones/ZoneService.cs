﻿using System;
using System.Collections.Generic;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Zones;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Services.Audits;
using Newtonsoft.Json;
using NaveoOneLib.Models.Lookups;
using System.Linq;
using System.Drawing;
using System.Text.RegularExpressions;

namespace NaveoOneLib.Services.Zones
{
    public class ZoneService
    {
        Errors er = new Errors();

        class coordinate
        {
            public Double x { get; set; }
            public Double y { get; set; }
        }
        class zoneGeom
        {
            public DataRowState oprType { get; set; }
            public String type { get; set; }
            public List<coordinate> coordinates { get; set; }
        }

        DataTable dtZoneHeader()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_ZoneHeader";
            dt.Columns.Add("ZoneID", typeof(int));
            dt.Columns.Add("Description", typeof(String));
            dt.Columns.Add("Displayed", typeof(int));
            dt.Columns.Add("Comments", typeof(String));
            dt.Columns.Add("Color", typeof(int));
            dt.Columns.Add("ExternalRef", typeof(String));
            dt.Columns.Add("Ref2", typeof(String));
            dt.Columns.Add("isMarker", typeof(int));
            dt.Columns.Add("iBuffer", typeof(int));

            dt.Columns.Add("GeomData", typeof(String));
            dt.Columns.Add("ZoneExpiry", typeof(DateTime));
            dt.Columns.Add("LookUpValue", typeof(int));
            dt.Columns.Add("NoOfHouseHold", typeof(int));
            dt.Columns.Add("oprType", typeof(DataRowState));

            return dt;
        }
        public Boolean SaveZone(Zone uZone, Boolean bInsert, int uLogin, String sConnStr)
        {
            //Oracle select z.GeomData.Get_WKT(), z.GeomData.ST_CoordDim(), z.GeomData.ST_IsValid(), z.*, SDO_UTIL.RECTIFY_GEOMETRY(z.GeomData, 0.005) from GFI_FLT_ZoneHeader z

            if (bInsert)
                uZone.oprType = DataRowState.Added;
            LookUpValues lookUpValue = new NaveoOneLib.Services.Lookups.LookUpValuesService().GetLookUpValuesByName("REGISTRY", sConnStr);

            if (uZone.zoneDetails != null && uZone.zoneDetails.Count == 1)
                uZone.isMarker = 1;

            DataTable dt = dtZoneHeader();
            DataRow dr = dt.NewRow();
            dr["ZoneID"] = uZone.zoneID;
            dr["Description"] = uZone.description;
            dr["Displayed"] = uZone.displayed;
            dr["Comments"] = uZone.comments;
            dr["Color"] = uZone.color;
            dr["ExternalRef"] = uZone.externalRef;
            dr["Ref2"] = uZone.ref2;
            dr["isMarker"] = uZone.isMarker;
            dr["iBuffer"] = uZone.iBuffer;
            dr["ZoneExpiry"] = uZone.ZoneExpiry.HasValue ? uZone.ZoneExpiry : (object)DBNull.Value;
            dr["oprType"] = uZone.oprType;
            dr["NoOfHouseHold"] = uZone.NoOfHouseHold;
            dr["GeomData"] = uZone.GeomData != null ? uZone.GeomData : (object)DBNull.Value;

            if (uZone.LookUpValue != 0)
                dr["LookUpValue"] = uZone.LookUpValue;

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "ZoneID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uZone.zoneID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dt.Rows.Add(dr);
            dsProcess.Merge(dt);


            if (uZone.GeomData != null)
            {
                #region GeomData 4326
                String KeyField = "ZoneID";
                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 2;
                drCtrl["TblName"] = "GeomDataField";
                drCtrl["NextIdAction"] = "SetToSP";
                drCtrl["NextIdFieldName"] = KeyField;
                drCtrl["ParentTblName"] = dt.TableName;
                drCtrl["CmdType"] = "STOREDPROC";
                String sql = "update " + dt.TableName + " set GeomData.STSrid = 4326 where " + KeyField + " = ";
                drCtrl["Command"] = sql + "?";
                if (!bInsert)
                    drCtrl["Command"] = sql + uZone.zoneID.ToString();
                dtCtrl.Rows.Add(drCtrl);

                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 3;
                drCtrl["TblName"] = "GeomDataField";
                drCtrl["NextIdAction"] = "SetToSP";
                drCtrl["NextIdFieldName"] = KeyField;
                drCtrl["ParentTblName"] = dt.TableName;
                drCtrl["CmdType"] = "STOREDPROC";
                sql = "update " + dt.TableName + " set GeomData = GeomData.MakeValid() where GeomData.STIsValid() = 0 and " + KeyField + " = ";
                drCtrl["Command"] = sql + "?";
                if (!bInsert)
                    drCtrl["Command"] = sql + uZone.zoneID.ToString();
                dtCtrl.Rows.Add(drCtrl);
                #endregion
            } else
            {
                String sql = "Geometry::STPolyFromText('POLYGON((";
                foreach(ZoneDetail zd in uZone.zoneDetails)
                    sql += zd.longitude + " " + zd.latitude + ", ";

                sql = sql.Substring(0, sql.Length - 2);
                sql += "))', 4326)";

                String KeyField = "ZoneID";
                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 2;
                drCtrl["TblName"] = "GeomDataField";
                drCtrl["NextIdAction"] = "SetToSP";
                drCtrl["NextIdFieldName"] = KeyField;
                drCtrl["ParentTblName"] = dt.TableName;
                drCtrl["CmdType"] = "STOREDPROC";
                sql = "update " + dt.TableName + " set GeomData = " + sql + " where " + KeyField + " = ";
                drCtrl["Command"] = sql + "?";
                if (!bInsert)
                    drCtrl["Command"] = sql + uZone.zoneID.ToString();
                dtCtrl.Rows.Add(drCtrl);

                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 3;
                drCtrl["TblName"] = "GeomDataField";
                drCtrl["NextIdAction"] = "SetToSP";
                drCtrl["NextIdFieldName"] = "ZoneID";
                drCtrl["ParentTblName"] = dt.TableName;
                drCtrl["CmdType"] = "STOREDPROC";
                sql = "update " + dt.TableName + " set GeomData = GeomData.MakeValid() where GeomData.STIsValid() = 0 and ZoneID = ";
                drCtrl["Command"] = sql + "?";
                if (!bInsert)
                    drCtrl["Command"] = sql + uZone.zoneID.ToString();
                dtCtrl.Rows.Add(drCtrl);
            }

            if (uZone.lMatrix == null || uZone.lMatrix.Count == 0)
            {
                uZone.lMatrix = new NaveoOneLib.Services.GMatrix.MatrixService().GetMatrixByUserID(uLogin, sConnStr);
                foreach (Matrix m in uZone.lMatrix)
                    m.oprType = DataRowState.Added;
            }

            DataTable dtMatrix = new myConverter().ListToDataTable<Matrix>(uZone.lMatrix);
            dtMatrix.TableName = "GFI_SYS_GroupMatrixZone";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 4;
            drCtrl["TblName"] = dtMatrix.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "MID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "iID";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtMatrix);

            List<ZoneDetail> lzd = new List<ZoneDetail>();
            DataTable dtZoneDetail = new myConverter().ListToDataTable<ZoneDetail>(lzd);
            dtZoneDetail.TableName = "GFI_FLT_ZoneDetail";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 5;
            drCtrl["TblName"] = dtZoneDetail.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "iID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "ZoneID";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            int i = 0;

            Boolean bModified = false;
            if (uZone.zoneDetails.Count == 0)
            {
                String[] Coordinates = uZone.GeomData.Split(',');
                int ord = 0;
                foreach (String s in Coordinates)
                {
                    String[] coord = s.Split(' ');
                    ZoneDetail zd = new ZoneDetail();
                    zd.oprType = DataRowState.Added;
                    zd.cordOrder = ord;
                    int x = 0;
                    if (coord.Length == 3)
                        x = 1;
                    zd.longitude = Convert.ToDouble(Regex.Replace(coord[x], "[^0-9.-]", ""));
                    zd.latitude = Convert.ToDouble(Regex.Replace(coord[x + 1], "[^0-9.-]", ""));
                    uZone.zoneDetails.Add(zd);
                    ord++;
                }
            }
            if (!bInsert)
                if (bModified)
                {
                    drCtrl = dtCtrl.NewRow();
                    drCtrl["SeqNo"] = 7;
                    drCtrl["ParentTblName"] = dt.TableName;
                    drCtrl["CmdType"] = "STOREDPROC";
                    drCtrl["Command"] = "delete from GFI_FLT_ZoneDetail where ZoneID = " + uZone.zoneID.ToString();
                    dtCtrl.Rows.Add(drCtrl);
                }

            String sOracleGeomData = String.Empty;
            foreach (ZoneDetail zd in uZone.zoneDetails)
            {
                DataRow drzd = dtZoneDetail.NewRow();
                drzd["iID"] = zd.iID;
                drzd["Latitude"] = zd.latitude;
                drzd["Longitude"] = zd.longitude;


                if(bInsert)
                {
                    drzd["ZoneID"] = zd.zoneID;
                    drzd["oprType"] = DataRowState.Added;
                }
                else
                {
                    drzd["ZoneID"] = uZone.zoneID;
                    drzd["oprType"] = DataRowState.Modified;
                }
            
                    

                drzd["CordOrder"] = i;
                //drzd["oprType"] = zd.oprType;
                dtZoneDetail.Rows.Add(drzd);
                //sOracleGeomData += zd.longitude.ToString() + "," + zd.latitude.ToString() + ", ";
                i++;
            }
            dsProcess.Merge(dtZoneDetail);

            List<ZoneHeadZoneType> lzhzt = new List<ZoneHeadZoneType>();
            DataTable dtZHZT = new myConverter().ListToDataTable<ZoneHeadZoneType>(lzhzt);
            dtZHZT.Columns.Add("oprType", typeof(DataRowState));
            dtZHZT.TableName = "GFI_FLT_ZoneHeadZoneType";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 6;
            drCtrl["TblName"] = dtZHZT.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "iID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "ZoneHeadID";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            if (uZone.lZoneType.Count == 0)
            {
                ZoneType zt = new ZoneType();
                zt = new ZoneTypeService().GetZoneTypeByDescription("Office/Depot Zone", sConnStr);
                zt.oprType = DataRowState.Added;
                uZone.lZoneType.Add(zt);
            }
            foreach (ZoneType zt in uZone.lZoneType)
            {
                DataRow drZHZT = dtZHZT.NewRow();
                drZHZT["ZoneHeadID"] = uZone.zoneID;
                drZHZT["ZoneTypeID"] = zt.ZoneTypeID;
                drZHZT["iID"] = zt.ZHZTiID;
                drZHZT["oprType"] = zt.oprType;
                dtZHZT.Rows.Add(drZHZT);
            }
            dsProcess.Merge(dtZHZT);

            int iTranType = 2;
            if (bInsert)
                iTranType = 1;

            DataTable dtAudit = new AuditService().LogTran(iTranType, "Zones", uZone.description, uLogin, 0);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "CallerFunction";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResults = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResults.Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public BaseModel SaveZonesFromShp(Guid sUserToken, String shpPath, String sConnStr)
        {
            List<String> lResult = new List<string>();
            int iCntNew = 0;
            int iCntUpdated = 0;

            int UID = new NaveoOneLib.Services.Users.UserService().GetUserID(sUserToken, sConnStr);

            BaseModel bm = new BaseModel();
            try
            {
                List<String> lfeatureIDs = NaveoOneLib.Maps.NaveoWebMapsCall.GetAllFeatureIDs(shpPath);
                foreach (String id in lfeatureIDs)
                {
                    List<KeyValuePair<string, string>> lFeatures = NaveoOneLib.Maps.NaveoWebMapsCall.GetAllFeaturesByID(shpPath, id);
                    Zone zone = new Zone();

                    zone.description = lFeatures.FirstOrDefault(x => x.Key == "FEATURE").Value;
                    zone.GeomData = lFeatures.FirstOrDefault(x => x.Key == "GeomData").Value.ToString() == string.Empty ? null : lFeatures.FirstOrDefault(x => x.Key == "GeomData").Value;
                    zone.color = Color.Red.ToArgb();
                    Boolean b = SaveZone(zone, true, UID, sConnStr);
                    if (b)
                        iCntNew++;
                }

                lResult.Add("Features : " + lfeatureIDs.Count);
                lResult.Add("iCntNew : " + iCntNew);
                lResult.Add("iCntUpdated : " + iCntUpdated);
                lResult.Add("iCntTotal : " + (iCntNew + iCntUpdated));
            }
            catch (Exception ex)
            {
                lResult.Add(ex.ToString());
            }
            bm.data = lResult;
            return bm;
        }

        private Zone GetZone(DataSet ds, String sConnStr)
        {
            Zone z = new Zone();
            z.zoneID = Convert.ToInt32(ds.Tables["dtZone"].Rows[0]["ZoneID"]);
            z.description = ds.Tables["dtZone"].Rows[0]["Description"].ToString();
            z.displayed = Convert.ToInt32(ds.Tables["dtZone"].Rows[0]["Displayed"]);
            z.comments = ds.Tables["dtZone"].Rows[0]["Comments"].ToString();
            z.color = Convert.ToInt32(ds.Tables["dtZone"].Rows[0]["Color"]);
            z.externalRef = ds.Tables["dtZone"].Rows[0]["ExternalRef"].ToString();
            z.ref2 = ds.Tables["dtZone"].Rows[0]["Ref2"].ToString();
            z.isMarker = Convert.ToInt32(ds.Tables["dtZone"].Rows[0]["isMarker"]);
            z.iBuffer = Convert.ToInt32(ds.Tables["dtZone"].Rows[0]["iBuffer"]);
            z.NoOfHouseHold = string.IsNullOrEmpty(ds.Tables["dtZone"].Rows[0]["NoOfHouseHold"].ToString()) ? 0 : Convert.ToInt32(ds.Tables["dtZone"].Rows[0]["NoOfHouseHold"]);
            z.LookUpValue = string.IsNullOrEmpty(ds.Tables["dtZone"].Rows[0]["LookUpValue"].ToString()) ? 0 : Convert.ToInt32(ds.Tables["dtZone"].Rows[0]["LookUpValue"]);
            if (ds.Tables["dtZone"].Columns.Contains("GeomData"))
                z.GeomData = ds.Tables["dtZone"].Rows[0]["GeomData"].ToString();
            if (ds.Tables["dtZone"].Rows[0]["ZoneExpiry"].ToString() != string.Empty)
                z.ZoneExpiry = Convert.ToDateTime(ds.Tables["dtZone"].Rows[0]["ZoneExpiry"]);

            List<Matrix> lMatrix = new List<Matrix>();
            MatrixService ms = new MatrixService();
            foreach (DataRow dr in ds.Tables["dtMatrix"].Rows)
                lMatrix.Add(ms.AssignMatrix(dr));
            z.lMatrix = lMatrix;

            List<ZoneType> lzt = new List<ZoneType>();
            foreach (DataRow dr in ds.Tables["dtZHZT"].Rows)
            {
                ZoneType zt = new ZoneType();
                zt.ZoneTypeID = Convert.ToInt32(dr["ZoneTypeID"]);
                zt.sDescription = dr["sDescription"].ToString();
                zt.sComments = dr["sComments"].ToString();
                zt.isSystem = dr["isSystem"].ToString();
                zt.ZHZTiID = Convert.ToInt32(dr["iID"]);

                lzt.Add(zt);
            }
            z.lZoneType = lzt;

            if (ds.Tables.Contains("dtZoneDetails"))
                z.zoneDetails = ZoneDetailService.lZoneDetails(ds.Tables["dtZoneDetails"]);

            return z;
        }
        public Zone GetZoneById(int ZoneID, Boolean hasZoneDetails, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"SELECT ZoneID, 
							 Description, 
							 Displayed, 
							 Comments, 
							 Color, ExternalRef, Ref2, isMarker, iBuffer, GeomData.ToString() as GeomData, ZoneExpiry,LookUpValue,NoOfHouseHold
					   from GFI_FLT_ZoneHeader
							 WHERE ZoneID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = ZoneID.ToString();
            drSql["sTableName"] = "dtZone";
            dtSql.Rows.Add(drSql);

            sql = new MatrixService().sGetMatrixId(ZoneID, "GFI_SYS_GroupMatrixZone");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);

            sql = @"select * from GFI_FLT_ZoneHeadZoneType zhzt
	                    inner join GFI_FLT_ZoneType zt on zhzt.ZoneTypeID = zt.ZoneTypeID
                    where ZoneHeadID = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = ZoneID.ToString();
            drSql["sTableName"] = "dtZHZT";
            dtSql.Rows.Add(drSql);

            if (hasZoneDetails)
            {
                sql = @"select * from GFI_FLT_ZoneDetail where ZoneID = ?";
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sParam"] = ZoneID.ToString();
                drSql["sTableName"] = "dtZoneDetails";
                dtSql.Rows.Add(drSql);
            }

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables["dtZone"].Rows.Count == 0)
                return null;
            else
            {
                Zone z = GetZone(ds, sConnStr);
                return z;
            }
        }
        public DataTable GetZoneByZoneName(String ZoneName, String sConnStr)
        {


            String sqlString = @"SELECT ZoneID, 
							 Description, 
							 Displayed, 
							 Comments, 
							 Color, ExternalRef, Ref2, isMarker, iBuffer,GeomData.ToString() as GeomData, ZoneExpiry
					   from GFI_FLT_ZoneHeader
							 WHERE Description = ?";


            return new Connection(sConnStr).GetDataDT(sqlString, ZoneName, null, sConnStr);



        }
        public static String sGetZoneIDs(List<Matrix> lMatrix, String sConnStr)
        {
            String sql = "declare @ZonesIDs table (i int)";
            foreach (Matrix m in lMatrix)
                sql += "\r\ninsert @ZonesIDs values (" + m.GMID.ToString() + ");";
            //@f > @fTmpZone
            //@t > @tTmpDistinctZone
            sql += @"
declare @fTmpZone table (i int)
;WITH  --postGreSQL RECURSIVE    --ZoneMatrixRetrieval
	parent AS 
	(
		SELECT distinct ParentGMID
		FROM GFI_SYS_GroupMatrix g
            inner join @ZonesIDs a on g.ParentGMID = a.i
	), 
	tree AS 
	(
		SELECT 
			x.GMID
			, x.GMDescription
			, x.ParentGMID
		FROM GFI_SYS_GroupMatrix x
		INNER JOIN parent ON x.ParentGMID = parent.ParentGMID
		UNION ALL
		SELECT 
			y.GMID
			, y.GMDescription
			, y.ParentGMID
		FROM GFI_SYS_GroupMatrix y
		INNER JOIN tree t ON y.ParentGMID = t.GMID
	)
insert into @fTmpZone(i)
	select distinct a.ZoneID   
		from GFI_FLT_ZoneHeader a
            inner join GFI_SYS_GroupMatrixZone m on a.ZoneID = m.iID
            inner join tree t on t.GMID = m.GMID 

insert into @fTmpZone(i)
	select distinct h.ZoneID  
		from GFI_FLT_ZoneHeader h
        inner join GFI_SYS_GroupMatrixZone m on h.ZoneID = m.iID
        inner join @ZonesIDs a on m.GMID = a.i;

declare @tTmpDistinctZone table (i int)
insert into @tTmpDistinctZone(i)
    select distinct * from @fTmpZone;

";
            return sql;
        }

        public DataTable GetZone(Guid sUserToken, String sConnStr)
        {
            int UID = new Users.UserService().GetUserID(sUserToken, sConnStr);
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(UID, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            String sqlString = sGetZoneHeader(iParentIDs, sConnStr, null, null);
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        public List<Zone> GetZoneList(List<Matrix> lMatrix, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            return GetZoneList(iParentIDs, sConnStr);
        }
        public List<Zone> GetZoneList(List<int> iParentIDs, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = sGetZoneHeader(iParentIDs, sConnStr, null, null);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZoneH";
            dtSql.Rows.Add(drSql);

            sql = sGetZoneDetail(iParentIDs, sConnStr, null, null);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZoneD";
            dtSql.Rows.Add(drSql);

            sql = new ZoneTypeService().sGetZoneTypes(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZoneT";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            List<Zone> l = new List<Zone>();
            DataTable dtZoneH = ds.Tables["dtZoneH"];
            if (dtZoneH.Rows.Count == 0)
                return l;

            DataTable dtZoneD = ds.Tables["dtZoneD"];
            List<ZoneDetail> ld = new myConverter().ConvertToList<ZoneDetail>(dtZoneD);

            l = new myConverter().ConvertToList<Zone>(dtZoneH);
            //foreach (Zone z in l)
            //    z.ZoneDetail = ld.FindAll(o => o.ZoneID == z.ZoneID);
            Parallel.ForEach(l, z => z.zoneDetails = ld.FindAll(o => o.zoneID == z.zoneID));

            return l;
        }

        public String sGetZoneHeaderFromZoneIDs(List<int> lZoneIDs, String sConnStr, int? Page, int? LimitPerPage)
        {
            String s = String.Empty;
            foreach (int i in lZoneIDs)
                s += i + ", ";
            if (s.Length > 2)
                s = s.Substring(0, s.Length - 2);

            Pagination p = Pagination.GetRowNums(Page, LimitPerPage);
            String sql = @"
;with cte as
(
	SELECT ROW_NUMBER() OVER (Order by ZoneID) AS RowNumber
    , z.ZoneID, z.Description, z.Displayed, z.Comments, z.Color, z.ExternalRef, z.Ref2, z.isMarker, z.iBuffer ,z.ZoneExpiry,z.LookUpValue from GFI_FLT_ZoneHeader z
    where z.ZoneID in (" + s + @")
)
 select * from cte";

            if (Page.HasValue)
                sql += " where RowNumber >= " + p.RowFrom + " and RowNumber <= " + p.RowTo;

            return sql;
        }
        public dtData GetZoneListFromZoneIDs(Guid sUserToken, List<int> lZoneIDs, String sConnStr, int? Page, int? LimitPerPage)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = sGetZoneHeaderFromZoneIDs(lZoneIDs, sConnStr, Page, LimitPerPage);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZoneH";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            DataTable dt = ds.Tables["dtZoneH"];

            if (dt.Columns.Contains("rownumber"))
            {
                dt.Columns["rownumber"].ColumnName = "RowNumber";

            }
             if (dt.Columns.Contains("zoneid"))
            {
                dt.Columns["zoneid"].ColumnName = "ZoneID";
            }
             if (dt.Columns.Contains("description"))
            {
                dt.Columns["description"].ColumnName = "Description";
            }
             if (dt.Columns.Contains("displayed"))
            {
                dt.Columns["displayed"].ColumnName = "Displayed";
            }
            if (dt.Columns.Contains("comments"))
            {
                dt.Columns["comments"].ColumnName = "Comments";
            }
            if (dt.Columns.Contains("color"))
            {
                dt.Columns["color"].ColumnName = "Color";
            }
            if (dt.Columns.Contains("externalref"))
            {
                dt.Columns["externalref"].ColumnName = "ExternalRef";
            }
            if (dt.Columns.Contains("ref2"))
            {
                dt.Columns["ref2"].ColumnName = "Ref2";
            }
             if (dt.Columns.Contains("ismarker"))
            {
                dt.Columns["ismarker"].ColumnName = "isMarker";
            }
             if (dt.Columns.Contains("ibuffer"))
            {
                dt.Columns["ibuffer"].ColumnName = "iBuffer";
            }
                
       
            

            dtData dtR = new dtData();
            dtR.Data = dt;
            if (Page == 1)
                dtR.Rowcnt = GetNumberOfZones(sUserToken, sConnStr);
            else
                dtR.Rowcnt = 0;

            return dtR;
        }

        public int GetNumberOfZones(Guid sUserToken, String sConnStr)
        {
            List<Matrix> lm = new MatrixService().GetMatrixByUserToken(sUserToken, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"
SELECT 'Tot' AS TableName
	SELECT count(distinct z.zoneID) from GFI_FLT_ZoneHeader z
		inner join GFI_SYS_GroupMatrixZone m on z.ZoneID = m.iID
    where m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + @")
";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            int x = 0;
            if (ds != null)
                if (ds.Tables.Contains("Tot"))
                    if (ds.Tables["Tot"].Rows.Count > 0)
                        int.TryParse(ds.Tables["Tot"].Rows[0][0].ToString(), out x);

            return x;
        }
        public DataTable dtGetZonesSpatial(Guid sUserToken, String sConnStr)
        {
            List<Matrix> lm = new MatrixService().GetMatrixByUserToken(sUserToken, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"
;with cte as
(
	SELECT ROW_NUMBER() OVER (Order by ZoneID) AS RowNumber
    , z.ZoneID, z.Description, z.Displayed, z.Comments, z.Color, z.ExternalRef, z.Ref2, z.isMarker, z.iBuffer, z.GeomData.MakeValid().ToString() GeomData from GFI_FLT_ZoneHeader z
		inner join GFI_SYS_GroupMatrixZone m on z.ZoneID = m.iID
    where m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + @")
        and z.GeomData is not null
)
select * from cte ";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "ZoneSpatial";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds.Tables["ZoneSpatial"];
        }

        public String sGetZoneHeader(List<int> iParentIDs, String sConnStr, int? Page, int? LimitPerPage)
        {
            //    String sql = @"SELECT z.* 
            //                from GFI_FLT_ZoneHeader z
            //                    inner join GFI_SYS_GroupMatrixZone m on z.ZoneID = m.iID
            //where m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs,sConnStr) + ") ";

            Pagination p = Pagination.GetRowNums(Page, LimitPerPage);
            String sql = @"
;with cte as
(
	SELECT ROW_NUMBER() OVER (Order by ZoneID) AS RowNumber
    , z.ZoneID, z.Description, z.Displayed, z.Comments, z.Color, z.ExternalRef, z.Ref2, z.isMarker, z.iBuffer,z.LookUpValue, z.NoOfHouseHold from GFI_FLT_ZoneHeader z
		inner join GFI_SYS_GroupMatrixZone m on z.ZoneID = m.iID
    where m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + @")
)
 select z.* from cte z";

            if (Page.HasValue)
                sql += " where RowNumber >= " + p.RowFrom + " and RowNumber <= " + p.RowTo;

            return sql;
        }
        public String sGetZoneDetail(List<int> iParentIDs, String sConnStr, int? Page, int? LimitPerPage)
        {
            Pagination p = Pagination.GetRowNums(Page, LimitPerPage);

            String sql = @"
 ;with cte as
(
	SELECT 
		ROW_NUMBER() OVER (Order by ZoneID) AS RowNumber, z.ZoneID
	from GFI_FLT_ZoneHeader z
		inner join GFI_SYS_GroupMatrixZone m on z.ZoneID = m.iID
    where m.GMID in (" + new GroupMatrixService().GetAllGMValuesWhereClause(iParentIDs, sConnStr) + @")
)
 select z.iID, 
		z.Latitude, 
		z.Longitude, 
		z.ZoneID, 
		CordOrder
from cte 
	inner join GFI_FLT_ZoneDetail z on cte.ZoneID = z.ZoneID
where 1 = 1 ";

            if (Page.HasValue)
                sql += " and RowNumber >= " + p.RowFrom + " and RowNumber <= " + p.RowTo;

            return sql;
        }
        public String sGetZoneDetailsLastPoints()
        {
            String sql = @"
SELECT m1.*
FROM GFI_FLT_ZoneDetail m1 
	LEFT JOIN GFI_FLT_ZoneDetail m2 ON (m1.ZoneID = m2.ZoneID AND m1.CordOrder > m2.CordOrder)
WHERE m2.ZoneID IS NULL
union all
SELECT m1.*
FROM GFI_FLT_ZoneDetail m1 
	LEFT JOIN GFI_FLT_ZoneDetail m2 ON (m1.ZoneID = m2.ZoneID AND m1.CordOrder < m2.CordOrder)
WHERE m2.ZoneID IS NULL
";
            return sql;
        }

        public Boolean DeleteZone(Zone uZone, int uLogin, out DataSet dsResults, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_ZoneHeader";
            dt.Columns.Add("ZoneID", uZone.zoneID.GetType());
            dt.Columns.Add("Description", uZone.description.GetType());
            dt.Columns.Add("Displayed", uZone.displayed.GetType());
            dt.Columns.Add("Comments", uZone.comments.GetType());
            dt.Columns.Add("Color", uZone.color.GetType());
            dt.Columns.Add("ExternalRef", uZone.externalRef.GetType());
            dt.Columns.Add("Ref2", uZone.ref2.GetType());
            dt.Columns.Add("isMarker", uZone.isMarker.GetType());
            dt.Columns.Add("iBuffer", uZone.iBuffer.GetType());
            DataRow dr = dt.NewRow();
            dr["ZoneID"] = uZone.zoneID;
            dr["Description"] = uZone.description;
            dr["Displayed"] = uZone.displayed;
            dr["Comments"] = uZone.comments;
            dr["Color"] = uZone.color;
            dr["ExternalRef"] = uZone.externalRef;
            dr["Ref2"] = uZone.ref2;
            dr["isMarker"] = uZone.isMarker;
            dr["iBuffer"] = uZone.iBuffer;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "ZoneID";
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    dri["oprType"] = DataRowState.Deleted;
            }

            #region GetData
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();
            List<int> iParentIDs = new List<int>();
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin, sConnStr);
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            String sql = new ZoneService().sGetZoneHeader(iParentIDs, sConnStr, null, null);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZoneH";
            dtSql.Rows.Add(drSql);

            sql = new ZoneService().sGetZoneDetail(iParentIDs, sConnStr, null, null);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZoneD";
            dtSql.Rows.Add(drSql);

            sql = new ZoneTypeService().sGetZoneTypes(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZoneTypes";
            dtSql.Rows.Add(drSql);

            sql = new ZoneHeadZoneTypeService().sGetZHZTs(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZHZT";
            dtSql.Rows.Add(drSql);

            sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Zones);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMZone";
            dtSql.Rows.Add(drSql);
            #endregion

            dsProcess.Merge(dtSql);
            dsProcess.Merge(dtCtrl);
            dsResults = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResults.Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                if (dtCtrl.Rows[0]["UpdateStatus"].ToString().ToUpper() == "TRUE")
                    bResult = true;
                else
                    bResult = false;
            }
            else
                bResult = false;

            return bResult;
        }
        public BaseModel DeleteZones(List<int> lZoneID, int uLogin, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            String s = String.Empty;
            foreach (int i in lZoneID)
                s += i.ToString() + ", ";
            if (s.Length > 2)
                s = s.Substring(0, s.Length - 2);
            s = "(" + s + ") ";

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_FLT_ZoneHeader where ZoneID in " + s;
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);

            DataSet dsProcess = new DataSet();
            DataTable dtAudit = new AuditService().LogTran(3, "ZoneBulkDel", "-999", uLogin, -999);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "BulkDel";
            drCtrl["ParentTblName"] = "GFI_FLT_ZoneHeader";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            BaseModel bm = new Models.Common.BaseModel();
            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                {
                    bm.dbResult = dsResult;
                    bm.dbResult.Tables["CTRLTBL"].TableName = "ResultCtrlTbl";
                    bm.dbResult.Merge(dsProcess);
                    bResult = false;
                }
                else
                    bResult = true;
            }
            else
                bResult = false;

            bm.data = bResult;
            return bm;
        }


        public Boolean IsPointInside(Zone z, Coordinate point, String sConnStr)
        {
            List<Coordinate> poly = new List<Coordinate>();
            if (z.zoneDetails == null)
                z = GetZoneById(z.zoneID, true, sConnStr);

            if (z.zoneDetails == null)
                return false;
            foreach (ZoneDetail zd in z.zoneDetails)
                poly.Add(new Coordinate(zd.latitude, zd.longitude));

            return GeoCalc.IsPointInside(poly, point);
        }
        public Coordinate getCentroid(Zone z)
        {
            double centroidX = 0.0;
            double centroidY = 0.0;

            if (z.zoneDetails == null)
                return new Coordinate(0, 0);

            foreach (ZoneDetail zd in z.zoneDetails)
            {
                centroidX += zd.longitude;
                centroidY += zd.latitude;
            }

            int ic = z.zoneDetails.Count;
            if (z.zoneDetails[0].latitude == z.zoneDetails[z.zoneDetails.Count - 1].latitude)
                if (z.zoneDetails[0].longitude == z.zoneDetails[z.zoneDetails.Count - 1].longitude)
                {
                    ic -= 1;
                    centroidX -= z.zoneDetails[0].longitude;
                    centroidY -= z.zoneDetails[0].latitude;
                }

            centroidX /= ic;
            centroidY /= ic;

            return (new Coordinate(centroidY, centroidX));
        }

        public DataTable GetZoneDistances(Zone zFr, List<Zone> lzTo)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ZoneID", typeof(int));
            dt.Columns.Add("Dist", typeof(Double));

            Coordinate fr = getCentroid(zFr);
            foreach (Zone z in lzTo)
            {
                Coordinate to = getCentroid(z);
                Double dist = EGIS.ShapeFileLib.ConversionFunctions.DistanceBetweenLatLongPoints(EGIS.ShapeFileLib.ConversionFunctions.RefEllipse, fr.Lt, fr.Lg, to.Lt, to.Lg);
                DataRow dr = dt.NewRow();
                dr["ZoneID"] = z.zoneID;
                dr["Dist"] = dist;
                dt.Rows.Add(dr);
            }

            try
            {
                dt.DefaultView.Sort = "Dist asc";
                dt = dt.DefaultView.ToTable(true);
            }
            catch { }
            return dt;
        }

        public DataSet GetZonesEnteredPerTrip(List<Matrix> lMatrix, int TripHeaderID, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            drSql = dtSql.NewRow();
            String sql = sGetZoneIDs(lMatrix, sConnStr);
            sql += @"
						declare @Zones table (ZoneID int PRIMARY KEY CLUSTERED, minLat float, minLon float, maxLat float, maxLon float)
						insert @Zones (ZoneID, minLat, minLon, maxLat, maxLon)
						    select d.ZoneID, min(d.Latitude) minLat, min(d.Longitude) minLon, max(d.Latitude) maxLat, max(d.Longitude) maxLon  
							 from GFI_FLT_ZoneDetail d
							  inner join @tTmpDistinctZone t on d.ZoneID = t.i
							  group by  d.ZoneID

						declare @Possible table (Hid int, dt datetime, Lon float, Lat float, Zid int)
						insert into @Possible (Hid, dt, Lon, Lat, Zid)
						 select h.iID, g.DateTimeGPS_UTC, g.Longitude, g.Latitude, z.ZoneID
						  from GFI_GPS_TripHeader h
						  inner join GFI_GPS_TripDetail d on h.iid = d.HeaderiID
						  inner join GFI_GPS_GPSData g on d.GPSDataUID = g.UID
						  left outer join @Zones z on 1 = 1 
						 where h.iID = " + TripHeaderID.ToString() + @" and (g.Longitude between minLon and maxLon) and (g.Latitude between minLat and maxLat) 

						declare @Result table (Hid int, Zid int)
						;with 
						cte as 
						(
						    select *, dbo.isPointInZone(Lon, Lat, Zid) isPointInZone
						 from @Possible
						)
						insert @Result (Hid, Zid)
						 select c.Hid, c.Zid
						 from @Possible p
						  inner join cte c on c.Hid = p.Hid
						 where isPointInZone = 1 
						 group by c.Hid, c.Zid

						select * from @Result r
						inner join GFI_FLT_ZoneHeader z on r.Zid = z.ZoneID
						";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZonePerTrip";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            return ds;
        }

        void initColors()
        {
            System.Drawing.Color c = System.Drawing.Color.LightGreen;
            int i = c.ToArgb();
            c = System.Drawing.Color.FromArgb(-1526749952);
            i = c.ToArgb();
        }

        #region Obsolete
        [Obsolete]
        public Boolean SaveZone(Zone uZone, Boolean bInsert, int uLogin, out DataSet dsResults, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_ZoneHeader";
            dt.Columns.Add("ZoneID", uZone.description.GetType());
            dt.Columns.Add("Description", uZone.description.GetType());
            dt.Columns.Add("Displayed", uZone.displayed.GetType());
            dt.Columns.Add("Comments", uZone.comments.GetType());
            dt.Columns.Add("Color", uZone.color.GetType());
            dt.Columns.Add("ExternalRef", uZone.externalRef.GetType());
            dt.Columns.Add("Ref2", uZone.ref2.GetType());
            dt.Columns.Add("isMarker", uZone.isMarker.GetType());
            dt.Columns.Add("iBuffer", uZone.iBuffer.GetType());
            dt.Columns.Add("NoOfHouseHold", uZone.NoOfHouseHold.GetType());
            DataRow dr = dt.NewRow();
            dr["ZoneID"] = uZone.zoneID;
            dr["Description"] = uZone.description;
            dr["Displayed"] = uZone.displayed;
            dr["Comments"] = uZone.comments;
            dr["Color"] = uZone.color;
            dr["ExternalRef"] = uZone.externalRef;
            dr["Ref2"] = uZone.ref2;
            dr["isMarker"] = uZone.isMarker;
            dr["iBuffer"] = uZone.iBuffer;
            dr["NoOfHouseHold"] = uZone.NoOfHouseHold;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "ZoneID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uZone.zoneID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            DataTable dtMatrix = new myConverter().ListToDataTable<Matrix>(uZone.lMatrix);
            dtMatrix.TableName = "GFI_SYS_GroupMatrixZone";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 2;
            drCtrl["TblName"] = dtMatrix.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "MID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "iID";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtMatrix);

            List<ZoneDetail> lzd = new List<ZoneDetail>();
            DataTable dtZoneDetail = new myConverter().ListToDataTable<ZoneDetail>(lzd);
            dtZoneDetail.TableName = "GFI_FLT_ZoneDetail";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 3;
            drCtrl["TblName"] = dtZoneDetail.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "iID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "ZoneID";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            int i = 0;
            foreach (ZoneDetail zd in uZone.zoneDetails)
            {
                DataRow drzd = dtZoneDetail.NewRow();
                drzd["iID"] = zd.iID;
                drzd["Latitude"] = zd.latitude;
                drzd["Longitude"] = zd.longitude;
                drzd["ZoneID"] = zd.zoneID;
                drzd["CordOrder"] = i;
                dtZoneDetail.Rows.Add(drzd);
                i++;
            }
            dsProcess.Merge(dtZoneDetail);

            List<ZoneHeadZoneType> lzhzt = new List<ZoneHeadZoneType>();
            DataTable dtZHZT = new myConverter().ListToDataTable<ZoneHeadZoneType>(lzhzt);
            dtZHZT.Columns.Add("oprType", typeof(DataRowState));
            dtZHZT.TableName = "GFI_FLT_ZoneHeadZoneType";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 4;
            drCtrl["TblName"] = dtZHZT.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "iID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "ZoneHeadID";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            foreach (ZoneType zt in uZone.lZoneType)
            {
                DataRow drZHZT = dtZHZT.NewRow();
                drZHZT["ZoneHeadID"] = uZone.zoneID;
                drZHZT["ZoneTypeID"] = zt.ZoneTypeID;
                drZHZT["iID"] = zt.ZHZTiID;
                drZHZT["oprType"] = zt.oprType;
                dtZHZT.Rows.Add(drZHZT);
            }
            dsProcess.Merge(dtZHZT);

            int iTranType = 2;
            if (bInsert)
                iTranType = 1;

            DataTable dtAudit = new AuditService().LogTran(iTranType, "Zones", uZone.description, uLogin, 0);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "CallerFunction";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }
            #region GetData
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();
            List<int> iParentIDs = new List<int>();
            List<Matrix> lm = new MatrixService().GetMatrixByUserID(uLogin, sConnStr);
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);

            String sql = new ZoneService().sGetZoneHeader(iParentIDs, sConnStr, null, null);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZoneH";
            dtSql.Rows.Add(drSql);

            sql = new ZoneService().sGetZoneDetail(iParentIDs, sConnStr, null, null);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZoneD";
            dtSql.Rows.Add(drSql);

            sql = new ZoneTypeService().sGetZoneTypes(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZoneTypes";
            dtSql.Rows.Add(drSql);

            sql = new ZoneHeadZoneTypeService().sGetZHZTs(iParentIDs, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtZHZT";
            dtSql.Rows.Add(drSql);

            sql = new GroupMatrixService().sGetAllGMValues(iParentIDs, NaveoModels.Zones);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtGMZone";
            dtSql.Rows.Add(drSql);
            #endregion

            dsProcess.Merge(dtSql);
            dsProcess.Merge(dtCtrl);
            dsResults = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResults.Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        [Obsolete]
        public Zone GetZoneByName(String Name, String strMatrixName, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"SELECT ZoneID, 
							 Description, 
							 Displayed, 
							 Comments, 
							 Color, ExternalRef, Ref2, isMarker, iBuffer from GFI_FLT_ZoneHeader
							 WHERE Description = ?
							 order by ZoneID";

            sql = @"select z.* from GFI_FLT_ZoneHeader z, GFI_SYS_GroupMatrixZone m, GFI_SYS_GroupMatrix gm
				    where m.GMID = gm.GMID and z.ZoneID = m.iID and z.Description = ? and gm.GMDescription = ?";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = Name + "¬" + strMatrixName;
            drSql["sTableName"] = "dtZone";
            dtSql.Rows.Add(drSql);

            sql = new MatrixService().sGetMatrixIdByField("GFI_FLT_ZoneHeader", "Description", Name, "ZoneID", "GFI_SYS_GroupMatrixZone");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "dtMatrix";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables["dtZone"].Rows.Count == 0)
                return null;
            else
                return GetZoneObsolete(ds, sConnStr);
        }
        [Obsolete]
        Zone GetZoneObsolete(DataSet ds, String sConnStr)
        {
            Zone z = new Zone();
            z.zoneID = Convert.ToInt32(ds.Tables["dtZone"].Rows[0]["ZoneID"]);
            z.description = ds.Tables["dtZone"].Rows[0]["Description"].ToString();
            z.displayed = Convert.ToInt32(ds.Tables["dtZone"].Rows[0]["Displayed"]);
            z.comments = ds.Tables["dtZone"].Rows[0]["Comments"].ToString();
            z.color = Convert.ToInt32(ds.Tables["dtZone"].Rows[0]["Color"]);
            z.externalRef = ds.Tables["dtZone"].Rows[0]["ExternalRef"].ToString();
            z.ref2 = ds.Tables["dtZone"].Rows[0]["Ref2"].ToString();
            z.isMarker = Convert.ToInt32(ds.Tables["dtZone"].Rows[0]["isMarker"]);
            z.iBuffer = Convert.ToInt32(ds.Tables["dtZone"].Rows[0]["iBuffer"]);
            z.zoneDetails = new ZoneDetailService().GetZoneDetailsByZoneId(z.zoneID, sConnStr);

            List<Matrix> lMatrix = new List<Matrix>();
            MatrixService ms = new MatrixService();
            foreach (DataRow dr in ds.Tables["dtMatrix"].Rows)
                lMatrix.Add(ms.AssignMatrix(dr));
            z.lMatrix = lMatrix;
            return z;
        }
        #endregion
    }
}