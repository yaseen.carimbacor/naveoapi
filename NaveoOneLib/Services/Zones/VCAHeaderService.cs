using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Zones;
using NaveoOneLib.Services.Audits;
using NaveoOneLib.Models.Common;

namespace NaveoOneLib.Services.Zones
{
    public class VCAHeaderService
    {
        Errors er = new Errors();
        public DataTable GetVCAHeader(String sConnStr)
        {
            String sqlString = @"SELECT VCAID,DistrictID,VCAName,iColor from GFI_GIS_VCA order by VCAID";
            //dbo.Geometry2Json(GeomData) JsonGeom
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        VCAHeader GetVCAHeader(DataRow dr, String sConnStr)
        {
            VCAHeader retVCAHeader = new VCAHeader();
            retVCAHeader.VCAID = Convert.ToInt32(dr["VCAID"]);
            retVCAHeader.Name = dr["Name"].ToString();
            retVCAHeader.Comments = dr["Comments"].ToString();
            retVCAHeader.Color = String.IsNullOrEmpty(dr["Color"].ToString()) ? (int?)null : Convert.ToInt32(dr["Color"]);
            retVCAHeader.ParentVCAID = String.IsNullOrEmpty(dr["ParentVCAID"].ToString()) ? (int?)null : Convert.ToInt32(dr["ParentVCAID"]);
            retVCAHeader.VCADetail = new VCADetailService().GetVCADetailsByVCAID(retVCAHeader.VCAID,sConnStr);

            return retVCAHeader;
        }
        public VCAHeader GetVCAHeaderById(int iID, String sConnStr)
        {
            String sqlString = @"SELECT * from GFI_GIS_VCA WHERE VCAID = ? order by VCAID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID.ToString(),sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetVCAHeader(dt.Rows[0], sConnStr);
        }
        public VCAHeader GetVCAHeaderByName(String sName, String sConnStr)
        {
            String sqlString = @"SELECT VCAID, Name, Comments, Color, ParentVCAID from GFI_FLT_VCAHeader WHERE Name = ? order by VCAID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, sName,sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetVCAHeader(dt.Rows[0], sConnStr);
        }


        public DataTable GetVCAByName(String sName, String sConnStr)
        {

            String sqlString = @"SELECT VCAID,NAME from GFI_FLT_VCAHeader WHERE Name ='" + sName + "' order by VCAID";

            DataTable dt = new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return dt;
        }


        bool isnull(object T)
        {
            return T == null ? true : false;
        }
        DataTable VCAHeaderDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_VCAHeader";
            dt.Columns.Add("VCAID", typeof(int));
            dt.Columns.Add("Name", typeof(String));
            dt.Columns.Add("Comments", typeof(String));
            dt.Columns.Add("Color", typeof(int));
            dt.Columns.Add("ParentVCAID", typeof(int));

            return dt;
        }
        public Boolean SaveVCAHeader(VCAHeader uVCAHeader, Boolean bInsert, String sConnStr)
        {
            DataTable dt = VCAHeaderDT();

            DataRow dr = dt.NewRow();
            dr["VCAID"] = uVCAHeader.VCAID;
            dr["Name"] = uVCAHeader.Name;
            dr["Comments"] = uVCAHeader.Comments;
            dr["Color"] = uVCAHeader.Color != null ? uVCAHeader.Color : (object)DBNull.Value;
            dr["ParentVCAID"] = uVCAHeader.ParentVCAID != null ? uVCAHeader.ParentVCAID : (object)DBNull.Value;
            dt.Rows.Add(dr);


            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "VCAID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uVCAHeader.VCAID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            if (!isnull(uVCAHeader.VCADetail))
            {
                List<VCADetail> lzd = new List<VCADetail>();
                DataTable dtZoneDetail = new myConverter().ListToDataTable<VCADetail>(lzd);
                dtZoneDetail.TableName = "GFI_FLT_VCADetail";
                drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 2;
                drCtrl["TblName"] = dtZoneDetail.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "iID";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdAction"] = "SET";
                drCtrl["NextIdFieldName"] = "VCAID";
                drCtrl["ParentTblName"] = dt.TableName;
                dtCtrl.Rows.Add(drCtrl);
                int i = 0;
                foreach (VCADetail zd in uVCAHeader.VCADetail)
                {
                    DataRow drzd = dtZoneDetail.NewRow();
                    drzd["iID"] = zd.iID;
                    drzd["Latitude"] = zd.Latitude;
                    drzd["Longitude"] = zd.Longitude;
                    drzd["VCAID"] = zd.VCAID;
                    drzd["CordOrder"] = i;
                    dtZoneDetail.Rows.Add(drzd);
                    i++;
                }
                dsProcess.Merge(dtZoneDetail);
            }

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public Boolean DeleteVCAHeader(VCAHeader uVCAHeader, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_FLT_VCAHeader where VCAID = " + uVCAHeader.VCAID.ToString();
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            DataTable dtAudit = new AuditService().LogTran(3, "VCAHeader", uVCAHeader.VCAID.ToString(), Globals.uLogin.UID, uVCAHeader.VCAID);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "CallerFunction";
            drCtrl["ParentTblName"] = "GFI_FLT_VCAHeader";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }

        public String sGetVCAInTree()
        {
            String sql = @"
WITH 
    parent AS 
    (
        SELECT distinct ParentVCAID
        FROM GFI_FLT_VCAHeader
    ), 
    tree AS 
    (
        SELECT 
            x.VCAID
            , x.Name
            , x.ParentVCAID
        FROM GFI_FLT_VCAHeader x
        INNER JOIN parent ON x.ParentVCAID = parent.ParentVCAID
        UNION ALL
        SELECT 
            y.VCAID
            , y.Name
            , y.ParentVCAID
        FROM GFI_FLT_VCAHeader y
        INNER JOIN tree t ON y.ParentVCAID = t.VCAID
    )
	
, DefaultTree AS ( 
        SELECT VCAID PGMID, 
                Name PGMDescription, 
                ParentVCAID PParentVCAID
        FROM GFI_FLT_VCAHeader

        UNION ALL 

        SELECT p.VCAID PGMID,
                p.Name PGMDescription,
                p.ParentVCAID PParentVCAID 
        FROM GFI_FLT_VCAHeader p
            JOIN DefaultTree t ON t.PParentVCAID = p.VCAID
    )

SELECT Distinct VCAID GMID, Name GMDescription, ParentVCAID ParentGMID, -2 StrucID , '' myStatus, -9 MID, -20 MatrixiID FROM tree
union all
    SELECT z.VCAID, z.Name, -1, -3 Struc, '' myStatus, -8 MID, -20 MatrixiID FROM GFI_FLT_VCAHeader z where ParentVCAID is null 
--union 
--	select -1, 'Root', null, 0, '', 0, 0
order by GMID";
            return sql;
        }
        public void ImportVCA(String sPath, String sConnStr)
        {
            DataTable dtH = Utils.ReadTxtFile(sPath, true);
            dtH.TableName = "ZoneHeader";
            String szd = sPath.Replace("ZoneHeader", "ZoneDetails");
            DataTable dtD = Utils.ReadTxtFile(szd, true);
            dtD.TableName = "ZoneDetails";

            VCAHeaderService vcaHeaderService = new VCAHeaderService();
            foreach (DataRow dr in dtH.Rows)
            {
                if (String.IsNullOrEmpty(dr["sDescription"].ToString()))
                    continue;

                VCAHeader v = new VCAHeader();
                v = vcaHeaderService.GetVCAHeaderByName(dr["sDescription"].ToString(), sConnStr);
                if (v != null)
                    continue;

                v = new VCAHeader();
                v.Name = dr["sDescription"].ToString();
                v.Color = 16777215;
                if(dtH.Columns.Contains("ParentVCAID"))
                    if(dr["ParentVCAID"].ToString().Length > 0)
                        v.ParentVCAID = Convert.ToInt32(dr["ParentVCAID"]);

                List<VCADetail> l = new List<VCADetail>();
                DataRow[] drd = dtD.Select("iZoneHeadID = '" + dr["iID"] + "'");
                foreach (DataRow d in drd)
                {
                    VCADetail zd = new VCADetail();
                    zd.Latitude = Convert.ToDouble(d["rLatitude"]);
                    zd.Longitude = Convert.ToDouble(d["rLongitude"]);
                    l.Add(zd);
                }
                if (l[l.Count - 1] != l[0])
                    l.Add(l[0]);

                v.VCADetail = l;
                Boolean b = vcaHeaderService.SaveVCAHeader(v, true, sConnStr);
            }
        }

        public Boolean SaveVCAHeader(VCAHeader uVCAHeader, String sConnStr)
        {
            DataTable dt = VCAHeaderDT();

            DataRow dr = dt.NewRow();
            dr["VCAID"] = uVCAHeader.VCAID;
            dr["Name"] = uVCAHeader.Name;
            dr["Comments"] = uVCAHeader.Comments;
            dr["Color"] = uVCAHeader.Color != null ? uVCAHeader.Color : (object)DBNull.Value;
            dr["ParentVCAID"] = uVCAHeader.ParentVCAID != null ? uVCAHeader.ParentVCAID : (object)DBNull.Value;
            dr["GeomData"] = uVCAHeader.GeomData != null ? uVCAHeader.GeomData : (object)DBNull.Value;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "VCAID";
            drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdAction"] = "GET";
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public Boolean DoesVCAExist(String name, String sConnStr)
        {
            String sqlString = @"SELECT NAME from GFI_FLT_VCAHeader WHERE Name ='" + name;

            DataTable dt = new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
            if (dt.Rows.Count == 0)
                return false;
            else
                return true;
        }

        public BaseModel SaveVCAFromShp(String shpPath, String sConnStr)
        {
            List<String> lResult = new List<string>();

            BaseModel bm = new BaseModel();
            List<String> lfeatureIDs = NaveoOneLib.Maps.NaveoWebMapsCall.GetAllFeatureIDs(shpPath);
            foreach (String id in lfeatureIDs)
            {
                List<KeyValuePair<string, string>> lFeatures = NaveoOneLib.Maps.NaveoWebMapsCall.GetAllFeaturesByID(shpPath, id);
                VCAHeader vca = new Models.Zones.VCAHeader();
                vca.Name = lFeatures.FirstOrDefault(x => x.Key == "NAME").Value.ToString();
                vca.GeomData = lFeatures.FirstOrDefault(x => x.Key == "GeomData").Value.ToString() == string.Empty ? null : lFeatures.FirstOrDefault(x => x.Key == "GeomData").Value;
                if (!new VCAHeaderService().DoesVCAExist(vca.Name, sConnStr))
                    new VCAHeaderService().SaveVCAHeader(vca, sConnStr);
            }

            lResult.Add("Features : " + lfeatureIDs.Count);
            bm.data = lResult;
            return bm;
        }
    }
}
