using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Zones;

namespace NaveoOneLib.Services.Zones
{
    public class ExtZonePropDetailsService
    {

        Errors er = new Errors();
        public DataTable GetExtZonePropDetails(String sConnStr)
        {
            String sqlString = @"SELECT UID,APL_ID, 
                                        BCM_CDATE, 
                                        BCM_CMT, 
                                        REF_DESC, BCM_SEQ from GFI_AMM_ExtZonePropDetails order by APL_ID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public int GetMaxSeqNo(String sConnStr)
        {
            String sqlString = @"SELECT max(BCM_SEQ) as maxseq from GFI_AMM_ExtZonePropDetails ";
            DataTable reDt = new Connection(sConnStr).GetDataDT(sqlString, sConnStr);

            int i = 0;
            Int32.TryParse(reDt.Rows[0]["maxseq"].ToString(), out i);
            return i;
        }

        public DataTable GetExtZonePropDetailsByAppID(String strAppid, String sConnStr)
        {
            String sqlString = @"SELECT APL_ID,
                                        BCM_CDATE, 
                                        BCM_CMT,
                                        BCM_DECISION,
                                        REF_DESC, BCM_SEQ from GFI_AMM_ExtZonePropDetails where APL_ID = '" + strAppid + "' order by BCM_CDATE desc";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        public DataTable GetExtZonePropDetailsLatestByAppID(String strAppid, String sConnStr)
        {
            String sqlString = @"SELECT top(1)                                       
                                        BCM_CMT, 
                                        BCM_DECISION , BCM_SEQ from GFI_AMM_ExtZonePropDetails where APL_ID = '" + strAppid + "' order by BCM_CDATE desc";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        public ExtZonePropDetail GetExtZonePropDetailsById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT UID,APL_ID, 
                                             BCM_CDATE, 
                                             BCM_CMT, 
                                             REF_DESC, BCM_SEQ from GFI_AMM_ExtZonePropDetails
                                             WHERE UID = ?
                                             order by APL_ID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                ExtZonePropDetail retExtZonePropDetails = new ExtZonePropDetail();
                retExtZonePropDetails.UID = Convert.ToInt32(dt.Rows[0]["UID"]);
                retExtZonePropDetails.APL_ID = dt.Rows[0]["APL_ID"].ToString();
                retExtZonePropDetails.BCM_CDATE = (DateTime)dt.Rows[0]["BCM_CDATE"];
                retExtZonePropDetails.BCM_CMT = dt.Rows[0]["BCM_CMT"].ToString();
                retExtZonePropDetails.REF_DESC = dt.Rows[0]["REF_DESC"].ToString();
                retExtZonePropDetails.BCM_SEQ = Convert.ToInt32(dt.Rows[0]["BCM_SEQ"]);
                return retExtZonePropDetails;
            }
        }

        public DataTable GetExtZonePropDetails(DateTime dtFrom, DateTime dtTo, String sConnStr)
        {

            String sqlString = @"SELECT UID,APL_ID, 
                                        BCM_CDATE, 
                                        BCM_CMT, 
                                        REF_DESC, BCM_SEQ from GFI_AMM_ExtZonePropDetails                                       
                                        WHERE BCM_CDATE BETWEEN '" + dtFrom + "' AND '" + dtTo + "'order by APL_ID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        public bool SaveExtZonePropDetails(ExtZonePropDetail uExtZonePropDetails, Boolean bInsert, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_ExtZonePropDetails";
            dt.Columns.Add("APL_ID", uExtZonePropDetails.APL_ID.GetType());
            dt.Columns.Add("BCM_CDATE", uExtZonePropDetails.BCM_CDATE.GetType());
            dt.Columns.Add("BCM_CMT", uExtZonePropDetails.BCM_CMT.GetType());
            dt.Columns.Add("REF_DESC", uExtZonePropDetails.REF_DESC.GetType());
            dt.Columns.Add("BCM_DECISION", uExtZonePropDetails.BCM_DECISION.GetType());
            dt.Columns.Add("BCM_SEQ", uExtZonePropDetails.BCM_SEQ.GetType());

            DataRow dr = dt.NewRow();
            dr["APL_ID"] = uExtZonePropDetails.APL_ID;
            dr["BCM_CDATE"] = uExtZonePropDetails.BCM_CDATE;
            dr["BCM_CMT"] = uExtZonePropDetails.BCM_CMT;
            dr["REF_DESC"] = uExtZonePropDetails.REF_DESC;
            dr["BCM_DECISION"] = uExtZonePropDetails.BCM_DECISION;
            dr["BCM_SEQ"] = uExtZonePropDetails.BCM_SEQ;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "UID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uExtZonePropDetails.UID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            //Data Snap Shot            
            DataTable dtSnapShot = new myConverter().ListToDataTable<DataSnapshot>(uExtZonePropDetails.lDataSnap);
            dtSnapShot.TableName = "GFI_PLA_DataSnapshot";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 2;
            drCtrl["TblName"] = dtSnapShot.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "UId";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "GroupingId";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtSnapShot);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public bool SaveExtZonePropDetailsNoSnapShot(ExtZonePropDetail uExtZonePropDetails, Boolean bInsert, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_ExtZonePropDetails";
            dt.Columns.Add("APL_ID", uExtZonePropDetails.APL_ID.GetType());
            dt.Columns.Add("BCM_CDATE", uExtZonePropDetails.BCM_CDATE.GetType());
            dt.Columns.Add("BCM_CMT", uExtZonePropDetails.BCM_CMT.GetType());
            dt.Columns.Add("REF_DESC", uExtZonePropDetails.REF_DESC.GetType());
            dt.Columns.Add("BCM_DECISION", uExtZonePropDetails.BCM_DECISION.GetType());
            dt.Columns.Add("BCM_SEQ", uExtZonePropDetails.BCM_SEQ.GetType());

            DataRow dr = dt.NewRow();
            dr["APL_ID"] = uExtZonePropDetails.APL_ID;
            dr["BCM_CDATE"] = uExtZonePropDetails.BCM_CDATE;
            dr["BCM_CMT"] = uExtZonePropDetails.BCM_CMT;
            dr["REF_DESC"] = uExtZonePropDetails.REF_DESC;
            dr["BCM_DECISION"] = uExtZonePropDetails.BCM_DECISION;
            dr["BCM_SEQ"] = uExtZonePropDetails.BCM_SEQ;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "UID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uExtZonePropDetails.UID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            ////Data Snap Shot            
            //DataTable dtSnapShot = new myConverter().ListToDataTable<DataSnapshot>(uExtZonePropDetails.lDataSnap);
            //dtSnapShot.TableName = "GFI_PLA_DataSnapshot";
            //drCtrl = dtCtrl.NewRow();
            //drCtrl["SeqNo"] = 2;
            //drCtrl["TblName"] = dtSnapShot.TableName;
            //drCtrl["CmdType"] = "TABLE";
            //drCtrl["KeyFieldName"] = "UId";
            //drCtrl["KeyIsIdentity"] = "TRUE";
            //drCtrl["NextIdAction"] = "SET";
            //drCtrl["NextIdFieldName"] = "GroupingId";
            //drCtrl["ParentTblName"] = dt.TableName;
            //dtCtrl.Rows.Add(drCtrl);
            //dsProcess.Merge(dtSnapShot);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }

        public bool UpdateExtZonePropDetails(ExtZonePropDetail uExtZonePropDetails, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_ExtZonePropDetails";
            dt.Columns.Add("APL_ID", uExtZonePropDetails.APL_ID.GetType());
            dt.Columns.Add("BCM_CDATE", uExtZonePropDetails.BCM_CDATE.GetType());
            dt.Columns.Add("BCM_CMT", uExtZonePropDetails.BCM_CMT.GetType());
            dt.Columns.Add("REF_DESC", uExtZonePropDetails.REF_DESC.GetType());
            dt.Columns.Add("BCM_SEQ", uExtZonePropDetails.BCM_SEQ.GetType());
            DataRow dr = dt.NewRow();
            dr["APL_ID"] = uExtZonePropDetails.APL_ID;
            dr["BCM_CDATE"] = uExtZonePropDetails.BCM_CDATE;
            dr["BCM_CMT"] = uExtZonePropDetails.BCM_CMT;
            dr["REF_DESC"] = uExtZonePropDetails.REF_DESC;
            dr["BCM_SEQ"] = uExtZonePropDetails.BCM_SEQ;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "UID = '" + uExtZonePropDetails.UID + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool DeleteExtZonePropDetails(ExtZonePropDetail uExtZonePropDetails, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_AMM_ExtZonePropDetails", "UID = '" + uExtZonePropDetails.UID + "'", sConnStr))
                return true;
            else
                return false;
        }
    }
}

