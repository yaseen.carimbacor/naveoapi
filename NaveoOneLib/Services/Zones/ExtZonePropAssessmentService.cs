

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Zones;

namespace NaveoOneLib.Services.Zones
{
    public class ExtZonePropAssessmentService
    {

        Errors er = new Errors();
        public DataTable GetExtZonePropAssessment(DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            String sqlString = @"SELECT  APL_ID,
            ASSESS_Code, 
            ASSESS_Date, 
            ASSESS_Detail, 
            ASSESS_Recommend, 
            ASSESS_Status, 
CreatedBy from GFI_AMM_ExtZonePropAssessment  WHERE ASSESS_Date BETWEEN '" + dtFrom + "' AND '" + dtTo + "'order by APL_ID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public ExtZonePropAssessment GetExtZonePropAssessmentById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT UID, 
APL_ID, 
ASSESS_Code, 
ASSESS_Date, 
ASSESS_Detail, 
ASSESS_Recommend, 
ASSESS_Status, 
CreatedBy, 
CreatedDate, 
UpdatedBy, 
UpdatedDate from GFI_AMM_ExtZonePropAssessment
WHERE TingPow = ?
order by TingPow";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                ExtZonePropAssessment retExtZonePropAssessment = new ExtZonePropAssessment();
                retExtZonePropAssessment.UID = Convert.ToInt32(dt.Rows[0]["UID"]);
                retExtZonePropAssessment.APL_ID = dt.Rows[0]["APL_ID"].ToString();
                retExtZonePropAssessment.ASSESS_Code = dt.Rows[0]["ASSESS_Code"].ToString();
                retExtZonePropAssessment.ASSESS_Date = (DateTime)dt.Rows[0]["ASSESS_Date"];
                retExtZonePropAssessment.ASSESS_Detail = dt.Rows[0]["ASSESS_Detail"].ToString();
                retExtZonePropAssessment.ASSESS_Recommend = dt.Rows[0]["ASSESS_Recommend"].ToString();
                retExtZonePropAssessment.ASSESS_Status = dt.Rows[0]["ASSESS_Status"].ToString();
                retExtZonePropAssessment.CreatedBy = dt.Rows[0]["CreatedBy"].ToString();
                retExtZonePropAssessment.CreatedDate = (DateTime)dt.Rows[0]["CreatedDate"];
                retExtZonePropAssessment.UpdatedBy = dt.Rows[0]["UpdatedBy"].ToString();
                retExtZonePropAssessment.UpdatedDate = (DateTime)dt.Rows[0]["UpdatedDate"];
                return retExtZonePropAssessment;
            }
        }

        public DataTable GetExtZonePropAssessmentByAPPLId(String iID, String sConnStr)
        {
            String sqlString = @"SELECT APL_Id,
            ASSESS_Code, 
            ASSESS_Date, 
            ASSESS_Detail, 
            ASSESS_Recommend, 
            ASSESS_Status, ASSESS_By,
            CreatedBy
         from GFI_AMM_ExtZonePropAssessment
            WHERE APL_ID = ?
            order by APL_ID asc, ASSESS_Date --desc";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            return dt;
        }
        public bool SaveExtZonePropAssessment(ExtZonePropAssessment uExtZonePropAssessment, Boolean bInsert, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_ExtZonePropAssessment";
            dt.Columns.Add("UID", uExtZonePropAssessment.UID.GetType());
            dt.Columns.Add("APL_ID", uExtZonePropAssessment.APL_ID.GetType());
            dt.Columns.Add("ASSESS_Code", uExtZonePropAssessment.ASSESS_Code.GetType());
            dt.Columns.Add("ASSESS_Date", uExtZonePropAssessment.ASSESS_Date.GetType());
            dt.Columns.Add("ASSESS_Detail", uExtZonePropAssessment.ASSESS_Detail.GetType());
            dt.Columns.Add("ASSESS_Recommend", uExtZonePropAssessment.ASSESS_Recommend.GetType());
            dt.Columns.Add("ASSESS_Status", uExtZonePropAssessment.ASSESS_Status.GetType());
            dt.Columns.Add("ASSESS_By", uExtZonePropAssessment.ASSESS_By.GetType());
            dt.Columns.Add("CreatedBy", uExtZonePropAssessment.CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", uExtZonePropAssessment.CreatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uExtZonePropAssessment.UpdatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uExtZonePropAssessment.UpdatedDate.GetType());

            DataRow dr = dt.NewRow();
            dr["UID"] = uExtZonePropAssessment.UID;
            dr["APL_ID"] = uExtZonePropAssessment.APL_ID;
            dr["ASSESS_Code"] = uExtZonePropAssessment.ASSESS_Code;
            dr["ASSESS_Date"] = uExtZonePropAssessment.ASSESS_Date;
            dr["ASSESS_Detail"] = uExtZonePropAssessment.ASSESS_Detail;
            dr["ASSESS_Recommend"] = uExtZonePropAssessment.ASSESS_Recommend;
            dr["ASSESS_Status"] = uExtZonePropAssessment.ASSESS_Status;
            dr["ASSESS_By"] = uExtZonePropAssessment.ASSESS_By;
            dr["CreatedBy"] = uExtZonePropAssessment.CreatedBy;
            dr["CreatedDate"] = uExtZonePropAssessment.CreatedDate;
            dr["UpdatedBy"] = uExtZonePropAssessment.UpdatedBy;
            dr["UpdatedDate"] = uExtZonePropAssessment.UpdatedDate;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "UID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uExtZonePropAssessment.UID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }


        public bool UpdateExtZonePropAssessment(ExtZonePropAssessment uExtZonePropAssessment, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_ExtZonePropAssessment";
            dt.Columns.Add("UID", uExtZonePropAssessment.UID.GetType());
            dt.Columns.Add("APL_ID", uExtZonePropAssessment.APL_ID.GetType());
            dt.Columns.Add("ASSESS_Code", uExtZonePropAssessment.ASSESS_Code.GetType());
            dt.Columns.Add("ASSESS_Date", uExtZonePropAssessment.ASSESS_Date.GetType());
            dt.Columns.Add("ASSESS_Detail", uExtZonePropAssessment.ASSESS_Detail.GetType());
            dt.Columns.Add("ASSESS_Recommend", uExtZonePropAssessment.ASSESS_Recommend.GetType());
            dt.Columns.Add("ASSESS_Status", uExtZonePropAssessment.ASSESS_Status.GetType());
            dt.Columns.Add("CreatedBy", uExtZonePropAssessment.CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", uExtZonePropAssessment.CreatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uExtZonePropAssessment.UpdatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uExtZonePropAssessment.UpdatedDate.GetType());
            DataRow dr = dt.NewRow();
            dr["UID"] = uExtZonePropAssessment.UID;
            dr["APL_ID"] = uExtZonePropAssessment.APL_ID;
            dr["ASSESS_Code"] = uExtZonePropAssessment.ASSESS_Code;
            dr["ASSESS_Date"] = uExtZonePropAssessment.ASSESS_Date;
            dr["ASSESS_Detail"] = uExtZonePropAssessment.ASSESS_Detail;
            dr["ASSESS_Recommend"] = uExtZonePropAssessment.ASSESS_Recommend;
            dr["ASSESS_Status"] = uExtZonePropAssessment.ASSESS_Status;
            dr["CreatedBy"] = uExtZonePropAssessment.CreatedBy;
            dr["CreatedDate"] = uExtZonePropAssessment.CreatedDate;
            dr["UpdatedBy"] = uExtZonePropAssessment.UpdatedBy;
            dr["UpdatedDate"] = uExtZonePropAssessment.UpdatedDate;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "UID = '" + uExtZonePropAssessment.UID + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool DeleteExtZonePropAssessment(ExtZonePropAssessment uExtZonePropAssessment, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_AMM_ExtZonePropAssessment", "UID = '" + uExtZonePropAssessment.UID + "'", sConnStr))
                return true;
            else
                return false;
        }

    }
}


