﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.Zones;
using NaveoOneLib.Services.Assets;

namespace NaveoOneLib.Services.Zones
{
    public class VisitedNotVisitedService
    {
        public DataSet GetDataDS(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, String sConnStr, Boolean bGPSDataDetail)
        {
            Boolean bChkArc = Globals.bNeedToQryArc(sConnStr, dtFrom);

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"
CREATE TABLE #Assets
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL,
	[iAssetID] [int] PRIMARY KEY CLUSTERED,
)

create table #GFI_GPS_GPSData 
(
	[UID] [int],
	[AssetID] [int] NULL,
	[DateTimeGPS_UTC] [datetime] NULL,
	[DateTimeServer] [datetime] NULL,
	[Longitude] [float] NULL,
	[Latitude] [float] NULL,
	[LongLatValidFlag] [int] NULL,
	[Speed] [float] NULL,
	[EngineOn] [int] NULL,
	[StopFlag] [int] NULL,
	[TripDistance] [float] NULL,
	[TripTime] [float] NULL,
	[WorkHour] [int] NULL,
	[DriverID] [int] NOT NULL,
)
";
            foreach (int i in lAssetID)
                sql += "insert #Assets (iAssetID) values (" + i.ToString() + ")\r\n";

            sql += @"
insert #GFI_GPS_GPSData
    select g.* 
    from GFI_GPS_GPSData g
        inner join #Assets a on g.AssetID = a.iAssetID
    where DateTimeGPS_UTC >= ? and DateTimeGPS_UTC <= ?
";

            if (bChkArc)
            {
                sql += @"
insert #GFI_GPS_GPSData
    select g.* 
    from GFI_ARC_GPSData g
        inner join #Assets a on g.AssetID = a.iAssetID
    where DateTimeGPS_UTC >= ? and DateTimeGPS_UTC <= ?
";
            }

            sql += " select * from #GFI_GPS_GPSData";
            String strParams = dtFrom.ToString() + "¬" + dtTo.ToString();
            if (bChkArc)
                strParams += "¬" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");
            
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = strParams;
            drSql["sTableName"] = "GPSData";
            dtSql.Rows.Add(drSql);

            if (bGPSDataDetail)
            {
                sql = "select d.* from #GFI_GPS_GPSData g inner join GFI_GPS_GPSDataDetail d on g.UID = d.UID ";
                if (bChkArc)
                    sql = "union select d.* from #GFI_GPS_GPSData g inner join GFI_ARC_GPSDataDetail d on g.UID = d.UID ";

                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sParam"] = strParams;
                drSql["sTableName"] = "GPSDataDetail";
                dtSql.Rows.Add(drSql);
            }
            else
            {
                sql = "select * from GFI_GPS_GPSData where 1 = 0";
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "GPSDataDetail";
                dtSql.Rows.Add(drSql);
            }

            sql = new AssetService().sGetAsset(lAssetID, sConnStr);
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "GPSAsset";
            dtSql.Rows.Add(drSql);

            return c.GetDataDS(dtSql,sConnStr);
        }
        DataTable GetZonesVisitedAndNot(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, List<Zone> MyZones, String sConnStr)
        {
            DataTable dtResult = new DataTable("TripDetailsTable");
            dtResult.Columns.Add("AssetID");
            dtResult.Columns.Add("ZoneName");
            dtResult.Columns.Add("DateTime", System.Type.GetType("System.DateTime"));
            dtResult.Columns.Add("Latitude", typeof(float));
            dtResult.Columns.Add("Longitude", typeof(float));
            dtResult.Columns.Add("Status");
            dtResult.Columns.Add("ZoneID", typeof(int));

            dtTo = dtTo.Date.AddDays(1).AddSeconds(-1);
            TimeZone localZone = TimeZone.CurrentTimeZone;
            TimeSpan ts = new Utils().GetTimeSpan(TimeZone.CurrentTimeZone.StandardName);

            //Detect SortieZone
            int iInZone = 0;
            String InZone = String.Empty;
            Boolean bInZone = false;

            DataSet ds = GetDataDS(lAssetID, dtFrom, dtTo,sConnStr, false);
            ZoneService zoneService = new ZoneService();
            foreach (int iAssetID in lAssetID)
            {
                DataRow[] ldr = ds.Tables["GPSData"].Select("AssetID = " + iAssetID.ToString());
                foreach (DataRow dr in ldr)
                {
                    DataRow drLogRecord = dtResult.NewRow();
                    drLogRecord["AssetID"] = dr["AssetID"];
                    drLogRecord["DateTime"] = Convert.ToDateTime(dr["DateTimeGPS_UTC"]).AddSeconds(ts.TotalSeconds).ToString("dd-MMM-yyyy HH:mm:ss tt");
                    drLogRecord["Latitude"] = dr["Latitude"];
                    drLogRecord["Longitude"] = dr["Longitude"];

                    if (!bInZone)
                        iInZone++;

                    foreach (Zone z in MyZones)
                    {
                        if (zoneService.IsPointInside(z, new Coordinate((Double)dr["Latitude"], (Double)dr["Longitude"]), sConnStr))
                        {
                            if (z.description != InZone)
                                iInZone++;

                            bInZone = true;
                            InZone = z.description;

                            drLogRecord["Status"] = iInZone.ToString();
                            drLogRecord["ZoneName"] = z.description;
                            drLogRecord["ZoneID"] = z.zoneID;
                            dtResult.Rows.Add(drLogRecord);
                            break;
                        }
                        else
                            bInZone = false;
                    }
                }
            }
            return dtResult;
        }

        public DataTable LoadReport(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, List<int> lZoneType, String sConnStr, Boolean bIncludeNotVisited)
        {
            List<Zone> MyZones = new List<Zone>();
            foreach (Zone z in Globals.lZone)
                foreach (ZoneType zt in z.lZoneType)
                    foreach (int i in lZoneType)
                        if (zt.ZoneTypeID == i)
                            if (!MyZones.Exists(o => o.zoneID == z.zoneID))
                                MyZones.Add(z);

            DataTable dtLog = GetZonesVisitedAndNot(lAssetID, dtFrom, dtTo, MyZones,sConnStr);

            DataTable dtResult = new DataTable("TripDetailsTable");
            dtResult.Columns.Add("Device");
            dtResult.Columns.Add("Zone");
            dtResult.Columns.Add("DateTimeIn", System.Type.GetType("System.DateTime"));
            dtResult.Columns.Add("DateTimeOut", System.Type.GetType("System.DateTime"));
            dtResult.Columns.Add("InZoneDuration",typeof(TimeSpan));
            dtResult.Columns.Add("ZoneID", typeof(int));
            dtResult.Columns.Add("HeatID", typeof(int));

            DataView view = new DataView(dtLog);
            DataTable distinctValues = view.ToTable(true, "Status");
            foreach (DataRow drdv in distinctValues.Rows)
            {
                DataRow[] d = dtLog.Select("Status = '" + drdv[0] + "'", "Status Desc");
                int iCount = 0;
                DateTime dtiFr = new DateTime();
                DateTime dtiTo = new DateTime();
                foreach (DataRow dr in d)
                {
                    if (iCount == 0)
                        dtiFr = Convert.ToDateTime(dr["DateTime"]);
                    else if (iCount == d.Length - 1)
                    {
                        dtiTo = Convert.ToDateTime(dr["DateTime"]);
                        DataRow myDR = dtResult.NewRow();

                        String strMyAsset = String.Empty;
                        foreach(Asset a in Globals.lAsset)
                            if (a.AssetID.ToString() == dr["AssetID"].ToString())
                            {
                                strMyAsset = a.AssetNumber;
                                break;
                            }
                        myDR["Device"] = strMyAsset;
                        myDR["Zone"] = dr["ZoneName"];
                        myDR["DateTimeIn"] = dtiFr;
                        myDR["DateTimeOut"] = dtiTo;
                        myDR["InZoneDuration"] = (dtiTo - dtiFr);
                        myDR["ZoneID"] = dr["ZoneID"];
                        myDR["HeatID"] = System.Drawing.Color.DarkSeaGreen.ToArgb();

                        dtResult.Rows.Add(myDR.ItemArray);
                    }
                    iCount++;
                }
            }

            //Zones NotVisited
            if (bIncludeNotVisited)
            {
                DataView v = new DataView(dtResult);
                DataTable distinctZones = v.ToTable(true, "Zone");
                foreach (Zone z in MyZones)
                {
                    Boolean bFound = false;
                    foreach (DataRow dr in distinctZones.Rows)
                        if (z.description == (String)dr["Zone"])
                        {
                            bFound = true;
                            break;
                        }

                    if (!bFound)
                    {
                        DataRow myDR = dtResult.NewRow();

                        myDR["Device"] = "Not Visited";
                        myDR["Zone"] = z.description;
                        myDR["ZoneID"] = z.zoneID;
                        myDR["Duration"] = String.Empty;
                        myDR["HeatID"] = System.Drawing.Color.DarkTurquoise.ToArgb();
                        dtResult.Rows.Add(myDR.ItemArray);
                    }
                }
            }
            return dtResult;
        }
        public DataTable Rume(String sConnStr)
        {
            List<int> i = new List<int>();
            i.Add(1);

            DateTime dtf = new DateTime(2014, 09, 18, 20, 00, 00);
            DateTime dtt = new DateTime(2014, 09, 19, 20, 00, 00);

            List<int> z = new List<int>();
            z.Add(25);

            return LoadReport(i, dtf, dtt, z, sConnStr, true);
        }
    }
}