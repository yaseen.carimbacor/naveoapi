using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Zones;

namespace NaveoOneLib.Services.Zones
{
    public class ExtZonePropService
    {

        Errors er = new Errors();
        public DataTable GetExtZoneProp(String sConnStr)
        {
            String sqlString = @"SELECT IID,APL_ID, 
                                             APL_NAME, 
                                             APL_FNAME, 
                                             APL_DATE, 
                                             APL_EFF_DATE, 
                                             APL_LOC_PLAN,
                                             APL_ADDR1, 
                                             APL_ADDR2, 
                                             APL_ADDR3, 
                                             APL_TV_NO, 
                                             APL_PROP_DEVP, 
                                             APL_EXTENT, 
                                             Link_Pdf, 
                                             TYPE, 
                                             BUFFERSIZE, 
                                             POINT_X, 
                                             POINT_Y, Status from GFI_AMM_ExtZoneProp  order by APL_ID ";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public DataTable GetExtZonePropDateLimit(String strDateLimit, String sConnStr)
        {
            // String strDateLimit 
            String sqlString = @"SELECT IID,APL_ID, 
                                             APL_NAME, 
                                             APL_FNAME, 
                                             APL_DATE, 
                                             APL_EFF_DATE, 
                                             APL_LOC_PLAN,
                                             APL_ADDR1, 
                                             APL_ADDR2, 
                                             APL_ADDR3, 
                                             APL_TV_NO, 
                                             APL_PROP_DEVP, 
                                             APL_EXTENT, 
                                             Link_Pdf, 
                                             TYPE, 
                                             BUFFERSIZE, 
                                             POINT_X, 
                                             POINT_Y, Status from GFI_AMM_ExtZoneProp where APL_DATE > '" + strDateLimit + "' order by APL_ID ";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public DataTable GetAPLIDExtZoneProp(string sIID, String sConnStr)
        {
            String sqlString = @"SELECT APL_ID from GFI_AMM_ExtZoneProp WHERE  IID ='" + sIID + "' order by APL_ID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        public DataTable GetAllInTree(String sConnStr)
        {
            String sqlString = @"SELECT -4 as GMID, 
                                          CONCAT( APL_ID, ' - ', APL_NAME, ' - ', APL_FNAME ) as GMDescription,
                                             CASE substring(APL_ID,1,2) 
                                                WHEN 'CO' then 2
                                                WHEN 'RE' THEN 3
                                                WHEN 'MO' THEN 4
                                                WHEN 'BL' THEN 4   --PL
                                                WHEN 'OP' THEN 5
                                                WHEN 'SE' THEN 6
                                                WHEN 'SG' THEN 7
                                                WHEN 'WA' THEN 8
                                                WHEN 'IN' THEN 9
                                                ELSE 1
                                             END as ParentGMID, 
                                             IID as StrucID, 
                                             'AC' as MyStatus, 
                                             IID as MID,
                                             IID as MatriXID
                                           from GFI_AMM_ExtZoneProp 
                                  UNION 
                                        Select 1 as GMID,'ALL PLANNINGS' As GMDescription,-1 as ParentGMID,
                                             0 as StrucID, 
                                             'AC' as MyStatus, 
                                             0 as MID,
                                             0 as MatriXID
                                  UNION
                                        Select 2 as GMID,'COMMERCIAL' As GMDescription,1 as ParentGMID,
                                             0 as StrucID, 
                                             'AC' as MyStatus, 
                                             0 as MID,
                                             0 as MatriXID
                                  UNION
                                       Select 3 as GMID,'RESIDENTIAL' As GMDescription,1 as ParentGMID,
                                             0 as StrucID, 
                                             'AC' as MyStatus, 
                                             0 as MID,
                                             0 as MatriXID
                                  UNION
                                        Select 4 as GMID,'MORCELLEMNT' As GMDescription,1 as ParentGMID,
                                             0 as StrucID, 
                                             'AC' as MyStatus, 
                                             0 as MID,
                                             0 as MatriXID
                                  UNION
                                             Select 5 as GMID,'OPP' As GMDescription,1 as ParentGMID,
                                             0 as StrucID, 
                                             'AC' as MyStatus, 
                                             0 as MID,
                                             0 as MatriXID
                                  UNION
                                       Select 6 as GMID,'SERV' As GMDescription,1 as ParentGMID,
                                             0 as StrucID, 
                                             'AC' as MyStatus, 
                                             0 as MID,
                                             0 as MatriXID
                                  UNION
                                        Select 7 as GMID,'SG' As GMDescription,1 as ParentGMID,
                                             0 as StrucID, 
                                             'AC' as MyStatus, 
                                             0 as MID,
                                             0 as MatriXID
                                  UNION
                                        Select 8 as GMID,'WALL' As GMDescription,1 as ParentGMID,
                                             0 as StrucID, 
                                             'AC' as MyStatus, 
                                             0 as MID,
                                             0 as MatriXID
                                  UNION
                                        Select 9 as GMID,'INDUSTRIAL' As GMDescription,1 as ParentGMID,
                                             0 as StrucID, 
                                             'AC' as MyStatus, 
                                             0 as MID,
                                             0 as MatriXID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public DataTable GetAllInTreeDateTrim(String dtFrom, String dtTo, String sConnStr)
        {
            String sqlString = @"SELECT -4 as GMID, 
                                     CONCAT( a.APL_ID, ' - ', APL_NAME, ' - ', APL_FNAME ) as GMDescription,
                                         CASE substring(a.APL_ID,1,2) 
                                         WHEN 'CO' then 2
                                         WHEN 'RE' THEN 3
                                         WHEN 'MO' THEN 4
                                         WHEN 'BL' THEN 4   --PL
                                         WHEN 'OP' THEN 5
                                         WHEN 'SE' THEN 6
                                         WHEN 'SG' THEN 7
                                         WHEN 'WA' THEN 8
                                         WHEN 'IN' THEN 9
                                         ELSE 1
                                         END as ParentGMID, 
                                         IID as StrucID, 
                                         'AC' as MyStatus, 
                                         IID as MID,
                                         IID as MatriXID
                                     from GFI_AMM_ExtZoneProp a 
                                        --left outer join GFI_AMM_ExtZonePropDetails b on a.APL_ID = b.APL_ID
                                        inner join GFI_AMM_ExtZonePropDetails b on a.APL_ID = b.APL_ID
			                         where ((b.BCM_CDATE between '" + dtFrom + "' and  '" + dtTo + "')) -- or b.BCM_CDATE is null) "
                                  + @"
                                  UNION 
                                        Select 1 as GMID,'ALL PLANNINGS' As GMDescription,-1 as ParentGMID,
                                             0 as StrucID, 
                                             'AC' as MyStatus, 
                                             0 as MID,
                                             0 as MatriXID
                                  UNION
                                        Select 2 as GMID,'COMMERCIAL' As GMDescription,1 as ParentGMID,
                                             0 as StrucID, 
                                             'AC' as MyStatus, 
                                             0 as MID,
                                             0 as MatriXID
                                  UNION
                                       Select 3 as GMID,'RESIDENTIAL' As GMDescription,1 as ParentGMID,
                                             0 as StrucID, 
                                             'AC' as MyStatus, 
                                             0 as MID,
                                             0 as MatriXID
                                  UNION
                                        Select 4 as GMID,'MORCELLEMNT' As GMDescription,1 as ParentGMID,
                                             0 as StrucID, 
                                             'AC' as MyStatus, 
                                             0 as MID,
                                             0 as MatriXID
                                  UNION
                                             Select 5 as GMID,'OPP' As GMDescription,1 as ParentGMID,
                                             0 as StrucID, 
                                             'AC' as MyStatus, 
                                             0 as MID,
                                             0 as MatriXID
                                  UNION
                                       Select 6 as GMID,'SERV' As GMDescription,1 as ParentGMID,
                                             0 as StrucID, 
                                             'AC' as MyStatus, 
                                             0 as MID,
                                             0 as MatriXID
                                  UNION
                                        Select 7 as GMID,'SG' As GMDescription,1 as ParentGMID,
                                             0 as StrucID, 
                                             'AC' as MyStatus, 
                                             0 as MID,
                                             0 as MatriXID
                                  UNION
                                        Select 8 as GMID,'WALL' As GMDescription,1 as ParentGMID,
                                             0 as StrucID, 
                                             'AC' as MyStatus, 
                                             0 as MID,
                                             0 as MatriXID
                                  UNION
                                        Select 9 as GMID,'INDUSTRIAL' As GMDescription,1 as ParentGMID,
                                             0 as StrucID, 
                                             'AC' as MyStatus, 
                                             0 as MID,
                                             0 as MatriXID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public DataTable GetAllInTree(String strWhereClause, String sConnStr)
        {
            String sqlString = @"SELECT IID,APL_ID, 
                                             APL_NAME, 
                                             APL_FNAME, 
                                             APL_DATE, 
                                             APL_EFF_DATE, 
                                             APL_LOC_PLAN, 
                                             APL_ADDR1, 
                                             APL_ADDR2, 
                                             APL_ADDR3, 
                                             APL_TV_NO, 
                                             APL_PROP_DEVP, 
                                             APL_EXTENT, 
                                             Link_Pdf, 
                                             TYPE, 
                                             BUFFERSIZE, 
                                             POINT_X, 
                                             POINT_Y,Status from GFI_AMM_ExtZoneProp  where iid in (" + strWhereClause + ") order by APL_ID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);

        }
        public DataTable GetAllInTree(String strWhereClause, DateTime dtfrom, DateTime dtto, String sConnStr)
        {
            string sqlString = @"
            ;with cte as
            (
	            select APL_ID, BCM_CDATE, 
	            row_number() over(partition by APL_ID order by BCM_CDATE desc) as RowNum
	            from GFI_AMM_ExtZonePropDetails
	            where BCM_CDATE between '" + dtfrom.ToString("dd/MMM/yyyy HH:mm:ss tt") + "' and '" + dtto.ToString("dd/MMM/yyyy HH:mm:ss tt") + "')" +
            " SELECT " +
            "IID,GFI_AMM_ExtZoneProp.APL_ID,  " +
            "APL_NAME, " +
            "APL_FNAME, " +
            "APL_DATE, " +
            "APL_EFF_DATE, " +
            "APL_ADDR1, " +
            "APL_ADDR2, " +
            "APL_ADDR3, " +
            "APL_LOC_PLAN, " +
            "APL_TV_NO, " +
            "APL_PROP_DEVP, " +
            "APL_EXTENT, " +
            "Link_Pdf, " +
            "TYPE, " +
            "BUFFERSIZE," +
            "POINT_X, " +
            "POINT_Y,Status,BCM_CDATE, rownum" +
            " from GFI_AMM_ExtZoneProp" +
            " inner join cte on GFI_AMM_ExtZoneProp.APL_ID = cte.APL_ID" +
            " where rownum = 1 and  iid in (" + strWhereClause + ") order by APL_ID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);

        }

        public DataTable GetAllInTreeUnmapped(String sConnStr)
        {
            String sqlString = @"SELECT IID,APL_ID, 
                                             APL_NAME, 
                                             APL_FNAME, 
                                             APL_DATE, 
                                             APL_EFF_DATE, 
                                             APL_ADDR1, 
                                             APL_ADDR2, 
                                             APL_ADDR3, 
                                             APL_LOC_PLAN, 
                                             APL_TV_NO, 
                                             APL_PROP_DEVP, 
                                             APL_EXTENT, 
                                             Link_Pdf, 
                                             TYPE, 
                                             BUFFERSIZE, 
                                             POINT_X, 
                                             POINT_Y, status from GFI_AMM_ExtZoneProp where POINT_X = -999 or POINT_Y = -999  order by APL_DATE Desc";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);

        }
        public DataTable GetAllInTreeMapped(String Excluding, String sConnStr)
        {
            String sqlString = @"SELECT IID,APL_ID, 
                                             APL_NAME, 
                                             APL_FNAME, 
                                             APL_DATE, 
                                             APL_EFF_DATE, 
                                             APL_ADDR1, 
                                             APL_ADDR2, 
                                             APL_ADDR3, 
                                             APL_LOC_PLAN, 
                                             APL_TV_NO, 
                                             APL_PROP_DEVP, 
                                             APL_EXTENT, 
                                             Link_Pdf, 
                                             TYPE, 
                                             BUFFERSIZE, 
                                             POINT_X, 
                                             POINT_Y, Status from GFI_AMM_ExtZoneProp where (POINT_X is not null or POINT_Y is not null ) and APL_ID <> '" + Excluding + "'  order by APL_DATE Desc";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);

        }

        public DataTable GetAllAssess(String sConnStr)
        {
            String sql = "select Replace(APL_ID, '-', '/') APL_ID, ASSESS_SEQ from GFI_AMM_ExtZonePropAssessment";
            return new Connection(sConnStr).GetDataDT(sql, sConnStr);
        }


        ExtZoneProp myExtZoneProp(DataRow dr)
        {
            ExtZoneProp retExtZoneProp = new ExtZoneProp();
            retExtZoneProp.IID = Convert.ToInt32(dr["IID"]);
            retExtZoneProp.APL_ID = dr["APL_ID"].ToString();
            retExtZoneProp.APL_NAME = dr["APL_NAME"].ToString();
            retExtZoneProp.APL_FNAME = dr["APL_FNAME"].ToString();
            retExtZoneProp.APL_DATE = (DateTime)dr["APL_DATE"];
            retExtZoneProp.APL_EFF_DATE = (DateTime)dr["APL_EFF_DATE"];
            retExtZoneProp.APL_ADDR1 = dr["APL_ADDR1"].ToString();
            retExtZoneProp.APL_ADDR2 = dr["APL_ADDR2"].ToString();
            retExtZoneProp.APL_ADDR3 = dr["APL_ADDR3"].ToString();
            retExtZoneProp.APL_LOC_PLAN = dr["APL_LOC_PLAN"].ToString();
            retExtZoneProp.APL_TV_NO = dr["APL_TV_NO"].ToString();
            retExtZoneProp.APL_PROP_DEVP = dr["APL_PROP_DEVP"].ToString();

            if (Convert.ToString(dr["APL_EXTENT"]) == "")
                retExtZoneProp.APL_EXTENT = 0;
            else
                retExtZoneProp.APL_EXTENT = Convert.ToDouble(Convert.ToString(dr["APL_EXTENT"]));

            retExtZoneProp.Link_Pdf = dr["Link_Pdf"].ToString();
            retExtZoneProp.TYPE = dr["TYPE"].ToString();
            //retExtZoneProp.BUFFERSIZE = (float)dr["BUFFERSIZE"];
            //retExtZoneProp.POINT_X = (float)dr["POINT_X"];
            //retExtZoneProp.POINT_Y = (float)dr["POINT_Y"];
            retExtZoneProp.STATUS = dr["Status"].ToString();
            return retExtZoneProp;
        }
        public ExtZoneProp GetExtZonePropById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT IID,APL_ID, 
                                             APL_NAME, 
                                             APL_FNAME, 
                                             APL_DATE, 
                                             APL_EFF_DATE,
                                             APL_LOC_PLAN,  
                                             APL_ADDR1, 
                                             APL_ADDR2, 
                                             APL_ADDR3, 

                                             APL_TV_NO, 
                                             APL_PROP_DEVP, 
                                             APL_EXTENT, 
                                             Link_Pdf, 
                                             TYPE, 
                                             BUFFERSIZE, 
                                             POINT_X, 
                                             POINT_Y,Status from GFI_AMM_ExtZoneProp
                                             WHERE IID = ?
                                             order by IID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID.ToString(), sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return myExtZoneProp(dt.Rows[0]);
        }
        public ExtZoneProp GetExtZonePropByAPLId(String iID, String sConnStr)
        {
            String sqlString = @"SELECT IID,APL_ID, 
                                             APL_NAME, 
                                             APL_FNAME, 
                                             APL_DATE, 
                                             APL_EFF_DATE,
                                              APL_LOC_PLAN,
                                             APL_ADDR1, 
                                             APL_ADDR2, 
                                             APL_ADDR3, 

                                             APL_TV_NO, 
                                             APL_PROP_DEVP, 
                                             APL_EXTENT, 
                                             Link_Pdf, 
                                             TYPE, 
                                             BUFFERSIZE, 
                                             POINT_X, 
                                             POINT_Y, status from GFI_AMM_ExtZoneProp
                                             WHERE APL_ID = ?
                                             order by IID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID.ToString(), sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return myExtZoneProp(dt.Rows[0]);
        }

        public bool SaveExtZoneProp(List<ExtZoneProp> uExtZoneProp, Boolean bInsert, String sConnStr)
        {
            //DataTable dt = new DataTable();
            //dt.TableName = "GFI_AMM_ExtZoneProp";
            //dt.Columns.Add("APL_ID", uExtZoneProp[0].APL_ID.GetType());
            //dt.Columns.Add("APL_NAME", uExtZoneProp[0].APL_NAME.GetType());
            //dt.Columns.Add("APL_FNAME", uExtZoneProp[0].APL_FNAME.GetType());
            //dt.Columns.Add("APL_DATE", uExtZoneProp[0].APL_DATE.GetType());
            //dt.Columns.Add("APL_EFF_DATE", uExtZoneProp[0].APL_EFF_DATE.GetType());
            //dt.Columns.Add("APL_ADDR1", uExtZoneProp[0].APL_ADDR1.GetType());
            //dt.Columns.Add("APL_ADDR2", uExtZoneProp[0].APL_ADDR2.GetType());
            //dt.Columns.Add("APL_ADDR3", uExtZoneProp[0].APL_ADDR3.GetType());
            //dt.Columns.Add("APL_LOC_PLAN", uExtZoneProp[0].APL_LOC_PLAN.GetType());
            //dt.Columns.Add("APL_TV_NO", uExtZoneProp[0].APL_TV_NO.GetType());
            //dt.Columns.Add("APL_PROP_DEVP", uExtZoneProp[0].APL_PROP_DEVP.GetType());
            //dt.Columns.Add("APL_EXTENT", uExtZoneProp[0].APL_EXTENT.GetType());

            //foreach (ExtZoneProp z in uExtZoneProp)
            //{
            //    DataRow dr = dt.NewRow();
            //    dr["APL_ID"] = z.APL_ID;
            //    dr["APL_NAME"] = z.APL_NAME;
            //    dr["APL_FNAME"] = z.APL_FNAME;
            //    dr["APL_DATE"] = z.APL_DATE;
            //    dr["APL_EFF_DATE"] = z.APL_EFF_DATE;
            //    dr["APL_ADDR1"] = z.APL_ADDR1;
            //    dr["APL_ADDR2"] = z.APL_ADDR2;
            //    dr["APL_ADDR3"] = z.APL_ADDR3;
            //    dr["APL_LOC_PLAN"] = z.APL_LOC_PLAN;
            //    dr["APL_TV_NO"] = z.APL_TV_NO;
            //    dr["APL_PROP_DEVP"] = z.APL_PROP_DEVP;
            //    dr["APL_EXTENT"] = z.APL_EXTENT;
            //    dt.Rows.Add(dr);
            //}

            DataTable dt = new myConverter().ListToDataTable<ExtZoneProp>(uExtZoneProp);
            dt.TableName = "GFI_AMM_ExtZoneProp";

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "IID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uExtZoneProp[0].IID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);


            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public bool SaveExtZonePropOra(ExtZoneProp u, List<ExtZoneProp> uExtZoneProp, List<ExtZonePropAssessment> lExtZonePropAssessment, List<ExtZonePropDetailsOra> lExtZonePropDetail, Boolean bInsert, String sConnStr)
        {
            DataTable dt = new myConverter().ListToDataTable<ExtZoneProp>(uExtZoneProp);
            dt.TableName = "GFI_AMM_ExtZoneProp";

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "IID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uExtZoneProp[0].IID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);


            DataTable dtAssessment = new myConverter().ListToDataTable<ExtZonePropAssessment>(lExtZonePropAssessment);
            dtAssessment.TableName = "GFI_AMM_ExtZonePropAssessment";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 2;
            drCtrl["TblName"] = dtAssessment.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "UID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "UID";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAssessment);

            DataTable dtCommittee = new myConverter().ListToDataTable<ExtZonePropDetailsOra>(lExtZonePropDetail);
            dtCommittee.TableName = "GFI_AMM_ExtZonePropDetails";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 3;
            drCtrl["TblName"] = dtCommittee.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "UID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "UID";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtCommittee);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }

        public bool UpdateExtZoneProp(ExtZoneProp uExtZoneProp, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_ExtZoneProp";
            dt.Columns.Add("APL_ID", uExtZoneProp.APL_ID.GetType());
            dt.Columns.Add("APL_NAME", uExtZoneProp.APL_NAME.GetType());
            dt.Columns.Add("APL_FNAME", uExtZoneProp.APL_FNAME.GetType());
            dt.Columns.Add("APL_DATE", uExtZoneProp.APL_DATE.GetType());
            dt.Columns.Add("APL_EFF_DATE", uExtZoneProp.APL_EFF_DATE.GetType());
            dt.Columns.Add("APL_ADDR1", uExtZoneProp.APL_ADDR1.GetType());
            dt.Columns.Add("APL_ADDR2", uExtZoneProp.APL_ADDR2.GetType());
            dt.Columns.Add("APL_ADDR3", uExtZoneProp.APL_ADDR3.GetType());
            dt.Columns.Add("APL_LOC_PLAN", uExtZoneProp.APL_LOC_PLAN.GetType());
            dt.Columns.Add("APL_TV_NO", uExtZoneProp.APL_TV_NO.GetType());
            dt.Columns.Add("APL_PROP_DEVP", uExtZoneProp.APL_PROP_DEVP.GetType());
            dt.Columns.Add("APL_EXTENT", uExtZoneProp.APL_EXTENT.GetType());
            dt.Columns.Add("Link_Pdf", uExtZoneProp.Link_Pdf.GetType());
            dt.Columns.Add("TYPE", uExtZoneProp.TYPE.GetType());
            dt.Columns.Add("BUFFERSIZE", uExtZoneProp.BUFFERSIZE.GetType());
            dt.Columns.Add("POINT_X", uExtZoneProp.POINT_X.GetType());
            dt.Columns.Add("POINT_Y", uExtZoneProp.POINT_Y.GetType());
            //dt.Columns.Add("STATUS", uExtZoneProp.POINT_Y.GetType());
            DataRow dr = dt.NewRow();
            dr["APL_ID"] = uExtZoneProp.APL_ID;
            dr["APL_NAME"] = uExtZoneProp.APL_NAME;
            dr["APL_FNAME"] = uExtZoneProp.APL_FNAME;
            dr["APL_DATE"] = uExtZoneProp.APL_DATE;
            dr["APL_EFF_DATE"] = uExtZoneProp.APL_EFF_DATE;
            dr["APL_ADDR1"] = uExtZoneProp.APL_ADDR1;
            dr["APL_ADDR2"] = uExtZoneProp.APL_ADDR2;
            dr["APL_ADDR3"] = uExtZoneProp.APL_ADDR3;
            dr["APL_LOC_PLAN"] = uExtZoneProp.APL_LOC_PLAN;
            dr["APL_TV_NO"] = uExtZoneProp.APL_TV_NO;
            dr["APL_PROP_DEVP"] = uExtZoneProp.APL_PROP_DEVP;
            dr["APL_EXTENT"] = uExtZoneProp.APL_EXTENT;
            dr["Link_Pdf"] = uExtZoneProp.Link_Pdf;
            dr["TYPE"] = uExtZoneProp.TYPE;
            dr["BUFFERSIZE"] = uExtZoneProp.BUFFERSIZE;
            dr["POINT_X"] = uExtZoneProp.POINT_X;
            dr["POINT_Y"] = uExtZoneProp.POINT_Y;
            // dr["STATUS"] = uExtZoneProp.STATUS != string.Empty ? Convert.ToDouble(uExtZoneProp.STATUS) : 0;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "IID = '" + uExtZoneProp.IID + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }

        public bool UpdateExtZoneProp(ExtZoneProp uExtZoneProp, bool bInsert, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_AMM_ExtZoneProp";
            dt.Columns.Add("APL_ID", uExtZoneProp.APL_ID.GetType());
            dt.Columns.Add("APL_NAME", uExtZoneProp.APL_NAME.GetType());
            dt.Columns.Add("APL_FNAME", uExtZoneProp.APL_FNAME.GetType());
            dt.Columns.Add("APL_DATE", uExtZoneProp.APL_DATE.GetType());
            dt.Columns.Add("APL_EFF_DATE", uExtZoneProp.APL_EFF_DATE.GetType());
            dt.Columns.Add("APL_ADDR1", uExtZoneProp.APL_ADDR1.GetType());
            dt.Columns.Add("APL_ADDR2", uExtZoneProp.APL_ADDR2.GetType());
            dt.Columns.Add("APL_ADDR3", uExtZoneProp.APL_ADDR3.GetType());
            dt.Columns.Add("APL_LOC_PLAN", uExtZoneProp.APL_LOC_PLAN.GetType());
            dt.Columns.Add("APL_TV_NO", uExtZoneProp.APL_TV_NO.GetType());
            dt.Columns.Add("APL_PROP_DEVP", uExtZoneProp.APL_PROP_DEVP.GetType());
            dt.Columns.Add("APL_EXTENT", uExtZoneProp.APL_EXTENT.GetType());
            dt.Columns.Add("Link_Pdf", uExtZoneProp.Link_Pdf.GetType());
            dt.Columns.Add("TYPE", uExtZoneProp.TYPE.GetType());
            dt.Columns.Add("BUFFERSIZE", uExtZoneProp.BUFFERSIZE.GetType());
            dt.Columns.Add("POINT_X", uExtZoneProp.POINT_X.GetType());
            dt.Columns.Add("POINT_Y", uExtZoneProp.POINT_Y.GetType());
            dt.Columns.Add("STATUS", uExtZoneProp.POINT_Y.GetType());
            // dt.Columns.Add("oprType", uExtZoneProp.oprType.GetType());

            DataRow dr = dt.NewRow();
            dr["APL_ID"] = uExtZoneProp.APL_ID;
            dr["APL_NAME"] = uExtZoneProp.APL_NAME;
            dr["APL_FNAME"] = uExtZoneProp.APL_FNAME;
            dr["APL_DATE"] = uExtZoneProp.APL_DATE;
            dr["APL_EFF_DATE"] = uExtZoneProp.APL_EFF_DATE;
            dr["APL_ADDR1"] = uExtZoneProp.APL_ADDR1;
            dr["APL_ADDR2"] = uExtZoneProp.APL_ADDR2;
            dr["APL_ADDR3"] = uExtZoneProp.APL_ADDR3;
            dr["APL_LOC_PLAN"] = uExtZoneProp.APL_LOC_PLAN;
            dr["APL_TV_NO"] = uExtZoneProp.APL_TV_NO;
            dr["APL_PROP_DEVP"] = uExtZoneProp.APL_PROP_DEVP;
            dr["APL_EXTENT"] = uExtZoneProp.APL_EXTENT;
            dr["Link_Pdf"] = uExtZoneProp.Link_Pdf;
            dr["TYPE"] = uExtZoneProp.TYPE;
            dr["BUFFERSIZE"] = uExtZoneProp.BUFFERSIZE;
            dr["POINT_X"] = uExtZoneProp.POINT_X;
            dr["POINT_Y"] = uExtZoneProp.POINT_Y;
            dr["STATUS"] = uExtZoneProp.STATUS != string.Empty ? Convert.ToDouble(uExtZoneProp.STATUS) : 0;
            //dr["oprType"] = uExtZoneProp.oprType;
            dt.Rows.Add(dr);


            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AssetId";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uExtZoneProp.IID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public bool DeleteExtZoneProp(ExtZoneProp uExtZoneProp, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_AMM_ExtZoneProp", "IID = '" + uExtZoneProp.IID + "'", sConnStr))
                return true;
            else
                return false;
        }
    }
}

