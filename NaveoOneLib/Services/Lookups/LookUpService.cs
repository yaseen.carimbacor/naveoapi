using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Lookups;

namespace NaveoOneLib.Services.Lookups
{
    public class LookUpService
    {

        Errors er = new Errors();
        public DataTable GetLookUp(String sConnStr)
        {
            String sqlString = @"SELECT LookUpId, 
                                    LookUpCode, 
                                    LookUpDesc, 
                                    LookUpParentID, 
                                    CreatedBy, 
                                    CreatedDate, 
                                    UpdatedBy, 
                                    UpdatedDate from GFI_SYS_LookUp order by LookUpId";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public LookUp GetLookUpById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT LookUpId, 
                                    LookUpCode, 
                                    LookUpDesc, 
                                    LookUpParentID, 
                                    CreatedBy, 
                                    CreatedDate, 
                                    UpdatedBy, 
                                    UpdatedDate from GFI_SYS_LookUp
                                    WHERE LookUpId = ?
                                    order by LookUpId";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID,sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                LookUp retLookUp = new LookUp();
                retLookUp.LookUpId = Convert.ToInt32(dt.Rows[0]["LookUpId"]);
                retLookUp.LookUpCode = dt.Rows[0]["LookUpCode"].ToString();
                retLookUp.LookUpDesc = dt.Rows[0]["LookUpDesc"].ToString();
                retLookUp.LookUpParentID = Convert.ToInt32(dt.Rows[0]["LookUpParentID"]);
                retLookUp.CreatedBy = dt.Rows[0]["CreatedBy"].ToString();
                retLookUp.CreatedDate = (DateTime)dt.Rows[0]["CreatedDate"];
               
                
                //retLookUp.UpdatedBy = dt.Rows[0]["UpdatedBy"].ToString();
                //retLookUp.UpdatedDate = (DateTime)dt.Rows[0]["UpdatedDate"];
                return retLookUp;
            }
        }
        public bool SaveLookUp(LookUp uLookUp, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_LookUp";
            dt.Columns.Add("LookUpId", uLookUp.LookUpId.GetType());
            dt.Columns.Add("LookUpCode", uLookUp.LookUpCode.GetType());
            dt.Columns.Add("LookUpDesc", uLookUp.LookUpDesc.GetType());
            dt.Columns.Add("LookUpParentID", uLookUp.LookUpParentID.GetType());
            dt.Columns.Add("CreatedBy", uLookUp.CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", uLookUp.CreatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uLookUp.UpdatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uLookUp.UpdatedDate.GetType());
            DataRow dr = dt.NewRow();
            dr["LookUpId"] = uLookUp.LookUpId;
            dr["LookUpCode"] = uLookUp.LookUpCode;
            dr["LookUpDesc"] = uLookUp.LookUpDesc;
            dr["LookUpParentID"] = uLookUp.LookUpParentID;
            dr["CreatedBy"] = uLookUp.CreatedBy;
            dr["CreatedDate"] = uLookUp.CreatedDate;
            dr["UpdatedBy"] = uLookUp.UpdatedBy;
            dr["UpdatedDate"] = uLookUp.UpdatedDate;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenInsert(dt, transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool UpdateLookUp(LookUp uLookUp, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_LookUp";
            dt.Columns.Add("LookUpId", uLookUp.LookUpId.GetType());
            dt.Columns.Add("LookUpCode", uLookUp.LookUpCode.GetType());
            dt.Columns.Add("LookUpDesc", uLookUp.LookUpDesc.GetType());
            dt.Columns.Add("LookUpParentID", uLookUp.LookUpParentID.GetType());
            dt.Columns.Add("CreatedBy", uLookUp.CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", uLookUp.CreatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uLookUp.UpdatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uLookUp.UpdatedDate.GetType());
            DataRow dr = dt.NewRow();
            dr["LookUpId"] = uLookUp.LookUpId;
            dr["LookUpCode"] = uLookUp.LookUpCode;
            dr["LookUpDesc"] = uLookUp.LookUpDesc;
            dr["LookUpParentID"] = uLookUp.LookUpParentID;
            dr["CreatedBy"] = uLookUp.CreatedBy;
            dr["CreatedDate"] = uLookUp.CreatedDate;
            dr["UpdatedBy"] = uLookUp.UpdatedBy;
            dr["UpdatedDate"] = uLookUp.UpdatedDate;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "LookUpId = '" + uLookUp.LookUpId + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool DeleteLookUp(LookUp uLookUp, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_SYS_LookUp", "LookUpId = '" + uLookUp.LookUpId + "'", sConnStr))
                return true;
            else
                return false;
        }
    }
}