using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Lookups;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Services.GMatrix;
using NaveoOneLib.Services.Audits;

namespace NaveoOneLib.Services.Lookups
{
    public class LookUpValuesService
    {
        Errors er = new Errors();

        public DataTable GetLookUpValues(String sConnStr)
        {
            String sqlString = @"SELECT VID, 
TID, 
Name, 
Description, 
Value, 
CreatedDate, 
UpdatedDate, 
CreatedBy, 
UpdatedBy, 
RootMatrixID from GFI_SYS_LookUpValues order by VID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        LookUpValues GetLookUpValues(DataRow dr)
        {
            LookUpValues retLookUpValues = new LookUpValues();
            retLookUpValues.VID = Convert.ToInt32(dr["VID"]);
            retLookUpValues.TID = Convert.ToInt32(dr["TID"]);
            retLookUpValues.Name = dr["Name"].ToString();
            retLookUpValues.Description = dr["Description"].ToString();
            retLookUpValues.Value = String.IsNullOrEmpty(dr["Value"].ToString()) ? 0.0 : Convert.ToDouble(dr["Value"]);
            retLookUpValues.Value2 = String.IsNullOrEmpty(dr["Value2"].ToString()) ? 0.0 : Convert.ToDouble(dr["Value2"]);
            retLookUpValues.CreatedDate = String.IsNullOrEmpty(dr["CreatedDate"].ToString()) ? (DateTime?)null : (DateTime)dr["CreatedDate"];
            retLookUpValues.UpdatedDate = String.IsNullOrEmpty(dr["UpdatedDate"].ToString()) ? (DateTime?)null : (DateTime)dr["UpdatedDate"];
            retLookUpValues.CreatedBy = String.IsNullOrEmpty(dr["CreatedBy"].ToString()) ? (int?)null : Convert.ToInt32(dr["CreatedBy"]);
            retLookUpValues.UpdatedBy = String.IsNullOrEmpty(dr["UpdatedBy"].ToString()) ? (int?)null : Convert.ToInt32(dr["UpdatedBy"]);
            retLookUpValues.RootMatrixID = String.IsNullOrEmpty(dr["RootMatrixID"].ToString()) ? (int?)null : Convert.ToInt32(dr["RootMatrixID"]);
            return retLookUpValues;
        }
        public LookUpValues GetLookUpValuesById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT VID, 
TID, 
Name, 
Description, 
Value, 
Value2,
CreatedDate, 
UpdatedDate, 
CreatedBy, 
UpdatedBy, 
RootMatrixID from GFI_SYS_LookUpValues
WHERE VID = ?
order by VID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetLookUpValues(dt.Rows[0]);
        }
        public LookUpValues GetLookUpValuesByName(String sName, String sConnStr)
        {
            String sqlString = @"SELECT * from GFI_SYS_LookUpValues WHERE name = ? order by VID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, sName,sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetLookUpValues(dt.Rows[0]);
        }

        public DataTable GetLookUpValues(List<Matrix> lMatrix, String sConnStr)
        {
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lMatrix)
                iParentIDs.Add(m.GMID);

            return GetLookUpValues(iParentIDs, sConnStr);
        }
        public DataTable GetLookUpValues(List<int> iParentIDs, String sConnStr)
        {
            String sql = new GroupMatrixService().sGetAllParentsfromGMID(iParentIDs[0]);
            sql = sql.Replace("SELECT * FROM tree", "SELECT * from GFI_SYS_LookUpValues where RootMatrixID is null");
            sql += "\r\n union \r\n";
            sql += @"SELECT t1.* from GFI_SYS_LookUpValues t1 
	                    inner join tree t2 on t1.RootMatrixID = t2.GMID";
            return new Connection(sConnStr).GetDataDT(sql, sConnStr);
        }

        public DataTable GetLookUpValuesByLookUpTypeName(String sName, String sConnStr)
        {
            String sqlString = @"SELECT lT.TID,lT.Code, 
lV.VID, 
lV.Name  from  GFI_SYS_LookUpTypes lT 
inner join  GFI_SYS_LookUpValues lV 
ON lT.TID = lV.TID
WHERE lT.Code =?
order by VID";

            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, sName, sConnStr);
            return dt;
        }


        public DataTable GetLookUpValuesByLTName(String sName, String sConnStr)
        {
            String sqlString = @"SELECT lv.*  from  GFI_SYS_LookUpTypes lT 
inner join  GFI_SYS_LookUpValues lV 
ON lT.TID = lV.TID
WHERE lT.Code =?
order by VID";

            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, sName, sConnStr);
            return dt;
        }

        DataTable LookUpValuesDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_LookUpValues";
            dt.Columns.Add("VID", typeof(int));
            dt.Columns.Add("TID", typeof(int));
            dt.Columns.Add("Name", typeof(String));
            dt.Columns.Add("Description", typeof(String));
            dt.Columns.Add("Value", typeof(double));
            dt.Columns.Add("Value2", typeof(double));
            dt.Columns.Add("CreatedDate", typeof(DateTime));
            dt.Columns.Add("UpdatedDate", typeof(DateTime));
            dt.Columns.Add("CreatedBy", typeof(int));
            dt.Columns.Add("UpdatedBy", typeof(int));
            dt.Columns.Add("RootMatrixID", typeof(int));

            return dt;
        }
        public Boolean SaveLookUpValues(LookUpValues uLookUpValues, Boolean bInsert, String sConnStr)
        {
            DataTable dt = LookUpValuesDT();

            DataRow dr = dt.NewRow();
            dr["VID"] = uLookUpValues.VID;
            dr["TID"] = uLookUpValues.TID;
            dr["Name"] = uLookUpValues.Name;
            dr["Description"] = uLookUpValues.Description;
            dr["Value"] = String.IsNullOrEmpty(uLookUpValues.Value.ToString()) ? 0.0 : Convert.ToDouble(uLookUpValues.Value);
            dr["Value2"] = String.IsNullOrEmpty(uLookUpValues.Value2.ToString()) ? 0.0 : Convert.ToDouble(uLookUpValues.Value2);
            dr["CreatedDate"] = uLookUpValues.CreatedDate != null ? uLookUpValues.CreatedDate : (object)DBNull.Value;
            dr["UpdatedDate"] = uLookUpValues.UpdatedDate != null ? uLookUpValues.UpdatedDate : (object)DBNull.Value;
            dr["CreatedBy"] = uLookUpValues.CreatedBy != null ? uLookUpValues.CreatedBy : (object)DBNull.Value;
            dr["UpdatedBy"] = uLookUpValues.UpdatedBy != null ? uLookUpValues.UpdatedBy : (object)DBNull.Value;
            dr["RootMatrixID"] = uLookUpValues.RootMatrixID != null ? uLookUpValues.RootMatrixID : (object)DBNull.Value;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "VID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uLookUpValues.VID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public Boolean DeleteLookUpValues(LookUpValues uLookUpValues, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_SYS_LookUpValues where VID = " + uLookUpValues.VID.ToString();
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            DataTable dtAudit = new AuditService().LogTran(3, "LookUpValues", uLookUpValues.VID.ToString(), Globals.uLogin.UID, uLookUpValues.VID);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "CallerFunction";
            drCtrl["ParentTblName"] = "GFI_SYS_LookUpValues";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
    }
}