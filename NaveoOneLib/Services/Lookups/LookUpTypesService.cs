using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Lookups;
using NaveoOneLib.Services.Audits;

namespace NaveoOneLib.Services.Lookups
{
    public class LookUpTypesService
    {
        Errors er = new Errors();

        public DataTable GetLookUpTypes(String sConnStr)
        {
            String sqlString = @"SELECT TID, 
Code, 
Description, 
CreatedDate, 
UpdatedDate, 
CreatedBy, 
UpdatedBy from GFI_SYS_LookUpTypes order by TID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }


        LookUpTypes GetLookUpTypes(DataRow dr)
        {
            LookUpTypes retLookUpTypes = new LookUpTypes();
            retLookUpTypes.TID = Convert.ToInt32(dr["TID"]);
            retLookUpTypes.Code = dr["Code"].ToString();
            retLookUpTypes.Description = dr["Description"].ToString();
            retLookUpTypes.CreatedDate = String.IsNullOrEmpty(dr["CreatedDate"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dr["CreatedDate"]);
            retLookUpTypes.UpdatedDate =String.IsNullOrEmpty(dr["UpdatedDate"].ToString()) ? (DateTime?)null : Convert.ToDateTime(dr["UpdatedDate"]);
            retLookUpTypes.CreatedBy = String.IsNullOrEmpty(dr["CreatedBy"].ToString()) ? (int?)null : Convert.ToInt32(dr["CreatedBy"]);
            retLookUpTypes.UpdatedBy = String.IsNullOrEmpty(dr["UpdatedBy"].ToString()) ? (int?)null : Convert.ToInt32(dr["UpdatedBy"]);
            return retLookUpTypes;
        }

        public LookUpTypes GetLookUpTypesById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT TID, 
Code, 
Description, 
CreatedDate, 
UpdatedDate, 
CreatedBy, 
UpdatedBy from GFI_SYS_LookUpTypes
WHERE TID = ?
order by TID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID,sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetLookUpTypes(dt.Rows[0]);
        }
        public LookUpTypes GetLookUpTypesByCode(String Code, String sConnStr)
        {
            String sqlString = @"SELECT TID, 
                                    Code, 
                                    Description, 
                                    CreatedDate, 
                                    UpdatedDate, 
                                    CreatedBy, 
                                    UpdatedBy from GFI_SYS_LookUpTypes
                                    WHERE Code = ?";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, Code,sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetLookUpTypes(dt.Rows[0]);
        }

        DataTable LookUpTypesDT()
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_SYS_LookUpTypes";
            dt.Columns.Add("TID", typeof(int));
            dt.Columns.Add("Code", typeof(String));
            dt.Columns.Add("Description", typeof(String));
            dt.Columns.Add("CreatedDate", typeof(DateTime));
            dt.Columns.Add("UpdatedDate", typeof(DateTime));
            dt.Columns.Add("CreatedBy", typeof(int));
            dt.Columns.Add("UpdatedBy", typeof(int));

            return dt;
        }
        public Boolean SaveLookUpTypes(LookUpTypes uLookUpTypes, Boolean bInsert, String sConnStr)
        {
            DataTable dt = LookUpTypesDT();

            DataRow dr = dt.NewRow();
            dr["TID"] = uLookUpTypes.TID;
            dr["Code"] = uLookUpTypes.Code;
            dr["Description"] = uLookUpTypes.Description;
            dr["CreatedDate"] = uLookUpTypes.CreatedDate != null ? uLookUpTypes.CreatedDate : (object)DBNull.Value;
            dr["UpdatedDate"] = uLookUpTypes.UpdatedDate != null ? uLookUpTypes.UpdatedDate : (object)DBNull.Value;
            dr["CreatedBy"] = uLookUpTypes.CreatedBy != null ? uLookUpTypes.CreatedBy : (object)DBNull.Value;
            dr["UpdatedBy"] = uLookUpTypes.UpdatedBy != null ? uLookUpTypes.UpdatedBy : (object)DBNull.Value;
            dt.Rows.Add(dr);


            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "TID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uLookUpTypes.TID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
        public Boolean DeleteLookUpTypes(LookUpTypes uLookUpTypes, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = "None";
            drCtrl["CmdType"] = "STOREDPROC";
            drCtrl["TimeOut"] = 1000;
            String sql = "delete from GFI_SYS_LookUpTypes where TID = " + uLookUpTypes.TID.ToString();
            drCtrl["Command"] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            DataTable dtAudit = new AuditService().LogTran(3, "LookUpTypes", uLookUpTypes.TID.ToString(), Globals.uLogin.UID, uLookUpTypes.TID);
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 100;
            drCtrl["TblName"] = dtAudit.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "AuditID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "CallerFunction";
            drCtrl["ParentTblName"] = "GFI_SYS_LookUpTypes";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables["CTRLTBL"];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
    }
}
