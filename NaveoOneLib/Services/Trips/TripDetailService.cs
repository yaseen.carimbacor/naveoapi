﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Trips;
using NaveoOneLib.Models.GPS;

namespace NaveoOneLib.Services.Trips
{
    class TripDetailService
    {

        Errors er = new Errors();
        public DataTable GetTripDetail(String sConnStr)
        {
            String sqlString = @"SELECT iID, 
                                    HeaderiID, 
                                    GPSDataUID from GFI_GPS_TripDetail order by iID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public TripDetail GetTripDetailById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT iID, 
                                    HeaderiID, 
                                    GPSDataUID from GFI_GPS_TripDetail
                                    WHERE iID = ?
                                    order by iID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID,sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                TripDetail retTripDetail = new TripDetail();
                retTripDetail.iID = Convert.ToInt32(dt.Rows[0]["iID"]);
                retTripDetail.HeaderiID = Convert.ToInt32(dt.Rows[0]["HeaderiID"]);
                retTripDetail.GPSDataUID = Convert.ToInt32(dt.Rows[0]["GPSDataUID"]);
                return retTripDetail;
            }
        }
        public List<TripDetail> GetTripDetailByHeader(String iID, String sConnStr)
        {
            String sql = @"SELECT * from GFI_GPS_TripDetail td, dbo.GFI_GPS_GPSData g
                                where HeaderiID = ? and td.GPSDataUID = g.UID
                              order by g.DateTimeGPS_UTC";

            DataTable dt = new Connection(sConnStr).GetDataTableBy(sql, iID, sConnStr);
            List<TripDetail> l = new List<TripDetail>();
            foreach (DataRow dr in dt.Rows)
                l.Add(GetTripDetail(dr));

            return l;
        }
        public List<TripDetail> GetTripDetailsByHeaderID(List<int> TripID, String sConnStr)
        {
            String sqlTripID = @"
CREATE TABLE #TripID
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL,
	[iTripID] [int] PRIMARY KEY CLUSTERED,
)
";
            foreach (int i in TripID)
                sqlTripID += "insert #TripID (iTripID) values (" + i.ToString() + ")\r\n";

            String sql = sqlTripID + @"
select * from GFI_GPS_TripDetail td
	inner join #TripID tid on td.HeaderiID = tid.iTripID
	inner join GFI_GPS_GPSData g on td.GPSDataUID = g.UID
order by tid.iTripID, g.DateTimeGPS_UTC";

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "TripDetail";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            List<TripDetail> l = new List<TripDetail>();
            foreach (DataRow dr in ds.Tables[0].Rows)
                l.Add(GetTripDetail(dr));

            return l;
        }

        public TripDetail GetTripDetail(DataRow dr)
        {
            TripDetail td = new TripDetail();
            td.iID = Convert.ToInt32( dr["iID"]);
            td.HeaderiID = Convert.ToInt32(dr["HeaderiID"]);
            td.GPSDataUID = Convert.ToInt32(dr["GPSDataUID"]);

            GPSData retGPSData = new GPSData();
            retGPSData.UID = Convert.ToInt32(dr["UID"]);
            retGPSData.AssetID = Convert.ToInt32(dr["AssetID"]);
            retGPSData.DateTimeGPS_UTC = (DateTime)dr["DateTimeGPS_UTC"];
            retGPSData.DateTimeServer = (DateTime)dr["DateTimeServer"];
            retGPSData.Longitude = (double)dr["Longitude"];
            retGPSData.Latitude = (double)dr["Latitude"];
            retGPSData.Speed = Convert.ToDouble(dr["Speed"].ToString());
            retGPSData.EngineOn = Convert.ToInt16(dr["EngineOn"].ToString());
            retGPSData.StopFlag = Convert.ToInt16(dr["StopFlag"].ToString());
            retGPSData.TripDistance = Convert.ToDouble(dr["TripDistance"].ToString());
            retGPSData.TripTime = Convert.ToDouble(dr["TripTime"].ToString());
            retGPSData.WorkHour = Convert.ToInt16(dr["WorkHour"].ToString());

            td.GpsData = retGPSData;

            return td;
        }
        public Boolean SaveTripDetail(TripDetail uTripDetail, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_GPS_TripDetail";
            dt.Columns.Add("HeaderiID", uTripDetail.HeaderiID.GetType());
            dt.Columns.Add("GPSDataUID", uTripDetail.GPSDataUID.GetType());
            DataRow dr = dt.NewRow();
            dr["HeaderiID"] = uTripDetail.HeaderiID;
            dr["GPSDataUID"] = uTripDetail.GPSDataUID;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenInsert(dt, transaction, sConnStr))
                return true;
            else
                return false;
        }

        public bool UpdateTripDetail(TripDetail uTripDetail, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_GPS_TripDetail";
            dt.Columns.Add("iID", uTripDetail.iID.GetType());
            dt.Columns.Add("HeaderiID", uTripDetail.HeaderiID.GetType());
            dt.Columns.Add("GPSDataUID", uTripDetail.GPSDataUID.GetType());
            DataRow dr = dt.NewRow();
            dr["iID"] = uTripDetail.iID;
            dr["HeaderiID"] = uTripDetail.HeaderiID;
            dr["GPSDataUID"] = uTripDetail.GPSDataUID;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "iID = '" + uTripDetail.iID + "'", transaction,sConnStr))
                return true;
            else
                return false;
        }
        public bool DeleteTripDetail(TripDetail uTripDetail, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_GPS_TripDetail", "iID = '" + uTripDetail.iID + "'", sConnStr))
                return true;
            else
                return false;
        }
    }
}