

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Trips;

namespace NaveoOneLib.Services.Trips
{
    public class ExtTripInfoResourcesService
    {
        Errors er = new Errors();

        public DataSet GetExtTripInfoResourcesByTripId(String iID, string dtFrom, string dtTo, String sConnStr)
        {
            DataSet ds = new DataSet();
            String sqlString = @" select      
            h.IID, 
           
			a.AssetName,
            h.ExtTripID,             
            h.Field1Name, 
            t1.Field1Label as attribute1,          
            h.Field1Value as attribute1_Value,              
            t1.Field1Type as Field1Type,   
            t1.sDataSource as sDataSource1,                     
            h.Field2Name,  
            t2.Field1Label as attribute2,                   
            h.Field2Value as attribute2_Value,             
            t2.Field1Type as Field2Type,            
            t2.sDataSource as sDataSource2,         
            h.Field3Name ,
            t3.Field1Label as attribute3,                   
            h.Field3Value as attribute3_Value,             
            t3.Field1Type as Field3Type,
            t3.sDataSource as sDataSource3, 
            h.Field4Name , 
            t4.Field1Label as attribute4,                   
            h.Field4Value as attribute4_Value,             
            t4.Field1Type as Field4Type,            
            t4.sDataSource as sDataSource4, 
            h.Field5Name , 
            t5.Field1Label as attribute5,                   
            h.Field5Value as attribute5_Value,             
            t5.Field1Type as Field5Type,            
            t5.sDataSource as sDataSource5, 
            h.Field6Name , 
            t6.Field1Label as attribute6,                   
            h.Field6Value as attribute6_Value,            
            t6.Field1Type as Field6Type,            
            t6.sDataSource as sDataSource6, 
            AdditionalValue1,             
            AdditionalField1 ,      
            AdditionalValue2,
            AdditionalField2,                                   
            
            DateTimeGPSStart_UTC + t7.UTCOffset as StartDateTime,
            DateTimeGPSStop_UTC + t7.UTCOffset as StopDateTime,
             convert(varchar(10),convert(int,(Timediff))/3600) +'h:'+ 
             convert(varchar(50),convert(int,(Timediff))%3600/60)+'m:'+
             convert(varchar(50),convert(int,(Timediff))%60)+'s' as TimeDiff,  
            Consumption as Consumption,
            ROUND(h.TripDistance,1) TripDistance             
            from GFI_FLT_ExtTripInfo h
            
            left outer join GFI_FLT_Asset a on h.AssetName = a.AssetID 
            left outer join GFI_FLT_ExtTripInfoType t1 on h.Field1Name = t1.IID 
            left outer join GFI_FLT_ExtTripInfoType t2 on h.Field2Name = t2.IID 
            left outer join GFI_FLT_ExtTripInfoType t3 on h.Field3Name = t3.IID
            left outer join GFI_FLT_ExtTripInfoType t4 on h.Field4Name = t4.IID  
            left outer join GFI_FLT_ExtTripInfoType t5 on h.Field5Name = t5.IID  
            left outer join GFI_FLT_ExtTripInfoType t6 on h.Field6Name = t6.IID 
            left outer join GFI_SYS_TimeZones t7 on t7.StandardName = a.TimeZoneID
            WHERE h.IID in  (" + iID + @")   
            order by IID";

            DataTable dt = new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
            ds.Tables.Add(dt);

            sqlString = @"select  NaaCodeValue as Name,ExtTripID	       	
	        from GFI_FLT_ExtTripInfoResources";
            dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            ds.Tables.Add(dt);
            return ds;
        }

        public DataTable GetExtCodeByNaaCode(String sConnStr)
        {
            String sqlString = @"SELECT iID,
                Convert(nvarchar(50),GFI_SYS_NaaAddress.LastName)+'-'+Convert(nvarchar(50),GFI_SYS_NaaAddress.OtherName) 
                as NaaName from GFI_HRM_EmpBase ,GFI_SYS_NaaAddress 
                where GFI_HRM_EmpBase.NaaCode = GFI_SYS_NaaAddress.iID
                order by EmpCode";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }


        public DataTable GetExtTripInfoResources(String sConnStr)
        {
            String sqlString = @"SELECT EmpCode, 
            NaaCodeValue, 
            ExtTripID from GFI_FLT_ExtTripInfoResources order by TingPow";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public ExtTripInfoResource GetExtTripInfoResourcesById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT EmpCode, 
            NaaCodeValue, 
            ExtTripID from GFI_FLT_ExtTripInfoResources
            WHERE TingPow = ?
            order by TingPow";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                ExtTripInfoResource retExtTripInfoResources = new ExtTripInfoResource();
                retExtTripInfoResources.EmpCode = Convert.ToInt32(dt.Rows[0]["EmpCode"].ToString());
                retExtTripInfoResources.NaaCodeValue = dt.Rows[0]["NaaCodeValue"].ToString();
                retExtTripInfoResources.ExtTripID = Convert.ToInt32(dt.Rows[0]["ExtTripID"].ToString());
                return retExtTripInfoResources;
            }
        }
        public bool SaveExtTripInfoResources(ExtTripInfoResource uExtTripInfoResources, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_ExtTripInfoResources";
            dt.Columns.Add("EmpCode", uExtTripInfoResources.EmpCode.GetType());
            dt.Columns.Add("NaaCodeValue", uExtTripInfoResources.NaaCodeValue.GetType());
            dt.Columns.Add("ExtTripID", uExtTripInfoResources.ExtTripID.GetType());
            DataRow dr = dt.NewRow();
            dr["EmpCode"] = uExtTripInfoResources.EmpCode;
            dr["NaaCodeValue"] = uExtTripInfoResources.NaaCodeValue;
            dr["ExtTripID"] = uExtTripInfoResources.ExtTripID;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenInsert(dt, transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool UpdateExtTripInfoResources(ExtTripInfoResource uExtTripInfoResources, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_ExtTripInfoResources";
            dt.Columns.Add("EmpCode", uExtTripInfoResources.EmpCode.GetType());
            dt.Columns.Add("NaaCodeValue", uExtTripInfoResources.NaaCodeValue.GetType());
            dt.Columns.Add("ExtTripID", uExtTripInfoResources.ExtTripID.GetType());
            DataRow dr = dt.NewRow();
            dr["EmpCode"] = uExtTripInfoResources.EmpCode;
            dr["NaaCodeValue"] = uExtTripInfoResources.NaaCodeValue;
            dr["ExtTripID"] = uExtTripInfoResources.ExtTripID;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "TingPow = '" + uExtTripInfoResources.EmpCode + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool DeleteExtTripInfoResources(ExtTripInfoResource uExtTripInfoResources, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_FLT_ExtTripInfoResources", "EmpCode = '" + uExtTripInfoResources.EmpCode + "'", sConnStr))
                return true;
            else
                return false;
        }
    }
}



