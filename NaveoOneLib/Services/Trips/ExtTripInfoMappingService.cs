using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Trips;

namespace NaveoOneLib.Services.Trips
{
    public class ExtTripInfoMappingService
    {

        Errors er = new Errors();
        
        public DataTable GetExtTripInfoMapping(String sConnStr)
        {
            String sqlString = @"SELECT distinct  
                                MappingId,
                                Description
                                from GFI_FLT_ExtTripInfoMapping order by MappingId";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        public ExtTripInfoMapping GetExtTripInfoMappingById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT IID, 
                                MappingId,
                                Description, 
                                ExtTripInfoId, 
                                CreatedBy, 
                                CreatedDate, 
                                UpdatedBy, 
                                UpdatedDate from GFI_FLT_ExtTripInfoMapping
                                WHERE IID = ?
                                order by IID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                ExtTripInfoMapping retExtTripInfoMapping = new ExtTripInfoMapping();
                retExtTripInfoMapping.IID = Convert.ToInt32(dt.Rows[0]["IID"]);
                retExtTripInfoMapping.MappingID = Convert.ToInt32(dt.Rows[0]["MappingId"]);
                retExtTripInfoMapping.Description = dt.Rows[0]["Description"].ToString();
                retExtTripInfoMapping.ExtTripInfoId = Convert.ToInt32(dt.Rows[0]["ExtTripInfoId"]);
                retExtTripInfoMapping.CreatedBy = dt.Rows[0]["CreatedBy"].ToString();
                retExtTripInfoMapping.CreatedDate = (DateTime)dt.Rows[0]["CreatedDate"];
                retExtTripInfoMapping.UpdatedBy = dt.Rows[0]["UpdatedBy"].ToString();
                retExtTripInfoMapping.UpdatedDate = (DateTime)dt.Rows[0]["UpdatedDate"];
                return retExtTripInfoMapping;
            }
        }
        
        public ExtTripInfoMapping GetExtTripInfoMappingByDescription(String iID, String sConnStr)
        {
            String sqlString = @"SELECT IID, 
                                MappingId,
                                Description, 
                                ExtTripInfoId, 
                                CreatedBy, 
                                CreatedDate, 
                                UpdatedBy, 
                                UpdatedDate from GFI_FLT_ExtTripInfoMapping
                                WHERE Description = ?";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                ExtTripInfoMapping retExtTripInfoMapping = new ExtTripInfoMapping();
                retExtTripInfoMapping.IID = Convert.ToInt32(dt.Rows[0]["IID"]);
                retExtTripInfoMapping.MappingID = Convert.ToInt32(dt.Rows[0]["MappingId"]);
                retExtTripInfoMapping.Description = dt.Rows[0]["Description"].ToString();
                retExtTripInfoMapping.ExtTripInfoId = Convert.ToInt32(dt.Rows[0]["ExtTripInfoId"]);
                retExtTripInfoMapping.CreatedBy = dt.Rows[0]["CreatedBy"].ToString();
                retExtTripInfoMapping.CreatedDate = (DateTime)dt.Rows[0]["CreatedDate"];
                retExtTripInfoMapping.UpdatedBy = dt.Rows[0]["UpdatedBy"].ToString();
                retExtTripInfoMapping.UpdatedDate = (DateTime)dt.Rows[0]["UpdatedDate"];
                return retExtTripInfoMapping;
            }
        }

        public Boolean SaveExtTripInfoMapping(ExtTripInfoMapping uExtTripInfoMapping, Boolean bInsert, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_ExtTripInfoMapping";
            dt.Columns.Add("IID", uExtTripInfoMapping.IID.GetType());
            dt.Columns.Add("MappingId", uExtTripInfoMapping.MappingID.GetType());
            dt.Columns.Add("Description", uExtTripInfoMapping.Description.GetType());
            dt.Columns.Add("ExtTripInfoId", uExtTripInfoMapping.ExtTripInfoId.GetType());
            dt.Columns.Add("CreatedBy", uExtTripInfoMapping.CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", uExtTripInfoMapping.CreatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uExtTripInfoMapping.UpdatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uExtTripInfoMapping.UpdatedDate.GetType());
            DataRow dr = dt.NewRow();
            dr["IID"] = uExtTripInfoMapping.IID;
            dr["MappingId"] = uExtTripInfoMapping.MappingID;
            dr["Description"] = uExtTripInfoMapping.Description;
            dr["ExtTripInfoId"] = uExtTripInfoMapping.ExtTripInfoId;
            dr["CreatedBy"] = uExtTripInfoMapping.CreatedBy;
            dr["CreatedDate"] = uExtTripInfoMapping.CreatedDate;
            dr["UpdatedBy"] = uExtTripInfoMapping.UpdatedBy;
            dr["UpdatedDate"] = uExtTripInfoMapping.UpdatedDate;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataSet dsProcess = new DataSet();
            DataTable dtCtrl = c.CTRLTBL();
            dsProcess.Merge(dt);

            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "IID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            dtCtrl.Rows.Add(drCtrl);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0 || dri["oprType"].ToString() == "0")
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            DataSet dsResults = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResults.Tables["CTRLTBL"];

            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }

        public DataTable GetExtTripInfoMappingByMappingId(String MID, String sConnStr)
        {
            String sqlString = @"SELECT map.IID, 
                                map.MappingId,
                                type.TypeName,
                                map.Description, 
                                
                                map.ExtTripInfoId, 
                                map.CreatedBy, 
                                map.CreatedDate, 
                                map.UpdatedBy, 
                                map.UpdatedDate 
                                from GFI_FLT_ExtTripInfoMapping map, GFI_FLT_ExtTripInfoType type
                                WHERE map.ExtTripInfoId = type.IID
                                and MappingId = ?
                                order by map.IID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, MID, sConnStr);
            return dt;
            //    if (dt.Rows.Count == 0)
            //        return null;
            //    else
            //    {
            //        ExtTripInfoMapping retExtTripInfoMapping = new ExtTripInfoMapping();
            //        retExtTripInfoMapping.IID = Convert.ToInt32(dt.Rows[0]["IID"];
            //        retExtTripInfoMapping.MappingID = Convert.ToInt32(dt.Rows[0]["MappingId"];
            //        retExtTripInfoMapping.Description = dt.Rows[0]["Description"].ToString();
            //        retExtTripInfoMapping.ExtTripInfoId = Convert.ToInt32(dt.Rows[0]["ExtTripInfoId"];
            //        retExtTripInfoMapping.CreatedBy = dt.Rows[0]["CreatedBy"].ToString();
            //        retExtTripInfoMapping.CreatedDate = (DateTime)dt.Rows[0]["CreatedDate"];
            //        retExtTripInfoMapping.UpdatedBy = dt.Rows[0]["UpdatedBy"].ToString();
            //        retExtTripInfoMapping.UpdatedDate = (DateTime)dt.Rows[0]["UpdatedDate"];
            //        return retExtTripInfoMapping;
            //    }
        }

        public bool SaveExtTripInfoMapping(ExtTripInfoMapping uExtTripInfoMapping, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_ExtTripInfoMapping";
            dt.Columns.Add("IID", uExtTripInfoMapping.IID.GetType());
            dt.Columns.Add("MappingId", uExtTripInfoMapping.MappingID.GetType());
            dt.Columns.Add("Description", uExtTripInfoMapping.Description.GetType());
            dt.Columns.Add("ExtTripInfoId", uExtTripInfoMapping.ExtTripInfoId.GetType());
            dt.Columns.Add("CreatedBy", uExtTripInfoMapping.CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", uExtTripInfoMapping.CreatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uExtTripInfoMapping.UpdatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uExtTripInfoMapping.UpdatedDate.GetType());
            DataRow dr = dt.NewRow();
            dr["IID"] = uExtTripInfoMapping.IID;
            dr["MappingId"] = uExtTripInfoMapping.MappingID;
            dr["Description"] = uExtTripInfoMapping.Description;
            dr["ExtTripInfoId"] = uExtTripInfoMapping.ExtTripInfoId;
            dr["CreatedBy"] = uExtTripInfoMapping.CreatedBy;
            dr["CreatedDate"] = uExtTripInfoMapping.CreatedDate;
            dr["UpdatedBy"] = uExtTripInfoMapping.UpdatedBy;
            dr["UpdatedDate"] = uExtTripInfoMapping.UpdatedDate;
            dt.Rows.Add(dr);

            //Connection _connection = new Connection(sConnStr);;
            //if (_connection.GenInsert(dt, transaction))
            //    return true;
            //else
            //    return false;

            Connection c = new Connection(sConnStr);
            DataSet dsProcess = new DataSet();
            DataTable dtCtrl = c.CTRLTBL();
            dsProcess.Merge(dt);

            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "IID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            dtCtrl.Rows.Add(drCtrl);

            Boolean bInsert = true;

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0 || dri["oprType"].ToString() == "0")
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            DataSet dsResults = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResults.Tables["CTRLTBL"];

            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }
        
        public bool SaveExtTripInfoMapping(List<ExtTripInfoMapping> ExtTripInfoMapping, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            Connection c = new Connection(sConnStr);
            DataSet dsProcess = new DataSet();
            DataTable dtCtrl = c.CTRLTBL();

            dt.TableName = "GFI_FLT_ExtTripInfoMapping";
            dt.Columns.Add("IID", ExtTripInfoMapping[0].IID.GetType());
            dt.Columns.Add("MappingId", ExtTripInfoMapping[0].MappingID.GetType());
            dt.Columns.Add("Description", ExtTripInfoMapping[0].Description.GetType());            
            dt.Columns.Add("ExtTripInfoId", ExtTripInfoMapping[0].ExtTripInfoId.GetType());
            dt.Columns.Add("CreatedBy", ExtTripInfoMapping[0].CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", ExtTripInfoMapping[0].CreatedDate.GetType());
            dt.Columns.Add("UpdatedBy", ExtTripInfoMapping[0].UpdatedBy.GetType());
            dt.Columns.Add("UpdatedDate", ExtTripInfoMapping[0].UpdatedDate.GetType());

            foreach (ExtTripInfoMapping uExtTripInfoMapping in ExtTripInfoMapping)
            {

                DataRow dr = dt.NewRow();
                dr["IID"] = uExtTripInfoMapping.IID;
                dr["MappingId"] = uExtTripInfoMapping.MappingID;
                dr["Description"] = uExtTripInfoMapping.Description;                
                dr["ExtTripInfoId"] = uExtTripInfoMapping.ExtTripInfoId;
                dr["CreatedBy"] = uExtTripInfoMapping.CreatedBy;
                dr["CreatedDate"] = uExtTripInfoMapping.CreatedDate;
                dr["UpdatedBy"] = uExtTripInfoMapping.UpdatedBy;
                dr["UpdatedDate"] = uExtTripInfoMapping.UpdatedDate;
                dt.Rows.Add(dr);
            }


            dsProcess.Merge(dt);

            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "IID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            dtCtrl.Rows.Add(drCtrl);

            Boolean bInsert = true;
            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0 || dri["oprType"].ToString() == "0")
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            DataSet dsResults = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResults.Tables["CTRLTBL"];

            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }

        public bool UpdateExtTripInfoMapping(ExtTripInfoMapping uExtTripInfoMapping, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_ExtTripInfoMapping";
            //dt.Columns.Add("IID" , uExtTripInfoMapping.IID.GetType());
            dt.Columns.Add("MappingId", uExtTripInfoMapping.MappingID.GetType());
            dt.Columns.Add("Description", uExtTripInfoMapping.Description.GetType());
            dt.Columns.Add("ExtTripInfoId", uExtTripInfoMapping.ExtTripInfoId.GetType());
            dt.Columns.Add("CreatedBy", uExtTripInfoMapping.CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", uExtTripInfoMapping.CreatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uExtTripInfoMapping.UpdatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uExtTripInfoMapping.UpdatedDate.GetType());
            DataRow dr = dt.NewRow();
            //dr["IID"] =  uExtTripInfoMapping.IID;
            dr["MappingId"] = uExtTripInfoMapping.MappingID;
            dr["Description"] = uExtTripInfoMapping.Description;
            dr["ExtTripInfoId"] = uExtTripInfoMapping.ExtTripInfoId;
            dr["CreatedBy"] = uExtTripInfoMapping.CreatedBy;
            dr["CreatedDate"] = uExtTripInfoMapping.CreatedDate;
            dr["UpdatedBy"] = uExtTripInfoMapping.UpdatedBy;
            dr["UpdatedDate"] = uExtTripInfoMapping.UpdatedDate;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "IID = '" + uExtTripInfoMapping.IID + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }
        
        public bool UpdateExtTripInfoMapping(List<ExtTripInfoMapping> ExtTripInfoMapping, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            Connection c = new Connection(sConnStr);
            DataSet dsProcess = new DataSet();
            DataTable dtCtrl = c.CTRLTBL();

            dt.TableName = "GFI_FLT_ExtTripInfoMapping";
            dt.Columns.Add("IID", ExtTripInfoMapping[0].IID.GetType());
            dt.Columns.Add("MappingId", ExtTripInfoMapping[0].MappingID.GetType());
            dt.Columns.Add("Description", ExtTripInfoMapping[0].Description.GetType());
            dt.Columns.Add("ExtTripInfoId", ExtTripInfoMapping[0].ExtTripInfoId.GetType());
            dt.Columns.Add("CreatedBy", ExtTripInfoMapping[0].CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", ExtTripInfoMapping[0].CreatedDate.GetType());
            dt.Columns.Add("UpdatedBy", ExtTripInfoMapping[0].UpdatedBy.GetType());
            dt.Columns.Add("UpdatedDate", ExtTripInfoMapping[0].UpdatedDate.GetType());

            foreach (ExtTripInfoMapping uExtTripInfoMapping in ExtTripInfoMapping)
            {

                DataRow dr = dt.NewRow();
                dr["IID"] = uExtTripInfoMapping.IID;
                dr["MappingId"] = uExtTripInfoMapping.MappingID;
                dr["Description"] = uExtTripInfoMapping.Description;
                dr["ExtTripInfoId"] = uExtTripInfoMapping.ExtTripInfoId;
                dr["CreatedBy"] = uExtTripInfoMapping.CreatedBy;
                dr["CreatedDate"] = uExtTripInfoMapping.CreatedDate;
                dr["UpdatedBy"] = uExtTripInfoMapping.UpdatedBy;
                dr["UpdatedDate"] = uExtTripInfoMapping.UpdatedDate;
                dt.Rows.Add(dr);
            }


            dsProcess.Merge(dt);

            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "IID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            dtCtrl.Rows.Add(drCtrl);

            Boolean bInsert = false;
            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0 || dri["oprType"].ToString() == "0")
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            DataSet dsResults = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResults.Tables["CTRLTBL"];

            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }

        public bool DeleteExtTripInfoMapping(ExtTripInfoMapping uExtTripInfoMapping, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_FLT_ExtTripInfoMapping", "IID = '" + uExtTripInfoMapping.IID + "'", sConnStr))
                return true;
            else
                return false;
        }
        
        public DataTable GetNextMappingId(String sConnStr)
        {
            String sqlString = @"SELECT MAX(MappingId)
                                from GFI_FLT_ExtTripInfoMapping";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        
        public bool Delete(List<ExtTripInfoMapping> listExtTripInfoMapping, String sConnStr)
        {
            int deleteNo = 0;

            Connection _connection = new Connection(sConnStr);

            foreach (ExtTripInfoMapping ext in listExtTripInfoMapping)
            {
                if (_connection.GenDelete("GFI_FLT_ExtTripInfoMapping", "IID = '" + ext.IID + "'", sConnStr))
                    deleteNo++;
            }

            if (deleteNo == listExtTripInfoMapping.Count)
                return true;
            else
                return false;
        }
    }
}

