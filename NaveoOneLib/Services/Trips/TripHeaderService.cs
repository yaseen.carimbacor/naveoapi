﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.Trips;
using NaveoOneLib.Models.GMatrix;
using NaveoOneLib.Models.Drivers;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.GPS;
using NaveoOneLib.Models.Zones;
using NaveoOneLib.Services.Drivers;
using NaveoOneLib.Services.Assets;
using NaveoOneLib.Services.Zones;
using NaveoOneLib.Services.GPS;
using NaveoOneLib.Services.Rules;
using NaveoOneLib.Maps;
using NaveoOneLib.Constant;
using NaveoOneLib.Models.Users;

namespace NaveoOneLib.Services.Trips
{
    public class TripHeaderService
    {
        Errors er = new Errors();
        DataSet GetTripsDS(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, int iMinTripDistance, Boolean bByDriver, Boolean bGetZones, List<Matrix> lMatrix, String sConnStr, int? Page, int? LimitPerPage)
        {
            return GetTripsDS(lAssetID, dtFrom, dtTo, iMinTripDistance, String.Empty, bByDriver, false, true, bGetZones, lMatrix, sConnStr, Page, LimitPerPage, null, false, null, true);
        }
        DataSet GetTripsDS(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, Double iMinTripDistance, String sWorkHrs, Boolean bByDriver, Boolean bInclClacOdometer, Boolean bhasDetails, Boolean bGetZones, List<Matrix> lMatrix, String sConnStr, int? Page, int? LimitPerPage, List<int> lTripIDs, Boolean bhasExceptions, List<String> sortColumns, Boolean sortOrderAsc)
        {
            #region #get rules based on user matrix
            DataTable dtRules = new RulesService().GetRules(lMatrix,string.Empty,sConnStr);

            var lRules = from myRow in dtRules.AsEnumerable()
                         select myRow.Field<int>("ParentRuleID");
            #endregion 

            //Doing this to use existing params
            #region Summary reports and Working hours
            Boolean bDailySummary = false;
            if (sWorkHrs.Contains("DailySummary"))
            {
                sWorkHrs = sWorkHrs.Replace("DailySummary", String.Empty);
                bDailySummary = true;
            }

            Boolean bSummarizedTrips = false;
            if (sWorkHrs.Contains("SummarizedTrips"))
            {
                sWorkHrs = sWorkHrs.Replace("SummarizedTrips", String.Empty);
                bSummarizedTrips = true;
            }

            Boolean bSpatialData = false;
            if (sWorkHrs.Contains("SpatialData"))
            {
                sWorkHrs = sWorkHrs.Replace("SpatialData", String.Empty);
                bSpatialData = true;
            }

            sWorkHrs = StaticUtils.GetWorkHrs(sWorkHrs);
            #endregion

            #region sort TripHeader
            String sSort = "h.AssetID,h.dtStart";
            if (sortColumns != null && sortColumns.Count > 0)
            {
                List<String> lCols = new List<string>() { "AssetID", "dtStart", "dtEnd", "MaxSpeed", "TripDistance", "TripTime", "StopTime", "IdlingTime", "DepartureAddress", "ArrivalAddress" };

                String ss = String.Join(",", sortColumns
                    .Where(s => !String.IsNullOrWhiteSpace(s))
                    .Where(s => lCols.Contains(s))
                    .Select(s => "h." + s));

                if (!String.IsNullOrEmpty(ss))
                    sSort = ss;
            }

            if (sortOrderAsc)
                sSort += " asc";
            else
                sSort += " desc";
            #endregion

            Pagination p = Pagination.GetRowNums(Page, LimitPerPage);

            String sTripIDs = String.Empty;
            if (lTripIDs != null && lTripIDs.Count() > 0)
            {
                foreach (int i in lTripIDs)
                    sTripIDs += i.ToString() + ",";

                sTripIDs = sTripIDs.Substring(0, sTripIDs.Length - 1);
                sTripIDs = "and h.iID in (" + sTripIDs + ") \r\n";
            }

            if (lAssetID.Count == 0)
                return null;

            lAssetID = lAssetID.Distinct().ToList();

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            Boolean bChkArc = Globals.bNeedToQryArc(sConnStr, dtFrom);

            String sqlAsset = @"
--TripsRetrieval
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 

--Creating temp tables
CREATE TABLE #GFI_GPS_TripHeader
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL,
	[iID] [int] PRIMARY KEY CLUSTERED,
	[GroupDate] [datetime] NULL,

	[AssetID] [int] NOT NULL,
    Asset nvarchar(120),
    TimeZone int,

	[DriverID] [int] NOT NULL,
    Driver nvarchar(100),

	[ExceptionFlag] [int] NULL,
	[MaxSpeed] [int] NULL,

	[IdlingTime] [float] NULL,
	[StopTime] [float] NULL,
	[TripDistance] [float] NULL,

    tsIdlingTime Time null,
    tsStopTime Time null,
    tsTripTime Time null,

    [TripTime] [float] NULL,
	[GPSDataStartUID] [int] NULL,
	[GPSDataEndUID] [int] NULL,
	[dtStart] [datetime] NULL,
	[fStartLon] [float] NULL,
	[fStartLat] [float] NULL,
	[dtEnd] [datetime] NULL,
	[fEndLon] [float] NULL,
	[fEndLat] [float] NULL,

	[OverSpeed1Time] [float] NOT NULL DEFAULT ((0)),
	[OverSpeed2Time] [float] NOT NULL DEFAULT ((0)),
    [tsOverSpeed1Time] Time null,
    [tsOverSpeed2Time] Time null,

	[Accel] [int] NOT NULL DEFAULT ((0)),
	[Brake] [int] NOT NULL DEFAULT ((0)),
	[Corner] [int] NOT NULL DEFAULT ((0)),

    [DepartureAddress] nvarchar(150),
    [ArrivalAddress] nvarchar(150),

    [iDepartureZone] int null,
    [sDepartureZone] nvarchar(500) null,
    [iDepartureZoneColor] int null,
                            
    [iArrivalZone] int null,
    [sArrivalZone] nvarchar(500) null,
    [iArrivalZoneColor] int null,

    [iArrivalZones] nvarchar(max) null,
    [sArrivalZones] nvarchar(max) null,
    [sArrivalZonesColor] nvarchar(max) null,

    [CalculatedOdometer] int null,
    [exceptionsCnt] int default 0,

	[dtUTCStart] [datetime] NULL,
	[dtUTCEnd] [datetime] NULL
)

CREATE NONCLUSTERED INDEX  #myIdx ON #GFI_GPS_TripHeader ([AssetID]) INCLUDE ([iID],[GPSDataStartUID],[GPSDataEndUID])

create table #GFI_GPS_GPSData 
(
	--[RowNumber] [int] IDENTITY(1,1) NOT NULL,
	--[UID] [int] PRIMARY KEY CLUSTERED,
	[RowNumber] [int] IDENTITY(1,1) PRIMARY KEY CLUSTERED NOT NULL,
	[UID] [int],
	[AssetID] [int] NULL,
	[DateTimeGPS_UTC] [datetime] NULL,
	[DateTimeServer] [datetime] NULL,
	[Longitude] [float] NULL,
	[Latitude] [float] NULL,
	[LongLatValidFlag] [int] NULL,
	[Speed] [float] NULL,
	[EngineOn] [int] NULL,
	[StopFlag] [int] NULL,
	[TripDistance] [float] NULL,
	[TripTime] [float] NULL,
	[WorkHour] [int] NULL,
	[DriverID] [int] NOT NULL,
    [RoadSpeed] [int] NULL,
	Misc1 nvarchar(100) null,
	Misc2 nvarchar(100) null,
	Misc3 nvarchar(100) null
)

CREATE NONCLUSTERED INDEX #myIndex ON #GFI_GPS_GPSData 
(
	[AssetID] ASC,
	[DateTimeGPS_UTC] ASC,
	[LongLatValidFlag] ASC,
	[StopFlag] ASC
)

CREATE NONCLUSTERED INDEX #myIndexGPSData ON #GFI_GPS_GPSData ([UID])
	INCLUDE ([RowNumber],[AssetID],[DateTimeGPS_UTC],[DateTimeServer],[Longitude],[Latitude],[LongLatValidFlag],[Speed],[EngineOn],[StopFlag],[TripDistance],[TripTime],[WorkHour],[DriverID],[Misc1],[Misc2],[Misc3])

CREATE TABLE #Assets
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY CLUSTERED,
	[iAssetID] [int],
    Asset nvarchar(120),
    TimeZone int,
	[iDriverID] [int]
)

--Oracle Starts Here
";
            String sql = String.Empty;
            String SqlH = String.Empty;

            if (bByDriver)
            {
                foreach (int i in lAssetID)
                    sqlAsset += "insert #Assets (iDriverID) values (" + i.ToString() + ");\r\n";

                sql = "--SelectIntoStarts\r\n";
                sql += new DriverService().sGetDriver(lMatrix, String.Empty, false, Driver.ResourceType.Driver, sConnStr);
                sql = sql.Replace("from GFI_FLT_Driver u", "into #FltAssets from GFI_FLT_Driver a");
                sql = sql.Replace("u.", "a.");
                sqlAsset += sql + ";";
                sqlAsset += @"
--SelectIntoEnds
DELETE FROM #Assets
    WHERE NOT EXISTS 
        (
            SELECT * FROM #FltAssets
            WHERE iDriverID = #FltAssets.DriverID
        )   --Oracle Add SemiColumn
";
                String s = new AssetService().sGetAsset(lMatrix, sConnStr);
                s = s.Replace("from GFI_FLT_Asset a", "into #myAssets from GFI_FLT_Asset a");
                sqlAsset += s;
            }
            else
            {
                foreach (int i in lAssetID)
                    sqlAsset += "insert #Assets (iAssetID) values (" + i.ToString() + ");\r\n";

                sql = "--SelectIntoStarts\r\n";
                sql += new AssetService().sGetAsset(lMatrix, sConnStr);
                sql = sql.Replace("from GFI_FLT_Asset a", "into #FltAssets from GFI_FLT_Asset a");
                sqlAsset += sql + ";";
                sqlAsset += @"
--SelectIntoEnds
--SelectIntoStarts
select 1 x into #myAssets   --Remove from Oracle
--SelectIntoEnds

DELETE FROM #Assets
    WHERE NOT EXISTS 
        (
            SELECT * FROM #FltAssets
            WHERE iAssetID = #FltAssets.AssetID
        )

UPDATE #Assets
    SET Asset = T2.AssetNumber, TimeZone = T3.TotalMinutes
FROM #Assets T1
    INNER JOIN #FltAssets T2 ON T2.AssetID = T1.iAssetID
    INNER JOIN GFI_SYS_TimeZones T3 on T2.TimeZoneID = T3.StandardName
";
            }

            sql = String.Empty;
            String sqlMain = sqlAsset + @"
delete #GFI_GPS_TripHeader;

insert #GFI_GPS_TripHeader (iID ,AssetID
                            , DriverID
                            , ExceptionFlag
                            , MaxSpeed
                            , IdlingTime
                            , StopTime
                            , TripDistance
                            , TripTime
                            , GPSDataStartUID
                            , GPSDataEndUID
	                        , dtStart
	                        , fStartLon
	                        , fStartLat
	                        , dtEnd
	                        , fEndLon
	                        , fEndLat
	                        , OverSpeed1Time 
	                        , OverSpeed2Time 
	                        , Accel
	                        , Brake
	                        , Corner
                            , DepartureAddress, ArrivalAddress
                            )  
    SELECT distinct iID
        , h.AssetID
        , h.DriverID
        , h.ExceptionFlag
        , h.MaxSpeed
        , h.IdlingTime
        , h.StopTime
        , ROUND(h.TripDistance,1) TripDistance
        , h.TripTime
        , h.GPSDataStartUID
        , h.GPSDataEndUID
	    , h.dtStart
	    , h.fStartLon
	    , h.fStartLat
	    , h.dtEnd
	    , h.fEndLon
	    , h.fEndLat
	    , h.OverSpeed1Time 
	    , h.OverSpeed2Time 
	    , h.Accel
	    , h.Brake
	    , h.Corner 
        , h.DepartureAddress, h.ArrivalAddress
    FROM 
    (
        select row_number() OVER (order by h.AssetID, h.dtStart) AS RowNum, h.*
            from #Assets a 
            inner join GFI_GPS_TripHeader h on a.iAssetID = h.AssetID
                where h.GPSDataStartUID <= h.GPSDataEndUID
	                and h.dtStart >= ?
	                and h.dtStart <= ? 
                    and h.TripDistance >= " + iMinTripDistance + " " + sWorkHrs;
            sqlMain += sTripIDs + " )h ";
            if (Page.HasValue)
                sqlMain += " WHERE h.RowNum between " + p.RowFrom + " and " + p.RowTo + " ";

            if (bByDriver)
                sqlMain = sqlMain.Replace("inner join GFI_GPS_TripHeader h on a.iAssetID = h.AssetID", "inner join GFI_GPS_TripHeader h on a.iDriverID = h.DriverID inner join #myAssets m on m.AssetID = H.AssetID"); //this should be caps

            if (bChkArc)
            {
                sqlMain += @"
--ArchiveData
union 
    select iID
            , h.AssetID
            , h.DriverID
            , h.ExceptionFlag
            , h.MaxSpeed
            , h.IdlingTime
            , h.StopTime
            , ROUND(h.TripDistance,1) TripDistance
            , h.TripTime
            , h.GPSDataStartUID
            , h.GPSDataEndUID
	        , h.dtStart
	        , h.fStartLon
	        , h.fStartLat
	        , h.dtEnd
	        , h.fEndLon
	        , h.fEndLat
	        , h.OverSpeed1Time 
	        , h.OverSpeed2Time 
	        , h.Accel
	        , h.Brake
	        , h.Corner
            , h.DepartureAddress, h.ArrivalAddress
    FROM
    (
        select row_number() OVER (order by " + sSort.Replace("h.", "") + @") AS RowNum, *
            from #Assets a 
            inner join GFI_ARC_TripHeader h on a.iAssetID = h.AssetID
                where h.GPSDataStartUID <= h.GPSDataEndUID
	                and h.dtStart >= ?
	                and h.dtStart <= ?
                    and h.TripDistance >= " + iMinTripDistance + " " + sWorkHrs + @"
    )h ";
                if (Page.HasValue)
                    sqlMain += " WHERE h.RowNum between " + p.RowFrom + " and " + p.RowTo;
            }
            sqlMain += "\r\n";
            sqlMain += " order by " + sSort.Replace("h.", ""); //Bug FIX sorting

            sqlMain += ";";

            sqlMain += @"

delete from #GFI_GPS_GPSData

if(1 = 0)
    begin --then
        insert #GFI_GPS_GPSData (UID, AssetID, DateTimeGPS_UTC, DateTimeServer, Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, StopFlag, TripDistance, TripTime, WorkHour, DriverID, RoadSpeed)
            select distinct g.* from #Assets a 
                inner join #GFI_GPS_TripHeader h on a.iAssetID = h.AssetID
                inner join GFI_GPS_TripDetail d on h.iID = d.HeaderiID
                inner join GFI_GPS_GPSData g on g.UID = d.GPSDataUID
                where h.GPSDataStartUID <= h.GPSDataEndUID
	                and h.dtStart >= ?
	                and h.dtStart <= ?
                    and h.TripDistance >= " + iMinTripDistance + @"
";
            sqlMain += sWorkHrs;

            if (bChkArc)
            {
                sqlMain += @" union --ArchiveData
            select distinct g.* from #Assets a 
                inner join GFI_ARC_TripHeader h on a.iAssetID = h.AssetID
                inner join GFI_ARC_TripDetail d on h.iID = d.HeaderiID
                inner join GFI_ARC_GPSData g on g.UID = d.GPSDataUID
                where h.GPSDataStartUID <= h.GPSDataEndUID
	                and h.dtStart >= ?
	                and h.dtStart <= ?
                    and h.TripDistance >= " + iMinTripDistance + @"
";
                sqlMain += sWorkHrs + "\r\n";
            }

            sqlMain += @" order by g.DateTimeGPS_UTC  --OracleAddSemiColumn
    end --Remove from Oracle
else 
    begin --Remove from Oracle
        insert #GFI_GPS_GPSData (UID, AssetID, DateTimeGPS_UTC, DateTimeServer, Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, StopFlag, TripDistance, TripTime, WorkHour, DriverID)
	        select GPSDataStartUID, AssetID, dtStart, dtStart, fStartLon, fStartLat, 1, MaxSpeed, 1, 10, 0, 0, 0, DriverID
		        from #GFI_GPS_TripHeader
	        union
	        select GPSDataEndUID, AssetID, dtEnd, dtEnd, fEndLon, fEndLat, 1, MaxSpeed, 0, 111, TripDistance, TripTime, 0, DriverID
		        from #GFI_GPS_TripHeader    --OracleAddSemiColumn
    end --end if;
";
            if (bhasDetails)
                sqlMain = sqlMain.Replace("if(1 = 0)", "if(1 = 1)");

            if (bByDriver)
                sqlMain = sqlMain.Replace("where h.AssetID in (", "where h.DriverID in (");

            String strParamMain = dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff")
                + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff")
                + "¬" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff")
                + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");

            if (bChkArc)
            {
                strParamMain
                    += "¬" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff")
                    + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff")
                    + "¬" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff")
                    + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");
            }

            #region Zones
            if (bGetZones)
            {
                #region Spatial
                sql = ZoneService.sGetZoneIDs(lMatrix, sConnStr);
                sql += @"
                        ;with cte as
                        (
	                        select h.*, z.* --ArrivalZoneTripsTmp
                            from 
	                            (
		                            select *
			                            , Geometry::STPointFromText('POINT(' + CONVERT(varchar, fEndLon) + ' ' + CONVERT(varchar, fEndLat) + ')', 4326) pointFr
		                            from #GFI_GPS_TripHeader
	                            ) h
								inner join 
									(
										select zh.* from GFI_FLT_ZoneHeader zh --WITH (INDEX(SIndx_GFI_FLT_ZoneHeader))
											--inner join @tTmpDistinctZone t on zh.ZoneID = t.i
									) z on z.GeomData.STIntersects(pointFr) = 1
                        )
                        select * into #ArrivalZoneTripsTmp from cte

                        select t1.* into #ArrivalZoneTrips 
                            from #ArrivalZoneTripsTmp t1
                                inner join @tTmpDistinctZone t2 on t1.ZoneID = t2.i

                        update #GFI_GPS_TripHeader 
                            set sArrivalZones = STUFF(
		                                        (SELECT ', ' + e.Description FROM #ArrivalZoneTrips e Where e.iID = h.iID FOR XML PATH(''))
	                                        , 1, 1, ''),
                                iArrivalZones = STUFF(
		                                        (SELECT ', ' + CONVERT(varchar(10), e.ZoneID) FROM #ArrivalZoneTrips e Where e.iID = h.iID FOR XML PATH(''))
	                                        , 1, 1, ''),
                                sArrivalZonesColor = STUFF(
		                                        (SELECT ', ' + CONVERT(varchar(10), e.Color) FROM #ArrivalZoneTrips e Where e.iID = h.iID FOR XML PATH(''))
	                                        , 1, 1, ''),
                                iArrivalZone = (SELECT top 1 e.ZoneID FROM #ArrivalZoneTrips e Where e.iID = h.iID ),
                                sArrivalZone = (SELECT top 1 e.Description FROM #ArrivalZoneTrips e Where e.iID = h.iID ),
                                iArrivalZoneColor = (SELECT top 1 e.Color FROM #ArrivalZoneTrips e Where e.iID = h.iID )
                        from #GFI_GPS_TripHeader h

                        --Departure
                        ;with cte as
                        (
	                        select h.*, z.* --DepartureZoneTripsTmp
                            from 
	                            (
		                            select *
			                            , Geometry::STPointFromText('POINT(' + CONVERT(varchar, fStartLon) + ' ' + CONVERT(varchar, fStartLat) + ')', 4326) pointFr
		                            from #GFI_GPS_TripHeader
	                            ) h
								inner join 
									(
										select zh.* from GFI_FLT_ZoneHeader zh --WITH (INDEX(SIndx_GFI_FLT_ZoneHeader))
											--inner join @tTmpDistinctZone t on zh.ZoneID = t.i
									) z on z.GeomData.STIntersects(pointFr) = 1
                        )
                        select * into #DepartureZoneTripsTmp from cte

                        select t1.* into #DepartureZoneTrips 
                            from #DepartureZoneTripsTmp t1
                                inner join @tTmpDistinctZone t2 on t1.ZoneID = t2.i

                        update #GFI_GPS_TripHeader 
                            set iDepartureZone = (SELECT top 1 e.ZoneID FROM #DepartureZoneTrips e Where e.iID = h.iID ),
                                sDepartureZone = (SELECT top 1 e.Description FROM #DepartureZoneTrips e Where e.iID = h.iID ),
                                iDepartureZoneColor = (SELECT top 1 e.Color FROM #DepartureZoneTrips e Where e.iID = h.iID )
                        from #GFI_GPS_TripHeader h
";
                #endregion

                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                dtSql.Rows.Add(drSql);
            }
            #endregion
            #region update Asset values
            sql = String.Empty;
            if (bByDriver)
            {
                sql = @"
insert #Assets (iAssetID)
    select distinct AssetID from #GFI_GPS_TripHeader

UPDATE #Assets
    SET Asset = T2.AssetNumber, TimeZone = T3.TotalMinutes
FROM #Assets T1
    INNER JOIN #myAssets T2 ON T2.AssetID = T1.iAssetID
    INNER JOIN GFI_SYS_TimeZones T3 on T2.TimeZoneID = T3.StandardName
";
            }
            sql += @"
UPDATE #GFI_GPS_TripHeader 
    set dtUTCStart = dtStart, dtUTCEnd = dtEnd

--OracleTHReplaceStarts0
UPDATE #GFI_GPS_TripHeader 
    SET Asset = T2.Asset
    , TimeZone = T2.TimeZone
    , dtStart = DATEADD(mi, T2.TimeZone, dtStart)
    , dtEnd = DATEADD(mi, T2.TimeZone, dtEnd)
    , GroupDate = CONVERT(date, DATEADD(mi, T2.TimeZone, dtStart))
    , tsIdlingTime = CAST(DATEADD(second, T1.IdlingTime, '1900-01-01') AS TIME)
    , tsStopTime   = CAST(DATEADD(second, T1.StopTime, '1900-01-01') AS TIME)
    , tsTripTime   = CAST(DATEADD(second, T1.TripTime, '1900-01-01') AS TIME)
    , tsOverSpeed1Time   = CAST(DATEADD(second, T1.OverSpeed1Time, '1900-01-01') AS TIME)
    , tsOverSpeed2Time   = CAST(DATEADD(second, T1.OverSpeed2Time, '1900-01-01') AS TIME)
FROM #GFI_GPS_TripHeader T1
INNER JOIN #Assets T2 ON T2.iAssetID = T1.AssetID
--OracleTHReplaceEnds0
";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            dtSql.Rows.Add(drSql);
            #endregion
            #region update Driver values
            sql = new DriverService().sGetDriver(lMatrix, " and u.DriverID in (select distinct DriverID from #GFI_GPS_TripHeader)", false, Driver.ResourceType.Driver, sConnStr);
            sql = sql.Replace("SELECT u.*", "SELECT u.* into #FltDrivers");
            sql = "--SelectIntoStarts\r\n" + sql + " --Oracle Add SemiColumn \r\n --SelectIntoEnds \r\n";

            sql += @"
UPDATE #GFI_GPS_TripHeader --UseUpdateCmd
    SET Driver = T2.sDriverName
FROM #GFI_GPS_TripHeader T1
INNER JOIN #FltDrivers T2 ON T2.DriverID = T1.DriverID

UPDATE #GFI_GPS_TripHeader SET Driver = 'No Access' where Driver is null
";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            dtSql.Rows.Add(drSql);
            #endregion
            #region  TripExceptions
            if (!bByDriver && bhasExceptions)
            {
                sql = ExceptionsService.sSQLExceptions(lAssetID, new List<int>(), dtFrom, dtTo, sWorkHrs, bByDriver, true, lMatrix, bChkArc);
                sql += @"
--Exceptions Codes in TH starts
CREATE NONCLUSTERED INDEX myIX ON #ExceptionDetails ([uid]) INCLUDE ([groupID],[ruleID])    --Remove from Oracle
--OracleTHReplaceStarts1
;with cte as
(
	select th.* from #GFI_GPS_TripHeader th
		inner join #Exceptions e on th.AssetID = e.AssetID
	where e.dateTimeToLocal >= th.dtStart and e.dateTimeToLocal <= th.dtEnd";

                if (lRules.Any())
                {
                    String strResult = String.Empty;

                    foreach (int r in lRules)
                        strResult += r.ToString() + ",";

                    strResult = strResult.TrimEnd(',');
                    sql += @" and e.RuleID in ( " + strResult;
                    sql += @")";
                }

                sql += @")
, ctf as 
(
	select iid, count(iid) cnt
		from cte
	group by iid

)
update #GFI_GPS_TripHeader 
    set exceptionsCnt = ctf.cnt
from #GFI_GPS_TripHeader th
	inner join ctf on th.iID = ctf.iID
--OracleTHReplaceEnds1
--Exceptions Codes in TH ends
";
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;

                if (bChkArc)
                {
                    drSql["sParam"] = dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff")
               + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");
                }
                else
                {
                    drSql["sParam"] = dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");
                }

                dtSql.Rows.Add(drSql);
            }
            #endregion

            #region TripHeader
            sql = String.Empty;
            if (bInclClacOdometer)
                sql = @"
select distinct AssetID, 0 odo into #odo from #GFI_GPS_TripHeader
update #odo set odo = dbo.GetCalcOdo(AssetId) --Oracle Add SemiColumn
update #GFI_GPS_TripHeader 
	set CalculatedOdometer = o.odo
from #GFI_GPS_TripHeader h
	inner join #odo o on o.AssetID = h.AssetID  --Oracle Add SemiColumn
";

            sql += "\r\n select h.* from #GFI_GPS_TripHeader h";

            if (bByDriver)
                sql = sql.Replace("where h.AssetID in (", "where h.DriverID in (");
            sql += " order by " + sSort + "   --Oracle Add SemiColumn";
            sql += "\r\n";
            sql += @"
begin try --Remove from Oracle
insert GFI_GPS_TripWizNoAddress (HeaderID, fStartLon, fStartLat, fEndLon, fEndLat)
    select iID, fStartLon, fStartLat, fEndLon, fEndLat from #GFI_GPS_TripHeader where DepartureAddress is null and ArrivalAddress is null --Oracle Add SemiColumn

insert GFI_GPS_TripWizNoAddress (HeaderID, fStartLon, fStartLat)
    select iID, fStartLon, fStartLat from #GFI_GPS_TripHeader where DepartureAddress is null and ArrivalAddress is not null --Oracle Add SemiColumn

insert GFI_GPS_TripWizNoAddress (HeaderID, fEndLon, fEndLat)
    select iID, fEndLon, fEndLat from #GFI_GPS_TripHeader where DepartureAddress is not null and ArrivalAddress is null --Oracle Add SemiColumn
end try      --Remove from Oracle
begin catch  --Remove from Oracle
end catch    --Remove from Oracle
";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "TripHeader";
            dtSql.Rows.Add(drSql);
            #endregion

            //TripDetail
            SqlH = @"select iID from #GFI_GPS_TripHeader h";
            if (bhasDetails)
            {
                //sql = "SELECT * from GFI_GPS_TripDetail td where HeaderiID in (" + SqlH + ")";
                sql = @"SELECT td.*, g.DateTimeGPS_UTC 
                            , row_number() over(order by g.DateTimeGPS_UTC) as RowNum
                            from GFI_GPS_TripDetail td
	                        left outer join #GFI_GPS_GPSData g on td.GPSDataUID = g.UID
	                        where HeaderiID in (select iID from #GFI_GPS_TripHeader h)";
                if (bChkArc)
                    sql += @" union --ArchiveData
                            SELECT td.*, g.DateTimeGPS_UTC
                            , row_number() over(order by g.DateTimeGPS_UTC) as RowNum
                            from GFI_ARC_TripDetail td
	                        left outer join #GFI_GPS_GPSData g on td.GPSDataUID = g.UID
	                        where HeaderiID in (select iID from #GFI_GPS_TripHeader h)";

                //else //if getting errors, remove ', g.DateTimeGPS_UTC' from cmd, and uncomment else 
                sql += "            order by g.DateTimeGPS_UTC";
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "TripDetail";
                dtSql.Rows.Add(drSql);
            }

            //GPSData
            if (bhasDetails)
            {
                sql = "SELECT GPSDataUID from GFI_GPS_TripDetail td where HeaderiID in (" + SqlH + ")";
                SqlH = sql;
                sql = "select * from #GFI_GPS_GPSData where UID in (" + SqlH + ")";

                if (bChkArc)
                {
                    String sqlArc = "--ArchiveData\r\n" + " union " + sql.Replace("GFI_GPS_TripDetail", "GFI_ARC_TripDetail");
                    sql += sqlArc;
                }

                sql += " order by AssetID, DateTimeGPS_UTC";
            }
            else
            {
                sql = @"select * from #Assets a
	                        inner join #GFI_GPS_TripHeader h on a.iAssetID = h.AssetID 
	                        inner join #GFI_GPS_GPSData g on (h.GPSDataStartUID = g.UID or h.GPSDataEndUID = g.UID) 
                        where h.GPSDataStartUID <= h.GPSDataEndUID";
                sql = "select 1 --Oracle Add SemiColumn";
            }
            if (bByDriver)
                sql = sql.Replace("where h.AssetID in (", "where h.DriverID in (");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "GPSData";
            dtSql.Rows.Add(drSql);

            if (bhasDetails)
            {
                sql = "select UID from #GFI_GPS_GPSData where UID in (" + SqlH + ")";
                SqlH = sql;
                sql = "select * from GFI_GPS_GPSDataDetail where UID in (" + SqlH + ")";

                if (bChkArc)
                    sql += "--ArchiveData\r\n" + "union select * from GFI_ARC_GPSDataDetail where UID in (" + SqlH + ")";

                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "GPSDataDetail";
                dtSql.Rows.Add(drSql);
            }

            #region  Totals
            String sqlTotal = "\r\n";
            if (p.RowFrom == 1)
            {

                if (bChkArc)
                {
                    sqlTotal = @"--PostGreSQLTotalStart[    --ArchiveData
                        SELECT x.totalTrips + y.totalTrips as totalTrips, 
                            x.maxSpeed + y.maxSpeed as maxSpeed,
                            x.totalTripDistance + y.totalTripDistance as totalTripDistance,
                            CAST(DATEADD(second, x.totalTripTime + y.totalTripTime, '1900-01-01') AS TIME)  as totalTripTime,
                            CAST(DATEADD(second, x.totalStopTime + y.totalStopTime, '1900-01-01') AS TIME) as totalStopTime,
                            CAST(DATEADD(second, x.totalIdlingTime + y.totalIdlingTime, '1900-01-01') AS TIME) as totalIdlingTime
                            FROM (";
                    sqlTotal += @"SELECT count(1) totalTrips, Max(maxSpeed) maxSpeed
				        , Round(sum(TripDistance), 0) totalTripDistance
				        , sum(TripTime) totalTripTime
				        , sum(StopTime) totalStopTime
				        , sum(IdlingTime) totalIdlingTime
                            from #Assets a 
                        inner join GFI_GPS_TripHeader h on a.iAssetID = H.AssetID";
                    if (bByDriver)
                        sqlTotal += @" inner join #myAssets m on m.AssetID = H.AssetID ";
                    sqlTotal += @"
                            where h.GPSDataStartUID <= h.GPSDataEndUID
	                            and h.dtStart >= ?
	                            and h.dtStart <= ? 
                                and h.TripDistance >= " + iMinTripDistance + " " + sWorkHrs;
                    sqlTotal += @") as x, ";
                    sqlTotal += @"(SELECT count(1) totalTrips, Max(maxSpeed) maxSpeed
				        , Round(sum(TripDistance), 0) totalTripDistance
				        , sum(TripTime) totalTripTime
				        , sum(StopTime) totalStopTime
				        , sum(IdlingTime) totalIdlingTime
                            from #Assets a 
                        inner join GFI_ARC_TripHeader h on a.iAssetID = H.AssetID";
                    if (bByDriver)
                        sqlTotal += @" inner join #myAssets m on m.AssetID = H.AssetID ";
                    sqlTotal += @"
                            where h.GPSDataStartUID <= h.GPSDataEndUID
	                            and h.dtStart >= ?
	                            and h.dtStart <= ? 
                                and h.TripDistance >= " + iMinTripDistance + " " + sWorkHrs;
                    sqlTotal += ") as y \r\n\r\n--]PostGreSQLTotalEnd";
                }
                else
                {
                    sqlTotal += "\r\n";
                    sqlTotal += @"SELECT count(1) totalTrips, Max(maxSpeed) maxSpeed
				        , Round(sum(TripDistance), 0) totalTripDistance
				        , sum(TripTime) totalTripTime
				        , sum(StopTime) totalStopTime
				        , sum(IdlingTime) totalIdlingTime
                            from #Assets a 
                        inner join GFI_GPS_TripHeader h on a.iAssetID = H.AssetID";
                    if (bByDriver)
                        sqlTotal += @" inner join #myAssets m on m.AssetID = H.AssetID ";
                    sqlTotal += @"
                            where h.GPSDataStartUID <= h.GPSDataEndUID
	                            and h.dtStart >= ?
	                            and h.dtStart <= ? 
                                and h.TripDistance >= " + iMinTripDistance + " " + sWorkHrs;
                }

            }
            else
                sqlTotal = "SELECT 1 totalTrips, 1 maxSpeed, 1 totalTripDistance, 1 totalTripTime, 1 totalStopTime, 1 totalIdlingTime";
            sqlTotal += " --Oracle Add SemiColumn";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sqlTotal;
            drSql["sTableName"] = "Totals";
            drSql["sParam"] = dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");
            dtSql.Rows.Add(drSql);
            #endregion
            #region DailySummary report
            if (bDailySummary)
            {
                #region sort DailySummarizedTrips
                String sSort1 = "registrationNo, date";
                if (sortColumns != null && sortColumns.Count > 0)
                {
                    List<String> lCols1 = new List<string>() { "registrationNo", "date", "driver", "trips", "startTime", "endTime", "kmTravelled", "idlingTime", "drivingTime", "inZoneStopTime", "outZoneStopTime" };

                    String ss = String.Join(",", sortColumns
                        .Where(s => !String.IsNullOrWhiteSpace(s))
                        .Where(s => lCols1.Contains(s))
                        .Select(s => "h." + s));

                    if (!String.IsNullOrEmpty(ss))
                        sSort1 = ss;
                }

                if (sortOrderAsc)
                    sSort1 += " asc";
                else
                    sSort1 += " desc";

                #endregion

                sql = @"
--SelectIntoStarts
select GroupDate [date]
	, (SELECT top 1 Asset from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.GroupDate = h.GroupDate) [registrationNo]
	, STUFF((SELECT ', ' + Driver from (select distinct Driver from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.GroupDate = h.GroupDate) Driver FOR XML PATH('')), 1, 1, '') driver
	, (select count(1) from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.GroupDate = h.GroupDate) trips
	, (select min(dtStart) from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.GroupDate = h.GroupDate) startTime
	, (select max(dtEnd) from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.GroupDate = h.GroupDate) endTime
	, Round((select Sum(TripDistance) from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.GroupDate = h.GroupDate), 0) kmTravelled
	, CAST(DATEADD(second, (select Sum(IdlingTime) from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.GroupDate = h.GroupDate), '1900-01-01') AS TIME) idlingTime
	, CAST(DATEADD(second, (select Sum(TripTime) from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.GroupDate = h.GroupDate), '1900-01-01') AS TIME) drivingTime
	, CAST(DATEADD(second, (select Sum(StopTime) from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.GroupDate = h.GroupDate and t.iArrivalZone is not null), '1900-01-01') AS TIME) inZoneStopTime
	, CAST(DATEADD(second, (select Sum(StopTime) from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.GroupDate = h.GroupDate and t.iArrivalZone is null), '1900-01-01') AS TIME) outZoneStopTime
into #DailySummary from #GFI_GPS_TripHeader h
	group by GroupDate, AssetID
--SelectIntoEnds

update #DailySummary set inZoneStopTime = '00:00:00' where inZoneStopTime is null --Oracle Add SemiColumn
update #DailySummary set OutZoneStopTime = '00:00:00' where OutZoneStopTime is null --Oracle Add SemiColumn
update #DailySummary set idlingTime = '00:00:00' where idlingTime is null --Oracle Add SemiColumn
update #DailySummary set drivingTime = '00:00:00' where drivingTime is null --Oracle Add SemiColumn

select * from #DailySummary order by " + sSort1.Replace("h.", "") + @"
";
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "DailySummary";
                dtSql.Rows.Add(drSql);
            }
            #endregion
            #region SummarizedTrips Report
            if (bSummarizedTrips)
            {
                #region sort SummarizedTrips
                String sSort2 = "registrationNo";
                if (sortColumns != null && sortColumns.Count > 0)
                {
                    List<String> lCols2 = new List<string>() { "registrationNo", "driver", "kmTravelled", "stopLess15Min", "stopLess30Min", "stopLess60Min", "stopLess5Hrs", "stopMore5Hrs", "trips", "idlingTime", "drivingTime", "speedMore70Kms", "speedMore110Kms", "CurrentOdometer" };

                    String ss = String.Join(",", sortColumns
                        .Where(s => !String.IsNullOrWhiteSpace(s))
                        .Where(s => lCols2.Contains(s))
                        .Select(s => "h." + s));

                    if (!String.IsNullOrEmpty(ss))
                        sSort2 = ss;
                }

                if (sortOrderAsc)
                    sSort2 += " asc";
                else
                    sSort2 += " desc";

                #endregion

                sql = @"
select 
      (select top 1  t.AssetID from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID) [assetid]
    , (select top 1 Asset from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID) [registrationNo]
	, STUFF((SELECT ', ' + Driver from (select distinct Driver from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID) Driver FOR XML PATH('')), 1, 1, '') driver
     , (SELECT max(MaxSpeed) from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID) MaxSpeed
	, Round((select Sum(TripDistance) from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID), 0) kmTravelled
	--, (select count(1) from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.StopTime < 15 * 60) stopLess15Min
	, (select count(1) from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.StopTime >= 15 * 60 and t.StopTime < 30 * 60) stopLess30Min
	, (select count(1) from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.StopTime >= 30 * 60 and t.StopTime < 60 * 60) stopLess60Min
	, (select count(1) from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.StopTime >= 60 * 60 and t.StopTime < 60 * 60 * 5) stopLess5Hrs
	, (select count(1) from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.StopTime >= 60 * 60 * 5) stopMore5Hrs
	, (select count(1) from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID) trips
	--, CAST(DATEADD(second, (select Sum(IdlingTime) from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID), '1900-01-01') AS TIME) idlingTime
	--, CAST(DATEADD(second, (select Sum(TripTime) from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID), '1900-01-01') AS TIME) drivingTime
	, (select Sum(IdlingTime) from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID) idlingTime
	, (select Sum(TripTime) from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID) drivingTime
	, (select count(1) from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.MaxSpeed > 70 and t.MaxSpeed <= 110) speedMore70Kms
    , (select count(1) from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.MaxSpeed > 80 and t.MaxSpeed <= 110) speedMore80Kms
	, (select count(1) from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.MaxSpeed > 110) speedMore110Kms
	, (select Max(CalculatedOdometer) from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID) CurrentOdometer
    , (select Count(1) from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID and tsStopTime  not in( '00:00:00.0000000')) Stops
	, (select count(1) from #GFI_GPS_TripHeader t where t.AssetID = h.AssetID and t.StopTime >= 30 * 60 ) stopMore60Min
from #GFI_GPS_TripHeader h group by AssetID order by " + sSort2.Replace("h.", "") + @"
";
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "SummarizedTrips";
                dtSql.Rows.Add(drSql);
            }
            #endregion
            #region SpatialData
            if (bSpatialData)
            {
                sql = @"
select iid TripID, row_number() OVER (order by AssetID, dtStart) AS OrdNo, CHECKSUM(NEWID()) color
		, Geometry::STMLineFromText(
		    'MULTILINESTRING ((' + STUFF(
				    (SELECT ', ' + CONVERT(varchar, g.Longitude, 128) + ' ' + CONVERT(varchar, g.Latitude, 128) FROM GFI_GPS_TripDetail d inner join GFI_GPS_GPSData g on g.UID = d.GPSDataUID Where d.HeaderiID = h.iID order by g.DateTimeGPS_UTC FOR XML PATH(''))
				    , 1, 1, '') + '))' 
		    , 4326).ToString() geomData
from #GFI_GPS_TripHeader h
";
                drSql = dtSql.NewRow();
                drSql["sSQL"] = sql;
                drSql["sTableName"] = "SpatialData";
                dtSql.Rows.Add(drSql);
            }
            #endregion

            int iTmp = 0;
            foreach (DataRow dr in dtSql.Rows)
            {
                sqlMain += "\r\n";
                if (!String.IsNullOrEmpty(dr["sTableName"].ToString()))
                {
                    if (iTmp == 0)
                        sqlMain += "\r\n--[PosgreSQLMoveDownStart\r\n";

                    iTmp++;
                    sqlMain += "SELECT '" + dr["sTableName"] + "' AS TableName";
                }
                sqlMain += "\r\n";
                String s = dr["sSQL"].ToString();
                if (bByDriver)
                {
                    s = s.Replace("= h.AssetID", "= h.DriverID");

                    if (dr["sTableName"].ToString() == "DailySummary")
                    {
                        s = s.Replace("t.AssetID = h.DriverID", "t.DriverID = h.DriverID");
                        s = s.Replace("group by GroupDate, AssetID", "group by GroupDate, DriverID");
                    }
                    else if (dr["sTableName"].ToString() == "SummarizedTrips")
                    {
                        s = s.Replace("t.AssetID = h.DriverID", "t.DriverID = h.DriverID");
                        s = s.Replace("group by AssetID order by RegistrationNo", "group by DriverID order by Driver");
                    }
                    else if (dr["sTableName"].ToString() == "Totals")
                    {
                        s = s.Replace("a.iAssetID = H.AssetID", "a.iDriverID = H.DriverID");
                    }
                }
                sqlMain += s;
                sqlMain += "\r\n";
                if (dr["sParam"].ToString().Length > 0)
                    strParamMain += "¬" + dr["sParam"];
            }

            sqlMain += "\r\n";
            sqlMain += @"
--Oracle ends Here
drop table #GFI_GPS_TripHeader --Oracle Add SemiColumn
drop table #GFI_GPS_GPSData --Oracle Add SemiColumn
drop table #Assets --Oracle Add SemiColumn
drop table #FltAssets --Oracle Add SemiColumn
drop table #FltDrivers --Oracle Add SemiColumn
drop table #myAssets --Oracle Add SemiColumn
";
            if (bGetZones)
                sqlMain += @"
drop table #ArrivalZoneTrips --Oracle Add SemiColumn
drop table #DepartureZoneTrips --Oracle Add SemiColumn
";
            if (bDailySummary)
                sqlMain += "\r\ndrop table #DailySummary --Oracle Add SemiColumn";
            sqlMain += "\r\n--]PosgreSQLMoveDownEnd";

            dtSql.Rows.Clear();
            sql = sqlMain;
            if (dtFrom == new DateTime(1975, 10, 08))
            {
                sql = sql.Replace("and h.dtStart >= ?", String.Empty);
                sql = sql.Replace("and h.dtStart <= ? ", String.Empty);
                sql = sql.Replace("#Assets", "GFI_FLT_Asset");
                sql = sql.Replace("iAssetID", "AssetID");
            }
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = strParamMain;
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr, 1800);
            return ds;
        }

        public DataSet GetTripHeaders(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, Double iMinTripDistance, String sWorkHrs, Boolean bByDriver, Boolean bClacOdometer, Boolean bGetZones, List<Matrix> lMatrix, String sConnStr, int? Page, int? LimitPerPage, Boolean bHasExceptions, List<String> sortColumns, Boolean sortOrderAsc)
        {
            DataSet ds = GetTripsDS(lAssetID, dtFrom, dtTo, iMinTripDistance, sWorkHrs, bByDriver, bClacOdometer, false, bGetZones, lMatrix, sConnStr, Page, LimitPerPage, null, bHasExceptions, sortColumns, sortOrderAsc);
            Boolean bFinish = false;
            if (ds == null)
                bFinish = true;
            else if (!ds.Tables.Contains("TripHeader"))
                bFinish = true;

            if (bFinish)
            {
                DataTable dtN = new DataTable();
                dtN.Columns.Add("Vehicle");
                dtN.Columns.Add("Driver");
                dtN.Columns.Add("DepartureZone");
                dtN.Columns.Add("ArrivalZone");
                dtN.Columns.Add("DepartureAddress");
                dtN.Columns.Add("ArrivalAddress");
                dtN.Columns.Add("From", typeof(DateTime));
                dtN.Columns.Add("GroupDate", typeof(DateTime));
                dtN.Columns.Add("To", typeof(DateTime));
                dtN.Columns.Add("ArrivalZoneID", typeof(int));
                dtN.Columns.Add("iColor", typeof(int));
                dtN.Columns.Add("tsIdlingTime", typeof(TimeSpan));
                dtN.Columns.Add("tsStopTime", typeof(TimeSpan));
                dtN.Columns.Add("tsTripTime", typeof(TimeSpan));
                dtN.TableName = "TripHeader";

                ds = new DataSet();
                ds.Tables.Add(dtN);
                return ds;
            }

            ds.Tables["TripHeader"].Columns["Asset"].ColumnName = "Vehicle";
            ds.Tables["TripHeader"].Columns["dtEnd"].ColumnName = "To";
            ds.Tables["TripHeader"].Columns["dtStart"].ColumnName = "From";

            ds.Tables["TripHeader"].Columns["sDepartureZone"].ColumnName = "DepartureZone";
            ds.Tables["TripHeader"].Columns["iDepartureZone"].ColumnName = "DepartureZoneId";
            ds.Tables["TripHeader"].Columns["iDepartureZoneColor"].ColumnName = "iColorDZ";

            ds.Tables["TripHeader"].Columns["sArrivalZones"].ColumnName = "ArrivalZone";
            ds.Tables["TripHeader"].Columns["iArrivalZone"].ColumnName = "ArrivalZoneID";
            ds.Tables["TripHeader"].Columns["iArrivalZoneColor"].ColumnName = "iColor";

            if (ds.Tables["TripHeader"].Columns.Contains("RowNumber"))
                ds.Tables["TripHeader"].Columns["RowNumber"].ColumnName = "rowNum";
            else
                ds.Tables["TripHeader"].Columns["Row_Number"].ColumnName = "rowNum";

            ds.Tables["TripHeader"].Columns.Remove("IdlingTime");
            ds.Tables["TripHeader"].Columns.Remove("StopTime");
            ds.Tables["TripHeader"].Columns.Remove("TripTime");
            ds.Tables["TripHeader"].Columns["tsIdlingTime"].ColumnName = "IdlingTime";
            ds.Tables["TripHeader"].Columns["tsStopTime"].ColumnName = "StopTime";
            ds.Tables["TripHeader"].Columns["tsTripTime"].ColumnName = "TripTime";

            ds.Tables["TripHeader"].Columns.Remove("OverSpeed1Time");
            ds.Tables["TripHeader"].Columns.Remove("OverSpeed2Time");
            ds.Tables["TripHeader"].Columns["tsOverSpeed1Time"].ColumnName = "OverSpeed1Time";
            ds.Tables["TripHeader"].Columns["tsOverSpeed2Time"].ColumnName = "OverSpeed2Time";

            ds.Tables["TripHeader"].SetColumnsOrder("Vehicle", "Driver", "From", "TripTime", "To", "TripDistance", "StopTime", "DepartureAddress", "ArrivalAddress", "DepartureZone", "ArrivalZone", "IdlingTime", "MaxSpeed");
            DataRow[] drDriver = ds.Tables["TripHeader"].Select("Driver <> 'Default Driver'");
            if (drDriver.Length == 0)
                ds.Tables["TripHeader"].Columns["Driver"].ColumnMapping = MappingType.Hidden;

            if (ds.Tables["TripHeader"].Columns["TripDistance"].ColumnName == "TRIPDISTANCE")
            {
                ds.Tables["TripHeader"].Columns["driver"].ColumnName = "driver";
                ds.Tables["TripHeader"].Columns["from"].ColumnName = "from";
                ds.Tables["TripHeader"].Columns["tripTime"].ColumnName = "tripTime";
                ds.Tables["TripHeader"].Columns["to"].ColumnName = "to";
                ds.Tables["TripHeader"].Columns["tripDistance"].ColumnName = "tripDistance";
                ds.Tables["TripHeader"].Columns["stopTime"].ColumnName = "stopTime";
                ds.Tables["TripHeader"].Columns["departureAddress"].ColumnName = "departureAddress";
                ds.Tables["TripHeader"].Columns["arrivalAddress"].ColumnName = "arrivalAddress";
                ds.Tables["TripHeader"].Columns["departureZone"].ColumnName = "departureZone";
                ds.Tables["TripHeader"].Columns["arrivalZone"].ColumnName = "arrivalZone";
                ds.Tables["TripHeader"].Columns["idlingTime"].ColumnName = "idlingTime";
                ds.Tables["TripHeader"].Columns["maxSpeed"].ColumnName = "maxSpeed";
                ds.Tables["TripHeader"].Columns["iID"].ColumnName = "iID";
                ds.Tables["TripHeader"].Columns["accel"].ColumnName = "accel";
                ds.Tables["TripHeader"].Columns["brake"].ColumnName = "brake";
                ds.Tables["TripHeader"].Columns["corner"].ColumnName = "corner";
                ds.Tables["TripHeader"].Columns["iColor"].ColumnName = "iColor";
                ds.Tables["TripHeader"].Columns["overSpeed1Time"].ColumnName = "overSpeed1Time";
                ds.Tables["TripHeader"].Columns["overSpeed2Time"].ColumnName = "overSpeed2Time";
                ds.Tables["TripHeader"].Columns["exceptionsCnt"].ColumnName = "exceptionsCnt";
                ds.Tables["TripHeader"].Columns["dtUTCStart"].ColumnName = "dtUTCStart";
                ds.Tables["TripHeader"].Columns["dtUTCEnd"].ColumnName = "dtUTCEnd";

                if (ds.Tables.Contains("Totals"))
                {
                    ds.Tables["Totals"].Columns["totalTrips"].ColumnName = "totalTrips";
                    ds.Tables["Totals"].Columns["maxSpeed"].ColumnName = "maxSpeed";
                    ds.Tables["Totals"].Columns["totalTripDistance"].ColumnName = "totalTripDistance";
                    ds.Tables["Totals"].Columns["totalTripTime"].ColumnName = "totalTripTime";
                    ds.Tables["Totals"].Columns["totalStopTime"].ColumnName = "totalStopTime";
                    ds.Tables["Totals"].Columns["totalIdlingTime"].ColumnName = "totalIdlingTime";
                }

            }
            if (ds.Tables["Totals"].Columns.Contains("totaltrips"))
                ds.Tables["Totals"].Columns["totaltrips"].ColumnName = "totalTrips";
            if (ds.Tables["Totals"].Columns.Contains("maxspeed"))
                ds.Tables["Totals"].Columns["maxspeed"].ColumnName = "maxSpeed";
            if (ds.Tables["Totals"].Columns.Contains("totaltripdistance"))
                ds.Tables["Totals"].Columns["totaltripdistance"].ColumnName = "totalTripDistance";
            if (ds.Tables["Totals"].Columns.Contains("totaltriptime"))
                ds.Tables["Totals"].Columns["totaltriptime"].ColumnName = "totalTripTime";
            if (ds.Tables["Totals"].Columns.Contains("totalidlingtime"))
                ds.Tables["Totals"].Columns["totalidlingtime"].ColumnName = "totalIdlingTime";

            //Oracle
            if (ds.Tables.Contains("DailySummary"))
            {
                if (ds.Tables["DailySummary"].Columns.Contains("GroupDate"))
                {
                    ds.Tables["DailySummary"].Columns["GroupDate"].ColumnName = "date";
                    ds.Tables["DailySummary"].Columns["registrationNo"].ColumnName = "registrationNo";
                    ds.Tables["DailySummary"].Columns["driver"].ColumnName = "driver";
                    ds.Tables["DailySummary"].Columns["trips"].ColumnName = "trips";
                    ds.Tables["DailySummary"].Columns["startTime"].ColumnName = "startTime";
                    ds.Tables["DailySummary"].Columns["endTime"].ColumnName = "endTime";
                    ds.Tables["DailySummary"].Columns["kmTravelled"].ColumnName = "kmTravelled";
                    ds.Tables["DailySummary"].Columns["idlingTime"].ColumnName = "idlingTime";
                    ds.Tables["DailySummary"].Columns["drivingTime"].ColumnName = "drivingTime";
                    ds.Tables["DailySummary"].Columns["inZoneStopTime"].ColumnName = "inZoneStopTime";
                    ds.Tables["DailySummary"].Columns["outZoneStopTime"].ColumnName = "outZoneStopTime";
                }
            }

            if (iMinTripDistance != NaveoEntity.GenericBypass)
            {
                DataRow[] drAddress = ds.Tables["TripHeader"].Select("ArrivalAddress is null");
                if (drAddress.Length > 0)
                    SimpleAddress.GetAddresses(drAddress, "ArrivalAddress", "fEndLon", "fEndLat");

                drAddress = ds.Tables["TripHeader"].Select("DepartureAddress is null");
                if (drAddress.Length > 0)
                    SimpleAddress.GetAddresses(drAddress, "DepartureAddress", "fStartLon", "fStartLat");
            }

            return ds;
        }
        public DataSet GetDailySummarizedTrips(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, Boolean bDriver, List<Matrix> lMatrix, String sConnStr, List<String> sortColumns, Boolean sortOrderAsc)
        {
            DataSet ds = GetTripHeaders(lAssetID, dtFrom, dtTo, NaveoEntity.GenericBypass, "DailySummary", bDriver, false, true, lMatrix, sConnStr, null, null, true, sortColumns, sortOrderAsc);
            ds.Tables["DailySummary"].TableName = "Report";
            return ds;
        }
        public DataTable GetSummarizedTrips(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, Boolean bDriver, List<Matrix> lMatrix, String sConnStr, List<String> sortColumns, Boolean sortOrderAsc)
        {
            DataSet ds = GetTripHeaders(lAssetID, dtFrom, dtTo, 0, "SummarizedTrips", bDriver, true, false, lMatrix, sConnStr, null, null, false, sortColumns, sortOrderAsc);
            DataTable dt = ds.Tables["SummarizedTrips"];

            if (dt != null)
            {
                if (dt.Columns.Contains("registrationno"))
                    dt.Columns["registrationno"].ColumnName = "registrationNo";

                if (dt.Columns.Contains("rownum"))
                    dt.Columns["rowNum"].ColumnName = "rowNum";


                if (dt.Columns.Contains("driver"))
                    dt.Columns["driver"].ColumnName = "driver";


                if (dt.Columns.Contains("kmtravelled"))
                    dt.Columns["kmtravelled"].ColumnName = "kmTravelled";


                if (dt.Columns.Contains("speedmore80kms"))
                    dt.Columns["speedmore80kms"].ColumnName = "speedMore80Kms";


                if (dt.Columns.Contains("stopless30min"))
                    dt.Columns["stopless30Min"].ColumnName = "stopLess30Min";


                if (dt.Columns.Contains("stopless60min"))
                    dt.Columns["stopless60min"].ColumnName = "stopLess60Min";


                if (dt.Columns.Contains("stopless5hrs"))
                    dt.Columns["stopless5hrs"].ColumnName = "stopLess5Hrs";

                if (dt.Columns.Contains("stopMore5hrs"))
                    dt.Columns["stopMore5hrs"].ColumnName = "stopMore5Hrs";

                if (dt.Columns.Contains("idlingtime"))
                    dt.Columns["idlingtime"].ColumnName = "idlingTime";

                if (dt.Columns.Contains("drivingtime"))
                    dt.Columns["drivingtime"].ColumnName = "drivingTime";

                if (dt.Columns.Contains("speedmore70Kms"))
                    dt.Columns["speedmore70Kms"].ColumnName = "speedMore70Kms";

                if (dt.Columns.Contains("speedMore110Kms"))
                    dt.Columns["speedmore110Kms"].ColumnName = "speedMore110Kms";

                if (dt.Columns.Contains("currentodometer"))
                    dt.Columns["currentodometer"].ColumnName = "CurrentOdometer";

                if (dt.Columns.Contains("maxspeed"))
                    dt.Columns["maxspeed"].ColumnName = "MaxSpeed";

                //foreach (DataRow dr in dt.Rows)
                //{
                //    int.TryParse(dr["drivingTimeInSec"].ToString(), out int idt);
                //    dr["drivingTime"] = TimeSpan.FromSeconds(idt);

                //    int.TryParse(dr["idlingTimeInSec"].ToString(), out int iit);
                //    dr["idlingTime"] = TimeSpan.FromSeconds(iit);
                //}

                //if (dt.Columns.Contains("drivingTimeInSec"))
                //    dt.Columns.Remove("drivingTimeInSec");

                //if (dt.Columns.Contains("idlingTimeInSec"))
                //    dt.Columns.Remove("idlingTimeInSec");
            }

            //if (dt.Columns["registrationNo"].ColumnName == "REGISTRATIONNO")
            //{
            //    dt.Columns["registrationNo"].ColumnName = "registrationNo";
            //    dt.Columns["driver"].ColumnName = "driver";
            //    dt.Columns["kmTravelled"].ColumnName = "kmTravelled";
            //    dt.Columns["stopLess15Min"].ColumnName = "stopLess15Min";
            //    dt.Columns["stopLess30Min"].ColumnName = "stopLess30Min";
            //    dt.Columns["stopLess60Min"].ColumnName = "stopLess60Min";
            //    dt.Columns["stopLess5Hrs"].ColumnName = "stopLess5Hrs";
            //    dt.Columns["stopMore5Hrs"].ColumnName = "stopMore5Hrs";
            //    dt.Columns["trips"].ColumnName = "trips";
            //    dt.Columns["idlingTime"].ColumnName = "idlingTime";
            //    dt.Columns["drivingTime"].ColumnName = "drivingTime";
            //    dt.Columns["speedMore70Kms"].ColumnName = "speedMore70Kms";
            //    dt.Columns["speedMore110Kms"].ColumnName = "speedMore110Kms";
            //    dt.Columns["CurrentOdometer"].ColumnName = "CurrentOdometer";
            //}

            return dt;
        }
        public DataTable GetTripsSpatial(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, Boolean byDriver, List<Matrix> lMatrix, String sConnStr)
        {
            DataSet ds = GetTripsDS(lAssetID, dtFrom, dtTo, 0, "SpatialData", byDriver, false, false, false, lMatrix, sConnStr, null, null, null, false, null, false);
            DataTable dt = ds.Tables["SpatialData"];

            return dt;
        }

        public DataSet GetDailyTripTime(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, String sTotalField, Boolean bByDriver, String sConnStr)
        {
            if (lAssetID.Count == 0)
                return null;

            //sTotalField: Trip, Idle, Stop, Distance
            switch (sTotalField)
            {
                case ("Trip"):
                    sTotalField = "ts_totalTripTime";
                    break;

                case ("Idle"):
                    sTotalField = "ts_totalIdlingTime";
                    break;

                case ("Stop"):
                    sTotalField = "ts_totalStopTime";
                    break;

                case ("Distance"):
                    sTotalField = "totalTripDistance";
                    break;

                default:
                    sTotalField = "ts_totalTripTime";
                    break;
            }

            lAssetID = lAssetID.Distinct().ToList();

            Boolean bChkArc = Globals.bNeedToQryArc(sConnStr, dtFrom);
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sqlAsset = @"
--GetDailyTripTime  [" + sTotalField + @"]
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 

CREATE TABLE #Assets
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL,
	[iAssetID] [int] PRIMARY KEY CLUSTERED
);
";
            String sAsset = String.Empty;
            foreach (int i in lAssetID)
                sAsset += "insert #Assets (iAssetID) values (" + i.ToString() + ");\r\n";

            String sql = sqlAsset + sAsset + @"
--statement
SELECT h.AssetID, cast(dtStart as date) dtStart, count(1) totalTrips, Max(maxSpeed) maxSpeed, cast(sum(TripDistance) as decimal (8,2)) totalTripDistance, sum(TripTime) totalTripTime, sum(StopTime) totalStopTime, sum(IdlingTime) totalIdlingTime --, sum(dbo.GetCalcOdo(h.AssetId)) AS CalculatedOdometer, sum(dbo.GetCalcEngHrs(h.AssetId)) AS CalculatedEngHrs
    into #TripHeader
    from #Assets a 
inner join GFI_GPS_TripHeader h on a.iAssetID = h.AssetID
    where h.GPSDataStartUID <= h.GPSDataEndUID
	   and h.dtStart >= ?
	   and h.dtStart <= ? 
group by
	h.AssetID, cast(dtStart as date)

";
            if (bChkArc)
                sql += @"
insert #TripHeader 
	SELECT h.AssetID, cast(dtStart as date) dtStart, count(1) totalTrips, Max(maxSpeed) maxSpeed, cast(sum(TripDistance) as decimal (8,2)) totalTripDistance, sum(TripTime) totalTripTime, sum(StopTime) totalStopTime, sum(IdlingTime) totalIdlingTime --, sum(dbo.GetCalcOdo(h.AssetId)) AS CalculatedOdometer, sum(dbo.GetCalcEngHrs(h.AssetId)) AS CalculatedEngHrs
		from #Assets a 
	inner join GFI_ARC_TripHeader h on a.iAssetID = h.AssetID
		where h.GPSDataStartUID <= h.GPSDataEndUID
		   and h.dtStart >= ?
		   and h.dtStart <= ? 
	group by
		h.AssetID, cast(dtStart as date)
";
            sql += @"

select a.AssetName, t.*
    , (CONVERT(varchar, DATEADD(s, totalIdlingTime, 0), 108)) ts_totalIdlingTime 
    --, (CONVERT(varchar, DATEADD(s, totalStopTime, 0), 108)) ts_totalStopTime 
    , (CONVERT(varchar, DATEADD(s, totalTripTime, 0), 108)) ts_totalTripTime 
into #TH
from #TripHeader t
    	inner join GFI_FLT_Asset a on t.AssetID = a.AssetID
order by dtStart, AssetName

alter table #TH add Total varchar(20)

update #TH 
    set Total = t2.Total
from #TH t1
	inner join (select AssetID, cast(sum(totalTripTime)/86400 as varchar(50))+':'+ Convert(VarChar, DateAdd(S, sum(totalTripTime), 0), 108) AS Total from #TH group by AssetID) t2 on t1.AssetID = t2.AssetID

select 'TripHeader' as TableName
select * from #TH order by dtStart, AssetName

--Pivot Table
DECLARE @DynamicPivotQuery AS NVARCHAR(MAX), @PivotColumnNames AS NVARCHAR(MAX)

--Get distinct values of the PIVOT Column
SELECT @PivotColumnNames= ISNULL(@PivotColumnNames + ',','') + QUOTENAME(dtStart)
	FROM (SELECT DISTINCT dtStart FROM #TH) AS cat
order by dtStart

SET @DynamicPivotQuery = 
N'
SELECT * FROM 
(
    SELECT dtStart, AssetName sDesc, " + sTotalField + @", Total  
    FROM #TH
) as s
PIVOT(max(" + sTotalField + @") for dtStart in (' + @PivotColumnNames + ')) as pvt';

select 'Pivot' as TableName
--Execute the Dynamic Pivot Query
EXEC sp_executesql @DynamicPivotQuery
";

            if (bByDriver)
            {
                sql = sql.Replace("AssetID", "DriverID");
                sql = sql.Replace("GFI_FLT_Asset", "GFI_FLT_Driver");
                sql = sql.Replace("AssetName", "sDriverName");
            }

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "MultipleDTfromQuery";
            drSql["sParam"] = dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");
            dtSql.Rows.Add(drSql);

            //TripExceptions
            //       sql = @"select h.iID, count(1) cnt from GFI_GPS_Exceptions e
            //                inner join GFI_GPS_TripDetail d on d.GPSDataUID = e.GPSDataID
            //                   inner join #GFI_GPS_TripHeader h on h.iID = d.HeaderiID
            //where
            //                   e.DateTimeGPS_UTC >= ? and e.DateTimeGPS_UTC <= ?
            //               group by h.iID, GroupID, RuleID ";
            //       drSql = dtSql.NewRow();
            //       drSql["sSQL"] = sql;
            //       drSql["sTableName"] = "TripExceptions";
            //       drSql["sParam"] = dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");
            //       dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr, 300);

            return ds;
        }

        public TripHeader GetCompleteTripById(String iID, List<Zone> lz, Boolean bReqestAddress, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = "SELECT * from GFI_GPS_TripHeader (nolock) WHERE iID = '" + iID + "'";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "TripHeader";
            dtSql.Rows.Add(drSql);

            sql = @"SELECT * 
                    from GFI_GPS_TripDetail (nolock) td
                    inner join GFI_GPS_GPSData (nolock) g on td.GPSDataUID = g.UID
                    where HeaderiID = '" + iID + "'order by g.DateTimeGPS_UTC";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "TripDetails";
            dtSql.Rows.Add(drSql);

            sql = @"SELECT Distinct a.AssetID, 
	                    AssetNumber, AssetName,
	                    AssetType, 
	                    Status_b, TimeZoneID, Odometer,
	                    CreatedDate, 
	                    CreatedBy, 
	                    UpdatedDate, 
	                    UpdatedBy 
                    from GFI_FLT_Asset a
                    inner join GFI_GPS_TripHeader (nolock) h on h.AssetID = a.AssetID
                    where h.iID = '" + iID + "'";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "Asset";
            dtSql.Rows.Add(drSql);

            sql = @"SELECT u.DriverID, iButton,
                        sDriverName, 
                        sEmployeeNo, 
                        sComments, EmployeeType 
                    from GFI_FLT_Driver u
                    inner join GFI_GPS_TripHeader (nolock) h on h.DriverID = u.DriverID
                    where h.iID = '" + iID + "'";
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "Driver";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            if (ds.Tables.Contains("dtErr"))
                return null;

            DataTable dtTripHeader = ds.Tables["TripHeader"];
            if (dtTripHeader.Rows.Count == 0)
                return null;

            List<TripHeader> lth = new myConverter().ConvertToList<TripHeader>(ds.Tables["TripHeader"]);
            TripHeader th = lth[0];

            TripDetailService td = new TripDetailService();
            DataTable dtTripDetails = ds.Tables["TripDetails"];
            List<TripDetail> l = new List<TripDetail>();
            foreach (DataRow dr in dtTripDetails.Rows)
                l.Add(td.GetTripDetail(dr));
            th.TDetails = l;

            List<Asset> lAsset = new myConverter().ConvertToList<Asset>(ds.Tables["Asset"]);
            foreach (Asset a in lAsset)
            {
                String tzName = a.TimeZoneID;
                if (tzName == String.Empty)
                    tzName = TimeZone.CurrentTimeZone.StandardName;
                a.TimeZoneTS = new Utils().GetTimeSpan(tzName);
            }
            List<Driver> lDriver = new myConverter().ConvertToList<Driver>(ds.Tables["Driver"]);

            th.myAsset = lAsset.Find(o => o.AssetID == th.AssetID);
            th.myDriver = lDriver.Find(o => o.DriverID == th.DriverID);

            return th;
        }
        public DataTable getCalculatedOdo(List<int> lAssetID, DateTime dtTo, String sConnStr)
        {
            if (lAssetID.Count == 0)
                return null;


            String sql = @"
--GetOdometer
            SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 

            CREATE TABLE #Assets
            (
	            RowNumber int IDENTITY(1,1) NOT NULL,
	            [iAssetID] [int] PRIMARY KEY CLUSTERED
            );
            ";

            lAssetID = lAssetID.Distinct().ToList();
            foreach (int i in lAssetID)
                sql += "insert #Assets (iAssetID) values (" + i.ToString() + ");\r\n";


            sql += @"SELECT iAssetID assetID
	                    , dbo.GetCalcOdo(iAssetId) - coalesce((select sum(tripDistance) from GFI_GPS_TripHeader where GPSDataStartUID <= GPSDataEndUID and dtEnd >= ? and AssetID = iAssetID), 0) AS calculatedOdometer
	                    , dbo.GetCalcEngHrs(iAssetId) - coalesce((select sum(tripTime) from GFI_GPS_TripHeader where GPSDataStartUID <= GPSDataEndUID and dtEnd >= ? and AssetID = iAssetID), 0) calculatedEngHrs
                     FROM #Assets a ";

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff")
;
            drSql["sTableName"] = "TripHeader";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds != null)
                if (ds.Tables.Count > 0)
                    if (ds.Tables[0].TableName == "TripHeader")
                        return ds.Tables[0];
            return new DataTable();
        }

        public string getServerName()
        {

            string sServerName = System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            User.NaveoDatabases myEnum;
            //Getting only the enum part
            sServerName = sServerName.Replace(".naveo.mu", String.Empty);
            //sServerName = sServerName.Replace(":44393", String.Empty);
            sServerName = sServerName.Replace("https://", String.Empty);
            sServerName = sServerName.Replace("http://", String.Empty);
            if (Enum.TryParse(sServerName, true, out myEnum))
                sServerName = sServerName;
            else
                sServerName = sServerName;



            sServerName = sServerName.ToUpper();


            return sServerName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lAssetID"></param>
        /// <param name="dtTo"></param>
        /// <param name="sConnStr"></param>
        /// <author>Yaseen</author>
        /// <returns></returns>
        public DataTable getInitialOdoInput(List<int> lAssetID, DateTime dtTo, String sConnStr)
        {
            if (lAssetID.Count == 0)
                return null;

            string strRowCommaSeparated = "";

            int cpt = 0;
            foreach (int assetId in lAssetID)
            {

                if (cpt == 0)
                {
                    strRowCommaSeparated = assetId.ToString();
                }
                else
                {
                    strRowCommaSeparated += ", " + assetId.ToString();
                }



                cpt++;
            }



            Connection c = new Connection(NaveoOneLib.Constant.NaveoEntity.MaintDB);

#if DEBUG
            string sServerName = "BLACKWIDOW";
#else
            string sServerName = getServerName();
#endif

            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = "select 'VehicleMaintenance' as TableName;\r\n";
            sql += "select COALESCE(vm.\"ActualOdometer\", 0) as ActualOdometer, cv.\"FMSAssetId\" as assetID from dbo.\"CustomerVehicle\" cv, dbo.\"FleetServer\" fs, dbo.\"GFI_AMM_VehicleMaintenance\" vm where cv.\"FMSAssetId\" IN (" + strRowCommaSeparated + ") AND vm.\"MaintTypeId_cbo\" = 20 AND vm.\"MaintStatusId_cbo\" = 5 AND cv.\"Id\" = vm.\"AssetId\" AND cv.\"FleetServerId\" = fs.\"Id\" AND fs.\"Name\" = '" + sServerName + "' ";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);


            DataSet ds = c.GetDataDS(dtSql, NaveoOneLib.Constant.NaveoEntity.MaintDB);

            DataTable dt = new DataTable();
            dt.Clear();
            dt.Columns.Add("assetID");
            dt.Columns.Add("RealOdometer");

            DataTable dtCalculatedOdo = getCalculatedOdo(lAssetID, dtTo, sConnStr);


            DataTable dtVehicleMaint = ds.Tables["VehicleMaintenance"];
            if (dtVehicleMaint != null && dtCalculatedOdo != null)
            {
                if (dtVehicleMaint.Rows.Count > 0 && dtCalculatedOdo.Rows.Count > 0)
                {

                    foreach (DataRow rowMaint in dtVehicleMaint.Rows)
                    {
                        foreach (DataRow rowOdo in dtCalculatedOdo.Rows)
                        {
                            if (rowMaint["assetid"].ToString() == rowOdo["assetid"].ToString())
                            {
                                DataRow asset = dt.NewRow();
                                asset["assetID"] = rowMaint["assetid"];

                                asset["RealOdometer"] = Convert.ToInt32(rowMaint["ActualOdometer"].ToString()) + Convert.ToDecimal(rowOdo["calculatedodometer"].ToString());
                                
                                dt.Rows.Add(asset);
                            }
                        }
                    }
                }
            }
            return dt;
        }

        public DataSet getTripDetails(List<int> lTripIDs, String sConnStr)
        {
            String sql = @"
--TripDetails
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 

CREATE TABLE #TripIDs
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL,
	[TripID] [int] PRIMARY KEY CLUSTERED,
)
--Oracle Starts Here
";
            foreach (int i in lTripIDs)
                sql += "insert #TripIDs (TripID) values (" + i.ToString() + ") --Oracle Add SemiColumn\r\n";

            sql += @"
--SelectIntoStarts
SELECT td.HeaderiID headerID, td.GPSDataUID gpsDataUID, DATEADD(MINUTE, t.TotalMinutes, g.DateTimeGPS_UTC) localTime, g.DateTimeServer dateTimeServer, g.Latitude lat, g.Longitude lon, g.LongLatValidFlag lonLatValidFlag
	, ROUND(g.Speed, 0) speed, g.RoadSpeed roadSpeed, g.EngineOn engineOn, g.UID uid
	, STUFF(
				(SELECT ', ' + d.TypeID + ' : ' + d.TypeValue + ' : ' + d.UOM FROM GFI_GPS_GPSDataDetail d Where d.UID = g.UID FOR XML PATH(''))
			, 1, 1, '') As details 
	, convert(varchar(300), NULL) as ruleName, convert(varchar(300), NULL) ruleID, null GrpID, 0 ExcpStart
    , convert(varchar(300), NULL) as Duration
    , th.GPSDataStartUID, GPSDataEndUID
	, g.AssetID, Row_Number() Over ( Order By  g.DateTimeGPS_UTC ) As RowNum
into #td
from GFI_GPS_TripDetail td
	inner join #TripIDs Tid on td.HeaderiID = Tid.TripID
    inner join GFI_GPS_TripHeader th on th.iID = Tid.TripID
	inner join GFI_GPS_GPSData g on td.GPSDataUID = g.UID
	inner join GFI_FLT_Asset a on g.AssetID = a.AssetID
	inner join GFI_SYS_TimeZones t on a.TimeZoneID = t.StandardName
    order by td.HeaderiID, g.DateTimeGPS_UTC

--insert into #td
--SELECT td.HeaderiID headerID, td.GPSDataUID gpsDataUID, DATEADD(MINUTE, t.TotalMinutes, g.DateTimeGPS_UTC) localTime, g.DateTimeServer dateTimeServer, g.Latitude lat, g.Longitude lon, g.LongLatValidFlag lonLatValidFlag
--	, ROUND(g.Speed, 0) speed, g.RoadSpeed roadSpeed, g.EngineOn engineOn, g.UID uid
--	, STUFF(
--				(SELECT ', ' + d.TypeID + ' : ' + d.TypeValue + ' : ' + d.UOM FROM GFI_ARC_GPSDataDetail d Where d.UID = g.UID FOR XML PATH(''))
--			, 1, 1, '') As details 
--	, convert(varchar(300), NULL) as ruleName, null ruleID
--    , th.GPSDataStartUID, GPSDataEndUID
--	, g.AssetID, null GrpID, 0 ExcpStart, Row_Number() Over ( Order By  g.DateTimeGPS_UTC ) As RowNum
--from GFI_ARC_TripDetail td
--	inner join #TripIDs Tid on td.HeaderiID = Tid.TripID
--    	inner join GFI_ARC_TripHeader th on th.iID = Tid.TripID
--	inner join GFI_ARC_GPSData g on td.GPSDataUID = g.UID
--	inner join GFI_FLT_Asset a on g.AssetID = a.AssetID
--	inner join GFI_SYS_TimeZones t on a.TimeZoneID = t.StandardName
--    order by td.HeaderiID, g.DateTimeGPS_UTC
--SelectIntoEnds

--Obsolete
update #td 
	set RuleName = r.RuleName, RuleID = r.ParentRuleID, GrpID = e.GroupID , Duration = CONVERT(varchar, DATEADD(s, DATEDIFF(SECOND, eh1.dateFrom, eh1.dateTo), 0), 108)
from #td f
	inner join GFI_GPS_Exceptions e on f.UID = e.GPSDataID
	inner join GFI_GPS_Rules r on e.RuleID = r.RuleID
    inner join gfi_gps_exceptionsheader eh1 on eh1.RuleID = e.RuleID and  eh1.AssetID = e.AssetID and  eh1.GroupID = e.GroupID

--update #td 
--	set RuleName = r.RuleName, RuleID = r.ParentRuleID, GrpID = e.GroupID
--from #td f
--	inner join GFI_ARC_Exceptions e on f.UID = e.GPSDataID
--	inner join GFI_GPS_Rules r on e.RuleID = r.RuleID

;with cte as
	(select *, row_number() over(partition by AssetID, GrpID, RuleID order by localTime) as ExpRowNum from #td where RuleID is not null)
update #td 
	set ExcpStart = 1
from #td 
	inner join cte on  #td.RowNum = cte.RowNum
where cte.ExpRowNum = 1

--select * from #td order by localTime
--Oracle ends Here

--Drop Table #TripIDs
--drop table #td
--select *, row_number() over(partition by AssetID, GrpID, RuleID order by localTime) as RowNum from #td where RuleID is not null order by localTime

--PostgreSQL ends here
select 'TripDetails' as TableName
select * from #td order by localTime --where RuleID is not null order by localTime

select 'TripExceptions' as TableName
select t.gpsdatauid, r.RuleID, r.RuleName, eh.groupID, eh.dateTo - eh.dateFrom as duration from #td t
	inner join GFI_GPS_Exceptions e on t.UID = e.GPSDataID
	inner join GFI_GPS_Rules r on e.RuleID = r.ParentRuleID and r.RuleID = r.ParentRuleID
    inner join gfi_gps_exceptionsheader eh on eh.RuleID = e.RuleID and  eh.AssetID = e.AssetID and  eh.GroupID = e.GroupID
order by t.localTime

";

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            int iRowNum = 0;
            Double dPrevLon = 0;
            Double dPrevLat = 0;
            DataTable dtTripDetails = ds.Tables["TripDetails"];
            dtTripDetails.Columns.Add("deviceDirection");
            dtTripDetails.Columns.Add("rowNum", typeof(int));
            foreach (DataRow dr in dtTripDetails.Rows)
            {
                Double dLon = Convert.ToDouble(dr["Lon"]);
                Double dLat = Convert.ToDouble(dr["Lat"]);

                if (Convert.ToInt32(dr["GPSDataStartUID"]) == Convert.ToInt32(dr["UID"]))
                {
                    iRowNum = 0;
                    dPrevLon = 0;
                    dPrevLat = 0;
                }
                else
                {
                    iRowNum++;
                }

                Boolean bExcp = false;
                if (!String.IsNullOrEmpty(dr["RuleName"].ToString()))
                    bExcp = true;

                Double dBearing = GeoCalc.calculateBearingTo(dPrevLat, dPrevLon, dLat, dLon);
                Boolean isStop = false;
                String sDetails = dr["Details"].ToString();
                if (sDetails.Contains("Ignition"))
                {
                    if (sDetails.Contains("Ignition : Off") || sDetails.Contains("Ignition : 0"))
                    {
                        isStop = true;
                    }
                    else
                    {
                        dBearing = -999999.999;
                        isStop = false;
                    }
                }
                else if (sDetails.Contains("Heading"))
                {
                    isStop = false;
                }
                else if (sDetails.Contains("Idling"))
                {
                    isStop = false;
                }

                if (iRowNum == 0)
                {
                    isStop = false;
                    dBearing = -999999.999;
                }
                if (Convert.ToInt32(dr["GPSDataEndUID"]) == Convert.ToInt32(dr["UID"]))
                {
                    isStop = true;
                    dBearing = -999999.999;
                }

                Boolean isSuspect = false;
                if (sDetails.Contains("InvalidGpsSignals"))
                {
                    isStop = true;
                    isSuspect = true;
                }

                String FN = GeoCalc.getPointImage(dBearing, isSuspect, isStop, bExcp);
                dPrevLon = dLon;
                dPrevLat = dLat;
                dr["RowNum"] = iRowNum;
                dr["DeviceDirection"] = FN;
            }

            dtTripDetails.Columns.Remove("GPSDataStartUID");
            dtTripDetails.Columns.Remove("GPSDataEndUID");

            if (dtTripDetails.Columns["HeaderID"].ColumnName == "HEADERID")
            {
                dtTripDetails.Columns["headerID"].ColumnName = "headerID";
                dtTripDetails.Columns["gpsDataUID"].ColumnName = "gpsDataUID";
                dtTripDetails.Columns["localTime"].ColumnName = "localTime";
                dtTripDetails.Columns["dateTimeServer"].ColumnName = "dateTimeServer";
                dtTripDetails.Columns["lat"].ColumnName = "lat";
                dtTripDetails.Columns["lon"].ColumnName = "lon";
                dtTripDetails.Columns["lonLatValidFlag"].ColumnName = "lonLatValidFlag";
                dtTripDetails.Columns["speed"].ColumnName = "speed";
                dtTripDetails.Columns["roadSpeed"].ColumnName = "roadSpeed";
                dtTripDetails.Columns["engineOn"].ColumnName = "engineOn";
                dtTripDetails.Columns["UID"].ColumnName = "UID";
                dtTripDetails.Columns["details"].ColumnName = "details";
                dtTripDetails.Columns["ruleName"].ColumnName = "ruleName";
                dtTripDetails.Columns["ruleID"].ColumnName = "ruleID";
            }

            return ds;
        }
        public DataTable GetNumberOfTrips(int iAssetID, DateTime dtFrom, DateTime dtTo, double iMinTripDistance, String sConnStr)
        {
            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"select count(1) TotalTrips, Coalesce(sum(TripDistance), 0) TripDistance, Coalesce(sum(TripTime), 0) TripTime, Coalesce(Sum(IdlingTime), 0) IdlingTime 
                            from GFI_GPS_TripHeader where AssetID = " + iAssetID
                         + @" and GPSDataStartUID <= GPSDataEndUID
                            and dtStart >= ? 
                            and dtStart <= ?
                            and TripDistance >=" + iMinTripDistance;

            String strParamMain = dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = strParamMain;
            drSql["sTableName"] = "Trips";
            dtSql.Rows.Add(drSql);

            //int i = 0;
            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables.Contains("dtErr"))
                return null;

            DataTable dt = ds.Tables[0];
            dt.Columns.Add("tsTripTime", typeof(TimeSpan));
            dt.Columns.Add("tsIdlingTime", typeof(TimeSpan));
            dt.Rows[0]["tsTripTime"] = TimeSpan.FromSeconds(Convert.ToDouble(ds.Tables[0].Rows[0]["TripTime"].ToString()));
            dt.Rows[0]["tsIdlingTime"] = TimeSpan.FromSeconds(Convert.ToDouble(ds.Tables[0].Rows[0]["IdlingTime"].ToString()));
            dt.Columns.Remove("TripTime");
            dt.Columns.Remove("IdlingTime");
            return dt;
        }
        public int GetNumberOfTrips(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            if (lAssetID.Count == 0)
                return 0;

            lAssetID = lAssetID.Distinct().ToList();

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String sql = @"
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 

CREATE TABLE #Assets
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL,
	[iAssetID] [int] PRIMARY KEY CLUSTERED,
)
";
            String sAsset = String.Empty;
            foreach (int i in lAssetID)
                sAsset += "insert #Assets (iAssetID) values (" + i.ToString() + ")\r\n";

            sql += sAsset;
            sql += @"
SELECT 'Tot' AS TableName
SELECT count(1)
        from #Assets a 
        inner join GFI_GPS_TripHeader h on a.iAssetID = h.AssetID
            where h.GPSDataStartUID <= h.GPSDataEndUID
	            and h.dtStart >= ?
	            and h.dtStart <= ?
";

            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sParam"] = dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);

            int x = 0;
            if (ds != null)
                if (ds.Tables.Contains("Tot"))
                    if (ds.Tables["Tot"].Rows.Count > 0)
                        int.TryParse(ds.Tables["Tot"].Rows[0][0].ToString(), out x);

            return x;
        }

        public Double GetKmRun(int AssetID, DateTime dtFr, DateTime dtTo, String sConnStr)
        {
            Double dResult = 0;
            String sql = @"
select sum(TripDistance) 
from GFI_GPS_TripHeader 
where AssetID = ?
    and GPSDataStartUID <= GPSDataEndUID
	and dtStart >= ?
	and dtStart <= ?
group by AssetID";

            Connection c = new Connection(sConnStr);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            String strParamMain = AssetID.ToString() + "¬" + dtFr.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");
            drSql["sSQL"] = sql;
            drSql["sParam"] = strParamMain;
            drSql["sTableName"] = "dtR";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables.Count > 0)
                if (ds.Tables[0].Rows.Count > 0)
                    Double.TryParse(ds.Tables[0].Rows[0][0].ToString(), out dResult);

            return dResult;
        }

        public TripHeader GetTripHeaderById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT * from GFI_GPS_TripHeader (nolock)
                                    WHERE iID = ?
                                    order by iID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return GetTrip(dt.Rows[0], sConnStr);
        }
        TripHeader GetTrip(DataRow dr, String sConnStr)
        {
            TripHeader retTripHeader = new TripHeader();
            retTripHeader.iID = Convert.ToInt32(dr["iID"]);
            retTripHeader.ExceptionFlag = Convert.ToInt32(dr["ExceptionFlag"]);

            retTripHeader.MaxSpeed = Convert.ToInt32(dr["MaxSpeed"]);
            retTripHeader.IdlingTime = String.IsNullOrEmpty(dr["IdlingTime"].ToString()) ? TimeSpan.FromSeconds(0.0) : TimeSpan.FromSeconds(Convert.ToDouble(dr["IdlingTime"]));
            retTripHeader.StopTime = TimeSpan.FromSeconds(Convert.ToDouble(dr["StopTime"]));
            retTripHeader.TripTime = TimeSpan.FromSeconds(Convert.ToDouble(dr["TripTime"]));
            retTripHeader.TripDistance = (float)Convert.ToDouble(dr["TripDistance"]);
            retTripHeader.CalculatedOdometer = 0;
            if (dr.Table.Columns.Contains("CalculatedOdomter"))
                retTripHeader.CalculatedOdometer = Convert.ToInt32(dr["CalculatedOdomter"]);

            int i = Convert.ToInt32(dr["GPSDataEndUID"]);
            GPSData g = new GPSData();
            GPSDataService gpsDataService = new GPSDataService();

            g = gpsDataService.GetGPSDataById(i.ToString(), sConnStr);
            retTripHeader.GPSDataEnd = g;

            i = Convert.ToInt32(dr["GPSDataStartUID"]);
            g = new GPSData();
            g = gpsDataService.GetGPSDataById(i.ToString(), sConnStr);
            retTripHeader.GPSDataStart = g;

            retTripHeader.AssetID = Convert.ToInt32(dr["AssetID"]);
            retTripHeader.myAsset = new AssetService().GetAssetById(retTripHeader.AssetID, 0, false, sConnStr); ;

            retTripHeader.DriverID = Convert.ToInt32(dr["DriverID"]);
            Driver d = new Driver();
            d = new DriverService().GetDriverById(retTripHeader.DriverID.ToString(), sConnStr);
            retTripHeader.myDriver = d;

            retTripHeader.TDetails = new TripDetailService().GetTripDetailByHeader(retTripHeader.iID.ToString(), sConnStr);
            return retTripHeader;
        }
        Boolean isDuplicate(TripHeader uTripHeader, DbTransaction tTrans, out int UID, out String strLogReason, String sConnStr)
        {
            UID = -1;
            strLogReason = String.Empty;
            Boolean bResult = false;
            String sql = "select * from GFI_GPS_GPSData (nolock) where AssetID = ? and ? between TripStart and TripEnd";
            String strParams = uTripHeader.AssetID.ToString() + "¬" + uTripHeader.GPSDataStart.DateTimeGPS_UTC.ToString();
            DataTable dt = new Connection(sConnStr).GetDataDT(sql, strParams, tTrans, sConnStr);
            if (dt.Rows.Count > 0)
            {
                bResult = true;
            }
            return bResult;
        }
        public DataTable GetTripHeader(String sConnStr)
        {
            String sqlString = @"SELECT * from GFI_GPS_TripHeader order by iID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public List<TripHeader> GetTrips(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            List<TripHeader> l = new List<TripHeader>();

            DataTable dt = GetTripsDT(lAssetID, dtFrom, dtTo, sConnStr);
            if (dt == null)
                return new List<TripHeader>();
            else
            {
                foreach (DataRow dr in dt.Rows)
                    l.Add(GetTrip(dr, sConnStr));
            }

            return l;
        }
        public DataTable GetTripsDT(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            String strAssetID = String.Empty;
            foreach (int i in lAssetID)
                strAssetID += i.ToString() + ",";
            strAssetID = strAssetID.Substring(0, strAssetID.Length - 1);

            String sql = @"select *
                             from GFI_GPS_TripHeader (nolock) h
                            where h.AssetID in (" + strAssetID + @") 
	                            and (select DateTimeGPS_UTC from GFI_GPS_GPSData (nolock) where h.GPSDataStartUID = UID ) >= ?
	                            and (select DateTimeGPS_UTC from GFI_GPS_GPSData (nolock) where h.GPSDataEndUID = UID ) <= ?";
            DataTable dt = new Connection(sConnStr).GetDataDT(sql, dtFrom.ToString() + "¬" + dtTo.ToString(), null, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return dt;
        }

        public DataTable GetScoreCards(List<int> lIDs, String sType, DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            String sql = @"
--GetDriverScoreCard

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

CREATE TABLE #Drivers
(
	[RowNumber] [int] IDENTITY(1,1) NOT NULL,
	[iDriverID] [int] PRIMARY KEY CLUSTERED
)

";
            foreach (int i in lIDs)
                sql += "insert #Drivers (iDriverID) values (" + i.ToString() + ");\r\n";

            switch (sType)
            {
                case "Drivers":
                    sql += @"
delete from #Drivers
   where iDriverID in (select DriverID from GFI_FLT_Driver where sDriverName = 'Default Driver'); 

select h.DriverID
    , a.AssetNumber
    , (select count(h.OverSpeed1Time)) OverSpeed1Duration
	, (select count(h.OverSpeed2Time)) OverSpeed2Duration
	, (Select sDriverName from GFI_FLT_Driver where DriverID = h.DriverID) DriverName
	, ROUND(sum(h.TripDistance), 2) TripDistance, CAST(DATEADD(second,sum(h.TripTime) , '1900-01-01') AS TIME) TripTime
    , CAST(DATEADD(second,sum(h.idlingTime) , '1900-01-01') AS TIME) Idlingtime
	--, CAST(DATEADD(second,SUM(h.OverSpeed1Time) , '1900-01-01') AS TIME) OverSpeed1Duration
	--, CAST(DATEADD(second,SUM(h.OverSpeed2Time) , '1900-01-01') AS TIME) OverSpeed2Duration
	, SUM(h.Accel) Accel, SUM(h.Brake) Brake, SUM(h.Corner) Corner
	, (select count(1) from gfi_sys_accidentmanagement where DriverID = h.DriverID and acc_accidentdate>='" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "'  and acc_accidentdate <= '" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff") + @"' ) Accidents
from GFI_GPS_TripHeader h
    inner join #Drivers d on d.iDriverID = h.DriverID
    inner join GFI_FLT_ASSET a on a.AssetID = h.AssetID
where h.GPSDataStartUID <= h.GPSDataEndUID
	and h.dtStart >= '" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "'  and h.dtStart <= '" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "'  and h.OverSpeed1Time > 0  and h.OverSpeed2Time > 0 group by h.DriverID,a.AssetID,a.AssetNumber";
                    break;

                case ("Assets"):
                    sql += @"
select h.AssetID
	, sum(h.TripDistance) TripDistance, SUM(h.TripTime) TripTime
	, SUM(h.OverSpeed1Time) OverSpeed1Duration
	, SUM(h.OverSpeed2Time) OverSpeed2Duration
	, SUM(h.Accel) Accel, SUM(h.Brake) Brake, SUM(h.Corner) Corner
from GFI_GPS_TripHeader h
    inner join #Drivers d on d.iDriverID = h.AssetID
where h.GPSDataStartUID <= h.GPSDataEndUID
	and h.dtStart >= '" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "'  and h.dtStart <= '" + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "' group by h.AssetID";
                    break;
            }

            Connection c = new Connection(sConnStr);
            //DataTable dtSql = c.dtSql();
            //DataRow drSql = dtSql.NewRow();

            //drSql["sSQL"] = sql;
            //drSql["sParam"] = dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬"
            //                + dtFrom.ToString("dd-MMM-yyyy HH:mm:ss.fff") + "¬" + dtTo.ToString("dd-MMM-yyyy HH:mm:ss.fff");
            //drSql["sTableName"] = "dtList";
            //dtSql.Rows.Add(drSql);

            //DataSet ds = c.GetDataDS(dtSql, sConnStr);
            DataTable ds = c.GetDataDT(sql, sConnStr);
            return ds;
        }


        public List<TripHeader> GetCompleteTrips(List<int> lAssetID, DateTime dtFrom, DateTime dtTo,
                                        int iMinTripDistance, String sWorkHrs, Boolean bhasDetails,
                                        Boolean bByDriver, Boolean bGetZones, List<Matrix> lMatrix,
                                        Boolean bInclCalcOdometer, Boolean bReqestAddress, String sConnStr)
        {
            List<TripHeader> l = new List<TripHeader>();
            Boolean bhasExceptions = false;
            DataSet ds = GetTripsDS(lAssetID, dtFrom, dtTo, iMinTripDistance, sWorkHrs, bByDriver, bInclCalcOdometer, bhasDetails, bGetZones, lMatrix, sConnStr, null, null, null, bhasExceptions, null, true);
            if (ds == null)
                return l;

            DataView dv = ds.Tables["GPSData"].DefaultView;
            dv.Sort = "UID";
            DataTable dtGPSData = dv.ToTable();

            DataTable dtGPSDataDetail = new DataTable();
            DataTable dtTripDetail = new DataTable();
            if (bhasDetails)
            {
                dv = ds.Tables["GPSDataDetail"].DefaultView;
                dv.Sort = "UID";
                dtGPSDataDetail = dv.ToTable();

                dv = ds.Tables["TripDetail"].DefaultView;
                dv.Sort = "HeaderiID";
                dtTripDetail = dv.ToTable();
            }

            l = new myConverter().ConvertToList<TripHeader>(ds.Tables["TripHeader"]);
            DataRow[] drChk;
            List<TripHeader> lTHWithErrors = new List<TripHeader>();
            foreach (TripHeader th in l)
            {
                try
                {
                    List<GPSData> lGPSData = new List<GPSData>();
                    if (!bhasExceptions)//using bhasExceptions to determine that only details is needed
                    {
                        lGPSData = new myConverter().ConvertToList<GPSData>(dtGPSData.Select("UID = " + th.GPSDataEndUID.ToString()).CopyToDataTable());
                        th.GPSDataEnd = lGPSData.Find(o => o.UID == th.GPSDataEndUID);
                        lGPSData = new myConverter().ConvertToList<GPSData>(dtGPSData.Select("UID = " + th.GPSDataStartUID.ToString()).CopyToDataTable());
                        th.GPSDataStart = lGPSData.Find(o => o.UID == th.GPSDataStartUID);
                    }

                    Driver d = new Driver();
                    d.DriverID = Convert.ToInt32(ds.Tables["TripHeader"].Select("iID = " + th.iID.ToString())[0]["DriverID"]);
                    d.sDriverName = ds.Tables["TripHeader"].Select("iID = " + th.iID.ToString())[0]["Driver"].ToString();
                    th.myDriver = d;
                    Asset a = new Asset();
                    a.AssetID = Convert.ToInt32(ds.Tables["TripHeader"].Select("iID = " + th.iID.ToString())[0]["AssetID"]);
                    a.AssetName = ds.Tables["TripHeader"].Select("iID = " + th.iID.ToString())[0]["Asset"].ToString();
                    th.myAsset = a;

                    List<GPSDataDetail> lGPSDataDetail = new List<GPSDataDetail>();
                    if (bhasDetails)
                    {
                        if (th.GPSDataEnd != null)
                        {
                            drChk = dtGPSDataDetail.Select("UID = " + th.GPSDataEndUID.ToString());
                            if (drChk.Length > 0)
                            {
                                lGPSDataDetail = new myConverter().ConvertToList<GPSDataDetail>(drChk.CopyToDataTable());
                                th.GPSDataEnd.GPSDataDetails = lGPSDataDetail;
                            }
                        }
                        if (th.GPSDataStart != null)
                        {
                            drChk = dtGPSDataDetail.Select("UID = " + th.GPSDataStartUID.ToString());
                            if (drChk.Length > 0)
                            {
                                lGPSDataDetail = new myConverter().ConvertToList<GPSDataDetail>(drChk.CopyToDataTable());
                                th.GPSDataStart.GPSDataDetails = lGPSDataDetail;
                            }
                        }

                        List<TripDetail> lDetail = new myConverter().ConvertToList<TripDetail>(dtTripDetail.Select("HeaderiID = " + th.iID.ToString(), "RowNum ASC").CopyToDataTable());
                        th.TDetails = lDetail;
                        foreach (TripDetail td in th.TDetails)
                        {
                            drChk = dtGPSData.Select("UID = " + td.GPSDataUID.ToString());
                            if (drChk.Length > 0)
                            {
                                lGPSData = new myConverter().ConvertToList<GPSData>(drChk.CopyToDataTable());
                                td.GpsData = lGPSData.Find(o => o.UID == td.GPSDataUID);

                                if (bhasExceptions)
                                {
                                    DataRow dr = ds.Tables["dtExceptions"].Select("GPSDataID = '" + td.GpsData.UID.ToString() + "'").FirstOrDefault();
                                    if (dr != null)
                                        td.GpsData.Availability = dr["RuleName"].ToString();
                                }
                            }

                            drChk = dtGPSDataDetail.Select("UID = " + td.GPSDataUID.ToString());
                            if (drChk.Length > 0)
                            {
                                lGPSDataDetail = new myConverter().ConvertToList<GPSDataDetail>(drChk.CopyToDataTable());
                                td.GpsData.GPSDataDetails = lGPSDataDetail;

                                if (bhasExceptions)
                                {
                                    DataRow dr = ds.Tables["dtExceptions"].Select("GPSDataID = '" + td.GpsData.UID.ToString() + "'").FirstOrDefault();
                                    if (dr != null)
                                        td.GpsData.Availability = dr["RuleName"].ToString();
                                }
                            }
                        }
                    }
                }
                catch
                {
                    lTHWithErrors.Add(th);
                }
            }

            foreach (TripHeader the in lTHWithErrors)
                l.Remove(the);

            return l;
        }

    }
}