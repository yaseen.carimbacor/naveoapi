

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models.Trips;

namespace NaveoOneLib.Services.Trips
{
    public class ExtTripInfoService
    {

        Errors er = new Errors();
        public DataTable GetExtTripInfo(String sConnStr)
        {
            String sqlString = @"SELECT IID, 
ExtTripID, 
Field1Name, 
Field1Value, 
Field2Name, 
Field2Value, 
Field3Name, 
Field3Value, 
Field4Name, 
Field4Value, 
Field5Name, 
Field5Value, 
Field6Name, 
Field6Value, 
CreatedBy, 
CreatedDate, 
UpdatedBy, 
UpdatedDate, 
AssetName from GFI_FLT_ExtTripInfo order by TingPow";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public ExtTripInfo GetExtTripInfoById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT IID, 
ExtTripID, 
Field1Name, 
Field1Value, 
Field2Name, 
Field2Value, 
Field3Name, 
Field3Value, 
CreatedBy, 
CreatedDate, 
UpdatedBy, 
UpdatedDate, 
AssetName from GFI_FLT_ExtTripInfo
WHERE TingPow = ?
order by TingPow";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
            {
                ExtTripInfo retExtTripInfo = new ExtTripInfo();
                retExtTripInfo.IID = Convert.ToInt32(dt.Rows[0]["IID"]);
                retExtTripInfo.ExtTripID = dt.Rows[0]["ExtTripID"].ToString();
                retExtTripInfo.Field1Name = Convert.ToInt32(dt.Rows[0]["Field1Name"].ToString());
                retExtTripInfo.Field1Value = dt.Rows[0]["Field1Value"].ToString();
                retExtTripInfo.Field2Name = Convert.ToInt32(dt.Rows[0]["Field2Name"].ToString());
                retExtTripInfo.Field2Value = dt.Rows[0]["Field2Value"].ToString();
                retExtTripInfo.Field3Name = Convert.ToInt32(dt.Rows[0]["Field3Name"].ToString());
                retExtTripInfo.Field3Value = dt.Rows[0]["Field3Value"].ToString();
                retExtTripInfo.CreatedBy = dt.Rows[0]["CreatedBy"].ToString();
                retExtTripInfo.CreatedDate = (DateTime)dt.Rows[0]["CreatedDate"];
                retExtTripInfo.UpdatedBy = dt.Rows[0]["UpdatedBy"].ToString();
                retExtTripInfo.UpdatedDate = (DateTime)dt.Rows[0]["UpdatedDate"];
                retExtTripInfo.AssetName = dt.Rows[0]["AssetName"].ToString();
                return retExtTripInfo;
            }
        }

        public DataTable GetExtTripInfoByAssetList(List<int> lAssetID, DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            string strAssetID = string.Empty;
            foreach (int i in lAssetID)
                strAssetID += i.ToString() + ",";
            strAssetID = strAssetID.Substring(0, strAssetID.Length - 1);

            #region OldQuery
            String sqlString = @"with cte as
            (
             select ExtTripID, min(DateTimeGPS_UTC) StartTime, max(DateTimeGPS_UTC) EndTime,                         
             convert(varchar(5),DateDiff(s, min(DateTimeGPS_UTC), max(DateTimeGPS_UTC))/3600)+'h:'+
             convert(varchar(5),DateDiff(s, min(DateTimeGPS_UTC),max(DateTimeGPS_UTC))%3600/60)+'m:'+
             convert(varchar(5),(DateDiff(s,  min(DateTimeGPS_UTC), max(DateTimeGPS_UTC))%60))+'s:' as  TimeDiff
             from GFI_FLT_ExtTripInfoDetail 
             where DateTimeGPS_UTC >= ? and DateTimeGPS_UTC <= ?
             group by ExtTripID 
            )
            select                
			a.AssetName,
            h.ExtTripID, 
            Field1Name as attribute1, 
            Field1Value as attribute1_Value, 
            Field2Name as attribute2, 
            Field2Value as attribute2_Value, 
            Field3Name as attribute3, 
            Field3Value as attribute3_Value, 
            Field4Name as attribute4, 
            Field4Value as attribute4_Value, 
            Field5Name as attribute5, 
            Field5Value as attribute5_Value, 
            Field6Name as attribute6, 
            Field6Value as attribute6_Value,
            AdditionalValue1,             
            AdditionalField1 as 'label1',           
            AdditionalValue2,
            AdditionalField2,                                  
            d.StartTime,d.EndTime,d.TimeDiff,
            '' as TotalDistance           
            from GFI_FLT_ExtTripInfo h
            left outer join GFI_FLT_Asset a on h.AssetName = a.AssetID 
            left outer join cte d on h.IID = d.ExtTripID
            WHERE h.AssetName IN (" + strAssetID + @")      
            order by IID";
            #endregion

            sqlString = @" select      
            h.IID, 
			a.AssetName,
            h.ExtTripID,             
            h.Field1Name, 
            t1.Field1Label as attribute1,          
            h.Field1Value as attribute1_Value,              
            t1.Field1Type as Field1Type,   
            t1.sDataSource as sDataSource1,                     
            h.Field2Name,  
            t2.Field1Label as attribute2,                   
            h.Field2Value as attribute2_Value,             
            t2.Field1Type as Field2Type,            
            t2.sDataSource as sDataSource2,         
            h.Field3Name ,
            t3.Field1Label as attribute3,                   
            h.Field3Value as attribute3_Value,             
            t3.Field1Type as Field3Type,
            t3.sDataSource as sDataSource3, 
            h.Field4Name , 
            t4.Field1Label as attribute4,                   
            h.Field4Value as attribute4_Value,             
            t4.Field1Type as Field4Type,            
            t4.sDataSource as sDataSource4, 
            h.Field5Name , 
            t5.Field1Label as attribute5,                   
            h.Field5Value as attribute5_Value,             
            t5.Field1Type as Field5Type,            
            t5.sDataSource as sDataSource5, 
            h.Field6Name , 
            t6.Field1Label as attribute6,                   
            h.Field6Value as attribute6_Value,            
            t6.Field1Type as Field6Type,            
            t6.sDataSource as sDataSource6, 
            AdditionalValue1,             
            AdditionalField1 ,      
            AdditionalValue2,
            AdditionalField2,                                             
            
            DateTimeGPSStart_UTC as StartDateTime,
            DateTimeGPSStop_UTC as StopDateTime,
            convert(varchar(10),convert(int,(Timediff))/3600) +'h:'+ 
            convert(varchar(50),convert(int,(Timediff))%3600/60)+'m:'+
            convert(varchar(50),convert(int,(Timediff))%60)+'s' as TimeDiff,  
            Consumption as Consumption,
            '' as TotalDistance           
            from GFI_FLT_ExtTripInfo h
            
            left outer join GFI_FLT_Asset a on h.AssetName = a.AssetID 
            left outer join GFI_FLT_ExtTripInfoType t1 on h.Field1Name = t1.IID 
            left outer join GFI_FLT_ExtTripInfoType t2 on h.Field2Name = t2.IID 
            left outer join GFI_FLT_ExtTripInfoType t3 on h.Field3Name = t3.IID
            left outer join GFI_FLT_ExtTripInfoType t4 on h.Field4Name = t4.IID  
            left outer join GFI_FLT_ExtTripInfoType t5 on h.Field5Name = t5.IID  
            left outer join GFI_FLT_ExtTripInfoType t6 on h.Field6Name = t6.IID 
            WHERE h.AssetName IN (" + strAssetID + @")  
            AND DateTimeGPSStart_UTC >= ? and DateTimeGPSStop_UTC <= ?    
            order by IID";          

            DataTable dt = new Connection(sConnStr).GetDataDT(sqlString, dtFrom.ToString() + "�" + dtTo.ToString(), null, sConnStr);
            if (dt.Rows.Count == 0)
                return dt;
            else
                return dt;
        }

        public DataTable GetExtTripInfoByAssetID(String iID, String sConnStr)
        {
            String sqlString = @"with cte as
            (
             select ExtTripID, min(DateTimeGPS_UTC) StartTime, max(DateTimeGPS_UTC) EndTime,            
             
             convert(varchar(5),DateDiff(s, min(DateTimeGPS_UTC), max(DateTimeGPS_UTC))/3600)+' h:'+
             convert(varchar(5),DateDiff(s, min(DateTimeGPS_UTC),max(DateTimeGPS_UTC))%3600/60)+' m:'+
             convert(varchar(5),(DateDiff(s,  min(DateTimeGPS_UTC), max(DateTimeGPS_UTC))%60))+' s:' as  TimeDiff

             from GFI_FLT_ExtTripInfoDetail 
             group by ExtTripID
            )
            select                
			a.AssetName,
            h.ExtTripID, 
            Field1Name as attribute1, 
            Field1Value as attribute1_Value, 
            Field2Name as attribute2, 
            Field2Value as attribute2_Value, 
            Field3Name as attribute3, 
            Field3Value as attribute3_Value, 
            Field4Name as attribute4, 
            Field4Value as attribute4_Value, 
            Field5Name as attribute5, 
            Field5Value as attribute5_Value, 
            Field6Name as attribute6, 
            Field6Value as attribute6_Value,
            AdditionalValue1,             
            AdditionalField1 as 'label1',           
AdditionalValue2,
AdditionalField2,             
            
           
                     
            d.StartTime,d.EndTime,d.TimeDiff,
            '' as TotalDistance           
            from GFI_FLT_ExtTripInfo h
            left outer join GFI_FLT_Asset a on h.AssetName = a.AssetID 
            left outer join cte d on h.IID = d.ExtTripID
            WHERE h.AssetName = ?            
            order by IID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return dt;
        }


        public DataTable GetMenu(String sConnStr)
        {
            String sqlString = @"SELECT 
	                           UID
                              ,ModuleName
                              ,Description
                              ,Command     
                              ,MenuHeader      
                              ,MenuName
                              ,OrderIndex
                              ,Title
                              ,Summary
                              ,GFI_SYS_Modules.MenuHeaderId      
                              ,GFI_SYS_Modules.MenuGroupId
                              ,GFI_SYS_Modules.OrderIndex      
                              FROM GFI_SYS_Modules, GFI_SYS_MenuHeader,GFI_SYS_MenuGroup    
                              where GFI_SYS_Modules.MenuHeaderId = GFI_SYS_MenuHeader.MenuHeaderId  
                              and GFI_SYS_Modules.MenuGroupId = GFI_SYS_MenuGroup.MenuId   
                              order by GFI_SYS_Modules.MenuHeaderId,GFI_SYS_Modules.MenuGroupId,GFI_SYS_Modules.OrderIndex";

            DataTable dt = new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
            return dt;
        }

        public DataTable GetTableLatestID(string sTable, String sConnStr)
        {
            String sqlString = @"SELECT COUNT(*)                                
                                from  " + sTable;
            //Reza
            sqlString += " where CreatedDate = CAST(GETDATE() AS DATE)";

            DataTable dt = new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
            return dt;
        }
        public bool SaveExtTripInfo(ExtTripInfo uExtTripInfo, Boolean bInsert, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_ExtTripInfo";
            dt.Columns.Add("IID", uExtTripInfo.IID.GetType());
            dt.Columns.Add("ExtTripID", uExtTripInfo.ExtTripID.GetType());
            dt.Columns.Add("Field1Name");
            dt.Columns.Add("Field1Value", uExtTripInfo.Field1Value.GetType());
            dt.Columns.Add("Field2Name");
            dt.Columns.Add("Field2Value", uExtTripInfo.Field2Value.GetType());
            dt.Columns.Add("Field3Name");
            dt.Columns.Add("Field3Value", uExtTripInfo.Field3Value.GetType());
            dt.Columns.Add("Field4Name");
            dt.Columns.Add("Field4Value", uExtTripInfo.Field4Value.GetType());
            dt.Columns.Add("Field5Name");
            dt.Columns.Add("Field5Value", uExtTripInfo.Field5Value.GetType());
            dt.Columns.Add("Field6Name");
            dt.Columns.Add("Field6Value", uExtTripInfo.Field6Value.GetType());
            dt.Columns.Add("AdditionalField1", uExtTripInfo.AdditionalField1Value.GetType());
            dt.Columns.Add("AdditionalField2", uExtTripInfo.AdditionalField2Value.GetType());
            dt.Columns.Add("AdditionalValue1", uExtTripInfo.AdditionalField1Label.GetType());
            dt.Columns.Add("AdditionalValue2", uExtTripInfo.AdditionalField2Label.GetType());
            dt.Columns.Add("Consumption", uExtTripInfo.Consumption.GetType());
            dt.Columns.Add("Timediff", uExtTripInfo.Timediff.GetType());
            dt.Columns.Add("DateTimeGPSStart_UTC", uExtTripInfo.DateTimeGPSStart_UTC.GetType());
            dt.Columns.Add("DateTimeGPSStop_UTC", uExtTripInfo.DateTimeGPSStop_UTC.GetType());
            dt.Columns.Add("CreatedBy", uExtTripInfo.CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", uExtTripInfo.CreatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uExtTripInfo.UpdatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uExtTripInfo.UpdatedDate.GetType());
            dt.Columns.Add("AssetName", uExtTripInfo.AssetName.GetType());
            dt.Columns.Add("TripDistance", uExtTripInfo.TripDistance.GetType());

            DataRow dr = dt.NewRow();
            dr["IID"] = uExtTripInfo.IID;
            dr["ExtTripID"] = uExtTripInfo.ExtTripID;
            dr["Field1Name"] = uExtTripInfo.Field1Name;
            dr["Field1Value"] = uExtTripInfo.Field1Value;
            dr["Field2Name"] = uExtTripInfo.Field2Name;
            dr["Field2Value"] = uExtTripInfo.Field2Value;
            dr["Field3Name"] = uExtTripInfo.Field3Name; ;
            dr["Field3Value"] = uExtTripInfo.Field3Value;
            dr["Field4Name"] = uExtTripInfo.Field4Name;
            dr["Field4Value"] = uExtTripInfo.Field4Value;
            dr["Field5Name"] = uExtTripInfo.Field5Name;
            dr["Field5Value"] = uExtTripInfo.Field5Value;
            dr["Field6Name"] = uExtTripInfo.Field6Name;
            dr["Field6Value"] = uExtTripInfo.Field6Value;

            dr["AdditionalField1"] = uExtTripInfo.AdditionalField1Value;
            dr["AdditionalField2"] = uExtTripInfo.AdditionalField2Value;
            dr["AdditionalValue1"] = uExtTripInfo.AdditionalField1Label;
            dr["AdditionalValue2"] = uExtTripInfo.AdditionalField2Label;

            dr["Consumption"] = uExtTripInfo.Consumption;
            dr["Timediff"] = uExtTripInfo.Timediff;
            dr["DateTimeGPSStart_UTC"] = uExtTripInfo.DateTimeGPSStart_UTC;
            dr["DateTimeGPSStop_UTC"] = uExtTripInfo.DateTimeGPSStop_UTC;

            dr["CreatedBy"] = uExtTripInfo.CreatedBy;
            dr["CreatedDate"] = uExtTripInfo.CreatedDate;
            dr["UpdatedBy"] = uExtTripInfo.UpdatedBy;
            dr["UpdatedDate"] = uExtTripInfo.UpdatedDate;
            dr["AssetName"] = uExtTripInfo.AssetName;
            dr["TripDistance"] = uExtTripInfo.TripDistance;
            dt.Rows.Add(dr);


            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "IID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            if (bInsert)
                drCtrl["NextIdAction"] = "GET";
            else
                drCtrl["NextIdValue"] = uExtTripInfo.IID;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();
            dsProcess.Merge(dt);

            //ExtDetail
            DataTable dtExtDetail = new myConverter().ListToDataTable<ExtTripInfoDetail>(uExtTripInfo.ExtDetail);
            dtExtDetail.TableName = "GFI_FLT_ExtTripInfoDetail";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 2;
            drCtrl["TblName"] = dtExtDetail.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "IID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "ExtTripId";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtExtDetail);

            //EmpResources
            DataTable dtEmpBase = new myConverter().ListToDataTable<ExtTripInfoResource>(uExtTripInfo.lEmpBase);
            dtEmpBase.TableName = "GFI_FLT_ExtTripInfoResources";
            drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 3;
            drCtrl["TblName"] = dtEmpBase.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "IID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            drCtrl["NextIdFieldName"] = "ExtTripID";
            drCtrl["ParentTblName"] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtEmpBase);


            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;

        }

        public DataTable GetExtTripInfoByPeriod(DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            String sqlString = @"SELECT IID, 
            ExtTripID, 
            Field1Name, 
            Field1Value, 
            Field2Name, 
            Field2Value, 
            Field3Name, 
            Field3Value, 
            Field4Name, 
            Field4Value, 
            Field5Name, 
            Field5Value, 
            Field6Name, 
            Field6Value, 
            CreatedBy, 
            CreatedDate, 
            UpdatedBy, 
            UpdatedDate, 
            AssetName from GFI_FLT_ExtTripInfo where CreatedDate >= '" + dtFrom + "' and DateTimeGPSStop_UTC <= '" + dtTo + "' order by CreatedDate";

            sqlString = @"
SELECT IID, 
        e.ExtTripID, 
        e.Field1Name, 
        e.Field1Value, 
        e.Field2Name, 
        e.Field2Value, 
        e.Field3Name, 
        e.Field3Value, 
        e.Field4Name, 
        e.Field4Value, 
        e.Field5Name, 
        e.Field5Value, 
        e.Field6Name, 
        e.Field6Value, 
        e.CreatedBy, 
        e.CreatedDate, 
        e.UpdatedBy, 
        e.UpdatedDate, 
        e.AssetName, e.DateTimeGPSStart_UTC + t.UTCOffSet DateTimeGPSStart
from GFI_FLT_ExtTripInfo e
	inner join GFI_FLT_Asset a on a.AssetID = e.AssetName
	left outer join GFI_SYS_TimeZones t on t.StandardName = a.TimeZoneID 
where e.CreatedDate >= '" + dtFrom + "' and e.CreatedDate <= '" + dtTo + "' order by e.CreatedDate";

            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }
        public DataTable GetExtTripInfoByTripPeriod(DateTime dtFrom, DateTime dtTo, String sConnStr)
        {
            String sqlString = @"
SELECT IID, 
        e.ExtTripID, 
        e.Field1Name, 
        e.Field1Value, 
        e.Field2Name, 
        e.Field2Value, 
        e.Field3Name, 
        e.Field3Value, 
        e.Field4Name, 
        e.Field4Value, 
        e.Field5Name, 
        e.Field5Value, 
        e.Field6Name, 
        e.Field6Value, 
        e.CreatedBy, 
        e.CreatedDate, 
        e.UpdatedBy, 
        e.UpdatedDate, 
        e.AssetName, e.DateTimeGPSStart_UTC + t.UTCOffSet DateTimeGPSStart
from GFI_FLT_ExtTripInfo e
	inner join GFI_FLT_Asset a on a.AssetID = e.AssetName
	left outer join GFI_SYS_TimeZones t on t.StandardName = a.TimeZoneID 
where (e.DateTimeGPSStart_UTC - t.UTCOffSet) >= '" + dtFrom + "' and (e.DateTimeGPSStart_UTC + t.UTCOffSet) <= '" + dtTo + "' order by e.DateTimeGPSStart_UTC";

            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        public bool UpdateExtTripInfo(ExtTripInfo uExtTripInfo, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_ExtTripInfo";
            dt.Columns.Add("IID", uExtTripInfo.IID.GetType());
            dt.Columns.Add("ExtTripID", uExtTripInfo.ExtTripID.GetType());
            dt.Columns.Add("Field1Name", uExtTripInfo.Field1Name.GetType());
            dt.Columns.Add("Field1Value", uExtTripInfo.Field1Value.GetType());
            dt.Columns.Add("Field2Name", uExtTripInfo.Field2Name.GetType());
            dt.Columns.Add("Field2Value", uExtTripInfo.Field2Value.GetType());
            dt.Columns.Add("Field3Name", uExtTripInfo.Field3Name.GetType());
            dt.Columns.Add("Field3Value", uExtTripInfo.Field3Value.GetType());
            dt.Columns.Add("CreatedBy", uExtTripInfo.CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", uExtTripInfo.CreatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uExtTripInfo.UpdatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uExtTripInfo.UpdatedDate.GetType());
            dt.Columns.Add("AssetName", uExtTripInfo.AssetName.GetType());
            DataRow dr = dt.NewRow();
            dr["IID"] = uExtTripInfo.IID;
            dr["ExtTripID"] = uExtTripInfo.ExtTripID;
            dr["Field1Name"] = uExtTripInfo.Field1Name;
            dr["Field1Value"] = uExtTripInfo.Field1Value;
            dr["Field2Name"] = uExtTripInfo.Field2Name;
            dr["Field2Value"] = uExtTripInfo.Field2Value;
            dr["Field3Name"] = uExtTripInfo.Field3Name;
            dr["Field3Value"] = uExtTripInfo.Field3Value;
            dr["CreatedBy"] = uExtTripInfo.CreatedBy;
            dr["CreatedDate"] = uExtTripInfo.CreatedDate;
            dr["UpdatedBy"] = uExtTripInfo.UpdatedBy;
            dr["UpdatedDate"] = uExtTripInfo.UpdatedDate;
            dr["AssetName"] = uExtTripInfo.AssetName;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "IID = '" + uExtTripInfo.IID + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool DeleteExtTripInfo(ExtTripInfo uExtTripInfo, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_FLT_ExtTripInfo", "IID = '" + uExtTripInfo.IID + "'", sConnStr))
                return true;
            else
                return false;
        }

    }
}



