
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
//using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;
using NaveoOneLib.Models;
using NaveoOneLib.Models.Trips;

namespace NaveoOneLib.Services.Trips
{
    public class ExtTripInfoTypeService
    {

        Errors er = new Errors();
        public DataTable GetExtTripInfoType(String sConnStr)
        {
            String sqlString = @"SELECT IID, 
                                    TypeName, 
                                    Field1Name, 
                                    Field1Label, 
                                    Field1Type, 
                                    sDataSource,                                 
                                    CreatedBy, 
                                    CreatedDate, 
                                    UpdatedBy, 
                                    UpdatedDate from GFI_FLT_ExtTripInfoType order by IID";
            return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);
        }

        public ExtTripInfoType GetExtTripInfoTypeById(String iID, String sConnStr)
        {
            String sqlString = @"SELECT IID, 
                                TypeName, 
                                Field1Name, 
                                Field1Label, 
                                Field1Type, 
                                sDataSource, 
                                CreatedBy, 
                                CreatedDate, 
                                UpdatedBy, 
                                UpdatedDate from GFI_FLT_ExtTripInfoType
                                WHERE IID = ?
                                order by IID";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return getExtTripInfoType(dt.Rows[0]);
        }
        ExtTripInfoType getExtTripInfoType(DataRow dr)
        {
            ExtTripInfoType retExtTripInfoType = new ExtTripInfoType();
            retExtTripInfoType.IID = Convert.ToInt32(dr["IID"]);
            retExtTripInfoType.TypeName = dr["TypeName"].ToString();
            retExtTripInfoType.Field1Name = dr["Field1Name"].ToString();
            retExtTripInfoType.Field1Label = dr["Field1Label"].ToString();
            retExtTripInfoType.Field1Type = dr["Field1Type"].ToString();
            retExtTripInfoType.sDataSource = dr["sDataSource"].ToString();

            retExtTripInfoType.CreatedBy = dr["CreatedBy"].ToString();
            retExtTripInfoType.CreatedDate = (DateTime)dr["CreatedDate"];
            retExtTripInfoType.UpdatedBy = dr["UpdatedBy"].ToString();
            retExtTripInfoType.UpdatedDate = (DateTime)dr["UpdatedDate"];
            return retExtTripInfoType;
        }
        public ExtTripInfoType GetExtTripInfoTypeByTypeName(String iID, String sConnStr)
        {
            String sqlString = @"SELECT IID, 
                                TypeName, 
                                Field1Name, 
                                Field1Label, 
                                Field1Type, 
                                sDataSource, 
                                CreatedBy, 
                                CreatedDate, 
                                UpdatedBy, 
                                UpdatedDate from GFI_FLT_ExtTripInfoType
                                WHERE TypeName = ?";
            DataTable dt = new Connection(sConnStr).GetDataTableBy(sqlString, iID, sConnStr);
            if (dt.Rows.Count == 0)
                return null;
            else
                return getExtTripInfoType(dt.Rows[0]);
        }

        public Boolean SaveExtTripInfoType(ExtTripInfoType uExtTripInfoType, Boolean bInsert, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_ExtTripInfoType";
            if (!dt.Columns.Contains("oprType"))
                dt.Columns.Add("oprType", typeof(DataRowState));
            dt.Columns.Add("IID", uExtTripInfoType.IID.GetType());
            dt.Columns.Add("TypeName", uExtTripInfoType.TypeName.GetType());
            dt.Columns.Add("Field1Name", uExtTripInfoType.Field1Name.GetType());
            dt.Columns.Add("Field1Label", uExtTripInfoType.Field1Label.GetType());
            dt.Columns.Add("Field1Type", uExtTripInfoType.Field1Type.GetType());
            dt.Columns.Add("sDataSource", uExtTripInfoType.sDataSource.GetType());
            dt.Columns.Add("CreatedBy", uExtTripInfoType.CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", uExtTripInfoType.CreatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uExtTripInfoType.UpdatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uExtTripInfoType.UpdatedDate.GetType());
            DataRow dr = dt.NewRow();
            dr["IID"] = uExtTripInfoType.IID;
            dr["TypeName"] = uExtTripInfoType.TypeName;
            dr["Field1Name"] = uExtTripInfoType.Field1Name;
            dr["Field1Label"] = uExtTripInfoType.Field1Label;
            dr["Field1Type"] = uExtTripInfoType.Field1Type;
            dr["sDataSource"] = uExtTripInfoType.sDataSource;
            dr["CreatedBy"] = uExtTripInfoType.CreatedBy;
            dr["CreatedDate"] = uExtTripInfoType.CreatedDate;
            dr["UpdatedBy"] = uExtTripInfoType.UpdatedBy;
            dr["UpdatedDate"] = uExtTripInfoType.UpdatedDate;
            dt.Rows.Add(dr);

            Connection c = new Connection(sConnStr);
            DataSet dsProcess = new DataSet();
            DataTable dtCtrl = c.CTRLTBL();
            dsProcess.Merge(dt);

            DataRow drCtrl = dtCtrl.NewRow();
            drCtrl["SeqNo"] = 1;
            drCtrl["TblName"] = dt.TableName;
            drCtrl["CmdType"] = "TABLE";
            drCtrl["KeyFieldName"] = "IID";
            drCtrl["KeyIsIdentity"] = "TRUE";
            drCtrl["NextIdAction"] = "SET";
            dtCtrl.Rows.Add(drCtrl);

            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains("oprType"))
                    dti.Columns.Add("oprType", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri["oprType"].ToString().Trim().Length == 0 || dri["oprType"].ToString() == "0")
                    {
                        if (bInsert)
                            dri["oprType"] = DataRowState.Added;
                        else
                            dri["oprType"] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            DataSet dsResults = c.ProcessData(dsProcess, sConnStr);
            dtCtrl = dsResults.Tables["CTRLTBL"];

            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select("UpdateStatus IS NULL or UpdateStatus <> 'TRUE'");

                if (dRow.Length > 0)
                    return false;
                else
                    return true;
            }
            else
                return false;
        }

        public bool UpdateExtTripInfoType(ExtTripInfoType uExtTripInfoType, DbTransaction transaction, String sConnStr)
        {
            DataTable dt = new DataTable();
            dt.TableName = "GFI_FLT_ExtTripInfoType";
            //dt.Columns.Add("IID" , uExtTripInfoType.IID.GetType());
            dt.Columns.Add("TypeName", uExtTripInfoType.TypeName.GetType());
            dt.Columns.Add("Field1Name", uExtTripInfoType.Field1Name.GetType());
            dt.Columns.Add("Field1Label", uExtTripInfoType.Field1Label.GetType());
            dt.Columns.Add("Field1Type", uExtTripInfoType.Field1Type.GetType());
            dt.Columns.Add("sDataSource", uExtTripInfoType.sDataSource.GetType());           
            dt.Columns.Add("CreatedBy", uExtTripInfoType.CreatedBy.GetType());
            dt.Columns.Add("CreatedDate", uExtTripInfoType.CreatedDate.GetType());
            dt.Columns.Add("UpdatedBy", uExtTripInfoType.UpdatedBy.GetType());
            dt.Columns.Add("UpdatedDate", uExtTripInfoType.UpdatedDate.GetType());
            DataRow dr = dt.NewRow();
            //dr["IID"] =  uExtTripInfoType.IID;
            dr["TypeName"] = uExtTripInfoType.TypeName;
            dr["Field1Name"] = uExtTripInfoType.Field1Name;
            dr["Field1Label"] = uExtTripInfoType.Field1Label;
            dr["Field1Type"] = uExtTripInfoType.Field1Type;
            dr["sDataSource"] = uExtTripInfoType.sDataSource;
           
            dr["CreatedBy"] = uExtTripInfoType.CreatedBy;
            dr["CreatedDate"] = uExtTripInfoType.CreatedDate;
            dr["UpdatedBy"] = uExtTripInfoType.UpdatedBy;
            dr["UpdatedDate"] = uExtTripInfoType.UpdatedDate;
            dt.Rows.Add(dr);

            Connection _connection = new Connection(sConnStr); ;
            if (_connection.GenUpdate(dt, "IID = '" + uExtTripInfoType.IID + "'", transaction, sConnStr))
                return true;
            else
                return false;
        }
        public bool DeleteExtTripInfoType(ExtTripInfoType uExtTripInfoType, String sConnStr)
        {
            Connection _connection = new Connection(sConnStr);
            if (_connection.GenDelete("GFI_FLT_ExtTripInfoType", "IID = '" + uExtTripInfoType.IID + "'", sConnStr))
                return true;
            else
                return false;
        }
    }
}

