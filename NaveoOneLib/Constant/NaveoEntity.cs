﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace NaveoOneLib.Constant
{
    public class NaveoEntity
    {
        public static string NaveoWebMapAddress = ConfigurationManager.AppSettings["NaveoWebMapAddress"].ToString();
        public static string GMSWebAddress = ConfigurationManager.AppSettings["GMSWebAddress"].ToString();
        public static string GMSAPIAddress = ConfigurationManager.AppSettings["GMSAPIAddress"].ToString();

        #region Category
        public const string USERS = "USERS";
        public const string ZONES = "ZONES";
        public const string LIVE = "LIVE";
        public const string TRIPS = "TRIPS";
        public const string PLANNING = "PLANNING";
        public const string TERRAPLANNING = "TerraPLANNING";
        public const string LOOKUPVALUES = "LOOKUPVALUES";
        public const string ZONES_UPDATE = "ZONES_UPDATE";
        public const string FixedAssetImage = "FixedAssetImage";
        public const string NoUploadedFile = "No file/image selected! Please upload a valid file or image";

        // Note: When a constant contains several words, 
        // it`s better to separate it by underscore for readability purposes
        public const string DAILY_SUMMARIZED_TRIPS = "DAILY_SUMMARIZED_TRIPS";
        public const string SUMMARIZED_TRIPS = "SUMMARIZED_TRIPS";
        public const string DAILY_TRIP_TIME = "DAILY_TRIP_TIME";
        public const string ASSET_SUMMARY_OUTSIDE_HO = "ASSET_SUMMARY_OUTSIDE_HO";
        public const string ASSET_LIST = "ASSET_LIST";
        public const string EXCEPTION_LIST = "EXCEPTION_LIST";
        public const string FUEL = "FUEL";
        public const string Zone_Visited_NotVisited = "Zone_Visited_NotVisited";
        public const string TripHistory_By_DataLog = "TripHistory_By_DataLog";
        public const string Temperature_Report = "Temperature_Report";
        public const string Customized_Temperature_Report = "Customized_Temperature_Report";
        public const string SensorData_Report = "SensorData_Report";
        public const string Planning_Report = "Planning_Report";
        public const string Schedule_Report = "Schedule_Report";
        public const string Fuel_Report = "Fuel_Report";
        public const string Terra_Planning_Report = "Terra_Planning_Report";
        public const string Customized_Trip_Report = "Customized_Trip_Report";
        public const string Auxiliary_Report = "AuxiliaryReport";
        public const string Amiran_Report = "AmiranReport";
        public const string MOLG_CustomizedReport = "MOLG_CustomizedReport";
        public const string Monitoring_Data = "MonitoringData";
        public const string Monitoring_Device = "MonitoringDevice";
        public const string AccidentMng = "AccidentManagement";
        public const string Shift = "Shift";
        public const string Roster = "Roster";
        public const string PlanningExceptions = "PlanningExceptions";
        public const string FuelExceptions = "FuelExceptions";
        public const string MaintenanceCostReport = "MaintenanceCostReport";
        public const int GenericBypass = -987;

        public const int FixedAssetGroup = 2147410000;

        public const String TerraStopConstantTeaBreak1 = "Tea Break1";
        public const String TerraStopConstantTeaBreak2 = "Tea Break2";
        public const String TerraStopConstantLunch = "Lunch";

        public const String MaintDB = "MaintDB";

        #endregion
    }
}