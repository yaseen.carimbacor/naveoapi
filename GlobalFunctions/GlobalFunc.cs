﻿using System;
using System.Collections.Generic;
using System.Text;
using NavLib.Globals;
using System.Data;
using NavLib.Data;
using NavLib.Processing;
using NaveoOneLib.DBCon;
using System.Configuration;

namespace GlobalFunctions
{
    public class GlobalFunc
    {
        public int RegisterDevice(String strUnitID)
        {
            int i = 0;
            NaveoSync.Service navService = new NaveoSync.Service();
            navService.Timeout = 120000; //2mins
            for (int iWS = 1; iWS <= 2; iWS++)
            {
                if (iWS == 1)
                    navService.Url = GlobalVariables.GetPrimarySyncService();
                else if (iWS == 2)
                    navService.Url = GlobalVariables.GetSecondarySyncService();

                i += navService.RegisterDevice(strUnitID);
            }

            return i;
        }
        public int DeRegisterDevice(String strUnitID)
        {
            int i = 0;
            NaveoSync.Service navService = new NaveoSync.Service();
            navService.Timeout = 120000; //2mins
            for (int iWS = 1; iWS <= 2; iWS++)
            {
                if (iWS == 1)
                    navService.Url = GlobalVariables.GetPrimarySyncService();
                else if (iWS == 2)
                    navService.Url = GlobalVariables.GetSecondarySyncService();

                i += navService.DeRegisterDevice(strUnitID);
            }

            return i;
        }

        public DataSet QryNaveoSvr(String strUnitID, int NoOfRec, Boolean bSummaryOnly, Boolean bWriteToFile, Boolean bAscOrder, Boolean bOldData)
        {
            DataSet dsResult = new DataSet();
            String strConn = "ConnStr";
            if (bOldData)
                strConn = "ConnStrTransferred";
            String sql = "select top " + NoOfRec + " *, cast(RawData as varchar(500))RData from RawData where unitID = '" + strUnitID + "' order by dtDatetime desc";
            sql = "select * from (" + sql + ")x order by dtDatetime asc";
            if (bAscOrder)
                sql = sql.Replace("desc)", "asc)");

            //sql += "\r\n select * from db_update where upd_id = '" + strUnitID + "'";
            sql += "\r\n select * from Devices where UnitID = '" + strUnitID + "'";

            String sRegistered1 = String.Empty;
            String sRegistered2 = String.Empty;
            NaveoSync.Service navService = new NaveoSync.Service();
            navService.Timeout = 120000; //2mins
            for (int iWS = 1; iWS <= 2; iWS++)
            {
                //if (iWS == 1)
                //    continue;
                if (iWS == 1)
                    navService.Url = GlobalVariables.GetPrimarySyncService();
                else if (iWS == 2)
                    navService.Url = GlobalVariables.GetSecondarySyncService();

#if DEBUG
                //navService.Url = "http://localhost:34125/Service.asmx";
                //sql = "select * from (select top 5 *, cast(RawData as varchar(500))RData from RawData where iid in (12155469, 12155506, 12156577) order by dtDatetime desc)x order by dtDatetime asc";
#endif

                DataSet ds = new DataSet();
                ds = Functions.DeCompressByteToDataSet(navService.GetDataSet(sql, strConn));
                if (ds.Tables.Count == 0)
                    continue;

                #region ReportingSummary
                DataTable dtRS = new DataTable("ReportingSummary");
                dtRS.Columns.Add("ReportingServer");
                dtRS.Columns.Add("ServerName");
                dtRS.Columns.Add("DBName");
                dtRS.Columns.Add("Sim");
                dtRS.Columns.Add("UnitID");
                dtRS.Columns.Add("UnitModel");
                dtRS.Columns.Add("Svr1DeviceRegistered");
                dtRS.Columns.Add("Svr2DeviceRegistered");

                String sRegistered = "NO !!!";
                if (ds.Tables.Count >= 2)
                    if (ds.Tables[1].Rows.Count >= 1)
                        sRegistered = "Yes";

                if (iWS == 1)
                    sRegistered1 = sRegistered;
                else if (iWS == 2)
                    sRegistered2 = sRegistered;

                String strReportingServer = "Server " + iWS.ToString();
                String strServerName = String.Empty;
                String strDBName = String.Empty;
                String strSvrDB = String.Empty;
                String strUnitModel = String.Empty;
                foreach (DataRow dr in ds.Tables[0].Rows)
                    if (!strSvrDB.Contains(dr["ServerName"].ToString()) | !strSvrDB.Contains(dr["DBName"].ToString()))
                    {
                        strSvrDB += "[" + dr["ServerName"].ToString() + " - " + dr["DBName"].ToString() + "] \r";
                        strServerName = dr["ServerName"].ToString();
                        strDBName = dr["DBName"].ToString();
                        strUnitModel = dr["Model"].ToString();

                        DataRow drInsert = dtRS.NewRow();
                        drInsert["ReportingServer"] = strReportingServer;
                        drInsert["ServerName"] = dr["ServerName"];
                        drInsert["DBName"] = dr["DBName"];
                        drInsert["UnitModel"] = dr["Model"];
                        drInsert["UnitID"] = dr["UnitID"];
                        drInsert["Svr1DeviceRegistered"] = sRegistered1;
                        drInsert["Svr2DeviceRegistered"] = sRegistered2;
                        dtRS.Rows.Add(drInsert);
                    }

                if (strSvrDB == String.Empty)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                        strUnitModel = dr["Model"].ToString();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow drInsert = dtRS.NewRow();
                        drInsert["ReportingServer"] = strReportingServer;
                        drInsert["ServerName"] = "Nobody Downloading";
                        drInsert["DBName"] = String.Empty;
                        drInsert["UnitModel"] = strUnitModel;
                        drInsert["UnitID"] = strUnitID;
                        dtRS.Rows.Add(drInsert);
                    }
                }

                String strIMSI = String.Empty;
                String strImsiTmp = String.Empty;
                String strModel = String.Empty;
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    strModel = dr["Model"].ToString();
                    try
                    {
                        switch (strModel)
                        {
                            case "AtrackImsi":
                                strImsiTmp = new NavLib.Atrack.DecodeAtrack((Byte[])dr["RawData"]).GetImsi().Trim();
                                break;

                            case "OrionImsi":
                                strImsiTmp = new NavLib.Orion.DecodeOrion((Byte[])dr["RawData"]).oReports[0].GetIMSI().Trim();
                                break;

                            case "MeitrackImsi":
                                strImsiTmp = new NavLib.Meitrack.DecodeMeiTrack((Byte[])dr["RawData"]).GetIMSI().Trim();
                                break;

                            case "AT100IMSI":
                                strImsiTmp = new NavLib.AT100.DecodeAT100IMSI((Byte[])dr["RawData"]).GetIMSI().Trim();
                                break;

                            case "EnforaImsi":
                                strImsiTmp = new NavLib.Enfora.clsGSM2218((Byte[])dr["RawData"]).GetIMSI().Trim();
                                break;

                            case "EnfornIMSI":
                                strImsiTmp = new NavLib.Enfora.DecodeEnfora((Byte[])dr["RawData"]).GetIMSI().Trim();
                                break;

                            case "TELTONIKAIMSI":
                                //NavLib.TelTonika.DecodeTeltonika dTeltonika = new NavLib.TelTonika.DecodeTeltonika((Byte[])dr["RawData"]);
                                //if (dTeltonika.tReports != null)
                                //    foreach (NavLib.TelTonika.TeltonikaReport tReport in dTeltonika.tReports)
                                //    {
                                //        String s = tReport.GetSimIMSI().ToString();
                                //        if (s.Trim().Length > 0)
                                //        {
                                //            strImsiTmp = s;
                                //            break;
                                //        }
                                //    }
                                break;
                        }
                        if (!strIMSI.Contains(strImsiTmp))
                        {
                            strIMSI += strImsiTmp;
                            foreach (DataRow dri in dtRS.Rows)
                                if (dri["ReportingServer"].ToString() == strReportingServer)
                                    if (dri["Sim"].ToString() == String.Empty)
                                        dri["Sim"] = strImsiTmp;
                                    else
                                    {
                                        DataRow drInsert = dtRS.NewRow();
                                        drInsert["ReportingServer"] = strReportingServer;
                                        drInsert["ServerName"] = strServerName;
                                        drInsert["DBName"] = strDBName;
                                        drInsert["Sim"] = strImsiTmp;
                                        drInsert["UnitModel"] = strUnitModel;
                                        drInsert["UnitID"] = strUnitID;
                                        dtRS.Rows.Add(drInsert);
                                    }
                        }
                    }
                    catch { }
                }
                dsResult.Merge(dtRS);
                #endregion
                #region Decoded Data
                if (!bSummaryOnly)
                {
                    DataTable dtDecode = new DataTable("Decode");
                    ModelProcessing mp = new ModelProcessing();
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        String strDecode = mp.GetUnitLogs(dr["Model"].ToString(), (Byte[])dr["RawData"], dr["unitID"].ToString());
                        if (String.IsNullOrEmpty(strDecode))
                            strDecode = dr["rData"].ToString();

                        String[] split = strDecode.Split(';');
                        if (split.Length > dtDecode.Columns.Count)
                        {
                            int i = dtDecode.Columns.Count;
                            while (dtDecode.Columns.Count <= split.Length)
                            {
                                dtDecode.Columns.Add("Field" + i.ToString(), typeof(String));
                                i++;
                            }
                        }
                        Object[] array = new Object[split.Length];
                        for (int j = 0; j < split.Length; j++)
                            array[j] = split[j];
                        dtDecode.Rows.Add(array);
                    }
                    dsResult.Merge(dtDecode);
                }
                if (bWriteToFile)
                {
                    ModelProcessing mp = new ModelProcessing();
                    foreach (DataRow dr in ds.Tables[0].Rows)
                        mp.WriteUnitLogsInAFile(dr["Model"].ToString(), (Byte[])dr["RawData"], dr["unitID"].ToString(), 0);
                }
                #endregion
                #region RawData
                if (!bSummaryOnly)
                {
                    String strBinF = String.Empty;
                    foreach (DataTable dt in ds.Tables)
                        foreach (DataColumn dc in dt.Columns)
                            if (dc.DataType.Name == "Byte[]")
                                strBinF += dc.ColumnName + ";";

                    foreach (DataTable dt in ds.Tables)
                    {
                        String[] x = strBinF.Split(';');
                        foreach (String g in x)
                            if (g != String.Empty)
                                dt.Columns.Remove(g);
                        strBinF = String.Empty;
                    }
                    dsResult.Merge(ds);
                }
                #endregion
            }
            if (dsResult.Tables["ReportingSummary"].Rows.Count == 0)
            {
                DataRow drInsert = dsResult.Tables["ReportingSummary"].NewRow();
                drInsert["ReportingServer"] = "Unit not reporting";
                drInsert["Svr1DeviceRegistered"] = sRegistered1;
                drInsert["Svr2DeviceRegistered"] = sRegistered2;
                dsResult.Tables["ReportingSummary"].Rows.Add(drInsert);
            }
            else
                foreach (DataRow dr in dsResult.Tables["ReportingSummary"].Rows)
                {
                    if (String.IsNullOrEmpty(dr["Svr1DeviceRegistered"].ToString().Trim()))
                        dr["Svr1DeviceRegistered"] = sRegistered1;
                    if (String.IsNullOrEmpty(dr["Svr2DeviceRegistered"].ToString().Trim()))
                        dr["Svr2DeviceRegistered"] = sRegistered2;
                }

            return dsResult;
        }
        /* public DataSet QryNaveoSvr(String sql, Boolean bOldData)
         {
             DataSet dsResult = new DataSet();
             String strConn = "ConnStr";
             if (bOldData)
                 strConn = "ConnStrTransferred";

             NaveoSync.Service navService = new NaveoSync.Service();
             navService.Timeout = 120000; //2mins
             for (int iWS = 1; iWS <= 1; iWS++)
             {
                 if (iWS == 1)
                     navService.Url = GlobalVariables.GetPrimarySyncService();
                 else if (iWS == 2)
                     navService.Url = GlobalVariables.GetSecondarySyncService();

                 DataSet ds = new DataSet();
                 for (int i = 0; i < 25; i++)
                 {
                     ds = Functions.DeCompressByteToDataSet(navService.GetDataSet(sql, strConn));
                     if (ds.Tables.Count > 0)
                         break;
                 }

                 if (ds.Tables.Count == 0)
                     continue;

                 dsResult.Merge(ds);
             }
             return dsResult;
         }*/

        public DataSet QryNaveoLnkSvr(String sql, Boolean bOldData)
        {
            String sWebServices = ConfigurationManager.AppSettings["WebServices"].ToString();

            DataSet dsResult = new DataSet();
            String strConn = "ConnStr";
            if (bOldData)
                strConn = "ConnStrTransferred";

            NaveoSync.Service navService = new NaveoSync.Service();
            navService.Timeout = 120000; //2mins

            String[] WebServices = sWebServices.Split(';');

            foreach (String ws in WebServices)
            {
                if (ws == "1")
                    navService.Url = GlobalVariables.GetPrimarySyncService();
                else if (ws == "2")
                    navService.Url = GlobalVariables.GetSecondarySyncService();
                else if (ws == "3")
                    navService.Url = GlobalVariables.GetTertiarySyncService();
                else if (ws == "4")
                    navService.Url = GlobalVariables.GetQuaternarySyncService();
                else if (ws == "5")
                    navService.Url = GlobalVariables.GetQuinarySyncService();
                else
                    navService.Url = ws;

                DataSet ds = new DataSet();
                for (int i = 0; i < 25; i++)
                {
                    ds = Functions.DeCompressByteToDataSet(navService.GetDataSet(sql, strConn));
                    if (ds.Tables.Count > 0)
                        break;
                }

                if (ds.Tables.Count == 0)
                    continue;

                dsResult.Merge(ds);
            }
            return dsResult;
        }

        public DataSet QryNaveoOne(String sql, String sWebUrl, String strDB)
        {
            Connection c = new Connection(strDB);
            DataTable dtSql = c.dtSql();
            DataRow drSql = dtSql.NewRow();

            Connection.UsingWebService = "Y";
            Connection.WebURL = sWebUrl;

            dtSql.Rows.Clear();
            drSql = dtSql.NewRow();
            drSql["sSQL"] = sql;
            drSql["sTableName"] = "MultipleDTfromQuery";
            dtSql.Rows.Add(drSql);

            DataSet dsResult = new DataSet();
            for (int i = 0; i < 25; i++)
            {
                dsResult = c.GetDataDS(dtSql, strDB);
                if (dsResult != null)
                    if (dsResult.Tables.Count > 0)
                        break;
            }

            return dsResult;
        }

        public Boolean SendMessageToSend(String sUnitID, String sUnitModel, String sMessage)
        {
            Boolean b = false;
            NaveoSync.Service navService = new NaveoSync.Service();
            navService.Timeout = 120000; //2mins
            for (int iWS = 1; iWS <= 2; iWS++)
            {
                if (iWS == 1)
                    navService.Url = GlobalVariables.GetPrimarySyncService();
                else if (iWS == 2)
                    navService.Url = GlobalVariables.GetSecondarySyncService();

                //testing. start Ws and NaveoLinkServer
#if DEBUG
                navService.Url = "http://localhost:34125/Service.asmx";
#endif

                if (!b)
                    b = navService.SendMessageToSend(sUnitID, sUnitModel, sMessage);
            }

            return b;
        }
        public DataTable QryMessageToSend()
        {
            DataTable dt = new DataTable();
            NaveoSync.Service navService = new NaveoSync.Service();
            navService.Timeout = 120000; //2mins

            for (int iWS = 1; iWS <= 2; iWS++)
            {
                if (iWS == 1)
                    navService.Url = GlobalVariables.GetPrimarySyncService();
                else if (iWS == 2)
                    navService.Url = GlobalVariables.GetSecondarySyncService();

                //navService.Url = "http://localhost:34125/Service.asmx";

                DataSet ds = new DataSet();
                ds = Functions.DeCompressByteToDataSet(navService.GetDataSet("select Model, UnitData, unitID from MessageData", "ConnStr"));
                if (ds.Tables.Count > 0)
                    dt.Merge(ds.Tables[0]);
            }

            DataTable dtUniqRecords = dt.DefaultView.ToTable(true, new String[] { "Model", "UnitData", "unitID" });
            return dtUniqRecords;
        }


        public Guid CreateBeginConnection(String InitialChk)
        {
            Guid g = new Guid();
            NaveoSync.Service navService = new NaveoSync.Service();
            navService.Timeout = 120000; //2mins
            for (int iWS = 1; iWS <= 2; iWS++)
            {
                if (iWS == 1)
                    navService.Url = GlobalVariables.GetPrimarySyncService();
                else if (iWS == 2)
                    navService.Url = GlobalVariables.GetSecondarySyncService();

                g = navService.CreateBeginConnection(InitialChk);
                if (g != Guid.NewGuid())
                    break;
            }

            return g;
        }
    }
}
