﻿using NaveoOneLib.DBCon;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.ErrorData;
using System;
using System.Data;

namespace NaveoService.Services
{
    public class ErrorService
    {
        public dtData GetFilteredErrorData(String sConnStr, ErrorData eData, int? Page, int? LimitPerPage)
        {
            DataTable dtErrorData = NaveoOneLib.Services.Error.ErrorService.GetFilteredErrorData(sConnStr, eData);

            //if (dtErrorData.Columns.Contains("errorid"))
            //    dtErrorData.Columns["errorid"].ColumnName = "ErrorID";

            //if (dtErrorData.Columns.Contains("requesttype"))
            //    dtErrorData.Columns["requesttype"].ColumnName = "Requestype";

            //if (dtErrorData.Columns.Contains("usertoken"))
            //    dtErrorData.Columns["usertoken"].ColumnName = "UserToken";

            //if (dtErrorData.Columns.Contains("description"))
            //    dtErrorData.Columns["description"].ColumnName = "Description";

            DataTable ResultTable = dtErrorData.Clone();
            ResultTable.Columns.Add("rowNum", typeof(int));
            ResultTable.Columns["rowNum"].AutoIncrement = true;
            ResultTable.Columns["rowNum"].AutoIncrementSeed = 1;
            ResultTable.Columns["rowNum"].AutoIncrementStep = 1;
            foreach (DataRow dr in dtErrorData.Rows)
                ResultTable.Rows.Add(dr.ItemArray);

            int iTotalRec = ResultTable.Rows.Count;
            Pagination p = Pagination.GetRowNums(Page, LimitPerPage);
            DataTable dt = new DataTable();
            DataRow[] drChk = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo);
            if (drChk != null && drChk.Length > 0)
                dt = ResultTable.Select("RowNum >= " + p.RowFrom + " and RowNum <= " + p.RowTo).CopyToDataTable();
            else
                dt = ResultTable;

            dtData dtR = new dtData();
            dtR.Data = dt;
            dtR.Rowcnt = iTotalRec;

            return dtR;
        }

        public Boolean UpdateErrorDataStatus(int ErrorID, int Status, String sConnStr) {
            var obj = new NaveoOneLib.Services.Error.ErrorService();
            BaseError err = obj.GetErrorById(ErrorID, sConnStr);
            err.Status = Status;
            return obj.UpdateErrorStatus(err, sConnStr);
        }
    }
}