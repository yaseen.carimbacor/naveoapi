﻿using System.Web;
using System.Web.Optimization;

namespace NaveoWebApi
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Resources/js/jquery-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Resources/js/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Resources/js/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Resources/js/moment.min.js",
                      "~/Resources/Template/assets/global/plugins/bower_components/bootstrap/dist/js/bootstrap.min.js",
                      "~/Resources/Template/assets/global/plugins/bower_components/jquery-cookie/jquery.cookie.js",
                      "~/Resources/Template/assets/global/plugins/bower_components/jquery-nicescroll/jquery.nicescroll.min.js",
                      "~/Resources/Template/assets/global/plugins/bower_components/jquery-easing-original/jquery.easing.1.3.min.js",
                      "~/Resources/Template/assets/global/plugins/bower_components/typehead.js/dist/handlebars.js",
                      "~/Resources/Template/assets/global/plugins/bower_components/typehead.js/dist/typeahead.bundle.min.js",
                      "~/Resources/Template/assets/global/plugins/bower_components/jquery.sparkline.min/index.js",
                      "~/Resources/Template/assets/global/plugins/bower_components/bootbox/bootbox.js",
                      "~/Resources/Template/assets/global/plugins/bower_components/noty/js/noty/packaged/jquery.noty.packaged.min.js",
                      "~/Resources/Template/assets/global/plugins/bower_components/ionsound/js/ion.sound.min.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/layout").Include(
                         "~/Resources/Template/assets/global/plugins/bower_components/select2/dist/js/select2.full.min.js",
                      "~/Resources/Template/assets/admin/js/apps.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Resources/Template/assets/global/plugins/bower_components/bootstrap/dist/css/bootstrap.min.css",
                      "~/Resources/Template/assets/global/plugins/bower_components/fontawesome/css/font-awesome.min.css",
                      "~/Resources/Template/assets/global/plugins/bower_components/animate.css/animate.min.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/Theme").Include(
                      "~/Resources/Template/assets/admin/css/reset.css",
                      "~/Resources/Template/assets/admin/css/layout.css",
                      "~/Resources/Template/assets/admin/css/components.css",
                      "~/Resources/Template/assets/admin/css/plugins.css",
                      "~/Resources/Template/assets/admin/css/themes/red.theme.css",
                      "~/Resources/Template/assets/admin/css/custom.css"));

            bundles.Add(new ScriptBundle("~/bundles/dashboard").Include(             
                      "~/Resources/Template/assets/admin/js/pages/blankon.dashboard.js"));

            bundles.Add(new ScriptBundle("~/bundles/signin").Include(
                      "~/Resources/Template/assets/admin/js/pages/blankon.sign.js",
                      "~/Resources/Template/assets/admin/js/demo.js"));

            bundles.Add(new StyleBundle("~/Content/signin").Include(
                       "~/Resources/Template/assets/admin/css/Pages/sign.css"));

            bundles.Add(new ScriptBundle("~/bundles/dashboardMaps").Include(
                      "~/Resources/Template/assets/global/plugins/bower_components/gmap3/dist/gmap3.min.js",
                      "~/Resources/Template/assets/global/plugins/bower_components/skycons-html5/skycons.js",
                      "~/Resources/Template/assets/admin/js/pages/blankon.maps.google.js"));

            bundles.Add(new ScriptBundle("~/bundles/ui-layout").Include(
                        "~/Resources/js/Splitter.js"));

            bundles.Add(new StyleBundle("~/Content/ui-layout").Include(
                      "~/Resources/css/layout-default-latest.css"));

            bundles.Add(new StyleBundle("~/Kendo/css").Include(
                      "~/Resources/css/kendo/2016/kendo.common.min.css",
                      "~/Resources/css/kendo/2016/kendo.mobile.all.min.css",
                      "~/Resources/css/kendo/2016/kendo.dataviz.min.css",
                      "~/Resources/css/kendo/2016/kendo.dataviz.default.min.css",
                      "~/Resources/css/kendo/2016/kendo.default.min.css"));

            bundles.Add(new ScriptBundle("~/Kendo/js").Include(
                    "~/Resources/js/kendo.core.min.js",                   
                    "~/Resources/js/kendo.all.min.js",
                    "~/Resources/js/kendo.aspnetmvc.min.js",
                     "~/Resources/js/jszip.min.js",
                    "~/Resources/js/kendo.culture.en-GB.min.js",
                    "~/Resources/js/kendo.modernizr.custom.js"));

            bundles.Add(new ScriptBundle("~/DataTables/js").Include(
                    "~/Resources/Template/assets/global/plugins/bower_components/datatables/js/jquery.dataTables.min.js",
                    "~/Resources/Template/assets/global/plugins/bower_components/datatables/extentions/dataTables.select.min.js",
                    "~/Resources/Template/assets/global/plugins/bower_components/datatables/extentions/dataTables.buttons.min.js",
                    "~/Resources/Template/assets/global/plugins/bower_components/datatables/extentions/jszip.min.js",
                    "~/Resources/Template/assets/global/plugins/bower_components/datatables/extentions/pdfmake.min.js",
                    "~/Resources/Template/assets/global/plugins/bower_components/datatables/extentions/vfs_fonts.js",
                    "~/Resources/Template/assets/global/plugins/bower_components/datatables/extentions/buttons.html5.min.js",
                    "~/Resources/Template/assets/global/plugins/bower_components/datatables/extentions/buttons.print.min.js",
                    "~/Resources/Template/assets/global/plugins/bower_components/datatables/extentions/full_numbers_no_ellipses.js",
                    "~/Resources/Template/assets/global/plugins/bower_components/datatables/extentions/dataTables.bootstrap.js",
                    "~/Resources/Template/assets/global/plugins/bower_components/datatables/extentions/datatables.responsive.js",
                    "~/Resources/Template/assets/global/plugins/bower_components/jquery-mockjax/jquery.mockjax.js",
                    "~/Resources/Template/assets/global/plugins/bower_components/jquery.gritter/js/jquery.gritter.min.js"));

            bundles.Add(new StyleBundle("~/DataTables/css").Include(
                    "~/Resources/Template/assets/global/plugins/bower_components/datatables/css/dataTables.bootstrap.css",
                    "~/Resources/Template/assets/global/plugins/bower_components/datatables/css/datatables.responsive.css",
                    "~/Resources/Template/assets/global/plugins/bower_components/datatables/css/select.dataTables.min.css",
                    "~/Resources/Template/assets/admin/css/pages/table-advanced.css",
                    "~/Resources/Template/assets/global/plugins/bower_components/datatables/css/buttons.dataTables.min.css"));

            bundles.Add(new ScriptBundle("~/Leaflet/js").Include(
                    "~/Resources/leaflet/leaflet.js",
                    "~/Resources/leaflet/catiline.js",
                    "~/Resources/leaflet/leaflet.shpfile.js",
                    "~/Resources/leaflet/leaflet.label.js",
                    "~/Resources/leaflet/leaflet.markercluster-src.js",
                    "~/Resources/leaflet/leaflet.spin.js",
                    "~/Resources/leaflet/spin.min.js",
                    "~/Resources/js/custom-leaflet.js"               
                    ));

            bundles.Add(new StyleBundle("~/Leaflet/css").Include(
                      "~/Resources/leaflet/leaflet.css",
                      "~/Resources/leaflet/leaflet.label.css",
                      "~/Resources/leaflet/MarkerCluster.css",
                      "~/Resources/leaflet/MarkerCluster.Default.css",
                      "~/Resources/css/custom-leaflet.css"));

            bundles.Add(new ScriptBundle("~/JsTree/js").Include(
                    "~/Resources/js/jstree/jstree.min.js"));

            bundles.Add(new StyleBundle("~/JsTree/css").Include(
                      "~/Resources/css/jstree/style.min.css"));

            bundles.Add(new StyleBundle("~/Choosen/css").Include(
                "~/Resources/Template/assets/global/plugins/bower_components/select2/dist/css/select2.min.css",
                "~/Resources/Template/assets/global/plugins/bower_components/chosen_v1.2.0/chosen.min.css"));
			bundles.Add(new StyleBundle("~/DatepickerRange/css").Include(
                      "~/Resources/css/daterangepicker.css"));
        
            bundles.Add(new ScriptBundle("~/DatepickerRange/js").Include(
                   "~/Resources/js/daterangepicker.js"));
        }
	}
}
