﻿using System.Web.Http;
using System.Web.Http.Routing;
using Microsoft.Web.Http.Routing;
using System.Net.Http.Headers;
using NaveoWebApi.Filters;
using System.Web.Http.ExceptionHandling;
using System;
using NaveoOneLib.Common;
using System.Threading;
using System.Net.Http;
using System.Threading.Tasks;
using System.Configuration;
using NaveoOneLib.Services;
using System.Data;
using NaveoOneLib.DBCon;

namespace NaveoWebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //Error Handling
            config.Services.Replace(typeof(IExceptionLogger), new UnhandledExceptionLogger());

            //Logging.
            string enableApiLogging = ConfigurationManager.AppSettings["enableApiLogging"];
            if (enableApiLogging == "yes")
                config.MessageHandlers.Add(new CustomLogHandler());

            // Add Api versioning
            config.AddApiVersioning();

            var constraintResolver = new DefaultInlineConstraintResolver()
            {
                ConstraintMap =
                {
                    ["apiVersion"] = typeof( ApiVersionRouteConstraint )
                }
            };
            config.MapHttpAttributeRoutes(constraintResolver);

            // NOTE: This code is commented
            // config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Filters.Add(new RequireSsl());

            //default response type in JSON
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
        }

        #region Error Handling
        public class UnhandledExceptionLogger : ExceptionLogger
        {
            public override void Log(ExceptionLoggerContext context)
            {
                String log = "WebApiConfig_UnhandledException /r/n" + context.Exception.ToString();
                Errors.WriteToErrorLog(log);
                String sConnStr = NaveoWebApi.Helpers.CommonHelper.GetCurrentConnStr();
                Guid userToken = NaveoWebApi.Helpers.SecurityHelper.GetUserToken(context.Request.Headers);
                logToDB(context, userToken, sConnStr);
            }

            private void logToDB(ExceptionLoggerContext context, Guid UserToken, String sConnStr)
            {
                DataTable dt = new NaveoOneLib.Services.Error.ErrorService().ErrorDt();
                bool bInsert = true;

                #region logic for getting module and functionname
                var s = context.Request.RequestUri.AbsolutePath;
                var controllername = context.Exception.StackTrace;

                string[] lines = controllername.Split(new[] { Environment.NewLine }, StringSplitOptions.None);

                var cN = lines[0];
                string[] cName = cN.Split('.');
                string module  = cName.GetValue(Array.IndexOf(cName, "V2") + 1).ToString();
                string FunctionName = cName.GetValue(Array.IndexOf(cName, "V2") + 2).ToString();

                module = module.Replace("Controller", "");
                string source = "MODULE : " + module + " , FUNCTION : " + FunctionName + " , PROJECT : " + context.Exception.Source;
                #endregion

                #region sentError MAILS
                MailSender ms = new MailSender();
                GlobalParamsService gs = new GlobalParamsService();
                
                String email = gs.GetGlobalParamsByName("SystemAdminEmail", sConnStr).PValue;
                String sBody = "Dear Admin \r\n\r\n";
                String title = "New Unhandled Error";
                sBody += @"Please be alert of the new error that has been reported at " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")+" \r\n\r\n";
                sBody += @"ERROR DETAILS \r\n\r\n\r\n\r\n";
                sBody += @"Error :" + context.Exception.Message + " \r\n\r\n";
                sBody += @"Source : " + source + " \r\n\r\n";
                sBody += @"StackTrace  \r\n\r\n\r\n\r\n" + context.Exception.StackTrace;

                ms.SendMail(email, title, sBody, String.Empty, sConnStr);
                #endregion

                DataRow dr = dt.NewRow();
                dr["RequestType"] = context.Request.Method.ToString();
                dr["UserToken"] = UserToken.ToString();
                dr["Description"] = context.Exception.Message;
                dr["Source"] = source;
                dr["Time"] = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                dr["StackTrace"] = context.Exception.StackTrace;
                dt.Rows.Add(dr);

                Connection c = new Connection(sConnStr);
                DataTable dtCtrl = c.CTRLTBL();
                DataRow drCtrl = dtCtrl.NewRow();
                drCtrl["SeqNo"] = 1;
                drCtrl["TblName"] = dt.TableName;
                drCtrl["CmdType"] = "TABLE";
                drCtrl["KeyFieldName"] = "ErrorID";
                drCtrl["KeyIsIdentity"] = "TRUE";
                drCtrl["NextIdAction"] = "GET";

                dtCtrl.Rows.Add(drCtrl);
                DataSet dsProcess = new DataSet();
                dsProcess.Merge(dt);

                foreach (DataTable dti in dsProcess.Tables)
                {
                    if (!dti.Columns.Contains("oprType"))
                        dti.Columns.Add("oprType", typeof(DataRowState));

                    foreach (DataRow dri in dti.Rows)
                        if (dri["oprType"].ToString().Trim().Length == 0)
                        {
                            if (bInsert)
                                dri["oprType"] = DataRowState.Added;
                            else
                                dri["oprType"] = DataRowState.Modified;
                        }
                }

                dsProcess.Merge(dtCtrl);
                dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables["CTRLTBL"];
            }
        }
        #endregion

        #region Logging
        public class LogMetadata
        {
            public string RequestContentType { get; set; }
            public string RequestUri { get; set; }
            public string RequestMethod { get; set; }
            public DateTime? RequestTimestamp { get; set; }
            public string ResponseContentType { get; set; }
            public System.Net.HttpStatusCode ResponseStatusCode { get; set; }
            public DateTime? ResponseTimestamp { get; set; }
        }

        public class CustomLogHandler : DelegatingHandler
        {
            protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
            {
                var logMetadata = BuildRequestMetadata(request);
                var response = await base.SendAsync(request, cancellationToken);
                logMetadata = BuildResponseMetadata(logMetadata, response);
                SendToLog(logMetadata);
                return response;
            }
            private LogMetadata BuildRequestMetadata(HttpRequestMessage request)
            {
                LogMetadata log = new LogMetadata
                {
                    RequestMethod = request.Method.Method,
                    RequestTimestamp = DateTime.Now,
                    RequestUri = request.RequestUri.ToString()
                };
                return log;
            }
            private LogMetadata BuildResponseMetadata(LogMetadata logMetadata, HttpResponseMessage response)
            {
                logMetadata.ResponseStatusCode = response.StatusCode;
                logMetadata.ResponseTimestamp = DateTime.Now;
                if (response.Content != null)
                    logMetadata.ResponseContentType = response.Content.Headers.ContentType.MediaType;
                return logMetadata;
            }
            private bool SendToLog(LogMetadata logMetadata)
            {
                if (logMetadata.RequestUri.Contains("HasFileToDwn"))
                    return true;

                // TODO: Write code here to store the logMetadata instance to a pre-configured log store...
                String log = "WebApiConfig_Log \r\n";
                log += "RequestContentType : " + logMetadata.RequestContentType + "\r\n";
                log += "RequestUri : " + logMetadata.RequestUri + "\r\n";
                log += "RequestMethod : " + logMetadata.RequestMethod + "\r\n";
                if (logMetadata.RequestTimestamp.HasValue)
                    log += "RequestTimestamp : " + logMetadata.RequestTimestamp.ToString() + "\r\n";
                log += "ResponseContentType : " + logMetadata.ResponseContentType + "\r\n";
                log += "ResponseStatusCode : " + logMetadata.ResponseStatusCode + "\r\n";
                if (logMetadata.ResponseTimestamp.HasValue)
                    log += "ResponseTimestamp : " + logMetadata.ResponseTimestamp + "\r\n";
                Errors.WriteToLog(log);
                return true;
            }
        }
        #endregion
    }
}
