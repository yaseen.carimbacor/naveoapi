﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;

namespace NaveoWebApi.Controllers.Extensions
{
    public static class HttpRequestMessageExtensions
    {

        private const string HttpContext = "MS_HttpContext";
        private const string RemoteEndpointMessage = "System.ServiceModel.Channels.RemoteEndpointMessageProperty";
        //private const string OwinContext = "MS_OwinContext";

        /// <summary>
        /// Get Sender IP as IPAdress
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>d
        public static IPAddress GetSenderIpAddress(this HttpRequestMessage request)
        {
            var ipAddressStr = request.GetSenderIp();
            IPAddress ipAddress = new IPAddress(0);
            if (IPAddress.TryParse(ipAddressStr, out ipAddress))
            {
                return ipAddress;
            }

            return ipAddress;
        }

        /// <summary>
        /// Get Sender IP as String
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static string GetSenderIp(this HttpRequestMessage request)
        {
            //Web-hosting
            if (request.Properties.ContainsKey(HttpContext))
            {
                dynamic ctx = request.Properties[HttpContext];
                if (ctx != null)
                {
                    return ctx.Request.UserHostAddress;
                }
            }
            //Self-hosting
            if (request.Properties.ContainsKey(RemoteEndpointMessage))
            {
                dynamic remoteEndpoint = request.Properties[RemoteEndpointMessage];
                if (remoteEndpoint != null)
                {
                    return remoteEndpoint.Address;
                }
            }
            //Owin-hosting
            //if (request.Properties.ContainsKey(OwinContext))
            //{
            //    dynamic ctx = request.Properties[OwinContext];
            //    if (ctx != null)
            //    {
            //        return ctx.Request.RemoteIpAddress;
            //    }
            //}
            if (System.Web.HttpContext.Current != null)
            {
                return System.Web.HttpContext.Current.Request.UserHostAddress;
            }
            // In case of failure
            return "0.0.0.0";
        }

    

    }
}