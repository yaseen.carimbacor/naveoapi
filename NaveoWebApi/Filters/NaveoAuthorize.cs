﻿using NaveoOneLib.Models.Audits;
using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Services.Permissions;
using NaveoWebApi.Constants;
using NaveoWebApi.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Principal;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Results;
using System.Web.Security;

namespace NaveoWebApi.Controllers.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class NaveoAuthorizeAttribute : AuthorizeAttribute
    {
        public string RedirectUrl = "~/Error/Unauthorized";

        //private string[] userAssignedRoles;
        //public NaveoAuthorizeAttribute()
        //{
        //}

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            Guid userToken = SecurityHelper.GetUserToken(actionContext.Request.Headers);
            string controller = "";
            string action = "";
            if (userToken != SecurityHelper.DEFAULT_GUID)
            {
                string connStr = CommonHelper.GetCurrentConnStr();
                controller = actionContext.ControllerContext.ControllerDescriptor.ControllerName;
                action = actionContext.ActionDescriptor.ActionName;
                string rawRequest = "{}";
                //using (var stream = new StreamReader(actionContext.Request.Content.ReadAsStreamAsync().Result))
                //{
                //    stream.BaseStream.Position = 0;
                //    rawRequest = stream.ReadToEnd();
                //}

                // TODO: Check isAuthorized Method from dll as it is returning false (maybe data not present in database tables)

                AuditData sAudit = new AuditData();
                sAudit.Action = action;
                sAudit.Controller = controller;
                sAudit.UserToken = userToken;
                sAudit.RawRequest = rawRequest;
                sAudit.UserToken = userToken;

                if (PermissionsService.isAuthorized(userToken, controller, action, connStr))
                {
                    if (sAudit.Action.ToLower().Contains("save") || sAudit.Action.ToLower().Contains("create") || sAudit.Action.ToLower().Contains("update") || sAudit.Action.ToLower().Contains("delete")) return true;
                    
                    sAudit.IsAccessGranted = 1;
                    Telemetry.Trace(sAudit, connStr);
                    return true;
                }

                sAudit.IsAccessGranted = 0;
                Telemetry.Trace(sAudit, connStr);
                //return false;
            }

            //HandleUnauthorizedRequest(actionContext);
            //returning true for the moment
            //Telemetry.Trace("User was denied access to " + controller + "/" + action);
           
            return false;
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext filterContext)
        {
            //base.HandleUnauthorizedRequest(filterContext);

            //if (filterContext.RequestContext.HttpContext.User.Identity.IsAuthenticated)
            //{
            //    if (filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
            //    {
            //        var urlHelper = new UrlHelper(filterContext.RequestContext);
            //        filterContext.HttpContext.Response.StatusCode = 202;
            //        filterContext.Result = new JsonResult
            //        {
            //            Data = new
            //            {
            //                status = "Access Denied.",
            //                statuscode = 0
            //            },
            //            JsonRequestBehavior = JsonRequestBehavior.AllowGet
            //        };
            //    }
            //    else
            //        filterContext.Result = new RedirectResult(RedirectUrl);
            //}

            var response = filterContext.Request.CreateResponse(System.Net.HttpStatusCode.Redirect);
            response.Headers.Add("Location", "~/Account/Login");
            filterContext.Response = response;
        }
    }
}