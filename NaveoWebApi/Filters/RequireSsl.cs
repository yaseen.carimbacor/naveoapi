﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;
using NaveoWebApi.Models;

namespace NaveoWebApi.Filters
{
    /// <summary>
    /// Https URI validator class
    /// </summary>
    public class RequireSsl : AuthorizationFilterAttribute
    {
        ///  <summary>
        ///  Validate request URI
        ///  </summary>
        ///  <param name="actionContext">HttpActionContext value</param>
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (actionContext != null && actionContext.Request != null &&
            !actionContext.Request.RequestUri.Scheme.Equals(Uri.UriSchemeHttps))
            {
                var controllerFilters = actionContext.ControllerContext.ControllerDescriptor.GetFilters();
                var actionFilters = actionContext.ActionDescriptor.GetFilters();

                if ((controllerFilters != null && controllerFilters.Select
                (t => t.GetType() == typeof(RequireSsl)).Count() > 0) ||
                    (actionFilters != null && actionFilters.Select(t =>
                    t.GetType() == typeof(RequireSsl)).Count() > 0))
                {
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Forbidden,
                        new Message() { Content = "SSL is needed for the request.Use HTTPS instead" },
                        new MediaTypeHeaderValue("text/json"));
                }
            }
        }
    }
}