﻿using System;
using System.Web.Mvc;
using NaveoService.Services;
using NaveoService.Utility;
using NaveoWebApi.Helpers;
using System.Collections.Generic;
using NaveoOneLib.Models;
using NaveoService.Models;
using System.Web.Security;
using System.Web;
using System.Web.Configuration;
using NaveoWebApi.Constants;
using System.Web.Hosting;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace NaveoWebApi.Controllers
{
    /// <summary>
    /// An extension to test api endpoints
    /// </summary>
    [AllowAnonymous]
    public class TestController: Controller
    {
        public ActionResult EndPoints()
        {
            return View("Test");
        }
       
    }
}
