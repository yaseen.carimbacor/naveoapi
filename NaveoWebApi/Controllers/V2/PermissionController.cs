﻿using Microsoft.Web.Http;
using NaveoOneLib.Models;
using NaveoService.Models;
using NaveoService.Services;
using NaveoWebApi.Constants;
using NaveoWebApi.Controllers.Extensions;
using NaveoWebApi.Helpers;
using NaveoWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using static NaveoService.Services.LibDataService;
using NaveoService.Models.DTO;
using Newtonsoft.Json;

namespace NaveoWebApi.Controllers.V2
{
    /// <summary>
    /// 
    /// </summary>
    [ApiVersion("2.0")]
    public class PermissionController : ApiBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet, ResponseType(typeof(UserPermissionsDTO))]
        [Route(Headers.API_PREFIX + "GetPermissionList")]
        public IHttpActionResult GetpermissionList()
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication(); //SecurityHelper.VerifyToken(Request.Headers, HttpContext.Current.Request.Url.DnsSafeHost);
            try
            {
                if (securityTokenObj.securityToken == null || !securityTokenObj.securityToken.IsAuthorized)
                    return base.Ok(false);
                else
                {
                    string permissionList = new PermissionService().GetUserPermissions(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr);
                    return base.Ok(castData(permissionList, securityTokenObj.securityToken.OneTimeToken));
                }
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="permissionList"></param>
        /// <param name="oneTimeToken"></param>
        /// <returns></returns>
        private UserPermissionsDTO castData(string permissionList, Guid oneTimeToken)
        {
            UserPermissionsDTO permission = new UserPermissionsDTO()
            {
                JsonPermissions = permissionList,
                oneTimeToken = oneTimeToken
            };
            return permission;
        }
        #endregion
    }
}