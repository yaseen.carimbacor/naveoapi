﻿using System;
using System.Web;
using System.IO;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using System.Globalization;
using Microsoft.Web.Http;
using NaveoService.Services;
using NaveoService.Helpers;
using NaveoService.Models;
using NaveoWebApi.Helpers;
using NaveoService.Constants;
using System.Configuration;
using System.Threading.Tasks;
using NaveoService.Models.Custom;
using System.Collections.Generic;
using System.Linq;
using static NaveoMonitoring.NaveoMonitoring;
using NaveoOneLib.Constant;


//using System.Collections.Generic;
//using System.IO;
//using System.IO.Compression;
//using System.IO.Compression.FileSystem;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class ExportController : ApiBaseController
    {
        private static string color { get; set; }
        private static readonly string rootExportFolderPath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings[ConfigurationKeys.ExportLocation]);
        private readonly string readyForDownloadFolder = Path.Combine(rootExportFolderPath, "Ready_For_Download");

        #region Api Methods

        /// <summary>
        /// Exports document based on entity and document format (Excel, csv or pdf)
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        [AllowAnonymous, HttpPost]
        [Route(Headers.API_PREFIX + "Export")]
        public HttpResponseMessage Export(Entity entity)
        {
            /*
               * E.g. call for Trip export
                  {
                    "entityName": "TRIPS",
	                "format": "XLSX",
	                "trip": {
		                "assetIds" : [17],
		                "dateFrom" : "2016-01-07 01:01:01",
		                "dateTo" : "2017-11-09 08:20:20"
	                }
                  }
            */
            //string uri = "https://localhost:44392/v2.0/Export";

            //Uri address = new Uri(uri);

            //HttpRequestMessage request = new HttpRequestMessage
            //        (HttpMethod.Post, address); // HttpMethod.Post is also works

            //var cert = new X509Certificate2(File.ReadAllBytes(@"c:\MyCert.cer"));
            ////var cert2 = new X509Certificate2(File.ReadAllBytes(@"c:\MyCert2.cer"));

            //List<string> certs = new List<string>();
            //certs.Add(cert.Thumbprint);
            ////certs.Add(cert2.Thumbprint);
            //request.Headers.Add("Thumbprint", certs);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            if (securityTokenObj.securityToken.IsAuthorized)
            {
                // Generate dynamic name for Exported document "Export-user"
                string documentName = string.Format("Export-{0}{1}", entity.entityName.ToLower(), DateTime.Now.ToString("MM_dd_yyyy_HH_mm_ss"));

                // Based on format requested in URI (PDF/XLSX), call the associated method.
                switch (entity.format)
                {
                    case FileSystem.Extension.CSV:
                        break;

                    case FileSystem.Extension.XLSX:
                        MemoryStream documentStream = new MemoryStream();

                        if (entity.entityName == NaveoEntity.Monitoring_Data)
                        {
                            documentStream = GenerateExcelStreamToExport(entity, securityTokenObj.securityToken, HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings[ConfigurationKeys.ExportLocation]));
                        }
                        else
                        {
                            documentStream = ExportDataService.GenerateExcelStreamToExport(entity, securityTokenObj.securityToken);
                        }

                        return FileHelper.ConvertStreamToFile(documentStream, documentName);

                    case FileSystem.Extension.PDF:
                    //MemoryStream pdfMemoryStream = ConvertXLSXtoPDF(documentStream);
                    //return FileAsAttachment(pdfMemoryStream, documentName);
                    //break;

                    default:
                        break;
                }
            }

            // If something fails or somebody calls invalid URI, throw error.
            return new HttpResponseMessage(HttpStatusCode.Unauthorized);
        }

        [AllowAnonymous, HttpPost]
        [Route(Headers.API_PREFIX + "PrepareFileToDwn")]
        public IHttpActionResult PrepareFileToDwn(Entity entity)
        {
            /*
              * E.g. call for Trip export
                 {
                   "entityName": "TRIPS",
                   "format": ".xlsx",
                   "trip": {
                       "assetIds" : [17],
                       "dateFrom" : "2016-01-07 01:01:01",
                       "dateTo" : "2017-11-09 08:20:20"
                   }
                 }
           */

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            if (securityTokenObj.securityToken.IsAuthorized)
            {
                // Generate dynamic name for Exported document "Export-user"
                string documentName = string.Format("Export-{0}", entity.entityName.ToLower());

                Boolean bExport = false;

                // Based on format requested in URI (PDF/XLSX), call the associated method.
                switch (entity.format)
                {
                    case FileSystem.Extension.CSV:
                        break;

                    case FileSystem.Extension.XLSX:
                        if (entity.entityName == NaveoEntity.Monitoring_Data | entity.entityName == NaveoEntity.Monitoring_Device)
                        {
                            bExport = SaveExcelExport(entity, securityTokenObj.securityToken, HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings[ConfigurationKeys.ExportLocation]));
                        }
                        else
                        {
                            bExport = ExportDataService.SaveExcelExport(entity, securityTokenObj.securityToken);
                        }

                            
                        return base.Ok(bExport.ToObjectDTO(securityTokenObj.securityToken));

                    case FileSystem.Extension.PDF:
                        bExport = ExportDataService.SavePDFExport(entity, securityTokenObj.securityToken);
                        return base.Ok(bExport.ToObjectDTO(securityTokenObj.securityToken));

                    default:
                        break;
                }
            }

            // If something fails or somebody calls invalid URI, throw error.
            return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
        }

        /// <summary>
        /// Heart beat for export
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous, HttpPost]
        [Route(Headers.API_PREFIX + "HasFileToDwn")]
        public IHttpActionResult HasFileToDwn()
        {
            try
            {
                SecurityTokenExtended securityTokenObj = ApiAuthentication();
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    string userToken = securityTokenObj.securityToken.UserToken.ToString();
                    string userExportFolderPath = Path.Combine(rootExportFolderPath, userToken);
                    HeartBeatResult result = new HeartBeatResult();

                    if (!(System.IO.Directory.Exists(userExportFolderPath)))
                        return Ok(result.ToHeartBeatDTO(securityTokenObj.securityToken));

                    #region Error Handling
                    FileInfo[] errorFiles = FileHelper.GetFiles(userExportFolderPath, FileSystem.Extension.TXT);
                    if (errorFiles != null && errorFiles.Any())
                    {
                        //Error present
                        // Return Name of file that could not be downloaded
                        result.errorList = new List<string>();
                        Parallel.ForEach(errorFiles, errorFile =>
                        {
                            result.errorList.Add(errorFile.Name.Replace(FileSystem.Extension.TXT, string.Empty));
                            result.errorList.Add(File.ReadAllText(errorFile.FullName));
                            File.Delete(errorFile.FullName);
                        });
                    }
                    #endregion

                    #region Files available for download

                    if (FileHelper.FileExists(userExportFolderPath, FileSystem.Extension.XLSX))
                    {
                        result.hasDownloads = true;
                        //if (!Directory.Exists(readyDownloadFolder))
                        //{
                        //    Directory.CreateDirectory(readyDownloadFolder);
                        //}
                        //System.IO.Compression.ZipFile.CreateFromDirectory(userExportFolderPath, Path.Combine(readyDownloadFolder, userToken + FileSystem.Extension.ZIP));
                    }


                    if (FileHelper.FileExists(userExportFolderPath, FileSystem.Extension.PDF))
                    {
                        result.hasDownloads = true;
                        //if (!Directory.Exists(readyDownloadFolder))
                        //{
                        //    Directory.CreateDirectory(readyDownloadFolder);
                        //}
                        //System.IO.Compression.ZipFile.CreateFromDirectory(userExportFolderPath, Path.Combine(readyDownloadFolder, userToken + FileSystem.Extension.ZIP));
                    }

                    #endregion

                    return Ok(result.ToHeartBeatDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [AllowAnonymous, HttpPost]
        [Route(Headers.API_PREFIX + "Download")]
        public IHttpActionResult Download()
        {
            try
            {
                SecurityTokenExtended securityTokenObj = ApiAuthentication();
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    string userToken = securityTokenObj.securityToken.UserToken.ToString();
                    string userExportFolderPath = Path.Combine(rootExportFolderPath, userToken);

                    #region Download

                    // Check to prevent exception error
                    if (FileHelper.FileExists(userExportFolderPath, FileSystem.Extension.XLSX))
                    {
                        if (!Directory.Exists(readyForDownloadFolder))
                        {
                            Directory.CreateDirectory(readyForDownloadFolder);
                        }

                        // Compress excel files
                        //string fileToDownloadPath = Path.Combine(readyForDownloadFolder, userToken + FileSystem.Extension.ZIP);
                        //System.IO.Compression.ZipFile.CreateFromDirectory(userExportFolderPath, fileToDownloadPath);

                        DownloadFile(userExportFolderPath, FileSystem.Extension.XLSX);

                        // DownloadZipFile(fileToDownloadPath);

                        // Delete excel files after download
                        FileHelper.TryDeleteFiles(userExportFolderPath, FileSystem.Extension.XLSX);

                        //Delete zip file after download,
                        //FileHelper.TryDeleteFile(fileToDownloadPath);
                        FileHelper.TryDeleteEmptyFolder(userExportFolderPath);

                        // TODO: Return files name that was downloaded + number of files also(???)

                        return Ok(true.ToBaseDTO(securityTokenObj.securityToken));
                    }


                    if (FileHelper.FileExists(userExportFolderPath, FileSystem.Extension.PDF))
                    {
                        if (!Directory.Exists(readyForDownloadFolder))
                        {
                            Directory.CreateDirectory(readyForDownloadFolder);
                        }

                        // Compress excel files
                        //string fileToDownloadPath = Path.Combine(readyForDownloadFolder, userToken + FileSystem.Extension.ZIP);
                        //System.IO.Compression.ZipFile.CreateFromDirectory(userExportFolderPath, fileToDownloadPath);

                        //DownloadZipFile(fileToDownloadPath);
                        DownloadFile(userExportFolderPath, FileSystem.Extension.PDF);

                        // Delete Pdf files after download
                        FileHelper.TryDeleteFiles(userExportFolderPath, FileSystem.Extension.PDF);
                        //Delete zip file after download,
                        // FileHelper.TryDeleteFile(fileToDownloadPath);
                        FileHelper.TryDeleteEmptyFolder(userExportFolderPath);



                        // TODO: Return files name that was downloaded + number of files also(???)

                        return Ok(true.ToBaseDTO(securityTokenObj.securityToken));
                    }



                    #endregion
                    securityTokenObj.securityToken.DebugMessage = "No files to download";
                    return Ok(false.ToBaseDTO(securityTokenObj.securityToken)); // No files has been been download
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        /// <summary>
        /// Download zip file
        /// </summary>
        /// <param name="userToken"></param>
        /// <param name="excelFilePath"></param>
        private static void DownloadZipFile(string downloadPath)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ContentType = FileHelper.ContentType(FileSystem.Extension.ZIP);
            HttpContext.Current.Response.AppendHeader("content-disposition", "attachment;filename=reports.zip");

            // From Nabla
            HttpContext.Current.Response.AppendHeader("x-filename", "reports.zip");
            HttpContext.Current.Response.AppendHeader("Access-Control-Request-Method", "*");
            HttpContext.Current.Response.AppendHeader("Access-Control-Request-Headers", "*");
            HttpContext.Current.Response.AppendHeader("Access-Control-Allow-Origin", "*");
            HttpContext.Current.Response.AppendHeader("Access-Control-Allow-Methods", "*");
            HttpContext.Current.Response.AppendHeader("Access-Control-Allow-Headers", "*");

            HttpContext.Current.Response.TransmitFile(downloadPath);

            HttpContext.Current.Response.End();
        }

        private static void DownloadFile(string downloadPath, string fileExtension)
        {

            DirectoryInfo folder = new DirectoryInfo(downloadPath);

            FileInfo[] files = folder.GetFiles("*");


            HttpContext.Current.Response.Clear();


            HttpContext.Current.Response.ContentType = FileHelper.ContentType(fileExtension);


            HttpContext.Current.Response.AppendHeader("content-disposition", "attachment;filename=" + files[0].Name);
            //HttpContext.Current.Response.AppendHeader("content-disposition", "attachment;filename=reports");

            // From Nabla
            HttpContext.Current.Response.AppendHeader("x-filename", files[0].Name);
            HttpContext.Current.Response.AppendHeader("Access-Control-Request-Method", "*");
            HttpContext.Current.Response.AppendHeader("Access-Control-Request-Headers", "*");
            HttpContext.Current.Response.AppendHeader("Access-Control-Allow-Origin", "*");
            HttpContext.Current.Response.AppendHeader("Access-Control-Allow-Methods", "*");
            HttpContext.Current.Response.AppendHeader("Access-Control-Allow-Headers", "*");

            HttpContext.Current.Response.TransmitFile(files[0].FullName);

            HttpContext.Current.Response.End();
        }

        /// <summary>
        /// Create a ZIP file of the files provided.
        /// </summary>
        /// <param name="fileName">The full path and name to store the ZIP file at.</param>
        /// <param name="files">The list of files to be added.</param>
        //public static void CreateZipFile(string fileName, IEnumerable<string> files)
        //{
        //    // Create and open a new ZIP file
        //    var zip = ZipFile.Open(fileName, ZipArchiveMode.Create);
        //    foreach (var file in files)
        //    {
        //        // Add the entry for each file
        //        zip.CreateEntryFromFile(file, Path.GetFileName(file), CompressionLevel.Optimal);
        //    }
        //    // Dispose of the object when we are done
        //    zip.Dispose();
        //}
        #endregion
    }
}

