﻿using Microsoft.Web.Http;
using NaveoService.Services;
using NaveoWebApi.Constants;
using NaveoWebApi.Helpers;
using System;
using System.Web.Http;
using System.Web.Http.Description;
using NaveoService.Models.DTO;
using NaveoWebApi.Models;
using NaveoOneLib.Models.Common;
using System.Web.Script.Serialization;
using NaveoService.Models;
using NaveoService.Helpers;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class AuditController : ApiBaseController
    {
        #region API Methods

        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "GetAuditData")]
        public IHttpActionResult GetAuditData(Audit audit)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new NaveoService.Services.AuditService().GetAuditData(securityTokenObj.securityToken.UserToken, audit.dateFrom,audit.dateTo ,securityTokenObj.securityToken.sConnStr, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage,audit.sortColumns,audit.sortOrderAsc, null);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "GetFilteredAuditData")]
        public IHttpActionResult GetFilteredAuditData(AuditFilter audit)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new NaveoService.Services.AuditService().GetAuditData(securityTokenObj.securityToken.UserToken, audit.dateFrom, audit.dateTo, securityTokenObj.securityToken.sConnStr, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage, audit.sortColumns, audit.sortOrderAsc, audit.userIds,true, audit.type);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "GetDrilledAuditData")]
        public IHttpActionResult GetDrilledAuditData(AuditDrill audit)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new NaveoService.Services.AuditService().GetDrilledAuditData(audit.controller, audit.id, securityTokenObj.securityToken.sConnStr, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        #endregion

    }

}