﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.Web.Http;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.Plannings;
using NaveoOneLib.Services.Plannings;
using NaveoService.Constants;
using NaveoService.Helpers;
using NaveoService.Models;
using NaveoService.Models.DTO;
using NaveoService.Services;
using NaveoWebApi.Helpers;
using System.Data;
using static NaveoService.Services.ScheduleService;
using Microsoft.ApplicationInsights.Extensibility.Implementation;
using Newtonsoft.Json;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class ScheduleRequestController : ApiBaseController
    {
        [System.Web.Http.HttpPost, ResponseType(typeof(PlanningDTO))]
        [System.Web.Http.Route(Headers.API_PREFIX + "GetScheduleRequest")]
        public IHttpActionResult GetScheduleRequest(NaveoService.Models.Planning p)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new NaveoService.Services.ScheduleService().GetPlanningScheduledRequest(securityTokenObj.securityToken.UserToken, new List<int>(), p.dateFrom, p.dateTo, securityTokenObj.securityToken.sConnStr, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage, p.sortColumns, p.sortOrderAsc);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                throw e;
                //return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [System.Web.Http.HttpPost, ResponseType(typeof(PlanningDTO))]
        [System.Web.Http.Route(Headers.API_PREFIX + "GetScheduleRequestByscheduledID")]
        public IHttpActionResult GetScheduleRequestByHid(NaveoService.Models.Planning p)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    Scheduled sch = new NaveoService.Services.ScheduleService().GetScheduledByHID(p.scheduledID, securityTokenObj.securityToken.sConnStr);
                    return Ok(new ObjectDTO
                    {
                        data = sch
                    }
                    );
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                throw e;
                //return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        [System.Web.Http.HttpPost, ResponseType(typeof(PlanningDTO))]
        [System.Web.Http.Route(Headers.API_PREFIX + "GetRosterDataForScheduling")]
        public IHttpActionResult GetScheduleRequest(NaveoService.Models.Roster r)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new NaveoService.Services.ScheduleService().GetRosterDataForScheduling(r.dateFrom, r.dateTo, securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage, r.sortColumns, r.sortOrderAsc);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                throw e;
                //return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [System.Web.Http.HttpPost, ResponseType(typeof(PlanningDTO))]
        [System.Web.Http.Route(Headers.API_PREFIX + "GetScheduledTrips")]
        public IHttpActionResult GetScheduledTrips(NaveoService.Models.Planning p)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new NaveoService.Services.ScheduleService().GetPlanningScheduledRequest(securityTokenObj.securityToken.UserToken, new List<int>(), p.dateFrom, p.dateTo, securityTokenObj.securityToken.sConnStr, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage, p.sortColumns, p.sortOrderAsc);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                throw e;
                //return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        [System.Web.Http.HttpPost, ResponseType(typeof(PlanningDTO))]
        [System.Web.Http.Route(Headers.API_PREFIX + "GetPlannerReport")]
        public IHttpActionResult GetPlannerReport(NaveoService.Models.Planning p)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new NaveoService.Services.ScheduleService().GetPlanningScheduledRequest(securityTokenObj.securityToken.UserToken,p.assetIds, p.dateFrom, p.dateTo, securityTokenObj.securityToken.sConnStr, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage, p.sortColumns, p.sortOrderAsc);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                throw e;
                //return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [System.Web.Http.HttpPost, ResponseType(typeof(PlanningDTO))]
        [System.Web.Http.Route(Headers.API_PREFIX + "GetScheduledViaPoints")]
        public IHttpActionResult GetScheduledViaPoints(NaveoService.Models.Planning p)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new NaveoService.Services.ScheduleService().GetScheduledViaPoints(p.PID.Value, securityTokenObj.securityToken.sConnStr);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                throw e;
                //return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        [System.Web.Http.AllowAnonymous, System.Web.Http.HttpPost, ResponseType(typeof(SaveScheduledRouting))]
        [System.Web.Http.Route(Headers.API_PREFIX + "GetPublicScheduledViaPoints")]
        public IHttpActionResult GetPublicScheduledViaPoints(NaveoService.Models.Planning p)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                BaseDTO mydtData = new NaveoService.Services.ScheduleService().GetPublicScheduledViaPoints(p.ParentRequestCode, securityTokenObj.securityToken);
                return Ok(mydtData);
            }
            catch (Exception e)
            {
                throw e;
                //return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        [System.Web.Http.HttpPost, ResponseType(typeof(PutDTO))]
        [System.Web.Http.Route(Headers.API_PREFIX + "SaveScheduled")]
        public IHttpActionResult SavePlanning(PutDTO ScheduledToSave)
        {
            #region save/Update Body
            /* Body 
             
                   {
	"data": {
    "assets": [{
    "assetId":0,
    "assetNumber":"Test1",
    "status_B":"AC",
    "createdDate":"0001-01-01T00:00:00",
    "updatedDate":"0001-01-01T00:00:00",
    "timeZoneId":"Arabian Standard Time",
    "timeZoneTs":"04:00:00",
    "oprType":7,
    "YearManufactured":0,
    "PurchasedDate":"1900-01-01T00:00:00",
    "PurchasedValue":0.0,
    "RefA":"Test123",
    "RefB":"Test123",
    "RefC":"Test12",
    "ExternalRef":"1111123",
    "EngineSerialNumber":"2",
    "EngineCapacity":10,
    "EnginePower":12523,
    "createdUser":null,
    "updatedUser":null,
    "assetType":{
      "id":1,
      "description":"**Default**"
    },
    "make":{
      "id":23,
      "description":"Honda"
    },
    "model":{
      "id":47,
      "description":"Test"
    },
    "color":{
      "id":17,
      "description":"Red"
    },
    "vehicleType":{
      "id":3,
      "description":"TRUCK"
    },
    "assetDeviceMap":{
      "mapId":0,
      "assetId":0,
      "deviceId":"29",
      "serialNumber":"",
      "status_B":"AC",
      "isFuel":true,
      "oprType":4,
      "deviceAuxilliaryMap":
        {
          "uId":0,
          "mapId":0,
          "deviceId":"29",
          "fuelProduct":{
            "id":43,
            "description":"Gasoline"
          },
          "thresholdHigh":100,
          "thresholdLow":0,
          "tankCapacity":0,
          "oprType":4,
          "lCalibration":[
            {
              "iId":0,
              "assetId":0,
              "deviceId":"29",
              "auxId":13,
              "StdConsumption":1.0,
              "readingUOM":{
                "id":2,
                "description":"%"
              },
              "reading":100.0,
              "convertedUOM":{
                "id":5,
                "description":"Liters"
              },
              "convertedValue":80.0,
              "oprType":4
            },
            {
              "iId":0,
              "assetId":0,
              "deviceId":"29",
              "auxId":13,
              "StdConsumption":2.0,
              "readingUOM":{
                "id":2,
                "description":"%"
              },
              "reading":100.0,
              "convertedUOM":{
                "id":5,
                "description":"Liters"
              },
              "convertedValue":0.0,
              "oprType":4
            }]
        }
    },
    "lMatrix":[
      {
        "oprType":4,
        "mId":0,
        "iId":0,
        "gMId":4,
        "gMDesc":"Naveo"
      }],
    "lUOM":[
      {
        "iId":0,
        "assetId":0,
        "uom":{
                "id":2,
                "description":"%"
              },
        "capacity":100,
        "oprType":4
      
      }]
        }]
	},

	"oneTimeToken": "00000000-0000-0000-0000-000000000000",
	"screenMode": "Add"
}

             */

            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    Scheduled jsonSaveObject = serializer.Deserialize<Scheduled>(ScheduledToSave.data.ToString());
                    Boolean ScheduledSaveUpdate = new ScheduleService().SaveUpdateScheduled(securityTokenObj, jsonSaveObject, true);
                    if (ScheduledSaveUpdate)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "ScheduleRequest";
                        string action = "SaveScheduleRequest";
                        string rawRequest = ScheduledToSave.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, Convert.ToInt32(jsonSaveObject.AssetSelectionID), securityTokenObj.securityToken.sConnStr, true);
                    }
                    //CommonHelper.DefaultData(ScheduledSaveUpdate, securityTokenObj);
                    return Ok(ScheduledSaveUpdate.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                throw e;
                //return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [System.Web.Http.HttpPost, ResponseType(typeof(PutDTO))]
        [System.Web.Http.Route(Headers.API_PREFIX + "UpdateScheduled")]
        public IHttpActionResult UpdatePlanning(PutDTO ScheduledToSave)
        {
            #region save/Update Body

            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    Scheduled jsonSaveObject = serializer.Deserialize<Scheduled>(ScheduledToSave.data.ToString());
                    Boolean ScheduledSaveUpdate = new ScheduleService().SaveUpdateScheduled(securityTokenObj, jsonSaveObject, false);
                    if (ScheduledSaveUpdate)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "ScheduleRequest";
                        string action = "UpdateScheduleRequest";
                        string rawRequest = ScheduledToSave.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, Convert.ToInt32(jsonSaveObject.AssetSelectionID), securityTokenObj.securityToken.sConnStr, true);
                    }
                    //CommonHelper.DefaultData(ScheduledSaveUpdate, securityTokenObj);
                    return Ok(ScheduledSaveUpdate.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                throw e;
                //return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [System.Web.Http.HttpPost, ResponseType(typeof(BaseDTO))]
        [System.Web.Http.Route(Headers.API_PREFIX + "getPlanningUserEmail")]
        public IHttpActionResult GetPlanningUserEmail(GetPlanningUserEmailApi emailToSend)
        {

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    Boolean ScheduledSaveUpdate = new ScheduleService().sendEmailToClient(emailToSend, securityTokenObj);
                    if (ScheduledSaveUpdate)
                    {

                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "ScheduleRequest";
                        string action = "getPlanningUserEmail";
                        string rawRequest = JsonConvert.SerializeObject(emailToSend);
                        Telemetry.Trace(userToken, controller, action, rawRequest, 0, securityTokenObj.securityToken.sConnStr, true);
                    }
                    //CommonHelper.DefaultData(ScheduledSaveUpdate, securityTokenObj);
                    return Ok(ScheduledSaveUpdate.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                throw e;
                //return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [System.Web.Http.HttpPost, ResponseType(typeof(BaseDTO))]
        [System.Web.Http.Route(Headers.API_PREFIX + "GetMaintList")]
        public IHttpActionResult GetMaintList(NaveoService.Models.Planning p)
        {
            #region save/Update Body
            

            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {

                    BaseModel ScheduledGetMaintList = new ScheduleService().GetMaintenanceStatus(p.dateFrom, p.dateTo, p.assetdIds, securityTokenObj.securityToken.sConnStr);
                    
                    //CommonHelper.DefaultData(ScheduledSaveUpdate, securityTokenObj);
                    return Ok(ScheduledGetMaintList.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [System.Web.Http.HttpPost, ResponseType(typeof(PlanningDTO))]
        [System.Web.Http.Route(Headers.API_PREFIX + "UnScheduled")]
        public IHttpActionResult UnScheduled(NaveoService.Models.Planning p)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    Boolean mydtData = new NaveoService.Services.ScheduleService().UnScheduled(p.scheduledID.ToString(), securityTokenObj);
                    if (mydtData)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "ScheduleRequest";
                        string action = "UnSchedule";
                        string rawRequest = "{'scheduledID':'" + p.scheduledID.ToString() + "'}";
                        Telemetry.Trace(userToken, controller, action, rawRequest,  p.scheduledID, securityTokenObj.securityToken.sConnStr, true);
                    }
                    //CommonHelper.DefaultData(mydtData, securityTokenObj);
                    return Ok(mydtData.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                throw e;
                //return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        [System.Web.Http.HttpPost, ResponseType(typeof(PlanningDTO))]
        [System.Web.Http.Route(Headers.API_PREFIX + "GetAssetDriver")]
        public IHttpActionResult GetAssetDriver(NaveoService.Models.Planning p)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new NaveoService.Services.ScheduleService().GetDriverAsset(securityTokenObj.securityToken.UserToken, p.dateFrom, p.dateTo, securityTokenObj.securityToken.sConnStr);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                throw e;
                //return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        [System.Web.Http.HttpPost, ResponseType(typeof(PlanningDTO))]
        [System.Web.Http.Route(Headers.API_PREFIX + "GetAssetDriverAssg")]
        public IHttpActionResult GetAssetDriverAssg(NaveoService.Models.Planning p)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new NaveoOneLib.Services.Plannings.ScheduledService().GetDriverAssetAssg(p.dateFrom, p.dateTo, securityTokenObj.securityToken.sConnStr);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                throw e;
                //return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        [System.Web.Http.HttpPost, ResponseType(typeof(PlanningDTO))]
        [System.Web.Http.Route(Headers.API_PREFIX + "GetDriverHomeZone")]
        public IHttpActionResult GetDriverHomeZone(NaveoService.Models.Planning p)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                { 
               
                    dtData mydtData = new NaveoService.Services.ScheduleService().GetNearestDriverVech(securityTokenObj.securityToken.UserToken,p.fieldCode, securityTokenObj.securityToken.sConnStr);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


    }
}