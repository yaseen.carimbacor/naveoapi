﻿using System;
using NaveoService.Services;
using NaveoService.Utility;
using NaveoWebApi.Helpers;
using System.Collections.Generic;
using NaveoOneLib.Models;
using System.Web.Security;
using System.Web;
using System.Linq;
using Newtonsoft.Json.Linq;
using System.Data;
using Newtonsoft.Json;
using System.Web.Http;
using Microsoft.Web.Http;
using System.Web.Http.Description;
using NaveoWebApi.Constants;
using NaveoWebApi.Controllers;
using NaveoService.Models.DTO;
using NaveoService.Helpers;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.GMatrix;
using NaveoService.Models;
using NaveoOneLib.Models.Permissions;
using System.Web.Script.Serialization;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class MatrixController : ApiBaseController
    {
        #region API Methods
        [AllowAnonymous, HttpGet, ResponseType(typeof(NaveoService.Models.Object))]
        [Route(Headers.API_PREFIX + "GetModules")]
        public IHttpActionResult GetModules()
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    DataTable dt = new GroupMatrixService().GetNaveoModules(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr);
                    dtData mydtData = new dtData();
                    mydtData.Data = dt;
                    mydtData.Rowcnt = dt.Rows.Count;

                    return base.Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        //[HttpPost, ResponseType(typeof(ObjectDTO))]
        //[Route(Headers.API_PREFIX + "GetMatrix")]
        //public IHttpActionResult GetAssets()
        //{
        //    SecurityTokenExtended securityTokenObj = ApiAuthentication();
        //    try
        //    {
        //        if (securityTokenObj.securityToken.IsAuthorized)
        //        {
        //            //dtData mydtData = new NaveoService.Services.AssetService().GetAssetData(securityTokenObj.securityToken.UserToken, t.assetIds, securityTokenObj.securityToken.sConnStr, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage);
        //            //return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
        //        }
        //        return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
        //    }
        //    catch (Exception e)
        //    {
        //        return Ok(SecurityHelper.ExceptionMessage(e));
        //    }
        //}

        [HttpPost, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "GetMatrix")]
        public IHttpActionResult GetMatrix(GMatrix Gm)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new NaveoService.Services.GroupMatrixService().GetMatrixData(securityTokenObj.securityToken.UserToken,Gm.gmIds, securityTokenObj.securityToken.sConnStr, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [AllowAnonymous, HttpPost, ResponseType(typeof(NaveoService.Models.Object))]
        [Route(Headers.API_PREFIX + "GetMatrixById")]
        public IHttpActionResult GetGroupMatrixById(NaveoService.Models.DTO.Matrix matrix)
        {
            /*Body 
         {
          "gmId":51
          }
         */

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    GroupMatrix gMatrix = new GroupMatrixService().GetGroupMatrixById(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr, matrix.gmId);
                    return Ok(gMatrix.ToMatrixDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        /// <summary>
        /// Creates a group matrix
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        [AllowAnonymous, HttpPost, ResponseType(typeof(ObjectDTO))]
        [Route(Headers.API_PREFIX + "CreateMatrix")]
        public IHttpActionResult Create(PutDTO groupMatrix)
        {
            #region Body
            /*
            {
	    "data": {
		"groupMatrix": {
			"isCompany": null, 
			"gmID":1021 ,
			"parentGMID": 3,
			"gmDescription": "TestOneTwo",
			"remarks": "Rem",
			"companyCode": null,
            "legalName": null,
           "lNaveoModules": [{
		                "iID": 4,
		                "description": null,
		                "MID": 3,
		                "OPRType": 2,
		                "lPermissions": null,
		                "lMatrix": null
	             }],
            "oprType": 16
            
		}
	},
  	"oneTimeToken": "00000000-0000-0000-0000-000000000000",
	"screenMode": "Edit"
           }

            header
            Content-Type:application/json
            HOST_URL:Demo.Naveo.mu
            USER_TOKEN:A9D5ED8D-AD38-442F-898E-F351DAFCC1CB
            //ONE_TIME_TOKEN:54fd9d15-0644-4172-bc8a-01ea6430c9d5                     
            */

            #endregion
            try
            {
                //GroupMatrixViewModel v = new GroupMatrixViewModel();
                //NaveoModule i = new NaveoModule();
                //i.iID = 8;
                //v.lNaveoModules = new List<NaveoModule>();
                //v.lNaveoModules.Add(i);
                //String f = JsonConvert.SerializeObject(v);

                //Check if all parameters are available. If not, return error message
                String sError = String.Empty;
                if (groupMatrix == null)
                    sError = "Missing parameters 0";
             
                if (!String.IsNullOrEmpty(sError))
                    return Content(System.Net.HttpStatusCode.Forbidden, sError);

                SecurityTokenExtended securityTokenObj = ApiAuthentication();
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    MatrixDTO jsonGMObject = serializer.Deserialize<MatrixDTO>(groupMatrix.data.ToString());
                    BaseDTO createResult = new GroupMatrixService().SaveGroupMatrix(securityTokenObj, jsonGMObject.groupMatrix,true);
                    if (createResult.sQryResult == "Succeeded")
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Matrix";
                        string action = "SaveMatrix";
                        string rawRequest = groupMatrix.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonGMObject.groupMatrix.gmID, securityTokenObj.securityToken.sConnStr, true);
                    }
                    CommonHelper.DefaultData(createResult, securityTokenObj);
                    return Ok(createResult);
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [AllowAnonymous, HttpPost, ResponseType(typeof(MatrixDTO))]
        [Route(Headers.API_PREFIX + "UpdateMatrix")]
        public IHttpActionResult Update(PutDTO groupMatrix)
        {
            try
            {
                String sError = String.Empty;
                if (groupMatrix == null)
                    sError = "Missing parameters 0";
             

                if (!String.IsNullOrEmpty(sError))
                    return Content(System.Net.HttpStatusCode.Forbidden, sError);

                SecurityTokenExtended securityTokenObj = ApiAuthentication();
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    MatrixDTO jsonGMObject = serializer.Deserialize<MatrixDTO>(groupMatrix.data.ToString());
                    BaseDTO updateResult = new GroupMatrixService().SaveGroupMatrix(securityTokenObj, jsonGMObject.groupMatrix, false);
                    if (updateResult.sQryResult == "Succeeded")
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Matrix";
                        string action = "UpdateMatrix";
                        string rawRequest = groupMatrix.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonGMObject.groupMatrix.gmID, securityTokenObj.securityToken.sConnStr, true);
                    }
                    CommonHelper.DefaultData(updateResult, securityTokenObj);
                    return Ok(updateResult);
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        /// <summary>
        /// Delete the specified group matrix
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AllowAnonymous, HttpPost, ResponseType(typeof(bool))]
        [Route(Headers.API_PREFIX + "Delete")]
        public IHttpActionResult Delete(int id)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseDTO deleteResult = new GroupMatrixService().Delete(new GroupMatrix(), CommonHelper.GetCurrentConnStr());
                    if (deleteResult.data)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Matrix";
                        string action = "Delete";
                        string rawRequest = "";
                        Telemetry.Trace(userToken, controller, action, rawRequest, -1, securityTokenObj.securityToken.sConnStr, true);
                    }
                    CommonHelper.DefaultData(deleteResult, securityTokenObj);
                    return Ok(deleteResult);
                }
                 return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        #endregion

        [AllowAnonymous, HttpGet, ResponseType(typeof(NaveoService.Models.Object))]
        [Route(Headers.API_PREFIX + "GetMatrixLegalNameByGMID")]
        public IHttpActionResult GetMatrixLegalNameByGMID()
        {
            /*Body 
         {
          "gmId":51
          }
         */

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new GroupMatrixService().GetMatrixLegalName(securityTokenObj.securityToken.UserToken,securityTokenObj.securityToken.sConnStr);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }



    }
}
