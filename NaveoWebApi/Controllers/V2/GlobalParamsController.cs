﻿using Microsoft.Web.Http;
using NaveoWebApi.Constants;
using NaveoWebApi.Helpers;
using System;
using System.Web.Http;
using System.Web.Http.Description;
using NaveoService.Models.DTO;
using NaveoService.Models;
using System.Data;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class GlobalParamsController : ApiBaseController
    {
        #region API Methods

        [HttpPost, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "GetGlobalParams")]
        public IHttpActionResult GetGlobalParams()
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    NaveoService.Services.GlobalParamsService service = new NaveoService.Services.GlobalParamsService();
                    DataTable dataTable = service.GetGlobalParams(securityTokenObj.securityToken.sConnStr);
                    return Ok(AllParamsToBaseDTO(dataTable, securityTokenObj.securityToken.OneTimeToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "UpdateGlobalParam")]
        public IHttpActionResult UpdateGlobalParam(apiGlobalParams paramToUpdate)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    NaveoService.Services.GlobalParamsService service = new NaveoService.Services.GlobalParamsService();
                    Boolean result = service.UpdateGlobalParams(paramToUpdate.iid, paramToUpdate.PValue, securityTokenObj.securityToken.sConnStr);
                    if (result)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "GlobalParams";
                        string action = "UpdateGlobalParam";
                        string rawRequest = "{'Iid':" + paramToUpdate.iid + "','PValue':'" + paramToUpdate.PValue +"'}";
                        Telemetry.Trace(userToken, controller, action, rawRequest, Convert.ToInt32(paramToUpdate.iid), securityTokenObj.securityToken.sConnStr, true);
                    }
                    return Ok(UpdateParamsToBaseDTO(result, securityTokenObj.securityToken.OneTimeToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
            return Ok(SecurityHelper.ExceptionMessage(null));
        }

       /* [HttpPost, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "UpdateGlobalParams")]
        public IHttpActionResult UpdateGlobalParams()
        {
            //SecurityTokenExtended securityTokenObj = ApiAuthentication();
            /*try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    NaveoService.Services.GlobalParamsService service = new NaveoService.Services.GlobalParamsService();
                    Boolean result = service.UpdateGlobalParams(paramToUpdate.iid, paramToUpdate.PValue, securityTokenObj.securityToken.sConnStr);
                    return Ok(UpdateParamsToBaseDTO(result, securityTokenObj.securityToken.OneTimeToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
            return Ok(SecurityHelper.ExceptionMessage(null));
        }*/

            #endregion


            #region Private Methods
           private BaseDTO AllParamsToBaseDTO(DataTable data, Guid oneTimeToken) {
            BaseDTO dto = new BaseDTO();
            dto.data = data;
            dto.oneTimeToken = oneTimeToken;
            return dto;
        }
        private BaseDTO UpdateParamsToBaseDTO(Boolean data, Guid oneTimeToken)
        {
            BaseDTO dto = new BaseDTO();
            dto.data = data;
            dto.oneTimeToken = oneTimeToken;
            return dto;
        }



        #endregion
    }
}