﻿using Microsoft.Web.Http;
using NaveoOneLib.Models.Common;
using NaveoService.Constants;
using NaveoService.Helpers;
using NaveoService.Models;
using NaveoService.Models.DTO;
using NaveoService.Services;
using NaveoWebApi.Helpers;
using NaveoWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using HttpPostAttribute = System.Web.Http.HttpPostAttribute;
using RouteAttribute = System.Web.Http.RouteAttribute;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class RulesController : ApiBaseController
    {
        #region API methods
        [HttpPost, ResponseType(typeof(RuleDTO))]
        [Route(Headers.API_PREFIX + "GetRules")]
        public IHttpActionResult GetRules(apiRules r)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new NaveoService.Services.RuleService().GetRules(securityTokenObj.securityToken.UserToken,r.ruleCategory,securityTokenObj.securityToken.sConnStr, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(RuleDTO))]
        [Route(Headers.API_PREFIX + "GetRulesById")]
        public IHttpActionResult GetRulesById(Rule r)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel mydtData = new NaveoService.Services.RuleService().GetRulesByParentRuleId(securityTokenObj.securityToken.UserToken, r.parentRuleId, securityTokenObj.securityToken.sConnStr);
                    return Ok(mydtData.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        /// <summary>
        /// Header example
        /// HOST_URL: mercury.naveo.mu
        /// USER_TOKEN: B8F03A2A-56A5-4501-9323-3BACFC35BE49
        /// [ONE_TIME_TOKEN]: 0c288a26-aa02-4f34-baff-078a9cab1ca7
        /// </summary>
        /// <returns></returns>
        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "SaveRule")]
        public IHttpActionResult SaveRule(PutDTO ruleToSave)
        {
            #region Body

            //            {
            //                "data": {
            //                    "rule": {
            //                        "ruleId":2343,
            //  "ruleName": "API TEST RULE",
            //  "ruleCategory": 1,
            //  "oprType": 0,       
            //  "zoneRules": {
            //                            "selectionType": "Zones",
            //    "selectionCriteria": "Entering Zone",
            //    "lTreeSelections": {
            //                                "group": "Zones",
            //      "lGroup": [
            //        {
            //          "type": "Child",
            //          "id": 3,
            //          "oprType": 4
            //        },

            //        {
            //          "type": "Child",
            //          "id": 4,
            //          "oprType": 4
            //        }

            //      ]
            //    },
            //    "oprType": 0

            //  },
            //  "assetDriverRules": {
            //   "selectionType": "Assets",
            //    "lTreeSelections": {
            //      "group": "Assets",
            //      "lGroup": [
            //        {
            //          "type": "Parent",
            //          "id": 3,
            //          "oprType": 4
            //        }
            //      ],
            //      "oprType": 4
            //    },
            //    "oprType": 0
            //  },
            //  "ruleConditions": [
            //    {
            //      "ruleType": 4,
            //      "struc": "Speed",
            //      "strucValue":"0",
            //      "strucCondition": "=",
            //      "strucType": "Stopped",
            //      "oprType": 0

            //    },
            //    {
            //      "ruleType": 5,
            //      "struc": "Time",
            //      "strucValue": "3200",
            //      "strucCondition": ">=",
            //      "strucType": "",
            //      "oprType": 4

            //    },
            //    {
            //      "ruleType": 5,
            //      "struc": "Time",
            //      "strucValue": "3200000",
            //      "strucCondition": "<=",
            //      "strucType": "",
            //      "oprType": 4

            //    }
            //  ],
            //  "notificationLists": [
            //    {
            //      "arID": 2188,
            //      "targetID": 2343,
            //      "notificationType": 110,
            //      "oprType": 16,
            //      "lDetails": [
            //        {
            //          "ardID": 2280,
            //          "arID": 0,
            //          "type": "Users",
            //          "userId": 12,
            //          "category": "Child",
            //          "oprType": 16
            //        }
            //      ]
            //    },

            //    {
            //      "arID": 2189,
            //      "targetID": 2343,
            //      "notificationType": 111,
            //      "oprType": 16,
            //      "lDetails": [
            //        {
            //          "ardID": 2281,
            //          "arID": 0,
            //          "type": "Users",
            //          "userId": 13,
            //          "category": "Child",
            //          "oprType": 16
            //        }
            //      ]
            //    }
            //  ]
            //		},
            //		"oneTimeToken": "79087e34-81ea-4e87-b8ff-17fbcb0f1cf0",
            //		"screenMode": "Add"
            //	}
            //}
            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    RuleDTO jsonSaveObject = serializer.Deserialize<RuleDTO>(ruleToSave.data.ToString());
                    Boolean RuleSaveUpdate = new RuleService().SaveUpdateRule(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr, jsonSaveObject, true);
                    if (RuleSaveUpdate)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Rules";
                        string action = "SaveRules";
                        string rawRequest = ruleToSave.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonSaveObject.rule.ruleId, securityTokenObj.securityToken.sConnStr, true);
                    }
                    //CommonHelper.DefaultData(RuleSaveUpdate, securityTokenObj);
                    return Ok(RuleSaveUpdate.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "UpdateRule")]
        public IHttpActionResult UpdateRule(PutDTO ruleToSave)
        {

            #region Body

            //            {
            //                "data": {
            //                    "rule": {
            //                        "ruleId":2343,
            //  "ruleName": "API TEST RULE",
            //  "ruleCategory": 1,
            //  "oprType": 0,       
            //  "zoneRules": {
            //                            "selectionType": "Zones",
            //    "selectionCriteria": "Entering Zone",
            //    "lTreeSelections": {
            //                                "group": "Zones",
            //      "lGroup": [
            //        {
            //          "type": "Child",
            //          "id": 3,
            //          "oprType": 4
            //        },

            //        {
            //          "type": "Child",
            //          "id": 4,
            //          "oprType": 4
            //        }

            //      ]
            //    },
            //    "oprType": 0

            //  },
            //  "assetDriverRules": {
            //   "selectionType": "Assets",
            //    "lTreeSelections": {
            //      "group": "Assets",
            //      "lGroup": [
            //        {
            //          "type": "Parent",
            //          "id": 3,
            //          "oprType": 4
            //        }
            //      ],
            //      "oprType": 4
            //    },
            //    "oprType": 0
            //  },
            //  "ruleConditions": [
            //    {
            //      "ruleType": 4,
            //      "struc": "Speed",
            //      "strucValue":"0",
            //      "strucCondition": "=",
            //      "strucType": "Stopped",
            //      "oprType": 0

            //    },
            //    {
            //      "ruleType": 5,
            //      "struc": "Time",
            //      "strucValue": "3200",
            //      "strucCondition": ">=",
            //      "strucType": "",
            //      "oprType": 4

            //    },
            //    {
            //      "ruleType": 5,
            //      "struc": "Time",
            //      "strucValue": "3200000",
            //      "strucCondition": "<=",
            //      "strucType": "",
            //      "oprType": 4

            //    }
            //  ],
            //  "notificationLists": [
            //    {
            //      "arID": 2188,
            //      "targetID": 2343,
            //      "notificationType": 110,
            //      "oprType": 16,
            //      "lDetails": [
            //        {
            //          "ardID": 2280,
            //          "arID": 0,
            //          "type": "Users",
            //          "userId": 12,
            //          "category": "Child",
            //          "oprType": 16
            //        }
            //      ]
            //    },

            //    {
            //      "arID": 2189,
            //      "targetID": 2343,
            //      "notificationType": 111,
            //      "oprType": 16,
            //      "lDetails": [
            //        {
            //          "ardID": 2281,
            //          "arID": 0,
            //          "type": "Users",
            //          "userId": 13,
            //          "category": "Child",
            //          "oprType": 16
            //        }
            //      ]
            //    }
            //  ]
            //		},
            //		"oneTimeToken": "79087e34-81ea-4e87-b8ff-17fbcb0f1cf0",
            //		"screenMode": "Add"
            //	}
            //}
            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    RuleDTO jsonSaveObject = serializer.Deserialize<RuleDTO>(ruleToSave.data.ToString());
                    Boolean RuleSaveUpdate = new RuleService().SaveUpdateRule(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr, jsonSaveObject, false);
                    if (RuleSaveUpdate)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Rules";
                        string action = "SaveRules";
                        string rawRequest = ruleToSave.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonSaveObject.rule.ruleId, securityTokenObj.securityToken.sConnStr, true);
                    }
                    //CommonHelper.DefaultData(RuleSaveUpdate, securityTokenObj);
                    return Ok(RuleSaveUpdate.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "GetRuleTemplates")]
        public IHttpActionResult GetRuleTemplates(Rule r)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {

                    BaseDTO RuleTemplate = new RuleService().getRuleTemplate(r.category);
                    //CommonHelper.DefaultData(RuleSaveUpdate, securityTokenObj);
                    return Ok(RuleTemplate);
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }


        }
        #endregion

        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "DeleteRules")]
        public IHttpActionResult DeleteRules(Rule r)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {

                    Boolean obj = new RuleService().Delete(securityTokenObj.securityToken.UserToken, r.parentRuleId, r.ruleName, securityTokenObj.securityToken.sConnStr);
                    if (obj)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Rules";
                        string action = "SaveRules";
                        string rawRequest = "{'parentRuleID':'" + r.parentRuleId + "'}";
                        Telemetry.Trace(userToken, controller, action, rawRequest, Convert.ToInt32(r.parentRuleId), securityTokenObj.securityToken.sConnStr, true);
                    }
                    return Ok(obj.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        #region Fuel Methods
        /// <summary> 
        /// Get trip details
        /// <para>BODY EXAMPLE</para>
        /// <para>{</para>
        ///   <para>"assetIds": [25] </para>
        ///   <para>"dateFrom": "2017-10-05 01:01:01"</para>
        ///   <para>"dateTo": "2017-10-05 18:20:20" </para>
        /// <para>}</para>
        /// <para>HEADERS EXAMPLE</para>
        /// <para>HOST_URL: mercury.naveo.mu</para>
        /// <para>USER_TOKEN: B8F03A2A-56A5-4501-9323-3BACFC35BE49</para> 
        /// <para>[ONE_TIME_TOKEN]: A8F453S2A-24C5-4320-4NAQWE3AK22</para> 
        /// <para>PAGE: 1</para>
        /// <para>ROWS_PER_PAGE: 5</para>
        /// </summary> 
        /// <returns></returns>
        [HttpPost, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "GetFuelRule")]
        public IHttpActionResult GetFuelRule(apiRules r)
        {
            /*
            https://localhost:44393/v2.0/GetFuelRule

            Headers
            Content - Type:application / json
            USER_TOKEN: 2c5c384f - 4a54 - 4f06 - aca6 - ea0c89c32f15
            HOST_URL: demo.naveo.mu
             //ONE_TIME_TOKEN:1ade61f2-94ed-422f-86be-c1e5abe0c1ac

             Body
             {
                "fuelType": "Fill"
             }
             */

            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;
            if (r == null)
                sError = "Missing parameters 0";
            else if (r.fuelType == null)
                sError = "Missing parameters 1";

            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    NaveoService.Models.FuelRule fuelRule = new RuleService().GetFuelRule(securityTokenObj.securityToken.UserToken, r.fuelType, securityTokenObj.securityToken.sConnStr);
                    return base.Ok(fuelRule.ToFuelRuleDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "UpdateFuelRule")]
        public IHttpActionResult UpdateFuelRule(apiRules r)
        {
            /*
            https://localhost:44393/v2.0/UpdateFuelRule

            Headers
            Content - Type:application / json
            USER_TOKEN: 2c5c384f - 4a54 - 4f06 - aca6 - ea0c89c32f15
            HOST_URL: demo.naveo.mu
             //ONE_TIME_TOKEN:1ade61f2-94ed-422f-86be-c1e5abe0c1ac

             Body
             {
                "fuelType": "Fill",
                "lUsers" " [1, 2, 3]
             }
             */

            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;
            if (r == null)
                sError = "Missing parameters 0";
            else if (r.fuelType == null)
                sError = "Missing parameters 1";
            else if (r.lUsers == null)
                sError = "Missing parameters 3";

            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    Boolean fuelRule = new RuleService().UpdateFuelRule(securityTokenObj.securityToken.UserToken, r.fuelType, r.lUsers, r.notificationList, securityTokenObj.securityToken.sConnStr);
                    return base.Ok(fuelRule.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }
        #endregion

        [System.Web.Http.HttpGet, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "GetExcepDashboardCounts")]
        public IHttpActionResult GetExcepDashboardCounts()
        {
            /*
            https://localhost:44393/v2.0/GetExcepDashboardCounts

            Headers
            Content - Type:application / json
            USER_TOKEN: 2c5c384f - 4a54 - 4f06 - aca6 - ea0c89c32f15
            HOST_URL: demo.naveo.mu
             //ONE_TIME_TOKEN:1ade61f2-94ed-422f-86be-c1e5abe0c1ac

            */



            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseDTO baseDTO = new RuleService().GetExcepDashboardCounts(securityTokenObj.securityToken.UserToken,securityTokenObj.securityToken.sConnStr);
                    return base.Ok(baseDTO);
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                throw e;
               
            }
        }



    }
}