﻿using Microsoft.Web.Http;
using NaveoWebApi.Constants;
using NaveoWebApi.Helpers;
using System;
using System.Web.Http;
using System.Web.Http.Description;
using NaveoService.Models.DTO;
using NaveoWebApi.Models;
using NaveoOneLib.Models.Common;
using System.Web.Script.Serialization;
using NaveoService.Models;
using NaveoService.Helpers;


namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class DriverVehicleController : ApiBaseController
    {
        #region API Methods
        //[HttpPost, ResponseType(typeof(BaseModel))]
        //[Route(Headers.API_PREFIX + "AccidentManagement")]
        //public IHttpActionResult AccidentMng(NaveoOneLib.Models.AccidentManagement.AccidentManagementDetailDto accidentManagementDetailDto)
        //{
        //    SecurityTokenExtended securityTokenObj = ApiAuthentication();
        //    try
        //    {
        //        if (securityTokenObj.securityToken.IsAuthorized)
        //        {
        //            //JavaScriptSerializer serializer = new JavaScriptSerializer();
        //            //NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto jsonSaveObject = serializer.Deserialize<NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto>(assetMaintenanceDetailDto.ToString());
        //            BaseModel bm = new NaveoOneLib.Services.AccidentManagement.AccidentManagementServices().AccidentManagement(accidentManagementDetailDto, securityTokenObj.securityToken.sConnStr);

        //            return Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
        //        }
        //        return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
        //    }
        //    catch (Exception e)
        //    {
        //        return Ok(SecurityHelper.ExceptionMessage(e));
        //    }
        //}



        [HttpPost, ResponseType(typeof(BaseModel))]
        [Route(Headers.API_PREFIX + "SaveDriverAsset")]
        public IHttpActionResult TestNewDriverAsset(NaveoOneLib.Models.DriverAsset.DriverAssetDto driverAssetDto)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    //JavaScriptSerializer serializer = new JavaScriptSerializer();
                    //NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto jsonSaveObject = serializer.Deserialize<NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto>(assetMaintenanceDetailDto.ToString());
                    BaseModel bm = new NaveoOneLib.Services.Plannings.DriverAssetService().DriverAssetMng(driverAssetDto, securityTokenObj.securityToken.sConnStr);

                    return Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }



        [HttpPost, ResponseType(typeof(BaseModel))]
        [Route(Headers.API_PREFIX + "UpdateDriverAsset")]
        public IHttpActionResult UpdateDriverAsset(NaveoOneLib.Models.DriverAsset.DriverAssetDto driverAssetDto)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    //JavaScriptSerializer serializer = new JavaScriptSerializer();
                    //NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto jsonSaveObject = serializer.Deserialize<NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto>(assetMaintenanceDetailDto.ToString());
                    BaseModel bm = new NaveoOneLib.Services.Plannings.DriverAssetService().DriverAssetMng(driverAssetDto, securityTokenObj.securityToken.sConnStr);

                    return Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }



        [HttpPost, ResponseType(typeof(BaseModel))]
        [Route(Headers.API_PREFIX + "DeleteDriverAsset")]
        public IHttpActionResult DeleteDriverAsset(NaveoOneLib.Models.DriverAsset.DriverAssetDto driverAssetDto)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    //JavaScriptSerializer serializer = new JavaScriptSerializer();
                    //NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto jsonSaveObject = serializer.Deserialize<NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto>(assetMaintenanceDetailDto.ToString());
                    BaseModel bm = new NaveoOneLib.Services.Plannings.DriverAssetService().DriverAssetMng(driverAssetDto, securityTokenObj.securityToken.sConnStr);

                    return Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }



        [HttpPost, ResponseType(typeof(BaseModel))]
        [Route(Headers.API_PREFIX + "ListDriverAsset")]
        public IHttpActionResult ListDriverAsset(NaveoOneLib.Models.DriverAsset.lstDriverAssetDto LstdriverAssetDto)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    //JavaScriptSerializer serializer = new JavaScriptSerializer();
                    //NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto jsonSaveObject = serializer.Deserialize<NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto>(assetMaintenanceDetailDto.ToString());
                    BaseModel bm = new NaveoOneLib.Services.Plannings.DriverAssetService().ListDriverAssetMng(LstdriverAssetDto, securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr);

                    return Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

       
        #endregion
    }
}