﻿using Microsoft.Web.Http;
using NaveoService.Services;
using NaveoWebApi.Constants;
using NaveoWebApi.Helpers;
using System;
using System.Web.Http;
using System.Web.Http.Description;
using NaveoService.Models.DTO;
using NaveoWebApi.Models;
using NaveoOneLib.Models.Common;
using System.Web.Script.Serialization;
using NaveoService.Models;
using NaveoService.Helpers;
using System.Data;
using NaveoOneLib.Models.ErrorData;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class ErrorController : ApiBaseController
    {
        #region API Methods

        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "GetFilteredError")]
        public IHttpActionResult GetFilteredError(ErrorData error)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new NaveoService.Services.ErrorService().GetFilteredErrorData(securityTokenObj.securityToken.sConnStr, error, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage);

                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "UpdateErrSt")]
        public IHttpActionResult UpdateErrSt(UpdateError updateError)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    NaveoService.Services.ErrorService service = new NaveoService.Services.ErrorService();
                    Boolean result = service.UpdateErrorDataStatus(updateError.ErrorID, updateError.Status, securityTokenObj.securityToken.sConnStr);
                    if (result)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Error";
                        string action = "UpdateErrorStatus";
                        string rawRequest = "{'ErrorID':" + updateError.ErrorID + "','Status':'" + updateError.Status + "'}";
                        Telemetry.Trace(userToken, controller, action, rawRequest, Convert.ToInt32(updateError.ErrorID), securityTokenObj.securityToken.sConnStr, true);
                    }
                    return Ok(UpdateParamsToBaseDTO(result, securityTokenObj.securityToken.OneTimeToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
            return Ok(SecurityHelper.ExceptionMessage(null));
        }

        #endregion

        private BaseDTO UpdateParamsToBaseDTO(Boolean data, Guid oneTimeToken)
        {
            BaseDTO dto = new BaseDTO();
            dto.data = data;
            dto.oneTimeToken = oneTimeToken;
            return dto;
        }

    }

}