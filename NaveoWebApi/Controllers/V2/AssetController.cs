﻿using Microsoft.Web.Http;
using NaveoService.Services;
using NaveoWebApi.Constants;
using NaveoWebApi.Helpers;
using System;
using System.Web.Http;
using System.Web.Http.Description;
using NaveoService.Models.DTO;
using NaveoWebApi.Models;
using NaveoOneLib.Models.Common;
using System.Web.Script.Serialization;
using NaveoService.Models;
using NaveoService.Helpers;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class AssetController : ApiBaseController
    {
        #region API Methods
        /// <summary>
        /// Get master data
        /// <para>HEADERS EXAMPLE</para>
        /// <para>HOST_URL: mercury.naveo.mu</para>
        /// <para>USER_TOKEN: B8F03A2A-56A5-4501-9323-3BACFC35BE49</para> 
        /// <para>[ONE_TIME_TOKEN]: A8F453S2A-24C5-4320-4NAQWE3AK22</para> 
        /// <para>PAGE: 1</para>
        /// <para>ROWS_PER_PAGE: 5</para>
        /// </summary> 
        //// <param name="nm"></param>
        /// <returns></returns>
        [HttpGet, ResponseType(typeof(NaveoService.Models.Object))]
        [Route(Headers.API_PREFIX + "GetMasterData")]
        public IHttpActionResult GetObjects(NaveoModels nm)
        {
            /*
            https://Mercury.naveo.mu/api/v2.0/GetMasterData?&nm=Assets
            
             Content-Type:application/json
            USER_TOKEN:b71929b9-8d57-4003-b14b-4f14416cfc04
            //HOST_URL:baby.naveo.mu
            ONE_TIME_TOKEN:97cc98da-91f2-4228-82bd-8b42a800d4d3
            */
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData obj = new ObjectService().NaveoMasterData(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr, nm, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage);
                    return Ok(obj.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                throw e;
                //return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpGet, ResponseType(typeof(NaveoService.Models.Object))]
        [Route(Headers.API_PREFIX + "GetMaintenanceTypeStatus")]
        public IHttpActionResult GetMaintenanceObjects()
        {
            /*
            https://Mercury.naveo.mu/api/v2.0/GetMasterData?&nm=Assets
            
             Content-Type:application/json
            USER_TOKEN:b71929b9-8d57-4003-b14b-4f14416cfc04
            //HOST_URL:baby.naveo.mu
            ONE_TIME_TOKEN:97cc98da-91f2-4228-82bd-8b42a800d4d3
            */
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData obj = new ObjectService().NaveoMaintenanceData(securityTokenObj.securityToken.UserToken, "MaintDB");
                    return Ok(obj.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
                //throw e;
                //return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpGet, ResponseType(typeof(NaveoService.Models.Object))]
        [Route(Headers.API_PREFIX + "GetMaintenanceTypeStatusFAOnly")]
        public IHttpActionResult GetMaintenanceTypeStatusFAOnly()
        {
            /*
            https://Mercury.naveo.mu/api/v2.0/GetMasterData?&nm=Assets
            
             Content-Type:application/json
            USER_TOKEN:b71929b9-8d57-4003-b14b-4f14416cfc04
            //HOST_URL:baby.naveo.mu
            ONE_TIME_TOKEN:97cc98da-91f2-4228-82bd-8b42a800d4d3
            */
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData obj = new ObjectService().NaveoMaintenanceDataFAOnly(securityTokenObj.securityToken.UserToken, "MaintDB");
                    return Ok(obj.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
                //throw e;
                //return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        /// <summary> 
        /// Get objects
        /// <para>HEADERS EXAMPLE</para>
        /// <para>HOST_URL: mercury.naveo.mu</para>
        /// <para>USER_TOKEN: B8F03A2A-56A5-4501-9323-3BACFC35BE49</para> 
        /// <para>[ONE_TIME_TOKEN]: A8F453S2A-24C5-4320-4NAQWE3AK22</para> 
        /// <para>PAGE: 1</para>
        /// <para>ROWS_PER_PAGE: 5</para>
        /// </summary> 
        /// <returns></returns>
        [HttpGet, ResponseType(typeof(NaveoService.Models.Object))]
        [Route(Headers.API_PREFIX + "GetObjects")]
        public IHttpActionResult GetObjects()
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    NaveoService.Models.Object obj = new ObjectService().dsAPIObjects(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr, true);
                    return Ok(castData(obj, securityTokenObj.securityToken.OneTimeToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch(System.Exception e)
            {
                throw e;
                ///return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        /// <summary>
        /// Header example
        /// HOST_URL: mercury.naveo.mu
        /// USER_TOKEN: B8F03A2A-56A5-4501-9323-3BACFC35BE49
        /// [ONE_TIME_TOKEN]: 0c288a26-aa02-4f34-baff-078a9cab1ca7 
        /// PAGE: 1
        /// ROWS_PER_PAGE: 5
        /// </summary>
        /// <returns></returns>
        [HttpGet, ResponseType(typeof(NaveoService.Models.Object))]
        [Route(Headers.API_PREFIX + "GetObjectsUsingList")]
        public IHttpActionResult GetObjectsUsingList()
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    NaveoService.Models.Object obj = new ObjectService().dsAPIObjects(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr, false);
                    return Ok(obj.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch(Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        [HttpGet, ResponseType(typeof(NaveoService.Models.Object))]
        [Route(Headers.API_PREFIX + "GetConnStr")]
        public IHttpActionResult GetConnStr(String Token)
        {
            if (Token != "N@veo2017!")
                return Ok("Unauthorized!!! Think twice before accessing this server...");

            String s = CommonHelper.GetCurrentConnStr();
            String g = CommonHelper.tst();

            return Ok("sConnStr : " + s + "; tst : " + g);
        }

        [HttpPost, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "GetAssets")]
        public IHttpActionResult GetAssets(Trips t)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new NaveoService.Services.AssetService().GetAssetData(securityTokenObj.securityToken.UserToken, t.assetIds, securityTokenObj.securityToken.sConnStr, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "SearchAssets")]
        public IHttpActionResult SearchAssets(SearchAsset searchAsset)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    NaveoModels nm = NaveoModels.Assets;
                    if (!Enum.TryParse(searchAsset.NaveoModel, true, out nm))
                        nm = NaveoModels.Assets;

                    dtData mydtData = new NaveoService.Services.AssetService().SearchAssets(securityTokenObj.securityToken.UserToken, nm, searchAsset.SearchValue,securityTokenObj.securityToken.sConnStr);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        /// <summary>
        /// Header example
        /// HOST_URL: mercury.naveo.mu
        /// USER_TOKEN: B8F03A2A-56A5-4501-9323-3BACFC35BE49
        /// [ONE_TIME_TOKEN]: 0c288a26-aa02-4f34-baff-078a9cab1ca7 
        /// </summary>
        /// <returns></returns>
        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "GetAssetById")]
        public IHttpActionResult  GetAssetById(lAssets ld)
        {
            /*Body 
           {
            "assetId":51
            }
           */

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    apiAsset asset = new AssetService().GetAssetDataById(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr, ld.assetId);
                    return Ok(asset.ToAssetDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                throw e;
                //return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        /// <summary>
        /// Header example
        /// HOST_URL: mercury.naveo.mu
        /// USER_TOKEN: B8F03A2A-56A5-4501-9323-3BACFC35BE49
        /// [ONE_TIME_TOKEN]: 0c288a26-aa02-4f34-baff-078a9cab1ca7 
        /// </summary>
        /// <returns></returns>
        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "SaveAsset")]
        public IHttpActionResult SaveAsset(PutDTO assetToSave)
        {
            #region save/Update Body
            /* Body 
             
                   {
	"data": {
    "assets": [{
    "assetId":0,
    "assetNumber":"Test1",
    "status_B":"AC",
    "createdDate":"0001-01-01T00:00:00",
    "updatedDate":"0001-01-01T00:00:00",
    "timeZoneId":"Arabian Standard Time",
    "timeZoneTs":"04:00:00",
    "oprType":7,
    "YearManufactured":0,
    "PurchasedDate":"1900-01-01T00:00:00",
    "PurchasedValue":0.0,
    "RefA":"Test123",
    "RefB":"Test123",
    "RefC":"Test12",
    "ExternalRef":"1111123",
    "EngineSerialNumber":"2",
    "EngineCapacity":10,
    "EnginePower":12523,
    "createdUser":null,
    "updatedUser":null,
    "assetType":{
      "id":1,
      "description":"**Default**"
    },
    "make":{
      "id":23,
      "description":"Honda"
    },
    "model":{
      "id":47,
      "description":"Test"
    },
    "color":{
      "id":17,
      "description":"Red"
    },
    "vehicleType":{
      "id":3,
      "description":"TRUCK"
    },
    "assetDeviceMap":{
      "mapId":0,
      "assetId":0,
      "deviceId":"29",
      "serialNumber":"",
      "status_B":"AC",
      "isFuel":true,
      "oprType":4,
      "deviceAuxilliaryMap":
        {
          "uId":0,
          "mapId":0,
          "deviceId":"29",
          "fuelProduct":{
            "id":43,
            "description":"Gasoline"
          },
          "thresholdHigh":100,
          "thresholdLow":0,
          "tankCapacity":0,
          "oprType":4,
          "lCalibration":[
            {
              "iId":0,
              "assetId":0,
              "deviceId":"29",
              "auxId":13,
              "StdConsumption":1.0,
              "readingUOM":{
                "id":2,
                "description":"%"
              },
              "reading":100.0,
              "convertedUOM":{
                "id":5,
                "description":"Liters"
              },
              "convertedValue":80.0,
              "oprType":4
            },
            {
              "iId":0,
              "assetId":0,
              "deviceId":"29",
              "auxId":13,
              "StdConsumption":2.0,
              "readingUOM":{
                "id":2,
                "description":"%"
              },
              "reading":100.0,
              "convertedUOM":{
                "id":5,
                "description":"Liters"
              },
              "convertedValue":0.0,
              "oprType":4
            }]
        }
    },
    "lMatrix":[
      {
        "oprType":4,
        "mId":0,
        "iId":0,
        "gMId":4,
        "gMDesc":"Naveo"
      }],
    "lUOM":[
      {
        "iId":0,
        "assetId":0,
        "uom":{
                "id":2,
                "description":"%"
              },
        "capacity":100,
        "oprType":4
      
      }]
        }]
	},

	"oneTimeToken": "00000000-0000-0000-0000-000000000000",
	"screenMode": "Add"
}

             */

            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    AssetDTO jsonSaveObject = serializer.Deserialize<AssetDTO>(assetToSave.data.ToString());
                    BaseDTO AssetSaveUpdate = new AssetService().SaveUpdateAsset(securityTokenObj, jsonSaveObject, true);
                    if (AssetSaveUpdate.sQryResult == "Succeeded")
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Asset";
                        string action = "SaveAsset";
                        string rawRequest = assetToSave.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonSaveObject.assets[0].assetId,securityTokenObj.securityToken.sConnStr, true);
                    }
                    CommonHelper.DefaultData(AssetSaveUpdate, securityTokenObj);
                    return Ok(AssetSaveUpdate);
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                throw e;
               // return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "UpdateAsset")]
        public IHttpActionResult UpdateAsset(PutDTO assetToSave)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    AssetDTO jsonSaveObject = serializer.Deserialize<AssetDTO>(assetToSave.data.ToString());
                    BaseDTO AssetSaveUpdate = new AssetService().SaveUpdateAsset(securityTokenObj, jsonSaveObject, false);
                    if (AssetSaveUpdate.sQryResult == "Succeeded")
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Asset";
                        string action = "UpdateAsset";
                        string rawRequest = assetToSave.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonSaveObject.assets[0].assetId, securityTokenObj.securityToken.sConnStr, true);
                    }
                    CommonHelper.DefaultData(AssetSaveUpdate, securityTokenObj);
                    return Ok(AssetSaveUpdate);
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                throw e;
                ///return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        /// <summary>
        /// Header example
        /// HOST_URL: mercury.naveo.mu
        /// USER_TOKEN: B8F03A2A-56A5-4501-9323-3BACFC35BE49
        /// [ONE_TIME_TOKEN]: 0c288a26-aa02-4f34-baff-078a9cab1ca7 
        /// </summary>
        /// <returns></returns>
        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "DeleteAsset")]
        public IHttpActionResult DeleteAsset(lAssets asset)
        {
            /*Body 
             {
              "assetId":51
              }
             */

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {

                    BaseModel deleteAsset = new AssetService().DeleteAsset(asset.assetId, securityTokenObj);
                    if (deleteAsset.data)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Asset";
                        string action = "DeleteAsset";
                        string rawRequest = "{'assetId':'" + asset.assetId + "'}";
                        Telemetry.Trace(userToken, controller, action, rawRequest, asset.assetId, securityTokenObj.securityToken.sConnStr, true);
                    }
                    return base.Ok(deleteAsset.ToNaveoOneLibBaseModelDTO(securityTokenObj.securityToken.OneTimeToken));
                    

                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }
        #endregion

        #region Garage
        [HttpPost, ResponseType(typeof(BaseModel))]
        [Route(Headers.API_PREFIX + "GetAssetMaintDetailsByAssetId")]
        public IHttpActionResult GetAssetMaintDetailsByAssetId(AssetGarage assetGarage)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel bm = new NaveoService.Services.AssetService().GetAssetMaintDetailsByAssetId(securityTokenObj, assetGarage.AssetID,assetGarage.DateTimeStart,assetGarage.DateTimeEnd,assetGarage.Odometer,assetGarage.EngineHours);
                    return Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpGet, ResponseType(typeof(BaseModel))]
        [Route(NaveoService.Constants.Headers.API_PREFIX + "MigrateAssetDataToGMS")]
        public IHttpActionResult MigrateAssetDataToGMS()
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel mydtData = new NaveoService.Services.AssetService().MigrateAssetToGMS(securityTokenObj);
                    return Ok(mydtData.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpGet, ResponseType(typeof(dtData))]
        [Route(NaveoService.Constants.Headers.API_PREFIX + "treeViewGarage")]
        public IHttpActionResult treeViewGarage(NaveoModels nm)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();

            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new NaveoService.Services.AssetService().treeViewSource(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr, nm, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage);

                    return Ok(mydtData.ToObjectDTOGarage(securityTokenObj.securityToken));

                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        /// <summary>
        /// Test Yaseen
        /// </summary>
        /// <param name="assetGarage"></param>
        /// <returns></returns>
        [HttpPost, ResponseType(typeof(BaseModel))]
        [Route(Headers.API_PREFIX + "GetFuelAndMaintenanceCost")]
        public IHttpActionResult GetFuelAndMaintenanceCost(MaintenanceAndFuelCost maintenanceAndFuelCost)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel bm = new NaveoService.Services.AssetService().GetMaintenanceFuelCost(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr, maintenanceAndFuelCost.assetIds, maintenanceAndFuelCost.DateTimeStart, maintenanceAndFuelCost.DateTimeEnd);
                    return Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        #endregion

        #region Private Methods
        private ObjectDTO castData(NaveoService.Models.Object obj, Guid oneTimeToken)
        {
            ObjectDTO d = new ObjectDTO();
            d.data = obj;
            d.oneTimeToken = oneTimeToken;
            return d;
        }

        //private ObjectDTO castData(dtData obj, Guid oneTimeToken)
        //{
        //    MasterDataItems x = new MasterDataItems();
        //    x.masterDataItems = obj.Data;

        //    ObjectDTO m = new ObjectDTO();
        //    m.data = x;
        //    //m.Data = obj.Data;//new dtData { Data = obj.Data }
        //    //m.masterDataItems = m.Data;
        //    m.totalItems = obj.Rowcnt;
        //    m.oneTimeToken = oneTimeToken;
        //    return m;
        //}



        #endregion
    }
}