﻿using Microsoft.Web.Http;
using NaveoWebApi.Constants;
using NaveoWebApi.Helpers;
using System;
using System.Web.Http;
using System.Web.Http.Description;
using NaveoService.Models.DTO;
using NaveoWebApi.Models;
using NaveoOneLib.Models.Common;
using System.Web.Script.Serialization;
using NaveoService.Models;
using NaveoService.Helpers;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class AccidentManagementController : ApiBaseController
    {
        #region API Methods
        //[HttpPost, ResponseType(typeof(BaseModel))]
        //[Route(Headers.API_PREFIX + "AccidentManagement")]
        //public IHttpActionResult AccidentMng(NaveoOneLib.Models.AccidentManagement.AccidentManagementDetailDto accidentManagementDetailDto)
        //{
        //    SecurityTokenExtended securityTokenObj = ApiAuthentication();
        //    try
        //    {
        //        if (securityTokenObj.securityToken.IsAuthorized)
        //        {
        //            //JavaScriptSerializer serializer = new JavaScriptSerializer();
        //            //NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto jsonSaveObject = serializer.Deserialize<NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto>(assetMaintenanceDetailDto.ToString());
        //            BaseModel bm = new NaveoOneLib.Services.AccidentManagement.AccidentManagementServices().AccidentManagement(accidentManagementDetailDto, securityTokenObj.securityToken.sConnStr);

        //            return Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
        //        }
        //        return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
        //    }
        //    catch (Exception e)
        //    {
        //        return Ok(SecurityHelper.ExceptionMessage(e));
        //    }
        //}



        [HttpPost, ResponseType(typeof(BaseModel))]
        [Route(Headers.API_PREFIX + "SaveAccidentMgt")]
        public IHttpActionResult SaveAccidentMgt(NaveoOneLib.Models.AccidentManagement.AccidentManagementDetailDto accidentManagementDetailDto)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    //JavaScriptSerializer serializer = new JavaScriptSerializer();
                    //NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto jsonSaveObject = serializer.Deserialize<NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto>(assetMaintenanceDetailDto.ToString());
                    BaseModel bm = new NaveoOneLib.Services.AccidentManagement.AccidentManagementServices().AccidentManagement(accidentManagementDetailDto, securityTokenObj.securityToken.sConnStr);
                    if (bm.data)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "AccidentMng";
                        string action = "SaveAccident";
                        string rawRequest = "";
                        Telemetry.Trace(userToken, controller, action, rawRequest, -1, securityTokenObj.securityToken.sConnStr, true);
                    }
                    return Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }



        [HttpPost, ResponseType(typeof(BaseModel))]
        [Route(Headers.API_PREFIX + "UpdateAccidentMgt")]
        public IHttpActionResult UpdateAccidentMgt(NaveoOneLib.Models.AccidentManagement.AccidentManagementDetailDto accidentManagementDetailDto)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    //JavaScriptSerializer serializer = new JavaScriptSerializer();
                    //NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto jsonSaveObject = serializer.Deserialize<NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto>(assetMaintenanceDetailDto.ToString());
                    BaseModel bm = new NaveoOneLib.Services.AccidentManagement.AccidentManagementServices().AccidentManagement(accidentManagementDetailDto, securityTokenObj.securityToken.sConnStr);
                    if (bm.data)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "AccidentMng";
                        string action = "UpdateAccident";
                        string rawRequest = "";
                        Telemetry.Trace(userToken, controller, action, rawRequest, -1, securityTokenObj.securityToken.sConnStr, true);
                    }
                    return Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }



        [HttpPost, ResponseType(typeof(BaseModel))]
        [Route(Headers.API_PREFIX + "DeleteAccidentMgt")]
        public IHttpActionResult DeleteAccidentMgt(NaveoOneLib.Models.AccidentManagement.AccidentManagementDetailDto accidentManagementDetailDto)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    //JavaScriptSerializer serializer = new JavaScriptSerializer();
                    //NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto jsonSaveObject = serializer.Deserialize<NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto>(assetMaintenanceDetailDto.ToString());
                    BaseModel bm = new NaveoOneLib.Services.AccidentManagement.AccidentManagementServices().AccidentManagement(accidentManagementDetailDto, securityTokenObj.securityToken.sConnStr);
                    if (bm.data)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "AccidentMng";
                        string action = "DeleteAccident";
                        string rawRequest = "";
                        Telemetry.Trace(userToken, controller, action, rawRequest, -1, securityTokenObj.securityToken.sConnStr, true);
                    }
                    return Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }



        [HttpPost, ResponseType(typeof(BaseModel))]
        [Route(Headers.API_PREFIX + "ListAccidentManagement")]
        public IHttpActionResult ListAccidentMng(NaveoOneLib.Models.AccidentManagement.lstAccidentManagementDto accidentManagementDto)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    //JavaScriptSerializer serializer = new JavaScriptSerializer();
                    //NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto jsonSaveObject = serializer.Deserialize<NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto>(assetMaintenanceDetailDto.ToString());
                    BaseModel bm = new NaveoOneLib.Services.AccidentManagement.AccidentManagementServices().ListAccidentManagement(accidentManagementDto, securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr);

                    return Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }



        [HttpPost, ResponseType(typeof(BaseModel))]
        [Route(Headers.API_PREFIX + "GetDropDownAccidentManagement")]
        public IHttpActionResult GetDropDownAccidentManagement(NaveoOneLib.Models.AccidentManagement.getAssetIdDto getAssetIdDto)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    //JavaScriptSerializer serializer = new JavaScriptSerializer();
                    //NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto jsonSaveObject = serializer.Deserialize<NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto>(assetMaintenanceDetailDto.ToString());
                    BaseModel bm = new NaveoOneLib.Services.AccidentManagement.AccidentManagementServices().getAssetId(getAssetIdDto, securityTokenObj.securityToken.sConnStr);

                    return Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(BaseModel))]
        [Route(Headers.API_PREFIX + "GetAccidentMngById")]
        public IHttpActionResult GetAccidentMngById(NaveoOneLib.Models.AccidentManagement.getAccidentMngDto accidentManagementDto)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    //JavaScriptSerializer serializer = new JavaScriptSerializer();
                    //NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto jsonSaveObject = serializer.Deserialize<NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto>(assetMaintenanceDetailDto.ToString());
                    BaseModel bm = new NaveoOneLib.Services.AccidentManagement.AccidentManagementServices().GetAccidentMngByAccId(accidentManagementDto, securityTokenObj.securityToken.sConnStr);

                    return Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }
        #endregion
    }
}