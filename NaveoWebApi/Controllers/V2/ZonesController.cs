﻿using Microsoft.Web.Http;
using NaveoOneLib.Models;
using NaveoService.Services;
using NaveoWebApi.Constants;
using NaveoWebApi.Models;
using NaveoService.Models.DTO;
using NaveoWebApi.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Description;
using NaveoService.Models;
using NaveoService.Helpers;
using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Models.Common;
using System.Web.Script.Serialization;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class ZonesController : ApiBaseController
    {
        BaseDTO baseDTO = new BaseDTO();

        #region API Methods
        /// <summary> 
        /// Get Get zones
        /// <para>HEADERS EXAMPLE</para>
        /// <para>HOST_URL: mercury.naveo.mu</para>
        /// <para>USER_TOKEN: B8F03A2A-56A5-4501-9323-3BACFC35BE49</para> 
        /// <para>[ONE_TIME_TOKEN]: A8F453S2A-24C5-4320-4NAQWE3AK22</para> 
        /// <para>PAGE: 1</para>
        /// <para>ROWS_PER_PAGE: 5</para>
        /// </summary> 
        /// <returns></returns>
        [HttpGet, ResponseType(typeof(ZoneDTO))]
        [Route(Headers.API_PREFIX + "GetZones")]
        public IHttpActionResult GetZones()
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    ZoneData zoneData = new ZoneService().GetZones(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr, true, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage);
                    return base.Ok(zoneData.ToZoneDTO(securityTokenObj.securityToken));
                }
                return base.Ok(baseDTO.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "GetZoneList")]
        public IHttpActionResult GetZoneList(lZones lz)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new NaveoService.Services.ZoneService().GetZoneListFromZoneIDs(securityTokenObj.securityToken.UserToken, lz.lZoneIDs, securityTokenObj.securityToken.sConnStr, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        /// <summary>
        /// Header example
        /// HOST_URL: mercury.naveo.mu
        /// USER_TOKEN: B8F03A2A-56A5-4501-9323-3BACFC35BE49
        /// [ONE_TIME_TOKEN]: 0c288a26-aa02-4f34-baff-078a9cab1ca7 
        /// </summary>
        /// <returns></returns>
        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "GetZoneById")]
        public IHttpActionResult GetZoneById(lZones lz)
        {
            /*Body 
           {
            "zoneId":[51]
            }
           */

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    ZoneData zd = new ZoneService().GetZoneById(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr, lz.lZoneIDs);
                    return Ok(zd.ToZoneDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "SaveZone")]
        public IHttpActionResult SaveZone(PutDTO zoneSave)
        {
            #region Body

            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    ZoneData jsonSaveObject = serializer.Deserialize<ZoneData>(zoneSave.data.ToString());
                    Boolean bSave = new ZoneService().SaveZone(securityTokenObj, securityTokenObj.securityToken.sConnStr, jsonSaveObject, true);
                    if (bSave)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Zone";
                        string action = "SaveZone";
                        string rawRequest = zoneSave.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonSaveObject.lZone[0].zoneID, securityTokenObj.securityToken.sConnStr, true);
                    }
                    return Ok(bSave.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "SaveZonesFromShp")]
        public IHttpActionResult SaveFixedAssetsFromShp(PutDTO shpZones)
        {
            #region save/Update Body
            //{
            //    "data": {
            //        "shpPath" : "E:\\Reza\\Geo\\Maps\\Creations\\MDC2019\\SHAPES\\Pole.shp",  --Map Server Path
            //  "iMatrix" : 3
            //                },
            // "oneTimeToken": "5e51f2eb-110e-4108-9529-5d408d29668f",
            // "screenMode": "Add"
            //}
            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    String shpPath = shpZones.data.ToString();
                    BaseDTO SaveZone = new ZoneService().SaveZonesFromShp(securityTokenObj, shpPath);
                    if (SaveZone.data)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Zone";
                        string action = "SaveZonesFromShp";
                        string rawRequest = "{'path':'" + shpPath + "'}";
                        Telemetry.Trace(userToken, controller, action, rawRequest, -1, securityTokenObj.securityToken.sConnStr, true);
                    }
                    CommonHelper.DefaultData(SaveZone, securityTokenObj);
                    return Ok(SaveZone);
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "UpdateZone")]
        public IHttpActionResult UpdateZone(PutDTO zoneUpdate)
        {
            #region Body

            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    ZoneData jsonSaveObject = serializer.Deserialize<ZoneData>(zoneUpdate.data.ToString());
                    Boolean bUpdate = new ZoneService().SaveZone(securityTokenObj, securityTokenObj.securityToken.sConnStr, jsonSaveObject, false);
                    if (bUpdate)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Zone";
                        string action = "UpdateZone";
                        string rawRequest = zoneUpdate.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonSaveObject.lZone[0].zoneID, securityTokenObj.securityToken.sConnStr, true);
                    }
                    return Ok(bUpdate.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "DeleteZoneById")]
        public IHttpActionResult DeleteZoneById(lZones lz)
        {
            /*Body 
               {
                    "zoneId":[51]
               }
           */

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseDTO zd = new ZoneService().DeleteZoneById(securityTokenObj, lz.lZoneIDs);
                    if (zd.data)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Zone";
                        string action = "DeleteZoneById";
                        string rawRequest = "{'lZoneIDs':'" + lz.lZoneIDs.ToString() + "'}";
                        Telemetry.Trace(userToken, controller, action, rawRequest, lz.lZoneIDs[0], securityTokenObj.securityToken.sConnStr, true);
                    }
                    CommonHelper.DefaultData(zd, securityTokenObj);
                    return Ok(zd);
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        #endregion

        #region API reports
        [HttpPost, ResponseType(typeof(ZoneDTO))]
        [Route(Headers.API_PREFIX + "GetZoneVisitedReport")]
        public IHttpActionResult ZoneVisitedReport(ZoneVisitedReport zVr)
        {
            //Check if all parameters are available. If not, return error messagek
            String sError = String.Empty;
            if (zVr == null)
                sError = "Missing parameters 0";
            else if (zVr.assetIds == null)
                sError = "Missing parameters 1";
            else if (zVr.dateFrom == null)
                sError = "Missing parameters 2";
            else if (zVr.dateTo == null)
                sError = "Missing parameters 3";

            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel getZV = new ZoneService().getZoneVisited(securityTokenObj.securityToken.UserToken, zVr.assetIds, zVr.sType, zVr.reportType, zVr.zIds, zVr.idleInMinutes, zVr.idlingSpeed, zVr.bGetZones, zVr.dateFrom, zVr.dateTo, zVr.byDriver, securityTokenObj.securityToken.sConnStr, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage);
                    return Ok(getZV.ToBaseDTO(securityTokenObj.securityToken));
                }
                return base.Ok(baseDTO.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(ZoneDTO))]
        [Route(Headers.API_PREFIX + "GetZoneVisitedDetails")]
        public IHttpActionResult getZoneVisitedDetails(zoneFormat zF)
        {
            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;
            if (zF == null)
                sError = "Missing parameters 0";


            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);


            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel getZV = new ZoneService().getZoneVisitedDetails(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr, zF.zoneId);
                    return Ok(getZV.ToBaseDTO(securityTokenObj.securityToken));
                }
                return base.Ok(baseDTO.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }

        }
        #endregion

        #region VCA
        class VCACreation
        {
            public String sPath { get; set; }
        }

        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "SaveVCAFromShp")]
        public IHttpActionResult SaveVCAFromShp(PutDTO VCA)
        {
            #region save/Update Body
            //{
            //    "data": {
            //        "shpPath" : "E:\\Reza\\Geo\\Maps\\Creations\\MDC2019\\SHAPES\\City.shp"
            //                },
            // "oneTimeToken": "5e51f2eb-110e-4108-9529-5d408d29668f",
            //}
            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    VCACreation jsonSaveObject = serializer.Deserialize<VCACreation>(VCA.data.ToString());
                    String shpPath = jsonSaveObject.sPath;
                    BaseDTO saveVCA = new ZoneService().SaveVCAFromShp(securityTokenObj, shpPath);
                    if (saveVCA.data)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Zone";
                        string action = "SaveVCAFromShp";
                        string rawRequest = VCA.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, -1, securityTokenObj.securityToken.sConnStr, true);
                    }
                    CommonHelper.DefaultData(saveVCA, securityTokenObj);
                    return Ok(saveVCA);
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }
        #endregion
    }
}