﻿using Microsoft.Web.Http;
using NaveoWebApi.Constants;
using NaveoWebApi.Helpers;
using System;
using System.Web.Http;
using System.Web.Http.Description;
using NaveoService.Models.DTO;
using NaveoWebApi.Models;
using NaveoOneLib.Models.Common;
using System.Web.Script.Serialization;
using NaveoService.Models;
using NaveoService.Helpers;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class MaintenanceController : ApiBaseController
    {
        #region API Methods
        [HttpPost, ResponseType(typeof(BaseModel))]
        [Route(Headers.API_PREFIX + "SaveMaintenance")]
        public IHttpActionResult SaveMaint(NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto assetMaintenanceDetailDto)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    //JavaScriptSerializer serializer = new JavaScriptSerializer();
                    //NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto jsonSaveObject = serializer.Deserialize<NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto>(assetMaintenanceDetailDto.ToString());
                    BaseModel bm = new NaveoService.Services.MaintAssetServices().SaveMaintenance(assetMaintenanceDetailDto, securityTokenObj.securityToken.sConnStr);
                    if (bm.data)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Maintenance";
                        string action = "SaveMaint";
                        string rawRequest = "";
                        Telemetry.Trace(userToken, controller, action, rawRequest, -1, securityTokenObj.securityToken.sConnStr, true);
                    }
                    return Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        [HttpGet, ResponseType(typeof(BaseModel))]
        [Route(Headers.API_PREFIX + "GetRecurringMaintenance")]
        public IHttpActionResult GetRecurringMaintenance()
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    //JavaScriptSerializer serializer = new JavaScriptSerializer();
                    //NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto jsonSaveObject = serializer.Deserialize<NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto>(assetMaintenanceDetailDto.ToString());
                    BaseModel bm = new NaveoService.Services.MaintAssetServices().GetRecurringMaint();
                    /*if (bm.data)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Maintenance";
                        string action = "GetRecurring";
                        string rawRequest = "";
                        Telemetry.Trace(userToken, controller, action, rawRequest, -1, securityTokenObj.securityToken.sConnStr, true);
                    }*/
                    return Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpGet, ResponseType(typeof(BaseModel))]
        [Route(Headers.API_PREFIX + "GetMaintenanceType")]
        public IHttpActionResult GetMaintenanceType()
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    //JavaScriptSerializer serializer = new JavaScriptSerializer();
                    //NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto jsonSaveObject = serializer.Deserialize<NaveoOneLib.Models.Maintenance.AssetMaintenanceDetailDto>(assetMaintenanceDetailDto.ToString());
                    BaseModel bm = new NaveoService.Services.MaintAssetServices().GetMaintenanceType();
                    /*if (bm.data)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Maintenance";
                        string action = "GetRecurring";
                        string rawRequest = "";
                        Telemetry.Trace(userToken, controller, action, rawRequest, -1, securityTokenObj.securityToken.sConnStr, true);
                    }*/
                    return Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        [HttpPost, ResponseType(typeof(BaseModel))]
        [Route(Headers.API_PREFIX + "PushMaintenanceStatus")]
        public IHttpActionResult PushMaintenanceStatus(NaveoOneLib.Models.Maintenance.CPMmaintenance CPM)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    NaveoOneLib.Models.Maintenance.CPMmaintenance jsonSaveObject = serializer.Deserialize<NaveoOneLib.Models.Maintenance.CPMmaintenance>(CPM.ToString());
                    BaseModel bm = new NaveoService.Services.MaintAssetServices().pushCPM(CPM, securityTokenObj.securityToken.sConnStr);
                    return Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        #endregion


    }
}