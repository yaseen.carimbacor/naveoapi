﻿using Microsoft.Web.Http;
using NaveoService.Services;
using NaveoWebApi.Constants;
using NaveoWebApi.Helpers;
using System;
using System.Web.Http;
using System.Web.Http.Description;
using NaveoService.Models.DTO;
using NaveoWebApi.Models;
using NaveoOneLib.Models.Common;
using System.Web.Script.Serialization;
using NaveoService.Models;
using NaveoService.Helpers;
using NaveoMonitoring;
using System.Data;
using static NaveoMonitoring.NaveoMonitoring;
using GlobalFunctions;
using System.Net;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class ConfigurationsController : ApiBaseController
    {
        #region MONITORING Methods
      
        [AllowAnonymous, HttpGet, ResponseType(typeof(NaveoService.Models.Object))]
        [Route(Headers.API_PREFIX + "GetConnStr")]
        public IHttpActionResult GetConnStr(String Token)
        {
            if (Token != "N@veo2017!")
                return Ok("Unauthorized!!! Think twice before accessing this server...");

            String s = CommonHelper.GetCurrentConnStr();
            String g = CommonHelper.tst();

            return Ok("sConnStr : " + s + "; tst : " + g);
        }

        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "GetSIMCardData")]
        public IHttpActionResult GetMonitoring()
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized) //getSIMCardData
                {
                    DataTable dt =  NaveoMonitoring.NaveoMonitoring.GetSIMCardData(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage);

                    dtData mydtData = new dtData();
                    mydtData.Data = dt;

                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "GetAllExceptions")]
        public IHttpActionResult GetMonitoringException(clsMonitoring cls)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    DataTable dt = NaveoMonitoring.NaveoMonitoring.GetExceptions(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr, cls.DateTimeStart.Value, cls.DateTimeEnd.Value, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage);
                    dtData mydtData = new dtData();
                    mydtData.Data = dt;

                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        #endregion

        #region Configure Units

        [HttpPost, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "GetDeviceData")]
        public IHttpActionResult GetDeviceData(Device device)
        {
            String sError = String.Empty;
            if (device == null)
                sError = "Missing parameters 0";
            else if (device.deviceID == null)
                sError = "Missing parameters 1";

            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    DataSet ds = new GlobalFunc().QryNaveoSvr(device.deviceID, device.nOfRec, device.SummaryData, false, device.sortAsc, device.oldData);
                    dtData myData = new dtData();
                    myData.dsData = ds;
                    return base.Ok(myData.ToObjectDTO(securityTokenObj.securityToken));
                }
                BaseDTO baseDTO = new BaseDTO();
                return base.Ok(baseDTO.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost]
        [Route(Headers.API_PREFIX + "SendMessageToSend")]
        public IHttpActionResult SendMessageToSend(MessageToDevice mesageToDevice)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    Boolean b = new GlobalFunctions.GlobalFunc().SendMessageToSend(mesageToDevice.deviceID, mesageToDevice.sUnitModel, mesageToDevice.sMessage);
                    return base.Ok(b.ToObjectDTO(securityTokenObj.securityToken));
                }
                BaseDTO baseDTO = new BaseDTO();
                return base.Ok(baseDTO.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpGet]
        [Route(Headers.API_PREFIX + "QryMessageToSend")]
        public IHttpActionResult QryMessageToSend()
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    DataTable dt = new GlobalFunctions.GlobalFunc().QryMessageToSend();
                    dtData mydtData = new dtData();
                    mydtData.Data = dt;
                    mydtData.Rowcnt = dt.Rows.Count;

                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                BaseDTO baseDTO = new BaseDTO();
                return base.Ok(baseDTO.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        #endregion

        #region Private Methods




        #endregion
    }
}