﻿using System.Web.Http;
using Microsoft.Web.Http;
using System;
using System.Collections.Generic;
using NaveoService.Services;
using NaveoOneLib.Models;
using Newtonsoft.Json;
using System.Web.Http.Description;
using NaveoWebApi.Models;
using System.Net;
using System.Web.Hosting;
using System.Data;
using NaveoService.Models;
using NaveoWebApi.Helpers;
using System.Net.Http.Headers;
using System.Web;
using NaveoWebApi.Constants;
using NaveoService.Models.DTO;
using NaveoOneLib.Models.Permissions;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class VehicleSummaryController : ApiBaseController
    {
        BaseDTO baseDTO = new BaseDTO();

        [AllowAnonymous, HttpPost, ResponseType(typeof(List<Live>))]
        [Route(Headers.API_PREFIX + "VehicleSummary")]
        public IHttpActionResult VehicleSummary(Models.AssetSummary a)
        {
            /*
                {
                    "assetId": 25,
                    "dateFrom": "2017-10-05 01:01:01",
                    "dateTo": "2017-10-05 18:20:20"
                }             */

            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;
            if (a == null)
                sError = "Missing parameters 0";
            else if (String.IsNullOrEmpty(a.assetId.ToString()))
                sError = "Missing parameters 1";
            else if (a.dateFrom == null)
                sError = "Missing parameters 2";
            else if (a.dateTo == null)
                sError = "Missing parameters 3";

            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            if (securityTokenObj.securityToken.IsAuthorized)
            {
                VehicleSummary vs = new VehicleSummaryService().GetVehicleSummary(securityTokenObj.securityToken.UserToken, a.assetId, a.dateFrom, a.dateTo,a.iMinTripDistance,securityTokenObj.securityToken.sConnStr);
                return Ok(castData(vs, securityTokenObj.securityToken.OneTimeToken));
            }
            return Ok(baseDTO.Unauthorized(securityTokenObj.securityToken.DebugMessage));
        }

        #region Private Methods
        private VehicleSummaryDTO castData(VehicleSummary vehicleSummary, Guid OneTimeToken)
        {
            VehicleSummaryDTO d = new VehicleSummaryDTO();
            d.data = vehicleSummary;
            d.oneTimeToken = OneTimeToken;
            d.sQryResult = Headers.AUTHORIZED;

            return d;
        }
        #endregion
    }
}