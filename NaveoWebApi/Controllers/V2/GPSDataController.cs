﻿using Microsoft.Web.Http;
using NaveoOneLib.Models;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.Permissions;
using NaveoService.Services;
using NaveoWebApi.Constants;
using NaveoService.Helpers;
using NaveoWebApi.Helpers;
using NaveoWebApi.Models;
using NaveoService.Models.DTO;
using System;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using NaveoService.Models;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class GPSDataController: ApiBaseController
    {
        BaseDTO baseDTO = new BaseDTO();

        #region API Methods
        /// <summary> 
        /// Get Debug Data
        /// <para>BODY EXAMPLE</para>
        /// <para>{</para>
        ///   <para>"assetIds": [25] </para>
        ///   <para>"dateFrom": "2017-10-05 01:01:01"</para>
        ///   <para>"dateTo": "2017-10-05 18:20:20" </para>
        ///   <para>"lTypeID": [] </para>
        /// <para>}</para>
        /// <para>HEADERS EXAMPLE</para>
        /// <para>HOST_URL: mercury.naveo.mu</para>
        /// <para>USER_TOKEN: B8F03A2A-56A5-4501-9323-3BACFC35BE49</para> 
        /// <para>[ONE_TIME_TOKEN]: A8F453S2A-24C5-4320-4NAQWE3AK22</para> 
        /// <para>PAGE: 1</para>
        /// <para>ROWS_PER_PAGE: 5</para>
        /// </summary> 
        /// <returns></returns>
        [HttpPost, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "DebugData")]
        public IHttpActionResult Trips(Trips t)
        {
            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;
            if (t == null)
                sError = "Missing parameters 0";
            else if (t.assetIds == null)
                sError = "Missing parameters 1";
            else if (t.dateFrom == null)
                sError = "Missing parameters 2";
            else if (t.dateTo == null)
                sError = "Missing parameters 3";

            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    NaveoService.Models.DebugData dd = new GPSDataService().GetDebugData(securityTokenObj.securityToken.UserToken, t.assetIds, t.dateFrom, t.dateTo, t.lTypeID, securityTokenObj.securityToken.sConnStr, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage);
                    return base.Ok(dd.ToDebugDataDTO(securityTokenObj.securityToken));
                }
                return base.Ok(baseDTO.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        /// <summary> 
        /// Get Debug Data
        /// <para>BODY EXAMPLE</para>
        /// <para>{</para>
        ///   <para>"assetIds": [25] </para>
        ///   <para>"dateFrom": "2017-10-05 01:01:01"</para>
        ///   <para>"dateTo": "2017-10-05 18:20:20" </para>
        ///   <para>"lTypeID": [] </para>
        /// <para>}</para>
        /// <para>HEADERS EXAMPLE</para>
        /// <para>HOST_URL: mercury.naveo.mu</para>
        /// <para>USER_TOKEN: B8F03A2A-56A5-4501-9323-3BACFC35BE49</para> 
        /// <para>[ONE_TIME_TOKEN]: A8F453S2A-24C5-4320-4NAQWE3AK22</para> 
        /// <para>PAGE: 1</para>
        /// <para>ROWS_PER_PAGE: 5</para>
        /// </summary> 
        /// <returns></returns>
        [HttpPost, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "AuxData")]
        public IHttpActionResult TripsAux(Trips t)
        {
            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;
            if (t == null)
                sError = "Missing parameters 0";
            else if (t.assetIds == null)
                sError = "Missing parameters 1";
            else if (t.dateFrom == null)
                sError = "Missing parameters 2";
            else if (t.dateTo == null)
                sError = "Missing parameters 3";

            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    NaveoService.Models.DebugData dd = new GPSDataService().GetAuxData(securityTokenObj.securityToken.UserToken, t.assetIds, t.dateFrom, t.dateTo, t.lTypeID, securityTokenObj.securityToken.sConnStr, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage);

                    return base.Ok(dd.ToAuxDTO(securityTokenObj.securityToken));
                }
                return base.Ok(baseDTO.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }
        /// <summary> 
        /// Get Debug Types
        /// <para>HEADERS EXAMPLE</para>
        /// <para>HOST_URL: mercury.naveo.mu</para>
        /// <para>USER_TOKEN: B8F03A2A-56A5-4501-9323-3BACFC35BE49</para> 
        /// <para>[ONE_TIME_TOKEN]: A8F453S2A-24C5-4320-4NAQWE3AK22</para> 
        /// <para>PAGE: 1</para>
        /// <para>ROWS_PER_PAGE: 5</para>
        /// </summary> 
        /// <param name="nm"></param>
        /// <returns></returns>
        [HttpGet, ResponseType(typeof(NaveoService.Models.Object))]
        [Route(Headers.API_PREFIX + "GetDebugTypes")]
        public IHttpActionResult GetDebugTypes()
        {
            /*
            https://Mercury.naveo.mu/api/v2.0/GetDebugTypes
            
             Content-Type:application/json
            USER_TOKEN:b71929b9-8d57-4003-b14b-4f14416cfc04
            //HOST_URL:baby.naveo.mu
            ONE_TIME_TOKEN:97cc98da-91f2-4228-82bd-8b42a800d4d3
            */
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    System.Data.DataTable dt = new System.Data.DataTable();
                    dt.Columns.Add("id");
                    dt.Columns.Add("value");
                    dt.Columns.Add("category");

                    System.Data.DataRow dr = dt.NewRow();
                    dr["id"] = "1";
                    dr["value"] = "Harsh Accelleration";
                    dr["category"] = "Driving";
                    dt.Rows.Add(dr);

                    dr = dt.NewRow();
                    dr["id"] = "2";
                    dr["value"] = "Harsh Breaking";
                    dr["category"] = "Driving";
                    dt.Rows.Add(dr);

                    dr = dt.NewRow();
                    dr["id"] = "3";
                    dr["value"] = "Harsh Cornering";
                    dr["category"] = "Driving";
                    dt.Rows.Add(dr);

                    dr = dt.NewRow();
                    dr["id"] = "4";
                    dr["value"] = "Heading";
                    dr["category"] = "Driving";
                    dt.Rows.Add(dr);

                    dr = dt.NewRow();
                    dr["id"] = "5";
                    dr["value"] = "Idling";
                    dr["category"] = "Driving";
                    dt.Rows.Add(dr);

                    dr = dt.NewRow();
                    dr["id"] = "6";
                    dr["value"] = "Ignition";
                    dr["category"] = "Driving";
                    dt.Rows.Add(dr);

                    dr = dt.NewRow();
                    dr["id"] = "7";
                    dr["value"] = "InvalidGpsSignals";
                    dr["category"] = "Device";
                    dt.Rows.Add(dr);

                    dr = dt.NewRow();
                    dr["id"] = "8";
                    dr["value"] = "ModemReset";
                    dr["category"] = "Device";
                    dt.Rows.Add(dr);

                    dr = dt.NewRow();
                    dr["id"] = "9";
                    dr["value"] = "No GPS";
                    dr["category"] = "Device";
                    dt.Rows.Add(dr);

                    dr = dt.NewRow();
                    dr["id"] = "10";
                    dr["value"] = "Reset";
                    dr["category"] = "Device";
                    dt.Rows.Add(dr);

                    dr = dt.NewRow();
                    dr["id"] = "11";
                    dr["value"] = "Temperature";
                    dr["category"] = "AUX";
                    dt.Rows.Add(dr);

                    dr = dt.NewRow();
                    dr["id"] = "12";
                    dr["value"] = "Temperature 2";
                    dr["category"] = "AUX";
                    dt.Rows.Add(dr);

                    dr = dt.NewRow();
                    dr["id"] = "13";
                    dr["value"] = "Temperature 3";
                    dr["category"] = "AUX";
                    dt.Rows.Add(dr);

                    dr = dt.NewRow();
                    dr["id"] = "14";
                    dr["value"] = "AUX1";
                    dr["category"] = "AUX";
                    dt.Rows.Add(dr);

                    dr = dt.NewRow();
                    dr["id"] = "15";
                    dr["value"] = "AUX2";
                    dr["category"] = "AUX";
                    dt.Rows.Add(dr);

                    dr = dt.NewRow();
                    dr["id"] = "16";
                    dr["value"] = "Fuel";
                    dr["category"] = "AUX";
                    dt.Rows.Add(dr);

                    dtData obj = new dtData();
                    obj.Data = dt;
                    obj.Rowcnt = dt.Rows.Count;
                    return base.Ok(obj.ToObjectDTO(securityTokenObj.securityToken));
                }
                return base.Ok(baseDTO.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        #endregion

    }
}