﻿using System.Web.Http;
using Microsoft.Web.Http;
using System;
using NaveoService.Services;
using NaveoOneLib.Models;
using System.Web.Http.Description;
using NaveoWebApi.Models;
using System.Net;
using NaveoService.Models;
using NaveoWebApi.Helpers;
using NaveoService.Models.DTO;
using NaveoWebApi.Constants;
using NaveoService.Helpers;
using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Models.Common;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using NaveoOneLib.Models.BLUP;
using NaveoOneLib.Models.Audits;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class BlupController : ApiBaseController
    {
        BaseDTO baseDTO = new BaseDTO();

        [HttpPost, ResponseType(typeof(String))]
        [Route(Headers.API_PREFIX + "CreateCircles")]
        public IHttpActionResult CreateCircles(Blup b)
        {
            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;
            if (b == null)
                sError = "Missing parameters 0";

            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel bm = new NaveoService.Services.BlupService().GetBlupWithinDistanceInMeters(securityTokenObj.securityToken.UserToken, 300, b.lon, b.lat, securityTokenObj.securityToken.sConnStr);
                    return base.Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
                }
                return base.Ok(baseDTO.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "GetBlupByIds")]
        public IHttpActionResult GetBlupByIds(Blup B)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel bm = new NaveoService.Services.BlupService().GetBLUPDataByIds(securityTokenObj.securityToken.UserToken, B.lids, B.bShowBuffers, securityTokenObj.securityToken.sConnStr);
                    return base.Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "GetBlupById")]
        public IHttpActionResult GetBlupById(lAssets ld)
        {
            /*Body 
           {
                "assetId":51
            }
           */

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel fixedasset = new BlupService().GetBLUPByIds(securityTokenObj.securityToken.UserToken, ld.assetId, securityTokenObj.securityToken.sConnStr);
                    return Ok(fixedasset.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "ShowBlupByLonLat")]
        public IHttpActionResult ShowBlupByLonLat(Blup B)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    int iDistance = 10;
                    if (B.bShowBuffers)
                        iDistance = 300;
                    BaseModel bm = new NaveoService.Services.BlupService().GetBlupWithinDistanceInMeters(securityTokenObj.securityToken.UserToken, iDistance, B.lon, B.lat, securityTokenObj.securityToken.sConnStr);
                    return base.Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }
      
        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "GetBlupByLonLat")]
        public IHttpActionResult GetBlupByLonLat(Blup B)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel bm = new NaveoService.Services.BlupService().GetBlupByLonLat(securityTokenObj.securityToken.UserToken, 100, B.lon, B.lat, securityTokenObj.securityToken.sConnStr);
                    return base.Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "GetBlupContextByLonLat")]
        public IHttpActionResult GetBlupContextByLonLat(Blup B)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel bm = new NaveoService.Services.BlupService().GetBlupContextByLonLat(securityTokenObj.securityToken.UserToken, 50, B.lon, B.lat, securityTokenObj.securityToken.sConnStr);
                    return base.Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [System.Web.Http.HttpGet, ResponseType(typeof(BaseDTO))]
        [System.Web.Http.Route(Headers.API_PREFIX + "GetBlupTree")]
        public IHttpActionResult GetBlupTree()
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new NaveoService.Services.BlupService().GetTree();
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "SaveBlup")]
        public IHttpActionResult SaveFixedAsset(PutDTO BlupToSave)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    BlupDTO jsonSaveObject = serializer.Deserialize<BlupDTO>(BlupToSave.data.ToString());

                    BaseDTO AssetSaveUpdate = new BlupService().SaveUpdateBlup(securityTokenObj, jsonSaveObject, BlupToSave.newData);
                    if (AssetSaveUpdate.sQryResult == "Succeeded") { 
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Blup";
                        string action = "SaveBlup";
                        string rawRequest = BlupToSave.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, Convert.ToInt32(jsonSaveObject.blup.iID), securityTokenObj.securityToken.sConnStr, true);
                    }


                    CommonHelper.DefaultData(AssetSaveUpdate, securityTokenObj);
                    return Ok(AssetSaveUpdate);
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        #region Nels Test
        [AllowAnonymous, HttpGet, ResponseType(typeof(NaveoService.Models.Object))]
        [Route(Headers.API_PREFIX + "GetBlupByNelsID")]
        public IHttpActionResult GetObjects(String nelsID)
        {
            //https://localhost:44393/v2.0/GetBlupByNelsID?nelsID=Mr%20Mathew
            var headers = Request.Headers;
            string hostUrl = System.Web.HttpContext.Current.Request.Url.DnsSafeHost;
            String sConnStr = CommonHelper.GetCurrentConnStr(hostUrl, headers);
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                BaseModel bm = new BlupService().GetBLUPByIds(nelsID, sConnStr);
                return base.Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        #endregion
    }
}