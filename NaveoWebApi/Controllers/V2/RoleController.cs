﻿using Microsoft.Web.Http;
using NaveoService.Services;
using NaveoWebApi.Constants;
using NaveoWebApi.Helpers;
using System;
using System.Web.Http;
using System.Web.Http.Description;
using NaveoService.Models.DTO;
using NaveoWebApi.Models;
using NaveoOneLib.Models.Common;
using System.Web.Script.Serialization;
using NaveoService.Models;
using NaveoService.Helpers;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class RoleController : ApiBaseController
    {
        BaseDTO baseDTO = new BaseDTO();

        [HttpGet, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "GetRole")]
        public IHttpActionResult GetRole()
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new NaveoService.Services.RoleService().GetRoles(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "GetRoleById")]
        public IHttpActionResult GetRoleById(lRole lR)
        {
            /*Body 
           {
            "assetId":51
            }
           */

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel role = new RoleService().GetRoleById(securityTokenObj, securityTokenObj.securityToken.sConnStr, lR.roleID);
                    return Ok(role.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        /// <summary>
        /// Create role
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(Headers.API_PREFIX + "CreateRole")]
        public IHttpActionResult CreateRole(PutDTO role)
        {
            #region Body
            /*
                  {
	"data": {
		"role": {
			"lMatrix": [{
				"GMID": 5,
				"oprType": 4
			}],
			"lPermissions": [{
				"PermissionID": 1,
				"iCreate": 1,
				"iRead": 1,
				"iUpdate": 0,
				"iDelete": 0,
				"oprType": 4
			}],
			"Description": "testRole",
			"oprType": 4
		}
	},
	"oneTimeToken": "00000000-0000-0000-0000-000000000000",
	"screenMode": "Add"
}
             */

            #endregion

            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;
            if (role == null)
                sError = "Missing parameters 0";

            if (!String.IsNullOrEmpty(sError))
                return Content(System.Net.HttpStatusCode.Forbidden, sError);


            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    RoleDTO jsonRoleObject = serializer.Deserialize<RoleDTO>(role.data.ToString());
                    BaseDTO createResult = new RoleService().CreateSaveRole(securityTokenObj,true, jsonRoleObject.role);
                    if (createResult.sQryResult == "Succeeded")
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Role";
                        string action = "SaveRole";
                        string rawRequest = role.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonRoleObject.role.RoleID, securityTokenObj.securityToken.sConnStr, true);
                    }
                    CommonHelper.DefaultData(createResult, securityTokenObj);
                    return Ok(createResult);

                }
                return base.Ok(baseDTO.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }

        }

        /// <summary>
        /// Update role
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(Headers.API_PREFIX + "UpdateRole")]
        public IHttpActionResult UpdateRole(PutDTO role)
        {

            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;
            if (role == null)
                sError = "Missing parameters 0";

            if (!String.IsNullOrEmpty(sError))
                return Content(System.Net.HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    RoleDTO jsonRoleObject = serializer.Deserialize<RoleDTO>(role.data.ToString());
                    BaseDTO createResult = new RoleService().CreateSaveRole(securityTokenObj,false,jsonRoleObject.role);
                    if (createResult.sQryResult == "Succeeded")
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Role";
                        string action = "UpdateRole";
                        string rawRequest = role.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonRoleObject.role.RoleID, securityTokenObj.securityToken.sConnStr, true);
                    }
                    CommonHelper.DefaultData(createResult, securityTokenObj);
                    return Ok(createResult);
                }
                return base.Ok(baseDTO.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }

        }


        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "DeleteRole")]
        public IHttpActionResult DeleteRole(lRole lRole)
        {
            /*Body 
             {
              "assetId":51
              }
             */

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {

                    BaseDTO deleteRole = new RoleService().DeleteRoles(securityTokenObj,lRole.roleID);
                    if (deleteRole.data)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Role";
                        string action = "DeleteRole";
                        string rawRequest = "{'roleID':'" + lRole.roleID + "'}";
                        Telemetry.Trace(userToken, controller, action, rawRequest, lRole.roleID, securityTokenObj.securityToken.sConnStr, true);
                    }
                    return Ok(deleteRole);

                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpGet, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "GetPermissions")]
        public IHttpActionResult GetPermissions()
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new NaveoService.Services.RoleService().GetPermissionsByMatrix(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpGet, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "GetPermissionByUID")]
        public IHttpActionResult GetPermissionByUID()
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new NaveoService.Services.RoleService().GetPermissionsByUID(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        [HttpGet, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "GetNaveoReport")]
        public IHttpActionResult GetNaveoReport()
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel BM = new NaveoService.Services.RoleService().GetNaveoReports();
                    return Ok(BM.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }
    }
}
