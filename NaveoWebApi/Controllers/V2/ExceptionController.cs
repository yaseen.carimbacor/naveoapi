﻿using Microsoft.Web.Http;
using NaveoOneLib.Models;
using NaveoOneLib.Models.Permissions;
using NaveoService.Models;
using NaveoWebApi.Constants;
using NaveoWebApi.Helpers;
using NaveoService.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using NaveoOneLib.Models.Common;
using System.Data;
using NaveoService.Helpers;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class ExceptionController : ApiBaseController
    {
        [AllowAnonymous, HttpPost, ResponseType(typeof(List<Alert>))]
        [Route(Headers.API_PREFIX + "VehicleExceptions")]
        public IHttpActionResult VehicleSummary(Models.AssetSummary a)
        {
            /*
                {
                    "assetId": 25,
                    "dateFrom": "2017-10-05 01:01:01",
                    "dateTo": "2017-10-05 18:20:20"
                }             
            */

            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;
            if (a == null)
                sError = "Missing parameters 0";
            else if (String.IsNullOrEmpty(a.assetId.ToString()))
                sError = "Missing parameters 1";
            else if (a.dateFrom == null)
                sError = "Missing parameters 2";
            else if (a.dateTo == null)
                sError = "Missing parameters 3";

            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            if (securityTokenObj.securityToken.IsAuthorized)
            {
                List<int> lAssetIDs = new List<int>();
                lAssetIDs.Add(a.assetId);
                Alert alert = new NaveoService.Services.ExceptionService().dtAlerts(securityTokenObj.securityToken.UserToken, lAssetIDs, a.dateFrom, a.dateTo, false, securityTokenObj.securityToken.sConnStr);
                return Ok(castData(alert, securityTokenObj.securityToken.OneTimeToken));
            }

            return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
        }

        [AllowAnonymous, HttpPost, ResponseType(typeof(List<Alert>))]
        [Route(Headers.API_PREFIX + "AllVehiclesExceptions")]
        public IHttpActionResult AllVehiclesExceptions(Models.AssetSummary a)
        {
            /*
                {
                    "dateFrom": "2017-10-05 01:01:01",
                    "dateTo": "2017-10-05 18:20:20"
                }             
            */

            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;
            if (a == null)
                sError = "Missing parameters 0";
            else if (a.dateFrom == null)
                sError = "Missing parameters 2";
            else if (a.dateTo == null)
                sError = "Missing parameters 3";

            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            if (securityTokenObj.securityToken.IsAuthorized)
            {
                Alert alert = new NaveoService.Services.ExceptionService().dtAlerts(securityTokenObj.securityToken.UserToken, null, a.dateFrom, a.dateTo, false, securityTokenObj.securityToken.sConnStr);
                return Ok(castData(alert, securityTokenObj.securityToken.OneTimeToken));
            }

            return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
        }

        [AllowAnonymous, HttpPost, ResponseType(typeof(List<BaseDTO>))]
        [Route(Headers.API_PREFIX + "GetExceptions")]
        public IHttpActionResult GetExceptions(NaveoService.Models.Exceptions exception)
        {    /*
               {
                "assetIds":[18,19],
                 "rulesIds":[],
                  "dateFrom": "2018-01-21 06:06:22",
                   "dateTo": "2018-01-21 06:06:22"
                }            
            */

            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;
            if (exception == null)
                sError = "Missing parameters 0";
            else if (exception.dateFrom == null)
                sError = "Missing parameters 2";
            else if (exception.dateTo == null)
                sError = "Missing parameters 3";

            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

          SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    DataSet ds = new NaveoService.Services.ExceptionService().GetExceptions(securityTokenObj.securityToken.UserToken, exception.assetIds, exception.ruleIds, exception.dateFrom, exception.dateTo, exception.byDriver, true, exception.bShowZones, securityTokenObj.securityToken.sConnStr);
                    return Ok(ds.ToExceptionDTO(securityTokenObj.securityToken)); 
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        [AllowAnonymous, HttpPost, ResponseType(typeof(List<BaseDTO>))]
        [Route(Headers.API_PREFIX + "GetPlanningExceptions")]
        public IHttpActionResult GetPlanningExceptions(NaveoService.Models.Exceptions exception)
        {    /*
               {
                "assetIds":[18,19],
                 "rulesIds":[],
                  "dateFrom": "2018-01-21 06:06:22",
                   "dateTo": "2018-01-21 06:06:22"
                }            
            */

            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;
            if (exception == null)
                sError = "Missing parameters 0";
            else if (exception.dateFrom == null)
                sError = "Missing parameters 2";
            else if (exception.dateTo == null)
                sError = "Missing parameters 3";

            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    DataTable dt = new NaveoService.Services.ExceptionService().GetPlanningExceptions(securityTokenObj.securityToken.UserToken, exception.assetIds, exception.ruleIds, exception.dateFrom, exception.dateTo, exception.byDriver,securityTokenObj.securityToken.sConnStr);
                    return Ok(dt.ToPlanningFuelExceptionDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        [AllowAnonymous, HttpPost, ResponseType(typeof(List<BaseDTO>))]
        [Route(Headers.API_PREFIX + "GetFuelExceptions")]
        public IHttpActionResult GetFuelExceptions(NaveoService.Models.Exceptions exception)
        {    /*
               {
                "assetIds":[18,19],
                 "rulesIds":[],
                  "dateFrom": "2018-01-21 06:06:22",
                   "dateTo": "2018-01-21 06:06:22"
                }            
            */

            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;
            if (exception == null)
                sError = "Missing parameters 0";
            else if (exception.dateFrom == null)
                sError = "Missing parameters 2";
            else if (exception.dateTo == null)
                sError = "Missing parameters 3";

            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    DataTable dt = new NaveoService.Services.ExceptionService().GetFuelExceptions(securityTokenObj.securityToken.UserToken, exception.assetIds, exception.ruleIds, exception.dateFrom, exception.dateTo, exception.byDriver,securityTokenObj.securityToken.sConnStr);
                    return Ok(dt.ToPlanningFuelExceptionDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        #region Private Methods
        private AlertDTO castData(Alert a, Guid OneTimeToken)
        {
            AlertDTO d = new AlertDTO();
            d.data = a;
            d.oneTimeToken = OneTimeToken;
            d.sQryResult = Headers.AUTHORIZED;
            return d;
        }
        #endregion
    }
}