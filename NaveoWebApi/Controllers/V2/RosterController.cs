﻿using Microsoft.Web.Http;
using NaveoOneLib.Models.Common;
using NaveoService.Constants;
using NaveoService.Helpers;
using NaveoService.Models;
using NaveoService.Models.DTO;
using NaveoService.Services;
using NaveoWebApi.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using HttpPostAttribute = System.Web.Http.HttpPostAttribute;
using RouteAttribute = System.Web.Http.RouteAttribute;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class RosterController : ApiBaseController
    {
        #region API methods
        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "GetRoster")]
        public IHttpActionResult GetRoster(Roster r)
       {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new NaveoService.Services.RosterService().GetRoster(r.dateFrom,r.dateTo,securityTokenObj.securityToken.UserToken,securityTokenObj.securityToken.sConnStr, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage,r.sortColumns,r.sortOrderAsc);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "GetRosterById")]
        public IHttpActionResult GetRosterById(Roster r)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel mydtData = new NaveoService.Services.RosterService().GetRosterByGroupId(securityTokenObj.securityToken.UserToken,r.GRID, securityTokenObj.securityToken.sConnStr);
                    return Ok(mydtData.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        /// <summary>
        /// Header example
        /// HOST_URL: mercury.naveo.mu
        /// USER_TOKEN: B8F03A2A-56A5-4501-9323-3BACFC35BE49
        /// [ONE_TIME_TOKEN]: 0c288a26-aa02-4f34-baff-078a9cab1ca7
        /// </summary>
        /// <returns></returns>
        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "SaveRoster")]
        public IHttpActionResult SaveRoster(PutDTO rosterSave)
        {

            #region Body



            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                     JavaScriptSerializer serializer = new JavaScriptSerializer();
                     RosterDTO jsonSaveObject = serializer.Deserialize<RosterDTO>(rosterSave.data.ToString());
                     Boolean RosterSaveUpdate = new RosterService().SaveUpdateRoster(securityTokenObj, securityTokenObj.securityToken.sConnStr,jsonSaveObject,true);
                    if (RosterSaveUpdate)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Roster";
                        string action = "SaveRoster";
                        string rawRequest = rosterSave.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonSaveObject.roster.ild, securityTokenObj.securityToken.sConnStr, true);
                    }
                    //CommonHelper.DefaultData(RuleSaveUpdate, securityTokenObj);
                    return Ok(RosterSaveUpdate.ToBaseDTO(securityTokenObj.securityToken)); 
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "UpdateRoster")]
        public IHttpActionResult UpdateRoster(PutDTO rosterToUpdate)
        {

            #region Body



            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    RosterDTO jsonSaveObject = serializer.Deserialize<RosterDTO>(rosterToUpdate.data.ToString());
                    Boolean RosterSaveUpdate = new RosterService().SaveUpdateRoster(securityTokenObj, securityTokenObj.securityToken.sConnStr, jsonSaveObject, false);
                    if (RosterSaveUpdate)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Roster";
                        string action = "UpdateRoster";
                        string rawRequest = rosterToUpdate.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonSaveObject.roster.ild, securityTokenObj.securityToken.sConnStr, true);
                    }
                    //CommonHelper.DefaultData(RuleSaveUpdate, securityTokenObj);
                    return Ok(RosterSaveUpdate.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

    

        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "DeleteRoster")]
        public IHttpActionResult DeleteRoster(Rule r)
        {

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {

                    Boolean obj = new RuleService().Delete(securityTokenObj.securityToken.UserToken,r.parentRuleId,r.ruleName,securityTokenObj.securityToken.sConnStr);
                    if (obj)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Roster";
                        string action = "DeleteRoster";
                        string rawRequest = "{'parentRuleId':'" + r.parentRuleId + "'}";
                        Telemetry.Trace(userToken, controller, action, rawRequest, Convert.ToInt32(r.parentRuleId), securityTokenObj.securityToken.sConnStr, true);
                    }
                    return Ok(obj.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        #endregion

    }
}