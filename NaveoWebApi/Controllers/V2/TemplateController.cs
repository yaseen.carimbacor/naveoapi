﻿using Microsoft.Web.Http;
using NaveoOneLib.Models;
using NaveoOneLib.Models.Permissions;
using NaveoService.Services;
using NaveoWebApi.Constants;
using NaveoService.Helpers;
using NaveoWebApi.Helpers;
using NaveoWebApi.Models;
using NaveoService.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using NaveoService.Models;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class TemplateController : ApiBaseController
    {
        BaseDTO baseDTO = new BaseDTO();
        #region Constants
        private const string AUTHORIZED = Headers.AUTHORIZED;
        private const string FAILED = Headers.FAILED;
        #endregion

        #region API Methods
        /// <summary> 
        /// Get trip details
        /// <para>BODY EXAMPLE</para>
        /// <para>{</para>
        ///   <para>"assetIds": [25] </para>
        ///   <para>"dateFrom": "2017-10-05 01:01:01"</para>
        ///   <para>"dateTo": "2017-10-05 18:20:20" </para>
        /// <para>}</para>
        /// <para>HEADERS EXAMPLE</para>
        /// <para>HOST_URL: mercury.naveo.mu</para>
        /// <para>USER_TOKEN: B8F03A2A-56A5-4501-9323-3BACFC35BE49</para> 
        /// <para>[ONE_TIME_TOKEN]: A8F453S2A-24C5-4320-4NAQWE3AK22</para> 
        /// <para>PAGE: 1</para>
        /// <para>ROWS_PER_PAGE: 5</para>
        /// </summary> 
        /// <returns></returns>
        [AllowAnonymous, HttpPost, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "GetTemplate")]
        public IHttpActionResult GetFuelRule(apiRules r)
        {
            /*
            https://localhost:44393/v2.0/GetFuelRule

            Headers
            Content - Type:application / json
            USER_TOKEN: 2c5c384f - 4a54 - 4f06 - aca6 - ea0c89c32f15
            HOST_URL: demo.naveo.mu
             //ONE_TIME_TOKEN:1ade61f2-94ed-422f-86be-c1e5abe0c1ac

             Body
             {
                "fuelType": "Fill"
             }
             */

            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;
            if (r == null)
                sError = "Missing parameters 0";
            else if (r.fuelType == null)
                sError = "Missing parameters 1";

            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    NaveoService.Models.FuelRule fuelRule = new RuleService().GetFuelRule(securityTokenObj.securityToken.UserToken, r.fuelType, securityTokenObj.securityToken.sConnStr);
                    return base.Ok(fuelRule.ToFuelRuleDTO(securityTokenObj.securityToken));
                }
                return base.Ok(baseDTO.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        #endregion

    }
}