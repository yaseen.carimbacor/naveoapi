﻿using System.Web.Http;
using Microsoft.Web.Http;
using System;
using NaveoOneLib.Models;
using Newtonsoft.Json;
using System.Web.Http.Description;
using System.Web;
using System.Text;
using System.Web.Hosting;
using System.Net;
using NaveoWebApi.Models;
using NaveoWebApi.Helpers;
using System.Net.Http.Headers;
using NaveoWebApi.Constants;
using NaveoService.Services;
using NaveoService.Models;
using System.Web.Security;
using System.Collections.Generic;
using System.Linq;
using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Models.Users;
using NaveoOneLib.Services.Users;
using NaveoService.Models.DTO;
using NaveoService.Models.Custom;
using NaveoService.Helpers;
using NaveoOneLib.Models.Common;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class DriverZoneController : ApiBaseController
    {
        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "GetDriverByZone")]
        public IHttpActionResult GetDriverByZone(Rule r)
        {

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {

                    DateTime dateTimeTo = new DateTime();

                    DateTime dateTimeFrom = new DateTime();


                    BaseModel bm = new DriverZoneService().GetDriverZone(dateTimeTo, dateTimeFrom, securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr);


                    return Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

    }
}