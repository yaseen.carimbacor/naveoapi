﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.Web.Http;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.Plannings;
using NaveoService.Constants;
using NaveoService.Helpers;
using NaveoService.Models;
using NaveoService.Models.DTO;
using NaveoService.Services;
using NaveoWebApi.Helpers;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class CWAPlanningController : ApiBaseController
    {
        [System.Web.Http.HttpPost, ResponseType(typeof(PlanningDTO))]
        [System.Web.Http.Route(Headers.API_PREFIX + "GetCWAPlanningRequest")]
        public IHttpActionResult GetCWAPlanningRequest(NaveoService.Models.Planning p)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new NaveoService.Services.PlanningService().GetPlanning(securityTokenObj.securityToken.UserToken, p.dateFrom, p.dateTo, p.PID, 1, securityTokenObj.securityToken.sConnStr, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage, p.sortColumns, p.sortOrderAsc);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        [System.Web.Http.HttpPost, ResponseType(typeof(PutDTO))]
        [System.Web.Http.Route(Headers.API_PREFIX + "SaveCWAPlanning")]
        public IHttpActionResult SavePlanning(PutDTO planningToSave)
        {
            #region save/Update Body
            /* Body 
             
                   {
	"data": {
    "assets": [{
    "assetId":0,
    "assetNumber":"Test1",
    "status_B":"AC",
    "createdDate":"0001-01-01T00:00:00",
    "updatedDate":"0001-01-01T00:00:00",
    "timeZoneId":"Arabian Standard Time",
    "timeZoneTs":"04:00:00",
    "oprType":7,
    "YearManufactured":0,
    "PurchasedDate":"1900-01-01T00:00:00",
    "PurchasedValue":0.0,
    "RefA":"Test123",
    "RefB":"Test123",
    "RefC":"Test12",
    "ExternalRef":"1111123",
    "EngineSerialNumber":"2",
    "EngineCapacity":10,
    "EnginePower":12523,
    "createdUser":null,
    "updatedUser":null,
    "assetType":{
      "id":1,
      "description":"**Default**"
    },
    "make":{
      "id":23,
      "description":"Honda"
    },
    "model":{
      "id":47,
      "description":"Test"
    },
    "color":{
      "id":17,
      "description":"Red"
    },
    "vehicleType":{
      "id":3,
      "description":"TRUCK"
    },
    "assetDeviceMap":{
      "mapId":0,
      "assetId":0,
      "deviceId":"29",
      "serialNumber":"",
      "status_B":"AC",
      "isFuel":true,
      "oprType":4,
      "deviceAuxilliaryMap":
        {
          "uId":0,
          "mapId":0,
          "deviceId":"29",
          "fuelProduct":{
            "id":43,
            "description":"Gasoline"
          },
          "thresholdHigh":100,
          "thresholdLow":0,
          "tankCapacity":0,
          "oprType":4,
          "lCalibration":[
            {
              "iId":0,
              "assetId":0,
              "deviceId":"29",
              "auxId":13,
              "StdConsumption":1.0,
              "readingUOM":{
                "id":2,
                "description":"%"
              },
              "reading":100.0,
              "convertedUOM":{
                "id":5,
                "description":"Liters"
              },
              "convertedValue":80.0,
              "oprType":4
            },
            {
              "iId":0,
              "assetId":0,
              "deviceId":"29",
              "auxId":13,
              "StdConsumption":2.0,
              "readingUOM":{
                "id":2,
                "description":"%"
              },
              "reading":100.0,
              "convertedUOM":{
                "id":5,
                "description":"Liters"
              },
              "convertedValue":0.0,
              "oprType":4
            }]
        }
    },
    "lMatrix":[
      {
        "oprType":4,
        "mId":0,
        "iId":0,
        "gMId":4,
        "gMDesc":"Naveo"
      }],
    "lUOM":[
      {
        "iId":0,
        "assetId":0,
        "uom":{
                "id":2,
                "description":"%"
              },
        "capacity":100,
        "oprType":4
      
      }]
        }]
	},

	"oneTimeToken": "00000000-0000-0000-0000-000000000000",
	"screenMode": "Add"
}

             */

            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {

                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    PlanningDTO jsonSaveObject = serializer.Deserialize<PlanningDTO>(planningToSave.data.ToString());
                    BaseDTO planningSaveUpdate = new PlanningService().SaveUpdatePlanning(securityTokenObj, jsonSaveObject, true, CommonHelper.GetCurrentWebsiteRoot());
                    if (planningSaveUpdate.sQryResult == "Succeeded")
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "CWAPlanning";
                        string action = "SaveCWAPlanning";
                        string rawRequest = planningToSave.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonSaveObject.lPlanning[0].pId, securityTokenObj.securityToken.sConnStr, true);
                    }
                    return Ok(planningSaveUpdate);
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [System.Web.Http.HttpPost, ResponseType(typeof(PutDTO))]
        [System.Web.Http.Route(Headers.API_PREFIX + "UpdateCWAPlanning")]
        public IHttpActionResult UpdatePlanning(PutDTO planningToSave)
        {
            #region save/Update Body
            /* Body 
             
                   {
	"data": {
    "assets": [{
    "assetId":0,
    "assetNumber":"Test1",
    "status_B":"AC",
    "createdDate":"0001-01-01T00:00:00",
    "updatedDate":"0001-01-01T00:00:00",
    "timeZoneId":"Arabian Standard Time",
    "timeZoneTs":"04:00:00",
    "oprType":7,
    "YearManufactured":0,
    "PurchasedDate":"1900-01-01T00:00:00",
    "PurchasedValue":0.0,
    "RefA":"Test123",
    "RefB":"Test123",
    "RefC":"Test12",
    "ExternalRef":"1111123",
    "EngineSerialNumber":"2",
    "EngineCapacity":10,
    "EnginePower":12523,
    "createdUser":null,
    "updatedUser":null,
    "assetType":{
      "id":1,
      "description":"**Default**"
    },
    "make":{
      "id":23,
      "description":"Honda"
    },
    "model":{
      "id":47,
      "description":"Test"
    },
    "color":{
      "id":17,
      "description":"Red"
    },
    "vehicleType":{
      "id":3,
      "description":"TRUCK"
    },
    "assetDeviceMap":{
      "mapId":0,
      "assetId":0,
      "deviceId":"29",
      "serialNumber":"",
      "status_B":"AC",
      "isFuel":true,
      "oprType":4,
      "deviceAuxilliaryMap":
        {
          "uId":0,
          "mapId":0,
          "deviceId":"29",
          "fuelProduct":{
            "id":43,
            "description":"Gasoline"
          },
          "thresholdHigh":100,
          "thresholdLow":0,
          "tankCapacity":0,
          "oprType":4,
          "lCalibration":[
            {
              "iId":0,
              "assetId":0,
              "deviceId":"29",
              "auxId":13,
              "StdConsumption":1.0,
              "readingUOM":{
                "id":2,
                "description":"%"
              },
              "reading":100.0,
              "convertedUOM":{
                "id":5,
                "description":"Liters"
              },
              "convertedValue":80.0,
              "oprType":4
            },
            {
              "iId":0,
              "assetId":0,
              "deviceId":"29",
              "auxId":13,
              "StdConsumption":2.0,
              "readingUOM":{
                "id":2,
                "description":"%"
              },
              "reading":100.0,
              "convertedUOM":{
                "id":5,
                "description":"Liters"
              },
              "convertedValue":0.0,
              "oprType":4
            }]
        }
    },
    "lMatrix":[
      {
        "oprType":4,
        "mId":0,
        "iId":0,
        "gMId":4,
        "gMDesc":"Naveo"
      }],
    "lUOM":[
      {
        "iId":0,
        "assetId":0,
        "uom":{
                "id":2,
                "description":"%"
              },
        "capacity":100,
        "oprType":4
      
      }]
        }]
	},

	"oneTimeToken": "00000000-0000-0000-0000-000000000000",
	"screenMode": "Add"
}

             */

            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    PlanningDTO jsonSaveObject = serializer.Deserialize<PlanningDTO>(planningToSave.data.ToString());
                    BaseDTO planningSaveUpdate = new PlanningService().SaveUpdatePlanning(securityTokenObj, jsonSaveObject, false, string.Empty);
                    //CommonHelper.DefaultData(AssetSaveUpdate, securityTokenObj);
                    if (planningSaveUpdate.sQryResult == "Succeeded")
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "CWAPlanning";
                        string action = "UpdateCWAPlanning";
                        string rawRequest = planningToSave.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonSaveObject.lPlanning[0].pId, securityTokenObj.securityToken.sConnStr, true);
                    }
                    return Ok(planningSaveUpdate);
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        [System.Web.Http.HttpPost, ResponseType(typeof(PlanningDTO))]
        [System.Web.Http.Route(Headers.API_PREFIX + "ApproveCWAPlanningRequest")]
        public IHttpActionResult ApprovePlanningRequest(NaveoService.Models.Planning p)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel approvalProcess = new NaveoService.Services.PlanningService().ApprovalProcess(securityTokenObj.securityToken.UserToken, p.PID.Value, p.statusApproval, securityTokenObj.securityToken.sConnStr);
                    return Ok(approvalProcess.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [System.Web.Http.HttpPost, ResponseType(typeof(PlanningDTO))]
        [System.Web.Http.Route(Headers.API_PREFIX + "ViewApproveCWAPlanningRequest")]
        public IHttpActionResult ViewApprovePlanningRequest(NaveoService.Models.Planning p)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData approvalProcess = new NaveoService.Services.PlanningService().ViewTransportRequest(securityTokenObj.securityToken.UserToken, p.dateFrom, p.dateTo, p.PID, 1, securityTokenObj.securityToken.sConnStr, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage, p.sortColumns, p.sortOrderAsc);
                    return Ok(approvalProcess.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }



    }
}