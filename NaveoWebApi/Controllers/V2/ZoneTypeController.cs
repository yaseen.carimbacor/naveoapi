﻿using Microsoft.Web.Http;
using NaveoOneLib.Models;
using NaveoService.Services;
using NaveoWebApi.Constants;
using NaveoWebApi.Models;
using NaveoService.Models.DTO;
using NaveoWebApi.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Description;
using NaveoService.Models;
using NaveoService.Helpers;
using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Models.Common;
using System.Web.Script.Serialization;
using NaveoOneLib.Models.Zones;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class ZoneTypeController : ApiBaseController
    {
        BaseDTO baseDTO = new BaseDTO();

        #region API Methods
   

        [HttpPost, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "GetZoneTypes")]
        public IHttpActionResult GetZoneList(ZoneTypes z)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new NaveoService.Services.ZoneTypeService().GetZoneListFromZoneIDs(securityTokenObj.securityToken.UserToken, z.lZoneIDs, securityTokenObj.securityToken.sConnStr, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "GetZoneTypesById")]
        public IHttpActionResult GetZoneById(ZoneTypes lz)
        {
            /*Body 
           {
            "zoneId":[51]
            }
           */

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData dt = new ZoneTypeService().GetZoneTypeById(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr, lz.zoneId);
                    return Ok(dt.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "SaveZoneTypes")]
        public IHttpActionResult SaveZoneTypes(PutDTO zoneSave)
        {
           

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    ZoneType jsonSaveObject = serializer.Deserialize<ZoneType>(zoneSave.data.ToString());
                    Boolean bSave = new ZoneTypeService().SaveUpdate(jsonSaveObject, true, securityTokenObj.securityToken.sConnStr);
                    if (bSave)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "ZoneTypes";
                        string action = "SaveZoneTypes";
                        string rawRequest = zoneSave.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonSaveObject.ZoneTypeID, securityTokenObj.securityToken.sConnStr, true);
                    }
                    return Ok(bSave.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

  
        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "UpdateZoneTypes")]
        public IHttpActionResult UpdateZoneTypes(PutDTO zoneUpdate)
        {
            

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    ZoneType jsonSaveObject = serializer.Deserialize<ZoneType>(zoneUpdate.data.ToString());
                    Boolean bSave = new ZoneTypeService().SaveUpdate(jsonSaveObject, false, securityTokenObj.securityToken.sConnStr);
                    if (bSave)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "ZoneTypes";
                        string action = "UpdateZoneTypes";
                        string rawRequest = zoneUpdate.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonSaveObject.ZoneTypeID, securityTokenObj.securityToken.sConnStr, false);
                    }
                    return Ok(bSave.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "DeleteZoneTypesById")]
        public IHttpActionResult DeleteZoneById(lZones lz)
        {
            /*Body 
               {
                    "zoneId":[51]
               }
           */

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseDTO zd = new ZoneService().DeleteZoneById(securityTokenObj, lz.lZoneIDs);
                    if (zd.data)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Zone";
                        string action = "DeleteZoneById";
                        string rawRequest = "{'lZoneIDs':'" + lz.lZoneIDs.ToString() + "'}";
                        Telemetry.Trace(userToken, controller, action, rawRequest, lz.lZoneIDs[0], securityTokenObj.securityToken.sConnStr, true);
                    }
                    CommonHelper.DefaultData(zd, securityTokenObj);
                    return Ok(zd);
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        #endregion

       
    }
}