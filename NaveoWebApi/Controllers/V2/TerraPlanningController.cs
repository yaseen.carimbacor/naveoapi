﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.Web.Http;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.Plannings;
using NaveoService.Constants;
using NaveoService.Helpers;
using NaveoService.Models;
using NaveoService.Models.DTO;
using NaveoService.Services;
using NaveoWebApi.Helpers;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class TerraPlanningController : ApiBaseController
    {
        [System.Web.Http.HttpPost, ResponseType(typeof(PlanningDTO))]
        [System.Web.Http.Route(Headers.API_PREFIX + "GetTerraPlanningRequest")]
        public IHttpActionResult GetTerraPlanningRequest(NaveoService.Models.Planning p)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new NaveoService.Services.ScheduleService().GetTerraPlanningRequest(securityTokenObj.securityToken.UserToken, p.dateFrom, p.dateTo, securityTokenObj.securityToken.sConnStr, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage, p.sortColumns, p.sortOrderAsc);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [System.Web.Http.HttpPost, ResponseType(typeof(PlanningDTO))]
        [System.Web.Http.Route(Headers.API_PREFIX + "GetTerraPlanningByRequestCode")]
        public IHttpActionResult GetTerraPlanningByRequestCode(NaveoService.Models.Planning p)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel myData = new NaveoService.Services.ScheduleService().GetPlanningRequestByRquestCode(securityTokenObj.securityToken.UserToken,p.ParentRequestCode, securityTokenObj.securityToken.sConnStr);
                    return Ok(myData.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [System.Web.Http.HttpPost, ResponseType(typeof(PutDTO))]
        [System.Web.Http.Route(Headers.API_PREFIX + "SaveTerraPlanning")]
        public IHttpActionResult SaveTerraPlanning(PutDTO planningToSave)
        {
            #region save/Update Body
            /* Body 
    

             */

            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {

                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    PlanningDTO jsonSaveObject = serializer.Deserialize<PlanningDTO>(planningToSave.data.ToString());
                    BaseDTO planningSaveUpdate = new ScheduleService().SaveUpdateTerraPlanning(securityTokenObj, jsonSaveObject, true);
                    if (planningSaveUpdate.sQryResult == "Succeeded")
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "TerraPlanning";
                        string action = "SaveTerraPlanning";
                        string rawRequest = planningToSave.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonSaveObject.planning.pId, securityTokenObj.securityToken.sConnStr, true);
                    }
                    return Ok(planningSaveUpdate);
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [System.Web.Http.HttpPost, ResponseType(typeof(PutDTO))]
        [System.Web.Http.Route(Headers.API_PREFIX + "UpdateTerraPlanning")]
        public IHttpActionResult UpdateTerraPlanning(PutDTO planningToSave)
        {
            #region save/Update Body
            /* Body 
             
           */

            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    PlanningDTO jsonSaveObject = serializer.Deserialize<PlanningDTO>(planningToSave.data.ToString());
                    BaseDTO planningSaveUpdate = new ScheduleService().SaveUpdateTerraPlanning(securityTokenObj, jsonSaveObject, false);
                    if (planningSaveUpdate.sQryResult == "Succeeded")
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "TerraPlanning";
                        string action = "UpdateTerraPlanning";
                        string rawRequest = planningToSave.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonSaveObject.planning.pId, securityTokenObj.securityToken.sConnStr, true);
                    }
                    //CommonHelper.DefaultData(AssetSaveUpdate, securityTokenObj);
                    return Ok(planningSaveUpdate);
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [System.Web.Http.HttpPost, ResponseType(typeof(PutDTO))]
        [System.Web.Http.Route(Headers.API_PREFIX + "GetFieldCode")]
        public IHttpActionResult GetFieldCode(NaveoService.Models.Planning p)
        {
            #region save/Update Body
            /* Body 
             
           */

            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {

                    dtData mydtData = new NaveoOneLib.Services.Plannings.PlanningService().GetFieldCode(p.fieldCode, securityTokenObj.securityToken.sConnStr);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        [System.Web.Http.HttpPost, ResponseType(typeof(PutDTO))]
        [System.Web.Http.Route(Headers.API_PREFIX + "GetTerraScheduledDetails")]
        public IHttpActionResult GetTerraScheduleDetails(NaveoService.Models.Planning p)
        {
            #region save/Update Body
            /* Body 
             
           */

            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {

                    BaseModel mydtData = new ScheduleService().GetPlanningScheduledDetails(securityTokenObj.securityToken.UserToken,p.ParentRequestCode,securityTokenObj.securityToken.sConnStr);
                    return Ok(mydtData.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


    }
}