﻿using NaveoWebApi.Constants;
using NaveoWebApi.Controllers.V2;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using System.Web.Http;
using Microsoft.Web.Http;
using NaveoWebApi.Helpers;
using NaveoService.Models.ImportTemplates;
using NaveoOneLib.Constant;
using NaveoWebApi.Controllers.Filters;
using NaveoService.Models.DTO;
using NaveoService.Models;
using System.Configuration;
using NaveoService.Helpers;
using NaveoService.Constants;
using NaveoOneLib.Common;
using System.Web.Script.Serialization;
using NaveoWebApi.Models;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class ImportController : ApiBaseController
    {
        private static readonly string rootImportFolderPath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings[NaveoService.Constants.ConfigurationKeys.ImportLocation]);
        private readonly string planningDownloadTemplateFolder = Path.Combine(rootImportFolderPath, "Planning");
        private readonly string zoneDownloadTemplateFolder = Path.Combine(rootImportFolderPath, "Zone");


        /// <summary>
        /// Import data from Excel file  
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(NaveoService.Constants.Headers.API_PREFIX + "Upload")]
        public BaseDTO Import()
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            BaseDTO BD = new BaseDTO();

            if (securityTokenObj.securityToken.IsAuthorized)
            {
                Task<BaseDTO> b = new NaveoService.Services.ImportService().UploadFile(securityTokenObj);
                BD = b.Result;
            }
            else
                BD.errorMessage = securityTokenObj.securityToken.DebugMessage;

            return BD;
        }

       
        static string GetNestedStringFromStartPos(int StartPos, string str, char start, char end)
        {
            int s = -1;
            int i = StartPos;
            while (++i < str.Length)
                if (str[i] == start)
                {
                    s = i;
                    break;
                }
            int e = -1;
            int depth = 0;
            while (++i < str.Length)
                if (str[i] == end)
                {
                    e = i;
                    if (depth == 0)
                        break;
                    else
                        --depth;
                }
                else if (str[i] == start)
                    ++depth;
            if (e > s)
                return str.Substring(s + 1, e - s - 1);
            return null;
        }


        /// <summary>
        /// Import data from csv
        /// </summary>
        /// <returns></returns>
        public async Task<string> ImportCsv(string entity)
        {
            if (HttpContext.Current.Request.Files.AllKeys.Any())
            {
                // Get the posted file
                HttpPostedFile csvFile = HttpContext.Current.Request.Files[0];
                if (NaveoWebApi.Helpers.UtilityHelper.File.IsValid(csvFile) && csvFile != null)
                {
                    try
                    {
                        switch (entity)
                        {
                            case NaveoEntity.USERS:
                                List<UserTemplateRow> userTemplateRows = await NaveoService.Helpers.UtilityHelper.Document.FillUserCsvTemplateRowAsync(csvFile);
                                DataTable userDataTable = NaveoService.Helpers.UtilityHelper.Datatable.ToDataTable(userTemplateRows);
                                return JsonConvert.SerializeObject(userDataTable);
                            default:
                                //List<UserTemplateRow> userTemplateRows = await ExcelHelper.FillUserTemplateRowAsync(excelFile);
                                break;
                        }

                    }
                    catch (Exception e)
                    {
                        return "An error occured while filling the template rows or process on datatable has failed!!! " + e.Message;
                    }
                }
                return "File format not supported. Use a file with .csv extension";
            }
            return "No file selected! Please upload csv file";
        }


        [AllowAnonymous, HttpPost]
        [Route(NaveoService.Constants.Headers.API_PREFIX + "DownloadImportTemplate")]
        public IHttpActionResult Download(Entity entity)
        {
            try
            {
                SecurityTokenExtended securityTokenObj = ApiAuthentication();
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    string pathForDownnload = string.Empty;
                    string templateDocument = string.Empty;
                    if (entity.entityName == "Planning_Schedule")
                    {
                        pathForDownnload = planningDownloadTemplateFolder;
                        templateDocument = ImportTemplate.Planning_Schedule; //"Planning_Scheduling_Import_Template.zip"; 

                    }
                    else if (entity.entityName == "Zone")
                    {
                        pathForDownnload = zoneDownloadTemplateFolder;
                        templateDocument = ImportTemplate.Zone; //"Planning_Scheduling_Import_Template.zip";  //ImportTemplate.Planning_Schedule;
                    }

                    #region Download
                    // Check to prevent exception error
                    if (FileHelper.FileExists(pathForDownnload, FileSystem.Extension.ZIP))
                    {
                        if (!Directory.Exists(pathForDownnload))
                        {
                            Directory.CreateDirectory(pathForDownnload);
                        }

                        string fileToDownloadPath = Path.Combine(pathForDownnload, templateDocument);
                        DownloadZipFile(fileToDownloadPath);

                        return Ok(true.ToBaseDTO(securityTokenObj.securityToken));
                    }

                    #endregion
                    securityTokenObj.securityToken.DebugMessage = "No files to download";
                    return Ok(false.ToBaseDTO(securityTokenObj.securityToken)); // No files has been been download
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        private static void DownloadZipFile(string downloadPath)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ContentType = FileHelper.ContentType(FileSystem.Extension.ZIP);
            HttpContext.Current.Response.AppendHeader("content-disposition", "attachment;filename=reports.zip");

            // From Nabla
            HttpContext.Current.Response.AppendHeader("x-filename", "reports.zip");
            HttpContext.Current.Response.AppendHeader("Access-Control-Request-Method", "*");
            HttpContext.Current.Response.AppendHeader("Access-Control-Request-Headers", "*");
            HttpContext.Current.Response.AppendHeader("Access-Control-Allow-Origin", "*");
            HttpContext.Current.Response.AppendHeader("Access-Control-Allow-Methods", "*");
            HttpContext.Current.Response.AppendHeader("Access-Control-Allow-Headers", "*");

            HttpContext.Current.Response.TransmitFile(downloadPath);

            HttpContext.Current.Response.End();
        }


        [HttpPost()]
        [Route(NaveoService.Constants.Headers.API_PREFIX + "UploadFiles")]
        public string UploadFiles()
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
            string sPath = "";
            sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/locker/");

            HttpFileCollection hfc = HttpContext.Current.Request.Files;

            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!File.Exists(sPath + Path.GetFileName(hpf.FileName)))
                    {
                        // SAVE THE FILES IN THE FOLDER.
                        hpf.SaveAs(sPath + Path.GetFileName(hpf.FileName));
                        iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            // RETURN A MESSAGE (OPTIONAL).
            if (iUploadedCnt > 0)
            {
                return iUploadedCnt + " Files Uploaded Successfully";
            }
            else
            {
                return "Upload Failed";
            }
        }
    }
}
