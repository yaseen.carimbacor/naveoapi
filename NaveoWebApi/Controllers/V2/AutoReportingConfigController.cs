﻿using Microsoft.Web.Http;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Services.Reportings;
using NaveoService.Constants;
using NaveoService.Helpers;
using NaveoService.Models;
using NaveoService.Models.DTO;
using NaveoWebApi.Helpers;
using NaveoWebApi.Models;
using System;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Script.Serialization;


namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class AutoReportingConfigController : ApiBaseController
    {
        BaseDTO baseDTO = new BaseDTO();

        [System.Web.Http.AllowAnonymous, HttpGet, ResponseType(typeof(NaveoService.Models.Object))]
        [System.Web.Http.Route(Headers.API_PREFIX + "GetAutoReportingList")]
        public IHttpActionResult GetAutoReportingList()
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            if (securityTokenObj.securityToken.IsAuthorized)
            {

                dtData mydtData = new NaveoService.Services.AutoReportingConfigService().GetAutoReportingConfig(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr);
                return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
            }

            return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
        }


        [HttpPost, ResponseType(typeof(BaseModel))]
        [System.Web.Http.Route(Headers.API_PREFIX + "GetAutoReportingConfigById")]
        public IHttpActionResult GetAutoReportingConfigById(AutomaticReporting ar)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            if (securityTokenObj.securityToken.IsAuthorized)
            {

                BaseModel mydtData = new NaveoService.Services.AutoReportingConfigService().GetAutoReportingConfigByID(ar.arId, securityTokenObj.securityToken.sConnStr);
                return Ok(mydtData.ToNaveoOneLibBaseModelDTO(securityTokenObj.securityToken.OneTimeToken));
            }

            return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
        }

        /// <summary>
        /// Header example
        /// HOST_URL: mercury.naveo.mu
        /// USER_TOKEN: B8F03A2A-56A5-4501-9323-3BACFC35BE49
        /// [ONE_TIME_TOKEN]: 0c288a26-aa02-4f34-baff-078a9cab1ca7 
        /// </summary>
        /// <returns></returns>
        /// 
        [System.Web.Http.AllowAnonymous, System.Web.Http.HttpPost, ResponseType(typeof(PutDTO))]
        [System.Web.Http.Route(Headers.API_PREFIX + "SaveAutoReportingConfig")]
        public IHttpActionResult SaveAutoReportingConfig(PutDTO jsonObj)
        {
            #region Body
            /*Body
             {
	    "data": {
		"automaticReporting": [{
			"tree": [{
				"lTreeSelections": [{
					"group": "Assets",
					"lGroup": [{
						"type": "Parent",
						"lIds": [3]
					}]
				}]
			}],
			"recepients": {
				"type": "Child",
				"lIds": [14]
			},
			"other": [{
             				"id": "Total Type",
             				"value": "Total Engine Hours"
             			}],
			"targetId": null,
			"targetType": "V2",
			"reportId": 10,
			"reportPeriod": 3,
			"triggerTime": "11:00",
			"triggerInterval": "",
			"reportFormat": "test",
			"reportType": "Excel",
			"saveToDisk": 0


		}]
	},
	"oneTimeToken": "00000000-0000-0000-0000-000000000000",
	"screenMode": "Add"
}
             */
            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();

                    AutoReportingConfigDTO jsonSaveObject = serializer.Deserialize<AutoReportingConfigDTO>(jsonObj.data.ToString());
                    BaseDTO AutoReportingConfigSave = new NaveoService.Services.AutoReportingConfigService().SaveAutoReporting(securityTokenObj, jsonSaveObject, true);
                    if (AutoReportingConfigSave.sQryResult == "Succeeded")
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "AutoReportingConfig";
                        string action = "SaveAutoReportingConfig";
                        string rawRequest = jsonObj.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonSaveObject.automaticReporting[0].ARID, securityTokenObj.securityToken.sConnStr, true);
                    }
                    CommonHelper.DefaultData(AutoReportingConfigSave, securityTokenObj);
                    return Ok(AutoReportingConfigSave);
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        [System.Web.Http.AllowAnonymous, System.Web.Http.HttpPost, ResponseType(typeof(PutDTO))]
        [System.Web.Http.Route(Headers.API_PREFIX + "FTPHealthCheck")]
        public IHttpActionResult FTPHealthCheck(PutDTO jsonObj)
        {
            #region Body
            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();

                    //AutoReportingConfigDTO jsonSaveObject = serializer.Deserialize<AutoReportingConfigDTO>(jsonObj.data.ToString());
                    BaseDTO AutoReportingConfigSave = new NaveoService.Services.AutoReportingConfigService().FTPHealthCheck(jsonObj);
                    return Ok(AutoReportingConfigSave);
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }




        /// <summary>
        /// Header example
        /// HOST_URL: mercury.naveo.mu
        /// USER_TOKEN: B8F03A2A-56A5-4501-9323-3BACFC35BE49
        /// [ONE_TIME_TOKEN]: 0c288a26-aa02-4f34-baff-078a9cab1ca7 
        /// </summary>
        /// <returns></returns>
        /// 
        [System.Web.Http.AllowAnonymous, System.Web.Http.HttpPost, ResponseType(typeof(PutDTO))]
        [System.Web.Http.Route(Headers.API_PREFIX + "UAutoReportingConfig")]
        public IHttpActionResult UpdateAutoReportingConfig(PutDTO jsonObj)
        {
            #region Body
            /*Body
             {
	    "data": {
		"automaticReporting": [{
			"tree": [{
				"lTreeSelections": [{
					"group": "Assets",
					"lGroup": [{
						"type": "Parent",
						"lIds": [3]
					}]
				}]
			}],
			"recepients": {
				"type": "Child",
				"lIds": [14]
			},
			"other": [{
             				"id": "Total Type",
             				"value": "Total Engine Hours"
             			}],
			"targetId": null,
			"targetType": "V2",
			"reportId": 10,
			"reportPeriod": 3,
			"triggerTime": "11:00",
			"triggerInterval": "",
			"reportFormat": "test",
			"reportType": "Excel",
			"saveToDisk": 0


		}]
	},
	"oneTimeToken": "00000000-0000-0000-0000-000000000000",
	"screenMode": "Add"
}
             */
            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();

                    AutoReportingConfigDTO jsonSaveObject = serializer.Deserialize<AutoReportingConfigDTO>(jsonObj.data.ToString());
                    BaseDTO AutoReportingConfigSave = new NaveoService.Services.AutoReportingConfigService().SaveAutoReporting(securityTokenObj, jsonSaveObject, false);
                    CommonHelper.DefaultData(AutoReportingConfigSave, securityTokenObj);
                    return Ok(AutoReportingConfigSave);
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        [AllowAnonymous, HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "DeleteReportingConfig")]
        public IHttpActionResult DeleteReportingConfig(AutomaticReporting ar)
        {
            /*Body 
             {
                "arId":51
              }
             */

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel deleteAutoConfig = new NaveoService.Services.AutoReportingConfigService().DeleteAutoReportingConfig(securityTokenObj, ar.arId);
                    return base.Ok(deleteAutoConfig.ToNaveoOneLibBaseModelDTO(securityTokenObj.securityToken.OneTimeToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }
    }
}