﻿using Microsoft.Web.Http;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Services.Devices;
using NaveoService.Helpers;
using NaveoService.Models;
using NaveoWebApi.Constants;
using NaveoWebApi.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class DeviceController : ApiBaseController
    {
        #region API Methods
        [HttpGet, ResponseType(typeof(NaveoService.Models.Object))]
        [Route(Headers.API_PREFIX + "GetDevices")]
        public IHttpActionResult GetDevices()
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData obj = new DeviceService().getDevices();
                    return Ok(obj.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion
    }
}