﻿using Microsoft.Web.Http;
using NaveoOneLib.Maps;
using NaveoOneLib.Models.BLUP;
using NaveoOneLib.Models.Common;
using NaveoService.Helpers;
using NaveoService.Models;
using NaveoService.Models.DTO;
using NaveoWebApi.Constants;
using NaveoWebApi.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class MapsController : ApiBaseController
    {
        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "GetAddressByLonLat")]
        public IHttpActionResult GetAddressByLonLat(Blup B)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel bm = new BaseModel();
                    bm.data = SimpleAddress.GetAddresses(B.lon, B.lat);
                    return base.Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }
    }
}