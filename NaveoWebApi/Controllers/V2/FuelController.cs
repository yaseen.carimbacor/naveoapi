﻿using Microsoft.Web.Http;
using NaveoOneLib.Models;
using NaveoOneLib.Models.Permissions;
using NaveoService.Services;
using NaveoWebApi.Constants;
using NaveoService.Helpers;
using NaveoWebApi.Helpers;
using NaveoWebApi.Models;
using NaveoService.Models.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using NaveoService.Models;
using NaveoOneLib.Models.Common;
using Fuel = NaveoService.Models.Fuel;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class FuelController : ApiBaseController
    {
        #region API Methods
        /// <summary> 
        /// Get trip details
        /// <para>BODY EXAMPLE</para>
        /// <para>{</para>
        ///   <para>"assetIds": [25] </para>
        ///   <para>"dateFrom": "2017-10-05 01:01:01"</para>
        ///   <para>"dateTo": "2017-10-05 18:20:20" </para>
        /// <para>}</para>
        /// <para>HEADERS EXAMPLE</para>
        /// <para>HOST_URL: mercury.naveo.mu</para>
        /// <para>USER_TOKEN: B8F03A2A-56A5-4501-9323-3BACFC35BE49</para> 
        /// <para>[ONE_TIME_TOKEN]: A8F453S2A-24C5-4320-4NAQWE3AK22</para> 
        /// <para>PAGE: 1</para>
        /// <para>ROWS_PER_PAGE: 5</para>
        /// </summary> 
        /// <returns></returns>
        [HttpPost, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "GetFuelData")]
        public IHttpActionResult GetFuelData(Fuel f)
        {
            /*
            https://localhost:44393/v2.0/GetFuelData

            Headers
            Content - Type:application / json
            USER_TOKEN: 2c5c384f - 4a54 - 4f06 - aca6 - ea0c89c32f15

            HOST_URL: demo.naveo.mu
             //ONE_TIME_TOKEN:1ade61f2-94ed-422f-86be-c1e5abe0c1ac

             Body
             {
                "assetId": 25,
                "dateFrom": "2017-10-05 01:01:01",
                "dateTo": "2017-10-05 18:20:20"
             }
             */

            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;
            if (f == null)
                sError = "Missing parameters 0";
            else if (f.dateFrom == null)
                sError = "Missing parameters 2";
            else if (f.dateTo == null)
                sError = "Missing parameters 3";

            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    DataTable dtFuel = new FuelService().GetFuelData(securityTokenObj.securityToken.UserToken, f.assetID, f.dateFrom, f.dateTo, securityTokenObj.securityToken.sConnStr);
                    return base.Ok(dtFuel.ToFuelDataDTO(securityTokenObj.securityToken));
                }
                return base.Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "GetFuelConsumption")]
        public IHttpActionResult GetFuelConsumption(Fuel f)
        {
            /*
            https://localhost:44393/v2.0/GetFuelData

            Headers
            Content - Type:application / json
            USER_TOKEN: 2c5c384f - 4a54 - 4f06 - aca6 - ea0c89c32f15

            HOST_URL: demo.naveo.mu
             //ONE_TIME_TOKEN:1ade61f2-94ed-422f-86be-c1e5abe0c1ac

             Body
             {
                "assetId": 25,
                
             }
             */

            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;
            if (f == null)
                sError = "Missing parameters 0";
            else if (f.assetID == 0)
                sError = "Missing parameters 1";


            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    if (!f.page.HasValue)
                    {
                        f.page = securityTokenObj.securityToken.Page;
                        f.limitPerPage = securityTokenObj.securityToken.LimitPerPage;
                    }
                    BaseModel dtFuelConsumption = new FuelService().GetFuelConsumption(securityTokenObj.securityToken.UserToken, f.assetID, f.page, f.limitPerPage, securityTokenObj.securityToken.sConnStr);
                    return base.Ok(dtFuelConsumption.ToBaseDTO(securityTokenObj.securityToken));
                }
                return base.Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "GetPossibleFuelDrops")]
        public IHttpActionResult GetPossibleFuelDrops(Fuel f)
        {
            /*
            https://localhost:44393/v2.0/GetFuelData

            Headers
            Content - Type:application / json
            USER_TOKEN: 2c5c384f - 4a54 - 4f06 - aca6 - ea0c89c32f15

            HOST_URL: demo.naveo.mu
             //ONE_TIME_TOKEN:1ade61f2-94ed-422f-86be-c1e5abe0c1ac

             Body
             {
                "fuelDataUID": 25,
                
             }
             */

            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;
            if (f == null)
                sError = "Missing parameters 0";
            else if (f.fuelDataUID == 0)
                sError = "Missing parameters 1";


            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel dtPossibleFuelDrops = new FuelService().GetPossibleFuelDrops(securityTokenObj.securityToken.UserToken, f.fuelDataUID, securityTokenObj.securityToken.sConnStr);
                    return base.Ok(dtPossibleFuelDrops.ToBaseDTO(securityTokenObj.securityToken));
                }
                return base.Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "GetFuelConsumptionReport")]
        public IHttpActionResult GetFuelConsumptionReport(Fuel f)
        {
            /*
            https://localhost:44393/v2.0/GetFuelData

            Headers
            Content - Type:application / json
            USER_TOKEN: 2c5c384f - 4a54 - 4f06 - aca6 - ea0c89c32f15

            HOST_URL: demo.naveo.mu
             //ONE_TIME_TOKEN:1ade61f2-94ed-422f-86be-c1e5abe0c1ac

             Body
             {
                "assetId": 25,
                
             }
             */

            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;
            if (f == null)
                sError = "Missing parameters 0";
            else if (f.lassetIds.Count == 0)
                sError = "Missing parameters 1";


            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel dtFuelConsumption = new FuelService().GetFuelConsumptionReport(securityTokenObj.securityToken.UserToken, f.dtFrom, f.dtTo, f.lassetIds, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage, securityTokenObj.securityToken.sConnStr, f.sortColumns, f.sortOrderAsc);
                    return base.Ok(dtFuelConsumption.ToBaseDTO(securityTokenObj.securityToken));
                }
                return base.Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }

        [HttpPost, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "GetPeriodicFuelSummary")]
        public IHttpActionResult GetPeriodicFuelSummary(Fuel f)
        {


            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;
            if (f == null)
                sError = "Missing parameters 0";
            else if (f.lassetIds.Count == 0)
                sError = "Missing parameters 1";


            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel dtFuelConsumption = new FuelService().GetPeriodicFuelSummary(securityTokenObj.securityToken.UserToken, f.dtFrom, f.dtTo, f.lassetIds, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage, securityTokenObj.securityToken.sConnStr, f.sortColumns, f.sortOrderAsc);
                    return base.Ok(dtFuelConsumption.ToBaseDTO(securityTokenObj.securityToken));
                }
                return base.Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }



        [HttpPost, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "GetFuelReport")]
        public IHttpActionResult GetFuelReport(Fuel f)
        {


            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;
            if (f == null)
                sError = "Missing parameters 0";
            else if (f.lassetIds.Count == 0)
                sError = "Missing parameters 1";


            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel dtFuelConsumption = new FuelService().GetFuelReport(securityTokenObj.securityToken.UserToken, f.dtFrom.Value, f.dtTo.Value, f.lassetIds, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage, securityTokenObj.securityToken.sConnStr);
                    return base.Ok(dtFuelConsumption.ToBaseDTO(securityTokenObj.securityToken));
                }
                return base.Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        #endregion
    }
}