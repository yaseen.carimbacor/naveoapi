﻿using NaveoService.Models;
using NaveoWebApi.Helpers;
using NaveoService.Models.DTO;
using NaveoWebApi.Constants;
using NaveoService.Helpers;
using NaveoOneLib.Models.Common;
using System;
using System.Web.Http;
using Microsoft.Web.Http;
using NaveoOneLib.Models.Maps;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class RoutingController: ApiBaseController
    {
        BaseDTO baseDTO = new BaseDTO();

        #region CreateRoutingMapData
        [HttpPost, AllowAnonymous]
        [Route(Headers.API_PREFIX + "CreateRoute")]
        public IHttpActionResult CreateRoute(RoutingData r)
        {
            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    r.UserToken = securityTokenObj.securityToken.UserToken;
                    String responseString = NaveoOneLib.Maps.NaveoWebMapsCall.CreateRouting(r);

                    BaseModel RoutingData = new BaseModel();
                    RoutingData.data = responseString;

                    return base.Ok(RoutingData.ToBaseDTO(securityTokenObj.securityToken));
                }
                return base.Ok(baseDTO.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }
        #endregion

    }
}