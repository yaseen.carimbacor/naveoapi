﻿using System.Web.Http;
using Microsoft.Web.Http;
using System;
using NaveoOneLib.Models;
using Newtonsoft.Json;
using System.Web.Http.Description;
using System.Web;
using System.Text;
using System.Web.Hosting;
using System.Net;
using NaveoWebApi.Models;
using NaveoWebApi.Helpers;
using System.Net.Http.Headers;
using NaveoWebApi.Constants;
using NaveoService.Services;
using NaveoService.Models;
using System.Web.Security;
using System.Collections.Generic;
using System.Linq;
using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Models.Users;
using NaveoOneLib.Services.Users;
using NaveoService.Models.DTO;
using NaveoService.Models.Custom;
using NaveoService.Helpers;
using NaveoOneLib.Models.Common;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class AccountController : ApiBaseController
    {
        #region API Methods
        //Does not require OTT but requires company token
        #region Login
        [AllowAnonymous, HttpPost, ResponseType(typeof(LoginDTO))]
        [Route(Headers.API_PREFIX + "Login")]
        public IHttpActionResult Login(siteLogin sl)
        {
            //Body
            /*
                {
	                "user": {
		                "email": "Support@naveo.mu",
		                "password": "Test1234."
	                }
                    , "companyToken" : "hA2.*c9x6a2PgaZMi_6Sg328@1pc35p9"
                }            
           */

            //Header
            /*
             * [{"key":"Content-Type","value":"application/json","description":""}]
              
                HOST_URL:Demo.Naveo.mu
                Content-Type:application/json
             */
            //Check if all parameters are available. If not, return error message

            string origin = "localhost";
            if (Request.Headers.Contains("Origin"))
                origin = Request.Headers.GetValues("Origin").First();

            String sError = String.Empty;
            if (sl == null)
                sError = "Missing parameters 0";
            else if (sl.user == null)
                sError = "Missing parameters 2";
            else if ((string.IsNullOrEmpty(sl.user.email)))
                sError = "Missing email";
            else if ((string.IsNullOrEmpty(sl.user.password)))
                sError = "Missing password";
            else if ((string.IsNullOrEmpty(sl.companyToken)))
                sError = "Missing Company Token";

            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            string sConnStr = CommonHelper.GetCurrentConnStr(HttpContext.Current.Request.Url.DnsSafeHost, Request.Headers);
            String CurrentPath = HostingEnvironment.ApplicationPhysicalPath;
            //check if selected db exists
            if (!FileHelper.DoesLineExist(CurrentPath + "\\Registers\\databases.dat", sConnStr))
                return Content(HttpStatusCode.Forbidden, "Wrong Database");

            //check if company's token is valid
            if (!FileHelper.ChkCompanyToken(CurrentPath + "\\Registers\\companies.dat", sl.companyToken))
                return Content(HttpStatusCode.Forbidden, "Unregistered Company");

            var context = new HttpContextWrapper(HttpContext.Current);
            HttpRequestBase request = context.Request;

            IPHostEntry ipHostEntry = Dns.GetHostEntry(string.Empty);
            IPAddress[] ipAdd = ipHostEntry.AddressList;


            try
            {

                sl.user.password = NaveoOneLib.Services.BaseService.RSADecrypt(sl.user.password);

            } catch(Exception ex)
            {
                sl.user.password = sl.user.password;
            }

            NaveoOneLib.Models.Users.User u = new NaveoOneLib.Services.Users.UserService().GetUserByeMail(sl.user.email, sl.user.password, sConnStr, false, false, request.IsSecureConnection, origin, ipAdd);
            int atp = u.LoginAttempt;

            if (u.QryResult == NaveoOneLib.Models.Users.User.UserResult.InvalidCredentials)
            {
                u = new NaveoOneLib.Models.Users.User();
                u.QryResult = NaveoOneLib.Models.Users.User.UserResult.InvalidCredentials;
                u.sQryResult = u.QryResult.ToString();
            }

            SecurityTokenExtended securityTokenObj = new SecurityTokenExtended(false);


            switch (u.QryResult)
            {
                case NaveoOneLib.Models.Users.User.UserResult.Succeeded:
                    securityTokenObj = GenerateAuthToken(Guid.NewGuid(), u.UserToken, u.Email, sConnStr);
                    u.Dummy1 = NaveoOneLib.Maps.NaveoWebMapsCall.CreateZones(securityTokenObj.securityToken.UserToken, sConnStr);
                    if (sl.companyToken == "bL9-pmM!cyGo*dJ3o8rRMPjOj0t5bECZ#")
                        new NaveoOneLib.Services.Users.UserService().AllowMobileAccess(u.UID, sConnStr);


                    #region live connections
                    try
                    {
                        
                        LiveConnections lc =  new NaveoOneLib.Services.Users.UserService().GetLiveConnectionsByUser(u.UserToken, sConnStr);
                        lc.FleetLogin = 1;
                        lc.oprType = System.Data.DataRowState.Modified;
                        BaseModel bM = new NaveoOneLib.Services.Users.UserService().SaveLiveConnections(lc,false,sConnStr);


                    }
                    catch(Exception e)
                    {
                        throw e;
                    }
                    #endregion


                    break;

                case NaveoOneLib.Models.Users.User.UserResult.SucceededButPswdToExpire:
                    securityTokenObj = GenerateAuthToken(Guid.NewGuid(), u.UserToken, u.Email, sConnStr);
                    break;

                case NaveoOneLib.Models.Users.User.UserResult.PasswordExpired:
                    securityTokenObj = GenerateAuthToken(Guid.NewGuid(), u.UserToken, u.Email, sConnStr);
                    break;

                case NaveoOneLib.Models.Users.User.UserResult.TokenExpected:
                    u.sQryResult = NaveoOneLib.Models.Users.User.UserResult.TokenExpected.ToString();
                    securityTokenObj = GenerateAuthToken(Guid.NewGuid(), u.UserToken, u.Email, sConnStr);
                    if (u.TwoStepVerif != null && u.TwoStepVerif != "")
                    {
                        if (sl.user.password == u.TwoStepVerif)
                        {
                            u.Status_b = "AC";
                            new NaveoOneLib.Services.Users.UserService().UpdateUserStatus(u, null, sConnStr);
                            u.sQryResult = NaveoOneLib.Models.Users.User.UserResult.Succeeded.ToString();
                        }
                        else
                        {
                            u.sQryResult = NaveoOneLib.Models.Users.User.UserResult.InvalidToken.ToString();
                        }
                    }
                    break;

                case NaveoOneLib.Models.Users.User.UserResult.InvalidCredentials:
                    int iAttemps = 3 - u.LoginAttempt;
                    if (iAttemps > 0)
                        ModelState.AddModelError("", NaveoWebApi.Helpers.CommonHelper.myMessageBox(iAttemps + " More Attempts", "lock"));
                    break;

                case NaveoOneLib.Models.Users.User.UserResult.InvalidToken:
                    int iTokenAttemps = 3 - u.TwoStepAttempts;
                    if (iTokenAttemps > 0)
                        ModelState.AddModelError("", NaveoWebApi.Helpers.CommonHelper.myMessageBox(iTokenAttemps + " More Attempts", "lock"));
                    break;

                case NaveoOneLib.Models.Users.User.UserResult.Locked:
                    break;

                case NaveoOneLib.Models.Users.User.UserResult.Inactive:
                    break;

                case NaveoOneLib.Models.Users.User.UserResult.CouldNotMailToken:
                    break;
            }



            Telemetry.Trace(sl.user.email, "Login", securityTokenObj.securityToken.IsAuthorized, sConnStr);
            return Ok(castUser(u, securityTokenObj.securityToken.OneTimeToken, atp));
        }
        #endregion

        [AllowAnonymous, HttpPost, ResponseType(typeof(BaseModel))]
        [Route(Headers.API_PREFIX + "MaintenanceLoginAPI")]
        public IHttpActionResult MaintenanceLoginAPI(ServerName pServerName)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel bm = new NaveoOneLib.Services.Users.UserService().GetMaintenanceLogIn(securityTokenObj.securityToken.UserToken, pServerName.sServerName);
                    return Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        #region Logout
        [AllowAnonymous, HttpPost, ResponseType(typeof(LoginDTO))]
        [Route(Headers.API_PREFIX + "Logout")]
        public IHttpActionResult Logout()
        {
            string sConnStr = CommonHelper.GetCurrentConnStr(HttpContext.Current.Request.Url.DnsSafeHost, Request.Headers);
            String CurrentPath = HostingEnvironment.ApplicationPhysicalPath;
            //check if selected db exists
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            BaseDTO bS = new NaveoService.Services.UserService().logoutUserByToken(securityTokenObj, sConnStr);

            #region live connections
            try
            {
                if(bS.data == true )
                {

                    LiveConnections lc = new NaveoOneLib.Services.Users.UserService().GetLiveConnectionsByUser(securityTokenObj.securityToken.UserToken, sConnStr);

                    if (lc.MaintenanceLogin == 1)
                    {
                        bS.data = false;

                        string message = "Your session in Garage Management Sytem (GMS) is still active , Please log out from the GMS platform in order to log out from NAVEO FMS.";
                        bS.errorMessage = message;
                    }
                    else
                    {
                        lc.FleetLogin = 0;
                        lc.oprType = System.Data.DataRowState.Modified;
                        BaseModel bM = new NaveoOneLib.Services.Users.UserService().SaveLiveConnections(lc, false, sConnStr);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            #endregion

            return Ok(bS);
            //return Ok(castUser(u, securityTokenObj.securityToken.OneTimeToken));
        }
        #endregion

        //Does not require OTT but requires company token
        #region RequestPasswordReset
        [AllowAnonymous]
        [HttpPost]
        [ResponseType(typeof(AccountDTO))]
        [Route(Headers.API_PREFIX + "RequestPasswordReset")]
        public IHttpActionResult RequestPasswordReset(siteLogin cp)
        {
            //headers:
            //USER_TOKEN: a9d5ed8d-ad38-442f-898e-f351dafcc1cb
            //HOST_URL:demo.naveo.mu
            //Content - Type:application / json

            //Body:
            //    {
            //    "user": {
            //        "email": "support@naveo.mu"
            //        },

            //     "companyToken": "hA2.*c9x6a2PgaZMi_6Sg328@1pc35p9"
            //    }
            //Check if all parameters are available. If not, return error message
            String errors = String.Empty;
            if (cp == null)
                errors += "Missing parameters 0";
            else if (cp.user == null)
                errors += "Missing parameters 2";
            else if ((string.IsNullOrEmpty(cp.user.email)))
                errors += "Missing mail";
            else if ((string.IsNullOrEmpty(cp.companyToken)))
                errors = "Missing Company Token";

            if (!String.IsNullOrEmpty(errors))
                return Content(HttpStatusCode.Forbidden, errors);

            //check if company's token is valid
            String CurrentPath = HostingEnvironment.ApplicationPhysicalPath;
            if (!FileHelper.ChkCompanyToken(CurrentPath + "\\Registers\\companies.dat", cp.companyToken))
                return Content(HttpStatusCode.Forbidden, "Unregistered Company");

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            var servername = CommonHelper.GetCurrentHostURL(HttpContext.Current.Request.Url.DnsSafeHost, Request.Headers);
            String s = new NaveoOneLib.Services.Users.UserService().RequestPasswordReset(0, cp.user.email, false, CommonHelper.GetCurrentConnStr(HttpContext.Current.Request.Url.DnsSafeHost, Request.Headers), servername);
            return Ok(castData(s, "RequestPasswordReset", securityTokenObj));
        }

        [AllowAnonymous, HttpGet, ResponseType(typeof(String))]
        [Route(Headers.API_PREFIX + "ResetPassword")]
        public IHttpActionResult ResetPassword(String sTokenID)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            //if (securityTokenObj.securityToken.IsAuthorized)
            //{
            Boolean b = new NaveoOneLib.Services.Users.UserService().ResetPassword(sTokenID, CommonHelper.GetCurrentConnStr());
            if (b)
                return Ok(castData(b, "ResetPassword", securityTokenObj));
            //}
            return Content(HttpStatusCode.Forbidden, castData(false, String.Empty, securityTokenObj));
        }
        #endregion

        #region ChangePassword
        [AllowAnonymous, HttpPost, ResponseType(typeof(AccountDTO))]
        [Route(Headers.API_PREFIX + "ChangePassword")]
        public IHttpActionResult ChangePassword(siteLogin sl)
        {
            //input
            //user.password - existing
            //cp other fields
            /*
            {
                    "user": {
		                "password": "Test1234."
		                "oldPassword": "Test1234."  //Optional
	                }
            }

            headers: 
                USER_TOKEN:075c3dda-a095-4353-8f8c-96fa09339c3b
                HOST_URL:demo.naveo.mu
                Content-Type:application/json
             */
            String sError = String.Empty;
            if (sl == null)
                sError = "Missing parameters 0";
            else if (sl.user == null)
                sError = "Missing parameters 1";
            else if ((string.IsNullOrEmpty(sl.user.password)))
                sError = "Missing password";

            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            if (securityTokenObj.securityToken.IsAuthorized)
            {
                String OldPassword = NaveoOneLib.Models.Users.User.BypassOldPswd;
                if (!string.IsNullOrEmpty(sl.user.oldPassword))
                    OldPassword = sl.user.oldPassword;
                Boolean b = new NaveoService.Services.UserService().ChangePassword(securityTokenObj.securityToken.UserToken.ToString(), OldPassword, sl.user.password, securityTokenObj.securityToken.sConnStr);
                if (b)
                    return Ok(castData(b, "ChangePassword", securityTokenObj));
                return Content(HttpStatusCode.Forbidden, castData(b, String.Empty, securityTokenObj));
            }
            SecurityTokenExtended defaultSecurityToken = new SecurityTokenExtended(SecurityHelper.DEFAULT_GUID);
            return Content(HttpStatusCode.Forbidden, castData(false, String.Empty, defaultSecurityToken));
        }
        #endregion

        #region getActivityTimeout
        [AllowAnonymous, HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "getActivityTimeout")]
        public IHttpActionResult getActivityTimeout()
        {
            NaveoOneLib.Services.GlobalParamsService globalParamsService = new NaveoOneLib.Services.GlobalParamsService();
            string sConnStr = CommonHelper.GetCurrentConnStr(HttpContext.Current.Request.Url.DnsSafeHost, Request.Headers);

            int b = Convert.ToInt32(globalParamsService.GetGlobalParamsByName("IDLEWAIT", sConnStr).PValue);
            int c = Convert.ToInt32(globalParamsService.GetGlobalParamsByName("MaxLoginAttempts", sConnStr).PValue);
            return Ok(new BaseDTO() { 
                sQryResult = "Succeeded",
                data = new ActivityDTO()
                {
                    idleWait = b,
                    maxLoginAttempts = c
                }
            });
        }


        [AllowAnonymous, HttpGet, ResponseType(typeof(AccountDTO))]
        [Route(Headers.API_PREFIX + "getMaintenanceConnectivity")]
        public IHttpActionResult getMaintenanceConnectivity()
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            if (securityTokenObj.securityToken.IsAuthorized)
            {
                
               LiveConnections lc = new NaveoOneLib.Services.Users.UserService().GetLiveConnectionsByUser(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr);
               BaseDTO baseDTO = new BaseDTO();

               Boolean b = false;

               if (lc.MaintenanceLogin == 1)
                          b = true;



               baseDTO.data = b;
              return base.Ok(baseDTO);


            }


            SecurityTokenExtended defaultSecurityToken = new SecurityTokenExtended(SecurityHelper.DEFAULT_GUID);
            return Content(HttpStatusCode.Forbidden, castData(false, String.Empty, defaultSecurityToken));
        }

        #endregion

        #region getAPIAuthenticatedUsers
        /// <summary> 
        /// getAPIAuthenticatedUsers
        /// <para>HEADERS EXAMPLE</para>
        /// <para>HOST_URL: mercury.naveo.mu</para>
        /// <para>USER_TOKEN: B8F03A2A-56A5-4501-9323-3BACFC35BE49</para> 
        /// <para>[ONE_TIME_TOKEN]: A8F453S2A-24C5-4320-4NAQWE3AK22</para> 
        /// </summary> 
        /// <returns></returns>
        [AllowAnonymous, HttpGet, ResponseType(typeof(NaveoService.Models.Object))]
        [Route(Headers.API_PREFIX + "getAPIAuthenticatedUsers")]
        public IHttpActionResult getAPIAuthenticatedUsers()
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    List<string> l = new List<string>();
                    foreach (AllowedUser au in AuthToken.allowedUsers)
                        l.Add("GUID : " + au.issuedTo.ToString() + ", issuedDate : " + au.issuedDate + ", expiryDate : " + au.expiryDate + ", AccessedAt : " + au.accessedAt + "; Token : " + au.oneTimeToken);

                    ObjectDTO d = new ObjectDTO();
                    d.data = l;
                    d.oneTimeToken = securityTokenObj.securityToken.OneTimeToken;
                    return base.Ok(d);
                }
                return base.Ok(new BaseDTO().Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        #endregion

        #region initToken
        /// <summary> 
        /// initToken after expiry\timeout
        /// <para>HEADERS EXAMPLE</para>
        /// <para>HOST_URL: mercury.naveo.mu</para>
        /// <para>USER_TOKEN: B8F03A2A-56A5-4501-9323-3BACFC35BE49</para> 
        /// <para>[ONE_TIME_TOKEN]: A8F453S2A-24C5-4320-4NAQWE3AK22</para> 
        /// </summary> 
        /// <returns></returns>
        [AllowAnonymous, HttpGet, ResponseType(typeof(NaveoService.Models.Object))]
        [Route(Headers.API_PREFIX + "initToken")]
        public IHttpActionResult initToken(Guid gUserToken)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (!securityTokenObj.securityToken.IsAuthorized)
                {
                    string sConnStr = CommonHelper.GetCurrentConnStr(HttpContext.Current.Request.Url.DnsSafeHost, Request.Headers);

                    String sEmail = String.Empty;
                    try
                    {
                        sEmail = NaveoOneLib.Services.Users.UserService.GetEmailByUserToken(gUserToken, sConnStr);
                    }
                    catch { }
                    securityTokenObj = GenerateAuthToken(Guid.NewGuid(), gUserToken, sEmail, sConnStr);

                    ObjectDTO d = new ObjectDTO();
                    d.data = true;
                    d.oneTimeToken = securityTokenObj.securityToken.OneTimeToken;
                    d.errorMessage = Headers.AUTHORIZED;
                    d.sQryResult = Headers.AUTHORIZED;
                    return base.Ok(d);
                }
                return base.Ok(new BaseDTO().Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        #endregion
        #endregion

        #region Private Methods
        private AccountDTO castData(Boolean b, String sProvider, SecurityTokenExtended securityTokenObj, int interval = 0)
        {
            String msg = String.Empty;

            Account am = new Account();
            if (sProvider == "RequestPasswordReset")
            {
                if (b)
                    msg = "Password reset successfully. Please check mail to login using one time generated password";
            }
            else if (sProvider == "ResetPassword")
            {
                if (b)
                    msg = "Password reset successfully.";
            }
            else if (sProvider == "ChangePassword")
            {
                if (b)
                    msg = "Password changed successfully.";
            }
            else if (sProvider == "getActivityTimeout")
            {
                if (b)
                    am.interval = interval;
            }

            am.success = b;
            am.message = msg;

            return am.ToAccountDTO(securityTokenObj.securityToken);
        }
        private AccountDTO castData(String s, String sProvider, SecurityTokenExtended securityTokenObj)
        {
            Boolean b = false;
            String msg = String.Empty;
            if (sProvider == "RequestPasswordReset")
            {
                if (s == "Mail Sent")
                {
                    b = true;
                    msg = "Password reset successfully. Please check mail to login using one time generated password";
                }
                else
                {
                    b = true;
                    msg = "Password reset failed. Please Verify the email address";
                }

            }

            Account am = new Account();
            am.success = b;
            am.message = msg;

            return am.ToAccountDTO(securityTokenObj.securityToken);
        }

        private LoginDTO castUser(NaveoOneLib.Models.Users.User u, Guid oneTimeToken, int loginAttempt)
        {
            Models.User au = new Models.User();
            au.userId = u.UID;
            au.userToken = u.UserToken;
            au.firstName = u.Names;
            au.lastName = u.LastName;
            au.email = u.Email;
            au.lastLogin = u.lastLogin;
            au.lastLogout = u.lastLogout;

            Models.SystemAdmin systemAdmin = new Models.SystemAdmin();
            systemAdmin.email = u.SystemAdminEmail;
            systemAdmin.phone = u.SystemAdminPhone;

            LoginResult ar = new LoginResult();
            ar.user = au;
            ar.loginAttempts = loginAttempt;

            LoginDTO d = new LoginDTO();
            d.data = ar;
            d.additionalData = systemAdmin;
            d.oneTimeToken = oneTimeToken;
            d.sQryResult = u.sQryResult;
            //d.errorMessage = u.QryResult.ToString();
            d.MapServerResponse = u.Dummy1;

            return d;
        }

        /// <summary>
        /// Generates OTT after successful login
        /// </summary>
        /// <param name="token"></param>
        /// <param name="sConnStr"></param>
        /// <returns></returns>
        private static SecurityTokenExtended GenerateAuthToken(Guid token, Guid userToken, string sEmail, string sConnStr)
        {
            SecurityTokenExtended tokenObj = new SecurityTokenExtended(false);
            try
            {
                // Caters for the case that a session gets killed (Remove assigned token on next login)
                #region Killed Session management
                AllowedUser allowedUser = AuthToken.allowedUsers.SingleOrDefault(x => x.issuedTo == userToken);
                if (allowedUser != null)
                {
                    AuthToken.allowedUsers.Remove(allowedUser);
                    Telemetry.Trace(allowedUser.sEmail, "Logout", true, sConnStr);
                }
                #endregion

                tokenObj.securityToken.IsAuthorized = true;
                tokenObj.securityToken.OneTimeToken = token;
                tokenObj.securityToken.UserToken = userToken;
                tokenObj.securityToken.IsExpired = false;

                int UID = new NaveoOneLib.Services.Users.UserService().GetUserID(userToken, sConnStr);
                tokenObj.securityToken.UID = UID;

                AuthToken.allowedUsers.Add(
                    new AllowedUser(userToken, UID, token, sEmail)
                );

                Telemetry.Trace(sEmail, "Login", true, sConnStr);

                return tokenObj;
            }
            catch (System.Exception)
            {
                return tokenObj;
            }
        }
        #endregion
    }
}