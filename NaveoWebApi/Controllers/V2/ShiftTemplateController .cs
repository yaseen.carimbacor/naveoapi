﻿using NaveoOneLib.Models;
using NaveoService.Constants;
using NaveoService.Services;
using NaveoService.ViewModel;
using NaveoWebApi.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;
using Microsoft.Web.Http;
using NaveoWebApi.Constants;
using System.Web.Http.Description;
using System.Net.Http;
using System.Net;
using System.IO;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using NaveoService.Helpers;
using NaveoOneLib.Models.Common;
using NaveoService.Models.DTO;
using NaveoOneLib.Models.Permissions;
using NaveoService.Models;
using NaveoWebApi.Models;
using System.Web.Script.Serialization;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class ShiftTemplateController : ApiBaseController
    {
        #region API Methods
        /// <summary>
        /// Get shift by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 

     
        [HttpPost, ResponseType(typeof(ShiftDTO))]
        [Route(NaveoService.Constants.Headers.API_PREFIX + "GetShiftTemplateDetailsById")]
        public IHttpActionResult GetShiftTemplateDetailsById(ShiftTemplate st)
        {
            #region Body
            /*Body 
             {
             "sid":51
             }
            */
            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel mydtData = new ShiftTemplateService().GetShiftTemplateDetailsByGroupdId(st.shifttemplateid, securityTokenObj.securityToken.sConnStr);
                    return Ok(mydtData.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        /// <summary>
        /// Creates a shift
        /// </summary>
        /// <param name="shiftToCreate"></param>
        /// <returns></returns>
        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Constants.Headers.API_PREFIX + "TestShiftTemplate")]
        public IHttpActionResult TestShiftTemplate(PutDTO shiftTemplateToCreate)
        {
            #region Body


            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    ShiftTemplateDTO jsonSaveObject = serializer.Deserialize<ShiftTemplateDTO>(shiftTemplateToCreate.data.ToString());
                    BaseModel ShiftSaveUpdate = new ShiftTemplateService().SaveUpdateShiftTemplate(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr, jsonSaveObject, true);
                    if (ShiftSaveUpdate.additionalData == "TRUE")
                    {   Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "ShiftTemplate";
                        string action = "SaveShiftTemplate";
                        string rawRequest = shiftTemplateToCreate.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, int.Parse((ShiftSaveUpdate.data)[0].ItemArray[10]), securityTokenObj.securityToken.sConnStr, true);
                    }
                    //CommonHelper.DefaultData(RuleSaveUpdate, securityTokenObj);
                    return Ok(ShiftSaveUpdate.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        /// <summary>
        /// Updates a specific shift
        /// </summary>
        /// <param name="shiftToUpdate"></param>
        /// <returns></returns>

        //[HttpPost, ResponseType(typeof(PutDTO))]
        //[Route(Constants.Headers.API_PREFIX + "UpdateShiftTemplate")]
        //public IHttpActionResult UpdateShiftTemplate(PutDTO shiftToUpdate)
        //{
        //    #region Body
           

        //    #endregion

        //    SecurityTokenExtended securityTokenObj = ApiAuthentication();
        //    try
        //    {
        //        if (securityTokenObj.securityToken.IsAuthorized)
        //        {
        //            JavaScriptSerializer serializer = new JavaScriptSerializer();
        //            ShiftDTO jsonSaveObject = serializer.Deserialize<ShiftDTO>(shiftToUpdate.data.ToString());
        //            Boolean ShiftSaveUpdate = new ShiftService().SaveUpdateShift(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr, jsonSaveObject, false);
        //            if (ShiftSaveUpdate)
        //            {
        //                Guid userToken = securityTokenObj.securityToken.UserToken;
        //                string controller = "ShiftTemplate";
        //                string action = "UpdateShiftTemplate";
        //                string rawRequest = shiftToUpdate.data.ToString();
        //                Telemetry.Trace(userToken, controller, action, rawRequest, jsonSaveObject.shift.SID, securityTokenObj.securityToken.sConnStr, true);
        //            }
        //            //CommonHelper.DefaultData(RuleSaveUpdate, securityTokenObj);
        //            return Ok(ShiftSaveUpdate.ToBaseDTO(securityTokenObj.securityToken));
        //        }
        //        return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
        //    }
        //    catch (Exception e)
        //    {
        //        return Ok(SecurityHelper.ExceptionMessage(e));
        //    }
        //}

        /// <summary>
        /// Delete the specified shift
        /// </summary>
        /// <param shift></param>
        /// <returns></returns>

        public IHttpActionResult Delete(Shift shift)
        {
            #region Body
            /*Body 
            {
             "sid":51
             }
            */
            #endregion
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    Boolean deleteResult = new ShiftService().DeleteShift(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr,shift.sid);
                    if (deleteResult)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "ShiftTemplate";
                        string action = "DeleteShiftTemplate";
                        string rawRequest = "{'sid':'" + shift.sid + "'}";
                        Telemetry.Trace(userToken, controller, action, rawRequest, shift.sid, securityTokenObj.securityToken.sConnStr, true);
                    }
                    return Ok(deleteResult.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        #endregion
    }
}
