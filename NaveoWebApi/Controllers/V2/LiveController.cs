﻿using System.Web.Http;
using Microsoft.Web.Http;
using System;
using System.Collections.Generic;
using NaveoService.Services;
using NaveoOneLib.Models;
using Newtonsoft.Json;
using System.Web.Http.Description;
using NaveoWebApi.Models;
using System.Net;
using System.Web.Hosting;
using System.Data;
using NaveoService.Models;
using NaveoWebApi.Helpers;
using System.Net.Http.Headers;
using System.Web;
using NaveoService.Models.DTO;
using NaveoWebApi.Constants;
using NaveoService.Helpers;
using NaveoService.Models.Custom;
using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Models.GPS;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    
    public class LiveController : ApiBaseController
    {
        BaseDTO baseDTO = new BaseDTO();

        #region API Methods
        [HttpPost, ResponseType(typeof(LiveDTO))]
        [Route(Headers.API_PREFIX + "Live")]
        public IHttpActionResult Live(liveAssets ld)
        {
            /*
             * Body
                {
                "assetIds" : [2,9,3,8]
                }  
                
            * Header
                Content-Type:application/json
                HOST_URL:Demo.Naveo.mu
                USER_TOKEN:df8fe13c-3ba8-4650-9499-44049bc163b7
                ONE_TIME_TOKEN:ebc61f24-05b3-46f9-ae5d-f6a23781ffbb           
            */
            String sError = String.Empty;
            if (ld == null)
                sError = "Missing parameters 0";
            else if (ld.assetIds == null)
                sError = "Missing parameters 0";

            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    List<GPSData> l = new LiveService().GetLive(securityTokenObj.securityToken.UserToken, ld.assetIds, ld.lType,ld.byDriver, securityTokenObj.securityToken.sConnStr);
                    return base.Ok(new apiDataLive().ToLiveDTO(l, securityTokenObj.securityToken));
                }
                return base.Ok(baseDTO.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {

                throw e;
                //return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        #endregion
    }
}