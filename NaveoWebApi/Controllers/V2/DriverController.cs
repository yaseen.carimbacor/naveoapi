﻿using Microsoft.Web.Http;
using NaveoService.Constants;
using NaveoService.Models.DTO;
using System;
using System.Web.Http;
using System.Web.Http.Description;
using NaveoService.Models;
using NaveoOneLib.Models.Common;
using NaveoService.Helpers;
using NaveoWebApi.Helpers;
using NaveoService.Services;
using System.Web.Script.Serialization;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class DriverController : ApiBaseController
    {
        BaseDTO baseDTO = new BaseDTO();

        /// <summary>
        /// Header example
        /// HOST_URL: mercury.naveo.mu
        /// USER_TOKEN: B8F03A2A-56A5-4501-9323-3BACFC35BE49
        /// [ONE_TIME_TOKEN]: 0c288a26-aa02-4f34-baff-078a9cab1ca7 
        /// </summary>
        /// <returns></returns>
        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "SaveDriver")]
        public IHttpActionResult SaveDriver(PutDTO resourceToSave)
        {
            #region save/Update Body
            /* Body 
             
                   

             */

            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    ResourceDTO jsonSaveObject = serializer.Deserialize<ResourceDTO>(resourceToSave.data.ToString());
                    Boolean ResourceSaveUpdate = new ResourceService().SaveUpdateResource(securityTokenObj, jsonSaveObject, true);
                    // CommonHelper.DefaultData(ResourceSaveUpdate, securityTokenObj);
                    if (ResourceSaveUpdate)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Driver";
                        string action = "SaveDriver";
                        string rawRequest = resourceToSave.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonSaveObject.resources[0].resourceId, securityTokenObj.securityToken.sConnStr, true);
                    }
                    return Ok(ResourceSaveUpdate.ToBaseDTO(securityTokenObj.securityToken));
                   
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "UpdateDriver")]
        public IHttpActionResult UpdateDriver(PutDTO resourceToSave)
        {
            #region save/Update Body
            /* Body 
             
                   

             */

            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    ResourceDTO jsonSaveObject = serializer.Deserialize<ResourceDTO>(resourceToSave.data.ToString());
                    Boolean ResourceSaveUpdate = new ResourceService().SaveUpdateResource(securityTokenObj, jsonSaveObject,false);
                    // CommonHelper.DefaultData(ResourceSaveUpdate, securityTokenObj);
                    if (ResourceSaveUpdate)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Driver";
                        string action = "UpdateDriver";
                        string rawRequest = resourceToSave.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonSaveObject.resources[0].resourceId, securityTokenObj.securityToken.sConnStr, true);
                    }
                    return Ok(ResourceSaveUpdate.ToBaseDTO(securityTokenObj.securityToken));

                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "DeleteDriver")]
        public IHttpActionResult DeleteDriver(apiResource ar)
        {

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel obj = new ResourceService().Delete(ar.resourceId, securityTokenObj.securityToken.sConnStr);
                    if (obj.data)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Driver";
                        string action = "DeleteDriver";
                        string rawRequest = "{'resourceId':'" + ar.resourceId + "'}";
                        Telemetry.Trace(userToken, controller, action, rawRequest, ar.resourceId, securityTokenObj.securityToken.sConnStr, true);
                    }
                    return Ok(obj.ToBaseDTO(securityTokenObj.securityToken));   //ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        [HttpPost, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "GetDriverScoreCards")]
        public IHttpActionResult GetDriverScoreCards(Trips t)
        {
            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;
            if (t == null)
                sError = "Missing parameters 0";
            else if (t.assetIds == null)
                sError = "Missing parameters 1";
            else if (t.dateFrom == null)
                sError = "Missing parameters 2";
            else if (t.dateTo == null)
                sError = "Missing parameters 3";


            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    //securityTokenObj.securityToken.UserToken,
                    dtData mydtData = new TripService().GetDriverScoreCard(t.assetIds,t.byDriver, t.dateFrom, t.dateTo, securityTokenObj.securityToken.sConnStr, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return base.Ok(baseDTO.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        [HttpGet, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "GetDriverExpiryList")]
        public IHttpActionResult GetDriverExpiryList()
        {
            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;
        
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                 
                    dtData mydtData = new ResourceService().GetDriverExpiry(securityTokenObj.securityToken.UserToken,securityTokenObj.securityToken.sConnStr);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return base.Ok(baseDTO.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }
    }
}