﻿using Microsoft.Web.Http;
using NaveoService.Constants;
using NaveoService.Models.DTO;
using System;
using System.Web.Http;
using System.Web.Http.Description;
using NaveoService.Models;
using NaveoOneLib.Models.Common;
using NaveoService.Helpers;
using NaveoWebApi.Helpers;
using NaveoService.Services;
using System.Web.Script.Serialization;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class ResourceController : ApiBaseController
    {
        [HttpPost, ResponseType(typeof(ResourceDTO))]
        [Route(Headers.API_PREFIX + "GetResources")]
        public IHttpActionResult GetResources(Resource r)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new NaveoService.Services.ResourceService().GetResources(securityTokenObj.securityToken.UserToken,r.resourceIds,securityTokenObj.securityToken.sConnStr, r.resourceType,securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "GetResourceById")]
        public IHttpActionResult GetResourceById(apiResource aR)
        {
            /*Body 
           {
            "resourceId":51
            }
           */

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    apiResource resource = new ResourceService().GetResoureById(securityTokenObj.securityToken.UserToken,securityTokenObj.securityToken.sConnStr, aR.resourceId);
                    return Ok(resource.ToResourceDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        /// <summary>
        /// Header example
        /// HOST_URL: mercury.naveo.mu
        /// USER_TOKEN: B8F03A2A-56A5-4501-9323-3BACFC35BE49
        /// [ONE_TIME_TOKEN]: 0c288a26-aa02-4f34-baff-078a9cab1ca7 
        /// </summary>
        /// <returns></returns>
        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "SaveResource")]
        public IHttpActionResult SaveResource(PutDTO resourceToSave)
        {
            #region save/Update Body
            /* Body 
             
                   

             */

            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    ResourceDTO jsonSaveObject = serializer.Deserialize<ResourceDTO>(resourceToSave.data.ToString());
                    Boolean ResourceSaveUpdate = new ResourceService().SaveUpdateResource(securityTokenObj, jsonSaveObject, true);
                    if (ResourceSaveUpdate)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Resource";
                        string action = "SaveResource";
                        string rawRequest = resourceToSave.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonSaveObject.resources[0].resourceId, securityTokenObj.securityToken.sConnStr, true);
                    }
                    // CommonHelper.DefaultData(ResourceSaveUpdate, securityTokenObj);
                    return Ok(ResourceSaveUpdate.ToBaseDTO(securityTokenObj.securityToken));
                   
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "UpdateResource")]
        public IHttpActionResult UpdateResource(PutDTO resourceToSave)
        {
            #region save/Update Body
            /* Body 
             
                   

             */

            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    ResourceDTO jsonSaveObject = serializer.Deserialize<ResourceDTO>(resourceToSave.data.ToString());
                    Boolean ResourceSaveUpdate = new ResourceService().SaveUpdateResource(securityTokenObj, jsonSaveObject,false);
                    if (ResourceSaveUpdate)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Resource";
                        string action = "UpdateResource";
                        string rawRequest = resourceToSave.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonSaveObject.resources[0].resourceId, securityTokenObj.securityToken.sConnStr, true);
                    }
                    // CommonHelper.DefaultData(ResourceSaveUpdate, securityTokenObj);
                    return Ok(ResourceSaveUpdate.ToBaseDTO(securityTokenObj.securityToken));

                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "CheckForDuplicateDriverToken")]
        public IHttpActionResult CheckForDuplicateDriverToken(apiResource aR)
        {

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    Boolean checkIbutton = new ResourceService().CheckIbutton(aR.driverTokenNo.ToString(), securityTokenObj.securityToken.sConnStr);
                    return Ok(checkIbutton.ToBaseDTO(securityTokenObj.securityToken));

                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }

        }

        
        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "GetResourcesByType")]
        public IHttpActionResult GetResourcesByType(apiResource ar)
        {
           
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData obj = new ResourceService().GetTreeResourceType(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr, ar.resourceType, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage);
                    return Ok(obj.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "DeleteResource")]
        public IHttpActionResult DeleteResource(apiResource ar)
        {

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel obj = new ResourceService().Delete(ar.resourceId, securityTokenObj.securityToken.sConnStr);
                    if (obj.data)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Resource";
                        string action = "SaveResource";
                        string rawRequest = "{'resourceId':'" + ar.resourceId + "'}";
                        Telemetry.Trace(userToken, controller, action, rawRequest, ar.resourceId, securityTokenObj.securityToken.sConnStr, true);
                    }
                    return Ok(obj.ToBaseDTO(securityTokenObj.securityToken));   //ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }
    }
}