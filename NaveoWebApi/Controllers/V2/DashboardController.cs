﻿using Microsoft.Web.Http;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Models.GPS;
using NaveoService.Helpers;
using NaveoService.Models;
using NaveoService.Models.Custom;
using NaveoService.Models.DTO;
using NaveoService.Services;
using NaveoWebApi.Constants;
using NaveoWebApi.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class DashboardController : ApiBaseController
    {
        #region API Methods
        [AllowAnonymous, HttpPost, ResponseType(typeof(LiveDTO))]
        [Route(Headers.API_PREFIX + "Dashboard")]
        public IHttpActionResult Dashboard()
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData dashboard = new DashboardService().GetDashboard(securityTokenObj.securityToken.UserToken, "Live", securityTokenObj.securityToken.sConnStr);
                    return Ok(dashboard.ToObjectDTO(securityTokenObj.securityToken));

                }
                return base.Ok(new BaseDTO().Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [AllowAnonymous, HttpPost, ResponseType(typeof(LiveDTO))]
        [Route(Headers.API_PREFIX + "GetDashboardType")]
        public IHttpActionResult GetDashboardType(Dashboard d)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData dashboard = new DashboardService().GetDashboard(securityTokenObj.securityToken.UserToken, d.dashboardType, securityTokenObj.securityToken.sConnStr);
                    return Ok(dashboard.ToObjectDTO(securityTokenObj.securityToken));

                }
                return base.Ok(new BaseDTO().Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }
        #endregion
    }
}