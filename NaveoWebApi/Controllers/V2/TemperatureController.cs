﻿using Microsoft.Web.Http;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Services.GPS;
using NaveoService.Constants;
using NaveoService.Helpers;
using NaveoService.Models;
using NaveoService.Models.DTO;
using NaveoService.Services;
using NaveoWebApi.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;


namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class TemperatureController : ApiBaseController
    {
        BaseDTO baseDTO = new BaseDTO();

        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "GetCustomizedTemp")]
        public IHttpActionResult GetCustomizedTemp(Temperature t)
        {
            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;
            if (t == null)
                sError = "Missing parameters 0";
            else if (t.assetIds == null)
                sError = "Missing parameters 1";
            else if (t.dateFrom == null)
                sError = "Missing parameters 2";
            else if (t.dateTo == null)
                sError = "Missing parameters 3";

            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData getCustTemp = new TemperatureService().getCustomizedTemperature(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr, t.assetIds, t.dateFrom, t.dateTo, t.lTypeID, t.TimeInterval, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage);
                    return Ok(getCustTemp.ToDynamicDataTableDTO(securityTokenObj.securityToken));
                }
                return base.Ok(baseDTO.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        /// <summary> 
        /// Get Temperature
        /// <para>BODY EXAMPLE</para>
        /// <para>{</para>
        ///   <para>"assetIds": [25] </para>
        ///   <para>"dateFrom": "2017-10-05 01:01:01"</para>
        ///   <para>"dateTo": "2017-10-05 18:20:20" </para>
        ///   <para>"lTypeID": [] </para>
        /// <para>}</para>
        /// <para>HEADERS EXAMPLE</para>
        /// <para>HOST_URL: mercury.naveo.mu</para>
        /// <para>USER_TOKEN: B8F03A2A-56A5-4501-9323-3BACFC35BE49</para> 
        /// <para>[ONE_TIME_TOKEN]: A8F453S2A-24C5-4320-4NAQWE3AK22</para> 
        /// <para>PAGE: 1</para>
        /// <para>ROWS_PER_PAGE: 5</para>
        /// </summary> 
        /// <returns></returns>
        [HttpPost, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "GetTemperature")]
        public IHttpActionResult GetTemperature(Trips t)
        {
            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;
            if (t == null)
                sError = "Missing parameters 0";
            else if (t.assetIds == null)
                sError = "Missing parameters 1";
            else if (t.dateFrom == null)
                sError = "Missing parameters 2";
            else if (t.dateTo == null)
                sError = "Missing parameters 3";

            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    NaveoService.Models.DebugData dd = new NaveoService.Services.GPSDataService().GetDebugData(securityTokenObj.securityToken.UserToken, t.assetIds, t.dateFrom, t.dateTo, t.lTypeID, securityTokenObj.securityToken.sConnStr, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage);
                    return base.Ok(dd.ToDebugDataDTO(securityTokenObj.securityToken));
                }
                return base.Ok(baseDTO.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

    }
}