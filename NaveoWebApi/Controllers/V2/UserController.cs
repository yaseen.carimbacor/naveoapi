﻿using NaveoOneLib.Models;
using NaveoService.Constants;
using NaveoService.Services;
using NaveoService.ViewModel;
using NaveoWebApi.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;
using Microsoft.Web.Http;
using NaveoWebApi.Constants;
using System.Web.Http.Description;
using System.Net.Http;
using System.Net;
using System.IO;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using NaveoService.Helpers;
using NaveoOneLib.Models.Common;
using NaveoService.Models.DTO;
using NaveoOneLib.Models.Permissions;
using NaveoService.Models;
using NaveoWebApi.Models;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class UserController : ApiBaseController
    {
        #region API Methods

        [HttpPost, ResponseType(typeof(TripDTO))]
        [Route(NaveoService.Constants.Headers.API_PREFIX + "GetUsers")]
        public IHttpActionResult GetUser(User u)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new NaveoService.Services.UserService().GetUserData(securityTokenObj.securityToken.UserToken, u.luserID, securityTokenObj.securityToken.sConnStr, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                throw e;
                //return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(NaveoService.Constants.Headers.API_PREFIX + "GetInactiveUsers")]
        public IHttpActionResult GetInactiveUsers()
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    List<apiUser> mydtData = new NaveoService.Services.UserService().GetInactiveUsers(securityTokenObj.securityToken.sConnStr);
                    return Ok(UsersToBaseDTO(mydtData));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                throw e;
                //return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(NaveoService.Constants.Headers.API_PREFIX + "UpdateUserStatus")]
        public IHttpActionResult UpdateUserStatus(UpdateUserStatus userToUpdate)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    Boolean result = new NaveoService.Services.UserService().UpdateUserStatus(userToUpdate.UserID, userToUpdate.Status, securityTokenObj.securityToken.sConnStr);
                    return Ok(BooleanToBaseDTO(result));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                throw e;
                //return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        /// <summary>
        /// Get user by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 
        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(NaveoService.Constants.Headers.API_PREFIX + "GetUserById")]
        public IHttpActionResult GetUserById(User user)
        {
            #region Body
            /*Body 
            {
             "userId":51
             }
            */
            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    apiUser uvm = new UserService().GetUserById(user.userId, CommonHelper.GetCurrentConnStr());
                    return Ok(uvm.ToUserDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                throw e;
                //return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        /// <summary>
        /// Creates a user
        /// </summary>
        /// <param name="userToCreate"></param>
        /// <returns></returns>
        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Constants.Headers.API_PREFIX + "CreateUser")]
        public IHttpActionResult Create(PutDTO userToCreate)
        {
            #region Body
            /* Body
              {
	    "data": {
		"user": {
			"lMatrix": [{
				"oprType": 4,
				"MID": 0,
				"iID": 0,
				"GMID": 3,
				"GMDesc": ""
			}],
			"lRoles": [{
				"lMatrix": null,
				"lPermissions": null,
				"iID": 2,
				"Description": null,
				"oprType": 4
			}],
			"Username": "JaneDoe123",
			"Names": "Jane",
			"LastName": "Doe",
			"Email": "janeDoe@naveo.mu",
			"Tel": "6322392",
			"MobileNo": "843023",
			"Status_b": "AC",
			"Password": "Test1778335.",
			"AccessTemplateId": 0,
			"MaxDayMail": 0,
			"ZoneID": 3,
			"PlnLkpVal": 15,
			"MobileAccess": 0,
			"ExternalAccess": 1,
			"TwoStepVerif": "Enabled",
			"UserToken": "0fbbd259-f435-4b3c-bd67-d0384ef587ef",
			"bTwoWayAuthEnabled": false
		}

	}
}

             */

            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    var servername =CommonHelper.GetCurrentHostURL(HttpContext.Current.Request.Url.DnsSafeHost, Request.Headers);
                    UserDTO jsonUserObject = JsonConvert.DeserializeObject<UserDTO>(userToCreate.data.ToString());
                    BaseDTO createResult = new UserService().SaveUser(securityTokenObj, jsonUserObject, true , servername);
                    if (createResult.sQryResult == "Succeeded")
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "User";
                        string action = "SaveUser";
                        string rawRequest = userToCreate.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, -1, securityTokenObj.securityToken.sConnStr, true);
                    }
                    CommonHelper.DefaultData(createResult, securityTokenObj);
                    return Ok(createResult);
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                throw e;
                //return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        /// <summary>
        /// Updates a specific user
        /// </summary>
        /// <param name="userToUpdate"></param>
        /// <returns></returns>
        // POST: User/Update/5
        [HttpPost, ResponseType(typeof(AccountDTO))]
        [Route(Constants.Headers.API_PREFIX + "UpdateUser")]
        public IHttpActionResult Update(PutDTO userToUpdate)
        {
            #region Body
            /* Body
              {
	    "data": {
		"user": {
			"lMatrix": [{
				"oprType": 16,
				"MID": 0,
				"iID": 0,
				"GMID": 3,
				"GMDesc": ""
			}],
			"lRoles": [{
				"lMatrix": null,
				"lPermissions": null,
				"iID": 2,
				"Description": null,
				"oprType": 16
			}],
			"Username": "JaneDoe123",
			"Names": "Jane",
			"LastName": "Doe",
			"Email": "janeDoe@naveo.mu",
			"Tel": "6322392",
			"MobileNo": "843023",
			"Status_b": "AC",
			"Password": "Test1778335.",
			"AccessTemplateId": 0,
			"MaxDayMail": 0,
			"ZoneID": 3,
			"PlnLkpVal": 15,
			"MobileAccess": 0,
			"ExternalAccess": 1,
			"TwoStepVerif": "Enabled",
			"UserToken": "0fbbd259-f435-4b3c-bd67-d0384ef587ef",
			"bTwoWayAuthEnabled": false
		}

	}
}

             */

            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    //int updatedBy = CommonService.GetUserID(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr);
                    UserDTO jsonUserObject = JsonConvert.DeserializeObject<UserDTO>(userToUpdate.data.ToString());
                    //jsonUserObject.user.UpdatedBy = updatedBy;
                    BaseDTO updateResult = new UserService().SaveUser(securityTokenObj, jsonUserObject, false,string.Empty);
                    if (updateResult.sQryResult == "Succeeded")
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "User";
                        string action = "UpdateUser";
                        string rawRequest = userToUpdate.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, -1, securityTokenObj.securityToken.sConnStr, true);
                    }
                    CommonHelper.DefaultData(updateResult, securityTokenObj);
                    return Ok(updateResult);
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                throw e;
                //return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        /// <summary>
        /// Delete the specified user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: User/Delete/5
        [HttpPost, ResponseType(typeof(AccountDTO))]
        [Route(Constants.Headers.API_PREFIX + "DeleteUser")]
        public IHttpActionResult Delete(User user)
        {
            #region Body
            /*Body 
            {
             "userId":51
             }
            */
            #endregion
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseDTO deleteResult = new UserService().Delete(securityTokenObj, user.email);
                    if (deleteResult.data)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "User";
                        string action = "SaveUser";
                        string rawRequest = "{'userId':'" + user.userId + "'}";
                        Telemetry.Trace(userToken, controller, action, rawRequest, user.userId, securityTokenObj.securityToken.sConnStr, true);
                    }
                    CommonHelper.DefaultData(deleteResult, securityTokenObj);
                    return Ok(deleteResult);
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                throw e;
                //return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(UserDTO))]
        [Route(NaveoService.Constants.Headers.API_PREFIX + "ResetToDefaultPassword")]
        public IHttpActionResult ResetToDefaultPassword(User u)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel mydtData = new NaveoService.Services.UserService().ResetPassword(u.email, securityTokenObj.securityToken.sConnStr);
                    return Ok(mydtData.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                throw e;
               // return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(UserDTO))]
        [Route(NaveoService.Constants.Headers.API_PREFIX + "GetUserApprovalValues")]
        public IHttpActionResult GetUserApprovalValues(NaveoService.Models.DTO.Matrix matrix)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new NaveoService.Services.UserService().getUserApprovalValues(matrix.gmId, securityTokenObj.securityToken.sConnStr);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        [HttpGet, ResponseType(typeof(UserDTO))]
        [Route(NaveoService.Constants.Headers.API_PREFIX + "MigrateUserDataToGMS")]
        public IHttpActionResult MigrateUserDataToGMS()
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel mydtData = new NaveoService.Services.UserService().MigrateUserToGMS(securityTokenObj);
                    return Ok(mydtData.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        #endregion

        #region private methods
        private BaseDTO UsersToBaseDTO(List<apiUser> users) {
            BaseDTO ret = new BaseDTO();
            ret.data = users;
            ret.sQryResult = "Succeeded";
            return ret;
        }

        private BaseDTO BooleanToBaseDTO(Boolean result)
        {
            BaseDTO ret = new BaseDTO();
            ret.data = result;
            ret.sQryResult = result ? "Succeeded" : "Failed";
            return ret;
        }

        #endregion
    }
}
