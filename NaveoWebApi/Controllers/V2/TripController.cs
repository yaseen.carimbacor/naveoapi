﻿using System.Web.Http;
using Microsoft.Web.Http;
using System;
using NaveoService.Services;
using NaveoOneLib.Models;
using System.Web.Http.Description;
using NaveoWebApi.Models;
using System.Net;
using NaveoService.Models;
using NaveoWebApi.Helpers;
using NaveoService.Models.DTO;
using NaveoWebApi.Constants;
using NaveoService.Helpers;
using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Models.Common;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class TripController : ApiBaseController
    {
        //Test Yaseen
        BaseDTO baseDTO = new BaseDTO();
        #region API Methods
        /// <summary> 
        /// Get trip details
        /// <para>BODY EXAMPLE</para>
        /// <para>{</para>
        ///   <para>"assetIds": [25] </para>
        ///   <para>"dateFrom": "2017-10-05 01:01:01"</para>
        ///   <para>"dateTo": "2017-10-05 18:20:20" </para>
        /// <para>}</para>
        /// <para>HEADERS EXAMPLE</para>
        /// <para>HOST_URL: mercury.naveo.mu</para>
        /// <para>USER_TOKEN: B8F03A2A-56A5-4501-9323-3BACFC35BE49</para> 
        /// <para>[ONE_TIME_TOKEN]: A8F453S2A-24C5-4320-4NAQWE3AK22</para> 
        /// <para>PAGE: 1</para>
        /// <para>ROWS_PER_PAGE: 5</para>
        /// </summary> 
        /// <returns></returns>
        [HttpPost, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "Trips")]
        public IHttpActionResult Trips(Trips t)
        {
            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;
            if (t == null)
                sError = "Missing parameters 0";
            else if (t.assetIds == null)
                sError = "Missing parameters 1";
            else if (t.dateFrom == null)
                sError = "Missing parameters 2";
            else if (t.dateTo == null)
                sError = "Missing parameters 3";

            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    NaveoService.Models.TripHeader tripHeader = new TripService().GetTripHeaders(securityTokenObj.securityToken.UserToken, t.assetIds, t.dateFrom, t.dateTo, t.byDriver, t.iMinTripDistance, t.sWorkHours, securityTokenObj.securityToken.sConnStr, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage,t.Odometre, t.assetId, true, t.sortColumns, t.sortOrderAsc);
                    return base.Ok(tripHeader.ToTripDTO(securityTokenObj.securityToken));
                }
                return base.Ok(baseDTO.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                throw e;
                //return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "GetOdo")]
        public IHttpActionResult TripOdo(Trips t)
        {
            //Check if all parameters are available. If not, return error message
            //dateTo is last last of trip
            String sError = String.Empty;
            if (t == null)
                sError = "Missing parameters 0";
            else if (t.assetIds == null)
                sError = "Missing parameters 1";
            else if (t.dateFrom == null)
                sError = "Missing parameters 2";
            else if (t.dateTo == null)
                sError = "Missing parameters 3";

            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    System.Data.DataTable dtTripOdo = new TripService().getCalculatedOdo(securityTokenObj.securityToken.UserToken, t.assetIds, t.dateTo, securityTokenObj.securityToken.sConnStr);
                    return base.Ok(dtTripOdo.ToOdoDTO(securityTokenObj.securityToken));
                }
                return base.Ok(baseDTO.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                throw e;
                //return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        /// <summary> 
        /// Get trip details
        /// <para>BODY EXAMPLE</para>
        /// <para>{ "lTrips" : [2,9,3,8,7] }</para>
        /// <para>HEADERS EXAMPLE</para>
        /// <para>HOST_URL: mercury.naveo.mu</para>
        /// <para>USER_TOKEN: B8F03A2A-56A5-4501-9323-3BACFC35BE49</para> 
        /// <para>[ONE_TIME_TOKEN]: A8F453S2A-24C5-4320-4NAQWE3AK22</para> 
        /// <para>PAGE: 1</para>
        /// <para>ROWS_PER_PAGE: 5</para>
        /// </summary> 
        /// <returns></returns>
        [HttpPost, ResponseType(typeof(TripDetailsDTO))]
        [Route(Headers.API_PREFIX + "TripDetails")]
        public IHttpActionResult TripsDetails(Trips t)
        {
            String sError = String.Empty;
            if (t.trips == null)
                sError = "Missing parameters 0";

            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    TripDetails tripDetails = new TripService().GetTripDetails(t.trips, securityTokenObj);
                    return base.Ok(tripDetails.ToTripDTO(securityTokenObj.securityToken));
                }
                return base.Ok(baseDTO.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                throw e;
                //return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        #region Summarized reports
        /// <summary> 
        /// Get DailySummarizedTrips
        /// <para>BODY EXAMPLE</para>
        /// <para>{</para>
        ///   <para>"assetIds": [25] </para>
        ///   <para>"dateFrom": "2017-10-05 01:01:01"</para>
        ///   <para>"dateTo": "2017-10-05 18:20:20" </para>
        /// <para>}</para>
        /// <para>HEADERS EXAMPLE</para>
        /// <para>HOST_URL: mercury.naveo.mu</para>
        /// <para>USER_TOKEN: B8F03A2A-56A5-4501-9323-3BACFC35BE49</para> 
        /// <para>[ONE_TIME_TOKEN]: A8F453S2A-24C5-4320-4NAQWE3AK22</para> 
        /// <para>PAGE: 1</para>
        /// <para>ROWS_PER_PAGE: 5</para>
        /// </summary> 
        /// <returns></returns>
        [HttpPost, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "DailySummarizedTrips")]
        public IHttpActionResult DailySummarizedTrips(Trips t)
        {
            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;
            if (t == null)
                sError = "Missing parameters 0";
            else if (t.assetIds == null)
                sError = "Missing parameters 1";
            else if (t.dateFrom == null)
                sError = "Missing parameters 2";
            else if (t.dateTo == null)
                sError = "Missing parameters 3";

            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new TripService().GetDailySummarizedTrips(securityTokenObj.securityToken.UserToken, t.assetIds, t.dateFrom, t.dateTo, t.byDriver, securityTokenObj.securityToken.sConnStr, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage, t.sortColumns, t.sortOrderAsc);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return base.Ok(baseDTO.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                throw e;
                //return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        /// <summary> 
        /// Get SummarizedTrips
        /// <para>BODY EXAMPLE</para>
        /// <para>{</para>
        ///   <para>"assetIds": [25] </para>
        ///   <para>"dateFrom": "2017-10-05 01:01:01"</para>
        ///   <para>"dateTo": "2017-10-05 18:20:20" </para>
        /// <para>}</para>
        /// <para>HEADERS EXAMPLE</para>
        /// <para>HOST_URL: mercury.naveo.mu</para>
        /// <para>USER_TOKEN: B8F03A2A-56A5-4501-9323-3BACFC35BE49</para> 
        /// <para>[ONE_TIME_TOKEN]: A8F453S2A-24C5-4320-4NAQWE3AK22</para> 
        /// <para>PAGE: 1</para>
        /// <para>ROWS_PER_PAGE: 5</para>
        /// </summary> 
        /// <returns></returns>
        [HttpPost, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "GetSummarizedTrips")]
        public IHttpActionResult GetSummarizedTrips(Trips t)
        {
            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;
            if (t == null)
                sError = "Missing parameters 0";
            else if (t.assetIds == null)
                sError = "Missing parameters 1";
            else if (t.dateFrom == null)
                sError = "Missing parameters 2";
            else if (t.dateTo == null)
                sError = "Missing parameters 3";

            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new TripService().GetSummarizedTrips(securityTokenObj.securityToken.UserToken, t.assetIds, t.dateFrom, t.dateTo, t.byDriver, securityTokenObj.securityToken.sConnStr, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage, t.sortColumns, t.sortOrderAsc);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return base.Ok(baseDTO.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                throw e;
                //return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        /// <summary> 
        /// Get GetDailyTripTime
        /// <para>BODY EXAMPLE</para>
        /// <para>{</para>
        ///   <para>"assetIds": [25] </para>
        ///   <para>"dateFrom": "2017-10-05 01:01:01"</para>
        ///   <para>"dateTo": "2017-10-05 18:20:20" </para>
        /// <para>}</para>
        /// <para>HEADERS EXAMPLE</para>
        /// <para>HOST_URL: mercury.naveo.mu</para>
        /// <para>USER_TOKEN: B8F03A2A-56A5-4501-9323-3BACFC35BE49</para> 
        /// <para>[ONE_TIME_TOKEN]: A8F453S2A-24C5-4320-4NAQWE3AK22</para> 
        /// <para>PAGE: 1</para>
        /// <para>ROWS_PER_PAGE: 5</para>
        /// </summary> 
        /// <returns></returns>
        [HttpPost, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "GetDailyTripTime")]
        public IHttpActionResult GetDailyTripTime(Trips t)
        {
            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;
            if (t == null)
                sError = "Missing parameters 0";
            else if (t.assetIds == null)
                sError = "Missing parameters 1";
            else if (t.dateFrom == null)
                sError = "Missing parameters 2";
            else if (t.dateTo == null)
                sError = "Missing parameters 3";

            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new TripService().GetDailyTripTime(securityTokenObj.securityToken.UserToken, t.assetIds, t.dateFrom, t.dateTo, t.sTotal, t.byDriver, securityTokenObj.securityToken.sConnStr, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage);
                    return Ok(mydtData.ToPivotDTO(securityTokenObj.securityToken));
                }
                return base.Ok(baseDTO.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                throw e;
                //return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        /// <summary> 
        /// Get AssetSummaryOutsideHO
        /// <para>BODY EXAMPLE</para>
        /// <para>{</para>
        ///   <para>"assetIds": [25] </para>
        ///   <para>"dateFrom": "2017-10-05 01:01:01"</para>
        ///   <para>"dateTo": "2017-10-05 18:20:20" </para>
        ///   <para>""iHOZone":1" </para>
        ///   <para>"zoneTypes":["Customer Zone", "Restricted Zone"]</para>
        /// <para>}</para>
        /// <para>HEADERS EXAMPLE</para>
        /// <para>HOST_URL: mercury.naveo.mu</para>
        /// <para>USER_TOKEN: B8F03A2A-56A5-4501-9323-3BACFC35BE49</para> 
        /// <para>[ONE_TIME_TOKEN]: A8F453S2A-24C5-4320-4NAQWE3AK22</para> 
        /// <para>PAGE: 1</para>
        /// <para>ROWS_PER_PAGE: 5</para>
        /// </summary> 
        /// <returns></returns>
        [HttpPost, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "AssetSummaryOutsideHO")]
        public IHttpActionResult AssetSummaryOutsideHO(Trips t)
        {
            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;
            if (t == null)
                sError = "Missing parameters 0";
            else if (t.assetIds == null)
                sError = "Missing parameters 1";
            else if (t.dateFrom == null)
                sError = "Missing parameters 2";
            else if (t.dateTo == null)
                sError = "Missing parameters 3";

            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new TripService().GetAssetSummaryOutsideHO(securityTokenObj.securityToken.UserToken, t.assetIds, t.byDriver, t.iHOZone, t.zoneTypes, t.dateFrom, t.dateTo, securityTokenObj.securityToken.sConnStr, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage);
                    return Ok(mydtData.ToDynamicDataTableDTO(securityTokenObj.securityToken));
                }
                return base.Ok(baseDTO.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                throw e;
                //return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }
        #endregion

        #region CreateTripsSpatial
        [HttpPost, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "CreateTripsSpatial")]
        public IHttpActionResult CreateTripsSpatial(Trips t)
        {
            //Check if all parameters are available. If not, return error message
            String sError = String.Empty;
            if (t == null)
                sError = "Missing parameters 0";
            else if (t.assetIds == null)
                sError = "Missing parameters 1";
            else if (t.dateFrom == null)
                sError = "Missing parameters 2";
            else if (t.dateTo == null)
                sError = "Missing parameters 3";

            if (!String.IsNullOrEmpty(sError))
                return Content(HttpStatusCode.Forbidden, sError);

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel SpatialStrips = new TripService().CreateTripsSpatial(securityTokenObj.securityToken.UserToken, t.assetIds, t.dateFrom, t.dateTo, t.byDriver, securityTokenObj.securityToken.sConnStr);
                    if (SpatialStrips.data)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "TripController";
                        string action = "CreateTripsSpatial";
                        string rawRequest =  "{}";
                        Telemetry.Trace(userToken, controller, action, rawRequest, -1, securityTokenObj.securityToken.sConnStr, true);
                    }
                    return base.Ok(SpatialStrips.ToBaseDTO(securityTokenObj.securityToken));
                }
                return base.Ok(baseDTO.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (System.Exception e)
            {
                return base.Ok(SecurityHelper.ExceptionMessage(e));
            }
        }
        #endregion

        #endregion
    }
}