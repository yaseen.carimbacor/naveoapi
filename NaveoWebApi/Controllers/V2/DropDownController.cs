﻿using System;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.Web.Http;
using NaveoOneLib.Models.Common;
using NaveoService.Models;
using NaveoService.Services;
using NaveoWebApi.Constants;
using NaveoService.Helpers;
using NaveoWebApi.Helpers;
using NaveoWebApi.Models;
using NaveoService.Models.DTO;


namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class DropDownController : ApiBaseController
    {
        /// <summary> 
        /// GetDropDownValues
        /// <para>BODY EXAMPLE</para>
        /// <para>{</para>
        ///   <para>"value":"AssetDropDown" </param>
        /// <para>}</para>
        /// <para>HEADERS EXAMPLE</para>
        /// <para>USER_TOKEN: B8F03A2A-56A5-4501-9323-3BACFC35BE49</para> 
        /// <para>[ONE_TIME_TOKEN]: A8F453S2A-24C5-4320-4NAQWE3AK22</para> 
        /// </summary> 
        /// <returns></returns>

        [AllowAnonymous, HttpPost, ResponseType(typeof(NaveoService.Models.Object))]
        [Route(Headers.API_PREFIX + "GetDropDownValues")]
        public IHttpActionResult GetDropDownValues(DropDownName ddn)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData obj = new dtData();

                    obj = new DropDownService().MergeDT(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr, ddn.value);
                                        
                    return Ok(obj.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }
    }
}