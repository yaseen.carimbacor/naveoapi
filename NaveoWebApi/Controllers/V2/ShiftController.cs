﻿using NaveoOneLib.Models;
using NaveoService.Constants;
using NaveoService.Services;
using NaveoService.ViewModel;
using NaveoWebApi.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;
using Microsoft.Web.Http;
using NaveoWebApi.Constants;
using System.Web.Http.Description;
using System.Net.Http;
using System.Net;
using System.IO;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using NaveoService.Helpers;
using NaveoOneLib.Models.Common;
using NaveoService.Models.DTO;
using NaveoOneLib.Models.Permissions;
using NaveoService.Models;
using NaveoWebApi.Models;
using System.Web.Script.Serialization;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class ShiftController : ApiBaseController
    {
        #region API Methods
        /// <summary>
        /// Get shift by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 

        [HttpPost, ResponseType(typeof(ShiftDTO))]
        [Route(NaveoService.Constants.Headers.API_PREFIX + "GetShifts")]
        public IHttpActionResult GetShifts()
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new NaveoService.Services.ShiftService().GetShifts(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr, securityTokenObj.securityToken.Page, securityTokenObj.securityToken.LimitPerPage);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(ShiftDTO))]
        [Route(NaveoService.Constants.Headers.API_PREFIX + "GetShiftById")]
        public IHttpActionResult GetShiftById(Shift s)
        {
            #region Body
            /*Body 
             {
             "sid":51
             }
            */
            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel mydtData = new ShiftService().GetShiftById(securityTokenObj.securityToken.UserToken, s.sid, securityTokenObj.securityToken.sConnStr);
                    return Ok(mydtData.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        /// <summary>
        /// Creates a shift
        /// </summary>
        /// <param name="shiftToCreate"></param>
        /// <returns></returns>
        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Constants.Headers.API_PREFIX + "SaveShift")]
        public IHttpActionResult SaveShift(PutDTO shiftToCreate)
        {
            #region Body


            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    ShiftDTO jsonSaveObject = serializer.Deserialize<ShiftDTO>(shiftToCreate.data.ToString());
                    Boolean ShiftSaveUpdate = new ShiftService().SaveUpdateShift(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr, jsonSaveObject, true);
                    if (ShiftSaveUpdate)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Shift";
                        string action = "SaveShift";
                        string rawRequest = shiftToCreate.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonSaveObject.shift.SID, securityTokenObj.securityToken.sConnStr, true);
                    }
                    //CommonHelper.DefaultData(RuleSaveUpdate, securityTokenObj);
                    return Ok(ShiftSaveUpdate.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        /// <summary>
        /// Updates a specific shift
        /// </summary>
        /// <param name="shiftToUpdate"></param>
        /// <returns></returns>

        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Constants.Headers.API_PREFIX + "UpdateShift")]
        public IHttpActionResult UpdateShift(PutDTO shiftToUpdate)
        {
            #region Body
           

            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    ShiftDTO jsonSaveObject = serializer.Deserialize<ShiftDTO>(shiftToUpdate.data.ToString());
                    Boolean ShiftSaveUpdate = new ShiftService().SaveUpdateShift(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr, jsonSaveObject, false);
                    if (ShiftSaveUpdate)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Shift";
                        string action = "UpdateShift";
                        string rawRequest = shiftToUpdate.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonSaveObject.shift.SID, securityTokenObj.securityToken.sConnStr, true);
                    }
                    //CommonHelper.DefaultData(RuleSaveUpdate, securityTokenObj);
                    return Ok(ShiftSaveUpdate.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        /// <summary>
        /// Delete the specified shift
        /// </summary>
        /// <param shift></param>
        /// <returns></returns>

        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Constants.Headers.API_PREFIX + "DeleteShift")]
        public IHttpActionResult DeleteShift(Shift shift)
        {
            #region Body
            /*Body 
            {
             "sid":51
             }
            */
            #endregion
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    Boolean deleteResult = new ShiftService().DeleteShift(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr,shift.sid);
                    if (deleteResult)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "Shift";
                        string action = "DeleteShift";
                        string rawRequest = "{'sid':'" + shift.sid + "'}";
                        Telemetry.Trace(userToken, controller, action, rawRequest, shift.sid, securityTokenObj.securityToken.sConnStr, true);
                    }
                    return Ok(deleteResult.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        #endregion
    }
}
