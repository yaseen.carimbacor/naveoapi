﻿using Microsoft.Web.Http;
using NaveoService.Services;
using NaveoWebApi.Constants;
using NaveoWebApi.Helpers;
using System;
using System.Web.Http;
using System.Web.Http.Description;
using NaveoService.Models.DTO;
using NaveoWebApi.Models;
using NaveoOneLib.Models.Common;
using System.Web.Script.Serialization;
using NaveoService.Models;
using NaveoService.Helpers;
using NaveoOneLib.Models.Assets;
using System.Web;
using NaveoOneLib.Models.BLUP;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class FixedAssetController : ApiBaseController
    {
        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "GetFixedAssetById")]
        public IHttpActionResult GetFixedAssetById(lAssets ld)
        {
            /*Body 
           {
                "assetId":51
            }
           */

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel fixedasset = new FixedAssetsService().GetFixedAssetById(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr, ld.assetId);
                    return Ok(fixedasset.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "SaveFixedAsset")]
        public IHttpActionResult SaveFixedAsset(PutDTO assetToSave)
        {
            #region save/Update Body
            /*
	        "data": {
                        "fixedAsset": {
                            "AssetID": 0,
		                    "AssetCode": null,
		                    "AssetType": 0,
		                    "dtCreated": "\/Date(-62135596800000)\/",
		                    "AssetName": null,
		                    "Descript": null,
		                    "RoadID": null,
		                    "LocalityID": null,
		                    "VillageID": null,
		                    "AssetCategory": null,
		                    "AssetFamily": null,
		                    "Owners": null,
		                    "Notes": null,
		                    "PictureName": null,
		                    "MaintLink": null,
		                    "sValue": null,
		                    "TitleDeed": null,
		                    "DistanceUnit": null,
		                    "Pin": null,
		                    "GeomData": null,
		                    "Height": null,
		                    "Wattage": null,
		                    "Price_m2": null,
		                    "SurfaceArea": null,
		                    "Distance": null,
		                    "ExpDate": null,
		                    "MarketValue": null,
		                    "NumberOfP": null,
		                    "oprType": 0,
		                    "lMatrix": [],
		                    "gisAssetType": {
		                	    "iID": 0,
		                	    "AssetType": null,
		                	    "oprType": 0
		                    },
		                    "gisAssetCategory": {
		                	    "iID": 0,
		                	    "AssetTypeID": 0,
		                	    "AssetCategory": null,
		                	    "oprType": 0
		                    }
	                    }
                */
            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    FixedAssetDTO jsonSaveObject = serializer.Deserialize<FixedAssetDTO>(assetToSave.data.ToString());

                    BaseDTO AssetSaveUpdate = new FixedAssetsService().SaveUpdateFixedAsset(securityTokenObj, jsonSaveObject, true);
                    if (AssetSaveUpdate.sQryResult == "Succeeded")
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "FixedASset";
                        string action = "SaveFixedAsset";
                        string rawRequest = assetToSave.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonSaveObject.fixedAsset.AssetID, securityTokenObj.securityToken.sConnStr, true);
                    }
                    CommonHelper.DefaultData(AssetSaveUpdate, securityTokenObj);
                    return Ok(AssetSaveUpdate);
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "UpdateFixedAsset")]
        public IHttpActionResult UpdateFixedAsset(PutDTO assetToSave)
        {
            #region save/Update Body
            /*
	        "data": {
                        "fixedAsset": {
                            "AssetID": 0,
		                    "AssetCode": null,
		                    "AssetType": 0,
		                    "dtCreated": "\/Date(-62135596800000)\/",
		                    "AssetName": null,
		                    "Descript": null,
		                    "RoadID": null,
		                    "LocalityID": null,
		                    "VillageID": null,
		                    "AssetCategory": null,
		                    "AssetFamily": null,
		                    "Owners": null,
		                    "Notes": null,
		                    "PictureName": null,
		                    "MaintLink": null,
		                    "sValue": null,
		                    "TitleDeed": null,
		                    "DistanceUnit": null,
		                    "Pin": null,
		                    "GeomData": null,
		                    "Height": null,
		                    "Wattage": null,
		                    "Price_m2": null,
		                    "SurfaceArea": null,
		                    "Distance": null,
		                    "ExpDate": null,
		                    "MarketValue": null,
		                    "NumberOfP": null,
		                    "oprType": 0,
		                    "lMatrix": [],
		                    "gisAssetType": {
		                	    "iID": 0,
		                	    "AssetType": null,
		                	    "oprType": 0
		                    },
		                    "gisAssetCategory": {
		                	    "iID": 0,
		                	    "AssetTypeID": 0,
		                	    "AssetCategory": null,
		                	    "oprType": 0
		                    }
	                    }
                */
            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    FixedAssetDTO jsonSaveObject = serializer.Deserialize<FixedAssetDTO>(assetToSave.data.ToString());
                    BaseDTO AssetSaveUpdate = new FixedAssetsService().SaveUpdateFixedAsset(securityTokenObj, jsonSaveObject, false);
                    if (AssetSaveUpdate.sQryResult == "Succeeded")
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "FixedASset";
                        string action = "UpdateFixedAsset";
                        string rawRequest = assetToSave.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonSaveObject.fixedAsset.AssetID, securityTokenObj.securityToken.sConnStr, true);
                    }
                    CommonHelper.DefaultData(AssetSaveUpdate, securityTokenObj);
                    return Ok(AssetSaveUpdate);
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost]
        [Route(NaveoService.Constants.Headers.API_PREFIX + "DeleteFixedAssetPicture")]
        public BaseDTO DeleteFixedAssetPicture(NaveoOneLib.Models.Assets.FixedAsset fixedAsset)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            BaseDTO BD = new BaseDTO();

            if (securityTokenObj.securityToken.IsAuthorized)
            {
                Boolean b = new NaveoService.Services.ImportService().DeleteFixedAssetPicture(fixedAsset);
                if (b)
                {
                    Guid userToken = securityTokenObj.securityToken.UserToken;
                    string controller = "FixedASset";
                    string action = "DeleteFixedAssetPicture";
                    string rawRequest = "{'AssetID':'" + fixedAsset.AssetID + "'}";
                    Telemetry.Trace(userToken, controller, action, rawRequest, fixedAsset.AssetID, securityTokenObj.securityToken.sConnStr, true);
                }
                BD.data = b;
            }
            else
                BD.errorMessage = securityTokenObj.securityToken.DebugMessage;

            return BD;
        }

        class FixedAssetsCreation
        {
            public String sPath { get; set; }
            public int iMatrix { get; set; }
        }

        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "SaveFixedAssetsFromShp")]
        public IHttpActionResult SaveFixedAssetsFromShp(PutDTO FixedAssets)
        {
            #region save/Update Body
            //{
            //    "data": {
            //        "shpPath" : "E:\\Reza\\Geo\\Maps\\Creations\\MDC2019\\SHAPES\\Pole.shp",  --Map Server Path
            //  "iMatrix" : 3
            //                },
            // "oneTimeToken": "5e51f2eb-110e-4108-9529-5d408d29668f",
            // "screenMode": "Add"
            //}
            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    FixedAssetsCreation jsonSaveObject = serializer.Deserialize<FixedAssetsCreation>(FixedAssets.data.ToString());
                    String shpPath = jsonSaveObject.sPath;
                    int iMatrix = jsonSaveObject.iMatrix;
                    BaseDTO AssetSaveUpdate = new FixedAssetsService().SaveFixedAssetsFromShp(securityTokenObj, shpPath, iMatrix);
                    if (AssetSaveUpdate.sQryResult == "Succeeded")
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "FixedASset";
                        string action = "SaveFixedAssetFromShp";
                        string rawRequest = FixedAssets.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, -1, securityTokenObj.securityToken.sConnStr, true);
                    }
                    CommonHelper.DefaultData(AssetSaveUpdate, securityTokenObj);
                    return Ok(AssetSaveUpdate);
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "GetFixedAssetsByIds")]
        public IHttpActionResult GetFixedAssetsByIds(Blup B)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel bm = new FixedAssetsService().GetFaDataByIds(securityTokenObj.securityToken.UID, B.lids, B.bShowBuffers, B.ShowDataOnly, B.MaintStatusId, B.MaintTypeId, B.roadnetwork, HttpContext.Current.Request.Url.DnsSafeHost, "MaintDB", securityTokenObj.securityToken.sConnStr);
                    return base.Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        //[HttpPost, ResponseType(typeof(BaseDTO))]
        //[Route(Headers.API_PREFIX + "GetFixedAssetsMaintByIds")]
        //public IHttpActionResult GetFixedAssetsByIdsMaint(NaveoService.Models.Blup B)
        //{
        //    SecurityTokenExtended securityTokenObj = ApiAuthentication();
        //    try
        //    {
        //        if (securityTokenObj.securityToken.IsAuthorized)
        //        {
        //            BaseModel bm = new FixedAssetsService().GetMaintStatusType(securityTokenObj.securityToken.UID, B.lids, B.MaintStatusId, B.MaintTypeId, HttpContext.Current.Request.Url.DnsSafeHost, "MaintDB");
        //            return base.Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
        //        }
        //        return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
        //    }
        //    catch (Exception e)
        //    {
        //        return Ok(SecurityHelper.ExceptionMessage(e));
        //    }
        //}

        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "GetFixedAssetsByLonLat")]
        public IHttpActionResult GetBlupByLonLat(Blup B)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel bm = new FixedAssetsService().GetFixedAssetsByLonLat(securityTokenObj.securityToken.UserToken, 0.1, B.lon, B.lat, securityTokenObj.securityToken.sConnStr);
                    return base.Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "ShowFixedAssetsByLonLat")]
        public IHttpActionResult ShowBlupByLonLat(Blup B)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    if (B.bShowBuffers)
                        B.dDistance = 300;
                    BaseModel bm = new FixedAssetsService().GetFAWithinDistanceInMeters(securityTokenObj.securityToken.UserToken, B.dDistance, B.lon, B.lat, securityTokenObj.securityToken.sConnStr);
                    return base.Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }



        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "SaveGISAssetCategory")]
        public IHttpActionResult SaveGISAssetCategory(PutDTO FixedAssets)
        {
            #region save Body
            //{
            //    {
            //        "gisAssetCategory": {
            //            "iID": 0,
            //      "AssetTypeID": 6,
            //      "AssetCategory": "known",
            //      "oprType": 4
            //        }
            //    }
            //}
            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    FixedAssetDTO jsonSaveObject = serializer.Deserialize<FixedAssetDTO>(FixedAssets.data.ToString());
                    BaseDTO AssetSaveUpdate = new FixedAssetsService().SaveUpdateGISAssetCategory(securityTokenObj, jsonSaveObject, true);
                    if (AssetSaveUpdate.sQryResult == "Succeeded")
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "FixedASset";
                        string action = "SaveGISAssetCategory";
                        string rawRequest = FixedAssets.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonSaveObject.fixedAsset.AssetID, securityTokenObj.securityToken.sConnStr, true);
                    }
                    CommonHelper.DefaultData(AssetSaveUpdate, securityTokenObj);
                    return Ok(AssetSaveUpdate);
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "UpdateGISAssetCategory")]
        public IHttpActionResult UpdateGISAssetCategory(PutDTO FixedAssets)
        {
            #region Update Body
            //           {
            //               "data": {
            //                   "gisAssetCategory": {
            //                       "iID": 1,
            //		"AssetTypeID":1,
            //		"AssetCategory":"vssdsvv",
            //		"oprType": 16,


            //}

            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    FixedAssetDTO jsonSaveObject = serializer.Deserialize<FixedAssetDTO>(FixedAssets.data.ToString());
                    BaseDTO AssetSaveUpdate = new FixedAssetsService().SaveUpdateGISAssetCategory(securityTokenObj, jsonSaveObject, false);
                    CommonHelper.DefaultData(AssetSaveUpdate, securityTokenObj);
                    if (AssetSaveUpdate.sQryResult == "Succeeded")
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "FixedASset";
                        string action = "UpdateGISAssetCategory";
                        string rawRequest = FixedAssets.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonSaveObject.fixedAsset.AssetID, securityTokenObj.securityToken.sConnStr, true);
                    }
                    return Ok(AssetSaveUpdate);
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        [HttpGet, ResponseType(typeof(BaseModel))]
        [Route(NaveoService.Constants.Headers.API_PREFIX + "MigrateFixedAssetDataToGMS")]
        public IHttpActionResult MigrateAssetDataToGMS()
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel mydtData = new NaveoService.Services.FixedAssetsService().MigrateFixedAssetToGMS(securityTokenObj);
                    return Ok(mydtData.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        #region RoadNetWork
        class shpCreation
        {
            public String sPath { get; set; }
            public String sType { get; set; }
            public int iMatrix { get; set; }
        }

        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "SaveRoadNetworkFromShp")]
        public IHttpActionResult SaveRoadNetworkFromShp(PutDTO shpData)
        {
            #region save/Update Body
            //{
            //  "data": {
            //      "shpPath" : "E:\\Reza\\Geo\\Maps\\Creations\\MDC2019\\SHAPES\\Pole.shp",
            //      "sType" : "District, VCA, RoadNetwork"
            //      "iMatrix" : 3
            //  }
            //}
            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    shpCreation jsonObject = serializer.Deserialize<shpCreation>(shpData.data.ToString());
                    String shpPath = jsonObject.sPath;
                    String sType = jsonObject.sType;
                    int iMatrix = jsonObject.iMatrix;
                    BaseDTO RoadNetworkSaveUpdate = new RoadNetworkService().SaveRoadNetworkFromShp(securityTokenObj, shpPath, sType, iMatrix);
                    CommonHelper.DefaultData(RoadNetworkSaveUpdate, securityTokenObj);
                    if (RoadNetworkSaveUpdate.sQryResult == "Succeeded")
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "FixedASset";
                        string action = "SaveRoadNetworkFromShp";
                        string rawRequest = shpData.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, -1, securityTokenObj.securityToken.sConnStr, true);
                    }
                    return Ok(RoadNetworkSaveUpdate);
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "GetRoadNetworkByIds")]
        public IHttpActionResult GetRoadNetworkByIds(Blup B)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel bm = new RoadNetworkService().GetRoadNetworkByIds(securityTokenObj.securityToken.UID, B.treeIDs, B.ShowDataOnly, securityTokenObj.securityToken.sConnStr);
                    return base.Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        #endregion
    }
}