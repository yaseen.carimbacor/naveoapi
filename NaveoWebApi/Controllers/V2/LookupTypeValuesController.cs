﻿using Microsoft.Web.Http;
using NaveoOneLib.Models;
using NaveoService.Services;
using NaveoWebApi.Constants;
using NaveoWebApi.Models;
using NaveoService.Models.DTO;
using NaveoWebApi.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Description;
using NaveoService.Models;
using NaveoService.Helpers;
using NaveoOneLib.Models.Permissions;
using NaveoOneLib.Models.Common;
using System.Web.Script.Serialization;
using NaveoOneLib.Models.Zones;
using NaveoOneLib.Models.Lookups;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class LookupTypeValuesController : ApiBaseController
    {
        BaseDTO baseDTO = new BaseDTO();

        #region LOOKUPTYPES
   

        [HttpGet, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "GetLookUpTypes")]
        public IHttpActionResult GetLookUpTypes()
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new LookUpTypeValuesService().GetLookUpTypes(securityTokenObj.securityToken.sConnStr);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "GetLookUpTypesById")]
        public IHttpActionResult GetLookUpTypesById(LookUpTypes lt)
        {
            /*Body 
           {
            "zoneId":[51]
            }
           */

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel bm = new LookUpTypeValuesService().GetLookUpTypesId(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr, lt.TID);
                    return Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "SaveLookUpTypes")]
        public IHttpActionResult SaveLookUpTypes(PutDTO LookUpTypesSave)
        {
           

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    LookUpTypes jsonSaveObject = serializer.Deserialize<LookUpTypes>(LookUpTypesSave.data.ToString());
                    Boolean bSave = new LookUpTypeValuesService().SaveLookUpTypes(jsonSaveObject, true, securityTokenObj.securityToken.sConnStr);
                    if (bSave)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "LookupTypeValues";
                        string action = "SaveLookUpTypes";
                        string rawRequest = LookUpTypesSave.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonSaveObject.TID, securityTokenObj.securityToken.sConnStr, true);
                    }
                    return Ok(bSave.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

  
        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "UpdateLookUpTypes")]
        public IHttpActionResult UpdateLookUpTypes(PutDTO LookUpTypesUpdate)
        {
            

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    LookUpTypes jsonSaveObject = serializer.Deserialize<LookUpTypes>(LookUpTypesUpdate.data.ToString());
                    Boolean bSave = new LookUpTypeValuesService().UpdateLookUpTypes(jsonSaveObject, false, securityTokenObj.securityToken.sConnStr);
                    if (bSave)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "LookupTypeValues";
                        string action = "UpdateLookUpTypes";
                        string rawRequest = LookUpTypesUpdate.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonSaveObject.TID, securityTokenObj.securityToken.sConnStr, true);
                    }
                    return Ok(bSave.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        //[HttpPost, ResponseType(typeof(BaseDTO))]
        //[Route(Headers.API_PREFIX + "DeleteLookUpTypes")]
        //public IHttpActionResult DeleteLookUpTypes(lZones lz)
        //{
        //    /*Body 
        //       {
        //            "zoneId":[51]
        //       }
        //   */

        //    SecurityTokenExtended securityTokenObj = ApiAuthentication();
        //    try
        //    {
        //        if (securityTokenObj.securityToken.IsAuthorized)
        //        {
        //            BaseDTO zd = new ZoneService().DeleteZoneById(securityTokenObj, lz.lZoneIDs);
        //            if (zd.data)
        //            {
        //                Guid userToken = securityTokenObj.securityToken.UserToken;
        //                string controller = "Zone";
        //                string action = "DeleteZoneById";
        //                string rawRequest = "{'lZoneIDs':'" + lz.lZoneIDs.ToString() + "'}";
        //                Telemetry.Trace(userToken, controller, action, rawRequest, lz.lZoneIDs[0], securityTokenObj.securityToken.sConnStr, true);
        //            }
        //            CommonHelper.DefaultData(zd, securityTokenObj);
        //            return Ok(zd);
        //        }
        //        return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
        //    }
        //    catch (Exception e)
        //    {
        //        return Ok(SecurityHelper.ExceptionMessage(e));
        //    }
        //}

        #endregion

        #region LOOKUPVALUES

        [HttpPost, ResponseType(typeof(TripDTO))]
        [Route(Headers.API_PREFIX + "GetLookUpValuesByLTName")]
        public IHttpActionResult GetLookUpValues(LookUpTypes lt)
        {
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    dtData mydtData = new LookUpTypeValuesService().GetLookUpValuesByLTName(lt.Code,securityTokenObj.securityToken.sConnStr);
                    return Ok(mydtData.ToObjectDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(BaseDTO))]
        [Route(Headers.API_PREFIX + "GetLookUpValuesById")]
        public IHttpActionResult GetLookUpValuesById(LookUpValues lv)
        {
           
            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel bm = new LookUpTypeValuesService().GetLookUpValuesId(securityTokenObj.securityToken.UserToken, securityTokenObj.securityToken.sConnStr, lv.VID);
                    return Ok(bm.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "SaveLookUpValues")]
        public IHttpActionResult SaveLookUpValues(PutDTO LookUpTypesSave)
        {


            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    LookUpValues jsonSaveObject = serializer.Deserialize<LookUpValues>(LookUpTypesSave.data.ToString());
                    Boolean bSave = new LookUpTypeValuesService().SaveLookUpValues(jsonSaveObject, true, securityTokenObj.securityToken.sConnStr);
                    if (bSave)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "LookupTypeValue";
                        string action = "SaveLookUpValues";
                        string rawRequest = LookUpTypesSave.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonSaveObject.TID, securityTokenObj.securityToken.sConnStr, true);
                    }
                    return Ok(bSave.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "UpdateLookUpValues")]
        public IHttpActionResult UpdateLookUpValues(PutDTO LookUpTypesUpdate)
        {


            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    LookUpValues jsonSaveObject = serializer.Deserialize<LookUpValues>(LookUpTypesUpdate.data.ToString());
                    Boolean bSave = new LookUpTypeValuesService().UpdateLookUpValues(jsonSaveObject, false, securityTokenObj.securityToken.sConnStr);
                    if (bSave)
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "LookupTypeValue";
                        string action = "UpdateLookUpValues";
                        string rawRequest = LookUpTypesUpdate.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonSaveObject.TID, securityTokenObj.securityToken.sConnStr, true);
                    }
                    return Ok(bSave.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }
        #endregion
    }
}