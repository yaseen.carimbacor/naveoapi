﻿using Microsoft.Web.Http;
using NaveoWebApi.Constants;
using System;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Net.Http;
using System.ServiceModel.Channels;

using NaveoService.Models.DTO;
using System.Data;
using NaveoOneLib.Models;
using NaveoOneLib.Services;
using System.Linq;
using System.Collections.Generic;
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Models.Common;
using NaveoOneLib.Services.Fuels;
using NaveoOneLib.Services.Assets;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class TestController : ApiBaseController
    {
        private const string HttpContext = "MS_HttpContext";
        private const string RemoteEndpointMessage = "System.ServiceModel.Channels.RemoteEndpointMessageProperty";
        #region API Methods
        [AllowAnonymous, HttpGet]
        [Route(Headers.API_PREFIX + "EndPoint")]
        public string EndPoint(HttpRequestMessage request = null)
        {
            request = request ?? Request;

            // 1st method
            //if (request.Properties.ContainsKey("MS_HttpContext"))
            //{
            //    return ((HttpContextWrapper)request.Properties["MS_HttpContext"]).Request.UserHostAddress;
            //}
            //else if (HttpContext.Current != null)
            //{
            //    return HttpContext.Current.Request.UserHostAddress;
            //}
            //else
            //{
            //    return null;
            //}

            //2nd method (Better)
            // Web-hosting
            if (request.Properties.ContainsKey(HttpContext))
            {
                dynamic ctx = request.Properties[HttpContext];
                if (ctx != null)
                {
                    return ctx.Request.UserHostAddress;
                }
            }
            //Self-hosting
            if (request.Properties.ContainsKey(RemoteEndpointMessage))
            {
                dynamic remoteEndpoint = request.Properties[RemoteEndpointMessage];
                if (remoteEndpoint != null)
                {
                    return remoteEndpoint.Address;
                }
            }
            //Owin-hosting
            //if (request.Properties.ContainsKey(OwinContext))
            //{
            //    dynamic ctx = request.Properties[OwinContext];
            //    if (ctx != null)
            //    {
            //        return ctx.Request.RemoteIpAddress;
            //    }
            //}
            if (System.Web.HttpContext.Current != null)
            {
                return System.Web.HttpContext.Current.Request.UserHostAddress;
            }
            // In case of failure
            return "0.0.0.0";
        }

        [AllowAnonymous, HttpGet]
        [Route(Headers.API_PREFIX + "MyHost")]
        public string MyHost()
        {
            return System.Web.HttpContext.Current.Request.UserHostAddress;
        }

        [AllowAnonymous, HttpGet]
        [Route(Headers.API_PREFIX + "AssetID")]
        public Asset AssetID()
        {
            return new Asset();
        }

        [AllowAnonymous, HttpGet]
        [Route(Headers.API_PREFIX + "BeginConn")]
        public Asset CreateBeginConnection()
        {
            Guid g = new GlobalFunctions.GlobalFunc().CreateBeginConnection("Naveo.mu");
            return new Asset();
        }

        #endregion
    }

    public class SensorData
    {
        public double fuelLevel { get; set; }
        public string tempTaggedAs { get; set; } //Analysing
        public string taggedAs { get; set; }
        public SensorData(double fuelLevel, string tempTaggedAs)
        {
            this.fuelLevel = fuelLevel;
            this.tempTaggedAs = tempTaggedAs;
            this.taggedAs = tempTaggedAs;
        }
    }
}