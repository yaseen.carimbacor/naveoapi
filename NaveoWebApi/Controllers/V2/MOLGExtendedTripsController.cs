﻿using Microsoft.Web.Http;
using NaveoService.Services;
using NaveoWebApi.Constants;
using NaveoWebApi.Helpers;
using System;
using System.Web.Http;
using System.Web.Http.Description;
using NaveoService.Models.DTO;
using NaveoWebApi.Models;
using NaveoOneLib.Models.Common;
using System.Web.Script.Serialization;
using NaveoService.Models;
using NaveoService.Helpers;
using NaveoOneLib.Models.Assets;
using NaveoOneLib.Services.Plannings;

namespace NaveoWebApi.Controllers.V2
{
    [ApiVersion("2.0")]
    public class MOLGExtendedTripsController : ApiBaseController
    {
        
      

        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "SaveMokaCustomTrips")]
        public IHttpActionResult SaveMokaCustomTrips(PutDTO CustomTripsToSave)
        {
            #region save/Update Body


            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    CustomizedTripsDTO jsonSaveObject = serializer.Deserialize<CustomizedTripsDTO>(CustomTripsToSave.data.ToString());
                    BaseDTO CustomTripsSaveUpdate = new CustomTripService().SaveMokaCustomTrips(securityTokenObj, jsonSaveObject, true);
                    if (CustomTripsSaveUpdate.sQryResult == "Succeeded")
                    {
                        Guid userToken = securityTokenObj.securityToken.UserToken;
                        string controller = "MOLGExtendedTrips";
                        string action = "SaveMokaCustomTrips";
                        string rawRequest = CustomTripsToSave.data.ToString();
                        Telemetry.Trace(userToken, controller, action, rawRequest, jsonSaveObject.customTrips[0].iID, securityTokenObj.securityToken.sConnStr, true);
                    }
                    CommonHelper.DefaultData(CustomTripsSaveUpdate, securityTokenObj);
                    return Ok(CustomTripsSaveUpdate);
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }


 

        [HttpPost, ResponseType(typeof(PutDTO))]
        [Route(Headers.API_PREFIX + "GetMokaCustomTripReport")]
        public IHttpActionResult GetMokaCustomTripReport(CustomTrips customTrips)
        {
            #region save/Update Body


            #endregion

            SecurityTokenExtended securityTokenObj = ApiAuthentication();
            try
            {
                if (securityTokenObj.securityToken.IsAuthorized)
                {
                    BaseModel customTripDTO = new NaveoService.Services.CustomTripService().GetMokaCustomTripsReport(securityTokenObj.securityToken.UserToken, customTrips.dateFrom, customTrips.dateTo, customTrips.assetdIds, securityTokenObj.securityToken.sConnStr, customTrips.sortColumns, customTrips.sortOrderAsc);
                    return Ok(customTripDTO.ToBaseDTO(securityTokenObj.securityToken));
                }
                return Ok(SecurityHelper.Unauthorized(securityTokenObj.securityToken.DebugMessage));
            }
            catch (Exception e)
            {
                return Ok(SecurityHelper.ExceptionMessage(e));
            }
        }

    }
}