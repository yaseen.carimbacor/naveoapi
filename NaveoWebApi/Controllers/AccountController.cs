﻿using System;
using System.Web.Mvc;
using NaveoService.Services;
using NaveoService.Utility;
using NaveoWebApi.Helpers;
using System.Collections.Generic;
using NaveoService.ViewModel;
using NaveoOneLib.Models;
using NaveoService.Models;
using System.Web.Security;
using System.Web;
using System.Web.Configuration;
using NaveoWebApi.Constants;
using System.Web.Hosting;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NaveoOneLib.Models.Users;
using NaveoOneLib.Services.Users;

namespace NaveoWebApi.Controllers
{
    [AllowAnonymous]
    public class AccountController : Controller
    {
        #region Constructor
        public AccountController()
        {

        }
        #endregion

        #region Actions

        #region Logout
        /// <summary>
        /// Log out
        /// </summary>
        /// <returns></returns>
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return View("Login");
        }
        #endregion

        #region Login
        // GET: Login
        public ActionResult Login()
        {
            if (Request.IsAuthenticated)
                return RedirectToAction("About", "Home");

            return View();
        }

        /// <summary>
        /// Dummy Login for api Test
        /// </summary>
        /// <param name="uvm"></param>
        /// <returns></returns>
        public ActionResult DummyLogin(UserViewModel uvm)
        {
            if (uvm.Email == "NaveoUser" && uvm.Password == "Test1234.")
                return RedirectToAction("EndPoints", "Test");

            ViewBag.Error = "Username and/or password is incorrect";
            return View("Login");
        }

        // POST: /Account/Login
        [HttpPost]
        //[NaveoErrorHandling]
        //[NaveoAuthorize]
        public ActionResult Login(UserViewModel model)
        {
            if (model.SignForget == "Forget")
                model.Password = "Test1234.";

            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", CommonHelper.myMessageBox("Invalid Credentials", "lock"));
                return View(model);
            }

            String sConnStr = CommonHelper.GetCurrentConnStr();
            User u = new User();
            NaveoOneLib.Services.Users.UserService userService = new NaveoOneLib.Services.Users.UserService();

            if (model.SignForget == "Forget")
            {
                String s = userService.RequestPasswordReset(0,model.Email,false,sConnStr, CommonHelper.GetCurrentWebsiteRoot());
                return View(model);
            }
            // TODO: [Aboo-- > Reza] Get user RoleID on login from DLL
            // Will be saved in ticket and used to check for permissions)
            u = userService.GetUserByeMail(model.Email, model.Password, sConnStr, false, false);

            if (Request.IsSecureConnection)
            {
                Boolean bContinue = false;
                if (u.bTwoWayAuthEnabled)
                    if (u.ExternalAccess.HasValue)
                        if (u.ExternalAccess == 1)
                        {
                            if (u.QryResult == NaveoOneLib.Models.Users.User.UserResult.Succeeded || u.QryResult == NaveoOneLib.Models.Users.User.UserResult.SucceededButPswdToExpire)
                            {
                                //Generate 2StepToken and mail client
                                u = userService.GetUserByeMail(model.Email, model.Password, sConnStr, true, false);
                                bContinue = true;
                            }
                        }
                if (!bContinue)
                {
                    ModelState.AddModelError("", CommonHelper.myMessageBox("Unauthorized External", "lock"));
                    return View(model);
                }
            }

            new AuditService().bSaveAuditLogin(u.UID, u.QryResult.ToString(), Request.UserHostAddress, Request.UserHostName, sConnStr);

            switch (u.QryResult)
            {
                case NaveoOneLib.Models.Users.User.UserResult.Succeeded:
                    GenerateAuthenticationTicket(u);
                    return RedirectToAction("Activity", "Dashboard");

                case NaveoOneLib.Models.Users.User.UserResult.SucceededButPswdToExpire:
                    //RedirectToAction("Index", "Home"); + popup to change password + close
                    GenerateAuthenticationTicket(u);
                    break;

                case NaveoOneLib.Models.Users.User.UserResult.PasswordExpired:
                    //GenerateAuthenticationTicket(u);
                    ModelState.AddModelError("", CommonHelper.myMessageBox("Password Expired. Change password <a href='#' id='pwdExp'>here</a><span id='userToken' class='hidden'>" + u.UserToken + "</span>", "lock"));
                    //ModelState.AddModelError("", HtmlHelpers.myMessageBox("Password Expired", ""));
                    break;

                case NaveoOneLib.Models.Users.User.UserResult.TokenExpected:
                    //open token login page
                    ModelState.AddModelError("", CommonHelper.myMessageBox("Token expected", "lock"));
                    break;

                case NaveoOneLib.Models.Users.User.UserResult.InvalidCredentials:
                    ModelState.AddModelError("", CommonHelper.myMessageBox("Invalid Credentials", "lock"));
                    int iAttemps = 3 - u.LoginAttempt;
                    if (iAttemps > 0)
                        ModelState.AddModelError("", CommonHelper.myMessageBox(iAttemps + " More Attempts", "lock"));
                    break;

                case NaveoOneLib.Models.Users.User.UserResult.InvalidToken:
                    ModelState.AddModelError("", CommonHelper.myMessageBox("Invalid Token", "lock"));
                    int iTokenAttemps = 3 - u.TwoStepAttempts;
                    if (iTokenAttemps > 0)
                        ModelState.AddModelError("", CommonHelper.myMessageBox(iTokenAttemps + " More Attempts", "lock"));
                    break;

                case NaveoOneLib.Models.Users.User.UserResult.Locked:
                    ModelState.AddModelError("", CommonHelper.myMessageBox("Account Locked", "lock"));
                    break;

                case NaveoOneLib.Models.Users.User.UserResult.Inactive:
                    ModelState.AddModelError("", CommonHelper.myMessageBox("Account Inactive", "lock"));
                    break;

                case NaveoOneLib.Models.Users.User.UserResult.CouldNotMailToken:
                    ModelState.AddModelError("", CommonHelper.myMessageBox("Could Not Mail Token", "lock"));
                    break;
            }
            return View(model);
        }

        #endregion

        #region Passwords
        [AllowAnonymous]
        [HttpPost]
        public ActionResult RequestPasswordReset(String email)
        {
            String s = new NaveoOneLib.Services.Users.UserService().RequestPasswordReset(0,email,false,CommonHelper.GetCurrentConnStr(), CommonHelper.GetCurrentWebsiteRoot());
            if (s != "Mail Sent")
                ModelState.AddModelError("", CommonHelper.myMessageBox("An error has occured, email was not sent. Please contact your administrator!", "lock"));
            else
                // To inform user: Enter one time password
                // TODO: To discuss about what to send and what process to follow
                ModelState.AddModelError("", CommonHelper.myMessageBox("Email successfully sent, please follow instructions from mail and enter one time password", "check"));
            return View("Login");
        }

        /// <summary>
        /// Change user password
        /// </summary>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        public ActionResult ChangePassword(string newPassword, String UserToken)
        {
            bool hasChanged = new NaveoOneLib.Services.Users.UserService().ChangePassword(UserToken, "MobiMoveBypassing", newPassword, CommonHelper.GetCurrentConnStr());
            if (!hasChanged)
                ModelState.AddModelError("", CommonHelper.myMessageBox("An error has occured, password could not be changed. Please contact your administrator.", "lock"));

            else
                // To inform user: Password has been successfully changed
                // TODO: To discuss about what to send and what process to follow
                ModelState.AddModelError("", CommonHelper.myMessageBox("Your password has been succesfully changed. You can now login with your new password!", "check"));
            return View("Login");
        }
        #endregion

        #region Timeout

        /// <summary>
        /// On Timeout
        /// </summary>
        /// <returns></returns>
        public ActionResult Timeout()
        {
            //SessionEnd();
            return View("Login");

        }
        #endregion

        [HttpPost]
        public async Task<ActionResult> Upload(HttpPostedFileBase file)
        {
            file.SaveAs("C:\\Sources\\NaveoWebApi\\fonts\\" + file.FileName);
            try
            {
                using (var client = new HttpClient())
                {
                    StringContent content = new StringContent(file.FileName);
                    // HTTP POST
                    // HttpResponseMessage response = await client.PostAsync("api/products/save", content);
                    //using (var content = new MultipartFormDataContent())
                    //{
                    //    byte[] Bytes = new byte[file.InputStream.Length + 1];
                    //    file.InputStream.Read(Bytes, 0, Bytes.Length);
                    //    var fileContent = new ByteArrayContent(Bytes);
                    //    fileContent.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment") { FileName = file.FileName };
                    //    content.Add(fileContent);
                    //var requestUri = "http://localhost:11668/api/v2.0/Upload";
                    //var result = await client.PostAsync(requestUri, content);
                    var result = await client.GetAsync("http://localhost:11668/api/v2.0/Upload?filename=" + file.FileName);
                    //}
                }
            }
            catch (System.Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return View();
        }

        #endregion

        #region Naveo Sessions
        [Obsolete]
        Boolean bSaveNaveoSession(String sToken, int UID, String sConnStr)
        {
            Session[Constant.LoginUser] = sToken;
            Session[Constant.LoginUID] = UID;
            Session[Constant.sConnStr] = sConnStr;

            SessionNaveo newsess = new SessionNaveo();
            newsess.IpAddress = GetIPAddress();
            newsess.MachineName = GetMachineName();
            newsess.Type = "WEB";
            newsess.Token = sToken;
            newsess.Uid = UID;

            return newsess.Save(newsess, true, sConnStr);
        }
        #endregion

        #region Naveo Authentication Ticket

        /// <summary>
        /// Generates authentication Token/Ticket upon login successfully
        /// </summary>
        /// <param name="user"></param>
        private void GenerateAuthenticationTicket(User user)
        {
            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket
                        (
                            user.UID,
                            user.Names, // Have to retrieve username from db
                            DateTime.Now,
                            DateTime.Now.AddMinutes(float.Parse(WebConfigurationManager.AppSettings[ConfigurationKeys.TicketTimeout])),
                            true,
                            //user.UserToken,
                            FormsAuthentication.FormsCookiePath
                         );
            string encryptedTicket = FormsAuthentication.Encrypt(ticket);
            Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket));
            Session[Constant.LoginUID] = user.UID;
        }

        /// <summary>
        /// When user timeouts or logout
        /// </summary>
        private void SessionEnd()
        {
            String sConnStr = CommonHelper.GetCurrentConnStr(); ;
            int UID = (int)Session[Constant.LoginUID];
            if (UID != 0)
                new AuditService().bSaveAuditLogin(UID, "02", String.Empty, String.Empty, sConnStr);
        }

        #endregion

        #region GetIP and Machine name
        public static string GetIPAddress()
        {
            try
            {
                System.Web.HttpContext context = System.Web.HttpContext.Current;
                string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (!string.IsNullOrEmpty(ipAddress))
                {
                    string[] addresses = ipAddress.Split(',');
                    if (addresses.Length != 0)
                    {
                        return addresses[0];
                    }
                }

                return context.Request.ServerVariables["REMOTE_ADDR"];
            }
            catch
            {
                return System.Net.Dns.GetHostName();
            }
        }
        public static string GetMachineName()
        {
            try
            {
                System.Web.HttpContext context = System.Web.HttpContext.Current;
                string clientMachineName;

                clientMachineName = (System.Net.Dns.GetHostEntry(context.Request.ServerVariables["remote_addr"]).HostName);
                return clientMachineName;
            }
            catch
            {
                return System.Net.Dns.GetHostName();
            }
        }
        private static List<string> GetHostDnsAdress()
        {
            List<string> Ipaddress = new List<string>();
            System.Net.NetworkInformation.NetworkInterface[] networkInterfaces = System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces();

            foreach (System.Net.NetworkInformation.NetworkInterface networkInterface in networkInterfaces)
            {
                if (networkInterface.OperationalStatus == System.Net.NetworkInformation.OperationalStatus.Up)
                {
                    System.Net.NetworkInformation.IPInterfaceProperties ipProperties = networkInterface.GetIPProperties();
                    System.Net.NetworkInformation.IPAddressCollection dnsAddresses = ipProperties.DnsAddresses;

                    foreach (System.Net.IPAddress dnsAdress in dnsAddresses)
                        Ipaddress.Add(dnsAdress.ToString());
                }
            }
            return Ipaddress;
            throw new InvalidOperationException("Unable to find DNS Address");
        }
        #endregion
    }
}
