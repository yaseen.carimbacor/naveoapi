﻿using NaveoOneLib.Models;
using NaveoOneLib.Models.Permissions;
using NaveoService.Models;
using NaveoService.Models.Custom;
using NaveoService.Services;
using NaveoWebApi.Constants;
using NaveoWebApi.Controllers.Filters;
using NaveoWebApi.Helpers;
using NaveoWebApi.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;

namespace NaveoWebApi.Controllers
{
    // SSl Int --> Comment [RequireSsl] to prevent https enforcement 
    // Put "SSL Enabled" to false in properties window 
    // (Select NaveoWebApi project and click on properties window to do that)
    [RequireSsl]
    [NaveoAuthorize]
    public class ApiBaseController : ApiController
    {
        /// <summary>
        /// Authentication for API access
        /// </summary>
        /// <returns></returns>
        protected SecurityTokenExtended ApiAuthentication()
        {
            SecurityTokenExtended securityTokenObj = new SecurityTokenExtended(false);

            #region Pre-Requisites
            var headers = Request.Headers;
            string hostUrl = HttpContext.Current.Request.Url.DnsSafeHost;
            String sConnStr = CommonHelper.GetCurrentConnStr(hostUrl, headers);
            Guid userToken = SecurityHelper.GetUserToken(headers);
            #endregion

            // Verify if database is valid 
            //TODO : [ABOO] Send back Debug message back to front in all api calls. example done in TripController. Pls analyse and replicate in all codes
            bool isDatabaseValid = SecurityHelper.IsDatabaseValid(sConnStr, headers);
            if (isDatabaseValid)
            {
                AllowedUser allowedUser = AuthToken.allowedUsers.SingleOrDefault(x => x.issuedTo == userToken);
                if (allowedUser != null)
                {
                    //Check if timedout
                    if (!SecurityHelper.HasTimedOut(allowedUser.accessedAt))
                    {
                        //Check if expired
                        if (SecurityHelper.IsTokenExpired(allowedUser.expiryDate))
                        {
                            securityTokenObj.securityToken.IsAuthorized = false;
                            securityTokenObj.securityToken.IsExpired = true;
                            securityTokenObj.securityToken.DebugMessage = Headers.EXPIRED;
                            AuthToken.allowedUsers.Remove(allowedUser);
                        }

                        #region WhiteListed Ips
                        // If Ip is whitelisted
                        if (SecurityService.isIPWhiteListed(HttpContext.Current.Request.UserHostAddress))
                        {
                            allowedUser.accessedAt = DateTime.Now;

                            securityTokenObj.securityToken.IsAuthorized = true;
                            securityTokenObj.securityToken.HasTimedOut = false;
                            securityTokenObj.securityToken.DebugMessage = Headers.AUTHORIZED;
                            securityTokenObj.securityToken.sConnStr = sConnStr;
                            securityTokenObj.securityToken.UserToken = userToken;
                            securityTokenObj.securityToken.UID = allowedUser.UserID;
                            securityTokenObj.securityToken.Page = SecurityHelper.GetPageRequested(headers);
                            securityTokenObj.securityToken.LimitPerPage = SecurityHelper.GetLimit(headers);
                            return securityTokenObj;
                        }
                        #endregion
                        else // Go through Token-based authentication
                        {
                            //check if header Auth is same
                            Guid headerAuthToken = SecurityHelper.GetAuthToken(headers);
                            if (headerAuthToken == allowedUser.oneTimeToken)
                            {
                                allowedUser.accessedAt = DateTime.Now;

                                securityTokenObj.securityToken.IsAuthorized = true;
                                securityTokenObj.securityToken.HasTimedOut = false;
                                securityTokenObj.securityToken.DebugMessage = Headers.AUTHORIZED;
                                securityTokenObj.securityToken.UserToken = userToken;
                                securityTokenObj.securityToken.UID = allowedUser.UserID;
                                securityTokenObj.securityToken.sConnStr = sConnStr;
                                securityTokenObj.securityToken.Page = SecurityHelper.GetPageRequested(headers);
                                securityTokenObj.securityToken.LimitPerPage = SecurityHelper.GetLimit(headers);
                                securityTokenObj.securityToken.OneTimeToken = allowedUser.oneTimeToken;
                            }
                            else
                                securityTokenObj.securityToken.DebugMessage = Headers.INVALIDTOKEN;
                        }
                    }
                    else
                    {
                        securityTokenObj.securityToken.HasTimedOut = true;
                        securityTokenObj.securityToken.IsAuthorized = false;
                        securityTokenObj.securityToken.DebugMessage = Headers.TIMEOUT;
                        AuthToken.allowedUsers.Remove(allowedUser);
                    }
                }
                else
                    securityTokenObj.securityToken.DebugMessage = Headers.UNAUTHORIZED;
            }
            else
                securityTokenObj.securityToken.DebugMessage = Headers.INVALIDDB;

            return securityTokenObj;
        }
    }
}
