﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoWebApi.Constants
{
    /// <summary>
    /// Configuration keys
    /// </summary>
    /// TODO: Put all config keys in this class
    public static class ConfigurationKeys
    {
        public const string TicketTimeout = "TicketTimeout";
        public const string CoreConnStr = "DB1";
        public const string CoreConnStrDB2 = "DB2";
        public const string CoreConnStrDB3 = "DB3";
        public const string CoreConnStrDB4 = "DB4";
        public const string CoreConnStrDB5 = "DB5";
        public const string CoreConnStrDB6 = "DB6";
        public const string CoreConnStrDB7 = "DB7";
        public const string CoreConnStrDB8 = "DB8";
        public const string CoreConnStrDB9 = "DB9";
        public const string CoreConnStrDB10 = "DB10";
        public const string CoreConnStrDB11 = "DB11";
        public const string CoreConnStrDB12 = "DB12";
        public const string CoreConnStrDB13 = "DB13";
        public const string CoreConnStrDB14 = "DB14";
        public const string CoreConnStrDB15 = "DB15";
        public const string CoreConnStrDB16 = "DB16";
        public const string CoreConnStrDB17 = "DB17";
        public const string CoreConnStrDB18 = "DB18";
        public const string CoreConnStrDB19 = "DB19";
        public const string CoreConnStrDB20 = "DB20";
        public const string CoreConnStrDB21 = "DB21";
        public const string CoreConnStrDB22 = "DB22";
        public const string CoreConnStrDB23 = "DB23";
        public const string CoreConnStrDB24 = "DB24";
        public const string CoreConnStrDB25 = "DB25";
        public const string CoreConnStrDB26 = "DB26";
        public const string CoreConnStrDB27 = "DB27";
        public const string CoreConnStrDB28 = "DB28";
        public const string CoreConnStrDB29 = "DB29";
        public const string CoreConnStrDB30 = "DB30";
        public const string CoreConnStrDB99 = "DB99";
        public const string ExportLocation = "ExportLocation";
        public const string ImportLocation = "ImportLocation";

        public static int TimeOut = 60;
    }
}