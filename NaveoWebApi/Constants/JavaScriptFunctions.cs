﻿namespace NaveoWebApi.Constants
{
    /// <summary>
    /// Javascript Function Names
    /// </summary>
    public class JavaScriptFunctions
    {
        /// <summary>
        /// This class contains contants used for JavaScript Functions
        /// </summary>
        public struct AjaxHandlers
        {
            /// <summary>
            /// The on failure handler
            /// </summary>
            public const string OnFailure = "handleFailure";

            /// <summary>
            /// The on success handler
            /// </summary>
            public const string OnSuccess = "handleSuccess";

            /// <summary>
            /// The on begin handler
            /// </summary>
            public const string OnBegin = "handleLoading";

            /// <summary>
            /// The on complete handler
            /// </summary>
            public const string OnComplete = "handleComplete";

            /// <summary>
            /// The on complete contract creation handler
            /// </summary>
            public const string OnCompleteUserCreation = "handleCompleteContractCreation";

            /// <summary>
            /// The on complete contract update handler
            /// </summary>
            public const string OnCompleteUserUpdate = "handleCompleteContractCreation";

            /// <summary>
            /// The on refresh handler
            /// </summary>
            public const string OnRefresh = "handleRefresh";
        }
    }
}
