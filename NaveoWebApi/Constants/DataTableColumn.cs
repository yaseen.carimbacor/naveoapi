﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoWebApi.Constants
{
    /// <summary>
    /// Configuration keys
    /// </summary>
    /// TODO: Put all config keys in this class
    public class DataTableColumn
    {
        public const string TOANALYSE = "ToAnalyse";
        public const string TAG = "Tag";
        public const string CONVERTED_VALUE = "ConvertedValue";
        public const string TYPE_VALUE = "TypeValue";
        public const string FINAL_VALUE = "FinalValue";
        public const string UID = "UID";
    }
}