﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoWebApi.Constants
{
    /// <summary>
    /// Configuration keys
    /// </summary>
    /// TODO: Put all config keys in this class
    public class Fuel
    {
        public const string FILL = "Fill";
        public const string DROP = "Drop";
        public const string NONE = "None";
        public const string REAL = "Real";
        public const string TEMP_DROP = "TempDrop";
        public const string TEMP_FILL = "TempFill";
        public const string FALSE_FILL = "False Fill";
        public const string FALSE_DROP = "False Drop";
    }
}