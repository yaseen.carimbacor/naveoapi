﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoWebApi.Constants
{
    public static class Headers
    {
        #region Packet Headers
        public const string ONE_TIME_TOKEN = "ONE_TIME_TOKEN";  //Optional. ignored when ip whitelisted
        public const string USER_TOKEN = "USER_TOKEN";          //compulsory
        public const string HOST_URL = "HOST_URL";              //Optional. mainly for mobile. uses safedns (http address) when HOST_URL not used
        public const string PAGE = "PAGE";                  //Optional. Page Number for data retrieval
        public const string ROWS_PER_PAGE = "ROWS_PER_PAGE";        //Optional. Number of rows per page
        public const string IS_DEBUG_MODE = "IS_DEBUG_MODE";

        #endregion

        #region API Metadata

        public const string API_PREFIX = "v{version:apiVersion}/";

        #endregion

        public const string UNAUTHORIZED = "Unauthorized";
        public const string AUTHORIZED = "Succeeded";
        public const string FAILED = "Failed";
        public const string EXPIRED = "Expired";
        public const string TIMEOUT = "TimeOut";
        public const string INVALIDDB = "InvalidDatabase";
        public const string INVALIDTOKEN = "InvalidToken";
        public static readonly List<string> LIST = new List<string>() { ONE_TIME_TOKEN, USER_TOKEN };
        public const int DEFAULT_START_ROW = 2;
    }
}