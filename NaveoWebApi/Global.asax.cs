﻿using NaveoService.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Http;
using NaveoWebApi.Helpers;
using NaveoService.Utility;
using NaveoOneLib.Models;
using NaveoWebApi.Handlers;
using NaveoWebApi.Constants;
using System.Configuration;
using Serilog;
using System.IO;

namespace NaveoWebApi
{
    public class MvcApplication : System.Web.HttpApplication
    {
        [HandleError]
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

            //from Nabla
            GlobalConfiguration.Configuration.MessageHandlers.Add(new CorsHandler());

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            HttpConfiguration config = new HttpConfiguration();
            WebApiConfig.Register(config);

            if (ConfigurationManager.AppSettings["apiTimeOut"] != null)
                ConfigurationKeys.TimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["apiTimeOut"]);

            String[] Lines = System.IO.File.ReadAllLines(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "\\Registers\\databases.dat");
            foreach (String sConnStr in Lines)
                if (!String.IsNullOrEmpty(sConnStr))
                    try
                    {
                       DBUpdate(sConnStr);
                    }
                    catch (System.Exception ex)
                    {
                        System.IO.File.AppendAllText(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "\\Registers\\err.dat", ex.Message);
                    }

            CultureInfo cInf = (CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            cInf.DateTimeFormat.DateSeparator = "/";
            cInf.DateTimeFormat.ShortDatePattern = "dd/MMM/yyyy";
            cInf.DateTimeFormat.LongDatePattern = "dd/MMM/yyyy hh:mm:ss tt";
            System.Threading.Thread.CurrentThread.CurrentCulture = cInf;
            System.Threading.Thread.CurrentThread.CurrentUICulture = cInf;
        }
        public void DBUpdate(String sConnStr)
        {
            new dbUpdateService().dbUpdate(sConnStr);
        }

        //TODO:Session end using FormsAuthenticationTicket. For Aboo [Done]
        //[Obsolete]
        //void Session_End(object sender, EventArgs e)
        //{
        //    String userId = Session[Constant.LoginUser] == null ? string.Empty : (string)Session[Constant.LoginUser];
        //    String sConnStr = (String)Session[Constant.sConnStr];
        //    if (!String.IsNullOrEmpty(sConnStr))
        //    {
        //        NaveoOneLib.Models.SessionNaveo newsess = new NaveoOneLib.Models.SessionNaveo();
        //        newsess.Type = "WEB";
        //        newsess.Token = userId;
        //        newsess.Uid = 0;
        //        newsess.Delete(newsess, sConnStr);

        //        int? UID = (int?)CommonHelper.UserNavId();
        //        if (UID.HasValue)
        //            new AuditService().bSaveAuditLogin((int)UID, "02", String.Empty, String.Empty, sConnStr);
        //    }
        //}
    }
}
