﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaveoWebApi.Models
{
    public class Message
    {
        #region Properties 
        
        public string Content { get; set; }

        #endregion
        
    }
}
