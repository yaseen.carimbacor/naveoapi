﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoWebApi.Models
{
    public class AssetSummary
    {
        public int assetId { get; set; }
        public DateTime dateFrom { get; set; }
        public DateTime dateTo { get; set; }
        public double iMinTripDistance { get; set; }
    }

}