﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoWebApi.Models
{
    public class Device
    {
        public String deviceID { get; set; }
        public int nOfRec { get; set; } = 10;
        public Boolean SummaryData { get; set; } = false;
        public Boolean sortAsc { get; set; } = false;
        public Boolean oldData { get; set; } = false;
    }

    public class MessageToDevice
    {
        public String deviceID { get; set; }
        public String sUnitModel { get; set; }
        public String sMessage { get; set; }
    }
}