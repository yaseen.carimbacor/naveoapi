﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoWebApi.Models
{
    public class User
    {
        public int userId { get; set; }
        public Guid userToken { get; set; }
        public String firstName { get; set; }
        public String lastName { get; set; }
        public String email { get; set; }
        public String password { get; set; }
        public String oldPassword { get; set; }
        public List<int> luserID { get; set; } = new List<int>();
        public DateTime lastLogin { get; set; }
        public DateTime lastLogout { get; set; }
    }



    public class SystemAdmin {
        public String email { get; set; }
        public int phone { get; set; }
    }

    public class UpdateUserStatus {
        public int UserID { get; set; }
        public String Status { get; set; }
    }
}