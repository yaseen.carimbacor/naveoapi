﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoWebApi.Models
{
    public class LoginResult
    {
        public User user { get; set; }

        public int loginAttempts { get; set; }
        //public String sQryResult { get; set; }
    }
}