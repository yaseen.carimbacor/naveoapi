﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoWebApi.Models
{ 
    public class siteLogin
    {
        public User user { get; set; }
        public String companyToken { get; set; }
    }

    public class lAssets
    {
        public int assetId { get; set; }
    }


    public class lRole
    {
        public int roleID { get; set; }
    }


    public class ServerName
    {
        public String sServerName { get; set; }
    }
}