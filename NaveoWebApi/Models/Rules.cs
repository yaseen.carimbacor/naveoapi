﻿using NaveoOneLib.Models.Rules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoWebApi.Models
{
    public class apiRules
    {
        public String fuelType { get; set; }
        public List<int> lUsers { get; set; } = new List<int>();

        public List<notifyList> notificationList { get; set; } = new List<notifyList>();

        public string ruleCategory { get; set; }
    }

}