﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoWebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class Ticket
    {
        /// <summary>
        /// 
        /// </summary>
        public Guid oneTimeToken { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int UID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Guid userToken { get; set; }

        public String senderIp { get; set; }
    }
}