﻿using NaveoService.Models;
using NaveoService.Models.DTO;
using NaveoService.Services;
using NaveoWebApi.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Security;
using static NaveoOneLib.Models.Users.User;

namespace NaveoWebApi.Helpers
{
    public static class CommonHelper
    {
        /// <summary>
        /// Get Current db name 
        /// </summary>
        /// <returns></returns>
        public static string GetCurrentConnStr()
        {
            if (HttpContext.Current == null)
                return String.Empty;

            String sConnStr = String.Empty;
            var hostUrl = HttpContext.Current.Request.Url.DnsSafeHost;
            hostUrl = hostUrl.Replace(".naveo.mu", String.Empty);
            NaveoOneLib.Models.Users.User.NaveoDatabases myEnum;
            if (Enum.TryParse(hostUrl, true, out myEnum))
            {
                switch (myEnum)
                {
                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Demo:
                        sConnStr = ConfigurationKeys.CoreConnStr;
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Mercury:
                        sConnStr = ConfigurationKeys.CoreConnStrDB2;
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Venus:
                        sConnStr = ConfigurationKeys.CoreConnStrDB3;
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Earth:
                        sConnStr = ConfigurationKeys.CoreConnStrDB4;
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Mars:
                        sConnStr = ConfigurationKeys.CoreConnStrDB5;
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Jupiter:
                        sConnStr = ConfigurationKeys.CoreConnStrDB6;
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Saturn:
                        sConnStr = ConfigurationKeys.CoreConnStrDB7;
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Uranus:
                        sConnStr = ConfigurationKeys.CoreConnStrDB8;
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Neptune:
                        sConnStr = ConfigurationKeys.CoreConnStrDB9;
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Pluto:
                        sConnStr = ConfigurationKeys.CoreConnStrDB10;
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Baby:
                        sConnStr = ConfigurationKeys.CoreConnStrDB11;
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Kids:
                        sConnStr = ConfigurationKeys.CoreConnStrDB12;
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Sun:
                        sConnStr = ConfigurationKeys.CoreConnStrDB13;
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Moon:
                        sConnStr = ConfigurationKeys.CoreConnStrDB14;
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.BlackWidow:
                        sConnStr = ConfigurationKeys.CoreConnStrDB15;
                        break;

                    case NaveoDatabases.Sirius:
                        sConnStr = ConfigurationKeys.CoreConnStrDB16;
                        break;

                    case NaveoDatabases.Vega:
                        sConnStr = ConfigurationKeys.CoreConnStrDB17;
                        break;

                    case NaveoDatabases.Pegasus:
                        sConnStr = ConfigurationKeys.CoreConnStrDB18;
                        break;

                    case NaveoDatabases.Andromeda:
                        sConnStr = ConfigurationKeys.CoreConnStrDB19;
                        break;

                    case NaveoDatabases.Centaurus:
                        sConnStr = ConfigurationKeys.CoreConnStrDB20;
                        break;

                    case NaveoDatabases.Cygnus:
                        sConnStr = ConfigurationKeys.CoreConnStrDB21;
                        break;

                    case NaveoDatabases.Cosmos:
                        sConnStr = ConfigurationKeys.CoreConnStrDB22;
                        break;


                    case NaveoDatabases.Epsilon:
                        sConnStr = ConfigurationKeys.CoreConnStrDB23;
                        break;


                }
            }
            if (sConnStr == String.Empty)
                sConnStr = ConfigurationKeys.CoreConnStrDB99;

            return sConnStr;
        }

        /// <summary>
        /// Get current db name using parameters [Mobile Specific]
        /// </summary>
        /// <param name="hostUrl"></param>
        /// <param name="headers"></param>
        /// <returns></returns>
        public static string GetCurrentConnStr(string hostUrl, HttpRequestHeaders headers)
        {
            //Case for mobile app
            //Fix url will be Demo.naveo.mu
            //Host_url will be the mobile registered drop down
            //if Host_url is blank, use hostUrl, else use Host_url from header 
            String s = GetHostUrl(headers);
            if (!String.IsNullOrEmpty(s))
                hostUrl = s;

            if (string.IsNullOrEmpty(hostUrl))
                return String.Empty;

            String sConnStr = ConfigurationKeys.CoreConnStrDB99;
            hostUrl = hostUrl.ToLower();
            hostUrl = hostUrl.Replace(".naveo.mu", String.Empty);
            NaveoOneLib.Models.Users.User.NaveoDatabases myEnum;
            if (Enum.TryParse(hostUrl, true, out myEnum))
            {
                switch (myEnum)
                {
                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Demo:
                        sConnStr = ConfigurationKeys.CoreConnStr;
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Mercury:
                        sConnStr = ConfigurationKeys.CoreConnStrDB2;
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Venus:
                        sConnStr = "DB3";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Earth:
                        sConnStr = "DB4";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Mars:
                        sConnStr = "DB5";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Jupiter:
                        sConnStr = "DB6";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Saturn:
                        sConnStr = "DB7";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Uranus:
                        sConnStr = "DB8";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Neptune:
                        sConnStr = "DB9";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Pluto:
                        sConnStr = "DB10";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Baby:
                        sConnStr = "DB11";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Kids:
                        sConnStr = "DB12";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Sun:
                        sConnStr = "DB13";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Moon:
                        sConnStr = "DB14";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.BlackWidow:
                        sConnStr = "DB15";
                        break;

                    case NaveoDatabases.Sirius:
                        sConnStr = "DB16";
                        break;

                    case NaveoDatabases.Vega:
                        sConnStr = "DB17";
                        break;

                    case NaveoDatabases.Pegasus:
                        sConnStr = "DB18";
                        break;

                    case NaveoDatabases.Andromeda:
                        sConnStr = "DB19";
                        break;

                    case NaveoDatabases.Centaurus:
                        sConnStr = "DB20";
                        break;

                    case NaveoDatabases.Cygnus:
                        sConnStr = "DB21";
                        break;

                    case NaveoDatabases.Cosmos:
                        sConnStr = "DB22";
                        break;


                    case NaveoDatabases.Epsilon:
                        sConnStr = "DB23";
                        break;
                }
            }
            else
            {
                //TODO: Find a way to detect environment (e.g. hostUrl.contains("localhost"))
                if (hostUrl.Contains("rezahp") || hostUrl.Contains("localhost"))
                {
                    hostUrl = GetHostUrl(headers);
                    hostUrl = hostUrl.ToLower();
                    hostUrl = hostUrl.Replace(".naveo.mu", String.Empty);
                    if (Enum.TryParse(hostUrl, true, out myEnum))
                    {
                        switch (myEnum)
                        {
                            case NaveoOneLib.Models.Users.User.NaveoDatabases.Demo:
                                sConnStr = ConfigurationKeys.CoreConnStr;
                                break;

                            case NaveoOneLib.Models.Users.User.NaveoDatabases.Mercury:
                                sConnStr = ConfigurationKeys.CoreConnStrDB2;
                                break;

                            case NaveoOneLib.Models.Users.User.NaveoDatabases.Venus:
                                sConnStr = "DB3";
                                break;

                            case NaveoOneLib.Models.Users.User.NaveoDatabases.Earth:
                                sConnStr = "DB4";
                                break;

                            case NaveoOneLib.Models.Users.User.NaveoDatabases.Mars:
                                sConnStr = "DB5";
                                break;

                            case NaveoOneLib.Models.Users.User.NaveoDatabases.Jupiter:
                                sConnStr = "DB6";
                                break;

                            case NaveoOneLib.Models.Users.User.NaveoDatabases.Saturn:
                                sConnStr = "DB7";
                                break;

                            case NaveoOneLib.Models.Users.User.NaveoDatabases.Uranus:
                                sConnStr = "DB8";
                                break;

                            case NaveoOneLib.Models.Users.User.NaveoDatabases.Neptune:
                                sConnStr = "DB9";
                                break;

                            case NaveoOneLib.Models.Users.User.NaveoDatabases.Pluto:
                                sConnStr = "DB10";
                                break;

                            case NaveoOneLib.Models.Users.User.NaveoDatabases.Baby:
                                sConnStr = "DB11";
                                break;

                            case NaveoOneLib.Models.Users.User.NaveoDatabases.Kids:
                                sConnStr = "DB12";
                                break;

                            case NaveoOneLib.Models.Users.User.NaveoDatabases.Sun:
                                sConnStr = "DB13";
                                break;

                            case NaveoOneLib.Models.Users.User.NaveoDatabases.Moon:
                                sConnStr = "DB14";
                                break;

                            case NaveoOneLib.Models.Users.User.NaveoDatabases.BlackWidow:
                                sConnStr = "DB15";
                                break;


                            case NaveoDatabases.Sirius:
                                sConnStr = "DB16";
                                break;

                            case NaveoDatabases.Vega:
                                sConnStr = "DB17";
                                break;

                            case NaveoDatabases.Pegasus:
                                sConnStr = "DB18";
                                break;

                            case NaveoDatabases.Andromeda:
                                sConnStr = "DB19";
                                break;

                            case NaveoDatabases.Centaurus:
                                sConnStr = "DB20";
                                break;

                            case NaveoDatabases.Cygnus:
                                sConnStr = "DB21";
                                break;

                            case NaveoDatabases.Cosmos:
                                sConnStr = "DB22";
                                break;


                            case NaveoDatabases.Epsilon:
                                sConnStr = "DB23";
                                break;
                        
                    }
                    }
                }
            }

            return sConnStr;
        }

        public static string GetAPIConnStr(String SvrName)
        {
            String sConnStr = String.Empty;
            NaveoOneLib.Models.Users.User.NaveoDatabases myEnum;
            if (Enum.TryParse(SvrName, true, out myEnum))
            {
                switch (myEnum)
                {
                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Demo:
                        sConnStr = ConfigurationKeys.CoreConnStr;
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Mercury:
                        sConnStr = ConfigurationKeys.CoreConnStrDB2;
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Venus:
                        sConnStr = "DB3";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Earth:
                        sConnStr = "DB4";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Mars:
                        sConnStr = "DB5";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Jupiter:
                        sConnStr = "DB6";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Saturn:
                        sConnStr = "DB7";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Uranus:
                        sConnStr = "DB8";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Neptune:
                        sConnStr = "DB9";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Pluto:
                        sConnStr = "DB10";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Baby:
                        sConnStr = "DB11";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Kids:
                        sConnStr = "DB12";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Sun:
                        sConnStr = "DB13";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.Moon:
                        sConnStr = "DB14";
                        break;

                    case NaveoOneLib.Models.Users.User.NaveoDatabases.BlackWidow:
                        sConnStr = "DB15";
                        break;

                    case NaveoDatabases.Sirius:
                        sConnStr = "DB16";
                        break;

                    case NaveoDatabases.Vega:
                        sConnStr = "DB17";
                        break;

                    case NaveoDatabases.Pegasus:
                        sConnStr = "DB18";
                        break;

                    case NaveoDatabases.Andromeda:
                        sConnStr = "DB19";
                        break;

                    case NaveoDatabases.Centaurus:
                        sConnStr = "DB20";
                        break;

                    case NaveoDatabases.Cygnus:
                        sConnStr = "DB21";
                        break;

                    case NaveoDatabases.Cosmos:
                        sConnStr = "DB22";
                        break;


                    case NaveoDatabases.Epsilon:
                        sConnStr = "DB23";
                        break;
                }
            }
            if (sConnStr == String.Empty)
                sConnStr = ConfigurationKeys.CoreConnStrDB99;

            return sConnStr;
        }

        /// <summary>
        /// Gets the current user Id (UID)
        /// </summary>
        /// <returns></returns>
        public static int UserNavId()
        {
            var decryptedtTicket = DecryptAuthTicket();
            return (decryptedtTicket != null) ? decryptedtTicket.Version : 0;
        }

        /// <summary>
        /// Gets the current user token
        /// </summary>
        /// <returns></returns>
        public static string GetUserToken()
        {
            var decryptedtTicket = DecryptAuthTicket();
            return (decryptedtTicket != null) ? decryptedtTicket.UserData : string.Empty;
        }

        /// <summary>
        /// Gets the user role id
        /// </summary>
        /// <returns></returns>
        public static int GetRoleId()
        {
            var decryptedtTicket = DecryptAuthTicket();
            return (decryptedtTicket != null) ? int.Parse(decryptedtTicket.UserData) : 0;
        }

        public static string GetCurrentHostURL(string hostUrl, HttpRequestHeaders headers)
        {
            String s = GetHostUrl(headers);
            if (!String.IsNullOrEmpty(s))
                hostUrl = s;

            hostUrl = hostUrl.ToLower();

            NaveoOneLib.Models.Users.User.NaveoDatabases myEnum;
            if (!Enum.TryParse(hostUrl, true, out myEnum))
            {
                //TODO: Find a way to detect environment (e.g. hostUrl.contains("localhost"))
                if (hostUrl.Contains("rezahp") || hostUrl.Contains("localhost"))
                    hostUrl = GetHostUrl(headers);
            }

            return hostUrl;
        }

        public static string IsCurrentAction(this HtmlHelper helper, string actionName, string controllerName)
        {
            string currentControllerName = (string)helper.ViewContext.RouteData.Values["controller"];
            string currentActionName = (string)helper.ViewContext.RouteData.Values["action"];

            if (currentControllerName.Equals(controllerName, StringComparison.CurrentCultureIgnoreCase) && currentActionName.Equals(actionName, StringComparison.CurrentCultureIgnoreCase))
                return "active";

            return "";
        }

        public static string IsCurrentParentMenu(this HtmlHelper helper, params string[] controllerName)
        {
            string currentControllerName = (string)helper.ViewContext.RouteData.Values["controller"];
            string currentActionName = (string)helper.ViewContext.RouteData.Values["action"];

            foreach (var controller in controllerName)
            {
                var controllerSplit = controller.Split('|');
                if (controllerSplit.Count() > 1)
                {
                    if (currentControllerName.Equals(controllerSplit[0], StringComparison.CurrentCultureIgnoreCase))
                    {
                        if (currentActionName.Equals(controllerSplit[1]))
                            return "active";
                    }
                }
                else
                {
                    if (currentControllerName.Equals(controller, StringComparison.CurrentCultureIgnoreCase))
                    {
                        return "active";
                    }
                }
            }
            return "";
        }

        /// <summary>
        /// Creates the error message to be displayed to user
        /// </summary>
        /// <param name="messageText"></param>
        /// <param name="boxIcon"></param>
        /// <returns></returns>
        public static String myMessageBox(String messageText, String boxIcon)
        {
            String sTxt = String.Empty;


            switch (messageText)
            {
                case "Account Locked":
                    sTxt = "Account Locked. Contact User Administrator";
                    break;

                case "Account Inactive":
                    sTxt = "Account Inactive. Contact User Administrator";
                    break;

                case "Account Signed":
                    sTxt = "You are already signed in. Contact User Administrator";
                    break;

                case "Invalid Credentials":
                    sTxt = "Invalid login credentials. Please try again.";
                    break;

                case "Invalid Token":
                    sTxt = "Invalid token verification";
                    break;

                case "Temporary Lock":
                    ParamService paramManager = new ParamService();
                    var PLockTime = paramManager.GetGlobalParamsByName("IdleTimeToLock", GetCurrentConnStr());

                    sTxt = "Your account has been temporarily locked out due to multiple failed login attempts. \n Please contact User Administrator or wait " + PLockTime.PValue.ToString() + " Minutes";
                    break;

                case "2 More Attempts":
                    sTxt = "You have 2 more attempts";
                    break;

                case "1 Last Attempt":
                    sTxt = "You have 1 more attempt";
                    break;

                case "Unknown Attempts Left":
                    sTxt = "Could not check number of attemps left.";
                    break;

                case "Token verification failed":
                    sTxt = "Token verification failed";
                    break;

                case "Unauthorized External":
                    sTxt = "You don't have external access to the system.";
                    break;

                case "Password Expired":
                    sTxt = "Password Expired";
                    break;

                default:
                    sTxt = messageText;
                    break;
            }

            if (boxIcon == "UserToken")
                return sTxt;

            return string.Format("<i class=\"fa fa-" + boxIcon + "\"></i> " + sTxt);
        }

        public static string GetCurrentWebsiteRoot()
        {
            return HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
        }

        public static string tst()
        {
            if (HttpContext.Current == null)
                return String.Empty;

            return HttpContext.Current.Request.Url.DnsSafeHost;
        }

        //#region Private Methods
        ///// <summary>
        ///// Access the authentication ticket
        ///// </summary>
        ///// <returns></returns>
        //private static FormsAuthenticationTicket DecryptAuthTicket()
        //{
        //    var authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
        //    return (authCookie != null) ? FormsAuthentication.Decrypt(authCookie.Value) : null;
        //}
        //#endregion

        /// <summary>
        /// Add default data to return object
        /// </summary>
        /// <param name="baseDTO"></param>
        /// <param name="securityObj"></param>
        public static void DefaultData(BaseDTO baseDTO, SecurityTokenExtended securityObj)
        {
            baseDTO.oneTimeToken = securityObj.securityToken.OneTimeToken;
            //baseDTO.errorMessage += securityObj.securityToken.DebugMessage;
        }

        #region private
        /// <summary>
        /// Get host url from packet header
        /// </summary>
        /// <returns></returns>
        private static string GetHostUrl(HttpRequestHeaders headers)
        {
            if (headers.Contains(Headers.HOST_URL))
                return headers.GetValues(Headers.HOST_URL).FirstOrDefault();

            return string.Empty;
        }

        /// <summary>
        /// Access the authentication ticket
        /// </summary>
        /// <returns></returns>
        private static FormsAuthenticationTicket DecryptAuthTicket()
        {
            var authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
            return (authCookie != null) ? FormsAuthentication.Decrypt(authCookie.Value) : null;
        }

        /// <summary>
        /// Convert to unix TimeStamp
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static int ToTimeStamp(DateTime dt)
        {
            Int32 unixTimestamp = (Int32)(dt.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            return unixTimestamp;
        }

        #endregion
    }
}