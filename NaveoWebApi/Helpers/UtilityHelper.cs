﻿using NaveoOneLib.Models;
using NaveoService.Services;
using NaveoWebApi.Constants;
using NaveoWebApi.Controllers.V2;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;

namespace NaveoWebApi.Helpers
{
    public static class UtilityHelper
    {
        #region Constants
        public const char SEMICOLON = ';';
        public const char COMMA = ',';
        #endregion
        /// <summary>
        /// A static helper class that includes parameter checking routines.
        /// </summary>
        public static class Guard
        {
            /// <summary>
            /// Throws <see cref="T:System.ArgumentNullException" /> if the given argument is null.
            /// </summary>
            /// <exception cref="T:System.ArgumentNullException"> if tested value if null.</exception>
            /// <param name="argumentValue">Argument value to test.</param>
            /// <param name="argumentName">Name of the argument being tested.</param>
            public static void ThrowIfNull(object argumentValue, string argumentName)
            {
                if (argumentValue == null)
                {
                    throw new ArgumentNullException(argumentName);
                }
            }

            /// <summary>
            /// Throws an exception if the tested string argument is null or the empty string.
            /// </summary>
            /// <exception cref="T:System.ArgumentNullException">Thrown if string value is null.</exception>
            /// <exception cref="T:System.ArgumentException">Thrown if the string is empty</exception>
            /// <param name="argumentValue">Argument value to check.</param>
            /// <param name="argumentName">Name of argument being checked.</param>
            public static void ThrowIfNullOrEmpty(string argumentValue, string argumentName)
            {
                if (argumentValue == null)
                {
                    throw new ArgumentNullException(argumentName);
                }
                if (argumentValue.Length == 0)
                {
                    throw new ArgumentException("Argument must not be empty", argumentName);
                }
            }
        }

        /// <summary>
        /// A static helper class for file operations 
        /// </summary>
        public static class File
        {
            /// <summary>
            /// Returns true if ... is valid.
            /// </summary>
            /// <param name="file">The file.</param>
            /// <returns>
            ///   <c>true</c> if the specified file is valid; otherwise, <c>false</c>.
            /// </returns>
            public static bool IsValid(HttpPostedFile file)
            {
                if ((file != null) && (file.ContentLength > 0))
                {
                    // Retrieve file name in input
                    string fileName = Path.GetFileName(file.FileName);
                    string extension = Path.GetExtension(fileName);
                    var validExtensions = new List<string>() { ".xlsx", ".xsl", ".csv" };

                    return validExtensions.Contains(extension);
                }
                return false;
            }

            /// <summary>
            /// Saves file on server
            /// </summary>
            /// <param name="file"></param>
            /// <param name="filepath"></param>
            public static void Save(HttpPostedFile file, string filepath)
            {
                // Get the complete file path
                filepath = Path.Combine(HttpContext.Current.Server.MapPath("~/UploadedFiles"), file.FileName);
                // Save the uploaded file to "UploadedFiles" folder
                file.SaveAs(filepath);
            }

            /// <summary>
            /// Deletes the file from server
            /// </summary>
            /// <param name="filePath"></param>
            public static void Delete(string filePath)
            {
                if (System.IO.File.Exists(filePath))
                {
                    try
                    {
                        File.Delete(filePath);
                    }
                    catch (IOException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        /// <summary>
        /// A static helper class for filling model template rows
        /// </summary>
        //public static class Document
        //{
        /// <summary>
        /// Fills the user template (used to validate type for each field)
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        //#region Fill Template Row 

        //#region User

        /// <summary>
        /// Fill User Template row from excel file
        /// </summary>
        /// <param name="csvFile"></param>
        /// <returns></returns>
        //    public async static Task<List<UserTemplateRow>> FillUserExcelTemplateRowAsync(HttpPostedFile excelFile)
        //    {
        //        return await Task.Run(() =>
        //        {
        //            if (excelFile != null)
        //            {
        //                using (ExcelPackage excelPackage = new ExcelPackage(excelFile.InputStream))
        //                {
        //                    List<UserTemplateRow> userTemplateRows = new List<UserTemplateRow>();
        //                    ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.First();

        //                    var maxRows = worksheet.Dimension.End.Row;
        //                    for (var row = Headers.DEFAULT_START_ROW; row <= maxRows; row++)
        //                    {
        //                        #region Fill User Template rows

        //                        #region Excel dropdowns values handling
        //                        string mobileAccess = worksheet.Cells[row, (int)UsersXlsxColumns.MobileAccess].Value?.ToString();
        //                        string externalAccess = worksheet.Cells[row, (int)UsersXlsxColumns.ExternalAccess].Value?.ToString();
        //                        string twoStepVerif = worksheet.Cells[row, (int)UsersXlsxColumns.TwoStepVerif].Value?.ToString();
        //                        string twoStepAttempts = worksheet.Cells[row, (int)UsersXlsxColumns.TwoStepAttempts].Value?.ToString();
        //                        #endregion

        //                        UserTemplateRow userTemplateRow = new UserTemplateRow
        //                        {
        //                            Username = worksheet.Cells[row, (int)UsersXlsxColumns.Username].Value?.ToString(),
        //                            Names = worksheet.Cells[row, (int)UsersXlsxColumns.Names].Value?.ToString(),
        //                            LastName = worksheet.Cells[row, (int)UsersXlsxColumns.LastName].Value?.ToString(),
        //                            Email = worksheet.Cells[row, (int)UsersXlsxColumns.Email].Value?.ToString(),
        //                            Tel = worksheet.Cells[row, (int)UsersXlsxColumns.Tel].Value?.ToString(),
        //                            MobileNo = worksheet.Cells[row, (int)UsersXlsxColumns.MobileNo].Value?.ToString(),
        //                            Status = worksheet.Cells[row, (int)UsersXlsxColumns.Status].Value?.ToString(),
        //                            Password = worksheet.Cells[row, (int)UsersXlsxColumns.Password].Value?.ToString(),
        //                            AccessList = worksheet.Cells[row, (int)UsersXlsxColumns.AccessList].Value?.ToString(),
        //                            StoreUnit = worksheet.Cells[row, (int)UsersXlsxColumns.StoreUnit].Value?.ToString(),
        //                            UType_cbo = worksheet.Cells[row, (int)UsersXlsxColumns.UType_cbo].Value?.ToString(),
        //                            MaxDayMail = int.Parse(worksheet.Cells[row, (int)UsersXlsxColumns.MaxDayMail].Value?.ToString()),
        //                            //UserTypeID = int.Parse(worksheet.Cells[row, (int)UsersXlsxColumns.UserTypeID].Value?.ToString()),
        //                            UserTypeDescription = worksheet.Cells[row, (int)UsersXlsxColumns.UserTypeDescription].Value?.ToString(),
        //                            ZoneID = int.Parse(worksheet.Cells[row, (int)UsersXlsxColumns.ZoneID].Value?.ToString()),
        //                            //PlanningLookUpValue = int.Parse(worksheet.Cells[row, (int)UsersXlsxColumns.PlanningLookUpValue].Value?.ToString()),
        //                            MobileAccess = (string.IsNullOrEmpty(mobileAccess) || mobileAccess == "No") ? 0 : 1,
        //                            ExternalAccess = (string.IsNullOrEmpty(externalAccess) || externalAccess == "No") ? 0 : 1,
        //                            TwoStepVerif = (string.IsNullOrEmpty(twoStepVerif) || twoStepVerif == "No") ? 0 : 1,
        //                            TwoStepAttempts = (string.IsNullOrEmpty(twoStepAttempts) || twoStepAttempts == "No") ? 0 : 1
        //                        };
        //                        #endregion

        //                        userTemplateRows.Add(userTemplateRow);
        //                    }
        //                    return userTemplateRows;
        //                }
        //            }
        //            else
        //                return new List<UserTemplateRow>();
        //        });
        //    }

        //    /// <summary>
        //    /// Fill User Template row from csv file
        //    /// </summary>
        //    /// <param name="csvFile"></param>
        //    /// <returns></returns>
        //    public async static Task<List<UserTemplateRow>> FillUserCsvTemplateRowAsync(HttpPostedFile csvFile)
        //    {
        //        return await Task.Run(() =>
        //        {
        //            if (csvFile != null)
        //            {
        //                string[] lines = System.IO.File.ReadAllLines(csvFile.FileName);
        //                string errorMessage = string.Empty;
        //                List<UserTemplateRow> UserTemplateRows = new List<UserTemplateRow>();
        //                foreach (var line in lines)
        //                {
        //                    if (line.Contains(SEMICOLON))
        //                        line.Replace(SEMICOLON, COMMA);

        //                    string[] values = line.Split(COMMA);
        //                    UserTemplateRow userRow = new UserTemplateRow
        //                    {
        //                        #region UserTemplateRow
        //                        Username = values[0],
        //                        LastName = values[1],
        //                        Email = values[2],
        //                        Tel = values[3],
        //                        MobileNo = values[4],
        //                        Status = values[5],
        //                        Password = values[6],
        //                        AccessList = values[7],
        //                        StoreUnit = values[8],
        //                        UType_cbo = values[9],
        //                        MaxDayMail = int.Parse(values[10]),
        //                        //UserTypeID = int.Parse(worksheet.Cells[row, (int)UsersXlsxColumns.UserTypeID].Value?.ToString()),
        //                        UserTypeDescription = values[11],
        //                        ZoneID = int.Parse(values[12]),
        //                        //PlanningLookUpValue = int.Parse(worksheet.Cells[row, (int)UsersXlsxColumns.PlanningLookUpValue].Value?.ToString()),
        //                        MobileAccess = int.Parse(values[13]),
        //                        ExternalAccess = int.Parse(values[14]),
        //                        TwoStepVerif = int.Parse(values[15]),
        //                        TwoStepAttempts = int.Parse(values[16])
        //                        #endregion
        //                    };
        //                    UserTemplateRows.Add(userRow);
        //                }
        //                return UserTemplateRows;
        //            }
        //            else
        //                return new List<UserTemplateRow>();
        //        });
        //    }

        //    #endregion

        //    #region Zone
        //    #endregion

        // #endregion
    }
}

