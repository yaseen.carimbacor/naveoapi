﻿using NaveoWebApi.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Hosting;
using NaveoService.Models.DTO;
using NaveoService.Helpers;

namespace NaveoWebApi.Helpers
{
    public class SecurityHelper
    {
        public static readonly Guid DEFAULT_GUID = new Guid("00000000-0000-0000-0000-000000000000");
        
        /// <summary>
        /// Protected Constructor
        /// </summary>
        protected SecurityHelper()
        {

        }

        /// <summary>
        /// Raise generique exception message
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public static string ExceptionMessage(Exception e)
        {
            return "Message: " + e.Message;
        }

        /// <summary>
        /// UnAuthorized API return call with additional message
        /// </summary>
        /// <param name="securityToken"></param>
        /// <returns></returns>
        public static BaseDTO Unauthorized(string message)
        {
            return new BaseDTO { errorMessage = message };
        }

        /// <summary>
        /// Get all headers from packet
        /// </summary>
        /// <returns></returns>
        public static List<string> GetHeaders(HttpRequestHeaders headers)
        {
            var headerValue = string.Empty;
            List<string> headerValues = new List<string>();
            Headers.LIST.ForEach(h =>
            {
                if (headers.Contains(h))
                {
                    headerValue = headers.GetValues(h).FirstOrDefault();
                    headerValues.Add(headerValue);
                }
            });

            return headerValues;
        }

        #region Obsolete. Done in api memory
        /// <summary>
        /// Verify authentication token 
        /// </summary>
        /// <param name="headers"></param>
        /// <returns></returns>
        //public static SecurityToken VerifyToken(Guid? authToken, HttpRequestHeaders headers, string hostURL, string sConnStr)
        //{
        //    SecurityToken securityToken =  SecurityService.VerifyToken(authToken, sConnStr, HttpContext.Current.Request.UserHostAddress);

        //    if (securityToken.IsAuthorized)
        //    {
        //        securityToken.UserToken = GetUserToken(headers);
        //        securityToken.sConnStr = sConnStr;

        //        securityToken.Page = GetPageRequested(headers);
        //        securityToken.LimitPerPage = GetLimit(headers);
        //    }
        //    return securityToken;
        //}
        #endregion

        /// <summary>
        /// Verifies if selected db is valid
        /// </summary>
        /// <param name="headers"></param>
        /// <returns></returns>
        public static bool IsDatabaseValid(string sConnStr, HttpRequestHeaders headers)
        {
            String CurrentPath = HostingEnvironment.ApplicationPhysicalPath;
            //check if selected db is allowed 
            if (FileHelper.DoesLineExist(CurrentPath + "\\Registers\\databases.dat", sConnStr))
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// Get one time password from packet header
        /// </summary>
        /// <returns></returns>
        public static Guid GetAuthToken(HttpRequestHeaders headers)
        {
            if (headers.Contains(Headers.ONE_TIME_TOKEN))
                return new Guid(headers.GetValues(Headers.ONE_TIME_TOKEN).First());

            return DEFAULT_GUID;
        }

        /// <summary>
        /// Determines whether debug mode is on or not
        /// </summary>
        /// <param name="headers"></param>
        /// <returns></returns>
        private static bool IsDebugMode(HttpRequestHeaders headers)
        {
            if (headers.Contains(Headers.IS_DEBUG_MODE))
                return true;

            return false;
        }

        /// <summary>
        /// Get USER_TOKEN from packet header
        /// </summary>
        /// <returns></returns>
        public static Guid GetUserToken(HttpRequestHeaders headers)
        {
            if (headers.Contains(Headers.USER_TOKEN))
                return new Guid(headers.GetValues(Headers.USER_TOKEN).First());

            return DEFAULT_GUID;
        }

        /// <summary>
        /// Get PAGE from packet header
        /// </summary>
        /// <returns></returns>
        public static int? GetPageRequested(HttpRequestHeaders headers)
        {
            if (headers.Contains(Headers.PAGE))
            {
                int i = 0;
                if (int.TryParse(headers.GetValues(Headers.PAGE).First(), out i))
                    return i;
            }

            return null;
        }

        /// <summary>
        /// Get LIMIT_PER_PAGE from packet header
        /// </summary>
        /// <returns></returns>
        public static int? GetLimit(HttpRequestHeaders headers)
        {
            if (headers.Contains(Headers.ROWS_PER_PAGE))
            {
                int i = 0;
                if (int.TryParse(headers.GetValues(Headers.ROWS_PER_PAGE).First(), out i))
                    return i;
            }

            return null;
        }

        /// <summary>
        /// Get USER_TOKEN from packet header
        /// </summary>
        /// <returns></returns>
        public static String GetHostURL(HttpRequestHeaders headers)
        {
            if (headers.Contains(Headers.HOST_URL))
                return (headers.GetValues(Headers.HOST_URL).First());

            return String.Empty;
        }

        /// <summary>
        /// Verifies if token has expired
        /// </summary>
        /// <param name="cachedToken"></param>
        /// <returns></returns>
        public static bool IsTokenExpired(DateTime expiryDate)
        {
            if (DateTime.Now > expiryDate)
                return true; // expired
            return false;
        }

        /// <summary>
        /// Verifies if user has timed out
        /// </summary>
        /// <param name="lastAccessedAt"></param>
        /// <returns></returns>
        public static bool HasTimedOut(DateTime lastAccessedAt)
        {
            TimeSpan diff = DateTime.Now - lastAccessedAt;
            if ((diff.TotalMinutes) > ConfigurationKeys.TimeOut)
                return true; // timed out
            return false;
        }
    }
}