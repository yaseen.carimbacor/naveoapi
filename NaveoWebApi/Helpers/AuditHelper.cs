﻿using NaveoOneLib.Models.Audits;
using NaveoOneLib.Models.Common;
using 
        //private static readonly ILogger logger;
NaveoService.Services;
using Serilog;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoWebApi.Helpers
{
    public class Telemetry
    {
        /// <summary>
        /// Protected Constructor
        /// </summary>
        protected Telemetry()
        {

        }
        public static BaseModel Trace(Guid UserToken, string controller, string Action, string rawRequest, int sID, string connStr, bool bInsert)
        {
            AuditData sAudit = new AuditData();
            sAudit.UserToken = UserToken;
            sAudit.Controller = controller;
            sAudit.Action = Action;
            sAudit.RawRequest = rawRequest;
            sAudit.sID = sID;
            //sAudit.ReferenceCode = referenceCode;
            sAudit.IsAccessGranted = 1;

            return new AuditService().Trace(sAudit, connStr, bInsert);
        }
        public static BaseModel Trace(AuditData sAudit, string connStr, Boolean bInsert = true)
        {
            //logger.Information(msg);

            // call auditService here to do insertion in table
            return new AuditService().Trace(sAudit, connStr, bInsert);
        }

        public static void Trace(string email, string sAction, bool isAccessGranted, string sConnStr)
        {
            //logger.Information(msg);

            // call auditService here to do insertion in table
            new AuditService().Trace(email, sAction, isAccessGranted, sConnStr);
        }
    }
}