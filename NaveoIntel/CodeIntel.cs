﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace NaveoIntel
{
    public static class CodeIntel
    {
        public static String RezaCodes(DataTable dt, String strObject, DataTable dts, String txtTableName)
        {
            String strResult = String.Empty;
            strResult += "\r\n\r\n\r\n";
            strResult += strService(dt, strObject, txtTableName);
            strResult += "<SQL Scripts>\r\n";
            foreach (DataRow dr in dts.Rows)
                strResult += dr["strQry"].ToString().Replace("\n", "\r\n");
            //strResult = strResult.Substring(0, strResult.Length - 1) + "WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
            strResult += "\r\n<SQL Scripts>\r\n";


            strResult += "\r\n\r\n\r\n";
            strResult += strFormDesigner(dt, strObject);

            strResult += "\r\n\r\n\r\n";
            strResult += strForm(dt, strObject);

            return strResult;
        }
        public static String strService(DataTable dt, String strObject, String txtTableName)
        {
            String strResult = @"using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NaveoOneLib.Common;
using NaveoOneLib.DBCon;
using NaveoOneLib.Models;
using System.Data;
using System.Data.Common;

namespace NaveoOneLib.Services
{
     class " + strObject + "Service " +
    "{   ";
            strResult += "\r\n";
            strResult += "Errors er = new Errors();";

            strResult += "\r\n";
            strResult += "public DataTable Get" + strObject + "(String sUserToken, String sConnStr)\r\n";
            strResult += "{";
            strResult += @"            List<Matrix> lm = new MatrixService().GetMatrixByUserToken(sUserToken, sConnStr);
            List<int> iParentIDs = new List<int>();
            foreach (Matrix m in lm)
                iParentIDs.Add(m.GMID);";
            strResult += "\r\n";
            strResult += "String sqlString = @\"SELECT ";
            foreach (DataColumn dc in dt.Columns)
                strResult += dc.ColumnName + ", \r\n";
            strResult = strResult.Substring(0, strResult.Length - 4) + " from " + txtTableName + " order by TingPow\";";
            strResult += "\r\n";
            strResult += "return new Connection(sConnStr).GetDataDT(sqlString, sConnStr);";
            strResult += "\r\n";
            strResult += "}";
            strResult += "\r\n";

            String sClass = strObject + " Get" + strObject + "(DataSet ds)\r\n";
            sClass += @"{ DataRow dr = ds.Tables[""dt"+strObject+@"""].Rows[0]; ";
            sClass += "\r\n";
            sClass += strNewObj(dt, strObject, "dt");

            sClass += @"
                        List<Matrix> lMatrix = new List<Matrix>();
                        MatrixService ms = new MatrixService();
                        foreach (DataRow r in ds.Tables[""dtMatrix""].Rows)
                            lMatrix.Add(ms.AssignMatrix(r));
                        ret" + strObject + ".lMatrix = lMatrix;  ";

            sClass += "\r\n";
            sClass += "return ret" + strObject + ";";
            sClass += "\r\n";
            sClass += "}";
            sClass += "\r\n";
            sClass = sClass.Replace("dt.Rows[0]", "dr");
            strResult += sClass;

            strResult += "public " + strObject + " Get" + strObject + "ById(String iID, String sConnStr)\r\n";
            strResult += "{";
            strResult += @" Connection c = new Connection(sConnStr);
                            DataTable dtSql = c.dtSql();
                            DataRow drSql = dtSql.NewRow();";
            strResult += "String sql = @\"SELECT ";
            foreach (DataColumn dc in dt.Columns)
                strResult += dc.ColumnName + ", \r\n";
            strResult = strResult.Substring(0, strResult.Length - 4) + " from " + txtTableName;
            strResult += @" WHERE TingPow = ? "";";
            strResult += "\r\n";
            strResult += @"drSql = dtSql.NewRow();
                            drSql[""sSQL""] = sql;
                            drSql[""sTableName""] = ""dt" + strObject + @""";
                            dtSql.Rows.Add(drSql);

            //Matrix
            sql = new MatrixService().sGetMatrixId(Convert.ToInt32(iID), ""GFI_SYS_GroupMatrix"+strObject+@""");
            drSql = dtSql.NewRow();
            drSql[""sSQL""] = sql;
            drSql[""sTableName""] = ""dtMatrix"";
            dtSql.Rows.Add(drSql);

            DataSet ds = c.GetDataDS(dtSql, sConnStr);
            if (ds.Tables[""dt" + strObject + @"""].Rows.Count == 0)
                return null;
            else
                return  Get" + strObject + "(ds);";
            strResult += "\r\n}";

            strResult += "\r\n";
            strResult += strDT(dt, strObject, txtTableName);
            strResult += "public Boolean Save" + strObject + " (" + strObject + " u" + strObject + ", Boolean bInsert, String sConnStr)";
            strResult += "\r\n";
            strResult += "{";
            strResult += strSaveDT(dt, strObject, txtTableName);
            strResult += @"            
                            Connection c = new Connection(sConnStr);
                            DataTable dtCtrl = c.CTRLTBL();
                            DataRow drCtrl = dtCtrl.NewRow();
                            drCtrl[""SeqNo""] = 1;
                            drCtrl[""TblName""] = dt.TableName;
                            drCtrl[""CmdType""] = ""TABLE"";
                            drCtrl[""KeyFieldName""] = ""TingPow"";
                            drCtrl[""KeyIsIdentity""] = ""TRUE"";
                            if (bInsert)
                                drCtrl[""NextIdAction""] = ""GET"";
                            else
                                drCtrl[""NextIdValue""] = u" + strObject + @".TingPow;
                            dtCtrl.Rows.Add(drCtrl);
                            DataSet dsProcess = new DataSet();
                            dsProcess.Merge(dt);

            //Matrix
            DataTable dtMatrix = new myConverter().ListToDataTable<Matrix>(u" + strObject + @".lMatrix);
            dtMatrix.TableName = ""GFI_SYS_GroupMatrix" + strObject + @""";
            drCtrl = dtCtrl.NewRow();
            drCtrl[""SeqNo""] = 2;
            drCtrl[""TblName""] = dtMatrix.TableName;
            drCtrl[""CmdType""] = ""TABLE"";
            drCtrl[""KeyFieldName""] = ""MID"";
            drCtrl[""KeyIsIdentity""] = ""TRUE"";
            drCtrl[""NextIdAction""] = ""SET"";
            drCtrl[""NextIdFieldName""] = ""iID"";
            drCtrl[""ParentTblName""] = dt.TableName;
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtMatrix);


            foreach (DataTable dti in dsProcess.Tables)
            {
                if (!dti.Columns.Contains(""OPRType""))
                    dti.Columns.Add(""OPRType"", typeof(DataRowState));

                foreach (DataRow dri in dti.Rows)
                    if (dri[""OPRType""].ToString().Trim().Length == 0)
                    {
                        if (bInsert)
                            dri[""OPRType""] = DataRowState.Added;
                        else
                            dri[""OPRType""] = DataRowState.Modified;
                    }
            }

            dsProcess.Merge(dtCtrl);
            dtCtrl = c.ProcessData(dsProcess, sConnStr).Tables[""CTRLTBL""];

            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select(""UpdateStatus IS NULL or UpdateStatus <> 'TRUE'"");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
";

            strResult += "public Boolean Delete" + strObject + " (" + strObject + " u" + strObject + ", String sConnStr)";
            strResult += "\r\n";
            strResult += @"        {
            Connection c = new Connection(sConnStr);
            DataTable dtCtrl = c.CTRLTBL();
            DataRow drCtrl = dtCtrl.NewRow();

            drCtrl = dtCtrl.NewRow();
            drCtrl[""SeqNo""] = 1;
            drCtrl[""TblName""] = ""None"";
            drCtrl[""CmdType""] = ""STOREDPROC"";
            drCtrl[""TimeOut""] = 1000;
            String sql = ""delete from " + txtTableName + @" where TingPow = "" + u" + strObject + @".TingPow.ToString();
            drCtrl[""Command""] = sql;
            dtCtrl.Rows.Add(drCtrl);
            DataSet dsProcess = new DataSet();

            DataTable dtAudit = new AuditService().LogTran(3, """ + strObject + @""", u" + strObject + @".TingPow.ToString(), Globals.uLogin.UID, u" + strObject + @".TingPow);
            drCtrl = dtCtrl.NewRow();
            drCtrl[""SeqNo""] = 100;
            drCtrl[""TblName""] = dtAudit.TableName;
            drCtrl[""CmdType""] = ""TABLE"";
            drCtrl[""KeyFieldName""] = ""AuditID"";
            drCtrl[""KeyIsIdentity""] = ""TRUE"";
            drCtrl[""NextIdAction""] = ""SET"";
            drCtrl[""NextIdFieldName""] = ""CallerFunction"";
            drCtrl[""ParentTblName""] = """ + txtTableName + @""";
            dtCtrl.Rows.Add(drCtrl);
            dsProcess.Merge(dtAudit);

            dsProcess.Merge(dtCtrl);
            DataSet dsResult = c.ProcessData(dsProcess, sConnStr);

            dtCtrl = dsResult.Tables[""CTRLTBL""];
            Boolean bResult = false;
            if (dtCtrl != null)
            {
                DataRow[] dRow = dtCtrl.Select(""UpdateStatus IS NULL or UpdateStatus <> 'TRUE'"");

                if (dRow.Length > 0)
                    bResult = false;
                else
                    bResult = true;
            }
            else
                bResult = false;

            return bResult;
        }
    }
}
";
            return strResult;
        }
        public static String strModel(DataTable dt, String strObject, String txtTableName)
        {
            String strResult = @"using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using NaveoOneLib.Common;
using NaveoOneLib.Services;
using System.Data.Common;                         

namespace NaveoOneLib.Models
{
    public class " + strObject + " " + "{   ";
            strResult += "\r\n";
            foreach (DataColumn dc in dt.Columns)
            {
                switch (dc.DataType.ToString())
                {
                    case "System.String":
                        strResult += "public String " + dc.ColumnName + "{ get; set; }\r\n";
                        break;
                    case "System.Int32":
                        strResult += "public int " + dc.ColumnName + " { get; set; }\r\n";
                        break;
                    case "System.Decimal":
                        strResult += "public float " + dc.ColumnName + " { get; set; }\r\n";
                        break;
                    case "System.Double":
                        strResult += "public float " + dc.ColumnName + " { get; set; }\r\n";
                        break;
                    case "System.DateTime":
                        strResult += "public DateTime " + dc.ColumnName + " { get; set; }\r\n";
                        break;
                    case "System.Byte[]":
                        strResult += "public Byte[] " + dc.ColumnName + " { get; set; }\r\n";
                        break;
                    default:
                        strResult += dc.ColumnName + " Please define " + dc.DataType.ToString() + "\r\n";
                        break;
                }
            }

            strResult += "\r\n";
            strResult += "        public DataRowState OPRType { get; set; }";
            strResult += "\r\n";
            strResult += "        public List<Matrix> lMatrix { get; set; }";
            strResult += "\r\n";
            strResult += "public DataTable Get" + strObject + "(String sUserToken, String sConnStr)\r\n{\r\n\t return new " + strObject + "Service().Get" + strObject + "(sUserToken, sConnStr);\r\n}\r\n";
            strResult += "public " + strObject + " Get" + strObject + "ById(String ID, String sConnStr)\r\n{\r\n\t return new " + strObject + "Service().Get" + strObject + "ById(ID, sConnStr);\r\n}\r\n";

            strResult += "\r\n";
            strResult += "public Boolean Save(" + strObject + " S" + strObject + ", Boolean bInsert, String sConnStr)\r\n{\r\n\t return new " + strObject + "Service().Save" + strObject + "(S" + strObject + ", bInsert, sConnStr);\r\n}\r\n";
            strResult += "public Boolean Update(" + strObject + " S" + strObject + ", String sConnStr)\r\n{\r\n\t return new " + strObject + "Service().Save" + strObject + "(S" + strObject + ", false, sConnStr);\r\n}\r\n";
            strResult += "public Boolean Delete(" + strObject + " S" + strObject + ", String sConnStr)\r\n{\r\n\t return new " + strObject + "Service().Delete" + strObject + "(S" + strObject + ", sConnStr);\r\n}";

            strResult += "\r\n";
            strResult += "public " + strObject + " GetClone() \r\n{\r\n return (" + strObject + ")this.MemberwiseClone();\r\n}";
            
            strResult += "\r\n    }\r\n}";

            return strResult;
        }

        private static String strNewObj(DataTable dt, String strObject, String strDataTable)
        {
            String strResult = String.Empty;
            String strRet = " ret" + strObject;
            strResult += strObject + strRet + "= new " + strObject + "();\r\n";
            foreach (DataColumn dc in dt.Columns)
            {
                switch (dc.DataType.ToString())
                {
                    case "System.String":
                        strResult += strRet + "." + dc.ColumnName + " = " + strDataTable + ".Rows[0][\"" + dc.ColumnName + "\"].ToString();";
                        break;
                    case "System.Int32":
                        strResult += strRet + "." + dc.ColumnName + " = String.IsNullOrEmpty(" + strDataTable + ".Rows[0][\"" + dc.ColumnName + "\"].ToString()) ? (int?)null : (int)dr[\"" + dc.ColumnName + "\"];";
                        break;
                    case "System.Decimal":
                        strResult += strRet + "." + dc.ColumnName + " = String.IsNullOrEmpty(" + strDataTable + ".Rows[0][\"" + dc.ColumnName + "\"].ToString()) ? (float?)null : (float)dr[\"" + dc.ColumnName + "\"];";
                        break;
                    case "System.Double":
                        strResult += strRet + "." + dc.ColumnName + " = String.IsNullOrEmpty(" + strDataTable + ".Rows[0][\"" + dc.ColumnName + "\"].ToString()) ? (float?)null : (float)dr[\"" + dc.ColumnName + "\"];";
                        break;
                    case "System.DateTime":
                        strResult += strRet + "." + dc.ColumnName + " = String.IsNullOrEmpty(" + strDataTable + ".Rows[0][\"" + dc.ColumnName + "\"].ToString()) ? (DateTime?)null : (DateTime)dr[\"" + dc.ColumnName + "\"];";
                        break;
                    case "System.Byte[]":
                        strResult += strRet + "." + dc.ColumnName + " = String.IsNullOrEmpty(" + strDataTable + ".Rows[0][\"" + dc.ColumnName + "\"].ToString()) ? (Byte[]?)null : (Byte[])dr[\"" + dc.ColumnName + "\"];";
                        break;
                    default:
                        strResult += dc.ColumnName + " Please define " + dc.DataType.ToString() + "\r\n";
                        break;
                }
                strResult += "\r\n";
            }
            return strResult;
        }
        private static String strSaveDT(DataTable dt, String strObject, String txtTableName)
        {
            String strResult = String.Empty;
            strResult += "DataTable dt = " + strObject + "DT();\r\n";
            strResult += "\r\nDataRow dr = dt.NewRow();\r\n";

            foreach (DataColumn dc in dt.Columns)
            {
                switch (dc.DataType.ToString())
                {
                    case "System.String":
                        strResult += "dr[\"" + dc.ColumnName + "\"] =  u" + strObject + "." + dc.ColumnName + ";\r\n";
                        break;
                    case "System.Int32":
                        strResult += "dr[\"" + dc.ColumnName + "\"] =  u" + strObject + "." + dc.ColumnName + "!= null ? " + "u" + strObject + "." + dc.ColumnName + " : (object)DBNull.Value;\r\n";
                        break;
                    case "System.Decimal":
                        strResult += "dr[\"" + dc.ColumnName + "\"] =  u" + strObject + "." + dc.ColumnName + "!= null ? " + "u" + strObject + "." + dc.ColumnName + " : (object)DBNull.Value;\r\n";
                        break;
                    case "System.Double":
                        strResult += "dr[\"" + dc.ColumnName + "\"] =  u" + strObject + "." + dc.ColumnName + "!= null ? " + "u" + strObject + "." + dc.ColumnName + " : (object)DBNull.Value;\r\n";
                        break;
                    case "System.DateTime":
                        strResult += "dr[\"" + dc.ColumnName + "\"] =  u" + strObject + "." + dc.ColumnName + "!= null ? " + "u" + strObject + "." + dc.ColumnName + " : (object)DBNull.Value;\r\n";
                        break;
                    case "System.Byte[]":
                        strResult += "dr[\"" + dc.ColumnName + "\"] =  u" + strObject + "." + dc.ColumnName + "!= null ? " + "u" + strObject + "." + dc.ColumnName + " : (object)DBNull.Value;\r\n";
                        break;
                    default:
                        strResult += dc.ColumnName + " Please define " + dc.DataType.ToString() + "\r\n";
                        break;
                }
            }
            strResult += "dt.Rows.Add(dr);\r\n";
            strResult += "\r\n";

            return strResult;
        }
        private static String strDT(DataTable dt, String strObject, String txtTableName)
        {
            String strResult = String.Empty;
            strResult = " DataTable " + strObject + "DT()";
            strResult += "\r\n{";
            strResult += "DataTable dt = new DataTable();\r\n";
            strResult += "dt.TableName = \"" + txtTableName + "\";\r\n";

            foreach (DataColumn dc in dt.Columns)
            {
                switch (dc.DataType.ToString())
                {
                    case "System.String":
                        strResult += "dt.Columns.Add(\"" + dc.ColumnName + "\" , typeof(String));\r\n";
                        break;
                    case "System.Int32":
                        strResult += "dt.Columns.Add(\"" + dc.ColumnName + "\" , typeof(int));\r\n";
                        break;
                    case "System.Decimal":
                        strResult += "dt.Columns.Add(\"" + dc.ColumnName + "\" , typeof(float));\r\n";
                        break;
                    case "System.Double":
                        strResult += "dt.Columns.Add(\"" + dc.ColumnName + "\" , typeof(float));\r\n";
                        break;
                    case "System.DateTime":
                        strResult += "dt.Columns.Add(\"" + dc.ColumnName + "\" , typeof(DateTime));\r\n";
                        break;
                    case "System.Byte[]":
                        strResult += "dt.Columns.Add(\"" + dc.ColumnName + "\" , typeof(Byte[]));\r\n";
                        break;
                    default:
                        strResult += dc.ColumnName + " Please define " + dc.DataType.ToString() + "\r\n";
                        break;
                }
            }
            strResult+="\r\nreturn dt; \r\n}";
            return strResult;
        }

        private static String strFormDesigner(DataTable dt, String strObject)
        {
            String strResult = "\r\n<Form Designer>    DESIGN SIZE 1130, 638      \r\n";
            strResult += "//*********in Form frmBaseTemplate";
            strResult += "\r\n//*********Add groupBox1 and textbox1 on Designer Screen\r\n";
            strResult += "\r\n//*********Replace textbox1 with code below\r\n";
            strResult += "\r\n//*********Fields CreatedBy, CreatedDate, UpdatedBy, UpdatedDate excluded\r\n";

            foreach (DataColumn dc in dt.Columns)
                switch (dc.ColumnName)
                {
                    case "CreatedBy":
                        break;

                    case "CreatedDate":
                        break;

                    case "UpdatedBy":
                        break;

                    case "UpdatedDate":
                        break;

                    default:
                        if (dc.ColumnName.Contains("_cbo"))
                            strResult += "private Telerik.WinControls.UI.RadDropDownList " + "cbo" + dc.ColumnName + ";\r\n";
                        else if (dc.ColumnName.Contains("_b"))
                            strResult += "private Telerik.WinControls.UI.RadCheckBox " + "chk" + dc.ColumnName + ";\r\n";
                        else if (dc.DataType == typeof(System.DateTime))
                            strResult += "private Telerik.WinControls.UI.RadDateTimePicker " + "dt" + dc.ColumnName + ";\r\n";
                        else
                            strResult += "private Telerik.WinControls.UI.RadTextBox " + "txt" + dc.ColumnName + ";\r\n";
                        strResult += "private Telerik.WinControls.UI.RadLabel " + "lbl" + dc.ColumnName + ";\r\n";
                        break;
                }

            strResult += "\r\n\r\n\r\n\r\n";
            strResult += "// InitializeComponent***************\r\n";
            foreach (DataColumn dc in dt.Columns)
                switch (dc.ColumnName)
                {
                    case "CreatedBy":
                        break;

                    case "CreatedDate":
                        break;

                    case "UpdatedBy":
                        break;

                    case "UpdatedDate":
                        break;

                    default:
                        if (dc.ColumnName.Contains("_cbo"))
                            strResult += "this.cbo" + dc.ColumnName + " = new Telerik.WinControls.UI.RadDropDownList();\r\n";
                        else if (dc.ColumnName.Contains("_b"))
                            strResult += "this.chk" + dc.ColumnName + " = new Telerik.WinControls.UI.RadCheckBox();\r\n";
                        else if (dc.DataType == typeof(System.DateTime))
                            strResult += "this.dt" + dc.ColumnName + " = new Telerik.WinControls.UI.RadDateTimePicker();\r\n";
                        else
                            strResult += "this.txt" + dc.ColumnName + " = new Telerik.WinControls.UI.RadTextBox();\r\n";
                        strResult += "this.lbl" + dc.ColumnName + " = new Telerik.WinControls.UI.RadLabel();\r\n";
                        break;
                }

            strResult += "\r\n\r\n\r\n\r\n";
            strResult += "// this.groupBox1.Controls.Add***************\r\n";
            foreach (DataColumn dc in dt.Columns)
                switch (dc.ColumnName)
                {
                    case "CreatedBy":
                        break;

                    case "CreatedDate":
                        break;

                    case "UpdatedBy":
                        break;

                    case "UpdatedDate":
                        break;

                    default:
                        if (dc.ColumnName.Contains("_cbo"))
                            strResult += "this.groupBox1.Controls.Add(this." + "cbo" + dc.ColumnName + ");\r\n";
                        else if (dc.ColumnName.Contains("_b"))
                            strResult += "this.groupBox1.Controls.Add(this." + "chk" + dc.ColumnName + ");\r\n";
                        else if (dc.DataType == typeof(System.DateTime))
                            strResult += "this.groupBox1.Controls.Add(this." + "dt" + dc.ColumnName + ");\r\n";
                        else
                            strResult += "this.groupBox1.Controls.Add(this." + "txt" + dc.ColumnName + ");\r\n";
                        strResult += "this.groupBox1.Controls.Add(this." + "lbl" + dc.ColumnName + ");\r\n";
                        break;
                }

            strResult += "\r\n\r\n\r\n\r\n";
            strResult += "// After groupBox1***************\r\n";
            int x = 40, y = 40, z = 1;
            foreach (DataColumn dc in dt.Columns)
                switch (dc.ColumnName)
                {
                    case "CreatedBy":
                        break;

                    case "CreatedDate":
                        break;

                    case "UpdatedBy":
                        break;

                    case "UpdatedDate":
                        break;

                    default:
                        strResult += "// \r\n";
                        strResult += "// lbl" + dc.ColumnName + "\r\n"; ;
                        strResult += "// \r\n";
                        strResult += "this.lbl" + dc.ColumnName + ".AutoSize = true;\r\n";
                        strResult += "this.lbl" + dc.ColumnName + ".Location = new System.Drawing.Point(" + x.ToString() + ", " + y.ToString() + ");\r\n";
                        strResult += "this.lbl" + dc.ColumnName + ".Name = \"lbl" + dc.ColumnName + "\";\r\n";
                        strResult += "this.lbl" + dc.ColumnName + ".Size = new System.Drawing.Size(79, 13);\r\n";
                        strResult += "this.lbl" + dc.ColumnName + ".TabIndex = " + z.ToString() + ";\r\n";
                        strResult += "this.lbl" + dc.ColumnName + ".Text = \"" + dc.ColumnName + "\";\r\n";
                        z++;
                        strResult += "// \r\n";
                        if (dc.ColumnName.Contains("_cbo"))
                        {
                            strResult += "// cbo" + dc.ColumnName + "\r\n";
                            strResult += "// \r\n";
                            strResult += "this.cbo" + dc.ColumnName + ".Location = new System.Drawing.Point(" + (100 + x).ToString() + ", " + y.ToString() + ");\r\n";
                            strResult += "this.cbo" + dc.ColumnName + ".Name = \"cbo" + dc.ColumnName + "\";\r\n";
                            strResult += "this.cbo" + dc.ColumnName + ".Size = new System.Drawing.Size(136, 20);\r\n";
                            strResult += "this.cbo" + dc.ColumnName + ".TabIndex = " + z.ToString() + ";\r\n";
                        }
                        else if (dc.ColumnName.Contains("_b"))
                        {
                            strResult += "// chk" + dc.ColumnName + "\r\n";
                            strResult += "// \r\n";
                            strResult += "this.chk" + dc.ColumnName + ".Location = new System.Drawing.Point(" + (100 + x).ToString() + ", " + y.ToString() + ");\r\n";
                            strResult += "this.chk" + dc.ColumnName + ".Name = \"chk" + dc.ColumnName + "\";\r\n";
                            strResult += "this.chk" + dc.ColumnName + ".Size = new System.Drawing.Size(136, 20);\r\n";
                            strResult += "this.chk" + dc.ColumnName + ".TabIndex = " + z.ToString() + ";\r\n";
                        }
                        else if (dc.DataType == typeof(System.DateTime))
                        {
                            strResult += "// dt" + dc.ColumnName + "\r\n";
                            strResult += "// \r\n";
                            strResult += "this.dt" + dc.ColumnName + ".Location = new System.Drawing.Point(" + (100 + x).ToString() + ", " + y.ToString() + ");\r\n";
                            strResult += "this.dt" + dc.ColumnName + ".Name = \"dt" + dc.ColumnName + "\";\r\n";
                            strResult += "this.dt" + dc.ColumnName + ".Size = new System.Drawing.Size(136, 20);\r\n";
                            strResult += "this.dt" + dc.ColumnName + ".TabIndex = " + z.ToString() + ";\r\n";
                        }
                        else
                        {
                            strResult += "// txt" + dc.ColumnName + "\r\n";
                            strResult += "// \r\n";
                            strResult += "this.txt" + dc.ColumnName + ".Location = new System.Drawing.Point(" + (100 + x).ToString() + ", " + y.ToString() + ");\r\n";
                            strResult += "this.txt" + dc.ColumnName + ".Name = \"txt" + dc.ColumnName + "\";\r\n";
                            strResult += "this.txt" + dc.ColumnName + ".Size = new System.Drawing.Size(136, 20);\r\n";
                            strResult += "this.txt" + dc.ColumnName + ".TabIndex = " + z.ToString() + ";\r\n";
                        }

                        y += 25;
                        z++;
                        break;
                }
            strResult += "// Before frm****************\r\n";

            strResult += "\r\n<Form Designer>\r\n";
            return strResult;
        }
        private static String strForm(DataTable dt, String strObject)
        {
            String strResult = "\r\n<Forms>\r\n";
            strResult += @"using NaveoOneLib.Common;
                            using NaveoOneLib.DBCon;
                            using NaveoOneLib.Models;
                            using System.Data;
                            using NaveoOneLib.Services;";
            strResult += "\r\n";
            strResult += "\r\n";
            strResult += "Errors Msg = new Errors();";
            strResult += "\r\n";
            strResult += "private " + strObject + " l" + strObject + ";\r\n";
            strResult += "private String lObjCode { get; set; }" + "\r\n";
            strResult += "DataTable ds" + strObject + " = new DataTable();" + "\r\n\r\n";
            strResult += "public override void LoadGridValues()" + "\r\n";
            strResult += "{" + "\r\n";
            strResult += "if (l" + strObject + " == null)" + "\r\n";
            strResult += "l" + strObject + " = new " + strObject + "();" + "\r\n";
            strResult += "RecordDataTable = l" + strObject + ".Get" + strObject + "();" + "\r\n";
            strResult += "}" + "\r\n";

            strResult += "private void Load" + strObject + "()\r\n";
            strResult += "{" + "\r\n";
            foreach (DataColumn dc in dt.Columns)
                switch (dc.ColumnName)
                {
                    case "CreatedBy":
                        break;

                    case "CreatedDate":
                        break;

                    case "UpdatedBy":
                        break;

                    case "UpdatedDate":
                        break;

                    default:
                        if (dc.ColumnName.Contains("_cbo"))
                            strResult += "cbo" + dc.ColumnName + ".Text = l" + strObject + "." + dc.ColumnName + ".ToString();\r\n";
                        else if (dc.ColumnName.Contains("_b"))
                            strResult += "chk" + dc.ColumnName + ".Text = l" + strObject + "." + dc.ColumnName + ".ToString();\r\n";
                        else if (dc.DataType == typeof(System.DateTime))
                            strResult += "dt" + dc.ColumnName + ".Text = l" + strObject + "." + dc.ColumnName + ".ToString();\r\n";
                        else
                            strResult += "txt" + dc.ColumnName + ".Text = l" + strObject + "." + dc.ColumnName + ".ToString();\r\n";
                        break;
                }
            strResult += "}" + "\r\n";

            strResult += "private bool Validate" + strObject + "()\r\n";
            strResult += "{" + "\r\n";
            strResult += "return true;\r\n";
            strResult += "}" + "\r\n";

            strResult += "private void Set" + strObject + "()\r\n";
            strResult += "{" + "\r\n";
            strResult += "if (l" + strObject + " == null)\r\n";
            strResult += "l" + strObject + " = new " + strObject + "();\r\n";
            String strTempIf = "            if (formMode == FormModes.Add)\r\n { \r\n";
            String strTempElse = "          else\r\n { \r\n";
            foreach (DataColumn dc in dt.Columns)
                switch (dc.ColumnName)
                {
                    case "CreatedBy":
                        strTempIf += "l" + strObject + "." + dc.ColumnName + " = Globals.uLogin.Username;\r\n";
                        strTempElse += "l" + strObject + "." + dc.ColumnName + " = String.Empty;\r\n";
                        break;

                    case "CreatedDate":
                        strTempIf += "l" + strObject + "." + dc.ColumnName + " = DateTime.UtcNow;\r\n";
                        strTempElse += "l" + strObject + "." + dc.ColumnName + " = Globals.NullDateTime;\r\n";
                        break;

                    case "UpdatedBy":
                        strTempIf += "l" + strObject + "." + dc.ColumnName + " = String.Empty;\r\n";
                        strTempElse += "l" + strObject + "." + dc.ColumnName + " = Globals.uLogin.Username;\r\n";
                        break;

                    case "UpdatedDate":
                        strTempIf += "l" + strObject + "." + dc.ColumnName + " = Constants.NullDateTime;\r\n";
                        strTempElse += "l" + strObject + "." + dc.ColumnName + " = Constants.DateTime.UtcNow;\r\n";
                        break;

                    default:
                        switch (dc.DataType.ToString())
                        {
                            case "System.Int32":
                                strResult += "l" + strObject + "." + dc.ColumnName + " = txt" + dc.ColumnName + ".Text == String.Empty ? 0 : Convert.ToInt16(txt" + dc.ColumnName + ".Text);\r\n";
                                break;
                            case "System.Decimal":
                                strResult += "l" + strObject + "." + dc.ColumnName + " = txt" + dc.ColumnName + ".Text == String.Empty ? 0 : Convert.ToInt16(txt" + dc.ColumnName + ".Text);\r\n";
                                break;
                            case "System.DateTime":
                                strResult += "l" + strObject + "." + dc.ColumnName + " = Convert.ToDateTime(dt" + dc.ColumnName + ".Text);\r\n";
                                break;
                            default:
                                if (dc.ColumnName.Contains("_cbo"))
                                    strResult += "l" + strObject + "." + dc.ColumnName + " = cbo" + dc.ColumnName + ".Text;\r\n";
                                else if (dc.ColumnName.Contains("_b"))
                                    strResult += "l" + strObject + "." + dc.ColumnName + " = chk" + dc.ColumnName + ".Text;\r\n";
                                else
                                    strResult += "l" + strObject + "." + dc.ColumnName + " = txt" + dc.ColumnName + ".Text;\r\n";
                                break;
                        }
                        break;
                }
            strResult = strResult + strTempIf + "}\r\n";
            strResult = strResult + strTempElse + "}\r\n";
            strResult += "}" + "\r\n";

            String strtmp = @"public override void AddClick()
                    {
                        base.AddClick();
                       // UnlockControls();
                        formMode = FormModes.Add;
                        BaseService.ClearTextBoxes(groupBox1);
                        BaseService.UnLockControls(groupBox1);
                    }
                    public override void Edit()
                    {
                        base.Edit();
                        formMode = FormModes.Edit;
                        DataRowView dr = Grid.SelectedRows[0].DataBoundItem as DataRowView;
                        if (dr != null)
                        {
                            lCustomerCode = dr[""CustomerCode""].ToString();
                            Customer _Customer = new Customer();
                            lCustomer = _Customer.GetCustomerById(lCustomerCode);
                            LoadCustomer();
                            txtCustomerCode.Enabled = false;
                        }
                      }
                    public override void SaveClick()
                    {
                        base.SaveClick();
                        if (ValidateCustomer())
                        {
                            SetCustomer();
                            Customer CustomerSave = new Customer();

                            if (FormMode == FormModes.Add)
                            {
                                if (CustomerSave.Save(lCustomer, true))
                                {
                                    RefreshGrid();
                                    Msg.ShowMsg(""Customer Inserted"");
                                    BaseService.ClearTextBoxes(groupBox1);
                                }
                            }
                            else if (FormMode == FormModes.Edit)
                            {
                                if (CustomerSave.Save(lCustomer, false))
                                {
                                    RefreshGrid();
                                    Msg.ShowMsg(""Customer updated"");
                                    BaseService.ClearTextBoxes(groupBox1);
                                }
                            }
                        }
                    }
                    public override void DeleteClick()
                    {
                        base.DeleteClick();
                        DataRowView dr = Grid.SelectedRows[0].DataBoundItem as DataRowView;
                        if (dr != null)
                        {
                            lCustomerCode= dr[""CustomerCode""].ToString();
                            String strCustomerCode = dr[""Name""].ToString();

                            if (lCustomer.CustomerCode == Constants.NullString)
                            {
                                Msg.ShowMsg(""Please double click on item to be deleted !!"");
                                return;
                            }

                            String msg = ""Are you sure you want to delete "" + lCustomerCode + "" - "" + strCustomerCode + ""?"";
                            if (Msg.ShowConfimation(msg))
                            {
                                if (lCustomer.Delete(lCustomer))
                                {
                                    RefreshGrid();
                                    BaseService.ClearTextBoxes(groupBox1);
                                }
                            }
                        }
                    }
                            ";
            strtmp = strtmp.Replace("lCustomerCode", "lObjCode");
            strtmp = strtmp.Replace("CustomerCode", "TingPow");
            strtmp = strtmp.Replace("Customer", strObject);
            strResult += strtmp;
            strResult += "\r\n<Forms>\r\n";
            return strResult;
        }
    }
}
