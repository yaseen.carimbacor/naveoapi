﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NaveoIntel
{
    public static class ProcessIntel
    {
        //public static String sProcessDevices()
        //{
        //    String sql = @"";
        //    return sql;
        //}

        public static String sProcessTrips(int iAssetID)
        {
            String sql = @"
--Trips Processor
--To resolve, just remove this line or change this line to SET NOCOUNT OFF; and everything works fine.
-- All Stop > 100
--	Stop ign off 111
--	Stop by Time 122
-- Trips < 100
--	New Trips < 50
--	New Trip 10
--	New Trip by Time 30
--	In Trip 60
--From CM
--update GFI_GPS_GPSData set StopFlag = 125 where StopFlag = 0
--update GFI_GPS_GPSData set StopFlag = 126 where StopFlag = 1
set nocount on

--Working table.
begin
	create table #GFI_GPS_GPSData 
	(
		[RowNumber] [int] IDENTITY(1,1) NOT NULL,
		[UID] [int] PRIMARY KEY CLUSTERED,
		[AssetID] [int] NULL,
		[DateTimeGPS_UTC] [datetime] NULL,
		[DateTimeServer] [datetime] NULL,
		[Longitude] [float] NULL,
		[Latitude] [float] NULL,
		[LongLatValidFlag] [int] NULL,
		[Speed] [float] NULL,
		[EngineOn] [int] NULL,
		[StopFlag] [int] NULL,
		[TripDistance] [float] NULL,
		[TripTime] [float] NULL,
		[WorkHour] [int] NULL,
		[DriverID] [int] NOT NULL,
        Misc1 nvarchar(100) null,
		Misc2 nvarchar(100) null,
		Misc3 nvarchar(100) null
	)
	CREATE NONCLUSTERED INDEX #myIndex ON #GFI_GPS_GPSData 
	(
		[AssetID] ASC,
		[DateTimeGPS_UTC] ASC,
		[LongLatValidFlag] ASC,
		[StopFlag] ASC,
		[Speed] ASC
	)
	CREATE TABLE #GFI_GPS_TripHeader
	(
		[iID] [int]  NOT NULL PRIMARY KEY CLUSTERED,
		[AssetID] [int] NOT NULL,
		[DriverID] [int] NOT NULL,
		[ExceptionFlag] [int] NULL,
		[MaxSpeed] [int] NULL,
		[IdlingTime] [float] NULL,
		[StopTime] [float] NULL,
		[TripDistance] [float] NULL,
		[TripTime] [float] NULL,
		[GPSDataStartUID] [int] NULL,
		[GPSDataEndUID] [int] NULL,
		[dtStart] [datetime] NULL,
		[fStartLon] [float] NULL,
		[fStartLat] [float] NULL,
		[dtEnd] [datetime] NULL,
		[fEndLon] [float] NULL,
		[fEndLat] [float] NULL,
		[OverSpeed1Time] [float] Default 0 NOT NULL,
		[OverSpeed2Time] [float] Default 0 NOT NULL,
		[Accel] [int] Default 0 NOT NULL,
		[Brake] [int] Default 0 NOT NULL,
		[Corner] [int] Default 0 NOT NULL,
	 ) 
	create table #GFI_GPS_TripDetail
	(
		[RowNumber] [int] IDENTITY(1,1) PRIMARY KEY CLUSTERED NOT NULL,
		HeaderiID int,
		GPSDataUID int
	)
end

--Oracle Starts Here

--Declaring variables
begin
	declare @ActualAssetID int

	declare @MyAssetID int
	declare @dtStart datetime

	declare @UID [int] 
	declare @AssetID [int] 
	declare @DateTimeGPS_UTC [datetime] 
	declare @Longitude [float] 
	declare @Latitude [float]
	declare @LongLatValidFlag [int]
	declare @Speed [float] 
	declare @EngineOn [int] 

	declare @PreviousUID [int] 
	declare @PreviousDateTimeGPS_UTC [datetime] 
	declare @PreviousLongitude [float] 
	declare @PreviousLatitude [float]

	Declare @TripDistance float
	Declare @TripTime float
	Declare @NewTrip int
		
	Declare @VeryFirstTripID int
	declare @PrevCalcTripDistance float
	declare @PrevCalcTripTime float
	declare @FloatIgnore float

	declare @tmpDate datetime
	declare @tmpDate2 datetime
	declare @DelDate datetime

	Declare @StartProcessDate datetime
	Declare @HeaderID int

	declare @StartDT [datetime] 
	declare @fStartLon [float] 
	declare @fStartLat [float]
	declare @EndDT [datetime] 
	declare @fEndLon [float] 
	declare @fEndLat [float]

	declare @TripID [int] 
	declare @GPSDataEndUID [int] 
	declare @GPSDataStartUID [int] 
	declare @Ignore [int] 

	Declare @StopFlag int
	Declare @DriverID int
	declare @FirstTrip int
	declare @LastTrip int

	Declare @iFirstTripInserted int
	Declare @StartTripUID int
	Declare @MaxSpeed int
	Declare @StartTripDT datetime
	Declare @StopTime int
	Declare @IdlingDuration int
	Declare @IdlingTime int
	DECLARE @cnt INT
	DECLARE @SpeedFr INT
	DECLARE @SpeedTo INT
	Declare @TripiID int

	declare @maxRows int;

end

set @ActualAssetID = " + iAssetID + @"
set @maxRows = 1000

RestartAsThereIsNotEnoughData:

delete from GFI_GPS_ProcessPending where ProcessCode = 'TripsIgnored'
delete from GFI_GPS_ProcessPending where ProcessCode = 'ExceptionsIgnored'

update GFI_GPS_ProcessPending set Processing = 1
	where ProcessCode = 'Trips' and AssetID = @ActualAssetID
		and Processing = 0;

--Cursor2 Starts -->>> Outer
 Declare @UnusedAa int
DECLARE ProcessCursor CURSOR FOR  
	SELECT GFI_GPS_ProcessPending.assetid, MIN(dtdatefrom) MinDate
		FROM GFI_GPS_ProcessPending where ProcessCode = 'Trips' and Processing = 1 and AssetID = @ActualAssetID
	GROUP BY GFI_GPS_ProcessPending.assetid
		
open ProcessCursor
	fetch next from ProcessCursor into @MyAssetID, @dtStart
	WHILE @@FETCH_STATUS = 0   
	BEGIN 
		Begin Try
			print 'MyAssetID ' +  convert(varchar(50), @MyAssetID) + ' ' + CONVERT(VarChar(50), GETDATE(), 114)
	  
			set @UID = null 
			set @AssetID = null 
			set @DateTimeGPS_UTC = null 
			set @Longitude = null 
			set @Latitude = null
			set @LongLatValidFlag = null
			set @Speed = null 
			set @EngineOn = null 
			set @PreviousUID = null 
			set @PreviousDateTimeGPS_UTC = null 
			set @PreviousLongitude = null
			set @PreviousLatitude = null
			set @TripDistance = null
			set @TripTime = null
			set @NewTrip = 10
			set @VeryFirstTripID = null

			set @PrevCalcTripDistance = null
			set @PrevCalcTripTime = null

			--Preparing data in GFI_GPS_GPSData. 
			--Trip_Cursor is a temp cursor, used many times below
			--Getting data as from end of last trip. this 1st row will be skipped in @NewTrip = 10
			begin
				--Get 1st record prior to the actual one
				set @tmpDate = (select top 1 DateTimeGPS_UTC from GFI_GPS_GPSData
									where DateTimeGPS_UTC < @dtStart 
										and AssetID = @MyAssetID and LongLatValidFlag = 1
										order by DateTimeGPS_UTC desc
								)

				--Get 1st stop prior to the result
				set @tmpDate2 = (select MAX(DateTimeGPS_UTC) 
									from GFI_GPS_GPSData
									where AssetID = @MyAssetID 
										and LongLatValidFlag = 1 
										and stopFlag > 100 --and stopFlag != 133 --and cmd added to test cut trips in middle. insert tripdetail using @AssetID added same time as this and cmd
										and DateTimeGPS_UTC < @tmpDate 
								)
				
				--If @tmpDate2 is null for x reason
				if(@tmpDate2 is null)
				begin
					set @tmpDate2 = (select MAX(DateTimeGPS_UTC) 
										from GFI_GPS_GPSData
										where AssetID = @MyAssetID 
											and LongLatValidFlag = 1 
											and stopFlag > 100 and stopFlag != 133
											and DateTimeGPS_UTC < @dtStart 
									)
				end

				set @tmpDate2 = coalesce(@tmpDate2, @dtStart)

				delete from #GFI_GPS_GPSData
				delete from #GFI_GPS_TripDetail

				insert #GFI_GPS_GPSData (UID, AssetID, DateTimeGPS_UTC, DateTimeServer, Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, StopFlag, TripDistance, TripTime, WorkHour, DriverID)
					select top (@maxRows) UID, AssetID, DateTimeGPS_UTC, DateTimeServer, Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, StopFlag, TripDistance, TripTime, WorkHour, DriverID from GFI_GPS_GPSData (nolock)
						where AssetID = @MyAssetID 
							and DateTimeGPS_UTC >= @tmpDate2
					ORDER BY DateTimeGPS_UTC

                select * from #GFI_GPS_GPSData;
				if(@@ROWCOUNT >= @maxRows - 1)
				begin
					insert into GFI_GPS_ProcessPending (AssetID, dtdatefrom, ProcessCode) 
						values (@MyAssetID, (select dateAdd(second, 1, max(DateTimeGPS_UTC)) from #GFI_GPS_GPSData ), 'Trips');
				end

				IF (SELECT CURSOR_STATUS('local','Trip_Cursor')) >= -1
				 BEGIN
				  IF (SELECT CURSOR_STATUS('local','Trip_Cursor')) > -1
				   BEGIN
					CLOSE Trip_Cursor
				   END
				 DEALLOCATE Trip_Cursor
				END

                --Orcl if c1%isopen then close c1; end if;
                --Cursor Starts
                Declare @UnusedA int
				DECLARE Trip_Cursor CURSOR LOCAL SCROLL STATIC FOR  
					select UID, AssetID, DateTimeGPS_UTC, Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, TripDistance, TripTime 
						from #GFI_GPS_GPSData 
						where AssetID = @MyAssetID 
							and LongLatValidFlag = 1
						order by DateTimeGPS_UTC

				set @StartProcessDate = null
				OPEN Trip_Cursor 
					FETCH NEXT FROM Trip_Cursor INTO @UID, @AssetID, @DateTimeGPS_UTC, @Longitude, @Latitude, @LongLatValidFlag, @Speed, @EngineOn, @PrevCalcTripDistance, @PrevCalcTripTime
					set @VeryFirstTripID = @UID
					set @StartProcessDate = @DateTimeGPS_UTC

					WHILE @@FETCH_STATUS = 0   
					BEGIN   
						FETCH PRIOR FROM Trip_Cursor INTO @PreviousUID, @AssetID, @PreviousDateTimeGPS_UTC, @PreviousLongitude, @PreviousLatitude, @LongLatValidFlag, @Speed, @EngineOn, @FloatIgnore, @FloatIgnore
						FETCH NEXT FROM Trip_Cursor INTO @UID, @AssetID, @DateTimeGPS_UTC, @Longitude, @Latitude, @LongLatValidFlag, @Speed, @EngineOn, @FloatIgnore, @FloatIgnore

						--In case this is a new batch. so @PreviousUID can also be null. skipping 1st row. end of last trip. So new Trip starting
						if(@NewTrip = 10)	
						begin
							set @NewTrip = 1
							set @TripDistance = 0
							set @TripTime = 0
						end
						else if (@EngineOn = 0)	--Ign off detected
						begin
							begin try
								if(@PreviousLatitude != @Latitude and @PreviousLongitude != @Longitude)
									set @TripDistance = @TripDistance + ACOS(SIN(PI()*@PreviousLatitude/180.0)*SIN(PI()*@Latitude/180.0)+COS(PI()*@PreviousLatitude/180.0)*COS(PI()*@Latitude/180.0)*COS(PI()*@Longitude/180.0-PI()*@PreviousLongitude/180.0))*6371
								set @TripTime = @TripTime + DATEDIFF(second, @PreviousDateTimeGPS_UTC, @DateTimeGPS_UTC)
							end Try
							Begin Catch
								print '1 Error ' + ERROR_MESSAGE() + convert(varchar(50), @uid)
								-- continue
							End catch

							update #GFI_GPS_GPSData set StopFlag = 111, TripDistance = @TripDistance, TripTime = @TripTime where UID = @UID
							set @TripDistance = 0 
							set @TripTime = 0
							set @NewTrip = 1
						end
						else if(@NewTrip = 1)--New Trip
						begin
							update #GFI_GPS_GPSData set StopFlag = 10, TripDistance = @TripDistance, TripTime = @TripTime where UID = @UID 
							set @TripDistance = 0 
							set @NewTrip = 0
						end

						--else if (@Speed < 5)	--Existing Trip on Time. Case where points are send evert minute
						--begin
						--	begin Try
						--		--locate idle start time
						--		select top 1 @PreviousUID = UID, @PreviousDateTimeGPS_UTC = DateTimeGPS_UTC, @PreviousLongitude = Longitude, @PreviousLatitude = Latitude
						--		from #GFI_GPS_GPSData 
						--		where AssetID = @MyAssetID 
						--			and LongLatValidFlag = 1
						--			and Speed >= 5
						--			and DateTimeGPS_UTC < @DateTimeGPS_UTC
						--		order by DateTimeGPS_UTC desc

						--		if (DATEDIFF(second, @PreviousDateTimeGPS_UTC, @DateTimeGPS_UTC) > 200)
						--		begin
						--			update #GFI_GPS_GPSData set StopFlag = 122, TripDistance = @TripDistance, TripTime = @TripTime where UID = @PreviousUID
						--			set @TripTime = 0
						--			set @TripDistance = 0

						--			update #GFI_GPS_GPSData set StopFlag = 122, TripDistance = @TripDistance, TripTime = @TripTime where DateTimeGPS_UTC > @PreviousDateTimeGPS_UTC and DateTimeGPS_UTC < @DateTimeGPS_UTC

						--			--no need for @NewTrip = 1, as both previous and actual rows are being updated
						--			update #GFI_GPS_GPSData set StopFlag = 30, TripDistance = @TripDistance, TripTime = @TripTime where UID = @UID 
						--			set @NewTrip = 0 
						--		end
						--		else
						--		begin
						--			update #GFI_GPS_GPSData set StopFlag = 60, TripDistance = @TripDistance, TripTime = @TripTime where UID = @UID
						--			set @NewTrip = 0 
						--		end
						--	end Try
						--	Begin Catch
						--		insert GFI_GPS_ProcessErrors (sError, sOrigine, sFieldName, sFieldValue, AssetID, dtUTC) values (ERROR_MESSAGE(), 'TripsM', 'None', ERROR_LINE(), @MyAssetID, @dtStart)
						--		print '2 Error ' + ERROR_MESSAGE() + convert(varchar(50), @uid)
						--		-- continue
						--	End catch
						--end

						else if (DATEDIFF(second, @PreviousDateTimeGPS_UTC, @DateTimeGPS_UTC) <= 200)	--Existing Trip on Time
						begin
							begin Try
								if(@PreviousLatitude != @Latitude and @PreviousLongitude != @Longitude)
									set @TripDistance = @TripDistance + ACOS(SIN(PI()*@PreviousLatitude/180.0)*SIN(PI()*@Latitude/180.0)+COS(PI()*@PreviousLatitude/180.0)*COS(PI()*@Latitude/180.0)*COS(PI()*@Longitude/180.0-PI()*@PreviousLongitude/180.0))*6371
								set @TripTime = @TripTime + DATEDIFF(second, @PreviousDateTimeGPS_UTC, @DateTimeGPS_UTC)
							end Try
							Begin Catch
								print '2 Error ' + ERROR_MESSAGE() + convert(varchar(50), @uid)
								-- continue
							End catch
						
							update #GFI_GPS_GPSData set StopFlag = 60, TripDistance = @TripDistance, TripTime = @TripTime where UID = @UID
							set @NewTrip = 0 
						end
						else
						begin
							--New Trip. All new trip start with 30. Stop by Time 122
							update #GFI_GPS_GPSData set StopFlag = 122, TripDistance = @TripDistance, TripTime = @TripTime where UID = @PreviousUID
							set @TripTime = 0
							set @TripDistance = 0
						
							--no need for @NewTrip = 1, as both previous and actual rows are being updated
							update #GFI_GPS_GPSData set StopFlag = 30, TripDistance = @TripDistance, TripTime = @TripTime where UID = @UID 
							set @NewTrip = 0 
						end
				
						FETCH NEXT FROM Trip_Cursor INTO @UID, @AssetID, @DateTimeGPS_UTC, @Longitude, @Latitude, @LongLatValidFlag, @Speed, @EngineOn, @FloatIgnore, @FloatIgnore
					END
					--Updating last record
					update #GFI_GPS_GPSData set StopFlag = 133 where UID = @UID
				close Trip_Cursor
				deallocate Trip_Cursor
                --Cursor2 ends
			end

			--Building Trips
			begin
				print 'Building Trips...'
				--delete from GFI_GPS_TripHeader where AssetID = @MyAssetID and dtStart >=  @tmpDate2
				set @DelDate = @tmpDate2
				delete from #GFI_GPS_TripHeader
				set @HeaderID = 0

				set @StopFlag = null
				set @DriverID = null
				set @FirstTrip = null
				set @LastTrip = null
				set	@UID = null 
				set	@PreviousUID = null
				set @iFirstTripInserted = 0 
				--Cursor2 Starts
                Declare @UnusedB int
                DECLARE Trip_Cursor CURSOR LOCAL SCROLL STATIC FOR  
					SELECT UID, AssetID, DriverID , DateTimeGPS_UTC, Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, StopFlag, TripDistance, TripTime
						FROM #GFI_GPS_GPSData 
					where AssetID = @MyAssetID and StopFlag > 100 and LongLatValidFlag = 1
						order by DateTimeGPS_UTC

				OPEN Trip_Cursor 
					FETCH NEXT FROM Trip_Cursor INTO @UID, @AssetID, @DriverID, @DateTimeGPS_UTC, @Longitude, @Latitude, @LongLatValidFlag, @Speed, @EngineOn, @StopFlag, @TripDistance, @TripTime
					WHILE @@FETCH_STATUS = 0   
					BEGIN
						FETCH PRIOR FROM Trip_Cursor INTO @PreviousUID, @AssetID, @DriverID, @PreviousDateTimeGPS_UTC, @PreviousLongitude, @PreviousLatitude, @LongLatValidFlag, @Speed, @EngineOn, @StopFlag, @TripDistance, @TripTime
						FETCH NEXT FROM Trip_Cursor INTO @UID, @AssetID, @DriverID, @DateTimeGPS_UTC, @Longitude, @Latitude, @LongLatValidFlag, @Speed, @EngineOn, @StopFlag, @TripDistance, @TripTime

						set @tmpDate = (select max(DateTimeGPS_UTC) from #GFI_GPS_GPSData
											where DateTimeGPS_UTC < @DateTimeGPS_UTC
												and StopFlag > 100  
												and LongLatValidFlag = 1 
												and AssetID = @MyAssetID
										)

						set @tmpDate2 = (select min(DateTimeGPS_UTC) from #GFI_GPS_GPSData 
											where AssetID = @MyAssetID 
												and LongLatValidFlag = 1
												and DateTimeGPS_UTC = @tmpDate
										)

						set @StartTripUID = null

						set @StartTripUID = (select top 1 UID from #GFI_GPS_GPSData 
												where AssetID = @MyAssetID 
													and LongLatValidFlag = 1
													and DateTimeGPS_UTC > @tmpDate2
												order by DateTimeGPS_UTC
											)

						set @MaxSpeed = null
						set @MaxSpeed = (select max(speed) from #GFI_GPS_GPSData where (UID between @StartTripUID and @UID) and LongLatValidFlag = 1 and AssetID = @MyAssetID)
				
						select @StartDT = DateTimeGPS_UTC, @fStartLon = Longitude, @fStartLat = Latitude  from #GFI_GPS_GPSData where UID = @StartTripUID
						select @EndDT = DateTimeGPS_UTC, @fEndLon = Longitude, @fEndLat = Latitude  from #GFI_GPS_GPSData where UID = @UID

						if(@TripDistance is not null and @TripTime is not null and @MaxSpeed is not null and @StartTripUID != @UID)
						begin
							set @HeaderID = @HeaderID + 1

							insert #GFI_GPS_TripHeader (iID, AssetID, DriverID, ExceptionFlag, MaxSpeed, IdlingTime, StopTime, TripDistance, TripTime, GPSDataStartUID, GPSDataEndUID, dtStart, fStartLon, fStartLat, dtEnd, fEndLon, fEndLat)
								values (@HeaderID, @AssetID, @DriverID, 0, @MaxSpeed, 0, 0, @TripDistance, @TripTime, @StartTripUID, @UID, @StartDT, @fStartLon, @fStartLat, @EndDT, @fEndLon, @fEndLat)
				
							if (@iFirstTripInserted = 0)
								set @FirstTrip = @HeaderID
							set @iFirstTripInserted = @iFirstTripInserted + 1
							set @LastTrip = @HeaderID

							set @StartTripDT = (select DateTimeGPS_UTC from #GFI_GPS_GPSData where UID = @StartTripUID)

							insert into #GFI_GPS_TripDetail (HeaderiID, GPSDataUID)
								select @HeaderID, UID 
									from #GFI_GPS_GPSData where (DateTimeGPS_UTC between @StartTripDT and @DateTimeGPS_UTC) and AssetID = @MyAssetID and LongLatValidFlag = 1
									order by DateTimeGPS_UTC;
						end

						FETCH NEXT FROM Trip_Cursor INTO @UID, @AssetID, @DriverID, @DateTimeGPS_UTC, @Longitude, @Latitude, @LongLatValidFlag, @Speed, @EngineOn, @StopFlag, @TripDistance, @TripTime
					END
				close Trip_Cursor
				deallocate Trip_Cursor
                --Cursor2 ends
			end

			--Calculating Stop Time and Idling time
			--if(1 = 0)
			begin
				print 'Calculating Stop Time'
				if(@FirstTrip is null)
					set @FirstTrip = @LastTrip
				print 'First Trip ' + convert(varchar(50), @FirstTrip) 
				print 'Last Trip ' + convert(varchar(50), @LastTrip) 
		
				if(@FirstTrip is null or @LastTrip = 1)
				begin
					set @maxRows = @maxRows + 1000
					if(@maxRows > 9000)
						goto ExitCode

					close ProcessCursor
					deallocate ProcessCursor
					goto RestartAsThereIsNotEnoughData
				end

				set	@TripID = null 
				set	@GPSDataEndUID = null 
				set	@GPSDataStartUID = null 
				set	@Ignore = null 
                --Cursor2 Starts
                Declare @UnusedC int
				DECLARE Trip_Cursor CURSOR LOCAL SCROLL STATIC FOR  
					select iID, GPSDataStartUID, GPSDataEndUID from #GFI_GPS_TripHeader
						where AssetID = @MyAssetID --and iID >= @FirstTrip and iID <= @LastTrip
						order by iID
		
				open Trip_Cursor
					FETCH Next FROM Trip_Cursor INTO @TripID, @Ignore, @GPSDataEndUID
					WHILE @@FETCH_STATUS = 0   
					BEGIN
						FETCH NEXT FROM Trip_Cursor INTO @Ignore, @GPSDataStartUID, @Ignore
						FETCH PRIOR FROM Trip_Cursor INTO @TripID, @Ignore, @GPSDataEndUID
				
						set @StopTime = null
						set @PreviousDateTimeGPS_UTC = (select DateTimeGPS_UTC from #GFI_GPS_GPSData where UID = @GPSDataEndUID)
						set @DateTimeGPS_UTC = (select DateTimeGPS_UTC from #GFI_GPS_GPSData where UID = @GPSDataStartUID)
						set @StopTime = DATEDIFF(second, @PreviousDateTimeGPS_UTC, @DateTimeGPS_UTC)
				
						--Calculating Idling time
						--Segregate speed <= 5 per TripHeader
						--Group above records, calculating min and max duration
						--Sum Total Duration
						--if(1=0)
						begin
							set @IdlingDuration = 5
							set @IdlingTime = 0

							--Get All trip details with Next record id
							;WITH CTE AS 
									(
										SELECT g.UID, g.DateTimeGPS_UTC, g.Speed, rownum = ROW_NUMBER() OVER (ORDER BY g.UID)
										from #GFI_GPS_TripDetail d
											inner join #GFI_GPS_GPSData g on d.GPSDataUID = g.UID
										where d.HeaderiID = @TripID 
									)
							SELECT cte.* into #trip FROM CTE

							--Get only idle details with Next record id
							;WITH CTE AS 
									(
										SELECT *, IdleRowNum = ROW_NUMBER() OVER (ORDER BY UID)
										from #trip where speed < @IdlingDuration
									)
							SELECT t.*
								, prev.UID PreviousIdleValue
								, nex.UID NextIdleValue
								, 0 GrpID
    							into #idles 
							FROM #trip t
							    left outer join CTE on t.UID = CTE.UID
							    LEFT JOIN CTE prev ON prev.rownum = CTE.rownum - 1
							    LEFT JOIN CTE nex ON nex.rownum = CTE.rownum + 1

							update #idles set NextIdleValue = PreviousIdleValue where NextIdleValue is null
							update #idles set GrpID = R.Grp
							from #idles T
								inner join (select *, (select count(1) from #idles where NextIdleValue is null and RowNum <= tt.RowNum) grp from #idles tt) R
								on R.RowNum = T.RowNum

							;WITH TopBottomRow
							AS
							(
								SELECT min(DateTimeGPS_UTC) mini, max(DateTimeGPS_UTC) maxi
									FROM #idles where NextIdleValue is not null
									Group BY GrpID
							)
							SELECT @IdlingTime = sum(DATEDIFF(SECOND, mini, maxi))  
								FROM TopBottomRow 

							drop table #trip
							drop table #idles
						end
					
						if(@StopTime < 0)	-- This is the last Trip
							set @StopTime = 0
						--print 'TripID ' + convert(varchar(10), @TripID)
						--	+ ' From ' + convert(varchar(50), @PreviousDateTimeGPS_UTC)
						--	+ ' To ' + convert(varchar(50), @DateTimeGPS_UTC)
						--	+ ' Stop ' + convert(varchar(50), @StopTime)
						--	+ ' Idle ' + convert(varchar(50), @IdlingTime)
						update #GFI_GPS_TripHeader set StopTime = @StopTime, IdlingTime = @IdlingTime where iID = @TripID
				
						FETCH Next FROM Trip_Cursor INTO @TripID, @Ignore, @GPSDataEndUID
					end
				close Trip_Cursor
				deallocate Trip_Cursor
                --Cursor2 ends
			end

			--Calculating ScoreCard
			--if(1 = 0)
			begin
				set @cnt = 0;
				set @SpeedFr = 75;
				set @SpeedTo = 110;

				WHILE @cnt < 2
				BEGIN
					--PRINT 'Inside FOR LOOP';
					IF OBJECT_ID('tempdb..#Speed') IS NOT NULL drop table #Speed
					IF OBJECT_ID('tempdb..#OuterT') IS NOT NULL drop table #OuterT
					IF OBJECT_ID('tempdb..#AllRows') IS NOT NULL drop table #AllRows
					IF OBJECT_ID('tempdb..#AllRowsPrepared') IS NOT NULL drop table #AllRowsPrepared

					if(@cnt = 1)
					begin
						Set @SpeedFr = 70; --110;
						Set @SpeedTo = 999;
					end

					--Adding NextID field for outer table
					;WITH CTE AS 
							(
								SELECT * from #GFI_GPS_GPSData 
							)
					SELECT t.*
						, prev.UID PreviousOuterValue
						, nex.UID NextOuterValue
						, 0 GrpID
    					into #OuterT 
					FROM #GFI_GPS_GPSData t
					    left outer join CTE on t.UID = CTE.UID
					    LEFT JOIN CTE prev ON prev.RowNumber = CTE.RowNumber - 1
					    LEFT JOIN CTE nex ON nex.RowNumber = CTE.RowNumber + 1
					order by dateTimeGPS_UTC

					--Adding NextID field for inner table
					;WITH CTE AS 
							(
								SELECT *, InnerRowNum = ROW_NUMBER() OVER (ORDER BY DateTimeGPS_UTC) from #GFI_GPS_GPSData where Speed >= @SpeedFr and Speed < @SpeedTo
							)
					SELECT t.*, CTE.InnerRowNum
					    into #AllRows 
					FROM #OuterT t
					    left outer join CTE on t.UID = CTE.UID

					--Merging outer and inner field
					;WITH CTE AS 
							(
								SELECT * from #AllRows
							)
					SELECT t.*
						, prev.UID PreviousInnerValue
						, nex.UID NextInnerValue
    					into #AllRowsPrepared
					FROM #AllRows t
					    left outer join CTE on t.UID = CTE.UID 
					    LEFT JOIN CTE prev ON prev.InnerRowNum = CTE.InnerRowNum - 1
					    LEFT JOIN CTE nex ON nex.InnerRowNum = CTE.InnerRowNum + 1
					order by DateTimeGPS_UTC

					--NextInner should = NextOuter, then this means a group. So when they are not same, update to null, then group sum of null  to get groupid
					update #AllRowsPrepared set NextInnerValue = null where NextInnerValue != NextOuterValue
					update #AllRowsPrepared set GrpID = R.Grp
						from #AllRowsPrepared T
							inner join (select *, (select count(1) from #AllRowsPrepared where NextInnerValue is null and InnerRowNum >= tt.InnerRowNum) grp from #AllRowsPrepared tt) R
							on R.uid = T.uid

					select GrpID, min(dateTimeGPS_UTC) dtFr, max(dateTimeGPS_UTC) dtTo, 0 Duration, 0 TID, 0 SumDuration, 0 Accel, 0 Brake, 0 Corner 
						into #Speed from #AllRowsPrepared where GrpID > 0 group by GrpID
					update #Speed set Duration = DateDiff(second, dtFr, dtTo)
					update #Speed set Duration = 10 where Duration = 0
					update #Speed set TID = t.iID
						from #Speed s
							inner join #GFI_GPS_TripHeader t on t.dtStart <= s.dtFr and s.dtTo <= t.dtEnd
						where t.iID >= @FirstTrip
					update s
						set s.SumDuration = r.SumDuration
					from #Speed s
						inner join (SELECT SUM(Duration) SumDuration, TID FROM #Speed GROUP BY TID) r ON s.TID = r.TID
					--Harsh
					if(@cnt = 0)
					begin
						update #Speed set Accel = f1.c 
							from #GFI_GPS_TripDetail d
								inner join 
									(select td.HeaderiID, COUNT(td.HeaderiID) c 
										from #GFI_GPS_TripDetail td 
											inner join GFI_GPS_GPSDataDetail g on g.UID = td.GPSDataUID and g.TypeID = 'Harsh Accelleration' 
										Group by td.HeaderiID
									) f1 on d.HeaderiID = f1.HeaderiID
								inner join #Speed s on s.TID = d.HeaderiID
							where d.HeaderiID >= @FirstTrip 

						update #Speed set Brake = f1.c 
							from #GFI_GPS_TripDetail d
								inner join 
									(select td.HeaderiID, COUNT(td.HeaderiID) c 
										from #GFI_GPS_TripDetail td 
											inner join GFI_GPS_GPSDataDetail g on g.UID = td.GPSDataUID and g.TypeID = 'Harsh Breaking' 
										Group by td.HeaderiID
									) f1 on d.HeaderiID = f1.HeaderiID
								inner join #Speed s on s.TID = d.HeaderiID
							where d.HeaderiID >= @FirstTrip

						update #Speed set Corner = f1.c 
							from #GFI_GPS_TripDetail d
								inner join 
									(select td.HeaderiID, COUNT(td.HeaderiID) c 
										from #GFI_GPS_TripDetail td 
											inner join GFI_GPS_GPSDataDetail g on g.UID = td.GPSDataUID and g.TypeID = 'Harsh Cornering' 
										Group by td.HeaderiID
									) f1 on d.HeaderiID = f1.HeaderiID
								inner join #Speed s on s.TID = d.HeaderiID
							where d.HeaderiID >= @FirstTrip 
					end
										
					if(@cnt = 0)
					update #GFI_GPS_TripHeader set OverSpeed1Time = s.SumDuration, Accel = s.Accel, Brake = s.Brake, Corner = s.Corner
						from #GFI_GPS_TripHeader t
							inner join #Speed s on t.iID = s.TID
						where t.iID >= @FirstTrip
					else if(@cnt = 1)
						update #GFI_GPS_TripHeader set OverSpeed2Time = s.SumDuration 
							from #GFI_GPS_TripHeader t
								inner join #Speed s on t.iID = s.TID
							where t.iID >= @FirstTrip

					SET @cnt = @cnt + 1;
				END;
			end

			--updating sql tables from tmp tables
			begin
				Begin Tran
					print 'Begin Tran ' + CONVERT(VarChar(50), GETDATE(), 114)
					delete from GFI_GPS_TripHeader where AssetID = @MyAssetID and dtStart >=  @DelDate

					set @TripiID = null
					SET @TripiID = IDENT_CURRENT('GFI_GPS_TripHeader')
					update #GFI_GPS_TripHeader set iID = iID + @TripiID
					update #GFI_GPS_TripDetail set HeaderiID = HeaderiID + @TripiID

					insert GFI_GPS_TripHeader (AssetID, DriverID, ExceptionFlag, MaxSpeed, IdlingTime, StopTime, TripDistance, TripTime, GPSDataStartUID, GPSDataEndUID, dtStart, fStartLon, fStartLat, dtEnd, fEndLon, fEndLat, OverSpeed1Time, OverSpeed2Time, Accel, Brake, Corner) 
						select AssetID, DriverID, ExceptionFlag, MaxSpeed, IdlingTime, StopTime, TripDistance, TripTime, GPSDataStartUID, GPSDataEndUID, dtStart, fStartLon, fStartLat, dtEnd, fEndLon, fEndLat, OverSpeed1Time, OverSpeed2Time, Accel, Brake, Corner from #GFI_GPS_TripHeader


					insert GFI_GPS_TripDetail (HeaderiID, GPSDataUID)
						select HeaderiID, GPSDataUID from #GFI_GPS_TripDetail

					update GFI_GPS_GPSData
						set StopFlag = t1.StopFlag, TripDistance = t1.TripDistance, TripTime = t1.TripTime 
					from #GFI_GPS_GPSData t1
						inner join GFI_GPS_GPSData t2 on t2.UID = t1.UID
					
					--LastTrip
					update GFI_GPS_TripHeader 
						set StopTime = 
							(
								DATEDIFF(second, T.dtEnd, (select top 1 dtStart from GFI_GPS_TripHeader where AssetID = r.AssetID and dtStart > r.dtStart order by dtStart asc))
							)
					from GFI_GPS_TripHeader T
						inner join (select * from GFI_GPS_TripHeader where StopTime is null and AssetID = @MyAssetID) R
						on R.iID = T.iID

					delete from GFI_GPS_ProcessPending where AssetID = @MyAssetID and ProcessCode = 'Trips' and Processing = 1
					print 'commit Tran ' + CONVERT(VarChar(50), GETDATE(), 114)
				commit tran
			end
		end try
		begin catch
			if(@@TRANCOUNT > 0)
				rollback tran

			begin try
				insert GFI_GPS_ProcessErrors (sError, sOrigine, sFieldName, sFieldValue, AssetID, dtUTC) values (ERROR_MESSAGE(), 'Trips', 'None', ERROR_LINE(), @MyAssetID, @dtStart)
				
				close Trip_Cursor
				deallocate Trip_Cursor
				drop table #trip
				drop table #idles
			end try
			begin catch
			end catch
		end catch

		fetch next from ProcessCursor into @MyAssetID, @dtStart
	END
close ProcessCursor
deallocate ProcessCursor
--Cursor2 ends

drop table #GFI_GPS_GPSData
drop table #GFI_GPS_TripHeader
drop table #GFI_GPS_TripDetail

ExitCode:

";
            return sql;
        }
        public static String OracleProcessTrips(int iAssetID)
        {
            String sql = @"
--SET SERVEROUTPUT ON
begin
    BEGIN
        EXECUTE IMMEDIATE 'truncate table GlobalTmpTripProcessor_GFI_GPS_GPSData';
        EXECUTE IMMEDIATE 'truncate table GlobalTmpTripProcessorGFI_GPS_TripHeader';
        EXECUTE IMMEDIATE 'truncate table GlobalTmpTripProcessorGFI_GPS_TripDetail';
        EXECUTE IMMEDIATE 'truncate table GlobalTmpTripProcessor_trip';
        EXECUTE IMMEDIATE 'truncate table GlobalTmpTripProcessor_idles';
        EXECUTE IMMEDIATE 'truncate table GlobalTmpTripProcessor_Speed';
        EXECUTE IMMEDIATE 'truncate table GlobalTmpTripProcessor_OuterT';
        EXECUTE IMMEDIATE 'truncate table GlobalTmpTripProcessor_AllRows';
        EXECUTE IMMEDIATE 'truncate table GlobalTmpTripProcessor_AllRowsPrepared';        
    EXCEPTION
        WHEN OTHERS THEN
            DECLARE SQLERRM NVARCHAR2(200);
            BEGIN DBMS_OUTPUT.put_line(SQLERRM); END;
    END; 

--Declaring variables
    Declare 
        v_ActualAssetID number;
        v_MyAssetID number;
        v_dtStart timestamp(3);
        v_UID number;
        v_AssetID number;
        v_DateTimeGPS_UTC timestamp(3);
        v_Longitude float;
        v_Latitude float;
        v_LongLatValidFlag number;
        v_Speed float;
        v_EngineOn number;
        v_PreviousUID number;
        v_PreviousDateTimeGPS_UTC timestamp(3);
        v_PreviousLongitude float;
        v_PreviousLatitude float;
        v_TripDistance float;
        v_TripTime float;
        v_NewTrip number;
        v_VeryFirstTripID number;
        v_PrevCalcTripDistance float;
        v_PrevCalcTripTime float;
        v_FloatIgnore float;
        v_tmpDate timestamp(3);
        v_tmpDate2 timestamp(3);
        v_DelDate timestamp(3);
        v_StartProcessDate timestamp(3);
        v_HeaderID number;
        v_StartDT timestamp(3);
        v_fStartLon float;
        v_fStartLat float;
        v_EndDT timestamp(3);
        v_fEndLon float;
        v_fEndLat float;
        v_TripID number;
        v_GPSDataEndUID number;
        v_GPSDataStartUID number;
        v_Ignore number;
        v_StopFlag number;
        v_DriverID number;
        v_FirstTrip number;
        v_LastTrip number;
        v_iFirstTripInserted number;
        v_StartTripUID number;
        v_MaxSpeed number;
        v_StartTripDT timestamp(3);
        v_StopTime number;
        v_IdlingDuration number;
        v_IdlingTime number;
        v_cnt number;
        v_SpeedFr number;
        v_SpeedTo number;
        v_TripiID number;
    begin
        v_ActualAssetID := " + iAssetID + @";
        DBMS_OUTPUT.put_line(v_ActualAssetID);
        delete from GFI_GPS_ProcessPending where ProcessCode = 'TripsIgnored';
        delete from GFI_GPS_ProcessPending where ProcessCode = 'ExceptionsIgnored';
        
        update GFI_GPS_ProcessPending set Processing = 1
            where ProcessCode = 'Trips' and AssetID = v_ActualAssetID and Processing = 0;
            
    Declare Cursor ProcessCursor is 
        SELECT GFI_GPS_ProcessPending.assetid, MIN(dtdatefrom) MinDate
            FROM GFI_GPS_ProcessPending where ProcessCode = 'Trips' and Processing = 1 and AssetID = v_ActualAssetID
        GROUP BY GFI_GPS_ProcessPending.assetid;
        BEGIN
            OPEN ProcessCursor;
            LOOP
                fetch ProcessCursor into v_MyAssetID, v_dtStart;
                EXIT WHEN ProcessCursor%notfound;
                BEGIN
                    DBMS_OUTPUT.put_line ('MyAssetID ' || v_MyAssetID || ' ' || v_dtStart);
                    
                    v_UID := null; 
                    v_AssetID := null; 
                    v_DateTimeGPS_UTC := null; 
                    v_Longitude := null; 
                    v_Latitude := null;
                    v_LongLatValidFlag := null;
                    v_Speed := null; 
                    v_EngineOn := null; 
                    v_PreviousUID := null; 
                    v_PreviousDateTimeGPS_UTC := null; 
                    v_PreviousLongitude := null;
                    v_PreviousLatitude := null;
                    v_TripDistance := null;
                    v_TripTime := null;
                    v_NewTrip := 10;
                    v_VeryFirstTripID := null;
        
                    v_PrevCalcTripDistance := null;
                    v_PrevCalcTripTime := null;
                    
                    --Preparing data in GFI_GPS_GPSData. 
                    --Trip_Cursor is a temp cursor, used many times below
                    --Getting data as from end of last trip. this 1st row will be skipped in v_NewTrip = 10
                    begin
                        begin
                            --Get 1st record prior to the actual one
                            select DateTimeGPS_UTC into v_tmpDate 
                                from GFI_GPS_GPSData
                            where DateTimeGPS_UTC < v_dtStart 
                                and AssetID = v_MyAssetID and LongLatValidFlag = 1 and rownum = 1
                                order by DateTimeGPS_UTC desc;
                        exception
                            when NO_DATA_FOUND then v_tmpDate := null;
                        end;

                        --Get 1st stop prior to the result
                        select MAX(DateTimeGPS_UTC) into v_tmpDate2
                            from GFI_GPS_GPSData
                        where AssetID = v_MyAssetID 
                            and LongLatValidFlag = 1 
                            and stopFlag > 100 --and stopFlag != 133 --and cmd added to test cut trips in middle. insert into tripdetail using v_AssetID added same time as this and cmd
                            and DateTimeGPS_UTC < v_tmpDate;

                        --If v_tmpDate2 is null for x reason
                        if(v_tmpDate2 is null)
                        then
                            select MAX(DateTimeGPS_UTC) into v_tmpDate2
                                from GFI_GPS_GPSData
                            where AssetID = v_MyAssetID 
                                and LongLatValidFlag = 1 
                                and stopFlag > 100 and stopFlag != 133
                                and DateTimeGPS_UTC < v_dtStart;
                        end if;

                        v_tmpDate2 := coalesce(v_tmpDate2, v_dtStart);
                        DBMS_OUTPUT.put_line('v_tmpDate2' || v_tmpDate2);
                            
                        delete from GlobalTmpTripProcessor_GFI_GPS_GPSData;
                        delete from GlobalTmpTripProcessorGFI_GPS_TripDetail;

                        insert into GlobalTmpTripProcessor_GFI_GPS_GPSData (""UID"", AssetID, DateTimeGPS_UTC, DateTimeServer, Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, StopFlag, TripDistance, TripTime, WorkHour, DriverID)
                            select ""UID"", AssetID, DateTimeGPS_UTC, DateTimeServer, Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, StopFlag, TripDistance, TripTime, WorkHour, DriverID from GFI_GPS_GPSData
                                where AssetID = v_MyAssetID 
                                    and DateTimeGPS_UTC >= v_tmpDate2
                            ORDER BY DateTimeGPS_UTC;
                            
                        declare Cursor Trip_Cursor is 
                            select ""UID"", AssetID, DateTimeGPS_UTC, Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, TripDistance, TripTime 
                                from GlobalTmpTripProcessor_GFI_GPS_GPSData 
                                where AssetID = v_MyAssetID 
                                    and LongLatValidFlag = 1
                                order by DateTimeGPS_UTC;

                        BEGIN 
                            v_StartProcessDate := null;
                            OPEN Trip_Cursor;
                            LOOP 
                                FETCH Trip_Cursor INTO v_UID, v_AssetID, v_DateTimeGPS_UTC, v_Longitude, v_Latitude, v_LongLatValidFlag, v_Speed, v_EngineOn, v_FloatIgnore, v_FloatIgnore;
                                v_VeryFirstTripID := v_UID;
                                v_StartProcessDate := v_DateTimeGPS_UTC;
                                
                                EXIT WHEN Trip_Cursor%notfound;                                
                                    BEGIN
                                        --DBMS_OUTPUT.put_line('UIDs' || v_UID || ' ' || v_PreviousUID || ' NewTrip ' || v_NewTrip || ' Engine on ' || v_EngineOn);

                                        --In case this is a new batch. so v_PreviousUID can also be null. skipping 1st row. end of last trip. So new Trip starting
                                        if(v_NewTrip = 10)	
                                        then
                                            v_NewTrip := 1;
                                            v_TripDistance := 0;
                                            v_TripTime := 0;
                                        else if (v_EngineOn = 0)	--Ign off detected
                                            then
                                                begin 
                                                    if(v_PreviousLatitude != v_Latitude and v_PreviousLongitude != v_Longitude) then
                                                        v_TripDistance := v_TripDistance + ACOS(SIN(PI()*v_PreviousLatitude/180.0)*SIN(PI()*v_Latitude/180.0)+COS(PI()*v_PreviousLatitude/180.0)*COS(PI()*v_Latitude/180.0)*COS(PI()*v_Longitude/180.0-PI()*v_PreviousLongitude/180.0))*6371;
                                                    end if;
                                                    v_TripTime := v_TripTime + round((cast(v_DateTimeGPS_UTC as date) - date '1970-01-01')*24*60*60 - (cast(v_PreviousDateTimeGPS_UTC as date) - date '1970-01-01')*24*60*60); -- DATEDIFF(second, v_PreviousDateTimeGPS_UTC, v_DateTimeGPS_UTC);
                                                exception
                                                    when others then
                                                        DBMS_OUTPUT.put_line('1 Error ' || SQLERRM || v_uid);
                                                        -- continue
                                                End;
                                        
                                                update GlobalTmpTripProcessor_GFI_GPS_GPSData set StopFlag = 111, TripDistance = v_TripDistance, TripTime = v_TripTime where ""UID"" = v_UID;
                                                v_TripDistance := 0; 
                                                v_TripTime := 0;
                                                v_NewTrip := 1;                                              
                                        else if(v_NewTrip = 1)--New Trip
                                            then
                                                update GlobalTmpTripProcessor_GFI_GPS_GPSData set StopFlag = 10, TripDistance = v_TripDistance, TripTime = v_TripTime where ""UID"" = v_UID ;
                                                v_TripDistance := 0;
                                                v_NewTrip := 0;

                                        else if (round((cast(v_DateTimeGPS_UTC as date) - date '1970-01-01')*24*60*60 - (cast(v_PreviousDateTimeGPS_UTC as date) - date '1970-01-01')*24*60*60) <= 200)	--Existing Trip on Time
                                            then
                                                begin 
                                                    if(v_PreviousLatitude != v_Latitude and v_PreviousLongitude != v_Longitude) then
                                                        v_TripDistance := v_TripDistance + ACOS(SIN(PI()*v_PreviousLatitude/180.0)*SIN(PI()*v_Latitude/180.0)+COS(PI()*v_PreviousLatitude/180.0)*COS(PI()*v_Latitude/180.0)*COS(PI()*v_Longitude/180.0-PI()*v_PreviousLongitude/180.0))*6371;
                                                    end if;
                                                    v_TripTime := v_TripTime + round((cast(v_DateTimeGPS_UTC as date) - date '1970-01-01')*24*60*60 - (cast(v_PreviousDateTimeGPS_UTC as date) - date '1970-01-01')*24*60*60);
                                                exception
                                                    when others then
                                                        DBMS_OUTPUT.put_line('2 Error ' || SQLERRM || v_uid);
                                                        -- continue
                                                End;
                                            
                                                update GlobalTmpTripProcessor_GFI_GPS_GPSData set StopFlag = 60, TripDistance = v_TripDistance, TripTime = v_TripTime where ""UID"" = v_UID;
                                                v_NewTrip := 0;

                                            else
                                                begin
                                                    --New Trip. All new trip start with 30. Stop by Time 122
                                                    update GlobalTmpTripProcessor_GFI_GPS_GPSData set StopFlag = 122, TripDistance = v_TripDistance, TripTime = v_TripTime where ""UID"" = v_PreviousUID;
                                                    v_TripTime := 0;
                                                    v_TripDistance := 0;
                                                
                                                    --no need for v_NewTrip = 1, as both previous and actual rows are being updated
                                                    update GlobalTmpTripProcessor_GFI_GPS_GPSData set StopFlag = 30, TripDistance = v_TripDistance, TripTime = v_TripTime where ""UID"" = v_UID;
                                                    v_NewTrip := 0; 
                                                end;
                                        end if;
                                        end if;
                                        end if;
                                        end if;
                                        --DBMS_OUTPUT.put_line('Trip_Cursor...');
                                    FETCH Trip_Cursor INTO v_PreviousUID, v_AssetID, v_PreviousDateTimeGPS_UTC, v_PreviousLongitude, v_PreviousLatitude, v_LongLatValidFlag, v_Speed, v_EngineOn, v_FloatIgnore, v_FloatIgnore;
                                    END;
                            END LOOP;
                            update GlobalTmpTripProcessor_GFI_GPS_GPSData set StopFlag = 133 where ""UID"" = v_UID;
                            CLOSE Trip_Cursor;
                        END; 
                     end;
                    
                    --Building Trips
                    begin
                        DBMS_OUTPUT.put_line('Building Trips...');
                        delete from GlobalTmpTripProcessorGFI_GPS_TripHeader;
                        v_DelDate := v_tmpDate2;
                        v_HeaderID := 0;
                        v_StopFlag := null;
                        v_DriverID := null;
                        v_FirstTrip := null;
                        v_LastTrip := null;
                        v_UID := null ;
                        v_PreviousUID := null;
                        v_iFirstTripInserted := 0; 
                            
                        Declare Cursor Trip_Cursor is 
                            SELECT ""UID"", AssetID, DriverID , DateTimeGPS_UTC, Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, StopFlag, TripDistance, TripTime
                                FROM GlobalTmpTripProcessor_GFI_GPS_GPSData 
                            where AssetID = v_MyAssetID and StopFlag > 100 and LongLatValidFlag = 1
                                order by DateTimeGPS_UTC;
                        BEGIN
                            OPEN Trip_Cursor;
                            LOOP 
                                FETCH Trip_Cursor INTO v_UID, v_AssetID, v_DriverID, v_DateTimeGPS_UTC, v_Longitude, v_Latitude, v_LongLatValidFlag, v_Speed, v_EngineOn, v_StopFlag, v_TripDistance, v_TripTime;
                                EXIT WHEN Trip_Cursor%notfound;
                                BEGIN
                                    select max(DateTimeGPS_UTC) into v_tmpDate
                                        from GlobalTmpTripProcessor_GFI_GPS_GPSData
                                    where DateTimeGPS_UTC < v_DateTimeGPS_UTC
                                         and StopFlag > 100  
                                         and LongLatValidFlag = 1 
                                         and AssetID = v_MyAssetID;
                        
                                    select min(DateTimeGPS_UTC) into v_tmpDate2 
                                        from GlobalTmpTripProcessor_GFI_GPS_GPSData 
                                    where AssetID = v_MyAssetID 
                                        and LongLatValidFlag = 1
                                        and DateTimeGPS_UTC = v_tmpDate;
                        
                                    v_StartTripUID := null;
                        
                                    if(v_tmpDate2 is not null) then
                                        select ""UID"" into v_StartTripUID 
                                            from GlobalTmpTripProcessor_GFI_GPS_GPSData 
                                        where AssetID = v_MyAssetID 
                                            and LongLatValidFlag = 1
                                            and DateTimeGPS_UTC > v_tmpDate2
                                            and RowNum = 1
                                        order by DateTimeGPS_UTC;
                            
                                        v_MaxSpeed := null;
                                        select max(speed)into v_MaxSpeed from GlobalTmpTripProcessor_GFI_GPS_GPSData where (""UID"" between v_StartTripUID and v_UID) and LongLatValidFlag = 1 and AssetID = v_MyAssetID;
                                
                                        select DateTimeGPS_UTC, Longitude, Latitude into v_StartDT, v_fStartLon, v_fStartLat from GlobalTmpTripProcessor_GFI_GPS_GPSData where ""UID"" = v_StartTripUID;
                                        select DateTimeGPS_UTC, Longitude, Latitude into v_EndDT, v_fEndLon, v_fEndLat from GlobalTmpTripProcessor_GFI_GPS_GPSData where ""UID"" = v_UID;
                                    end if;
                                    
                                    if(v_TripDistance is not null and v_TripTime is not null and v_MaxSpeed is not null and v_StartTripUID != v_UID)
                                    then
                                        v_HeaderID := v_HeaderID + 1;
                        
                                        insert into GlobalTmpTripProcessorGFI_GPS_TripHeader (iID, AssetID, DriverID, ExceptionFlag, MaxSpeed, IdlingTime, StopTime, TripDistance, TripTime, GPSDataStartUID, GPSDataEndUID, dtStart, fStartLon, fStartLat, dtEnd, fEndLon, fEndLat)
                                            values (v_HeaderID, v_AssetID, v_DriverID, 0, v_MaxSpeed, 0, 0, v_TripDistance, v_TripTime, v_StartTripUID, v_UID, v_StartDT, v_fStartLon, v_fStartLat, v_EndDT, v_fEndLon, v_fEndLat);
                            
                                        if (v_iFirstTripInserted = 0) then
                                            v_FirstTrip := v_HeaderID;
                                        end if;
                                        v_iFirstTripInserted := v_iFirstTripInserted + 1;
                                        v_LastTrip := v_HeaderID;
                        
                                        select DateTimeGPS_UTC into v_StartTripDT from GlobalTmpTripProcessor_GFI_GPS_GPSData where ""UID"" = v_StartTripUID;
                        
                                        insert into GlobalTmpTripProcessorGFI_GPS_TripDetail (HeaderiID, GPSDataUID)
                                            select v_HeaderID, ""UID"" 
                                                from GlobalTmpTripProcessor_GFI_GPS_GPSData where (DateTimeGPS_UTC between v_StartTripDT and v_DateTimeGPS_UTC) and AssetID = v_MyAssetID and LongLatValidFlag = 1
                                                order by DateTimeGPS_UTC;
                                    end if;
                                   
                                    FETCH Trip_Cursor INTO v_PreviousUID, v_AssetID, v_DriverID, v_PreviousDateTimeGPS_UTC, v_PreviousLongitude, v_PreviousLatitude, v_LongLatValidFlag, v_Speed, v_EngineOn, v_StopFlag, v_TripDistance, v_TripTime;
                                END;
                            END LOOP;
                            CLOSE Trip_Cursor;
                        END;                            
                    end;  
                    
                    --Calculating Stop Time and Idling time
                    --if(1 = 0)
                    begin
                        DBMS_OUTPUT.put_line('Calculating stop time...');
                        if(v_FirstTrip is null) then 
                            v_FirstTrip := v_LastTrip; 
                        end if;
                        DBMS_OUTPUT.put_line('First Trip ' || v_FirstTrip || ' Last Trip ' || v_LastTrip);
                        
                        v_TripID := null; 
                        v_GPSDataEndUID := null; 
                        v_GPSDataStartUID := null;
                        v_Ignore := null; 
                                                 
                        Declare Cursor Trip_Cursor is 
                            select iID, GPSDataStartUID, GPSDataEndUID from GlobalTmpTripProcessorGFI_GPS_TripHeader
                                where AssetID = v_MyAssetID
                            order by iID;
                        BEGIN 
                            OPEN Trip_Cursor;
                            LOOP 
                                FETCH Trip_Cursor INTO  v_Ignore, v_GPSDataStartUID, v_Ignore;
                                EXIT WHEN Trip_Cursor%notfound;        
                                BEGIN 
                                    if(v_GPSDataEndUID is not null) then
                                        v_StopTime := null;
                                        select DateTimeGPS_UTC into v_PreviousDateTimeGPS_UTC from GlobalTmpTripProcessor_GFI_GPS_GPSData where ""UID"" = v_GPSDataEndUID;
                                        select DateTimeGPS_UTC into v_DateTimeGPS_UTC from GlobalTmpTripProcessor_GFI_GPS_GPSData where ""UID"" = v_GPSDataStartUID;
                                        v_StopTime := DATEDIFF('second', v_PreviousDateTimeGPS_UTC, v_DateTimeGPS_UTC);
                            
                                        --Calculating Idling time
                                        --Segregate speed <= 5 per TripHeader
                                        --Group above records, calculating min and max duration
                                        --Sum Total Duration
                                        --if(1=0)
                                        begin
                                            v_IdlingDuration := 5;
                                            v_IdlingTime := 0;
                                            
                                            --Get All trip details with Next record id
                                            insert into GlobalTmpTripProcessor_trip
                                                WITH CTE AS
                                                (	
                                                    SELECT g.""UID"", g.DateTimeGPS_UTC, g.Speed, ROW_NUMBER() OVER (ORDER BY g.""UID"") as Row_Num
                                                        from GlobalTmpTripProcessorGFI_GPS_TripDetail d
                                                    inner join GlobalTmpTripProcessor_GFI_GPS_GPSData g on d.GPSDataUID = g.""UID""
                                                        where d.HeaderiID = v_TripID
                                                )
                                                select cte.* FROM CTE;
                        
                                            --Get only idle details with Next record id
                                            insert into GlobalTmpTripProcessor_idles
                                                WITH CTE AS
                                                (	
                                                    SELECT t.*, ROW_NUMBER() OVER (ORDER BY t.""UID"") as IdleRow_Num
                                                    from GlobalTmpTripProcessor_trip t where speed < v_IdlingDuration
                                                )
                                                SELECT t.*
                                                    , prev.""UID"" PreviousIdleValue
                                                    , nex.""UID"" NextIdleValue
                                                    , 0 GrpID
                                                FROM GlobalTmpTripProcessor_trip t
                                                    left outer join CTE on t.""UID"" = CTE.""UID""
                                                    LEFT JOIN CTE prev ON prev.Row_Num = CTE.Row_Num - 1
                                                    LEFT JOIN CTE nex ON nex.Row_Num = CTE.Row_Num + 1;
                                                
                                            update GlobalTmpTripProcessor_idles set NextIdleValue = PreviousIdleValue where NextIdleValue is null;
                                            /*
                                            update GlobalTmpTripProcessor_idles i
                                            set i.GrpID =
                                            (
                                                select T.GrpID
                                                from GlobalTmpTripProcessor_idles T
                                                    inner join (select tt.*, (select count(1) from GlobalTmpTripProcessor_idles where NextIdleValue is null and Row_Num <= tt.Row_Num) grp from GlobalTmpTripProcessor_idles tt) R
                                                    on R.Row_Num = T.Row_Num
                                            )
                                            */
                                            MERGE INTO GlobalTmpTripProcessor_idles T
                                            USING 
                                            (
                                                SELECT T.ROWID row_id, R.Grp
                                                    from GlobalTmpTripProcessor_idles T
                                                inner join (select tt.*, (select count(1) from GlobalTmpTripProcessor_idles where NextIdleValue is null and Row_Num <= tt.Row_Num) grp from GlobalTmpTripProcessor_idles tt) R on R.Row_Num = T.Row_Num
                                            ) src
                                            ON (T.ROWID = src.row_id)
                                            WHEN MATCHED THEN UPDATE set GrpID = src.Grp;
                                            
                                            WITH TopBottomRow as
                                            (	
                                                SELECT min(DateTimeGPS_UTC) mini, max(DateTimeGPS_UTC) maxi
                                                FROM GlobalTmpTripProcessor_idles where NextIdleValue is not null
                                                Group BY GrpID
                                            )SELECT sum(DATEDIFF('SECOND', mini, maxi)) into v_IdlingTime FROM TopBottomRow;
                        
                                            delete from GlobalTmpTripProcessor_trip;
                                            delete from GlobalTmpTripProcessor_idles;
                                            --DBMS_OUTPUT.put_line('GlobalTmpTripProcessor_idles  ...');
                                        end;
                                        if(v_StopTime < 0) then	-- This is the last Trip
                                            v_StopTime := 0;
                                        end if;
                                        --print 'TripID ' + convert(varchar(10), v_TripID)
                                        --	+ ' From ' + convert(varchar(50), v_PreviousDateTimeGPS_UTC)
                                        --	+ ' To ' + convert(varchar(50), v_DateTimeGPS_UTC)
                                        --	+ ' Stop ' + convert(varchar(50), v_StopTime)
                                        --	+ ' Idle ' + convert(varchar(50), v_IdlingTime)
                                        update GlobalTmpTripProcessorGFI_GPS_TripHeader set StopTime = v_StopTime, IdlingTime = v_IdlingTime where iID = v_TripID;
                                    end if;    
                                    FETCH Trip_Cursor INTO v_TripID, v_Ignore, v_GPSDataEndUID;
                                END;
                            END LOOP;
                            CLOSE Trip_Cursor;
                        END;                                                     
                    end;
                   
                    --Calculating ScoreCard
                    --if(1 = 0)
                    begin
                        DBMS_OUTPUT.put_line('Calculating ScoreCard...');
                        v_SpeedFr := 75;
                        v_SpeedTo := 110;
                        v_cnt := 0;
                        WHILE v_cnt < 2
                        LOOP
                            DBMS_OUTPUT.put_line('Inside FOR LOOP' || v_cnt);
                            delete from GlobalTmpTripProcessor_Speed;
                            delete from GlobalTmpTripProcessor_OuterT;
                            delete from GlobalTmpTripProcessor_AllRows;
                            delete from GlobalTmpTripProcessor_AllRowsPrepared;
                        
                            if(v_cnt = 1)
                            then
                                v_SpeedFr := 70; --110;
                                v_SpeedTo := 999;
                            end if;
                        
                            --Adding NextID field for outer table
                            insert into GlobalTmpTripProcessor_OuterT
                                WITH CTE AS
                                (
                                    SELECT * from GlobalTmpTripProcessor_GFI_GPS_GPSData	
                                )SELECT t.*
                                        , prev.""UID"" PreviousOuterValue
                                        , nex.""UID"" NextOuterValue
                                        , 0 GrpID
                                    FROM GlobalTmpTripProcessor_GFI_GPS_GPSData t
                                        left outer join CTE on t.""UID"" = CTE.""UID""
                                        LEFT JOIN CTE prev ON prev.Row_Number = CTE.Row_Number - 1
                                        LEFT JOIN CTE nex ON nex.Row_Number = CTE.Row_Number + 1
                                    order by t.dateTimeGPS_UTC;
                                
                            --Adding NextID field for inner table
                            insert into GlobalTmpTripProcessor_AllRows
                                WITH CTE AS
                                (	
                                    SELECT x.*, ROW_NUMBER() OVER (ORDER BY DateTimeGPS_UTC) as InnerRow_Num from GlobalTmpTripProcessor_GFI_GPS_GPSData x where Speed >= v_SpeedFr and Speed < v_SpeedTo           
                                )SELECT t.*, CTE.InnerRow_Num
                                    FROM GlobalTmpTripProcessor_OuterT t
                                        left outer join CTE on t.""UID"" = CTE.""UID"";
                        
                            --Merging outer and inner field
                            insert into GlobalTmpTripProcessor_AllRowsPrepared
                                WITH CTE AS
                                (	
                                    SELECT * from GlobalTmpTripProcessor_AllRows
                                )SELECT t.*
                                    , prev.""UID"" PreviousInnerValue
                                    , nex.""UID"" NextInnerValue
                                FROM GlobalTmpTripProcessor_AllRows t
                                    left outer join CTE on t.""UID"" = CTE.""UID""
                                    LEFT JOIN CTE prev ON prev.InnerRow_Num = CTE.InnerRow_Num - 1
                                    LEFT JOIN CTE nex ON nex.InnerRow_Num = CTE.InnerRow_Num + 1
                                order by t.DateTimeGPS_UTC;
                        
                            --NextInner should = NextOuter, then this means a group. So when they are not same, update to null, then group sum of null  to get groupid
                            update GlobalTmpTripProcessor_AllRowsPrepared set NextInnerValue = null where NextInnerValue != NextOuterValue;
                            MERGE INTO GlobalTmpTripProcessor_AllRowsPrepared T
                            USING 
                            (
                                SELECT T.ROWID row_id, R.Grp
                                    from GlobalTmpTripProcessor_AllRowsPrepared T
                                inner join (select tt.*, (select count(1) from GlobalTmpTripProcessor_AllRowsPrepared where NextInnerValue is null and InnerRow_Num >= tt.InnerRow_Num) grp 
                                                from GlobalTmpTripProcessor_AllRowsPrepared tt
                                           ) R on R.""UID"" = T.""UID""
                            ) src
                            ON (T.ROWID = src.row_id)
                            WHEN MATCHED THEN UPDATE set GrpID = src.Grp;
                        
                            insert into GlobalTmpTripProcessor_Speed
                                select GrpID, min(dateTimeGPS_UTC) dtFr, max(dateTimeGPS_UTC) dtTo, 0 Durations, 0 TID, 0 SumDuration, 0 Accel, 0 Brake, 0 Corner 
                                    from GlobalTmpTripProcessor_AllRowsPrepared where GrpID > 0 group by GrpID;
                            update GlobalTmpTripProcessor_Speed set Durations = DateDiff('second', dtFr, dtTo);
                            update GlobalTmpTripProcessor_Speed set Durations = 10 where Durations = 0;
                            MERGE INTO GlobalTmpTripProcessor_Speed s
                            USING 
                            (
                                SELECT s.ROWID row_id, t.iID
                                    from GlobalTmpTripProcessor_Speed s
                                inner join GlobalTmpTripProcessorGFI_GPS_TripHeader t on t.dtStart <= s.dtFr and s.dtTo <= t.dtEnd
                                    where t.iID >= v_FirstTrip
                            ) src ON (s.ROWID = src.row_id)
                            WHEN MATCHED THEN UPDATE set TID = src.iID;    
                            MERGE INTO GlobalTmpTripProcessor_Speed s
                            USING 
                            (
                                SELECT s.ROWID row_id, r.SumDuration
                                    from GlobalTmpTripProcessor_Speed s
                                inner join (SELECT SUM(Durations) SumDuration, TID FROM GlobalTmpTripProcessor_Speed GROUP BY TID) r ON s.TID = r.TID
                            ) src ON (s.ROWID = src.row_id)
                            WHEN MATCHED THEN UPDATE set s.SumDuration = src.SumDuration;
                            --Harsh
                            if(v_cnt = 0)
                            then
                                MERGE INTO GlobalTmpTripProcessor_Speed d
                                USING
                                (
                                    SELECT D.ROWID row_id, f1.c 
                                        FROM GlobalTmpTripProcessorGFI_GPS_TripDetail d
                                    inner join 
                                        (select td.HeaderiID, COUNT(td.HeaderiID) c 
                                            from GlobalTmpTripProcessorGFI_GPS_TripDetail td 
                                                inner join GFI_GPS_GPSDataDetail g on g.""UID"" = td.GPSDataUID and g.TypeID = 'Harsh Accelleration' 
                                            Group by td.HeaderiID
                                        ) f1 on d.HeaderiID = f1.HeaderiID
                                    inner join GlobalTmpTripProcessor_Speed s on s.TID = d.HeaderiID
                                    where d.HeaderiID >= v_FirstTrip 
                                ) src ON ( D.ROWID = src.row_id )
                                WHEN MATCHED THEN UPDATE SET Accel = src.c;
                            
                                MERGE INTO GlobalTmpTripProcessor_Speed d
                                USING
                                (
                                    SELECT D.ROWID row_id, f1.c 
                                        FROM GlobalTmpTripProcessorGFI_GPS_TripDetail d
                                    inner join 
                                        (select td.HeaderiID, COUNT(td.HeaderiID) c 
                                            from GlobalTmpTripProcessorGFI_GPS_TripDetail td 
                                                inner join GFI_GPS_GPSDataDetail g on g.""UID"" = td.GPSDataUID and g.TypeID = 'Harsh Breaking' 
                                            Group by td.HeaderiID
                                        ) f1 on d.HeaderiID = f1.HeaderiID
                                    inner join GlobalTmpTripProcessor_Speed s on s.TID = d.HeaderiID
                                    where d.HeaderiID >= v_FirstTrip 
                                ) src ON ( D.ROWID = src.row_id )
                                WHEN MATCHED THEN UPDATE SET Brake = src.c;
                            
                                MERGE INTO GlobalTmpTripProcessor_Speed d
                                USING
                                (
                                    SELECT D.ROWID row_id, f1.c 
                                        FROM GlobalTmpTripProcessorGFI_GPS_TripDetail d
                                    inner join 
                                        (select td.HeaderiID, COUNT(td.HeaderiID) c 
                                            from GlobalTmpTripProcessorGFI_GPS_TripDetail td 
                                                inner join GFI_GPS_GPSDataDetail g on g.""UID"" = td.GPSDataUID and g.TypeID = 'Harsh Cornering' 
                                            Group by td.HeaderiID
                                        ) f1 on d.HeaderiID = f1.HeaderiID
                                    inner join GlobalTmpTripProcessor_Speed s on s.TID = d.HeaderiID
                                    where d.HeaderiID >= v_FirstTrip 
                                ) src ON ( D.ROWID = src.row_id )
                                WHEN MATCHED THEN UPDATE SET Corner = src.c;
                            end if;
                                                
                            if(v_cnt = 0) 
                            then
                                /*
                                MERGE INTO GlobalTmpTripProcessorGFI_GPS_TripHeader t
                                USING 
                                (
                                    SELECT t.ROWID row_id, s.SumDuration, s.Accel, s.Brake, s.Corner
                                        from GlobalTmpTripProcessorGFI_GPS_TripHeader t
                                            inner join GlobalTmpTripProcessor_Speed s on t.iID = s.TID
                                    --where t.iID >= v_FirstTrip
                                ) src ON (t.ROWID = src.row_id)
                                WHEN MATCHED THEN UPDATE set OverSpeed1Time = src.SumDuration,  Accel = src.Accel,  Brake = src.Brake,  Corner = src.Corner;
                                */
                                update GlobalTmpTripProcessorGFI_GPS_TripHeader t
                                    set t.OverSpeed1Time = (select s.SumDuration from GlobalTmpTripProcessor_Speed s where t.iID = s.TID and RowNum = 1 and t.iID >= v_FirstTrip)
                                        , t.Accel = (select s.Accel from GlobalTmpTripProcessor_Speed s where t.iID = s.TID and RowNum = 1 and t.iID >= v_FirstTrip)
                                        , t.Brake = (select s.Brake from GlobalTmpTripProcessor_Speed s where t.iID = s.TID and RowNum = 1 and t.iID >= v_FirstTrip)
                                        , t.Corner = (select s.Corner from GlobalTmpTripProcessor_Speed s where t.iID = s.TID and RowNum = 1 and t.iID >= v_FirstTrip);

                                update GlobalTmpTripProcessorGFI_GPS_TripHeader set Accel = 0 where Accel is null;
                                update GlobalTmpTripProcessorGFI_GPS_TripHeader set Brake = 0 where Brake is null;
                                update GlobalTmpTripProcessorGFI_GPS_TripHeader set Corner = 0 where Corner is null;
                                    
                            else if(v_cnt = 1)
                                then
                                    update GlobalTmpTripProcessorGFI_GPS_TripHeader t
                                        set t.OverSpeed2Time = (select s.SumDuration from GlobalTmpTripProcessor_Speed s where t.iID = s.TID and RowNum = 1 and t.iID >= v_FirstTrip);                                        
                                end if;
                            end if;
                        
                            v_cnt := v_cnt + 1;
                        END LOOP;
                    end;

                    --updating sql tables from tmp tables
                    begin
                        DBMS_OUTPUT.put_line('updating sql tables from tmp tables...');
                        delete from GFI_GPS_TripHeader where AssetID = v_MyAssetID and dtStart >=  v_DelDate;
                        
                        v_TripiID := null;
                        v_TripiID := GFI_GPS_TRIPHEADER_SEQ.NEXTVAL;
                        update GlobalTmpTripProcessorGFI_GPS_TripHeader set iID = iID + v_TripiID;
                        update GlobalTmpTripProcessorGFI_GPS_TripDetail set HeaderiID = HeaderiID + v_TripiID;
                        
                        insert into GFI_GPS_TripHeader (AssetID, DriverID, ExceptionFlag, MaxSpeed, IdlingTime, StopTime, TripDistance, TripTime, GPSDataStartUID, GPSDataEndUID, dtStart, fStartLon, fStartLat, dtEnd, fEndLon, fEndLat, OverSpeed1Time, OverSpeed2Time, Accel, Brake, Corner) 
                            select AssetID, DriverID, ExceptionFlag, MaxSpeed, IdlingTime, StopTime, TripDistance, TripTime, GPSDataStartUID, GPSDataEndUID, dtStart, fStartLon, fStartLat, dtEnd, fEndLon, fEndLat, OverSpeed1Time, OverSpeed2Time, Accel, Brake, Corner from GlobalTmpTripProcessorGFI_GPS_TripHeader;
                        
                        insert into GFI_GPS_TripDetail (HeaderiID, GPSDataUID)
                            select HeaderiID, GPSDataUID from GlobalTmpTripProcessorGFI_GPS_TripDetail;
                        
                        MERGE INTO GFI_GPS_GPSData t
                        USING 
                        (
                            SELECT t.ROWID row_id, t1.StopFlag, t1.TripDistance, t1.TripTime
                                from GlobalTmpTripProcessor_GFI_GPS_GPSData t1
                            inner join GFI_GPS_GPSData t on t.""UID"" = t1.""UID""
                        ) src
                        ON (t.ROWID = src.row_id)
                        WHEN MATCHED THEN UPDATE set StopFlag = src.StopFlag,  TripDistance = src.TripDistance,  TripTime = src.TripTime;
                        
                        --LastTrip
                        MERGE INTO GFI_GPS_TripHeader T
                        USING 
                        (
                            SELECT T.ROWID row_id, DATEDIFF('second', T.dtEnd, (select min(dtStart) from GFI_GPS_TripHeader where AssetID = r.AssetID and dtStart > r.dtStart)) as StopTime
                                from GFI_GPS_TripHeader T 
                                    inner join (select t.* from GFI_GPS_TripHeader t where StopTime is null and AssetID = v_MyAssetID) R on R.iID = T.iID
                        ) src ON (T.ROWID = src.row_id)
                        WHEN MATCHED THEN UPDATE set StopTime = src.StopTime;
                        
                        delete from GFI_GPS_ProcessPending where AssetID = v_MyAssetID and ProcessCode = 'Trips' and Processing = 1;
                    end;
                END;
            END LOOP;
            CLOSE ProcessCursor;
        END;           
        commit;
    end;
EXCEPTION
    WHEN OTHERS THEN
        Declare v_errMsg NVARCHAR2 (200) := SQLERRM; 
                v_err_code NVARCHAR2 (50);
        BEGIN
            v_errMsg := SUBSTR(SQLERRM, 1, 200);
            v_err_code := SQLCODE; --SUBSTR(SQLCODE, 1, 50);
            ROLLBACK;              
            BEGIN                 
                BEGIN
                    INSERT INTO GFI_GPS_ProcessErrors (sError, sOrigine, sFieldValue)
                        VALUES (v_errMsg, 'Trips', v_err_code );             
                END;
            EXCEPTION
                WHEN OTHERS THEN dbms_output.put_line(v_errMsg);
            END;   
        END;
END;
";
            sql += "\r\n--AlreadyOracleStatement";
            return sql;
        }
		public static String sPostgreSQLTrips(int iAssetID)
		{
			String sql = @"
--Trips Processor
--Asset : t_ActualAssetID := 1;
--To resolve, just remove this line or change this line to SET NOCOUNT OFF; and everything works fine.
-- All Stop > 100
--	Stop ign off 111
--	Stop by Time 122
-- Trips < 100
--	New Trips < 50
--	New Trip 10
--	New Trip by Time 30
--	In Trip 60
--From CM
--update GFI_GPS_GPSData set StopFlag = 125 where StopFlag = 0
--update GFI_GPS_GPSData set StopFlag = 126 where StopFlag = 1
do $$

--Working table.
begin
	create TEMPORARY sequence _GFI_GPS_GPSData_seq;
	create TEMPORARY table _GFI_GPS_GPSData 
	(
		RowNumber int DEFAULT NEXTVAL ('_GFI_GPS_GPSData_seq') NOT NULL,
		UID int PRIMARY KEY,
		AssetID int NULL,
		DateTimeGPS_UTC Timestamp NULL,
		DateTimeServer Timestamp NULL,
		Longitude Double precision NULL,
		Latitude Double precision NULL,
		LongLatValidFlag int NULL,
		Speed Double precision NULL,
		EngineOn int NULL,
		StopFlag int NULL,
		TripDistance Double precision NULL,
		TripTime Double precision NULL,
		WorkHour int NULL,
		DriverID int NOT NULL,
        Misc1 varchar(100) null,
		Misc2 varchar(100) null,
		Misc3 varchar(100) null
	);
	CREATE INDEX _myIndex ON _GFI_GPS_GPSData 
	(
		AssetID ASC,
		DateTimeGPS_UTC ASC,
		LongLatValidFlag ASC,
		StopFlag ASC,
		Speed ASC
	);

	CREATE TEMPORARY TABLE _GFI_GPS_TripHeader
	(
		iID int  NOT NULL PRIMARY KEY,
		AssetID int NOT NULL,
		DriverID int NOT NULL,
		ExceptionFlag int NULL,
		MaxSpeed int NULL,
		IdlingTime Double precision NULL,
		StopTime Double precision NULL,
		TripDistance Double precision NULL,
		TripTime Double precision NULL,
		GPSDataStartUID int NULL,
		GPSDataEndUID int NULL,
		dtStart Timestamp NULL,
		fStartLon Double precision NULL,
		fStartLat Double precision NULL,
		dtEnd Timestamp NULL,
		fEndLon Double precision NULL,
		fEndLat Double precision NULL,
		OverSpeed1Time Double precision Default 0 NOT NULL,
		OverSpeed2Time Double precision Default 0 NOT NULL,
		Accel int Default 0 NOT NULL,
		Brake int Default 0 NOT NULL,
		Corner int Default 0 NOT NULL
	 );

	create TEMPORARY sequence _GFI_GPS_TripDetail_seq;
	create TEMPORARY table _GFI_GPS_TripDetail
	(
		RowNumber int DEFAULT NEXTVAL ('_GFI_GPS_TripDetail_seq') PRIMARY KEY NOT NULL,
		HeaderiID int,
		GPSDataUID int
	);

	declare t_ActualAssetID int;
	Declare t_TripDistance double precision;
	Declare t_TripTime double precision;
	Declare t_NewTrip int;
	Declare t_VeryFirstTripID int;
	declare t_PrevCalcTripDistance double precision;
	declare t_PrevCalcTripTime double precision;
	declare t_FloatIgnore double precision;
	declare t_tmpDate timestamp;
	declare t_tmpDate2 timestamp;
	declare t_DelDate timestamp;
	Declare t_StartProcessDate timestamp;
	Declare t_HeaderID int;
	declare t_TripID int;
	declare t_GPSDataEndUID int; 
	declare t_GPSDataStartUID int; 
	declare t_Ignore int;
	Declare t_StopFlag int;
	Declare t_DriverID int;
	declare t_FirstTrip int;
	declare t_LastTrip int;
	Declare t_iFirstTripInserted int;
	Declare t_StartTripUID int;
	Declare t_MaxSpeed int;
	Declare t_StartTripDT timestamp;
	Declare t_StopTime int;
	Declare t_IdlingDuration int;
	Declare t_IdlingTime int;
	DECLARE t_cnt INT;
	DECLARE t_SpeedFr INT;
	DECLARE t_SpeedTo INT;
	Declare t_TripiID int;
	Declare t_RecStart record;
	Declare t_RecEnd record;
	Declare t_LastGPSiID int;
	
	declare total_rows int;
	declare _maxRows int;
	
	begin
		t_ActualAssetID := 1;
		_maxRows := 1000;

		delete from GFI_GPS_ProcessPending where ProcessCode = 'TripsIgnored';
		delete from GFI_GPS_ProcessPending where ProcessCode = 'ExceptionsIgnored';

		update GFI_GPS_ProcessPending set Processing = 1
			where ProcessCode = 'Trips' and AssetID = t_ActualAssetID
				and Processing = 0;

		Declare	ProcessRec RECORD;
		DECLARE ProcessCursor CURSOR FOR  
			SELECT GFI_GPS_ProcessPending.assetid, MIN(dtdatefrom) MinDate
				FROM GFI_GPS_ProcessPending where ProcessCode = 'Trips' and Processing = 1 and AssetID = t_ActualAssetID
			GROUP BY GFI_GPS_ProcessPending.assetid;

		begin
			open ProcessCursor;
				LOOP
					fetch ProcessCursor into ProcessRec;
					EXIT WHEN NOT FOUND;
						RAISE NOTICE 'MyAssetID % @ %', ProcessRec.AssetID ,ProcessRec.MinDate;
					begin	
						t_TripDistance = null;
						t_TripTime = null;
						t_NewTrip = 10;
						t_VeryFirstTripID = null;
						t_PrevCalcTripDistance = null;
						t_PrevCalcTripTime = null;

						--Preparing data in GFI_GPS_GPSData. 
						--Trip_Cursor is a temp cursor, used many times below
						--Getting data as from end of last trip. this 1st row will be skipped in t_NewTrip = 10
						--	begin
						--Get 1st record prior to the actual one
						t_tmpDate := (select DateTimeGPS_UTC from GFI_GPS_GPSData
											where DateTimeGPS_UTC < ProcessRec.MinDate 
												and AssetID = ProcessRec.AssetID and LongLatValidFlag = 1
												order by DateTimeGPS_UTC desc limit 1
										);

						--Get 1st stop prior to the result
						t_tmpDate2 := (select MAX(DateTimeGPS_UTC) 
											from GFI_GPS_GPSData
											where AssetID = ProcessRec.AssetID 
												and LongLatValidFlag = 1 
												and stopFlag > 100 --and stopFlag != 133 --and cmd added to test cut trips in middle. insert tripdetail using t_AssetID added same time as this and cmd
												and DateTimeGPS_UTC < t_tmpDate 
										);
				
						--If t_tmpDate2 is null for x reason
						if(t_tmpDate2 is null)
						then
							t_tmpDate2 := (select MAX(DateTimeGPS_UTC) 
												from GFI_GPS_GPSData
												where AssetID = ProcessRec.AssetID 
													and LongLatValidFlag = 1 
													and stopFlag > 100 and stopFlag != 133
													and DateTimeGPS_UTC < ProcessRec.MinDate 
											);
						end if;

						t_tmpDate2 := coalesce(t_tmpDate2, ProcessRec.MinDate);
						delete from _GFI_GPS_GPSData;
						delete from _GFI_GPS_TripDetail;

						insert into _GFI_GPS_GPSData (UID, AssetID, DateTimeGPS_UTC, DateTimeServer, Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, StopFlag, TripDistance, TripTime, WorkHour, DriverID)
							select UID, AssetID, DateTimeGPS_UTC, DateTimeServer, Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, StopFlag, TripDistance, TripTime, WorkHour, DriverID from GFI_GPS_GPSData
								where AssetID = ProcessRec.AssetID 
									and DateTimeGPS_UTC >= t_tmpDate2
							ORDER BY DateTimeGPS_UTC limit _maxRows;

						perform * from _GFI_GPS_GPSData;
						GET DIAGNOSTICS total_rows := ROW_COUNT;
						if(total_rows >= _maxRows - 1) then
							insert into GFI_GPS_ProcessPending (AssetID, dtdatefrom, ProcessCode) 
								values (ProcessRec.AssetID, (select max(DateTimeGPS_UTC) + interval '1 second' from _GFI_GPS_GPSData ), 'Trips');
						end if;

						--Cursor Starts
						t_StartProcessDate := null;
						Declare	p_TripRec RECORD;
						Declare	TripRec RECORD;
						DECLARE Trip_Cursor CURSOR FOR  
							select UID, AssetID, DateTimeGPS_UTC, Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, TripDistance, TripTime 
								from _GFI_GPS_GPSData 
								where AssetID = ProcessRec.AssetID 
									and LongLatValidFlag = 1
								order by DateTimeGPS_UTC;

						begin
							open Trip_Cursor;
								LOOP
									fetch Trip_Cursor into TripRec;
									EXIT WHEN NOT FOUND;

									t_VeryFirstTripID := TripRec.UID;
									t_StartProcessDate := TripRec.DateTimeGPS_UTC;

									FETCH PRIOR FROM Trip_Cursor INTO p_TripRec;
									FETCH NEXT FROM Trip_Cursor INTO TripRec;

									--In case this is a new batch. so t_PreviousUID can also be null. skipping 1st row. end of last trip. So new Trip starting
									if(t_NewTrip = 10)	then
										begin
											t_NewTrip := 1;
											t_TripDistance := 0;
											t_TripTime := 0;
										end;
									elseif (TripRec.EngineOn = 0) then	--Ign off detected
										begin
											begin
												if(p_TripRec.Latitude != TripRec.Latitude and p_TripRec.Longitude != TripRec.Longitude) then
													t_TripDistance := t_TripDistance + ACOS(SIN(PI()*p_TripRec.Latitude/180.0)*SIN(PI()*TripRec.Latitude/180.0)+COS(PI()*p_TripRec.Latitude/180.0)*COS(PI()*TripRec.Latitude/180.0)*COS(PI()*TripRec.Longitude/180.0-PI()*p_TripRec.Longitude/180.0))*6371;
												end if;
												t_TripTime := t_TripTime + DATEDIFF('second', p_TripRec.DateTimeGPS_UTC::timestamp, TripRec.DateTimeGPS_UTC::timestamp);
											Exception
												WHEN OTHERS THEN raise notice 'Error 1 % and %', SQLERRM,  TripRec.UID;
												-- continue
											End;

											update _GFI_GPS_GPSData set StopFlag = 111, TripDistance = t_TripDistance, TripTime = t_TripTime where UID = TripRec.UID;
											t_TripDistance := 0;
											t_TripTime := 0;
											t_NewTrip := 1;
										end;
									elseif(t_NewTrip = 1) then --New Trip
										begin
											update _GFI_GPS_GPSData set StopFlag = 10, TripDistance = t_TripDistance, TripTime = t_TripTime where UID = TripRec.UID;
											t_TripDistance := 0; 
											t_NewTrip := 0;
										end;
									elseif (DATEDIFF('second', p_TripRec.DateTimeGPS_UTC, TripRec.DateTimeGPS_UTC) <= 200)	then --Existing Trip on Time
										begin
											begin
												if(p_TripRec.Latitude != TripRec.Latitude and p_TripRec.Longitude != TripRec.Longitude) then
													t_TripDistance := t_TripDistance + ACOS(SIN(PI()*p_TripRec.Latitude/180.0)*SIN(PI()*TripRec.Latitude/180.0)+COS(PI()*p_TripRec.Latitude/180.0)*COS(PI()*TripRec.Latitude/180.0)*COS(PI()*TripRec.Longitude/180.0-PI()*p_TripRec.Longitude/180.0))*6371;
												end if;
												t_TripTime := t_TripTime + DATEDIFF('second', p_TripRec.DateTimeGPS_UTC, TripRec.DateTimeGPS_UTC);
											EXCEPTION
												WHEN OTHERS THEN raise notice 'Error 2 % and %', SQLERRM,  TripRec.UID;
												-- continue
											end;
						
											update _GFI_GPS_GPSData set StopFlag = 60, TripDistance = t_TripDistance, TripTime = t_TripTime where UID = TripRec.UID;
											t_NewTrip := 0;
										end;
									else
										begin
											--New Trip. All new trip start with 30. Stop by Time 122
											update _GFI_GPS_GPSData set StopFlag = 122, TripDistance = t_TripDistance, TripTime = t_TripTime where UID = p_TripRec.UID;
											t_TripTime := 0;
											t_TripDistance := 0;
						
											--no need for t_NewTrip = 1, as both previous and actual rows are being updated
											update _GFI_GPS_GPSData set StopFlag = 30, TripDistance = t_TripDistance, TripTime = t_TripTime where UID = TripRec.UID;
											t_NewTrip := 0; 
										end;
									end if;
									t_LastGPSiID := TripRec.UID;
								END LOOP;
								--Updating last record
								update _GFI_GPS_GPSData set StopFlag = 133 where UID = t_LastGPSiID;
							close Trip_Cursor;
							--DEALLOCATE Trip_Cursor;
						end; 
						--Cursor2 ends

						--Building Trips
						begin
							RAISE NOTICE 'Building Trips...';

							--delete from GFI_GPS_TripHeader where AssetID = ProcessRec.MyAssetID and dtStart >=  t_tmpDate2
							t_DelDate := t_tmpDate2;
							delete from _GFI_GPS_TripHeader;
							t_HeaderID := 0;

							t_StopFlag := null;
							t_DriverID := null;
							t_FirstTrip := null;
							t_LastTrip := null;
							t_iFirstTripInserted := 0;
							--Cursor2 Starts

							Declare	p_TripRec RECORD;
							Declare	TripRec RECORD;
							DECLARE Trip_Cursor CURSOR FOR  
								SELECT UID, AssetID, DriverID , DateTimeGPS_UTC, Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, StopFlag, TripDistance, TripTime
									FROM _GFI_GPS_GPSData 
								where AssetID = ProcessRec.AssetID and StopFlag > 100 and LongLatValidFlag = 1
									order by DateTimeGPS_UTC;

							begin
								open Trip_Cursor;
									LOOP
										fetch NEXT FROM Trip_Cursor into TripRec;
										EXIT WHEN NOT FOUND;

										FETCH PRIOR FROM Trip_Cursor INTO p_TripRec;
										FETCH NEXT FROM Trip_Cursor INTO TripRec;

										t_tmpDate := (select max(DateTimeGPS_UTC) from _GFI_GPS_GPSData
															where DateTimeGPS_UTC < TripRec.DateTimeGPS_UTC
																and StopFlag > 100  
																and LongLatValidFlag = 1 
																and AssetID = ProcessRec.AssetID
														);

										t_tmpDate2 := (select min(DateTimeGPS_UTC) from _GFI_GPS_GPSData 
															where AssetID = ProcessRec.AssetID 
																and LongLatValidFlag = 1
																and DateTimeGPS_UTC = t_tmpDate
														);

										t_StartTripUID := null;

										t_StartTripUID := (select UID from _GFI_GPS_GPSData 
																where AssetID = ProcessRec.AssetID 
																	and LongLatValidFlag = 1
																	and DateTimeGPS_UTC > t_tmpDate2
																order by DateTimeGPS_UTC limit 1
															);

										t_MaxSpeed := null;
										t_MaxSpeed := (select max(speed) from _GFI_GPS_GPSData where (UID between t_StartTripUID and TripRec.UID) and LongLatValidFlag = 1 and AssetID = ProcessRec.AssetID);

										select DateTimeGPS_UTC, Longitude, Latitude from _GFI_GPS_GPSData into t_RecStart where UID = t_StartTripUID;
										select DateTimeGPS_UTC, Longitude, Latitude from _GFI_GPS_GPSData into t_RecEnd where UID = TripRec.UID;

										if(t_TripDistance is not null and t_TripTime is not null and t_MaxSpeed is not null and t_StartTripUID != TripRec.UID) then
											t_HeaderID := t_HeaderID + 1;

											insert into _GFI_GPS_TripHeader (iID, AssetID, DriverID, ExceptionFlag, MaxSpeed, IdlingTime, StopTime, TripDistance, TripTime, GPSDataStartUID, GPSDataEndUID, dtStart, fStartLon, fStartLat, dtEnd, fEndLon, fEndLat)
												values (t_HeaderID, TripRec.AssetID, TripRec.DriverID, 0, t_MaxSpeed, 0, 0, TripRec.TripDistance, TripRec.TripTime, t_StartTripUID, TripRec.UID, t_RecStart.DateTimeGPS_UTC, t_RecStart.Longitude, t_RecStart.Latitude, t_RecEnd.DateTimeGPS_UTC, t_RecEnd.Longitude, t_RecEnd.Latitude);
											
											RAISE NOTICE 'inserting Trips... %', t_HeaderID;

											if (t_iFirstTripInserted = 0) then
												t_FirstTrip := t_HeaderID;
											end if;

											t_iFirstTripInserted := t_iFirstTripInserted + 1;
											t_LastTrip := t_HeaderID;

											t_StartTripDT := (select DateTimeGPS_UTC from _GFI_GPS_GPSData where UID = t_StartTripUID);

											insert into _GFI_GPS_TripDetail (HeaderiID, GPSDataUID)
												select t_HeaderID, UID 
													from _GFI_GPS_GPSData where (DateTimeGPS_UTC between t_StartTripDT and TripRec.DateTimeGPS_UTC) and AssetID = ProcessRec.AssetID and LongLatValidFlag = 1
													order by DateTimeGPS_UTC;
										end if;
									END LOOP;
								close Trip_Cursor;
								--DEALLOCATE Trip_Cursor;
							end; 
						end;

						--Calculating Stop Time and Idling time
						--if(1 = 0)
						begin
							raise notice 'Calculating Stop Time';
							if(t_FirstTrip is null) then
								t_FirstTrip := t_LastTrip;
							end if;
							raise notice 'First Trip %', t_FirstTrip;
							raise notice 'Last Trip %', t_LastTrip;
		
							t_TripID := null;
							t_GPSDataEndUID := null; 
							t_GPSDataStartUID := null; 
							t_Ignore := null;

							Declare	p_TripRec RECORD;
							Declare	TripRec RECORD;
							DECLARE Trip_Cursor CURSOR FOR  
								select iID, GPSDataStartUID, GPSDataEndUID from _GFI_GPS_TripHeader
									where AssetID = ProcessRec.AssetID --and iID >= t_FirstTrip and iID <= t_LastTrip
									order by iID;

							begin
								open Trip_Cursor;
									LOOP
										fetch next from Trip_Cursor into TripRec;
										EXIT WHEN NOT FOUND;

										FETCH NEXT FROM Trip_Cursor INTO TripRec;
										FETCH PRIOR FROM Trip_Cursor INTO p_TripRec;

										t_StopTime := null;
										t_StopTime := DATEDIFF('second', (select DateTimeGPS_UTC from _GFI_GPS_GPSData where UID = p_TripRec.GPSDataEndUID), (select DateTimeGPS_UTC from _GFI_GPS_GPSData where UID = TripRec.GPSDataStartUID));
										--Calculating Idling time
										--Segregate speed <= 5 per TripHeader
										--Group above records, calculating min and max duration
										--Sum Total Duration
										--if(1=0)
										begin
											t_IdlingDuration := 5;
											t_IdlingTime := 0;

											--Get All trip details with Next record id
											CREATE TEMP TABLE _trip AS
											WITH CTE AS 
													(
														SELECT g.UID, g.DateTimeGPS_UTC, g.Speed, ROW_NUMBER() OVER (ORDER BY g.UID) as rownum
														from _GFI_GPS_TripDetail d
															inner join _GFI_GPS_GPSData g on d.GPSDataUID = g.UID
														where d.HeaderiID = p_TripRec.iID 
													)
											SELECT cte.* FROM CTE;

											--Get only idle details with Next record id
											CREATE TEMP TABLE _idles AS
											WITH CTE AS 
													(
														SELECT *, ROW_NUMBER() OVER (ORDER BY UID) as IdleRowNum
														from _trip where speed < t_IdlingDuration
													)
											SELECT t.*
												, prev.UID PreviousIdleValue
												, nex.UID NextIdleValue
												, 0 GrpID
											FROM _trip t
												left outer join CTE on t.UID = CTE.UID
												LEFT JOIN CTE prev ON prev.rownum = CTE.rownum - 1
												LEFT JOIN CTE nex ON nex.rownum = CTE.rownum + 1;

											update _idles set NextIdleValue = PreviousIdleValue where NextIdleValue is null;
											update _idles set GrpID = R.Grp
												from (select *, (select count(1) from _idles where NextIdleValue is null and RowNum <= tt.RowNum) grp from _idles tt) R
													where R.RowNum = _idles.RowNum;

											t_IdlingTime := (
																WITH TopBottomRow
																AS
																(
																	SELECT min(DateTimeGPS_UTC) mini, max(DateTimeGPS_UTC) maxi
																		FROM _idles where NextIdleValue is not null
																		Group BY GrpID
																)
																SELECT sum(DATEDIFF('second', mini, maxi))  
																	FROM TopBottomRow
															);

											drop table _trip;
											drop table _idles;
										end;
					
										if(t_StopTime < 0)	then-- This is the last Trip
											t_StopTime = 0;
										end if;
										update _GFI_GPS_TripHeader set StopTime = t_StopTime, IdlingTime = t_IdlingTime where iID = p_TripRec.iID;
									END LOOP;
								close Trip_Cursor;
								--DEALLOCATE Trip_Cursor;
							end; 
						end;

						--Calculating ScoreCard
						--if(1 = 0)
						begin
							t_cnt := 0;
							t_SpeedFr := 75;
							t_SpeedTo := 110;

							LOOP
								BEGIN
									--PRINT 'Inside FOR LOOP';
									drop table if exists _Speed;
									drop table if exists _OuterT;
									drop table if exists _AllRows;
									drop table if exists _AllRowsPrepared;
									drop table if exists _tmp1;

									if(t_cnt = 1)
									then
										t_SpeedFr := 70; --110;
										t_SpeedTo := 999;
									end if;

									--Adding NextID field for outer table
									CREATE TEMP TABLE _OuterT AS
										WITH CTE AS 
											(
												SELECT * from _GFI_GPS_GPSData 
											)
										SELECT t.*
											, prev.UID PreviousOuterValue
											, nex.UID NextOuterValue
											, 0 GrpID
										FROM _GFI_GPS_GPSData t
											left outer join CTE on t.UID = CTE.UID
											LEFT JOIN CTE prev ON prev.RowNumber = CTE.RowNumber - 1
											LEFT JOIN CTE nex ON nex.RowNumber = CTE.RowNumber + 1
										order by dateTimeGPS_UTC;

									--Adding NextID field for inner table
									CREATE TEMP TABLE _AllRows AS
										WITH CTE AS 
											(
												SELECT *, ROW_NUMBER() OVER (ORDER BY DateTimeGPS_UTC) as InnerRowNum from _GFI_GPS_GPSData where Speed >= t_SpeedFr and Speed < t_SpeedTo
											)
										SELECT t.*, CTE.InnerRowNum
										FROM _OuterT t
											left outer join CTE on t.UID = CTE.UID;

									--Merging outer and inner field
									CREATE TEMP TABLE _AllRowsPrepared AS
										WITH CTE AS 
											(
												SELECT * from _AllRows
											)
										SELECT t.*
											, prev.UID PreviousInnerValue
											, nex.UID NextInnerValue
										FROM _AllRows t
											left outer join CTE on t.UID = CTE.UID 
											LEFT JOIN CTE prev ON prev.InnerRowNum = CTE.InnerRowNum - 1
											LEFT JOIN CTE nex ON nex.InnerRowNum = CTE.InnerRowNum + 1
										order by DateTimeGPS_UTC;
									
									--COPY (select * from _AllRowsPrepared) To 'c:/Reza/tmp/output.csv';
									--NextInner should = NextOuter, then this means a group. So when they are not same, update to null, then group sum of null  to get groupid
									update _AllRowsPrepared set NextInnerValue = null where NextInnerValue != NextOuterValue;

									CREATE TEMP TABLE _tmp1 AS(
										select *
											from _AllRowsPrepared T
												inner join (select UID as tUID, (select count(1) from _AllRowsPrepared where NextInnerValue is null and InnerRowNum >= tt.InnerRowNum) grp from _AllRowsPrepared tt) R
												on R.tUID = T.uid);

									update _AllRowsPrepared set GrpID = R.Grp
										from _tmp1 R where R.uid = _AllRowsPrepared.uid;

									CREATE TEMP TABLE _Speed AS
										select GrpID, min(dateTimeGPS_UTC) dtFr, max(dateTimeGPS_UTC) dtTo, 0 Duration, 0 TID, 0 SumDuration, 0 Accel, 0 Brake, 0 Corner 
											from _AllRowsPrepared where GrpID > 0 group by GrpID;
									update _Speed set Duration = DateDiff('second', dtFr, dtTo);
									update _Speed set Duration = 10 where Duration = 0;
									update _Speed set TID = t.iID
										from _GFI_GPS_TripHeader t 
											where t.dtStart <= _Speed.dtFr and _Speed.dtTo <= t.dtEnd
												and t.iID >= t_FirstTrip;

									update _Speed
										set SumDuration = r.SumDuration
									from (SELECT SUM(Duration) SumDuration, TID FROM _Speed GROUP BY TID) r where _Speed.TID = r.TID;
										
									--Harsh
									if(t_cnt = 0)
									then
										update _Speed set Accel = f1.c 
											from _GFI_GPS_TripDetail d
												inner join 
													(select td.HeaderiID, COUNT(td.HeaderiID) c 
														from _GFI_GPS_TripDetail td 
															inner join GFI_GPS_GPSDataDetail g on g.UID = td.GPSDataUID and g.TypeID = 'Harsh Accelleration' 
														Group by td.HeaderiID
													) f1 on d.HeaderiID = f1.HeaderiID
											where _Speed.TID = d.HeaderiID and d.HeaderiID >= t_FirstTrip;

										update _Speed set Brake = f1.c 
											from _GFI_GPS_TripDetail d
												inner join 
													(select td.HeaderiID, COUNT(td.HeaderiID) c 
														from _GFI_GPS_TripDetail td 
															inner join GFI_GPS_GPSDataDetail g on g.UID = td.GPSDataUID and g.TypeID = 'Harsh Breaking' 
														Group by td.HeaderiID
													) f1 on d.HeaderiID = f1.HeaderiID
											where _Speed.TID = d.HeaderiID and d.HeaderiID >= t_FirstTrip;

										update _Speed set Corner = f1.c 
											from _GFI_GPS_TripDetail d
												inner join 
													(select td.HeaderiID, COUNT(td.HeaderiID) c 
														from _GFI_GPS_TripDetail td 
															inner join GFI_GPS_GPSDataDetail g on g.UID = td.GPSDataUID and g.TypeID = 'Harsh Cornering' 
														Group by td.HeaderiID
													) f1 on d.HeaderiID = f1.HeaderiID
											where _Speed.TID = d.HeaderiID and d.HeaderiID >= t_FirstTrip;
									end if;
										
									if(t_cnt = 0) then
										update _GFI_GPS_TripHeader set OverSpeed1Time = s.SumDuration, Accel = s.Accel, Brake = s.Brake, Corner = s.Corner
											from _Speed s where _GFI_GPS_TripHeader.iID = s.TID
												and _GFI_GPS_TripHeader.iID >= t_FirstTrip;
									elseif(t_cnt = 1) then
										update _GFI_GPS_TripHeader set OverSpeed2Time = s.SumDuration 
											from _Speed s where _GFI_GPS_TripHeader.iID = s.TID
												and _GFI_GPS_TripHeader.iID >= t_FirstTrip;
									end if;

									t_cnt := t_cnt + 1;
								   EXIT WHEN t_cnt >= 2;
								END;
							END LOOP;
						end;

						--updating sql tables from tmp tables
						begin
							raise notice 'Begin Tran %', now();
							delete from GFI_GPS_TripHeader where AssetID = ProcessRec.AssetID and dtStart >=  t_DelDate;

							t_TripiID := null;
							t_TripiID := (select last_value as CN from GFI_GPS_TripHeader_seq);
							if(t_TripiID = 1) then
								t_TripiID = 0;
							end if;
							raise notice 't_TripiID %', t_TripiID;

							update _GFI_GPS_TripHeader set iID = iID + 1000000;
							update _GFI_GPS_TripHeader set iID = iID - 1000000 + t_TripiID;
							update _GFI_GPS_TripDetail set HeaderiID = HeaderiID + t_TripiID;

							insert into GFI_GPS_TripHeader (AssetID, DriverID, ExceptionFlag, MaxSpeed, IdlingTime, StopTime, TripDistance, TripTime, GPSDataStartUID, GPSDataEndUID, dtStart, fStartLon, fStartLat, dtEnd, fEndLon, fEndLat, OverSpeed1Time, OverSpeed2Time, Accel, Brake, Corner) 
								select AssetID, DriverID, ExceptionFlag, MaxSpeed, IdlingTime, StopTime, TripDistance, TripTime, GPSDataStartUID, GPSDataEndUID, dtStart, fStartLon, fStartLat, dtEnd, fEndLon, fEndLat, OverSpeed1Time, OverSpeed2Time, Accel, Brake, Corner from _GFI_GPS_TripHeader order by iID;

							insert into GFI_GPS_TripDetail (HeaderiID, GPSDataUID)
								select HeaderiID, GPSDataUID from _GFI_GPS_TripDetail;

							update GFI_GPS_GPSData
								set StopFlag = _GFI_GPS_GPSData.StopFlag, TripDistance = _GFI_GPS_GPSData.TripDistance, TripTime = _GFI_GPS_GPSData.TripTime 
							from _GFI_GPS_GPSData
								where _GFI_GPS_GPSData.UID = GFI_GPS_GPSData.UID;

							--LastTrip
							update GFI_GPS_TripHeader T
								set StopTime = 
									(
										DATEDIFF('second', T.dtEnd, (select dtStart from GFI_GPS_TripHeader where AssetID = r.AssetID and dtStart > r.dtStart order by dtStart asc limit 1))
									)
							from (select * from GFI_GPS_TripHeader where StopTime is null and AssetID = ProcessRec.AssetID) R
								where R.iID = T.iID 
									and T.iID = (select max(iid) from _GFI_GPS_TripHeader);

							delete from GFI_GPS_ProcessPending where AssetID = ProcessRec.AssetID and ProcessCode = 'Trips' and Processing = 1;
							--raise notice 'commit Tran %', now();
							--commit;

						EXCEPTION
							WHEN OTHERS THEN
								declare errMsg VARCHAR (200);
										err_code VARCHAR (50);
								BEGIN
									errMsg := SUBSTR(SQLERRM, 1, 200);
									err_code := SUBSTR(SQLSTATE, 1, 200);
									--ROLLBACK;              
									BEGIN                 
										BEGIN
											insert into GFI_GPS_ProcessErrors (sError, sOrigine, sFieldName, sFieldValue, AssetID, dtUTC) 
												values (errMsg, 'Trips', 'None', err_code, ProcessRec.AssetID, ProcessRec.MinDate);
										END;
									EXCEPTION
										WHEN OTHERS THEN raise notice 'errMsg';
									END;   
								END;
						end;

					EXCEPTION
						WHEN OTHERS THEN
							declare errMsg VARCHAR (200);
									err_code VARCHAR (50);
							BEGIN
								errMsg := SUBSTR(SQLERRM, 1, 200);
								err_code := SUBSTR(SQLSTATE, 1, 200);
								--ROLLBACK;              
								BEGIN                 
									BEGIN
										insert into GFI_GPS_ProcessErrors (sError, sOrigine, sFieldName, sFieldValue, AssetID, dtUTC) 
											values (errMsg, 'Trips', 'None', err_code, ProcessRec.AssetID, ProcessRec.MinDate);
									END;
								EXCEPTION
									WHEN OTHERS THEN raise notice 'errMsg';
								END;   
							END;
					end;
				END LOOP;
			close ProcessCursor;
        end; 
	end;
end;
$$ LANGUAGE plpgsql;

drop table _gfi_gps_gpsdata;
drop sequence _gfi_gps_gpsdata_seq;
drop table _gfi_gps_tripheader;
drop table _gfi_gps_tripdetail;
drop sequence _gfi_gps_tripdetail_seq;

--with cte as
--(
--	SELECT datname,pid, age(clock_timestamp(), query_start), usename, query, wait_event_type
--		FROM pg_stat_activity 
--	WHERE query != '<IDLE>' 
--		AND query NOT ILIKE '%pg_stat_activity%' 
--		and age(clock_timestamp(), query_start) > '03:00:00.000000'
--		and datname = current_database()
--	ORDER BY query_start asc
--)
--SELECT pid, pg_cancel_backend(pid) from cte
--union
--SELECT pid, pg_terminate_backend(pid) from cte;

--AlreadyPostgreSQLStatement
";
			sql = sql.Replace("t_ActualAssetID := 1;", "t_ActualAssetID := " + iAssetID + ";");
			return sql; 
		}

        public static String sGetDevicesToBeProcessed(String sProcessCode)
        {
            String sql = "select distinct AssetID from GFI_GPS_ProcessPending where ProcessCode = '" + sProcessCode + "'";
            return sql;
        }

        public static String sProcessExceptions(int iAssetID)
        {
            String sql = @"
--Exceptions Processor
--Rule Types
--0   ZoneTypes
--1   Zones
--2   Assets
--3   Drivers
--4   Speed
--5   Time
--6   Auxilaries
--7   Duration
--8   ExceptionType, EngineAux, Aux
--9   Fuel
--10  RoadSpeed
--11  Day
--12  Planning
--13  Maintenance

set nocount on
begin
	--Declaring all variables
	Begin
		declare @ActualAssetID int
		declare @MyAssetID int
		declare @dtStart datetime
		declare @dtStartClone datetime
		declare @dtStartWithDuration datetime

		DECLARE	@RowNum [int] 
		DECLARE	@UID [int] 
		DECLARE	@AssetID [int] 
		DECLARE	@DateTimeGPS_UTC [datetime] 
		DECLARE	@Longitude [float] 
		DECLARE	@Latitude [float]
		DECLARE	@Speed [float] 

		--RulesCursor
		declare @ParentRuleID int
		declare @RuleID int
		declare @RuleType int
		Declare @StrucType nvarchar(50)
		Declare @StructValue nvarchar(50)
		Declare @StrucCondition nvarchar(50)
		Declare @Struc nvarchar(50)
		Declare @WhereCondition nvarchar(Max)
		Declare @TmpCondition nvarchar(Max)
		Declare @MinTriggerValue int
		DECLARE @SQLQuery AS NVARCHAR(Max)

		declare @RowCnt int
		Declare @iDuration int
		Declare @iRule int
		declare @bDetailsNeed int
		Declare @myTime nvarchar(50)
		Declare @PreviousLongitude float
		Declare @PreviousLatitude float
		Declare @NextLongitude float
		Declare @NextLatitude float
		Declare @EngineAuxCondition nvarchar(Max)
		Declare @EngineAuxStrucCondition nvarchar(Max)
		declare @MaxGrpID int
		DECLARE @ParentRuleIDTriggered int
		DECLARE @Misc1 nvarchar(100)
		DECLARE @Misc3 int

		Create table #Stopped (myRowNum int)
		declare @StopQry nvarchar(1500)
		declare @myStopRowNum int
		declare @myPreviousStopRowNum int
		Declare @InTran int

		declare @maxRows int;

		Create table #ZoneHeader (ZoneID int)
        create table #AssetFrmGMID (GMID int, GMDescription nvarchar(1000), ParentGMID int, StrucID int)
	end

	CREATE TABLE #ExTemp 
	(
		[RowNum] [int],
		[UID] [int] ,
		[ParentRuleID] [int] NULL,
		[RuleID] [int] NULL,
		[RuleType] int null,
		[StrucID] nvarchar(50),
		[Others] nvarchar(100)
	)
	Create Table #ResultTable
	(
		ParentRuleIDTriggered [int],
		[RowNumber] [int] NOT NULL,
		[UID] [int], --PRIMARY KEY CLUSTERED,
		[AssetID] [int] NULL,
		[DateTimeGPS_UTC] [datetime] NULL,
		[DateTimeServer] [datetime] NULL,
		[Longitude] [float] NULL,
		[Latitude] [float] NULL,
		[LongLatValidFlag] [int] NULL,
		[Speed] [float] NULL,
		[EngineOn] [int] NULL,
		[StopFlag] [int] NULL,
		[TripDistance] [float] NULL,
		[TripTime] [float] NULL,
		[WorkHour] [int] NULL,
		[DriverID] [int] NOT NULL,
		Misc1 nvarchar(100) null,
		Misc2 int null,
		Misc3 int null,
		RoadSpeed [float] NULL,
		Misc4 int null
        , index ix1 nonclustered (Misc1, Misc2) 
	)

	set @ActualAssetID = " + iAssetID + @"
	set @InTran = 0;
	set @maxRows = 500;

	update GFI_GPS_ProcessPending set Processing = 1
		where ProcessCode = 'Exceptions' and AssetID = @ActualAssetID
			and Processing = 0

	DECLARE ProcessCursor CURSOR FOR 
		SELECT GFI_GPS_ProcessPending.assetid, MIN(dtdatefrom) MinDate
		FROM GFI_GPS_ProcessPending where ProcessCode = 'Exceptions' and Processing = 1 and AssetID = @ActualAssetID
		GROUP BY GFI_GPS_ProcessPending.assetid
		
	--Working with 1 Asset at a time
	open ProcessCursor
		fetch next from ProcessCursor into @MyAssetID, @dtStart
		WHILE @@FETCH_STATUS = 0   
		BEGIN 
			Begin Try
				--begin tran

				--Getting 1 point before @dtStart
				begin
					set @dtStart = (
										select Coalesce(max(DateTimeGPS_UTC), @dtStart)
											from GFI_GPS_GPSData 
											where AssetID = @MyAssetID
												and DateTimeGPS_UTC < @dtStart
									)
					set @dtStartClone = @dtStart

					--if asset has a durarion rule, retrieve data from @dtStart - duration period
					DECLARE @TempRle TABLE (ParentRuleID [int] NULL)

					;WITH tree AS (		
						SELECT GMID, 
								GMDescription, 
								ParentGMID,
								1 as level 
						FROM GFI_SYS_GroupMatrix
						WHERE GMID in (SELECT m.GMID
											from GFI_SYS_GroupMatrixAsset m 
												inner join GFI_SYS_GroupMatrix gm on m.GMID = gm.GMID
												inner join GFI_FLT_Asset a (readpast) on m.iID = a.AssetID
											WHERE a.AssetID = @MyAssetID
										)
						UNION ALL 
						SELECT p.GMID,
								p.GMDescription,
								p.ParentGMID, 
								t.level + 1
						FROM GFI_SYS_GroupMatrix p
							JOIN tree t ON t.ParentGMID = p.GMID
					)
					insert @TempRle
						SELECT r.ParentRuleID		--By GMID
						FROM tree, GFI_GPS_Rules r	
						where tree.GMID = r.StrucValue and StrucType = 'GMID'
						union								
						select ParentRuleID			--By AssetID
						from GFI_GPS_Rules where StrucValue = @MyAssetID and StrucType = 'ID'

					select @iDuration = max(Try_Cast(StrucValue as int)) from @TempRle t
						inner join GFI_GPS_Rules r on t.ParentRuleID = r.ParentRuleID
					where r.Struc = 'Duration'

					set @dtStart = (
										select Coalesce(max(DateTimeGPS_UTC), @dtStart)
											from GFI_GPS_GPSData 
											where AssetID = @MyAssetID
												and DateTimeGPS_UTC < DATEADD(second, -@iDuration, @dtStart)
									)
					set @dtStartWithDuration = @dtStart

					print @dtStart
				end

				--Working table. GPSData_cursor from the same working table
				create table #GFI_GPS_GPSData 
				(
					[RowNumber] [int] IDENTITY(1,1) NOT NULL,
					[UID] [int] PRIMARY KEY CLUSTERED,
					[AssetID] [int] NULL,
					[DateTimeGPS_UTC] [datetime] NULL,
					[DateTimeServer] [datetime] NULL,
					[Longitude] [float] NULL,
					[Latitude] [float] NULL,
					[LongLatValidFlag] [int] NULL,
					[Speed] [float] NULL,
					[EngineOn] [int] NULL,
					[StopFlag] [int] NULL,
					[TripDistance] [float] NULL,
					[TripTime] [float] NULL,
					[WorkHour] [int] NULL,
					[DriverID] [int] NOT NULL,
					Misc1 nvarchar(100) null,
					Misc2 int null,
					Misc3 int null,
					RoadSpeed [float] NULL,
				)
				CREATE NONCLUSTERED INDEX #myIndex ON #GFI_GPS_GPSData 
				(
					[AssetID] ASC,
					[DateTimeGPS_UTC] ASC,
					[LongLatValidFlag] ASC,
					[StopFlag] ASC,
					[Speed] ASC
				)
				CREATE NONCLUSTERED INDEX #xxx ON #GFI_GPS_GPSData ([LongLatValidFlag],[DateTimeGPS_UTC],[DriverID]) INCLUDE ([RowNumber],[UID])
				CREATE NONCLUSTERED INDEX #yyy ON #GFI_GPS_GPSData ([AssetID],[LongLatValidFlag],[DateTimeGPS_UTC]) INCLUDE ([RowNumber],[UID])

				CREATE TABLE #GFI_GPS_GPSDataDetail
				(
					[GPSDetailID] [bigint] NOT NULL,
					[UID] [int] NULL,
					[TypeID] [nvarchar](30) NULL,
					[TypeValue] [nvarchar](30) NULL,
					[UOM] [nvarchar](15) NULL,
					[Remarks] [nvarchar](50) NULL,
				)
				ALTER TABLE #GFI_GPS_GPSDataDetail  WITH CHECK ADD CONSTRAINT FK FOREIGN KEY([UID])
					REFERENCES #GFI_GPS_GPSData ([UID])
					ON DELETE CASCADE
				CREATE NONCLUSTERED INDEX #MyIdxDTripProcessor ON #GFI_GPS_GPSDataDetail([TypeID] ASC)INCLUDE([UID],[TypeValue],[UOM])

				delete from #GFI_GPS_GPSData
				insert #GFI_GPS_GPSData (UID, AssetID, DateTimeGPS_UTC, DateTimeServer, Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, StopFlag, TripDistance, TripTime, WorkHour, DriverID, RoadSpeed)
					select top (@maxRows) UID, AssetID, DateTimeGPS_UTC, DateTimeServer, Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, StopFlag, TripDistance, TripTime, WorkHour, DriverID, RoadSpeed from GFI_GPS_GPSData 
					    where AssetID = @MyAssetID 
						    and DateTimeGPS_UTC >= @dtStart
					    ORDER BY DateTimeGPS_UTC

                select * from #GFI_GPS_GPSData;
				if(@@ROWCOUNT = 0)
				begin
					delete from GFI_GPS_ProcessPending where AssetID = @MyAssetID and ProcessCode = 'Exceptions' and Processing = 1

					fetch next from ProcessCursor into @MyAssetID, @dtStart
				end

                select * from #GFI_GPS_GPSData;
				if(@@ROWCOUNT >= @maxRows - 1)
				begin
					insert into GFI_GPS_ProcessPending (AssetID, dtdatefrom, ProcessCode) 
						values (@MyAssetID, (select dateAdd(second, 1, max(DateTimeGPS_UTC)) from #GFI_GPS_GPSData ), 'Exceptions');
				end

				create table #GFI_GPS_GPSDataWithDetails 
				(
					[RowNumber] [int] IDENTITY(1,1) NOT NULL,
					[UID] [int], --PRIMARY KEY CLUSTERED,
					[AssetID] [int] NULL,
					[DateTimeGPS_UTC] [datetime] NULL,
					[DateTimeServer] [datetime] NULL,
					[Longitude] [float] NULL,
					[Latitude] [float] NULL,
					[LongLatValidFlag] [int] NULL,
					[Speed] [float] NULL,
					[EngineOn] [int] NULL,
					[StopFlag] [int] NULL,
					[TripDistance] [float] NULL,
					[TripTime] [float] NULL,
					[WorkHour] [int] NULL,
					[DriverID] [int] NOT NULL,
					TypeID nvarchar(100) null,
					TypeValue nvarchar(100) null,
					UOM nvarchar(100) null
				)
				CREATE NONCLUSTERED INDEX #myIndexDetail ON #GFI_GPS_GPSDataWithDetails 
				(
					[AssetID] ASC,
					[DateTimeGPS_UTC] ASC,
					[LongLatValidFlag] ASC,
					[StopFlag] ASC,
					[Speed] ASC, [TypeID] ASC
				) INCLUDE ([UID],[TypeValue])
				--CREATE NONCLUSTERED INDEX #myIndexUID ON #GFI_GPS_GPSDataWithDetails([UID] ASC)

				delete from #GFI_GPS_GPSDataWithDetails
				delete from #ExTemp
				delete from #ResultTable

				--This cursor gets all rules applied to the asset, including driver
				declare RulesCursor CURSOR LOCAL SCROLL STATIC FOR
					WITH tree AS (		
						SELECT GMID, 
								GMDescription, 
								ParentGMID,
								1 as level 
						FROM GFI_SYS_GroupMatrix
						WHERE GMID in (SELECT m.GMID
											from GFI_SYS_GroupMatrixAsset m 
												inner join GFI_SYS_GroupMatrix gm on m.GMID = gm.GMID
												inner join GFI_FLT_Asset a (readpast) on m.iID = a.AssetID
											WHERE a.AssetID = @MyAssetID
										)
						UNION ALL 
						SELECT p.GMID,
								p.GMDescription,
								p.ParentGMID, 
								t.level + 1
						FROM GFI_SYS_GroupMatrix p
							JOIN tree t ON t.ParentGMID = p.GMID
					)
					SELECT r.ParentRuleID		--By GMID
					FROM tree, GFI_GPS_Rules r	
					where tree.GMID = r.StrucValue and StrucType = 'GMID'
					union								
					select ParentRuleID			--By AssetID
					from GFI_GPS_Rules where StrucValue = @MyAssetID and StrucType = 'ID'

				--Processing 1 rule at a time
				open RulesCursor
					fetch next from RulesCursor into @ParentRuleID
					WHILE @@FETCH_STATUS = 0   
					BEGIN 
						begin try

						--check if rule  is valid
						select @iRule = count(distinct(Struc)) from GFI_GPS_Rules where ParentRuleID = @ParentRuleID
						if(@iRule < 3)
							fetch next from RulesCursor into @ParentRuleID

						select * from GFI_GPS_Rules where ParentRuleID = @ParentRuleID and RuleType = 12
						if(@@ROWCOUNT > 0)
							fetch next from RulesCursor into @ParentRuleID

						select * from GFI_GPS_Rules where ParentRuleID = @ParentRuleID and RuleType = 13
						if(@@ROWCOUNT > 0)
							fetch next from RulesCursor into @ParentRuleID


						set @dtStart = @dtStartClone
						--if rule has duration, set dtStart accordingly
						select * from GFI_GPS_Rules where ParentRuleID = @ParentRuleID and RuleType = 7 and Struc = 'Duration'
						if(@@ROWCOUNT > 0)
						begin
							set @iDuration = null
							select @iDuration = Try_Cast(StrucValue as int) from GFI_GPS_Rules where ParentRuleID = @ParentRuleID and Struc = 'Duration'

							set @dtStart = (
												select Coalesce(max(DateTimeGPS_UTC), @dtStart)
													from GFI_GPS_GPSData 
													where AssetID = @MyAssetID
														and DateTimeGPS_UTC < DATEADD(second, -@iDuration, @dtStart)
											)
							
						end

						--insert #GFI_GPS_GPSDataDetail if needed
						begin
							delete from #GFI_GPS_GPSDataDetail
							set @bDetailsNeed = 0
							select * from GFI_GPS_Rules where ParentRuleID = @ParentRuleID and RuleType = 6 and Struc = 'Aux'
							if(@@ROWCOUNT > 0)
								set @bDetailsNeed = 1
							select * from GFI_GPS_Rules where ParentRuleID = @ParentRuleID and RuleType = 6 and Struc = 'EngineAux'
							if(@@ROWCOUNT > 0)
								set @bDetailsNeed = 1
							if(@bDetailsNeed = 1)
								insert #GFI_GPS_GPSDataDetail
									select d.*
										from #GFI_GPS_GPSData g
											inner join GFI_GPS_GPSDataDetail d on g.UID = d.UID 
										where g.AssetID = @MyAssetID 
											and g.DateTimeGPS_UTC >= @dtStart
						end

						--insert #ExTemp for each and every condition, except duration
						print ' @ParentRuleID >' + convert(nvarchar(50), @ParentRuleID) + ' AssetID >' + convert(nvarchar(50), @MyAssetID) + ' @ ' + CONVERT(VARCHAR(24), @dtStart, 113) + ' @ ' + CONVERT(VARCHAR(24), GetDate(), 113)
						set @WhereCondition = ''

						-- Asset
						--if(1=0)
						select * from GFI_GPS_Rules where ParentRuleID = @ParentRuleID and RuleType = 2
						if(@@ROWCOUNT > 0)
						begin
							set @RowCnt = 0
							set @WhereCondition = ''
							delete from #AssetFrmGMID
							set @RuleType = 2
							set @Struc = 'Assets'

							;WITH 
								parent AS 
								(
									SELECT distinct ParentGMID
									FROM GFI_SYS_GroupMatrix m
										inner join GFI_GPS_Rules r on m.ParentGMID = r.StrucValue
									WHERE ParentRuleID = @ParentRuleID 
										and RuleType = @RuleType and StrucType = 'GMID'
								), 
								tree AS 
								(
									SELECT 
										x.GMID
										, x.GMDescription
										, x.ParentGMID
									FROM GFI_SYS_GroupMatrix x
									INNER JOIN parent ON x.ParentGMID = parent.ParentGMID
									UNION ALL
									SELECT 
										y.GMID
										, y.GMDescription
										, y.ParentGMID
									FROM GFI_SYS_GroupMatrix y
									INNER JOIN tree t ON y.ParentGMID = t.GMID
								)
							insert into #AssetFrmGMID
								SELECT Distinct GMID, GMDescription, ParentGMID, -2 StrucID FROM tree
								union all
									SELECT z.GMID, z.GMDescription, -1, -3 Struc
									FROM GFI_SYS_GroupMatrix z 
										inner join GFI_GPS_Rules r on z.GMID = r.StrucValue
									WHERE ParentRuleID = @ParentRuleID 
										and RuleType = @RuleType and StrucType = 'GMID'
								union all
									select -4, a.AssetNumber, tree.GMID as ParentGMID, a.AssetID
										from GFI_FLT_Asset a (readpast)
											inner join GFI_SYS_GroupMatrixAsset m on a.AssetID = m.iID
											inner join tree on tree.GMID = m.GMID
								union all
									select -5, a.AssetNumber, m.GMID as ParentGMID, a.AssetID 
									from GFI_FLT_Asset a (readpast)
										inner join GFI_SYS_GroupMatrixAsset m on a.AssetID = m.iID
										inner join GFI_GPS_Rules r on m.GMID = r.StrucValue
									WHERE ParentRuleID = @ParentRuleID 
										and RuleType = @RuleType and StrucType = 'GMID'
		
							select StrucValue from GFI_GPS_Rules where ParentRuleID = @ParentRuleID 
								and RuleType = @RuleType and StrucType = 'ID' and StrucValue = @MyAssetID
							union
							select StrucID from #AssetFrmGMID where StrucID > 0 and StrucID = @MyAssetID
							set @RowCnt = @@ROWCOUNT
							
							if(@RowCnt > 0)
								set @WhereCondition = @MyAssetID

							if(len(@WhereCondition) > 0)
							begin
								--print 'whereCond ' + @WhereCondition

								set @SQLQuery = 'select RowNumber, UID, ' 
									+ convert(nvarchar(50), @ParentRuleID) + ', ' 
									+ convert(nvarchar(50), @ParentRuleID) + ', '
									+ convert(nvarchar(50), @RuleType) + ', '
									+ '''' + @Struc + '''' + ', '
									+ '''Assets'''
									+ ' from #GFI_GPS_GPSData where AssetID = ' + @WhereCondition  
									+ ' and LongLatValidFlag = 1'
									+ ' and DateTimeGPS_UTC >= ''' + convert(nvarchar(50), @dtStart) + ''''
								--print @SQLQuery
								insert #ExTemp
									EXECUTE(@SQLQuery) 
							end
						end

						-- Driver
						--if(1=0)
						select * from GFI_GPS_Rules where ParentRuleID = @ParentRuleID and RuleType = 3
						if(@@ROWCOUNT > 0)
						begin
							set @RowCnt = 0
							set @WhereCondition = ''
							delete from #AssetFrmGMID
							set @RuleType = 3
							set @Struc = 'Drivers'

							;WITH 
								parent AS 
								(
									SELECT distinct ParentGMID
									FROM GFI_SYS_GroupMatrix m
										inner join GFI_GPS_Rules r on m.ParentGMID = r.StrucValue
									WHERE ParentRuleID = @ParentRuleID 
										and RuleType = @RuleType and StrucType = 'GMID'
								), 
								tree AS 
								(
									SELECT 
										x.GMID
										, x.GMDescription
										, x.ParentGMID
									FROM GFI_SYS_GroupMatrix x
									INNER JOIN parent ON x.ParentGMID = parent.ParentGMID
									UNION ALL
									SELECT 
										y.GMID
										, y.GMDescription
										, y.ParentGMID
									FROM GFI_SYS_GroupMatrix y
									INNER JOIN tree t ON y.ParentGMID = t.GMID
								)
							insert into #AssetFrmGMID
								SELECT Distinct GMID, GMDescription, ParentGMID, -2 StrucID FROM tree
								union all
									SELECT z.GMID, z.GMDescription, -1, -3 Struc
									FROM GFI_SYS_GroupMatrix z 
										inner join GFI_GPS_Rules r on z.GMID = r.StrucValue
									WHERE ParentRuleID = @ParentRuleID 
										and RuleType = @RuleType and StrucType = 'GMID'
								union all
									select -4, a.sDriverName, tree.GMID as ParentGMID, a.DriverID
										from GFI_FLT_Driver a, GFI_SYS_GroupMatrixDriver m, tree
									where tree.GMID = m.GMID and a.DriverID = m.iID
								union all
									select -5, a.sDriverName, m.GMID as ParentGMID, a.DriverID 
									from GFI_FLT_Driver a
										inner join GFI_SYS_GroupMatrixDriver m on a.DriverID = m.iID
										inner join GFI_GPS_Rules r on m.GMID = r.StrucValue
									WHERE ParentRuleID = @ParentRuleID 
										and RuleType = @RuleType and StrucType = 'GMID'
		
							insert into #AssetFrmGMID
								select -1, -1, -1, StrucValue from GFI_GPS_Rules where ParentRuleID = @ParentRuleID 
									and RuleType = @RuleType and StrucType = 'ID'

							set @SQLQuery = 'select RowNumber, UID, ' 
								+ convert(nvarchar(50), @ParentRuleID) + ', ' 
								+ convert(nvarchar(50), @RuleID) + ', '
								+ convert(nvarchar(50), @RuleType) + ', '
								+ '''' + @Struc + '''' + ', '
								+ '''Driver'''
								+ ' from #GFI_GPS_GPSData where DriverID in (select StrucID from #AssetFrmGMID where StrucID > 0)'  
								+ ' and LongLatValidFlag = 1'
								+ ' and DateTimeGPS_UTC >= ''' + convert(nvarchar(50), @dtStart) + ''''
							--print @SQLQuery
							insert #ExTemp
								EXECUTE(@SQLQuery) 
						end

						--Time
						--select * from GFI_GPS_GPSData where CONVERT(varchar, DateTimeGPS_UTC,108) BETWEEN '06:00:00' AND '07:00:00'
						--if(1=0)
						select * from GFI_GPS_Rules where ParentRuleID = @ParentRuleID and RuleType = 5
						if(@@ROWCOUNT > 0)
						begin
							set @WhereCondition = ''
							IF CURSOR_STATUS('global', 'ProcessingRuleCursor') >= -1
							BEGIN
								DEALLOCATE ProcessingRuleCursor
							END
							Declare ProcessingRuleCursor cursor for
								select RuleID, RuleType, Struc, StrucCondition, StrucValue from GFI_GPS_Rules where ParentRuleID = @ParentRuleID 
									and RuleType = 5 order by MinTriggerValue
							open ProcessingRuleCursor
								fetch next from ProcessingRuleCursor into @RuleID, @RuleType, @Struc, @StrucCondition, @StructValue
								WHILE @@FETCH_STATUS = 0   
								BEGIN
									set @myTime = CONVERT(varchar, DATEADD(ss, cast(@StructValue as float), 0), 108);
									if (@WhereCondition = '')
										set @WhereCondition = 'CONVERT(varchar, DateTimeGPS_UTC,108) BETWEEN ''' + @myTime + ''' and '''
									else
										set @WhereCondition = @WhereCondition + @myTime + ''''
									fetch next from ProcessingRuleCursor into @RuleID, @RuleType, @Struc, @StrucCondition, @StructValue	
								END
								if(len(@WhereCondition) > 0)
								begin
									--print 'whereCond ' + @WhereCondition
									set @SQLQuery = 'select RowNumber, UID, ' 
										+ convert(nvarchar(50), @ParentRuleID) + ', ' 
										+ convert(nvarchar(50), @RuleID) + ', '
										+ convert(nvarchar(50), @RuleType) + ', '
										+ '''' + @Struc + '''' + ', '
										+ '''nothing'''
										+ ' from #GFI_GPS_GPSData where AssetID = ' + convert(nvarchar(50), @MyAssetID) 
										+ ' and LongLatValidFlag = 1'
										+ ' and DateTimeGPS_UTC >= ''' + convert(nvarchar(50), @dtStart) + ''' and '
										+ ' ( ' + @WhereCondition + ')'
									--print @SQLQuery
									insert #ExTemp
										EXECUTE(@SQLQuery) 
								end
							close ProcessingRuleCursor
							deallocate ProcessingRuleCursor
						end

						--Day
						--select * from GFI_GPS_GPSData where CONVERT(varchar, DateTimeGPS_UTC,108) BETWEEN '06:00:00' AND '07:00:00'
						--if(1=0)
						select * from GFI_GPS_Rules where ParentRuleID = @ParentRuleID and RuleType = 11
						if(@@ROWCOUNT > 0)
						begin
							set @WhereCondition = ''
							IF CURSOR_STATUS('global', 'ProcessingRuleCursor') >= -1
							BEGIN
								DEALLOCATE ProcessingRuleCursor
							END
							Declare ProcessingRuleCursor cursor for
								select RuleID, RuleType, Struc, StrucCondition, StrucValue from GFI_GPS_Rules where ParentRuleID = @ParentRuleID 
									and RuleType = 11 order by MinTriggerValue
							open ProcessingRuleCursor
								fetch next from ProcessingRuleCursor into @RuleID, @RuleType, @Struc, @StrucCondition, @StructValue
								WHILE @@FETCH_STATUS = 0   
								BEGIN
									set @WhereCondition = 'DATENAME(weekday, DateTimeGPS_UTC) in (' + @StrucCondition + ')'

									fetch next from ProcessingRuleCursor into @RuleID, @RuleType, @Struc, @StrucCondition, @StructValue	
								END
								if(len(@WhereCondition) > 0)
								begin
									--print 'whereCond ' + @WhereCondition
									set @SQLQuery = 'select RowNumber, UID, ' 
										+ convert(nvarchar(50), @ParentRuleID) + ', ' 
										+ convert(nvarchar(50), @RuleID) + ', '
										+ convert(nvarchar(50), @RuleType) + ', '
										+ '''' + @Struc + '''' + ', '
										+ '''nothing'''
										+ ' from #GFI_GPS_GPSData where AssetID = ' + convert(nvarchar(50), @MyAssetID) 
										+ ' and LongLatValidFlag = 1'
										+ ' and DateTimeGPS_UTC >= ''' + convert(nvarchar(50), @dtStart) + ''' and '
										+ ' ( ' + @WhereCondition + ')'
									--print @SQLQuery
									insert #ExTemp
										EXECUTE(@SQLQuery) 
								end
							close ProcessingRuleCursor
							deallocate ProcessingRuleCursor
						end


						-- Speed
						--if(1=0)
						select * from GFI_GPS_Rules where ParentRuleID = @ParentRuleID and RuleType = 4
						if(@@ROWCOUNT > 0)
						begin
							set @WhereCondition = ''
							IF CURSOR_STATUS('global', 'ProcessingRuleCursor') >= -1
							BEGIN
								DEALLOCATE ProcessingRuleCursor
							END
							Declare ProcessingRuleCursor cursor for
								select RuleID, RuleType, Struc, StrucCondition, StrucValue, StrucType, MinTriggerValue from GFI_GPS_Rules where ParentRuleID = @ParentRuleID 
									and RuleType = 4 
							open ProcessingRuleCursor
								fetch next from ProcessingRuleCursor into @RuleID, @RuleType, @Struc, @StrucCondition, @StructValue, @StrucType, @MinTriggerValue
								WHILE @@FETCH_STATUS = 0   
								BEGIN
									set @TmpCondition = ' '
									if(@WhereCondition <> '')
									begin
										--if(@MinTriggerValue = 1)
											set @TmpCondition = ' or '
										--else 
										if(@MinTriggerValue = 2)
											set @TmpCondition = ' and '
									end

									set @WhereCondition = @WhereCondition + @TmpCondition + @Struc + @StrucCondition + @StructValue --' and ' -- was or
									fetch next from ProcessingRuleCursor into @RuleID, @RuleType, @Struc, @StrucCondition, @StructValue, @StrucType, @MinTriggerValue	
								END
								if(len(@WhereCondition) > 0)
								begin
									--print 'whereCond ' + @WhereCondition
									set @SQLQuery = 'select RowNumber, UID, ' 
										+ convert(nvarchar(50), @ParentRuleID) + ', ' 
										+ convert(nvarchar(50), @RuleID) + ', '
										+ convert(nvarchar(50), @RuleType) + ', '
										+ '''' + @Struc + '''' + ', '
										+ '''nothing'''
										+ ' from #GFI_GPS_GPSData where AssetID = ' + convert(nvarchar(50), @MyAssetID) 
										+ ' and LongLatValidFlag = 1'
										+ ' and DateTimeGPS_UTC >= ''' + convert(nvarchar(50), @dtStart) + ''' and '
										+ ' ( ' + @WhereCondition + ')'
									if(@StrucType = 'Stopped')
										set @SQLQuery = @SQLQuery + ' and EngineOn = 0'
									else
										set @SQLQuery = @SQLQuery + ' and EngineOn = 1'
							
									--inserting First Engine On after EngineOff to get duration
									if(@StrucType = 'Stopped. NowObsoleteNeedToLookForAnotherLogic')
									begin
										set @StopQry = 'select RowNumber '
														+ ' from #GFI_GPS_GPSData where AssetID = ' + convert(nvarchar(50), @MyAssetID) 
														+ ' and LongLatValidFlag = 1'
														+ ' and DateTimeGPS_UTC >= ''' + convert(nvarchar(50), @dtStart) + ''' and '
														+ ' ( ' + @WhereCondition + ')' + ' and EngineOn = 0'
										delete from #Stopped
										Insert #Stopped
											Execute (@StopQry)

										IF (SELECT CURSOR_STATUS('local','ProcessingStoppedCursor')) >= -1
										BEGIN
											IF (SELECT CURSOR_STATUS('local','ProcessingStoppedCursor')) > -1
											BEGIN
												CLOSE ProcessingStoppedCursor
											END
											DEALLOCATE ProcessingStoppedCursor
										END
										DECLARE ProcessingStoppedCursor CURSOR LOCAL SCROLL STATIC FOR
											select myRowNum from #Stopped
										open ProcessingStoppedCursor
											fetch next from ProcessingStoppedCursor into @myStopRowNum
											WHILE @@FETCH_STATUS = 0   
											BEGIN
												fetch prior from ProcessingStoppedCursor into @myPreviousStopRowNum	
												fetch next from ProcessingStoppedCursor into @myStopRowNum	
												if(@myPreviousStopRowNum + 1 != @myStopRowNum)
												begin
													print @StopQry
													set @StopQry = 'select RowNumber, UID, ' 
														+ convert(nvarchar(50), @ParentRuleID) + ', ' 
														+ convert(nvarchar(50), @RuleID) + ', '
														+ convert(nvarchar(50), @RuleType) + ', '
														+ '''' + @Struc + '''' + ', '
														+ '''nothing'''
														+ ' from #GFI_GPS_GPSData where AssetID = ' + convert(nvarchar(50), @MyAssetID) 
														+ ' and RowNumber = ' + convert(nvarchar(50), @myPreviousStopRowNum + 1)
													print @StopQry
													insert #ExTemp
														EXECUTE(@StopQry) 
												end
												fetch next from ProcessingStoppedCursor into @myStopRowNum	
											END
										close ProcessingStoppedCursor
										deallocate ProcessingStoppedCursor

										--print @SQLQuery
										--select 'Stopped'
										--EXECUTE(@SQLQuery) 
									end

									insert #ExTemp
										EXECUTE(@SQLQuery) 
								end
							close ProcessingRuleCursor
							deallocate ProcessingRuleCursor
						end

						-- RoadSpeed
						--if(1=0)
						select @RuleID = RuleID, @RuleType = 10 from GFI_GPS_Rules where ParentRuleID = @ParentRuleID and RuleType = 10 and Struc = 'RoadSpeed'
						if(@@ROWCOUNT > 0)
						begin
							set @SQLQuery = 'select RowNumber, UID, ' 
										+ convert(nvarchar(50), @ParentRuleID) + ', ' 
										+ convert(nvarchar(50), @RuleID) + ', '
										+ convert(nvarchar(50), @RuleType) + ', '
										+ '''' + @Struc + '''' + ', '
										+ '''RoadSpeed'''
										+ ' from #GFI_GPS_GPSData where AssetID = ' + convert(nvarchar(50), @MyAssetID) 
										+ ' and LongLatValidFlag = 1'
										+ ' and Speed > RoadSpeed'
							
							--	print @SQLQuery
							insert #ExTemp
								EXECUTE(@SQLQuery) 
						end

						-- Aux
						--if(1=0)
						select * from GFI_GPS_Rules where ParentRuleID = @ParentRuleID and RuleType = 6 and Struc = 'Aux'
						if(@@ROWCOUNT > 0)
						begin
							IF CURSOR_STATUS('global', 'ProcessingRuleCursor') >= -1
							BEGIN
								DEALLOCATE ProcessingRuleCursor
							END
							Declare ProcessingRuleCursor cursor for
								select RuleID, RuleType, Struc, StrucCondition, StrucValue from GFI_GPS_Rules where ParentRuleID = @ParentRuleID 
									and RuleType = 6 and Struc = 'Aux'
							open ProcessingRuleCursor
								fetch next from ProcessingRuleCursor into @RuleID, @RuleType, @Struc, @StrucCondition, @StructValue
								WHILE @@FETCH_STATUS = 0   
								BEGIN
									--Need to loop to get and\or condition
									--And = MinimumTriggerValue 1
									--Or  = MinimumTriggerValue 2
									set @WhereCondition = ''
									set @WhereCondition = 'TypeID = ''' + @StructValue + ''''
										 + ' and TypeValue ' + @StrucCondition	--This condition is under testing. if ommitted, both on and off will be in exception
									if(len(@WhereCondition) > 0)
									begin
										delete from #GFI_GPS_GPSDataWithDetails
										insert #GFI_GPS_GPSDataWithDetails (UID, AssetID, DateTimeGPS_UTC, DateTimeServer, Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, StopFlag, TripDistance, TripTime, WorkHour, DriverID, TypeID, TypeValue, UOM)
										select g.UID, g.AssetID, g.DateTimeGPS_UTC, g.DateTimeServer, g.Longitude, g.Latitude, g.LongLatValidFlag, g.Speed, g.EngineOn, g.StopFlag,g. TripDistance, g.TripTime, g.WorkHour, g.DriverID, d.TypeID, d.TypeValue, d.UOM 
												from #GFI_GPS_GPSData g
													inner join #GFI_GPS_GPSDataDetail d on g.UID = d.UID 
												where g.AssetID = @MyAssetID 
													and g.DateTimeGPS_UTC >= @dtStart
													and d.TypeID = @StructValue
												ORDER BY g.DateTimeGPS_UTC


										--print 'whereCond ' + @WhereCondition
										set @SQLQuery = 'select -1, UID, ' 
											+ convert(nvarchar(50), @ParentRuleID) + ', ' 
											+ convert(nvarchar(50), @RuleID) + ', '
											+ convert(nvarchar(50), @RuleType) + ', '
											+ '''' + @Struc + '''' + ', '
											+ 'TypeID + '' '' + TypeValue'
											+ ' from #GFI_GPS_GPSDataWithDetails where AssetID = ' + convert(nvarchar(50), @MyAssetID) 
											--+ ' and LongLatValidFlag = 1'
											+ ' and DateTimeGPS_UTC >= ''' + convert(nvarchar(50), @dtStart) + ''' and '
											+ ' ( ' + @WhereCondition + ')'
										--print @SQLQuery
										insert #ExTemp
											EXECUTE(@SQLQuery)
									end
									fetch next from ProcessingRuleCursor into @RuleID, @RuleType, @Struc, @StrucCondition, @StructValue	
								END
							close ProcessingRuleCursor
							deallocate ProcessingRuleCursor
						end

						--Zones and ZoneTypes
						--StrucValue is either ID or GMID. If it is GMID, we will need to took for the corresponding IDs
						--Zone ID
						--if(1=0)
						select * from GFI_GPS_Rules where ParentRuleID = @ParentRuleID and (RuleType = 1 or RuleType = 0)
						if(@@ROWCOUNT > 0)
						begin
							delete from #ZoneHeader
							IF CURSOR_STATUS('global', 'ProcessingRuleCursor') >= -1
							BEGIN
								DEALLOCATE ProcessingRuleCursor
							END
							Declare ProcessingRuleCursor cursor for
								select RuleID, RuleType, Struc, StrucCondition, StrucValue, StrucType from GFI_GPS_Rules where ParentRuleID = @ParentRuleID 
									and (RuleType = 1 or RuleType = 0)
							open ProcessingRuleCursor
								fetch next from ProcessingRuleCursor into @RuleID, @RuleType, @Struc, @StrucCondition, @StructValue, @StrucType
								WHILE @@FETCH_STATUS = 0   
								BEGIN
									if(@RuleType = 1)	--Zones
										begin
											if(@StrucType = 'ID')
											begin
												insert #ZoneHeader 
													values (@StructValue)
											end
											else if(@StrucType = 'GMID')
											begin
												declare @t table (i int)
												;WITH 
													parent AS 
													(
														SELECT distinct ParentGMID
														FROM GFI_SYS_GroupMatrix
														WHERE ParentGMID in (@StructValue)
													), 
													tree AS 
													(
														SELECT 
															x.GMID
															, x.GMDescription
															, x.ParentGMID
														FROM GFI_SYS_GroupMatrix x
														INNER JOIN parent ON x.ParentGMID = parent.ParentGMID
														UNION ALL
														SELECT 
															y.GMID
															, y.GMDescription
															, y.ParentGMID
														FROM GFI_SYS_GroupMatrix y
														INNER JOIN tree t ON y.ParentGMID = t.GMID
													)
												insert into @t(i)
													select a.ZoneID   
														from GFI_FLT_ZoneHeader a, GFI_SYS_GroupMatrixZone m, tree
													where tree.GMID = m.GMID and a.ZoneID = m.iID

												insert into @t(i)
													select a.ZoneID  
														from GFI_FLT_ZoneHeader a, GFI_SYS_GroupMatrixZone m
													where m.GMID in (@StructValue) and a.ZoneID = m.iID

												insert #ZoneHeader 
													select i from @t
											end
										end
									else				-- ZoneTypes. RuleType = 0
										begin
											--Need to link Zones with GroupMatrix
											delete from @t
											insert into @t
												select ZoneHeadID from GFI_FLT_ZoneHeadZoneType where ZoneTypeID = @StructValue

											insert #ZoneHeader 
												select i from @t
										end
									fetch next from ProcessingRuleCursor into @RuleID, @RuleType, @Struc, @StrucCondition, @StructValue, @StrucType	
								END
							close ProcessingRuleCursor
							deallocate ProcessingRuleCursor

                            --Harcoding SIndx_GFI_FLT_ZoneHeader
							--select distinct z.ZoneID, g.RowNumber, g.UID, g.AssetID, g.DateTimeGPS_UTC, g.Longitude, g.Latitude, g.Speed into #inZone
							--from #ZoneHeader t
							--	inner join GFI_FLT_ZoneHeader z WITH (INDEX(SIndx_GFI_FLT_ZoneHeader)) on z.ZoneID = t.ZoneID
							--	inner join #GFI_GPS_GPSData g on z.GeomData.STIntersects(Geometry::STPointFromText('POINT(' + CONVERT(varchar, g.Longitude) + ' ' + CONVERT(varchar, g.Latitude) + ')', 4326)) = 1

                            --Better option, but not using spatial index
                            --select distinct x.*
                            --    from #ZoneHeader t
	                        --        inner join (
					        --                        select z.ZoneID, g.RowNumber, g.UID, g.AssetID, g.DateTimeGPS_UTC, g.Longitude, g.Latitude, g.Speed from GFI_FLT_ZoneHeader z
						    --                            inner join #GFI_GPS_GPSData g on z.GeomData.STIntersects(Geometry::STPointFromText('POINT(' + CONVERT(varchar, g.Longitude) + ' ' + CONVERT(varchar, g.Latitude) + ')', 4326)) = 1
				            --                    ) x on x.ZoneID = t.ZoneID

                            --using spatial index, but quering all Zone records
							select g.*, z.ZoneID, 1 InZone into #InAllZone from GFI_FLT_ZoneHeader z 
								inner join #GFI_GPS_GPSData g on z.GeomData.STIntersects(Geometry::STPointFromText('POINT(' + CONVERT(varchar, g.Longitude, 128) + ' ' + CONVERT(varchar, g.Latitude, 128) + ')', 4326)) = 1 

							select distinct z.* into #InZone
								from #ZoneHeader t
									inner join #inAllZone z on z.ZoneID = t.ZoneID

							insert #InZone
								select g.*, -1 ZoneID, 0 InZone from #GFI_GPS_GPSData g where RowNumber not in (select RowNumber from #InZone)
									
							select distinct * into #ZoneData from #InZone
							--select g.*, null ZoneID, 0 InZone into #ZoneData from #GFI_GPS_GPSData g 
							--update #ZoneData
							--	set ZoneID = 0, InZone = 1
							--from #ZoneData g
							--	inner join GFI_FLT_ZoneHeader z on z.GeomData.STIntersects(Geometry::STPointFromText('POINT(' + CONVERT(varchar, g.Longitude) + ' ' + CONVERT(varchar, g.Latitude) + ')', 4326)) = 1

							if (@StrucCondition = 'Entering zone')
								begin 
									set @SQLQuery = 'select Z2.RowNumber, Z2.UID, ' 
											+ convert(nvarchar(50), @ParentRuleID)  + ', ' 
											+ convert(nvarchar(50), @RuleID) + ', '
											+ convert(nvarchar(50), @RuleType) + ', '
											+ '''' + @Struc + '''' + ', '
											+ ' Z2.ZoneID '
											+ ' from #ZoneData z1 inner join #ZoneData z2 on z2.InZone = 1 and z2.RowNumber = z1.RowNumber + 1 and z1.InZone = 0 '
										--print 'Entering ' + @SQLQuery
										insert #ExTemp
											EXECUTE(@SQLQuery) 
								end
								else if (@StrucCondition = 'Leaving zone')
								begin 
									set @SQLQuery = 'select Z2.RowNumber, Z2.UID, ' 
											+ convert(nvarchar(50), @ParentRuleID)  + ', ' 
											+ convert(nvarchar(50), @RuleID) + ', '
											+ convert(nvarchar(50), @RuleType) + ', '
											+ '''' + @Struc + '''' + ', '
											+ ' Z1.ZoneID '
											+ ' from #ZoneData z1 inner join #ZoneData z2 on z2.InZone = 0 and z2.RowNumber = z1.RowNumber + 1 and z1.InZone = 1 '
										--print 'Entering ' + @SQLQuery
										insert #ExTemp
											EXECUTE(@SQLQuery) 
								end
								else if (@StrucCondition = 'Stopped inside zone' or @StrucCondition = 'Inside zone bounds')
									begin
										set @SQLQuery = 'select RowNumber, UID, ' 
											+ convert(nvarchar(50), @ParentRuleID)  + ', ' 
											+ convert(nvarchar(50), @RuleID) + ', '
											+ convert(nvarchar(50), @RuleType) + ', '
											+ '''' + @Struc + '''' + ', '
											+ '''ZoneID '''
											+ ' from #ZoneData where InZone = 1' 
										--print 'In Zone ' + @SQLQuery
										insert #ExTemp
											EXECUTE(@SQLQuery) 
									end
								else if (@StrucCondition = 'Stopped outside zone' or @StrucCondition = 'Outside zone bounds')
									begin
										set @SQLQuery = 'select RowNumber, UID, ' 
											+ convert(nvarchar(50), @ParentRuleID)  + ', ' 
											+ convert(nvarchar(50), @RuleID) + ', '
											+ convert(nvarchar(50), @RuleType) + ', '
											+ '''' + @Struc + '''' + ', '
											+ '''ZoneID '''
											+ ' from #ZoneData where InZone = 0' 
										--print 'In Zone ' + @SQLQuery
										insert #ExTemp
											EXECUTE(@SQLQuery) 
									end
								else
									begin
										set @SQLQuery = 'select RowNumber, UID, ' 
											+ convert(nvarchar(50), @ParentRuleID)  + ', ' 
											+ convert(nvarchar(50), @RuleID) + ', '
											+ convert(nvarchar(50), @RuleType) + ', '
											+ '''' + @Struc + '''' + ', '
											+ '''ZoneID '''
											+ ' from #ZoneData where InZone = 1'  
										--print @SQLQuery
										insert #ExTemp
											EXECUTE(@SQLQuery) 
									end

							drop table #inAllZone
							drop table #inZone
							drop table #ZoneData
						end

						-- EngineAux
						--if(1=0)
						select * from GFI_GPS_Rules where ParentRuleID = @ParentRuleID and RuleType = 6 and Struc = 'EngineAux'
						if(@@ROWCOUNT > 0)
						begin
							set @WhereCondition = ''
							set @TmpCondition = ' '
							IF CURSOR_STATUS('global', 'ProcessingRuleCursor') >= -1
							BEGIN
								DEALLOCATE ProcessingRuleCursor
							END
							Declare ProcessingRuleCursor cursor for
								select RuleID, RuleType, Struc, StrucCondition, StrucValue, StrucType from GFI_GPS_Rules where ParentRuleID = @ParentRuleID 
									and RuleType = 6 and Struc = 'EngineAux'
							open ProcessingRuleCursor
								fetch next from ProcessingRuleCursor into @RuleID, @RuleType, @Struc, @StrucCondition, @StructValue, @StrucType
								WHILE @@FETCH_STATUS = 0   
								BEGIN
									if(@WhereCondition <> '')
									begin
										if(@MinTriggerValue = 1)
											set @TmpCondition = ' or '
										else 
										if(@MinTriggerValue = 2)
											set @TmpCondition = ' and '
									end

									set @EngineAuxCondition = 
										CASE @StructValue 
											WHEN 'Harsh Acceleration' then 'Harsh Accelleration'
											WHEN 'Harsh Breaking' THEN 'Harsh Breaking'
											WHEN 'Power Reset' THEN 'Reset'
											WHEN 'Fuel' THEN 'Fuel'
											WHEN 'Temperature' THEN @StructValue
											WHEN 'Temperature 2' THEN @StructValue
											WHEN 'GPSLost' THEN 'InvalidGpsSignals'
											WHEN 'Impact' THEN 'Impact'
											ELSE 'TingPow'
										END

									set @EngineAuxStrucCondition = 
										CASE @StructValue 
											WHEN 'Fuel' THEN ' and cast (TypeValue as float) ' + @StrucCondition + ' ''' + @StrucType + ''''
											WHEN 'Temperature' THEN ' and cast (TypeValue as float) ' + @StrucCondition + ' ''' + @StrucType + ''''
											WHEN 'Temperature 2' THEN ' and cast (TypeValue as float) ' + @StrucCondition + ' ''' + @StrucType + ''''
											ELSE ' '
										END

									set @WhereCondition = @TmpCondition + 'TypeID = ''' + @EngineAuxCondition + ''''
										 + @EngineAuxStrucCondition

									fetch next from ProcessingRuleCursor into @RuleID, @RuleType, @Struc, @StrucCondition, @StructValue, @StrucType	
								END
								if(len(@WhereCondition) > 10)
								begin
									delete from #GFI_GPS_GPSDataWithDetails
									insert #GFI_GPS_GPSDataWithDetails (UID, AssetID, DateTimeGPS_UTC, DateTimeServer, Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, StopFlag, TripDistance, TripTime, WorkHour, DriverID, TypeID, TypeValue, UOM)
										select g.UID, g.AssetID, g.DateTimeGPS_UTC, g.DateTimeServer, g.Longitude, g.Latitude, g.LongLatValidFlag, g.Speed, g.EngineOn, g.StopFlag,g. TripDistance, g.TripTime, g.WorkHour, g.DriverID, d.TypeID, d.TypeValue, d.UOM 
											from #GFI_GPS_GPSData g
												inner join #GFI_GPS_GPSDataDetail d on g.UID = d.UID 
											where g.AssetID = @MyAssetID 
												and g.DateTimeGPS_UTC >= @dtStart
												and d.TypeID = @StructValue
											ORDER BY g.DateTimeGPS_UTC
									--set @WhereCondition =  REPLACE(@WhereCondition, 'TypeValue', 'TypeValue');
									--print 'whereCond ' + @WhereCondition
									set @SQLQuery = 'select -1, UID, ' 
										+ convert(nvarchar(50), @ParentRuleID) + ', ' 
										+ convert(nvarchar(50), @RuleID) + ', '
										+ convert(nvarchar(50), @RuleType) + ', '
										+ '''' + @Struc + '''' + ', '
										+ 'TypeID + '' '' + TypeValue'
										+ ' from #GFI_GPS_GPSDataWithDetails where AssetID = ' + convert(nvarchar(50), @MyAssetID) 
										+ ' and LongLatValidFlag = 1'
										+ ' and DateTimeGPS_UTC >= ''' + convert(nvarchar(50), @dtStart) + ''' and '
										+ ' ( ' + @WhereCondition + ')'
									--print @SQLQuery
									insert #ExTemp
										EXECUTE(@SQLQuery)
								end

							close ProcessingRuleCursor
							deallocate ProcessingRuleCursor
						end

						end try
						begin catch
							insert GFI_GPS_ProcessErrors (sError, sOrigine, sFieldName, sFieldValue, AssetID, dtUTC) 
								values (ERROR_MESSAGE(), 'Exceptions', 'ParentRuleID', @ParentRuleID, @MyAssetID, @dtStart)
						end catch

						--Main Cursor
						fetch next from RulesCursor into @ParentRuleID
					end

					--Checking all conditions in rule, except Duration
					begin
						fetch first from RulesCursor into @ParentRuleID
						WHILE @@FETCH_STATUS = 0   
						BEGIN 
							insert #ResultTable
								SELECT @ParentRuleID Parent, g.*, 0
								FROM #GFI_GPS_GPSData g
									INNER JOIN
									(
										--SELECT  distinct e.UID 
										--FROM    #ExTemp e, GFI_GPS_Rules r
										--WHERE   e.RuleID = r.RuleID and r.ParentRuleID = @ParentRuleID and e.RuleType not in (7, 8)	--Duration 7 and ExceptionType 8
										--group by uid
										--having count(e.UID) >= (SELECT   count(distinct RuleType)   --Was =. Now >= for situations where TypeID saved more than once in GPPSDataDetails. Will ensure this is not the case when saving in GPPSDataDetails
										--						FROM    GFI_GPS_Rules r
										--						WHERE   ParentRuleID = @ParentRuleID and RuleType not in (7, 8)		--Duration 7 and ExceptionType 8
										--						group by ParentRuleID
										--						)
										select t.UID from
											(
												SELECT  distinct e.UID, e.RuleType, 1 cnt
														FROM    #ExTemp e, GFI_GPS_Rules r
														WHERE   e.RuleID = r.RuleID and r.ParentRuleID = @ParentRuleID and e.RuleType not in (7, 8)	--Duration 7 and ExceptionType 8
														group by uid, e.RuleType
											) t group by t.UID
													having count(t.UID) >= (SELECT   count(distinct RuleType)   
																			FROM    GFI_GPS_Rules r
																			WHERE   ParentRuleID = @ParentRuleID and RuleType not in (7, 8)		--Duration 7 and ExceptionType 8
																			group by ParentRuleID
																			)
									) e ON g.UID = e.UID

							fetch next from RulesCursor into @ParentRuleID
						end		
					end

					--Assigning groupID
					--if(1 = 0)
					begin
						;WITH CTE AS 
						(
							SELECT *, InnerRowNum = ROW_NUMBER() OVER (ORDER BY ParentRuleIDTriggered, DateTimeGPS_UTC) from #ResultTable
						)
						SELECT distinct t.*
							, prev.RowNumber PreviousRN
							, t.RowNumber ActualRN
							, nex.RowNumber NextRN
							, 0 GrpID
						into #tmp10 
						FROM #ResultTable t
							left outer join CTE on t.RowNumber = CTE.RowNumber
							LEFT JOIN CTE prev ON prev.RowNumber = CTE.RowNumber - 1
							LEFT JOIN CTE nex ON nex.RowNumber = CTE.RowNumber + 1
						order by dateTimeGPS_UTC

						select *
							, (select count(1) from #tmp10 where ActualRN = tt.PreviousRN and ParentRuleIDTriggered = tt.ParentRuleIDTriggered) grpSemiFinal into #tmp11
						from #tmp10 tt order by DateTimeGPS_UTC

						select *
							, (select count(1) from #tmp11 where grpSemiFinal = 0 and ActualRN <= tt.ActualRN and ParentRuleIDTriggered = tt.ParentRuleIDTriggered) grpFinal into #tmp12
						from #tmp11 tt order by DateTimeGPS_UTC
						
						update #tmp12 set GrpID = R.Grp
							from #tmp12 T
								inner join (
												select *
													, (select count(1) from #tmp12 where NextRN is null and ActualRN < tt.ActualRN and ParentRuleIDTriggered = tt.ParentRuleIDTriggered) grp 
												from #tmp12 tt
											) R
								on R.UID = T.UID and t.ParentRuleIDTriggered = r.ParentRuleIDTriggered
						
						update #ResultTable set Misc2 = R.grpFinal
							from #ResultTable T
								inner join #tmp12 R on T.UID = R.UID and T.ParentRuleIDTriggered = R.ParentRuleIDTriggered

						drop table #tmp10
						drop table #tmp11
						drop table #tmp12
					end

					--Processing Rules for duration
					--if(1=0)
					begin
						--Creating a header table with duration, group by Misc2, ParentRuleIDTriggered
						;WITH TopBottomRow
						AS
						(
							SELECT Misc2, ParentRuleIDTriggered, min(DateTimeGPS_UTC) mini, max(DateTimeGPS_UTC) maxi
								FROM #ResultTable where Misc2 is not null
								Group BY Misc2, ParentRuleIDTriggered
						)
						SELECT *, DATEDIFF(SECOND, mini, maxi) Duration into #GrpDuration
							FROM TopBottomRow 
							order by Misc2, ParentRuleIDTriggered

						--update TopBottomRow with existing exceptions data, based of @dtStart
						--if @dtStart exists in GFI_GPS_Exceptions, mini will be 1st occurence

						fetch first from RulesCursor into @ParentRuleID
						WHILE @@FETCH_STATUS = 0   
						BEGIN 
							IF CURSOR_STATUS('global', 'ProcessingRuleCursor') >= -1
							BEGIN
								DEALLOCATE ProcessingRuleCursor
							END
							Declare ProcessingRuleCursor cursor for
								select RuleID, RuleType, Struc, StrucCondition, StrucValue from GFI_GPS_Rules where ParentRuleID = @ParentRuleID 
									and RuleType = 7 
							open ProcessingRuleCursor
								fetch next from ProcessingRuleCursor into @RuleID, @RuleType, @Struc, @StrucCondition, @StructValue
								WHILE @@FETCH_STATUS = 0   
								BEGIN
									delete from #ResultTable 
										from #ResultTable t1
											INNER JOIN (select * from #GrpDuration where ParentRuleIDTriggered = @ParentRuleID and Duration <= @StructValue) t2 
											on t1.ParentRuleIDTriggered = t2.ParentRuleIDTriggered and t1.DateTimeGPS_UTC >= t2.mini and t1.DateTimeGPS_UTC <= t2.maxi

									delete from #ResultTable where Misc2 is null and ParentRuleIDTriggered = @ParentRuleID
									
									fetch next from ProcessingRuleCursor into @RuleID, @RuleType, @Struc, @StrucCondition, @StructValue	
								END
							close ProcessingRuleCursor
							deallocate ProcessingRuleCursor
	
							fetch next from RulesCursor into @ParentRuleID
						END		
					
						drop table #GrpDuration
					end 

				close RulesCursor
				deallocate RulesCursor

				--Getting earliest timestamp
				set @dtStart = @dtStartWithDuration
				select * into #OldException from GFI_GPS_Exceptions where AssetID = @MyAssetID and GPSDataID in 
						(select g.UID from #GFI_GPS_GPSData g
							where AssetID = @MyAssetID
								and g.DateTimeGPS_UTC >= @dtStart)
		
				SELECT @MaxGrpID = PValue from GFI_SYS_GlobalParams WHERE Paramname = 'Exceptions'
				if(@MaxGrpID is null)
				begin
					insert GFI_SYS_GlobalParams (ParamName, PValue, ParamComments) values ('Exceptions', 0, 'Exceptions GroupID')
					SELECT @MaxGrpID = PValue from GFI_SYS_GlobalParams WHERE Paramname = 'Exceptions'
				end
				--select @MaxGrpID, @MyAssetID

				--Misc4 is Posted
				--Misc3 is Existing GroupID of GFI_GPS_Exceptions
				--Misc2 is the final GroupID
				--Misc1 is the calculated GroupID
				--Update 1 init
				--Update 2 Reassign deleted GroupID for matching rows. Instead, using new GroupID, but Updating Posted for matching rows
				--Update 3 Assign same GroupID to related GroupID
				--Update 4 Assign GroupID to new Groups
				update #ResultTable set Misc4 = 0, Misc3 = 0, Misc1 = Misc2
				UPDATE #ResultTable SET Misc4 = Posted
					FROM #ResultTable T
					INNER JOIN #OldException S ON S.GPSDataID = T.UID and S.RuleID = T.ParentRuleIDTriggered

				;with cte as
				(
					select RuleID, GPSDataID, GroupID from #OldException 
				)
				update #ResultTable set Misc3 = c.GroupID, WorkHour = -100
					from #ResultTable T
						inner join cte c on T.ParentRuleIDTriggered = c.RuleID and T.UID = c.GPSDataID

				DECLARE GrpIDCursor CURSOR FOR 
					select ParentRuleIDTriggered, Misc3, Misc1 from #ResultTable where Misc3 != 0 group by ParentRuleIDTriggered, Misc3, Misc1

				open GrpIDCursor
					fetch next from GrpIDCursor into @ParentRuleIDTriggered, @Misc3, @Misc1
					WHILE @@FETCH_STATUS = 0   
					BEGIN 
						update #ResultTable 
							set Misc3 = @Misc3
								, Misc4 = (select top 1 Posted from #OldException where RuleID = @ParentRuleIDTriggered order by DateTimeGPS_UTC)  
						where Misc1 = @Misc1 
							and ParentRuleIDTriggered = @ParentRuleIDTriggered
						fetch next from GrpIDCursor into @ParentRuleIDTriggered, @Misc3, @Misc1
					END
				close GrpIDCursor
				deallocate GrpIDCursor

				begin tran
					set @InTran = 1
					if(1 = 0)
						delete from GFI_GPS_Exceptions 
							where AssetID = @MyAssetID 
								and GPSDataID in 
									(select g.UID from #GFI_GPS_GPSData g
										where AssetID = @MyAssetID
											and g.DateTimeGPS_UTC >= @dtStart)
								and RuleID not in 
									(select r.ParentRuleID from GFI_GPS_Rules r where r.StrucValue = 'Not Reported' or r.Struc = 'Maintenance')

					update #ResultTable set Misc2 = Misc3 where Misc3 != 0
					update #ResultTable set Misc2 = @MaxGrpID + Misc1 where Misc3 = 0
					select @MaxGrpID = max(Misc2) from #ResultTable
					if(@MaxGrpID is not null)
						update GFI_SYS_GlobalParams set PValue = @MaxGrpID WHERE Paramname = 'Exceptions' and @MaxGrpID > CAST(PValue AS int)
					--select '#ResultTable'
					--select * from #ResultTable

					insert [GFI_GPS_Exceptions]
						(RuleID, GPSDataID, AssetID, DriverID, GroupID, Posted, DateTimeGPS_UTC)
							(select ParentRuleIDTriggered, UID, AssetID, DriverID, Misc2, Misc4, DateTimeGPS_UTC from #ResultTable where WorkHour != -100)

					update #ResultTable
						set WorkHour = -100
					from #ResultTable T1
						inner join (select ParentRuleIDTriggered, Misc2
										from #ResultTable where WorkHour = -100
										group by ParentRuleIDTriggered, Misc2
									) T2 on T1.ParentRuleIDTriggered = T2.ParentRuleIDTriggered and T1.Misc2 = T2.Misc2
					where T1.WorkHour = 0

					insert GFI_GPS_ExceptionsHeader (RuleID, AssetID, GroupID, dateFrom, DateTo)
						select ParentRuleIDTriggered, AssetID, Misc2, min(DateTimeGPS_UTC) dtFr, max(DateTimeGPS_UTC) dtTo
							from #ResultTable 
						where WorkHour != -100
							Group by ParentRuleIDTriggered, AssetID, Misc2

					update GFI_GPS_ExceptionsHeader
						set dateFrom = T2.dtFr, dateTo = T2.dtTo
					from GFI_GPS_ExceptionsHeader T1
						inner join (select RuleID, AssetID, GroupID, min(DateTimeGPS_UTC) dtFr, max(DateTimeGPS_UTC) dtTo, min(InsertedAt) InsertedAt
										from GFI_GPS_Exceptions where AssetID = @ActualAssetID 
										Group by RuleID, AssetID, GroupID
									) T2 on T1.AssetID = T2.AssetID and T1.GroupID = T2.GroupID and T1.RuleID = T2.RuleID
					where DATEADD(day, -3, T2.InsertedAt) <= GETDATE() --and T2.AssetID = @ActualAssetID

					delete from GFI_GPS_ProcessPending where AssetID = @MyAssetID and ProcessCode = 'Exceptions' and Processing = 1
				commit tran
				set @InTran = 0

				IF OBJECT_ID('tempdb..#Stopped') IS NOT NULL DROP TABLE #Stopped;
				drop table #ZoneHeader
				drop table #OldException
				drop table #GFI_GPS_GPSDataDetail
				drop table #GFI_GPS_GPSData 
				drop table #GFI_GPS_GPSDataWithDetails
			end try
			begin catch
				if(@InTran = 1)
					begin
						rollback tran
					end

				begin try
					insert GFI_GPS_ProcessErrors (sError, sOrigine, sFieldName, sFieldValue, AssetID, dtUTC) values (ERROR_MESSAGE() + ' - Error occured at: ' + CONVERT(VARCHAR(20),  ERROR_LINE()), 'Exceptions', 'ParentRuleID', @ParentRuleID, @MyAssetID, @dtStart)
				
					close RulesCursor
					deallocate RulesCursor
					close ProcessingRuleCursor
					deallocate ProcessingRuleCursor

					drop table #Stopped
					drop table #ZoneHeader
					drop table #GFI_GPS_GPSData 
					drop table #GFI_GPS_GPSDataWithDetails
					drop table #OuterT
					drop table #AllRows
					drop table #AllRowsPrepared
					drop table #GrpDuration
					drop table #OldException
				end try
				begin catch
				end catch
			end catch
			fetch next from ProcessCursor into @MyAssetID, @dtStart
		END
	close ProcessCursor
	deallocate ProcessCursor
	drop table #AssetFrmGMID
    drop table #ExTemp
    drop table #ResultTable
end
";
            return sql;
        }
		public static String sProcessExceptionsPostgreSQL(int iAssetID)
		{
			String sql = @"
--Exceptions Processor
--Rule Types
--0   ZoneTypes
--1   Zones
--2   Assets
--3   Drivers
--4   Speed
--5   Time
--6   Auxilaries
--7   Duration
--8   ExceptionType, EngineAux, Aux
--9   Fuel
--10  RoadSpeed
--11  Day

drop table if exists _ResultTable;
drop table if exists _zoneheader;
drop table if exists _assetfrmgmid;
drop table if exists _stopped;
drop table if exists _extemp;
drop table if exists _temprle;
drop table if exists _gfi_gps_gpsdatadetail ;
drop table if exists _gfi_gps_gpsdata;
drop table if exists _GFI_GPS_GPSDataWithDetails;
drop table if exists _t;
drop table if exists _inallzone;
drop table if exists _inzone;
drop table if exists _zonedata;
drop table if exists _tmp10;
drop table if exists _tmp11;
drop table if exists _tmp12;
drop table if exists _GrpDuration;
drop table if exists _OldException;
drop table if exists _GFI_FLT_Asset;

select * into temp _GFI_FLT_Asset from GFI_FLT_Asset for share;
do $$
--Asset : _ActualAssetID := 1;	Exceptions Processor

begin
	Create temp table _ZoneHeader (ZoneID int);
    create temp table _AssetFrmGMID (GMID int, GMDescription varchar(1000), ParentGMID int, StrucID int);
	Create temp table _Stopped (myRowNum int);

	CREATE temp TABLE _ExTemp 
	(
		RowNum int,
		UID int ,
		ParentRuleID int NULL,
		RuleID int NULL,
		RuleType int null,
		StrucID varchar(50),
		Others varchar(100)
	);

	Create temp Table _ResultTable
	(
		ParentRuleIDTriggered int,
		RowNumber int NOT NULL,
		UID int, --PRIMARY KEY CLUSTERED,
		AssetID int NULL,
		DateTimeGPS_UTC Timestamp NULL,
		DateTimeServer Timestamp NULL,
		Longitude float NULL,
		Latitude float NULL,
		LongLatValidFlag int NULL,
		Speed float NULL,
		EngineOn int NULL,
		StopFlag int NULL,
		TripDistance float NULL,
		TripTime float NULL,
		WorkHour int NULL,
		DriverID int NOT NULL,
		Misc1 varchar(100) null,
		Misc2 int null,
		Misc3 int null,
		RoadSpeed float NULL,
		Misc4 int null
	);
	CREATE INDEX ix1 ON _ResultTable (Misc1, Misc2);

	create temp TABLE _TempRle (ParentRuleID int NULL);

	--Working table. GPSData_cursor from the same working table
	create temp table _GFI_GPS_GPSData 
	(
		RowNumber int GENERATED ALWAYS AS IDENTITY,
		UID int PRIMARY KEY,
		AssetID int NULL,
		DateTimeGPS_UTC timestamptz NULL,
		DateTimeServer timestamptz NULL,
		Longitude float NULL,
		Latitude float NULL,
		LongLatValidFlag int NULL,
		Speed float NULL,
		EngineOn int NULL,
		StopFlag int NULL,
		TripDistance float NULL,
		TripTime float NULL,
		WorkHour int NULL,
		DriverID int NOT NULL,
		Misc1 varchar(100) null,
		Misc2 int null,
		Misc3 int null,
		RoadSpeed float NULL
	);
	CREATE INDEX _myIndex ON _GFI_GPS_GPSData 
	(
		AssetID ASC,
		DateTimeGPS_UTC ASC,
		LongLatValidFlag ASC,
		StopFlag ASC,
		Speed ASC
	);
	CREATE INDEX _xxx ON _GFI_GPS_GPSData (LongLatValidFlag, DateTimeGPS_UTC, DriverID, RowNumber, UID);
	CREATE INDEX _yyy ON _GFI_GPS_GPSData (AssetID, LongLatValidFlag, DateTimeGPS_UTC, RowNumber, UID);

	CREATE temp TABLE _GFI_GPS_GPSDataDetail
	(
		GPSDetailID bigint NOT NULL,
		UID int NULL,
		TypeID varchar(30) NULL,
		TypeValue varchar(30) NULL,
		UOM varchar(15) NULL,
		Remarks varchar(50) NULL
	);
	ALTER TABLE _GFI_GPS_GPSDataDetail  ADD CONSTRAINT FK FOREIGN KEY(UID)
		REFERENCES _GFI_GPS_GPSData (UID)
		ON DELETE CASCADE;
	CREATE INDEX _MyIdxDTripProcessor ON _GFI_GPS_GPSDataDetail(TypeID, UID, TypeValue, UOM);

	create temp table _GFI_GPS_GPSDataWithDetails 
	(
		RowNumber int GENERATED ALWAYS AS IDENTITY,
		UID int, --PRIMARY KEY CLUSTERED,
		AssetID int NULL,
		DateTimeGPS_UTC timestamptz NULL,
		DateTimeServer timestamptz NULL,
		Longitude float NULL,
		Latitude float NULL,
		LongLatValidFlag int NULL,
		Speed float NULL,
		EngineOn int NULL,
		StopFlag int NULL,
		TripDistance float NULL,
		TripTime float NULL,
		WorkHour int NULL,
		DriverID int NOT NULL,
		TypeID varchar(100) null,
		TypeValue varchar(100) null,
		UOM varchar(100) null
	);
	CREATE INDEX _myIndexDetail ON _GFI_GPS_GPSDataWithDetails 
	(
		AssetID ASC,
		DateTimeGPS_UTC ASC,
		LongLatValidFlag ASC,
		StopFlag ASC,
		Speed ASC, TypeID ASC, UID,TypeValue
	);

	create temp table _t (i int);

	create temp table _InAllZone 
	(
		RowNumber int null,
		UID int ,--PRIMARY KEY,
		AssetID int NULL,
		DateTimeGPS_UTC timestamptz NULL,
		DateTimeServer timestamptz NULL,
		Longitude float NULL,
		Latitude float NULL,
		LongLatValidFlag int NULL,
		Speed float NULL,
		EngineOn int NULL,
		StopFlag int NULL,
		TripDistance float NULL,
		TripTime float NULL,
		WorkHour int NULL,
		DriverID int NOT NULL,
		Misc1 varchar(100) null,
		Misc2 int null,
		Misc3 int null,
		RoadSpeed float NULL, ZoneID int null, InZone int null
	);

	create temp table _InZone 
	(
		RowNumber int null,
		UID int PRIMARY KEY,
		AssetID int NULL,
		DateTimeGPS_UTC timestamptz NULL,
		DateTimeServer timestamptz NULL,
		Longitude float NULL,
		Latitude float NULL,
		LongLatValidFlag int NULL,
		Speed float NULL,
		EngineOn int NULL,
		StopFlag int NULL,
		TripDistance float NULL,
		TripTime float NULL,
		WorkHour int NULL,
		DriverID int NOT NULL,
		Misc1 varchar(100) null,
		Misc2 int null,
		Misc3 int null,
		RoadSpeed float NULL, ZoneID int null, InZone int null
	);

	create temp table _ZoneData 
	(
		RowNumber int null,
		UID int PRIMARY KEY,
		AssetID int NULL,
		DateTimeGPS_UTC timestamptz NULL,
		DateTimeServer timestamptz NULL,
		Longitude float NULL,
		Latitude float NULL,
		LongLatValidFlag int NULL,
		Speed float NULL,
		EngineOn int NULL,
		StopFlag int NULL,
		TripDistance float NULL,
		TripTime float NULL,
		WorkHour int NULL,
		DriverID int NOT NULL,
		Misc1 varchar(100) null,
		Misc2 int null,
		Misc3 int null,
		RoadSpeed float NULL, ZoneID int null, InZone int null
	);

	Create temp Table _tmp10
	(
		ParentRuleIDTriggered int,
		RowNumber int NOT NULL,
		UID int, --PRIMARY KEY CLUSTERED,
		AssetID int NULL,
		DateTimeGPS_UTC Timestamp NULL,
		DateTimeServer Timestamp NULL,
		Longitude float NULL,
		Latitude float NULL,
		LongLatValidFlag int NULL,
		Speed float NULL,
		EngineOn int NULL,
		StopFlag int NULL,
		TripDistance float NULL,
		TripTime float NULL,
		WorkHour int NULL,
		DriverID int NOT NULL,
		Misc1 varchar(100) null,
		Misc2 int null,
		Misc3 int null,
		RoadSpeed float NULL,
		Misc4 int null
		, PreviousRN int null
		, ActualRN int null
		, NextRN int null
		, GrpID int default 0
	);
	
	Create temp Table _tmp11
	(
		ParentRuleIDTriggered int,
		RowNumber int NOT NULL,
		UID int, --PRIMARY KEY CLUSTERED,
		AssetID int NULL,
		DateTimeGPS_UTC Timestamp NULL,
		DateTimeServer Timestamp NULL,
		Longitude float NULL,
		Latitude float NULL,
		LongLatValidFlag int NULL,
		Speed float NULL,
		EngineOn int NULL,
		StopFlag int NULL,
		TripDistance float NULL,
		TripTime float NULL,
		WorkHour int NULL,
		DriverID int NOT NULL,
		Misc1 varchar(100) null,
		Misc2 int null,
		Misc3 int null,
		RoadSpeed float NULL,
		Misc4 int null
		, PreviousRN int null
		, ActualRN int null
		, NextRN int null
		, GrpID int default 0
		, grpSemiFinal int null
	);

	Create temp Table _tmp12
	(
		ParentRuleIDTriggered int,
		RowNumber int NOT NULL,
		UID int, --PRIMARY KEY CLUSTERED,
		AssetID int NULL,
		DateTimeGPS_UTC Timestamp NULL,
		DateTimeServer Timestamp NULL,
		Longitude float NULL,
		Latitude float NULL,
		LongLatValidFlag int NULL,
		Speed float NULL,
		EngineOn int NULL,
		StopFlag int NULL,
		TripDistance float NULL,
		TripTime float NULL,
		WorkHour int NULL,
		DriverID int NOT NULL,
		Misc1 varchar(100) null,
		Misc2 int null,
		Misc3 int null,
		RoadSpeed float NULL,
		Misc4 int null
		, PreviousRN int null
		, ActualRN int null
		, NextRN int null
		, GrpID int default 0
		, grpSemiFinal int null
		, grpFinal int null
	);

	create temp table _GrpDuration
	(
		Misc2 int null, ParentRuleIDTriggered int null, mini Timestamp NULL, maxi Timestamp NULL, Duration int default 0
	);

	create temp table _OldException
	( 
		iID int  NULL,
		RuleID int NULL,
		GPSDataID bigint NULL,
		AssetID int NULL,
		DriverID int NULL,
		Severity varchar(100) NULL,
		GroupID int NULL,
		Posted int NOT NULL,
		InsertedAt Timestamp NOT NULL,
		DateTimeGPS_UTC Timestamp NOT NULL
	);

	--Declaring all variables
	declare _ActualAssetID int;
	declare _dtStart Timestamp;
	declare _dtStartClone Timestamp;
	declare _dtStartWithDuration Timestamp;
	declare _dtOfExistingExceptions Timestamp;

	DECLARE	_RowNum int;
	DECLARE	_UID int;
	DECLARE	_AssetID int;
	DECLARE	_DateTimeGPS_UTC Timestamp; 
	DECLARE	_Longitude float; 
	DECLARE	_Latitude float;
	DECLARE	_Speed float; 

	--RulesCursor
	declare _ParentRuleID int;
	declare _RuleID int;
	declare _RuleType int;
	Declare _StrucType varchar(50);
	Declare _StrucValue varchar(50);
	Declare _StrucCondition varchar(50);
	Declare _Struc varchar(50);
	Declare _WhereCondition varchar(2000);
	Declare _TmpCondition varchar(2000);
	Declare _MinTriggerValue int;
	DECLARE _SQLQuery varchar(2000);

	declare _RowCnt int;
	Declare _iDuration int;
	Declare _iRule int;
	declare _bDetailsNeed int;
	Declare _myTime varchar(50);
	Declare _PreviousLongitude float;
	Declare _PreviousLatitude float;
	Declare _NextLongitude float;
	Declare _NextLatitude float;
	Declare _EngineAuxCondition varchar(2000);
	Declare _EngineAuxStrucCondition varchar(2000);
	declare _MaxGrpID int;
	DECLARE _ParentRuleIDTriggered int;
	DECLARE _Misc1 varchar(100);
	DECLARE _Misc3 int;

	declare _StopQry varchar(1500);
	declare _myStopRowNum int;
	declare _myPreviousStopRowNum int;
	Declare _InTran int;

	declare total_rows int;
	declare _maxRows int;
	declare _continueProcess int;

	Declare	ProcessRecord RECORD;
	Declare	RulesRecord RECORD;
	
	begin
		_ActualAssetID := 1;
		_maxRows = 500;
		_InTran = 0;
		_dtOfExistingExceptions = now();
 
		update GFI_GPS_ProcessPending set Processing = 1
			where ProcessCode = 'Exceptions' and AssetID = _ActualAssetID
				and Processing = 0;

		DECLARE ProcessCursor CURSOR FOR 
			SELECT GFI_GPS_ProcessPending.assetid, MIN(dtdatefrom) MinDate
				FROM GFI_GPS_ProcessPending where ProcessCode = 'Exceptions' and Processing = 1 and AssetID = _ActualAssetID
			GROUP BY GFI_GPS_ProcessPending.assetid;

		Begin
			--Working with 1 Asset at a time
			open ProcessCursor;
				LOOP
				FETCH ProcessCursor INTO ProcessRecord;
				EXIT WHEN NOT FOUND;  
					raise notice 'AssetID %, MinDate %', ProcessRecord.AssetID, ProcessRecord.MinDate;

					begin
						_dtStart := (
										select Coalesce(max(DateTimeGPS_UTC), ProcessRecord.MinDate)
											from GFI_GPS_GPSData 
											where AssetID = ProcessRecord.AssetID
												and DateTimeGPS_UTC < ProcessRecord.MinDate
										);

						--if asset has a durarion rule, retrieve data from ProcessRecord.MinDate - duration period
						delete from _TempRle;
						WITH RECURSIVE tree AS (		
							SELECT GMID, 
									GMDescription, 
									ParentGMID,
									1 as level 
							FROM GFI_SYS_GroupMatrix
							WHERE GMID in (SELECT m.GMID
												from GFI_SYS_GroupMatrixAsset m 
													inner join GFI_SYS_GroupMatrix gm on m.GMID = gm.GMID
													inner join _GFI_FLT_Asset a on m.iID = a.AssetID
												WHERE a.AssetID = ProcessRecord.AssetID FOR UPDATE OF a SKIP LOCKED
											)
							UNION ALL 
							SELECT p.GMID,
									p.GMDescription,
									p.ParentGMID, 
									t.level + 1
							FROM GFI_SYS_GroupMatrix p
								JOIN tree t ON t.ParentGMID = p.GMID
						)
						insert into _TempRle
							SELECT r.ParentRuleID		--By GMID
								FROM tree, GFI_GPS_Rules r	
							where tree.GMID = cast(r.StrucValue as int) and StrucType = 'GMID'
							union								
							select ParentRuleID			--By AssetID
								from GFI_GPS_Rules where StrucValue = cast(ProcessRecord.AssetID as text) and StrucType = 'ID';

						_iDuration := (
										select _iDuration = max(try_cast_int(StrucValue)) from _TempRle t
											inner join GFI_GPS_Rules r on t.ParentRuleID = r.ParentRuleID
										where r.Struc = 'Duration'							
										);

						_dtStart := (
										select Coalesce(max(DateTimeGPS_UTC), _dtStart)
											from GFI_GPS_GPSData 
											where AssetID = ProcessRecord.AssetID
												and DateTimeGPS_UTC < DATEADD('ss', -_iDuration::int, _dtStart)
										);

						_dtStartClone := _dtStart;
						_dtStartWithDuration := _dtStart;
						if(_dtStartWithDuration < _dtOfExistingExceptions) then
							_dtOfExistingExceptions = _dtStartWithDuration;
						end if;

						raise notice 'AssetID %, MinDate %, _dtStart %', ProcessRecord.AssetID, ProcessRecord.MinDate, _dtStart;
					end;

					delete from _GFI_GPS_GPSData;
					insert into _GFI_GPS_GPSData (UID, AssetID, DateTimeGPS_UTC, DateTimeServer, Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, StopFlag, TripDistance, TripTime, WorkHour, DriverID, RoadSpeed)
						select UID, AssetID, DateTimeGPS_UTC, DateTimeServer, Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, StopFlag, TripDistance, TripTime, WorkHour, DriverID, RoadSpeed from GFI_GPS_GPSData 
							where AssetID = ProcessRecord.AssetID 
								and DateTimeGPS_UTC >= _dtStart
							ORDER BY DateTimeGPS_UTC limit _maxRows;
                    
                   _continueProcess := 0;
                    perform * from _GFI_GPS_GPSData;
                    GET DIAGNOSTICS total_rows := ROW_COUNT;
                    if(total_rows > 0) then
                        _continueProcess = 1;

						if(total_rows >= _maxRows - 1) then
							insert into GFI_GPS_ProcessPending (AssetID, dtdatefrom, ProcessCode) 
								values (ProcessRecord.AssetID, (select max(DateTimeGPS_UTC) + interval '1 second' from _GFI_GPS_GPSData ), 'Exceptions');
						end if;

						delete from _GFI_GPS_GPSDataWithDetails;
						delete from _ExTemp;
						delete from _ResultTable;

						--This cursor gets all rules applied to the asset, including driver
						declare RulesCursor CURSOR FOR
							WITH RECURSIVE tree AS (		
								SELECT GMID, 
										GMDescription, 
										ParentGMID,
										1 as level 
								FROM GFI_SYS_GroupMatrix
								WHERE GMID in (SELECT m.GMID
													from GFI_SYS_GroupMatrixAsset m 
														inner join GFI_SYS_GroupMatrix gm on m.GMID = gm.GMID
														inner join _GFI_FLT_Asset a on m.iID = a.AssetID
													WHERE a.AssetID = ProcessRecord.AssetID FOR UPDATE OF a SKIP LOCKED
												)
								UNION ALL 
								SELECT p.GMID,
										p.GMDescription,
										p.ParentGMID, 
										t.level + 1
								FROM GFI_SYS_GroupMatrix p
									JOIN tree t ON t.ParentGMID = p.GMID
							)
							SELECT r.ParentRuleID		--By GMID
								FROM tree, GFI_GPS_Rules r	
							where tree.GMID = cast(r.StrucValue as int) and StrucType = 'GMID'
							union								
							select ParentRuleID			--By AssetID
								from GFI_GPS_Rules where StrucValue = cast(ProcessRecord.AssetID as text) and StrucType = 'ID';

                        --Processing 1 rule at a time
                        begin
                            open RulesCursor;
                                LOOP
                                FETCH RulesCursor INTO RulesRecord;
                                EXIT WHEN NOT FOUND; 
                                    begin
                                        --check if rule  is valid
                                        _iRule := (select count(distinct(Struc)) from GFI_GPS_Rules where ParentRuleID = RulesRecord.ParentRuleID);
                                        if(_iRule < 3) then
                                            fetch next from RulesCursor into RulesRecord;
                                        end if;

                                        _ParentRuleID := RulesRecord.ParentRuleID;
                                        _dtStart := _dtStartClone;

                                        --if rule has duration, set dtStart accordingly
                                        perform * from GFI_GPS_Rules where ParentRuleID = RulesRecord.ParentRuleID and RuleType = 7 and Struc = 'Duration';
                                        GET DIAGNOSTICS total_rows := ROW_COUNT;
                                        if(total_rows > 0) 
                                        then
                                            _iDuration := null;
                                            _iDuration = (select try_cast_int(StrucValue) from GFI_GPS_Rules where ParentRuleID = RulesRecord.ParentRuleID and Struc = 'Duration');

                                            _dtStart := (select Coalesce(max(DateTimeGPS_UTC), _dtStart)
																from GFI_GPS_GPSData 
																where AssetID = ProcessRecord.AssetID
																	and DateTimeGPS_UTC < DATEADD('ss', -_iDuration, _dtStart)
                                                        );
											raise notice '_dtStart %, _iDuration %', _dtStart, _iDuration;
                                        end if;
                                        raise notice 'ParentRuleID % AssetID % _dtStart % @ %', RulesRecord.ParentRuleID, ProcessRecord.AssetID, _dtStart, now();

                                        --insert _GFI_GPS_GPSDataDetail if needed
                                        begin
                                            delete from _GFI_GPS_GPSDataDetail;
                                            _bDetailsNeed := 0;

                                            perform * from GFI_GPS_Rules where ParentRuleID = RulesRecord.ParentRuleID and RuleType = 6 and Struc = 'Aux';
                                            GET DIAGNOSTICS total_rows := ROW_COUNT;
                                            if(total_rows > 0)
                                            then
                                                _bDetailsNeed := 1;
                                            end if;

                                            perform * from GFI_GPS_Rules where ParentRuleID = RulesRecord.ParentRuleID and RuleType = 6 and Struc = 'EngineAux';
                                            GET DIAGNOSTICS total_rows := ROW_COUNT;
                                            if(total_rows > 0)
                                            then
                                                _bDetailsNeed := 1;
                                            end if;

                                            if(_bDetailsNeed = 1)
                                            then
                                                insert into _GFI_GPS_GPSDataDetail
                                                    select d.*
                                                        from _GFI_GPS_GPSData g
                                                            inner join GFI_GPS_GPSDataDetail d on g.UID = d.UID 
                                                        where g.AssetID = ProcessRecord.AssetID 
                                                            and g.DateTimeGPS_UTC >= _dtStart;
                                            end if;
                                        end;

                                        --insert _ExTemp for each and every condition, except duration
                                        _WhereCondition := '';

                                        -- Asset
                                        --if(1=0)
                                        raise notice 'Asset';
                                        perform * from GFI_GPS_Rules where ParentRuleID = RulesRecord.ParentRuleID and RuleType = 2;
                                        GET DIAGNOSTICS total_rows := ROW_COUNT;
                                        if(total_rows > 0)
                                        then
                                            _RowCnt := 0;
                                            _WhereCondition := '';
                                            delete from _AssetFrmGMID;
                                            _RuleType = 2;
                                            _Struc = 'Assets';

                                            WITH RECURSIVE
                                                parent AS 
                                                (
                                                    SELECT distinct ParentGMID
                                                    FROM GFI_SYS_GroupMatrix m
                                                        inner join GFI_GPS_Rules r on m.ParentGMID = cast(r.StrucValue as int)
                                                    WHERE ParentRuleID = RulesRecord.ParentRuleID 
                                                        and RuleType = _RuleType and StrucType = 'GMID'
                                                ), 
                                                tree AS 
                                                (
                                                    SELECT 
                                                        x.GMID
                                                        , x.GMDescription
                                                        , x.ParentGMID
                                                    FROM GFI_SYS_GroupMatrix x
                                                    INNER JOIN parent ON x.ParentGMID = parent.ParentGMID
                                                    UNION ALL
                                                    SELECT 
                                                        y.GMID
                                                        , y.GMDescription
                                                        , y.ParentGMID
                                                    FROM GFI_SYS_GroupMatrix y
                                                    INNER JOIN tree t ON y.ParentGMID = t.GMID
                                                )
                                            insert into _AssetFrmGMID
                                                SELECT Distinct GMID, GMDescription, ParentGMID, -2 StrucID FROM tree
                                                union all
                                                    SELECT z.GMID, z.GMDescription, -1, -3 Struc
                                                    FROM GFI_SYS_GroupMatrix z 
                                                        inner join GFI_GPS_Rules r on z.GMID = cast(r.StrucValue as int)
                                                    WHERE ParentRuleID = RulesRecord.ParentRuleID 
                                                        and RuleType = _RuleType and StrucType = 'GMID'
                                                union all
                                                    select -4, a.AssetNumber, tree.GMID as ParentGMID, a.AssetID
                                                        from _GFI_FLT_Asset a 
                                                            inner join GFI_SYS_GroupMatrixAsset m on a.AssetID = m.iID
                                                            inner join tree on tree.GMID = m.GMID 
                                                union all
                                                    select -5, a.AssetNumber, m.GMID as ParentGMID, a.AssetID 
                                                    from _GFI_FLT_Asset a --(readpast)
                                                        inner join GFI_SYS_GroupMatrixAsset m on a.AssetID = m.iID
                                                        inner join GFI_GPS_Rules r on m.GMID = cast(r.StrucValue as int)
                                                    WHERE ParentRuleID = RulesRecord.ParentRuleID 
                                                        and RuleType = _RuleType and StrucType = 'GMID';
        
                                            perform StrucValue from GFI_GPS_Rules where ParentRuleID = RulesRecord.ParentRuleID 
                                                and RuleType = _RuleType and StrucType = 'ID' and StrucValue = cast(ProcessRecord.AssetID as varchar(20))
                                            union
                                            select cast(StrucID as varchar(20)) from _AssetFrmGMID where StrucID > 0 and StrucID = ProcessRecord.AssetID;
                                            GET DIAGNOSTICS _RowCnt := ROW_COUNT;
                                
                                            if(_RowCnt > 0)
                                            then
                                                _WhereCondition := ProcessRecord.AssetID;
                                            end if;

                                            if(length(_WhereCondition) > 0)
                                            then
                                                --raise notice 'whereCond %', _WhereCondition;

                                                _SQLQuery := 'select RowNumber, UID, ' 
                                                    || cast (RulesRecord.ParentRuleID as varchar(50)) || ', ' 
                                                    || cast (RulesRecord.ParentRuleID as varchar(50)) || ', '
                                                    || cast (_RuleType as varchar(50)) || ', '
                                                    || '''' || _Struc || '''' || ', '
                                                    || '''Assets'''
                                                    || ' from _GFI_GPS_GPSData where AssetID = ' || _WhereCondition  
                                                    || ' and LongLatValidFlag = 1'
                                                    || ' and DateTimeGPS_UTC >= ''' || _dtStart || '''';
                                                
                                                EXECUTE 'insert into _ExTemp ' || _SQLQuery; 
                                            end if;
                                        end if;

                                        -- Driver
                                        --if(1=0)
                                        raise notice 'Driver';
                                        perform * from GFI_GPS_Rules where ParentRuleID = RulesRecord.ParentRuleID and RuleType = 3;
                                        GET DIAGNOSTICS total_rows := ROW_COUNT;
                                        if(total_rows > 0)
                                        then
                                            _RowCnt := 0;
                                            _WhereCondition := '';
                                            delete from _AssetFrmGMID;
                                            _RuleType := 3;
                                            _Struc := 'Drivers';
                                            _SQLQuery := 'Drivers';

                                            WITH RECURSIVE
                                                parent AS 
                                                (
                                                    SELECT distinct ParentGMID
                                                    FROM GFI_SYS_GroupMatrix m
                                                        inner join GFI_GPS_Rules r on m.ParentGMID = cast(r.StrucValue as int)
                                                    WHERE ParentRuleID = RulesRecord.ParentRuleID 
                                                        and RuleType = _RuleType and StrucType = 'GMID'
                                                ), 
                                                tree AS 
                                                (
                                                    SELECT 
                                                        x.GMID
                                                        , x.GMDescription
                                                        , x.ParentGMID
                                                    FROM GFI_SYS_GroupMatrix x
                                                    INNER JOIN parent ON x.ParentGMID = parent.ParentGMID
                                                    UNION ALL
                                                    SELECT 
                                                        y.GMID
                                                        , y.GMDescription
                                                        , y.ParentGMID
                                                    FROM GFI_SYS_GroupMatrix y
                                                    INNER JOIN tree t ON y.ParentGMID = t.GMID
                                                )
                                            insert into _AssetFrmGMID
                                                SELECT Distinct GMID, GMDescription, ParentGMID, -2 StrucID FROM tree
                                                union all
                                                    SELECT z.GMID, z.GMDescription, -1, -3 Struc
                                                    FROM GFI_SYS_GroupMatrix z 
                                                        inner join GFI_GPS_Rules r on z.GMID = cast(r.StrucValue as int)
                                                    WHERE ParentRuleID = RulesRecord.ParentRuleID 
                                                        and RuleType = _RuleType and StrucType = 'GMID'
                                                union all
                                                    select -4, a.sDriverName, tree.GMID as ParentGMID, a.DriverID
                                                        from GFI_FLT_Driver a, GFI_SYS_GroupMatrixDriver m, tree
                                                    where tree.GMID = m.GMID and a.DriverID = m.iID
                                                union all
                                                    select -5, a.sDriverName, m.GMID as ParentGMID, a.DriverID 
                                                    from GFI_FLT_Driver a
                                                        inner join GFI_SYS_GroupMatrixDriver m on a.DriverID = m.iID
                                                        inner join GFI_GPS_Rules r on m.GMID = cast(r.StrucValue as int)
                                                    WHERE ParentRuleID = RulesRecord.ParentRuleID 
                                                        and RuleType = _RuleType and StrucType = 'GMID';
            
                                            insert into _AssetFrmGMID
                                                select -1, -1, -1, StrucValue::integer from GFI_GPS_Rules where ParentRuleID = RulesRecord.ParentRuleID 
                                                    and RuleType = _RuleType and StrucType = 'ID';

                                            _SQLQuery := 'select RowNumber, UID, ' 
                                                || cast(RulesRecord.ParentRuleID as varchar(50)) || ', ' 
                                                || cast(RulesRecord.ParentRuleID as varchar(50)) || ', '
                                                || cast(_RuleType as varchar(50)) || ', '
                                                || '''' || _Struc || '''' || ', '
                                                || '''Driver'''
                                                || ' from _GFI_GPS_GPSData where DriverID in (select StrucID from _AssetFrmGMID where StrucID > 0)'  
                                                || ' and LongLatValidFlag = 1'
                                                || ' and DateTimeGPS_UTC >= ''' || _dtStart || '''';
                                            --print _SQLQuery
                                            EXECUTE 'insert into _ExTemp ' || _SQLQuery; 
                                        end if;

                                        --Time
                                        --select * from GFI_GPS_GPSData where CONVERT(varchar, DateTimeGPS_UTC,108) BETWEEN '06:00:00' AND '07:00:00'
                                        --if(1=0)
                                        raise notice 'Time';
                                        perform * from GFI_GPS_Rules where ParentRuleID = RulesRecord.ParentRuleID and RuleType = 5;
                                        GET DIAGNOSTICS total_rows := ROW_COUNT;
                                        if(total_rows > 0)
                                        then
                                            _WhereCondition := '';
                                            _SQLQuery := 'Time';

                                            Declare	ProcessingRuleRecord RECORD;
                                            Declare ProcessingRuleCursor cursor for
                                                select RuleID, RuleType, Struc, StrucCondition, StrucValue from GFI_GPS_Rules where ParentRuleID = RulesRecord.ParentRuleID
                                                    and RuleType = 5 order by MinTriggerValue;

                                            Begin
                                                open ProcessingRuleCursor;
                                                    LOOP
                                                    FETCH ProcessingRuleCursor INTO ProcessingRuleRecord;
                                                    EXIT WHEN NOT FOUND;  
                                                        _myTime := ProcessingRuleRecord.StrucValue::integer * interval '1 second';
                                                        if (_WhereCondition = '')
                                                        then
                                                            _WhereCondition := 'DateTimeGPS_UTC::time BETWEEN time ''' || _myTime || ''' and time ''';
                                                        else
                                                            _WhereCondition := _WhereCondition || _myTime || '''';
                                                        end if;

                                                    END LOOP;
                                                close ProcessingRuleCursor;

                                                if(length(_WhereCondition) > 0)
                                                then
                                                    --print 'whereCond ' + _WhereCondition
                                                    _SQLQuery := 'select RowNumber, UID, ' 
                                                        || cast(RulesRecord.ParentRuleID as varchar(50)) || ', ' 
                                                        || cast(RulesRecord.ParentRuleID as varchar(50)) || ', '
                                                        || cast(5 as varchar(50)) || ', '
                                                        || '''' || 'Time' || '''' || ', '
                                                        || '''nothing'''
                                                        || ' from _GFI_GPS_GPSData where AssetID = ' || cast(ProcessRecord.AssetID as varchar(50)) 
                                                        || ' and LongLatValidFlag = 1'
                                                        || ' and DateTimeGPS_UTC >= ''' || cast(_dtStart as varchar(50)) 
														|| ''' and '
                                                        || ' ( ' || _WhereCondition || ')';
                                                    --print _SQLQuery
                                                    EXECUTE 'insert into _ExTemp ' || _SQLQuery; 
                                                end if;
                                            end;
                                        end if;


                                        --Day
                                        --select * from GFI_GPS_GPSData where CONVERT(varchar, DateTimeGPS_UTC,108) BETWEEN '06:00:00' AND '07:00:00'
                                        --if(1=0)
                                        raise notice 'Day';
                                        perform * from GFI_GPS_Rules where ParentRuleID = RulesRecord.ParentRuleID and RuleType = 11;
                                        GET DIAGNOSTICS total_rows := ROW_COUNT;
                                        if(total_rows > 0)
                                        then
                                            _WhereCondition := '';
                                            _SQLQuery := 'Day';

                                            Declare	ProcessingRuleRecord RECORD;
                                            Declare ProcessingRuleCursor cursor for
                                                select RuleID, RuleType, Struc, StrucCondition, StrucValue from GFI_GPS_Rules where ParentRuleID = RulesRecord.ParentRuleID
                                                    and RuleType = 11 order by MinTriggerValue;

                                            Begin
                                                open ProcessingRuleCursor;
                                                    LOOP
                                                    FETCH ProcessingRuleCursor INTO ProcessingRuleRecord;
                                                    EXIT WHEN NOT FOUND;  
                                                        _WhereCondition := 'trim(to_char(DateTimeGPS_UTC, ''day'')) in (' || ProcessingRuleRecord.StrucCondition || ')';
                                                    END LOOP;
                                                close ProcessingRuleCursor;

                                                if(length(_WhereCondition) > 0)
                                                then
                                                    --print 'whereCond ' + _WhereCondition
                                                    _SQLQuery := 'select RowNumber, UID, ' 
                                                        || cast(RulesRecord.ParentRuleID as varchar(50)) || ', ' 
                                                        || cast(RulesRecord.ParentRuleID as varchar(50)) || ', '
                                                        || cast(11 as varchar(50)) || ', '
                                                        || '''' || 'Day' || '''' || ', '
                                                        || '''nothing'''
                                                        || ' from _GFI_GPS_GPSData where AssetID = ' || cast(ProcessRecord.AssetID as varchar(50)) 
                                                        || ' and LongLatValidFlag = 1'
                                                        || ' and DateTimeGPS_UTC >= ''' || cast(_dtStart as varchar(50)) 
														|| ''' and '
                                                        || ' ( ' || _WhereCondition || ')';
                                                    --print _SQLQuery
                                                    EXECUTE 'insert into _ExTemp ' || _SQLQuery; 
                                                end if;
                                            end;
                                        end if;


                                        -- Speed
                                        --if(1=0)
                                        raise notice 'Speed';
                                        perform * from GFI_GPS_Rules where ParentRuleID = RulesRecord.ParentRuleID and RuleType = 4;
                                        GET DIAGNOSTICS total_rows := ROW_COUNT;
                                        if(total_rows > 0)
                                        then
                                            _WhereCondition := '';
                                            _SQLQuery := 'Speed';

                                            Declare	ProcessingRuleRecord RECORD;
                                            Declare ProcessingRuleCursor cursor for
                                                select RuleID, RuleType, Struc, StrucCondition, StrucValue, StrucType, MinTriggerValue from GFI_GPS_Rules where ParentRuleID = RulesRecord.ParentRuleID 
                                                    and RuleType = 4;

                                            Begin
                                                open ProcessingRuleCursor;
                                                    LOOP
                                                    FETCH ProcessingRuleCursor INTO ProcessingRuleRecord;
                                                    EXIT WHEN NOT FOUND;  
                                                        _TmpCondition = ' ';
                                                        if(_WhereCondition <> '')
                                                        then
                                                            _TmpCondition = ' or ';

                                                            if(ProcessingRuleRecord.MinTriggerValue = 2) then
                                                                _TmpCondition = ' and ';
                                                            end if;
                                                        end if;

                                                        _WhereCondition := _WhereCondition || _TmpCondition || ProcessingRuleRecord.Struc || ProcessingRuleRecord.StrucCondition || cast(ProcessingRuleRecord.StrucValue as int); --' and ' -- was or
                                                        _StrucCondition := ProcessingRuleRecord.StrucCondition;
                                                        _RuleID := ProcessingRuleRecord.RuleID;
                                                        _RuleType := ProcessingRuleRecord.RuleType;
                                                        _Struc := ProcessingRuleRecord.Struc;
                                                        _StrucValue := ProcessingRuleRecord.StrucValue;
                                                        _StrucType := ProcessingRuleRecord.StrucType;
                                                    END LOOP;

                                                if(length(_WhereCondition) > 0)
                                                then
                                                    raise notice 'whereCond %', _WhereCondition;
                                                    _SQLQuery := 'select RowNumber, UID, ' 
                                                        || cast(RulesRecord.ParentRuleID as varchar(50)) || ', ' 
                                                        || cast(RulesRecord.ParentRuleID as varchar(50)) || ', '
                                                        || cast(_RuleType as varchar(50)) || ', '
                                                        || '''' || _Struc || '''' || ', '
                                                        || '''nothing'''
                                                        || ' from _GFI_GPS_GPSData where AssetID = ' || cast(ProcessRecord.AssetID as varchar(50)) 
                                                        || ' and LongLatValidFlag = 1'
                                                        || ' and DateTimeGPS_UTC >= ''' || cast(_dtStart as varchar(50)) 
														|| ''' and '
                                                        || ' ( ' || _WhereCondition || ')';
                                                    if(ProcessingRuleRecord.StrucType = 'Stopped') then
                                                        _SQLQuery := _SQLQuery || ' and EngineOn = 0';
                                                    else
                                                        _SQLQuery := _SQLQuery || ' and EngineOn = 1';
                                                    end if;
                                                    --inserting First Engine On after EngineOff to get duration
                                                    if(ProcessingRuleRecord.StrucType = 'Stopped. NowObsoleteNeedToLookForAnotherLogic')
                                                    then
                                                        raise notice 'Obsolete';
                                                    end if;
                                                    if(_SQLQuery IS NOT NULL) then
                                                        EXECUTE 'insert into _ExTemp ' || _SQLQuery;
                                                        --COPY (select * from _ExTemp) To 'c:/Reza/tmp/output.csv';
                                                    end if;
                                                end if;
                                                close ProcessingRuleCursor;
                                            end;
                                        end if;

                                        -- RoadSpeed
                                        --if(1=0)
                                        raise notice 'RoadSpeed';
                                        perform * from GFI_GPS_Rules where ParentRuleID = RulesRecord.ParentRuleID and RuleType = 10 and Struc = 'RoadSpeed';
                                        GET DIAGNOSTICS total_rows := ROW_COUNT;
                                        if(total_rows > 0)
                                        then
                                            _SQLQuery := 'RoadSpeed';
                                            _RuleID := (select RuleID from GFI_GPS_Rules where ParentRuleID = RulesRecord.ParentRuleID and RuleType = 10 and Struc = 'RoadSpeed' limit 1);
                                            _Struc := (select Struc from GFI_GPS_Rules where ParentRuleID = RulesRecord.ParentRuleID and RuleType = 10 and Struc = 'RoadSpeed' limit 1);
                                            _RuleType := 10;
                                            _SQLQuery := 'select RowNumber, UID, ' 
                                                        || cast(RulesRecord.ParentRuleID as varchar(50)) || ', ' 
                                                        || cast(RulesRecord.ParentRuleID as varchar(50)) || ', '
                                                        || cast(_RuleType as varchar(50)) || ', '
                                                        || '''' || _Struc || '''' || ', '
                                                        || '''RoadSpeed'''
                                                        || ' from _GFI_GPS_GPSData where AssetID = ' || cast(ProcessRecord.AssetID as varchar(50)) 
                                                        || ' and LongLatValidFlag = 1'
                                                        || ' and Speed > RoadSpeed';
                                
                                            --	print @SQLQuery
                                            EXECUTE 'insert into _ExTemp ' || _SQLQuery; 
                                        end if;

                                        -- Aux
                                        --if(1=0)
                                        raise notice 'AUX';
                                        perform * from GFI_GPS_Rules where ParentRuleID = RulesRecord.ParentRuleID and RuleType = 6 and Struc = 'Aux';
                                        GET DIAGNOSTICS total_rows := ROW_COUNT;
                                        if(total_rows > 0)
                                        then
                                            _SQLQuery := 'AUX';
                                            Declare	ProcessingRuleRecord RECORD;
                                            Declare ProcessingRuleCursor cursor for
                                                select RuleID, RuleType, Struc, StrucCondition, StrucValue from GFI_GPS_Rules where ParentRuleID = RulesRecord.ParentRuleID 
                                                    and RuleType = 6 and Struc = 'Aux';

                                            Begin
                                                open ProcessingRuleCursor;
                                                    LOOP
                                                    FETCH ProcessingRuleCursor INTO ProcessingRuleRecord;
                                                    EXIT WHEN NOT FOUND;  
                                                        --Need to loop to get and\or condition
                                                        --And = MinimumTriggerValue 1
                                                        --Or  = MinimumTriggerValue 2
                                                        _WhereCondition := '';
                                                        _WhereCondition := 'TypeID = ''' || ProcessingRuleRecord.StrucValue || ''''
                                                            || ' and TypeValue ' || ProcessingRuleRecord.StrucCondition;	--This condition is under testing. if ommitted, both on and off will be in exception
                                                        if(length(_WhereCondition) > 0)
                                                        then
                                                            delete from _GFI_GPS_GPSDataWithDetails;
                                                            insert into _GFI_GPS_GPSDataWithDetails (UID, AssetID, DateTimeGPS_UTC, DateTimeServer, Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, StopFlag, TripDistance, TripTime, WorkHour, DriverID, TypeID, TypeValue, UOM)
                                                                select g.UID, g.AssetID, g.DateTimeGPS_UTC, g.DateTimeServer, g.Longitude, g.Latitude, g.LongLatValidFlag, g.Speed, g.EngineOn, g.StopFlag,g. TripDistance, g.TripTime, g.WorkHour, g.DriverID, d.TypeID, d.TypeValue, d.UOM 
                                                                        from _GFI_GPS_GPSData g
                                                                            inner join _GFI_GPS_GPSDataDetail d on g.UID = d.UID 
                                                                        where g.AssetID = ProcessRecord.AssetID 
                                                                            and g.DateTimeGPS_UTC >= _dtStart
                                                                            and d.TypeID = ProcessingRuleRecord.StrucValue
                                                                        ORDER BY g.DateTimeGPS_UTC;


                                                            --print 'whereCond ' + @WhereCondition
                                                            _SQLQuery := 'select -1, UID, ' 
                                                                || cast(RulesRecord.ParentRuleID as varchar(50)) || ', ' 
                                                                || cast(ProcessingRuleRecord.RuleID as varchar(50)) || ', '
                                                                || cast(ProcessingRuleRecord.RuleType as varchar(50)) || ', '
                                                                || '''' || ProcessingRuleRecord.Struc || '''' || ', '
                                                                || 'TypeID + '' '' + TypeValue'
                                                                || ' from _GFI_GPS_GPSDataWithDetails where AssetID = ' || convert(nvarchar(50), ProcessRecord.AssetID) 
                                                                --+ ' and LongLatValidFlag = 1'
																|| ' and DateTimeGPS_UTC >= ''' || cast(_dtStart as varchar(50)) 
																|| ''' and '
                                                                || ' ( ' || _WhereCondition || ')';
                                                            --print @SQLQuery
                                                            EXECUTE 'insert into _ExTemp ' || _SQLQuery; 
                                                        end if;
                                                    END LOOP;
                                                close ProcessingRuleCursor;
                                            end;
                                        end if;

                                        --Zones and ZoneTypes
                                        --StrucValue is either ID or GMID. If it is GMID, we will need to took for the corresponding IDs
                                        --Zone ID
                                        --if(1=0)
                                        raise notice 'Zones';
                                        perform * from GFI_GPS_Rules where ParentRuleID = RulesRecord.ParentRuleID and (RuleType = 1 or RuleType = 0);
                                        GET DIAGNOSTICS total_rows := ROW_COUNT;
                                        if(total_rows > 0)
                                        then
                                            _SQLQuery := 'Zones';
                                            delete from _ZoneHeader;
                                            Declare	ProcessingRuleRecord RECORD;
                                            Declare ProcessingRuleCursor cursor for
                                                select RuleID, RuleType, Struc, StrucCondition, StrucValue, StrucType from GFI_GPS_Rules where ParentRuleID = RulesRecord.ParentRuleID 
                                                    and (RuleType = 1 or RuleType = 0);
                                            Begin
                                                open ProcessingRuleCursor;
                                                    LOOP
                                                    FETCH ProcessingRuleCursor INTO ProcessingRuleRecord;
                                                    EXIT WHEN NOT FOUND;  
                                                        _StrucCondition := ProcessingRuleRecord.StrucCondition;
                                                        _RuleID := ProcessingRuleRecord.RuleID;
                                                        _RuleType := ProcessingRuleRecord.RuleType;
                                                        _Struc := ProcessingRuleRecord.Struc;
                                                        _StrucValue := ProcessingRuleRecord.StrucValue;
                                                        _StrucType := ProcessingRuleRecord.StrucType;

                                                        if(_RuleType = 1)	--Zones
                                                        then
                                                            begin
                                                                if(_StrucType = 'ID') then
                                                                    begin
                                                                        insert into _ZoneHeader 
                                                                            values (cast(_StrucValue as int));
                                                                    end;
                                                                elseif(_StrucType = 'GMID') then
                                                                    begin
                                                                        delete from _t;
                                                                        WITH RECURSIVE
                                                                            parent AS 
                                                                            (
                                                                                SELECT distinct ParentGMID
                                                                                FROM GFI_SYS_GroupMatrix
                                                                                WHERE ParentGMID in (_StrucValue)
                                                                            ), 
                                                                            tree AS 
                                                                            (
                                                                                SELECT 
                                                                                    x.GMID
                                                                                    , x.GMDescription
                                                                                    , x.ParentGMID
                                                                                FROM GFI_SYS_GroupMatrix x
                                                                                INNER JOIN parent ON x.ParentGMID = parent.ParentGMID
                                                                                UNION ALL
                                                                                SELECT 
                                                                                    y.GMID
                                                                                    , y.GMDescription
                                                                                    , y.ParentGMID
                                                                                FROM GFI_SYS_GroupMatrix y
                                                                                INNER JOIN tree t ON y.ParentGMID = t.GMID
                                                                            )
                                                                        insert into _t(i)
                                                                            select a.ZoneID   
                                                                                from GFI_FLT_ZoneHeader a, GFI_SYS_GroupMatrixZone m, tree
                                                                            where tree.GMID = m.GMID and a.ZoneID = m.iID;

                                                                        insert into _t(i)
                                                                            select a.ZoneID  
                                                                                from GFI_FLT_ZoneHeader a, GFI_SYS_GroupMatrixZone m
                                                                            where m.GMID in (_StrucValue) and a.ZoneID = m.iID;

                                                                        insert into _ZoneHeader 
                                                                            select i from _t;
                                                                    end;
                                                                end if;
                                                            end;
                                                        else				-- ZoneTypes. RuleType = 0
                                                            begin
                                                                --Need to link Zones with GroupMatrix
                                                                delete from _t;
                                                                insert into _t
                                                                    select ZoneHeadID from GFI_FLT_ZoneHeadZoneType where ZoneTypeID = _StrucValue;

                                                                insert into _ZoneHeader 
                                                                    select i from _t;
                                                            end;
                                                        end if;
                                                    END LOOP;
                                                close ProcessingRuleCursor;
                                            end;

                                            --Harcoding SIndx_GFI_FLT_ZoneHeader
                                            --select distinct z.ZoneID, g.RowNumber, g.UID, g.AssetID, g.DateTimeGPS_UTC, g.Longitude, g.Latitude, g.Speed into _inZone
                                            --from _ZoneHeader t
                                            --	inner join GFI_FLT_ZoneHeader z WITH (INDEX(SIndx_GFI_FLT_ZoneHeader)) on z.ZoneID = t.ZoneID
                                            --	inner join _GFI_GPS_GPSData g on z.GeomData.STIntersects(Geometry::STPointFromText('POINT(' + CONVERT(varchar, g.Longitude) + ' ' + CONVERT(varchar, g.Latitude) + ')', 4326)) = 1

                                            --Better option, but not using spatial index
                                            --select distinct x.*
                                            --    from _ZoneHeader t
                                            --        inner join (
                                            --                        select z.ZoneID, g.RowNumber, g.UID, g.AssetID, g.DateTimeGPS_UTC, g.Longitude, g.Latitude, g.Speed from GFI_FLT_ZoneHeader z
                                            --                            inner join _GFI_GPS_GPSData g on z.GeomData.STIntersects(Geometry::STPointFromText('POINT(' + CONVERT(varchar, g.Longitude) + ' ' + CONVERT(varchar, g.Latitude) + ')', 4326)) = 1
                                            --                    ) x on x.ZoneID = t.ZoneID

                                            --using spatial index, but quering all Zone records
                                            insert into _InAllZone
                                                select g.*, z.ZoneID, 1 InZone from GFI_FLT_ZoneHeader z 
                                                    inner join _GFI_GPS_GPSData g on ST_Intersects(z.GeomData, ST_GeomFromText('POINT(' || g.Longitude || ' ' || g.Latitude || ')', 4326)); 
                                                        
                                            insert into _InZone
                                                select distinct z.*
                                                    from _ZoneHeader t
                                                        inner join _inAllZone z on z.ZoneID = t.ZoneID;

                                            insert into _InZone
                                                select g.*, -1 ZoneID, 0 InZone from _GFI_GPS_GPSData g where RowNumber not in (select RowNumber from _InZone);
                                        
                                            insert into _ZoneData
                                                select distinct * from _InZone;
                                            --select g.*, null ZoneID, 0 InZone into _ZoneData from _GFI_GPS_GPSData g 
                                            --update _ZoneData
                                            --	set ZoneID = 0, InZone = 1
                                            --from _ZoneData g
                                            --	inner join GFI_FLT_ZoneHeader z on z.GeomData.STIntersects(Geometry::STPointFromText('POINT(' + CONVERT(varchar, g.Longitude) + ' ' + CONVERT(varchar, g.Latitude) + ')', 4326)) = 1
                                            
                                            if (_StrucCondition = 'Entering zone') then
                                                begin 
                                                    _SQLQuery = 'select Z2.RowNumber, Z2.UID, ' 
                                                            || cast(RulesRecord.ParentRuleID as varchar(50)) || ', ' 
                                                            || cast(RulesRecord.ParentRuleID as varchar(50)) || ', '
                                                            || cast(_RuleType as varchar(50)) || ', '
                                                            || '''' || _Struc || '''' || ', '
                                                            || ' Z2.ZoneID '
                                                            || ' from _ZoneData z1 inner join _ZoneData z2 on z2.InZone = 1 and z2.RowNumber = z1.RowNumber + 1 and z1.InZone = 0 ';
                                                        --print 'Entering ' + @SQLQuery
                                                        EXECUTE 'insert into _ExTemp ' || _SQLQuery; 
                                                end;
                                            elseif (_StrucCondition = 'Leaving zone') then
                                                begin 
                                                    _SQLQuery = 'select Z2.RowNumber, Z2.UID, ' 
                                                            || cast(RulesRecord.ParentRuleID as varchar(50)) || ', ' 
                                                            || cast(RulesRecord.ParentRuleID as varchar(50)) || ', '
                                                            || cast(_RuleType as varchar(50)) || ', '
                                                            || '''' || _Struc || '''' || ', '
                                                            || ' Z1.ZoneID '
                                                            || ' from _ZoneData z1 inner join _ZoneData z2 on z2.InZone = 0 and z2.RowNumber = z1.RowNumber + 1 and z1.InZone = 1 ';
                                                        --print 'Entering ' + @SQLQuery
                                                    EXECUTE 'insert into _ExTemp ' || _SQLQuery; 
                                                end;
                                            elseif (_StrucCondition = 'Stopped inside zone' or _StrucCondition = 'Inside zone bounds') then
                                                begin
                                                    _SQLQuery = 'select RowNumber, UID, ' 
                                                        || cast(RulesRecord.ParentRuleID as varchar(50)) || ', ' 
                                                        || cast(RulesRecord.ParentRuleID as varchar(50)) || ', '
                                                        || cast(_RuleType as varchar(50)) || ', '
                                                        || '''' || _Struc || '''' || ', '
                                                        || '''ZoneID '''
                                                        || ' from _ZoneData where InZone = 1';
                                                    --print 'In Zone ' + @SQLQuery
                                                    EXECUTE 'insert into _ExTemp ' || _SQLQuery; 
                                                end;
                                            elseif (_StrucCondition = 'Stopped outside zone' or _StrucCondition = 'Outside zone bounds') then
                                                begin
                                                    _SQLQuery = 'select RowNumber, UID, ' 
                                                        || cast(RulesRecord.ParentRuleID as varchar(50)) || ', ' 
                                                        || cast(RulesRecord.ParentRuleID as varchar(50)) || ', '
                                                        || cast(_RuleType as varchar(50)) || ', '
                                                        || '''' || _Struc || '''' || ', '
                                                        || '''ZoneID '''
                                                        || ' from _ZoneData where InZone = 0';
                                                    --print 'In Zone ' + @SQLQuery
                                                    EXECUTE 'insert into _ExTemp ' || _SQLQuery; 
                                                end;
                                            else
                                                begin
                                                    _SQLQuery = 'select RowNumber, UID, ' 
                                                        || cast(RulesRecord.ParentRuleID as varchar(50)) || ', ' 
                                                        || cast(RulesRecord.ParentRuleID as varchar(50)) || ', '
                                                        || cast(_RuleType as varchar(50)) || ', '
                                                        || '''' || _Struc || '''' || ', '
                                                        || '''ZoneID '''
                                                        || ' from _ZoneData where InZone = 1'; 
                                                    --print @SQLQuery
                                                    EXECUTE 'insert into _ExTemp ' || _SQLQuery; 
                                                end;
                                            end if;
                                            delete from _inAllZone;
                                            delete from _inZone;
                                            delete from _ZoneData;
                                        end if;

                                        -- EngineAux
                                        --if(1=0)
                                        raise notice 'EngineAux';
                                        perform * from GFI_GPS_Rules where ParentRuleID = RulesRecord.ParentRuleID and RuleType = 6 and Struc = 'EngineAux';
                                        GET DIAGNOSTICS total_rows := ROW_COUNT;
                                        if(total_rows > 0)
                                        then
                                            _WhereCondition = '';
                                            _TmpCondition = ' ';
                                            _SQLQuery := 'EngineAux';

                                            Declare	ProcessingRuleRecord RECORD;
                                            Declare ProcessingRuleCursor cursor for
                                                select RuleID, RuleType, Struc, StrucCondition, StrucValue, StrucType, MinTriggerValue from GFI_GPS_Rules where ParentRuleID = RulesRecord.ParentRuleID 
                                                    and RuleType = 6 and Struc = 'EngineAux';

                                            Begin
                                                open ProcessingRuleCursor;
                                                    LOOP
                                                    FETCH ProcessingRuleCursor INTO ProcessingRuleRecord;
                                                    EXIT WHEN NOT FOUND;  
                                                        _StrucCondition := ProcessingRuleRecord.StrucCondition;
                                                        _RuleID := ProcessingRuleRecord.RuleID;
                                                        _RuleType := ProcessingRuleRecord.RuleType;
                                                        _Struc := ProcessingRuleRecord.Struc;
                                                        _StrucValue := ProcessingRuleRecord.StrucValue;
                                                        _StrucType := ProcessingRuleRecord.StrucType;

                                                        if(_WhereCondition <> '') 
                                                        then
                                                            if(ProcessingRuleRecord.MinTriggerValue = 1) then
                                                                _TmpCondition := ' or ';
                                                            elseif(ProcessingRuleRecord.MinTriggerValue = 2) then
                                                                _TmpCondition = ' and ';
                                                            end if;
                                                        end if;

                                                        _EngineAuxCondition := 
                                                            CASE ProcessingRuleRecord.StrucValue 
                                                                WHEN 'Harsh Acceleration' then 'Harsh Accelleration'
                                                                WHEN 'Harsh Breaking' THEN 'Harsh Breaking'
                                                                WHEN 'Power Reset' THEN 'Reset'
                                                                WHEN 'Fuel' THEN 'Fuel'
                                                                WHEN 'Temperature' THEN ProcessingRuleRecord.StrucValue
                                                                WHEN 'Temperature 2' THEN ProcessingRuleRecord.StrucValue
                                                                WHEN 'GPSLost' THEN 'InvalidGpsSignals'
                                                                WHEN 'Impact' THEN 'Impact'
                                                                ELSE 'TingPow'
                                                            END;

                                                        _EngineAuxStrucCondition := 
                                                            CASE ProcessingRuleRecord.StrucValue 
                                                                WHEN 'Fuel' THEN ' and cast (TypeValue as float) ' || ProcessingRuleRecord.StrucCondition || ' ''' || ProcessingRuleRecord.StrucType || ''''
                                                                WHEN 'Temperature' THEN ' and cast (TypeValue as float) ' || ProcessingRuleRecord.StrucCondition || ' ''' || ProcessingRuleRecord.StrucType || ''''
                                                                WHEN 'Temperature 2' THEN ' and cast (TypeValue as float) ' || ProcessingRuleRecord.StrucCondition || ' ''' || ProcessingRuleRecord.StrucType || ''''
                                                                ELSE ' '
                                                            END;

                                                        _WhereCondition := _TmpCondition || 'TypeID = ''' || _EngineAuxCondition || ''''
                                                            || _EngineAuxStrucCondition;
                                                    END LOOP;
                                                close ProcessingRuleCursor;									

                                                if(length(_WhereCondition) > 10)
                                                then
                                                    delete from _GFI_GPS_GPSDataWithDetails;
                                                    insert into _GFI_GPS_GPSDataWithDetails (UID, AssetID, DateTimeGPS_UTC, DateTimeServer, Longitude, Latitude, LongLatValidFlag, Speed, EngineOn, StopFlag, TripDistance, TripTime, WorkHour, DriverID, TypeID, TypeValue, UOM)
                                                        select g.UID, g.AssetID, g.DateTimeGPS_UTC, g.DateTimeServer, g.Longitude, g.Latitude, g.LongLatValidFlag, g.Speed, g.EngineOn, g.StopFlag,g. TripDistance, g.TripTime, g.WorkHour, g.DriverID, d.TypeID, d.TypeValue, d.UOM 
                                                            from _GFI_GPS_GPSData g
                                                                inner join _GFI_GPS_GPSDataDetail d on g.UID = d.UID 
                                                            where g.AssetID = ProcessRecord.AssetID 
                                                                and g.DateTimeGPS_UTC >= _dtStart
                                                                and d.TypeID = _StrucValue
                                                            ORDER BY g.DateTimeGPS_UTC;
                                                    --set @WhereCondition =  REPLACE(@WhereCondition, 'TypeValue', 'TypeValue');
                                                    --print 'whereCond ' + @WhereCondition
                                                    _SQLQuery := 'select -1, UID, ' 
                                                        || cast(RulesRecord.ParentRuleID as varchar(50)) || ', ' 
                                                        || cast(RulesRecord.ParentRuleID as varchar(50)) || ', '
                                                        || cast(_RuleType as varchar(50)) || ', '
                                                        || '''' || _Struc || '''' || ', '
                                                        || 'TypeID || '' '' || TypeValue'
                                                        || ' from _GFI_GPS_GPSDataWithDetails where AssetID = ' || cast(ProcessRecord.AssetID as varchar(50)) 
                                                        || ' and LongLatValidFlag = 1'
                                                        || ' and DateTimeGPS_UTC >= ''' || cast(_dtStart as varchar(50)) 
														|| ''' and '
                                                        || ' ( ' || _WhereCondition || ')';
                                                    --print @SQLQuery
                                                    EXECUTE 'insert into _ExTemp ' || _SQLQuery; 
                                                end if;
                                            end;
                                        end if;

                                    EXCEPTION
                                        WHEN OTHERS THEN
                                            declare errMsg VARCHAR (1200);
                                                    err_code VARCHAR (50);
                                            BEGIN
                                                errMsg := SUBSTR(SQLERRM, 1, 200);
                                                if(_SQLQuery is not null)
                                                then
                                                    errMsg := errMsg || ' > ' || _SQLQuery;
                                                end if;
                                                err_code := RulesRecord.ParentRuleID || ' ' || SUBSTR(SQLSTATE, 1, 175);
                                                --ROLLBACK;              
                                                BEGIN                 
                                                    BEGIN
                                                        raise notice 'error catched in RulesCursor Batch 1 errMsg % err_code % Rule % ', errMsg, err_code, RulesRecord.ParentRuleID;
                                                        insert into GFI_GPS_ProcessErrors (sError, sOrigine, sFieldName, sFieldValue, AssetID, dtUTC) 
                                                            values (errMsg, 'Xceptions1', 'ParentRuleID', err_code, ProcessRecord.AssetID, _dtStart);
                                                    END;
                                                EXCEPTION
                                                    WHEN OTHERS THEN raise notice 'errMsg 1';
                                                END;   
                                            END;
                                    end;
                                END LOOP;

                                --Checking all conditions in rule, except Duration
                                begin
                                    raise notice 'Checking all conditions in rule, except Duration';
                                    fetch first from RulesCursor into RulesRecord;
                                    LOOP
                                    EXIT WHEN NOT FOUND;  
                                        insert into _ResultTable
                                            SELECT RulesRecord.ParentRuleID Parent, g.*, 0
                                            FROM _GFI_GPS_GPSData g
                                                INNER JOIN
                                                (
                                                    --SELECT  distinct e.UID 
                                                    --FROM    _ExTemp e, GFI_GPS_Rules r
                                                    --WHERE   e.RuleID = r.RuleID and r.ParentRuleID = @ParentRuleID and e.RuleType not in (7, 8)	--Duration 7 and ExceptionType 8
                                                    --group by uid
                                                    --having count(e.UID) >= (SELECT   count(distinct RuleType)   --Was =. Now >= for situations where TypeID saved more than once in GPPSDataDetails. Will ensure this is not the case when saving in GPPSDataDetails
                                                    --						FROM    GFI_GPS_Rules r
                                                    --						WHERE   ParentRuleID = @ParentRuleID and RuleType not in (7, 8)		--Duration 7 and ExceptionType 8
                                                    --						group by ParentRuleID
                                                    --						)
                                                    select t.UID from
                                                        (
                                                            SELECT  distinct e.UID, e.RuleType, 1 cnt
                                                                    FROM    _ExTemp e, GFI_GPS_Rules r
                                                                    WHERE   e.RuleID = r.RuleID and r.ParentRuleID = RulesRecord.ParentRuleID and e.RuleType not in (7, 8)	--Duration 7 and ExceptionType 8
                                                                    group by uid, e.RuleType
                                                        ) t group by t.UID
                                                                having count(t.UID) >= (SELECT   count(distinct RuleType)   
                                                                                        FROM    GFI_GPS_Rules r
                                                                                        WHERE   ParentRuleID = RulesRecord.ParentRuleID and RuleType not in (7, 8)		--Duration 7 and ExceptionType 8
                                                                                        group by ParentRuleID
                                                                                        )
                                                ) e ON g.UID = e.UID;
                                    fetch next from RulesCursor into RulesRecord;
                                    END LOOP;
                                end;
                                --COPY (select * from _ResultTable) To 'c:/Reza/tmp/_ResultTable.csv';
                                --Assigning groupID
                                --if(1 = 0)
                                begin
                                    raise notice 'Assigning groupID';

                                    insert into _tmp10
                                        WITH CTE AS 
                                        (
                                            SELECT *,  ROW_NUMBER() OVER (ORDER BY ParentRuleIDTriggered, DateTimeGPS_UTC) as InnerRowNum from _ResultTable
                                        )
                                        SELECT distinct t.*
                                            , prev.RowNumber PreviousRN
                                            , t.RowNumber ActualRN
                                            , nex.RowNumber NextRN
                                            , 0 GrpID
                                        FROM _ResultTable t
                                            left outer join CTE on t.RowNumber = CTE.RowNumber
                                            LEFT JOIN CTE prev ON prev.RowNumber = CTE.RowNumber - 1
                                            LEFT JOIN CTE nex ON nex.RowNumber = CTE.RowNumber + 1
                                        order by dateTimeGPS_UTC;

                                    insert into _tmp11
                                        select *
                                            , (select count(1) from _tmp10 where ActualRN = tt.PreviousRN and ParentRuleIDTriggered = tt.ParentRuleIDTriggered) grpSemiFinal 
                                        from _tmp10 tt order by DateTimeGPS_UTC;

                                    insert into _tmp12
                                        select *
                                            , (select count(1) from _tmp11 where grpSemiFinal = 0 and ActualRN <= tt.ActualRN and ParentRuleIDTriggered = tt.ParentRuleIDTriggered) grpFinal 
                                        from _tmp11 tt order by DateTimeGPS_UTC;
                            
                                    update _tmp12 T set GrpID = R.Grp
                                        from (
                                                select *
                                                    , (select count(1) from _tmp12 where NextRN is null and ActualRN < tt.ActualRN and ParentRuleIDTriggered = tt.ParentRuleIDTriggered) grp 
                                                from _tmp12 tt
                                            ) R
                                            where R.UID = T.UID and t.ParentRuleIDTriggered = r.ParentRuleIDTriggered;
                            
                                    update _ResultTable T set Misc2 = R.grpFinal
                                        from _tmp12 R where T.UID = R.UID and T.ParentRuleIDTriggered = R.ParentRuleIDTriggered;

                                    delete from _tmp10;
                                    delete from _tmp11;
                                    delete from _tmp12;
                                end;

                                --Processing Rules for duration
                                --if(1=0)
                                begin
                                    raise notice 'Processing Rules for duration';
                                    --Creating a header table with duration, group by Misc2, ParentRuleIDTriggered
                                    insert into _GrpDuration
                                        WITH TopBottomRow
                                        AS
                                        (
                                            SELECT Misc2, ParentRuleIDTriggered, min(DateTimeGPS_UTC) mini, max(DateTimeGPS_UTC) maxi
                                                FROM _ResultTable where Misc2 is not null
                                                Group BY Misc2, ParentRuleIDTriggered
                                        )
                                        SELECT *, DATEDIFF('SECOND', mini, maxi) Duration
                                            FROM TopBottomRow 
                                            order by Misc2, ParentRuleIDTriggered;

                                    --update TopBottomRow with existing exceptions data, based of _dtStart
                                    --if _dtStart exists in GFI_GPS_Exceptions, mini will be 1st occurence
                                    fetch first from RulesCursor into RulesRecord;
                                    LOOP
                                    EXIT WHEN NOT FOUND;  
                                        Declare	ProcessingRuleRecord record;
                                        Declare ProcessingRuleCursor cursor for
                                            select RuleID, RuleType, Struc, StrucCondition, StrucValue from GFI_GPS_Rules where ParentRuleID = RulesRecord.ParentRuleID 
                                                and RuleType = 7;

                                        Begin
                                            open ProcessingRuleCursor;
                                                LOOP
                                                FETCH ProcessingRuleCursor INTO ProcessingRuleRecord;
                                                EXIT WHEN NOT FOUND;  
                                                    delete from _ResultTable t1 
                                                        using (select * from _GrpDuration where ParentRuleIDTriggered = RulesRecord.ParentRuleID and Duration <= cast(ProcessingRuleRecord.StrucValue as int)) as t2 
                                                            where t1.ParentRuleIDTriggered = t2.ParentRuleIDTriggered and t1.DateTimeGPS_UTC >= t2.mini and t1.DateTimeGPS_UTC <= t2.maxi;

                                                    delete from _ResultTable where Misc2 is null and ParentRuleIDTriggered = RulesRecord.ParentRuleID;
                                                END LOOP;
                                            close ProcessingRuleCursor;
                                        end;
                                    fetch next from RulesCursor into RulesRecord;
                                    END	LOOP;	
                        
                                    delete from _GrpDuration;
                                end; 
                            close RulesCursor;
                        end;
                    end if;
				END LOOP;
			close ProcessCursor;

            if(_continueProcess = 1) THEN
                --Getting earliest timestamp
                _dtStart := _dtStartWithDuration;
                insert into _OldException
                    select * from GFI_GPS_Exceptions where AssetID = _ActualAssetID and GPSDataID in 
                            (select g.UID from GFI_GPS_GPSData g
                                where AssetID = _ActualAssetID
                                    and g.DateTimeGPS_UTC >= _dtOfExistingExceptions);
                --COPY (select * from _OldException) To 'c:/Reza/tmp/output.csv';

                raise notice 'Existing Exceptions at %', _dtOfExistingExceptions;
                --Not yet in sql. above too, now using GFI_GPS_GPSData and _dtOfExistingExceptions
                insert into _ResultTable (parentruleidtriggered, RowNumber, uid, assetid, driverid, misc2, misc3, misc4, datetimegps_utc)
                    SELECT ruleid, -1, gpsdataid, assetid, driverid, groupid, groupid, posted, datetimegps_utc
                        FROM _OldException; 
            
                _MaxGrpID := (SELECT PValue from GFI_SYS_GlobalParams WHERE Paramname = 'Exceptions');
                if(_MaxGrpID is null)
                then
                    insert into GFI_SYS_GlobalParams (ParamName, PValue, ParamComments) values ('Exceptions', 0, 'Exceptions GroupID');
                    _MaxGrpID := (SELECT PValue from GFI_SYS_GlobalParams WHERE Paramname = 'Exceptions');
                end if;
                --select @MaxGrpID, ProcessRecord.AssetID

                --Misc4 is Posted
                --Misc3 is Existing GroupID of GFI_GPS_Exceptions
                --Misc2 is the final GroupID
                --Misc1 is the calculated GroupID
                --Update 1 init
                --Update 2 Reassign deleted GroupID for matching rows. Instead, using new GroupID, but Updating Posted for matching rows
                --Update 3 Assign same GroupID to related GroupID
                --Update 4 Assign GroupID to new Groups
                update _ResultTable set Misc4 = 0, Misc3 = 0, Misc1 = Misc2;
                UPDATE _ResultTable T SET Misc4 = Posted
                    FROM _OldException S where S.GPSDataID = T.UID and S.RuleID = T.ParentRuleIDTriggered;

                with cte as
                (
                    select RuleID, GPSDataID, GroupID from _OldException 
                )
                update _ResultTable T set Misc3 = c.GroupID, WorkHour = -100
                    from cte c where T.ParentRuleIDTriggered = c.RuleID and T.UID = c.GPSDataID;

                Declare	GrpIDRecord RECORD;
                DECLARE GrpIDCursor CURSOR FOR 
                    select ParentRuleIDTriggered, Misc3, Misc1 from _ResultTable where Misc3 != 0 group by ParentRuleIDTriggered, Misc3, Misc1;
                Begin
                    open GrpIDCursor;
                        LOOP
                        FETCH GrpIDCursor INTO GrpIDRecord;
                        EXIT WHEN NOT FOUND;  
                            update _ResultTable 
                                set Misc3 = GrpIDRecord.Misc3
                                    , Misc4 = (select Posted from _OldException where RuleID = GrpIDRecord.ParentRuleIDTriggered order by DateTimeGPS_UTC limit 1)  
                            where Misc1 = GrpIDRecord.Misc1 
                                and ParentRuleIDTriggered = @ParentRuleIDTriggered;
                        END LOOP;
                    close GrpIDCursor;
                end;

                begin --tran
                    _InTran := 1;
                    if(1 = 0) then
                        delete from GFI_GPS_Exceptions 
                            where AssetID = ProcessRecord.AssetID 
                                and GPSDataID in 
                                    (select g.UID from _GFI_GPS_GPSData g
                                        where AssetID = ProcessRecord.AssetID
                                            and g.DateTimeGPS_UTC >= _dtStart)
                                and RuleID not in 
                                    (select r.ParentRuleID from GFI_GPS_Rules r where r.StrucValue = 'Not Reported' or r.Struc = 'Maintenance');
                    end if;

                    update _ResultTable set Misc2 = Misc3 where Misc3 != 0;
                    update _ResultTable set Misc2 = _MaxGrpID + cast(Misc1 as int) where Misc3 = 0;
                    _MaxGrpID := (select max(Misc2) from _ResultTable);
                    if(_MaxGrpID is not null) then
                        update GFI_SYS_GlobalParams set PValue = _MaxGrpID WHERE Paramname = 'Exceptions' and _MaxGrpID > CAST(PValue AS int);
                    end if;
                    --select '_ResultTable'
                    --select * from _ResultTable

                    insert into GFI_GPS_Exceptions
                        (RuleID, GPSDataID, AssetID, DriverID, GroupID, Posted, DateTimeGPS_UTC)
                            (select ParentRuleIDTriggered, UID, AssetID, DriverID, Misc2, Misc4, DateTimeGPS_UTC from _ResultTable where WorkHour != -100);

                    update _ResultTable T1
                        set WorkHour = -100
                    from (select ParentRuleIDTriggered, Misc2
                                    from _ResultTable where WorkHour = -100
                                    group by ParentRuleIDTriggered, Misc2
                                ) T2 
                    where T1.ParentRuleIDTriggered = T2.ParentRuleIDTriggered and T1.Misc2 = T2.Misc2
                        and T1.WorkHour = 0;

                    insert into GFI_GPS_ExceptionsHeader (RuleID, AssetID, GroupID, dateFrom, DateTo)
                        select ParentRuleIDTriggered, AssetID, Misc2, min(DateTimeGPS_UTC) dtFr, max(DateTimeGPS_UTC) dtTo
                            from _ResultTable 
                        where WorkHour != -100
                            Group by ParentRuleIDTriggered, AssetID, Misc2;

                    update GFI_GPS_ExceptionsHeader T1
                        set dateFrom = T2.dtFr, dateTo = T2.dtTo
                    from 
                        (select RuleID, AssetID, GroupID, min(DateTimeGPS_UTC) dtFr, max(DateTimeGPS_UTC) dtTo, min(InsertedAt) InsertedAt
                                        from GFI_GPS_Exceptions where AssetID = _ActualAssetID 
                                        Group by RuleID, AssetID, GroupID
                        ) T2 
                    where T1.AssetID = T2.AssetID and T1.GroupID = T2.GroupID and T1.RuleID = T2.RuleID
                        and DATEADD('day', -3, T2.InsertedAt) <= now(); --and T2.AssetID = @ActualAssetID

                end;
                --commit tran
                _InTran := 0;
            END IF;
            delete from GFI_GPS_ProcessPending where AssetID = _ActualAssetID and ProcessCode = 'Exceptions' and Processing = 1;

		EXCEPTION
			WHEN OTHERS THEN
				declare errMsg VARCHAR (200);
						err_code VARCHAR (50);
				BEGIN
					errMsg := SUBSTR(SQLERRM, 1, 200);
					err_code := _ParentRuleID || ' ' || SUBSTR(SQLSTATE, 1, 175);
					--ROLLBACK;              
					BEGIN                 
						BEGIN
							raise notice 'error catched errMsg 2 % err_code % ', errMsg, err_code;
							insert into GFI_GPS_ProcessErrors (sError, sOrigine, sFieldName, sFieldValue, AssetID, dtUTC) 
								values (errMsg, 'Xceptions2', 'ParentRuleID', err_code, _ActualAssetID, _dtStart);
						END;
					EXCEPTION
						WHEN OTHERS THEN raise notice 'errMsg 2';
					END;   
				END;
		end;
	end;
end;
$$ LANGUAGE plpgsql;

delete from gfi_sys_notification
	WHERE exceptionID IN
		(SELECT iID FROM 
			(SELECT iID, ROW_NUMBER() OVER(PARTITION BY ruleid, gpsdataid, assetid	ORDER BY iID) AS row_num
				FROM GFI_GPS_Exceptions 
			) t
		WHERE t.row_num > 1 
		);

DELETE FROM GFI_GPS_Exceptions
	WHERE iID IN
		(SELECT iID FROM 
			(SELECT iID, ROW_NUMBER() OVER(PARTITION BY ruleid, gpsdataid, assetid	ORDER BY iID) AS row_num
				FROM GFI_GPS_Exceptions 
			) t
		WHERE t.row_num > 1 
		);

delete FROM GFI_GPS_ExceptionsHeader h
	WHERE not exists
			(select *
				from GFI_GPS_Exceptions e where h.groupID = e.GroupID and h.RuleID = e.RuleID and h.AssetID = e.AssetID
			);
";
			sql = sql.Replace("_ActualAssetID := 1;", "_ActualAssetID := " + iAssetID + ";"); 
			return sql;
		}

        public static String sProcessNotReportedExceptions()
        {
            String sql = @"
--Next Logic. Rules for Not Reported, (Heart beat simulation)
--Skipping in PostgreSQL
set nocount on

begin
	Declare @Period int
	declare @NoReportRule int
	Declare @ChkNoReport Table
	(
		AssetID [int]
	)
	DECLARE ProcessCursor CURSOR FOR 
		select r.ParentRuleID from GFI_GPS_Rules r where r.StrucValue = 'Not Reported'

	open ProcessCursor
		fetch next from ProcessCursor into @NoReportRule
		WHILE @@FETCH_STATUS = 0   
		BEGIN 
			set @Period = ''
			set @Period = (select StrucCondition from GFI_GPS_Rules where ParentRuleID = @NoReportRule and StrucValue = 'Not Reported')

			--Populating @ChkNoReport table will all assets from rule
			delete from @ChkNoReport
			--ID
			insert @ChkNoReport
				select StrucValue from GFI_GPS_Rules where ParentRuleID = @NoReportRule and Struc = 'Assets' and StrucType = 'ID'

			--GMID
			declare @tg table (GMID int, GMDescription nvarchar(1000), ParentGMID int, StrucID int)
			delete from @tg
			;WITH 
				parent AS 
				(
					SELECT distinct ParentGMID
					FROM GFI_SYS_GroupMatrix
					WHERE ParentGMID in (select StrucValue from GFI_GPS_Rules where ParentRuleID = @NoReportRule and Struc = 'Assets' and StrucType = 'GMID')
				), 
				tree AS 
				(
					SELECT 
						x.GMID
						, x.GMDescription
						, x.ParentGMID
					FROM GFI_SYS_GroupMatrix x
					INNER JOIN parent ON x.ParentGMID = parent.ParentGMID
					UNION ALL
					SELECT 
						y.GMID
						, y.GMDescription
						, y.ParentGMID
					FROM GFI_SYS_GroupMatrix y
					INNER JOIN tree t ON y.ParentGMID = t.GMID
				)
			insert into @tg
				SELECT Distinct GMID, GMDescription, ParentGMID, -2 StrucID FROM tree
				union all
					SELECT z.GMID, z.GMDescription, -1, -3 Struc
					FROM GFI_SYS_GroupMatrix z where z.GMID in (select StrucValue from GFI_GPS_Rules where ParentRuleID = @NoReportRule and Struc = 'Assets' and StrucType = 'GMID')
				union all
					select -4, a.AssetNumber, tree.GMID as ParentGMID, a.AssetID
						from GFI_FLT_Asset a, GFI_SYS_GroupMatrixAsset m, tree
					where tree.GMID = m.GMID and a.AssetID = m.iID
				union all
					select -5, a.AssetNumber, m.GMID as ParentGMID, a.AssetID 
						from GFI_FLT_Asset a, GFI_SYS_GroupMatrixAsset m
					where m.GMID in (select StrucValue from GFI_GPS_Rules where ParentRuleID = @NoReportRule and Struc = 'Assets' and StrucType = 'GMID') 
						and a.AssetID = m.iID

			insert @ChkNoReport
				select StrucID from @tg where StrucID > 0 order by StrucID

			--insert in Exceptions Table
			begin
				declare @MaxGrpIDg int
				set @MaxGrpIDg = (select coalesce(max(GroupID), 0) from GFI_GPS_Exceptions)
				set @MaxGrpIDg = @MaxGrpIDg + 1

				DECLARE @dtHeartBeat TABLE 
				(
					[ParentRuleID] [int] NULL,
					[UID] [int] ,
					[AssetID] [int] ,
					[DateTimeGPS_UTC] [DateTime] ,
					[DriverID] [int],
					[RowNum] [int]
				)
				delete from @dtHeartBeat
				insert @dtHeartBeat
					select @NoReportRule ParentRuleID, uid, l.AssetID, DateTimeGPS_UTC,
						l.DriverID,
						row_number() over(partition by l.AssetID order by DateTimeGPS_UTC desc) as RowNum
					from GFI_GPS_LiveData l
						inner join @ChkNoReport c on l.AssetID = c.AssetID
				delete from @dtHeartBeat where RowNum > 1

				--Never reported
				insert @dtHeartBeat
					select @NoReportRule ParentRuleID, -1, l.AssetID, DATEADD(YY, -15, GETUTCDATE()),
						1 DriverID,
						1 RowNum
					from @ChkNoReport l
					where l.AssetID in (select AssetID from @ChkNoReport where AssetID not in (select AssetID from GFI_GPS_LiveData) )

				--select * from @dtHeartBeat
				update @dtHeartBeat 
					set DateTimeGPS_UTC = T1.DateTimeGPS_UTC
				from GFI_GPS_HeartBeat T1
					inner join (select AssetID, max(DateTimeGPS_UTC) DateTimeGPS_UTC from GFI_GPS_LiveData group by AssetID) T2 on T2.AssetID = T1.AssetID 
					inner join @dtHeartBeat T3 on T3.AssetID = T2.AssetID
				where T1.DateTimeGPS_UTC > T2.DateTimeGPS_UTC 
				--select * from @dtHeartBeat
				insert [GFI_GPS_Exceptions]	(RuleID, GPSDataID, AssetID, DriverID, GroupID, DateTimeGPS_UTC)
					select ParentRuleID, UID, h.AssetID, h.DriverID, @MaxGrpIDg, GETUTCDATE() -- cte.DateTimeGPS_UTC  
						from @dtHeartBeat h
							left outer join GFI_GPS_Exceptions e on 
                                h.UID = e.GPSDataID 
                                and h.ParentRuleID = e.RuleID
                                and dateadd(dd, datediff(dd, 0, e.InsertedAt), 0) = dateadd(dd, datediff(dd, 0, GETUTCDATE()), 0) 
						where RowNum = 1
							and e.GPSDataID is null and h.DateTimeGPS_UTC < DATEADD(hour, -@Period, GETUTCDATE())
			end
			
			fetch next from ProcessCursor into @NoReportRule
		END
	close ProcessCursor
	deallocate ProcessCursor
end
";
            return sql;
        }
        public static String sProcessMaintenanceExceptions()
        {
            String sql = @"
--Next Logic. Rules for Maintenance
--Uncomment when testing only this condition
--Declare @tg table (GMID int, GMDescription nvarchar(1000), ParentGMID int, StrucID int)
--Declare @StructValue nvarchar(50)
set nocount on
begin
	declare @MaxGrpIDm int
	declare @MaintenanceRule int
	Declare @MaintenanceStrucCondition nvarchar(50)
	Declare @StructValue nvarchar(50)

	Declare @ChkMaintenance Table
	(
		AssetID [int]
	)

	DECLARE ProcessCursor CURSOR FOR 
		select r.ParentRuleID, r.StrucCondition, r.StrucValue from GFI_GPS_Rules r where r.Struc = 'Maintenance'

	open ProcessCursor
		fetch next from ProcessCursor into @MaintenanceRule, @MaintenanceStrucCondition, @StructValue
		WHILE @@FETCH_STATUS = 0
		BEGIN 
			--Populating @ChkMaintenance table will all assets from rule
			delete from @ChkMaintenance

			--iID
			insert @ChkMaintenance
				select StrucValue from GFI_GPS_Rules where ParentRuleID = @MaintenanceRule and Struc = 'Assets' and StrucType = 'ID'

			--GMID
			declare @tg table (GMID int, GMDescription nvarchar(1000), ParentGMID int, StrucID int)
			delete from @tg 
			;WITH 
				parent AS 
				(
					SELECT distinct ParentGMID
					FROM GFI_SYS_GroupMatrix
					WHERE ParentGMID in (select StrucValue from GFI_GPS_Rules where ParentRuleID = @MaintenanceRule and Struc = 'Assets' and StrucType = 'GMID')
				), 
				tree AS 
				(
					SELECT 
						x.GMID
						, x.GMDescription
						, x.ParentGMID
					FROM GFI_SYS_GroupMatrix x
					INNER JOIN parent ON x.ParentGMID = parent.ParentGMID
					UNION ALL
					SELECT 
						y.GMID
						, y.GMDescription
						, y.ParentGMID
					FROM GFI_SYS_GroupMatrix y
					INNER JOIN tree t ON y.ParentGMID = t.GMID
				)
			insert into @tg
				SELECT Distinct GMID, GMDescription, ParentGMID, -2 StrucID FROM tree
				union all
					SELECT z.GMID, z.GMDescription, -1, -3 Struc
					FROM GFI_SYS_GroupMatrix z where z.GMID in (select StrucValue from GFI_GPS_Rules where ParentRuleID = @MaintenanceRule and Struc = 'Assets' and StrucType = 'GMID')
				union all
					select -4, a.AssetNumber, tree.GMID as ParentGMID, a.AssetID
						from GFI_FLT_Asset a, GFI_SYS_GroupMatrixAsset m, tree
					where tree.GMID = m.GMID and a.AssetID = m.iID
				union all
					select -5, a.AssetNumber, m.GMID as ParentGMID, a.AssetID 
						from GFI_FLT_Asset a, GFI_SYS_GroupMatrixAsset m
					where m.GMID in (select StrucValue from GFI_GPS_Rules where ParentRuleID = @MaintenanceRule and Struc = 'Assets' and StrucType = 'GMID') 
						and a.AssetID = m.iID

			insert @ChkMaintenance
				select StrucID from @tg where StrucID > 0 order by StrucID

			--insert in Exceptions Table
			begin
				if(@MaintenanceStrucCondition = 'Status')
				begin
					set @MaxGrpIDm = (select coalesce(max(GroupID), 0) from GFI_GPS_Exceptions)
					set @MaxGrpIDm = @MaxGrpIDm + 1

					;with cte as (
						select @MaintenanceRule ParentRuleID, cm.AssetID
							FROM GFI_AMM_VehicleMaintTypeLink vmtl
							LEFT OUTER JOIN GFI_AMM_VehicleMaintStatus vms ON vmtl.Status = vms.MaintStatusId and vms.MaintStatusId = @StructValue
								INNER JOIN GFI_AMM_VehicleMaintType vmt ON vmtl.MaintTypeId = vmt.MaintTypeId
								INNER JOIN GFI_FLT_Asset a ON vmtl.AssetId = a.AssetId
								INNER JOIN GFI_AMM_VehicleMaintenance vm ON vmtl.MaintURI = vm.URI
								inner join @ChkMaintenance cm on vmtl.AssetId = cm.AssetID
					)
					insert [GFI_GPS_Exceptions]	(RuleID, AssetID, GroupID)
						select ParentRuleID, cte.AssetID, @MaxGrpIDm 
							from cte 
								left outer join GFI_GPS_Exceptions e on cte.ParentRuleID = e.RuleID 
									and cte.AssetID = e.AssetID 
									and dateadd(dd, datediff(dd, 0, e.InsertedAt), 0) = dateadd(dd, datediff(dd, 0, GETDATE()), 0)
							where e.AssetID is null
				end
				if(@MaintenanceStrucCondition = 'Availability')
				begin
					set @MaxGrpIDm = (select coalesce(max(GroupID), 0) from GFI_GPS_Exceptions)
					set @MaxGrpIDm = @MaxGrpIDm + 1

					;with cte as (
						select @MaintenanceRule ParentRuleID, cm.AssetID
							FROM GFI_AMM_VehicleMaintTypeLink vmtl
							LEFT OUTER JOIN GFI_AMM_VehicleMaintStatus vms ON vmtl.Status = vms.MaintStatusId
								INNER JOIN GFI_AMM_VehicleMaintType vmt ON vmtl.MaintTypeId = vmt.MaintTypeId
								INNER JOIN GFI_FLT_Asset a ON vmtl.AssetId = a.AssetId
								INNER JOIN GFI_AMM_VehicleMaintenance vm ON vmtl.MaintURI = vm.URI and vm.AssetStatus = @StructValue
								inner join @ChkMaintenance cm on vmtl.AssetId = cm.AssetID
					)
					insert [GFI_GPS_Exceptions]	(RuleID, AssetID, GroupID)
						select ParentRuleID, cte.AssetID, @MaxGrpIDm 
							from cte 
								left outer join GFI_GPS_Exceptions e on cte.ParentRuleID = e.RuleID 
									and cte.AssetID = e.AssetID 
									and dateadd(dd, datediff(dd, 0, e.InsertedAt), 0) = dateadd(dd, datediff(dd, 0, GETDATE()), 0)
							where e.AssetID is null
				end
			end		

			fetch next from ProcessCursor into @MaintenanceRule, @MaintenanceStrucCondition, @StructValue
		END

	close ProcessCursor
	deallocate ProcessCursor
end";
            return sql;
        }
    }
}