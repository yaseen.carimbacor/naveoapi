﻿using NaveoWebMaps.Helpers;
using NaveoWebMaps.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Xml;
using ThinkGeo.MapSuite;
using ThinkGeo.MapSuite.Drawing;
using ThinkGeo.MapSuite.Layers;
using ThinkGeo.MapSuite.Shapes;
using ThinkGeo.MapSuite.Styles;
using ThinkGeo.MapSuite.WebApi;
using static NaveoWebMaps.Models.FixedAssetsData;
using static NaveoWebMaps.Models.ZonesData;

namespace NaveoWebMaps.Controllers
{
    [RoutePrefix("ShapeFiles")]
    public class ShapeFilesController : ApiController
    {
        #region NaveoMaps
        [Route("NaveoMaps/{z}/{x}/{y}")]
        [HttpGet]
        public HttpResponseMessage NaveoMaps(int z, int x, int y)
        {
            return OverlayHelper.DrawLayerOverlay(new EasyGisFileHelper().OpenNaveoMaps(z, x, y, "BaseESRIMaps"), z, x, y, "BaseESRIMaps", true);
        }
        #endregion
        #region MokaMaps
        [Route("MokaMaps/{z}/{x}/{y}")]
        [HttpGet]
        public HttpResponseMessage OpenMokaMaps(int z, int x, int y)
        {
            return OverlayHelper.DrawLayerOverlay(new EasyGisFileHelper().OpenMokaMaps(z, x, y, "MokaMaps"), z, x, y, "MokaMaps", true);
        }
        #endregion

        #region Specifics
        [Route("Specifics/{folder}/{file}/{z}/{x}/{y}")]
        [HttpGet]
        public HttpResponseMessage OpenSpecificMaps(String folder, String file, int z, int x, int y)
        {
            return OverlayHelper.DrawLayerOverlay(new ShapeFileHelper().ShowLayer(folder, file), z, x, y, folder + file, true);
        }
        #endregion

        #region Zones
        [Route("CreateZones")]
        [HttpPost]
        public HttpResponseMessage CreateZones(ZoneCreation zc)
        {
            Boolean b = new ShapeFileHelper().CreateZones(zc);
            if (b)
                return new HttpResponseMessage(HttpStatusCode.OK);
            return new HttpResponseMessage(HttpStatusCode.InternalServerError);
        }

        [Route("ShowZones/{UserToken}/{z}/{x}/{y}")]
        [HttpGet]
        public HttpResponseMessage ShowZones(int z, int x, int y, Guid UserToken)
        {
            return OverlayHelper.DrawLayerOverlay(new ShapeFileHelper().ShowZones(UserToken), z, x, y, "ZonesData", false);
        }

        [Route("ShowZones/{z}/{x}/{y}")]
        [HttpPost]
        public HttpResponseMessage ShowZones(int z, int x, int y)
        {
            Guid UserToken = CommonHelper.GetUserToken(Request.Headers);
            return OverlayHelper.DrawLayerOverlay(new ShapeFileHelper().ShowZones(UserToken), z, x, y, "ZonesData", false);
        }

        #endregion

        #region Trips
        [Route("CreateTrips")]
        [HttpPost]
        public HttpResponseMessage CreateTrips(TripData td)
        {
            Boolean b = new ShapeFileHelper().CreateTrips(td);
            if (b)
                return new HttpResponseMessage(HttpStatusCode.OK);
            return new HttpResponseMessage(HttpStatusCode.InternalServerError);
        }

        [Route("ShowTrips/{UserToken}/{z}/{x}/{y}")]
        [HttpGet]
        public HttpResponseMessage ShowTrips(int z, int x, int y, Guid UserToken)
        {
            return OverlayHelper.DrawLayerOverlay(new ShapeFileHelper().ShowTrips(UserToken), z, x, y, "TripsData", false);
        }

        [Route("ShowTrips/{z}/{x}/{y}")]
        [HttpPost]
        public HttpResponseMessage ShowTrips(int z, int x, int y)
        {
            Guid UserToken = CommonHelper.GetUserToken(Request.Headers);
            return OverlayHelper.DrawLayerOverlay(new ShapeFileHelper().ShowTrips(UserToken), z, x, y, "TripsData", false);
        }
        #endregion

        #region Indexes
        /// <summary>
        ///   
        //    function ajaxBuildIndex() {
        //    const Http = new XMLHttpRequest();

        //    Http.onreadystatechange = function () {
        //        if (this.readyState == 4 && this.status == 200) {
        //            alert("fni")
        //        }
        //    };

        //    Http.open("GET", '/ShapeFiles/BuildIndex');
        //    Http.send();
        //    }
        //    ajaxBuildIndex();
        // http://localhost:27080/ShapeFiles/BuildIndex?sPath=c:/Users/Reza/Downloads/RoadNetwork
        [Route("BuildIndex")]
        [HttpGet]
        public HttpResponseMessage BuildIndex(String sPath)
        {
            new ShapeFileHelper().BuildIndexFromDir(sPath);
            return new HttpResponseMessage(HttpStatusCode.OK);
        }
        #endregion

        #region FixedAssets
        [Route("GetAllFeatureIDs")]
        [HttpPost]
        public IHttpActionResult GetAllFeatureIDs(FixedAssetsCreation fac)
        {
            List<String> featureIds = new ShapeFileHelper().GetAllFeatureIDs(fac.sPath);
            return Ok(featureIds);
        }

        [Route("GetAllFeaturesByID")]
        [HttpPost]
        public IHttpActionResult GetAllFeaturesByID(FixedAssetsCreation fac)
        {
            List<KeyValuePair<string, string>> lFeatures = new ShapeFileHelper().GetAllFeaturesByID(fac.sPath, fac.sFeatureID);
            return Ok(lFeatures);
        }
        #endregion

        #region GenericShp
        [Route("CreateShp")]
        [HttpPost]
        public HttpResponseMessage CreateShp(ZoneCreation zc)
        {
            Boolean b = new ShapeFileHelper().CreateShp(zc);
            if (b)
                return new HttpResponseMessage(HttpStatusCode.OK);
            return new HttpResponseMessage(HttpStatusCode.InternalServerError);
        }

        #endregion

        #region Tests
        [Route("InMemo/{z}/{x}/{y}")]
        [HttpGet]
        public HttpResponseMessage InMemo(int z, int x, int y)
        {
            return OverlayHelper.DrawLayerOverlay(new InMemoryHelper().ShowZones(), z, x, y, "InMemo", false);
        }
        #endregion
    }
}
