﻿using NaveoWebMaps.Helpers;
using NaveoWebMaps.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using static NaveoWebMaps.Models.BlupData;

namespace NaveoWebMaps.Controllers
{
    [RoutePrefix("Blup")]
    public class BlupController: ApiController
    {
        [Route("CreateBlupWithinDistanceInMeters")]
        [HttpPost]
        public HttpResponseMessage CreateBlupWithinDistanceInMeters(BlupCreation bc)
        {
            String s = new ShapeFileHelper().CreateBlupWithinDistanceInMeters(bc);

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StringContent(s, Encoding.UTF8, "application/json");
            return response;
        }

        [Route("ShowLayers/{path}/{z}/{x}/{y}")]
        [HttpGet]
        public HttpResponseMessage ShowLayers(String path, int z, int x, int y)
        {
            return OverlayHelper.DrawLayerOverlay(new ShapeFileHelper().ShowBlupLayers(path), z, x, y, "BlupCircles", false);
        }

        [Route("ShowOutlineScheme/{sFile}/{z}/{x}/{y}")]
        [HttpGet]
        public HttpResponseMessage ShowOutlinePlanningScheme(String sFile, int z, int x, int y)
        {
            return OverlayHelper.DrawLayerOverlay(new ShapeFileHelper().ShowOutlinePlanningScheme(sFile), z, x, y, "OutlineScheme", false);
        }


        #region Applications
        [Route("AnalizeApplications")]
        [HttpPost]
        public HttpResponseMessage AnalizeApplications(BlupApplications ba)
        {
            DataTable dt = new BLUPService().AnalyzeApplications(ba);

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            String json = JsonConvert.SerializeObject(dt);
            response.Content = new StringContent(json, Encoding.UTF8, "application/json");
            return response;
        }
        #endregion
    }
}