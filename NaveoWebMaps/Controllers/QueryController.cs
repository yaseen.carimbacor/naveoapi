﻿using NaveoWebMaps.Constants;
using NaveoWebMaps.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace NaveoWebMaps.Controllers
{
    [RoutePrefix("Query")]
    public class QueryController : ApiController
    {
        #region Reading shapes features
        [HttpPost, Route("Shp")]
        public HttpResponseMessage QryShp([FromBody] JObject jObject)
        {
            // get parameters
            double x = jObject.Property("x").Value.ToObject<double>();
            double y = jObject.Property("y").Value.ToObject<double>();

            double radius = 1;
            JToken token = jObject["radius"];
            if (token != null)
                radius = jObject.Property("radius").Value.ToObject<double>();

            Guid userToken = new Guid();
            token = jObject["userToken"];
            if (token != null)
                userToken = jObject.Property("userToken").Value.ToObject<Guid>();

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            JObject result = new ShapeFileHelper().QryShp(x, y, radius, userToken);
            response.Content = new StringContent(result.ToString(), Encoding.UTF8, "application/json");

            return response;
        }

        [HttpPost, Route("FromShp")]
        public HttpResponseMessage FromShp([FromBody] JObject jObject)
        {
            // get parameters
            double x = jObject.Property("lon").Value.ToObject<double>();
            double y = jObject.Property("lat").Value.ToObject<double>();
            List<String> layers = jObject.Property("layers").Value.ToObject<List<String>>();
            String folder = jObject.Property("folder").Value.ToObject<String>();

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            DataSet dsResult = new DataSet();
            foreach (String s in layers)
            {
                DataSet ds = new DataSet();
                switch (s)
                {
                    default:
                        if (folder == "BLUP")
                            ds = ShapeFileHelper.QryAllFeatures(x, y, 5, LayerPaths.BlupShpPath(s + ".shp"));
                        else if(folder == "Fixed Asset")
                            ds = ShapeFileHelper.QryAllFeatures(x, y, 5, LayerPaths.FixedAssetsPath(s + ".shp"));
                        break;
                }
                dsResult.Merge(ds);
            }
            String result = JsonConvert.SerializeObject(dsResult);
            response.Content = new StringContent(result, Encoding.UTF8, "application/json");

            return response;
        }

        [HttpPost, Route("MokaShp")]
        public HttpResponseMessage QryMokaShp([FromBody] JObject jObject)
        {
            // get parameters
            double x = jObject.Property("x").Value.ToObject<double>();
            double y = jObject.Property("y").Value.ToObject<double>();

            double radius = 1;
            JToken token = jObject["radius"];
            if (token != null)
                radius = jObject.Property("radius").Value.ToObject<double>();

            Guid userToken = new Guid();
            token = jObject["userToken"];
            if (token != null)
                userToken = jObject.Property("userToken").Value.ToObject<Guid>();

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            JObject result = new ShapeFileHelper().QryMokaShp(x, y, radius);
            response.Content = new StringContent(result.ToString(), Encoding.UTF8, "application/json");

            return response;
        }

        [HttpPost, Route("InMemoryZone")]
        public HttpResponseMessage QryInMemoryZone([FromBody] JObject jObject)
        {
            // get parameters
            double x = jObject.Property("x").Value.ToObject<double>();
            double y = jObject.Property("y").Value.ToObject<double>();

            Guid userToken = new Guid();
            JToken token = jObject["userToken"];
            if (token != null)
                userToken = jObject.Property("userToken").Value.ToObject<Guid>();

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            JObject result = new InMemoryHelper().QryZonesLayer(x, y, userToken);
            response.Content = new StringContent(result.ToString(), Encoding.UTF8, "application/json");

            return response;
        }
        #endregion

    }
}