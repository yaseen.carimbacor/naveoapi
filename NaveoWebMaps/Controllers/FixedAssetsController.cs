﻿using NaveoWebMaps.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using static NaveoWebMaps.Models.BlupData;

namespace NaveoWebMaps.Controllers
{
    [RoutePrefix("FixedAssets")]
    public class FixedAssetsController : ApiController
    {
        #region FixedAssets
        [Route("CreateFAWithinDistanceInMeters")]
        [HttpPost]
        public HttpResponseMessage CreateBlupWithinDistanceInMeters(BlupCreation bc)
        {
            String s = new ShapeFileHelper().CreateFAWithinDistanceInMeters(bc);

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StringContent(s, Encoding.UTF8, "application/json");
            return response;
        }

        [Route("ShowLayers/{path}/{z}/{x}/{y}")]
        [HttpGet]
        public HttpResponseMessage ShowLayers(String path, int z, int x, int y)
        {
            return OverlayHelper.DrawLayerOverlay(new ShapeFileHelper().ShowFALayers(path), z, x, y, "FixedAssets", false);
        }
        #endregion

        #region RoadNetwork
        [Route("CreateRoadNetwork")]
        [HttpPost]
        public HttpResponseMessage CreateRoadNetwork(BlupCreation bc)
        {
            String s = new ShapeFileHelper().CreateRoadNetwork(bc);

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StringContent(s, Encoding.UTF8, "application/json");
            return response;
        }
        #endregion
    }
}