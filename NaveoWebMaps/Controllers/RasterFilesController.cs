﻿using NaveoWebMaps.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NaveoWebMaps.Controllers
{
    [RoutePrefix("Raster")]
    public class RasterFilesController : ApiController
    {
        [HttpGet, Route("MokaDrone/{z}/{x}/{y}")]
        public HttpResponseMessage MokaDrone(int z, int x, int y)
        {
            return OverlayHelper.DrawLayerOverlay(new RasterFileHelper().MokaDrone(255, z, x, y, "MokaDrone"), z, x, y, "MokaDrone", true);
        }

        [HttpGet, Route("OpenRasterImage/{fn}/{z}/{x}/{y}")]
        public HttpResponseMessage OpenRasterImage(String fn, int z, int x, int y)
        {
            return OverlayHelper.DrawLayerOverlay(new RasterFileHelper().openImage(255, fn, z, x, y, "OpenRasterImage"), z, x, y, "OpenRasterImage", true);
        }
    }
}
