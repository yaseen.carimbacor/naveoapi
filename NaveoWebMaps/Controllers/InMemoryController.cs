﻿using System;
using NaveoWebMaps.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using static NaveoWebMaps.Models.ZonesData;

namespace NaveoWebMaps.Controllers
{
    [RoutePrefix("InMemory")]
    public class InMemoryController: ApiController
    {
        #region Zones
        [Route("CreateZones")]
        [HttpPost]
        public HttpResponseMessage CreateZones(ZoneCreation zc)
        {
            Boolean b = new InMemoryHelper().CreateZones(zc);
            if (b)
                return new HttpResponseMessage(HttpStatusCode.OK);
            return new HttpResponseMessage(HttpStatusCode.InternalServerError);
        }

        [Route("ShowZones/{UserToken}/{z}/{x}/{y}")]
        [HttpGet]
        public HttpResponseMessage ShowZones(int z, int x, int y, Guid UserToken)
        {
            return OverlayHelper.DrawLayerOverlay(new InMemoryHelper().ShowZones(UserToken), z, x, y, "ZonesData", false);
        }
        #endregion

    }
}