﻿using NaveoWebMaps.Helpers;
using NaveoWebMaps.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;

namespace NaveoWebMaps.Controllers
{
    [RoutePrefix("qgis")]
    public class qgisController : ApiController
    {
        #region QGISMaps
        [Route("QGISMaps/Test")]
        [HttpGet]
        public HttpResponseMessage Test()
        {
            List<String> lFeaturesToExclude = new List<string>();
            return OverlayHelper.DrawLayerOverlay(new QGISFileHelper().OpenQGIS("0", lFeaturesToExclude), 14, 10815, 9133, "QGISMaps", false);
        }

        [Route("QGISMaps/{LayerID}/{SubLayer}/{z}/{x}/{y}")]
        [HttpGet]
        public HttpResponseMessage OpenQGIS(String LayerID, String SubLayer, int z, int x, int y)
        {
            List<String> lFeaturesToExclude = new List<string>();
            if (!String.IsNullOrEmpty(SubLayer))
            {
                String[] SubLayers = SubLayer.Split(',');
                foreach (String s in SubLayers)
                    lFeaturesToExclude.Add(s.Trim());
            }
            lFeaturesToExclude = new List<string>();
            return OverlayHelper.DrawLayerOverlay(new QGISFileHelper().OpenQGIS(LayerID, lFeaturesToExclude), z, x, y, "QGISMaps", false);
        }

        [Route("getQGISLayers")]
        [HttpGet]
        public IHttpActionResult getQGISLayers()
        {
            return Ok(new QGISFileHelper().GetLayers());
        }

        [Route("PrepareLayers")]
        [HttpPost]
        public HttpResponseMessage PrepareLayers(List<qgisLayer> layers)
        {
            String s = new QGISFileHelper().PrepareLayers(layers);

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StringContent(s, Encoding.UTF8, "application/json");
            return response;
        }

        [Route("ShowLayers/{path}/{z}/{x}/{y}")]
        [HttpGet]
        public HttpResponseMessage ShowLayers(String path, int z, int x, int y)
        {
            return OverlayHelper.DrawLayerOverlay(new QGISFileHelper().ShowLayers(path), z, x, y, "QGISMaps", false);
        }
        #endregion

    }
}