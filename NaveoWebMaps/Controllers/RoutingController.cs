﻿using NaveoWebMaps.Helpers;
using NaveoWebMaps.Models;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace NaveoWebMaps.Controllers
{
    [RoutePrefix("Routing")]
    public class RoutingController : ApiController
    {
        [HttpPost, Route("CreateRoute")]
        public HttpResponseMessage CreateRoute(RoutingData routing)
        {
            String Result = new RoutingHelper().CreateRouting(routing);
            HttpResponseMessage msg = new HttpResponseMessage(HttpStatusCode.OK);
            msg.Content = new StringContent(Result, Encoding.UTF8, "text/plain");
            return msg;
        }

        [HttpGet, Route("ShowRoute/{path}/{z}/{x}/{y}")]
        public HttpResponseMessage ShowRoute(int z, int x, int y, String path)
        {
            return OverlayHelper.DrawLayerOverlay(new ShapeFileHelper().ShowRoute(path), z, x, y, "Routing", false);
        }

        [HttpPost, Route("GetRoute")]
        public HttpResponseMessage GetRoute(RoutingData routing)
        {
            String Result = new RoutingHelper().GetRoute(routing);
            HttpResponseMessage msg = new HttpResponseMessage(HttpStatusCode.OK);
            msg.Content = new StringContent(Result, Encoding.UTF8, "application/json");
            return msg;
        }

        [HttpPost, Route("GetBestRoute")]
        public HttpResponseMessage GetBestRoute(RoutingData routing)
        {
            String Result = new RoutingHelper().GetBestRoute(routing);
            HttpResponseMessage msg = new HttpResponseMessage(HttpStatusCode.OK);
            msg.Content = new StringContent(Result, Encoding.UTF8, "application/json");
            return msg;
        }
    }
}