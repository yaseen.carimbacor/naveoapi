﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoWebMaps.Models
{
    public class qgisLayer
    {
        public int order { get; set; }
        public String id { get; set; }
        public String name { get; set; }
        public Boolean ticked { get; set; }
        public String NaveoModels { get; set; }
        public List<qgisLayerFeatures> lFeatures { get; set; }
    }

    public class qgisLayerFeatures
    {
        public String featureName { get; set; }
        public Boolean ticked { get; set; }
    }
}