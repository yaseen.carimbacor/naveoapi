﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoWebMaps.Models
{
    public class RoutingData
    {
        public Guid UserToken { get; set; }
        public List<lonLatCoordinate> lPoints { get; set; } = new List<lonLatCoordinate>();
        public double searchRadiusInMeters { get; set; } = 150;
        public int bestRouteIterations { get; set; } = 100;
    }

    public class lonLatCoordinate
    {
        public Double lon { get; set; }
        public Double lat { get; set; }
    }

}