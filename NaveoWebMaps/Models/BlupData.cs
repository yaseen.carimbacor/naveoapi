﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace NaveoWebMaps.Models
{
    public class BlupData
    {
        public class BlupCreation
        {
            public String BlupPath { get; set; }
            public DataTable dtData { get; set; }
            public String dtTableName { get; set; }
            public Double lon { get; set; }
            public Double lat { get; set; }
            public Boolean bShowBuffers { get; set; }

            public DataSet ds { get; set; }
        }

        public class BlupApplications
        {
            public String GeomData { get; set; }
            public Double Height { get; set; }
            public Double SurfaceArea { get; set; }
            public String type { get; set; }
        }
    }
}