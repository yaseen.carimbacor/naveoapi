﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace NaveoWebMaps.Models
{
    public class TripData
    {
        public Guid UserToken { get; set; }
        public DataTable dtData { get; set; }
    }
}