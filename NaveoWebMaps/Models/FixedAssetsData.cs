﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoWebMaps.Models
{
    public class FixedAssetsData
    {
        public class FixedAssetsCreation
        {
            public String sPath { get; set; }
            public String sFeatureID { get; set; }
        }
    }
}