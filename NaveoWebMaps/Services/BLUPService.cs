﻿using NaveoWebMaps.Constants;
using NaveoWebMaps.Helpers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Web;
using ThinkGeo.MapSuite.Shapes;
using static NaveoWebMaps.Models.BlupData;

namespace NaveoWebMaps.Services
{
    public class BLUPService
    {
        String SettlementBoundaryPath = LayerPaths.BlupShpPath("SettlementBoundary.shp");
        String OnEdgePath = LayerPaths.BlupShpPath("SettlementBuffer.shp");
        String RiverPath = LayerPaths.BlupShpPath("River.shp");
        String River16mPath = LayerPaths.BlupShpPath("RiverBuffer.shp");
        String GrowthZonePath = LayerPaths.BlupShpPath("GrowthZone.shp");
        String ScenicPath = LayerPaths.BlupShpPath("ScenicLandscape.shp");
        String MinResBufferPath = LayerPaths.BlupShpPath("MinResBuffer.shp");
        String ABRdBufferPath = LayerPaths.BlupShpPath("ABRdBuffer.shp");
        String ResidentialBufferPath = LayerPaths.BlupShpPath("ResidentialBuffer.shp");
        String CBDpath = LayerPaths.BlupShpPath("CentralBusinessDistrict.shp");
        String ParkingPath = LayerPaths.BlupShpPath("Parking.shp");
        String BlupPath = LayerPaths.BlupShpPath("");

        Boolean isSD1(BaseShape polygon)
        {
            //inside SettlementBoundary
            Boolean bResult = false;
            DataTable dt = ShapeFileHelper.QryPolygonInside(polygon, SettlementBoundaryPath);
            if (dt.Rows.Count > 0)
                bResult = true;

            return bResult;
        }
        Boolean isSD3(BaseShape polygon)
        {
            //inside OnEdge. OnEdge in SettlementBoundary outside buffer. So it outside SettlementBoundary, but inside OnEdge
            //|----------------|    OnEdge
            //| -------------  |    SettlementBoundary
            //| |           |  |
            //| |           |  |
            //| -------------  |    SettlementBoundary
            //|----------------|    OnEdge

            Boolean bResult = isSD1(polygon);
            if (!bResult)
            {
                DataTable dt = ShapeFileHelper.QryPolygonInside(polygon, OnEdgePath);
                if (dt.Rows.Count > 0)
                    bResult = true;
            }

            return bResult;
        }
        Boolean isSD4(BaseShape polygon)
        {
            //outside
            Boolean bResult = false;
            DataTable dt = ShapeFileHelper.QryPolygonInside(polygon, OnEdgePath);
            if (dt.Rows.Count == 0)
                bResult = true;

            return bResult;
        }
        Boolean isInside(BaseShape polygon, String shpPath)
        {
            //inside shpPath
            Boolean bResult = false;
            DataTable dt = ShapeFileHelper.QryPolygonInside(polygon, shpPath);
            if (dt.Rows.Count > 0)
                bResult = true;

            return bResult;
        }
        Boolean isOverlap(BaseShape polygon, String shpPath)
        {
            //inside shpPath
            Boolean bResult = false;
            DataTable dt = ShapeFileHelper.QryPolygonOverlap(polygon, shpPath);
            if (dt.Rows.Count > 0)
                bResult = true;

            return bResult;
        }

        public DataTable AnalyzeApplications(BlupApplications ba)
        {
            BaseShape poly = BaseShape.CreateShapeFromWellKnownData(ba.GeomData);
            DataTable dtResult = new DataTable("Result");
            dtResult.Columns.Add("id");
            dtResult.Columns.Add("description");
            dtResult.Columns.Add("value");
            dtResult.Columns.Add("Link");
            dtResult.Columns.Add("BoundingBox");
            dtResult.Columns.Add("icon");
            dtResult.Columns.Add("LayerName");
            Boolean b = false;
            int iCnt = 0;
            DataTable dt = new DataTable();
            switch (ba.type)
            {
                #region Residential
                case ("Residential"):
                    #region Settlement Boundary
                    String value = String.Empty;
                    String Desc = String.Empty;
                    b = isSD1(poly);
                    if (b)
                    {
                        Desc = "SD1";
                        value = "Inside Settlement Boundary";
                    }
                    else
                    {
                        b = isSD3(poly);
                        if (b)
                        {
                            Desc = "SD3";
                            value = "On Edge";
                        }
                        else
                        {
                            b = isSD4(poly);
                            if (b)
                            {
                                Desc = "SD4";
                                value = "Outside Settlement Boundary";
                            }
                        }
                    }
                    dtResult.Rows.Add("SD", Desc, value);

                    //Outside Settlement Boundary
                    if (Desc == "SD4")
                    {
                        DataTable dtNeatests = ShapeFileHelper.QryPolygonNeatestTo(poly, 2, SettlementBoundaryPath);
                        foreach (DataRow r in dtNeatests.Rows)
                            dtResult.Rows.Add("Distance", r["AssetName"].ToString(), r["NDistance"].ToString());
                    }
                    #endregion

                    //GrowthZone
                    DataTable dtGrowthZone = ShapeFileHelper.QryPolygonInside(poly, GrowthZonePath);
                    if (dtGrowthZone.Rows.Count > 0)
                        dtResult.Rows.Add("GrowthZone", dtGrowthZone.Rows[0]["AssetName"].ToString(), "true");
                    else
                    {
                        dtResult.Rows.Add("GrowthZone", "Inside", "false");
                        dtGrowthZone = ShapeFileHelper.QryPolygonNeatestTo(poly, 1, GrowthZonePath);
                        foreach (DataRow r in dtGrowthZone.Rows)
                            dtResult.Rows.Add("Distance", r["AssetName"].ToString(), r["NDistance"].ToString());
                    }

                    //Scenic Landscape
                    if (ba.Height > 7.5)
                    {
                        DataTable dtSnenic = ShapeFileHelper.QryPolygonNeatestTo(poly, 1, 1000, ScenicPath);
                        foreach (DataRow r in dtSnenic.Rows)
                            dtResult.Rows.Add("Distance", r["AssetName"].ToString(), r["NDistance"].ToString());
                    }

                    //MineralResource
                    b = isOverlap(poly, MinResBufferPath);
                    if (b)
                        dtResult.Rows.Add("Mineral Resource", "Inside", "true");
                    else
                        dtResult.Rows.Add("Mineral Resource", "Inside", "false");

                    //Road setbacks
                    b = isOverlap(poly, ABRdBufferPath);
                    if (b)
                        dtResult.Rows.Add("Road AB 6m", "Inside", "true");
                    else if (isOverlap(poly, ResidentialBufferPath))
                        dtResult.Rows.Add("Road Residential 3m", "Inside", "true");
                    else
                        dtResult.Rows.Add("Road", "Inside", "false");

                    #region Rivers
                    b = isOverlap(poly, RiverPath);
                    if (b)
                    {
                        Desc = "On River";
                        value = "true";
                    }
                    else
                    {
                        b = isOverlap(poly, River16mPath);
                        Desc = "On River Buffer 16 m";
                        if (b)
                            value = "true";
                        else
                            value = "false";
                    }
                    dtResult.Rows.Add("River", Desc, value);

                    dt = ShapeFileHelper.QryPolygonNeatestTo(poly, 1, RiverPath);
                    foreach (DataRow r in dt.Rows)
                        dtResult.Rows.Add("Distance", r["AssetName"].ToString(), r["NDistance"].ToString());
                    #endregion
                    break;
                #endregion

                #region Commercial
                case ("Commercial"):
                    b = isInside(poly, CBDpath);
                    if (b)
                    {
                        dtResult.Rows.Add("CBD", "Inside", "true");
                        dt = ShapeFileHelper.QryPolygonNeatestTo(poly, 250, 100, ParkingPath);
                        DataTable dtDistinct = new DataView(dt).ToTable(true, "Type");
                        foreach (DataRow r in dtDistinct.Rows)
                        {
                            iCnt = 0;
                            int i = 0;
                            DataRow[] dataRows = dt.Select("Type = '" + r["Type"].ToString() + "'");
                            foreach (DataRow dr in dataRows)
                            {
                                int.TryParse(dr["Quantity"].ToString(), out i);
                                iCnt += i;
                            }
                            dtResult.Rows.Add("Parking within 100m", r["Type"].ToString(), iCnt.ToString());
                        }
                    }
                    else
                        dtResult.Rows.Add("CBD", "Inside", "false");

                    #region Distances
                    List<String> lDistances = new List<String> { "BeautyParlour", "CitizenAdviceBureau", "HardwareStore", "Kendra", "Market", "Restaurants", "SubHall" };
                    foreach (String s in lDistances)
                    {
                        dt = ShapeFileHelper.QryPolygonNeatestTo(poly, 1, BlupPath + s + ".shp");
                        foreach (DataRow r in dt.Rows)
                            dtResult.Rows.Add("Distance", r["AssetName"].ToString(), r["NDistance"].ToString(), String.Empty, r["BoundingBox"].ToString(), String.Empty, s);
                    }

                    List<int> lParking = new List<int>() { 50, 100, 200 };
                    DataTable dtBB = new DataTable();
                    dtBB.Columns.Add("UpperLeftPointX", typeof(Double));
                    dtBB.Columns.Add("UpperLeftPointY", typeof(Double));
                    dtBB.Columns.Add("LowerRightPointX", typeof(Double));
                    dtBB.Columns.Add("LowerRightPointY", typeof(Double));
                    foreach (int i in lParking)
                    {
                        iCnt = 0;
                        dt = ShapeFileHelper.QryPolygonNeatestTo(poly, 250, i, ParkingPath);
                        foreach (DataRow r in dt.Rows)
                        {
                            int x = 0;
                            int.TryParse(r["Quantity"].ToString(), out x);
                            iCnt += x;

                            String[] ls = r["BoundingBox"].ToString().Split(',');
                            int y = 0;
                            DataRow dataRow = dtBB.NewRow();
                            foreach (String s in ls)
                            {
                                Double d = 0;
                                Double.TryParse(s, out d);
                                dataRow[y] = d;
                                y++;
                            }
                            dtBB.Rows.Add(dataRow);
                        }
                        String UpperLeftPointX = dtBB.Compute("Max(UpperLeftPointX)", String.Empty).ToString();
                        String UpperLeftPointY = dtBB.Compute("Max(UpperLeftPointY)", String.Empty).ToString();
                        String LowerRightPointX = dtBB.Compute("Min(LowerRightPointX)", String.Empty).ToString();
                        String LowerRightPointY = dtBB.Compute("Min(LowerRightPointY)", String.Empty).ToString();
                        String bb = UpperLeftPointX + "," + UpperLeftPointY + "," + LowerRightPointX + "," + LowerRightPointY;
                        if (bb == ",,,")
                            bb = String.Empty;
                        dtResult.Rows.Add("Parking", " within " + i + "m", iCnt.ToString(), String.Empty, bb, String.Empty, "Parking");
                    }
                    #endregion

                    break;
                    #endregion
            }

            #region GreenSpace
            dt = ShapeFileHelper.QryPolygonNeatestTo(poly, 1000, 100, BlupPath + "GreenSpace.shp");
            Double area = 0;
            Double area2 = 0;
            String GAbb = String.Empty;
            foreach (DataRow r in dt.Rows)
            {
                Double d = 0;
                Double.TryParse(r["Area"].ToString(), out d);
                area += d;
                GAbb = r["BoundingBox"].ToString();

                Collection<Feature> fss = new Collection<Feature>();
                Feature gfeatures = Feature.CreateFeatureFromWellKnownData(r["feature"].ToString());
                fss.Add(gfeatures);
                area2 += GetUtmAreaMeters(4326, gfeatures);
            }

            Collection<Feature> fs = new Collection<Feature>();
            Feature gfeature = Feature.CreateFeatureFromWellKnownData(ba.GeomData);
            fs.Add(gfeature);
            double polyarea = GetUtmAreaMeters(4326, gfeature);

            Double GreenPercent = (polyarea / area) * 100;
            dtResult.Rows.Add("Area", "Green Space Area", GreenPercent, String.Empty, GAbb, String.Empty, "GreenSpace");
            #endregion

            #region Household
            dt = ShapeFileHelper.QryPolygonNeatestTo(poly, 250, 100, BlupPath + "ResidentialRoad.shp");
            iCnt = 0;
            foreach (DataRow r in dt.Rows)
            {
                int i = 0;
                int.TryParse(r["Household"].ToString(), out i);
                iCnt += i;
            }
            dtResult.Rows.Add("Number of household 100m", String.Empty, iCnt.ToString());
            #endregion
            return dtResult;
        }
        public double GetUtmAreaMeters(int sourceDataEPSGId, Feature sourceFeature)
        {
            BaseShape sourceShape = sourceFeature.GetShape();
            PointShape centerShape = sourceShape.GetCenterPoint();
            int utmzoneEpsg = Convert.ToInt32(32700.0 - Math.Round((45.0 + centerShape.Y) / 90.0) * 100.0 + Math.Round((183.0 + centerShape.X) / 6.0));
            Proj4Projection projection = new Proj4Projection();
            projection.InternalProjectionParametersString = Proj4Projection.GetEpsgParametersString(sourceDataEPSGId);
            projection.ExternalProjectionParametersString = Proj4Projection.GetEpsgParametersString(utmzoneEpsg);
            projection.Open();
            var projectedShape = projection.ConvertToExternalProjection(sourceShape);
            var wkb = projectedShape.GetWellKnownBinary();
            var wkbReader = new NetTopologySuite.IO.WKBReader();
            var geometry = wkbReader.Read(wkb);

            return geometry.Area;
        }

        void GetResult(BaseShape polygon)
        {
            Boolean b = isSD1(polygon);
            Boolean b2 = isSD4(polygon);
        }
        public void test()
        {
            BaseShape bs = BaseShape.CreateShapeFromWellKnownData("POLYGON ((57.58181656176885 -20.114445853168515,57.58181656176885 -20.073925877961724,57.62136137094444 -20.073925877961724,57.62136137094444 -20.114445853168515,57.58181656176885 -20.114445853168515))");

            ShapeFileHelper shapeFileHelper = new ShapeFileHelper();
            shapeFileHelper.BuildIndexFromDir(@"C:\Reza\Geo\SDK\NaveoOneWebV2\NaveoWebMaps\MapData\Blup");
            shapeFileHelper.QryAllFeaturesFromShp(1, 2, 4, @"C:\Reza\Geo\Maps\Creations\ROAD\Tmp\SD4.shp");

            //Esperance
            MultipolygonShape esperance = new MultipolygonShape("MULTIPOLYGON(((57.593017578125 -20.2274780273438,57.5892944335938 -20.2288208007813,57.5859985351563 -20.2265014648438,57.586181640625 -20.2208862304688,57.5903930664063 -20.2203369140625,57.5941162109375 -20.2239379882813,57.593017578125 -20.2274780273438)))");

            //Mapou
            MultipolygonShape mapou = new MultipolygonShape("MULTIPOLYGON(((57.6071166992188 -20.0836181640625,57.60400390625 -20.0859375,57.601806640625 -20.0848999023438,57.6013793945313 -20.0811767578125,57.6033325195313 -20.077880859375,57.6057739257813 -20.075439453125,57.6088256835938 -20.0748901367188,57.6097412109375 -20.078125,57.608642578125 -20.0799560546875,57.6071166992188 -20.0836181640625)))");

            DataTable dtEsperance = ShapeFileHelper.QryPolygonInside(esperance, @"C:\Reza\Geo\Maps\Creations\ROAD\SHAPE\DistrictMoka.shp");
            DataTable dtMapou = ShapeFileHelper.QryPolygonInside(mapou, @"C:\Reza\Geo\Maps\Creations\ROAD\SHAPE\DistrictMoka.shp");

            DataTable dtEsperanceDistance = ShapeFileHelper.QryPolygonDistance(esperance, 9000, @"C:\Reza\Geo\Maps\Creations\ROAD\SHAPE\DistrictMoka.shp");
            DataTable dtMapouDistance = ShapeFileHelper.QryPolygonDistance(mapou, 9000, @"C:\Reza\Geo\Maps\Creations\ROAD\SHAPE\DistrictMoka.shp");

            DataTable dtEsperanceNearRoad = ShapeFileHelper.QryPolygonNeatestTo(esperance, 1, @"C:\Reza\Geo\Maps\Creations\ROAD\SHAPE\Esperance_Road_Network.shp");

            //SettlementBoundary
            MultipolygonShape SettlementBoundary = new MultipolygonShape("MULTIPOLYGON(((57.4967086335715 -20.2399600453259,57.496101871015 -20.2395317423449,57.4958520276093 -20.2393532827694,57.4956021842038 -20.2392462070242,57.4951381893076 -20.2395674342599,57.4949597297321 -20.2401741968164,57.4947812701566 -20.2407095755427,57.4945671186661 -20.2413163380992,57.4944600429209 -20.2417089491651,57.4941388156851 -20.242065868316,57.4934249773834 -20.2423157117217,57.4930680582325 -20.2424227874669,57.4925326795062 -20.2425655551273,57.4894988667238 -20.242458479382,57.4799691253954 -20.2423514036367,57.4798977415652 -20.242065868316,57.4796835900747 -20.241637565335,57.4794337466691 -20.2414591057596,57.4787556002825 -20.2414234138445,57.4781845296411 -20.2415661815048,57.4777919185751 -20.2416732572501,57.4773636155941 -20.241637565335,57.4771494641034 -20.2414947976746,57.4769710045281 -20.2413877219294,57.4771494641034 -20.2411378785238,57.4774349994241 -20.2409594189484,57.4779346862354 -20.2405668078823,57.4780774538958 -20.2400671210711,57.4796478981597 -20.2378185304206,57.4798263577351 -20.2374259193547,57.4798263577351 -20.2371046921189,57.4797549739049 -20.236747772968,57.4796478981597 -20.2364979295623,57.4795408224144 -20.2360339346662,57.479005443688 -20.2357483993456,57.478577140707 -20.2352844044494,57.4782202215561 -20.2347847176382,57.4780417619808 -20.2342493389119,57.4779703781505 -20.2337853440158,57.4782559134712 -20.2336068844403,57.4786842164522 -20.2333213491196,57.4789340598578 -20.2328573542235,57.4790411356032 -20.2323933593274,57.4789340598578 -20.2320007482614,57.4787556002825 -20.2317152129407,57.4786842164522 -20.2313939857049,57.4786842164522 -20.2311441422992,57.4788269841127 -20.2307872231484,57.4791839032634 -20.2305730716579,57.4794337466691 -20.2302518444221,57.4795408224144 -20.2299663091014,57.4794694385842 -20.2295023142052,57.4793623628389 -20.229002627394,57.4791482113484 -20.2285743244129,57.478969751773 -20.2281460214319,57.4789340598578 -20.2277891022811,57.4788983679427 -20.2271466478095,57.4789340598578 -20.2267183448284,57.4788269841127 -20.2263971175927,57.4789340598578 -20.226147274187,57.4791839032634 -20.2260401984418,57.4796835900747 -20.2258260469512,57.4799334334804 -20.2255762035456,57.4799334334804 -20.2251835924797,57.4798620496501 -20.2247195975835,57.4798620496501 -20.224469754178,57.4799334334804 -20.2242556026874,57.4801118930558 -20.2242556026874,57.4803260445464 -20.224505446093,57.4806115798671 -20.2247552894987,57.4808257313576 -20.2247195975835,57.4810755747631 -20.224505446093,57.4813611100838 -20.224505446093,57.4815752615743 -20.2245768299232,57.4817180292347 -20.2248623652439,57.4816466454045 -20.2251835924797,57.481432493914 -20.2253620520551,57.4812540343386 -20.2255762035456,57.4812540343386 -20.2258617388663,57.4812540343386 -20.2271466478095,57.4813611100838 -20.2274678750452,57.481468185829 -20.2277177184508,57.4816466454045 -20.2279318699414,57.4819321807253 -20.2280032537716,57.4821820241308 -20.2282530971772,57.482217716046 -20.2286457082432,57.4820749483855 -20.2289312435639,57.4819321807253 -20.22939523846,57.4820749483855 -20.2308942988937,57.4823604837063 -20.2315724452803,57.4824675594515 -20.2320364401765,57.4825746351967 -20.2325361269877,57.4825746351967 -20.2331428895442,57.4825032513665 -20.233892419761,57.482646019027 -20.2360696265814,57.4828244786024 -20.2365693133926,57.4829672462627 -20.2369262325435,57.4833598573287 -20.2373188436094,57.4836097007343 -20.2376757627603,57.4839666198852 -20.2381040657414,57.4843592309511 -20.2383182172318,57.4847875339323 -20.2383182172318,57.4853586045735 -20.2383182172318,57.4856084479792 -20.2380326819112,57.4858582913848 -20.2380326819112,57.4861795186207 -20.2381754495715,57.4866435135167 -20.2384609848922,57.4873573518184 -20.2384609848922,57.4878927305448 -20.2385680606375,57.4886779526768 -20.2396745100052,57.4892847152332 -20.2392818989392,57.4885351850163 -20.2381040657414,57.4903911646009 -20.234285030827,57.4907480837518 -20.2340351874214,57.4910336190726 -20.2338567278459,57.4913191543933 -20.2335711925252,57.4914976139686 -20.2331785814593,57.4916760735441 -20.2326075108178,57.4918188412045 -20.232250591667,57.4943172752606 -20.2325361269877,57.4947455782416 -20.2324647431575,57.4953523407981 -20.232286283582,57.495709259949 -20.231857980601,57.4959234114395 -20.2313582937898,57.49613756293 -20.2310013746389,57.4966729416564 -20.230608763573,57.4981363101749 -20.2299306171863,57.4990642999672 -20.2296093899506,57.4991356837973 -20.2291453950544,57.4991713757125 -20.2285743244129,57.4992427595425 -20.2281103295168,57.4994926029482 -20.2279318699414,57.5000279816745 -20.2278604861112,57.5002778250802 -20.2278247941962,57.5004919765707 -20.2276106427056,57.5004919765707 -20.227325107385,57.5001707493349 -20.2271823397246,57.4995639867785 -20.226968188234,57.4990999918823 -20.2267540367436,57.4996353706085 -20.2257903550361,57.5007418199764 -20.2245768299232,57.5008132038064 -20.2230063856593,57.4984575374108 -20.2231491533197,57.4985646131559 -20.2213288656503,57.4992070676275 -20.2215430171408,57.499813830184 -20.2215430171408,57.5002421331652 -20.2213288656503,57.5006704361462 -20.2212217899049,57.5007418199764 -20.2207934869239,57.5005990523159 -20.220436567773,57.5002421331652 -20.2202224162825,57.499849522099 -20.2200796486221,57.4988501484766 -20.2181522852074,57.4983504616654 -20.2184378205282,57.4982076940051 -20.2189018154243,57.4982790778353 -20.2192587345752,57.4986716889013 -20.2196513456411,57.4991356837973 -20.2199725728769,57.4993498352878 -20.2202581081976,57.4979221586844 -20.2203651839429,57.4971369365525 -20.2204722596881,57.4958877195245 -20.2204722596881,57.495316648883 -20.2206150273485,57.4947812701566 -20.2205079516032,57.4941031237701 -20.2199725728769,57.4931394420627 -20.2196156537261,57.492461295676 -20.2192587345752,57.4911050029027 -20.2192587345752,57.4903197807707 -20.2195085779808,57.4888207203371 -20.2197941133014,57.4879998062901 -20.2196513456411,57.4869647407526 -20.2193658103204,57.4861795186207 -20.2193658103204,57.4857512156395 -20.219579961811,57.4853229126585 -20.2196156537261,57.4847875339323 -20.2193301184053,57.4842521552059 -20.2188304315941,57.4832884734985 -20.2184735124433,57.482610327112 -20.2184021286131,57.4839309279701 -20.2172599873303,57.4844306147813 -20.2176169064812,57.4847875339323 -20.2177596741415,57.4852158369133 -20.2179024418019,57.4856084479792 -20.2176525983962,57.4858939832999 -20.2175455226509,57.4866792054319 -20.2175455226509,57.4877142709694 -20.2176525983962,57.489213331403 -20.2173670630755,57.4898200939595 -20.2168673762642,57.4901770131104 -20.2161178460475,57.4906766999217 -20.2157609268966,57.4912120786479 -20.2156538511513,57.4919259169498 -20.2156538511513,57.4922828361005 -20.2158680026418,57.4917831492893 -20.216331997538,57.4916760735441 -20.2168316843492,57.4922114522703 -20.217152911585,57.4933892854682 -20.2173313711605,57.4946385024963 -20.2176525983962,57.4954594165433 -20.2179381337169,57.496530173996 -20.2180452094622,57.4973510880429 -20.2179024418019,57.4976723152789 -20.2176169064812,57.4984218454956 -20.2173670630755,57.4987073808163 -20.2172599873303,57.4979935425146 -20.216367689453,57.499813830184 -20.2154040077457,57.5004919765707 -20.2165818409436,57.5015270421083 -20.2164390732833,57.5023122642401 -20.2161892298777,57.503097486372 -20.2158323107268,57.503454405523 -20.2167959924341,57.5039897842492 -20.2164390732833,57.50427531957 -20.2168673762642,57.5047750063811 -20.2166532247738,57.5053460770226 -20.218009517547,57.5052746931923 -20.2186519720186,57.5051676174471 -20.2192587345752,57.5052746931923 -20.2196156537261,57.5056673042583 -20.2197227294713,57.5062383748998 -20.2199011890468,57.506773753626 -20.220008264792,57.5072734404372 -20.2199368809618,57.5074519000127 -20.2194728860657,57.5074875919277 -20.2189375073394,57.5077731272485 -20.2184378205282,57.5083085059749 -20.2185805881885,57.5085583493804 -20.2188304315941,57.5093792634275 -20.2189018154243,57.5105214047102 -20.2188661235092,57.5118063136534 -20.2187233558489,57.5128413791908 -20.2186519720186,57.5133767579173 -20.218366436698,57.513697985153 -20.2180809013773,57.5139121366435 -20.2181522852074,57.5140192123889 -20.2184735124433,57.5145545911152 -20.2187233558489,57.5155896566527 -20.2190088911696,57.5168031817657 -20.2192944264902,57.5171244090015 -20.2191159669148,57.5171244090015 -20.2186519720186,57.5175170200674 -20.2182236690376,57.5182665502842 -20.2173670630755,57.5188019290106 -20.2160821541323,57.519801302633 -20.2149400128495,57.5211219034912 -20.2140477149724,57.5228351154154 -20.2133338766706,57.5239058728681 -20.2126557302839,57.5247981707452 -20.2133695685857,57.5240843324435 -20.2141190988025,57.5249766303207 -20.2145117098685,57.5256190847922 -20.2145117098685,57.5259403120281 -20.2142261745478,57.5264399988393 -20.2135123362461,57.5265113826695 -20.2129412656046,57.5266898422448 -20.2122274273029,57.5273679886315 -20.2119062000672,57.5277962916125 -20.2116920485766,57.5278319835277 -20.2120489677275,57.5282245945937 -20.2125843464538,57.5289741248104 -20.2130126494349,57.5292596601312 -20.2134409524159,57.5272252209713 -20.2158323107268,57.5337925333473 -20.2204722596881,57.5357912805922 -20.2181879771225,57.5354700533564 -20.218009517547,57.5356485129319 -20.2176525983962,57.5343636039888 -20.2165818409436,57.535719896762 -20.2148329371043,57.5375401844315 -20.2160821541323,57.5381112550728 -20.2153326239156,57.5388964772047 -20.2158680026418,57.5397887750819 -20.2150113966798,57.5401456942329 -20.2147615532741,57.5404312295536 -20.214725861359,57.5408595325346 -20.2150113966798,57.5419302899872 -20.215511083491,57.5407881487045 -20.2167246086039,57.541466295091 -20.217152911585,57.5409309163648 -20.2178667498867,57.5425370525437 -20.2190088911696,57.5427512040342 -20.218794739679,57.5440718048926 -20.219579961811,57.5424299767985 -20.2217928605464,57.5451782542602 -20.2227922341688,57.5461419359676 -20.2218285524615,57.5457136329865 -20.2215787090558,57.5467843904391 -20.2201867243675,57.547926531722 -20.2209719464993,57.5482477589578 -20.2203294920278,57.5474625368258 -20.2199368809618,57.5477123802314 -20.2192587345752,57.5472126934201 -20.2189731992544,57.547926531722 -20.2181165932924,57.548747445769 -20.2186876639337,57.5499609708819 -20.2172242954152,57.5503535819479 -20.2175098307358,57.5508175768441 -20.2168673762642,57.5517812585513 -20.2174741388208,57.5523523291927 -20.2165104571134,57.5585984143331 -20.2208648707541,57.5562427479372 -20.223684532046,57.552066793872 -20.2209362545842,57.551638490891 -20.2214716333106,57.5523880211078 -20.2218999362916,57.5511388040798 -20.223256229065,57.5486046781085 -20.2215073252257,57.5445357997885 -20.2262900418473,57.5442145725528 -20.2267183448284,57.5441431887226 -20.2271823397246,57.5443930321283 -20.2277534103659,57.5445714917038 -20.2284315567526,57.5397173912519 -20.2294666222902,57.5399672346574 -20.230216152507,57.5389321691199 -20.2303946120825,57.5392890882707 -20.2320364401765,57.5380398712427 -20.2323933593274,57.537790027837 -20.2312512180446,57.5343636039888 -20.231857980601,57.5337211495171 -20.2297521576109,57.5327574678097 -20.2300733848467,57.5310799478007 -20.224469754178,57.53079441248 -20.224898057159,57.5305802609895 -20.2252549763099,57.5301876499233 -20.22532636014,57.5295451954519 -20.2250408248194,57.5298307307726 -20.2271109558944,57.5272966048013 -20.2276463346207,57.527510756292 -20.2285743244129,57.5260116958581 -20.2287527839884,57.5255477009621 -20.2272537235547,57.5246197111697 -20.2276106427056,57.5244412515945 -20.2268968044039,57.5232634183965 -20.2272537235547,57.5220498932835 -20.2270038801491,57.5218714337082 -20.2262186580172,57.5240129486133 -20.2258974307815,57.5239058728681 -20.2252906682249,57.5225138881796 -20.224898057159,57.5202652975291 -20.224898057159,57.5204794490197 -20.2258617388663,57.5192659239066 -20.2257546631211,57.5194443834821 -20.2268611124888,57.5183022421994 -20.2270395720643,57.5180167068787 -20.226147274187,57.5158038081433 -20.2266826529134,57.513662293238 -20.2273964912151,57.5143047477095 -20.2291453950544,57.5111281672667 -20.2298592333561,57.5107355562008 -20.2286814001583,57.5098789502388 -20.2287884759035,57.5098075664085 -20.2281817133471,57.5077731272485 -20.2290383193091,57.506773753626 -20.2296807737807,57.5058457638338 -20.2304303039975,57.5051319255321 -20.2312512180446,57.5044180872304 -20.2319293644312,57.5034187136077 -20.2324647431575,57.5080586625692 -20.2406024997974,57.5087011170409 -20.241244954269,57.5094149553426 -20.2419944844858,57.510414328965 -20.2422800198066,57.5114850864176 -20.2423514036367,57.5144475153699 -20.2427083227876,57.5121275408891 -20.2446713781175,57.5089866523616 -20.2467415091925,57.5078088191636 -20.2461704385511,57.505917147664 -20.246527357702,57.5058100719188 -20.2459919789757,57.5055959204283 -20.2454922921644,57.5052746931923 -20.2451710649287,57.5044894710604 -20.2448498376929,57.503847016589 -20.2449926053532,57.5029190267966 -20.2451353730136,57.5021338046646 -20.2453495245041,57.5019196531741 -20.2455279840796,57.5022408804101 -20.2437076964101,57.5019553450893 -20.243279393429,57.5013485825328 -20.2429938581083,57.5014556582781 -20.2423157117217,57.5016698097686 -20.2416018734199,57.5010630472121 -20.2411378785238,57.50020644125 -20.2407809593729,57.4990642999672 -20.2404597321371,57.4989215323068 -20.2403169644768,57.499421219118 -20.2386394444677,57.5000636735897 -20.2365693133926,57.5008488957216 -20.2344634904024,57.5014913501931 -20.2334284248648,57.5017055016836 -20.2324647431575,57.5013842744479 -20.2321435159217,57.500634744231 -20.2319650563463,57.4999565978445 -20.2321435159217,57.4999565978445 -20.2328216623084,57.5000636735897 -20.2334641167799,57.4993141433728 -20.2348561014683,57.4986003050711 -20.2362480861568,57.4982790778353 -20.2375329951,57.4977436991089 -20.2388892878733,57.4978150829392 -20.2399600453259,57.4976723152789 -20.2404954240522,57.4967086335715 -20.2399600453259)))");
            DataTable dtSettlementBoundary = ShapeFileHelper.QryPolygonInside(SettlementBoundary, @"C:\Reza\Geo\Maps\Creations\ROAD\SHAPE\MauritiusLocalityVca.shp");


            MultipolygonShape SD1 = new MultipolygonShape("MULTIPOLYGON(((57.5278547643551 -20.2162995253995,57.5297250207056 -20.2176558181727,57.5275692290344 -20.2195546280554,57.5258845706422 -20.2181840585161,57.5278547643551 -20.2162995253995)))");
            MultipolygonShape SD4 = new MultipolygonShape("MULTIPOLYGON(((57.5313002238914 -20.2115739158419,57.532727900495 -20.2101462392384,57.5345172551714 -20.2126589500605,57.53394618453 -20.2135345917107,57.5313002238914 -20.2115739158419)))");
            GetResult(SD1);
            GetResult(SD4);
        }
    }
}