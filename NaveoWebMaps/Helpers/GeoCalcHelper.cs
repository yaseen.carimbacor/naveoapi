﻿using NaveoWebMaps.Constants;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using ThinkGeo.MapSuite;
using ThinkGeo.MapSuite.Drawing;
using ThinkGeo.MapSuite.Layers;
using ThinkGeo.MapSuite.Serialize;
using ThinkGeo.MapSuite.Shapes;
using ThinkGeo.MapSuite.Styles;
using ThinkGeo.MapSuite.WebApi;
using static NaveoWebMaps.Models.BlupData;

namespace NaveoWebMaps.Helpers
{
    public class GeoCalcHelper
    {
        #region Using InMemory
        DataTable initDTBuffer()
        {
            DataTable dtBuffer = new DataTable("dtBuffer");
            dtBuffer.Columns.Add("X", typeof(float));
            dtBuffer.Columns.Add("Y", typeof(float));
            dtBuffer.Columns.Add("Radius", typeof(int));
            dtBuffer.Columns.Add("iColor", typeof(int));
            dtBuffer.Columns.Add("sDesc", typeof(String));
            return dtBuffer;
        }

        GeoCollection<InMemoryFeatureLayer> GetCircles(Double lon, Double lat)
        {
            DataTable dtBuffer = initDTBuffer();
            DataRow dr = dtBuffer.NewRow();

            dr = dtBuffer.NewRow();
            dr["X"] = lon;
            dr["Y"] = lat;
            dr["radius"] = 300;
            dr["iColor"] = Color.FromArgb(75, Color.Red).ToArgb();
            dtBuffer.Rows.Add(dr);

            dr = dtBuffer.NewRow();
            dr["X"] = lon;
            dr["Y"] = lat;
            dr["radius"] = 200;
            dr["iColor"] = Color.FromArgb(75, Color.Yellow).ToArgb();
            dtBuffer.Rows.Add(dr);

            dr = dtBuffer.NewRow();
            dr["X"] = lon;
            dr["Y"] = lat;
            dr["radius"] = 100;
            dr["iColor"] = Color.FromArgb(75, Color.Green).ToArgb();
            dtBuffer.Rows.Add(dr);

            return GetApplicationWith300m(dtBuffer);
        }
        GeoCollection<InMemoryFeatureLayer> GetApplicationWith300m(DataTable dtBuffer)
        {
            //LayerOverlay layerOverlay = new LayerOverlay();
            //Proj4Projection proj4 = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetSphericalMercatorParametersString());
            //proj4 = new Proj4Projection(4326, 3857);
            //if (!proj4.IsOpen) proj4.Open();
            GeoCollection<InMemoryFeatureLayer> lmemo = new GeoCollection<InMemoryFeatureLayer>();
            foreach (DataRow dr in dtBuffer.Rows)
            {
                PointShape centerPointInDecimalDegree = new PointShape((float)Convert.ToDouble(dr["X"]), (float)Convert.ToDouble(dr["Y"]));
                EllipseShape circle = new EllipseShape(centerPointInDecimalDegree, Convert.ToInt32(dr["Radius"]), GeographyUnit.DecimalDegree, DistanceUnit.Meter);
                Feature f = new Feature(circle);//.MakeValid();
                f.ColumnValues.Add("Radius", dr["Radius"].ToString());

                Color c = Color.FromArgb(Convert.ToInt32(dr["iColor"]));
                GeoColor color = GeoColor.FromArgb(c.A, c.R, c.G, c.B);
                InMemoryFeatureLayer memo = new InMemoryFeatureLayer();

                memo.Open();
                memo.EditTools.BeginTransaction();
                memo.EditTools.Add(f);
                memo.EditTools.CommitTransaction();
                memo.Close();

                //memo.FeatureSource.Projection = proj4;
                memo.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = new ThinkGeo.MapSuite.Styles.AreaStyle(new ThinkGeo.MapSuite.Drawing.GeoSolidBrush(color));
                memo.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = new ThinkGeo.MapSuite.Styles.LineStyle(new GeoPen(color));
                memo.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
                lmemo.Add(memo);
            }
            return lmemo;
        }
        InMemoryFeatureLayer GetPoints(DataTable dt)
        {
            InMemoryFeatureLayer mapLayer = new InMemoryFeatureLayer();
            mapLayer.Name = "BlupPoints";
            mapLayer.Open();
            mapLayer.EditTools.BeginTransaction();
            foreach (DataRow dr in dt.Rows)
            {
                Feature feature = new Feature(dr["GeomData"].ToString());
                feature.ColumnValues.Add("APL_ID", dr["APL_ID"].ToString());
                mapLayer.EditTools.Add(feature);
            }
            mapLayer.EditTools.CommitTransaction();

            mapLayer.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = new ThinkGeo.MapSuite.Styles.AreaStyle(GeoPens.Black);
            mapLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level16;
            mapLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("aplid", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
            //mapLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = new PointStyle(PointSymbolType.Diamond2, new GeoSolidBrush(GeoColors.Yellow), 3);
            mapLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = new PointStyle(new GeoImage(LayerPaths.iconsPath("BlupHome.png")));
            mapLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

            mapLayer.Close();
            //Proj4Projection proj4 = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetSphericalMercatorParametersString());
            //proj4 = new Proj4Projection(4326, 3857);
            //if (!proj4.IsOpen) proj4.Open();
            //mapLayer.FeatureSource.Projection = proj4;
            //mapLayer.ZoomLevelSet.ZoomLevel10.DefaultAreaStyle = new ThinkGeo.MapSuite.Styles.AreaStyle(GeoPens.Black);
            //mapLayer.ZoomLevelSet.ZoomLevel10.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level16;

            //mapLayer.ZoomLevelSet.ZoomLevel17.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("aplid", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
            //mapLayer.ZoomLevelSet.ZoomLevel17.DefaultPointStyle = new PointStyle(PointSymbolType.Diamond2, new GeoSolidBrush(GeoColors.Yellow), 3);
            //mapLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = new PointStyle(new GeoImage(LayerPaths.iconsPath("BlupHome.png")));
            //mapLayer.ZoomLevelSet.ZoomLevel17.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

            return mapLayer;
        }
        public String CreateBlupWithinDistanceInMetersUsinInMemory(BlupCreation bc)
        {
            String sError = String.Empty;
            try
            {
                bc.BlupPath = Guid.NewGuid().ToString();

                GeoCollection<InMemoryFeatureLayer> getCircles = new GeoCollection<InMemoryFeatureLayer>();
                if (bc.bShowBuffers)
                    getCircles = GetCircles(bc.lon, bc.lat);
                InMemoryFeatureLayer getPoints = GetPoints(bc.dtData);
                getCircles.Add(getPoints);

                if (!(System.IO.Directory.Exists(LayerPaths.BLUPPath(String.Empty))))
                    System.IO.Directory.CreateDirectory(LayerPaths.BLUPPath(String.Empty));

                string[] filePaths = Directory.GetFiles(LayerPaths.BLUPPath(String.Empty), bc.BlupPath.ToString() + "*.*");
                filePaths = Directory.GetFiles(LayerPaths.BLUPPath(String.Empty), "*.*");
                foreach (String file in filePaths)
                {
                    try
                    {
                        FileInfo fi = new FileInfo(file);
                        if (fi.LastAccessTime < DateTime.Now.AddHours(-1))
                            fi.Delete();
                    }
                    catch { }
                }

                GeoSerializer gs = new GeoSerializer(new XmlGeoSerializationFormatter());
                int iCnt = 0;
                foreach (InMemoryFeatureLayer g in getCircles)
                {
                    gs.Serialize(g, LayerPaths.BLUPPath(bc.BlupPath.ToString() + "_" + iCnt + ".mem"));
                    iCnt++;
                }
            }
            catch (Exception ex)
            {
                sError += ex.ToString();
            }
            return bc.BlupPath + sError;
        }
        public LayerOverlay ShowInMemoryLayers(String path)
        {
            LayerOverlay layerOverlay = new LayerOverlay();
            String[] filePaths = Directory.GetFiles(LayerPaths.BLUPPath(String.Empty), path + "*.shp");
            if (filePaths.Length == 0)
                return layerOverlay;

            Proj4Projection proj4 = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetSphericalMercatorParametersString());
            proj4 = new Proj4Projection(4326, 3857);

            GeoSerializer gs = new GeoSerializer(new XmlGeoSerializationFormatter());
            foreach (String file in filePaths)
            {
                InMemoryFeatureLayer memo = gs.Deserialize(file, GeoFileReadWriteMode.Read) as InMemoryFeatureLayer;
                memo.FeatureSource.Projection = proj4;
                layerOverlay.Layers.Add(memo);
            }

            return layerOverlay;
        }

        #endregion
    }
}