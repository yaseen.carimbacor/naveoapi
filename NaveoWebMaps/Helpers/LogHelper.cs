﻿using NaveoWebMaps.Constants;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace NaveoWebMaps.Helpers
{
    public class LogHelper
    {
        public static void WriteErrorToLog(string msg, string title, string app_path)
        {
            if (!(System.IO.Directory.Exists(app_path + "\\Errors\\")))
                System.IO.Directory.CreateDirectory(app_path + "\\Errors\\");

            FileStream fs = new FileStream(app_path + "\\Errors\\errlog.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite);
            StreamWriter s = new StreamWriter(fs);
            s.Close();
            fs.Close();

            try
            {
                FileInfo f = new FileInfo(app_path + "\\Errors\\errlog.txt");
                if (f.Length > 10000)
                    File.Delete(app_path + "\\Errors\\errlog.txt");
            }
            catch { }

            FileStream fs1 = new FileStream(app_path + "\\Errors\\errlog.txt", FileMode.Append, FileAccess.Write);
            StreamWriter s1 = new StreamWriter(fs1);
            s1.Write("Title: " + title + "\r\n");
            s1.Write("Message: " + msg + "\r\n");
            s1.Write("Date/Time: " + DateTime.Now.ToString() + "\r\n");
            s1.Write("===========================================================================================" + "\r\n");
            s1.Close();
            fs1.Close();
        }
        public static void WriteErrorToLog(string msg, string title)
        {
            WriteErrorToLog(msg, title, LayerPaths.logPath(String.Empty));
        }

        public static void WriteToLog(String strMessage)
        {
            try
            {
                FileInfo f = new FileInfo(LayerPaths.logPath(String.Empty));
                if (f.Length > 1000000)
                    File.Delete(LayerPaths.logPath(String.Empty) + "log.txt");
            }
            catch { }

            strMessage = DateTime.Now.ToString() + " > " + strMessage;

            try
            {
                File.AppendAllText(LayerPaths.logPath(String.Empty) + "log.txt", strMessage + "\r\n");
            }
            catch { }
        }

    }
}