﻿using NaveoWebMaps.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web;
using ThinkGeo.MapSuite;
using ThinkGeo.MapSuite.Drawing;
using ThinkGeo.MapSuite.Layers;
using ThinkGeo.MapSuite.Shapes;
using ThinkGeo.MapSuite.Styles;
using ThinkGeo.MapSuite.WebApi;
using static NaveoWebMaps.Models.RoutingData;
using static NaveoWebMaps.Models.ZonesData;

namespace NaveoWebMaps.Helpers
{
    public class InMemoryHelper
    {
        InMemoryFeatureLayer x()
        {
            InMemoryFeatureLayer m = new InMemoryFeatureLayer();
            m.Open();
            m.EditTools.BeginTransaction();

            Feature f = new Feature("POLYGON ((57.4967967864359 -20.269072983185, 57.4979876872385 -20.2695862700166, 57.4975585337961 -20.2709348979127, 57.4967109557474 -20.2717903647548, 57.495380580076 -20.2696969787184, 57.4967216845835 -20.269083047649, 57.4967967864359 -20.269072983185))");
            f.ColumnValues.Add("zoneid", "1");
            f.ColumnValues.Add("zonecolor", "123588856449");
            f.ColumnValues.Add("descrip", "Description");
            m.EditTools.Add(f);

            f = new Feature("POLYGON ((57.5586699839914 -20.0980341007879, 57.5606011744821 -20.0976210041371, 57.5601291056955 -20.0952230069331, 57.5587880011881 -20.0949610386272, 57.5579726096475 -20.0959786822835, 57.5586592551554 -20.0980341007879, 57.5586699839914 -20.0980341007879))");
            f.ColumnValues.Add("zoneid", "2");
            f.ColumnValues.Add("zonecolor", "-987123588449");
            f.ColumnValues.Add("descrip", "Descri");
            m.EditTools.Add(f);

            f = new Feature("POLYGON ((57.5010561343515 -20.168889217737, 57.5022792216623 -20.1700977267999, 57.5031589862192 -20.1690604237569, 57.5019788142527 -20.1679828297954, 57.5010561343515 -20.1688791467888, 57.5010561343515 -20.168889217737))");
            f.ColumnValues.Add("zoneid", "3");
            f.ColumnValues.Add("zonecolor", "12356449");
            f.ColumnValues.Add("descrip", "Destion");
            m.EditTools.Add(f);

            f = new Feature("LINESTRING(57.4967967864359 -20.269072983185, 57.5010561343515 -20.168889217737 ), 4326)");
            f.ColumnValues.Add("zoneid", "4");
            f.ColumnValues.Add("zonecolor", "12356449");
            f.ColumnValues.Add("descrip", "Destion");
            m.EditTools.Add(f);

            m.EditTools.CommitTransaction();
            m.Close();

            Layer t = m.CloneDeep();
            return m;
        }

        Layer l()
        {
            return x().CloneDeep();
        }

        public LayerOverlay ShowZones()
        {
            LayerOverlay layerOverlay = new LayerOverlay();

            Proj4Projection proj4 = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetSphericalMercatorParametersString());
            proj4 = new Proj4Projection(4326, 3857);
            if (!proj4.IsOpen) proj4.Open();

            InMemoryFeatureLayer m = x();
            m.FeatureSource.Projection = new Proj4Projection(4326, 3857);
            m.Name = "Zones";

            //m.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("descrip", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
            m.ZoomLevelSet.ZoomLevel01.DefaultAreaStyle = new AreaStyle(new ThinkGeo.MapSuite.Drawing.GeoSolidBrush(ThinkGeo.MapSuite.Drawing.GeoColor.StandardColors.Black));
            m.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = new LineStyle(new GeoPen(GeoColor.StandardColors.Blue));
            m.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

            layerOverlay.Layers.Add(m.Name, m);
            return layerOverlay;
        }

        void SaveRetrieve()
        {
            // Create a new FeatureLayer
            InMemoryFeatureLayer featureLayer1 = new InMemoryFeatureLayer();
            featureLayer1.InternalFeatures.Add(new Feature(1, 1));
            featureLayer1.Close();

            // Serialize to file
            FileStream fs1 = new FileStream("test.dat", FileMode.Create, FileAccess.ReadWrite);
            BinaryFormatter formatter1 = new BinaryFormatter();
            formatter1.Serialize(fs1, featureLayer1);
            fs1.Close();

            // Deserialize later .
            BinaryFormatter formatter2 = new BinaryFormatter();
            FileStream fs2 = new FileStream("test.dat", FileMode.Open, FileAccess.Read);
            InMemoryFeatureLayer featureLayer2 = (InMemoryFeatureLayer)formatter2.Deserialize(fs2); // The feature(1,1) exists in featureLayer2
        }

        public String SaveLayer(LayerOverlay myData, String Source)
        {
            Guid newFile = Guid.NewGuid();
            String path = String.Empty;
            String result = String.Empty;
            if (Source == "Routing")
            {
                if (!(System.IO.Directory.Exists(LayerPaths.RoutingTmpPath(String.Empty))))
                    System.IO.Directory.CreateDirectory(LayerPaths.RoutingTmpPath(String.Empty));

                string[] filePaths = Directory.GetFiles(LayerPaths.RoutingTmpPath(String.Empty), newFile.ToString() + ".*");
                foreach (String s in filePaths)
                    if (File.Exists(s))
                        try
                        {
                            File.Delete(s);
                        }
                        catch { }

                filePaths = Directory.GetFiles(LayerPaths.RoutingTmpPath(String.Empty), "*.*");
                foreach (String file in filePaths)
                {
                    try
                    {
                        FileInfo fi = new FileInfo(file);
                        if (fi.LastAccessTime < DateTime.Now.AddHours(-1))
                            fi.Delete();
                    }
                    catch { }
                }

                path = LayerPaths.RoutingTmpPath(newFile.ToString()) + ".mem";
                result = newFile.ToString() + ".mem|RoutingTmp";
            }

            InMemoryFeatureLayer memo = new InMemoryFeatureLayer();
            foreach (InMemoryFeatureLayer l in myData.Layers)
                foreach (Feature internalLayer in l.InternalFeatures)
                    memo.InternalFeatures.Add(internalLayer);

            FileStream fs = new FileStream(path, FileMode.Create, FileAccess.ReadWrite);
            BinaryFormatter formatter1 = new BinaryFormatter();
            formatter1.Serialize(fs, memo);
            fs.Close();

            return result;
        }

        public LayerOverlay LoadLayer(String path)
        {
            //path: "newGuid|RoutingTmp"
            if (path.EndsWith("|RoutingTmp"))
                path = path.Replace("|RoutingTmp", String.Empty);

            BinaryFormatter formatter = new BinaryFormatter();
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
            InMemoryFeatureLayer memo = (InMemoryFeatureLayer)formatter.Deserialize(fs);
            memo.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = new LineStyle(new GeoPen(GeoColors.DarkGreen, 3f));
            memo.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColors.DarkSeaGreen), 15);
            memo.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            fs.Close();

            LayerOverlay l = new LayerOverlay();
            l.Layers.Add(memo);
            return l;
        }

        #region Zones
        public Boolean CreateZones(ZoneCreation zc)
        {
            Boolean b = false;
            try
            {
                if (!(System.IO.Directory.Exists(LayerPaths.ZonePath(String.Empty))))
                    System.IO.Directory.CreateDirectory(LayerPaths.ZonePath(String.Empty));

                string[] filePaths = Directory.GetFiles(LayerPaths.ZonePath(String.Empty), zc.UserToken.ToString() + ".*");
                foreach (String s in filePaths)
                    if (File.Exists(s))
                        try
                        {
                            File.Delete(s);
                        }
                        catch { }

                filePaths = Directory.GetFiles(LayerPaths.ZonePath(String.Empty), "*.*");
                foreach (String file in filePaths)
                    if (File.Exists(file))
                        try
                        {
                            FileInfo fi = new FileInfo(file);
                            if (fi.LastAccessTime < DateTime.Now.AddDays(-1))
                                fi.Delete();
                        }
                        catch { }
                InMemoryFeatureLayer m = new InMemoryFeatureLayer();
                m.Open();
                m.EditTools.BeginTransaction();

                foreach (DataRow dr in zc.dtData.Rows)
                {
                    Feature f = new Feature(dr["GeomData"].ToString());
                    f.ColumnValues.Add("zoneid", dr["ZoneID"].ToString());
                    f.ColumnValues.Add("zonecolor", dr["color"].ToString());
                    f.ColumnValues.Add("descrip", dr["Description"].ToString());
                    m.EditTools.Add(f);
                }

                m.EditTools.CommitTransaction();
                m.BuildIndex();
                m.Close();

                String path = LayerPaths.ZonePath(zc.UserToken.ToString()) + ".mem";
                FileStream fs = new FileStream(path, FileMode.Create, FileAccess.ReadWrite);
                BinaryFormatter formatter1 = new BinaryFormatter();
                formatter1.Serialize(fs, m);
                fs.Close();

                b = true;
            }
            catch (Exception ex) { }
            return b;
        }

        public LayerOverlay ShowZones(Guid UserToken)
        {
            LayerOverlay layerOverlay = new LayerOverlay();
            String sPath = LayerPaths.ZonePath(UserToken.ToString()) + ".mem";

            Proj4Projection proj4 = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetSphericalMercatorParametersString());
            proj4 = new Proj4Projection(4326, 3857);
            if (!proj4.IsOpen) proj4.Open();

            BinaryFormatter formatter = new BinaryFormatter();
            FileStream fs = new FileStream(sPath, FileMode.Open, FileAccess.Read);
            InMemoryFeatureLayer memo = (InMemoryFeatureLayer)formatter.Deserialize(fs);
            memo.FeatureSource.Projection = new Proj4Projection(4326, 3857);
            memo.Name = "Zones";
            memo.ZoomLevelSet.ZoomLevel10.DefaultAreaStyle = new ZoneAreaStyle(GeoPens.Black);
            memo.ZoomLevelSet.ZoomLevel10.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level16;
            memo.ZoomLevelSet.ZoomLevel17.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("descrip", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
            memo.ZoomLevelSet.ZoomLevel17.DefaultAreaStyle = new ZoneAreaStyle(GeoPens.Black);
            memo.ZoomLevelSet.ZoomLevel17.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            fs.Close();

            layerOverlay.Layers.Add(memo.Name, memo);
            return layerOverlay;
        }
        public JObject QryZonesLayer(Double x, Double y, Guid UserToken)
        {
            JObject result = new JObject();

            String sPath = LayerPaths.ZonePath(UserToken.ToString()) + ".mem";
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream fs = new FileStream(sPath, FileMode.Open, FileAccess.Read);
            InMemoryFeatureLayer memo = (InMemoryFeatureLayer)formatter.Deserialize(fs);
            fs.Close();

            memo.Open();
            //PointShape centerPointInDecimalDegree = new PointShape(x, y);
            //EllipseShape circle = new EllipseShape(centerPointInDecimalDegree, radius, GeographyUnit.DecimalDegree, DistanceUnit.Meter);
            //RectangleShape r = circle.GetBoundingBox();
            //Collection<Feature> f = memo.QueryTools.GetFeaturesInsideBoundingBox(r, new string[2] { "zoneid", "descrip" });
            //Collection<Feature> f = memo.QueryTools.GetFeaturesNearestTo(
            //    new PointShape(x, y),
            //    GeographyUnit.DecimalDegree,
            //    1,
            //    new string[2] { "zoneid", "descrip" },
            //    radius,
            //    DistanceUnit.Meter
            //);

            Collection<Feature> f = memo.QueryTools.GetFeaturesContaining(
                new PointShape(x, y),
                new string[2] { "zoneid", "descrip" }
            );
            memo.Close();
            if (f.Count > 0)
            {
                string descrip = f[0].ColumnValues["descrip"];
                string zoneid = f[0].ColumnValues["zoneid"];
                string zonecolor = f[0].ColumnValues["zonecolor"];
                string sFeature = f[0].ToString();

                result.Add("name", JToken.FromObject(descrip));
                result.Add("zoneid", JToken.FromObject(zoneid));
                result.Add("zonecolor", JToken.FromObject(zonecolor));
                result.Add("sFeature", JToken.FromObject(sFeature));
                result.Add("success", JToken.FromObject(true));
            }
            else
            {
                result.Add("success", JToken.FromObject(false));
            }
            return result;
        }
        #endregion

        #region Custom styles
        class myStyleData
        {
            public PointSymbolType symbolType;
            public DataSet ds;
            public String RenderLabel;
        }

        class ZoneAreaStyle : AreaStyle
        {
            public ZoneAreaStyle(GeoPen outlinePen) : base(outlinePen) { }

            protected override Collection<string> GetRequiredColumnNamesCore()
            {
                return new Collection<string>() { "zonecolor" };
            }
            protected override void DrawCore(IEnumerable<Feature> features, GeoCanvas canvas, Collection<SimpleCandidate> labelsInThisLayer, Collection<SimpleCandidate> labelsInAllLayers)
            {
                //base.DrawCore(features, canvas, labelsInThisLayer, labelsInAllLayers);
                foreach (Feature feature in features)
                {
                    Color c = Color.DarkRed;
                    int colorCode = c.ToArgb();
                    if (!int.TryParse(feature.ColumnValues["zonecolor"], out colorCode))
                        colorCode = c.ToArgb();
                    string hexColor = "#" + colorCode.ToString("X2").Substring(2);
                    GeoColor color = GeoColor.FromHtml(hexColor);
                    color.AlphaComponent = 125; //Transparency

                    canvas.DrawArea(feature, new GeoPen(GeoColor.FromHtml(hexColor), base.OutlinePen.Width), new GeoSolidBrush(color), DrawingLevel.LevelOne);
                }
            }
        }
        #endregion
    }
}