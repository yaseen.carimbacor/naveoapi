﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using ThinkGeo.MapSuite;
using ThinkGeo.MapSuite.Layers;
using ThinkGeo.MapSuite.Shapes;

namespace NaveoWebMaps.Helpers
{
    internal static class XyzTileCacheHelper
    {
        #region variables
        private static RectangleShape maxExtent;
        private static List<double> scales;
        static int daysToKeepFiles = 27;
        public readonly static int tileSize = 256;//512;
        #endregion

        #region private classes
        static XyzTileCacheHelper()
        {
            scales = new List<double>();
            double worldWidth = 20037508.2314698 * 2;
            for (int i = 0; i <= 22; i++)
            {
                double resolution = worldWidth / (tileSize * Math.Pow(2, i));
                double scale = 39.3701 * 96 * resolution;
                scales.Add(scale);
            }

            maxExtent = new RectangleShape(-20037508.2314698, 20037508.2314698, 20037508.2314698, -20037508.2314698);
        }
        private static TileMatrix GetTileMatrix()
        {
            TileMatrix tileMatrix = new MapSuiteTileMatrix(590591790);
            tileMatrix.BoundingBoxUnit = GeographyUnit.Meter;
            tileMatrix.TileHeight = tileSize;
            tileMatrix.TileWidth = tileSize;
            tileMatrix.BoundingBox = maxExtent;
            return tileMatrix;
        }
        #endregion

        #region public classes
        public static RectangleShape GetTileExtentByXyz(long x, long y, int z)
        {
            double scale = scales[z];
            TileMatrix tileMatrix = GetTileMatrix();
            tileMatrix.Scale = scale;

            return tileMatrix.GetCell(y, x).BoundingBox;
        }
        public static Bitmap GetTile(string cacheDirectory, string cacheId, long x, long y, int z)
        {
            Bitmap tileImage = null;
            string tileCachePath = Path.Combine(cacheDirectory, cacheId, $"{z}/{x}/{y}.png");
            if (File.Exists(tileCachePath))
                if (File.GetLastWriteTime(tileCachePath) < DateTime.Now.AddDays(-daysToKeepFiles))
                    File.Delete(tileCachePath);
                else
                    tileImage = new Bitmap(tileCachePath);

            return tileImage;
        }
        public static Boolean TileExists(string cacheDirectory, string cacheId, long x, long y, int z)
        {
            Boolean b = false;
            string tileCachePath = Path.Combine(cacheDirectory, cacheId, $"{z}/{x}/{y}.png");
            if (File.Exists(tileCachePath))
                if (File.GetLastWriteTime(tileCachePath) < DateTime.Now.AddDays(-daysToKeepFiles))
                {
                    File.Delete(tileCachePath);
                    b = false;
                }
                else
                    b = true;

            return b;
        }
        public static void SaveTile(Bitmap tileImage, string cacheDirectory, string cacheId, long x, long y, int z)
        {
            string tileCachePath = Path.Combine(cacheDirectory, cacheId, $"{z}/{x}/{y}.png");
            lock (string.Intern(tileCachePath))
            {
                if (!File.Exists(tileCachePath) && tileImage != null)
                {
                    string dir = Path.GetDirectoryName(tileCachePath);
                    if (!Directory.Exists(dir))
                        Directory.CreateDirectory(dir);
                    tileImage.Save(tileCachePath, ImageFormat.Png);
                }
            }
        }
        #endregion
    }
}