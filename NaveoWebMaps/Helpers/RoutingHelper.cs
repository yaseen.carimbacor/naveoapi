﻿using NaveoWebMaps.Constants;
using NaveoWebMaps.Models;
using NaveoWebMaps.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using ThinkGeo.MapSuite;
using ThinkGeo.MapSuite.Drawing;
using ThinkGeo.MapSuite.Layers;
using ThinkGeo.MapSuite.Routing;
using ThinkGeo.MapSuite.Shapes;
using ThinkGeo.MapSuite.Styles;
using ThinkGeo.MapSuite.WebApi;

namespace NaveoWebMaps.Helpers
{
    public class RoutingHelper
    {
        String RoutableMap = "MauritiusRoadNetwork.routable";
        public String CreateRouting(RoutingData routing)
        {
            // source
            ShapeFileFeatureSource shapeFileSource = new ShapeFileFeatureSource(LayerPaths.RoutingPath(RoutableMap + ".shp"));
            RtgRoutingSource routeSource = new RtgRoutingSource(LayerPaths.RoutingPath(RoutableMap + ".rtg"));
            routeSource.GeographyUnit = GeographyUnit.Meter;

            String RoutingData = FindRoute(shapeFileSource, routeSource, routing.lPoints, routing.searchRadiusInMeters).guidPath.ToString();
            return RoutingData;
        }
        public String GetRoute(RoutingData routing)
        {
            // source
            ShapeFileFeatureSource shapeFileSource = new ShapeFileFeatureSource(LayerPaths.RoutingPath(RoutableMap + ".shp"));
            RtgRoutingSource routeSource = new RtgRoutingSource(LayerPaths.RoutingPath(RoutableMap + ".rtg"));
            routeSource.GeographyUnit = GeographyUnit.Meter;

            RoutingResult rr = GetRoutingResult(shapeFileSource, routeSource, routing.lPoints, routing.searchRadiusInMeters);
            return rr.Route.GetWellKnownText();
        }
        public String GetBestRoute(RoutingData routing)
        {
            ShapeFileFeatureSource shapeFileSource = new ShapeFileFeatureSource(LayerPaths.RoutingPath(RoutableMap + ".shp"));
            RtgRoutingSource routeSource = new RtgRoutingSource(LayerPaths.RoutingPath(RoutableMap + ".rtg"));
            routeSource.GeographyUnit = GeographyUnit.Meter;

            List<String> ls = new List<String>();
            for (int i = 0; i < routing.lPoints.Count; i++)
                ls.Add(i.ToString());
            List<String> lsPermute = new Permutation().permute(ls, 0, ls.Count - 1);


            RoutingResult rr = FindBestRoute(shapeFileSource, routeSource, routing.lPoints, routing.bestRouteIterations);
            return rr.Route.GetWellKnownText();
        }


        class xxx
        {
            public List<LineShape> lineShapes { get; set; }
            public Double distance { get; set; }
        }
        private RoutingResult GetRoutingResult(ShapeFileFeatureSource shapeFileSource, RtgRoutingSource routeSource, List<lonLatCoordinate> lPoints, double searchRadiusInMeters)
        {
            PointShape startPoint = new PointShape(57.6538, -20.1305);
            PointShape endPoint = new PointShape(57.4283, -20.4254);
            Proj4Projection proj4 = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetLatLongParametersString());
            if(!proj4.IsOpen) proj4.Open();
            List<PointShape> viaPoints = new List<PointShape>();
            for (int i = 0; i < lPoints.Count; i++)
            {
                Double lon = lPoints[i].lon;
                Double lat = lPoints[i].lat;
                PointShape myPoint = new PointShape(lon, lat);
                myPoint = (PointShape)proj4.ConvertToExternalProjection(myPoint);
                if (i == 0)
                    startPoint = myPoint;
                else if (i == lPoints.Count - 1)
                    endPoint = myPoint;
                else
                    viaPoints.Add(myPoint);
            }

            // route shape
            RoutingEngine engine = new RoutingEngine(routeSource, shapeFileSource); //new AStarRoutingAlgorithm()
            engine.SearchRadiusInMeters = searchRadiusInMeters;




            ///////////////////////////////////
            //List<xxx> xxxes = new List<xxx>();
            //List<String> l = new List<String>();
            //for (int i = 0; i < lPoints.Count; i++)
            //    l.Add(lPoints[i].lon.ToString() + " " + lPoints[i].lat.ToString());
            //List<String> lsPermute = new Permutation().permute(l, 0, l.Count - 1);
            //List<String> wawaw = new Permutation().permute(new List<string>() { "a","b","c","d","e"}, 0, 4);

            //foreach (String lLonLat in lsPermute)
            //{
            //    List<lonLatCoordinate> lCoord = new List<lonLatCoordinate>();
            //    String[] LonLat = lLonLat.Split(' ');
            //    for (int x = 0; x < LonLat.Length; x++)
            //    {
            //        if (String.IsNullOrEmpty(LonLat[x]))
            //            break;
            //        lonLatCoordinate c = new lonLatCoordinate();
            //        c.lon = Convert.ToDouble(LonLat[x]);
            //        c.lat = Convert.ToDouble(LonLat[x+1]);
            //        lCoord.Add(c);
            //        x++;
            //    }

            //    viaPoints = new List<PointShape>(); 
            //    for (int i = 0; i < lCoord.Count; i++)
            //    {
            //        Double lon = lPoints[i].lon;
            //        Double lat = lPoints[i].lat;
            //        PointShape myPoint = new PointShape(lon, lat);
            //        viaPoints.Add(myPoint);
            //    }

            //    List<LineShape> lls = new List<LineShape>();
            //    for (int i = 0; i < viaPoints.Count - 1; i++)
            //        lls.Add(engine.GetRoute(viaPoints[i], viaPoints[i + 1]).Route);

            //    Double distance = 0.0;
            //    foreach (LineShape i in lls)
            //        distance += i.GetAccurateLength(4326, DistanceUnit.Meter, DistanceCalculationMode.LocalizedUtmZone);

            //    xxx z = new xxx();
            //    z.lineShapes = lls;
            //    z.distance = distance;
            //    xxxes.Add(z);
            //    break;
            //}
            ////paths[0].GetAccurateLength(4326, DistanceUnit.Meter, DistanceCalculationMode.LocalizedUtmZone)

            //List<xxx> SortedList = xxxes.OrderBy(o => o.distance).ToList();
            //RoutingResult rr = new RoutingResult();
            //foreach (LineShape ls in SortedList[0].lineShapes)
            //    foreach (Vertex v in ls.Vertices)
            //        rr.Route.Vertices.Add(v);
            ///////////////////////////////////





            List<LineShape> paths = new List<LineShape>();
            viaPoints = new List<PointShape>();
            for (int i = 0; i < lPoints.Count; i++)
            {
                Double lon = lPoints[i].lon;
                Double lat = lPoints[i].lat;
                PointShape myPoint = new PointShape(lon, lat);
                viaPoints.Add(myPoint);
            }
            for (int i = 0; i < viaPoints.Count - 1; i++)
                paths.Add(engine.GetRoute(viaPoints[i], viaPoints[i + 1]).Route);

            RoutingResult rr = new RoutingResult();
            foreach (LineShape ls in paths)
                foreach (Vertex v in ls.Vertices)
                    rr.Route.Vertices.Add(v);

            return rr;
        }
        private RouteResult FindRoute(ShapeFileFeatureSource shapeFileSource, RtgRoutingSource routeSource, List<lonLatCoordinate> lPoints, double searchRadiusInMeters)
        {
            RouteResult routeResult = new RouteResult();
            RoutingResult rr = GetRoutingResult(shapeFileSource, routeSource, lPoints, searchRadiusInMeters);
            LineShape routeShape = rr.Route;
            if (routeShape.Vertices.Count == 0)
            {
                routeResult.resultMessage= "Cannot find route within " + searchRadiusInMeters + " meters from start AndOr end cordinates";
                return routeResult;
            }
            routeResult.guidPath = new ShapeFileHelper().CreateRoute(routeShape, lPoints);
            return routeResult;
        }
        private RoutingResult FindBestRoute(ShapeFileFeatureSource shapeFileSource, RoutingSource routingSource, List<lonLatCoordinate> lPoints, int iterations)
        {
            RoutingEngine routingEngine = new RoutingEngine(routingSource, shapeFileSource);
            routingEngine.GeographyUnit = GeographyUnit.Meter;
            routingEngine.SearchRadiusInMeters = 100;

            PointShape startPoint = new PointShape(57.6538, -20.1305);
            PointShape endPoint = new PointShape(57.4283, -20.4254);
            Proj4Projection proj4 = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetLatLongParametersString());
            if (!proj4.IsOpen) proj4.Open();
            List<PointShape> viaPoints = new List<PointShape>();
            for (int i = 0; i < lPoints.Count; i++)
            {
                Double lon = lPoints[i].lon;
                Double lat = lPoints[i].lat;
                PointShape myPoint = new PointShape(lon, lat);
                myPoint = (PointShape)proj4.ConvertToExternalProjection(myPoint);
                if (i == 0)
                    startPoint = myPoint;
                else if (i == lPoints.Count - 1)
                    endPoint = myPoint;
                else
                    viaPoints.Add(myPoint);
            }
            
            RoutingResult routingResult = routingEngine.GetRoute(startPoint, endPoint, viaPoints, iterations);
            return routingResult;
        }

        class RouteResult 
        {
            public Guid guidPath { get; set; } = Headers.DEFAULT_GUID;
            public String resultMessage { get; set; }
        }

        #region Obsolete
        [Obsolete]
        public LayerOverlay GetRoutingForOSMProjection(RoutingData routing)
        {
            // source
            ShapeFileFeatureSource shapeFileSource = new ShapeFileFeatureSource(LayerPaths.RoutingPath("Mauritius-3857-20180929.Routable.shp"));
            shapeFileSource.Projection = new Proj4Projection(3857, 4326);
            RtgRoutingSource routeSource = new RtgRoutingSource(LayerPaths.RoutingPath("Mauritius-3857-20180929.Routable.rtg"));

            return FindPathOSMProjection(shapeFileSource, routeSource, routing.lPoints, routing.searchRadiusInMeters);
        }
        [Obsolete]
        public String CreateRoutingOSMProjection(RoutingData routing)
        {
            // source
            ShapeFileFeatureSource shapeFileSource = new ShapeFileFeatureSource(LayerPaths.RoutingPath("Mauritius-3857-20180929.Routable.shp"));
            shapeFileSource.Projection = new Proj4Projection(3857, 4326);
            RtgRoutingSource routeSource = new RtgRoutingSource(LayerPaths.RoutingPath("Mauritius-3857-20180929.Routable.rtg"));

            LayerOverlay RoutingData = FindPath(shapeFileSource, routeSource, routing.lPoints, routing.searchRadiusInMeters);
            String Result = new InMemoryHelper().SaveLayer(RoutingData, "Routing");
            return Result;
        }
        [Obsolete]
        private LayerOverlay FindPathOSMProjection(ShapeFileFeatureSource shapeFileSource, RtgRoutingSource routeSource, List<lonLatCoordinate> lPoints, double searchRadiusInMeters)
        {
            Proj4Projection proj4 = new Proj4Projection(4326, 3857);
            proj4.Open();

            // route point layer
            LayerOverlay routeOverlay = new LayerOverlay("Route");

            // route layer
            InMemoryFeatureLayer routeLayer = new InMemoryFeatureLayer();
            routeLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = new LineStyle(new GeoPen(GeoColor.FromHtml("#ff7f50"), 3f));
            routeLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

            // route point layer
            InMemoryFeatureLayer routePointLayer = new InMemoryFeatureLayer();
            routePointLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColor.FromHtml("#008080")), 15);
            routePointLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            routePointLayer.InternalFeatures.Clear();

            PointShape startPoint = new PointShape(57.6538, -20.1305);
            PointShape endPoint = new PointShape(57.4283, -20.4254);
            List<PointShape> points = new List<PointShape>();
            for (int i = 0; i < lPoints.Count; i++)
            {
                Double lon = lPoints[i].lon;
                Double lat = lPoints[i].lat;
                PointShape myPoint = new PointShape(lon, lat);
                if (i == 0)
                    startPoint = myPoint;
                else if (i == lPoints.Count - 1)
                    endPoint = myPoint;
                else
                    points.Add(myPoint);

                //routePointLayer.InternalFeatures.Add(new Feature(myPoint));
                routePointLayer.InternalFeatures.Add(new Feature(proj4.ConvertToExternalProjection(myPoint)));
            }

            // route shape
            RoutingEngine engine = new RoutingEngine(routeSource, shapeFileSource);
            engine.SearchRadiusInMeters = searchRadiusInMeters;
            MultilineShape routeShape = engine.GetRoute(startPoint, endPoint, points).RouteLines;
            if (routeShape.Lines.Count > 0)
            {
                //var x = engine.GetRoute(startPoint, endPoint, points).OrderedStops;

                // Add projection logic here, convert 4326 result to 3857 for match your base map now
                BaseShape routeShape_3857 = proj4.ConvertToExternalProjection(routeShape);

                // route layer
                routeLayer.InternalFeatures.Clear();
                routeLayer.InternalFeatures.Add(new Feature(routeShape_3857));
                //routeLayer.InternalFeatures.Add(new Feature(routeShape));

                routeOverlay.Layers.Add("Route", routeLayer);
                routeOverlay.Layers.Add("RoutePoint", routePointLayer);

                //Saving to shp file. Actually testing
                //new ShapeFileHelper().CreateRoute(routeShape);
            }
            return routeOverlay; //routeOverlay.Redraw();
        }

        [Obsolete]
        public LayerOverlay GetRouting(RoutingData routing)
        {
            // source
            ShapeFileFeatureSource shapeFileSource = new ShapeFileFeatureSource(LayerPaths.RoutingPath(RoutableMap + ".shp"));
            shapeFileSource.Projection = new Proj4Projection(4326, 3857);
            RtgRoutingSource routeSource = new RtgRoutingSource(LayerPaths.RoutingPath(RoutableMap + ".rtg"));

            return FindPathOSMProjection(shapeFileSource, routeSource, routing.lPoints, routing.searchRadiusInMeters);
        }
        [Obsolete]
        private LayerOverlay FindPath(ShapeFileFeatureSource shapeFileSource, RtgRoutingSource routeSource, List<lonLatCoordinate> lPoints, double searchRadiusInMeters)
        {
            //Proj4Projection proj4 = new Proj4Projection(4326, 3857);
            //proj4.Open();

            // route point layer
            LayerOverlay routeOverlay = new LayerOverlay("Route");

            // route layer
            InMemoryFeatureLayer routeLayer = new InMemoryFeatureLayer();
            routeLayer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = new LineStyle(new GeoPen(GeoColor.FromHtml("#ff7f50"), 3f));
            routeLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

            // route point layer
            InMemoryFeatureLayer routePointLayer = new InMemoryFeatureLayer();
            routePointLayer.ZoomLevelSet.ZoomLevel01.DefaultPointStyle = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColor.FromHtml("#008080")), 15);
            routePointLayer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            routePointLayer.InternalFeatures.Clear();

            PointShape startPoint = new PointShape(57.6538, -20.1305);
            PointShape endPoint = new PointShape(57.4283, -20.4254);
            List<PointShape> points = new List<PointShape>();
            for (int i = 0; i < lPoints.Count; i++)
            {
                Double lon = lPoints[i].lon;
                Double lat = lPoints[i].lat;
                PointShape myPoint = new PointShape(lon, lat);
                if (i == 0)
                    startPoint = myPoint;
                else if (i == lPoints.Count - 1)
                    endPoint = myPoint;
                else
                    points.Add(myPoint);

                routePointLayer.InternalFeatures.Add(new Feature(myPoint));
                //routePointLayer.InternalFeatures.Add(new Feature(proj4.ConvertToExternalProjection(myPoint)));
            }

            // route shape
            RoutingEngine engine = new RoutingEngine(routeSource, shapeFileSource);
            engine.SearchRadiusInMeters = searchRadiusInMeters;
            RoutingResult rr = engine.GetRoute(startPoint, endPoint);
            LineShape routeShape = rr.Route;
            if (routeShape.Vertices.Count > 0)
            {
                //var x = engine.GetRoute(startPoint, endPoint, points).OrderedStops;

                // Add projection logic here, convert 4326 result to 3857 for match your base map now
                //BaseShape routeShape_3857 = proj4.ConvertToExternalProjection(routeShape);

                // route layer
                routeLayer.InternalFeatures.Clear();
                //routeLayer.InternalFeatures.Add(new Feature(routeShape_3857));
                routeLayer.InternalFeatures.Add(new Feature(routeShape));

                routeOverlay.Layers.Add("Route", routeLayer);
                routeOverlay.Layers.Add("RoutePoint", routePointLayer);

                //Saving to shp file. Actually testing
                //new ShapeFileHelper().CreateRoute(routeShape);
            }
            return routeOverlay; //routeOverlay.Redraw();
        }
        #endregion
    }
}