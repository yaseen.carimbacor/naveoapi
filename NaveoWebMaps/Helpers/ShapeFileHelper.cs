﻿using NaveoWebMaps.Constants;
using NaveoWebMaps.CustomStyles;
using NaveoWebMaps.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using ThinkGeo.MapSuite;
using ThinkGeo.MapSuite.Drawing;
using ThinkGeo.MapSuite.Layers;
using ThinkGeo.MapSuite.Shapes;
using ThinkGeo.MapSuite.Styles;
using ThinkGeo.MapSuite.WebApi;
using static NaveoWebMaps.Models.BlupData;
using static NaveoWebMaps.Models.ZonesData;

namespace NaveoWebMaps.Helpers
{
    public class ShapeFileHelper
    {
        #region Specifics
        public LayerOverlay ShowLayer(String sFolder, String sFile)
        {
            LayerOverlay layerOverlay = new LayerOverlay();
            String shpPath = LayerPaths.BaseESRIMapsPath(String.Empty);
            shpPath = Path.Combine(shpPath, "Specifics", sFolder, sFile + ".shp");

            Proj4Projection proj4 = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetSphericalMercatorParametersString());
            proj4 = new Proj4Projection(4326, 3857);
            if (!proj4.IsOpen) proj4.Open();

            ShapeFileFeatureLayer shp = new ShapeFileFeatureLayer(shpPath);
            shp.Name = sFolder + sFile;

            switch (sFile.ToLower())
            {
                case "contour":
                    shp.ZoomLevelSet.ZoomLevel05.DefaultLineStyle = new LineStyle(new GeoPen(GeoColors.DarkSeaGreen));
                    break;

                case "natural_drains":
                    shp.ZoomLevelSet.ZoomLevel05.DefaultLineStyle = new LineStyle(new GeoPen(GeoColors.ForestGreen));
                    break;

                case "rivers":
                    shp.ZoomLevelSet.ZoomLevel05.DefaultLineStyle = new LineStyle(new GeoPen(GeoColors.BrightBlue));
                    break;

                default:
                    shp.ZoomLevelSet.ZoomLevel05.DefaultLineStyle = new LineStyle(new GeoPen(GeoColors.Black));
                    break;
            }
            shp.ZoomLevelSet.ZoomLevel05.DefaultAreaStyle = new AreaStyle(new GeoPen(new GeoSolidBrush(GeoColors.Blue), 1f));
            shp.ZoomLevelSet.ZoomLevel05.DefaultPointStyle = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColors.BrightBlue), 2);
            shp.ZoomLevelSet.ZoomLevel05.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level16;

            //shp.ZoomLevelSet.ZoomLevel17.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("ordno", "Aerial", 12, DrawingFontStyles.Italic, GeoColors.Black);
            shp.ZoomLevelSet.ZoomLevel17.DefaultAreaStyle = new AreaStyle(new GeoPen(new GeoSolidBrush(GeoColors.Blue), 1f));
            shp.ZoomLevelSet.ZoomLevel17.DefaultLineStyle = new LineStyle(new GeoPen(GeoColors.Black));
            shp.ZoomLevelSet.ZoomLevel17.DefaultPointStyle = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColors.BrightBlue), 3);
            shp.ZoomLevelSet.ZoomLevel17.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

            shp.FeatureSource.Projection = proj4;
            layerOverlay.Layers.Add(shp.Name, shp);
            return layerOverlay;
        }
        #endregion

        #region Zones
        public Boolean CreateZones(ZoneCreation zc)
        {
            Boolean b = false;
            try
            {
                if (!(System.IO.Directory.Exists(LayerPaths.ZonePath(String.Empty))))
                    System.IO.Directory.CreateDirectory(LayerPaths.ZonePath(String.Empty));

                string[] filePaths = Directory.GetFiles(LayerPaths.ZonePath(String.Empty), zc.UserToken.ToString() + ".*");
                foreach (String s in filePaths)
                    if (File.Exists(s))
                        try
                        {
                            File.Delete(s);
                        }
                        catch { }

                List<DbfColumn> dbfColumns = new List<DbfColumn>();
                dbfColumns.Add(new DbfColumn("zoneid", DbfColumnType.Numeric, 10, 0));
                dbfColumns.Add(new DbfColumn("zonecolor", DbfColumnType.Numeric, 15, 0));
                dbfColumns.Add(new DbfColumn("descrip", DbfColumnType.Character, 500, 0));

                String path = LayerPaths.ZonePath(zc.UserToken.ToString()) + ".shp";
                ShapeFileFeatureLayer.CreateShapeFile(ShapeFileType.Polygon, path, dbfColumns, Encoding.Default, OverwriteMode.Overwrite);

                ShapeFileFeatureLayer newShapeFile = new ShapeFileFeatureLayer(path, GeoFileReadWriteMode.ReadWrite);
                newShapeFile.Open();
                newShapeFile.EditTools.BeginTransaction();

                foreach (DataRow dr in zc.dtData.Rows)
                {
                    Feature feature = new Feature(dr["GeomData"].ToString());
                    feature.ColumnValues.Add("zoneid", dr["ZoneID"].ToString());
                    feature.ColumnValues.Add("zonecolor", dr["color"].ToString());
                    feature.ColumnValues.Add("descrip", dr["Description"].ToString());
                    if (feature.GetWellKnownBinary() != null)
                        if (feature.GetWellKnownType() == WellKnownType.Polygon)
                            newShapeFile.EditTools.Add(feature);
                }

                newShapeFile.EditTools.CommitTransaction();
                newShapeFile.Close();

                b = true;
            }
            catch (Exception ex) { }
            return b;
        }

        public LayerOverlay ShowZones(Guid UserToken)
        {
            LayerOverlay layerOverlay = new LayerOverlay();
            String shpPath = LayerPaths.ZonePath(UserToken.ToString()) + ".shp";

            Proj4Projection proj4 = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetSphericalMercatorParametersString());
            proj4 = new Proj4Projection(4326, 3857);
            if (!proj4.IsOpen) proj4.Open();

            ShapeFileFeatureLayer shp = new ShapeFileFeatureLayer(shpPath);
            shp.FeatureSource.Projection = new Proj4Projection(4326, 3857);
            shp.Name = "Zones";

            shp.ZoomLevelSet.ZoomLevel10.DefaultAreaStyle = new CustomAreaStyle(GeoPens.Black);
            shp.ZoomLevelSet.ZoomLevel10.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level16;

            shp.ZoomLevelSet.ZoomLevel17.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("descrip", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
            shp.ZoomLevelSet.ZoomLevel17.DefaultAreaStyle = new CustomAreaStyle(GeoPens.Black);
            shp.ZoomLevelSet.ZoomLevel17.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

            layerOverlay.Layers.Add(shp.Name, shp);
            return layerOverlay;
        }
        #endregion

        #region Custom shape styles
        class myStyleData
        {
            public PointSymbolType symbolType;
            public DataSet ds;
            public String RenderLabel;
        }

        class CustomAreaStyle : AreaStyle
        {
            public CustomAreaStyle(GeoPen outlinePen) : base(outlinePen) { }

            protected override Collection<string> GetRequiredColumnNamesCore()
            {
                return new Collection<string>() { "zonecolor" };
            }
            protected override void DrawCore(IEnumerable<Feature> features, GeoCanvas canvas, Collection<SimpleCandidate> labelsInThisLayer, Collection<SimpleCandidate> labelsInAllLayers)
            {
                //base.DrawCore(features, canvas, labelsInThisLayer, labelsInAllLayers);
                foreach (Feature feature in features)
                {
                    Color c = Color.DarkRed;
                    int colorCode = c.ToArgb();
                    if (!int.TryParse(feature.ColumnValues["zonecolor"], out colorCode))
                        colorCode = c.ToArgb();
                    string hexColor = "#" + colorCode.ToString("X2").Substring(2);

                    GeoColor color = GeoColor.FromHtml(hexColor);
                    color.AlphaComponent = 50; //(byte)Math.Round(0.204 * 255); //Transparency

                    //base.FillSolidBrush.Color = color;
                    //base.OutlinePen.Color = GeoColor.FromHtml(hexColor);
                    //base.DrawCore(new Feature[] { feature }, canvas, labelsInThisLayer, labelsInAllLayers);
                    canvas.DrawArea(feature, new GeoPen(GeoColor.FromHtml(hexColor), base.OutlinePen.Width), new GeoSolidBrush(color), DrawingLevel.LevelOne);
                }
            }
        }

        class CustomLineStyle : LineStyle
        {
            public CustomLineStyle(GeoPen outlinePen) : base(outlinePen) { }
            protected override Collection<string> GetRequiredColumnNamesCore()
            {
                return new Collection<string>() { "tripcolor" };
            }
            protected override void DrawCore(IEnumerable<Feature> features, GeoCanvas canvas, Collection<SimpleCandidate> labelsInThisLayer, Collection<SimpleCandidate> labelsInAllLayers)
            {
                foreach (Feature feature in features)
                {
                    Color c = Color.DarkRed;
                    int colorCode = c.ToArgb();
                    if (!int.TryParse(feature.ColumnValues["tripcolor"], out colorCode))
                        colorCode = c.ToArgb();
                    string hexColor = "#" + colorCode.ToString("X2").Substring(2);

                    canvas.DrawLine(feature, new GeoPen(GeoColor.FromHtml(hexColor), 3), DrawingLevel.LevelOne);
                }
            }
        }

        class CustomPointStyle : PointStyle
        {
            myStyleData myStyle = new myStyleData();
            public CustomPointStyle(GeoImage geoImage) : base(geoImage) { }
            public CustomPointStyle(PointSymbolType symbolType, GeoSolidBrush symbolSolidBrush, int symbolSize, myStyleData myData) : base(symbolType, symbolSolidBrush, symbolSize) { myStyle = myData; }
            protected override Collection<string> GetRequiredColumnNamesCore()
            {
                return new Collection<string>() { "AplID" };
            }
            protected override void DrawCore(IEnumerable<Feature> features, GeoCanvas canvas, Collection<SimpleCandidate> labelsInThisLayer, Collection<SimpleCandidate> labelsInAllLayers)
            {
                foreach (Feature feature in features)
                {
                    String aplid = feature.ColumnValues["AplID"];
                    GeoColor geoColor = GeoColors.DarkBlue;
                    if (aplid.StartsWith("RESI"))
                        geoColor = GeoColors.DarkGreen;
                    else if (aplid.StartsWith("MORC"))
                        geoColor = GeoColors.DarkGray;
                    else if (aplid.StartsWith("SERV"))
                        geoColor = GeoColors.DarkCyan;

                    GeoPen geoPen = new GeoPen(geoColor, 3);
                    //canvas.DrawEllipse(feature, 3, 3, geoPen, DrawingLevel.LevelOne);
                    PointStyle ps = new PointStyle(myStyle.symbolType, new GeoSolidBrush(GeoColors.Yellow), geoPen, 10);
                    ps.Draw(features, canvas, labelsInThisLayer, labelsInAllLayers);
                }
            }
        }

        class RoadLineStyle : LineStyle
        {
            Double ZoomLevel;
            public RoadLineStyle(GeoPen outlinePen, Double ZoomLvl) : base(outlinePen) { ZoomLevel = ZoomLvl; }
            protected override Collection<string> GetRequiredColumnNamesCore()
            {
                return new Collection<string>() { "roadtype" };
            }
            protected override void DrawCore(IEnumerable<Feature> features, GeoCanvas canvas, Collection<SimpleCandidate> labelsInThisLayer, Collection<SimpleCandidate> labelsInAllLayers)
            {
                float penWidthScale = (float)Convert.ToDouble(1.355403 + ZoomLevel - 10);
                features = features.OrderByDescending(x => x.ColumnValues["roadtype"]);
                foreach (Feature feature in features)
                {
                    LineStyle result = new LineStyle(new GeoPen(GeoColors.BurlyWood));
                    GeoPen outerPen = new GeoPen(new GeoSolidBrush(GeoColors.Green), penWidthScale);
                    GeoPen centerPen = new GeoPen(new GeoSolidBrush(GeoColors.Yellow), penWidthScale - 1);

                    switch (feature.ColumnValues["roadtype"])
                    {
                        case "RESIDENTIAL":
                            outerPen = new GeoPen(new GeoSolidBrush(GeoColors.ForestGreen), penWidthScale);
                            centerPen = new GeoPen(new GeoSolidBrush(GeoColors.PaleYellow), penWidthScale - 1);

                            result = new LineStyle(outerPen);
                            result.CenterPen = centerPen;
                            break;

                        case "A":
                            outerPen = new GeoPen(new GeoSolidBrush(GeoColors.Salmon), penWidthScale);
                            centerPen = new GeoPen(new GeoSolidBrush(GeoColors.Red), penWidthScale - 1);

                            result = new LineStyle(outerPen);
                            result.CenterPen = centerPen;
                            break;
                    }
                    result.Draw(new Feature[] { feature }, canvas, labelsInThisLayer, labelsInAllLayers);
                }
            }
        }

        class ProjectCPMPointStyle : PointStyle
        {
            public ProjectCPMPointStyle(PointSymbolType symbolType, GeoSolidBrush symbolSolidBrush, int symbolSize) : base(symbolType, symbolSolidBrush, symbolSize) { }
            protected override Collection<string> GetRequiredColumnNamesCore()
            {
                return new Collection<string>() { "Category" };
            }
            protected override void DrawCore(IEnumerable<Feature> features, GeoCanvas canvas, Collection<SimpleCandidate> labelsInThisLayer, Collection<SimpleCandidate> labelsInAllLayers)
            {
                foreach (Feature feature in features)
                {
                    String Category = feature.ColumnValues["Category"];
                    GeoColor geoColor = GeoColors.DarkBlue;
                    switch (Category)
                    {

                        case ("Red"):
                            geoColor = GeoColors.Red;
                            break;

                        case ("Yellow"):
                            geoColor = GeoColors.Yellow;
                            break;

                        case ("Green"):
                            geoColor = GeoColors.Green;
                            break;
                    }

                    GeoPen geoPen = new GeoPen(geoColor, 3);
                    PointStyle ps = new PointStyle(PointSymbolType.Star2, new GeoSolidBrush(geoColor), new GeoPen(GeoColors.DarkGreen, 3), 20);
                    ps.Draw(features, canvas, labelsInThisLayer, labelsInAllLayers);
                }
            }
        }


        class TrafficLineStyle : LineStyle
        {
            Double ZoomLevel;
            public TrafficLineStyle(GeoPen outlinePen, Double ZoomLvl) : base(outlinePen) { ZoomLevel = ZoomLvl; }
            protected override Collection<string> GetRequiredColumnNamesCore()
            {
                return new Collection<string>() { "Colour" };
            }
            protected override void DrawCore(IEnumerable<Feature> features, GeoCanvas canvas, Collection<SimpleCandidate> labelsInThisLayer, Collection<SimpleCandidate> labelsInAllLayers)
            {
                float penWidthScale = 1.5F; // (float)Convert.ToDouble(1.355403 + ZoomLevel - 10);
                features = features.OrderByDescending(x => x.ColumnValues["Colour"]);
                foreach (Feature feature in features)
                {
                    LineStyle result = new LineStyle(new GeoPen(GeoColors.BurlyWood));
                    GeoPen outerPen = new GeoPen(new GeoSolidBrush(GeoColors.Green), penWidthScale);
                    GeoPen centerPen = new GeoPen(new GeoSolidBrush(GeoColors.Yellow), penWidthScale - 1);

                    switch (feature.ColumnValues["Colour"])
                    {
                        case "Red":
                            outerPen = new GeoPen(new GeoSolidBrush(GeoColors.DarkRed), penWidthScale);
                            centerPen = new GeoPen(new GeoSolidBrush(GeoColors.Red), penWidthScale - 1);

                            result = new LineStyle(outerPen);
                            result.CenterPen = centerPen;
                            break;

                        case "Green":
                            outerPen = new GeoPen(new GeoSolidBrush(GeoColors.DarkGreen), penWidthScale);
                            centerPen = new GeoPen(new GeoSolidBrush(GeoColors.Green), penWidthScale - 1);

                            result = new LineStyle(outerPen);
                            result.CenterPen = centerPen;
                            break;

                        case "Yellow":
                            outerPen = new GeoPen(new GeoSolidBrush(GeoColors.DarkYellow), penWidthScale);
                            centerPen = new GeoPen(new GeoSolidBrush(GeoColors.Yellow), penWidthScale - 1);

                            result = new LineStyle(outerPen);
                            result.CenterPen = centerPen;
                            break;
                    }
                    result.Draw(new Feature[] { feature }, canvas, labelsInThisLayer, labelsInAllLayers);
                }
            }
        }
        #endregion

        #region Trips
        public Boolean CreateTrips(TripData td)
        {
            Boolean b = false;
            try
            {
                if (!(System.IO.Directory.Exists(LayerPaths.TripPath(String.Empty))))
                    System.IO.Directory.CreateDirectory(LayerPaths.TripPath(String.Empty));

                string[] filePaths = Directory.GetFiles(LayerPaths.TripPath(String.Empty), td.UserToken.ToString() + ".*");
                foreach (String s in filePaths)
                    if (File.Exists(s))
                        try
                        {
                            File.Delete(s);
                        }
                        catch { }

                List<DbfColumn> dbfColumns = new List<DbfColumn>();
                dbfColumns.Add(new DbfColumn("tripid", DbfColumnType.Numeric, 10, 0));
                dbfColumns.Add(new DbfColumn("tripcolor", DbfColumnType.Numeric, 15, 0));
                dbfColumns.Add(new DbfColumn("ordno", DbfColumnType.Character, 500, 0));

                String path = LayerPaths.TripPath(td.UserToken.ToString()) + ".shp";
                ShapeFileFeatureLayer.CreateShapeFile(ShapeFileType.Polyline, path, dbfColumns, Encoding.Default, OverwriteMode.Overwrite);

                ShapeFileFeatureLayer newShapeFile = new ShapeFileFeatureLayer(path, GeoFileReadWriteMode.ReadWrite);
                newShapeFile.Open();
                newShapeFile.EditTools.BeginTransaction();

                foreach (DataRow dr in td.dtData.Rows)
                {
                    Feature feature = new Feature(dr["GeomData"].ToString());
                    feature.ColumnValues.Add("tripid", dr["TripID"].ToString());
                    feature.ColumnValues.Add("tripcolor", dr["color"].ToString());
                    feature.ColumnValues.Add("ordno", dr["OrdNo"].ToString());
                    newShapeFile.EditTools.Add(feature);
                }

                newShapeFile.EditTools.CommitTransaction();
                newShapeFile.Close();

                b = true;
            }
            catch (Exception ex) { }

            return b;
        }
        public LayerOverlay ShowTrips(Guid UserToken)
        {
            LayerOverlay layerOverlay = new LayerOverlay();
            String shpPath = LayerPaths.TripPath(UserToken.ToString()) + ".shp";

            Proj4Projection proj4 = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetSphericalMercatorParametersString());
            proj4 = new Proj4Projection(4326, 3857);
            if (!proj4.IsOpen) proj4.Open();

            ShapeFileFeatureLayer shp = new ShapeFileFeatureLayer(shpPath);
            shp.FeatureSource.Projection = new Proj4Projection(4326, 3857);
            shp.Name = "Trips";

            shp.ZoomLevelSet.ZoomLevel10.DefaultAreaStyle = new CustomAreaStyle(GeoPens.Black);
            shp.ZoomLevelSet.ZoomLevel10.DefaultLineStyle = new CustomLineStyle(GeoPens.Black);
            shp.ZoomLevelSet.ZoomLevel10.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level16;

            shp.ZoomLevelSet.ZoomLevel17.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("ordno", "Aerial", 12, DrawingFontStyles.Italic, GeoColors.Black);
            shp.ZoomLevelSet.ZoomLevel17.DefaultAreaStyle = new CustomAreaStyle(GeoPens.Black);
            shp.ZoomLevelSet.ZoomLevel17.DefaultLineStyle = new CustomLineStyle(GeoPens.Black);
            shp.ZoomLevelSet.ZoomLevel17.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

            layerOverlay.Layers.Add(shp.Name, shp);
            return layerOverlay;
        }

        #endregion

        #region Indexes
        public void BuildIndexFromDir(String sPath)
        {
            string[] filePaths;
            if (String.IsNullOrEmpty(sPath))
                filePaths = Directory.GetFiles(LayerPaths.QGISPath(String.Empty), "*.shp");
            else
                filePaths = Directory.GetFiles(sPath, "*.shp");

            foreach (String f in filePaths)
                ShapeFileFeatureLayer.BuildIndexFile(f, BuildIndexMode.Rebuild);
        }
        #endregion

        #region Reading shapes features
        public JObject QryShp(Double x, Double y, Double radius, Guid UserToken)
        {
            // return response
            JObject result = new JObject();

            //Zones
            String shpPath = LayerPaths.ZonePath(UserToken.ToString()) + ".shp";
            if (File.Exists(shpPath))
                result = QryZoneShp(x, y, radius, shpPath, 5);

            //NaveoBaseMap
            JToken token = result["name"];
            if (token == null)
            {
                result = QryRoadShp(x, y, 1, LayerPaths.BaseESRIMapsPath("RoadResidential.shp"), 1);
                token = result["name"];
                if (token == null)
                    result = QryRoadShp(x, y, 1, LayerPaths.BaseESRIMapsPath("RoadB2014.shp"), 1);
                token = result["name"];
                if (token == null)
                    result = QryRoadShp(x, y, 1, LayerPaths.BaseESRIMapsPath("RoadA2014.shp"), 1);
                token = result["name"];
                if (token == null)
                    result = QryRoadShp(x, y, 1, LayerPaths.BaseESRIMapsPath("Motorway2014.shp"), 1);
                token = result["name"];
                if (token == null)
                {
                    result = QryRoadShp(x, y, 1, LayerPaths.BaseESRIMapsPath("Poi.shp"), 1);
                    token = result["name"];
                    if (token != null)
                        result["name"] = "near " + result["name"];
                }

                JObject VillageTown = QryRoadShp(x, y, 1, LayerPaths.BaseESRIMapsPath("Village_limit.shp"), 1);
                token = VillageTown["name"];
                if (token == null)
                    VillageTown = QryRoadShp(x, y, 1, LayerPaths.BaseESRIMapsPath("Township_Area.shp"), 1);

                if (VillageTown["success"].ToString() == "True")
                    result["success"] = "true";

                result["name"] += " " + VillageTown["name"];
            }

            return result;
        }
        public JObject QryMokaShp(Double x, Double y, Double radius)
        {
            // return response
            JObject result = new JObject();
            string[] filePaths = Directory.GetFiles(LayerPaths.MokaAssetsPath(String.Empty), "*.shp");
            foreach (String f in filePaths)
            {
                result = QryAllFeaturesFromShp(x, y, 5, f);
                if (result["success"].ToString() == "True")
                    break;
            }

            if (result["success"].ToString() != "True")
                result = QryShp(x, y, 5, new Guid("fc1f50ed-0cee-4da7-9dd0-345c6e930666"));

            return result;
        }

        JObject QryZoneShp(Double x, Double y, Double radius, String sPath, int maxItemToFind)
        {
            JObject result = new JObject();
            ShapeFileFeatureSource source = new ShapeFileFeatureSource(sPath);
            source.Open();

            Collection<Feature> features = source.GetFeaturesNearestTo(
                new PointShape(x, y),
                GeographyUnit.DecimalDegree,
                maxItemToFind,
                new string[2] { "zoneid", "descrip" },
                radius,
                DistanceUnit.Meter
            );
            source.Close();

            if (features.Count > 0)
            {
                string zoneid = String.Empty;
                string descrip = String.Empty;

                foreach (Feature f in features)
                {
                    zoneid += f.ColumnValues["zoneid"] + " ";
                    descrip += f.ColumnValues["descrip"] + ", ";
                }
                if (descrip.Length > 2)
                    descrip = descrip.Remove(descrip.Length - 2);
                result.Add("zoneid", JToken.FromObject(zoneid));
                result.Add("name", JToken.FromObject(descrip));
                result.Add("success", JToken.FromObject(true));
            }
            else
            {
                result.Add("success", JToken.FromObject(false));
            }
            return result;
        }
        JObject QryRoadShp(Double x, Double y, Double radius, String sPath, int maxItemToFind)
        {
            JObject result = new JObject();
            ShapeFileFeatureSource source = new ShapeFileFeatureSource(sPath);
            source.Open();
            Collection<Feature> features = source.GetFeaturesNearestTo(
                new PointShape(x, y),
                GeographyUnit.DecimalDegree,
                10,
                new string[1] { "NAME" },
                10,
                DistanceUnit.Meter
            );
            source.Close();

            if (features.Count > 0)
            {
                string descrip = features[0].ColumnValues["name"];

                result.Add("name", JToken.FromObject(descrip));
                result.Add("success", JToken.FromObject(true));
            }
            else
            {
                result.Add("success", JToken.FromObject(false));
            }
            return result;
        }

        public JObject QryAllFeaturesFromShp(Double x, Double y, Double radius, String sPath)
        {
            JObject result = new JObject();
            ShapeFileFeatureSource source = new ShapeFileFeatureSource(sPath);
            source.Open();

            Collection<FeatureSourceColumn> cols = source.GetColumns();
            List<String> lCols = new List<string>();
            foreach (FeatureSourceColumn f in cols)
                lCols.Add(f.ColumnName);

            foreach (Feature d in source.GetAllFeatures(lCols))
            { }

            Collection<Feature> features = source.GetFeaturesNearestTo(
                new PointShape(x, y),
                GeographyUnit.DecimalDegree,
                1,
                lCols,
                radius,
                DistanceUnit.Meter
            );
            source.Close();

            if (features.Count > 0)
            {
                string descrip = String.Empty;
                DataTable dt = new DataTable("features");
                dt.Columns.Add("key");
                dt.Columns.Add("value");

                foreach (Feature f in features)
                    foreach (var v in f.ColumnValues)
                        //dt.Rows.Add(v.Key, v.Value);
                        descrip += v.Key + " : " + v.Value;

                if (descrip.Length > 2)
                    descrip = descrip.Remove(descrip.Length - 2);

                //result.Add("name", JToken.FromObject(dt));
                result.Add("name", JToken.FromObject(descrip));
                result.Add("success", JToken.FromObject(true));
            }
            else
            {
                result.Add("success", JToken.FromObject(false));
            }
            return result;
        }
        #endregion

        #region Routing
        public Guid CreateRoute(LineShape ls, List<lonLatCoordinate> lPoints)
        {
            Guid result = Guid.NewGuid();

            CommonHelper.DeleteOldFiles(LayerPaths.RoutingTmpPath(String.Empty), 1);

            #region ls
            List<DbfColumn> dbfColumns = new List<DbfColumn>();
            dbfColumns.Add(new DbfColumn("routeid", DbfColumnType.Numeric, 10, 0));

            String path = LayerPaths.RoutingTmpPath(result.ToString()) + "L.shp";
            ShapeFileFeatureLayer.CreateShapeFile(ShapeFileType.Polyline, path, dbfColumns, Encoding.Default, OverwriteMode.Overwrite);

            ShapeFileFeatureLayer newShapeFile = new ShapeFileFeatureLayer(path, GeoFileReadWriteMode.ReadWrite);
            newShapeFile.Open();
            newShapeFile.EditTools.BeginTransaction();

            Feature feature = new Feature(ls);
            feature.ColumnValues.Add("routeid", "0");
            newShapeFile.EditTools.Add(feature);

            newShapeFile.EditTools.CommitTransaction();
            newShapeFile.Close();
            #endregion

            #region Points
            dbfColumns = new List<DbfColumn>();
            dbfColumns.Add(new DbfColumn("PointID", DbfColumnType.Character, 10, 0));

            path = LayerPaths.RoutingTmpPath(result.ToString()) + "_P.shp";
            ShapeFileFeatureLayer.CreateShapeFile(ShapeFileType.Point, path, dbfColumns, Encoding.Default, OverwriteMode.Overwrite);

            newShapeFile = new ShapeFileFeatureLayer(path, GeoFileReadWriteMode.ReadWrite);
            newShapeFile.Open();
            newShapeFile.EditTools.BeginTransaction();

            int i = 0;
            foreach (lonLatCoordinate llc in lPoints)
            {
                String s = "End";
                if (i == 0) s = "Start";
                else if (i < lPoints.Count - 1) s = "ViaPoint " + i;

                feature = new Feature(llc.lon, llc.lat);              
                feature.ColumnValues.Add("PointID", s);
                newShapeFile.EditTools.Add(feature);
                i++;
            }
            newShapeFile.EditTools.CommitTransaction();
            newShapeFile.Close();
            #endregion

            return result;
        }
        public LayerOverlay ShowRoute(String routePath)
        {
            LayerOverlay layerOverlay = new LayerOverlay();
            String[] filePaths = Directory.GetFiles(LayerPaths.RoutingTmpPath(String.Empty), routePath + "*.shp");
            if (filePaths.Length == 0)
                return layerOverlay;

            Proj4Projection proj4 = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetSphericalMercatorParametersString());
            proj4 = new Proj4Projection(4326, 3857);
            if (!proj4.IsOpen) proj4.Open();

            foreach (String file in filePaths)
            {
                ShapeFileFeatureLayer shp = new ShapeFileFeatureLayer(file);
                shp.FeatureSource.Projection = proj4;
                shp.Name = file;

                shp.ZoomLevelSet.ZoomLevel10.DefaultLineStyle = new CustomLineStyle(GeoPens.BlueViolet);
                shp.ZoomLevelSet.ZoomLevel10.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level16;

                shp.ZoomLevelSet.ZoomLevel17.DefaultLineStyle = new CustomLineStyle(GeoPens.BlueViolet);
                shp.ZoomLevelSet.ZoomLevel17.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

                if (file.EndsWith("_P.shp"))
                {
                    shp.ZoomLevelSet.ZoomLevel10.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("PointID", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                    shp.ZoomLevelSet.ZoomLevel10.DefaultPointStyle = new PointStyle(PointSymbolType.Star, new GeoSolidBrush(GeoColors.Yellow), new GeoPen(GeoColors.Blue, 1), 30);
                    shp.ZoomLevelSet.ZoomLevel10.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level16;

                    shp.ZoomLevelSet.ZoomLevel17.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("PointID", "Aerial", 9, DrawingFontStyles.Italic, GeoColors.Black);
                    shp.ZoomLevelSet.ZoomLevel17.DefaultPointStyle = new PointStyle(PointSymbolType.Star, new GeoSolidBrush(GeoColors.Yellow), new GeoPen(GeoColors.Blue, 1), 30);
                    shp.ZoomLevelSet.ZoomLevel17.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
                }

                layerOverlay.Layers.Add(shp.Name, shp);
            }
            return layerOverlay;
        }
        #endregion

        #region BLUP
        public String CreateBlupWithinDistanceInMeters(BlupCreation bc)
        {
            String sError = String.Empty;
            try
            {
                if (!(System.IO.Directory.Exists(LayerPaths.BLUPPath(String.Empty))))
                    System.IO.Directory.CreateDirectory(LayerPaths.BLUPPath(String.Empty));

                string[] filePaths = Directory.GetFiles(LayerPaths.BLUPPath(String.Empty), "*.*");
                foreach (String file in filePaths)
                {
                    try
                    {
                        FileInfo fi = new FileInfo(file);
                        if (fi.LastAccessTime < DateTime.Now.AddHours(-1))
                            fi.Delete();
                    }
                    catch { }
                }

                bc.BlupPath = Guid.NewGuid().ToString();

                GeoCollection<ShapeFileFeatureLayer> getCircles = new GeoCollection<ShapeFileFeatureLayer>();
                if (bc.bShowBuffers)
                    getCircles.Add(GetCircles(bc.lon, bc.lat, bc.BlupPath));
                ShapeFileFeatureLayer getPoints = GetPoints(bc.dtData, bc.BlupPath.ToString());
                getCircles.Add(getPoints);
            }
            catch (Exception ex)
            {
                sError += ex.ToString();
            }
            return bc.BlupPath + sError;
        }
        public LayerOverlay ShowBlupLayers(String path)
        {
            LayerOverlay layerOverlay = new LayerOverlay();
            String[] filePaths = Directory.GetFiles(LayerPaths.BLUPPath(String.Empty), path + "*.shp");
            if (filePaths.Length == 0)
                return layerOverlay;

            Proj4Projection proj4 = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetSphericalMercatorParametersString());
            proj4 = new Proj4Projection(4326, 3857);

            foreach (String file in filePaths)
            {
                TextStyle ts = TextStyles.CreateSimpleTextStyle("APL_ID", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                ts.OverlappingRule = LabelOverlappingRule.NoOverlapping;

                ShapeFileFeatureLayer shp = new ShapeFileFeatureLayer(file);

                IconValueStyle iconValueStyle = new IconValueStyle();
                iconValueStyle.ColumnName = "Cat";
                iconValueStyle.OverlappingRule = LabelOverlappingRule.NoOverlapping;

                ValueStyle v = new ValueStyle();
                v.ColumnName = "Cat";

                if (file.EndsWith("_P.shp"))
                {
                    for (int i = 1; i <= 12; i++)
                        iconValueStyle.IconValueItems.Add(new IconValueItem(i.ToString(), blupImage(i.ToString()), ts));
                }
                else if (file.EndsWith("_Y.shp"))
                {
                    for (int i = 1; i <= 12; i++)
                        v.ValueItems.Add(new ValueItem(i.ToString(), new AreaStyle(new GeoPen(blupColor(i.ToString())), new GeoSolidBrush(blupColor(i.ToString())))));
                }
                if (file.EndsWith("_L.shp"))
                {
                    for (int i = 1; i <= 12; i++)
                        v.ValueItems.Add(new ValueItem(i.ToString(), new LineStyle(new GeoPen(blupColor(i.ToString()), 3), new GeoPen(GeoColors.Gray, 1.5F))));
                }

                ZoomLevelSet zoomLevelSet = new CustomZoomLevelSet();
                var zoomLevels = zoomLevelSet.GetZoomLevels();
                foreach (ZoomLevel zoomLevel in zoomLevels)
                {
                    zoomLevel.CustomStyles.Add(iconValueStyle);
                    zoomLevel.CustomStyles.Add(v);

                    if (zoomLevel.Scale < 18000)
                        zoomLevel.CustomStyles.Add(ts);
                    else
                        zoomLevel.CustomStyles.Add(GetClusterPointStyle());
                }

                shp.ZoomLevelSet = zoomLevelSet;
                shp.FeatureSource.Projection = proj4;
                layerOverlay.Layers.Add(shp);
            }

            return layerOverlay;
        }

        GeoColor blupColor(String value)
        {
            GeoColor geoColor = GeoColors.Black;
            switch (value)
            {
                case "1":
                    geoColor = GeoColors.Red;
                    break;

                case "2":
                    geoColor = GeoColors.Blue;
                    break;

                case "3":
                    geoColor = GeoColors.Yellow;
                    break;

                case "4":
                    geoColor = GeoColors.Green;
                    break;

                case "5":
                    geoColor = GeoColors.DarkBlue;
                    break;

                case "6":
                    geoColor = GeoColors.DarkKhaki;
                    break;

                case "7":
                    geoColor = GeoColors.DarkOrange;
                    break;

                case "8":
                    geoColor = GeoColors.DarkRed;
                    break;

                case "9":
                    geoColor = GeoColors.DarkYellow;
                    break;

                case "10":
                    geoColor = GeoColors.DarkMagenta;
                    break;

                case "11":
                    geoColor = GeoColors.DarkCyan;
                    break;

                case "12":
                    geoColor = GeoColors.DarkTurquoise;
                    break;

                default:
                    geoColor = GeoColors.DarkViolet;
                    break;
            }
            return geoColor;
        }
        GeoImage blupImage(String value)
        {
            GeoColor newColor = blupColor(value);
            Color c = Color.FromArgb(newColor.AlphaComponent, newColor.RedComponent, newColor.GreenComponent, newColor.BlueComponent);

            Bitmap bmp = null;
            String filename = LayerPaths.iconsPath("Blup.png");
            bmp = (Bitmap)Image.FromFile(filename);
            bmp = ChangeColor(bmp, c);
            GeoImage geoImage = new GeoImage(bmp);
            return geoImage;
        }
        private static ClassBreakClusterPointStyle GetClusterPointStyle()
        {
            ClassBreakClusterPointStyle clusterPointStyle = new ClassBreakClusterPointStyle();
            clusterPointStyle.CellSize = 65;

            // Create the PointStyle for different class breaks.
            PointStyle pointStyle1 = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColor.FromArgb(250, 222, 226, 153)), new GeoPen(GeoColor.FromArgb(100, 222, 226, 153), 5), 8);
            clusterPointStyle.ClassBreakPoints.Add(1, pointStyle1);

            PointStyle pointStyle2 = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColor.FromArgb(250, 222, 226, 153)), new GeoPen(GeoColor.FromArgb(100, 222, 226, 153), 8), 15);
            clusterPointStyle.ClassBreakPoints.Add(2, pointStyle2);

            PointStyle pointStyle3 = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColor.FromArgb(250, 255, 183, 76)), new GeoPen(GeoColor.FromArgb(100, 255, 183, 76), 10), 25);
            clusterPointStyle.ClassBreakPoints.Add(50, pointStyle3);

            PointStyle pointStyle4 = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColor.FromArgb(250, 243, 193, 26)), new GeoPen(GeoColor.FromArgb(100, 243, 193, 26), 15), 35);
            clusterPointStyle.ClassBreakPoints.Add(150, pointStyle4);

            PointStyle pointStyle5 = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColor.FromArgb(250, 245, 7, 10)), new GeoPen(GeoColor.FromArgb(100, 245, 7, 10), 15), 40);
            clusterPointStyle.ClassBreakPoints.Add(350, pointStyle5);

            PointStyle pointStyle6 = new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColor.FromArgb(250, 245, 7, 10)), new GeoPen(GeoColor.FromArgb(100, 245, 7, 10), 20), 50);
            clusterPointStyle.ClassBreakPoints.Add(500, pointStyle6);

            clusterPointStyle.TextStyle = TextStyles.CreateSimpleTextStyle("FeatureCount", "Arail", 10, DrawingFontStyles.Regular, GeoColor.SimpleColors.Black);
            clusterPointStyle.TextStyle.PointPlacement = PointPlacement.Center;

            return clusterPointStyle;
        }


        Bitmap ChangeColor(Bitmap scrBitmap, Color newColor)
        {
            Color actualColor;
            //make an empty bitmap the same size as scrBitmap. 
            //197:105   197:222
            //325:105   327:222
            Bitmap newBitmap = new Bitmap(scrBitmap.Width, scrBitmap.Height);
            for (int i = 0; i < scrBitmap.Width; i++)
            {
                for (int j = 0; j < scrBitmap.Height; j++)
                {
                    //get the pixel from the scrBitmap image
                    actualColor = scrBitmap.GetPixel(i, j);
                    // > 150 because.. Images edges can be of low pixel colr. if we set all pixel color to new then there will be no smoothness left.
                    if (actualColor.A > 150)
                        newBitmap.SetPixel(i, j, newColor);
                    else
                        newBitmap.SetPixel(i, j, actualColor);
                }
            }
            return newBitmap;
        }

        public LayerOverlay ShowOutlinePlanningScheme(String sFile)
        {
            LayerOverlay layerOverlay = new LayerOverlay();
            String shpPath = LayerPaths.BlupShpPath(String.Empty);
            shpPath = Path.Combine(shpPath, sFile + ".shp");

            Proj4Projection proj4 = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetSphericalMercatorParametersString());
            proj4 = new Proj4Projection(4326, 3857);

            ShapeFileFeatureLayer shp = new ShapeFileFeatureLayer(shpPath);
            shp.Name = sFile;

            ZoomLevelSet zls = new CustomZoomLevelSet();
            var zl = zls.GetZoomLevels();

            MemoryStream memoryStream = new MemoryStream();
            Bitmap bitmap = new Bitmap(25, 25);
            GeoImage geoImage = new GeoImage();
            GeoColor geoColor = GeoColors.RosyBrown;
            GeoPen geoPen = new GeoPen();
            AreaStyle areaStyle = new AreaStyle();

            switch (sFile)
            {
                case "3mFromRoad":
                    geoColor = GeoColor.FromHtml("#aa162c");
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("Name", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = new AreaStyle(new GeoPen(geoColor), new GeoSolidBrush(geoColor));
                    }
                    break;

                case "ARoad":
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("Name", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultLineStyle = new LineStyle(new GeoPen(GeoColors.Black, 3), new GeoPen(GeoColors.Gray, 1.5F));
                    }
                    break;

                case "BRoad":
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("Name", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultLineStyle = new LineStyle(new GeoPen(GeoColors.Black, 2), new GeoPen(GeoColors.Gray, 1));
                    }
                    break;

                case "ARdBuffer":
                    geoColor = GeoColors.DeepSkyBlue;
                    geoColor.AlphaComponent = 150;
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultAreaStyle = new AreaStyle(new GeoPen(GeoColors.GrayText), new GeoSolidBrush(geoColor));
                    }
                    break;

                case "BRdBuffer":
                    geoColor = GeoColors.DeepSkyBlue;
                    geoColor.AlphaComponent = 150;
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultAreaStyle = new AreaStyle(new GeoPen(GeoColors.GrayText), new GeoSolidBrush(geoColor));
                    }
                    break;

                case "BeautyParlour":
                    geoColor = GeoColors.Purple;
                    areaStyle = AreaStyles.CreateHatchStyle(GeoHatchStyle.WideUpwardDiagonal, GeoColor.StandardColors.Transparent, GeoColor.FromArgb(150, geoColor), geoColor, 1, LineDashStyle.Solid, 0, 0);

                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = areaStyle;
                    }
                    break;

                case "BuiltUpArea":
                    GeoColor grayText = GeoColors.GrayText;
                    grayText.AlphaComponent = 150;
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = new AreaStyle(new GeoPen(GeoColors.GrayText), new GeoSolidBrush(grayText));
                    }
                    break;

                case "CatchmentBoundaries":
                    bitmap = new Bitmap(50, 50);
                    memoryStream = new MemoryStream();
                    geoImage = new GeoImage();
                    using (Graphics o = Graphics.FromImage(bitmap))
                    {
                        System.Drawing.Drawing2D.Matrix rotation = new System.Drawing.Drawing2D.Matrix();
                        rotation.Rotate(30);
                        o.Transform = rotation;

                        Font newFont = new Font("Times New Roman", 17);
                        o.DrawString("-", newFont, new SolidBrush(Color.Blue), 10, 10);

                        bitmap.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Png);
                    }
                    geoImage = new GeoImage(memoryStream);

                    geoColor = GeoColors.Blue;
                    geoColor.AlphaComponent = 50;
                    geoPen = new GeoPen(GeoColors.Blue);
                    geoPen.DashStyle = LineDashStyle.Dot;
                    areaStyle = new AreaStyle(geoPen, new GeoSolidBrush(geoColor));
                    areaStyle.Advanced.FillCustomBrush = new GeoTextureBrush(geoImage, GeoWrapMode.Tile);
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = areaStyle;
                    }
                    break;

                case "CentralBusinessDistrict":
                    GeoColor red = GeoColors.Red;
                    red.AlphaComponent = 40;
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = new AreaStyle(new GeoPen(GeoColors.Red, 1), new GeoSolidBrush(red));
                    }
                    break;

                case "CitizenAdviceBureau":
                    geoColor = GeoColors.Black;
                    areaStyle = AreaStyles.CreateHatchStyle(GeoHatchStyle.LightUpwardDiagonal, GeoColor.StandardColors.Yellow, GeoColor.FromArgb(150, geoColor), geoColor, 1, LineDashStyle.Solid, 0, 0);

                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = areaStyle;
                    }
                    break;

                case "CommercialBuildings":
                    geoColor = GeoColors.Brown;
                    geoColor.AlphaComponent = 50;
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = new AreaStyle(new GeoPen(GeoColors.Brown), new GeoSolidBrush(geoColor));
                    }
                    break;

                case "ElectricalPowerLine":
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultLineStyle = new LineStyle(new GeoPen(GeoColors.DarkMagenta, 1));
                    }
                    break;

                case "ElectricalSubstation":
                    GeoColor Fuchsia = GeoColors.Fuchsia;
                    Fuchsia.AlphaComponent = 50;
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = new AreaStyle(new GeoPen(GeoColors.Fuchsia), new GeoSolidBrush(Fuchsia));
                    }
                    break;

                case "GreenSpace":
                    bitmap = new Bitmap(25, 25);
                    memoryStream = new MemoryStream();
                    using (Graphics o = Graphics.FromImage(bitmap))
                    {
                        System.Drawing.Drawing2D.Matrix rotation = new System.Drawing.Drawing2D.Matrix();
                        rotation.Rotate(30);
                        o.Transform = rotation;

                        Font newFont = new Font("Times New Roman", 8);
                        o.DrawString("v", newFont, new SolidBrush(Color.GreenYellow), 5, 5);

                        bitmap.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Png);
                    }
                    geoImage = new GeoImage(memoryStream);

                    geoColor = GeoColors.GreenYellow;
                    geoColor.AlphaComponent = 75;
                    areaStyle = new AreaStyle(new GeoPen(GeoColors.Transparent), new GeoSolidBrush(geoColor));
                    areaStyle.Advanced.FillCustomBrush = new GeoTextureBrush(geoImage, GeoWrapMode.Tile);
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = areaStyle;
                    }
                    break;

                case "GrowthZones":
                    GeoColor OrangeRed = GeoColors.OrangeRed;
                    OrangeRed.AlphaComponent = 150;
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = new AreaStyle(new GeoPen(GeoColors.OrangeRed), new GeoSolidBrush(OrangeRed));
                    }
                    break;

                case "GrowthZone":
                    GeoColor OrangeReds = GeoColors.OrangeRed;
                    OrangeReds.AlphaComponent = 150;
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = new AreaStyle(new GeoPen(GeoColors.OrangeRed), new GeoSolidBrush(OrangeReds));
                    }
                    break;

                case "HardwareStore":
                    geoColor = GeoColors.Red;
                    areaStyle = AreaStyles.CreateHatchStyle(GeoHatchStyle.WideUpwardDiagonal, GeoColor.StandardColors.Yellow, GeoColor.FromArgb(150, geoColor), geoColor, 1, LineDashStyle.Solid, 0, 0);

                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = areaStyle;
                    }
                    break;

                case "Kendra":
                    geoColor = GeoColors.Yellow;
                    areaStyle = AreaStyles.CreateHatchStyle(GeoHatchStyle.WideUpwardDiagonal, GeoColor.StandardColors.Transparent, GeoColor.FromArgb(150, geoColor), geoColor, 1, LineDashStyle.Solid, 0, 0);

                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = areaStyle;
                    }
                    break;

                case "Market":
                    geoColor = GeoColors.DarkMagenta;
                    areaStyle = AreaStyles.CreateHatchStyle(GeoHatchStyle.WideUpwardDiagonal, GeoColor.StandardColors.Transparent, GeoColor.FromArgb(150, geoColor), geoColor, 1, LineDashStyle.Solid, 0, 0);

                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = areaStyle;
                    }
                    break;

                case "MelrosePrison":
                    geoColor = GeoColors.Orange;
                    geoColor.AlphaComponent = 50;
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = new AreaStyle(new GeoPen(GeoColors.Black), new GeoSolidBrush(geoColor));
                    }
                    break;

                case "MineralResources":
                    GeoColor SandyBrown = GeoColors.SandyBrown;
                    SandyBrown.AlphaComponent = 50;
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = new AreaStyle(new GeoPen(GeoColors.SandyBrown), new GeoSolidBrush(SandyBrown));
                    }
                    break;

                case "Motorway":
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultLineStyle = new LineStyle(new GeoPen(GeoColors.Black, 4), new GeoPen(GeoColors.Gray, 2));
                    }
                    break;

                case "MountainSlope":
                    bitmap = new Bitmap(50, 50);
                    memoryStream = new MemoryStream();
                    geoImage = new GeoImage();
                    using (Graphics o = Graphics.FromImage(bitmap))
                    {
                        System.Drawing.Drawing2D.Matrix rotation = new System.Drawing.Drawing2D.Matrix();
                        rotation.Rotate(30);
                        o.Transform = rotation;

                        Font newFont = new Font("Times New Roman", 17);
                        o.DrawString("o", newFont, new SolidBrush(Color.Gray), 10, 10);

                        bitmap.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Png);
                    }
                    geoImage = new GeoImage(memoryStream);

                    geoColor = GeoColors.RosyBrown;
                    geoColor.AlphaComponent = 50;
                    areaStyle = new AreaStyle(new GeoPen(GeoColors.RosyBrown), new GeoSolidBrush(geoColor));
                    areaStyle.Advanced.FillCustomBrush = new GeoTextureBrush(geoImage, GeoWrapMode.Tile);
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = areaStyle;
                    }
                    break;

                case "MRBuffer1km":
                    GeoColor SandyBrownz = GeoColors.SandyBrown;
                    SandyBrownz.AlphaComponent = 75;
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = new AreaStyle(new GeoPen(GeoColors.SandyBrown), new GeoSolidBrush(SandyBrownz));
                    }
                    break;

                case "MinResBuffer":
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = new AreaStyle(new GeoPen(GeoColors.Black));
                    }
                    break;

                case "PlaceOfWorship":
                    GeoColor green = GeoColors.DarkOliveGreen;
                    green.AlphaComponent = 50;
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = new AreaStyle(new GeoPen(GeoColors.DarkOliveGreen, 2), new GeoSolidBrush(green));
                    }
                    break;

                case "Parking":
                    geoColor = GeoColors.Black;
                    geoColor.AlphaComponent = 90;
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultAreaStyle = new AreaStyle(new GeoPen(geoColor), new GeoSolidBrush(geoColor));
                    }
                    break;

                case "PrisonInnerBuf":
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultAreaStyle = new AreaStyle(new GeoPen(GeoColors.Black));
                    }
                    break;

                case "PrisonOuterBuf":
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultAreaStyle = new AreaStyle(new GeoPen(GeoColors.Black));
                    }
                    break;

                case "PrivatelyOwnedMountainReserve":
                    GeoColor DarkGreen = GeoColors.DarkGreen;
                    DarkGreen.AlphaComponent = 50;
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = new AreaStyle(new GeoPen(GeoColors.DarkGreen), new GeoSolidBrush(DarkGreen));
                    }
                    break;

                case "ProjectCPM":
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("Project", "Aerial", 12, DrawingFontStyles.Bold, GeoColors.DarkViolet, 8, 0);
                        zoomLevel.DefaultPointStyle = new ProjectCPMPointStyle(PointSymbolType.Star2, new GeoSolidBrush(GeoColors.Yellow), 20);
                    }
                    break;

                case "ProposedBoundary":
                    GeoColor Gold = GeoColors.Gold;
                    Gold.AlphaComponent = 50;
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = new AreaStyle(new GeoPen(GeoColors.Gold), new GeoSolidBrush(Gold));
                    }
                    break;

                case "RangePeak":
                    bitmap = new Bitmap(25, 25);
                    memoryStream = new MemoryStream();
                    using (Graphics o = Graphics.FromImage(bitmap))
                    {
                        System.Drawing.Drawing2D.Matrix rotation = new System.Drawing.Drawing2D.Matrix();
                        rotation.Rotate(30);
                        o.Transform = rotation;

                        Font newFont = new Font("Times New Roman", 8);
                        o.DrawString("o", newFont, new SolidBrush(Color.Gray), 5, 5);

                        bitmap.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Png);
                    }
                    geoImage = new GeoImage(memoryStream);

                    geoColor = GeoColors.RosyBrown;
                    geoColor.AlphaComponent = 75;
                    areaStyle = new AreaStyle(new GeoPen(GeoColors.RosyBrown), new GeoSolidBrush(geoColor));
                    areaStyle.Advanced.FillCustomBrush = new GeoTextureBrush(geoImage, GeoWrapMode.Tile);
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = areaStyle;
                    }
                    break;

                case "RdReserve":
                    geoColor = GeoColors.DarkGreen;
                    geoColor.AlphaComponent = 150;
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultAreaStyle = new AreaStyle(new GeoPen(GeoColors.DarkGreen), new GeoSolidBrush(geoColor));
                    }
                    break;

                case "ResidentialRoad":
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("Name", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultLineStyle = new LineStyle(new GeoPen(GeoColors.Black, 1), new GeoPen(GeoColors.Gray, 0.5F));
                    }
                    break;

                case "ResRdBuffer":
                    geoColor = GeoColors.DeepSkyBlue;
                    geoColor.AlphaComponent = 150;
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultAreaStyle = new AreaStyle(new GeoPen(GeoColors.GrayText), new GeoSolidBrush(geoColor));
                    }
                    break;

                case "Restaurants":
                    geoColor = GeoColors.Cyan;
                    areaStyle = AreaStyles.CreateHatchStyle(GeoHatchStyle.WideUpwardDiagonal, GeoColor.StandardColors.Transparent, GeoColor.FromArgb(150, geoColor), geoColor, 1, LineDashStyle.Solid, 0, 0);

                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = areaStyle;
                    }
                    break;

                case "RiverBuffer":
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = new AreaStyle(new GeoPen(GeoColors.Blue), new GeoSolidBrush(GeoColors.PaleBlue));
                    }
                    break;

                case "Road":
                    geoColor = GeoColors.SkyBlue;
                    geoColor.AlphaComponent = 150;
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = new AreaStyle(new GeoPen(GeoColors.Blue), new GeoSolidBrush(geoColor));
                    }
                    break;

                case "ScenicLandscape":
                    GeoColor LawnGreen = GeoColors.LawnGreen;
                    LawnGreen.AlphaComponent = 50;
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = new AreaStyle(new GeoPen(GeoColors.LawnGreen), new GeoSolidBrush(LawnGreen));
                    }
                    break;

                case "Setback":
                    geoColor = GeoColors.Red;
                    geoColor.AlphaComponent = 50;
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = new AreaStyle(new GeoPen(GeoColors.Red, 1), new GeoSolidBrush(geoColor));
                    }
                    break;

                case "SettlementBoundary":
                    GeoColor orange = GeoColors.PaleOrange;
                    orange.AlphaComponent = 50;
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = new AreaStyle(new GeoPen(GeoColors.Orange, 2), new GeoSolidBrush(orange));
                    }
                    break;

                case "SettlementBuffer":
                    GeoColor gray = GeoColors.Gray;
                    gray.AlphaComponent = 50;
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = new AreaStyle(new GeoPen(GeoColors.Gray), new GeoSolidBrush(gray));
                    }
                    break;

                case "StateForestLand":
                    GeoColor ForestGreen = GeoColors.ForestGreen;
                    ForestGreen.AlphaComponent = 50;
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = new AreaStyle(new GeoPen(GeoColors.ForestGreen), new GeoSolidBrush(ForestGreen));
                    }
                    break;

                case "SubHall":
                    geoColor = GeoColors.Red;
                    areaStyle = AreaStyles.CreateHatchStyle(GeoHatchStyle.WideUpwardDiagonal, GeoColor.StandardColors.Transparent, GeoColor.FromArgb(150, geoColor), geoColor, 1, LineDashStyle.Solid, 0, 0);

                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = areaStyle;
                    }
                    break;

                case "SugarFactory":
                    GeoColor Purple = GeoColors.Purple;
                    Purple.AlphaComponent = 50;
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = new AreaStyle(new GeoPen(GeoColors.Purple), new GeoSolidBrush(Purple));
                    }
                    break;

                case "Traffic":
                    int iZoom = 0;
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("Name", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultLineStyle = new TrafficLineStyle(GeoPens.AliceBlue, iZoom);
                        iZoom++;
                    }
                    break;

                case "WaterResources":
                    GeoColor SkyBlue = GeoColors.SkyBlue;
                    SkyBlue.AlphaComponent = 50;
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = new AreaStyle(new GeoPen(GeoColors.SkyBlue), new GeoSolidBrush(SkyBlue));
                    }
                    break;

                case "Wall":
                    foreach (var zoomLevel in zl)
                        zoomLevel.DefaultLineStyle = new LineStyle(new GeoPen(GeoColors.ForestGreen, 2));
                    break;

                default:
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("AssetName", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = new AreaStyle(new GeoPen(GeoColors.Lavender), new GeoSolidBrush(GeoColors.DarkBlue));
                        zoomLevel.DefaultLineStyle = new LineStyle(new GeoPen(GeoColors.DarkBlue));
                        zoomLevel.DefaultPointStyle = new PointStyle(PointSymbolType.Star, new GeoSolidBrush(GeoColors.Yellow), new GeoPen(GeoColors.Blue, 1), 30);
                    }
                    break;
            }

            shp.ZoomLevelSet = zls;
            shp.FeatureSource.Projection = proj4;
            layerOverlay.Layers.Add(shp.Name, shp);
            return layerOverlay;
        }

        DataTable initDTBuffer()
        {
            DataTable dtBuffer = new DataTable("dtBuffer");
            dtBuffer.Columns.Add("X", typeof(float));
            dtBuffer.Columns.Add("Y", typeof(float));
            dtBuffer.Columns.Add("Radius", typeof(int));
            dtBuffer.Columns.Add("iColor", typeof(int));
            dtBuffer.Columns.Add("sDesc", typeof(String));
            return dtBuffer;
        }
        ShapeFileFeatureLayer GetCircles(Double lon, Double lat, String path)
        {
            DataTable dtBuffer = initDTBuffer();
            DataRow dr = dtBuffer.NewRow();

            dr = dtBuffer.NewRow();
            dr["X"] = lon;
            dr["Y"] = lat;
            dr["radius"] = 300;
            dr["iColor"] = Color.FromArgb(75, Color.Red).ToArgb();
            dtBuffer.Rows.Add(dr);

            dr = dtBuffer.NewRow();
            dr["X"] = lon;
            dr["Y"] = lat;
            dr["radius"] = 200;
            dr["iColor"] = Color.FromArgb(75, Color.Yellow).ToArgb();
            dtBuffer.Rows.Add(dr);

            dr = dtBuffer.NewRow();
            dr["X"] = lon;
            dr["Y"] = lat;
            dr["radius"] = 100;
            dr["iColor"] = Color.FromArgb(75, Color.Green).ToArgb();
            dtBuffer.Rows.Add(dr);

            return GetCircles(dtBuffer, path);
        }
        ShapeFileFeatureLayer GetCircles(DataTable dtBuffer, String path)
        {
            String shpPath = LayerPaths.BLUPPath(path) + "_C.shp";

            List<DbfColumn> dbfColumns = new List<DbfColumn>();
            dbfColumns.Add(new DbfColumn("Radius", DbfColumnType.Character, 500, 0));
            dbfColumns.Add(new DbfColumn("iColor", DbfColumnType.Numeric, 20, 0));
            ShapeFileFeatureLayer.CreateShapeFile(ShapeFileType.Polygon, shpPath, dbfColumns, Encoding.Default, OverwriteMode.Overwrite);
            ShapeFileFeatureLayer memo = new ShapeFileFeatureLayer(shpPath, GeoFileReadWriteMode.ReadWrite);
            memo.Open();
            memo.EditTools.BeginTransaction();
            foreach (DataRow dr in dtBuffer.Rows)
            {
                PointShape centerPointInDecimalDegree = new PointShape((float)Convert.ToDouble(dr["X"]), (float)Convert.ToDouble(dr["Y"]));
                EllipseShape circle = new EllipseShape(centerPointInDecimalDegree, Convert.ToInt32(dr["Radius"]), GeographyUnit.DecimalDegree, DistanceUnit.Meter);
                Feature f = new Feature(circle);
                f.ColumnValues.Add("Radius", dr["Radius"].ToString());
                f.ColumnValues.Add("iColor", dr["iColor"].ToString());
                memo.EditTools.Add(f);
            }
            memo.EditTools.CommitTransaction();
            memo.Close();

            return memo;
        }
        ShapeFileFeatureLayer GetPoints(DataTable dt, String path)
        {
            String shpPathPoint = LayerPaths.BLUPPath(path) + "_P.shp";
            String shpPathPoly = LayerPaths.BLUPPath(path) + "_Y.shp";
            String shpPathLine = LayerPaths.BLUPPath(path) + "_L.shp";

            List<DbfColumn> dbfColumns = new List<DbfColumn>();
            dbfColumns.Add(new DbfColumn("cat", DbfColumnType.Character, 20, 0));
            dbfColumns.Add(new DbfColumn("APL_ID", DbfColumnType.Character, 500, 0));

            ShapeFileFeatureLayer.CreateShapeFile(ShapeFileType.Point, shpPathPoint, dbfColumns, Encoding.Default, OverwriteMode.Overwrite);
            ShapeFileFeatureLayer mapLayerPoint = new ShapeFileFeatureLayer(shpPathPoint, GeoFileReadWriteMode.ReadWrite);
            mapLayerPoint.Name = "BlupPoints";
            mapLayerPoint.Open();
            mapLayerPoint.EditTools.BeginTransaction();

            ShapeFileFeatureLayer.CreateShapeFile(ShapeFileType.Polygon, shpPathPoly, dbfColumns, Encoding.Default, OverwriteMode.Overwrite);
            ShapeFileFeatureLayer mapLayerPoly = new ShapeFileFeatureLayer(shpPathPoly, GeoFileReadWriteMode.ReadWrite);
            mapLayerPoly.Name = "BlupPoly";
            mapLayerPoly.Open();
            mapLayerPoly.EditTools.BeginTransaction();

            ShapeFileFeatureLayer.CreateShapeFile(ShapeFileType.Polyline, shpPathLine, dbfColumns, Encoding.Default, OverwriteMode.Overwrite);
            ShapeFileFeatureLayer mapLayerLine = new ShapeFileFeatureLayer(shpPathLine, GeoFileReadWriteMode.ReadWrite);
            mapLayerLine.Name = "BlupLine";
            mapLayerLine.Open();
            mapLayerLine.EditTools.BeginTransaction();


            foreach (DataRow dr in dt.Rows)
            {
                Feature feature = new Feature(dr["GeomData"].ToString());
                feature.ColumnValues.Add("cat", dr["cat"].ToString());
                feature.ColumnValues.Add("APL_ID", dr["APL_ID"].ToString());

                switch (feature.GetWellKnownType())
                {
                    case WellKnownType.Point:
                        mapLayerPoint.EditTools.Add(feature);
                        break;
                    case WellKnownType.Multipoint:
                        mapLayerPoint.EditTools.Add(feature);
                        break;

                    case WellKnownType.Polygon:
                        mapLayerPoly.EditTools.Add(feature);
                        break;

                    case WellKnownType.Multipolygon:
                        mapLayerPoly.EditTools.Add(feature);
                        break;

                    case WellKnownType.Line:
                        mapLayerLine.EditTools.Add(feature);
                        break;

                    case WellKnownType.Multiline:
                        mapLayerLine.EditTools.Add(feature);
                        break;
                }
            }
            mapLayerPoint.EditTools.CommitTransaction();
            mapLayerPoint.Close();

            mapLayerPoly.EditTools.CommitTransaction();
            mapLayerPoly.Close();

            mapLayerLine.EditTools.CommitTransaction();
            mapLayerLine.Close();

            return mapLayerPoint;
        }

        class BlupAreaStyle : AreaStyle
        {
            public BlupAreaStyle(GeoPen outlinePen) : base(outlinePen) { }

            protected override Collection<string> GetRequiredColumnNamesCore()
            {
                return new Collection<string>() { "iColor" };
            }
            protected override void DrawCore(IEnumerable<Feature> features, GeoCanvas canvas, Collection<SimpleCandidate> labelsInThisLayer, Collection<SimpleCandidate> labelsInAllLayers)
            {
                foreach (Feature feature in features)
                {
                    Color c = Color.DarkRed;
                    int colorCode = c.ToArgb();
                    if (int.TryParse(feature.ColumnValues["iColor"], out colorCode))
                        c = Color.FromArgb(colorCode);
                    string hexColor = "#" + colorCode.ToString("X2").Substring(2);

                    GeoColor color = GeoColor.FromHtml(hexColor);
                    color.AlphaComponent = 125;

                    canvas.DrawArea(feature, new GeoPen(GeoColor.FromHtml(hexColor), base.OutlinePen.Width), new GeoSolidBrush(color), DrawingLevel.LevelOne);
                }
            }
        }
        #endregion

        #region FixedAssets
        public List<String> GetAllFeatureIDs(String sPath)
        {
            ShapeFileFeatureSource source = new ShapeFileFeatureSource(sPath);
            ShapeFileFeatureLayer.BuildIndexFile(sPath, BuildIndexMode.DoNotRebuild);
            source.Open();
            Collection<String> featureIds = source.GetFeatureIds();
            source.Close();

            return featureIds.ToList();
        }
        public List<KeyValuePair<string, string>> GetAllFeaturesByID(String sPath, String sFeatureID)
        {
            List<KeyValuePair<string, string>> lFeatures = new List<KeyValuePair<string, string>>();

            ShapeFileFeatureSource source = new ShapeFileFeatureSource(sPath);
            source.Encoding = Encoding.UTF8;
            source.Open();
            Collection<FeatureSourceColumn> cols = source.GetColumns();
            List<String> lCols = new List<string>();
            foreach (FeatureSourceColumn f in cols)
                lCols.Add(f.ColumnName);

            Feature feature = source.GetFeatureById(sFeatureID, lCols);
            source.Close();

            foreach (KeyValuePair<string, string> v in feature.ColumnValues)
                lFeatures.Add(v);

            KeyValuePair<String, String> k = new KeyValuePair<string, string>("GeomData", feature.ToString());
            lFeatures.Add(k);

            return lFeatures;
        }

        public String CreateFAWithinDistanceInMeters(BlupCreation bc)
        {
            String sError = String.Empty;
            String boundingBox = String.Empty;
            try
            {
                if (!(System.IO.Directory.Exists(LayerPaths.FixedAssetsPath(String.Empty))))
                    System.IO.Directory.CreateDirectory(LayerPaths.FixedAssetsPath(String.Empty));

                string[] filePaths = Directory.GetFiles(LayerPaths.FixedAssetsPath(String.Empty), "*.*");
                foreach (String file in filePaths)
                {
                    try
                    {
                        FileInfo fi = new FileInfo(file);
                        if (fi.LastAccessTime < DateTime.Now.AddHours(-1))
                            fi.Delete();
                    }
                    catch { }
                }

                bc.BlupPath = Guid.NewGuid().ToString();
                String shpPathPoint = LayerPaths.FixedAssetsPath(bc.BlupPath) + "_FAPoint.shp";
                String shpPathLine = LayerPaths.FixedAssetsPath(bc.BlupPath) + "_FALine.shp";
                String shpPathPoly = LayerPaths.FixedAssetsPath(bc.BlupPath) + "_FAPoly.shp";

                List<DbfColumn> dbfColumns = new List<DbfColumn>();
                dbfColumns.Add(new DbfColumn("Code", DbfColumnType.Character, 500, 0));
                dbfColumns.Add(new DbfColumn("Type", DbfColumnType.Character, 50, 0));
                dbfColumns.Add(new DbfColumn("Category", DbfColumnType.Character, 50, 0));
                dbfColumns.Add(new DbfColumn("Family", DbfColumnType.Character, 50, 0));
                ShapeFileFeatureLayer.CreateShapeFile(ShapeFileType.Point, shpPathPoint, dbfColumns, Encoding.Default, OverwriteMode.Overwrite);
                ShapeFileFeatureLayer.CreateShapeFile(ShapeFileType.Polyline, shpPathLine, dbfColumns, Encoding.Default, OverwriteMode.Overwrite);
                ShapeFileFeatureLayer.CreateShapeFile(ShapeFileType.Polygon, shpPathPoly, dbfColumns, Encoding.Default, OverwriteMode.Overwrite);
                ShapeFileFeatureLayer mapLayerPoint = new ShapeFileFeatureLayer(shpPathPoint, GeoFileReadWriteMode.ReadWrite);
                ShapeFileFeatureLayer mapLayerLine = new ShapeFileFeatureLayer(shpPathLine, GeoFileReadWriteMode.ReadWrite);
                ShapeFileFeatureLayer mapLayerPoly = new ShapeFileFeatureLayer(shpPathPoly, GeoFileReadWriteMode.ReadWrite);
                mapLayerPoint.Name = "FixedAssets";
                mapLayerPoint.Open();
                mapLayerPoint.EditTools.BeginTransaction();

                mapLayerLine.Name = "FixedAssets";
                mapLayerLine.Open();
                mapLayerLine.EditTools.BeginTransaction();

                mapLayerPoly.Name = "FixedAssets";
                mapLayerPoly.Open();
                mapLayerPoly.EditTools.BeginTransaction();

                List<Feature> lUnknown = new List<Feature>();
                foreach (DataRow dr in bc.dtData.Rows)
                {
                    Feature feature = new Feature(dr["GeomData"].ToString());
                    feature.ColumnValues.Add("Code", dr["AssetCode"].ToString());
                    feature.ColumnValues.Add("Type", dr["AssetTypeDesc"].ToString());
                    feature.ColumnValues.Add("Category", dr["AssetCategoryDesc"].ToString());
                    feature.ColumnValues.Add("Family", dr["AssetFamily"].ToString());
                    if (feature.GetWellKnownBinary() != null)
                    {
                        switch (feature.GetWellKnownType())
                        {
                            case WellKnownType.Point:
                                mapLayerPoint.EditTools.Add(feature);
                                break;

                            case WellKnownType.Multipoint:
                                mapLayerPoint.EditTools.Add(feature);
                                break;

                            case WellKnownType.Line:
                                mapLayerLine.EditTools.Add(feature);
                                break;

                            case WellKnownType.Multiline:
                                mapLayerLine.EditTools.Add(feature);
                                break;

                            case WellKnownType.Polygon:
                                mapLayerPoly.EditTools.Add(feature);
                                break;

                            case WellKnownType.Multipolygon:
                                mapLayerPoly.EditTools.Add(feature);
                                break;

                            default:
                                lUnknown.Add(feature);
                                break;
                        }
                    }
                }
                mapLayerPoint.EditTools.CommitTransaction();
                mapLayerLine.EditTools.CommitTransaction();
                mapLayerPoly.EditTools.CommitTransaction();

                RectangleShape bb = new RectangleShape();
                String newRec = new RectangleShape().GetBoundingBox().ToString();
                RectangleShape tmp = mapLayerPoint.GetBoundingBox();
                if (tmp.GetBoundingBox().ToString() != newRec)
                {
                    bb.Union(tmp);
                    if (bb.GetBoundingBox().ToString() == newRec)
                        bb = tmp;
                }
                tmp = mapLayerLine.GetBoundingBox();
                if (tmp.GetBoundingBox().ToString() != newRec)
                {
                    bb.Union(tmp);
                    if (bb.GetBoundingBox().ToString() == newRec)
                        bb = tmp;
                }
                tmp = mapLayerPoly.GetBoundingBox();
                if (tmp.GetBoundingBox().ToString() != newRec)
                {
                    bb.Union(tmp);
                    if (bb.GetBoundingBox().ToString() == newRec)
                        bb = tmp;
                }

                boundingBox = "," + bb;

                mapLayerPoint.Close();
                mapLayerLine.Close();
                mapLayerPoly.Close();
            }
            catch (Exception ex)
            {
                sError += ex.ToString();
            }
            return bc.BlupPath + boundingBox + sError;
        }
        public LayerOverlay ShowFALayers(String path)
        {
            LayerOverlay layerOverlay = new LayerOverlay();
            String[] filePaths = Directory.GetFiles(LayerPaths.FixedAssetsPath(String.Empty), path + "*.shp");
            if (filePaths.Length == 0)
                return layerOverlay;

            Proj4Projection proj4 = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetSphericalMercatorParametersString());
            proj4 = new Proj4Projection(4326, 3857);

            foreach (String file in filePaths)
            {
                ShapeFileFeatureLayer shp = new ShapeFileFeatureLayer(file);

                Collection<ValueItem> cValueItems = new Collection<ValueItem>();
                String sColName = "Category";
                CustomValueStyle cvs = new CustomValueStyle(sColName, cValueItems);

                String FamilyName = "Family";
                Collection<ValueItem> FamilyItems = new Collection<ValueItem>();
                CustomBrushAreaStyle cbas = new CustomBrushAreaStyle(FamilyName, FamilyItems);
                Boolean ZoomLevelToSet = true;

                GeoPen geoPenDot = new GeoPen();
                geoPenDot = new GeoPen(GeoColors.Red, 4.5F);
                geoPenDot.DashStyle = LineDashStyle.Dot;

                //if(shp.FeatureSource.GetAllFeatures(ReturningColumnsType.AllColumns).FirstOrDefault().GetShape().GetWellKnownType()==WellKnownType.Line)
                if (file.EndsWith("Point.shp"))
                {
                    cValueItems.Add(new ValueItem("COMPLETED", new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColors.Blue), new GeoPen(GeoColors.DarkGray, 1), 10)));
                    cValueItems.Add(new ValueItem("SCHEDULED", new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(geoPenDot.Color), geoPenDot, 10)));
                    cValueItems.Add(new ValueItem("OVERDUE", new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColors.Red), new GeoPen(GeoColors.DarkGray, 1), 10)));

                    cValueItems.Add(new ValueItem("Sodium", new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColors.Red), new GeoPen(GeoColors.DarkGray, 1), 10)));
                    cValueItems.Add(new ValueItem("CPL", new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColors.Blue), new GeoPen(GeoColors.DarkGray, 1), 10)));
                    cValueItems.Add(new ValueItem("LED", new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColors.Yellow), new GeoPen(GeoColors.DarkGray, 1), 10)));
                    cValueItems.Add(new ValueItem("NA", new PointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColors.Green), new GeoPen(GeoColors.DarkGray, 1), 10)));

                    cValueItems.Add(new ValueItem("Convex Mirror", new PointStyle(PointSymbolType.Diamond, new GeoSolidBrush(GeoColors.Blue), new GeoPen(GeoColors.DarkGray, 1), 10)));
                    cValueItems.Add(new ValueItem("Street Name Plate", new PointStyle(PointSymbolType.Diamond, new GeoSolidBrush(GeoColors.Yellow), new GeoPen(GeoColors.DarkGray, 1), 10)));

                    cvs.otherStyle = new PointStyle(PointSymbolType.Triangle, new GeoSolidBrush(GeoColors.Yellow), new GeoPen(GeoColors.Red, 3), 10);
                }
                else if (file.EndsWith("Line.shp"))
                {
                    cValueItems.Add(new ValueItem("COMPLETED", new LineStyle(new GeoPen(GeoColors.Blue, 4.5F))));
                    cValueItems.Add(new ValueItem("SCHEDULED", new LineStyle(geoPenDot)));
                    cValueItems.Add(new ValueItem("OVERDUE", new LineStyle(new GeoPen(GeoColors.Red, 4.5F))));

                    cValueItems.Add(new ValueItem("Proposed drain (not funded)", new LineStyle(new GeoPen(GeoColors.Orange, 4.5F))));
                    cValueItems.Add(new ValueItem("Approved drain", new LineStyle(new GeoPen(GeoColors.Green, 4.5F))));
                    cValueItems.Add(new ValueItem("Upgrading of drain", new LineStyle(new GeoPen(GeoColor.FromHtml("#8a4a19"), 4.5F))));
                    cValueItems.Add(new ValueItem("Construction of drain", new LineStyle(new GeoPen(GeoColors.Magenta, 4.5F))));
                    cValueItems.Add(new ValueItem("Open", new LineStyle(new GeoPen(GeoColor.FromHtml("#40ff00"), 4.5F))));// 
                    cValueItems.Add(new ValueItem("Covered", new LineStyle(new GeoPen(GeoColor.FromHtml("#f1ff57"), 4.5F))));
                    cValueItems.Add(new ValueItem("River/Canal", new LineStyle(new GeoPen(GeoColor.FromHtml("#0040ff"), 4.5F))));

                    cvs.otherStyle = new LineStyle(new GeoPen(GeoColors.DarkViolet, 4.5F));
                }
                else if (file.EndsWith("Poly.shp"))
                {
                    cValueItems.Add(new ValueItem("COMPLETED", new AreaStyle(GeoPens.Blue, new GeoSolidBrush(new GeoColor(75, GeoColors.LightBlue)))));
                    cValueItems.Add(new ValueItem("SCHEDULED", new AreaStyle(geoPenDot, new GeoSolidBrush(new GeoColor(75, GeoColors.LightRed)))));
                    cValueItems.Add(new ValueItem("OVERDUE", new AreaStyle(GeoPens.Red, new GeoSolidBrush(new GeoColor(75, GeoColors.LightRed)))));

                    cValueItems.Add(new ValueItem("Known Owners", new AreaStyle(GeoPens.DarkGreen, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#30965e"))))));
                    cValueItems.Add(new ValueItem("Unknown Owners", new AreaStyle(GeoPens.DarkRed, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#876129"))))));
                    cValueItems.Add(new ValueItem("Partially Known", new AreaStyle(GeoPens.DarkOrange, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#fa9907"))))));
                    cValueItems.Add(new ValueItem("CCC-DCM (Existing GS)", new AreaStyle(GeoPens.Green, new GeoSolidBrush(new GeoColor(75, GeoColors.Chartreuse)))));
                    cValueItems.Add(new ValueItem("CCC-DCM (Existing ES)", new AreaStyle(GeoPens.DarkOrange, new GeoSolidBrush(new GeoColor(75, GeoColors.Chartreuse)))));
                    cValueItems.Add(new ValueItem("CCC-MOE (Existing GS)", new AreaStyle(GeoPens.DarkOrange, new GeoSolidBrush(new GeoColor(75, GeoColors.Chartreuse)))));
                    cValueItems.Add(new ValueItem("CCC-MOE (Existing ES)", new AreaStyle(GeoPens.DarkOrange, new GeoSolidBrush(new GeoColor(75, GeoColors.Chartreuse)))));
                    cValueItems.Add(new ValueItem("CCC-ENL (Existing GS)", new AreaStyle(GeoPens.DarkOrange, new GeoSolidBrush(new GeoColor(75, GeoColors.Chartreuse)))));
                    cValueItems.Add(new ValueItem("CCC-ENL (Existing ES)", new AreaStyle(GeoPens.DarkOrange, new GeoSolidBrush(new GeoColor(75, GeoColors.Chartreuse)))));
                    cValueItems.Add(new ValueItem("CCC-ALTEO (Existing GS)", new AreaStyle(GeoPens.DarkOrange, new GeoSolidBrush(new GeoColor(75, GeoColors.Chartreuse)))));
                    cValueItems.Add(new ValueItem("CCC-DCM (Proposed GS)", new AreaStyle(GeoPens.DarkOrange, new GeoSolidBrush(new GeoColor(75, GeoColors.OrangeRed)))));
                    cValueItems.Add(new ValueItem("CCC-DCM (Proposed ES)", new AreaStyle(GeoPens.DarkOrange, new GeoSolidBrush(new GeoColor(75, GeoColors.OrangeRed)))));
                    cValueItems.Add(new ValueItem("CCC-MOE (Proposed GS)", new AreaStyle(GeoPens.DarkOrange, new GeoSolidBrush(new GeoColor(75, GeoColors.OrangeRed)))));
                    cValueItems.Add(new ValueItem("CCC-ENL (Proposed ES)", new AreaStyle(GeoPens.DarkOrange, new GeoSolidBrush(new GeoColor(75, GeoColors.OrangeRed)))));
                    cValueItems.Add(new ValueItem("CCC-ALTEO (Proposed GS)", new AreaStyle(GeoPens.DarkOrange, new GeoSolidBrush(new GeoColor(75, GeoColors.OrangeRed)))));
                    cValueItems.Add(new ValueItem("CCC-Forestry Dept (Proposed GS)", new AreaStyle(GeoPens.DarkOrange, new GeoSolidBrush(new GeoColor(75, GeoColors.OrangeRed)))));
                    cValueItems.Add(new ValueItem("CCC-ENL (Proposed GS)", new AreaStyle(GeoPens.DarkOrange, new GeoSolidBrush(new GeoColor(75, GeoColors.OrangeRed)))));
                    cValueItems.Add(new ValueItem("CCC-ENL (Proposed ES)", new AreaStyle(GeoPens.DarkOrange, new GeoSolidBrush(new GeoColor(75, GeoColors.OrangeRed)))));
                    cValueItems.Add(new ValueItem("CCC-ALTEO (Proposed GS)", new AreaStyle(GeoPens.DarkOrange, new GeoSolidBrush(new GeoColor(75, GeoColors.OrangeRed)))));

                    cValueItems.Add(new ValueItem("Village Hall", new AreaStyle(GeoPens.Yellow, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#fc0303"))))));
                    cValueItems.Add(new ValueItem("Sub Hall", new AreaStyle(GeoPens.Orange, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#8c1f1f"))))));
                    cValueItems.Add(new ValueItem("Vestiaire", new AreaStyle(GeoPens.Green, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#7472a3"))))));
                    cValueItems.Add(new ValueItem("Cemetery Office", new AreaStyle(GeoPens.Black, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#fca2a2"))))));
                    cValueItems.Add(new ValueItem("Multipurpose hall", new AreaStyle(GeoPens.Blue, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#a17f7f"))))));
                    cValueItems.Add(new ValueItem("Recreational Center", new AreaStyle(GeoPens.DarkRed, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#807a63"))))));
                    cValueItems.Add(new ValueItem("Market", new AreaStyle(GeoPens.Olive, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#b39215"))))));
                    cValueItems.Add(new ValueItem("Social Hall", new AreaStyle(GeoPens.Violet, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#ffcd12"))))));
                    cValueItems.Add(new ValueItem("Janaza Platform", new AreaStyle(GeoPens.DarkGreen, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#64ff17"))))));
                    cValueItems.Add(new ValueItem("Recreational Centre", new AreaStyle(GeoPens.Fuchsia, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#30965e"))))));
                    cValueItems.Add(new ValueItem("Dubreuil stadium vestiare", new AreaStyle(GeoPens.Firebrick, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#30965e"))))));
                    cValueItems.Add(new ValueItem("GYM", new AreaStyle(GeoPens.DarkSalmon, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#409914"))))));
                    cValueItems.Add(new ValueItem("Public Toilet", new AreaStyle(GeoPens.DarkOrange, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#868c82"))))));
                    cValueItems.Add(new ValueItem("Village Council", new AreaStyle(GeoPens.DarkKhaki, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#39e3fa"))))));
                    cValueItems.Add(new ValueItem("Health Track", new AreaStyle(GeoPens.GreenYellow, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#047e8f"))))));
                    cValueItems.Add(new ValueItem("Gradin", new AreaStyle(GeoPens.DarkGray, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#0f08d1"))))));
                    cValueItems.Add(new ValueItem("Football Ground", new AreaStyle(GeoPens.DarkOliveGreen, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#30965e"))))));

                    cValueItems.Add(new ValueItem("Mini Soccer", new AreaStyle(GeoPens.AliceBlue, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#30965e"))))));
                    cValueItems.Add(new ValueItem("Children Garden", new AreaStyle(GeoPens.Beige, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#1f7d5c"))))));
                    cValueItems.Add(new ValueItem("Football Ground", new AreaStyle(GeoPens.Bisque, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#888f8c"))))));
                    cValueItems.Add(new ValueItem("Basketball", new AreaStyle(GeoPens.BlueViolet, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#00c3ff"))))));
                    cValueItems.Add(new ValueItem("Volleyball Pitch", new AreaStyle(GeoPens.BrightRed, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#016280"))))));
                    cValueItems.Add(new ValueItem("HealthTrack", new AreaStyle(GeoPens.BrightYellow, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#6219ff"))))));
                    cValueItems.Add(new ValueItem("Kiosk", new AreaStyle(GeoPens.Brown, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#2b0a73"))))));
                    cValueItems.Add(new ValueItem("Leisure Park", new AreaStyle(GeoPens.BurlyWood, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#b665bf"))))));
                    cValueItems.Add(new ValueItem("Tennis", new AreaStyle(GeoPens.Chartreuse, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#cda9d1"))))));
                    cValueItems.Add(new ValueItem("Parking Area", new AreaStyle(GeoPens.Chocolate, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#9700a8"))))));
                    cValueItems.Add(new ValueItem("stadium tribunes", new AreaStyle(GeoPens.DeepPink, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#6e2951"))))));
                    cValueItems.Add(new ValueItem("Parking", new AreaStyle(GeoPens.HoneyDew, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#30965e"))))));
                    cValueItems.Add(new ValueItem("OUTDOOR GYM", new AreaStyle(GeoPens.IndianRed, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#9e005c"))))));
                    cValueItems.Add(new ValueItem("Petanque", new AreaStyle(GeoPens.Lavender, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#917987"))))));
                    cValueItems.Add(new ValueItem("Garden-Space", new AreaStyle(GeoPens.Linen, new GeoSolidBrush(new GeoColor(255, GeoColor.FromHtml("#00ad45"))))));

                    AreaStyle other = new AreaStyle(GeoPens.DarkGreen, new GeoSolidBrush(GeoColors.Yellow));
                    other.Advanced.FillCustomBrush = new GeoTextureBrush(new GeoImage(LayerPaths.iconsPath("BlupComm.png")), GeoWrapMode.Tile);
                    cvs.otherStyle = other;

                    #region AssetFamily
                    AreaStyle arg = new AreaStyle(new GeoPen(GeoColors.Transparent), new GeoSolidBrush(GeoColors.Transparent));
                    arg.Advanced.FillCustomBrush = new GeoTextureBrush(new GeoImage(LayerPaths.iconsPath("G.png")), GeoWrapMode.Tile);
                    FamilyItems.Add(new ValueItem("Government", arg));

                    AreaStyle arp = new AreaStyle(new GeoPen(GeoColors.Transparent), new GeoSolidBrush(GeoColors.Transparent));
                    arp.Advanced.FillCustomBrush = new GeoTextureBrush(new GeoImage(LayerPaths.iconsPath("P.png")), GeoWrapMode.Tile);
                    FamilyItems.Add(new ValueItem("Private", arp));

                    AreaStyle arc = new AreaStyle(new GeoPen(GeoColors.Transparent), new GeoSolidBrush(GeoColors.Transparent));
                    arc.Advanced.FillCustomBrush = new GeoTextureBrush(new GeoImage(LayerPaths.iconsPath("C.png")), GeoWrapMode.Tile);
                    FamilyItems.Add(new ValueItem("Council", arc));
                    #endregion
                }
                else if (file.EndsWith("District.shp"))
                {
                    ZoomLevelToSet = false;
                    ZoomLevelSet zls = new CustomZoomLevelSet();
                    var zl = zls.GetZoomLevels();
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("name", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = new CustomAreaStyle(GeoPens.Black);
                    }
                    shp.ZoomLevelSet = zls;
                }
                else if (file.EndsWith("VCA.shp"))
                {
                    ZoomLevelToSet = false;
                    ZoomLevelSet zls = new CustomZoomLevelSet();
                    var zl = zls.GetZoomLevels();
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("name", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultAreaStyle = new CustomAreaStyle(GeoPens.Black);
                    }
                    shp.ZoomLevelSet = zls;
                }
                else if (file.EndsWith("Road.shp"))
                {
                    ZoomLevelToSet = false;
                    ZoomLevelSet zls = new CustomZoomLevelSet();
                    var zl = zls.GetZoomLevels();
                    int iZoom = 0;
                    foreach (var zoomLevel in zl)
                    {
                        zoomLevel.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("name", "Aerial", 7, DrawingFontStyles.Italic, GeoColors.Black);
                        zoomLevel.DefaultLineStyle = new RoadLineStyle(GeoPens.AliceBlue, iZoom);
                        iZoom++;
                    }
                    shp.ZoomLevelSet = zls;
                }

                if (ZoomLevelToSet)
                {
                    ZoomLevelSet zoomLevelSet = new CustomZoomLevelSet();
                    var zoomLevels = zoomLevelSet.GetZoomLevels();
                    foreach (var zoomLevel in zoomLevels)
                    {
                        //if (file.EndsWith("Poly.shp"))
                        //{
                        //    if (zoomLevel.Scale > 40000)
                        //        zoomLevel.CustomStyles.Add(GetClusterPointStyle());
                        //    else
                        //    {
                        //        zoomLevel.CustomStyles.Add(cvs);
                        //        zoomLevel.CustomStyles.Add(cbas);
                        //    }
                        //}
                        //else
                        //{
                        //if (zoomLevel.Scale > 40000)
                        zoomLevel.CustomStyles.Add(GetClusterPointStyle());

                        zoomLevel.CustomStyles.Add(cvs);
                        zoomLevel.CustomStyles.Add(cbas);
                        if (file.EndsWith("Line.shp"))
                            if (zoomLevel.Scale < 18100)
                                zoomLevel.CustomStyles.Add(TextStyles.CreateSimpleTextStyle("Code", "Aerial", 12, DrawingFontStyles.Bold, GeoColors.Black, 5, 5));
                        //}
                    }
                    shp.ZoomLevelSet = zoomLevelSet;
                }
                shp.FeatureSource.Projection = proj4;
                layerOverlay.Layers.Add(shp);
            }

            return layerOverlay;
        }

        public class CustomValueStyle : ValueStyle
        {
            public CustomValueStyle() : base()
            { }

            public CustomValueStyle(string columnName, Collection<ValueItem> valueItems) : base(columnName, valueItems)
            { }

            public Style otherStyle { get; set; }

            protected override void DrawCore(IEnumerable<Feature> features, GeoCanvas canvas, Collection<SimpleCandidate> labelsInThisLayer, Collection<SimpleCandidate> labelsInAllLayers)
            {
                base.DrawCore(features, canvas, labelsInThisLayer, labelsInAllLayers);

                Collection<string> exsitItems = new Collection<string>();
                foreach (var item in ValueItems)
                {
                    if (!exsitItems.Contains(item.Value))
                        exsitItems.Add(item.Value);
                }

                foreach (var feature in features)
                {
                    string fieldValue = feature.ColumnValues[ColumnName].Trim();
                    if (!exsitItems.Contains(fieldValue))
                    {
                        if (otherStyle != null)
                            otherStyle.Draw(new Feature[] { feature }, canvas, labelsInThisLayer, labelsInAllLayers);
                    }
                }
            }
        }

        public class CustomBrushAreaStyle : ValueStyle
        {
            public CustomBrushAreaStyle() : base()
            { }

            public CustomBrushAreaStyle(string columnName, Collection<ValueItem> valueItems) : base(columnName, valueItems)
            { }

            protected override void DrawCore(IEnumerable<Feature> features, GeoCanvas canvas, Collection<SimpleCandidate> labelsInThisLayer, Collection<SimpleCandidate> labelsInAllLayers)
            {
                base.DrawCore(features, canvas, labelsInThisLayer, labelsInAllLayers);
            }
        }
        #endregion

        #region RoadNetwork
        public String CreateRoadNetwork(BlupCreation bc)
        {
            String sError = String.Empty;
            String boundingBox = String.Empty;
            try
            {
                if (!(System.IO.Directory.Exists(LayerPaths.FixedAssetsPath(String.Empty))))
                    System.IO.Directory.CreateDirectory(LayerPaths.FixedAssetsPath(String.Empty));

                string[] filePaths = Directory.GetFiles(LayerPaths.FixedAssetsPath(String.Empty), "*.*");
                foreach (String file in filePaths)
                {
                    try
                    {
                        FileInfo fi = new FileInfo(file);
                        if (fi.LastAccessTime < DateTime.Now.AddHours(-1))
                            fi.Delete();
                    }
                    catch { }
                }

                bc.BlupPath = Guid.NewGuid().ToString();
                foreach (DataTable dt in bc.ds.Tables)
                {
                    String sTableName = dt.TableName;
                    String shpPath = LayerPaths.FixedAssetsPath(bc.BlupPath) + "_" + sTableName + ".shp";

                    List<DbfColumn> dbfColumns = new List<DbfColumn>();
                    dbfColumns.Add(new DbfColumn("id", DbfColumnType.Numeric, 10, 0));
                    dbfColumns.Add(new DbfColumn("zonecolor", DbfColumnType.Numeric, 15, 0));
                    dbfColumns.Add(new DbfColumn("name", DbfColumnType.Character, 500, 0));
                    dbfColumns.Add(new DbfColumn("roadtype", DbfColumnType.Character, 500, 0));

                    switch (sTableName)
                    {
                        case ("dtDistrict"):
                            ShapeFileFeatureLayer.CreateShapeFile(ShapeFileType.Polygon, shpPath, dbfColumns, Encoding.Default, OverwriteMode.Overwrite);
                            break;

                        case ("dtVCA"):
                            ShapeFileFeatureLayer.CreateShapeFile(ShapeFileType.Polygon, shpPath, dbfColumns, Encoding.Default, OverwriteMode.Overwrite);
                            break;

                        case ("dtRoad"):
                            ShapeFileFeatureLayer.CreateShapeFile(ShapeFileType.Polyline, shpPath, dbfColumns, Encoding.Default, OverwriteMode.Overwrite);
                            break;
                    }
                    ShapeFileFeatureLayer mapLayer = new ShapeFileFeatureLayer(shpPath, GeoFileReadWriteMode.ReadWrite);
                    mapLayer.Name = sTableName;
                    mapLayer.Open();
                    mapLayer.EditTools.BeginTransaction();

                    switch (sTableName)
                    {
                        case ("dtDistrict"):
                            foreach (DataRow dr in dt.Rows)
                            {
                                Feature feature = new Feature(dr["GeomData"].ToString());
                                feature.ColumnValues.Add("id", dr["DistrictID"].ToString());
                                feature.ColumnValues.Add("zonecolor", dr["iColor"].ToString());
                                feature.ColumnValues.Add("name", dr["DistrictName"].ToString());
                                mapLayer.EditTools.Add(feature);
                            }
                            break;

                        case ("dtVCA"):
                            foreach (DataRow dr in dt.Rows)
                            {
                                Feature feature = new Feature(dr["GeomData"].ToString());
                                feature.ColumnValues.Add("id", dr["VCAID"].ToString());
                                feature.ColumnValues.Add("zonecolor", dr["iColor"].ToString());
                                feature.ColumnValues.Add("name", dr["VCAName"].ToString());
                                mapLayer.EditTools.Add(feature);
                            }
                            break;

                        case ("dtRoad"):
                            foreach (DataRow dr in dt.Rows)
                            {
                                Feature feature = new Feature(dr["GeomData"].ToString());
                                feature.ColumnValues.Add("id", dr["RoadID"].ToString());
                                feature.ColumnValues.Add("zonecolor", "0");
                                feature.ColumnValues.Add("name", dr["RoadName"].ToString());
                                feature.ColumnValues.Add("roadtype", dr["RoadType"].ToString());
                                mapLayer.EditTools.Add(feature);
                            }
                            break;
                    }

                    mapLayer.EditTools.CommitTransaction();

                    RectangleShape bb = new RectangleShape();
                    String newRec = new RectangleShape().GetBoundingBox().ToString();
                    RectangleShape tmp = mapLayer.GetBoundingBox();
                    if (tmp.GetBoundingBox().ToString() != newRec)
                    {
                        bb.Union(tmp);
                        if (bb.GetBoundingBox().ToString() == newRec)
                            bb = tmp;
                    }
                    boundingBox = "," + bb;

                    mapLayer.Close();
                }
            }
            catch (Exception ex)
            {
                sError += ex.ToString();
            }
            return bc.BlupPath + boundingBox + sError;
        }
        #endregion

        #region QryPolygons
        public void test()
        {
            BuildIndexFromDir(@"C:\Reza\Geo\Maps\Creations\ROAD\Tmp");
            QryAllFeaturesFromShp(1, 2, 4, @"C:\Reza\Geo\Maps\Creations\ROAD\Tmp\SettlementBoundary.shp");

            //Esperance
            MultipolygonShape esperance = new MultipolygonShape("MULTIPOLYGON(((57.593017578125 -20.2274780273438,57.5892944335938 -20.2288208007813,57.5859985351563 -20.2265014648438,57.586181640625 -20.2208862304688,57.5903930664063 -20.2203369140625,57.5941162109375 -20.2239379882813,57.593017578125 -20.2274780273438)))");

            //Mapou
            MultipolygonShape mapou = new MultipolygonShape("MULTIPOLYGON(((57.6071166992188 -20.0836181640625,57.60400390625 -20.0859375,57.601806640625 -20.0848999023438,57.6013793945313 -20.0811767578125,57.6033325195313 -20.077880859375,57.6057739257813 -20.075439453125,57.6088256835938 -20.0748901367188,57.6097412109375 -20.078125,57.608642578125 -20.0799560546875,57.6071166992188 -20.0836181640625)))");

            JObject result = QryPolygonShp(mapou, 50, "C:\\Reza\\Geo\\Maps\\Creations\\ROAD\\SHAPE\\MauritiusLocalityVca.shp", 5);

            DataTable dtEsperance = QryPolygonInside(esperance, @"C:\Reza\Geo\Maps\Creations\ROAD\SHAPE\DistrictMoka.shp");
            DataTable dtMapou = QryPolygonInside(mapou, @"C:\Reza\Geo\Maps\Creations\ROAD\SHAPE\DistrictMoka.shp");

            DataTable dtEsperanceDistance = QryPolygonDistance(esperance, 9000, @"C:\Reza\Geo\Maps\Creations\ROAD\SHAPE\DistrictMoka.shp");
            DataTable dtMapouDistance = QryPolygonDistance(mapou, 9000, @"C:\Reza\Geo\Maps\Creations\ROAD\SHAPE\DistrictMoka.shp");

            DataTable dtEsperanceNearRoad = QryPolygonNeatestTo(esperance, 1, @"C:\Reza\Geo\Maps\Creations\ROAD\SHAPE\Esperance_Road_Network.shp");

            //SettlementBoundary
            MultipolygonShape SettlementBoundary = new MultipolygonShape("MULTIPOLYGON(((57.4967086335715 -20.2399600453259,57.496101871015 -20.2395317423449,57.4958520276093 -20.2393532827694,57.4956021842038 -20.2392462070242,57.4951381893076 -20.2395674342599,57.4949597297321 -20.2401741968164,57.4947812701566 -20.2407095755427,57.4945671186661 -20.2413163380992,57.4944600429209 -20.2417089491651,57.4941388156851 -20.242065868316,57.4934249773834 -20.2423157117217,57.4930680582325 -20.2424227874669,57.4925326795062 -20.2425655551273,57.4894988667238 -20.242458479382,57.4799691253954 -20.2423514036367,57.4798977415652 -20.242065868316,57.4796835900747 -20.241637565335,57.4794337466691 -20.2414591057596,57.4787556002825 -20.2414234138445,57.4781845296411 -20.2415661815048,57.4777919185751 -20.2416732572501,57.4773636155941 -20.241637565335,57.4771494641034 -20.2414947976746,57.4769710045281 -20.2413877219294,57.4771494641034 -20.2411378785238,57.4774349994241 -20.2409594189484,57.4779346862354 -20.2405668078823,57.4780774538958 -20.2400671210711,57.4796478981597 -20.2378185304206,57.4798263577351 -20.2374259193547,57.4798263577351 -20.2371046921189,57.4797549739049 -20.236747772968,57.4796478981597 -20.2364979295623,57.4795408224144 -20.2360339346662,57.479005443688 -20.2357483993456,57.478577140707 -20.2352844044494,57.4782202215561 -20.2347847176382,57.4780417619808 -20.2342493389119,57.4779703781505 -20.2337853440158,57.4782559134712 -20.2336068844403,57.4786842164522 -20.2333213491196,57.4789340598578 -20.2328573542235,57.4790411356032 -20.2323933593274,57.4789340598578 -20.2320007482614,57.4787556002825 -20.2317152129407,57.4786842164522 -20.2313939857049,57.4786842164522 -20.2311441422992,57.4788269841127 -20.2307872231484,57.4791839032634 -20.2305730716579,57.4794337466691 -20.2302518444221,57.4795408224144 -20.2299663091014,57.4794694385842 -20.2295023142052,57.4793623628389 -20.229002627394,57.4791482113484 -20.2285743244129,57.478969751773 -20.2281460214319,57.4789340598578 -20.2277891022811,57.4788983679427 -20.2271466478095,57.4789340598578 -20.2267183448284,57.4788269841127 -20.2263971175927,57.4789340598578 -20.226147274187,57.4791839032634 -20.2260401984418,57.4796835900747 -20.2258260469512,57.4799334334804 -20.2255762035456,57.4799334334804 -20.2251835924797,57.4798620496501 -20.2247195975835,57.4798620496501 -20.224469754178,57.4799334334804 -20.2242556026874,57.4801118930558 -20.2242556026874,57.4803260445464 -20.224505446093,57.4806115798671 -20.2247552894987,57.4808257313576 -20.2247195975835,57.4810755747631 -20.224505446093,57.4813611100838 -20.224505446093,57.4815752615743 -20.2245768299232,57.4817180292347 -20.2248623652439,57.4816466454045 -20.2251835924797,57.481432493914 -20.2253620520551,57.4812540343386 -20.2255762035456,57.4812540343386 -20.2258617388663,57.4812540343386 -20.2271466478095,57.4813611100838 -20.2274678750452,57.481468185829 -20.2277177184508,57.4816466454045 -20.2279318699414,57.4819321807253 -20.2280032537716,57.4821820241308 -20.2282530971772,57.482217716046 -20.2286457082432,57.4820749483855 -20.2289312435639,57.4819321807253 -20.22939523846,57.4820749483855 -20.2308942988937,57.4823604837063 -20.2315724452803,57.4824675594515 -20.2320364401765,57.4825746351967 -20.2325361269877,57.4825746351967 -20.2331428895442,57.4825032513665 -20.233892419761,57.482646019027 -20.2360696265814,57.4828244786024 -20.2365693133926,57.4829672462627 -20.2369262325435,57.4833598573287 -20.2373188436094,57.4836097007343 -20.2376757627603,57.4839666198852 -20.2381040657414,57.4843592309511 -20.2383182172318,57.4847875339323 -20.2383182172318,57.4853586045735 -20.2383182172318,57.4856084479792 -20.2380326819112,57.4858582913848 -20.2380326819112,57.4861795186207 -20.2381754495715,57.4866435135167 -20.2384609848922,57.4873573518184 -20.2384609848922,57.4878927305448 -20.2385680606375,57.4886779526768 -20.2396745100052,57.4892847152332 -20.2392818989392,57.4885351850163 -20.2381040657414,57.4903911646009 -20.234285030827,57.4907480837518 -20.2340351874214,57.4910336190726 -20.2338567278459,57.4913191543933 -20.2335711925252,57.4914976139686 -20.2331785814593,57.4916760735441 -20.2326075108178,57.4918188412045 -20.232250591667,57.4943172752606 -20.2325361269877,57.4947455782416 -20.2324647431575,57.4953523407981 -20.232286283582,57.495709259949 -20.231857980601,57.4959234114395 -20.2313582937898,57.49613756293 -20.2310013746389,57.4966729416564 -20.230608763573,57.4981363101749 -20.2299306171863,57.4990642999672 -20.2296093899506,57.4991356837973 -20.2291453950544,57.4991713757125 -20.2285743244129,57.4992427595425 -20.2281103295168,57.4994926029482 -20.2279318699414,57.5000279816745 -20.2278604861112,57.5002778250802 -20.2278247941962,57.5004919765707 -20.2276106427056,57.5004919765707 -20.227325107385,57.5001707493349 -20.2271823397246,57.4995639867785 -20.226968188234,57.4990999918823 -20.2267540367436,57.4996353706085 -20.2257903550361,57.5007418199764 -20.2245768299232,57.5008132038064 -20.2230063856593,57.4984575374108 -20.2231491533197,57.4985646131559 -20.2213288656503,57.4992070676275 -20.2215430171408,57.499813830184 -20.2215430171408,57.5002421331652 -20.2213288656503,57.5006704361462 -20.2212217899049,57.5007418199764 -20.2207934869239,57.5005990523159 -20.220436567773,57.5002421331652 -20.2202224162825,57.499849522099 -20.2200796486221,57.4988501484766 -20.2181522852074,57.4983504616654 -20.2184378205282,57.4982076940051 -20.2189018154243,57.4982790778353 -20.2192587345752,57.4986716889013 -20.2196513456411,57.4991356837973 -20.2199725728769,57.4993498352878 -20.2202581081976,57.4979221586844 -20.2203651839429,57.4971369365525 -20.2204722596881,57.4958877195245 -20.2204722596881,57.495316648883 -20.2206150273485,57.4947812701566 -20.2205079516032,57.4941031237701 -20.2199725728769,57.4931394420627 -20.2196156537261,57.492461295676 -20.2192587345752,57.4911050029027 -20.2192587345752,57.4903197807707 -20.2195085779808,57.4888207203371 -20.2197941133014,57.4879998062901 -20.2196513456411,57.4869647407526 -20.2193658103204,57.4861795186207 -20.2193658103204,57.4857512156395 -20.219579961811,57.4853229126585 -20.2196156537261,57.4847875339323 -20.2193301184053,57.4842521552059 -20.2188304315941,57.4832884734985 -20.2184735124433,57.482610327112 -20.2184021286131,57.4839309279701 -20.2172599873303,57.4844306147813 -20.2176169064812,57.4847875339323 -20.2177596741415,57.4852158369133 -20.2179024418019,57.4856084479792 -20.2176525983962,57.4858939832999 -20.2175455226509,57.4866792054319 -20.2175455226509,57.4877142709694 -20.2176525983962,57.489213331403 -20.2173670630755,57.4898200939595 -20.2168673762642,57.4901770131104 -20.2161178460475,57.4906766999217 -20.2157609268966,57.4912120786479 -20.2156538511513,57.4919259169498 -20.2156538511513,57.4922828361005 -20.2158680026418,57.4917831492893 -20.216331997538,57.4916760735441 -20.2168316843492,57.4922114522703 -20.217152911585,57.4933892854682 -20.2173313711605,57.4946385024963 -20.2176525983962,57.4954594165433 -20.2179381337169,57.496530173996 -20.2180452094622,57.4973510880429 -20.2179024418019,57.4976723152789 -20.2176169064812,57.4984218454956 -20.2173670630755,57.4987073808163 -20.2172599873303,57.4979935425146 -20.216367689453,57.499813830184 -20.2154040077457,57.5004919765707 -20.2165818409436,57.5015270421083 -20.2164390732833,57.5023122642401 -20.2161892298777,57.503097486372 -20.2158323107268,57.503454405523 -20.2167959924341,57.5039897842492 -20.2164390732833,57.50427531957 -20.2168673762642,57.5047750063811 -20.2166532247738,57.5053460770226 -20.218009517547,57.5052746931923 -20.2186519720186,57.5051676174471 -20.2192587345752,57.5052746931923 -20.2196156537261,57.5056673042583 -20.2197227294713,57.5062383748998 -20.2199011890468,57.506773753626 -20.220008264792,57.5072734404372 -20.2199368809618,57.5074519000127 -20.2194728860657,57.5074875919277 -20.2189375073394,57.5077731272485 -20.2184378205282,57.5083085059749 -20.2185805881885,57.5085583493804 -20.2188304315941,57.5093792634275 -20.2189018154243,57.5105214047102 -20.2188661235092,57.5118063136534 -20.2187233558489,57.5128413791908 -20.2186519720186,57.5133767579173 -20.218366436698,57.513697985153 -20.2180809013773,57.5139121366435 -20.2181522852074,57.5140192123889 -20.2184735124433,57.5145545911152 -20.2187233558489,57.5155896566527 -20.2190088911696,57.5168031817657 -20.2192944264902,57.5171244090015 -20.2191159669148,57.5171244090015 -20.2186519720186,57.5175170200674 -20.2182236690376,57.5182665502842 -20.2173670630755,57.5188019290106 -20.2160821541323,57.519801302633 -20.2149400128495,57.5211219034912 -20.2140477149724,57.5228351154154 -20.2133338766706,57.5239058728681 -20.2126557302839,57.5247981707452 -20.2133695685857,57.5240843324435 -20.2141190988025,57.5249766303207 -20.2145117098685,57.5256190847922 -20.2145117098685,57.5259403120281 -20.2142261745478,57.5264399988393 -20.2135123362461,57.5265113826695 -20.2129412656046,57.5266898422448 -20.2122274273029,57.5273679886315 -20.2119062000672,57.5277962916125 -20.2116920485766,57.5278319835277 -20.2120489677275,57.5282245945937 -20.2125843464538,57.5289741248104 -20.2130126494349,57.5292596601312 -20.2134409524159,57.5272252209713 -20.2158323107268,57.5337925333473 -20.2204722596881,57.5357912805922 -20.2181879771225,57.5354700533564 -20.218009517547,57.5356485129319 -20.2176525983962,57.5343636039888 -20.2165818409436,57.535719896762 -20.2148329371043,57.5375401844315 -20.2160821541323,57.5381112550728 -20.2153326239156,57.5388964772047 -20.2158680026418,57.5397887750819 -20.2150113966798,57.5401456942329 -20.2147615532741,57.5404312295536 -20.214725861359,57.5408595325346 -20.2150113966798,57.5419302899872 -20.215511083491,57.5407881487045 -20.2167246086039,57.541466295091 -20.217152911585,57.5409309163648 -20.2178667498867,57.5425370525437 -20.2190088911696,57.5427512040342 -20.218794739679,57.5440718048926 -20.219579961811,57.5424299767985 -20.2217928605464,57.5451782542602 -20.2227922341688,57.5461419359676 -20.2218285524615,57.5457136329865 -20.2215787090558,57.5467843904391 -20.2201867243675,57.547926531722 -20.2209719464993,57.5482477589578 -20.2203294920278,57.5474625368258 -20.2199368809618,57.5477123802314 -20.2192587345752,57.5472126934201 -20.2189731992544,57.547926531722 -20.2181165932924,57.548747445769 -20.2186876639337,57.5499609708819 -20.2172242954152,57.5503535819479 -20.2175098307358,57.5508175768441 -20.2168673762642,57.5517812585513 -20.2174741388208,57.5523523291927 -20.2165104571134,57.5585984143331 -20.2208648707541,57.5562427479372 -20.223684532046,57.552066793872 -20.2209362545842,57.551638490891 -20.2214716333106,57.5523880211078 -20.2218999362916,57.5511388040798 -20.223256229065,57.5486046781085 -20.2215073252257,57.5445357997885 -20.2262900418473,57.5442145725528 -20.2267183448284,57.5441431887226 -20.2271823397246,57.5443930321283 -20.2277534103659,57.5445714917038 -20.2284315567526,57.5397173912519 -20.2294666222902,57.5399672346574 -20.230216152507,57.5389321691199 -20.2303946120825,57.5392890882707 -20.2320364401765,57.5380398712427 -20.2323933593274,57.537790027837 -20.2312512180446,57.5343636039888 -20.231857980601,57.5337211495171 -20.2297521576109,57.5327574678097 -20.2300733848467,57.5310799478007 -20.224469754178,57.53079441248 -20.224898057159,57.5305802609895 -20.2252549763099,57.5301876499233 -20.22532636014,57.5295451954519 -20.2250408248194,57.5298307307726 -20.2271109558944,57.5272966048013 -20.2276463346207,57.527510756292 -20.2285743244129,57.5260116958581 -20.2287527839884,57.5255477009621 -20.2272537235547,57.5246197111697 -20.2276106427056,57.5244412515945 -20.2268968044039,57.5232634183965 -20.2272537235547,57.5220498932835 -20.2270038801491,57.5218714337082 -20.2262186580172,57.5240129486133 -20.2258974307815,57.5239058728681 -20.2252906682249,57.5225138881796 -20.224898057159,57.5202652975291 -20.224898057159,57.5204794490197 -20.2258617388663,57.5192659239066 -20.2257546631211,57.5194443834821 -20.2268611124888,57.5183022421994 -20.2270395720643,57.5180167068787 -20.226147274187,57.5158038081433 -20.2266826529134,57.513662293238 -20.2273964912151,57.5143047477095 -20.2291453950544,57.5111281672667 -20.2298592333561,57.5107355562008 -20.2286814001583,57.5098789502388 -20.2287884759035,57.5098075664085 -20.2281817133471,57.5077731272485 -20.2290383193091,57.506773753626 -20.2296807737807,57.5058457638338 -20.2304303039975,57.5051319255321 -20.2312512180446,57.5044180872304 -20.2319293644312,57.5034187136077 -20.2324647431575,57.5080586625692 -20.2406024997974,57.5087011170409 -20.241244954269,57.5094149553426 -20.2419944844858,57.510414328965 -20.2422800198066,57.5114850864176 -20.2423514036367,57.5144475153699 -20.2427083227876,57.5121275408891 -20.2446713781175,57.5089866523616 -20.2467415091925,57.5078088191636 -20.2461704385511,57.505917147664 -20.246527357702,57.5058100719188 -20.2459919789757,57.5055959204283 -20.2454922921644,57.5052746931923 -20.2451710649287,57.5044894710604 -20.2448498376929,57.503847016589 -20.2449926053532,57.5029190267966 -20.2451353730136,57.5021338046646 -20.2453495245041,57.5019196531741 -20.2455279840796,57.5022408804101 -20.2437076964101,57.5019553450893 -20.243279393429,57.5013485825328 -20.2429938581083,57.5014556582781 -20.2423157117217,57.5016698097686 -20.2416018734199,57.5010630472121 -20.2411378785238,57.50020644125 -20.2407809593729,57.4990642999672 -20.2404597321371,57.4989215323068 -20.2403169644768,57.499421219118 -20.2386394444677,57.5000636735897 -20.2365693133926,57.5008488957216 -20.2344634904024,57.5014913501931 -20.2334284248648,57.5017055016836 -20.2324647431575,57.5013842744479 -20.2321435159217,57.500634744231 -20.2319650563463,57.4999565978445 -20.2321435159217,57.4999565978445 -20.2328216623084,57.5000636735897 -20.2334641167799,57.4993141433728 -20.2348561014683,57.4986003050711 -20.2362480861568,57.4982790778353 -20.2375329951,57.4977436991089 -20.2388892878733,57.4978150829392 -20.2399600453259,57.4976723152789 -20.2404954240522,57.4967086335715 -20.2399600453259)))");
            DataTable dtSettlementBoundary = QryPolygonInside(SettlementBoundary, @"C:\Reza\Geo\Maps\Creations\ROAD\SHAPE\MauritiusLocalityVca.shp");
        }

        public static DataTable QryPolygonInside(BaseShape polygon, String sPath)
        {
            ShapeFileFeatureLayer shp = new ShapeFileFeatureLayer(sPath);
            shp.Open();

            DataTable dt = new DataTable();
            List<String> lCols = new List<string>();
            Collection<FeatureSourceColumn> cols = shp.QueryTools.GetColumns();
            foreach (FeatureSourceColumn f in cols)
            {
                dt.Columns.Add(f.ColumnName);
                lCols.Add(f.ColumnName);
            }

            Collection<Feature> feaContains = shp.QueryTools.GetFeaturesContaining(polygon, lCols);
            Collection<Feature> feaOverlap = shp.QueryTools.GetFeaturesOverlapping(polygon, lCols);
            Collection<Feature> feaTouch = shp.QueryTools.GetFeaturesTouching(polygon, lCols);

            shp.Close();

            if (feaContains.Count > 0)
            {
                foreach (Feature f in feaContains)
                {
                    DataRow dr = dt.NewRow();
                    foreach (KeyValuePair<String, String> k in f.ColumnValues)
                        dr[k.Key] = k.Value;
                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }
        public static DataTable QryPolygonOverlap(BaseShape polygon, String sPath)
        {
            ShapeFileFeatureLayer shp = new ShapeFileFeatureLayer(sPath);
            shp.Open();

            DataTable dt = new DataTable();
            List<String> lCols = new List<string>();
            Collection<FeatureSourceColumn> cols = shp.QueryTools.GetColumns();
            foreach (FeatureSourceColumn f in cols)
            {
                dt.Columns.Add(f.ColumnName);
                lCols.Add(f.ColumnName);
            }

            Collection<Feature> feaOverlap = shp.QueryTools.GetFeaturesOverlapping(polygon, lCols);
            shp.Close();

            if (feaOverlap.Count > 0)
            {
                foreach (Feature f in feaOverlap)
                {
                    DataRow dr = dt.NewRow();
                    foreach (KeyValuePair<String, String> k in f.ColumnValues)
                        dr[k.Key] = k.Value;
                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }
        public static DataTable QryPolygonTouch(BaseShape polygon, String sPath)
        {
            ShapeFileFeatureLayer shp = new ShapeFileFeatureLayer(sPath);
            shp.Open();

            DataTable dt = new DataTable();
            List<String> lCols = new List<string>();
            Collection<FeatureSourceColumn> cols = shp.QueryTools.GetColumns();
            foreach (FeatureSourceColumn f in cols)
            {
                dt.Columns.Add(f.ColumnName);
                lCols.Add(f.ColumnName);
            }

            Collection<Feature> feaTouch = shp.QueryTools.GetFeaturesTouching(polygon, lCols);
            shp.Close();

            if (feaTouch.Count > 0)
            {
                foreach (Feature f in feaTouch)
                {
                    DataRow dr = dt.NewRow();
                    foreach (KeyValuePair<String, String> k in f.ColumnValues)
                        dr[k.Key] = k.Value;
                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }
        public static DataTable QryPolygonDistance(BaseShape polygon, Double radius, String sPath)
        {
            ShapeFileFeatureLayer shp = new ShapeFileFeatureLayer(sPath);
            shp.Open();

            DataTable dt = new DataTable();
            List<String> lCols = new List<string>();
            Collection<FeatureSourceColumn> cols = shp.QueryTools.GetColumns();
            foreach (FeatureSourceColumn f in cols)
            {
                dt.Columns.Add(f.ColumnName);
                lCols.Add(f.ColumnName);
            }

            Collection<Feature> fea = shp.QueryTools.GetFeaturesWithinDistanceOf(polygon, GeographyUnit.DecimalDegree, DistanceUnit.Meter, radius, lCols);
            shp.Close();

            if (fea.Count > 0)
            {
                foreach (Feature f in fea)
                {
                    DataRow dr = dt.NewRow();
                    foreach (KeyValuePair<String, String> k in f.ColumnValues)
                        dr[k.Key] = k.Value;
                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }
        public static DataTable QryPolygonDistance(BaseShape polygon, Double radius, String sPath, String shpCondition)
        {
            ShapeFileFeatureLayer shp = new ShapeFileFeatureLayer(sPath);
            shp.Open();

            DataTable dt = new DataTable();
            List<String> lCols = new List<string>();
            Collection<FeatureSourceColumn> cols = shp.QueryTools.GetColumns();
            foreach (FeatureSourceColumn f in cols)
            {
                dt.Columns.Add(f.ColumnName);
                lCols.Add(f.ColumnName);
            }

            String sql = "select * from " + Path.GetFileNameWithoutExtension(sPath) + " where " + shpCondition;
            GeoDataTable geoDataTable = shp.QueryTools.ExecuteQuery(sql);

            Collection<Feature> fea = shp.QueryTools.GetFeaturesWithinDistanceOf(polygon, GeographyUnit.DecimalDegree, DistanceUnit.Meter, radius, lCols);
            shp.Close();

            if (fea.Count > 0)
            {
                foreach (Feature f in fea)
                {
                    DataRow dr = dt.NewRow();
                    foreach (KeyValuePair<String, String> k in f.ColumnValues)
                        dr[k.Key] = k.Value;
                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }

        public static DataTable QryPolygonNeatestTo(BaseShape polygon, int maxItemToFind, String sPath)
        {
            ShapeFileFeatureLayer shp = new ShapeFileFeatureLayer(sPath);
            shp.Open();

            DataTable dt = new DataTable();
            List<String> lCols = new List<string>();
            Collection<FeatureSourceColumn> cols = shp.QueryTools.GetColumns();
            foreach (FeatureSourceColumn f in cols)
            {
                dt.Columns.Add(f.ColumnName);
                lCols.Add(f.ColumnName);
            }
            dt.Columns.Add("NDistance");
            dt.Columns.Add("BoundingBox");

            Collection<Feature> fea = shp.QueryTools.GetFeaturesNearestTo(polygon, GeographyUnit.DecimalDegree, maxItemToFind, lCols);
            shp.Close();

            if (fea.Count > 0)
            {
                foreach (Feature f in fea)
                {
                    DataRow dr = dt.NewRow();
                    foreach (KeyValuePair<String, String> k in f.ColumnValues)
                        dr[k.Key] = k.Value;

                    Double distance = polygon.GetDistanceTo(f, GeographyUnit.DecimalDegree, DistanceUnit.Meter);
                    dr["NDistance"] = distance;
                    dr["BoundingBox"] = f.GetBoundingBox().ToString();
                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }
        public static DataTable QryPolygonNeatestTo(BaseShape polygon, int maxItemToFind, Double radius, String sPath)
        {
            ShapeFileFeatureLayer shp = new ShapeFileFeatureLayer(sPath);
            shp.Open();

            DataTable dt = new DataTable();
            List<String> lCols = new List<string>();
            Collection<FeatureSourceColumn> cols = shp.QueryTools.GetColumns();
            foreach (FeatureSourceColumn f in cols)
            {
                dt.Columns.Add(f.ColumnName);
                lCols.Add(f.ColumnName);
            }
            dt.Columns.Add("NDistance");
            dt.Columns.Add("BoundingBox");
            dt.Columns.Add("feature");

            Collection<Feature> fea = shp.QueryTools.GetFeaturesNearestTo(polygon, GeographyUnit.DecimalDegree, maxItemToFind, lCols, radius, DistanceUnit.Meter);
            shp.Close();

            if (fea.Count > 0)
            {
                foreach (Feature f in fea)
                {
                    DataRow dr = dt.NewRow();
                    foreach (KeyValuePair<String, String> k in f.ColumnValues)
                        dr[k.Key] = k.Value;

                    Double distance = polygon.GetDistanceTo(f, GeographyUnit.DecimalDegree, DistanceUnit.Meter);
                    dr["NDistance"] = distance;
                    dr["BoundingBox"] = f.GetBoundingBox().ToString();
                    dr["feature"] = f.ToString();
                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }
        public static DataSet QryAllFeatures(Double x, Double y, Double radius, String sPath)
        {
            DataSet dsResult = new DataSet();
            ShapeFileFeatureSource source = new ShapeFileFeatureSource(sPath);
            source.Open();

            //String sFN = Path.GetFileNameWithoutExtension(sPath);
            //DataTable dt = new DataTable(sFN);
            DataTable dt = new DataTable("Data");
            Collection<FeatureSourceColumn> cols = source.GetColumns();
            List<String> lCols = new List<string>();
            foreach (FeatureSourceColumn f in cols)
            {
                lCols.Add(f.ColumnName);
                dt.Columns.Add(f.ColumnName);
            }
            dt.Columns.Add("GeomData");

            Collection<Feature> features = source.GetFeaturesNearestTo(
                new PointShape(x, y),
                GeographyUnit.DecimalDegree,
                1,
                lCols,
                radius,
                DistanceUnit.Meter
            );
            source.Close();

            if (features.Count > 0)
            {
                InMemoryFeatureLayer memo = new InMemoryFeatureLayer();
                foreach (Feature f in features)
                {
                    memo.InternalFeatures.Add(f);
                    DataRow dr = dt.NewRow();
                    foreach (KeyValuePair<String, String> k in f.ColumnValues)
                        dr[k.Key] = k.Value;
                    dr["GeomData"] = f.ToString();

                    dt.Rows.Add(dr);
                }
                memo.Open();
                String BoundingBox = memo.GetBoundingBox().ToString();
                memo.Close();
                DataTable dtBoundingBox = new DataTable("BoundingBox");
                dtBoundingBox.Columns.Add("id");
                dtBoundingBox.Columns.Add("Desc");
                dtBoundingBox.Rows.Add("BoundingBox", BoundingBox);
                dsResult.Tables.Add(dtBoundingBox);
            }
            dsResult.Tables.Add(dt);
            return dsResult;
        }


        JObject QryPolygonShp(BaseShape polygon, Double radius, String sPath, int maxItemToFind)
        {
            JObject result = new JObject();

            ShapeFileFeatureLayer shp = new ShapeFileFeatureLayer(sPath);
            shp.Open();

            DataTable dt = new DataTable();
            List<String> lCols = new List<string>();
            Collection<FeatureSourceColumn> cols = shp.QueryTools.GetColumns();
            foreach (FeatureSourceColumn f in cols)
            {
                dt.Columns.Add(f.ColumnName);
                lCols.Add(f.ColumnName);
            }

            Collection<Feature> fea = shp.QueryTools.GetFeaturesContaining(polygon, lCols);
            shp.Close();

            ShapeFileFeatureSource source = new ShapeFileFeatureSource(sPath);
            source.Open();
            Collection<Feature> features = source.GetFeaturesInsideBoundingBox(
                polygon.GetBoundingBox(),
                lCols
            );
            source.Close();


            if (fea.Count > 0)
            {
                foreach (Feature f in fea)
                {
                    DataRow dr = dt.NewRow();
                    foreach (KeyValuePair<String, String> k in f.ColumnValues)
                        dr[k.Key] = k.Value;
                    dt.Rows.Add(dr);
                }
                result.Add("success", JToken.FromObject(true));
                result.Add("dt", JToken.FromObject(dt));
            }
            else
            {
                result.Add("success", JToken.FromObject(false));
            }
            return result;
        }
        JObject QryLineShp(Double x, Double y, Double radius, String sPath, int maxItemToFind)
        {
            JObject result = new JObject();
            ShapeFileFeatureSource source = new ShapeFileFeatureSource(sPath);
            source.Open();
            Collection<Feature> features = source.GetFeaturesNearestTo(
                new PointShape(x, y),
                GeographyUnit.DecimalDegree,
                10,
                new string[1] { "NAME" },
                10,
                DistanceUnit.Meter
            );
            source.Close();

            if (features.Count > 0)
            {
                string descrip = features[0].ColumnValues["name"];

                result.Add("name", JToken.FromObject(descrip));
                result.Add("success", JToken.FromObject(true));
            }
            else
            {
                result.Add("success", JToken.FromObject(false));
            }
            return result;
        }

        #endregion

        #region Generic Shp
        public Boolean CreateShp(ZoneCreation zc)
        {
            Boolean b = false;
            try
            {
                if (!(System.IO.Directory.Exists(LayerPaths.ZonePath(String.Empty))))
                    System.IO.Directory.CreateDirectory(LayerPaths.ZonePath(String.Empty));

                string[] filePaths = Directory.GetFiles(LayerPaths.ZonePath(String.Empty), zc.UserToken.ToString() + ".*");
                foreach (String s in filePaths)
                    if (File.Exists(s))
                        try
                        {
                            File.Delete(s);
                        }
                        catch { }

                List<DbfColumn> dbfColumns = new List<DbfColumn>();
                foreach (DataColumn dc in zc.dtData.Columns)
                    if (dc.ColumnName == "GeomData")
                        continue;
                    else
                        dbfColumns.Add(new DbfColumn(dc.ColumnName, DbfColumnType.Character, 500, 0));

                String path = LayerPaths.ZonePath(zc.UserToken.ToString()) + ".shp";
                ShapeFileFeatureLayer.CreateShapeFile(ShapeFileType.Polygon, path, dbfColumns, Encoding.Default, OverwriteMode.Overwrite);

                ShapeFileFeatureLayer newShapeFile = new ShapeFileFeatureLayer(path, GeoFileReadWriteMode.ReadWrite);
                newShapeFile.Open();
                newShapeFile.EditTools.BeginTransaction();

                foreach (DataRow dr in zc.dtData.Rows)
                {
                    Feature feature = new Feature(dr["GeomData"].ToString());
                    foreach (DataColumn dc in zc.dtData.Columns)
                        if (dc.ColumnName == "GeomData")
                            continue;
                        else
                            feature.ColumnValues.Add(dc.ColumnName, dr[dc].ToString());

                    if (feature.GetWellKnownBinary() != null)
                        if (feature.GetWellKnownType() == WellKnownType.Polygon)
                            newShapeFile.EditTools.Add(feature);
                }

                newShapeFile.EditTools.CommitTransaction();
                newShapeFile.Close();

                b = true;
            }
            catch (Exception ex) { }
            return b;
        }
        #endregion
    }
}