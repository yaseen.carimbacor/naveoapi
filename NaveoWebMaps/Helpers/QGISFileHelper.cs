﻿using NaveoWebMaps.Constants;
using NaveoWebMaps.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using ThinkGeo.MapSuite;
using ThinkGeo.MapSuite.Drawing;
using ThinkGeo.MapSuite.Layers;
using ThinkGeo.MapSuite.Shapes;
using ThinkGeo.MapSuite.Styles;
using ThinkGeo.MapSuite.WebApi;

namespace NaveoWebMaps.Helpers
{
    public class QGISFileHelper
    {
        #region public functions
        public LayerOverlay OpenQGIS(String LayerID, List<String> FeaturesToExclude)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(LayerPaths.QGISPath("naveomaps.qgs"));//MokaDec2018  MDC2019QGIS
            XmlElement prjElement = (XmlElement)doc.GetElementsByTagName("qgis").Item(0);
            return ReadQGISXml(prjElement, LayerPaths.QGISPath(String.Empty), LayerID, FeaturesToExclude);
        }

        public List<qgisLayer> GetLayers()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(LayerPaths.QGISPath("MokaDec2018.qgs"));
            XmlElement projectElement = (XmlElement)doc.GetElementsByTagName("qgis").Item(0);

            List<qgisLayer> l = new List<Models.qgisLayer>();
            XmlNodeList layerOrder = projectElement.GetElementsByTagName("layerorder");
            XmlNodeList layer = ((XmlElement)layerOrder[0]).GetElementsByTagName("layer");
            if (layer != null && layer.Count > 0)
            {
                for (int iLayer = layer.Count - 1; iLayer >= 0; iLayer--)
                {
                    XmlElement LayerElement = (XmlElement)layer[iLayer];
                    String layerID = LayerElement.Attributes["id"].InnerText;

                    XmlNode treelLayerNode = projectElement.SelectSingleNode("/qgis/layer-tree-group/layer-tree-layer[@id = '" + layerID + "']");
                    if (treelLayerNode != null)
                    {
                        qgisLayer qgislayer = new Models.qgisLayer();
                        qgislayer.order = iLayer;
                        qgislayer.id = layerID;
                        qgislayer.name = treelLayerNode.Attributes["name"].InnerText;
                        String chk = treelLayerNode.Attributes["checked"].InnerText;
                        if (chk == "Qt::Checked")
                            qgislayer.ticked = true;

                        qgislayer.NaveoModels = "BLUP";
                        #region Features
                        XmlNode maplayerNode = projectElement.SelectSingleNode("/qgis/projectlayers/maplayer[id = '" + layerID + "']");
                        XmlNode RenderAttrNode = maplayerNode.SelectSingleNode("renderer-v2");
                        if (RenderAttrNode != null)
                        {
                            XmlNodeList categoryList = RenderAttrNode.SelectNodes("categories/category");
                            if (categoryList != null)
                            {
                                List<qgisLayerFeatures> lFeatures = new List<qgisLayerFeatures>();
                                foreach (XmlNode n in categoryList)
                                {
                                    String x = n.Attributes["value"].InnerText;
                                    if (!String.IsNullOrEmpty(x))
                                    {
                                        qgisLayerFeatures f = new qgisLayerFeatures();
                                        f.featureName = x;
                                        f.ticked = false;
                                        String y = n.Attributes["render"].InnerText;
                                        if (y == "true")
                                            f.ticked = true;

                                        //f.ticked = false;
                                        lFeatures.Add(f);
                                    }
                                }
                                qgislayer.lFeatures = lFeatures;
                            }
                        }

                        #endregion
                        l.Add(qgislayer);
                    }
                }
            }
            return l;
        }

        public String PrepareLayers(List<qgisLayer> layers)
        {
            if (!(System.IO.Directory.Exists(LayerPaths.QGISTmpPath(String.Empty))))
                System.IO.Directory.CreateDirectory(LayerPaths.QGISTmpPath(String.Empty));

            string[] filePaths = Directory.GetFiles(LayerPaths.QGISTmpPath(String.Empty), "*.*");
            foreach (String file in filePaths)
            {
                try
                {
                    FileInfo fi = new FileInfo(file);
                    if (fi.LastAccessTime < DateTime.Now.AddHours(-1))
                        fi.Delete();
                }
                catch { }
            }

            string json = JsonConvert.SerializeObject(layers);

            //write string to file
            String newFile = Guid.NewGuid().ToString() + ".txt";
            System.IO.File.WriteAllText(LayerPaths.QGISTmpPath(String.Empty) + newFile, json);
            return newFile;
        }
        public LayerOverlay ShowLayers(String path)
        {
            LayerOverlay layerOverlay = new LayerOverlay();
            if (!File.Exists(path))
                return layerOverlay;

            String json = System.IO.File.ReadAllText(path);
            List<qgisLayer> layers = JsonConvert.DeserializeObject<List<qgisLayer>>(json);
            foreach (qgisLayer l in layers)
            {
                if (!l.ticked)
                    continue;

                List<String> FeaturesToExclude = new List<string>();
                if (l.lFeatures != null)
                    foreach (qgisLayerFeatures feature in l.lFeatures)
                        if (!feature.ticked)
                            FeaturesToExclude.Add(feature.featureName);

                return ShowLayers(l.id, FeaturesToExclude);
                //Layer overlay = ShowLayers(l.id, FeaturesToExclude);
                //layerOverlay.Layers.Add(overlay);
                //return layerOverlay;
            }

            return layerOverlay;
        }
        #endregion

        #region local variables
        String sLayerTreeLayer = "/qgis/layer-tree-group/layer-tree-layer";
        String sNodeCategory = "renderer-v2/categories/category";
        String sNodeSymbol = "renderer-v2/symbols/symbol";

        DataTable RenderDT()
        {
            DataTable dtRender = new DataTable("Render");

            //Polygon
            dtRender.Columns.Add("Descrip", typeof(String));
            dtRender.Columns.Add("FillColor", typeof(Color));
            dtRender.Columns.Add("OutLineColor", typeof(Color));

            //PolyLine
            dtRender.Columns.Add("Symbol", typeof(int));
            dtRender.Columns.Add("line_color", typeof(Color));
            dtRender.Columns.Add("line_width", typeof(Double));
            dtRender.Columns.Add("line_style", typeof(String));
            dtRender.Columns.Add("Order", typeof(int));

            //LineStyleRuleBase
            dtRender.Columns.Add("hasChildren", typeof(int));
            dtRender.Columns.Add("MinZoom", typeof(int));
            dtRender.Columns.Add("MaxZoom", typeof(int));
            dtRender.Columns.Add("filterField", typeof(String));

            //Points
            dtRender.Columns.Add("color", typeof(Color));
            dtRender.Columns.Add("outline_width", typeof(Double));
            dtRender.Columns.Add("outline_style", typeof(String));
            dtRender.Columns.Add("name", typeof(String));
            dtRender.Columns.Add("size", typeof(int));

            return dtRender;
        }
        #endregion

        #region local functions
        private LayerOverlay ShowLayers(String LayerID, List<String> FeaturesToExclude)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(LayerPaths.QGISPath("MokaDec2018.qgs"));//MokaDec2018  MDC2019QGIS
            XmlElement prjElement = (XmlElement)doc.GetElementsByTagName("qgis").Item(0);
            return myLayer(prjElement, LayerPaths.QGISPath(String.Empty), LayerID, FeaturesToExclude);
        }
        private LayerOverlay ReadQGISXml(XmlElement projectElement, String sPath, String sLayerID, List<String> FeaturesToExclude)
        {
            LayerOverlay layerOverlay = myLayer(projectElement, sPath, sLayerID, FeaturesToExclude);
            //layerOverlay.Layers.Add(myLayer(projectElement, sPath, sLayerID, FeaturesToExclude));
            return layerOverlay;
        }

        private LayerOverlay myLayer(XmlElement projectElement, String sPath, String sLayerID, List<String> FeaturesToExclude)
        {
            LayerOverlay layerOverlay = new LayerOverlay();
            XmlNodeList nodeList = projectElement.SelectNodes(sLayerTreeLayer);
            if (nodeList != null && nodeList.Count > 0)
            {
                for (int iLayer = nodeList.Count - 1; iLayer >= 0; iLayer--)
                {
                    XmlElement LayerElement = (XmlElement)nodeList[iLayer];
                    String layerID = LayerElement.Attributes["id"].InnerText;

                    XmlNode treelLayerNode = projectElement.SelectSingleNode(sLayerTreeLayer + "[@id = '" + layerID + "']");
                    if (treelLayerNode != null)
                    {
                        Boolean bShowLayer = false;
                        if (sLayerID.Trim() == layerID)
                            bShowLayer = true;
                        else if (sLayerID.Trim() == "0")
                        {
                            String chk = treelLayerNode.Attributes["checked"].InnerText;
                            if (chk == "Qt::Checked")
                                bShowLayer = true;
                        }

                        if (bShowLayer)
                        {
                            String source = treelLayerNode.Attributes["source"].InnerText.Split('|')[0];
                            String name = treelLayerNode.Attributes["name"].InnerText;

                            XmlNode maplayerNode = projectElement.SelectSingleNode("/qgis/projectlayers/maplayer[id = '" + layerID + "']");
                            XmlElement mapLayerElement = ((XmlElement)maplayerNode);
                            string path = mapLayerElement.GetElementsByTagName("datasource")[0].InnerText;
                            int srid = Convert.ToInt32(mapLayerElement.SelectSingleNode("srs/spatialrefsys/srid").InnerText);
                            //Proj4Projection proj4 = new Proj4Projection(srid, mapLayerElement.SelectSingleNode("srs/spatialrefsys/proj4").InnerText);//3857
                            Proj4Projection proj4 = new Proj4Projection(srid, Proj4Projection.GetSphericalMercatorParametersString());//3857
                            if (!proj4.IsOpen) proj4.Open();

                            String LayerType = mapLayerElement.Attributes["type"].InnerText;
                            switch (LayerType)
                            {
                                case "vector":
                                    ShapeFileFeatureLayer shp = new ShapeFileFeatureLayer(Path.Combine(sPath, source));
                                    shp.Name = name;
                                    shp.ReadWriteMode = GeoFileReadWriteMode.Read;
                                    shp.FeatureSource.Projection = proj4;

                                    //shp.RequireIndex = false;
                                    String encoding = mapLayerElement.SelectSingleNode("resourceMetadata/encoding").InnerText;
                                    if (!String.IsNullOrEmpty(encoding))
                                    {
                                        int i = 0;
                                        if (int.TryParse(encoding, out i))
                                            shp.Encoding = Encoding.GetEncoding(i);
                                    }

                                    shp.ZoomLevelSet = GetQGISZoomLevelSet(mapLayerElement, sPath, FeaturesToExclude);
                                    shp.DrawingMarginInPixel = 20;
                                    layerOverlay.Layers.Add(shp);
                                    break;

                                case "raster":
                                    String spath = Path.Combine(sPath, source);
                                    layerOverlay.Layers.Add(new RasterFileHelper().OpenImage(spath, 255));
                                    break;
                            }
                        }
                    }
                }
            }

            return layerOverlay;
        }
        private ZoomLevelSet GetQGISZoomLevelSet(XmlElement rootNode, String sPath, List<String> FeaturesToExclude)
        {
            ZoomLevelSet result = new ZoomLevelSet();

            int minZoomLevel = GetMinZoomLevel(rootNode, "minScale");
            int maxZoomLevel = GetMaxZoomLevel(rootNode, "maxScale");
            int minLabelZoomLevel = GetMinRenderLabelZoom(rootNode);
            String GeometryType = rootNode.Attributes["geometry"].InnerText;
            String labelsEnabled = rootNode.Attributes["labelsEnabled"].InnerText;

            System.Collections.ObjectModel.Collection<ZoomLevel> zoomLevels = result.GetZoomLevels();
            for (int i = 0; i < zoomLevels.Count; i++)
            {
                if (i - 1 >= minZoomLevel && i - 1 <= maxZoomLevel)
                {
                    if (GeometryType == "Polygon")
                    {
                        zoomLevels[i].DefaultAreaStyle = GetAreaStyle(rootNode, FeaturesToExclude);

                        if (labelsEnabled == "1")
                        {
                            XmlNode labelingNode = rootNode.SelectSingleNode("labeling");
                            XmlNode renderingNode = rootNode.SelectSingleNode("labeling/settings/rendering");

                            //LineStyle condition havin rules
                            if (renderingNode == null)
                            {
                                labelingNode = rootNode.SelectSingleNode("labeling/rules/rule");
                                renderingNode = rootNode.SelectSingleNode("labeling/rules/rule/settings/rendering");
                            }

                            if (renderingNode != null)
                            {
                                int minScale = GetMinZoomLevel(renderingNode, "scaleMax");
                                int maxScale = GetMaxZoomLevel(renderingNode, "scaleMin");

                                for (int j = 0; j < zoomLevels.Count; j++)
                                {
                                    if (j - 1 >= minScale && j - 1 <= maxScale)
                                    {
                                        zoomLevels[j].DefaultTextStyle = GetTextStyle(labelingNode, j, FeaturesToExclude);
                                    }
                                }
                            }
                        }
                    }
                    else if (GeometryType == "Line")
                    {
                        zoomLevels[i].DefaultLineStyle = GetLineStyle(rootNode, FeaturesToExclude);

                        if (labelsEnabled == "1")
                        {
                            XmlNode labelingNode = rootNode.SelectSingleNode("labeling");
                            XmlNode renderingNode = rootNode.SelectSingleNode("labeling/settings/rendering");

                            //LineStyle condition havin rules
                            if (renderingNode == null)
                            {
                                labelingNode = rootNode.SelectSingleNode("labeling/rules/rule");
                                renderingNode = rootNode.SelectSingleNode("labeling/rules/rule/settings/rendering");
                            }

                            if (renderingNode != null)
                            {
                                int minScale = GetMinZoomLevel(renderingNode, "scaleMax");
                                int maxScale = GetMaxZoomLevel(renderingNode, "scaleMin");

                                for (int j = 0; j < zoomLevels.Count; j++)
                                {
                                    if (j - 1 >= minScale && j - 1 <= maxScale)
                                    {
                                        TextStyle ts = GetTextStyle(labelingNode, j, FeaturesToExclude);
                                        zoomLevels[j].DefaultTextStyle = ts;
                                    }
                                }
                            }
                        }
                    }
                    else if (GeometryType == "Point")
                    {
                        zoomLevels[i].DefaultPointStyle = GetPointStyle(rootNode, FeaturesToExclude);

                        if (labelsEnabled == "1")
                        {
                            XmlNode labelingNode = rootNode.SelectSingleNode("labeling");
                            XmlNode renderingNode = rootNode.SelectSingleNode("labeling/settings/rendering");

                            //LineStyle condition havin rules
                            if (renderingNode == null)
                            {
                                labelingNode = rootNode.SelectSingleNode("labeling/rules/rule");
                                renderingNode = rootNode.SelectSingleNode("labeling/rules/rule/settings/rendering");
                            }

                            if (renderingNode != null)
                            {
                                int minScale = GetMinZoomLevel(renderingNode, "scaleMax");
                                int maxScale = GetMaxZoomLevel(renderingNode, "scaleMin");
                                if (minScale < minZoomLevel)
                                    minScale = minZoomLevel;
                                if (maxScale < maxZoomLevel)
                                    maxScale = maxZoomLevel;

                                for (int j = 0; j < zoomLevels.Count; j++)
                                {
                                    if (j - 1 >= minScale && j - 1 <= maxScale)
                                    {
                                        TextStyle ts = GetTextStyle(labelingNode, j, FeaturesToExclude);
                                        zoomLevels[j].DefaultTextStyle = ts;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return result;
        }
        private int GetMinZoomLevel(XmlNode rootNode, String Attribute)
        {
            int result = 1;
            int.TryParse(rootNode.Attributes[Attribute].InnerText, out result);
            return minZoomLvl(result);
        }
        private int minZoomLvl(int zoomlvl)
        {
            int result = 16;
            if (zoomlvl >= 250000)
                result = 10;
            else if (zoomlvl >= 125000)
                result = 11;
            else if (zoomlvl >= 30000)
                result = 12;
            else if (zoomlvl >= 5000)
                result = 15;
            return result;
        }
        private int GetMaxZoomLevel(XmlNode rootNode, String Attribute)
        {
            // TODO : we need to calculate the zoom level value
            int result = 20;
            int.TryParse(rootNode.Attributes[Attribute].InnerText, out result);
            return maxZoomLvl(result);
        }
        private int maxZoomLvl(int zoomlvl)
        {
            int result = 20;
            return result;
        }

        private int GetMinRenderLabelZoom(XmlNode rootNode)
        {
            // TODO : we need to calculate the zoom level value
            int result = int.Parse(rootNode.Attributes["labelsEnabled"].InnerText);
            if (result < 0 || result > 19)
            {
                result = 20;
            }

            return result;
        }
        private GeoColor GetFontColor(XmlNode rootNode)
        {
            string textColor = rootNode.SelectSingleNode("settings/text-style").Attributes["textColor"].InnerText;
            String[] split = textColor.Split(',');
            Color c = Color.FromArgb(Convert.ToInt32(split[3]) / 255, Convert.ToInt32(split[0]), Convert.ToInt32(split[1]), Convert.ToInt32(split[2]));
            GeoColor result = GeoColor.FromHtml(ColorTranslator.ToHtml(c));
            return result;
        }

        DataTable dtRender(XmlNode rootNode, String symbolID, String label)
        {
            DataTable dtRender = RenderDT();
            XmlNodeList symbolList = rootNode.SelectNodes(sNodeSymbol);
            foreach (XmlNode symbol in symbolList)
            {
                if (symbol.Attributes["name"].InnerText == symbolID)
                {
                    Double alpha = Convert.ToDouble(symbol.Attributes["alpha"].InnerText);
                    String type = symbol.Attributes["type"].InnerText;

                    XmlNodeList layerNodeList = symbol.SelectNodes("layer");
                    foreach (XmlNode layerNode in layerNodeList)
                    {
                        String layerEnabled = layerNode.Attributes["enabled"].InnerText;
                        if (layerEnabled == "1")
                        {
                            XmlNodeList layerProps = layerNode.SelectNodes("prop");
                            String sColorARGB = String.Empty;
                            String sLineColorARGB = String.Empty;
                            String sOutlineColorARGB = String.Empty;
                            String style = String.Empty;
                            Double LineWidth = 1.0;
                            String name = String.Empty;
                            float size = 3;

                            foreach (XmlNode x in layerProps)
                            {
                                if (x.Attributes["k"].InnerText == "color")
                                    sColorARGB = x.Attributes["v"].InnerText;

                                if (x.Attributes["k"].InnerText == "outline_color")
                                    sOutlineColorARGB = x.Attributes["v"].InnerText;

                                if (x.Attributes["k"].InnerText == "style")
                                    style = x.Attributes["v"].InnerText;

                                if (x.Attributes["k"].InnerText == "line_color")
                                    sLineColorARGB = x.Attributes["v"].InnerText;

                                if (x.Attributes["k"].InnerText == "line_width")
                                    LineWidth = Convert.ToDouble(x.Attributes["v"].InnerText);

                                if (x.Attributes["k"].InnerText == "line_style")
                                    style = x.Attributes["v"].InnerText;

                                if (x.Attributes["k"].InnerText == "outline_width")
                                    LineWidth = Convert.ToDouble(x.Attributes["v"].InnerText);

                                if (x.Attributes["k"].InnerText == "outline_style")
                                    style = x.Attributes["v"].InnerText;

                                if (x.Attributes["k"].InnerText == "name")
                                    name = x.Attributes["v"].InnerText;

                                if (x.Attributes["k"].InnerText == "size")
                                    size = (float)Convert.ToDouble(x.Attributes["v"].InnerText);
                            }

                            String[] split = sColorARGB.Split(',');
                            DataRow dr = dtRender.NewRow();
                            dr["Descrip"] = label;
                            if (style != "no")
                                if (split.Length > 3)
                                    dr["FillColor"] = Color.FromArgb(Convert.ToInt32(Convert.ToInt32(split[3]) * alpha), Convert.ToInt32(split[0]), Convert.ToInt32(split[1]), Convert.ToInt32(split[2]));

                            if (split.Length > 3)
                                dr["Color"] = Color.FromArgb(Convert.ToInt32(Convert.ToInt32(split[3]) * alpha), Convert.ToInt32(split[0]), Convert.ToInt32(split[1]), Convert.ToInt32(split[2]));

                            split = sOutlineColorARGB.Split(',');
                            if (split.Length > 3)
                                dr["OutLineColor"] = Color.FromArgb(Convert.ToInt32(Convert.ToInt32(split[3]) * alpha), Convert.ToInt32(split[0]), Convert.ToInt32(split[1]), Convert.ToInt32(split[2]));

                            dr["Symbol"] = symbol.Attributes["name"].Value;
                            split = sLineColorARGB.Split(',');
                            if (style != "no")
                                if (split.Length > 3)
                                    dr["line_color"] = Color.FromArgb(Convert.ToInt32(Convert.ToInt32(split[3]) * alpha), Convert.ToInt32(split[0]), Convert.ToInt32(split[1]), Convert.ToInt32(split[2]));
                            
                            dr["line_width"] = LineWidth;
                            dr["line_style"] = style;
                            dr["Order"] = layerNode.Attributes["pass"].InnerText;

                            dr["outline_width"] = LineWidth;
                            dr["outline_style"] = style;
                            dr["name"] = name;
                            dr["size"] = size;

                            dtRender.Rows.Add(dr);
                        }
                    }
                    break;
                }
            }
            return dtRender;
        }
        DataSet dsRenderSymbol(XmlNode rootNode, XmlNode RenderAttrNode)
        {
            DataTable dtRender = RenderDT();
            DataTable dtSymbol = new DataTable("Symbol");
            dtSymbol.Columns.Add("symbolID");
            dtSymbol.Columns.Add("Descrip");

            if (RenderAttrNode.Attributes["attr"] != null)
            {
                XmlNodeList categoryList = rootNode.SelectNodes(sNodeCategory);
                foreach (XmlNode n in categoryList)
                {
                    String symbolID = n.Attributes["symbol"].InnerText;
                    String label = n.Attributes["label"].InnerText;
                    dtSymbol.Rows.Add(symbolID, label);
                    Boolean bRender = Convert.ToBoolean(n.Attributes["render"].InnerText);
                    if (bRender)
                        dtRender.Merge(this.dtRender(rootNode, symbolID, label));
                }
            }
            else
            {
                XmlNodeList categoryList = rootNode.SelectNodes(sNodeSymbol);
                foreach (XmlNode n in categoryList)
                {
                    String layerEnabled = n.ChildNodes[0].Attributes["enabled"].InnerText;
                    if (layerEnabled == "1")
                    {
                        String symbolID = n.Attributes["name"].InnerText;
                        dtSymbol.Rows.Add(symbolID, "");
                        dtRender.Merge(this.dtRender(rootNode, symbolID, string.Empty));
                    }
                }
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dtRender);
            ds.Tables.Add(dtSymbol);
            return ds;
        }

        private TextStyle GetTextStyle(XmlNode rootNode, int zoomlevelIndex, List<String> FeaturesToExclude)
        {
            #region TextStyle
            TextStyle ts = new TextStyle();

            String fontFamily = rootNode.SelectSingleNode("settings/text-style").Attributes["fontFamily"].InnerText;
            float fontSize = float.Parse(rootNode.SelectSingleNode("settings/text-style").Attributes["fontSize"].InnerText);
            if (!Enum.TryParse(rootNode.SelectSingleNode("settings/text-style").Attributes["namedStyle"].InnerText.Replace("Semi", String.Empty), out DrawingFontStyles style))
                style = DrawingFontStyles.Italic;
            GeoFont font = new GeoFont(fontFamily, fontSize, style, DrawingGraphicsUnit.Pixel);
            ts.Font = font;

            GeoSolidBrush textSolidBrush = new GeoSolidBrush(GetFontColor(rootNode));
            ts.TextSolidBrush = textSolidBrush;

            ts.OverlappingRule = LabelOverlappingRule.NoOverlapping;
            ts.AllowLineCarriage = true;
            ts.BestPlacement = true;
            ts.TextLineSegmentRatio = 2;
            #endregion

            DataSet dsResult = new DataSet();
            
            DataTable dtExclude = new DataTable("dtExclude");
            dtExclude.Columns.Add("name");
            foreach (String s in FeaturesToExclude)
                dtExclude.Rows.Add(s);
            dsResult.Merge(dtExclude);

            String RenderLabel = rootNode.SelectSingleNode("settings/text-style").Attributes["fieldName"].InnerText;
            ts.TextColumnName = RenderLabel;
            
            myStyleData myStyle = new myStyleData();
            myStyle.ds = dsResult;
            myStyle.RenderLabel = RenderLabel;
            CustomTextStyle cts = new CustomTextStyle(ts.TextColumnName, ts.Font, ts.TextSolidBrush, myStyle);
            return cts;
        }
        private AreaStyle GetAreaStyle(XmlNode rootNode, List<String> FeaturesToExclude)
        {
            DataSet dsResult = GetAreaStyleDS(rootNode);

            DataTable dtExclude = new DataTable("dtExclude");
            dtExclude.Columns.Add("name");
            foreach (String s in FeaturesToExclude)
                dtExclude.Rows.Add(s);
            dsResult.Merge(dtExclude);

            String RenderLabel = String.Empty;
            XmlNode x = rootNode.SelectSingleNode("renderer-v2").Attributes["attr"];
            if (x != null)
                RenderLabel = x.InnerText;

            myStyleData myStyle = new myStyleData();
            myStyle.ds = dsResult;
            myStyle.RenderLabel = RenderLabel;
            CustomAreaStyle areaStyle = new CustomAreaStyle(GeoPens.AliceBlue, myStyle);
            return areaStyle;
        }
        private LineStyle GetLineStyle(XmlNode rootNode, List<String> FeaturesToExclude)
        {
            DataSet dsResult = GetLineStyleDS(rootNode);

            DataTable dtExclude = new DataTable("dtExclude");
            dtExclude.Columns.Add("name");
            foreach (String s in FeaturesToExclude)
                dtExclude.Rows.Add(s);
            dsResult.Merge(dtExclude);

            String RenderLabel = String.Empty;
            XmlNode x = rootNode.SelectSingleNode("renderer-v2").Attributes["attr"];
            if (x != null)
                RenderLabel = x.InnerText;

            myStyleData myStyle = new myStyleData();
            myStyle.ds = dsResult;
            myStyle.RenderLabel = RenderLabel;
            CustomLineStyle lineStyle = new CustomLineStyle(GeoPens.AliceBlue, myStyle);
            return lineStyle;
        }
        private PointStyle GetPointStyle(XmlNode rootNode, List<String> FeaturesToExclude)
        {
            DataSet dsResult = GetPointStyle(rootNode);

            DataTable dtExclude = new DataTable("dtExclude");
            dtExclude.Columns.Add("name");
            foreach (String s in FeaturesToExclude)
                dtExclude.Rows.Add(s);
            dsResult.Merge(dtExclude);

            String RenderLabel = String.Empty;
            XmlNode x = rootNode.SelectSingleNode("renderer-v2").Attributes["attr"];
            if (x != null)
                RenderLabel = x.InnerText;

            myStyleData myStyle = new myStyleData();
            myStyle.ds = dsResult;
            myStyle.RenderLabel = RenderLabel;
            CustomPointStyle pointStyle = new CustomPointStyle(PointSymbolType.Circle, new GeoSolidBrush(GeoColors.Green), 3, myStyle);
            return pointStyle;
        }

        private DataSet GetAreaStyleDS(XmlNode rootNode)
        {
            DataSet dsResult = new DataSet();
            //DataTable dtRender = new DataTable();

            XmlNode RenderAttrNode = rootNode.SelectSingleNode("renderer-v2");
            String renderType = RenderAttrNode.Attributes["type"].Value;
            switch (renderType)
            {
                case "nullSymbol":
                    break;

                case "categorizedSymbol":
                    dsResult = dsRenderSymbol(rootNode, RenderAttrNode);
                    break;

                case "singleSymbol":
                    dsResult = dsRenderSymbol(rootNode, RenderAttrNode);
                    break;
            }

            return dsResult;
        }
        private DataSet GetLineStyleDS(XmlNode rootNode)
        {
            DataSet dsResult = new DataSet();

            XmlNode RenderAttrNode = rootNode.SelectSingleNode("renderer-v2");
            String renderType = RenderAttrNode.Attributes["type"].Value;
            {
                switch (renderType)
                {
                    case "nullSymbol":
                        break;

                    case "RuleRenderer":
                        dsResult = dsRenderSymbol(rootNode, RenderAttrNode);
                        break;

                    case "singleSymbol":
                        dsResult = dsRenderSymbol(rootNode, RenderAttrNode);
                        break;
                }
            }

            return dsResult;
        }
        private DataSet GetPointStyle(XmlNode rootNode)
        {
            DataSet dsResult = new DataSet();

            XmlNode RenderAttrNode = rootNode.SelectSingleNode("renderer-v2");
            String renderType = RenderAttrNode.Attributes["type"].Value;
            {
                switch (renderType)
                {
                    case "nullSymbol":
                        break;

                    case "categorizedSymbol":
                        dsResult = dsRenderSymbol(rootNode, RenderAttrNode);
                        break;

                    case "singleSymbol":
                        dsResult = dsRenderSymbol(rootNode, RenderAttrNode);
                        break;
                }
            }
            return dsResult;
        }
        #endregion

        #region Custom styles
        class myStyleData
        {
            public DataSet ds;
            public String RenderLabel;
        }

        class CustomTextStyle : TextStyle
        {
            myStyleData myStyle = new myStyleData();

            public CustomTextStyle(string textColumnName, GeoFont textFont, GeoSolidBrush textSolidBrush, myStyleData myData) : base(textColumnName, textFont, textSolidBrush)
            {
                myStyle = myData;
            }
            protected override Collection<string> GetRequiredColumnNamesCore()
            {
                return new Collection<string>() { myStyle.RenderLabel };
            }
            protected override void DrawCore(IEnumerable<Feature> features, GeoCanvas canvas, Collection<SimpleCandidate> labelsInThisLayer, Collection<SimpleCandidate> labelsInAllLayers)
            {
                foreach (Feature feature in features)
                {
                    if (myStyle.ds != null)
                        if (myStyle.ds.Tables.Contains("dtExclude"))
                            if (myStyle.ds.Tables["dtExclude"].Rows.Count > 0)
                            {
                                Dictionary<string, string> featureValue = feature.ColumnValues;
                                DataRow[] drExclude = myStyle.ds.Tables["dtExclude"].Select(string.Format("name ='{0}'", featureValue.Values.FirstOrDefault().ToString().Replace("'", "''")));
                                if (drExclude.Length > 0)
                                    return;
                            }

                    base.DrawCore(features, canvas, labelsInThisLayer, labelsInAllLayers);
                }
            }
        }

        class CustomAreaStyle : AreaStyle
        {
            myStyleData myStyle = new myStyleData();
            public CustomAreaStyle(GeoPen outlinePen, myStyleData myData) : base(outlinePen) { myStyle = myData; }

            protected override Collection<string> GetRequiredColumnNamesCore()
            {
                return new Collection<string>() { myStyle.RenderLabel };
            }
            protected override void DrawCore(IEnumerable<Feature> features, GeoCanvas canvas, Collection<SimpleCandidate> labelsInThisLayer, Collection<SimpleCandidate> labelsInAllLayers)
            {
                Boolean bHasExclude = false;
                if (myStyle.ds != null)
                    if (myStyle.ds.Tables.Contains("dtExclude"))
                        if (myStyle.ds.Tables["dtExclude"].Rows.Count > 0)
                            bHasExclude = true;

                foreach (Feature feature in features)
                {
                    GeoColor FillColor = GeoColors.Transparent;
                    GeoColor OutlineColor = GeoColors.Black;
                    foreach (DataRow drSymbol in myStyle.ds.Tables["Symbol"].Rows)
                    {
                        if (!String.IsNullOrEmpty(myStyle.RenderLabel))
                        {
                            DataRow dr = myStyle.ds.Tables["Render"].Select(string.Format("Descrip ='{0}'", feature.ColumnValues[myStyle.RenderLabel].ToString().Replace("'", "''"))).FirstOrDefault();
                            if (dr == null) //Feature not selected
                                continue;

                            if (bHasExclude)
                            {
                                DataRow drExclude = myStyle.ds.Tables["dtExclude"].Select(string.Format("name ='{0}'", feature.ColumnValues[myStyle.RenderLabel].ToString().Replace("'", "''"))).FirstOrDefault();
                                if (drExclude != null)
                                    continue;
                            }

                            Color col = (Color)dr["FillColor"];
                            int colorCode = col.ToArgb();
                            String hexColor = "#" + colorCode.ToString("X2").Substring(2);

                            FillColor = GeoColor.FromHtml(hexColor);
                            FillColor.AlphaComponent = (byte)col.A; //Transparency

                            col = (Color)dr["OutLineColor"];
                            colorCode = col.ToArgb();
                            hexColor = "#" + colorCode.ToString("X2").Substring(2);
                            if (hexColor.Length != 7)
                                hexColor = "#" + colorCode.ToString("X2");
                            if (hexColor.Length != 7)
                                hexColor = "#" + Color.Gold.ToArgb().ToString("X2").Substring(2);
                            OutlineColor = GeoColor.FromHtml(hexColor);
                            OutlineColor.AlphaComponent = (byte)col.A; //Transparency
                        }
                        else
                        {
                            if (myStyle.ds.Tables["Render"].Rows.Count == 1)
                            {
                                DataRow dr = myStyle.ds.Tables["Render"].Rows[0];

                                Color col = (Color)dr["OutLineColor"];
                                int colorCode = col.ToArgb();
                                String hexColor = "#" + colorCode.ToString("X2").Substring(2);
                                if (hexColor.Length != 7)
                                    hexColor = "#" + colorCode.ToString("X2");
                                if (hexColor.Length != 7)
                                    hexColor = "#" + Color.Gold.ToArgb().ToString("X2").Substring(2);
                                OutlineColor = GeoColor.FromHtml(hexColor);

                                if (!String.IsNullOrEmpty(dr["FillColor"].ToString()))
                                {
                                    col = (Color)dr["FillColor"];
                                    colorCode = col.ToArgb();
                                    hexColor = "#" + colorCode.ToString("X2").Substring(2);
                                    if (hexColor.Length != 7)
                                        hexColor = "#" + colorCode.ToString("X2");
                                    if (hexColor.Length != 7)
                                        hexColor = "#" + Color.Gold.ToArgb().ToString("X2").Substring(2);

                                    FillColor = GeoColor.FromHtml(hexColor);
                                    FillColor.AlphaComponent = (byte)col.A;
                                }
                            }
                        }
                    }
                    canvas.DrawArea(feature, new GeoPen(OutlineColor, base.OutlinePen.Width), new GeoSolidBrush(FillColor), DrawingLevel.LevelOne);
                }
            }
        }

        class CustomLineStyle : LineStyle
        {
            myStyleData myStyle = new myStyleData();

            public CustomLineStyle(GeoPen outlinePen, myStyleData myData) : base(outlinePen)
            {
                myStyle = myData;
                if (myStyle.ds.Tables.Contains("Render"))
                {
                    DataTable distinctValues = new DataView(myStyle.ds.Tables["Render"]).ToTable(true, "filterField");
                    foreach (DataRow dr in distinctValues.Rows)
                        if (dr["filterField"].ToString() != "ELSE")
                            myStyle.RenderLabel = dr["filterField"].ToString();
                }
            }
            protected override Collection<string> GetRequiredColumnNamesCore()
            {
                return new Collection<string>() { myStyle.RenderLabel };
            }
            protected override void DrawCore(IEnumerable<Feature> features, GeoCanvas canvas, Collection<SimpleCandidate> labelsInThisLayer, Collection<SimpleCandidate> labelsInAllLayers)
            {
                Boolean bHasExclude = false;
                if (myStyle.ds != null)
                    if (myStyle.ds.Tables.Contains("dtExclude"))
                        if (myStyle.ds.Tables["dtExclude"].Rows.Count > 0)
                            bHasExclude = true;
                
                foreach (Feature feature in features)
                {
                    if (myStyle.ds.Tables.Contains("Render"))
                    {
                            if (bHasExclude)
                            {
                                DataRow drExclude = myStyle.ds.Tables["dtExclude"].Select(string.Format("name ='{0}'", feature.ColumnValues[myStyle.RenderLabel].ToString().Replace("'", "''"))).FirstOrDefault();
                                if (drExclude != null)
                                    continue;
                            }

                        DataRow[] dr = myStyle.ds.Tables["Render"].Select(string.Format("Descrip ='{0}'", feature.ColumnValues[myStyle.RenderLabel].ToString().Replace("'", "''")));
                        foreach (DataRow r in dr)
                        {
                            Boolean bContinue = false;
                            Double d = canvas.CurrentScale;
                            if (Double.TryParse(r["MaxZoom"].ToString(), out d))
                            {
                                if (canvas.CurrentScale <= d)
                                    bContinue = true;
                            }
                            else
                                bContinue = true;

                            if (!bContinue)
                                continue;

                            DataRow[] drRender = myStyle.ds.Tables["Render"].Select("Symbol = " + r["Symbol"].ToString(), "Order ASC");
                            foreach (DataRow render in drRender)
                            {
                                Color c = (Color)render["line_color"];
                                int colorCode = c.ToArgb();
                                string hexColor = "#" + colorCode.ToString("X2").Substring(2);
                                GeoColor geoColor = GeoColor.FromHtml(hexColor);
                                geoColor.AlphaComponent = c.A;

                                float lineWidth = (float)Convert.ToDouble(render["line_width"]);
                                GeoPen geoPen = new GeoPen(geoColor, lineWidth);

                                LineDashStyle lineDashStyle = LineDashStyle.Solid;
                                String lineStyle = render["line_style"].ToString().ToLower();
                                if (lineStyle.Length > 2)
                                {
                                    lineStyle = char.ToUpper(lineStyle[0]) + lineStyle.Substring(1);
                                    if (!Enum.TryParse(lineStyle, out lineDashStyle))
                                        lineDashStyle = LineDashStyle.Solid;
                                }
                                geoPen.DashStyle = lineDashStyle;

                                canvas.DrawLine(feature, geoPen, DrawingLevel.LevelOne);
                            }
                        }
                    }
                }
            }
        }

        class CustomPointStyle : PointStyle
        {
            myStyleData myStyle = new myStyleData();

            public CustomPointStyle(PointSymbolType symbolType, GeoSolidBrush symbolSolidBrush, int symbolSize, myStyleData myData) : base(symbolType, symbolSolidBrush, symbolSize)
            {
                myStyle = myData;
            }
            protected override Collection<string> GetRequiredColumnNamesCore()
            {
                return new Collection<string>() { myStyle.RenderLabel };
            }
            protected override void DrawCore(IEnumerable<Feature> features, GeoCanvas canvas, Collection<SimpleCandidate> labelsInThisLayer, Collection<SimpleCandidate> labelsInAllLayers)
            {
                Boolean bHasExclude = false;
                if (myStyle.ds != null)
                    if (myStyle.ds.Tables.Contains("dtExclude"))
                        if (myStyle.ds.Tables["dtExclude"].Rows.Count > 0)
                            bHasExclude = true;
                
                foreach (Feature feature in features)
                {
                    if (myStyle.ds.Tables.Contains("Symbol"))
                    {
                            if (bHasExclude)
                            {
                                DataRow drExclude = myStyle.ds.Tables["dtExclude"].Select(string.Format("name ='{0}'", feature.ColumnValues[myStyle.RenderLabel].ToString().Replace("'", "''"))).FirstOrDefault();
                                if (drExclude != null)
                                    continue;
                            }

                        DataRow[] dr = myStyle.ds.Tables["Symbol"].Select(string.Format("Descrip ='{0}'", feature.ColumnValues[myStyle.RenderLabel].ToString().Replace("'", "''")));
                        foreach (DataRow r in dr)
                        {
                            DataRow[] drRender = myStyle.ds.Tables["Render"].Select("Symbol = " + r["SymbolID"].ToString(), "Order ASC");
                            foreach (DataRow render in drRender)
                            {
                                Color c = (Color)render["color"];
                                int colorCode = c.ToArgb();
                                string hexColor = "#" + colorCode.ToString("X2").Substring(2);
                                GeoColor geoColor = GeoColor.FromHtml(hexColor);
                                geoColor.AlphaComponent = c.A;

                                float size = (float)Convert.ToDouble(render["size"]);

                                GeoPen geoPen = new GeoPen(geoColor, size);
                                canvas.DrawEllipse(feature, size, size, geoPen, DrawingLevel.LevelOne);
                            }
                        }
                    }
                }
            }
        }
        #endregion
    }
}