﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Xml;
using ThinkGeo.MapSuite;
using ThinkGeo.MapSuite.Drawing;
using ThinkGeo.MapSuite.Layers;
using ThinkGeo.MapSuite.Shapes;
using ThinkGeo.MapSuite.Styles;
using ThinkGeo.MapSuite.WebApi;
using System.Drawing;
using System.Collections.ObjectModel;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Data;
using NaveoWebMaps.Constants;

namespace NaveoWebMaps.Helpers
{
    public class EasyGisFileHelper
    {        
        #region NaveoMaps
        public LayerOverlay OpenNaveoMaps(int z, int x, int y, String CacheID)
        {
            Boolean b = XyzTileCacheHelper.TileExists(LayerPaths.tileCacheDirectory(), CacheID, x, y, z);
            if (b)
                return new LayerOverlay();

            XmlDocument doc = new XmlDocument();
            doc.Load(LayerPaths.BaseESRIMapsPath("webMarch2014.egp"));
            XmlElement prjElement = (XmlElement)doc.GetElementsByTagName("sfproject").Item(0);
            return ReadXml(prjElement, LayerPaths.BaseESRIMapsPath(String.Empty));
        }
        #endregion
        #region MokaMaps
        public LayerOverlay OpenMokaMaps(int z, int x, int y, String CacheID)
        {
            Boolean b = XyzTileCacheHelper.TileExists(LayerPaths.tileCacheDirectory(), CacheID, x, y, z);
            if (b)
                return new LayerOverlay();
            
            XmlDocument doc = new XmlDocument();
            doc.Load(LayerPaths.MokaAssetsPath("Reza.egp"));
            XmlElement prjElement = (XmlElement)doc.GetElementsByTagName("sfproject").Item(0);
            return ReadXml(prjElement, LayerPaths.MokaAssetsPath(String.Empty));
        }
        #endregion

        #region Reading egp files
        private LayerOverlay ReadXml(XmlElement projectElement, String sPath)
        {
            Proj4Projection proj4 = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetSphericalMercatorParametersString());
            proj4 = new Proj4Projection(4326, 3857);
            if (!proj4.IsOpen)
                proj4.Open();

            LayerOverlay layerOverlay = new LayerOverlay();
            XmlNodeList layerNodeList = projectElement.GetElementsByTagName("layers");
            XmlNodeList sfList = ((XmlElement)layerNodeList[0]).GetElementsByTagName("shapefile");
            if (sfList != null && sfList.Count > 0)
            {
                for (int n = 0; n < sfList.Count; n++)
                {
                    XmlElement element = (XmlElement)sfList[n];
                    string path = element.GetElementsByTagName("path")[0].InnerText;
                    string Name = element.GetElementsByTagName("name")[0].InnerText;

                    ShapeFileFeatureLayer shp = new ShapeFileFeatureLayer(sPath + Name);
                    shp.Name = path;
                    shp.ReadWriteMode = GeoFileReadWriteMode.Read;
                    shp.IndexPathFilename = sPath + path + ".idx";
                    shp.Encoding = GetStringEncoding(element);
                    shp.FeatureSource.Projection = proj4;

                    XmlNodeList renderList = element.GetElementsByTagName("renderer");
                    if (renderList != null && renderList.Count > 0)
                        shp.ZoomLevelSet = GetZoomLevelSet(element, sPath);

                    layerOverlay.Layers.Add(n.ToString(), shp);
                }
            }
            return layerOverlay;
        }
        private Encoding GetStringEncoding(XmlNode rootNode)
        {
            int codepage = int.Parse(rootNode.SelectSingleNode("renderer/StringEncoding").InnerText);
            var result = Encoding.GetEncoding(codepage);
            return result;
        }

        private ZoomLevelSet GetZoomLevelSet(XmlNode rootNode, String sPath)
        {
            ZoomLevelSet result = new ZoomLevelSet();
            System.Collections.ObjectModel.Collection<ZoomLevel> zoomLevels = result.GetZoomLevels();

            int minZoomLevel = GetMinZoomLevel(rootNode);
            int maxZoomLevel = GetMaxZoomLevel(rootNode);
            int minLabelZoomLevel = GetMinRenderLabelZoom(rootNode);

            for (int i = 0; i < zoomLevels.Count; i++)
            {
                if (i - 1 >= minZoomLevel && i - 1 <= maxZoomLevel)
                {
                    zoomLevels[i].DefaultPointStyle = GetPointStyle(rootNode, sPath);
                    zoomLevels[i].DefaultLineStyle = GetLineStyle(rootNode, i);
                    zoomLevels[i].DefaultAreaStyle = GetAreaStyle(rootNode);
                    zoomLevels[i].DefaultTextStyle = GetTextStyle(rootNode);
                }
            }

            return result;
        }
        private int GetMinZoomLevel(XmlNode rootNode)
        {
            // TODO : we need to calculate the zoom level value
            int result = int.Parse(rootNode.SelectSingleNode("renderer/MinZoomLevel").InnerText);
            //if (result < 0 || result > 19)
            //    result = 0;

            if (result < 0)
                result = 0;
            else if (result == 8000)
                result = result = 12;
            else if (result == 10000)
                result = result = 13;
            else if (result == 67000)
                result = result = 16;

            return result;
        }
        private int GetMaxZoomLevel(XmlNode rootNode)
        {
            // TODO : we need to calculate the zoom level value
            int result = int.Parse(rootNode.SelectSingleNode("renderer/MaxZoomLevel").InnerText);
            //if (result < 0 || result > 19)
            //    result = 19;

            if (result < 0)
                result = 19;
            else if (result == 15000)
                result = 14;
            else if (result == 20000)
                result = 15;
            else if (result == 200000)
                result = 20;

            return result;
        }
        private int GetMinRenderLabelZoom(XmlNode rootNode)
        {
            // TODO : we need to calculate the zoom level value
            int result = int.Parse(rootNode.SelectSingleNode("renderer/MinRenderLabelZoom").InnerText);
            if (result < 0 || result > 19)
            {
                result = 20;
            }

            return result;
        }
        private PointStyle GetPointStyle(XmlNode rootNode, String sPath)
        {
            // TODO : check color of symbolSolidBrush and width of symbolPen
            PointStyle result = new PointStyle();
            String icon = null;// GetIcon(rootNode);
            if (icon != null)
            {
                GeoImage g = new GeoImage(sPath + icon);
                result = new PointStyle(g);
            }
            else
            {
                PointSymbolType symbolType = PointSymbolType.Circle;
                GeoSolidBrush symbolSolidBrush = new GeoSolidBrush(GetFillColor(rootNode));
                GeoPen symbolPen = new GeoPen(new GeoSolidBrush(GetOutlineColor(rootNode)), 1f);
                int symbolSize = GetPointSize(rootNode);

                result = new PointStyle(symbolType, symbolSolidBrush, symbolPen, symbolSize);
            }
            return result;
        }

        private GeoColor GetFillColor(XmlNode rootNode)
        {
            string colorText = rootNode.SelectSingleNode("renderer/FillColor").InnerText;
            if (colorText.StartsWith("#") == false)
            {
                colorText = ColorTranslator.ToHtml(Color.FromName(colorText));
            }

            var result = GeoColor.FromHtml(colorText);
            return result;
        }
        private GeoColor GetOutlineColor(XmlNode rootNode)
        {
            string colorText = rootNode.SelectSingleNode("renderer/OutlineColor").InnerText;
            if (colorText.StartsWith("#") == false)
            {
                colorText = ColorTranslator.ToHtml(Color.FromName(colorText));
            }

            var result = GeoColor.FromHtml(colorText);
            return result;
        }
        private int GetPointSize(XmlNode rootNode)
        {
            int result = int.Parse(rootNode.SelectSingleNode("renderer/PointSize").InnerText);
            return result;
        }
        private string GetIcon(XmlNode rootNode)
        {
            String result = null;
            XmlNode xmlnode = rootNode.SelectSingleNode("renderer/PointImageSymbol");
            if (xmlnode != null)
                result = xmlnode.InnerText;
            if (string.IsNullOrEmpty(result) || string.IsNullOrEmpty(result.Trim()))
                result = null;

            return result;
        }


        private LineStyle GetLineStyle(XmlNode rootNode, int ZoomLvl)
        {
            //Roads width
            //layer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = new LineStyle(outerPen, innerPen, centerPen);
            //layer.ZoomLevelSet.ZoomLevel01.DefaultLineStyle = style1;
            //layer.ZoomLevelSet.ZoomLevel01.ApplyUntilZoomLevel = ZoomLevels10;

            //layer.ZoomLevelSet.ZoomLevel11.DefaultLineStyle = style2;
            //layer.ZoomLevelSet.ZoomLevel11.ApplyUntilZoomLevel = ZoomLevels15;


            // TODO : check width of outerPen
            float penWidthScale = (PenWidthScale(rootNode) * 10000) + ZoomLvl - 10;
            GeoPen outerPen = new GeoPen(new GeoSolidBrush(GetOutlineColor(rootNode)), penWidthScale);
            GeoPen innerPen = new GeoPen(new GeoSolidBrush(GetFillColor(rootNode)), penWidthScale);
            GeoPen centerPen = new GeoPen(new GeoSolidBrush(GetFillColor(rootNode)), penWidthScale - 1);

            LineStyle result = new LineStyle(outerPen);
            //result.InnerPen = innerPen;
            result.CenterPen = centerPen;
            return result;
        }
        private AreaStyle GetAreaStyle(XmlNode rootNode)
        {
            // TODO : check width of outlinePen
            GeoPen outlinePen = new GeoPen(new GeoSolidBrush(GetOutlineColor(rootNode)), 1f);
            GeoSolidBrush fillSolidBrush = new GeoSolidBrush(GetFillColor(rootNode));

            AreaStyle result = new AreaStyle();
            result.OutlinePen = outlinePen;
            if (FillInterior(rootNode))
                result.FillSolidBrush = fillSolidBrush;

            return result;
        }

        private Boolean FillInterior(XmlNode rootNode)
        {
            string val = rootNode.SelectSingleNode("renderer/FillInterior").InnerText;
            if (val.ToLower() == "true")
                return true;

            return false;
        }
        private float PenWidthScale(XmlNode rootNode)
        {
            string PenWidthScale = rootNode.SelectSingleNode("renderer/PenWidthScale").InnerText;
            float f = 1f;
            float.TryParse(PenWidthScale, out f);

            return f;
        }


        private TextStyle GetTextStyle(XmlNode rootNode)
        {
            TextStyle result = null;
            string fieldName = GetFieldName(rootNode);
            if (string.IsNullOrEmpty(fieldName) == false)
            {
                var textFont = GetFont(rootNode);
                var textSolidBrush = new GeoSolidBrush(GetFontColor(rootNode));

                // TODO : check Duplicate and Overlapping props of TextStyle
                result = new TextStyle(fieldName, textFont, textSolidBrush);
                result.DuplicateRule = LabelDuplicateRule.NoDuplicateLabels;
                result.GridSize = 1000;
                result.OverlappingRule = LabelOverlappingRule.NoOverlapping;

                if (GetShadowText(rootNode))
                {
                    var color = GeoColor.FromArgb(textSolidBrush.Color.AlphaComponent / 2, textSolidBrush.Color);

                    // TODO : check color and width of HaloPen
                    result.HaloPen = new GeoPen(color, 1f);
                }
            }

            return result;
        }
        private string GetFieldName(XmlNode rootNode)
        {
            string result = rootNode.SelectSingleNode("renderer/FieldName").InnerText;
            if (string.IsNullOrEmpty(result) || string.IsNullOrEmpty(result.Trim()))
            {
                result = null;
            }

            return result;
        }
        private GeoFont GetFont(XmlNode rootNode)
        {
            // TODO: check the DrawingGraphicsUnit
            var fontNode = rootNode.SelectSingleNode("renderer/Font");

            float emSize = float.Parse(fontNode.Attributes["Size"].Value);
            string familyName = fontNode.Attributes["Name"].Value;
            var style = (DrawingFontStyles)Enum.Parse(
                typeof(DrawingFontStyles),
                fontNode.Attributes["Style"].Value
            );

            var font = new GeoFont(familyName, emSize, style, DrawingGraphicsUnit.Pixel);
            return font;
        }
        private GeoColor GetFontColor(XmlNode rootNode)
        {
            string colorText = rootNode.SelectSingleNode("renderer/FontColor").InnerText;
            if (colorText.StartsWith("#") == false)
            {
                colorText = ColorTranslator.ToHtml(Color.FromName(colorText));
            }

            var result = GeoColor.FromHtml(colorText);
            return result;
        }
        private bool GetShadowText(XmlNode rootNode)
        {
            bool result = bool.Parse(rootNode.SelectSingleNode("renderer/ShadowText").InnerText);
            return result;
        }
        #endregion

        #region Custom shape styles
        class CustomAreaStyle : AreaStyle
        {
            public CustomAreaStyle(GeoPen outlinePen) : base(outlinePen) { }

            protected override Collection<string> GetRequiredColumnNamesCore()
            {
                return new Collection<string>() { "zonecolor" };
            }
            protected override void DrawCore(IEnumerable<Feature> features, GeoCanvas canvas, Collection<SimpleCandidate> labelsInThisLayer, Collection<SimpleCandidate> labelsInAllLayers)
            {
                //base.DrawCore(features, canvas, labelsInThisLayer, labelsInAllLayers);
                foreach (Feature feature in features)
                {
                    Color c = Color.DarkRed;
                    int colorCode = c.ToArgb();
                    if (!int.TryParse(feature.ColumnValues["zonecolor"], out colorCode))
                        colorCode = c.ToArgb();
                    string hexColor = "#" + colorCode.ToString("X2").Substring(2);

                    GeoColor color = GeoColor.FromHtml(hexColor);
                    color.AlphaComponent = 125; //(byte)Math.Round(0.204 * 255); //Transparency

                    //base.FillSolidBrush.Color = color;
                    //base.OutlinePen.Color = GeoColor.FromHtml(hexColor);
                    //base.DrawCore(new Feature[] { feature }, canvas, labelsInThisLayer, labelsInAllLayers);
                    canvas.DrawArea(feature, new GeoPen(GeoColor.FromHtml(hexColor), base.OutlinePen.Width), new GeoSolidBrush(color), DrawingLevel.LevelOne);
                }
            }
        }
        #endregion

        #region Indexes
        public void BuildIndex()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(LayerPaths.MokaAssetsPath("EASYGIS-MOKA ASSETS PLOT.egp"));
            //doc.Load(BaseESRIMapsPath("webMarch2014.egp"));
            XmlElement prjElement = (XmlElement)doc.GetElementsByTagName("sfproject").Item(0);
            BuildIndexFromXml(prjElement, LayerPaths.MokaAssetsPath(String.Empty));
        }
        private void BuildIndexFromXml(XmlElement projectElement, String sPath)
        {
            XmlNodeList colorList = projectElement.GetElementsByTagName("MapBackColor");

            XmlNodeList layerNodeList = projectElement.GetElementsByTagName("layers");
            XmlNodeList sfList = ((XmlElement)layerNodeList[0]).GetElementsByTagName("shapefile");
            if (sfList != null && sfList.Count > 0)
            {
                for (int n = 0; n < sfList.Count; n++)
                {
                    XmlElement element = (XmlElement)sfList[n];
                    string Name = element.GetElementsByTagName("name")[0].InnerText;
                    ShapeFileFeatureLayer.BuildIndexFile(sPath + Name, BuildIndexMode.Rebuild);
                }
            }
        }
        #endregion
    }
}