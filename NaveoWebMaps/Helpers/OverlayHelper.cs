﻿using NaveoWebMaps.Constants;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using ThinkGeo.MapSuite;
using ThinkGeo.MapSuite.Drawing;
using ThinkGeo.MapSuite.Layers;
using ThinkGeo.MapSuite.Shapes;
using ThinkGeo.MapSuite.WebApi;

namespace NaveoWebMaps.Helpers
{
    public static class OverlayHelper
    {
        public static HttpResponseMessage DrawLayerOverlay(LayerOverlay layerOverlay, int z, int x, int y, String CacheID, Boolean enableTileCache)
        {
            Bitmap bitmap = null;
            if (enableTileCache == true)
                bitmap = XyzTileCacheHelper.GetTile(LayerPaths.tileCacheDirectory(), CacheID, x, y, z);

            if (bitmap == null)
            {
                if (layerOverlay.Layers.Count == 0)
                    return new HttpResponseMessage(HttpStatusCode.OK);

                int tileSize = XyzTileCacheHelper.tileSize;// 256;
                bitmap = new Bitmap(tileSize, tileSize);
                PlatformGeoCanvas geoCanvas = new PlatformGeoCanvas();
                ZoomLevelSet zoomLevelSet = new CustomZoomLevelSet();
                RectangleShape boundingBox = WebApiExtentHelper.GetBoundingBoxForXyz(x, y, z, GeographyUnit.Meter, zoomLevelSet);
                geoCanvas.BeginDrawing(bitmap, boundingBox, GeographyUnit.Meter);
                layerOverlay.Draw(geoCanvas);
                geoCanvas.EndDrawing();

                if (enableTileCache == true)
                {
                    Task.Factory.StartNew((obj) =>
                    {
                        var image = obj as Bitmap;
                        XyzTileCacheHelper.SaveTile(image, LayerPaths.tileCacheDirectory(), CacheID, x, y, z);
                        image.Dispose();
                    }, bitmap.Clone());
                }
            }

            MemoryStream ms = new MemoryStream();
            bitmap.Save(ms, ImageFormat.Png);
            bitmap.Dispose();

            HttpResponseMessage msg = new HttpResponseMessage(HttpStatusCode.OK);
            msg.Content = new ByteArrayContent(ms.ToArray());
            msg.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");

            return msg;
        }
    }

    public class CustomZoomLevelSet : ZoomLevelSet
    {
        public CustomZoomLevelSet()
        {
            double worldWidth = 20037508.2314698 * 2;
            int tileSize = XyzTileCacheHelper.tileSize;
            for (int zoom = 0; zoom <= 25; zoom++)
            {
                double resolution = worldWidth / (tileSize * Math.Pow(2, zoom));
                double scale = 39.3701 * 96 * resolution;
                CustomZoomLevels.Add(new ZoomLevel(scale));
            }
        }
    }
}