﻿using NaveoWebMaps.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using ThinkGeo.MapSuite;
using ThinkGeo.MapSuite.Drawing;
using ThinkGeo.MapSuite.Layers;
using ThinkGeo.MapSuite.Shapes;
using ThinkGeo.MapSuite.Styles;
using ThinkGeo.MapSuite.WebApi;

namespace NaveoWebMaps.Helpers
{
    public class RasterFileHelper
    {
        String MapsPath(String Filename)
        {
            return HttpContext.Current.Server.MapPath(@"~/MapData/RasterImages/" + Filename);
        }

        public LayerOverlay Moka()
        {
            LayerOverlay layerOverlay = new LayerOverlay();

            Proj4Projection proj4 = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetSphericalMercatorParametersString());
            proj4 = new Proj4Projection(32740, 3857);
            if (!proj4.IsOpen) proj4.Open();

            GeoTiffRasterLayer layer = new GeoTiffRasterLayer(MapsPath("LEsperance Village-V2_transparent_mosaic_group1.tif"));
            layer.Name = "EsperanceVillage";
            layer.ImageSource.Projection = proj4;
            //layer.Transparency = 150;
            layerOverlay.Layers.Add(layer.Name, layer);

            return layerOverlay;
        }

        public LayerOverlay openImage(int alpha, String fileName, int z, int x, int y, String CacheID)
        {
            Boolean b = XyzTileCacheHelper.TileExists(LayerPaths.tileCacheDirectory(), CacheID, x, y, z);
            if (b)
                return new LayerOverlay();
            
            LayerOverlay layerOverlay = new LayerOverlay();
            Layer layer = OpenImage(MapsPath(String.Empty) + "Others/" + fileName, alpha);
            layerOverlay.Layers.Add(layer.Name, layer);

            return layerOverlay;
        }

        public LayerOverlay MokaDrone(int alpha, int z, int x, int y, String CacheID)
        {
            LayerOverlay layerOverlay = new LayerOverlay();
            Boolean b = XyzTileCacheHelper.TileExists(LayerPaths.tileCacheDirectory(), CacheID, x, y, z);
            if (b)
                return new LayerOverlay();
            
            String sPath = MapsPath(String.Empty) + "MokaDrone\\";
            string[] files = Directory.GetFiles(sPath);
            foreach (String f in files)
            {
                Layer layer = OpenImage(f, alpha);
                layerOverlay.Layers.Add(layer.Name, layer);
            }
            return layerOverlay;
        }

        public Layer OpenImage(String sPath, int alpha)
        {
            Layer layer = null;
            try
            {
                Proj4Projection proj4 = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetSphericalMercatorParametersString());
                proj4 = new Proj4Projection(32740, 3857);
                if (!proj4.IsOpen) proj4.Open();

                switch (Path.GetExtension(sPath).ToLower())
                {
                    case ".ecw":
                        EcwRasterLayer ewclayer = new CustomEcwRasterLayer(sPath);
                        ewclayer.Name = Path.GetFileNameWithoutExtension(sPath);
                        ewclayer.ImageSource.Projection = proj4;

                        layer = ewclayer;
                        break;

                    case ".tif":
                        CustomGeoTiffRasterLayer geoTiff = new CustomGeoTiffRasterLayer(sPath);
                        geoTiff.Name = Path.GetFileNameWithoutExtension(sPath);
                        geoTiff.ImageSource.Projection = proj4;

                        layer = geoTiff;
                        break;

                    case ".png":
                        Jpeg2000RasterLayer jpg = new Jpeg2000RasterLayer(sPath);
                        jpg.Name = Path.GetFileNameWithoutExtension(sPath);
                        jpg.ImageSource.Projection = new Proj4Projection(32740, 4326);
                        layer = jpg;
                        break;

                    default:
                        NativeImageRasterLayer native = new NativeImageRasterLayer(sPath);
                        //native.Name = Path.GetFileNameWithoutExtension(sPath);
                        //native.ImageSource.Projection = new Proj4Projection(32740, 4326);
                        
                        //layer = native;
                        break;
                }
                if (layer != null)
                    layer.Transparency = alpha;
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorToLog(ex.ToString(), "RasterFileHelper_OpenImage");
            }
            return layer;
        }

        private LayerOverlay NativeImage()
        {
            LayerOverlay layerOverlay = new LayerOverlay();

            Proj4Projection proj4 = new Proj4Projection(Proj4Projection.GetWgs84ParametersString(), Proj4Projection.GetSphericalMercatorParametersString());
            proj4 = new Proj4Projection(32740, 3857);
            if (!proj4.IsOpen) proj4.Open();

            NativeImageRasterLayer layer = new NativeImageRasterLayer("");
            layerOverlay.Layers.Add("layer", layer);
            return layerOverlay;
        }
    }

    public class CustomGeoTiffRasterLayer : GeoTiffRasterLayer
    {
        public CustomGeoTiffRasterLayer(string sPath) : base(sPath) { }
        protected override void DrawCore(GeoCanvas geoCanvas, Collection<SimpleCandidate> labelsInAllLayers)
        {
            var bufferWidth = geoCanvas.CurrentWorldExtent.Width / 4;
            var bufferHeight = geoCanvas.CurrentWorldExtent.Height / 4;
            var newUpperLeftX = geoCanvas.CurrentWorldExtent.UpperLeftPoint.X - bufferWidth;
            var newUpperLeftY = geoCanvas.CurrentWorldExtent.UpperLeftPoint.Y + bufferHeight;
            var newLowerRightX = geoCanvas.CurrentWorldExtent.LowerRightPoint.X + bufferWidth;
            var newLowerRightY = geoCanvas.CurrentWorldExtent.LowerRightPoint.Y - bufferHeight;
            var bufferExtent = new RectangleShape(newUpperLeftX, newUpperLeftY, newLowerRightX, newLowerRightY);

            Bitmap bitmap = new Bitmap(384, 384);
            PlatformGeoCanvas canvas = new PlatformGeoCanvas();
            canvas.BeginDrawing(bitmap, bufferExtent, GeographyUnit.Meter);
            base.DrawCore(canvas, labelsInAllLayers);
            canvas.EndDrawing();
            using (GeoImage geoImage = new GeoImage(bitmap))
            {
                geoCanvas.DrawScreenImageWithoutScaling(geoImage, 128, 128, DrawingLevel.LevelOne, 0, 0, 0);
            }
        }
    }
    public class CustomEcwRasterLayer : EcwRasterLayer
    {
        public CustomEcwRasterLayer(string sPath) : base(sPath) { }
        protected override void DrawCore(GeoCanvas geoCanvas, Collection<SimpleCandidate> labelsInAllLayers)
        {
            var bufferWidth = geoCanvas.CurrentWorldExtent.Width / 4;
            var bufferHeight = geoCanvas.CurrentWorldExtent.Height / 4;
            var newUpperLeftX = geoCanvas.CurrentWorldExtent.UpperLeftPoint.X - bufferWidth;
            var newUpperLeftY = geoCanvas.CurrentWorldExtent.UpperLeftPoint.Y + bufferHeight;
            var newLowerRightX = geoCanvas.CurrentWorldExtent.LowerRightPoint.X + bufferWidth;
            var newLowerRightY = geoCanvas.CurrentWorldExtent.LowerRightPoint.Y - bufferHeight;
            var bufferExtent = new RectangleShape(newUpperLeftX, newUpperLeftY, newLowerRightX, newLowerRightY);

            Bitmap bitmap = new Bitmap(384, 384);
            PlatformGeoCanvas canvas = new PlatformGeoCanvas();
            canvas.BeginDrawing(bitmap, bufferExtent, GeographyUnit.Meter);
            base.DrawCore(canvas, labelsInAllLayers);
            canvas.EndDrawing();
            using (GeoImage geoImage = new GeoImage(bitmap))
            {
                geoCanvas.DrawScreenImageWithoutScaling(geoImage, 128, 128, DrawingLevel.LevelOne, 0, 0, 0);
            }
        }
    }
}