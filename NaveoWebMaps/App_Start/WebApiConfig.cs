﻿using NaveoWebMaps.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;

namespace NaveoWebMaps
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //Error Handling
            config.Services.Replace(typeof(IExceptionLogger), new UnhandledExceptionLogger());

            //Logging.
            string enableApiLogging = ConfigurationManager.AppSettings["enableApiLogging"];
            if (enableApiLogging == "yes")
                config.MessageHandlers.Add(new CustomLogHandler());


            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { controller = "Account", action = "Login", id = RouteParameter.Optional }
            );
        }
    }

    #region Error Handling
    public class UnhandledExceptionLogger : ExceptionLogger
    {
        public override void Log(ExceptionLoggerContext context)
        {
            String log = context.Exception.ToString();
            LogHelper.WriteErrorToLog(log, "WebApiConfig_UnhandledException");
        }
    }
    #endregion

    #region Logging
    public class LogMetadata
    {
        public string RequestContentType { get; set; }
        public string RequestUri { get; set; }
        public string RequestMethod { get; set; }
        public DateTime? RequestTimestamp { get; set; }
        public string ResponseContentType { get; set; }
        public System.Net.HttpStatusCode ResponseStatusCode { get; set; }
        public DateTime? ResponseTimestamp { get; set; }
    }

    public class CustomLogHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var logMetadata = BuildRequestMetadata(request);
            var response = await base.SendAsync(request, cancellationToken);
            logMetadata = BuildResponseMetadata(logMetadata, response);
            SendToLog(logMetadata);
            return response;
        }
        private LogMetadata BuildRequestMetadata(HttpRequestMessage request)
        {
            LogMetadata log = new LogMetadata
            {
                RequestMethod = request.Method.Method,
                RequestTimestamp = DateTime.Now,
                RequestUri = request.RequestUri.ToString()
            };
            return log;
        }
        private LogMetadata BuildResponseMetadata(LogMetadata logMetadata, HttpResponseMessage response)
        {
            logMetadata.ResponseStatusCode = response.StatusCode;
            logMetadata.ResponseTimestamp = DateTime.Now;
            if (response.Content != null)
                logMetadata.ResponseContentType = response.Content.Headers.ContentType.MediaType;
            return logMetadata;
        }
        private bool SendToLog(LogMetadata logMetadata)
        {
            if (logMetadata.RequestUri.Contains("HasFileToDwn"))
                return true;

            // TODO: Write code here to store the logMetadata instance to a pre-configured log store...
            String log = "WebApiConfig_Log \r\n";
            log += "RequestContentType : " + logMetadata.RequestContentType + "\r\n";
            log += "RequestUri : " + logMetadata.RequestUri + "\r\n";
            log += "RequestMethod : " + logMetadata.RequestMethod + "\r\n";
            if (logMetadata.RequestTimestamp.HasValue)
                log += "RequestTimestamp : " + logMetadata.RequestTimestamp.ToString() + "\r\n";
            log += "ResponseContentType : " + logMetadata.ResponseContentType + "\r\n";
            log += "ResponseStatusCode : " + logMetadata.ResponseStatusCode + "\r\n";
            if (logMetadata.ResponseTimestamp.HasValue)
                log += "ResponseTimestamp : " + logMetadata.ResponseTimestamp + "\r\n";
            LogHelper.WriteToLog(log);
            return true;
        }
    }
    #endregion

}
