﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoWebMaps.Constants
{
    public static class LayerPaths
    {
        public static string tileCacheDirectory()
        {
            if (String.IsNullOrEmpty(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath))
                return HttpContext.Current.Server.MapPath(@"~/MapData/Cache/");

            return System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + @"/MapData/Cache/";
        }
        public static String BaseESRIMapsPath(String Filename)
        {
            return HttpContext.Current.Server.MapPath(@"~/MapData/BaseESRIMaps/" + Filename);
        }
        public static String MokaAssetsPath(String Filename)
        {
            return HttpContext.Current.Server.MapPath(@"~/MapData/MokaAssets/" + Filename);
        }
        public static String QGISPath(String Filename)
        {
            return HttpContext.Current.Server.MapPath(@"~/MapData/qgis/" + Filename);//was qgis  QGISMDC2019
        }
        public static String RoutingPath(String Filename)
        {
            return HttpContext.Current.Server.MapPath(@"~/MapData/RoutingData/" + Filename);
        }
        public static String ZonePath(String Filename)
        {
            return HttpContext.Current.Server.MapPath(@"~/MapData/UserData/Zones/" + Filename);
        }
        public static String TripPath(String Filename)
        {
            return HttpContext.Current.Server.MapPath(@"~/MapData/UserData/Trips/" + Filename);
        }
        public static String RoutingTmpPath(String Filename)
        {
            return HttpContext.Current.Server.MapPath(@"~/MapData/UserData/RoutingData/" + Filename);
        }
        public static String BLUPPath(String Filename)
        {
            return HttpContext.Current.Server.MapPath(@"~/MapData/UserData/BLUP/" + Filename);
        }
        public static String QGISTmpPath(String Filename)
        {
            return HttpContext.Current.Server.MapPath(@"~/MapData/UserData/QGIS/" + Filename);
        }
        public static String iconsPath(String Filename)
        {
            return HttpContext.Current.Server.MapPath(@"~/MapData/Icons/" + Filename);
        }
        public static String FixedAssetsPath(String Filename)
        {
            return HttpContext.Current.Server.MapPath(@"~/MapData/UserData/FixedAssets/" + Filename);
        }
        public static String logPath(String Filename)
        {
            return HttpContext.Current.Server.MapPath(@"~/MapData/Log/" + Filename);
        }
        public static String BlupShpPath(String Filename)
        {
            if (String.IsNullOrEmpty(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath))
                return (@"C:\Reza\Geo\SDK\NaveoOneWebV2\NaveoWebMaps\MapData\Blup\" + Filename);
            return HttpContext.Current.Server.MapPath(@"~/MapData/Blup/" + Filename);
        }
    }
}