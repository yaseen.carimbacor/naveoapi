﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NaveoWebMaps.Constants
{
    public static class Headers
    {
        public static readonly Guid DEFAULT_GUID = new Guid("00000000-0000-0000-0000-000000000000");
        public const string USER_TOKEN = "USER_TOKEN";
    }
}